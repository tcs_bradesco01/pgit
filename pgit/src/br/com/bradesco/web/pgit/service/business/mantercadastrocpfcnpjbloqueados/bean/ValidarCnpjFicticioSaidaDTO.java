/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ValidarCnpjFicticioSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ValidarCnpjFicticioSaidaDTO {
	
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdContratoPerfil. */
    private Long cdContratoPerfil;
    
    /** Atributo cdCnpjCpf. */
    private Long cdCnpjCpf;
    
    /** Atributo cdFilialCpfCnpj. */
    private Integer cdFilialCpfCnpj;
    
    /** Atributo cdCtrlCpfCnpj. */
    private Integer cdCtrlCpfCnpj;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
    
    /**
     * Get: cpfCnpjFormatado.
     *
     * @return cpfCnpjFormatado
     */
    public String getCpfCnpjFormatado(){
    	return CpfCnpjUtils.formatarCpfCnpj(cdCnpjCpf, cdFilialCpfCnpj,cdCtrlCpfCnpj);
    }
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdContratoPerfil.
	 *
	 * @return cdContratoPerfil
	 */
	public Long getCdContratoPerfil() {
		return cdContratoPerfil;
	}
	
	/**
	 * Set: cdContratoPerfil.
	 *
	 * @param cdContratoPerfil the cd contrato perfil
	 */
	public void setCdContratoPerfil(Long cdContratoPerfil) {
		this.cdContratoPerfil = cdContratoPerfil;
	}
	
	/**
	 * Get: cdCnpjCpf.
	 *
	 * @return cdCnpjCpf
	 */
	public Long getCdCnpjCpf() {
		return cdCnpjCpf;
	}
	
	/**
	 * Set: cdCnpjCpf.
	 *
	 * @param cdCnpjCpf the cd cnpj cpf
	 */
	public void setCdCnpjCpf(Long cdCnpjCpf) {
		this.cdCnpjCpf = cdCnpjCpf;
	}
	
	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	
	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	
	/**
	 * Get: cdCtrlCpfCnpj.
	 *
	 * @return cdCtrlCpfCnpj
	 */
	public Integer getCdCtrlCpfCnpj() {
		return cdCtrlCpfCnpj;
	}
	
	/**
	 * Set: cdCtrlCpfCnpj.
	 *
	 * @param cdCtrlCpfCnpj the cd ctrl cpf cnpj
	 */
	public void setCdCtrlCpfCnpj(Integer cdCtrlCpfCnpj) {
		this.cdCtrlCpfCnpj = cdCtrlCpfCnpj;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

}
