/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: InscricaoFavorecidosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class InscricaoFavorecidosSaidaDTO {

	/** Atributo cdTipoInscricaoFavorecidos. */
	private Integer cdTipoInscricaoFavorecidos;
	
	/** Atributo dsTipoInscricaoFavorecidos. */
	private String dsTipoInscricaoFavorecidos;
	
	/**
	 * Inscricao favorecidos saida dto.
	 */
	public InscricaoFavorecidosSaidaDTO() {
		super();
	}

	/**
	 * Inscricao favorecidos saida dto.
	 *
	 * @param cdTipoInscricaoFavorecidos the cd tipo inscricao favorecidos
	 * @param dsTipoInscricaoFavorecidos the ds tipo inscricao favorecidos
	 */
	public InscricaoFavorecidosSaidaDTO(Integer cdTipoInscricaoFavorecidos, String dsTipoInscricaoFavorecidos) {
		super();
		this.cdTipoInscricaoFavorecidos = cdTipoInscricaoFavorecidos;
		this.dsTipoInscricaoFavorecidos = dsTipoInscricaoFavorecidos;
	}

	/**
	 * Get: cdTipoInscricaoFavorecidos.
	 *
	 * @return cdTipoInscricaoFavorecidos
	 */
	public Integer getCdTipoInscricaoFavorecidos() {
		return cdTipoInscricaoFavorecidos;
	}

	/**
	 * Set: cdTipoInscricaoFavorecidos.
	 *
	 * @param cdTipoInscricaoFavorecidos the cd tipo inscricao favorecidos
	 */
	public void setCdTipoInscricaoFavorecidos(Integer cdTipoInscricaoFavorecidos) {
		this.cdTipoInscricaoFavorecidos = cdTipoInscricaoFavorecidos;
	}

	/**
	 * Get: dsTipoInscricaoFavorecidos.
	 *
	 * @return dsTipoInscricaoFavorecidos
	 */
	public String getDsTipoInscricaoFavorecidos() {
		return dsTipoInscricaoFavorecidos;
	}

	/**
	 * Set: dsTipoInscricaoFavorecidos.
	 *
	 * @param dsTipoInscricaoFavorecidos the ds tipo inscricao favorecidos
	 */
	public void setDsTipoInscricaoFavorecidos(String dsTipoInscricaoFavorecidos) {
		this.dsTipoInscricaoFavorecidos = dsTipoInscricaoFavorecidos;
	}
	
}
