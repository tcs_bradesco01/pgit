/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarDuplicacaoArqRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarDuplicacaoArqRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridContrato
     */
    private long _cdPessoaJuridContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridContrato
     */
    private boolean _has_cdPessoaJuridContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdTipoArquivoRetorno
     */
    private int _cdTipoArquivoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoArquivoRetorno
     */
    private boolean _has_cdTipoArquivoRetorno;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarDuplicacaoArqRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.request.ListarDuplicacaoArqRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridContrato
     * 
     */
    public void deleteCdPessoaJuridContrato()
    {
        this._has_cdPessoaJuridContrato= false;
    } //-- void deleteCdPessoaJuridContrato() 

    /**
     * Method deleteCdTipoArquivoRetorno
     * 
     */
    public void deleteCdTipoArquivoRetorno()
    {
        this._has_cdTipoArquivoRetorno= false;
    } //-- void deleteCdTipoArquivoRetorno() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdPessoaJuridContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridContrato'.
     */
    public long getCdPessoaJuridContrato()
    {
        return this._cdPessoaJuridContrato;
    } //-- long getCdPessoaJuridContrato() 

    /**
     * Returns the value of field 'cdTipoArquivoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoArquivoRetorno'.
     */
    public int getCdTipoArquivoRetorno()
    {
        return this._cdTipoArquivoRetorno;
    } //-- int getCdTipoArquivoRetorno() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdPessoaJuridContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridContrato()
    {
        return this._has_cdPessoaJuridContrato;
    } //-- boolean hasCdPessoaJuridContrato() 

    /**
     * Method hasCdTipoArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoArquivoRetorno()
    {
        return this._has_cdTipoArquivoRetorno;
    } //-- boolean hasCdTipoArquivoRetorno() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridContrato'.
     * 
     * @param cdPessoaJuridContrato the value of field
     * 'cdPessoaJuridContrato'.
     */
    public void setCdPessoaJuridContrato(long cdPessoaJuridContrato)
    {
        this._cdPessoaJuridContrato = cdPessoaJuridContrato;
        this._has_cdPessoaJuridContrato = true;
    } //-- void setCdPessoaJuridContrato(long) 

    /**
     * Sets the value of field 'cdTipoArquivoRetorno'.
     * 
     * @param cdTipoArquivoRetorno the value of field
     * 'cdTipoArquivoRetorno'.
     */
    public void setCdTipoArquivoRetorno(int cdTipoArquivoRetorno)
    {
        this._cdTipoArquivoRetorno = cdTipoArquivoRetorno;
        this._has_cdTipoArquivoRetorno = true;
    } //-- void setCdTipoArquivoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarDuplicacaoArqRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.request.ListarDuplicacaoArqRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.request.ListarDuplicacaoArqRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.request.ListarDuplicacaoArqRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.request.ListarDuplicacaoArqRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
