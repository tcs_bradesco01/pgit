/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPendencia
     */
    private long _cdPendencia = 0;

    /**
     * keeps track of state for field: _cdPendencia
     */
    private boolean _has_cdPendencia;

    /**
     * Field _dsPendenciaPagamentoIntegrado
     */
    private java.lang.String _dsPendenciaPagamentoIntegrado;

    /**
     * Field _nrPendenciaContratoNegocio
     */
    private long _nrPendenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrPendenciaContratoNegocio
     */
    private boolean _has_nrPendenciaContratoNegocio;

    /**
     * Field _dtGeracaoPendencia
     */
    private java.lang.String _dtGeracaoPendencia;

    /**
     * Field _hrGeracaoPendencia
     */
    private java.lang.String _hrGeracaoPendencia;

    /**
     * Field _dsTipoUnidadeOrganizacional
     */
    private java.lang.String _dsTipoUnidadeOrganizacional;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsUnidadeOrganizacional
     */
    private java.lang.String _dsUnidadeOrganizacional;

    /**
     * Field _cdTipoBaixa
     */
    private int _cdTipoBaixa = 0;

    /**
     * keeps track of state for field: _cdTipoBaixa
     */
    private boolean _has_cdTipoBaixa;

    /**
     * Field _dsTipoBaixa
     */
    private java.lang.String _dsTipoBaixa;

    /**
     * Field _cdSituacaoPendenciaContrato
     */
    private int _cdSituacaoPendenciaContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoPendenciaContrato
     */
    private boolean _has_cdSituacaoPendenciaContrato;

    /**
     * Field _dsSituacaoContrato
     */
    private java.lang.String _dsSituacaoContrato;

    /**
     * Field _dtBaixaPendenciaContrato
     */
    private java.lang.String _dtBaixaPendenciaContrato;

    /**
     * Field _hrBaixaPendencia
     */
    private java.lang.String _hrBaixaPendencia;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _nrSequenciaUnidadeOrganizacional
     */
    private int _nrSequenciaUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaUnidadeOrganizacional
     */
    private boolean _has_nrSequenciaUnidadeOrganizacional;

    /**
     * Field _dsPessoaJuridica
     */
    private java.lang.String _dsPessoaJuridica;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPendencia
     * 
     */
    public void deleteCdPendencia()
    {
        this._has_cdPendencia= false;
    } //-- void deleteCdPendencia() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdSituacaoPendenciaContrato
     * 
     */
    public void deleteCdSituacaoPendenciaContrato()
    {
        this._has_cdSituacaoPendenciaContrato= false;
    } //-- void deleteCdSituacaoPendenciaContrato() 

    /**
     * Method deleteCdTipoBaixa
     * 
     */
    public void deleteCdTipoBaixa()
    {
        this._has_cdTipoBaixa= false;
    } //-- void deleteCdTipoBaixa() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNrPendenciaContratoNegocio
     * 
     */
    public void deleteNrPendenciaContratoNegocio()
    {
        this._has_nrPendenciaContratoNegocio= false;
    } //-- void deleteNrPendenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaUnidadeOrganizacional
     * 
     */
    public void deleteNrSequenciaUnidadeOrganizacional()
    {
        this._has_nrSequenciaUnidadeOrganizacional= false;
    } //-- void deleteNrSequenciaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdPendencia'.
     * 
     * @return long
     * @return the value of field 'cdPendencia'.
     */
    public long getCdPendencia()
    {
        return this._cdPendencia;
    } //-- long getCdPendencia() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdSituacaoPendenciaContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPendenciaContrato'.
     */
    public int getCdSituacaoPendenciaContrato()
    {
        return this._cdSituacaoPendenciaContrato;
    } //-- int getCdSituacaoPendenciaContrato() 

    /**
     * Returns the value of field 'cdTipoBaixa'.
     * 
     * @return int
     * @return the value of field 'cdTipoBaixa'.
     */
    public int getCdTipoBaixa()
    {
        return this._cdTipoBaixa;
    } //-- int getCdTipoBaixa() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'dsPendenciaPagamentoIntegrado'.
     */
    public java.lang.String getDsPendenciaPagamentoIntegrado()
    {
        return this._dsPendenciaPagamentoIntegrado;
    } //-- java.lang.String getDsPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'dsPessoaJuridica'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridica'.
     */
    public java.lang.String getDsPessoaJuridica()
    {
        return this._dsPessoaJuridica;
    } //-- java.lang.String getDsPessoaJuridica() 

    /**
     * Returns the value of field 'dsSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContrato'.
     */
    public java.lang.String getDsSituacaoContrato()
    {
        return this._dsSituacaoContrato;
    } //-- java.lang.String getDsSituacaoContrato() 

    /**
     * Returns the value of field 'dsTipoBaixa'.
     * 
     * @return String
     * @return the value of field 'dsTipoBaixa'.
     */
    public java.lang.String getDsTipoBaixa()
    {
        return this._dsTipoBaixa;
    } //-- java.lang.String getDsTipoBaixa() 

    /**
     * Returns the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeOrganizacional'.
     */
    public java.lang.String getDsTipoUnidadeOrganizacional()
    {
        return this._dsTipoUnidadeOrganizacional;
    } //-- java.lang.String getDsTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizacional'.
     */
    public java.lang.String getDsUnidadeOrganizacional()
    {
        return this._dsUnidadeOrganizacional;
    } //-- java.lang.String getDsUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dtBaixaPendenciaContrato'.
     * 
     * @return String
     * @return the value of field 'dtBaixaPendenciaContrato'.
     */
    public java.lang.String getDtBaixaPendenciaContrato()
    {
        return this._dtBaixaPendenciaContrato;
    } //-- java.lang.String getDtBaixaPendenciaContrato() 

    /**
     * Returns the value of field 'dtGeracaoPendencia'.
     * 
     * @return String
     * @return the value of field 'dtGeracaoPendencia'.
     */
    public java.lang.String getDtGeracaoPendencia()
    {
        return this._dtGeracaoPendencia;
    } //-- java.lang.String getDtGeracaoPendencia() 

    /**
     * Returns the value of field 'hrBaixaPendencia'.
     * 
     * @return String
     * @return the value of field 'hrBaixaPendencia'.
     */
    public java.lang.String getHrBaixaPendencia()
    {
        return this._hrBaixaPendencia;
    } //-- java.lang.String getHrBaixaPendencia() 

    /**
     * Returns the value of field 'hrGeracaoPendencia'.
     * 
     * @return String
     * @return the value of field 'hrGeracaoPendencia'.
     */
    public java.lang.String getHrGeracaoPendencia()
    {
        return this._hrGeracaoPendencia;
    } //-- java.lang.String getHrGeracaoPendencia() 

    /**
     * Returns the value of field 'nrPendenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrPendenciaContratoNegocio'.
     */
    public long getNrPendenciaContratoNegocio()
    {
        return this._nrPendenciaContratoNegocio;
    } //-- long getNrPendenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeOrganizacional'
     */
    public int getNrSequenciaUnidadeOrganizacional()
    {
        return this._nrSequenciaUnidadeOrganizacional;
    } //-- int getNrSequenciaUnidadeOrganizacional() 

    /**
     * Method hasCdPendencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPendencia()
    {
        return this._has_cdPendencia;
    } //-- boolean hasCdPendencia() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdSituacaoPendenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPendenciaContrato()
    {
        return this._has_cdSituacaoPendenciaContrato;
    } //-- boolean hasCdSituacaoPendenciaContrato() 

    /**
     * Method hasCdTipoBaixa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoBaixa()
    {
        return this._has_cdTipoBaixa;
    } //-- boolean hasCdTipoBaixa() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNrPendenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPendenciaContratoNegocio()
    {
        return this._has_nrPendenciaContratoNegocio;
    } //-- boolean hasNrPendenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeOrganizacional()
    {
        return this._has_nrSequenciaUnidadeOrganizacional;
    } //-- boolean hasNrSequenciaUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPendencia'.
     * 
     * @param cdPendencia the value of field 'cdPendencia'.
     */
    public void setCdPendencia(long cdPendencia)
    {
        this._cdPendencia = cdPendencia;
        this._has_cdPendencia = true;
    } //-- void setCdPendencia(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdSituacaoPendenciaContrato'.
     * 
     * @param cdSituacaoPendenciaContrato the value of field
     * 'cdSituacaoPendenciaContrato'.
     */
    public void setCdSituacaoPendenciaContrato(int cdSituacaoPendenciaContrato)
    {
        this._cdSituacaoPendenciaContrato = cdSituacaoPendenciaContrato;
        this._has_cdSituacaoPendenciaContrato = true;
    } //-- void setCdSituacaoPendenciaContrato(int) 

    /**
     * Sets the value of field 'cdTipoBaixa'.
     * 
     * @param cdTipoBaixa the value of field 'cdTipoBaixa'.
     */
    public void setCdTipoBaixa(int cdTipoBaixa)
    {
        this._cdTipoBaixa = cdTipoBaixa;
        this._has_cdTipoBaixa = true;
    } //-- void setCdTipoBaixa(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @param dsPendenciaPagamentoIntegrado the value of field
     * 'dsPendenciaPagamentoIntegrado'.
     */
    public void setDsPendenciaPagamentoIntegrado(java.lang.String dsPendenciaPagamentoIntegrado)
    {
        this._dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
    } //-- void setDsPendenciaPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridica'.
     * 
     * @param dsPessoaJuridica the value of field 'dsPessoaJuridica'
     */
    public void setDsPessoaJuridica(java.lang.String dsPessoaJuridica)
    {
        this._dsPessoaJuridica = dsPessoaJuridica;
    } //-- void setDsPessoaJuridica(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContrato'.
     * 
     * @param dsSituacaoContrato the value of field
     * 'dsSituacaoContrato'.
     */
    public void setDsSituacaoContrato(java.lang.String dsSituacaoContrato)
    {
        this._dsSituacaoContrato = dsSituacaoContrato;
    } //-- void setDsSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoBaixa'.
     * 
     * @param dsTipoBaixa the value of field 'dsTipoBaixa'.
     */
    public void setDsTipoBaixa(java.lang.String dsTipoBaixa)
    {
        this._dsTipoBaixa = dsTipoBaixa;
    } //-- void setDsTipoBaixa(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @param dsTipoUnidadeOrganizacional the value of field
     * 'dsTipoUnidadeOrganizacional'.
     */
    public void setDsTipoUnidadeOrganizacional(java.lang.String dsTipoUnidadeOrganizacional)
    {
        this._dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
    } //-- void setDsTipoUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizacional'.
     * 
     * @param dsUnidadeOrganizacional the value of field
     * 'dsUnidadeOrganizacional'.
     */
    public void setDsUnidadeOrganizacional(java.lang.String dsUnidadeOrganizacional)
    {
        this._dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    } //-- void setDsUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'dtBaixaPendenciaContrato'.
     * 
     * @param dtBaixaPendenciaContrato the value of field
     * 'dtBaixaPendenciaContrato'.
     */
    public void setDtBaixaPendenciaContrato(java.lang.String dtBaixaPendenciaContrato)
    {
        this._dtBaixaPendenciaContrato = dtBaixaPendenciaContrato;
    } //-- void setDtBaixaPendenciaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtGeracaoPendencia'.
     * 
     * @param dtGeracaoPendencia the value of field
     * 'dtGeracaoPendencia'.
     */
    public void setDtGeracaoPendencia(java.lang.String dtGeracaoPendencia)
    {
        this._dtGeracaoPendencia = dtGeracaoPendencia;
    } //-- void setDtGeracaoPendencia(java.lang.String) 

    /**
     * Sets the value of field 'hrBaixaPendencia'.
     * 
     * @param hrBaixaPendencia the value of field 'hrBaixaPendencia'
     */
    public void setHrBaixaPendencia(java.lang.String hrBaixaPendencia)
    {
        this._hrBaixaPendencia = hrBaixaPendencia;
    } //-- void setHrBaixaPendencia(java.lang.String) 

    /**
     * Sets the value of field 'hrGeracaoPendencia'.
     * 
     * @param hrGeracaoPendencia the value of field
     * 'hrGeracaoPendencia'.
     */
    public void setHrGeracaoPendencia(java.lang.String hrGeracaoPendencia)
    {
        this._hrGeracaoPendencia = hrGeracaoPendencia;
    } //-- void setHrGeracaoPendencia(java.lang.String) 

    /**
     * Sets the value of field 'nrPendenciaContratoNegocio'.
     * 
     * @param nrPendenciaContratoNegocio the value of field
     * 'nrPendenciaContratoNegocio'.
     */
    public void setNrPendenciaContratoNegocio(long nrPendenciaContratoNegocio)
    {
        this._nrPendenciaContratoNegocio = nrPendenciaContratoNegocio;
        this._has_nrPendenciaContratoNegocio = true;
    } //-- void setNrPendenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @param nrSequenciaUnidadeOrganizacional the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     */
    public void setNrSequenciaUnidadeOrganizacional(int nrSequenciaUnidadeOrganizacional)
    {
        this._nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
        this._has_nrSequenciaUnidadeOrganizacional = true;
    } //-- void setNrSequenciaUnidadeOrganizacional(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
