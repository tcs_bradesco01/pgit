/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean;

/**
 * Nome: ConsultarListaPendenciasContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaPendenciasContratoSaidaDTO {
	
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numLinhas. */
	private int numLinhas;
	
	/** Atributo consultaSaida. */
	private int consultaSaida;
	
	/** Atributo codPendenciaPagInt. */
	private long codPendenciaPagInt;
	
	/** Atributo dsPendenciaPagInt. */
	private String dsPendenciaPagInt;
	
	/** Atributo numPendenciaContratoNeg. */
	private long numPendenciaContratoNeg;
	
	/** Atributo dataGeracaoPendencia. */
	private String dataGeracaoPendencia;
	
	/** Atributo dsTipoUnidOrg. */
	private String dsTipoUnidOrg;
	
	/** Atributo cosTipoUnidOrg. */
	private int cosTipoUnidOrg;
	
	/** Atributo dsUnidadeOrg. */
	private String dsUnidadeOrg;
	
	/** Atributo codTipoBaixa. */
	private int codTipoBaixa;
	
	/** Atributo dsTipoBaixa. */
	private String dsTipoBaixa;
	
	/** Atributo codSituacaoPendContrato. */
	private int codSituacaoPendContrato;
	
	/** Atributo dsSituacaoPendContrato. */
	private String dsSituacaoPendContrato;
	
	/** Atributo dataBaixaPendContrato. */
	private String dataBaixaPendContrato;
	
	/** Atributo codPessoaJuridica. */
	private long codPessoaJuridica;
	
	/** Atributo numSeqUnidadeOrg. */
	private int numSeqUnidadeOrg;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo dsTipo. */
	private String dsTipo;
	
	/** Atributo cdMotivoSituacao. */
	private int cdMotivoSituacao;
	
	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;
	
	/** Atributo hrGeracaoPendencia. */
	private String hrGeracaoPendencia;
	
	/** Atributo hrBaixaPendencia. */
	private String hrBaixaPendencia;

	
	/**
	 * Get: hrGeracaoPendencia.
	 *
	 * @return hrGeracaoPendencia
	 */
	public String getHrGeracaoPendencia() {
		return hrGeracaoPendencia;
	}
	
	/**
	 * Set: hrGeracaoPendencia.
	 *
	 * @param hrGeracaoPendencia the hr geracao pendencia
	 */
	public void setHrGeracaoPendencia(String hrGeracaoPendencia) {
		this.hrGeracaoPendencia = hrGeracaoPendencia;
	}
	
	/**
	 * Get: hrBaixaPendencia.
	 *
	 * @return hrBaixaPendencia
	 */
	public String getHrBaixaPendencia() {
		return hrBaixaPendencia;
	}
	
	/**
	 * Set: hrBaixaPendencia.
	 *
	 * @param hrBaixaPendencia the hr baixa pendencia
	 */
	public void setHrBaixaPendencia(String hrBaixaPendencia) {
		this.hrBaixaPendencia = hrBaixaPendencia;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: codPendenciaPagInt.
	 *
	 * @return codPendenciaPagInt
	 */
	public long getCodPendenciaPagInt() {
		return codPendenciaPagInt;
	}
	
	/**
	 * Set: codPendenciaPagInt.
	 *
	 * @param codPendenciaPagInt the cod pendencia pag int
	 */
	public void setCodPendenciaPagInt(long codPendenciaPagInt) {
		this.codPendenciaPagInt = codPendenciaPagInt;
	}
	
	/**
	 * Get: codPessoaJuridica.
	 *
	 * @return codPessoaJuridica
	 */
	public long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}
	
	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}
	
	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(int codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}
	
	/**
	 * Get: codSituacaoPendContrato.
	 *
	 * @return codSituacaoPendContrato
	 */
	public int getCodSituacaoPendContrato() {
		return codSituacaoPendContrato;
	}
	
	/**
	 * Set: codSituacaoPendContrato.
	 *
	 * @param codSituacaoPendContrato the cod situacao pend contrato
	 */
	public void setCodSituacaoPendContrato(int codSituacaoPendContrato) {
		this.codSituacaoPendContrato = codSituacaoPendContrato;
	}
	
	/**
	 * Get: codTipoBaixa.
	 *
	 * @return codTipoBaixa
	 */
	public int getCodTipoBaixa() {
		return codTipoBaixa;
	}
	
	/**
	 * Set: codTipoBaixa.
	 *
	 * @param codTipoBaixa the cod tipo baixa
	 */
	public void setCodTipoBaixa(int codTipoBaixa) {
		this.codTipoBaixa = codTipoBaixa;
	}
	
	/**
	 * Get: consultaSaida.
	 *
	 * @return consultaSaida
	 */
	public int getConsultaSaida() {
		return consultaSaida;
	}
	
	/**
	 * Set: consultaSaida.
	 *
	 * @param consultaSaida the consulta saida
	 */
	public void setConsultaSaida(int consultaSaida) {
		this.consultaSaida = consultaSaida;
	}
	
	/**
	 * Get: cosTipoUnidOrg.
	 *
	 * @return cosTipoUnidOrg
	 */
	public int getCosTipoUnidOrg() {
		return cosTipoUnidOrg;
	}
	
	/**
	 * Set: cosTipoUnidOrg.
	 *
	 * @param cosTipoUnidOrg the cos tipo unid org
	 */
	public void setCosTipoUnidOrg(int cosTipoUnidOrg) {
		this.cosTipoUnidOrg = cosTipoUnidOrg;
	}
	
	/**
	 * Get: dataBaixaPendContrato.
	 *
	 * @return dataBaixaPendContrato
	 */
	public String getDataBaixaPendContrato() {
		return dataBaixaPendContrato;
	}
	
	/**
	 * Set: dataBaixaPendContrato.
	 *
	 * @param dataBaixaPendContrato the data baixa pend contrato
	 */
	public void setDataBaixaPendContrato(String dataBaixaPendContrato) {
		this.dataBaixaPendContrato = dataBaixaPendContrato;
	}
	
	/**
	 * Get: dataGeracaoPendencia.
	 *
	 * @return dataGeracaoPendencia
	 */
	public String getDataGeracaoPendencia() {
		return dataGeracaoPendencia;
	}
	
	/**
	 * Set: dataGeracaoPendencia.
	 *
	 * @param dataGeracaoPendencia the data geracao pendencia
	 */
	public void setDataGeracaoPendencia(String dataGeracaoPendencia) {
		this.dataGeracaoPendencia = dataGeracaoPendencia;
	}
	
	/**
	 * Get: dsPendenciaPagInt.
	 *
	 * @return dsPendenciaPagInt
	 */
	public String getDsPendenciaPagInt() {
		return dsPendenciaPagInt;
	}
	
	/**
	 * Set: dsPendenciaPagInt.
	 *
	 * @param dsPendenciaPagInt the ds pendencia pag int
	 */
	public void setDsPendenciaPagInt(String dsPendenciaPagInt) {
		this.dsPendenciaPagInt = dsPendenciaPagInt;
	}
	
	/**
	 * Get: dsSituacaoPendContrato.
	 *
	 * @return dsSituacaoPendContrato
	 */
	public String getDsSituacaoPendContrato() {
		return dsSituacaoPendContrato;
	}
	
	/**
	 * Set: dsSituacaoPendContrato.
	 *
	 * @param dsSituacaoPendContrato the ds situacao pend contrato
	 */
	public void setDsSituacaoPendContrato(String dsSituacaoPendContrato) {
		this.dsSituacaoPendContrato = dsSituacaoPendContrato;
	}
	
	/**
	 * Get: dsTipoBaixa.
	 *
	 * @return dsTipoBaixa
	 */
	public String getDsTipoBaixa() {
		return dsTipoBaixa;
	}
	
	/**
	 * Set: dsTipoBaixa.
	 *
	 * @param dsTipoBaixa the ds tipo baixa
	 */
	public void setDsTipoBaixa(String dsTipoBaixa) {
		this.dsTipoBaixa = dsTipoBaixa;
	}
	
	/**
	 * Get: dsTipoUnidOrg.
	 *
	 * @return dsTipoUnidOrg
	 */
	public String getDsTipoUnidOrg() {
		return dsTipoUnidOrg;
	}
	
	/**
	 * Set: dsTipoUnidOrg.
	 *
	 * @param dsTipoUnidOrg the ds tipo unid org
	 */
	public void setDsTipoUnidOrg(String dsTipoUnidOrg) {
		this.dsTipoUnidOrg = dsTipoUnidOrg;
	}
	
	/**
	 * Get: dsUnidadeOrg.
	 *
	 * @return dsUnidadeOrg
	 */
	public String getDsUnidadeOrg() {
		return dsUnidadeOrg;
	}
	
	/**
	 * Set: dsUnidadeOrg.
	 *
	 * @param dsUnidadeOrg the ds unidade org
	 */
	public void setDsUnidadeOrg(String dsUnidadeOrg) {
		this.dsUnidadeOrg = dsUnidadeOrg;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: numLinhas.
	 *
	 * @return numLinhas
	 */
	public int getNumLinhas() {
		return numLinhas;
	}
	
	/**
	 * Set: numLinhas.
	 *
	 * @param numLinhas the num linhas
	 */
	public void setNumLinhas(int numLinhas) {
		this.numLinhas = numLinhas;
	}
	
	/**
	 * Get: numPendenciaContratoNeg.
	 *
	 * @return numPendenciaContratoNeg
	 */
	public long getNumPendenciaContratoNeg() {
		return numPendenciaContratoNeg;
	}
	
	/**
	 * Set: numPendenciaContratoNeg.
	 *
	 * @param numPendenciaContratoNeg the num pendencia contrato neg
	 */
	public void setNumPendenciaContratoNeg(long numPendenciaContratoNeg) {
		this.numPendenciaContratoNeg = numPendenciaContratoNeg;
	}
	
	/**
	 * Get: numSeqUnidadeOrg.
	 *
	 * @return numSeqUnidadeOrg
	 */
	public int getNumSeqUnidadeOrg() {
		return numSeqUnidadeOrg;
	}
	
	/**
	 * Set: numSeqUnidadeOrg.
	 *
	 * @param numSeqUnidadeOrg the num seq unidade org
	 */
	public void setNumSeqUnidadeOrg(int numSeqUnidadeOrg) {
		this.numSeqUnidadeOrg = numSeqUnidadeOrg;
	}
	
	/**
	 * Get: cdMotivoSituacao.
	 *
	 * @return cdMotivoSituacao
	 */
	public int getCdMotivoSituacao() {
		return cdMotivoSituacao;
	}
	
	/**
	 * Set: cdMotivoSituacao.
	 *
	 * @param cdMotivoSituacao the cd motivo situacao
	 */
	public void setCdMotivoSituacao(int cdMotivoSituacao) {
		this.cdMotivoSituacao = cdMotivoSituacao;
	}
	
	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}
	
	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}
	
	/**
	 * Get: dsTipo.
	 *
	 * @return dsTipo
	 */
	public String getDsTipo() {
		return dsTipo;
	}
	
	/**
	 * Set: dsTipo.
	 *
	 * @param dsTipo the ds tipo
	 */
	public void setDsTipo(String dsTipo) {
		this.dsTipo = dsTipo;
	}
	
	/**
	 * Get: dataHoraGeracaoFormatada.
	 *
	 * @return dataHoraGeracaoFormatada
	 */
	public String getDataHoraGeracaoFormatada(){
	 return getDataGeracaoPendencia() +" - " + getHrGeracaoPendencia();
	}	
	

}
