/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id: Ocorrencia.java,v 1.2 2009/05/13 19:39:14 cpm.com.br\heslei.silva Exp $
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencia.
 * 
 * @version $Revision: 1.2 $ $Date: 2009/05/13 19:39:14 $
 */
public class Ocorrencia implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dtContabil
     */
    private java.lang.String _dtContabil;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _descConta
     */
    private java.lang.String _descConta;

    /**
     * Field _codTipoSaldo
     */
    private int _codTipoSaldo = 0;

    /**
     * keeps track of state for field: _codTipoSaldo
     */
    private boolean _has_codTipoSaldo;

    /**
     * Field _vlSaldoContabil
     */
    private java.lang.String _vlSaldoContabil = "0";

    /**
     * Field _vlSaldoProduto
     */
    private java.lang.String _vlSaldoProduto = "0";

    /**
     * Field _vlSaldoDiferenca
     */
    private java.lang.String _vlSaldoDiferenca = "0";


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencia() 
     {
        super();
        setVlSaldoContabil("0");
        setVlSaldoProduto("0");
        setVlSaldoDiferenca("0");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.response.Ocorrencia()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCodTipoSaldo
     * 
     */
    public void deleteCodTipoSaldo()
    {
        this._has_codTipoSaldo= false;
    } //-- void deleteCodTipoSaldo() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'codTipoSaldo'.
     * 
     * @return int
     * @return the value of field 'codTipoSaldo'.
     */
    public int getCodTipoSaldo()
    {
        return this._codTipoSaldo;
    } //-- int getCodTipoSaldo() 

    /**
     * Returns the value of field 'descConta'.
     * 
     * @return String
     * @return the value of field 'descConta'.
     */
    public java.lang.String getDescConta()
    {
        return this._descConta;
    } //-- java.lang.String getDescConta() 

    /**
     * Returns the value of field 'dtContabil'.
     * 
     * @return String
     * @return the value of field 'dtContabil'.
     */
    public java.lang.String getDtContabil()
    {
        return this._dtContabil;
    } //-- java.lang.String getDtContabil() 

    /**
     * Returns the value of field 'vlSaldoContabil'.
     * 
     * @return String
     * @return the value of field 'vlSaldoContabil'.
     */
    public java.lang.String getVlSaldoContabil()
    {
        return this._vlSaldoContabil;
    } //-- java.lang.String getVlSaldoContabil() 

    /**
     * Returns the value of field 'vlSaldoDiferenca'.
     * 
     * @return String
     * @return the value of field 'vlSaldoDiferenca'.
     */
    public java.lang.String getVlSaldoDiferenca()
    {
        return this._vlSaldoDiferenca;
    } //-- java.lang.String getVlSaldoDiferenca() 

    /**
     * Returns the value of field 'vlSaldoProduto'.
     * 
     * @return String
     * @return the value of field 'vlSaldoProduto'.
     */
    public java.lang.String getVlSaldoProduto()
    {
        return this._vlSaldoProduto;
    } //-- java.lang.String getVlSaldoProduto() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCodTipoSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodTipoSaldo()
    {
        return this._has_codTipoSaldo;
    } //-- boolean hasCodTipoSaldo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'codTipoSaldo'.
     * 
     * @param codTipoSaldo the value of field 'codTipoSaldo'.
     */
    public void setCodTipoSaldo(int codTipoSaldo)
    {
        this._codTipoSaldo = codTipoSaldo;
        this._has_codTipoSaldo = true;
    } //-- void setCodTipoSaldo(int) 

    /**
     * Sets the value of field 'descConta'.
     * 
     * @param descConta the value of field 'descConta'.
     */
    public void setDescConta(java.lang.String descConta)
    {
        this._descConta = descConta;
    } //-- void setDescConta(java.lang.String) 

    /**
     * Sets the value of field 'dtContabil'.
     * 
     * @param dtContabil the value of field 'dtContabil'.
     */
    public void setDtContabil(java.lang.String dtContabil)
    {
        this._dtContabil = dtContabil;
    } //-- void setDtContabil(java.lang.String) 

    /**
     * Sets the value of field 'vlSaldoContabil'.
     * 
     * @param vlSaldoContabil the value of field 'vlSaldoContabil'.
     */
    public void setVlSaldoContabil(java.lang.String vlSaldoContabil)
    {
        this._vlSaldoContabil = vlSaldoContabil;
    } //-- void setVlSaldoContabil(java.lang.String) 

    /**
     * Sets the value of field 'vlSaldoDiferenca'.
     * 
     * @param vlSaldoDiferenca the value of field 'vlSaldoDiferenca'
     */
    public void setVlSaldoDiferenca(java.lang.String vlSaldoDiferenca)
    {
        this._vlSaldoDiferenca = vlSaldoDiferenca;
    } //-- void setVlSaldoDiferenca(java.lang.String) 

    /**
     * Sets the value of field 'vlSaldoProduto'.
     * 
     * @param vlSaldoProduto the value of field 'vlSaldoProduto'.
     */
    public void setVlSaldoProduto(java.lang.String vlSaldoProduto)
    {
        this._vlSaldoProduto = vlSaldoProduto;
    } //-- void setVlSaldoProduto(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencia
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.response.Ocorrencia unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.response.Ocorrencia) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.response.Ocorrencia.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.response.Ocorrencia unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
