/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean;

/**
 * Nome: ConsultarContratoClienteFiltroEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarContratoClienteFiltroEntradaDTO {
	
	/** Atributo cdClubPessoa. */
	private Long cdClubPessoa;
	
	/** Atributo cdCpfCnpjPssoa. */
	private Long cdCpfCnpjPssoa;
	
	/** Atributo cdFilialCnpjPssoa. */
	private Integer cdFilialCnpjPssoa;
	
	/** Atributo cdControleCpfPssoa. */
	private Integer cdControleCpfPssoa;

	/**
	 * Get: cdClubPessoa.
	 *
	 * @return cdClubPessoa
	 */
	public Long getCdClubPessoa() {
		return cdClubPessoa;
	}

	/**
	 * Set: cdClubPessoa.
	 *
	 * @param cdClubPessoa the cd club pessoa
	 */
	public void setCdClubPessoa(Long cdClubPessoa) {
		this.cdClubPessoa = cdClubPessoa;
	}

	/**
	 * Get: cdCpfCnpjPssoa.
	 *
	 * @return cdCpfCnpjPssoa
	 */
	public Long getCdCpfCnpjPssoa() {
		return cdCpfCnpjPssoa;
	}

	/**
	 * Set: cdCpfCnpjPssoa.
	 *
	 * @param cdCpfCnpjPssoa the cd cpf cnpj pssoa
	 */
	public void setCdCpfCnpjPssoa(Long cdCpfCnpjPssoa) {
		this.cdCpfCnpjPssoa = cdCpfCnpjPssoa;
	}

	/**
	 * Get: cdFilialCnpjPssoa.
	 *
	 * @return cdFilialCnpjPssoa
	 */
	public Integer getCdFilialCnpjPssoa() {
		return cdFilialCnpjPssoa;
	}

	/**
	 * Set: cdFilialCnpjPssoa.
	 *
	 * @param cdFilialCnpjPssoa the cd filial cnpj pssoa
	 */
	public void setCdFilialCnpjPssoa(Integer cdFilialCnpjPssoa) {
		this.cdFilialCnpjPssoa = cdFilialCnpjPssoa;
	}

	/**
	 * Get: cdControleCpfPssoa.
	 *
	 * @return cdControleCpfPssoa
	 */
	public Integer getCdControleCpfPssoa() {
		return cdControleCpfPssoa;
	}

	/**
	 * Set: cdControleCpfPssoa.
	 *
	 * @param cdControleCpfPssoa the cd controle cpf pssoa
	 */
	public void setCdControleCpfPssoa(Integer cdControleCpfPssoa) {
		this.cdControleCpfPssoa = cdControleCpfPssoa;
	}

}
