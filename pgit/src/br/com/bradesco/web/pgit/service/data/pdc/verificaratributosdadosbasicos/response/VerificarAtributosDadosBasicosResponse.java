/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class VerificarAtributosDadosBasicosResponse.
 * 
 * @version $Revision$ $Date$
 */
public class VerificarAtributosDadosBasicosResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdFormaAutorizacaoPagamentoAlterar
     */
    private int _cdFormaAutorizacaoPagamentoAlterar = 0;

    /**
     * keeps track of state for field:
     * _cdFormaAutorizacaoPagamentoAlterar
     */
    private boolean _has_cdFormaAutorizacaoPagamentoAlterar;


      //----------------/
     //- Constructors -/
    //----------------/

    public VerificarAtributosDadosBasicosResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.response.VerificarAtributosDadosBasicosResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaAutorizacaoPagamentoAlterar
     * 
     */
    public void deleteCdFormaAutorizacaoPagamentoAlterar()
    {
        this._has_cdFormaAutorizacaoPagamentoAlterar= false;
    } //-- void deleteCdFormaAutorizacaoPagamentoAlterar() 

    /**
     * Returns the value of field
     * 'cdFormaAutorizacaoPagamentoAlterar'.
     * 
     * @return int
     * @return the value of field
     * 'cdFormaAutorizacaoPagamentoAlterar'.
     */
    public int getCdFormaAutorizacaoPagamentoAlterar()
    {
        return this._cdFormaAutorizacaoPagamentoAlterar;
    } //-- int getCdFormaAutorizacaoPagamentoAlterar() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdFormaAutorizacaoPagamentoAlterar
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamentoAlterar()
    {
        return this._has_cdFormaAutorizacaoPagamentoAlterar;
    } //-- boolean hasCdFormaAutorizacaoPagamentoAlterar() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field
     * 'cdFormaAutorizacaoPagamentoAlterar'.
     * 
     * @param cdFormaAutorizacaoPagamentoAlterar the value of field
     * 'cdFormaAutorizacaoPagamentoAlterar'.
     */
    public void setCdFormaAutorizacaoPagamentoAlterar(int cdFormaAutorizacaoPagamentoAlterar)
    {
        this._cdFormaAutorizacaoPagamentoAlterar = cdFormaAutorizacaoPagamentoAlterar;
        this._has_cdFormaAutorizacaoPagamentoAlterar = true;
    } //-- void setCdFormaAutorizacaoPagamentoAlterar(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return VerificarAtributosDadosBasicosResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.response.VerificarAtributosDadosBasicosResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.response.VerificarAtributosDadosBasicosResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.response.VerificarAtributosDadosBasicosResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.response.VerificarAtributosDadosBasicosResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
