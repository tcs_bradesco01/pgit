/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirVinculoEmprPagContSalRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirVinculoEmprPagContSalRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaNegocio
     */
    private long _cdPessoaJuridicaNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaNegocio
     */
    private boolean _has_cdPessoaJuridicaNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdBancoSalario
     */
    private int _cdBancoSalario = 0;

    /**
     * keeps track of state for field: _cdBancoSalario
     */
    private boolean _has_cdBancoSalario;

    /**
     * Field _cdAgenciaSalario
     */
    private int _cdAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdAgenciaSalario
     */
    private boolean _has_cdAgenciaSalario;

    /**
     * Field _cdDigitoAgenciaSalario
     */
    private int _cdDigitoAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaSalario
     */
    private boolean _has_cdDigitoAgenciaSalario;

    /**
     * Field _cdContaSalario
     */
    private long _cdContaSalario = 0;

    /**
     * keeps track of state for field: _cdContaSalario
     */
    private boolean _has_cdContaSalario;

    /**
     * Field _dsDigitoContaSalario
     */
    private java.lang.String _dsDigitoContaSalario;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirVinculoEmprPagContSalRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.request.IncluirVinculoEmprPagContSalRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaSalario
     * 
     */
    public void deleteCdAgenciaSalario()
    {
        this._has_cdAgenciaSalario= false;
    } //-- void deleteCdAgenciaSalario() 

    /**
     * Method deleteCdBancoSalario
     * 
     */
    public void deleteCdBancoSalario()
    {
        this._has_cdBancoSalario= false;
    } //-- void deleteCdBancoSalario() 

    /**
     * Method deleteCdContaSalario
     * 
     */
    public void deleteCdContaSalario()
    {
        this._has_cdContaSalario= false;
    } //-- void deleteCdContaSalario() 

    /**
     * Method deleteCdDigitoAgenciaSalario
     * 
     */
    public void deleteCdDigitoAgenciaSalario()
    {
        this._has_cdDigitoAgenciaSalario= false;
    } //-- void deleteCdDigitoAgenciaSalario() 

    /**
     * Method deleteCdPessoaJuridicaNegocio
     * 
     */
    public void deleteCdPessoaJuridicaNegocio()
    {
        this._has_cdPessoaJuridicaNegocio= false;
    } //-- void deleteCdPessoaJuridicaNegocio() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaSalario'.
     */
    public int getCdAgenciaSalario()
    {
        return this._cdAgenciaSalario;
    } //-- int getCdAgenciaSalario() 

    /**
     * Returns the value of field 'cdBancoSalario'.
     * 
     * @return int
     * @return the value of field 'cdBancoSalario'.
     */
    public int getCdBancoSalario()
    {
        return this._cdBancoSalario;
    } //-- int getCdBancoSalario() 

    /**
     * Returns the value of field 'cdContaSalario'.
     * 
     * @return long
     * @return the value of field 'cdContaSalario'.
     */
    public long getCdContaSalario()
    {
        return this._cdContaSalario;
    } //-- long getCdContaSalario() 

    /**
     * Returns the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaSalario'.
     */
    public int getCdDigitoAgenciaSalario()
    {
        return this._cdDigitoAgenciaSalario;
    } //-- int getCdDigitoAgenciaSalario() 

    /**
     * Returns the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaNegocio'.
     */
    public long getCdPessoaJuridicaNegocio()
    {
        return this._cdPessoaJuridicaNegocio;
    } //-- long getCdPessoaJuridicaNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsDigitoContaSalario'.
     * 
     * @return String
     * @return the value of field 'dsDigitoContaSalario'.
     */
    public java.lang.String getDsDigitoContaSalario()
    {
        return this._dsDigitoContaSalario;
    } //-- java.lang.String getDsDigitoContaSalario() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaSalario()
    {
        return this._has_cdAgenciaSalario;
    } //-- boolean hasCdAgenciaSalario() 

    /**
     * Method hasCdBancoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoSalario()
    {
        return this._has_cdBancoSalario;
    } //-- boolean hasCdBancoSalario() 

    /**
     * Method hasCdContaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaSalario()
    {
        return this._has_cdContaSalario;
    } //-- boolean hasCdContaSalario() 

    /**
     * Method hasCdDigitoAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaSalario()
    {
        return this._has_cdDigitoAgenciaSalario;
    } //-- boolean hasCdDigitoAgenciaSalario() 

    /**
     * Method hasCdPessoaJuridicaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaNegocio()
    {
        return this._has_cdPessoaJuridicaNegocio;
    } //-- boolean hasCdPessoaJuridicaNegocio() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaSalario'.
     * 
     * @param cdAgenciaSalario the value of field 'cdAgenciaSalario'
     */
    public void setCdAgenciaSalario(int cdAgenciaSalario)
    {
        this._cdAgenciaSalario = cdAgenciaSalario;
        this._has_cdAgenciaSalario = true;
    } //-- void setCdAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdBancoSalario'.
     * 
     * @param cdBancoSalario the value of field 'cdBancoSalario'.
     */
    public void setCdBancoSalario(int cdBancoSalario)
    {
        this._cdBancoSalario = cdBancoSalario;
        this._has_cdBancoSalario = true;
    } //-- void setCdBancoSalario(int) 

    /**
     * Sets the value of field 'cdContaSalario'.
     * 
     * @param cdContaSalario the value of field 'cdContaSalario'.
     */
    public void setCdContaSalario(long cdContaSalario)
    {
        this._cdContaSalario = cdContaSalario;
        this._has_cdContaSalario = true;
    } //-- void setCdContaSalario(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @param cdDigitoAgenciaSalario the value of field
     * 'cdDigitoAgenciaSalario'.
     */
    public void setCdDigitoAgenciaSalario(int cdDigitoAgenciaSalario)
    {
        this._cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
        this._has_cdDigitoAgenciaSalario = true;
    } //-- void setCdDigitoAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @param cdPessoaJuridicaNegocio the value of field
     * 'cdPessoaJuridicaNegocio'.
     */
    public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio)
    {
        this._cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
        this._has_cdPessoaJuridicaNegocio = true;
    } //-- void setCdPessoaJuridicaNegocio(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dsDigitoContaSalario'.
     * 
     * @param dsDigitoContaSalario the value of field
     * 'dsDigitoContaSalario'.
     */
    public void setDsDigitoContaSalario(java.lang.String dsDigitoContaSalario)
    {
        this._dsDigitoContaSalario = dsDigitoContaSalario;
    } //-- void setDsDigitoContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirVinculoEmprPagContSalRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.request.IncluirVinculoEmprPagContSalRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.request.IncluirVinculoEmprPagContSalRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.request.IncluirVinculoEmprPagContSalRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.request.IncluirVinculoEmprPagContSalRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
