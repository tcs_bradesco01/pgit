/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.IConDataUltPagamentoEfetivadoService;
import br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean.ListarUltimaDataPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean.ListarUltimaDataPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.request.ListarUltimaDataPagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.response.ListarUltimaDataPagtoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConDataUltPagamentoEfetivado
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConDataUltPagamentoEfetivadoServiceImpl implements IConDataUltPagamentoEfetivadoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
	
	
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    /**
     * Construtor.
     */
    public ConDataUltPagamentoEfetivadoServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.IConDataUltPagamentoEfetivadoService#listarUltimaDataPagto(br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean.ListarUltimaDataPagtoEntradaDTO)
     */
    public List<ListarUltimaDataPagtoSaidaDTO> listarUltimaDataPagto (ListarUltimaDataPagtoEntradaDTO entrada){
    	
    	List<ListarUltimaDataPagtoSaidaDTO> listaSaida = new ArrayList<ListarUltimaDataPagtoSaidaDTO>();
    	ListarUltimaDataPagtoRequest request = new ListarUltimaDataPagtoRequest();
    	ListarUltimaDataPagtoResponse response = new ListarUltimaDataPagtoResponse();    	

    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));    	
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
    	
    	response = getFactoryAdapter().getListarUltimaDataPagtoPDCAdapter().invokeProcess(request);
    	
    	ListarUltimaDataPagtoSaidaDTO saida;
    	
    	
    	for(int i = 0;i<response.getOcorrenciasCount();i++)
    	{
    		saida = new ListarUltimaDataPagtoSaidaDTO();
    		saida.setCodMensagem(response.getCodMensagem());
    		saida.setMensagem(response.getMensagem());
    		saida.setCdCpfCnpj(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdCpfCnpj()));    		
    		saida.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdPessoaJuridicaContrato()));
    		saida.setCdProdutoOperacao(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdProdutoOperacao()));
    		saida.setCdProdutoRelacionado(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdProdutoRelacionado()));
    		saida.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdTipoContratoNegocio()));    		
    		saida.setDsProdutoOperacao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsProdutoOperacao()));
    		saida.setDsProdutoRelacionado(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsProdutoRelacionado()));
    		saida.setDsRazaoSocial(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsRazaoSocial()));
    		saida.setDtUltimoPagamento(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDtUltimoPagamento()));
    		saida.setHrUltimoPagamento(response.getOcorrencias(i).getHrUltimoPagamento());
    		saida.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getNrSequenciaContratoNegocio()));
    		
    		saida.setDataHoraFormatada(PgitUtil.concatenarCampos(saida.getDtUltimoPagamento(), saida.getHrUltimoPagamento(), " - "));
    		
    		/*if((response.getOcorrencias(i).getCdTipoTela() >= 1 && response.getOcorrencias(i).getCdTipoTela() <= 5) ||
        	    	response.getOcorrencias(i).getCdTipoTela() == 13 || response.getOcorrencias(i).getCdTipoTela() == 14){
    	    	 try{
        	    	ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
        	    	consultarDescBancoAgenciaContaEntradaDTO.setCdBanco(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdBancoCredito()));
        	    	consultarDescBancoAgenciaContaEntradaDTO.setCdAgencia(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdAgenciaCredito()));
        	    	consultarDescBancoAgenciaContaEntradaDTO.setCdConta(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdContaCredito()));
        	    	consultarDescBancoAgenciaContaEntradaDTO.setCdDigitoConta(PgitUtil.complementaDigito(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdDigitoContaCredito()), 2));
        	    	ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaContaSaidaDTO = getConsultasServiceImpl().consultarDescBancoAgenciaConta(consultarDescBancoAgenciaContaEntradaDTO);
        	    	saida.setDsBancoCredito(consultarDescBancoAgenciaContaSaidaDTO.getDsBanco());
        	    	saida.setDsAgenciaCredito(consultarDescBancoAgenciaContaSaidaDTO.getDsAgencia());
        	    	saida.setDsTipoContaCredito(consultarDescBancoAgenciaContaSaidaDTO.getDsTipoConta());
    	    	 }catch(PdcAdapterFunctionalException e){
    	    		 saida.setDsBancoCredito("");
    	    		 saida.setDsAgenciaCredito("");
    	    		 saida.setDsTipoContaCredito("");
    	    	 }
    	  }*/    		

    	
    		
    		listaSaida.add(saida);
    	}
    	
		return listaSaida;
    	
    }

    
    
    
    
}

