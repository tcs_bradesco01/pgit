/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;

import java.awt.Color;
import java.util.Date;

import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;
import br.com.bradesco.web.pgit.utils.DateUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: LayoutEmpresaReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LayoutEmpresaReport extends BasicPdfReport {
	
	/** Atributo fTexto. */
	private Font fTexto = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabela. */
	private Font fTextoTabela = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
	
	/** Atributo fTextoTabelaCinza. */
	private Font fTextoTabelaCinza = new Font(Font.TIMES_ROMAN, 8, Font.BOLD,Color.GRAY);
	
	/** Atributo imagemRelatorio. */
	private Image imagemRelatorio = null;
	
	/** Atributo saidaImprimirContrato. */
	private ImprimirContratoSaidaDTO saidaImprimirContrato;
	
	/**
	 * Layout empresa report.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public LayoutEmpresaReport(ImprimirContratoSaidaDTO saidaImprimirContrato) {
		super();
		this.saidaImprimirContrato = saidaImprimirContrato;
		this.imagemRelatorio = getImagemRelatorio(); 
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	protected void popularPdf(Document document) throws DocumentException {
		// TODO Auto-generated method stub
		preencherCabec(document);
		document.add(Chunk.NEWLINE);
		preencherAnexo(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherTitulo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaCondicoes(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaPrimeira(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaPrimeiraParagrafo1(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaSegunda(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoPrimeiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoSegundo(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiroA(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiroB(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiroC(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiroD(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoQuarto(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoQuinto(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoSexto(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoSetimo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceira(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceira1(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceira2(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuarta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaPrimeiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaSegundo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaTerceiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaTerceiroA(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaTerceiroB(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaTerceiroC(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaQuarto(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaQuinto(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuinta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaPrimeiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaSegundo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaTerceiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaQuarto(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaQuinto(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaSexta(document);
		
		document.add(Chunk.NEWLINE);
		preencherQuinta(document);
		
		document.add(Chunk.NEWLINE);
		preencherQuintaPrimeira(document);
		
		document.add(Chunk.NEWLINE);
		preencherAcordo(document);
		
		document.add(Chunk.NEXTPAGE);
		preencherData(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaUm(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherAssinaturas3(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaDois(document);
				
	}
	
	/**
	 * Preencher cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		tabela.setWidths(new float[] {30.0f, 70.0f});
		
		Paragraph pLinha1Esq = new Paragraph(new Chunk("ANEXO AO CONTRATO DE PRESTA��O DE SERVI�OS DE PAGAMENTOS.", fTextoTabelaCinza));
		pLinha1Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfPCell.NO_BORDER);
		celula1.addElement(imagemRelatorio);
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher anexo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAnexo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("ANEXO 4", fTextoTabelaCinza));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher titulo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherTitulo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("Pagamento de Tributos e Contas de Consumo por Meio de Troca Eletr�nica de Arquivos.", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher clausula condicoes.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaCondicoes(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("As partes nomeadas e qualificadas no pre�mbulo do Contrato, ");
		textoClausula.append("ao qual este Anexo faz parte, denominadas Banco e Cliente, resolvem estabelecer as condi��es operacionais ");
		textoClausula.append("e comerciais espec�ficas � presta��o dos servi�os contratados.");
		Paragraph condicoes = new Paragraph(new Chunk(textoClausula.toString(), fTexto));
		condicoes.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(condicoes);
	}
	
	/**
	 * Preencher clausula primeira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaPrimeira(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Este Anexo tem por objeto a presta��o pelo Banco ao Cliente, ");
		textoClausula.append("dos servi�os de pagamento eletr�nico de tributos e contas de consumo, por meio de troca eletr�nica de arquivos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Primeira: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula primeira paragrafo1.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaPrimeiraParagrafo1(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Para a execu��o dos servi�os objeto deste Anexo, ");
		textoClausula.append("o Cliente utilizar� software e/ou sistema pr�prio, por ele desenvolvido e homologado, sendo que os layouts ");
		textoClausula.append("de arquivos de remessa e retorno e formatos de comprovantes foram desenvolvidos de acordo com o ");
		textoClausula.append("padr�o previamente estabelecido e aprovado pelo Banco, ficando o Cliente, neste ato, exclusivamente ");
		textoClausula.append("respons�vel pelas informa��es disponibilizadas nos arquivos e comprovantes gerados por meio do seu software e/ou sistema.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula segunda.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaSegunda(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente se compromete a fornecer ao Banco ");
		textoClausula.append("todas as informa��es necess�rias � realiza��o dos pagamentos de seus tributos ");
		textoClausula.append("e contas de consumo, de compet�ncia dos respectivos �rg�os/Concession�rias, as quais ");
		textoClausula.append("ser�o encaminhadas ao Banco por meio da troca eletr�nica de dados - EDI (Eletronic Data Interchange).");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Segunda: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoPrimeiro(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente se obriga a tomar as cautelas necess�rias para a correta ");
		textoClausula.append("transcri��o de todos os dados dos pagamentos a serem realizados com base ");
		textoClausula.append("neste Anexo, isentando o Banco, neste ato, de toda e qualquer responsabilidade relativa a eventuais reclama��es, ");
		textoClausula.append("preju�zos, perdas e danos, lucros cessantes e/ou emergentes, inclusive perante terceiros, ");
		textoClausula.append("decorrentes de erros, falhas, irregularidades e omiss�es dos dados constantes de cada pagamento.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoSegundo(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Exceto com rela��o ao disposto na cl�usula terceira ");
		textoClausula.append("e seus par�grafos, os arquivos para o pagamento dos tributos e contas de consumo ");
		textoClausula.append("dever�o ser transmitidos ao Banco, obrigatoriamente, ");
		textoClausula.append("at� �s 23h30min (hor�rio de Bras�lia), do dia indicado para pagamento, para a realiza��o do pagamento.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiro(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente dever� provisionar a(s) sua(s) conta(s) corrente(s) ");
		textoClausula.append( saidaImprimirContrato.getCdContaAgenciaGestor() + "-" + saidaImprimirContrato.getCdDigitoContaGestor()+ ", ag�ncia ");
		textoClausula.append( saidaImprimirContrato.getCdAgenciaGestorContrato() + "-" + saidaImprimirContrato.getCdDigitoAgenciaGestor());
		textoClausula.append(", cadastrada no Banco Bradesco S.A., com valor suficiente para efetiva��o dos pagamentos, ficando o Banco ");
		textoClausula.append("autorizado a realizar na(s) referida(s) conta(s) corrente(s) os respectivos d�bitos, ");
		textoClausula.append("sendo que ele n�o se responsabilizar� pela n�o realiza��o dos pagamentos nos seguintes casos:");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo terceiro a.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiroA(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("falhas nas informa��es prestadas;");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("A) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo terceiro b.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiroB(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("atraso no envio dos arquivos ao Banco;");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("B) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo terceiro c.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiroC(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("aus�ncia da autoriza��o de d�bito dos tributos e contas de consumo constantes do arquivo ");
		textoClausula.append("remessa por parte do Cliente; e");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("C) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo terceiro d.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiroD(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("inexist�ncia de saldo suficiente e dispon�vel para a realiza��o dos pagamentos ");
		textoClausula.append("em conformidade com o previsto no par�grafo primeiro, da cl�usula quarta.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("D) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo quarto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoQuarto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente se responsabiliza por eventuais perdas e danos que venham ");
		textoClausula.append("a causar a si pr�pria, ao Banco e a terceiros, e que decorram do acesso e utiliza��o inadequada ");
		textoClausula.append("ou impr�pria do seu software e/ou sistema por pessoas n�o autorizadas ou credenciadas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Quarto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo quinto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoQuinto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("� de responsabilidade do Cliente os pagamentos dos tributos e contas ");
		textoClausula.append("de consumo realizados ap�s a data de vencimento sem os devidos acr�scimos, ");
		textoClausula.append("bem como pela respectiva regulariza��o junto aos �rg�os/Concession�rias competentes.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Quinto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo sexto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoSexto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente se compromete a realizar em seu software e/ou sistema ");
		textoClausula.append("os ajustes necess�rios em raz�o de eventuais altera��es na legisla��o aplic�vel ao ");
		textoClausula.append("pagamento eletr�nico de tributos e contas de consumo, bem como de atualiza��o de regras e tabelas disponibilizadas pelos �rg�os competentes.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Sexto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo setimo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoSetimo(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Caso as altera��es indicadas no par�grafo sexto, interfiram e modifiquem os layouts ");
		textoClausula.append("dos comprovantes j� validados pelo Banco, conforme disposto na cl�usula quinta e nos anexos pertinentes, ");
		textoClausula.append("o Cliente, conforme orienta��o do Banco, dever� realizar as adapta��es necess�rias nos layouts ");
		textoClausula.append("dos comprovantes, e, ap�s aprova��o e valida��o pelo Banco, as partes dever�o ");
		textoClausula.append("alterar os referidos anexos, mediante termo aditivo ao Contrato, para consignar os novos modelos de layouts de comprovantes.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo S�timo: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula terceira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTerceira(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Banco processar� os arquivos enviados pelo Cliente, ");
		textoClausula.append("entre 00h30min e 23h30min (hor�rio de Bras�lia), efetuando a quita��o dos tributos e contas de consumo ");
		textoClausula.append("dos arquivos recebidos e aceitos pelo Banco, debitando da(s) conta(s) corrente(s) do Cliente, ");
		textoClausula.append("desde que exista saldo suficiente e dispon�vel.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Terceira: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula terceira1.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTerceira1(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Banco poder�, a seu crit�rio, efetuar pagamentos em montante ");
		textoClausula.append("superior ao saldo dispon�vel na(s) conta(s) do Cliente. Se isso vier a ocorrer, o(s) valor(es) ");
		textoClausula.append("disponibilizado(s) pelo Banco � Cliente ser� havido como �adiantamento a depositante�.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula terceira2.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTerceira2(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Na ocorr�ncia de situa��es at�picas que impe�am o processamento dos ");
		textoClausula.append("arquivos remessa enviados pelo Cliente, em at� 60 (sessenta) minutos ap�s o seu envio, ");
		textoClausula.append("o Banco enviar� arquivo retorno, contendo os dados dos tributos ");
		textoClausula.append("e contas de consumo como pagamentos n�o realizados.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuarta(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O BANCO, ap�s o recebimento e processamento do arquivo remessa ");
		textoClausula.append("encaminhado pelo Cliente, tornar� dispon�vel o arquivo retorno contendo o resultado do processamento, ");
		textoClausula.append("ficando sob a exclusiva responsabilidade do Cliente realizar a confer�ncia dos dados ");
		textoClausula.append("constantes desse arquivo retorno e verificar/corrigir os dados dos registros que resultarem inconsistentes, ");
		textoClausula.append("at� a data programada para o d�bito.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Quarta: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaPrimeiro(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Na hip�tese de agendamentos de pagamentos de tributos e contas ");
		textoClausula.append("de consumo, a consulta do saldo na(s) conta(s) corrente(s) do Cliente ser� realizada �s 21h00 ");
		textoClausula.append("(hor�rio de Bras�lia), do dia indicado para o pagamento.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaSegundo(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Quando o pagamento for realizado no mesmo dia do envio do arquivo remessa, ");
		textoClausula.append("o Banco far� a consulta de saldo na(s) conta(s) corrente(s) do Cliente ");
		textoClausula.append("imediatamente ap�s a recep��o e processamento do arquivo remessa, e, caso n�o ");
		textoClausula.append("haja saldo suficiente e dispon�vel naquele momento, os tributos ");
		textoClausula.append("e contas de consumo ser�o devolvidos ao CLIENTE como n�o pagos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaTerceiro(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O saldo dispon�vel ser� representado por:");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta terceiro a.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaTerceiroA(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("disponibilidade real na(s) conta(s) corrente(s) indicada(s) no par�grafo terceiro, da cl�usula segunda, do Cliente;");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("A) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta terceiro b.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaTerceiroB(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("aplica��es financeiras com baixa autom�tica, registrados na(s) referida(s) conta(s) corrente(s); e");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("B) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta terceiro c.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaTerceiroC(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("limite dispon�vel decorrente de contrato de: (i) Saque - F�cil ");
		textoClausula.append("ou (ii) Conta Corrente Garantida ou (iii) Cr�dito Rotativo com Garantia de Cheques (CRGC).");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("C) ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta quarto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaQuarto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Na hip�tese do saldo dispon�vel ser insuficiente para a quita��o total dos tributos ");
		textoClausula.append("e contas de consumo agendados, os pagamentos ser�o efetuados at� ");
		textoClausula.append("o montante do saldo dispon�vel observada a ordem decrescente dos valores dos pagamentos a serem efetuados.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Quarto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quarta quinto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaQuinto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Os tributos e contas de consumo pagos em dias n�o �teis, constar�o ");
		textoClausula.append("no extrato banc�rio do Cliente no pr�ximo dia �til.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Quinto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quinta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuinta(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Banco comprovar� a realiza��o dos pagamentos mediante o envio ao Cliente ");
		textoClausula.append("de arquivo retorno contendo os dados dos tributos e contas de consumo pagos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Quinta: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quinta primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaPrimeiro(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Banco disponibilizar� ao Cliente, imediatamente ap�s a realiza��o dos pagamentos, ");
		textoClausula.append("arquivo retorno com os dados para emiss�o dos comprovantes dos pagamentos realizados.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quinta segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaSegundo(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O arquivo retorno de que trata o item 5.2, acima, ficar� dispon�vel ao Cliente pelo prazo ");
		textoClausula.append("m�ximo de 10 (dez) dias �teis, sendo que, ap�s esse per�odo, os arquivos retornos n�o extra�dos ");
		textoClausula.append("pelo Cliente ser�o automaticamente exclu�dos.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quinta terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaTerceiro(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Em raz�o do Cliente utilizar software e/ou sistema pr�prio, por ele desenvolvido, ");
		textoClausula.append("de acordo com o padr�o previamente estabelecido pelo Banco, o Cliente declara que os ");
		textoClausula.append("comprovantes dos pagamentos realizados e que s�o emitidos por esse software e/ou ");
		textoClausula.append("sistema cont�m o formato aprovado pelo Banco, cujos modelos, contendo as especifica��es pertinentes, ");
		textoClausula.append("e devidamente assinados pelas partes, fazem parte integrante deste Anexo na forma de Adendo.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quinta quarto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaQuarto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Todas as informa��es contidas nos comprovantes (campos obrigat�rios) dever�o ");
		textoClausula.append("ser as mesmas fornecidas pelo Banco por meio do arquivo retorno dos pagamentos realizados, ");
		textoClausula.append("ou seja, o Cliente dever� manter total integridade dos dados enviados pelo Banco ");
		textoClausula.append("para a emiss�o dos comprovantes, com as devidas informa��es e autentica��es.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Quarto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula quinta quinto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaQuinto(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente assume, neste ato, de maneira irrevog�vel e irretrat�vel, total e integral ");
		textoClausula.append("responsabilidade, por quaisquer perdas e danos, pessoais, morais ou materiais, que vierem a ser sofrido ");
		textoClausula.append("pelo Banco ou terceiros em raz�o do descumprimento do disposto no par�grafo quarto.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Quinto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher clausula sexta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaSexta(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Para todos os fins e efeitos de direito, o Cliente reconhecer� como l�quido e certo, ");
		textoClausula.append("o valor de todos os lan�amentos de d�bito, efetuados em sua(s) conta(s) corrente(s) indicada(s) ");
		textoClausula.append("no par�grafo terceiro, da cl�usula segunda.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Sexto: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher quinta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherQuinta(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("O Cliente n�o pagar� ao Banco quaisquer valores pelos servi�os de Pagamento ");
		textoClausula.append("Eletr�nico de Tributos ora contratados, por�m, n�o estar� isento do pagamento das tarifas de movimenta��o ");
		textoClausula.append("que venham a incidir em sua(s) conta(s) corrente(s) decorrentes dos pagamentos efetuados.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Cl�usula Quinta: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher quinta primeira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherQuintaPrimeira(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Posteriormente, as Partes poder�o negociar a cobran�a de tarifas pela presta��o ");
		textoClausula.append("dos servi�os objeto deste Anexo, mediante a celebra��o do competente termo aditivo ao Contrato.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher acordo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAcordo(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Por se acharem de pleno acordo, com tudo aqui pactuado, as partes firmam este ");
		textoClausula.append("Anexo em 03 (tr�s) vias de igual teor e forma, juntamente com as testemunhas abaixo:");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher data.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherData(Document document) throws DocumentException {
		Paragraph data = new Paragraph(new Chunk("Osasco, " + DateUtils.formatarDataPorExtenso(new Date()) + ".", fTexto));
		data.setAlignment(Paragraph.ALIGN_RIGHT);
		document.add(data);
	}
	
	/**
	 * Assinatura um.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaUm(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  CLIENTE:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  BANCO BRADESCO S/A:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  Ag�ncia:", fTexto));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Esq);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher assinaturas3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAssinaturas3(Document document) throws DocumentException {
		Paragraph testemunha = new Paragraph(new Chunk("  TESTEMUNHAS:"));
		testemunha.setAlignment(Element.ALIGN_LEFT);
		document.add(testemunha);
	}
	
	/**
	 * Assinatura dois.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaDois(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
		celula1.addElement(pLinha3Esq);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Dir);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}

	/**
	 * Get: saidaImprimirContrato.
	 *
	 * @return saidaImprimirContrato
	 */
	public ImprimirContratoSaidaDTO getSaidaImprimirContrato() {
		return saidaImprimirContrato;
	}

	/**
	 * Set: saidaImprimirContrato.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public void setSaidaImprimirContrato(
			ImprimirContratoSaidaDTO saidaImprimirContrato) {
		this.saidaImprimirContrato = saidaImprimirContrato;
	}

}

