/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarresponscontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaUnidadeOrganizacional
     */
    private long _cdPessoaUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdPessoaUnidadeOrganizaciona
     */
    private boolean _has_cdPessoaUnidadeOrganizacional;

    /**
     * Field _nrSeqUnidadeOrganizacional
     */
    private int _nrSeqUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _nrSeqUnidadeOrganizacional
     */
    private boolean _has_nrSeqUnidadeOrganizacional;

    /**
     * Field _cdTipoRespOrganizacional
     */
    private int _cdTipoRespOrganizacional = 0;

    /**
     * keeps track of state for field: _cdTipoRespOrganizacional
     */
    private boolean _has_cdTipoRespOrganizacional;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarresponscontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaUnidadeOrganizacional
     * 
     */
    public void deleteCdPessoaUnidadeOrganizacional()
    {
        this._has_cdPessoaUnidadeOrganizacional= false;
    } //-- void deleteCdPessoaUnidadeOrganizacional() 

    /**
     * Method deleteCdTipoRespOrganizacional
     * 
     */
    public void deleteCdTipoRespOrganizacional()
    {
        this._has_cdTipoRespOrganizacional= false;
    } //-- void deleteCdTipoRespOrganizacional() 

    /**
     * Method deleteNrSeqUnidadeOrganizacional
     * 
     */
    public void deleteNrSeqUnidadeOrganizacional()
    {
        this._has_nrSeqUnidadeOrganizacional= false;
    } //-- void deleteNrSeqUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdPessoaUnidadeOrganizacional'.
     * 
     * @return long
     * @return the value of field 'cdPessoaUnidadeOrganizacional'.
     */
    public long getCdPessoaUnidadeOrganizacional()
    {
        return this._cdPessoaUnidadeOrganizacional;
    } //-- long getCdPessoaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdTipoRespOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdTipoRespOrganizacional'.
     */
    public int getCdTipoRespOrganizacional()
    {
        return this._cdTipoRespOrganizacional;
    } //-- int getCdTipoRespOrganizacional() 

    /**
     * Returns the value of field 'nrSeqUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSeqUnidadeOrganizacional'.
     */
    public int getNrSeqUnidadeOrganizacional()
    {
        return this._nrSeqUnidadeOrganizacional;
    } //-- int getNrSeqUnidadeOrganizacional() 

    /**
     * Method hasCdPessoaUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaUnidadeOrganizacional()
    {
        return this._has_cdPessoaUnidadeOrganizacional;
    } //-- boolean hasCdPessoaUnidadeOrganizacional() 

    /**
     * Method hasCdTipoRespOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRespOrganizacional()
    {
        return this._has_cdTipoRespOrganizacional;
    } //-- boolean hasCdTipoRespOrganizacional() 

    /**
     * Method hasNrSeqUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqUnidadeOrganizacional()
    {
        return this._has_nrSeqUnidadeOrganizacional;
    } //-- boolean hasNrSeqUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaUnidadeOrganizacional'.
     * 
     * @param cdPessoaUnidadeOrganizacional the value of field
     * 'cdPessoaUnidadeOrganizacional'.
     */
    public void setCdPessoaUnidadeOrganizacional(long cdPessoaUnidadeOrganizacional)
    {
        this._cdPessoaUnidadeOrganizacional = cdPessoaUnidadeOrganizacional;
        this._has_cdPessoaUnidadeOrganizacional = true;
    } //-- void setCdPessoaUnidadeOrganizacional(long) 

    /**
     * Sets the value of field 'cdTipoRespOrganizacional'.
     * 
     * @param cdTipoRespOrganizacional the value of field
     * 'cdTipoRespOrganizacional'.
     */
    public void setCdTipoRespOrganizacional(int cdTipoRespOrganizacional)
    {
        this._cdTipoRespOrganizacional = cdTipoRespOrganizacional;
        this._has_cdTipoRespOrganizacional = true;
    } //-- void setCdTipoRespOrganizacional(int) 

    /**
     * Sets the value of field 'nrSeqUnidadeOrganizacional'.
     * 
     * @param nrSeqUnidadeOrganizacional the value of field
     * 'nrSeqUnidadeOrganizacional'.
     */
    public void setNrSeqUnidadeOrganizacional(int nrSeqUnidadeOrganizacional)
    {
        this._nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
        this._has_nrSeqUnidadeOrganizacional = true;
    } //-- void setNrSeqUnidadeOrganizacional(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarresponscontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarresponscontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarresponscontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarresponscontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
