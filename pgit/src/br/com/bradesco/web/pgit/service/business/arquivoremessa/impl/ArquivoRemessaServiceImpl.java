/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.arquivoremessa.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.arquivoremessa.IArquivoRemessaService;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.IArquivoRemessaServiceConstants;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarArquivoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarArquivoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.DetalharSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.DetalharSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ExcluirSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ExcluirSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.IncluirSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.IncluirSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.OcorrenciasDetalharSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.request.ConsultarArquivoRecuperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.response.ConsultarArquivoRecuperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.request.ConsultarSolicRecuperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.response.ConsultarSolicRecuperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.request.DetalharSolicRecuperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.DetalharSolicRecuperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicrecuperacao.request.ExcluirSolicRecuperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicrecuperacao.response.ExcluirSolicRecuperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.IncluirSolicRecuperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.response.IncluirSolicRecuperacaoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ArquivoRemessa
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ArquivoRemessaServiceImpl implements IArquivoRemessaService {
	
	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public ArquivoRemessaServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.arquivoremessa.IArquivoRemessaService#consultarSolicRecuperacao(br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarSolicRecuperacaoEntradaDTO)
     */
    public List<ConsultarSolicRecuperacaoSaidaDTO> consultarSolicRecuperacao(ConsultarSolicRecuperacaoEntradaDTO consultarSolicRecuperacaoEntradaDTO) {
		
		ConsultarSolicRecuperacaoRequest request = new ConsultarSolicRecuperacaoRequest();
		ConsultarSolicRecuperacaoResponse response = new ConsultarSolicRecuperacaoResponse();
		List<ConsultarSolicRecuperacaoSaidaDTO> listaSaida = new ArrayList<ConsultarSolicRecuperacaoSaidaDTO>();		
		
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(consultarSolicRecuperacaoEntradaDTO.getCdPessoaJuridicaContrato()));		
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(consultarSolicRecuperacaoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(consultarSolicRecuperacaoEntradaDTO.getNrSequenciaContratoNegocio()));        
		request.setDtInicioSolicitacao(PgitUtil.verificaStringNula(consultarSolicRecuperacaoEntradaDTO.getDtInicioSolicitacao()));    
		request.setDtFinalSolicitacao(PgitUtil.verificaStringNula(consultarSolicRecuperacaoEntradaDTO.getDtFinalSolicitacao()));    
		request.setCdUsuarioSolicitacao(PgitUtil.verificaStringNula(consultarSolicRecuperacaoEntradaDTO.getCdUsuarioSolicitacao()));    
		request.setCdSituacaoSolicitacaoRecuperacao(PgitUtil.verificaIntegerNulo(consultarSolicRecuperacaoEntradaDTO.getCdSituacaoSolicitacaoRecuperacao()));		
		request.setNrOcorrencias(IArquivoRemessaServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		
		response = getFactoryAdapter().getConsultarSolicRecuperacaoPDCAdapter().invokeProcess(request);
		
		ConsultarSolicRecuperacaoSaidaDTO saidaDTO;
		
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			saidaDTO = new ConsultarSolicRecuperacaoSaidaDTO();			
			saidaDTO.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
			saidaDTO.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
			saidaDTO.setNumeroLinhas(response.getNumeroLinhas());
			saidaDTO.setNrSolicitacaoRecuperacao(response.getOcorrencias(i).getNrSolicitacaoRecuperacao());
			saidaDTO.setHrSolicitacaoRecuperacao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getHrSolicitacaoRecuperacao()));
			saidaDTO.setCdUsuarioSolicitacao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdUsuarioSolicitacao()));
			saidaDTO.setDsSituacaoSolicitacaoRecuperacao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsSituacaoSolicitacaoRecuperacao()));
			saidaDTO.setDsDestinoRecuperacao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsDestinoRecuperacao()));
			saidaDTO.setDataFormatada(FormatarData.formatarDataTrilha(saidaDTO.getHrSolicitacaoRecuperacao()));
			listaSaida.add(saidaDTO);
		}		
		
    	return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoremessa.IArquivoRemessaService#detalharSolicRecuperacao(br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.DetalharSolicRecuperacaoEntradaDTO)
	 */
	public DetalharSolicRecuperacaoSaidaDTO detalharSolicRecuperacao(DetalharSolicRecuperacaoEntradaDTO detalharSolicRecuperacaoEntradaDTO) {
			
		DetalharSolicRecuperacaoRequest request = new DetalharSolicRecuperacaoRequest();
		DetalharSolicRecuperacaoResponse response = new DetalharSolicRecuperacaoResponse();		
		
		request.setNrSolicitacaoRecuperacao(PgitUtil.verificaIntegerNulo(detalharSolicRecuperacaoEntradaDTO.getNrSolicitacaoRecuperacao()));		
		
		DetalharSolicRecuperacaoSaidaDTO saidaDTO = new DetalharSolicRecuperacaoSaidaDTO();
		response = getFactoryAdapter().getDetalharSolicRecuperacaoPDCAdapter().invokeProcess(request);		
			
		saidaDTO.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
		saidaDTO.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
		saidaDTO.setHrSolicitacaoRecuperacao(PgitUtil.verificaStringNula(response.getHrSolicitacaoRecuperacao()));
		saidaDTO.setCdUsuarioSolicitacao(PgitUtil.verificaStringNula(response.getCdUsuarioSolicitacao()));
		saidaDTO.setDsSituacaoRecuperacao(PgitUtil.verificaStringNula(response.getDsSituacaoRecuperacao()));
	    saidaDTO.setDsMotvtoSituacaoRecuperacao(PgitUtil.verificaStringNula(response.getDsMotvtoSituacaoRecuperacao()));
	    saidaDTO.setDsDestinoRelatorio(PgitUtil.verificaStringNula(response.getDsDestinoRelatorio()));
	    saidaDTO.setDsDestinoArqIsd(PgitUtil.verificaStringNula(response.getDsDestinoArqIsd()));
	    saidaDTO.setDsDestinoArquivoRetorno(PgitUtil.verificaStringNula(response.getDsDestinoArquivoRetorno()));
	    saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());    
	    saidaDTO.setDsCanalInclusao(PgitUtil.verificaStringNula(response.getDsCanalInclusao()));
	    saidaDTO.setCdAutenticacaoSegregacaoInclusao(PgitUtil.verificaStringNula(response.getCdAutenticacaoSegregacaoInclusao()));
	    saidaDTO.setNmOperacaoFluxoInclusao(PgitUtil.verificaStringNula(response.getNmOperacaoFluxoInclusao()));
	    
	    saidaDTO.setHrInclusaoRegistro(PgitUtil.verificaStringNula(response.getHrInclusaoRegistro()));
	    
	   
	    
	    saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
	    saidaDTO.setDsCanalManutencao(PgitUtil.verificaStringNula(response.getDsCanalManutencao()));
	    saidaDTO.setCdAutenticacaoSegregacaoManutencao(PgitUtil.verificaStringNula(response.getCdAutenticacaoSegregacaoManutencao()));
	    saidaDTO.setNmOperacaoFluxoManutencao(PgitUtil.verificaStringNula(response.getNmOperacaoFluxoManutencao()));
	    saidaDTO.setHrManutencaoRegistro(PgitUtil.verificaStringNula(response.getHrManutencaoRegistro()));
	    saidaDTO.setNrSolicitacao(response.getNrSolicitacao());    			
	    
	    saidaDTO.setDataFormatada(FormatarData.formatarDataTrilha(response.getHrSolicitacaoRecuperacao()));
	    saidaDTO.setDataFormatadaInclusaoRemessa(FormatarData.formatarDataTrilha(saidaDTO.getHrSolicitacaoRecuperacao()));
	    saidaDTO.setDataFormatadaManutencaoRemessa(FormatarData.formatarDataTrilha(saidaDTO.getHrManutencaoRegistro()));
	    
	    List<OcorrenciasDetalharSolicRecuperacaoSaidaDTO> listaDetalharSolicRecuperacao = new ArrayList<OcorrenciasDetalharSolicRecuperacaoSaidaDTO>();
	    OcorrenciasDetalharSolicRecuperacaoSaidaDTO ocorrencia;
	    
	    for(int i=0;i<response.getOcorrenciasCount();i++){
	    	ocorrencia = new OcorrenciasDetalharSolicRecuperacaoSaidaDTO();
	    	ocorrencia.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
	    	ocorrencia.setCdCnpjCpf(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdCnpjCpf()));
	    	ocorrencia.setDsPessoa(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsPessoa()));
	    	ocorrencia.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
	    	ocorrencia.setDsTipoLayoutArquivo(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsTipoLayoutArquivo()));
	    	ocorrencia.setCdClienteTransferenciaArquivo(response.getOcorrencias(i).getCdClienteTransferenciaArquivo());
	    	ocorrencia.setNrSequenciaRemessa(response.getOcorrencias(i).getNrSequenciaRemessa());
	    	ocorrencia.setHrInclusaoRemessa(PgitUtil.verificaStringNula(response.getOcorrencias(i).getHrInclusaoRemessa()));
	    	
	    	ocorrencia.setDataFormatada(FormatarData.formatarDataTrilha(ocorrencia.getHrInclusaoRemessa()));
	    	
	    	ocorrencia.setDsSituacaoProcessamento(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsSituacaoProcessamento()));
	    	ocorrencia.setDsCondicaoProcessamento(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsCondicaoProcessamento()));
	    	ocorrencia.setDsMeioTransmissao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsMeioTransmissao()));
	    	java.math.BigDecimal num = new java.math.BigDecimal( response.getOcorrencias(i).getQtdeRegistro());
	    	ocorrencia.setQtdeRegistro(nf.format( num ));	    	
	    	ocorrencia.setVlrRegistro(PgitUtil.verificaBigDecimalNulo(response.getOcorrencias(i).getVlrRegistro()));  
	    	listaDetalharSolicRecuperacao.add(ocorrencia);	    	
	    }
	    
	    saidaDTO.setListaDetalharSolicRecuperacao(listaDetalharSolicRecuperacao);
	    
	    return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoremessa.IArquivoRemessaService#consultarArquivoRecuperacao(br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarArquivoRecuperacaoEntradaDTO)
	 */
	public List<ConsultarArquivoRecuperacaoSaidaDTO> consultarArquivoRecuperacao(ConsultarArquivoRecuperacaoEntradaDTO consultarArquivoRecuperacaoEntradaDTO) {
		
		ConsultarArquivoRecuperacaoRequest  request = new ConsultarArquivoRecuperacaoRequest();
		ConsultarArquivoRecuperacaoResponse response = new ConsultarArquivoRecuperacaoResponse();
		List<ConsultarArquivoRecuperacaoSaidaDTO> listaSaida = new ArrayList<ConsultarArquivoRecuperacaoSaidaDTO>();		
		
		request.setCdPessoa(PgitUtil.verificaLongNulo(consultarArquivoRecuperacaoEntradaDTO.getCdPessoa())); 
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(consultarArquivoRecuperacaoEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(consultarArquivoRecuperacaoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(consultarArquivoRecuperacaoEntradaDTO.getNrSequenciaContratoNegocio()));
		request.setDtInicioInclusaoRemessa(PgitUtil.verificaStringNula(consultarArquivoRecuperacaoEntradaDTO.getDtInicioInclusaoRemessa()));
		request.setDtFinalInclusaoRemessa(PgitUtil.verificaStringNula(consultarArquivoRecuperacaoEntradaDTO.getDtFinalInclusaoRemessa()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(consultarArquivoRecuperacaoEntradaDTO.getCdTipoLayoutArquivo()));
		request.setNrArquivoRemessa(PgitUtil.verificaLongNulo(consultarArquivoRecuperacaoEntradaDTO.getNrArquivoRemessa()));
		request.setCdSituacaoProcessamentoRemessa(PgitUtil.verificaIntegerNulo(consultarArquivoRecuperacaoEntradaDTO.getCdSituacaoProcessamentoRemessa()));
		request.setCdCondicaoProcessamentoRemessa(PgitUtil.verificaIntegerNulo(consultarArquivoRecuperacaoEntradaDTO.getCdCondicaoProcessamentoRemessa()));
		request.setNrOcorrencias(IArquivoRemessaServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR_INCLUIR);		
	
		response = getFactoryAdapter().getConsultarArquivoRecuperacaoPDCAdapter().invokeProcess(request);
		
		ConsultarArquivoRecuperacaoSaidaDTO saidaDTO;
		
		
		
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			saidaDTO = new ConsultarArquivoRecuperacaoSaidaDTO();			
			saidaDTO.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
			saidaDTO.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
			saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
			saidaDTO.setDsPessoaJuridicaContrato(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsPessoaJuridicaContrato()));
			saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			saidaDTO.setDsTipoContratoNegocio(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsTipoContratoNegocio()));
			saidaDTO.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
			saidaDTO.setDsContrato(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsContrato()));
			saidaDTO.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
			saidaDTO.setCdCnpjCpfPessoa(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdCnpjCpfPessoa()));
			saidaDTO.setDsPessoa(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsPessoa()));
			saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
			saidaDTO.setDsTipoLayoutArquivo(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsTipoLayoutArquivo()));
			saidaDTO.setCdClienteTransfArquivo(response.getOcorrencias(i).getCdClienteTransfArquivo());
			saidaDTO.setDsArquivoRemessaEnvio(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsArquivoRemessaEnvio()));
			saidaDTO.setNrArquivoRemessa(response.getOcorrencias(i).getNrArquivoRemessa());
			saidaDTO.setHrInclusaoRegistro(PgitUtil.verificaStringNula(response.getOcorrencias(i).getHrInclusaoRegistro()));
			saidaDTO.setDataFormatada(PgitUtil.verificaStringNula(FormatarData.formatarDataTrilha(saidaDTO.getHrInclusaoRegistro())));
			saidaDTO.setDsSituacaoProcessamentoRemessa(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsSituacaoProcessamentoRemessa()));
			saidaDTO.setDsCondicaoProcessamentoRemessa(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsCondicaoProcessamentoRemessa()));
			saidaDTO.setDsMeioTransmissao(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsMeioTransmissao()));
			saidaDTO.setDsAmbiente(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsAmbiente()));
			
			java.math.BigDecimal num = new java.math.BigDecimal( response.getOcorrencias(i).getQtRegistroArqRemessa());
			saidaDTO.setQtRegistroArqRemessa(nf.format( num ));
	
			saidaDTO.setVlRegistroArqRemessa(PgitUtil.verificaBigDecimalNulo(response.getOcorrencias(i).getVlRegistroArqRemessa()));			
			listaSaida.add(saidaDTO);
		}		
		
    	return listaSaida;
	}
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoremessa.IArquivoRemessaService#incluirSolicRecuperacao(br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.IncluirSolicRecuperacaoEntradaDTO)
	 */
	public IncluirSolicRecuperacaoSaidaDTO incluirSolicRecuperacao(IncluirSolicRecuperacaoEntradaDTO incluirSolicRecuperacaoEntradaDTO) {
			
		IncluirSolicRecuperacaoRequest request = new IncluirSolicRecuperacaoRequest();
		IncluirSolicRecuperacaoResponse response = new IncluirSolicRecuperacaoResponse();
		IncluirSolicRecuperacaoSaidaDTO saidaDTO = new IncluirSolicRecuperacaoSaidaDTO();	
			
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(incluirSolicRecuperacaoEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(incluirSolicRecuperacaoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(incluirSolicRecuperacaoEntradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdDestinoRelat(PgitUtil.verificaStringNula(incluirSolicRecuperacaoEntradaDTO.getCdDestinoRelat()));
		request.setCdDestinoIsd(PgitUtil.verificaStringNula(incluirSolicRecuperacaoEntradaDTO.getCdDestinoIsd()));
		request.setCdDestinoRet(PgitUtil.verificaStringNula(incluirSolicRecuperacaoEntradaDTO.getCdDestinoRet()));
		request.setQtdeOcorrencias(PgitUtil.verificaIntegerNulo(incluirSolicRecuperacaoEntradaDTO.getListaIncluirSolicRecuperacao().size()));              
		
		
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias>();
		
    	for (int i=0; i<incluirSolicRecuperacaoEntradaDTO.getListaIncluirSolicRecuperacao().size(); i++){    		     		
    		br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias();
    		ocorrencia.setCdPessoa(PgitUtil.verificaLongNulo(incluirSolicRecuperacaoEntradaDTO.getListaIncluirSolicRecuperacao().get(i).getCdPessoa()));    		
    		ocorrencia.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(incluirSolicRecuperacaoEntradaDTO.getListaIncluirSolicRecuperacao().get(i).getCdTipoLayoutArquivo()));
    		ocorrencia.setCdClienteTransfArq(PgitUtil.verificaLongNulo(incluirSolicRecuperacaoEntradaDTO.getListaIncluirSolicRecuperacao().get(i).getCdClienteTransfArquivo()));
    		ocorrencia.setNrArquivoRemessa(PgitUtil.verificaLongNulo(incluirSolicRecuperacaoEntradaDTO.getListaIncluirSolicRecuperacao().get(i).getNrArquivoRemessa()));
    		ocorrencia.setHrInclusaoRemessaSistema(PgitUtil.verificaStringNula(incluirSolicRecuperacaoEntradaDTO.getListaIncluirSolicRecuperacao().get(i).getHrInclusaoRegistro()));
    		ocorrencias.add(ocorrencia);     	  
     	} 	
    	
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias[]) ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.request.Ocorrencias[0]));
		
		response = getFactoryAdapter().getIncluirSolicRecuperacaoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
	
		return saidaDTO;
	}     
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivoremessa.IArquivoRemessaService#excluirSolicRecuperacao(br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ExcluirSolicRecuperacaoEntradaDTO)
	 */
	public ExcluirSolicRecuperacaoSaidaDTO excluirSolicRecuperacao(ExcluirSolicRecuperacaoEntradaDTO excluirSolicRecuperacaoEntradaDTO) {
				
		ExcluirSolicRecuperacaoRequest request = new ExcluirSolicRecuperacaoRequest();
		ExcluirSolicRecuperacaoResponse response = new ExcluirSolicRecuperacaoResponse();
		
	  
		request.setNrSolicitacaoRecuperacao(PgitUtil.verificaIntegerNulo(excluirSolicRecuperacaoEntradaDTO.getNrSolicitacaoRecuperacao()));	
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(excluirSolicRecuperacaoEntradaDTO.getCdTipoContratoNegocio()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(excluirSolicRecuperacaoEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(excluirSolicRecuperacaoEntradaDTO.getNrSequenciaContratoNegocio()));
		
		response = getFactoryAdapter().getExcluirSolicRecuperacaoPDCAdapter().invokeProcess(request);
		
		ExcluirSolicRecuperacaoSaidaDTO saidaDTO = new ExcluirSolicRecuperacaoSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());		
		
		return saidaDTO;
		
	}
}