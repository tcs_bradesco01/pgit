/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarInconsistenciasRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarInconsistenciasRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _codigoSistema
     */
    private java.lang.String _codigoSistema;

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filial
     */
    private int _filial = 0;

    /**
     * keeps track of state for field: _filial
     */
    private boolean _has_filial;

    /**
     * Field _controle
     */
    private int _controle = 0;

    /**
     * keeps track of state for field: _controle
     */
    private boolean _has_controle;

    /**
     * Field _perfilComunicacao
     */
    private long _perfilComunicacao = 0;

    /**
     * keeps track of state for field: _perfilComunicacao
     */
    private boolean _has_perfilComunicacao;

    /**
     * Field _numeroArquivoRemessa
     */
    private long _numeroArquivoRemessa = 0;

    /**
     * keeps track of state for field: _numeroArquivoRemessa
     */
    private boolean _has_numeroArquivoRemessa;

    /**
     * Field _horaInclusaoRemessa
     */
    private java.lang.String _horaInclusaoRemessa;

    /**
     * Field _numeroLoteRemessa
     */
    private long _numeroLoteRemessa = 0;

    /**
     * keeps track of state for field: _numeroLoteRemessa
     */
    private boolean _has_numeroLoteRemessa;

    /**
     * Field _sequenciaRegistroRemessa
     */
    private long _sequenciaRegistroRemessa = 0;

    /**
     * keeps track of state for field: _sequenciaRegistroRemessa
     */
    private boolean _has_sequenciaRegistroRemessa;

    /**
     * Field _tipoPesquisa
     */
    private java.lang.String _tipoPesquisa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarInconsistenciasRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.request.ConsultarInconsistenciasRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteControle
     * 
     */
    public void deleteControle()
    {
        this._has_controle= false;
    } //-- void deleteControle() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilial
     * 
     */
    public void deleteFilial()
    {
        this._has_filial= false;
    } //-- void deleteFilial() 

    /**
     * Method deleteNumeroArquivoRemessa
     * 
     */
    public void deleteNumeroArquivoRemessa()
    {
        this._has_numeroArquivoRemessa= false;
    } //-- void deleteNumeroArquivoRemessa() 

    /**
     * Method deleteNumeroLoteRemessa
     * 
     */
    public void deleteNumeroLoteRemessa()
    {
        this._has_numeroLoteRemessa= false;
    } //-- void deleteNumeroLoteRemessa() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Method deletePerfilComunicacao
     * 
     */
    public void deletePerfilComunicacao()
    {
        this._has_perfilComunicacao= false;
    } //-- void deletePerfilComunicacao() 

    /**
     * Method deleteSequenciaRegistroRemessa
     * 
     */
    public void deleteSequenciaRegistroRemessa()
    {
        this._has_sequenciaRegistroRemessa= false;
    } //-- void deleteSequenciaRegistroRemessa() 

    /**
     * Returns the value of field 'codigoSistema'.
     * 
     * @return String
     * @return the value of field 'codigoSistema'.
     */
    public java.lang.String getCodigoSistema()
    {
        return this._codigoSistema;
    } //-- java.lang.String getCodigoSistema() 

    /**
     * Returns the value of field 'controle'.
     * 
     * @return int
     * @return the value of field 'controle'.
     */
    public int getControle()
    {
        return this._controle;
    } //-- int getControle() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'filial'.
     * 
     * @return int
     * @return the value of field 'filial'.
     */
    public int getFilial()
    {
        return this._filial;
    } //-- int getFilial() 

    /**
     * Returns the value of field 'horaInclusaoRemessa'.
     * 
     * @return String
     * @return the value of field 'horaInclusaoRemessa'.
     */
    public java.lang.String getHoraInclusaoRemessa()
    {
        return this._horaInclusaoRemessa;
    } //-- java.lang.String getHoraInclusaoRemessa() 

    /**
     * Returns the value of field 'numeroArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'numeroArquivoRemessa'.
     */
    public long getNumeroArquivoRemessa()
    {
        return this._numeroArquivoRemessa;
    } //-- long getNumeroArquivoRemessa() 

    /**
     * Returns the value of field 'numeroLoteRemessa'.
     * 
     * @return long
     * @return the value of field 'numeroLoteRemessa'.
     */
    public long getNumeroLoteRemessa()
    {
        return this._numeroLoteRemessa;
    } //-- long getNumeroLoteRemessa() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Returns the value of field 'perfilComunicacao'.
     * 
     * @return long
     * @return the value of field 'perfilComunicacao'.
     */
    public long getPerfilComunicacao()
    {
        return this._perfilComunicacao;
    } //-- long getPerfilComunicacao() 

    /**
     * Returns the value of field 'sequenciaRegistroRemessa'.
     * 
     * @return long
     * @return the value of field 'sequenciaRegistroRemessa'.
     */
    public long getSequenciaRegistroRemessa()
    {
        return this._sequenciaRegistroRemessa;
    } //-- long getSequenciaRegistroRemessa() 

    /**
     * Returns the value of field 'tipoPesquisa'.
     * 
     * @return String
     * @return the value of field 'tipoPesquisa'.
     */
    public java.lang.String getTipoPesquisa()
    {
        return this._tipoPesquisa;
    } //-- java.lang.String getTipoPesquisa() 

    /**
     * Method hasControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControle()
    {
        return this._has_controle;
    } //-- boolean hasControle() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilial()
    {
        return this._has_filial;
    } //-- boolean hasFilial() 

    /**
     * Method hasNumeroArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroArquivoRemessa()
    {
        return this._has_numeroArquivoRemessa;
    } //-- boolean hasNumeroArquivoRemessa() 

    /**
     * Method hasNumeroLoteRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLoteRemessa()
    {
        return this._has_numeroLoteRemessa;
    } //-- boolean hasNumeroLoteRemessa() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method hasPerfilComunicacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPerfilComunicacao()
    {
        return this._has_perfilComunicacao;
    } //-- boolean hasPerfilComunicacao() 

    /**
     * Method hasSequenciaRegistroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasSequenciaRegistroRemessa()
    {
        return this._has_sequenciaRegistroRemessa;
    } //-- boolean hasSequenciaRegistroRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codigoSistema'.
     * 
     * @param codigoSistema the value of field 'codigoSistema'.
     */
    public void setCodigoSistema(java.lang.String codigoSistema)
    {
        this._codigoSistema = codigoSistema;
    } //-- void setCodigoSistema(java.lang.String) 

    /**
     * Sets the value of field 'controle'.
     * 
     * @param controle the value of field 'controle'.
     */
    public void setControle(int controle)
    {
        this._controle = controle;
        this._has_controle = true;
    } //-- void setControle(int) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'filial'.
     * 
     * @param filial the value of field 'filial'.
     */
    public void setFilial(int filial)
    {
        this._filial = filial;
        this._has_filial = true;
    } //-- void setFilial(int) 

    /**
     * Sets the value of field 'horaInclusaoRemessa'.
     * 
     * @param horaInclusaoRemessa the value of field
     * 'horaInclusaoRemessa'.
     */
    public void setHoraInclusaoRemessa(java.lang.String horaInclusaoRemessa)
    {
        this._horaInclusaoRemessa = horaInclusaoRemessa;
    } //-- void setHoraInclusaoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'numeroArquivoRemessa'.
     * 
     * @param numeroArquivoRemessa the value of field
     * 'numeroArquivoRemessa'.
     */
    public void setNumeroArquivoRemessa(long numeroArquivoRemessa)
    {
        this._numeroArquivoRemessa = numeroArquivoRemessa;
        this._has_numeroArquivoRemessa = true;
    } //-- void setNumeroArquivoRemessa(long) 

    /**
     * Sets the value of field 'numeroLoteRemessa'.
     * 
     * @param numeroLoteRemessa the value of field
     * 'numeroLoteRemessa'.
     */
    public void setNumeroLoteRemessa(long numeroLoteRemessa)
    {
        this._numeroLoteRemessa = numeroLoteRemessa;
        this._has_numeroLoteRemessa = true;
    } //-- void setNumeroLoteRemessa(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Sets the value of field 'perfilComunicacao'.
     * 
     * @param perfilComunicacao the value of field
     * 'perfilComunicacao'.
     */
    public void setPerfilComunicacao(long perfilComunicacao)
    {
        this._perfilComunicacao = perfilComunicacao;
        this._has_perfilComunicacao = true;
    } //-- void setPerfilComunicacao(long) 

    /**
     * Sets the value of field 'sequenciaRegistroRemessa'.
     * 
     * @param sequenciaRegistroRemessa the value of field
     * 'sequenciaRegistroRemessa'.
     */
    public void setSequenciaRegistroRemessa(long sequenciaRegistroRemessa)
    {
        this._sequenciaRegistroRemessa = sequenciaRegistroRemessa;
        this._has_sequenciaRegistroRemessa = true;
    } //-- void setSequenciaRegistroRemessa(long) 

    /**
     * Sets the value of field 'tipoPesquisa'.
     * 
     * @param tipoPesquisa the value of field 'tipoPesquisa'.
     */
    public void setTipoPesquisa(java.lang.String tipoPesquisa)
    {
        this._tipoPesquisa = tipoPesquisa;
    } //-- void setTipoPesquisa(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarInconsistenciasRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.request.ConsultarInconsistenciasRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.request.ConsultarInconsistenciasRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.request.ConsultarInconsistenciasRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.request.ConsultarInconsistenciasRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
