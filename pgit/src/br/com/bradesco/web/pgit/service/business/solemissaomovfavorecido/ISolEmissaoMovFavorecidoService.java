/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ConsultarSolEmiAviMovtoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ConsultarSolEmiAviMovtoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.DetalharSolEmiAviMovtoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.DetalharSolEmiAviMovtoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ExcluirSolEmiAviMovtoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.ExcluirSolEmiAviMovtoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.IncluirSolEmiAviMovtoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean.IncluirSolEmiAviMovtoFavorecidoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: SolEmissaoMovFavorecido
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolEmissaoMovFavorecidoService {
	
	/**
	 * Consultar sol emi avi movto favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar sol emi avi movto favorecido saida dt o>
	 */
	List<ConsultarSolEmiAviMovtoFavorecidoSaidaDTO> consultarSolEmiAviMovtoFavorecido(ConsultarSolEmiAviMovtoFavorecidoEntradaDTO entradaDTO);
	
	/**
	 * Detalhar sol emi avi movto favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar sol emi avi movto favorecido saida dto
	 */
	DetalharSolEmiAviMovtoFavorecidoSaidaDTO detalharSolEmiAviMovtoFavorecido(DetalharSolEmiAviMovtoFavorecidoEntradaDTO entradaDTO);
	
	/**
	 * Incluir sol emi avi movto favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the incluir sol emi avi movto favorecido saida dto
	 */
	IncluirSolEmiAviMovtoFavorecidoSaidaDTO incluirSolEmiAviMovtoFavorecido(IncluirSolEmiAviMovtoFavorecidoEntradaDTO entradaDTO);	
	
	/**
	 * Excluir sol emi avi movto favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir sol emi avi movto favorecido saida dto
	 */
	ExcluirSolEmiAviMovtoFavorecidoSaidaDTO excluirSolEmiAviMovtoFavorecido(ExcluirSolEmiAviMovtoFavorecidoEntradaDTO entradaDTO);
}

