/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarInconsistenciasResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarInconsistenciasResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _numeroLinhas
     */
    private int _numeroLinhas = 0;

    /**
     * keeps track of state for field: _numeroLinhas
     */
    private boolean _has_numeroLinhas;

    /**
     * Field _CMPIW034CONSULTASSAIDAList
     */
    private java.util.Vector _CMPIW034CONSULTASSAIDAList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarInconsistenciasResponse() 
     {
        super();
        _CMPIW034CONSULTASSAIDAList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.ConsultarInconsistenciasResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addCMPIW034CONSULTASSAIDA
     * 
     * 
     * 
     * @param vCMPIW034CONSULTASSAIDA
     */
    public void addCMPIW034CONSULTASSAIDA(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA vCMPIW034CONSULTASSAIDA)
        throws java.lang.IndexOutOfBoundsException
    {
        _CMPIW034CONSULTASSAIDAList.addElement(vCMPIW034CONSULTASSAIDA);
    } //-- void addCMPIW034CONSULTASSAIDA(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA) 

    /**
     * Method addCMPIW034CONSULTASSAIDA
     * 
     * 
     * 
     * @param index
     * @param vCMPIW034CONSULTASSAIDA
     */
    public void addCMPIW034CONSULTASSAIDA(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA vCMPIW034CONSULTASSAIDA)
        throws java.lang.IndexOutOfBoundsException
    {
        _CMPIW034CONSULTASSAIDAList.insertElementAt(vCMPIW034CONSULTASSAIDA, index);
    } //-- void addCMPIW034CONSULTASSAIDA(int, br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA) 

    /**
     * Method deleteNumeroLinhas
     * 
     */
    public void deleteNumeroLinhas()
    {
        this._has_numeroLinhas= false;
    } //-- void deleteNumeroLinhas() 

    /**
     * Method enumerateCMPIW034CONSULTASSAIDA
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateCMPIW034CONSULTASSAIDA()
    {
        return _CMPIW034CONSULTASSAIDAList.elements();
    } //-- java.util.Enumeration enumerateCMPIW034CONSULTASSAIDA() 

    /**
     * Method getCMPIW034CONSULTASSAIDA
     * 
     * 
     * 
     * @param index
     * @return CMPIW034CONSULTASSAIDA
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA getCMPIW034CONSULTASSAIDA(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _CMPIW034CONSULTASSAIDAList.size())) {
            throw new IndexOutOfBoundsException("getCMPIW034CONSULTASSAIDA: Index value '"+index+"' not in range [0.."+(_CMPIW034CONSULTASSAIDAList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA) _CMPIW034CONSULTASSAIDAList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA getCMPIW034CONSULTASSAIDA(int) 

    /**
     * Method getCMPIW034CONSULTASSAIDA
     * 
     * 
     * 
     * @return CMPIW034CONSULTASSAIDA
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA[] getCMPIW034CONSULTASSAIDA()
    {
        int size = _CMPIW034CONSULTASSAIDAList.size();
        br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA) _CMPIW034CONSULTASSAIDAList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA[] getCMPIW034CONSULTASSAIDA() 

    /**
     * Method getCMPIW034CONSULTASSAIDACount
     * 
     * 
     * 
     * @return int
     */
    public int getCMPIW034CONSULTASSAIDACount()
    {
        return _CMPIW034CONSULTASSAIDAList.size();
    } //-- int getCMPIW034CONSULTASSAIDACount() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numeroLinhas'.
     * 
     * @return int
     * @return the value of field 'numeroLinhas'.
     */
    public int getNumeroLinhas()
    {
        return this._numeroLinhas;
    } //-- int getNumeroLinhas() 

    /**
     * Method hasNumeroLinhas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLinhas()
    {
        return this._has_numeroLinhas;
    } //-- boolean hasNumeroLinhas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllCMPIW034CONSULTASSAIDA
     * 
     */
    public void removeAllCMPIW034CONSULTASSAIDA()
    {
        _CMPIW034CONSULTASSAIDAList.removeAllElements();
    } //-- void removeAllCMPIW034CONSULTASSAIDA() 

    /**
     * Method removeCMPIW034CONSULTASSAIDA
     * 
     * 
     * 
     * @param index
     * @return CMPIW034CONSULTASSAIDA
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA removeCMPIW034CONSULTASSAIDA(int index)
    {
        java.lang.Object obj = _CMPIW034CONSULTASSAIDAList.elementAt(index);
        _CMPIW034CONSULTASSAIDAList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA removeCMPIW034CONSULTASSAIDA(int) 

    /**
     * Method setCMPIW034CONSULTASSAIDA
     * 
     * 
     * 
     * @param index
     * @param vCMPIW034CONSULTASSAIDA
     */
    public void setCMPIW034CONSULTASSAIDA(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA vCMPIW034CONSULTASSAIDA)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _CMPIW034CONSULTASSAIDAList.size())) {
            throw new IndexOutOfBoundsException("setCMPIW034CONSULTASSAIDA: Index value '"+index+"' not in range [0.." + (_CMPIW034CONSULTASSAIDAList.size() - 1) + "]");
        }
        _CMPIW034CONSULTASSAIDAList.setElementAt(vCMPIW034CONSULTASSAIDA, index);
    } //-- void setCMPIW034CONSULTASSAIDA(int, br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA) 

    /**
     * Method setCMPIW034CONSULTASSAIDA
     * 
     * 
     * 
     * @param CMPIW034CONSULTASSAIDAArray
     */
    public void setCMPIW034CONSULTASSAIDA(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA[] CMPIW034CONSULTASSAIDAArray)
    {
        //-- copy array
        _CMPIW034CONSULTASSAIDAList.removeAllElements();
        for (int i = 0; i < CMPIW034CONSULTASSAIDAArray.length; i++) {
            _CMPIW034CONSULTASSAIDAList.addElement(CMPIW034CONSULTASSAIDAArray[i]);
        }
    } //-- void setCMPIW034CONSULTASSAIDA(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numeroLinhas'.
     * 
     * @param numeroLinhas the value of field 'numeroLinhas'.
     */
    public void setNumeroLinhas(int numeroLinhas)
    {
        this._numeroLinhas = numeroLinhas;
        this._has_numeroLinhas = true;
    } //-- void setNumeroLinhas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarInconsistenciasResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.ConsultarInconsistenciasResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.ConsultarInconsistenciasResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.ConsultarInconsistenciasResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.ConsultarInconsistenciasResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
