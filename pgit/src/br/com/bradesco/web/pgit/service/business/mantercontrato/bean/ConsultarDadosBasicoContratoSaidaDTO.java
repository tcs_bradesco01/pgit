/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarDadosBasicoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDadosBasicoContratoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdGrupoEconomicoParticipante. */
	private long cdGrupoEconomicoParticipante;

	/** Atributo dsGrupoEconomico. */
	private String dsGrupoEconomico;

	/** Atributo cdAtividadeEconomicaParticipante. */
	private int cdAtividadeEconomicaParticipante;

	/** Atributo dsAtividadeEconomica. */
	private String dsAtividadeEconomica;

	/** Atributo cdSegmentoEconomicoParticipante. */
	private int cdSegmentoEconomicoParticipante;

	/** Atributo dsSegmentoEconomico. */
	private String dsSegmentoEconomico;

	/** Atributo cdSubSegmentoEconomico. */
	private int cdSubSegmentoEconomico;

	/** Atributo dsSubSegmentoEconomico. */
	private String dsSubSegmentoEconomico;

	/** Atributo nomeContrato. */
	private String nomeContrato;

	/** Atributo cdIdioma. */
	private int cdIdioma;

	/** Atributo dsIdioma. */
	private String dsIdioma;

	/** Atributo cdMoeda. */
	private int cdMoeda;

	/** Atributo dsMoeda. */
	private String dsMoeda;

	/** Atributo cdOrigem. */
	private int cdOrigem;

	/** Atributo dsOrigem. */
	private String dsOrigem;

	/** Atributo dsPossuiAditivo. */
	private String dsPossuiAditivo;

	/** Atributo cdSituacaoContrato. */
	private int cdSituacaoContrato;

	/** Atributo dsSituacaoContrato. */
	private String dsSituacaoContrato;

	/** Atributo dataCadastroContrato. */
	private String dataCadastroContrato;

	/** Atributo horaCadastroContrato. */
	private String horaCadastroContrato;

	/** Atributo dataVigenciaContrato. */
	private String dataVigenciaContrato;

	/** Atributo horaVigenciaContrato. */
	private String horaVigenciaContrato;

	/** Atributo numeroComercialContrato. */
	private String numeroComercialContrato;

	/** Atributo dsTipoParticipacaoContrato. */
	private String dsTipoParticipacaoContrato;

	/** Atributo dataAssinaturaContrato. */
	private String dataAssinaturaContrato;

	/** Atributo horaAssinaturaContrato. */
	private String horaAssinaturaContrato;

	/** Atributo cdIndicaPermiteEncerramento. */
	private int cdIndicaPermiteEncerramento;

	/** Atributo dsIndicaPermiteEncerramento. */
	private String dsIndicaPermiteEncerramento;

	/** Atributo cdAgenciaOperadora. */
	private int cdAgenciaOperadora;

	/** Atributo nomeAgenciaOperadora. */
	private String nomeAgenciaOperadora;

	/** Atributo cdAgenciaContabil. */
	private int cdAgenciaContabil;

	/** Atributo nomeAgenciaContabil. */
	private String nomeAgenciaContabil;

	/** Atributo cdDeptoGestor. */
	private int cdDeptoGestor;

	/** Atributo dsDeptoGestor. */
	private String dsDeptoGestor;

	/** Atributo cdFuncionarioBradesco. */
	private long cdFuncionarioBradesco;

	/** Atributo dsFuncionarioBradesco. */
	private String dsFuncionarioBradesco;

	/** Atributo cdPessoaJuridica. */
	private long cdPessoaJuridica;

	/** Atributo numeroSequencialUnidadeOrganizacional. */
	private int numeroSequencialUnidadeOrganizacional;

	/** Atributo dataInclusaoContrato. */
	private String dataInclusaoContrato;

	/** Atributo horaInclusaoContrato. */
	private String horaInclusaoContrato;

	/** Atributo usuarioInclusaoContrato. */
	private String usuarioInclusaoContrato;

	/** Atributo cdTipoCanalInclusao. */
	private int cdTipoCanalInclusao;

	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;

	/** Atributo operacaoFluxoInclusao. */
	private String operacaoFluxoInclusao;

	/** Atributo dataManutencaoContrato. */
	private String dataManutencaoContrato;

	/** Atributo horaManutencaoContrato. */
	private String horaManutencaoContrato;

	/** Atributo usuarioManutencaoContrato. */
	private String usuarioManutencaoContrato;

	/** Atributo cdTipoCanalManutencao. */
	private int cdTipoCanalManutencao;

	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;

	/** Atributo operacaoFluxoManutencao. */
	private String operacaoFluxoManutencao;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo cdSetorContrato. */
	private Integer cdSetorContrato;

	/** Atributo dsSetorContrato. */
	private String dsSetorContrato;

	/** Atributo cdSituacaoMotivoContrato. */
	private Integer cdSituacaoMotivoContrato;

	/** Atributo dsSituacaoMotivoContrato. */
	private String dsSituacaoMotivoContrato;

	/** Atributo dsContratoNegocio. */
	private String dsContratoNegocio;

	/** Atributo dtVigenciaContrato. */
	private String dtVigenciaContrato;

	/** Atributo cdPessoaJuridicaProposta. */
	private Long cdPessoaJuridicaProposta;

	/** Atributo cdTipoContratoProposta. */
	private Integer cdTipoContratoProposta;

	/** Atributo nrContratoProposta. */
	private Long nrContratoProposta;

	/** Atributo dsPessoaJuridicaProposta. */
	private String dsPessoaJuridicaProposta;

	/** Atributo dsTipoContratoProposta. */
	private String dsTipoContratoProposta;

	/** Atributo vlSaldoVirtualTeste. */
	private BigDecimal vlSaldoVirtualTeste;
	
	/** Atributo nrSequenciaContratoOutros. */
	private long nrSequenciaContratoOutros;
	
	/** Atributo nrSequenciaContratoPagamentoSalario. */
	private long nrSequenciaContratoPagamentoSalario;

	/** Atributo cdFormaAutorizacaoPagamento. */
    private Integer cdFormaAutorizacaoPagamento = null;

    /** Atributo dsFormaAutorizacaoPagamento. */
    private String dsFormaAutorizacaoPagamento = null;

	/**
	 * Get: vlSaldoVirtualTeste.
	 *
	 * @return vlSaldoVirtualTeste
	 */
	public BigDecimal getVlSaldoVirtualTeste() {
		return vlSaldoVirtualTeste;
	}

	/**
	 * Set: vlSaldoVirtualTeste.
	 *
	 * @param vlSaldoVirtualTeste the vl saldo virtual teste
	 */
	public void setVlSaldoVirtualTeste(BigDecimal vlSaldoVirtualTeste) {
		this.vlSaldoVirtualTeste = vlSaldoVirtualTeste;
	}

	/**
	 * Get: cdAgenciaContabil.
	 *
	 * @return cdAgenciaContabil
	 */
	public int getCdAgenciaContabil() {

		return cdAgenciaContabil;
	}

	/**
	 * Set: cdAgenciaContabil.
	 *
	 * @param cdAgenciaContabil the cd agencia contabil
	 */
	public void setCdAgenciaContabil(int cdAgenciaContabil) {
		this.cdAgenciaContabil = cdAgenciaContabil;
	}

	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public int getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}

	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(int cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}

	/**
	 * Get: cdAtividadeEconomicaParticipante.
	 *
	 * @return cdAtividadeEconomicaParticipante
	 */
	public int getCdAtividadeEconomicaParticipante() {
		return cdAtividadeEconomicaParticipante;
	}

	/**
	 * Set: cdAtividadeEconomicaParticipante.
	 *
	 * @param cdAtividadeEconomicaParticipante the cd atividade economica participante
	 */
	public void setCdAtividadeEconomicaParticipante(int cdAtividadeEconomicaParticipante) {
		this.cdAtividadeEconomicaParticipante = cdAtividadeEconomicaParticipante;
	}

	/**
	 * Get: cdDeptoGestor.
	 *
	 * @return cdDeptoGestor
	 */
	public int getCdDeptoGestor() {
		return cdDeptoGestor;
	}

	/**
	 * Set: cdDeptoGestor.
	 *
	 * @param cdDeptoGestor the cd depto gestor
	 */
	public void setCdDeptoGestor(int cdDeptoGestor) {
		this.cdDeptoGestor = cdDeptoGestor;
	}

	/**
	 * Get: cdFuncionarioBradesco.
	 *
	 * @return cdFuncionarioBradesco
	 */
	public long getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}

	/**
	 * Set: cdFuncionarioBradesco.
	 *
	 * @param cdFuncionarioBradesco the cd funcionario bradesco
	 */
	public void setCdFuncionarioBradesco(long cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}

	/**
	 * Get: cdGrupoEconomicoParticipante.
	 *
	 * @return cdGrupoEconomicoParticipante
	 */
	public long getCdGrupoEconomicoParticipante() {
		return cdGrupoEconomicoParticipante;
	}

	/**
	 * Set: cdGrupoEconomicoParticipante.
	 *
	 * @param cdGrupoEconomicoParticipante the cd grupo economico participante
	 */
	public void setCdGrupoEconomicoParticipante(long cdGrupoEconomicoParticipante) {
		this.cdGrupoEconomicoParticipante = cdGrupoEconomicoParticipante;
	}

	/**
	 * Get: cdIdioma.
	 *
	 * @return cdIdioma
	 */
	public int getCdIdioma() {
		return cdIdioma;
	}

	/**
	 * Set: cdIdioma.
	 *
	 * @param cdIdioma the cd idioma
	 */
	public void setCdIdioma(int cdIdioma) {
		this.cdIdioma = cdIdioma;
	}

	/**
	 * Get: cdIndicaPermiteEncerramento.
	 *
	 * @return cdIndicaPermiteEncerramento
	 */
	public int getCdIndicaPermiteEncerramento() {
		return cdIndicaPermiteEncerramento;
	}

	/**
	 * Set: cdIndicaPermiteEncerramento.
	 *
	 * @param cdIndicaPermiteEncerramento the cd indica permite encerramento
	 */
	public void setCdIndicaPermiteEncerramento(int cdIndicaPermiteEncerramento) {
		this.cdIndicaPermiteEncerramento = cdIndicaPermiteEncerramento;
	}

	/**
	 * Get: cdMoeda.
	 *
	 * @return cdMoeda
	 */
	public int getCdMoeda() {
		return cdMoeda;
	}

	/**
	 * Set: cdMoeda.
	 *
	 * @param cdMoeda the cd moeda
	 */
	public void setCdMoeda(int cdMoeda) {
		this.cdMoeda = cdMoeda;
	}

	/**
	 * Get: cdOrigem.
	 *
	 * @return cdOrigem
	 */
	public int getCdOrigem() {
		return cdOrigem;
	}

	/**
	 * Set: cdOrigem.
	 *
	 * @param cdOrigem the cd origem
	 */
	public void setCdOrigem(int cdOrigem) {
		this.cdOrigem = cdOrigem;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdSegmentoEconomicoParticipante.
	 *
	 * @return cdSegmentoEconomicoParticipante
	 */
	public int getCdSegmentoEconomicoParticipante() {
		return cdSegmentoEconomicoParticipante;
	}

	/**
	 * Set: cdSegmentoEconomicoParticipante.
	 *
	 * @param cdSegmentoEconomicoParticipante the cd segmento economico participante
	 */
	public void setCdSegmentoEconomicoParticipante(int cdSegmentoEconomicoParticipante) {
		this.cdSegmentoEconomicoParticipante = cdSegmentoEconomicoParticipante;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public int getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(int cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdSubSegmentoEconomico.
	 *
	 * @return cdSubSegmentoEconomico
	 */
	public int getCdSubSegmentoEconomico() {
		return cdSubSegmentoEconomico;
	}

	/**
	 * Set: cdSubSegmentoEconomico.
	 *
	 * @param cdSubSegmentoEconomico the cd sub segmento economico
	 */
	public void setCdSubSegmentoEconomico(int cdSubSegmentoEconomico) {
		this.cdSubSegmentoEconomico = cdSubSegmentoEconomico;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public int getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(int cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public int getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(int cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dataAssinaturaContrato.
	 *
	 * @return dataAssinaturaContrato
	 */
	public String getDataAssinaturaContrato() {
		return dataAssinaturaContrato;
	}

	/**
	 * Set: dataAssinaturaContrato.
	 *
	 * @param dataAssinaturaContrato the data assinatura contrato
	 */
	public void setDataAssinaturaContrato(String dataAssinaturaContrato) {
		this.dataAssinaturaContrato = dataAssinaturaContrato;
	}

	/**
	 * Get: dataCadastroContrato.
	 *
	 * @return dataCadastroContrato
	 */
	public String getDataCadastroContrato() {
		return dataCadastroContrato;
	}

	/**
	 * Set: dataCadastroContrato.
	 *
	 * @param dataCadastroContrato the data cadastro contrato
	 */
	public void setDataCadastroContrato(String dataCadastroContrato) {
		this.dataCadastroContrato = dataCadastroContrato;
	}

	/**
	 * Get: dataInclusaoContrato.
	 *
	 * @return dataInclusaoContrato
	 */
	public String getDataInclusaoContrato() {
		return dataInclusaoContrato;
	}

	/**
	 * Set: dataInclusaoContrato.
	 *
	 * @param dataInclusaoContrato the data inclusao contrato
	 */
	public void setDataInclusaoContrato(String dataInclusaoContrato) {
		this.dataInclusaoContrato = dataInclusaoContrato;
	}

	/**
	 * Get: dataManutencaoContrato.
	 *
	 * @return dataManutencaoContrato
	 */
	public String getDataManutencaoContrato() {
		return dataManutencaoContrato;
	}

	/**
	 * Set: dataManutencaoContrato.
	 *
	 * @param dataManutencaoContrato the data manutencao contrato
	 */
	public void setDataManutencaoContrato(String dataManutencaoContrato) {
		this.dataManutencaoContrato = dataManutencaoContrato;
	}

	/**
	 * Get: dataVigenciaContrato.
	 *
	 * @return dataVigenciaContrato
	 */
	public String getDataVigenciaContrato() {
		return dataVigenciaContrato;
	}

	/**
	 * Set: dataVigenciaContrato.
	 *
	 * @param dataVigenciaContrato the data vigencia contrato
	 */
	public void setDataVigenciaContrato(String dataVigenciaContrato) {
		this.dataVigenciaContrato = dataVigenciaContrato;
	}

	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}

	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}

	/**
	 * Get: dsDeptoGestor.
	 *
	 * @return dsDeptoGestor
	 */
	public String getDsDeptoGestor() {
		return dsDeptoGestor;
	}

	/**
	 * Set: dsDeptoGestor.
	 *
	 * @param dsDeptoGestor the ds depto gestor
	 */
	public void setDsDeptoGestor(String dsDeptoGestor) {
		this.dsDeptoGestor = dsDeptoGestor;
	}

	/**
	 * Get: dsFuncionarioBradesco.
	 *
	 * @return dsFuncionarioBradesco
	 */
	public String getDsFuncionarioBradesco() {
		return dsFuncionarioBradesco;
	}

	/**
	 * Set: dsFuncionarioBradesco.
	 *
	 * @param dsFuncionarioBradesco the ds funcionario bradesco
	 */
	public void setDsFuncionarioBradesco(String dsFuncionarioBradesco) {
		this.dsFuncionarioBradesco = dsFuncionarioBradesco;
	}

	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}

	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}

	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}

	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}

	/**
	 * Get: dsIndicaPermiteEncerramento.
	 *
	 * @return dsIndicaPermiteEncerramento
	 */
	public String getDsIndicaPermiteEncerramento() {
		return dsIndicaPermiteEncerramento;
	}

	/**
	 * Set: dsIndicaPermiteEncerramento.
	 *
	 * @param dsIndicaPermiteEncerramento the ds indica permite encerramento
	 */
	public void setDsIndicaPermiteEncerramento(String dsIndicaPermiteEncerramento) {
		this.dsIndicaPermiteEncerramento = dsIndicaPermiteEncerramento;
	}

	/**
	 * Get: dsMoeda.
	 *
	 * @return dsMoeda
	 */
	public String getDsMoeda() {
		return dsMoeda;
	}

	/**
	 * Set: dsMoeda.
	 *
	 * @param dsMoeda the ds moeda
	 */
	public void setDsMoeda(String dsMoeda) {
		this.dsMoeda = dsMoeda;
	}

	/**
	 * Get: dsOrigem.
	 *
	 * @return dsOrigem
	 */
	public String getDsOrigem() {
		return dsOrigem;
	}

	/**
	 * Set: dsOrigem.
	 *
	 * @param dsOrigem the ds origem
	 */
	public void setDsOrigem(String dsOrigem) {
		this.dsOrigem = dsOrigem;
	}

	/**
	 * Get: dsPossuiAditivo.
	 *
	 * @return dsPossuiAditivo
	 */
	public String getDsPossuiAditivo() {
		return dsPossuiAditivo;
	}

	/**
	 * Set: dsPossuiAditivo.
	 *
	 * @param dsPossuiAditivo the ds possui aditivo
	 */
	public void setDsPossuiAditivo(String dsPossuiAditivo) {
		this.dsPossuiAditivo = dsPossuiAditivo;
	}

	/**
	 * Get: dsSegmentoEconomico.
	 *
	 * @return dsSegmentoEconomico
	 */
	public String getDsSegmentoEconomico() {
		return dsSegmentoEconomico;
	}

	/**
	 * Set: dsSegmentoEconomico.
	 *
	 * @param dsSegmentoEconomico the ds segmento economico
	 */
	public void setDsSegmentoEconomico(String dsSegmentoEconomico) {
		this.dsSegmentoEconomico = dsSegmentoEconomico;
	}

	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}

	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}

	/**
	 * Get: dsSubSegmentoEconomico.
	 *
	 * @return dsSubSegmentoEconomico
	 */
	public String getDsSubSegmentoEconomico() {
		return dsSubSegmentoEconomico;
	}

	/**
	 * Set: dsSubSegmentoEconomico.
	 *
	 * @param dsSubSegmentoEconomico the ds sub segmento economico
	 */
	public void setDsSubSegmentoEconomico(String dsSubSegmentoEconomico) {
		this.dsSubSegmentoEconomico = dsSubSegmentoEconomico;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	/**
	 * Get: dsTipoParticipacaoContrato.
	 *
	 * @return dsTipoParticipacaoContrato
	 */
	public String getDsTipoParticipacaoContrato() {
		return dsTipoParticipacaoContrato;
	}

	/**
	 * Set: dsTipoParticipacaoContrato.
	 *
	 * @param dsTipoParticipacaoContrato the ds tipo participacao contrato
	 */
	public void setDsTipoParticipacaoContrato(String dsTipoParticipacaoContrato) {
		this.dsTipoParticipacaoContrato = dsTipoParticipacaoContrato;
	}

	/**
	 * Get: horaAssinaturaContrato.
	 *
	 * @return horaAssinaturaContrato
	 */
	public String getHoraAssinaturaContrato() {
		return horaAssinaturaContrato;
	}

	/**
	 * Set: horaAssinaturaContrato.
	 *
	 * @param horaAssinaturaContrato the hora assinatura contrato
	 */
	public void setHoraAssinaturaContrato(String horaAssinaturaContrato) {
		this.horaAssinaturaContrato = horaAssinaturaContrato;
	}

	/**
	 * Get: horaCadastroContrato.
	 *
	 * @return horaCadastroContrato
	 */
	public String getHoraCadastroContrato() {
		return horaCadastroContrato;
	}

	/**
	 * Set: horaCadastroContrato.
	 *
	 * @param horaCadastroContrato the hora cadastro contrato
	 */
	public void setHoraCadastroContrato(String horaCadastroContrato) {
		this.horaCadastroContrato = horaCadastroContrato;
	}

	/**
	 * Get: horaInclusaoContrato.
	 *
	 * @return horaInclusaoContrato
	 */
	public String getHoraInclusaoContrato() {
		return horaInclusaoContrato;
	}

	/**
	 * Set: horaInclusaoContrato.
	 *
	 * @param horaInclusaoContrato the hora inclusao contrato
	 */
	public void setHoraInclusaoContrato(String horaInclusaoContrato) {
		this.horaInclusaoContrato = horaInclusaoContrato;
	}

	/**
	 * Get: horaManutencaoContrato.
	 *
	 * @return horaManutencaoContrato
	 */
	public String getHoraManutencaoContrato() {
		return horaManutencaoContrato;
	}

	/**
	 * Set: horaManutencaoContrato.
	 *
	 * @param horaManutencaoContrato the hora manutencao contrato
	 */
	public void setHoraManutencaoContrato(String horaManutencaoContrato) {
		this.horaManutencaoContrato = horaManutencaoContrato;
	}

	/**
	 * Get: horaVigenciaContrato.
	 *
	 * @return horaVigenciaContrato
	 */
	public String getHoraVigenciaContrato() {
		return horaVigenciaContrato;
	}

	/**
	 * Set: horaVigenciaContrato.
	 *
	 * @param horaVigenciaContrato the hora vigencia contrato
	 */
	public void setHoraVigenciaContrato(String horaVigenciaContrato) {
		this.horaVigenciaContrato = horaVigenciaContrato;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nomeAgenciaContabil.
	 *
	 * @return nomeAgenciaContabil
	 */
	public String getNomeAgenciaContabil() {
		return nomeAgenciaContabil;
	}

	/**
	 * Set: nomeAgenciaContabil.
	 *
	 * @param nomeAgenciaContabil the nome agencia contabil
	 */
	public void setNomeAgenciaContabil(String nomeAgenciaContabil) {
		this.nomeAgenciaContabil = nomeAgenciaContabil;
	}

	/**
	 * Get: nomeAgenciaOperadora.
	 *
	 * @return nomeAgenciaOperadora
	 */
	public String getNomeAgenciaOperadora() {
		return nomeAgenciaOperadora;
	}

	/**
	 * Set: nomeAgenciaOperadora.
	 *
	 * @param nomeAgenciaOperadora the nome agencia operadora
	 */
	public void setNomeAgenciaOperadora(String nomeAgenciaOperadora) {
		this.nomeAgenciaOperadora = nomeAgenciaOperadora;
	}

	/**
	 * Get: nomeContrato.
	 *
	 * @return nomeContrato
	 */
	public String getNomeContrato() {
		return nomeContrato;
	}

	/**
	 * Set: nomeContrato.
	 *
	 * @param nomeContrato the nome contrato
	 */
	public void setNomeContrato(String nomeContrato) {
		this.nomeContrato = nomeContrato;
	}

	/**
	 * Get: numeroComercialContrato.
	 *
	 * @return numeroComercialContrato
	 */
	public String getNumeroComercialContrato() {
		return numeroComercialContrato;
	}

	/**
	 * Set: numeroComercialContrato.
	 *
	 * @param numeroComercialContrato the numero comercial contrato
	 */
	public void setNumeroComercialContrato(String numeroComercialContrato) {
		this.numeroComercialContrato = numeroComercialContrato;
	}

	/**
	 * Get: numeroSequencialUnidadeOrganizacional.
	 *
	 * @return numeroSequencialUnidadeOrganizacional
	 */
	public int getNumeroSequencialUnidadeOrganizacional() {
		return numeroSequencialUnidadeOrganizacional;
	}

	/**
	 * Set: numeroSequencialUnidadeOrganizacional.
	 *
	 * @param numeroSequencialUnidadeOrganizacional the numero sequencial unidade organizacional
	 */
	public void setNumeroSequencialUnidadeOrganizacional(int numeroSequencialUnidadeOrganizacional) {
		this.numeroSequencialUnidadeOrganizacional = numeroSequencialUnidadeOrganizacional;
	}

	/**
	 * Get: operacaoFluxoInclusao.
	 *
	 * @return operacaoFluxoInclusao
	 */
	public String getOperacaoFluxoInclusao() {
		return operacaoFluxoInclusao;
	}

	/**
	 * Set: operacaoFluxoInclusao.
	 *
	 * @param operacaoFluxoInclusao the operacao fluxo inclusao
	 */
	public void setOperacaoFluxoInclusao(String operacaoFluxoInclusao) {
		this.operacaoFluxoInclusao = operacaoFluxoInclusao;
	}

	/**
	 * Get: operacaoFluxoManutencao.
	 *
	 * @return operacaoFluxoManutencao
	 */
	public String getOperacaoFluxoManutencao() {
		return operacaoFluxoManutencao;
	}

	/**
	 * Set: operacaoFluxoManutencao.
	 *
	 * @param operacaoFluxoManutencao the operacao fluxo manutencao
	 */
	public void setOperacaoFluxoManutencao(String operacaoFluxoManutencao) {
		this.operacaoFluxoManutencao = operacaoFluxoManutencao;
	}

	/**
	 * Get: usuarioInclusaoContrato.
	 *
	 * @return usuarioInclusaoContrato
	 */
	public String getUsuarioInclusaoContrato() {
		return usuarioInclusaoContrato;
	}

	/**
	 * Set: usuarioInclusaoContrato.
	 *
	 * @param usuarioInclusaoContrato the usuario inclusao contrato
	 */
	public void setUsuarioInclusaoContrato(String usuarioInclusaoContrato) {
		this.usuarioInclusaoContrato = usuarioInclusaoContrato;
	}

	/**
	 * Get: usuarioManutencaoContrato.
	 *
	 * @return usuarioManutencaoContrato
	 */
	public String getUsuarioManutencaoContrato() {
		return usuarioManutencaoContrato;
	}

	/**
	 * Set: usuarioManutencaoContrato.
	 *
	 * @param usuarioManutencaoContrato the usuario manutencao contrato
	 */
	public void setUsuarioManutencaoContrato(String usuarioManutencaoContrato) {
		this.usuarioManutencaoContrato = usuarioManutencaoContrato;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: cdSetorContrato.
	 *
	 * @return cdSetorContrato
	 */
	public Integer getCdSetorContrato() {
		return cdSetorContrato;
	}

	/**
	 * Set: cdSetorContrato.
	 *
	 * @param cdSetorContrato the cd setor contrato
	 */
	public void setCdSetorContrato(Integer cdSetorContrato) {
		this.cdSetorContrato = cdSetorContrato;
	}

	/**
	 * Get: dsSetorContrato.
	 *
	 * @return dsSetorContrato
	 */
	public String getDsSetorContrato() {
		return dsSetorContrato;
	}

	/**
	 * Set: dsSetorContrato.
	 *
	 * @param dsSetorContrato the ds setor contrato
	 */
	public void setDsSetorContrato(String dsSetorContrato) {
		this.dsSetorContrato = dsSetorContrato;
	}

	/**
	 * Get: cdSituacaoMotivoContrato.
	 *
	 * @return cdSituacaoMotivoContrato
	 */
	public Integer getCdSituacaoMotivoContrato() {
		return cdSituacaoMotivoContrato;
	}

	/**
	 * Set: cdSituacaoMotivoContrato.
	 *
	 * @param cdSituacaoMotivoContrato the cd situacao motivo contrato
	 */
	public void setCdSituacaoMotivoContrato(Integer cdSituacaoMotivoContrato) {
		this.cdSituacaoMotivoContrato = cdSituacaoMotivoContrato;
	}

	/**
	 * Get: dsSituacaoMotivoContrato.
	 *
	 * @return dsSituacaoMotivoContrato
	 */
	public String getDsSituacaoMotivoContrato() {
		return dsSituacaoMotivoContrato;
	}

	/**
	 * Set: dsSituacaoMotivoContrato.
	 *
	 * @param dsSituacaoMotivoContrato the ds situacao motivo contrato
	 */
	public void setDsSituacaoMotivoContrato(String dsSituacaoMotivoContrato) {
		this.dsSituacaoMotivoContrato = dsSituacaoMotivoContrato;
	}

	/**
	 * Get: dsContratoNegocio.
	 *
	 * @return dsContratoNegocio
	 */
	public String getDsContratoNegocio() {
		return dsContratoNegocio;
	}

	/**
	 * Set: dsContratoNegocio.
	 *
	 * @param dsContratoNegocio the ds contrato negocio
	 */
	public void setDsContratoNegocio(String dsContratoNegocio) {
		this.dsContratoNegocio = dsContratoNegocio;
	}

	/**
	 * Get: dtVigenciaContrato.
	 *
	 * @return dtVigenciaContrato
	 */
	public String getDtVigenciaContrato() {
		return dtVigenciaContrato;
	}

	/**
	 * Set: dtVigenciaContrato.
	 *
	 * @param dtVigenciaContrato the dt vigencia contrato
	 */
	public void setDtVigenciaContrato(String dtVigenciaContrato) {
		this.dtVigenciaContrato = dtVigenciaContrato;
	}

	/**
	 * Get: cdPessoaJuridicaProposta.
	 *
	 * @return cdPessoaJuridicaProposta
	 */
	public Long getCdPessoaJuridicaProposta() {
		return cdPessoaJuridicaProposta;
	}

	/**
	 * Set: cdPessoaJuridicaProposta.
	 *
	 * @param cdPessoaJuridicaProposta the cd pessoa juridica proposta
	 */
	public void setCdPessoaJuridicaProposta(Long cdPessoaJuridicaProposta) {
		this.cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
	}

	/**
	 * Get: cdTipoContratoProposta.
	 *
	 * @return cdTipoContratoProposta
	 */
	public Integer getCdTipoContratoProposta() {
		return cdTipoContratoProposta;
	}

	/**
	 * Set: cdTipoContratoProposta.
	 *
	 * @param cdTipoContratoProposta the cd tipo contrato proposta
	 */
	public void setCdTipoContratoProposta(Integer cdTipoContratoProposta) {
		this.cdTipoContratoProposta = cdTipoContratoProposta;
	}

	/**
	 * Get: dsPessoaJuridicaProposta.
	 *
	 * @return dsPessoaJuridicaProposta
	 */
	public String getDsPessoaJuridicaProposta() {
		return dsPessoaJuridicaProposta;
	}

	/**
	 * Set: dsPessoaJuridicaProposta.
	 *
	 * @param dsPessoaJuridicaProposta the ds pessoa juridica proposta
	 */
	public void setDsPessoaJuridicaProposta(String dsPessoaJuridicaProposta) {
		this.dsPessoaJuridicaProposta = dsPessoaJuridicaProposta;
	}

	/**
	 * Get: dsTipoContratoProposta.
	 *
	 * @return dsTipoContratoProposta
	 */
	public String getDsTipoContratoProposta() {
		return dsTipoContratoProposta;
	}

	/**
	 * Set: dsTipoContratoProposta.
	 *
	 * @param dsTipoContratoProposta the ds tipo contrato proposta
	 */
	public void setDsTipoContratoProposta(String dsTipoContratoProposta) {
		this.dsTipoContratoProposta = dsTipoContratoProposta;
	}

	/**
	 * Get: nrContratoProposta.
	 *
	 * @return nrContratoProposta
	 */
	public Long getNrContratoProposta() {
		return nrContratoProposta;
	}

	/**
	 * Set: nrContratoProposta.
	 *
	 * @param nrContratoProposta the nr contrato proposta
	 */
	public void setNrContratoProposta(Long nrContratoProposta) {
		this.nrContratoProposta = nrContratoProposta;
	}

	/**
	 * @return the nrSequenciaContratoOutros
	 */
	public long getNrSequenciaContratoOutros() {
		return nrSequenciaContratoOutros;
	}

	/**
	 * @param nrSequenciaContratoOutros the nrSequenciaContratoOutros to set
	 */
	public void setNrSequenciaContratoOutros(long nrSequenciaContratoOutros) {
		this.nrSequenciaContratoOutros = nrSequenciaContratoOutros;
	}

	/**
	 * @return the nrSequenciaContratoPagamentoSalario
	 */
	public long getNrSequenciaContratoPagamentoSalario() {
		return nrSequenciaContratoPagamentoSalario;
	}

	/**
	 * @param nrSequenciaContratoPagamentoSalario the nrSequenciaContratoPagamentoSalario to set
	 */
	public void setNrSequenciaContratoPagamentoSalario(
			long nrSequenciaContratoPagamentoSalario) {
		this.nrSequenciaContratoPagamentoSalario = nrSequenciaContratoPagamentoSalario;
	}

    /**
     * Nome: getCdFormaAutorizacaoPagamento
     *
     * @return cdFormaAutorizacaoPagamento
     */
    public Integer getCdFormaAutorizacaoPagamento() {
        return cdFormaAutorizacaoPagamento;
    }

    /**
     * Nome: setCdFormaAutorizacaoPagamento
     *
     * @param cdFormaAutorizacaoPagamento
     */
    public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
        this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
    }

    /**
     * Nome: getDsFormaAutorizacaoPagamento
     *
     * @return dsFormaAutorizacaoPagamento
     */
    public String getDsFormaAutorizacaoPagamento() {
        return dsFormaAutorizacaoPagamento;
    }

    /**
     * Nome: setDsFormaAutorizacaoPagamento
     *
     * @param dsFormaAutorizacaoPagamento
     */
    public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
        this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    }
}