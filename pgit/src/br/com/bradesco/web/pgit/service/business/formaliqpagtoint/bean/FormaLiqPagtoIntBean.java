/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService;
import br.com.bradesco.web.pgit.utils.MessageUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: FormaLiqPagtoIntBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FormaLiqPagtoIntBean {

    /** Atributo FORMA_LIQ_TITULO_OUTROS. */
    private static int FORMA_LIQ_TITULO_OUTROS = 24;

    /** Atributo FORMA_LIQ_TITULO_BRADESCO. */
    private static int FORMA_LIQ_TITULO_BRADESCO = 25;
	
	/** Atributo CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO. */
	private static final String CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO = "conFormaLiquidacaoPagamentoIntegrado";
	
	/** Atributo INC_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO. */
	private static final String INC_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO = "incFormaLiquidacaoPagamentoIntegrado";
	
	/** Atributo HIS_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO. */
	private static final String HIS_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO = "hisFormaLiquidacaoPagamentoIntegrado";
	
	/** Atributo formaLiqPgtoIntService. */
	private IFormaLiqPgtoIntService formaLiqPgtoIntService;
	
	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo entradaListarLiquidacaoPagto. */
	private ListarLiquidacaoPagamentoEntradaDTO entradaListarLiquidacaoPagto;
	
	/** Atributo entradaIncluirLiquidacaoPagto. */
	private IncluirLiquidacaoPagamentoEntradaDTO entradaIncluirLiquidacaoPagto = new IncluirLiquidacaoPagamentoEntradaDTO();
	
	/** Atributo entradaListarHistLiquidacaoPagto. */
	private ListarHistLiquidacaoPagamentoEntradaDTO entradaListarHistLiquidacaoPagto;
	
	/** Atributo entradaAlterarHistLiquidacaoPagto. */
	private AlterarLiquidacaoPagamentoEntradaDTO entradaAlterarHistLiquidacaoPagto = new AlterarLiquidacaoPagamentoEntradaDTO();
	
	/** Atributo listaLiquidacaoPagto. */
	private List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> listaLiquidacaoPagto;

	/** Atributo listaHistLiquidacaoPagto. */
	private List<ListarHistLiquidacaoPagamentoSaidaDTO> listaHistLiquidacaoPagto;
	
	/** Atributo saidaListarLiquidacaoPagto. */
	private ListarLiquidacaoPagamentoSaidaDTO saidaListarLiquidacaoPagto;
	
	/** Atributo ocorrenciasaidaListarLiquidacaoPagto. */
	private ListarLiquidacaoPagamentoOcorrenciaSaidaDTO ocorrenciasaidaListarLiquidacaoPagto;
	
	/** Atributo saidaDetalharLiquidacaoPagto. */
	private DetalharLiquidacaoPagamentoSaidaDTO saidaDetalharLiquidacaoPagto;
	
	/** Atributo saidaListarHistLiquidacaoPagto. */
	private ListarHistLiquidacaoPagamentoSaidaDTO saidaListarHistLiquidacaoPagto;
	
	/** Atributo saidaDetalharHistLiquidacaoPagto. */
	private DetalharHistLiquidacaoPagamentoSaidaDTO saidaDetalharHistLiquidacaoPagto;

	/** Atributo listaCentroCustoLupa. */
	private List<CentroCustoSaidaDTO> listaCentroCustoLupa = new ArrayList<CentroCustoSaidaDTO>();
	
	/** Atributo listaCentroCustoLupaSelectItem. */
	private List<SelectItem> listaCentroCustoLupaSelectItem =  new ArrayList<SelectItem>();
	
	/** Atributo formaLiquidacaoFiltro. */
	private Integer formaLiquidacaoFiltro;
	
	/** Atributo itemSelecionadoLista. */
	private String itemSelecionadoLista;
	
	/** Atributo itemSelecionadoListaHistorico. */
	private String itemSelecionadoListaHistorico;
	
	/** Atributo centroCustoLupaFiltro. */
	private String centroCustoLupaFiltro;
	
	/** Atributo formaLiquidacao. */
	private String formaLiquidacao;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo dataInicioManutencao. */
	private Date dataInicioManutencao;
	
	/** Atributo dataFimManutencao. */
	private Date dataFimManutencao;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo btoAcionadoHistorico. */
	private Boolean btoAcionadoHistorico;
	
	/** Atributo dsFormaLiquidacao. */
	private String dsFormaLiquidacao;
	
	/** Atributo listaFormaLiquidacao. */
	private List<SelectItem> listaFormaLiquidacao = new ArrayList<SelectItem>();
	
	/** Atributo listaFormaLiquidacaoHash. */
	private Map<Integer, String> listaFormaLiquidacaoHash = new HashMap<Integer, String>();
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	/** Atributo listaControleHistorico. */
	private List<SelectItem> listaControleHistorico = new ArrayList<SelectItem>();


	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public String getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(String itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Inicializar liq pagto integrado.
	 *
	 * @return the string
	 */
	public String inicializarLiqPagtoIntegrado() {
		entradaListarLiquidacaoPagto = new ListarLiquidacaoPagamentoEntradaDTO();
		entradaListarHistLiquidacaoPagto = new ListarHistLiquidacaoPagamentoEntradaDTO();
		setItemSelecionadoLista(null);
		setBtoAcionado(false);
		try{
			preencheFormaLiquidacao();
		}catch(PdcAdapterFunctionalException e){
			listaFormaLiquidacao = new ArrayList<SelectItem>();
		}
		return CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO;
	}
	
	// Consultar
	/**
	 * Pequisar pagto integrado.
	 *
	 * @return the string
	 */
	public String pequisarPagtoIntegrado() {
		consultarPagtoIntegrado();
		return CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO;
	}
	
	/**
	 * Consultar pagto integrado.
	 */
	public void consultarPagtoIntegrado() {
		listaLiquidacaoPagto = new ArrayList<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO>();

		saidaListarLiquidacaoPagto = formaLiqPgtoIntService.consultarLiquidacaoPagto(this.entradaListarLiquidacaoPagto);
		listaLiquidacaoPagto = saidaListarLiquidacaoPagto.getOcorrencias();
		
		
		setItemSelecionadoLista(null);
		setBtoAcionado(true);
	}
	
	/**
	 * Consultar pagto integrado conf exc.
	 *
	 * @return the string
	 */
	public String consultarPagtoIntegradoConfExc() {
		listaLiquidacaoPagto = new ArrayList<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO>();
		
		try {
		    saidaListarLiquidacaoPagto = formaLiqPgtoIntService.consultarLiquidacaoPagto(this.entradaListarLiquidacaoPagto);
	        listaLiquidacaoPagto = saidaListarLiquidacaoPagto.getOcorrencias();
		} catch (PdcAdapterFunctionalException e) {
            BradescoCommonServiceFactory.getLogManager().info(this.getClass(), e.getMessage());
		    String messageCode = MessageUtils.getMessageCode(e);
            if (messageCode == null || !messageCode.equals("PGIT0003")) {
                throw e;
            }
        }

		setItemSelecionadoLista(null);
		setBtoAcionado(true);
		return CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO;
	}
	
	/**
	 * Consultar pagto integrado excluir.
	 */
	public void consultarPagtoIntegradoExcluir() {
		listaLiquidacaoPagto = new ArrayList<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO>();
		setItemSelecionadoLista(null);
		setBtoAcionado(true);
		try{
			saidaListarLiquidacaoPagto = formaLiqPgtoIntService.consultarLiquidacaoPagto(this.entradaListarLiquidacaoPagto);
			listaLiquidacaoPagto = saidaListarLiquidacaoPagto.getOcorrencias();
		}catch(PdcAdapterFunctionalException e){
			saidaListarLiquidacaoPagto = new ListarLiquidacaoPagamentoSaidaDTO();
		}
	}

	/**
	 * Is lista liquidacao pagto empty.
	 *
	 * @return true, if is lista liquidacao pagto empty
	 */
	public boolean isListaLiquidacaoPagtoEmpty() {
		return this.listaLiquidacaoPagto == null || this.listaLiquidacaoPagto.isEmpty();
	}

	/**
	 * Get: selecionaLiquidacaoPagto.
	 *
	 * @return selecionaLiquidacaoPagto
	 */
	public List<SelectItem> getSelecionaLiquidacaoPagto() {

		if (this.listaLiquidacaoPagto == null || this.listaLiquidacaoPagto.isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listaLiquidacaoPagto.size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	// Incluir
	/**
	 * Incluir liq pagto integrado.
	 *
	 * @return the string
	 */
	public String incluirLiqPagtoIntegrado() {
		entradaIncluirLiquidacaoPagto = new IncluirLiquidacaoPagamentoEntradaDTO();
		setListaCentroCustoLupa(new ArrayList<CentroCustoSaidaDTO>());
		setCentroCustoLupaFiltro("");
		return INC_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO;
	}

	/**
	 * Avancar incluir liq pagto integrado.
	 *
	 * @return the string
	 */
	public String avancarIncluirLiqPagtoIntegrado() {
		
		if (this.getListaFormaLiquidacao() == null && this.entradaIncluirLiquidacaoPagto.getCdFormaLiquidacao().equals(0)) {
				return INC_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO;
		} else {
			for (SelectItem element : this.getListaFormaLiquidacao()) {
				if (element.getValue().equals(this.entradaIncluirLiquidacaoPagto.getCdFormaLiquidacao())) {
					this.setFormaLiquidacao(element.getLabel());
					break;
				}
			}
		}
		
		if (this.getListaCentroCustoLupa() == null && this.entradaIncluirLiquidacaoPagto.getCdSistema().trim().equals("")) {
			return INC_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO;
		} else {
			for (CentroCustoSaidaDTO saidaCentroCusto : getListaCentroCustoLupa()) {
				if (saidaCentroCusto.getCdCentroCusto().trim().equals(entradaIncluirLiquidacaoPagto.getCdSistema())) {
					this.setCentroCusto(saidaCentroCusto.getCodigoDescricao());
					break;
				}
			}
		}
			
		return "incFormaLiquidacaoPagamentoIntegrado2";
	}

	/**
	 * Confirmar incluisao liq pagto integrado.
	 *
	 * @return the string
	 */
	public String confirmarIncluisaoLiqPagtoIntegrado() {
		entradaIncluirLiquidacaoPagto.setCdControleHoraLimite(entradaIncluirLiquidacaoPagto.getCdSistema().equalsIgnoreCase("pgit") ? 1 : 2);
		entradaIncluirLiquidacaoPagto.setHrLimiteProcessamento(replaceDoisPontosPorUm(entradaIncluirLiquidacaoPagto.getHrLimiteProcessamento()));
		entradaIncluirLiquidacaoPagto.setHrConsultaFolhaPgto(replaceDoisPontosPorUm(entradaIncluirLiquidacaoPagto.getHrConsultaFolhaPgto()));
		entradaIncluirLiquidacaoPagto.setHrConsultasSaldoPagamento(replaceDoisPontosPorUm(entradaIncluirLiquidacaoPagto.getHrConsultasSaldoPagamento()));
		entradaIncluirLiquidacaoPagto.setHrLimiteValorSuperior(replaceDoisPontosPorUm(entradaIncluirLiquidacaoPagto.getHrLimiteValorSuperior()));

		try {
			IncluirLiquidacaoPagamentoSaidaDTO saidaIncluirLiquidacaoPagto = formaLiqPgtoIntService.incluirLiqPagamentoIntegrado(this.entradaIncluirLiquidacaoPagto);
			inverterPontosAposRetornoFluxo();

			BradescoFacesUtils.addInfoModalMessage("(" + saidaIncluirLiquidacaoPagto.getCodMensagem() + ") " + saidaIncluirLiquidacaoPagto.getMensagem(), CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO, BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterFunctionalException p) {
			inverterPontosAposRetornoFluxo();

			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "incFormaLiquidacaoPagamentoIntegrado2", BradescoViewExceptionActionType.ACTION, false);
			return null;
		} finally {
			consultarPagtoIntegrado();
		}

		return "incFormaLiquidacaoPagamentoIntegrado2";
	}

	/**
	 * Inverter pontos apos retorno fluxo.
	 */
	private void inverterPontosAposRetornoFluxo() {
		entradaIncluirLiquidacaoPagto.setHrLimiteProcessamento(replaceUmPontoPorDois(entradaIncluirLiquidacaoPagto.getHrLimiteProcessamento()));
		entradaIncluirLiquidacaoPagto.setHrConsultaFolhaPgto(replaceUmPontoPorDois(entradaIncluirLiquidacaoPagto.getHrConsultaFolhaPgto()));
		entradaIncluirLiquidacaoPagto.setHrConsultasSaldoPagamento(replaceUmPontoPorDois(entradaIncluirLiquidacaoPagto.getHrConsultasSaldoPagamento()));
		entradaIncluirLiquidacaoPagto.setHrLimiteValorSuperior(replaceUmPontoPorDois(entradaIncluirLiquidacaoPagto.getHrLimiteValorSuperior()));
	}

	/**
	 * Replace dois pontos por um.
	 *
	 * @param horario the horario
	 * @return the string
	 */
	private String replaceDoisPontosPorUm(String horario) {
		return replaceGenericChar(horario, ':', '.');
	}

	/**
	 * Replace um ponto por dois.
	 *
	 * @param horario the horario
	 * @return the string
	 */
	private String replaceUmPontoPorDois(String horario) {
		return replaceGenericChar(horario, '.', ':');
	}

	/**
	 * Replace generic char.
	 *
	 * @param horario the horario
	 * @param c1 the c1
	 * @param c2 the c2
	 * @return the string
	 */
	private String replaceGenericChar(String horario, char c1, char c2) {
		return PgitUtil.verificaStringNula(horario).replace(c1, c2);
	}

	/**
	 * Voltar inclusao.
	 *
	 * @return the string
	 */
	public String voltarInclusao() {
		return INC_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO;
	}
	// Detalhar
	/**
	 * Detalahar liq pagto integrado.
	 *
	 * @return the string
	 */
	public String detalaharLiqPagtoIntegrado() {

		this.setSaidaDetalharLiquidacaoPagto(new DetalharLiquidacaoPagamentoSaidaDTO());
		DetalharLiquidacaoPagamentoEntradaDTO entrada = new DetalharLiquidacaoPagamentoEntradaDTO();
		
//		saidaListarLiquidacaoPagto = listaLiquidacaoPagto.get(Integer.parseInt(itemSelecionadoLista));

		entrada.setCdFormaLiquidacao(listaLiquidacaoPagto.get(Integer.parseInt(itemSelecionadoLista)).getCdFormaLiquidacao());
		entrada.setDtFinalVigencia("");
		entrada.setDtInicioVigencia("");
		entrada.setHrInclusaoRegistroHistorico("");
		entrada.setQtConsultas(1);
		entrada.setHrConsultaFolhaPgto("");
		entrada.setHrConsultasSaldoPagamento("");
		

		try {
			saidaDetalharLiquidacaoPagto = formaLiqPgtoIntService.detalheLiqPagamentoIntegrado(entrada);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO,	BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "detFormaLiquidacaoPagamentoIntegrado";
	}

	// Alterar
	/**
	 * Alterar liq pagto integrado.
	 *
	 * @return the string
	 */
	public String alterarLiqPagtoIntegrado() {
		setEntradaAlterarHistLiquidacaoPagto(new AlterarLiquidacaoPagamentoEntradaDTO());
		ListarLiquidacaoPagamentoOcorrenciaSaidaDTO registroSelecionado = listaLiquidacaoPagto.get(Integer.valueOf(itemSelecionadoLista));

		setCentroCustoLupaFiltro(registroSelecionado.getCdSistema());
		entradaAlterarHistLiquidacaoPagto.setCdSistema(registroSelecionado.getCdSistema());
		obterCentroCusto();

		entradaAlterarHistLiquidacaoPagto.setCdControleHoraLimite(registroSelecionado.getCdControleHora());
		entradaAlterarHistLiquidacaoPagto.setCdFormaLiquidacao(registroSelecionado.getCdFormaLiquidacao());
		entradaAlterarHistLiquidacaoPagto.setCdPrioridadeDebForma(registroSelecionado.getCdPrioridadeDebito());
		entradaAlterarHistLiquidacaoPagto.setHrLimiteProcessamento(registroSelecionado.getHrLimiteProcessamento());
		entradaAlterarHistLiquidacaoPagto.setQtMinutoMargemSeguranca(registroSelecionado.getQtMinutosMargemSeguranca());	
		entradaAlterarHistLiquidacaoPagto.setHrConsultaFolhaPgto(registroSelecionado.getHrConsultaFolhaPgto());
		entradaAlterarHistLiquidacaoPagto.setHrConsultasSaldoPagamento(registroSelecionado.getHrConsultasSaldoPagamento());
		entradaAlterarHistLiquidacaoPagto.setQtMinutosValorSuperior(registroSelecionado.getQtMinutosValorSuperior());
		entradaAlterarHistLiquidacaoPagto.setHrLimiteValorSuperior(registroSelecionado.getHrLimiteValorSuperior());
		if (registroSelecionado.getQtdTempoAgendamentoCobranca() != null && registroSelecionado.getQtdTempoAgendamentoCobranca() > 0) {
		    entradaAlterarHistLiquidacaoPagto.setQtdTempoAgendamentoCobranca(registroSelecionado.getQtdTempoAgendamentoCobranca());
		}
		if (registroSelecionado.getQtdTempoEfetivacaoCiclicaCobranca() != null && registroSelecionado.getQtdTempoEfetivacaoCiclicaCobranca() > 0) {
		    entradaAlterarHistLiquidacaoPagto.setQtdTempoEfetivacaoCiclicaCobranca(registroSelecionado.getQtdTempoEfetivacaoCiclicaCobranca());
		}
		if (registroSelecionado.getQtdTempoEfetivacaoDiariaCobranca() != null && registroSelecionado.getQtdTempoEfetivacaoDiariaCobranca() > 0) {
		    entradaAlterarHistLiquidacaoPagto.setQtdTempoEfetivacaoDiariaCobranca(registroSelecionado.getQtdTempoEfetivacaoDiariaCobranca());
		}

		if (registroSelecionado.getQtdLimiteProcessamentoExcedido() != null && registroSelecionado.getQtdLimiteProcessamentoExcedido() > 0) {
		    entradaAlterarHistLiquidacaoPagto.setQtdLimiteProcessamentoExcedido(registroSelecionado.getQtdLimiteProcessamentoExcedido());
		}
		
		setDsFormaLiquidacao(registroSelecionado.getDsFormaLiquidacao());

		return "altFormaLiquidacaoPagamentoIntegrado";
	}

	/**
	 * Avancar alterar liq pagto integrado.
	 *
	 * @return the string
	 */
	public String avancarAlterarLiqPagtoIntegrado() {
		
		if (this.getListaCentroCustoLupa() == null && this.entradaAlterarHistLiquidacaoPagto.getCdSistema().trim().equals("")) {
			return "altFormaLiquidacaoPagamentoIntegrado";
		} else {
			for (CentroCustoSaidaDTO saidaCentroCusto : getListaCentroCustoLupa()) {
				if (saidaCentroCusto.getCdCentroCusto().trim().equals(entradaAlterarHistLiquidacaoPagto.getCdSistema())) {
					this.setCentroCusto(saidaCentroCusto.getCodigoDescricao());
					break;
				}
			}
		}
		
		return "altFormaLiquidacaoPagamentoIntegrado2";
	}

	/**
	 * Confirmar alteracao liq pagto integrado.
	 *
	 * @return the string
	 */
	public String confirmarAlteracaoLiqPagtoIntegrado() {
		entradaAlterarHistLiquidacaoPagto.setHrLimiteProcessamento(replaceDoisPontosPorUm(entradaAlterarHistLiquidacaoPagto.getHrLimiteProcessamento()));
		entradaAlterarHistLiquidacaoPagto.setHrConsultaFolhaPgto(replaceDoisPontosPorUm(entradaAlterarHistLiquidacaoPagto.getHrConsultaFolhaPgto()));
		entradaAlterarHistLiquidacaoPagto.setHrConsultasSaldoPagamento(replaceDoisPontosPorUm(entradaAlterarHistLiquidacaoPagto.getHrConsultasSaldoPagamento()));
		entradaAlterarHistLiquidacaoPagto.setHrLimiteValorSuperior(replaceDoisPontosPorUm(entradaAlterarHistLiquidacaoPagto.getHrLimiteValorSuperior()));

		try {
			AlterarLiquidacaoPagamentoSaidaDTO saida = formaLiqPgtoIntService.alterarLiquidacaoPagto(this.entradaAlterarHistLiquidacaoPagto);
			inverterPontosAposRetornoFluxoAlterar();

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO, BradescoViewExceptionActionType.ACTION, false);
		} catch(PdcAdapterException p) { 

			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " +  p.getMessage(), "altFormaLiquidacaoPagamentoIntegrado2", BradescoViewExceptionActionType.ACTION, false);
			return null;
		} finally {
			consultarPagtoIntegrado();
		}

		return "altFormaLiquidacaoPagamentoIntegrado2";
	}

	/**
	 * Inverter pontos apos retorno fluxo alterar.
	 */
	private void inverterPontosAposRetornoFluxoAlterar() {
		entradaAlterarHistLiquidacaoPagto.setHrLimiteProcessamento(replaceUmPontoPorDois(entradaAlterarHistLiquidacaoPagto.getHrLimiteProcessamento()));
		entradaAlterarHistLiquidacaoPagto.setHrConsultaFolhaPgto(replaceUmPontoPorDois(entradaAlterarHistLiquidacaoPagto.getHrConsultaFolhaPgto()));
		entradaAlterarHistLiquidacaoPagto.setHrConsultasSaldoPagamento(replaceUmPontoPorDois(entradaAlterarHistLiquidacaoPagto.getHrConsultasSaldoPagamento()));
		entradaAlterarHistLiquidacaoPagto.setHrLimiteValorSuperior(replaceUmPontoPorDois(entradaAlterarHistLiquidacaoPagto.getHrLimiteValorSuperior()));
	}

	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar() {
		return "altFormaLiquidacaoPagamentoIntegrado";
	}

	// Excluir
	/**
	 * Excluir liq pagto integrado.
	 *
	 * @return the string
	 */
	public String excluirLiqPagtoIntegrado() {
		ocorrenciasaidaListarLiquidacaoPagto =  new ListarLiquidacaoPagamentoOcorrenciaSaidaDTO();
		this.setOcorrenciasaidaListarLiquidacaoPagto(listaLiquidacaoPagto.get(Integer.parseInt(itemSelecionadoLista)));
		
		return "excFormaLiquidacaoPagamentoIntegrado";
	}

	/**
	 * Confirmar exclusao liq pagto integrado.
	 *
	 * @return the string
	 */
	public String confirmarExclusaoLiqPagtoIntegrado() {
		
		ExcluirLiquidacaoPagamentoEntradaDTO entradaExcluirLiquidacaoPagto = new ExcluirLiquidacaoPagamentoEntradaDTO();
		ExcluirLiquidacaoPagamentoSaidaDTO saida = new ExcluirLiquidacaoPagamentoSaidaDTO();
		
		entradaExcluirLiquidacaoPagto.setCdFormaLiquidacao(ocorrenciasaidaListarLiquidacaoPagto.getCdFormaLiquidacao());
		entradaExcluirLiquidacaoPagto.setDtFinalVigencia("");
		entradaExcluirLiquidacaoPagto.setDtInicioVigencia("");
		entradaExcluirLiquidacaoPagto.setHrInclusaoRegistroHistorico("");
		entradaExcluirLiquidacaoPagto.setNumeroOcorrencias(0);
		entradaExcluirLiquidacaoPagto.setHrConsultaFolhaPgto("");
		entradaExcluirLiquidacaoPagto.setHrConsultasSaldoPagamento("");
		
		
		try {
			saida = formaLiqPgtoIntService.excluirLiqPagamentoIntegrado(entradaExcluirLiquidacaoPagto);	
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " +  saida.getMensagem(), "#{formaLiqPagtoIntBean.consultarPagtoIntegradoConfExc}", CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO, false);
	
		} catch(PdcAdapterException p) { 
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " +  p.getMessage(), CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		
		return "excFormaLiquidacaoPagamentoIntegrado";
	}

	// Historico
	/**
	 * Historico liq pagto integrado.
	 *
	 * @return the string
	 */
	public String historicoLiqPagtoIntegrado() {
		setEntradaListarHistLiquidacaoPagto(new ListarHistLiquidacaoPagamentoEntradaDTO());
		setListaHistLiquidacaoPagto(new ArrayList<ListarHistLiquidacaoPagamentoSaidaDTO>());
		setSaidaListarHistLiquidacaoPagto(new ListarHistLiquidacaoPagamentoSaidaDTO());
		setItemSelecionadoListaHistorico("");
		setDataInicioManutencao(new Date());
		setDataFimManutencao(new Date());
		setBtoAcionadoHistorico(false);
		
		
		if (this.itemSelecionadoLista != null) {
			List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> ocorrencia = new ArrayList<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO>();
			ocorrencia.add(listaLiquidacaoPagto.get(Integer.parseInt(itemSelecionadoLista)));
			this.setSaidaListarLiquidacaoPagto(new ListarLiquidacaoPagamentoSaidaDTO(ocorrencia));
			entradaListarHistLiquidacaoPagto.setCdFormaLiquidacao(saidaListarLiquidacaoPagto.getOcorrencias().get(0).getCdFormaLiquidacao());
		}
		
		return HIS_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO;
	}

	/**
	 * Listar historico liq pagto integrado.
	 *
	 * @return the string
	 */
	public String listarHistoricoLiqPagtoIntegrado() {
		
		entradaListarHistLiquidacaoPagto.setDtInicioVigencia(br.com.bradesco.web.pgit.view.converters.FormatarData.formataDiaMesAnoToPdc(dataInicioManutencao));
		entradaListarHistLiquidacaoPagto.setDtFinalVigencia(br.com.bradesco.web.pgit.view.converters.FormatarData.formataDiaMesAnoToPdc(dataFimManutencao));
		entradaListarHistLiquidacaoPagto.setHrInclusaoRegistroHistorico("");
		entradaListarHistLiquidacaoPagto.setQtConsultas(100);
		
		listaHistLiquidacaoPagto = formaLiqPgtoIntService.consultarHistLiquidacaoPagto(this.entradaListarHistLiquidacaoPagto);
		setBtoAcionadoHistorico(true);
		return HIS_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO;
	}

	/**
	 * Limpar campos historico.
	 *
	 * @return the string
	 */
	public String limparCamposHistorico() {
		setEntradaListarHistLiquidacaoPagto(new ListarHistLiquidacaoPagamentoEntradaDTO());
		setListaHistLiquidacaoPagto(new ArrayList<ListarHistLiquidacaoPagamentoSaidaDTO>());
		setDataInicioManutencao(new Date());
		setDataFimManutencao(new Date());
		setBtoAcionadoHistorico(false);
		return HIS_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO;
	}
	
	/**
	 * Get: selecionaHistLiquidacaoPagto.
	 *
	 * @return selecionaHistLiquidacaoPagto
	 */
	public List<SelectItem> getSelecionaHistLiquidacaoPagto() {

		if (this.listaHistLiquidacaoPagto == null || this.listaHistLiquidacaoPagto.isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listaHistLiquidacaoPagto.size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Detalhar historico liq pagto integrado.
	 *
	 * @return the string
	 */
	public String detalharHistoricoLiqPagtoIntegrado() {
		setSaidaListarHistLiquidacaoPagto(listaHistLiquidacaoPagto.get(Integer.parseInt(itemSelecionadoListaHistorico)));

		DetalharHistLiquidacaoPagamentoEntradaDTO entrada = new DetalharHistLiquidacaoPagamentoEntradaDTO();
		entrada.setCdFormaLiquidacao(saidaListarHistLiquidacaoPagto.getCdFormaLiquidacao());
		entrada.setDtFinalVigencia(entradaListarHistLiquidacaoPagto.getDtFinalVigencia());
		entrada.setDtInicioVigencia(entradaListarHistLiquidacaoPagto.getDtInicioVigencia());
		entrada.setHrInclusaoRegistroHistorico(saidaListarHistLiquidacaoPagto.getHrInclusaoRegistro());
		entrada.setHrConsultasSaldoPagamento("");
		entrada.setHrConsultaFolhaPgto("");
		entrada.setQtConsultas(1);

		saidaDetalharHistLiquidacaoPagto = formaLiqPgtoIntService.detalheHistLiqPagamentoIntegrado(entrada);

		return "hisFormaLiquidacaoPagamentoIntegrado2";
	}

	/**
	 * Voltar historico.
	 *
	 * @return the string
	 */
	public String voltarHistorico() {
		setItemSelecionadoListaHistorico(null);
		return HIS_FORMA_LIQUIDACAO_PAGAMENTO_INTEGRADO;
	}
	
	/**
	 * Preenche forma liquidacao.
	 *
	 * @return the list< select item>
	 */
	public List<SelectItem> preencheFormaLiquidacao() {
		listaFormaLiquidacao = new ArrayList<SelectItem>();
		List<ListarFormaLiquidacaoSaidaDTO> listaFormaLiq = new ArrayList<ListarFormaLiquidacaoSaidaDTO>();

		listaFormaLiq = comboService.listarFormaLiquidacao();
		listaFormaLiquidacaoHash.clear();
		for (ListarFormaLiquidacaoSaidaDTO combo : listaFormaLiq) {
			listaFormaLiquidacaoHash.put(combo.getCdFormaLiquidacao(), combo.getDsFormaLiquidacao());
			listaFormaLiquidacao.add(new SelectItem(combo.getCdFormaLiquidacao(), combo.getDsFormaLiquidacao()));
		}
		return listaFormaLiquidacao;
	}

	/**
	 * Voltar inicio.
	 *
	 * @return the string
	 */
	public String voltarInicio() {
		setItemSelecionadoLista(null);
		return CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO;
	}

	/**
	 * Obter centro custo.
	 */
	public void obterCentroCusto() {
		setListaCentroCustoLupa(new ArrayList<CentroCustoSaidaDTO>());
		if (getCentroCustoLupaFiltro() != null && !getCentroCustoLupaFiltro().trim().equals("")) {
			setListaCentroCustoLupa(getComboService().listarCentroCusto(new CentroCustoEntradaDTO(getCentroCustoLupaFiltro().toUpperCase())));
		}
		
		setListaCentroCustoLupaSelectItem(new ArrayList<SelectItem>());
		for (int i = 0; i < getListaCentroCustoLupa().size(); i++) {
			listaCentroCustoLupaSelectItem.add(new SelectItem(listaCentroCustoLupa.get(i).getCdCentroCusto().toUpperCase(), listaCentroCustoLupa.get(i).getCdCentroCusto().toUpperCase() + " - " + listaCentroCustoLupa.get(i).getDsCentroCusto().toUpperCase()));
		}
//		getEntradaIncluirLiquidacaoPagto().setQtMinutoMargemSeguranca(null);
	}
	
	/**
	 * Get: listaControleHistorico.
	 *
	 * @return listaControleHistorico
	 */
	public List<SelectItem> getListaControleHistorico() {
		return listaControleHistorico;
	}

	/**
	 * Set: listaControleHistorico.
	 *
	 * @param listaControleHistorico the lista controle historico
	 */
	public void setListaControleHistorico(
			List<SelectItem> listaControleHistorico) {
		this.listaControleHistorico = listaControleHistorico;
	}

	/**
	 * Get: listaFormaLiquidacao.
	 *
	 * @return listaFormaLiquidacao
	 */
	public List<SelectItem> getListaFormaLiquidacao() {
		return listaFormaLiquidacao;
	}

	/**
	 * Set: listaFormaLiquidacao.
	 *
	 * @param listaFormaLiquidacao the lista forma liquidacao
	 */
	public void setListaFormaLiquidacao(List<SelectItem> listaFormaLiquidacao) {
		this.listaFormaLiquidacao = listaFormaLiquidacao;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos() {
		setBtoAcionado(false);
		this.setItemSelecionadoLista(null);
		listaLiquidacaoPagto = new ArrayList<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO>();
		entradaListarLiquidacaoPagto.setCdFormaLiquidacao(0);
		return CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO;
	}

	/**
	 * Limpar pagina.
	 *
	 * @return the string
	 */
	public String limparPagina() {
		return "";
	}

	/**
	 * Limpar campos inclusao.
	 */
	public void limparCamposInclusao() {
		entradaIncluirLiquidacaoPagto.setCdControleHoraLimite(null);
	    entradaIncluirLiquidacaoPagto.setCdPrioridadeDebForma(null);
	    entradaIncluirLiquidacaoPagto.setCdSistema(null);
	    entradaIncluirLiquidacaoPagto.setHrLimiteProcessamento(null);
	    entradaIncluirLiquidacaoPagto.setQtMinutoMargemSeguranca(null);
		entradaIncluirLiquidacaoPagto.setHrConsultasSaldoPagamento(null);
    	entradaIncluirLiquidacaoPagto.setHrConsultaFolhaPgto(null);
    	entradaIncluirLiquidacaoPagto.setHrLimiteValorSuperior("");
    	entradaIncluirLiquidacaoPagto.setQtMinutosValorSuperior(null);
	}
	
	/**
	 * Is habilita hora qtde limite valor superior.
	 *
	 * @return true, if is habilita hora qtde limite valor superior
	 */
	public boolean isHabilitaHoraQtdeLimiteValorSuperior(){
		if(entradaIncluirLiquidacaoPagto.getCdFormaLiquidacao() != null && entradaIncluirLiquidacaoPagto.getCdFormaLiquidacao() == 24){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Is habilita hora qtde limite valor superior alt.
	 *
	 * @return true, if is habilita hora qtde limite valor superior alt
	 */
	public boolean isHabilitaHoraQtdeLimiteValorSuperiorAlt(){
		if(getDsFormaLiquidacao().equalsIgnoreCase("T�TULOS OUTROS BANCOS")){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Is habilita campos hr min valor superior.
	 *
	 * @return true, if is habilita campos hr min valor superior
	 */
	public boolean isHabilitaCamposHrMinValorSuperior(){
		if(saidaDetalharLiquidacaoPagto.getDsFormaLiquidacao().equalsIgnoreCase("T�TULOS OUTROS BANCOS")){
			return true;
		}else{
			return false;
		}
	}

	/**
     * Is forma liquidacao titulo det.
     * 
     * @return true, if is forma liquidacao titulo det
     */
	public boolean isFormaLiquidacaoTituloDet() {
	    return isFormaLiquidacaoTitulo(saidaDetalharLiquidacaoPagto.getCdFormaLiquidacao());
	}

	/**
     * Is forma liquidacao titulo exc.
     * 
     * @return true, if is forma liquidacao titulo exc
     */
	public boolean isFormaLiquidacaoTituloExc() {
        return isFormaLiquidacaoTitulo(ocorrenciasaidaListarLiquidacaoPagto.getCdFormaLiquidacao());
    }
	
	/**
     * Is forma liquidacao titulo hist.
     * 
     * @return true, if is forma liquidacao titulo hist
     */
	public boolean isFormaLiquidacaoTituloHist() {
        return isFormaLiquidacaoTitulo(saidaDetalharHistLiquidacaoPagto.getCdFormaLiquidacao());
    }

	/**
     * Is forma liquidacao titulo alt.
     * 
     * @return true, if is forma liquidacao titulo alt
     */
	public boolean isFormaLiquidacaoTituloAlt() {
        return isFormaLiquidacaoTitulo(entradaAlterarHistLiquidacaoPagto.getCdFormaLiquidacao());
    }
	
	/**
     * Is forma liquidacao titulo inc.
     * 
     * @return true, if is forma liquidacao titulo inc
     */
	public boolean isFormaLiquidacaoTituloInc() {
        return isFormaLiquidacaoTitulo(entradaIncluirLiquidacaoPagto.getCdFormaLiquidacao());
    }

    /**
     * Is forma liquidacao titulo.
     * 
     * @param cdFormaLiquidacao
     *            the cd forma liquidacao
     * @return true, if is forma liquidacao titulo
     */
    private boolean isFormaLiquidacaoTitulo(Integer cdFormaLiquidacao) {
        return cdFormaLiquidacao != null
            && (cdFormaLiquidacao == FORMA_LIQ_TITULO_OUTROS || cdFormaLiquidacao == FORMA_LIQ_TITULO_BRADESCO);
    }

	/**
	 * Get: entradaListarLiquidacaoPagto.
	 *
	 * @return entradaListarLiquidacaoPagto
	 */
	public ListarLiquidacaoPagamentoEntradaDTO getEntradaListarLiquidacaoPagto() {
		return entradaListarLiquidacaoPagto;
	}

	/**
	 * Set: entradaListarLiquidacaoPagto.
	 *
	 * @param entradaListarLiquidacaoPagto the entrada listar liquidacao pagto
	 */
	public void setEntradaListarLiquidacaoPagto(
			ListarLiquidacaoPagamentoEntradaDTO entradaListarLiquidacaoPagto) {
		this.entradaListarLiquidacaoPagto = entradaListarLiquidacaoPagto;
	}

	/**
	 * Get: formaLiqPgtoIntService.
	 *
	 * @return formaLiqPgtoIntService
	 */
	public IFormaLiqPgtoIntService getFormaLiqPgtoIntService() {
		return formaLiqPgtoIntService;
	}

	/**
	 * Set: formaLiqPgtoIntService.
	 *
	 * @param formaLiqPgtoIntService the forma liq pgto int service
	 */
	public void setFormaLiqPgtoIntService(
			IFormaLiqPgtoIntService formaLiqPgtoIntService) {
		this.formaLiqPgtoIntService = formaLiqPgtoIntService;
	}

	/**
	 * Get: listaLiquidacaoPagto.
	 *
	 * @return listaLiquidacaoPagto
	 */
	public List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> getListaLiquidacaoPagto() {
		return listaLiquidacaoPagto;
	}

	/**
	 * Set: listaLiquidacaoPagto.
	 *
	 * @param listaLiquidacaoPagto the lista liquidacao pagto
	 */
	public void setListaLiquidacaoPagto(
			List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> listaLiquidacaoPagto) {
		this.listaLiquidacaoPagto = listaLiquidacaoPagto;
	}

	/**
	 * Get: entradaIncluirLiquidacaoPagto.
	 *
	 * @return entradaIncluirLiquidacaoPagto
	 */
	public IncluirLiquidacaoPagamentoEntradaDTO getEntradaIncluirLiquidacaoPagto() {
		return entradaIncluirLiquidacaoPagto;
	}

	/**
	 * Set: entradaIncluirLiquidacaoPagto.
	 *
	 * @param entradaIncluirLiquidacaoPagto the entrada incluir liquidacao pagto
	 */
	public void setEntradaIncluirLiquidacaoPagto(
			IncluirLiquidacaoPagamentoEntradaDTO entradaIncluirLiquidacaoPagto) {
		this.entradaIncluirLiquidacaoPagto = entradaIncluirLiquidacaoPagto;
	}

	/**
	 * Get: saidaDetalharLiquidacaoPagto.
	 *
	 * @return saidaDetalharLiquidacaoPagto
	 */
	public DetalharLiquidacaoPagamentoSaidaDTO getSaidaDetalharLiquidacaoPagto() {
		return saidaDetalharLiquidacaoPagto;
	}

	/**
	 * Set: saidaDetalharLiquidacaoPagto.
	 *
	 * @param saidaDetalharLiquidacaoPagto the saida detalhar liquidacao pagto
	 */
	public void setSaidaDetalharLiquidacaoPagto(
			DetalharLiquidacaoPagamentoSaidaDTO saidaDetalharLiquidacaoPagto) {
		this.saidaDetalharLiquidacaoPagto = saidaDetalharLiquidacaoPagto;
	}

	/**
	 * Get: saidaListarLiquidacaoPagto.
	 *
	 * @return saidaListarLiquidacaoPagto
	 */
	public ListarLiquidacaoPagamentoSaidaDTO getSaidaListarLiquidacaoPagto() {
		return saidaListarLiquidacaoPagto;
	}

	/**
	 * Set: saidaListarLiquidacaoPagto.
	 *
	 * @param saidaListarLiquidacaoPagto the saida listar liquidacao pagto
	 */
	public void setSaidaListarLiquidacaoPagto(
			ListarLiquidacaoPagamentoSaidaDTO saidaListarLiquidacaoPagto) {
		this.saidaListarLiquidacaoPagto = saidaListarLiquidacaoPagto;
	}

	/**
	 * Get: listaHistLiquidacaoPagto.
	 *
	 * @return listaHistLiquidacaoPagto
	 */
	public List<ListarHistLiquidacaoPagamentoSaidaDTO> getListaHistLiquidacaoPagto() {
		return listaHistLiquidacaoPagto;
	}

	/**
	 * Set: listaHistLiquidacaoPagto.
	 *
	 * @param listaHistLiquidacaoPagto the lista hist liquidacao pagto
	 */
	public void setListaHistLiquidacaoPagto(
			List<ListarHistLiquidacaoPagamentoSaidaDTO> listaHistLiquidacaoPagto) {
		this.listaHistLiquidacaoPagto = listaHistLiquidacaoPagto;
	}

	/**
	 * Get: saidaListarHistLiquidacaoPagto.
	 *
	 * @return saidaListarHistLiquidacaoPagto
	 */
	public ListarHistLiquidacaoPagamentoSaidaDTO getSaidaListarHistLiquidacaoPagto() {
		return saidaListarHistLiquidacaoPagto;
	}

	/**
	 * Set: saidaListarHistLiquidacaoPagto.
	 *
	 * @param saidaListarHistLiquidacaoPagto the saida listar hist liquidacao pagto
	 */
	public void setSaidaListarHistLiquidacaoPagto(
			ListarHistLiquidacaoPagamentoSaidaDTO saidaListarHistLiquidacaoPagto) {
		this.saidaListarHistLiquidacaoPagto = saidaListarHistLiquidacaoPagto;
	}

	/**
	 * Get: entradaListarHistLiquidacaoPagto.
	 *
	 * @return entradaListarHistLiquidacaoPagto
	 */
	public ListarHistLiquidacaoPagamentoEntradaDTO getEntradaListarHistLiquidacaoPagto() {
		return entradaListarHistLiquidacaoPagto;
	}

	/**
	 * Set: entradaListarHistLiquidacaoPagto.
	 *
	 * @param entradaListarHistLiquidacaoPagto the entrada listar hist liquidacao pagto
	 */
	public void setEntradaListarHistLiquidacaoPagto(
			ListarHistLiquidacaoPagamentoEntradaDTO entradaListarHistLiquidacaoPagto) {
		this.entradaListarHistLiquidacaoPagto = entradaListarHistLiquidacaoPagto;
	}

	/**
	 * Get: itemSelecionadoListaHistorico.
	 *
	 * @return itemSelecionadoListaHistorico
	 */
	public String getItemSelecionadoListaHistorico() {
		return itemSelecionadoListaHistorico;
	}

	/**
	 * Set: itemSelecionadoListaHistorico.
	 *
	 * @param itemSelecionadoListaHistorico the item selecionado lista historico
	 */
	public void setItemSelecionadoListaHistorico(
			String itemSelecionadoListaHistorico) {
		this.itemSelecionadoListaHistorico = itemSelecionadoListaHistorico;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: entradaAlterarHistLiquidacaoPagto.
	 *
	 * @return entradaAlterarHistLiquidacaoPagto
	 */
	public AlterarLiquidacaoPagamentoEntradaDTO getEntradaAlterarHistLiquidacaoPagto() {
		return entradaAlterarHistLiquidacaoPagto;
	}

	/**
	 * Set: entradaAlterarHistLiquidacaoPagto.
	 *
	 * @param entradaAlterarHistLiquidacaoPagto the entrada alterar hist liquidacao pagto
	 */
	public void setEntradaAlterarHistLiquidacaoPagto(
			AlterarLiquidacaoPagamentoEntradaDTO entradaAlterarHistLiquidacaoPagto) {
		this.entradaAlterarHistLiquidacaoPagto = entradaAlterarHistLiquidacaoPagto;
	}

	/**
	 * Get: formaLiquidacaoFiltro.
	 *
	 * @return formaLiquidacaoFiltro
	 */
	public Integer getFormaLiquidacaoFiltro() {
		return formaLiquidacaoFiltro;
	}

	/**
	 * Set: formaLiquidacaoFiltro.
	 *
	 * @param formaLiquidacaoFiltro the forma liquidacao filtro
	 */
	public void setFormaLiquidacaoFiltro(Integer formaLiquidacaoFiltro) {
		this.formaLiquidacaoFiltro = formaLiquidacaoFiltro;
	}

	/**
	 * Get: dataFimManutencao.
	 *
	 * @return dataFimManutencao
	 */
	public Date getDataFimManutencao() {
		return dataFimManutencao;
	}

	/**
	 * Set: dataFimManutencao.
	 *
	 * @param dataFimManutencao the data fim manutencao
	 */
	public void setDataFimManutencao(Date dataFimManutencao) {
		this.dataFimManutencao = dataFimManutencao;
	}

	/**
	 * Get: dataInicioManutencao.
	 *
	 * @return dataInicioManutencao
	 */
	public Date getDataInicioManutencao() {
		return dataInicioManutencao;
	}

	/**
	 * Set: dataInicioManutencao.
	 *
	 * @param dataInicioManutencao the data inicio manutencao
	 */
	public void setDataInicioManutencao(Date dataInicioManutencao) {
		this.dataInicioManutencao = dataInicioManutencao;
	}

	/**
	 * Get: saidaDetalharHistLiquidacaoPagto.
	 *
	 * @return saidaDetalharHistLiquidacaoPagto
	 */
	public DetalharHistLiquidacaoPagamentoSaidaDTO getSaidaDetalharHistLiquidacaoPagto() {
		return saidaDetalharHistLiquidacaoPagto;
	}

	/**
	 * Set: saidaDetalharHistLiquidacaoPagto.
	 *
	 * @param saidaDetalharHistLiquidacaoPagto the saida detalhar hist liquidacao pagto
	 */
	public void setSaidaDetalharHistLiquidacaoPagto(
			DetalharHistLiquidacaoPagamentoSaidaDTO saidaDetalharHistLiquidacaoPagto) {
		this.saidaDetalharHistLiquidacaoPagto = saidaDetalharHistLiquidacaoPagto;
	}

	/**
	 * Get: centroCustoLupaFiltro.
	 *
	 * @return centroCustoLupaFiltro
	 */
	public String getCentroCustoLupaFiltro() {
		return centroCustoLupaFiltro;
	}

	/**
	 * Set: centroCustoLupaFiltro.
	 *
	 * @param centroCustoLupaFiltro the centro custo lupa filtro
	 */
	public void setCentroCustoLupaFiltro(String centroCustoLupaFiltro) {
		this.centroCustoLupaFiltro = centroCustoLupaFiltro;
	}

	/**
	 * Get: listaCentroCustoLupa.
	 *
	 * @return listaCentroCustoLupa
	 */
	public List<CentroCustoSaidaDTO> getListaCentroCustoLupa() {
		return listaCentroCustoLupa;
	}

	/**
	 * Set: listaCentroCustoLupa.
	 *
	 * @param listaCentroCustoLupa the lista centro custo lupa
	 */
	public void setListaCentroCustoLupa(
			List<CentroCustoSaidaDTO> listaCentroCustoLupa) {
		this.listaCentroCustoLupa = listaCentroCustoLupa;
	}

	/**
	 * Get: formaLiquidacao.
	 *
	 * @return formaLiquidacao
	 */
	public String getFormaLiquidacao() {
		return formaLiquidacao;
	}

	/**
	 * Set: formaLiquidacao.
	 *
	 * @param formaLiquidacao the forma liquidacao
	 */
	public void setFormaLiquidacao(String formaLiquidacao) {
		this.formaLiquidacao = formaLiquidacao;
	}

	/**
	 * Get: listaFormaLiquidacaoHash.
	 *
	 * @return listaFormaLiquidacaoHash
	 */
	public Map<Integer, String> getListaFormaLiquidacaoHash() {
		return listaFormaLiquidacaoHash;
	}

	/**
	 * Set lista forma liquidacao hash.
	 *
	 * @param listaFormaLiquidacaoHash the lista forma liquidacao hash
	 */
	public void setListaFormaLiquidacaoHash(
			Map<Integer, String> listaFormaLiquidacaoHash) {
		this.listaFormaLiquidacaoHash = listaFormaLiquidacaoHash;
	}

	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}

	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: btoAcionadoHistorico.
	 *
	 * @return btoAcionadoHistorico
	 */
	public Boolean getBtoAcionadoHistorico() {
		return btoAcionadoHistorico;
	}

	/**
	 * Set: btoAcionadoHistorico.
	 *
	 * @param btoAcionadoHistorico the bto acionado historico
	 */
	public void setBtoAcionadoHistorico(Boolean btoAcionadoHistorico) {
		this.btoAcionadoHistorico = btoAcionadoHistorico;
	}

	/**
	 * Get: listaCentroCustoLupaSelectItem.
	 *
	 * @return listaCentroCustoLupaSelectItem
	 */
	public List<SelectItem> getListaCentroCustoLupaSelectItem() {
		return listaCentroCustoLupaSelectItem;
	}

	/**
	 * Set: listaCentroCustoLupaSelectItem.
	 *
	 * @param listaCentroCustoLupaSelectItem the lista centro custo lupa select item
	 */
	public void setListaCentroCustoLupaSelectItem(
			List<SelectItem> listaCentroCustoLupaSelectItem) {
		this.listaCentroCustoLupaSelectItem = listaCentroCustoLupaSelectItem;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: ocorrenciasaidaListarLiquidacaoPagto.
	 *
	 * @return ocorrenciasaidaListarLiquidacaoPagto
	 */
	public ListarLiquidacaoPagamentoOcorrenciaSaidaDTO getOcorrenciasaidaListarLiquidacaoPagto() {
		return ocorrenciasaidaListarLiquidacaoPagto;
	}

	/**
	 * Set: ocorrenciasaidaListarLiquidacaoPagto.
	 *
	 * @param ocorrenciasaidaListarLiquidacaoPagto the ocorrenciasaida listar liquidacao pagto
	 */
	public void setOcorrenciasaidaListarLiquidacaoPagto(
			ListarLiquidacaoPagamentoOcorrenciaSaidaDTO ocorrenciasaidaListarLiquidacaoPagto) {
		this.ocorrenciasaidaListarLiquidacaoPagto = ocorrenciasaidaListarLiquidacaoPagto;
	}

	
	
}
