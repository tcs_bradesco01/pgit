/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistoricoBloqueioRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistoricoBloqueioRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdProcessoSistema
     */
    private int _cdProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdProcessoSistema
     */
    private boolean _has_cdProcessoSistema;

    /**
     * Field _dtSistema
     */
    private java.lang.String _dtSistema;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistoricoBloqueioRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.request.DetalharHistoricoBloqueioRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProcessoSistema
     * 
     */
    public void deleteCdProcessoSistema()
    {
        this._has_cdProcessoSistema= false;
    } //-- void deleteCdProcessoSistema() 

    /**
     * Returns the value of field 'cdProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcessoSistema'.
     */
    public int getCdProcessoSistema()
    {
        return this._cdProcessoSistema;
    } //-- int getCdProcessoSistema() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'dtSistema'.
     * 
     * @return String
     * @return the value of field 'dtSistema'.
     */
    public java.lang.String getDtSistema()
    {
        return this._dtSistema;
    } //-- java.lang.String getDtSistema() 

    /**
     * Method hasCdProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcessoSistema()
    {
        return this._has_cdProcessoSistema;
    } //-- boolean hasCdProcessoSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProcessoSistema'.
     * 
     * @param cdProcessoSistema the value of field
     * 'cdProcessoSistema'.
     */
    public void setCdProcessoSistema(int cdProcessoSistema)
    {
        this._cdProcessoSistema = cdProcessoSistema;
        this._has_cdProcessoSistema = true;
    } //-- void setCdProcessoSistema(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'dtSistema'.
     * 
     * @param dtSistema the value of field 'dtSistema'.
     */
    public void setDtSistema(java.lang.String dtSistema)
    {
        this._dtSistema = dtSistema;
    } //-- void setDtSistema(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistoricoBloqueioRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.request.DetalharHistoricoBloqueioRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.request.DetalharHistoricoBloqueioRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.request.DetalharHistoricoBloqueioRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.request.DetalharHistoricoBloqueioRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
