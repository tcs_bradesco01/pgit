/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ConsutarContrEnvioFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ConsutarContrEnvioFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.DetalharContrEnvioFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.DetalharContrEnvioFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ExcluirContrEnvioArqFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ExcluirContrEnvioArqFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.IncluirContrEnvioArqFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.IncluirContrEnvioArqFormaLiquidacaoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterConEnvioArqFormaLiquidacao
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterConEnvioArqFormaLiquidacaoService {
	
	/**
	 * Consutar contr envio forma liquidacao.
	 *
	 * @param entrada the entrada
	 * @return the list< consutar contr envio forma liquidacao saida dt o>
	 */
	List<ConsutarContrEnvioFormaLiquidacaoSaidaDTO> consutarContrEnvioFormaLiquidacao(ConsutarContrEnvioFormaLiquidacaoEntradaDTO entrada);
	
	/**
	 * Detalhar contr envio forma liquidacao.
	 *
	 * @param entrada the entrada
	 * @return the detalhar contr envio forma liquidacao saida dto
	 */
	DetalharContrEnvioFormaLiquidacaoSaidaDTO detalharContrEnvioFormaLiquidacao(DetalharContrEnvioFormaLiquidacaoEntradaDTO entrada);
	
	/**
	 * Excluir contr envio arq forma liquidacao.
	 *
	 * @param entrada the entrada
	 * @return the excluir contr envio arq forma liquidacao saida dto
	 */
	ExcluirContrEnvioArqFormaLiquidacaoSaidaDTO excluirContrEnvioArqFormaLiquidacao(ExcluirContrEnvioArqFormaLiquidacaoEntradaDTO entrada);
	
	/**
	 * Incluir contr envio arq forma liquidacao.
	 *
	 * @param entrada the entrada
	 * @return the incluir contr envio arq forma liquidacao saida dto
	 */
	IncluirContrEnvioArqFormaLiquidacaoSaidaDTO incluirContrEnvioArqFormaLiquidacao(IncluirContrEnvioArqFormaLiquidacaoEntradaDTO entrada);

   

}

