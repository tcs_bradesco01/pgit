/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasModTributos
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasModTributos {

	/** Atributo cdModalidadeTributos. */
	private String cdModalidadeTributos;

	/**
	 * Ocorrencias mod tributos.
	 */
	public OcorrenciasModTributos() {
		super();
	}

	/**
	 * Ocorrencias mod tributos.
	 *
	 * @param cdModalidadeTributos the cd modalidade tributos
	 */
	public OcorrenciasModTributos(String cdModalidadeTributos) {
		super();
		this.cdModalidadeTributos = cdModalidadeTributos;
	}

	/**
	 * Get: cdModalidadeTributos.
	 *
	 * @return cdModalidadeTributos
	 */
	public String getCdModalidadeTributos() {
		return cdModalidadeTributos;
	}

	/**
	 * Set: cdModalidadeTributos.
	 *
	 * @param cdModalidadeTributos the cd modalidade tributos
	 */
	public void setCdModalidadeTributos(String cdModalidadeTributos) {
		this.cdModalidadeTributos = cdModalidadeTributos;
	}
}