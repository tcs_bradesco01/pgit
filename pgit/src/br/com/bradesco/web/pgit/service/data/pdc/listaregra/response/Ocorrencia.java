/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id: Ocorrencia.java,v 1.4 2009/05/28 12:54:06 cpm.com.br\heslei.silva Exp $
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaregra.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencia.
 * 
 * @version $Revision: 1.4 $ $Date: 2009/05/28 12:54:06 $
 */
public class Ocorrencia implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codRegraAlcada
     */
    private int _codRegraAlcada = 0;

    /**
     * keeps track of state for field: _codRegraAlcada
     */
    private boolean _has_codRegraAlcada;

    /**
     * Field _descRegraAlcada
     */
    private java.lang.String _descRegraAlcada;

    /**
     * Field _descSituacao
     */
    private java.lang.String _descSituacao;

    /**
     * Field _dataAtivacaoRegra
     */
    private java.lang.String _dataAtivacaoRegra;

    /**
     * Field _codEmpresa
     */
    private long _codEmpresa = 0;

    /**
     * keeps track of state for field: _codEmpresa
     */
    private boolean _has_codEmpresa;

    /**
     * Field _descEmpresa
     */
    private java.lang.String _descEmpresa;

    /**
     * Field _codDeptoGesto
     */
    private int _codDeptoGesto = 0;

    /**
     * keeps track of state for field: _codDeptoGesto
     */
    private boolean _has_codDeptoGesto;

    /**
     * Field _descDeptoGesto
     */
    private java.lang.String _descDeptoGesto;

    /**
     * Field _codWorkFlow
     */
    private java.lang.String _codWorkFlow;

    /**
     * Field _codSituacao
     */
    private int _codSituacao = 0;

    /**
     * keeps track of state for field: _codSituacao
     */
    private boolean _has_codSituacao;

    /**
     * Field _codProcessoNegocio
     */
    private java.lang.String _codProcessoNegocio;

  
      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencia() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodDeptoGesto
     * 
     */
    public void deleteCodDeptoGesto()
    {
        this._has_codDeptoGesto= false;
    } //-- void deleteCodDeptoGesto() 

    /**
     * Method deleteCodEmpresa
     * 
     */
    public void deleteCodEmpresa()
    {
        this._has_codEmpresa= false;
    } //-- void deleteCodEmpresa() 

    /**
     * Method deleteCodRegraAlcada
     * 
     */
    public void deleteCodRegraAlcada()
    {
        this._has_codRegraAlcada= false;
    } //-- void deleteCodRegraAlcada() 

    /**
     * Method deleteCodSituacao
     * 
     */
    public void deleteCodSituacao()
    {
        this._has_codSituacao= false;
    } //-- void deleteCodSituacao() 

   
    /**
     * Returns the value of field 'codDeptoGesto'.
     * 
     * @return int
     * @return the value of field 'codDeptoGesto'.
     */
    public int getCodDeptoGesto()
    {
        return this._codDeptoGesto;
    } //-- int getCodDeptoGesto() 

    /**
     * Returns the value of field 'codEmpresa'.
     * 
     * @return long
     * @return the value of field 'codEmpresa'.
     */
    public long getCodEmpresa()
    {
        return this._codEmpresa;
    } //-- long getCodEmpresa() 

    /**
     * Returns the value of field 'codProcessoNegocio'.
     * 
     * @return String
     * @return the value of field 'codProcessoNegocio'.
     */
    public java.lang.String getCodProcessoNegocio()
    {
        return this._codProcessoNegocio;
    } //-- java.lang.String getCodProcessoNegocio() 

    /**
     * Returns the value of field 'codRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'codRegraAlcada'.
     */
    public int getCodRegraAlcada()
    {
        return this._codRegraAlcada;
    } //-- int getCodRegraAlcada() 

    /**
     * Returns the value of field 'codSituacao'.
     * 
     * @return int
     * @return the value of field 'codSituacao'.
     */
    public int getCodSituacao()
    {
        return this._codSituacao;
    } //-- int getCodSituacao() 

    /**
     * Returns the value of field 'codWorkFlow'.
     * 
     * @return String
     * @return the value of field 'codWorkFlow'.
     */
    public java.lang.String getCodWorkFlow()
    {
        return this._codWorkFlow;
    } //-- java.lang.String getCodWorkFlow() 

    /**
     * Returns the value of field 'dataAtivacaoRegra'.
     * 
     * @return String
     * @return the value of field 'dataAtivacaoRegra'.
     */
    public java.lang.String getDataAtivacaoRegra()
    {
        return this._dataAtivacaoRegra;
    } //-- java.lang.String getDataAtivacaoRegra() 

    /**
     * Returns the value of field 'descDeptoGesto'.
     * 
     * @return String
     * @return the value of field 'descDeptoGesto'.
     */
    public java.lang.String getDescDeptoGesto()
    {
        return this._descDeptoGesto;
    } //-- java.lang.String getDescDeptoGesto() 

    /**
     * Returns the value of field 'descEmpresa'.
     * 
     * @return String
     * @return the value of field 'descEmpresa'.
     */
    public java.lang.String getDescEmpresa()
    {
        return this._descEmpresa;
    } //-- java.lang.String getDescEmpresa() 

    /**
     * Returns the value of field 'descRegraAlcada'.
     * 
     * @return String
     * @return the value of field 'descRegraAlcada'.
     */
    public java.lang.String getDescRegraAlcada()
    {
        return this._descRegraAlcada;
    } //-- java.lang.String getDescRegraAlcada() 

    /**
     * Returns the value of field 'descSituacao'.
     * 
     * @return String
     * @return the value of field 'descSituacao'.
     */
    public java.lang.String getDescSituacao()
    {
        return this._descSituacao;
    } //-- java.lang.String getDescSituacao() 


    /**
     * Method hasCodDeptoGesto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodDeptoGesto()
    {
        return this._has_codDeptoGesto;
    } //-- boolean hasCodDeptoGesto() 

    /**
     * Method hasCodEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodEmpresa()
    {
        return this._has_codEmpresa;
    } //-- boolean hasCodEmpresa() 

    /**
     * Method hasCodRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodRegraAlcada()
    {
        return this._has_codRegraAlcada;
    } //-- boolean hasCodRegraAlcada() 

    /**
     * Method hasCodSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodSituacao()
    {
        return this._has_codSituacao;
    } //-- boolean hasCodSituacao() 

  
    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codDeptoGesto'.
     * 
     * @param codDeptoGesto the value of field 'codDeptoGesto'.
     */
    public void setCodDeptoGesto(int codDeptoGesto)
    {
        this._codDeptoGesto = codDeptoGesto;
        this._has_codDeptoGesto = true;
    } //-- void setCodDeptoGesto(int) 

    /**
     * Sets the value of field 'codEmpresa'.
     * 
     * @param codEmpresa the value of field 'codEmpresa'.
     */
    public void setCodEmpresa(long codEmpresa)
    {
        this._codEmpresa = codEmpresa;
        this._has_codEmpresa = true;
    } //-- void setCodEmpresa(long) 

    /**
     * Sets the value of field 'codProcessoNegocio'.
     * 
     * @param codProcessoNegocio the value of field
     * 'codProcessoNegocio'.
     */
    public void setCodProcessoNegocio(java.lang.String codProcessoNegocio)
    {
        this._codProcessoNegocio = codProcessoNegocio;
    } //-- void setCodProcessoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'codRegraAlcada'.
     * 
     * @param codRegraAlcada the value of field 'codRegraAlcada'.
     */
    public void setCodRegraAlcada(int codRegraAlcada)
    {
        this._codRegraAlcada = codRegraAlcada;
        this._has_codRegraAlcada = true;
    } //-- void setCodRegraAlcada(int) 

    /**
     * Sets the value of field 'codSituacao'.
     * 
     * @param codSituacao the value of field 'codSituacao'.
     */
    public void setCodSituacao(int codSituacao)
    {
        this._codSituacao = codSituacao;
        this._has_codSituacao = true;
    } //-- void setCodSituacao(int) 

   

    /**
     * Sets the value of field 'codWorkFlow'.
     * 
     * @param codWorkFlow the value of field 'codWorkFlow'.
     */
    public void setCodWorkFlow(java.lang.String codWorkFlow)
    {
        this._codWorkFlow = codWorkFlow;
    } //-- void setCodWorkFlow(java.lang.String) 

    /**
     * Sets the value of field 'dataAtivacaoRegra'.
     * 
     * @param dataAtivacaoRegra the value of field
     * 'dataAtivacaoRegra'.
     */
    public void setDataAtivacaoRegra(java.lang.String dataAtivacaoRegra)
    {
        this._dataAtivacaoRegra = dataAtivacaoRegra;
    } //-- void setDataAtivacaoRegra(java.lang.String) 

    /**
     * Sets the value of field 'descDeptoGesto'.
     * 
     * @param descDeptoGesto the value of field 'descDeptoGesto'.
     */
    public void setDescDeptoGesto(java.lang.String descDeptoGesto)
    {
        this._descDeptoGesto = descDeptoGesto;
    } //-- void setDescDeptoGesto(java.lang.String) 

    /**
     * Sets the value of field 'descEmpresa'.
     * 
     * @param descEmpresa the value of field 'descEmpresa'.
     */
    public void setDescEmpresa(java.lang.String descEmpresa)
    {
        this._descEmpresa = descEmpresa;
    } //-- void setDescEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'descRegraAlcada'.
     * 
     * @param descRegraAlcada the value of field 'descRegraAlcada'.
     */
    public void setDescRegraAlcada(java.lang.String descRegraAlcada)
    {
        this._descRegraAlcada = descRegraAlcada;
    } //-- void setDescRegraAlcada(java.lang.String) 

    /**
     * Sets the value of field 'descSituacao'.
     * 
     * @param descSituacao the value of field 'descSituacao'.
     */
    public void setDescSituacao(java.lang.String descSituacao)
    {
        this._descSituacao = descSituacao;
    } //-- void setDescSituacao(java.lang.String) 

 
    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencia
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
