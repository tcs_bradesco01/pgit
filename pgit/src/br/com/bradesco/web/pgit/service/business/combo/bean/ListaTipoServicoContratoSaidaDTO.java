/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListaTipoServicoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListaTipoServicoContratoSaidaDTO {

	/** Atributo cdParametroTela. */
	private Integer cdParametroTela;
	
	/** Atributo cdprodutoServicoOperacao. */
	private Integer cdprodutoServicoOperacao;
	
	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo dsProdutoOperacaoRelacionado. */
	private String dsProdutoOperacaoRelacionado;
	
	/** Atributo cdRelacionamentoProdutoProduto. */
	private Integer cdRelacionamentoProdutoProduto;
	
	/** Atributo dtInclusaoServico. */
	private String dtInclusaoServico;
	
	/** Atributo cdSituacaoServicoRelacionado. */
	private Integer cdSituacaoServicoRelacionado;
	
	/** Atributo dsSituacaoServicoRelacionado. */
	private String dsSituacaoServicoRelacionado;
	
	/** Atributo cdNaturezaOperacaoPagamento. */
	private Integer cdNaturezaOperacaoPagamento;
	
	/** Atributo cdServicoCompostoPagamento. */
	private Long cdServicoCompostoPagamento;
	
	/**
	 * Get: cdParametroTela.
	 *
	 * @return cdParametroTela
	 */
	public Integer getCdParametroTela() {
		return cdParametroTela;
	}
	
	/**
	 * Set: cdParametroTela.
	 *
	 * @param cdParametroTela the cd parametro tela
	 */
	public void setCdParametroTela(Integer cdParametroTela) {
		this.cdParametroTela = cdParametroTela;
	}
	
	/**
	 * Get: cdprodutoServicoOperacao.
	 *
	 * @return cdprodutoServicoOperacao
	 */
	public Integer getCdprodutoServicoOperacao() {
		return cdprodutoServicoOperacao;
	}
	
	/**
	 * Set: cdprodutoServicoOperacao.
	 *
	 * @param cdprodutoServicoOperacao the cdproduto servico operacao
	 */
	public void setCdprodutoServicoOperacao(Integer cdprodutoServicoOperacao) {
		this.cdprodutoServicoOperacao = cdprodutoServicoOperacao;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdRelacionamentoProdutoProduto.
	 *
	 * @return cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProdutoProduto.
	 *
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: dtInclusaoServico.
	 *
	 * @return dtInclusaoServico
	 */
	public String getDtInclusaoServico() {
		return dtInclusaoServico;
	}
	
	/**
	 * Set: dtInclusaoServico.
	 *
	 * @param dtInclusaoServico the dt inclusao servico
	 */
	public void setDtInclusaoServico(String dtInclusaoServico) {
		this.dtInclusaoServico = dtInclusaoServico;
	}
	
	/**
	 * Get: cdSituacaoServicoRelacionado.
	 *
	 * @return cdSituacaoServicoRelacionado
	 */
	public Integer getCdSituacaoServicoRelacionado() {
		return cdSituacaoServicoRelacionado;
	}
	
	/**
	 * Set: cdSituacaoServicoRelacionado.
	 *
	 * @param cdSituacaoServicoRelacionado the cd situacao servico relacionado
	 */
	public void setCdSituacaoServicoRelacionado(Integer cdSituacaoServicoRelacionado) {
		this.cdSituacaoServicoRelacionado = cdSituacaoServicoRelacionado;
	}
	
	/**
	 * Get: dsSituacaoServicoRelacionado.
	 *
	 * @return dsSituacaoServicoRelacionado
	 */
	public String getDsSituacaoServicoRelacionado() {
		return dsSituacaoServicoRelacionado;
	}
	
	/**
	 * Set: dsSituacaoServicoRelacionado.
	 *
	 * @param dsSituacaoServicoRelacionado the ds situacao servico relacionado
	 */
	public void setDsSituacaoServicoRelacionado(String dsSituacaoServicoRelacionado) {
		this.dsSituacaoServicoRelacionado = dsSituacaoServicoRelacionado;
	}
	
	/**
	 * Get: cdNaturezaOperacaoPagamento.
	 *
	 * @return cdNaturezaOperacaoPagamento
	 */
	public Integer getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}
	
	/**
	 * Set: cdNaturezaOperacaoPagamento.
	 *
	 * @param cdNaturezaOperacaoPagamento the cd natureza operacao pagamento
	 */
	public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}
	
	/**
	 * Get: cdServicoCompostoPagamento.
	 *
	 * @return cdServicoCompostoPagamento
	 */
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}
	
	/**
	 * Set: cdServicoCompostoPagamento.
	 *
	 * @param cdServicoCompostoPagamento the cd servico composto pagamento
	 */
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}

}
