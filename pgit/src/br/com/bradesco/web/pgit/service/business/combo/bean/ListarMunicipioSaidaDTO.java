/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMunicipioSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMunicipioSaidaDTO {
	
	/** Atributo cdMunicipio. */
	private Long cdMunicipio;
	
	/** Atributo dsMunicipio. */
	private String dsMunicipio;
	
	/**
	 * Get: cdMunicipio.
	 *
	 * @return cdMunicipio
	 */
	public Long getCdMunicipio() {
		return cdMunicipio;
	}
	
	/**
	 * Set: cdMunicipio.
	 *
	 * @param cdMunicipio the cd municipio
	 */
	public void setCdMunicipio(Long cdMunicipio) {
		this.cdMunicipio = cdMunicipio;
	}
	
	/**
	 * Get: dsMunicipio.
	 *
	 * @return dsMunicipio
	 */
	public String getDsMunicipio() {
		return dsMunicipio;
	}
	
	/**
	 * Set: dsMunicipio.
	 *
	 * @param dsMunicipio the ds municipio
	 */
	public void setDsMunicipio(String dsMunicipio) {
		this.dsMunicipio = dsMunicipio;
	}
	
	
}
