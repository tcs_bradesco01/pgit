/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.IConsultarOrdemPagtoAgePagService;
import br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.IConsultarOrdemPagtoAgePagServiceConstants;
import br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean.ConsultarOrdemPagtoAgePagEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean.ConsultarOrdemPagtoAgePagSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.request.ConsultarOrdemPagtoPorAgePagadoraRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.response.ConsultarOrdemPagtoPorAgePagadoraResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConsultarOrdemPagtoAgePag
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConsultarOrdemPagtoAgePagServiceImpl implements IConsultarOrdemPagtoAgePagService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.IConsultarOrdemPagtoAgePagService#consultarOrdemPagtoAgePag(br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean.ConsultarOrdemPagtoAgePagEntradaDTO)
	 */
	public List<ConsultarOrdemPagtoAgePagSaidaDTO> consultarOrdemPagtoAgePag(ConsultarOrdemPagtoAgePagEntradaDTO entradaDTO) {

		ConsultarOrdemPagtoPorAgePagadoraRequest request = new ConsultarOrdemPagtoPorAgePagadoraRequest();
		ConsultarOrdemPagtoPorAgePagadoraResponse response = new ConsultarOrdemPagtoPorAgePagadoraResponse();
    	List<ConsultarOrdemPagtoAgePagSaidaDTO> listaSaida = new ArrayList<ConsultarOrdemPagtoAgePagSaidaDTO>();
    	
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getCdCnpjCpf()));
    	request.setCdControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjCpf()));
    	request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjCpf()));
    	request.setDataFim(FormatarData.formatarDataToPdc(PgitUtil.verificaStringNula(entradaDTO.getDataFim())));
    	request.setDataInicio(FormatarData.formatarDataToPdc(PgitUtil.verificaStringNula(entradaDTO.getDataInicio())));
    	request.setNumeroOcorrencias(IConsultarOrdemPagtoAgePagServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setVlPagamentoClienteFim(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoClienteFim()));
    	request.setVlPagamentoClienteInicio(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoClienteInicio()));
    	request.setCdTipoInscricao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoInscricao()));
    	
        request.setCdSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoPagamento()));
        request.setCdTipoRetornoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoRetornoPagamento()));
    	
    	response = getFactoryAdapter().getConsultarOrdemPagtoPorAgePagadoraPDCAdapter().invokeProcess(request);
    	
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		ConsultarOrdemPagtoAgePagSaidaDTO saida = new ConsultarOrdemPagtoAgePagSaidaDTO();
    		saida.setCorpoCpfPagador(response.getOcorrencias(i).getCorpoCpfPagador());
    		saida.setFilialCpfPagador(response.getOcorrencias(i).getFilialCpfPagador());
    		saida.setControleCpfPagador(response.getOcorrencias(i).getControleCpfPagador());
    		saida.setDsClientePagador(response.getOcorrencias(i).getDsClientePagador());
    		saida.setCdAgenciaPagadora(response.getOcorrencias(i).getCdAgenciaPagadora());
    		saida.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido());
    		saida.setCdIdentificacaoInscricaoFavorecido(response.getOcorrencias(i).getCdIdentificacaoInscricaoFavorecido());
    		saida.setDsIdentificacaoInscricaoFavorecido(response.getOcorrencias(i).getDsIdentificacaoInscricaoFavorecido());
    		saida.setDsAbrevFavorecido(response.getOcorrencias(i).getDsAbrevFavorecido());
    		saida.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento());
    		saida.setDtCreditoPagamentoFormatada(FormatarData.formatarDataFromPdc(response.getOcorrencias(i).getDtCreditoPagamento()));
    		saida.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
    		saida.setVlPagamentoClientePagador(response.getOcorrencias(i).getVlPagamentoClientePagador());
    		saida.setDtDisponivelAte(response.getOcorrencias(i).getDtDisponivelAte());
    		saida.setDtDisponivelAteFormatada(FormatarData.formatarDataFromPdc(response.getOcorrencias(i).getDtDisponivelAte()));
    		saida.setCdSituacaoOperacao(response.getOcorrencias(i).getCdSituacaoOperacao());
    		saida.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
    		saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
    		//saida.setDsEmpresaContrato(response.getOcorrencias(i).getDsEmpresaContrato());
    		saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
    		saida.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
    		//saida.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
    		saida.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
    		saida.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
    		saida.setDigitoAgenciaDebito(response.getOcorrencias(i).getDigitoAgenciaDebito());
    		saida.setContaDebito(response.getOcorrencias(i).getContaDebito());
    		saida.setDigitoContaDebito(response.getOcorrencias(i).getDigitoContaDebito());
    		saida.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
    		saida.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
    		saida.setDigitoAgenciaCredito(response.getOcorrencias(i).getDigitoAgenciaCredito());
    		saida.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
    		saida.setDigitoContaCredito(response.getOcorrencias(i).getDigitoContaCredito());
    		saida.setDsTipoContaCredito(response.getOcorrencias(i).getDsTipoContaCredito());
    		saida.setCdServido(response.getOcorrencias(i).getCdServido());
    		saida.setCdModalidade(response.getOcorrencias(i).getCdModalidade());
    		saida.setDsServico(response.getOcorrencias(i).getDsServico());
    		/*saida.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
    		saida.setCdInscricaoBeneficiario(response.getOcorrencias(i).getCdInscricaoBeneficiario());
    		saida.setCdTipoInscricaoBeneficiario(response.getOcorrencias(i).getCdTipoInscricaoBeneficiario());
    		saida.setDsTipoInscricaoBeneficiario(response.getOcorrencias(i).getDsTipoInscricaoBeneficiario());
    		saida.setDsBeneficiario(response.getOcorrencias(i).getDsBeneficiario());*/
    		saida.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
    		saida.setCdAgenciaVencimento(response.getOcorrencias(i).getCdAgenciaVencimento());
    		
    		saida.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(saida.getCorpoCpfPagador(), saida.getFilialCpfPagador(), saida.getControleCpfPagador()));
    		saida.setClientePagadorFormatado(PgitUtil.concatenarCampos(saida.getCpfCnpjFormatado(), saida.getDsClientePagador()));
    		
    	    saida.setBancoCreditoFormatado(PgitUtil.formatBanco(response.getOcorrencias(i).getCdBancoCredito(), false));
    	    saida.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getDigitoAgenciaCredito(), false));
    	    saida.setContaCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdContaCredito(), String.valueOf(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDigitoContaCredito())), false));
    	    
    	    saida.setCdTipoRetornoPagamento(response.getOcorrencias(i).getCdTipoRetornoPagamento());
    	    saida.setDescTipoRetornoPagamento(response.getOcorrencias(i).getDescTipoRetornoPagamento());
    	    saida.setCdFavorecido((Long)PgitUtil.verificaZero(response.getOcorrencias(i).getCdFavorecido()));   
    	    
    	    saida.setInscricaoFavorecidoFormatado(saida.getCdInscricaoFavorecido() == 0L ? PgitUtil.formatarCnpj(saida.getCdInscricaoFavorecido().toString()) : PgitUtil.formataFavorecido((Integer)PgitUtil.verificaZero(saida.getCdIdentificacaoInscricaoFavorecido()), (String)PgitUtil.verificaZero(saida.getCdInscricaoFavorecido().toString())));
    		
    		listaSaida.add(saida);
    	}
    	
    	return listaSaida;	
	}
}

