/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;


/**
 * Nome: ConsultarTarifaContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarTarifaContratoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdProduto. */
	private int cdProduto;
	
	/** Atributo dsProduto. */
	private String dsProduto;
	
	/** Atributo cdOperacao. */
	private int cdOperacao;
	
	/** Atributo dsOperacao. */
	private String dsOperacao;
	
	/** Atributo cdTipoProduto. */
	private int cdTipoProduto;
	
	/** Atributo dtInicioVigencia. */
	private String dtInicioVigencia;
	
	/** Atributo dtFimVigencia. */
	private String dtFimVigencia;
	
	/** Atributo cdCategoriaOperacao. */
	private int cdCategoriaOperacao;
	
	/** Atributo cdCondicoesEconomica. */
	private int cdCondicoesEconomica;
	
	/** Atributo vlTarifa. */
	private String vlTarifa;
	
	/** Atributo vlrReferenciaMinimo. */
	private String vlrReferenciaMinimo;
	
	/** Atributo vlrReferenciaMaximo. */
	private String vlrReferenciaMaximo;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private int cdTipoCanalInclusao;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private int cdTipoCanalManutencao;
	
	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;
	
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo dsProdutoServicoRelacionado. */
    private String dsProdutoServicoRelacionado;        
        
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
	    return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
	    this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: dsProdutoServicoRelacionado.
	 *
	 * @return dsProdutoServicoRelacionado
	 */
	public String getDsProdutoServicoRelacionado() {
	    return dsProdutoServicoRelacionado;
	}
	
	/**
	 * Set: dsProdutoServicoRelacionado.
	 *
	 * @param dsProdutoServicoRelacionado the ds produto servico relacionado
	 */
	public void setDsProdutoServicoRelacionado(String dsProdutoServicoRelacionado) {
	    this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdCategoriaOperacao.
	 *
	 * @return cdCategoriaOperacao
	 */
	public int getCdCategoriaOperacao() {
		return cdCategoriaOperacao;
	}
	
	/**
	 * Set: cdCategoriaOperacao.
	 *
	 * @param cdCategoriaOperacao the cd categoria operacao
	 */
	public void setCdCategoriaOperacao(int cdCategoriaOperacao) {
		this.cdCategoriaOperacao = cdCategoriaOperacao;
	}
	
	/**
	 * Get: cdCondicoesEconomica.
	 *
	 * @return cdCondicoesEconomica
	 */
	public int getCdCondicoesEconomica() {
		return cdCondicoesEconomica;
	}
	
	/**
	 * Set: cdCondicoesEconomica.
	 *
	 * @param cdCondicoesEconomica the cd condicoes economica
	 */
	public void setCdCondicoesEconomica(int cdCondicoesEconomica) {
		this.cdCondicoesEconomica = cdCondicoesEconomica;
	}
	
	/**
	 * Get: cdOperacao.
	 *
	 * @return cdOperacao
	 */
	public int getCdOperacao() {
		return cdOperacao;
	}
	
	/**
	 * Set: cdOperacao.
	 *
	 * @param cdOperacao the cd operacao
	 */
	public void setCdOperacao(int cdOperacao) {
		this.cdOperacao = cdOperacao;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdProduto.
	 *
	 * @return cdProduto
	 */
	public int getCdProduto() {
		return cdProduto;
	}
	
	/**
	 * Set: cdProduto.
	 *
	 * @param cdProduto the cd produto
	 */
	public void setCdProduto(int cdProduto) {
		this.cdProduto = cdProduto;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public int getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(int cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public int getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(int cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoProduto.
	 *
	 * @return cdTipoProduto
	 */
	public int getCdTipoProduto() {
		return cdTipoProduto;
	}
	
	/**
	 * Set: cdTipoProduto.
	 *
	 * @param cdTipoProduto the cd tipo produto
	 */
	public void setCdTipoProduto(int cdTipoProduto) {
		this.cdTipoProduto = cdTipoProduto;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsOperacao.
	 *
	 * @return dsOperacao
	 */
	public String getDsOperacao() {
		return dsOperacao;
	}
	
	/**
	 * Set: dsOperacao.
	 *
	 * @param dsOperacao the ds operacao
	 */
	public void setDsOperacao(String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}
	
	/**
	 * Get: dsProduto.
	 *
	 * @return dsProduto
	 */
	public String getDsProduto() {
		return dsProduto;
	}
	
	/**
	 * Set: dsProduto.
	 *
	 * @param dsProduto the ds produto
	 */
	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: dtFimVigencia.
	 *
	 * @return dtFimVigencia
	 */
	public String getDtFimVigencia() {
		return dtFimVigencia;
	}
	
	/**
	 * Set: dtFimVigencia.
	 *
	 * @param dtFimVigencia the dt fim vigencia
	 */
	public void setDtFimVigencia(String dtFimVigencia) {
		this.dtFimVigencia = dtFimVigencia;
	}
	
	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}
	
	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: vlrReferenciaMaximo.
	 *
	 * @return vlrReferenciaMaximo
	 */
	public String getVlrReferenciaMaximo() {
		return vlrReferenciaMaximo;
	}
	
	/**
	 * Set: vlrReferenciaMaximo.
	 *
	 * @param vlrReferenciaMaximo the vlr referencia maximo
	 */
	public void setVlrReferenciaMaximo(String vlrReferenciaMaximo) {
		this.vlrReferenciaMaximo = vlrReferenciaMaximo;
	}
	
	/**
	 * Get: vlrReferenciaMinimo.
	 *
	 * @return vlrReferenciaMinimo
	 */
	public String getVlrReferenciaMinimo() {
		return vlrReferenciaMinimo;
	}
	
	/**
	 * Set: vlrReferenciaMinimo.
	 *
	 * @param vlrReferenciaMinimo the vlr referencia minimo
	 */
	public void setVlrReferenciaMinimo(String vlrReferenciaMinimo) {
		this.vlrReferenciaMinimo = vlrReferenciaMinimo;
	}
	
	/**
	 * Get: vlTarifa.
	 *
	 * @return vlTarifa
	 */
	public String getVlTarifa() {
		return vlTarifa;
	}
	
	/**
	 * Set: vlTarifa.
	 *
	 * @param vlTarifa the vl tarifa
	 */
	public void setVlTarifa(String vlTarifa) {
		this.vlTarifa = vlTarifa;
	}

	

}
