/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.histcomplementar.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.histcomplementar.IHistCompServiceConstants;
import br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.AlterarMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.AlterarMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharHistMsgHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharHistMsgHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ExcluirMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ExcluirMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.IncluirMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.IncluirMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarHistMsgHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarHistMsgHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsghistoricocomplementar.request.AlterarMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsghistoricocomplementar.response.AlterarMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistmanutmsghistcomplementar.request.DetalharHistManutMsgHistComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistmanutmsghistcomplementar.response.DetalharHistManutMsgHistComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsghistoricocomplementar.request.DetalharMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsghistoricocomplementar.response.DetalharMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsghistoricocomplementar.request.ExcluirMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsghistoricocomplementar.response.ExcluirMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.request.IncluirMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.response.IncluirMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistmsghistoricocomplementar.request.DetalharHistMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistmsghistoricocomplementar.response.DetalharHistMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.request.ListarMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response.ListarMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterHistoricoComplementar
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class HistComplementarServiceImpl implements IHistComplementarService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * Construtor.
     */
    public HistComplementarServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.histcomplementar.IManterHistoricoComplementar#sampleManterHistoricoComplementar()
     */
    public void sampleManterHistoricoComplementar() {
        // TODO: Implementa�ao
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService#listarMantHistoricoComplementar(br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarMantHistoricoComplementarEntradaDTO)
	 */
	public List<ListarMantHistoricoComplementarSaidaDTO> listarMantHistoricoComplementar (ListarMantHistoricoComplementarEntradaDTO listarMantHistoricoComplementarEntradaDTO) {
		
		List<ListarMantHistoricoComplementarSaidaDTO> listaRetorno = new ArrayList<ListarMantHistoricoComplementarSaidaDTO>();
		ListarMsgHistoricoComplementarRequest request = new ListarMsgHistoricoComplementarRequest();
		ListarMsgHistoricoComplementarResponse response = new ListarMsgHistoricoComplementarResponse();
		
		request.setCdMensagemLinhaExtrato(listarMantHistoricoComplementarEntradaDTO.getCodigoHistoricoComplementar());
		request.setCdProdutoServicoOperacao(listarMantHistoricoComplementarEntradaDTO.getTipoServico());
		request.setCdProdutoOperacaoRelacionado(listarMantHistoricoComplementarEntradaDTO.getModalidadeServico());
		request.setCdRelacionamentoProduto(listarMantHistoricoComplementarEntradaDTO.getTipoRelacionamento());
		request.setCdIndicadorRestricaoContrato(0);
		request.setCdTipoMensagemExtrato(0);
		request.setQtOcorrencias(IHistCompServiceConstants.QTDE_CONSULTAS_LISTAR);
		
		response = getFactoryAdapter().getListarMsgHistoricoComplementarPDCAdapter().invokeProcess(request);
		
		ListarMantHistoricoComplementarSaidaDTO saidaDTO;
		
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			saidaDTO = new ListarMantHistoricoComplementarSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodLancamentoPersonalizado(response.getOcorrencias(i).getCdMensagemLinhaExtrato());
			saidaDTO.setRestricao(response.getOcorrencias(i).getCdIndicadorRestricaoContrato());
			saidaDTO.setTipoLancamento(response.getOcorrencias(i).getCdTipoMensagemExtrato());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			saidaDTO.setCdModalidadeServico(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			saidaDTO.setTipoRelacionamento(response.getOcorrencias(i).getCdRelacionadoProduto());
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService#incluirMantHistoricoComplementar(br.com.bradesco.web.pgit.service.business.histcomplementar.bean.IncluirMantHistoricoComplementarEntradaDTO)
	 */
	public IncluirMantHistoricoComplementarSaidaDTO incluirMantHistoricoComplementar (IncluirMantHistoricoComplementarEntradaDTO entradaDTO) {
		
		IncluirMantHistoricoComplementarSaidaDTO incluirMantHistoricoComplementarSaidaDTO = new IncluirMantHistoricoComplementarSaidaDTO();
		IncluirMsgHistoricoComplementarRequest incluirMantHistoricoComplementarRequest = new IncluirMsgHistoricoComplementarRequest();
		IncluirMsgHistoricoComplementarResponse incluirMantHistoricoComplementarResponse = new IncluirMsgHistoricoComplementarResponse();
		
		incluirMantHistoricoComplementarRequest.setCdMensagemLinhaExtrato(entradaDTO.getCodigoHistoricoComplementar());
		incluirMantHistoricoComplementarRequest.setCdSistemaLancamentoDebito(entradaDTO.getPrefixoDebito());
		incluirMantHistoricoComplementarRequest.setNrEventoLancamentoDebito(entradaDTO.getEventoMensagemDebito());
		incluirMantHistoricoComplementarRequest.setCdRecursoLancamentoDebito(entradaDTO.getRecursoDebito());
		incluirMantHistoricoComplementarRequest.setCdIdiomaLancamentoDebito(entradaDTO.getIdiomaDebito());
		incluirMantHistoricoComplementarRequest.setCdSistemaLancamentoCredito(entradaDTO.getPrefixoCredito());
		incluirMantHistoricoComplementarRequest.setNrEventoLancamentoCredito(entradaDTO.getEventoMensagemCredito());
		incluirMantHistoricoComplementarRequest.setCdRecursoLancamentoCredito(entradaDTO.getRecursoCredito());
		incluirMantHistoricoComplementarRequest.setCdIdiomaLancamentoCredito(entradaDTO.getIdiomaCredito());
		incluirMantHistoricoComplementarRequest.setCdIndicadorRestricaoContrato(entradaDTO.getRestricao());
		incluirMantHistoricoComplementarRequest.setCdTipoMensagemExtrato(entradaDTO.getTipoLancamento());
		incluirMantHistoricoComplementarRequest.setCdProdutoServicoOperacao(entradaDTO.getTipoServico());
		incluirMantHistoricoComplementarRequest.setCdProdutoOperacaoRelacionado(entradaDTO.getModalidadeServico());
		incluirMantHistoricoComplementarRequest.setCdRelacionamentoProduto(entradaDTO.getTipoRelacionamento());
		incluirMantHistoricoComplementarRequest.setDsSegundaLinhaExtratoCredito(entradaDTO.getDsSegundaLinhaExtratoCredito());
		incluirMantHistoricoComplementarRequest.setDsSegundaLinhaExtratoDebito(PgitUtil.verificaStringNula(entradaDTO.getDsSegundaLinhaExtratoDebito()));
		
		incluirMantHistoricoComplementarResponse = getFactoryAdapter().getIncluirMsgHistoricoComplementarPDCAdapter().invokeProcess(incluirMantHistoricoComplementarRequest);
		
		incluirMantHistoricoComplementarSaidaDTO.setCodMensagem(incluirMantHistoricoComplementarResponse.getCodMensagem());
		incluirMantHistoricoComplementarSaidaDTO.setMensagem(incluirMantHistoricoComplementarResponse.getMensagem());
		
		return incluirMantHistoricoComplementarSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService#alterarMantHistoricoComplementar(br.com.bradesco.web.pgit.service.business.histcomplementar.bean.AlterarMantHistoricoComplementarEntradaDTO)
	 */
	public AlterarMantHistoricoComplementarSaidaDTO alterarMantHistoricoComplementar (AlterarMantHistoricoComplementarEntradaDTO entradaDTO) {
		
		AlterarMantHistoricoComplementarSaidaDTO alterarMantHistoricoComplementarSaidaDTO = new AlterarMantHistoricoComplementarSaidaDTO();
		AlterarMsgHistoricoComplementarRequest alterarMantHistoricoComplementarRequest = new AlterarMsgHistoricoComplementarRequest();
		AlterarMsgHistoricoComplementarResponse alterarMantHistoricoComplementarResponse = new AlterarMsgHistoricoComplementarResponse();
		
		alterarMantHistoricoComplementarRequest.setCdMensagemLinhaExtrato(entradaDTO.getCodigoHistoricoComplementar());
		alterarMantHistoricoComplementarRequest.setCdSistemaLancamentoDebito(entradaDTO.getPrefixoDebito());
		alterarMantHistoricoComplementarRequest.setNrEventoLancamentoDebito(entradaDTO.getEventoMensagemDebito());
		alterarMantHistoricoComplementarRequest.setCdRecursoLancamentoDebito(entradaDTO.getRecursoDebito());
		alterarMantHistoricoComplementarRequest.setCdIdiomaLancamentoDebito(entradaDTO.getIdiomaDebito());
		alterarMantHistoricoComplementarRequest.setCdSistemaLancamentoCredito(entradaDTO.getPrefixoCredito());
		alterarMantHistoricoComplementarRequest.setNrEventoLancamentoCredito(entradaDTO.getEventoMensagemCredito());
		alterarMantHistoricoComplementarRequest.setCdRecursoLancamentoCredito(entradaDTO.getRecursoCredito());
		alterarMantHistoricoComplementarRequest.setCdIdiomaLancamentoCredito(entradaDTO.getIdiomaCredito());
		alterarMantHistoricoComplementarRequest.setCdIndicadorRestricaoContrato(entradaDTO.getRestricao());
		alterarMantHistoricoComplementarRequest.setCdTipoMensagemExtrato(entradaDTO.getTipoLancamento());
		alterarMantHistoricoComplementarRequest.setCdProdutoServicoOperacao(entradaDTO.getTipoServico());
		alterarMantHistoricoComplementarRequest.setCdProdutoOperacaoRelacionado(entradaDTO.getModalidadeServico());
		alterarMantHistoricoComplementarRequest.setCdRelacionamentoProduto(entradaDTO.getTipoRelacionamento());
		alterarMantHistoricoComplementarRequest.setDsSegundaLinhaExtratoCredito(entradaDTO.getDsSegundaLinhaExtratoCredito());
		alterarMantHistoricoComplementarRequest.setDsSegundaLinhaExtratoDebito(PgitUtil.verificaStringNula(entradaDTO.getDsSegundaLinhaExtratoDebito()));
	
		alterarMantHistoricoComplementarResponse = getFactoryAdapter().getAlterarMsgHistoricoComplementarPDCAdapter().invokeProcess(alterarMantHistoricoComplementarRequest);
					
		alterarMantHistoricoComplementarSaidaDTO.setCodMensagem(alterarMantHistoricoComplementarResponse.getCodMensagem());
		alterarMantHistoricoComplementarSaidaDTO.setMensagem(alterarMantHistoricoComplementarResponse.getMensagem());
				
		return alterarMantHistoricoComplementarSaidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService#excluirMantHistoricoComplementar(br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ExcluirMantHistoricoComplementarEntradaDTO)
	 */
	public ExcluirMantHistoricoComplementarSaidaDTO excluirMantHistoricoComplementar (ExcluirMantHistoricoComplementarEntradaDTO excluirMantHistoricoComplementarEntradaDTO) {
		
		ExcluirMantHistoricoComplementarSaidaDTO excluirMantHistoricoComplementarSaidaDTO = new ExcluirMantHistoricoComplementarSaidaDTO();
		ExcluirMsgHistoricoComplementarRequest excluirMantHistoricoComplementarRequest = new ExcluirMsgHistoricoComplementarRequest();
		ExcluirMsgHistoricoComplementarResponse excluirMantHistoricoComplementarResponse = new ExcluirMsgHistoricoComplementarResponse();
		
		excluirMantHistoricoComplementarRequest.setCdMensagemLinhaExtrato(excluirMantHistoricoComplementarEntradaDTO.getCodigoHistoricoComplementar());
		excluirMantHistoricoComplementarRequest.setCdProdutoServicoOperacao(excluirMantHistoricoComplementarEntradaDTO.getTipoServico());
		excluirMantHistoricoComplementarRequest.setCdProdutoOperacaoRelacionado(excluirMantHistoricoComplementarEntradaDTO.getModalidadeServico());
		excluirMantHistoricoComplementarRequest.setCdRelacionamentoProduto(excluirMantHistoricoComplementarEntradaDTO.getTipoRelacionamento());
		
		excluirMantHistoricoComplementarResponse = getFactoryAdapter().getExcluirMsgHistoricoComplementarPDCAdapter().invokeProcess(excluirMantHistoricoComplementarRequest);
		
		excluirMantHistoricoComplementarSaidaDTO.setCodMensagem(excluirMantHistoricoComplementarResponse.getCodMensagem());
		excluirMantHistoricoComplementarSaidaDTO.setMensagem(excluirMantHistoricoComplementarResponse.getMensagem());
	
		return excluirMantHistoricoComplementarSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService#detalharMantHistoricoComplementar(br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharMantHistoricoComplementarEntradaDTO)
	 */
	public DetalharMantHistoricoComplementarSaidaDTO detalharMantHistoricoComplementar (DetalharMantHistoricoComplementarEntradaDTO detalharMantHistoricoComplementarEntradaDTO) {
		
		DetalharMantHistoricoComplementarSaidaDTO saidaDTO = new DetalharMantHistoricoComplementarSaidaDTO();
		DetalharMsgHistoricoComplementarRequest request = new DetalharMsgHistoricoComplementarRequest();
		DetalharMsgHistoricoComplementarResponse response = new DetalharMsgHistoricoComplementarResponse();
		
		request.setCdMensagemLinhaExtrato(detalharMantHistoricoComplementarEntradaDTO.getCodigoHistoricoComplementar());
		request.setCdProdutoServicoOperacao(detalharMantHistoricoComplementarEntradaDTO.getTipoServico());
		request.setCdProdutoOperacaoRelacionado(detalharMantHistoricoComplementarEntradaDTO.getModalidadeServico());
		request.setCdRelacionadoProduto(detalharMantHistoricoComplementarEntradaDTO.getTipoRelacionamento());
		
		response = getFactoryAdapter().getDetalharMsgHistoricoComplementarPDCAdapter().invokeProcess(request);
		
		saidaDTO = new DetalharMantHistoricoComplementarSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setHistoricoComplementar(response.getOcorrencias(0).getCdMensagemLinhaExtrato());
		saidaDTO.setPrefixoDebito(response.getOcorrencias(0).getCdSistemaLancamentoDebito());
		saidaDTO.setCdMensagemDebito(response.getOcorrencias(0).getNrEventoLancamentoDebito());
		saidaDTO.setDsMensagemDebito(response.getOcorrencias(0).getDsEventoLancamentoDebito());
		saidaDTO.setCdRecursoDebito(response.getOcorrencias(0).getCdRecursoLancamentoDebito());
		saidaDTO.setDsRecursoDebito(response.getOcorrencias(0).getDsRecursoLancamentoDebito());
		saidaDTO.setCdIdiomaDebito(response.getOcorrencias(0).getCdIdiomaLancamentoDebito());
		saidaDTO.setDsIdiomaDebito(response.getOcorrencias(0).getDsIdiomaLancamentoDebito());
		saidaDTO.setPrefixoCredito(response.getOcorrencias(0).getCdSistemaLancamentoCredito());
		saidaDTO.setCdMensagemCredito(response.getOcorrencias(0).getNrEventoLancamentoCredito());
		saidaDTO.setDsMensagemCredito(response.getOcorrencias(0).getDsEventoLancamentoCredito());
		saidaDTO.setCdRecursoCredito(response.getOcorrencias(0).getCdRecursoLancamentoCredito());
		saidaDTO.setDsRecursoCredito(response.getOcorrencias(0).getDsRecursoLancamentoCredito());
		saidaDTO.setCdIdiomaCredito(response.getOcorrencias(0).getCdIdiomaLancamentoCredito());
		saidaDTO.setDsIdiomaCredito(response.getOcorrencias(0).getDsIdiomaLancamentoCredito());
		saidaDTO.setRestricao(response.getOcorrencias(0).getCdIndicadorRestricaoContrato());
		saidaDTO.setTipoLancamento(response.getOcorrencias(0).getCdTipoMensagemExtrato());
		saidaDTO.setCdTipoServico(response.getOcorrencias(0).getCdProdutoServicoOperacao());
		saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOperacao());
		saidaDTO.setCdModalidadeServico(response.getOcorrencias(0).getCdProdutoOperacaoRelacionado());
		saidaDTO.setDsModalidadeServico(response.getOcorrencias(0).getDsProdutoOperacaoRelacionado());
		saidaDTO.setCdRelacionamento(response.getOcorrencias(0).getCdRelacionadoProduto());
		saidaDTO.setCdTipoCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperacaoFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setCdTipoCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenticacaoSegurancaManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperacaoFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		saidaDTO.setDsSegundaLinhaExtratoCredito(response.getOcorrencias(0).getDsSegundaLinhaExtratoCredito());
		saidaDTO.setDsSegundaLinhaExtratoDebito(response.getOcorrencias(0).getDsSegundaLinhaExtratoDebito());
		return saidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService#listarHistMsgHistoricoComplementar(br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarHistMsgHistoricoComplementarEntradaDTO)
	 */
	public List<ListarHistMsgHistoricoComplementarSaidaDTO> listarHistMsgHistoricoComplementar (ListarHistMsgHistoricoComplementarEntradaDTO listarHistMsgHistoricoComplementarEntradaDTO) {
		
		List<ListarHistMsgHistoricoComplementarSaidaDTO> listaRetorno = new ArrayList<ListarHistMsgHistoricoComplementarSaidaDTO>();
		DetalharHistMsgHistoricoComplementarRequest request = new DetalharHistMsgHistoricoComplementarRequest();
		DetalharHistMsgHistoricoComplementarResponse response = new DetalharHistMsgHistoricoComplementarResponse();
		
		request.setCdMensagemLinhaExtrato(listarHistMsgHistoricoComplementarEntradaDTO.getCodigoHistoricoComplementar());
		request.setHrInclusaoRegistroHistorico("");
		request.setDtInicio(listarHistMsgHistoricoComplementarEntradaDTO.getPeriodoInicial());
		request.setDtFim(listarHistMsgHistoricoComplementarEntradaDTO.getPeriodoFinal());
		request.setQtOcorrencias(IHistCompServiceConstants.QTDE_CONSULTAS_LISTAR);
		
		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");		
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		response = getFactoryAdapter().getListarHistMsgHistoricoComplementarPDCAdapter().invokeProcess(request);
			
		ListarHistMsgHistoricoComplementarSaidaDTO saidaDTO;
		
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			saidaDTO = new ListarHistMsgHistoricoComplementarSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodigoHistoricoComplementar(response.getOcorrencias(i).getCdMensagemLinhaExtrato());
			saidaDTO.setResponsavel(response.getOcorrencias(i).getCdUsuarioManutencao());
			saidaDTO.setTipoManutencao(response.getOcorrencias(i).getCdIndicadorTipoManutencao());
			if (response.getOcorrencias(i).getHrInclusaoRegistroHistorico() != null && response.getOcorrencias(i).getHrInclusaoRegistroHistorico().length() >=19 ){
				
				String stringData = response.getOcorrencias(i).getHrInclusaoRegistroHistorico().substring(0, 19);
				
				try {
					saidaDTO.setDataManutencaoDesc(formato2.format(formato1.parse(stringData)));
				}  catch (ParseException e) {
					saidaDTO.setDataManutencaoDesc("");
				}
			}else{
				saidaDTO.setDataManutencaoDesc("");
			}
			
			
			saidaDTO.setDataManutencao(response.getOcorrencias(i).getHrInclusaoRegistroHistorico());
			listaRetorno.add(saidaDTO);
		}
	
		
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService#detalharHistMsgHistoricoComplementar(br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharHistMsgHistoricoComplementarEntradaDTO)
	 */
	public DetalharHistMsgHistoricoComplementarSaidaDTO detalharHistMsgHistoricoComplementar (DetalharHistMsgHistoricoComplementarEntradaDTO detalharHistMsgHistoricoComplementarEntradaDTO) {
		
		DetalharHistMsgHistoricoComplementarSaidaDTO saidaDTO = new DetalharHistMsgHistoricoComplementarSaidaDTO();
		DetalharHistManutMsgHistComplementarRequest request = new DetalharHistManutMsgHistComplementarRequest();
		DetalharHistManutMsgHistComplementarResponse response = new DetalharHistManutMsgHistComplementarResponse();
		
		request.setCdMensagemLinhaExtrato(detalharHistMsgHistoricoComplementarEntradaDTO.getCodigoHistoricoComplementar());
		request.setHrInclusaoRegistroHistorico(detalharHistMsgHistoricoComplementarEntradaDTO.getDataManutencao());
		request.setDtInicio(detalharHistMsgHistoricoComplementarEntradaDTO.getPeriodoInicial());
		request.setDtFIm(detalharHistMsgHistoricoComplementarEntradaDTO.getPeriodoFinal());
		request.setQtOcorrencias(0);
		
		response = getFactoryAdapter().getDetalharHistManutMsgHistComplementarPDCAdapter().invokeProcess(request);
		
		saidaDTO = new DetalharHistMsgHistoricoComplementarSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCodigoHistoricoComplementar(response.getOcorrencias(0).getCdMensagemLinhaExtrato());
		saidaDTO.setPrefixoDebito(response.getOcorrencias(0).getCdSistemaLancamentoDebito());
		saidaDTO.setCdMensagemDebito(response.getOcorrencias(0).getNrEventoLancamentoDebito());
		saidaDTO.setDsMensagemDebito(response.getOcorrencias(0).getDsEventoLancamentoDebito());
		saidaDTO.setCdRecursoDebito(response.getOcorrencias(0).getCdRecursoLancamentoDebito());
		saidaDTO.setDsRecursoDebito(response.getOcorrencias(0).getDsRecursoLancamentoDebito());
		saidaDTO.setCdIdiomaDebito(response.getOcorrencias(0).getCdIdiomaLancamentoDebito());
		saidaDTO.setDsIdiomaDebito(response.getOcorrencias(0).getDsIdiomaLancamentoDebito());
		saidaDTO.setPrefixoCredito(response.getOcorrencias(0).getCdSistemaLancamentoCredito());
		saidaDTO.setCdMensagemCredito(response.getOcorrencias(0).getNrEventoLancamentoCredito());
		saidaDTO.setDsMensagemCredito(response.getOcorrencias(0).getDsEventoLancamentoCredito());
		saidaDTO.setCdRecursoCredito(response.getOcorrencias(0).getCdRecursoLancamentoCredito());
		saidaDTO.setDsRecursoCredito(response.getOcorrencias(0).getDsRecursoLancamentoCredito());
		saidaDTO.setCdIdiomaCredito(response.getOcorrencias(0).getCdIdiomaLancamentoCredito());
		saidaDTO.setDsIdiomaCredito(response.getOcorrencias(0).getDsIdiomaLancamentoCredito());
		saidaDTO.setRestricao(response.getOcorrencias(0).getCdIndicadorRestricaoContrato());
		saidaDTO.setTipoLancamento(response.getOcorrencias(0).getCdTipoMensagemExtrato());
		saidaDTO.setCdTipoServico(response.getOcorrencias(0).getCdProdutoServicoOperacao());
		saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOperacao());
		saidaDTO.setCdModalidadeServico(response.getOcorrencias(0).getCdProdutoOperacaoRelacionado());
		saidaDTO.setDsModalidadeServico(response.getOcorrencias(0).getDsProdutoOperacaoRelacionado());
		saidaDTO.setRelacionamento(response.getOcorrencias(0).getCdRelacionadoProduto());
		saidaDTO.setCdTipoCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenticacaoSegurancaoInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperacaoFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setCdTipoCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenticacaoSegurancaoManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperacaoFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		saidaDTO.setDsSegundaLinhaExtratoCredito(response.getOcorrencias(0).getDsSegundaLinhaExtratoCredito());
		saidaDTO.setDsSegundaLinhaExtratoDebito(response.getOcorrencias(0).getDsSegundaLinhaExtratoDebito());
		
		return saidaDTO;
		
	}
	
    
    
    
}

