/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarPerfilTrocaArquivoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarPerfilTrocaArquivoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPerfilTrocaArquivo
     */
    private long _cdPerfilTrocaArquivo = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArquivo
     */
    private boolean _has_cdPerfilTrocaArquivo;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdSistemaOrigemArquivo
     */
    private java.lang.String _cdSistemaOrigemArquivo;

    /**
     * Field _dsUserNameCliente
     */
    private java.lang.String _dsUserNameCliente;

    /**
     * Field _dsNomeArquivoRemessa
     */
    private java.lang.String _dsNomeArquivoRemessa;

    /**
     * Field _dsNameArquivoRetorno
     */
    private java.lang.String _dsNameArquivoRetorno;

    /**
     * Field _cdRejeicaoAcltoRemessa
     */
    private int _cdRejeicaoAcltoRemessa = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAcltoRemessa
     */
    private boolean _has_cdRejeicaoAcltoRemessa;

    /**
     * Field _qtMaxIncotRemessa
     */
    private int _qtMaxIncotRemessa = 0;

    /**
     * keeps track of state for field: _qtMaxIncotRemessa
     */
    private boolean _has_qtMaxIncotRemessa;

    /**
     * Field _percentualInconsistenciaRejeicaoRemessa
     */
    private java.math.BigDecimal _percentualInconsistenciaRejeicaoRemessa = new java.math.BigDecimal("0");

    /**
     * Field _cdNivelControleRemessa
     */
    private int _cdNivelControleRemessa = 0;

    /**
     * keeps track of state for field: _cdNivelControleRemessa
     */
    private boolean _has_cdNivelControleRemessa;

    /**
     * Field _cdControleNumeroRemessa
     */
    private int _cdControleNumeroRemessa = 0;

    /**
     * keeps track of state for field: _cdControleNumeroRemessa
     */
    private boolean _has_cdControleNumeroRemessa;

    /**
     * Field _cdPeriodicidadeContagemRemessa
     */
    private int _cdPeriodicidadeContagemRemessa = 0;

    /**
     * keeps track of state for field:
     * _cdPeriodicidadeContagemRemessa
     */
    private boolean _has_cdPeriodicidadeContagemRemessa;

    /**
     * Field _nrMaximoContagemRemessa
     */
    private long _nrMaximoContagemRemessa = 0;

    /**
     * keeps track of state for field: _nrMaximoContagemRemessa
     */
    private boolean _has_nrMaximoContagemRemessa;

    /**
     * Field _cdMeioPrincipalRemessa
     */
    private int _cdMeioPrincipalRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRemessa
     */
    private boolean _has_cdMeioPrincipalRemessa;

    /**
     * Field _cdMeioAlternativoRemessa
     */
    private int _cdMeioAlternativoRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioAlternativoRemessa
     */
    private boolean _has_cdMeioAlternativoRemessa;

    /**
     * Field _cdNivelControleRetorno
     */
    private int _cdNivelControleRetorno = 0;

    /**
     * keeps track of state for field: _cdNivelControleRetorno
     */
    private boolean _has_cdNivelControleRetorno;

    /**
     * Field _cdControleNumeroRetorno
     */
    private int _cdControleNumeroRetorno = 0;

    /**
     * keeps track of state for field: _cdControleNumeroRetorno
     */
    private boolean _has_cdControleNumeroRetorno;

    /**
     * Field _cdPerdcContagemRetorno
     */
    private int _cdPerdcContagemRetorno = 0;

    /**
     * keeps track of state for field: _cdPerdcContagemRetorno
     */
    private boolean _has_cdPerdcContagemRetorno;

    /**
     * Field _nrMaximoContagemRetorno
     */
    private long _nrMaximoContagemRetorno = 0;

    /**
     * keeps track of state for field: _nrMaximoContagemRetorno
     */
    private boolean _has_nrMaximoContagemRetorno;

    /**
     * Field _cdMeioPrincipalRetorno
     */
    private int _cdMeioPrincipalRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRetorno
     */
    private boolean _has_cdMeioPrincipalRetorno;

    /**
     * Field _cdMeioAlternativoRetorno
     */
    private int _cdMeioAlternativoRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioAlternativoRetorno
     */
    private boolean _has_cdMeioAlternativoRetorno;

    /**
     * Field _cdSerieAplicacaoTransmissao
     */
    private java.lang.String _cdSerieAplicacaoTransmissao;

    /**
     * Field _cdPessoaJuridicaParceiro
     */
    private long _cdPessoaJuridicaParceiro = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaParceiro
     */
    private boolean _has_cdPessoaJuridicaParceiro;

    /**
     * Field _cdEmpresaResponsavel
     */
    private int _cdEmpresaResponsavel = 0;

    /**
     * keeps track of state for field: _cdEmpresaResponsavel
     */
    private boolean _has_cdEmpresaResponsavel;

    /**
     * Field _cdCustoTransmicao
     */
    private java.math.BigDecimal _cdCustoTransmicao = new java.math.BigDecimal("0");

    /**
     * Field _cdApliFormat
     */
    private long _cdApliFormat = 0;

    /**
     * keeps track of state for field: _cdApliFormat
     */
    private boolean _has_cdApliFormat;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsNomeContatoCliente
     */
    private java.lang.String _dsNomeContatoCliente;

    /**
     * Field _cdAreaFone
     */
    private int _cdAreaFone = 0;

    /**
     * keeps track of state for field: _cdAreaFone
     */
    private boolean _has_cdAreaFone;

    /**
     * Field _cdFoneContatoCliente
     */
    private long _cdFoneContatoCliente = 0;

    /**
     * keeps track of state for field: _cdFoneContatoCliente
     */
    private boolean _has_cdFoneContatoCliente;

    /**
     * Field _cdRamalContatoCliente
     */
    private java.lang.String _cdRamalContatoCliente;

    /**
     * Field _dsEmailContatoCliente
     */
    private java.lang.String _dsEmailContatoCliente;

    /**
     * Field _dsSolicitacaoPendente
     */
    private java.lang.String _dsSolicitacaoPendente;

    /**
     * Field _cdAreaFonePend
     */
    private int _cdAreaFonePend = 0;

    /**
     * keeps track of state for field: _cdAreaFonePend
     */
    private boolean _has_cdAreaFonePend;

    /**
     * Field _cdFoneSolicitacaoPend
     */
    private long _cdFoneSolicitacaoPend = 0;

    /**
     * keeps track of state for field: _cdFoneSolicitacaoPend
     */
    private boolean _has_cdFoneSolicitacaoPend;

    /**
     * Field _cdRamalSolctPend
     */
    private java.lang.String _cdRamalSolctPend;

    /**
     * Field _dsEmailSolctPend
     */
    private java.lang.String _dsEmailSolctPend;

    /**
     * Field _qtMesRegistroTrafg
     */
    private long _qtMesRegistroTrafg = 0;

    /**
     * keeps track of state for field: _qtMesRegistroTrafg
     */
    private boolean _has_qtMesRegistroTrafg;

    /**
     * Field _dsObsGeralPerfil
     */
    private java.lang.String _dsObsGeralPerfil;

    /**
     * Field _cdIndicadorGeracaoSegmentoB
     */
    private int _cdIndicadorGeracaoSegmentoB = 0;

    /**
     * keeps track of state for field: _cdIndicadorGeracaoSegmentoB
     */
    private boolean _has_cdIndicadorGeracaoSegmentoB;

    /**
     * Field _cdIndicadorGeracaoSegmentoZ
     */
    private int _cdIndicadorGeracaoSegmentoZ = 0;

    /**
     * keeps track of state for field: _cdIndicadorGeracaoSegmentoZ
     */
    private boolean _has_cdIndicadorGeracaoSegmentoZ;

    /**
     * Field _cdIndicadorConsisteContaDebito
     */
    private int _cdIndicadorConsisteContaDebito = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorConsisteContaDebito
     */
    private boolean _has_cdIndicadorConsisteContaDebito;

    /**
     * Field _cdIndicadorCpfLayout
     */
    private int _cdIndicadorCpfLayout = 0;

    /**
     * keeps track of state for field: _cdIndicadorCpfLayout
     */
    private boolean _has_cdIndicadorCpfLayout;

    /**
     * Field _cdIndicadorAssociacaoLayout
     */
    private int _cdIndicadorAssociacaoLayout = 0;

    /**
     * keeps track of state for field: _cdIndicadorAssociacaoLayout
     */
    private boolean _has_cdIndicadorAssociacaoLayout;

    /**
     * Field _cdIndicadorContaComplementar
     */
    private int _cdIndicadorContaComplementar = 0;

    /**
     * keeps track of state for field: _cdIndicadorContaComplementar
     */
    private boolean _has_cdIndicadorContaComplementar;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarPerfilTrocaArquivoRequest() 
     {
        super();
        setPercentualInconsistenciaRejeicaoRemessa(new java.math.BigDecimal("0"));
        setCdCustoTransmicao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.request.AlterarPerfilTrocaArquivoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdApliFormat
     * 
     */
    public void deleteCdApliFormat()
    {
        this._has_cdApliFormat= false;
    } //-- void deleteCdApliFormat() 

    /**
     * Method deleteCdAreaFone
     * 
     */
    public void deleteCdAreaFone()
    {
        this._has_cdAreaFone= false;
    } //-- void deleteCdAreaFone() 

    /**
     * Method deleteCdAreaFonePend
     * 
     */
    public void deleteCdAreaFonePend()
    {
        this._has_cdAreaFonePend= false;
    } //-- void deleteCdAreaFonePend() 

    /**
     * Method deleteCdControleNumeroRemessa
     * 
     */
    public void deleteCdControleNumeroRemessa()
    {
        this._has_cdControleNumeroRemessa= false;
    } //-- void deleteCdControleNumeroRemessa() 

    /**
     * Method deleteCdControleNumeroRetorno
     * 
     */
    public void deleteCdControleNumeroRetorno()
    {
        this._has_cdControleNumeroRetorno= false;
    } //-- void deleteCdControleNumeroRetorno() 

    /**
     * Method deleteCdEmpresaResponsavel
     * 
     */
    public void deleteCdEmpresaResponsavel()
    {
        this._has_cdEmpresaResponsavel= false;
    } //-- void deleteCdEmpresaResponsavel() 

    /**
     * Method deleteCdFoneContatoCliente
     * 
     */
    public void deleteCdFoneContatoCliente()
    {
        this._has_cdFoneContatoCliente= false;
    } //-- void deleteCdFoneContatoCliente() 

    /**
     * Method deleteCdFoneSolicitacaoPend
     * 
     */
    public void deleteCdFoneSolicitacaoPend()
    {
        this._has_cdFoneSolicitacaoPend= false;
    } //-- void deleteCdFoneSolicitacaoPend() 

    /**
     * Method deleteCdIndicadorAssociacaoLayout
     * 
     */
    public void deleteCdIndicadorAssociacaoLayout()
    {
        this._has_cdIndicadorAssociacaoLayout= false;
    } //-- void deleteCdIndicadorAssociacaoLayout() 

    /**
     * Method deleteCdIndicadorConsisteContaDebito
     * 
     */
    public void deleteCdIndicadorConsisteContaDebito()
    {
        this._has_cdIndicadorConsisteContaDebito= false;
    } //-- void deleteCdIndicadorConsisteContaDebito() 

    /**
     * Method deleteCdIndicadorContaComplementar
     * 
     */
    public void deleteCdIndicadorContaComplementar()
    {
        this._has_cdIndicadorContaComplementar= false;
    } //-- void deleteCdIndicadorContaComplementar() 

    /**
     * Method deleteCdIndicadorCpfLayout
     * 
     */
    public void deleteCdIndicadorCpfLayout()
    {
        this._has_cdIndicadorCpfLayout= false;
    } //-- void deleteCdIndicadorCpfLayout() 

    /**
     * Method deleteCdIndicadorGeracaoSegmentoB
     * 
     */
    public void deleteCdIndicadorGeracaoSegmentoB()
    {
        this._has_cdIndicadorGeracaoSegmentoB= false;
    } //-- void deleteCdIndicadorGeracaoSegmentoB() 

    /**
     * Method deleteCdIndicadorGeracaoSegmentoZ
     * 
     */
    public void deleteCdIndicadorGeracaoSegmentoZ()
    {
        this._has_cdIndicadorGeracaoSegmentoZ= false;
    } //-- void deleteCdIndicadorGeracaoSegmentoZ() 

    /**
     * Method deleteCdMeioAlternativoRemessa
     * 
     */
    public void deleteCdMeioAlternativoRemessa()
    {
        this._has_cdMeioAlternativoRemessa= false;
    } //-- void deleteCdMeioAlternativoRemessa() 

    /**
     * Method deleteCdMeioAlternativoRetorno
     * 
     */
    public void deleteCdMeioAlternativoRetorno()
    {
        this._has_cdMeioAlternativoRetorno= false;
    } //-- void deleteCdMeioAlternativoRetorno() 

    /**
     * Method deleteCdMeioPrincipalRemessa
     * 
     */
    public void deleteCdMeioPrincipalRemessa()
    {
        this._has_cdMeioPrincipalRemessa= false;
    } //-- void deleteCdMeioPrincipalRemessa() 

    /**
     * Method deleteCdMeioPrincipalRetorno
     * 
     */
    public void deleteCdMeioPrincipalRetorno()
    {
        this._has_cdMeioPrincipalRetorno= false;
    } //-- void deleteCdMeioPrincipalRetorno() 

    /**
     * Method deleteCdNivelControleRemessa
     * 
     */
    public void deleteCdNivelControleRemessa()
    {
        this._has_cdNivelControleRemessa= false;
    } //-- void deleteCdNivelControleRemessa() 

    /**
     * Method deleteCdNivelControleRetorno
     * 
     */
    public void deleteCdNivelControleRetorno()
    {
        this._has_cdNivelControleRetorno= false;
    } //-- void deleteCdNivelControleRetorno() 

    /**
     * Method deleteCdPerdcContagemRetorno
     * 
     */
    public void deleteCdPerdcContagemRetorno()
    {
        this._has_cdPerdcContagemRetorno= false;
    } //-- void deleteCdPerdcContagemRetorno() 

    /**
     * Method deleteCdPerfilTrocaArquivo
     * 
     */
    public void deleteCdPerfilTrocaArquivo()
    {
        this._has_cdPerfilTrocaArquivo= false;
    } //-- void deleteCdPerfilTrocaArquivo() 

    /**
     * Method deleteCdPeriodicidadeContagemRemessa
     * 
     */
    public void deleteCdPeriodicidadeContagemRemessa()
    {
        this._has_cdPeriodicidadeContagemRemessa= false;
    } //-- void deleteCdPeriodicidadeContagemRemessa() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaParceiro
     * 
     */
    public void deleteCdPessoaJuridicaParceiro()
    {
        this._has_cdPessoaJuridicaParceiro= false;
    } //-- void deleteCdPessoaJuridicaParceiro() 

    /**
     * Method deleteCdRejeicaoAcltoRemessa
     * 
     */
    public void deleteCdRejeicaoAcltoRemessa()
    {
        this._has_cdRejeicaoAcltoRemessa= false;
    } //-- void deleteCdRejeicaoAcltoRemessa() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNrMaximoContagemRemessa
     * 
     */
    public void deleteNrMaximoContagemRemessa()
    {
        this._has_nrMaximoContagemRemessa= false;
    } //-- void deleteNrMaximoContagemRemessa() 

    /**
     * Method deleteNrMaximoContagemRetorno
     * 
     */
    public void deleteNrMaximoContagemRetorno()
    {
        this._has_nrMaximoContagemRetorno= false;
    } //-- void deleteNrMaximoContagemRetorno() 

    /**
     * Method deleteQtMaxIncotRemessa
     * 
     */
    public void deleteQtMaxIncotRemessa()
    {
        this._has_qtMaxIncotRemessa= false;
    } //-- void deleteQtMaxIncotRemessa() 

    /**
     * Method deleteQtMesRegistroTrafg
     * 
     */
    public void deleteQtMesRegistroTrafg()
    {
        this._has_qtMesRegistroTrafg= false;
    } //-- void deleteQtMesRegistroTrafg() 

    /**
     * Returns the value of field 'cdApliFormat'.
     * 
     * @return long
     * @return the value of field 'cdApliFormat'.
     */
    public long getCdApliFormat()
    {
        return this._cdApliFormat;
    } //-- long getCdApliFormat() 

    /**
     * Returns the value of field 'cdAreaFone'.
     * 
     * @return int
     * @return the value of field 'cdAreaFone'.
     */
    public int getCdAreaFone()
    {
        return this._cdAreaFone;
    } //-- int getCdAreaFone() 

    /**
     * Returns the value of field 'cdAreaFonePend'.
     * 
     * @return int
     * @return the value of field 'cdAreaFonePend'.
     */
    public int getCdAreaFonePend()
    {
        return this._cdAreaFonePend;
    } //-- int getCdAreaFonePend() 

    /**
     * Returns the value of field 'cdControleNumeroRemessa'.
     * 
     * @return int
     * @return the value of field 'cdControleNumeroRemessa'.
     */
    public int getCdControleNumeroRemessa()
    {
        return this._cdControleNumeroRemessa;
    } //-- int getCdControleNumeroRemessa() 

    /**
     * Returns the value of field 'cdControleNumeroRetorno'.
     * 
     * @return int
     * @return the value of field 'cdControleNumeroRetorno'.
     */
    public int getCdControleNumeroRetorno()
    {
        return this._cdControleNumeroRetorno;
    } //-- int getCdControleNumeroRetorno() 

    /**
     * Returns the value of field 'cdCustoTransmicao'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdCustoTransmicao'.
     */
    public java.math.BigDecimal getCdCustoTransmicao()
    {
        return this._cdCustoTransmicao;
    } //-- java.math.BigDecimal getCdCustoTransmicao() 

    /**
     * Returns the value of field 'cdEmpresaResponsavel'.
     * 
     * @return int
     * @return the value of field 'cdEmpresaResponsavel'.
     */
    public int getCdEmpresaResponsavel()
    {
        return this._cdEmpresaResponsavel;
    } //-- int getCdEmpresaResponsavel() 

    /**
     * Returns the value of field 'cdFoneContatoCliente'.
     * 
     * @return long
     * @return the value of field 'cdFoneContatoCliente'.
     */
    public long getCdFoneContatoCliente()
    {
        return this._cdFoneContatoCliente;
    } //-- long getCdFoneContatoCliente() 

    /**
     * Returns the value of field 'cdFoneSolicitacaoPend'.
     * 
     * @return long
     * @return the value of field 'cdFoneSolicitacaoPend'.
     */
    public long getCdFoneSolicitacaoPend()
    {
        return this._cdFoneSolicitacaoPend;
    } //-- long getCdFoneSolicitacaoPend() 

    /**
     * Returns the value of field 'cdIndicadorAssociacaoLayout'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAssociacaoLayout'.
     */
    public int getCdIndicadorAssociacaoLayout()
    {
        return this._cdIndicadorAssociacaoLayout;
    } //-- int getCdIndicadorAssociacaoLayout() 

    /**
     * Returns the value of field 'cdIndicadorConsisteContaDebito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorConsisteContaDebito'.
     */
    public int getCdIndicadorConsisteContaDebito()
    {
        return this._cdIndicadorConsisteContaDebito;
    } //-- int getCdIndicadorConsisteContaDebito() 

    /**
     * Returns the value of field 'cdIndicadorContaComplementar'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorContaComplementar'.
     */
    public int getCdIndicadorContaComplementar()
    {
        return this._cdIndicadorContaComplementar;
    } //-- int getCdIndicadorContaComplementar() 

    /**
     * Returns the value of field 'cdIndicadorCpfLayout'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCpfLayout'.
     */
    public int getCdIndicadorCpfLayout()
    {
        return this._cdIndicadorCpfLayout;
    } //-- int getCdIndicadorCpfLayout() 

    /**
     * Returns the value of field 'cdIndicadorGeracaoSegmentoB'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorGeracaoSegmentoB'.
     */
    public int getCdIndicadorGeracaoSegmentoB()
    {
        return this._cdIndicadorGeracaoSegmentoB;
    } //-- int getCdIndicadorGeracaoSegmentoB() 

    /**
     * Returns the value of field 'cdIndicadorGeracaoSegmentoZ'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorGeracaoSegmentoZ'.
     */
    public int getCdIndicadorGeracaoSegmentoZ()
    {
        return this._cdIndicadorGeracaoSegmentoZ;
    } //-- int getCdIndicadorGeracaoSegmentoZ() 

    /**
     * Returns the value of field 'cdMeioAlternativoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternativoRemessa'.
     */
    public int getCdMeioAlternativoRemessa()
    {
        return this._cdMeioAlternativoRemessa;
    } //-- int getCdMeioAlternativoRemessa() 

    /**
     * Returns the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternativoRetorno'.
     */
    public int getCdMeioAlternativoRetorno()
    {
        return this._cdMeioAlternativoRetorno;
    } //-- int getCdMeioAlternativoRetorno() 

    /**
     * Returns the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRemessa'.
     */
    public int getCdMeioPrincipalRemessa()
    {
        return this._cdMeioPrincipalRemessa;
    } //-- int getCdMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRetorno'.
     */
    public int getCdMeioPrincipalRetorno()
    {
        return this._cdMeioPrincipalRetorno;
    } //-- int getCdMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdNivelControleRemessa'.
     * 
     * @return int
     * @return the value of field 'cdNivelControleRemessa'.
     */
    public int getCdNivelControleRemessa()
    {
        return this._cdNivelControleRemessa;
    } //-- int getCdNivelControleRemessa() 

    /**
     * Returns the value of field 'cdNivelControleRetorno'.
     * 
     * @return int
     * @return the value of field 'cdNivelControleRetorno'.
     */
    public int getCdNivelControleRetorno()
    {
        return this._cdNivelControleRetorno;
    } //-- int getCdNivelControleRetorno() 

    /**
     * Returns the value of field 'cdPerdcContagemRetorno'.
     * 
     * @return int
     * @return the value of field 'cdPerdcContagemRetorno'.
     */
    public int getCdPerdcContagemRetorno()
    {
        return this._cdPerdcContagemRetorno;
    } //-- int getCdPerdcContagemRetorno() 

    /**
     * Returns the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArquivo'.
     */
    public long getCdPerfilTrocaArquivo()
    {
        return this._cdPerfilTrocaArquivo;
    } //-- long getCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'cdPeriodicidadeContagemRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeContagemRemessa'.
     */
    public int getCdPeriodicidadeContagemRemessa()
    {
        return this._cdPeriodicidadeContagemRemessa;
    } //-- int getCdPeriodicidadeContagemRemessa() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaParceiro'.
     */
    public long getCdPessoaJuridicaParceiro()
    {
        return this._cdPessoaJuridicaParceiro;
    } //-- long getCdPessoaJuridicaParceiro() 

    /**
     * Returns the value of field 'cdRamalContatoCliente'.
     * 
     * @return String
     * @return the value of field 'cdRamalContatoCliente'.
     */
    public java.lang.String getCdRamalContatoCliente()
    {
        return this._cdRamalContatoCliente;
    } //-- java.lang.String getCdRamalContatoCliente() 

    /**
     * Returns the value of field 'cdRamalSolctPend'.
     * 
     * @return String
     * @return the value of field 'cdRamalSolctPend'.
     */
    public java.lang.String getCdRamalSolctPend()
    {
        return this._cdRamalSolctPend;
    } //-- java.lang.String getCdRamalSolctPend() 

    /**
     * Returns the value of field 'cdRejeicaoAcltoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAcltoRemessa'.
     */
    public int getCdRejeicaoAcltoRemessa()
    {
        return this._cdRejeicaoAcltoRemessa;
    } //-- int getCdRejeicaoAcltoRemessa() 

    /**
     * Returns the value of field 'cdSerieAplicacaoTransmissao'.
     * 
     * @return String
     * @return the value of field 'cdSerieAplicacaoTransmissao'.
     */
    public java.lang.String getCdSerieAplicacaoTransmissao()
    {
        return this._cdSerieAplicacaoTransmissao;
    } //-- java.lang.String getCdSerieAplicacaoTransmissao() 

    /**
     * Returns the value of field 'cdSistemaOrigemArquivo'.
     * 
     * @return String
     * @return the value of field 'cdSistemaOrigemArquivo'.
     */
    public java.lang.String getCdSistemaOrigemArquivo()
    {
        return this._cdSistemaOrigemArquivo;
    } //-- java.lang.String getCdSistemaOrigemArquivo() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsEmailContatoCliente'.
     * 
     * @return String
     * @return the value of field 'dsEmailContatoCliente'.
     */
    public java.lang.String getDsEmailContatoCliente()
    {
        return this._dsEmailContatoCliente;
    } //-- java.lang.String getDsEmailContatoCliente() 

    /**
     * Returns the value of field 'dsEmailSolctPend'.
     * 
     * @return String
     * @return the value of field 'dsEmailSolctPend'.
     */
    public java.lang.String getDsEmailSolctPend()
    {
        return this._dsEmailSolctPend;
    } //-- java.lang.String getDsEmailSolctPend() 

    /**
     * Returns the value of field 'dsNameArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsNameArquivoRetorno'.
     */
    public java.lang.String getDsNameArquivoRetorno()
    {
        return this._dsNameArquivoRetorno;
    } //-- java.lang.String getDsNameArquivoRetorno() 

    /**
     * Returns the value of field 'dsNomeArquivoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRemessa'.
     */
    public java.lang.String getDsNomeArquivoRemessa()
    {
        return this._dsNomeArquivoRemessa;
    } //-- java.lang.String getDsNomeArquivoRemessa() 

    /**
     * Returns the value of field 'dsNomeContatoCliente'.
     * 
     * @return String
     * @return the value of field 'dsNomeContatoCliente'.
     */
    public java.lang.String getDsNomeContatoCliente()
    {
        return this._dsNomeContatoCliente;
    } //-- java.lang.String getDsNomeContatoCliente() 

    /**
     * Returns the value of field 'dsObsGeralPerfil'.
     * 
     * @return String
     * @return the value of field 'dsObsGeralPerfil'.
     */
    public java.lang.String getDsObsGeralPerfil()
    {
        return this._dsObsGeralPerfil;
    } //-- java.lang.String getDsObsGeralPerfil() 

    /**
     * Returns the value of field 'dsSolicitacaoPendente'.
     * 
     * @return String
     * @return the value of field 'dsSolicitacaoPendente'.
     */
    public java.lang.String getDsSolicitacaoPendente()
    {
        return this._dsSolicitacaoPendente;
    } //-- java.lang.String getDsSolicitacaoPendente() 

    /**
     * Returns the value of field 'dsUserNameCliente'.
     * 
     * @return String
     * @return the value of field 'dsUserNameCliente'.
     */
    public java.lang.String getDsUserNameCliente()
    {
        return this._dsUserNameCliente;
    } //-- java.lang.String getDsUserNameCliente() 

    /**
     * Returns the value of field 'nrMaximoContagemRemessa'.
     * 
     * @return long
     * @return the value of field 'nrMaximoContagemRemessa'.
     */
    public long getNrMaximoContagemRemessa()
    {
        return this._nrMaximoContagemRemessa;
    } //-- long getNrMaximoContagemRemessa() 

    /**
     * Returns the value of field 'nrMaximoContagemRetorno'.
     * 
     * @return long
     * @return the value of field 'nrMaximoContagemRetorno'.
     */
    public long getNrMaximoContagemRetorno()
    {
        return this._nrMaximoContagemRetorno;
    } //-- long getNrMaximoContagemRetorno() 

    /**
     * Returns the value of field
     * 'percentualInconsistenciaRejeicaoRemessa'.
     * 
     * @return BigDecimal
     * @return the value of field
     * 'percentualInconsistenciaRejeicaoRemessa'.
     */
    public java.math.BigDecimal getPercentualInconsistenciaRejeicaoRemessa()
    {
        return this._percentualInconsistenciaRejeicaoRemessa;
    } //-- java.math.BigDecimal getPercentualInconsistenciaRejeicaoRemessa() 

    /**
     * Returns the value of field 'qtMaxIncotRemessa'.
     * 
     * @return int
     * @return the value of field 'qtMaxIncotRemessa'.
     */
    public int getQtMaxIncotRemessa()
    {
        return this._qtMaxIncotRemessa;
    } //-- int getQtMaxIncotRemessa() 

    /**
     * Returns the value of field 'qtMesRegistroTrafg'.
     * 
     * @return long
     * @return the value of field 'qtMesRegistroTrafg'.
     */
    public long getQtMesRegistroTrafg()
    {
        return this._qtMesRegistroTrafg;
    } //-- long getQtMesRegistroTrafg() 

    /**
     * Method hasCdApliFormat
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdApliFormat()
    {
        return this._has_cdApliFormat;
    } //-- boolean hasCdApliFormat() 

    /**
     * Method hasCdAreaFone
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFone()
    {
        return this._has_cdAreaFone;
    } //-- boolean hasCdAreaFone() 

    /**
     * Method hasCdAreaFonePend
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFonePend()
    {
        return this._has_cdAreaFonePend;
    } //-- boolean hasCdAreaFonePend() 

    /**
     * Method hasCdControleNumeroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleNumeroRemessa()
    {
        return this._has_cdControleNumeroRemessa;
    } //-- boolean hasCdControleNumeroRemessa() 

    /**
     * Method hasCdControleNumeroRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleNumeroRetorno()
    {
        return this._has_cdControleNumeroRetorno;
    } //-- boolean hasCdControleNumeroRetorno() 

    /**
     * Method hasCdEmpresaResponsavel
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaResponsavel()
    {
        return this._has_cdEmpresaResponsavel;
    } //-- boolean hasCdEmpresaResponsavel() 

    /**
     * Method hasCdFoneContatoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFoneContatoCliente()
    {
        return this._has_cdFoneContatoCliente;
    } //-- boolean hasCdFoneContatoCliente() 

    /**
     * Method hasCdFoneSolicitacaoPend
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFoneSolicitacaoPend()
    {
        return this._has_cdFoneSolicitacaoPend;
    } //-- boolean hasCdFoneSolicitacaoPend() 

    /**
     * Method hasCdIndicadorAssociacaoLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAssociacaoLayout()
    {
        return this._has_cdIndicadorAssociacaoLayout;
    } //-- boolean hasCdIndicadorAssociacaoLayout() 

    /**
     * Method hasCdIndicadorConsisteContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorConsisteContaDebito()
    {
        return this._has_cdIndicadorConsisteContaDebito;
    } //-- boolean hasCdIndicadorConsisteContaDebito() 

    /**
     * Method hasCdIndicadorContaComplementar
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorContaComplementar()
    {
        return this._has_cdIndicadorContaComplementar;
    } //-- boolean hasCdIndicadorContaComplementar() 

    /**
     * Method hasCdIndicadorCpfLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCpfLayout()
    {
        return this._has_cdIndicadorCpfLayout;
    } //-- boolean hasCdIndicadorCpfLayout() 

    /**
     * Method hasCdIndicadorGeracaoSegmentoB
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorGeracaoSegmentoB()
    {
        return this._has_cdIndicadorGeracaoSegmentoB;
    } //-- boolean hasCdIndicadorGeracaoSegmentoB() 

    /**
     * Method hasCdIndicadorGeracaoSegmentoZ
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorGeracaoSegmentoZ()
    {
        return this._has_cdIndicadorGeracaoSegmentoZ;
    } //-- boolean hasCdIndicadorGeracaoSegmentoZ() 

    /**
     * Method hasCdMeioAlternativoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternativoRemessa()
    {
        return this._has_cdMeioAlternativoRemessa;
    } //-- boolean hasCdMeioAlternativoRemessa() 

    /**
     * Method hasCdMeioAlternativoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternativoRetorno()
    {
        return this._has_cdMeioAlternativoRetorno;
    } //-- boolean hasCdMeioAlternativoRetorno() 

    /**
     * Method hasCdMeioPrincipalRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRemessa()
    {
        return this._has_cdMeioPrincipalRemessa;
    } //-- boolean hasCdMeioPrincipalRemessa() 

    /**
     * Method hasCdMeioPrincipalRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRetorno()
    {
        return this._has_cdMeioPrincipalRetorno;
    } //-- boolean hasCdMeioPrincipalRetorno() 

    /**
     * Method hasCdNivelControleRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelControleRemessa()
    {
        return this._has_cdNivelControleRemessa;
    } //-- boolean hasCdNivelControleRemessa() 

    /**
     * Method hasCdNivelControleRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelControleRetorno()
    {
        return this._has_cdNivelControleRetorno;
    } //-- boolean hasCdNivelControleRetorno() 

    /**
     * Method hasCdPerdcContagemRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcContagemRetorno()
    {
        return this._has_cdPerdcContagemRetorno;
    } //-- boolean hasCdPerdcContagemRetorno() 

    /**
     * Method hasCdPerfilTrocaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArquivo()
    {
        return this._has_cdPerfilTrocaArquivo;
    } //-- boolean hasCdPerfilTrocaArquivo() 

    /**
     * Method hasCdPeriodicidadeContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeContagemRemessa()
    {
        return this._has_cdPeriodicidadeContagemRemessa;
    } //-- boolean hasCdPeriodicidadeContagemRemessa() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaParceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaParceiro()
    {
        return this._has_cdPessoaJuridicaParceiro;
    } //-- boolean hasCdPessoaJuridicaParceiro() 

    /**
     * Method hasCdRejeicaoAcltoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAcltoRemessa()
    {
        return this._has_cdRejeicaoAcltoRemessa;
    } //-- boolean hasCdRejeicaoAcltoRemessa() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNrMaximoContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMaximoContagemRemessa()
    {
        return this._has_nrMaximoContagemRemessa;
    } //-- boolean hasNrMaximoContagemRemessa() 

    /**
     * Method hasNrMaximoContagemRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMaximoContagemRetorno()
    {
        return this._has_nrMaximoContagemRetorno;
    } //-- boolean hasNrMaximoContagemRetorno() 

    /**
     * Method hasQtMaxIncotRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMaxIncotRemessa()
    {
        return this._has_qtMaxIncotRemessa;
    } //-- boolean hasQtMaxIncotRemessa() 

    /**
     * Method hasQtMesRegistroTrafg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesRegistroTrafg()
    {
        return this._has_qtMesRegistroTrafg;
    } //-- boolean hasQtMesRegistroTrafg() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdApliFormat'.
     * 
     * @param cdApliFormat the value of field 'cdApliFormat'.
     */
    public void setCdApliFormat(long cdApliFormat)
    {
        this._cdApliFormat = cdApliFormat;
        this._has_cdApliFormat = true;
    } //-- void setCdApliFormat(long) 

    /**
     * Sets the value of field 'cdAreaFone'.
     * 
     * @param cdAreaFone the value of field 'cdAreaFone'.
     */
    public void setCdAreaFone(int cdAreaFone)
    {
        this._cdAreaFone = cdAreaFone;
        this._has_cdAreaFone = true;
    } //-- void setCdAreaFone(int) 

    /**
     * Sets the value of field 'cdAreaFonePend'.
     * 
     * @param cdAreaFonePend the value of field 'cdAreaFonePend'.
     */
    public void setCdAreaFonePend(int cdAreaFonePend)
    {
        this._cdAreaFonePend = cdAreaFonePend;
        this._has_cdAreaFonePend = true;
    } //-- void setCdAreaFonePend(int) 

    /**
     * Sets the value of field 'cdControleNumeroRemessa'.
     * 
     * @param cdControleNumeroRemessa the value of field
     * 'cdControleNumeroRemessa'.
     */
    public void setCdControleNumeroRemessa(int cdControleNumeroRemessa)
    {
        this._cdControleNumeroRemessa = cdControleNumeroRemessa;
        this._has_cdControleNumeroRemessa = true;
    } //-- void setCdControleNumeroRemessa(int) 

    /**
     * Sets the value of field 'cdControleNumeroRetorno'.
     * 
     * @param cdControleNumeroRetorno the value of field
     * 'cdControleNumeroRetorno'.
     */
    public void setCdControleNumeroRetorno(int cdControleNumeroRetorno)
    {
        this._cdControleNumeroRetorno = cdControleNumeroRetorno;
        this._has_cdControleNumeroRetorno = true;
    } //-- void setCdControleNumeroRetorno(int) 

    /**
     * Sets the value of field 'cdCustoTransmicao'.
     * 
     * @param cdCustoTransmicao the value of field
     * 'cdCustoTransmicao'.
     */
    public void setCdCustoTransmicao(java.math.BigDecimal cdCustoTransmicao)
    {
        this._cdCustoTransmicao = cdCustoTransmicao;
    } //-- void setCdCustoTransmicao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdEmpresaResponsavel'.
     * 
     * @param cdEmpresaResponsavel the value of field
     * 'cdEmpresaResponsavel'.
     */
    public void setCdEmpresaResponsavel(int cdEmpresaResponsavel)
    {
        this._cdEmpresaResponsavel = cdEmpresaResponsavel;
        this._has_cdEmpresaResponsavel = true;
    } //-- void setCdEmpresaResponsavel(int) 

    /**
     * Sets the value of field 'cdFoneContatoCliente'.
     * 
     * @param cdFoneContatoCliente the value of field
     * 'cdFoneContatoCliente'.
     */
    public void setCdFoneContatoCliente(long cdFoneContatoCliente)
    {
        this._cdFoneContatoCliente = cdFoneContatoCliente;
        this._has_cdFoneContatoCliente = true;
    } //-- void setCdFoneContatoCliente(long) 

    /**
     * Sets the value of field 'cdFoneSolicitacaoPend'.
     * 
     * @param cdFoneSolicitacaoPend the value of field
     * 'cdFoneSolicitacaoPend'.
     */
    public void setCdFoneSolicitacaoPend(long cdFoneSolicitacaoPend)
    {
        this._cdFoneSolicitacaoPend = cdFoneSolicitacaoPend;
        this._has_cdFoneSolicitacaoPend = true;
    } //-- void setCdFoneSolicitacaoPend(long) 

    /**
     * Sets the value of field 'cdIndicadorAssociacaoLayout'.
     * 
     * @param cdIndicadorAssociacaoLayout the value of field
     * 'cdIndicadorAssociacaoLayout'.
     */
    public void setCdIndicadorAssociacaoLayout(int cdIndicadorAssociacaoLayout)
    {
        this._cdIndicadorAssociacaoLayout = cdIndicadorAssociacaoLayout;
        this._has_cdIndicadorAssociacaoLayout = true;
    } //-- void setCdIndicadorAssociacaoLayout(int) 

    /**
     * Sets the value of field 'cdIndicadorConsisteContaDebito'.
     * 
     * @param cdIndicadorConsisteContaDebito the value of field
     * 'cdIndicadorConsisteContaDebito'.
     */
    public void setCdIndicadorConsisteContaDebito(int cdIndicadorConsisteContaDebito)
    {
        this._cdIndicadorConsisteContaDebito = cdIndicadorConsisteContaDebito;
        this._has_cdIndicadorConsisteContaDebito = true;
    } //-- void setCdIndicadorConsisteContaDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorContaComplementar'.
     * 
     * @param cdIndicadorContaComplementar the value of field
     * 'cdIndicadorContaComplementar'.
     */
    public void setCdIndicadorContaComplementar(int cdIndicadorContaComplementar)
    {
        this._cdIndicadorContaComplementar = cdIndicadorContaComplementar;
        this._has_cdIndicadorContaComplementar = true;
    } //-- void setCdIndicadorContaComplementar(int) 

    /**
     * Sets the value of field 'cdIndicadorCpfLayout'.
     * 
     * @param cdIndicadorCpfLayout the value of field
     * 'cdIndicadorCpfLayout'.
     */
    public void setCdIndicadorCpfLayout(int cdIndicadorCpfLayout)
    {
        this._cdIndicadorCpfLayout = cdIndicadorCpfLayout;
        this._has_cdIndicadorCpfLayout = true;
    } //-- void setCdIndicadorCpfLayout(int) 

    /**
     * Sets the value of field 'cdIndicadorGeracaoSegmentoB'.
     * 
     * @param cdIndicadorGeracaoSegmentoB the value of field
     * 'cdIndicadorGeracaoSegmentoB'.
     */
    public void setCdIndicadorGeracaoSegmentoB(int cdIndicadorGeracaoSegmentoB)
    {
        this._cdIndicadorGeracaoSegmentoB = cdIndicadorGeracaoSegmentoB;
        this._has_cdIndicadorGeracaoSegmentoB = true;
    } //-- void setCdIndicadorGeracaoSegmentoB(int) 

    /**
     * Sets the value of field 'cdIndicadorGeracaoSegmentoZ'.
     * 
     * @param cdIndicadorGeracaoSegmentoZ the value of field
     * 'cdIndicadorGeracaoSegmentoZ'.
     */
    public void setCdIndicadorGeracaoSegmentoZ(int cdIndicadorGeracaoSegmentoZ)
    {
        this._cdIndicadorGeracaoSegmentoZ = cdIndicadorGeracaoSegmentoZ;
        this._has_cdIndicadorGeracaoSegmentoZ = true;
    } //-- void setCdIndicadorGeracaoSegmentoZ(int) 

    /**
     * Sets the value of field 'cdMeioAlternativoRemessa'.
     * 
     * @param cdMeioAlternativoRemessa the value of field
     * 'cdMeioAlternativoRemessa'.
     */
    public void setCdMeioAlternativoRemessa(int cdMeioAlternativoRemessa)
    {
        this._cdMeioAlternativoRemessa = cdMeioAlternativoRemessa;
        this._has_cdMeioAlternativoRemessa = true;
    } //-- void setCdMeioAlternativoRemessa(int) 

    /**
     * Sets the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @param cdMeioAlternativoRetorno the value of field
     * 'cdMeioAlternativoRetorno'.
     */
    public void setCdMeioAlternativoRetorno(int cdMeioAlternativoRetorno)
    {
        this._cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
        this._has_cdMeioAlternativoRetorno = true;
    } //-- void setCdMeioAlternativoRetorno(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @param cdMeioPrincipalRemessa the value of field
     * 'cdMeioPrincipalRemessa'.
     */
    public void setCdMeioPrincipalRemessa(int cdMeioPrincipalRemessa)
    {
        this._cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
        this._has_cdMeioPrincipalRemessa = true;
    } //-- void setCdMeioPrincipalRemessa(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @param cdMeioPrincipalRetorno the value of field
     * 'cdMeioPrincipalRetorno'.
     */
    public void setCdMeioPrincipalRetorno(int cdMeioPrincipalRetorno)
    {
        this._cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
        this._has_cdMeioPrincipalRetorno = true;
    } //-- void setCdMeioPrincipalRetorno(int) 

    /**
     * Sets the value of field 'cdNivelControleRemessa'.
     * 
     * @param cdNivelControleRemessa the value of field
     * 'cdNivelControleRemessa'.
     */
    public void setCdNivelControleRemessa(int cdNivelControleRemessa)
    {
        this._cdNivelControleRemessa = cdNivelControleRemessa;
        this._has_cdNivelControleRemessa = true;
    } //-- void setCdNivelControleRemessa(int) 

    /**
     * Sets the value of field 'cdNivelControleRetorno'.
     * 
     * @param cdNivelControleRetorno the value of field
     * 'cdNivelControleRetorno'.
     */
    public void setCdNivelControleRetorno(int cdNivelControleRetorno)
    {
        this._cdNivelControleRetorno = cdNivelControleRetorno;
        this._has_cdNivelControleRetorno = true;
    } //-- void setCdNivelControleRetorno(int) 

    /**
     * Sets the value of field 'cdPerdcContagemRetorno'.
     * 
     * @param cdPerdcContagemRetorno the value of field
     * 'cdPerdcContagemRetorno'.
     */
    public void setCdPerdcContagemRetorno(int cdPerdcContagemRetorno)
    {
        this._cdPerdcContagemRetorno = cdPerdcContagemRetorno;
        this._has_cdPerdcContagemRetorno = true;
    } //-- void setCdPerdcContagemRetorno(int) 

    /**
     * Sets the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @param cdPerfilTrocaArquivo the value of field
     * 'cdPerfilTrocaArquivo'.
     */
    public void setCdPerfilTrocaArquivo(long cdPerfilTrocaArquivo)
    {
        this._cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
        this._has_cdPerfilTrocaArquivo = true;
    } //-- void setCdPerfilTrocaArquivo(long) 

    /**
     * Sets the value of field 'cdPeriodicidadeContagemRemessa'.
     * 
     * @param cdPeriodicidadeContagemRemessa the value of field
     * 'cdPeriodicidadeContagemRemessa'.
     */
    public void setCdPeriodicidadeContagemRemessa(int cdPeriodicidadeContagemRemessa)
    {
        this._cdPeriodicidadeContagemRemessa = cdPeriodicidadeContagemRemessa;
        this._has_cdPeriodicidadeContagemRemessa = true;
    } //-- void setCdPeriodicidadeContagemRemessa(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @param cdPessoaJuridicaParceiro the value of field
     * 'cdPessoaJuridicaParceiro'.
     */
    public void setCdPessoaJuridicaParceiro(long cdPessoaJuridicaParceiro)
    {
        this._cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
        this._has_cdPessoaJuridicaParceiro = true;
    } //-- void setCdPessoaJuridicaParceiro(long) 

    /**
     * Sets the value of field 'cdRamalContatoCliente'.
     * 
     * @param cdRamalContatoCliente the value of field
     * 'cdRamalContatoCliente'.
     */
    public void setCdRamalContatoCliente(java.lang.String cdRamalContatoCliente)
    {
        this._cdRamalContatoCliente = cdRamalContatoCliente;
    } //-- void setCdRamalContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdRamalSolctPend'.
     * 
     * @param cdRamalSolctPend the value of field 'cdRamalSolctPend'
     */
    public void setCdRamalSolctPend(java.lang.String cdRamalSolctPend)
    {
        this._cdRamalSolctPend = cdRamalSolctPend;
    } //-- void setCdRamalSolctPend(java.lang.String) 

    /**
     * Sets the value of field 'cdRejeicaoAcltoRemessa'.
     * 
     * @param cdRejeicaoAcltoRemessa the value of field
     * 'cdRejeicaoAcltoRemessa'.
     */
    public void setCdRejeicaoAcltoRemessa(int cdRejeicaoAcltoRemessa)
    {
        this._cdRejeicaoAcltoRemessa = cdRejeicaoAcltoRemessa;
        this._has_cdRejeicaoAcltoRemessa = true;
    } //-- void setCdRejeicaoAcltoRemessa(int) 

    /**
     * Sets the value of field 'cdSerieAplicacaoTransmissao'.
     * 
     * @param cdSerieAplicacaoTransmissao the value of field
     * 'cdSerieAplicacaoTransmissao'.
     */
    public void setCdSerieAplicacaoTransmissao(java.lang.String cdSerieAplicacaoTransmissao)
    {
        this._cdSerieAplicacaoTransmissao = cdSerieAplicacaoTransmissao;
    } //-- void setCdSerieAplicacaoTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'cdSistemaOrigemArquivo'.
     * 
     * @param cdSistemaOrigemArquivo the value of field
     * 'cdSistemaOrigemArquivo'.
     */
    public void setCdSistemaOrigemArquivo(java.lang.String cdSistemaOrigemArquivo)
    {
        this._cdSistemaOrigemArquivo = cdSistemaOrigemArquivo;
    } //-- void setCdSistemaOrigemArquivo(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'dsEmailContatoCliente'.
     * 
     * @param dsEmailContatoCliente the value of field
     * 'dsEmailContatoCliente'.
     */
    public void setDsEmailContatoCliente(java.lang.String dsEmailContatoCliente)
    {
        this._dsEmailContatoCliente = dsEmailContatoCliente;
    } //-- void setDsEmailContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailSolctPend'.
     * 
     * @param dsEmailSolctPend the value of field 'dsEmailSolctPend'
     */
    public void setDsEmailSolctPend(java.lang.String dsEmailSolctPend)
    {
        this._dsEmailSolctPend = dsEmailSolctPend;
    } //-- void setDsEmailSolctPend(java.lang.String) 

    /**
     * Sets the value of field 'dsNameArquivoRetorno'.
     * 
     * @param dsNameArquivoRetorno the value of field
     * 'dsNameArquivoRetorno'.
     */
    public void setDsNameArquivoRetorno(java.lang.String dsNameArquivoRetorno)
    {
        this._dsNameArquivoRetorno = dsNameArquivoRetorno;
    } //-- void setDsNameArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRemessa'.
     * 
     * @param dsNomeArquivoRemessa the value of field
     * 'dsNomeArquivoRemessa'.
     */
    public void setDsNomeArquivoRemessa(java.lang.String dsNomeArquivoRemessa)
    {
        this._dsNomeArquivoRemessa = dsNomeArquivoRemessa;
    } //-- void setDsNomeArquivoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeContatoCliente'.
     * 
     * @param dsNomeContatoCliente the value of field
     * 'dsNomeContatoCliente'.
     */
    public void setDsNomeContatoCliente(java.lang.String dsNomeContatoCliente)
    {
        this._dsNomeContatoCliente = dsNomeContatoCliente;
    } //-- void setDsNomeContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsObsGeralPerfil'.
     * 
     * @param dsObsGeralPerfil the value of field 'dsObsGeralPerfil'
     */
    public void setDsObsGeralPerfil(java.lang.String dsObsGeralPerfil)
    {
        this._dsObsGeralPerfil = dsObsGeralPerfil;
    } //-- void setDsObsGeralPerfil(java.lang.String) 

    /**
     * Sets the value of field 'dsSolicitacaoPendente'.
     * 
     * @param dsSolicitacaoPendente the value of field
     * 'dsSolicitacaoPendente'.
     */
    public void setDsSolicitacaoPendente(java.lang.String dsSolicitacaoPendente)
    {
        this._dsSolicitacaoPendente = dsSolicitacaoPendente;
    } //-- void setDsSolicitacaoPendente(java.lang.String) 

    /**
     * Sets the value of field 'dsUserNameCliente'.
     * 
     * @param dsUserNameCliente the value of field
     * 'dsUserNameCliente'.
     */
    public void setDsUserNameCliente(java.lang.String dsUserNameCliente)
    {
        this._dsUserNameCliente = dsUserNameCliente;
    } //-- void setDsUserNameCliente(java.lang.String) 

    /**
     * Sets the value of field 'nrMaximoContagemRemessa'.
     * 
     * @param nrMaximoContagemRemessa the value of field
     * 'nrMaximoContagemRemessa'.
     */
    public void setNrMaximoContagemRemessa(long nrMaximoContagemRemessa)
    {
        this._nrMaximoContagemRemessa = nrMaximoContagemRemessa;
        this._has_nrMaximoContagemRemessa = true;
    } //-- void setNrMaximoContagemRemessa(long) 

    /**
     * Sets the value of field 'nrMaximoContagemRetorno'.
     * 
     * @param nrMaximoContagemRetorno the value of field
     * 'nrMaximoContagemRetorno'.
     */
    public void setNrMaximoContagemRetorno(long nrMaximoContagemRetorno)
    {
        this._nrMaximoContagemRetorno = nrMaximoContagemRetorno;
        this._has_nrMaximoContagemRetorno = true;
    } //-- void setNrMaximoContagemRetorno(long) 

    /**
     * Sets the value of field
     * 'percentualInconsistenciaRejeicaoRemessa'.
     * 
     * @param percentualInconsistenciaRejeicaoRemessa the value of
     * field 'percentualInconsistenciaRejeicaoRemessa'.
     */
    public void setPercentualInconsistenciaRejeicaoRemessa(java.math.BigDecimal percentualInconsistenciaRejeicaoRemessa)
    {
        this._percentualInconsistenciaRejeicaoRemessa = percentualInconsistenciaRejeicaoRemessa;
    } //-- void setPercentualInconsistenciaRejeicaoRemessa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'qtMaxIncotRemessa'.
     * 
     * @param qtMaxIncotRemessa the value of field
     * 'qtMaxIncotRemessa'.
     */
    public void setQtMaxIncotRemessa(int qtMaxIncotRemessa)
    {
        this._qtMaxIncotRemessa = qtMaxIncotRemessa;
        this._has_qtMaxIncotRemessa = true;
    } //-- void setQtMaxIncotRemessa(int) 

    /**
     * Sets the value of field 'qtMesRegistroTrafg'.
     * 
     * @param qtMesRegistroTrafg the value of field
     * 'qtMesRegistroTrafg'.
     */
    public void setQtMesRegistroTrafg(long qtMesRegistroTrafg)
    {
        this._qtMesRegistroTrafg = qtMesRegistroTrafg;
        this._has_qtMesRegistroTrafg = true;
    } //-- void setQtMesRegistroTrafg(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarPerfilTrocaArquivoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.request.AlterarPerfilTrocaArquivoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.request.AlterarPerfilTrocaArquivoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.request.AlterarPerfilTrocaArquivoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.request.AlterarPerfilTrocaArquivoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
