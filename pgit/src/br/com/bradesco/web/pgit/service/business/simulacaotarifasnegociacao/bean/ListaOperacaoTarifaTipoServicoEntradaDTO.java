/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

/**
 * Nome: ListaOperacaoTarifaTipoServicoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListaOperacaoTarifaTipoServicoEntradaDTO {

	/** Atributo cdFluxoNegocio. */
	private Integer cdFluxoNegocio;
	
	/** Atributo cdGrupoInfoFluxo. */
	private Integer cdGrupoInfoFluxo;
	
	/** Atributo cdProdutoOperacaoDeflt. */
	private Integer cdProdutoOperacaoDeflt;
	
	/** Atributo cdOperacaoProdutoServico. */
	private Integer cdOperacaoProdutoServico;
	
	/** Atributo cdTipoCanal. */
	private Integer cdTipoCanal;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo nrOcorGrupoProposta. */
	private Integer nrOcorGrupoProposta;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;

	/** Atributo cdNegocioRenegocio. */
	private Integer cdNegocioRenegocio;
	
	/** Atributo cdPessoaContrato. */
	private Long cdPessoaContrato;
	
	/** Atributo cdPessoaJuridicaProposta. */
	private Long cdPessoaJuridicaProposta;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo cdTipoContratoProposta. */
	private Integer cdTipoContratoProposta;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;

	/**
	 * Get: cdFluxoNegocio.
	 *
	 * @return cdFluxoNegocio
	 */
	public Integer getCdFluxoNegocio() {
		return cdFluxoNegocio;
	}

	/**
	 * Set: cdFluxoNegocio.
	 *
	 * @param cdFluxoNegocio the cd fluxo negocio
	 */
	public void setCdFluxoNegocio(Integer cdFluxoNegocio) {
		this.cdFluxoNegocio = cdFluxoNegocio;
	}

	/**
	 * Get: cdGrupoInfoFluxo.
	 *
	 * @return cdGrupoInfoFluxo
	 */
	public Integer getCdGrupoInfoFluxo() {
		return cdGrupoInfoFluxo;
	}

	/**
	 * Set: cdGrupoInfoFluxo.
	 *
	 * @param cdGrupoInfoFluxo the cd grupo info fluxo
	 */
	public void setCdGrupoInfoFluxo(Integer cdGrupoInfoFluxo) {
		this.cdGrupoInfoFluxo = cdGrupoInfoFluxo;
	}

	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public Integer getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}

	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdProdutoOperacaoDeflt.
	 *
	 * @return cdProdutoOperacaoDeflt
	 */
	public Integer getCdProdutoOperacaoDeflt() {
		return cdProdutoOperacaoDeflt;
	}

	/**
	 * Set: cdProdutoOperacaoDeflt.
	 *
	 * @param cdProdutoOperacaoDeflt the cd produto operacao deflt
	 */
	public void setCdProdutoOperacaoDeflt(Integer cdProdutoOperacaoDeflt) {
		this.cdProdutoOperacaoDeflt = cdProdutoOperacaoDeflt;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(
			Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}

	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrOcorGrupoProposta.
	 *
	 * @return nrOcorGrupoProposta
	 */
	public Integer getNrOcorGrupoProposta() {
		return nrOcorGrupoProposta;
	}

	/**
	 * Set: nrOcorGrupoProposta.
	 *
	 * @param nrOcorGrupoProposta the nr ocor grupo proposta
	 */
	public void setNrOcorGrupoProposta(Integer nrOcorGrupoProposta) {
		this.nrOcorGrupoProposta = nrOcorGrupoProposta;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

	/**
	 * Get: cdNegocioRenegocio.
	 *
	 * @return cdNegocioRenegocio
	 */
	public Integer getCdNegocioRenegocio() {
		return cdNegocioRenegocio;
	}

	/**
	 * Set: cdNegocioRenegocio.
	 *
	 * @param cdNegocioRenegocio the cd negocio renegocio
	 */
	public void setCdNegocioRenegocio(Integer cdNegocioRenegocio) {
		this.cdNegocioRenegocio = cdNegocioRenegocio;
	}

	/**
	 * Get: cdPessoaContrato.
	 *
	 * @return cdPessoaContrato
	 */
	public Long getCdPessoaContrato() {
		return cdPessoaContrato;
	}

	/**
	 * Set: cdPessoaContrato.
	 *
	 * @param cdPessoaContrato the cd pessoa contrato
	 */
	public void setCdPessoaContrato(Long cdPessoaContrato) {
		this.cdPessoaContrato = cdPessoaContrato;
	}

	/**
	 * Get: cdPessoaJuridicaProposta.
	 *
	 * @return cdPessoaJuridicaProposta
	 */
	public Long getCdPessoaJuridicaProposta() {
		return cdPessoaJuridicaProposta;
	}

	/**
	 * Set: cdPessoaJuridicaProposta.
	 *
	 * @param cdPessoaJuridicaProposta the cd pessoa juridica proposta
	 */
	public void setCdPessoaJuridicaProposta(Long cdPessoaJuridicaProposta) {
		this.cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: cdTipoContratoProposta.
	 *
	 * @return cdTipoContratoProposta
	 */
	public Integer getCdTipoContratoProposta() {
		return cdTipoContratoProposta;
	}

	/**
	 * Set: cdTipoContratoProposta.
	 *
	 * @param cdTipoContratoProposta the cd tipo contrato proposta
	 */
	public void setCdTipoContratoProposta(Integer cdTipoContratoProposta) {
		this.cdTipoContratoProposta = cdTipoContratoProposta;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

}
