/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ConsultarEnderecoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarEnderecoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdCpfCnpjRecebedor. */
	private Long cdCpfCnpjRecebedor;
	
	/** Atributo cdFilialCnpjRecebedor. */
	private Integer cdFilialCnpjRecebedor;
	
	/** Atributo cdControleCpfRecebedor. */
	private Integer cdControleCpfRecebedor;
	
	/** Atributo dsNomeRazao. */
	private String dsNomeRazao;
	
	/** Atributo dsLogradouroPagador. */
	private String dsLogradouroPagador;
	
	/** Atributo dsNumeroLogradouroPagador. */
	private String dsNumeroLogradouroPagador;
	
	/** Atributo dsComplementoLogradouroPagador. */
	private String dsComplementoLogradouroPagador;
	
	/** Atributo dsBairroClientePagador. */
	private String dsBairroClientePagador;
	
	/** Atributo dsMunicipioClientePagador. */
	private String dsMunicipioClientePagador;
	
	/** Atributo cdSiglaUfPagador. */
	private String cdSiglaUfPagador;
	
	/** Atributo dsPais. */
	private String dsPais;
	
	/** Atributo cdCepPagador. */
	private Integer cdCepPagador;
	
	/** Atributo cdCepComplementoPagador. */
	private Integer cdCepComplementoPagador;
	
	/** Atributo dsCepComplementoPagador. */
	private String dsCepComplementoPagador;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas(){
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas){
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdCpfCnpjRecebedor.
	 *
	 * @return cdCpfCnpjRecebedor
	 */
	public Long getCdCpfCnpjRecebedor(){
		return cdCpfCnpjRecebedor;
	}

	/**
	 * Set: cdCpfCnpjRecebedor.
	 *
	 * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
	 */
	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor){
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}

	/**
	 * Get: cdFilialCnpjRecebedor.
	 *
	 * @return cdFilialCnpjRecebedor
	 */
	public Integer getCdFilialCnpjRecebedor(){
		return cdFilialCnpjRecebedor;
	}

	/**
	 * Set: cdFilialCnpjRecebedor.
	 *
	 * @param cdFilialCnpjRecebedor the cd filial cnpj recebedor
	 */
	public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor){
		this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
	}

	/**
	 * Get: cdControleCpfRecebedor.
	 *
	 * @return cdControleCpfRecebedor
	 */
	public Integer getCdControleCpfRecebedor(){
		return cdControleCpfRecebedor;
	}

	/**
	 * Set: cdControleCpfRecebedor.
	 *
	 * @param cdControleCpfRecebedor the cd controle cpf recebedor
	 */
	public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor){
		this.cdControleCpfRecebedor = cdControleCpfRecebedor;
	}

	/**
	 * Get: dsNomeRazao.
	 *
	 * @return dsNomeRazao
	 */
	public String getDsNomeRazao(){
		return dsNomeRazao;
	}

	/**
	 * Set: dsNomeRazao.
	 *
	 * @param dsNomeRazao the ds nome razao
	 */
	public void setDsNomeRazao(String dsNomeRazao){
		this.dsNomeRazao = dsNomeRazao;
	}

	/**
	 * Get: dsLogradouroPagador.
	 *
	 * @return dsLogradouroPagador
	 */
	public String getDsLogradouroPagador(){
		return dsLogradouroPagador;
	}

	/**
	 * Set: dsLogradouroPagador.
	 *
	 * @param dsLogradouroPagador the ds logradouro pagador
	 */
	public void setDsLogradouroPagador(String dsLogradouroPagador){
		this.dsLogradouroPagador = dsLogradouroPagador;
	}

	/**
	 * Get: dsNumeroLogradouroPagador.
	 *
	 * @return dsNumeroLogradouroPagador
	 */
	public String getDsNumeroLogradouroPagador(){
		return dsNumeroLogradouroPagador;
	}

	/**
	 * Set: dsNumeroLogradouroPagador.
	 *
	 * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
	 */
	public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador){
		this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
	}

	/**
	 * Get: dsComplementoLogradouroPagador.
	 *
	 * @return dsComplementoLogradouroPagador
	 */
	public String getDsComplementoLogradouroPagador(){
		return dsComplementoLogradouroPagador;
	}

	/**
	 * Set: dsComplementoLogradouroPagador.
	 *
	 * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
	 */
	public void setDsComplementoLogradouroPagador(String dsComplementoLogradouroPagador){
		this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
	}

	/**
	 * Get: dsBairroClientePagador.
	 *
	 * @return dsBairroClientePagador
	 */
	public String getDsBairroClientePagador(){
		return dsBairroClientePagador;
	}

	/**
	 * Set: dsBairroClientePagador.
	 *
	 * @param dsBairroClientePagador the ds bairro cliente pagador
	 */
	public void setDsBairroClientePagador(String dsBairroClientePagador){
		this.dsBairroClientePagador = dsBairroClientePagador;
	}

	/**
	 * Get: dsMunicipioClientePagador.
	 *
	 * @return dsMunicipioClientePagador
	 */
	public String getDsMunicipioClientePagador(){
		return dsMunicipioClientePagador;
	}

	/**
	 * Set: dsMunicipioClientePagador.
	 *
	 * @param dsMunicipioClientePagador the ds municipio cliente pagador
	 */
	public void setDsMunicipioClientePagador(String dsMunicipioClientePagador){
		this.dsMunicipioClientePagador = dsMunicipioClientePagador;
	}

	/**
	 * Get: cdSiglaUfPagador.
	 *
	 * @return cdSiglaUfPagador
	 */
	public String getCdSiglaUfPagador(){
		return cdSiglaUfPagador;
	}

	/**
	 * Set: cdSiglaUfPagador.
	 *
	 * @param cdSiglaUfPagador the cd sigla uf pagador
	 */
	public void setCdSiglaUfPagador(String cdSiglaUfPagador){
		this.cdSiglaUfPagador = cdSiglaUfPagador;
	}

	/**
	 * Get: dsPais.
	 *
	 * @return dsPais
	 */
	public String getDsPais(){
		return dsPais;
	}

	/**
	 * Set: dsPais.
	 *
	 * @param dsPais the ds pais
	 */
	public void setDsPais(String dsPais){
		this.dsPais = dsPais;
	}

	/**
	 * Get: cdCepPagador.
	 *
	 * @return cdCepPagador
	 */
	public Integer getCdCepPagador(){
		if(cdCepPagador == null){
			return null;
		}
		return cdCepPagador;
	}

	/**
	 * Set: cdCepPagador.
	 *
	 * @param cdCepPagador the cd cep pagador
	 */
	public void setCdCepPagador(Integer cdCepPagador){
		this.cdCepPagador = cdCepPagador;
	}

	/**
	 * Get: cdCepComplementoPagador.
	 *
	 * @return cdCepComplementoPagador
	 */
	public Integer getCdCepComplementoPagador(){
		if(cdCepComplementoPagador == null){
			return null;
		}		
		return cdCepComplementoPagador;
	}

	/**
	 * Set: cdCepComplementoPagador.
	 *
	 * @param cdCepComplementoPagador the cd cep complemento pagador
	 */
	public void setCdCepComplementoPagador(Integer cdCepComplementoPagador){
		this.cdCepComplementoPagador = cdCepComplementoPagador;
	}

	/**
	 * Get: cepFormatado.
	 *
	 * @return cepFormatado
	 */
	public String getCepFormatado() {
		
		if(dsCepComplementoPagador != null){
		setDsCepComplementoPagador(PgitUtil.preencherZerosAEsquerda(dsCepComplementoPagador, 3));
			return cdCepPagador + "-" + dsCepComplementoPagador;
		}else{
			return "";
		}
	}

	/**
	 * Get: dsCepComplementoPagador.
	 *
	 * @return dsCepComplementoPagador
	 */
	public String getDsCepComplementoPagador() {
		return dsCepComplementoPagador;
	}

	/**
	 * Set: dsCepComplementoPagador.
	 *
	 * @param dsCepComplementoPagador the ds cep complemento pagador
	 */
	public void setDsCepComplementoPagador(String dsCepComplementoPagador) {
		if(dsCepComplementoPagador != null && !dsCepComplementoPagador.equals("")){
			this.dsCepComplementoPagador = dsCepComplementoPagador;
			cdCepComplementoPagador = Integer.parseInt(dsCepComplementoPagador);
		}
		
	}
}