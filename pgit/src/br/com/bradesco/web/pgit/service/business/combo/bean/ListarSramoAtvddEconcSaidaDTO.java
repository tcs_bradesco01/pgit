/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarSramoAtvddEconcSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSramoAtvddEconcSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdSubRamoAtividade. */
    private Integer cdSubRamoAtividade;
    
    /** Atributo cdAtividadeEconomica. */
    private Integer cdAtividadeEconomica;
    
    /** Atributo dsAtividadeEconomica. */
    private String dsAtividadeEconomica;
    
    /** Atributo subRamoAtividadeFormatado. */
    private String subRamoAtividadeFormatado;

    /**
     * Get: subRamoAtividadeFormatado.
     *
     * @return subRamoAtividadeFormatado
     */
    public String getSubRamoAtividadeFormatado() {
        return subRamoAtividadeFormatado;
    }
    
    /**
     * Set: subRamoAtividadeFormatado.
     *
     * @param subRamoAtividadeFormatado the sub ramo atividade formatado
     */
    public void setSubRamoAtividadeFormatado(String subRamoAtividadeFormatado) {
        this.subRamoAtividadeFormatado = subRamoAtividadeFormatado;
    }
    
    /**
     * Get: cdAtividadeEconomica.
     *
     * @return cdAtividadeEconomica
     */
    public Integer getCdAtividadeEconomica() {
        return cdAtividadeEconomica;
    }
    
    /**
     * Set: cdAtividadeEconomica.
     *
     * @param cdAtividadeEconomica the cd atividade economica
     */
    public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
        this.cdAtividadeEconomica = cdAtividadeEconomica;
    }
    
    /**
     * Get: cdSubRamoAtividade.
     *
     * @return cdSubRamoAtividade
     */
    public Integer getCdSubRamoAtividade() {
        return cdSubRamoAtividade;
    }
    
    /**
     * Set: cdSubRamoAtividade.
     *
     * @param cdSubRamoAtividade the cd sub ramo atividade
     */
    public void setCdSubRamoAtividade(Integer cdSubRamoAtividade) {
        this.cdSubRamoAtividade = cdSubRamoAtividade;
    }
    
    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }
    
    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }
    
    /**
     * Get: dsAtividadeEconomica.
     *
     * @return dsAtividadeEconomica
     */
    public String getDsAtividadeEconomica() {
        return dsAtividadeEconomica;
    }
    
    /**
     * Set: dsAtividadeEconomica.
     *
     * @param dsAtividadeEconomica the ds atividade economica
     */
    public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
        this.dsAtividadeEconomica = dsAtividadeEconomica;
    }
    
    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }
    
    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    

}
