/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirContaContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirContaContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdPessoaJuridicaVinculo
     */
    private long _cdPessoaJuridicaVinculo = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaVinculo
     */
    private boolean _has_cdPessoaJuridicaVinculo;

    /**
     * Field _cdTipoContratoNegocioVinculo
     */
    private int _cdTipoContratoNegocioVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocioVinculo
     */
    private boolean _has_cdTipoContratoNegocioVinculo;

    /**
     * Field _nrSequenciaContratoNegocioVinculo
     */
    private long _nrSequenciaContratoNegocioVinculo = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaContratoNegocioVinculo
     */
    private boolean _has_nrSequenciaContratoNegocioVinculo;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirContaContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.request.IncluirContaContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaVinculo
     * 
     */
    public void deleteCdPessoaJuridicaVinculo()
    {
        this._has_cdPessoaJuridicaVinculo= false;
    } //-- void deleteCdPessoaJuridicaVinculo() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoNegocioVinculo
     * 
     */
    public void deleteCdTipoContratoNegocioVinculo()
    {
        this._has_cdTipoContratoNegocioVinculo= false;
    } //-- void deleteCdTipoContratoNegocioVinculo() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocioVinculo
     * 
     */
    public void deleteNrSequenciaContratoNegocioVinculo()
    {
        this._has_nrSequenciaContratoNegocioVinculo= false;
    } //-- void deleteNrSequenciaContratoNegocioVinculo() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaVinculo'.
     */
    public long getCdPessoaJuridicaVinculo()
    {
        return this._cdPessoaJuridicaVinculo;
    } //-- long getCdPessoaJuridicaVinculo() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoNegocioVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocioVinculo'.
     */
    public int getCdTipoContratoNegocioVinculo()
    {
        return this._cdTipoContratoNegocioVinculo;
    } //-- int getCdTipoContratoNegocioVinculo() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'nrSequenciaContratoNegocioVinculo'.
     * 
     * @return long
     * @return the value of field
     * 'nrSequenciaContratoNegocioVinculo'.
     */
    public long getNrSequenciaContratoNegocioVinculo()
    {
        return this._nrSequenciaContratoNegocioVinculo;
    } //-- long getNrSequenciaContratoNegocioVinculo() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaVinculo()
    {
        return this._has_cdPessoaJuridicaVinculo;
    } //-- boolean hasCdPessoaJuridicaVinculo() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoNegocioVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocioVinculo()
    {
        return this._has_cdTipoContratoNegocioVinculo;
    } //-- boolean hasCdTipoContratoNegocioVinculo() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocioVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocioVinculo()
    {
        return this._has_nrSequenciaContratoNegocioVinculo;
    } //-- boolean hasNrSequenciaContratoNegocioVinculo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @param cdPessoaJuridicaVinculo the value of field
     * 'cdPessoaJuridicaVinculo'.
     */
    public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo)
    {
        this._cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
        this._has_cdPessoaJuridicaVinculo = true;
    } //-- void setCdPessoaJuridicaVinculo(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocioVinculo'.
     * 
     * @param cdTipoContratoNegocioVinculo the value of field
     * 'cdTipoContratoNegocioVinculo'.
     */
    public void setCdTipoContratoNegocioVinculo(int cdTipoContratoNegocioVinculo)
    {
        this._cdTipoContratoNegocioVinculo = cdTipoContratoNegocioVinculo;
        this._has_cdTipoContratoNegocioVinculo = true;
    } //-- void setCdTipoContratoNegocioVinculo(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocioVinculo'.
     * 
     * @param nrSequenciaContratoNegocioVinculo the value of field
     * 'nrSequenciaContratoNegocioVinculo'.
     */
    public void setNrSequenciaContratoNegocioVinculo(long nrSequenciaContratoNegocioVinculo)
    {
        this._nrSequenciaContratoNegocioVinculo = nrSequenciaContratoNegocioVinculo;
        this._has_nrSequenciaContratoNegocioVinculo = true;
    } //-- void setNrSequenciaContratoNegocioVinculo(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirContaContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.request.IncluirContaContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.request.IncluirContaContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.request.IncluirContaContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.request.IncluirContaContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
