/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _corpoCpfPagador
     */
    private long _corpoCpfPagador = 0;

    /**
     * keeps track of state for field: _corpoCpfPagador
     */
    private boolean _has_corpoCpfPagador;

    /**
     * Field _filialCpfPagador
     */
    private int _filialCpfPagador = 0;

    /**
     * keeps track of state for field: _filialCpfPagador
     */
    private boolean _has_filialCpfPagador;

    /**
     * Field _controleCpfPagador
     */
    private int _controleCpfPagador = 0;

    /**
     * keeps track of state for field: _controleCpfPagador
     */
    private boolean _has_controleCpfPagador;

    /**
     * Field _dsClientePagador
     */
    private java.lang.String _dsClientePagador;

    /**
     * Field _cdAgenciaPagadora
     */
    private int _cdAgenciaPagadora = 0;

    /**
     * keeps track of state for field: _cdAgenciaPagadora
     */
    private boolean _has_cdAgenciaPagadora;

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _cdIdentificacaoInscricaoFavorecido
     */
    private int _cdIdentificacaoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificacaoInscricaoFavorecido
     */
    private boolean _has_cdIdentificacaoInscricaoFavorecido;

    /**
     * Field _dsIdentificacaoInscricaoFavorecido
     */
    private java.lang.String _dsIdentificacaoInscricaoFavorecido;

    /**
     * Field _dsAbrevFavorecido
     */
    private java.lang.String _dsAbrevFavorecido;

    /**
     * Field _dtCreditoPagamento
     */
    private java.lang.String _dtCreditoPagamento;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _vlPagamentoClientePagador
     */
    private java.math.BigDecimal _vlPagamentoClientePagador = new java.math.BigDecimal("0");

    /**
     * Field _dtDisponivelAte
     */
    private java.lang.String _dtDisponivelAte;

    /**
     * Field _cdSituacaoOperacao
     */
    private int _cdSituacaoOperacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacao
     */
    private boolean _has_cdSituacaoOperacao;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private java.lang.String _cdMotivoSituacaoPagamento;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _digitoAgenciaDebito
     */
    private java.lang.String _digitoAgenciaDebito;

    /**
     * Field _contaDebito
     */
    private long _contaDebito = 0;

    /**
     * keeps track of state for field: _contaDebito
     */
    private boolean _has_contaDebito;

    /**
     * Field _digitoContaDebito
     */
    private java.lang.String _digitoContaDebito;

    /**
     * Field _cdBancoCredito
     */
    private int _cdBancoCredito = 0;

    /**
     * keeps track of state for field: _cdBancoCredito
     */
    private boolean _has_cdBancoCredito;

    /**
     * Field _cdAgenciaCredito
     */
    private int _cdAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdAgenciaCredito
     */
    private boolean _has_cdAgenciaCredito;

    /**
     * Field _digitoAgenciaCredito
     */
    private java.lang.String _digitoAgenciaCredito;

    /**
     * Field _cdContaCredito
     */
    private long _cdContaCredito = 0;

    /**
     * keeps track of state for field: _cdContaCredito
     */
    private boolean _has_cdContaCredito;

    /**
     * Field _digitoContaCredito
     */
    private java.lang.String _digitoContaCredito;

    /**
     * Field _dsTipoContaCredito
     */
    private java.lang.String _dsTipoContaCredito;

    /**
     * Field _cdServido
     */
    private int _cdServido = 0;

    /**
     * keeps track of state for field: _cdServido
     */
    private boolean _has_cdServido;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _dsServico
     */
    private java.lang.String _dsServico;

    /**
     * Field _cdTipoRetornoPagamento
     */
    private int _cdTipoRetornoPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoRetornoPagamento
     */
    private boolean _has_cdTipoRetornoPagamento;

    /**
     * Field _descTipoRetornoPagamento
     */
    private java.lang.String _descTipoRetornoPagamento;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _dsEfetivacaoPagamento
     */
    private java.lang.String _dsEfetivacaoPagamento;

    /**
     * Field _cdAgenciaVencimento
     */
    private int _cdAgenciaVencimento = 0;

    /**
     * keeps track of state for field: _cdAgenciaVencimento
     */
    private boolean _has_cdAgenciaVencimento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamentoClientePagador(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaCredito
     * 
     */
    public void deleteCdAgenciaCredito()
    {
        this._has_cdAgenciaCredito= false;
    } //-- void deleteCdAgenciaCredito() 

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdAgenciaPagadora
     * 
     */
    public void deleteCdAgenciaPagadora()
    {
        this._has_cdAgenciaPagadora= false;
    } //-- void deleteCdAgenciaPagadora() 

    /**
     * Method deleteCdAgenciaVencimento
     * 
     */
    public void deleteCdAgenciaVencimento()
    {
        this._has_cdAgenciaVencimento= false;
    } //-- void deleteCdAgenciaVencimento() 

    /**
     * Method deleteCdBancoCredito
     * 
     */
    public void deleteCdBancoCredito()
    {
        this._has_cdBancoCredito= false;
    } //-- void deleteCdBancoCredito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaCredito
     * 
     */
    public void deleteCdContaCredito()
    {
        this._has_cdContaCredito= false;
    } //-- void deleteCdContaCredito() 

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdIdentificacaoInscricaoFavorecido
     * 
     */
    public void deleteCdIdentificacaoInscricaoFavorecido()
    {
        this._has_cdIdentificacaoInscricaoFavorecido= false;
    } //-- void deleteCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdServido
     * 
     */
    public void deleteCdServido()
    {
        this._has_cdServido= false;
    } //-- void deleteCdServido() 

    /**
     * Method deleteCdSituacaoOperacao
     * 
     */
    public void deleteCdSituacaoOperacao()
    {
        this._has_cdSituacaoOperacao= false;
    } //-- void deleteCdSituacaoOperacao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoRetornoPagamento
     * 
     */
    public void deleteCdTipoRetornoPagamento()
    {
        this._has_cdTipoRetornoPagamento= false;
    } //-- void deleteCdTipoRetornoPagamento() 

    /**
     * Method deleteContaDebito
     * 
     */
    public void deleteContaDebito()
    {
        this._has_contaDebito= false;
    } //-- void deleteContaDebito() 

    /**
     * Method deleteControleCpfPagador
     * 
     */
    public void deleteControleCpfPagador()
    {
        this._has_controleCpfPagador= false;
    } //-- void deleteControleCpfPagador() 

    /**
     * Method deleteCorpoCpfPagador
     * 
     */
    public void deleteCorpoCpfPagador()
    {
        this._has_corpoCpfPagador= false;
    } //-- void deleteCorpoCpfPagador() 

    /**
     * Method deleteFilialCpfPagador
     * 
     */
    public void deleteFilialCpfPagador()
    {
        this._has_filialCpfPagador= false;
    } //-- void deleteFilialCpfPagador() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaCredito'.
     */
    public int getCdAgenciaCredito()
    {
        return this._cdAgenciaCredito;
    } //-- int getCdAgenciaCredito() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdAgenciaPagadora'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaPagadora'.
     */
    public int getCdAgenciaPagadora()
    {
        return this._cdAgenciaPagadora;
    } //-- int getCdAgenciaPagadora() 

    /**
     * Returns the value of field 'cdAgenciaVencimento'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaVencimento'.
     */
    public int getCdAgenciaVencimento()
    {
        return this._cdAgenciaVencimento;
    } //-- int getCdAgenciaVencimento() 

    /**
     * Returns the value of field 'cdBancoCredito'.
     * 
     * @return int
     * @return the value of field 'cdBancoCredito'.
     */
    public int getCdBancoCredito()
    {
        return this._cdBancoCredito;
    } //-- int getCdBancoCredito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaCredito'.
     * 
     * @return long
     * @return the value of field 'cdContaCredito'.
     */
    public long getCdContaCredito()
    {
        return this._cdContaCredito;
    } //-- long getCdContaCredito() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public int getCdIdentificacaoInscricaoFavorecido()
    {
        return this._cdIdentificacaoInscricaoFavorecido;
    } //-- int getCdIdentificacaoInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public java.lang.String getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- java.lang.String getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdServido'.
     * 
     * @return int
     * @return the value of field 'cdServido'.
     */
    public int getCdServido()
    {
        return this._cdServido;
    } //-- int getCdServido() 

    /**
     * Returns the value of field 'cdSituacaoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacao'.
     */
    public int getCdSituacaoOperacao()
    {
        return this._cdSituacaoOperacao;
    } //-- int getCdSituacaoOperacao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoRetornoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRetornoPagamento'.
     */
    public int getCdTipoRetornoPagamento()
    {
        return this._cdTipoRetornoPagamento;
    } //-- int getCdTipoRetornoPagamento() 

    /**
     * Returns the value of field 'contaDebito'.
     * 
     * @return long
     * @return the value of field 'contaDebito'.
     */
    public long getContaDebito()
    {
        return this._contaDebito;
    } //-- long getContaDebito() 

    /**
     * Returns the value of field 'controleCpfPagador'.
     * 
     * @return int
     * @return the value of field 'controleCpfPagador'.
     */
    public int getControleCpfPagador()
    {
        return this._controleCpfPagador;
    } //-- int getControleCpfPagador() 

    /**
     * Returns the value of field 'corpoCpfPagador'.
     * 
     * @return long
     * @return the value of field 'corpoCpfPagador'.
     */
    public long getCorpoCpfPagador()
    {
        return this._corpoCpfPagador;
    } //-- long getCorpoCpfPagador() 

    /**
     * Returns the value of field 'descTipoRetornoPagamento'.
     * 
     * @return String
     * @return the value of field 'descTipoRetornoPagamento'.
     */
    public java.lang.String getDescTipoRetornoPagamento()
    {
        return this._descTipoRetornoPagamento;
    } //-- java.lang.String getDescTipoRetornoPagamento() 

    /**
     * Returns the value of field 'digitoAgenciaCredito'.
     * 
     * @return String
     * @return the value of field 'digitoAgenciaCredito'.
     */
    public java.lang.String getDigitoAgenciaCredito()
    {
        return this._digitoAgenciaCredito;
    } //-- java.lang.String getDigitoAgenciaCredito() 

    /**
     * Returns the value of field 'digitoAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'digitoAgenciaDebito'.
     */
    public java.lang.String getDigitoAgenciaDebito()
    {
        return this._digitoAgenciaDebito;
    } //-- java.lang.String getDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'digitoContaCredito'.
     * 
     * @return String
     * @return the value of field 'digitoContaCredito'.
     */
    public java.lang.String getDigitoContaCredito()
    {
        return this._digitoContaCredito;
    } //-- java.lang.String getDigitoContaCredito() 

    /**
     * Returns the value of field 'digitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'digitoContaDebito'.
     */
    public java.lang.String getDigitoContaDebito()
    {
        return this._digitoContaDebito;
    } //-- java.lang.String getDigitoContaDebito() 

    /**
     * Returns the value of field 'dsAbrevFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsAbrevFavorecido'.
     */
    public java.lang.String getDsAbrevFavorecido()
    {
        return this._dsAbrevFavorecido;
    } //-- java.lang.String getDsAbrevFavorecido() 

    /**
     * Returns the value of field 'dsClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsClientePagador'.
     */
    public java.lang.String getDsClientePagador()
    {
        return this._dsClientePagador;
    } //-- java.lang.String getDsClientePagador() 

    /**
     * Returns the value of field 'dsEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsEfetivacaoPagamento'.
     */
    public java.lang.String getDsEfetivacaoPagamento()
    {
        return this._dsEfetivacaoPagamento;
    } //-- java.lang.String getDsEfetivacaoPagamento() 

    /**
     * Returns the value of field
     * 'dsIdentificacaoInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'dsIdentificacaoInscricaoFavorecido'.
     */
    public java.lang.String getDsIdentificacaoInscricaoFavorecido()
    {
        return this._dsIdentificacaoInscricaoFavorecido;
    } //-- java.lang.String getDsIdentificacaoInscricaoFavorecido() 

    /**
     * Returns the value of field 'dsServico'.
     * 
     * @return String
     * @return the value of field 'dsServico'.
     */
    public java.lang.String getDsServico()
    {
        return this._dsServico;
    } //-- java.lang.String getDsServico() 

    /**
     * Returns the value of field 'dsTipoContaCredito'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaCredito'.
     */
    public java.lang.String getDsTipoContaCredito()
    {
        return this._dsTipoContaCredito;
    } //-- java.lang.String getDsTipoContaCredito() 

    /**
     * Returns the value of field 'dtCreditoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamento'.
     */
    public java.lang.String getDtCreditoPagamento()
    {
        return this._dtCreditoPagamento;
    } //-- java.lang.String getDtCreditoPagamento() 

    /**
     * Returns the value of field 'dtDisponivelAte'.
     * 
     * @return String
     * @return the value of field 'dtDisponivelAte'.
     */
    public java.lang.String getDtDisponivelAte()
    {
        return this._dtDisponivelAte;
    } //-- java.lang.String getDtDisponivelAte() 

    /**
     * Returns the value of field 'filialCpfPagador'.
     * 
     * @return int
     * @return the value of field 'filialCpfPagador'.
     */
    public int getFilialCpfPagador()
    {
        return this._filialCpfPagador;
    } //-- int getFilialCpfPagador() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'vlPagamentoClientePagador'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoClientePagador'.
     */
    public java.math.BigDecimal getVlPagamentoClientePagador()
    {
        return this._vlPagamentoClientePagador;
    } //-- java.math.BigDecimal getVlPagamentoClientePagador() 

    /**
     * Method hasCdAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaCredito()
    {
        return this._has_cdAgenciaCredito;
    } //-- boolean hasCdAgenciaCredito() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdAgenciaPagadora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaPagadora()
    {
        return this._has_cdAgenciaPagadora;
    } //-- boolean hasCdAgenciaPagadora() 

    /**
     * Method hasCdAgenciaVencimento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaVencimento()
    {
        return this._has_cdAgenciaVencimento;
    } //-- boolean hasCdAgenciaVencimento() 

    /**
     * Method hasCdBancoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoCredito()
    {
        return this._has_cdBancoCredito;
    } //-- boolean hasCdBancoCredito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaCredito()
    {
        return this._has_cdContaCredito;
    } //-- boolean hasCdContaCredito() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdIdentificacaoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificacaoInscricaoFavorecido()
    {
        return this._has_cdIdentificacaoInscricaoFavorecido;
    } //-- boolean hasCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdServido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServido()
    {
        return this._has_cdServido;
    } //-- boolean hasCdServido() 

    /**
     * Method hasCdSituacaoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacao()
    {
        return this._has_cdSituacaoOperacao;
    } //-- boolean hasCdSituacaoOperacao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoRetornoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRetornoPagamento()
    {
        return this._has_cdTipoRetornoPagamento;
    } //-- boolean hasCdTipoRetornoPagamento() 

    /**
     * Method hasContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasContaDebito()
    {
        return this._has_contaDebito;
    } //-- boolean hasContaDebito() 

    /**
     * Method hasControleCpfPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControleCpfPagador()
    {
        return this._has_controleCpfPagador;
    } //-- boolean hasControleCpfPagador() 

    /**
     * Method hasCorpoCpfPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCorpoCpfPagador()
    {
        return this._has_corpoCpfPagador;
    } //-- boolean hasCorpoCpfPagador() 

    /**
     * Method hasFilialCpfPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilialCpfPagador()
    {
        return this._has_filialCpfPagador;
    } //-- boolean hasFilialCpfPagador() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaCredito'.
     * 
     * @param cdAgenciaCredito the value of field 'cdAgenciaCredito'
     */
    public void setCdAgenciaCredito(int cdAgenciaCredito)
    {
        this._cdAgenciaCredito = cdAgenciaCredito;
        this._has_cdAgenciaCredito = true;
    } //-- void setCdAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdAgenciaPagadora'.
     * 
     * @param cdAgenciaPagadora the value of field
     * 'cdAgenciaPagadora'.
     */
    public void setCdAgenciaPagadora(int cdAgenciaPagadora)
    {
        this._cdAgenciaPagadora = cdAgenciaPagadora;
        this._has_cdAgenciaPagadora = true;
    } //-- void setCdAgenciaPagadora(int) 

    /**
     * Sets the value of field 'cdAgenciaVencimento'.
     * 
     * @param cdAgenciaVencimento the value of field
     * 'cdAgenciaVencimento'.
     */
    public void setCdAgenciaVencimento(int cdAgenciaVencimento)
    {
        this._cdAgenciaVencimento = cdAgenciaVencimento;
        this._has_cdAgenciaVencimento = true;
    } //-- void setCdAgenciaVencimento(int) 

    /**
     * Sets the value of field 'cdBancoCredito'.
     * 
     * @param cdBancoCredito the value of field 'cdBancoCredito'.
     */
    public void setCdBancoCredito(int cdBancoCredito)
    {
        this._cdBancoCredito = cdBancoCredito;
        this._has_cdBancoCredito = true;
    } //-- void setCdBancoCredito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaCredito'.
     * 
     * @param cdContaCredito the value of field 'cdContaCredito'.
     */
    public void setCdContaCredito(long cdContaCredito)
    {
        this._cdContaCredito = cdContaCredito;
        this._has_cdContaCredito = true;
    } //-- void setCdContaCredito(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @param cdIdentificacaoInscricaoFavorecido the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public void setCdIdentificacaoInscricaoFavorecido(int cdIdentificacaoInscricaoFavorecido)
    {
        this._cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
        this._has_cdIdentificacaoInscricaoFavorecido = true;
    } //-- void setCdIdentificacaoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(java.lang.String cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    } //-- void setCdMotivoSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdServido'.
     * 
     * @param cdServido the value of field 'cdServido'.
     */
    public void setCdServido(int cdServido)
    {
        this._cdServido = cdServido;
        this._has_cdServido = true;
    } //-- void setCdServido(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacao'.
     * 
     * @param cdSituacaoOperacao the value of field
     * 'cdSituacaoOperacao'.
     */
    public void setCdSituacaoOperacao(int cdSituacaoOperacao)
    {
        this._cdSituacaoOperacao = cdSituacaoOperacao;
        this._has_cdSituacaoOperacao = true;
    } //-- void setCdSituacaoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoRetornoPagamento'.
     * 
     * @param cdTipoRetornoPagamento the value of field
     * 'cdTipoRetornoPagamento'.
     */
    public void setCdTipoRetornoPagamento(int cdTipoRetornoPagamento)
    {
        this._cdTipoRetornoPagamento = cdTipoRetornoPagamento;
        this._has_cdTipoRetornoPagamento = true;
    } //-- void setCdTipoRetornoPagamento(int) 

    /**
     * Sets the value of field 'contaDebito'.
     * 
     * @param contaDebito the value of field 'contaDebito'.
     */
    public void setContaDebito(long contaDebito)
    {
        this._contaDebito = contaDebito;
        this._has_contaDebito = true;
    } //-- void setContaDebito(long) 

    /**
     * Sets the value of field 'controleCpfPagador'.
     * 
     * @param controleCpfPagador the value of field
     * 'controleCpfPagador'.
     */
    public void setControleCpfPagador(int controleCpfPagador)
    {
        this._controleCpfPagador = controleCpfPagador;
        this._has_controleCpfPagador = true;
    } //-- void setControleCpfPagador(int) 

    /**
     * Sets the value of field 'corpoCpfPagador'.
     * 
     * @param corpoCpfPagador the value of field 'corpoCpfPagador'.
     */
    public void setCorpoCpfPagador(long corpoCpfPagador)
    {
        this._corpoCpfPagador = corpoCpfPagador;
        this._has_corpoCpfPagador = true;
    } //-- void setCorpoCpfPagador(long) 

    /**
     * Sets the value of field 'descTipoRetornoPagamento'.
     * 
     * @param descTipoRetornoPagamento the value of field
     * 'descTipoRetornoPagamento'.
     */
    public void setDescTipoRetornoPagamento(java.lang.String descTipoRetornoPagamento)
    {
        this._descTipoRetornoPagamento = descTipoRetornoPagamento;
    } //-- void setDescTipoRetornoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'digitoAgenciaCredito'.
     * 
     * @param digitoAgenciaCredito the value of field
     * 'digitoAgenciaCredito'.
     */
    public void setDigitoAgenciaCredito(java.lang.String digitoAgenciaCredito)
    {
        this._digitoAgenciaCredito = digitoAgenciaCredito;
    } //-- void setDigitoAgenciaCredito(java.lang.String) 

    /**
     * Sets the value of field 'digitoAgenciaDebito'.
     * 
     * @param digitoAgenciaDebito the value of field
     * 'digitoAgenciaDebito'.
     */
    public void setDigitoAgenciaDebito(java.lang.String digitoAgenciaDebito)
    {
        this._digitoAgenciaDebito = digitoAgenciaDebito;
    } //-- void setDigitoAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'digitoContaCredito'.
     * 
     * @param digitoContaCredito the value of field
     * 'digitoContaCredito'.
     */
    public void setDigitoContaCredito(java.lang.String digitoContaCredito)
    {
        this._digitoContaCredito = digitoContaCredito;
    } //-- void setDigitoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'digitoContaDebito'.
     * 
     * @param digitoContaDebito the value of field
     * 'digitoContaDebito'.
     */
    public void setDigitoContaDebito(java.lang.String digitoContaDebito)
    {
        this._digitoContaDebito = digitoContaDebito;
    } //-- void setDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsAbrevFavorecido'.
     * 
     * @param dsAbrevFavorecido the value of field
     * 'dsAbrevFavorecido'.
     */
    public void setDsAbrevFavorecido(java.lang.String dsAbrevFavorecido)
    {
        this._dsAbrevFavorecido = dsAbrevFavorecido;
    } //-- void setDsAbrevFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsClientePagador'.
     * 
     * @param dsClientePagador the value of field 'dsClientePagador'
     */
    public void setDsClientePagador(java.lang.String dsClientePagador)
    {
        this._dsClientePagador = dsClientePagador;
    } //-- void setDsClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsEfetivacaoPagamento'.
     * 
     * @param dsEfetivacaoPagamento the value of field
     * 'dsEfetivacaoPagamento'.
     */
    public void setDsEfetivacaoPagamento(java.lang.String dsEfetivacaoPagamento)
    {
        this._dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    } //-- void setDsEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsIdentificacaoInscricaoFavorecido'.
     * 
     * @param dsIdentificacaoInscricaoFavorecido the value of field
     * 'dsIdentificacaoInscricaoFavorecido'.
     */
    public void setDsIdentificacaoInscricaoFavorecido(java.lang.String dsIdentificacaoInscricaoFavorecido)
    {
        this._dsIdentificacaoInscricaoFavorecido = dsIdentificacaoInscricaoFavorecido;
    } //-- void setDsIdentificacaoInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsServico'.
     * 
     * @param dsServico the value of field 'dsServico'.
     */
    public void setDsServico(java.lang.String dsServico)
    {
        this._dsServico = dsServico;
    } //-- void setDsServico(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaCredito'.
     * 
     * @param dsTipoContaCredito the value of field
     * 'dsTipoContaCredito'.
     */
    public void setDsTipoContaCredito(java.lang.String dsTipoContaCredito)
    {
        this._dsTipoContaCredito = dsTipoContaCredito;
    } //-- void setDsTipoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dtCreditoPagamento'.
     * 
     * @param dtCreditoPagamento the value of field
     * 'dtCreditoPagamento'.
     */
    public void setDtCreditoPagamento(java.lang.String dtCreditoPagamento)
    {
        this._dtCreditoPagamento = dtCreditoPagamento;
    } //-- void setDtCreditoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtDisponivelAte'.
     * 
     * @param dtDisponivelAte the value of field 'dtDisponivelAte'.
     */
    public void setDtDisponivelAte(java.lang.String dtDisponivelAte)
    {
        this._dtDisponivelAte = dtDisponivelAte;
    } //-- void setDtDisponivelAte(java.lang.String) 

    /**
     * Sets the value of field 'filialCpfPagador'.
     * 
     * @param filialCpfPagador the value of field 'filialCpfPagador'
     */
    public void setFilialCpfPagador(int filialCpfPagador)
    {
        this._filialCpfPagador = filialCpfPagador;
        this._has_filialCpfPagador = true;
    } //-- void setFilialCpfPagador(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'vlPagamentoClientePagador'.
     * 
     * @param vlPagamentoClientePagador the value of field
     * 'vlPagamentoClientePagador'.
     */
    public void setVlPagamentoClientePagador(java.math.BigDecimal vlPagamentoClientePagador)
    {
        this._vlPagamentoClientePagador = vlPagamentoClientePagador;
    } //-- void setVlPagamentoClientePagador(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
