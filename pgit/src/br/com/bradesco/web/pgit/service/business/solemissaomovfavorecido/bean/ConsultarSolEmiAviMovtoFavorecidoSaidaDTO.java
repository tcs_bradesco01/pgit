/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean;


/**
 * Nome: ConsultarSolEmiAviMovtoFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolEmiAviMovtoFavorecidoSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroLinhas. */
    private Integer numeroLinhas;
    
    /** Atributo cdTipoSolicitacao. */
    private Integer cdTipoSolicitacao;
    
    /** Atributo nrSolicitacao. */
    private Integer nrSolicitacao;
    
    /** Atributo dtHoraSolicitacao. */
    private String dtHoraSolicitacao;
    
    /** Atributo dtHoraAtendimento. */
    private String dtHoraAtendimento;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo cdMotivoSituacao. */
    private Integer cdMotivoSituacao;
    
    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo dsTipoCanal. */
    private String dsTipoCanal;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato;
    
    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo dsSituacaoFormatado. */
    private String dsSituacaoFormatado;
    
    /** Atributo dsMotivoFormatado. */
    private String dsMotivoFormatado;
    
    /** Atributo dsContratoFormatado. */
    private String dsContratoFormatado;
    
    /** Atributo dtHoraSolicitacaoFormatada. */
    private String dtHoraSolicitacaoFormatada;
    
	/**
	 * Get: cdMotivoSituacao.
	 *
	 * @return cdMotivoSituacao
	 */
	public Integer getCdMotivoSituacao() {
		return cdMotivoSituacao;
	}
	
	/**
	 * Set: cdMotivoSituacao.
	 *
	 * @param cdMotivoSituacao the cd motivo situacao
	 */
	public void setCdMotivoSituacao(Integer cdMotivoSituacao) {
		this.cdMotivoSituacao = cdMotivoSituacao;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoSolicitacao.
	 *
	 * @return cdTipoSolicitacao
	 */
	public Integer getCdTipoSolicitacao() {
		return cdTipoSolicitacao;
	}
	
	/**
	 * Set: cdTipoSolicitacao.
	 *
	 * @param cdTipoSolicitacao the cd tipo solicitacao
	 */
	public void setCdTipoSolicitacao(Integer cdTipoSolicitacao) {
		this.cdTipoSolicitacao = cdTipoSolicitacao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}
	
	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: dsTipoCanal.
	 *
	 * @return dsTipoCanal
	 */
	public String getDsTipoCanal() {
		return dsTipoCanal;
	}
	
	/**
	 * Set: dsTipoCanal.
	 *
	 * @param dsTipoCanal the ds tipo canal
	 */
	public void setDsTipoCanal(String dsTipoCanal) {
		this.dsTipoCanal = dsTipoCanal;
	}
	
	/**
	 * Get: dtHoraAtendimento.
	 *
	 * @return dtHoraAtendimento
	 */
	public String getDtHoraAtendimento() {
		return dtHoraAtendimento;
	}
	
	/**
	 * Set: dtHoraAtendimento.
	 *
	 * @param dtHoraAtendimento the dt hora atendimento
	 */
	public void setDtHoraAtendimento(String dtHoraAtendimento) {
		this.dtHoraAtendimento = dtHoraAtendimento;
	}
	
	/**
	 * Get: dtHoraSolicitacao.
	 *
	 * @return dtHoraSolicitacao
	 */
	public String getDtHoraSolicitacao() {
		return dtHoraSolicitacao;
	}
	
	/**
	 * Set: dtHoraSolicitacao.
	 *
	 * @param dtHoraSolicitacao the dt hora solicitacao
	 */
	public void setDtHoraSolicitacao(String dtHoraSolicitacao) {
		this.dtHoraSolicitacao = dtHoraSolicitacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	
	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
	
	/**
	 * Get: dsContratoFormatado.
	 *
	 * @return dsContratoFormatado
	 */
	public String getDsContratoFormatado() {
		return dsContratoFormatado;
	}
	
	/**
	 * Set: dsContratoFormatado.
	 *
	 * @param dsContratoFormatado the ds contrato formatado
	 */
	public void setDsContratoFormatado(String dsContratoFormatado) {
		this.dsContratoFormatado = dsContratoFormatado;
	}
	
	/**
	 * Get: dsMotivoFormatado.
	 *
	 * @return dsMotivoFormatado
	 */
	public String getDsMotivoFormatado() {
		return dsMotivoFormatado;
	}
	
	/**
	 * Set: dsMotivoFormatado.
	 *
	 * @param dsMotivoFormatado the ds motivo formatado
	 */
	public void setDsMotivoFormatado(String dsMotivoFormatado) {
		this.dsMotivoFormatado = dsMotivoFormatado;
	}
	
	/**
	 * Get: dsSituacaoFormatado.
	 *
	 * @return dsSituacaoFormatado
	 */
	public String getDsSituacaoFormatado() {
		return dsSituacaoFormatado;
	}
	
	/**
	 * Set: dsSituacaoFormatado.
	 *
	 * @param dsSituacaoFormatado the ds situacao formatado
	 */
	public void setDsSituacaoFormatado(String dsSituacaoFormatado) {
		this.dsSituacaoFormatado = dsSituacaoFormatado;
	}
	
	/**
	 * Get: dtHoraSolicitacaoFormatada.
	 *
	 * @return dtHoraSolicitacaoFormatada
	 */
	public String getDtHoraSolicitacaoFormatada() {
		return dtHoraSolicitacaoFormatada;
	}
	
	/**
	 * Set: dtHoraSolicitacaoFormatada.
	 *
	 * @param dtHoraSolicitacaoFormatada the dt hora solicitacao formatada
	 */
	public void setDtHoraSolicitacaoFormatada(String dtHoraSolicitacaoFormatada) {
		this.dtHoraSolicitacaoFormatada = dtHoraSolicitacaoFormatada;
	}
}