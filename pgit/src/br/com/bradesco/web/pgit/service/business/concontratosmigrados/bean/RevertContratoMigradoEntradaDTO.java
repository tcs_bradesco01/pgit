/*
 * Nome: br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean;

/**
 * Nome: RevertContratoMigradoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RevertContratoMigradoEntradaDTO {
	
	/** Atributo cdAcao. */
	private Integer cdAcao;
	
	/** Atributo centroCustoOrigem. */
	private Integer centroCustoOrigem;
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	
	/**
	 * Get: cdAcao.
	 *
	 * @return cdAcao
	 */
	public Integer getCdAcao() {
		return cdAcao;
	}
	
	/**
	 * Set: cdAcao.
	 *
	 * @param cdAcao the cd acao
	 */
	public void setCdAcao(Integer cdAcao) {
		this.cdAcao = cdAcao;
	}
	
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: centroCustoOrigem.
	 *
	 * @return centroCustoOrigem
	 */
	public Integer getCentroCustoOrigem() {
		return centroCustoOrigem;
	}
	
	/**
	 * Set: centroCustoOrigem.
	 *
	 * @param centroCustoOrigem the centro custo origem
	 */
	public void setCentroCustoOrigem(Integer centroCustoOrigem) {
		this.centroCustoOrigem = centroCustoOrigem;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	

}
