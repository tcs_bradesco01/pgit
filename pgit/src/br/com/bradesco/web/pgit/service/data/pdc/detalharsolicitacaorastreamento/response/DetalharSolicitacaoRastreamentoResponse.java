/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolicitacaoRastreamentoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolicitacaoRastreamentoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridContrato
     */
    private long _cdPessoaJuridContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridContrato
     */
    private boolean _has_cdPessoaJuridContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdBancoDeb
     */
    private int _cdBancoDeb = 0;

    /**
     * keeps track of state for field: _cdBancoDeb
     */
    private boolean _has_cdBancoDeb;

    /**
     * Field _dsBancoDeb
     */
    private java.lang.String _dsBancoDeb;

    /**
     * Field _cdAgenciaDeb
     */
    private int _cdAgenciaDeb = 0;

    /**
     * keeps track of state for field: _cdAgenciaDeb
     */
    private boolean _has_cdAgenciaDeb;

    /**
     * Field _cdDigitoAgenciaDeb
     */
    private int _cdDigitoAgenciaDeb = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaDeb
     */
    private boolean _has_cdDigitoAgenciaDeb;

    /**
     * Field _dsAgenciaDeb
     */
    private java.lang.String _dsAgenciaDeb;

    /**
     * Field _cdContaDeb
     */
    private long _cdContaDeb = 0;

    /**
     * keeps track of state for field: _cdContaDeb
     */
    private boolean _has_cdContaDeb;

    /**
     * Field _cdDigitoContaDeb
     */
    private java.lang.String _cdDigitoContaDeb;

    /**
     * Field _dtInicioRastreabilidadeFavorecido
     */
    private java.lang.String _dtInicioRastreabilidadeFavorecido;

    /**
     * Field _dtFimRastreabilidadeFavorecido
     */
    private java.lang.String _dtFimRastreabilidadeFavorecido;

    /**
     * Field _cdIndicadorInclusaoFavorecido
     */
    private int _cdIndicadorInclusaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdIndicadorInclusaoFavorecid
     */
    private boolean _has_cdIndicadorInclusaoFavorecido;

    /**
     * Field _dsIndicadorInclusaoFavorecido
     */
    private java.lang.String _dsIndicadorInclusaoFavorecido;

    /**
     * Field _cdFormaFavorecido
     */
    private int _cdFormaFavorecido = 0;

    /**
     * keeps track of state for field: _cdFormaFavorecido
     */
    private boolean _has_cdFormaFavorecido;

    /**
     * Field _dsFormaCadastroFavorecidos
     */
    private java.lang.String _dsFormaCadastroFavorecidos;

    /**
     * Field _cdIdDestinoRetorno
     */
    private int _cdIdDestinoRetorno = 0;

    /**
     * keeps track of state for field: _cdIdDestinoRetorno
     */
    private boolean _has_cdIdDestinoRetorno;

    /**
     * Field _dsIdDestinoRetorno
     */
    private java.lang.String _dsIdDestinoRetorno;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _dsSituacaoSolicitacaoPagamento
     */
    private java.lang.String _dsSituacaoSolicitacaoPagamento;

    /**
     * Field _cdMotivoSituacaoSolicitacao
     */
    private int _cdMotivoSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoSolicitacao
     */
    private boolean _has_cdMotivoSituacaoSolicitacao;

    /**
     * Field _dsMotivoSituacaoSolicitacao
     */
    private java.lang.String _dsMotivoSituacaoSolicitacao;

    /**
     * Field _dtSolicitacao
     */
    private java.lang.String _dtSolicitacao;

    /**
     * Field _hrSolicitacao
     */
    private java.lang.String _hrSolicitacao;

    /**
     * Field _dtAtendimento
     */
    private java.lang.String _dtAtendimento;

    /**
     * Field _hrAtendimento
     */
    private java.lang.String _hrAtendimento;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _numPercentualDescTarifa
     */
    private java.math.BigDecimal _numPercentualDescTarifa = new java.math.BigDecimal("0");

    /**
     * Field _vlrTarifaAtual
     */
    private java.math.BigDecimal _vlrTarifaAtual = new java.math.BigDecimal("0");

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExter
     */
    private java.lang.String _cdUsuarioInclusaoExter;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperCanalInclusao
     */
    private java.lang.String _cdOperCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExter
     */
    private java.lang.String _cdUsuarioManutencaoExter;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperCanalManutencao
     */
    private java.lang.String _cdOperCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolicitacaoRastreamentoResponse() 
     {
        super();
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
        setNumPercentualDescTarifa(new java.math.BigDecimal("0"));
        setVlrTarifaAtual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.response.DetalharSolicitacaoRastreamentoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDeb
     * 
     */
    public void deleteCdAgenciaDeb()
    {
        this._has_cdAgenciaDeb= false;
    } //-- void deleteCdAgenciaDeb() 

    /**
     * Method deleteCdBancoDeb
     * 
     */
    public void deleteCdBancoDeb()
    {
        this._has_cdBancoDeb= false;
    } //-- void deleteCdBancoDeb() 

    /**
     * Method deleteCdContaDeb
     * 
     */
    public void deleteCdContaDeb()
    {
        this._has_cdContaDeb= false;
    } //-- void deleteCdContaDeb() 

    /**
     * Method deleteCdDigitoAgenciaDeb
     * 
     */
    public void deleteCdDigitoAgenciaDeb()
    {
        this._has_cdDigitoAgenciaDeb= false;
    } //-- void deleteCdDigitoAgenciaDeb() 

    /**
     * Method deleteCdFormaFavorecido
     * 
     */
    public void deleteCdFormaFavorecido()
    {
        this._has_cdFormaFavorecido= false;
    } //-- void deleteCdFormaFavorecido() 

    /**
     * Method deleteCdIdDestinoRetorno
     * 
     */
    public void deleteCdIdDestinoRetorno()
    {
        this._has_cdIdDestinoRetorno= false;
    } //-- void deleteCdIdDestinoRetorno() 

    /**
     * Method deleteCdIndicadorInclusaoFavorecido
     * 
     */
    public void deleteCdIndicadorInclusaoFavorecido()
    {
        this._has_cdIndicadorInclusaoFavorecido= false;
    } //-- void deleteCdIndicadorInclusaoFavorecido() 

    /**
     * Method deleteCdMotivoSituacaoSolicitacao
     * 
     */
    public void deleteCdMotivoSituacaoSolicitacao()
    {
        this._has_cdMotivoSituacaoSolicitacao= false;
    } //-- void deleteCdMotivoSituacaoSolicitacao() 

    /**
     * Method deleteCdPessoaJuridContrato
     * 
     */
    public void deleteCdPessoaJuridContrato()
    {
        this._has_cdPessoaJuridContrato= false;
    } //-- void deleteCdPessoaJuridContrato() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaDeb'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDeb'.
     */
    public int getCdAgenciaDeb()
    {
        return this._cdAgenciaDeb;
    } //-- int getCdAgenciaDeb() 

    /**
     * Returns the value of field 'cdBancoDeb'.
     * 
     * @return int
     * @return the value of field 'cdBancoDeb'.
     */
    public int getCdBancoDeb()
    {
        return this._cdBancoDeb;
    } //-- int getCdBancoDeb() 

    /**
     * Returns the value of field 'cdContaDeb'.
     * 
     * @return long
     * @return the value of field 'cdContaDeb'.
     */
    public long getCdContaDeb()
    {
        return this._cdContaDeb;
    } //-- long getCdContaDeb() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDeb'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaDeb'.
     */
    public int getCdDigitoAgenciaDeb()
    {
        return this._cdDigitoAgenciaDeb;
    } //-- int getCdDigitoAgenciaDeb() 

    /**
     * Returns the value of field 'cdDigitoContaDeb'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDeb'.
     */
    public java.lang.String getCdDigitoContaDeb()
    {
        return this._cdDigitoContaDeb;
    } //-- java.lang.String getCdDigitoContaDeb() 

    /**
     * Returns the value of field 'cdFormaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdFormaFavorecido'.
     */
    public int getCdFormaFavorecido()
    {
        return this._cdFormaFavorecido;
    } //-- int getCdFormaFavorecido() 

    /**
     * Returns the value of field 'cdIdDestinoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdIdDestinoRetorno'.
     */
    public int getCdIdDestinoRetorno()
    {
        return this._cdIdDestinoRetorno;
    } //-- int getCdIdDestinoRetorno() 

    /**
     * Returns the value of field 'cdIndicadorInclusaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorInclusaoFavorecido'.
     */
    public int getCdIndicadorInclusaoFavorecido()
    {
        return this._cdIndicadorInclusaoFavorecido;
    } //-- int getCdIndicadorInclusaoFavorecido() 

    /**
     * Returns the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoSolicitacao'.
     */
    public int getCdMotivoSituacaoSolicitacao()
    {
        return this._cdMotivoSituacaoSolicitacao;
    } //-- int getCdMotivoSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdOperCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalInclusao'.
     */
    public java.lang.String getCdOperCanalInclusao()
    {
        return this._cdOperCanalInclusao;
    } //-- java.lang.String getCdOperCanalInclusao() 

    /**
     * Returns the value of field 'cdOperCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalManutencao'.
     */
    public java.lang.String getCdOperCanalManutencao()
    {
        return this._cdOperCanalManutencao;
    } //-- java.lang.String getCdOperCanalManutencao() 

    /**
     * Returns the value of field 'cdPessoaJuridContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridContrato'.
     */
    public long getCdPessoaJuridContrato()
    {
        return this._cdPessoaJuridContrato;
    } //-- long getCdPessoaJuridContrato() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExter'.
     */
    public java.lang.String getCdUsuarioInclusaoExter()
    {
        return this._cdUsuarioInclusaoExter;
    } //-- java.lang.String getCdUsuarioInclusaoExter() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExter'.
     */
    public java.lang.String getCdUsuarioManutencaoExter()
    {
        return this._cdUsuarioManutencaoExter;
    } //-- java.lang.String getCdUsuarioManutencaoExter() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaDeb'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDeb'.
     */
    public java.lang.String getDsAgenciaDeb()
    {
        return this._dsAgenciaDeb;
    } //-- java.lang.String getDsAgenciaDeb() 

    /**
     * Returns the value of field 'dsBancoDeb'.
     * 
     * @return String
     * @return the value of field 'dsBancoDeb'.
     */
    public java.lang.String getDsBancoDeb()
    {
        return this._dsBancoDeb;
    } //-- java.lang.String getDsBancoDeb() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsFormaCadastroFavorecidos'.
     * 
     * @return String
     * @return the value of field 'dsFormaCadastroFavorecidos'.
     */
    public java.lang.String getDsFormaCadastroFavorecidos()
    {
        return this._dsFormaCadastroFavorecidos;
    } //-- java.lang.String getDsFormaCadastroFavorecidos() 

    /**
     * Returns the value of field 'dsIdDestinoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsIdDestinoRetorno'.
     */
    public java.lang.String getDsIdDestinoRetorno()
    {
        return this._dsIdDestinoRetorno;
    } //-- java.lang.String getDsIdDestinoRetorno() 

    /**
     * Returns the value of field 'dsIndicadorInclusaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorInclusaoFavorecido'.
     */
    public java.lang.String getDsIndicadorInclusaoFavorecido()
    {
        return this._dsIndicadorInclusaoFavorecido;
    } //-- java.lang.String getDsIndicadorInclusaoFavorecido() 

    /**
     * Returns the value of field 'dsMotivoSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoSolicitacao'.
     */
    public java.lang.String getDsMotivoSituacaoSolicitacao()
    {
        return this._dsMotivoSituacaoSolicitacao;
    } //-- java.lang.String getDsMotivoSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacaoPagamento'.
     */
    public java.lang.String getDsSituacaoSolicitacaoPagamento()
    {
        return this._dsSituacaoSolicitacaoPagamento;
    } //-- java.lang.String getDsSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'dtAtendimento'.
     * 
     * @return String
     * @return the value of field 'dtAtendimento'.
     */
    public java.lang.String getDtAtendimento()
    {
        return this._dtAtendimento;
    } //-- java.lang.String getDtAtendimento() 

    /**
     * Returns the value of field 'dtFimRastreabilidadeFavorecido'.
     * 
     * @return String
     * @return the value of field 'dtFimRastreabilidadeFavorecido'.
     */
    public java.lang.String getDtFimRastreabilidadeFavorecido()
    {
        return this._dtFimRastreabilidadeFavorecido;
    } //-- java.lang.String getDtFimRastreabilidadeFavorecido() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field
     * 'dtInicioRastreabilidadeFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'dtInicioRastreabilidadeFavorecido'.
     */
    public java.lang.String getDtInicioRastreabilidadeFavorecido()
    {
        return this._dtInicioRastreabilidadeFavorecido;
    } //-- java.lang.String getDtInicioRastreabilidadeFavorecido() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacao'.
     */
    public java.lang.String getDtSolicitacao()
    {
        return this._dtSolicitacao;
    } //-- java.lang.String getDtSolicitacao() 

    /**
     * Returns the value of field 'hrAtendimento'.
     * 
     * @return String
     * @return the value of field 'hrAtendimento'.
     */
    public java.lang.String getHrAtendimento()
    {
        return this._hrAtendimento;
    } //-- java.lang.String getHrAtendimento() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'hrSolicitacao'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacao'.
     */
    public java.lang.String getHrSolicitacao()
    {
        return this._hrSolicitacao;
    } //-- java.lang.String getHrSolicitacao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'numPercentualDescTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'numPercentualDescTarifa'.
     */
    public java.math.BigDecimal getNumPercentualDescTarifa()
    {
        return this._numPercentualDescTarifa;
    } //-- java.math.BigDecimal getNumPercentualDescTarifa() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Returns the value of field 'vlrTarifaAtual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaAtual'.
     */
    public java.math.BigDecimal getVlrTarifaAtual()
    {
        return this._vlrTarifaAtual;
    } //-- java.math.BigDecimal getVlrTarifaAtual() 

    /**
     * Method hasCdAgenciaDeb
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDeb()
    {
        return this._has_cdAgenciaDeb;
    } //-- boolean hasCdAgenciaDeb() 

    /**
     * Method hasCdBancoDeb
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDeb()
    {
        return this._has_cdBancoDeb;
    } //-- boolean hasCdBancoDeb() 

    /**
     * Method hasCdContaDeb
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDeb()
    {
        return this._has_cdContaDeb;
    } //-- boolean hasCdContaDeb() 

    /**
     * Method hasCdDigitoAgenciaDeb
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaDeb()
    {
        return this._has_cdDigitoAgenciaDeb;
    } //-- boolean hasCdDigitoAgenciaDeb() 

    /**
     * Method hasCdFormaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaFavorecido()
    {
        return this._has_cdFormaFavorecido;
    } //-- boolean hasCdFormaFavorecido() 

    /**
     * Method hasCdIdDestinoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdDestinoRetorno()
    {
        return this._has_cdIdDestinoRetorno;
    } //-- boolean hasCdIdDestinoRetorno() 

    /**
     * Method hasCdIndicadorInclusaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorInclusaoFavorecido()
    {
        return this._has_cdIndicadorInclusaoFavorecido;
    } //-- boolean hasCdIndicadorInclusaoFavorecido() 

    /**
     * Method hasCdMotivoSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoSolicitacao()
    {
        return this._has_cdMotivoSituacaoSolicitacao;
    } //-- boolean hasCdMotivoSituacaoSolicitacao() 

    /**
     * Method hasCdPessoaJuridContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridContrato()
    {
        return this._has_cdPessoaJuridContrato;
    } //-- boolean hasCdPessoaJuridContrato() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDeb'.
     * 
     * @param cdAgenciaDeb the value of field 'cdAgenciaDeb'.
     */
    public void setCdAgenciaDeb(int cdAgenciaDeb)
    {
        this._cdAgenciaDeb = cdAgenciaDeb;
        this._has_cdAgenciaDeb = true;
    } //-- void setCdAgenciaDeb(int) 

    /**
     * Sets the value of field 'cdBancoDeb'.
     * 
     * @param cdBancoDeb the value of field 'cdBancoDeb'.
     */
    public void setCdBancoDeb(int cdBancoDeb)
    {
        this._cdBancoDeb = cdBancoDeb;
        this._has_cdBancoDeb = true;
    } //-- void setCdBancoDeb(int) 

    /**
     * Sets the value of field 'cdContaDeb'.
     * 
     * @param cdContaDeb the value of field 'cdContaDeb'.
     */
    public void setCdContaDeb(long cdContaDeb)
    {
        this._cdContaDeb = cdContaDeb;
        this._has_cdContaDeb = true;
    } //-- void setCdContaDeb(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDeb'.
     * 
     * @param cdDigitoAgenciaDeb the value of field
     * 'cdDigitoAgenciaDeb'.
     */
    public void setCdDigitoAgenciaDeb(int cdDigitoAgenciaDeb)
    {
        this._cdDigitoAgenciaDeb = cdDigitoAgenciaDeb;
        this._has_cdDigitoAgenciaDeb = true;
    } //-- void setCdDigitoAgenciaDeb(int) 

    /**
     * Sets the value of field 'cdDigitoContaDeb'.
     * 
     * @param cdDigitoContaDeb the value of field 'cdDigitoContaDeb'
     */
    public void setCdDigitoContaDeb(java.lang.String cdDigitoContaDeb)
    {
        this._cdDigitoContaDeb = cdDigitoContaDeb;
    } //-- void setCdDigitoContaDeb(java.lang.String) 

    /**
     * Sets the value of field 'cdFormaFavorecido'.
     * 
     * @param cdFormaFavorecido the value of field
     * 'cdFormaFavorecido'.
     */
    public void setCdFormaFavorecido(int cdFormaFavorecido)
    {
        this._cdFormaFavorecido = cdFormaFavorecido;
        this._has_cdFormaFavorecido = true;
    } //-- void setCdFormaFavorecido(int) 

    /**
     * Sets the value of field 'cdIdDestinoRetorno'.
     * 
     * @param cdIdDestinoRetorno the value of field
     * 'cdIdDestinoRetorno'.
     */
    public void setCdIdDestinoRetorno(int cdIdDestinoRetorno)
    {
        this._cdIdDestinoRetorno = cdIdDestinoRetorno;
        this._has_cdIdDestinoRetorno = true;
    } //-- void setCdIdDestinoRetorno(int) 

    /**
     * Sets the value of field 'cdIndicadorInclusaoFavorecido'.
     * 
     * @param cdIndicadorInclusaoFavorecido the value of field
     * 'cdIndicadorInclusaoFavorecido'.
     */
    public void setCdIndicadorInclusaoFavorecido(int cdIndicadorInclusaoFavorecido)
    {
        this._cdIndicadorInclusaoFavorecido = cdIndicadorInclusaoFavorecido;
        this._has_cdIndicadorInclusaoFavorecido = true;
    } //-- void setCdIndicadorInclusaoFavorecido(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @param cdMotivoSituacaoSolicitacao the value of field
     * 'cdMotivoSituacaoSolicitacao'.
     */
    public void setCdMotivoSituacaoSolicitacao(int cdMotivoSituacaoSolicitacao)
    {
        this._cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
        this._has_cdMotivoSituacaoSolicitacao = true;
    } //-- void setCdMotivoSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdOperCanalInclusao'.
     * 
     * @param cdOperCanalInclusao the value of field
     * 'cdOperCanalInclusao'.
     */
    public void setCdOperCanalInclusao(java.lang.String cdOperCanalInclusao)
    {
        this._cdOperCanalInclusao = cdOperCanalInclusao;
    } //-- void setCdOperCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperCanalManutencao'.
     * 
     * @param cdOperCanalManutencao the value of field
     * 'cdOperCanalManutencao'.
     */
    public void setCdOperCanalManutencao(java.lang.String cdOperCanalManutencao)
    {
        this._cdOperCanalManutencao = cdOperCanalManutencao;
    } //-- void setCdOperCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridContrato'.
     * 
     * @param cdPessoaJuridContrato the value of field
     * 'cdPessoaJuridContrato'.
     */
    public void setCdPessoaJuridContrato(long cdPessoaJuridContrato)
    {
        this._cdPessoaJuridContrato = cdPessoaJuridContrato;
        this._has_cdPessoaJuridContrato = true;
    } //-- void setCdPessoaJuridContrato(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @param cdUsuarioInclusaoExter the value of field
     * 'cdUsuarioInclusaoExter'.
     */
    public void setCdUsuarioInclusaoExter(java.lang.String cdUsuarioInclusaoExter)
    {
        this._cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
    } //-- void setCdUsuarioInclusaoExter(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @param cdUsuarioManutencaoExter the value of field
     * 'cdUsuarioManutencaoExter'.
     */
    public void setCdUsuarioManutencaoExter(java.lang.String cdUsuarioManutencaoExter)
    {
        this._cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
    } //-- void setCdUsuarioManutencaoExter(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDeb'.
     * 
     * @param dsAgenciaDeb the value of field 'dsAgenciaDeb'.
     */
    public void setDsAgenciaDeb(java.lang.String dsAgenciaDeb)
    {
        this._dsAgenciaDeb = dsAgenciaDeb;
    } //-- void setDsAgenciaDeb(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDeb'.
     * 
     * @param dsBancoDeb the value of field 'dsBancoDeb'.
     */
    public void setDsBancoDeb(java.lang.String dsBancoDeb)
    {
        this._dsBancoDeb = dsBancoDeb;
    } //-- void setDsBancoDeb(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaCadastroFavorecidos'.
     * 
     * @param dsFormaCadastroFavorecidos the value of field
     * 'dsFormaCadastroFavorecidos'.
     */
    public void setDsFormaCadastroFavorecidos(java.lang.String dsFormaCadastroFavorecidos)
    {
        this._dsFormaCadastroFavorecidos = dsFormaCadastroFavorecidos;
    } //-- void setDsFormaCadastroFavorecidos(java.lang.String) 

    /**
     * Sets the value of field 'dsIdDestinoRetorno'.
     * 
     * @param dsIdDestinoRetorno the value of field
     * 'dsIdDestinoRetorno'.
     */
    public void setDsIdDestinoRetorno(java.lang.String dsIdDestinoRetorno)
    {
        this._dsIdDestinoRetorno = dsIdDestinoRetorno;
    } //-- void setDsIdDestinoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorInclusaoFavorecido'.
     * 
     * @param dsIndicadorInclusaoFavorecido the value of field
     * 'dsIndicadorInclusaoFavorecido'.
     */
    public void setDsIndicadorInclusaoFavorecido(java.lang.String dsIndicadorInclusaoFavorecido)
    {
        this._dsIndicadorInclusaoFavorecido = dsIndicadorInclusaoFavorecido;
    } //-- void setDsIndicadorInclusaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoSolicitacao'.
     * 
     * @param dsMotivoSituacaoSolicitacao the value of field
     * 'dsMotivoSituacaoSolicitacao'.
     */
    public void setDsMotivoSituacaoSolicitacao(java.lang.String dsMotivoSituacaoSolicitacao)
    {
        this._dsMotivoSituacaoSolicitacao = dsMotivoSituacaoSolicitacao;
    } //-- void setDsMotivoSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacaoPagamento'.
     * 
     * @param dsSituacaoSolicitacaoPagamento the value of field
     * 'dsSituacaoSolicitacaoPagamento'.
     */
    public void setDsSituacaoSolicitacaoPagamento(java.lang.String dsSituacaoSolicitacaoPagamento)
    {
        this._dsSituacaoSolicitacaoPagamento = dsSituacaoSolicitacaoPagamento;
    } //-- void setDsSituacaoSolicitacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtAtendimento'.
     * 
     * @param dtAtendimento the value of field 'dtAtendimento'.
     */
    public void setDtAtendimento(java.lang.String dtAtendimento)
    {
        this._dtAtendimento = dtAtendimento;
    } //-- void setDtAtendimento(java.lang.String) 

    /**
     * Sets the value of field 'dtFimRastreabilidadeFavorecido'.
     * 
     * @param dtFimRastreabilidadeFavorecido the value of field
     * 'dtFimRastreabilidadeFavorecido'.
     */
    public void setDtFimRastreabilidadeFavorecido(java.lang.String dtFimRastreabilidadeFavorecido)
    {
        this._dtFimRastreabilidadeFavorecido = dtFimRastreabilidadeFavorecido;
    } //-- void setDtFimRastreabilidadeFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreabilidadeFavorecido'.
     * 
     * @param dtInicioRastreabilidadeFavorecido the value of field
     * 'dtInicioRastreabilidadeFavorecido'.
     */
    public void setDtInicioRastreabilidadeFavorecido(java.lang.String dtInicioRastreabilidadeFavorecido)
    {
        this._dtInicioRastreabilidadeFavorecido = dtInicioRastreabilidadeFavorecido;
    } //-- void setDtInicioRastreabilidadeFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtSolicitacao'.
     * 
     * @param dtSolicitacao the value of field 'dtSolicitacao'.
     */
    public void setDtSolicitacao(java.lang.String dtSolicitacao)
    {
        this._dtSolicitacao = dtSolicitacao;
    } //-- void setDtSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'hrAtendimento'.
     * 
     * @param hrAtendimento the value of field 'hrAtendimento'.
     */
    public void setHrAtendimento(java.lang.String hrAtendimento)
    {
        this._hrAtendimento = hrAtendimento;
    } //-- void setHrAtendimento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacao'.
     * 
     * @param hrSolicitacao the value of field 'hrSolicitacao'.
     */
    public void setHrSolicitacao(java.lang.String hrSolicitacao)
    {
        this._hrSolicitacao = hrSolicitacao;
    } //-- void setHrSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Sets the value of field 'numPercentualDescTarifa'.
     * 
     * @param numPercentualDescTarifa the value of field
     * 'numPercentualDescTarifa'.
     */
    public void setNumPercentualDescTarifa(java.math.BigDecimal numPercentualDescTarifa)
    {
        this._numPercentualDescTarifa = numPercentualDescTarifa;
    } //-- void setNumPercentualDescTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTarifaAtual'.
     * 
     * @param vlrTarifaAtual the value of field 'vlrTarifaAtual'.
     */
    public void setVlrTarifaAtual(java.math.BigDecimal vlrTarifaAtual)
    {
        this._vlrTarifaAtual = vlrTarifaAtual;
    } //-- void setVlrTarifaAtual(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolicitacaoRastreamentoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.response.DetalharSolicitacaoRastreamentoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.response.DetalharSolicitacaoRastreamentoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.response.DetalharSolicitacaoRastreamentoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.response.DetalharSolicitacaoRastreamentoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
