/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterfavorecido;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheHistoricoContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheHistoricoContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.DetalharHistoricoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.DetalharHistoricoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarFavorecidoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterFavorecido
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterFavorecidoService {
	
	 /**
 	 * Pesquisar favorecidos.
 	 *
 	 * @param entrada the entrada
 	 * @return the list< listar favorecido saida dt o>
 	 */
 	List<ListarFavorecidoSaidaDTO> pesquisarFavorecidos(ListarFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Confirmar bloquear favorecido.
 	 *
 	 * @param entrada the entrada
 	 * @return the bloquear favorecido saida dto
 	 */
 	BloquearFavorecidoSaidaDTO confirmarBloquearFavorecido(BloquearFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Consultar detalhe favorecido.
 	 *
 	 * @param entrada the entrada
 	 * @return the consultar detalhe favorecido saida dto
 	 */
 	ConsultarDetalheFavorecidoSaidaDTO consultarDetalheFavorecido(ConsultarDetalheFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Consultar historicos favorecidos.
 	 *
 	 * @param entrada the entrada
 	 * @return the list< consultar lista historico favorecido saida dt o>
 	 */
 	List<ConsultarListaHistoricoFavorecidoSaidaDTO> consultarHistoricosFavorecidos(ConsultarListaHistoricoFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Consultar detalhe historico favorecido.
 	 *
 	 * @param entrada the entrada
 	 * @return the detalhar historico favorecido saida dto
 	 */
 	DetalharHistoricoFavorecidoSaidaDTO consultarDetalheHistoricoFavorecido(DetalharHistoricoFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Consultar conta favorecido.
 	 *
 	 * @param entrada the entrada
 	 * @return the list< consultar lista conta favorecido saida dt o>
 	 */
 	List<ConsultarListaContaFavorecidoSaidaDTO> consultarContaFavorecido (ConsultarListaContaFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Consultar detalhe conta favorecido.
 	 *
 	 * @param entrada the entrada
 	 * @return the consultar detalhe conta favorecido saida dto
 	 */
 	ConsultarDetalheContaFavorecidoSaidaDTO consultarDetalheContaFavorecido (ConsultarDetalheContaFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Confirmar bloqueo conta favorecido.
 	 *
 	 * @param entrada the entrada
 	 * @return the bloquear conta favorecido saida dto
 	 */
 	BloquearContaFavorecidoSaidaDTO confirmarBloqueoContaFavorecido(BloquearContaFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Consultar historicos conta favorecido.
 	 *
 	 * @param entrada the entrada
 	 * @return the list< consultar lista historico conta favorecido saida dt o>
 	 */
 	List<ConsultarListaHistoricoContaFavorecidoSaidaDTO> consultarHistoricosContaFavorecido (ConsultarListaHistoricoContaFavorecidoEntradaDTO entrada);
	
	 /**
 	 * Consultar detalhe historico conta favorecido.
 	 *
 	 * @param entrada the entrada
 	 * @return the consultar detalhe historico conta favorecido saida dto
 	 */
 	ConsultarDetalheHistoricoContaFavorecidoSaidaDTO consultarDetalheHistoricoContaFavorecido(ConsultarDetalheHistoricoContaFavorecidoEntradaDTO entrada);
	
}

