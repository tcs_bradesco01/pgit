/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean;

/**
 * Nome: DetalharHistoricoPerfilTrocaArquivoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistoricoPerfilTrocaArquivoEntradaDTO{
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
	
	/** Atributo hrInclusaoRegistroHist. */
	private String hrInclusaoRegistroHist;

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo(){
		return cdPerfilTrocaArquivo;
	}

	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo){
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	/**
	 * Get: hrInclusaoRegistroHist.
	 *
	 * @return hrInclusaoRegistroHist
	 */
	public String getHrInclusaoRegistroHist(){
		return hrInclusaoRegistroHist;
	}

	/**
	 * Set: hrInclusaoRegistroHist.
	 *
	 * @param hrInclusaoRegistroHist the hr inclusao registro hist
	 */
	public void setHrInclusaoRegistroHist(String hrInclusaoRegistroHist){
		this.hrInclusaoRegistroHist = hrInclusaoRegistroHist;
	}
}