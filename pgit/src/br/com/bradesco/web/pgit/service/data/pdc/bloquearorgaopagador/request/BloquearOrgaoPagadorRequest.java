/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class BloquearOrgaoPagadorRequest.
 * 
 * @version $Revision$ $Date$
 */
public class BloquearOrgaoPagadorRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdOrgaoPagador
     */
    private int _cdOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdOrgaoPagador
     */
    private boolean _has_cdOrgaoPagador;

    /**
     * Field _cdSituacao
     */
    private int _cdSituacao = 0;

    /**
     * keeps track of state for field: _cdSituacao
     */
    private boolean _has_cdSituacao;

    /**
     * Field _dsMotivoBloqueio
     */
    private java.lang.String _dsMotivoBloqueio;


      //----------------/
     //- Constructors -/
    //----------------/

    public BloquearOrgaoPagadorRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.request.BloquearOrgaoPagadorRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOrgaoPagador
     * 
     */
    public void deleteCdOrgaoPagador()
    {
        this._has_cdOrgaoPagador= false;
    } //-- void deleteCdOrgaoPagador() 

    /**
     * Method deleteCdSituacao
     * 
     */
    public void deleteCdSituacao()
    {
        this._has_cdSituacao= false;
    } //-- void deleteCdSituacao() 

    /**
     * Returns the value of field 'cdOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdOrgaoPagador'.
     */
    public int getCdOrgaoPagador()
    {
        return this._cdOrgaoPagador;
    } //-- int getCdOrgaoPagador() 

    /**
     * Returns the value of field 'cdSituacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacao'.
     */
    public int getCdSituacao()
    {
        return this._cdSituacao;
    } //-- int getCdSituacao() 

    /**
     * Returns the value of field 'dsMotivoBloqueio'.
     * 
     * @return String
     * @return the value of field 'dsMotivoBloqueio'.
     */
    public java.lang.String getDsMotivoBloqueio()
    {
        return this._dsMotivoBloqueio;
    } //-- java.lang.String getDsMotivoBloqueio() 

    /**
     * Method hasCdOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrgaoPagador()
    {
        return this._has_cdOrgaoPagador;
    } //-- boolean hasCdOrgaoPagador() 

    /**
     * Method hasCdSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacao()
    {
        return this._has_cdSituacao;
    } //-- boolean hasCdSituacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOrgaoPagador'.
     * 
     * @param cdOrgaoPagador the value of field 'cdOrgaoPagador'.
     */
    public void setCdOrgaoPagador(int cdOrgaoPagador)
    {
        this._cdOrgaoPagador = cdOrgaoPagador;
        this._has_cdOrgaoPagador = true;
    } //-- void setCdOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdSituacao'.
     * 
     * @param cdSituacao the value of field 'cdSituacao'.
     */
    public void setCdSituacao(int cdSituacao)
    {
        this._cdSituacao = cdSituacao;
        this._has_cdSituacao = true;
    } //-- void setCdSituacao(int) 

    /**
     * Sets the value of field 'dsMotivoBloqueio'.
     * 
     * @param dsMotivoBloqueio the value of field 'dsMotivoBloqueio'
     */
    public void setDsMotivoBloqueio(java.lang.String dsMotivoBloqueio)
    {
        this._dsMotivoBloqueio = dsMotivoBloqueio;
    } //-- void setDsMotivoBloqueio(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return BloquearOrgaoPagadorRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.request.BloquearOrgaoPagadorRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.request.BloquearOrgaoPagadorRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.request.BloquearOrgaoPagadorRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.request.BloquearOrgaoPagadorRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
