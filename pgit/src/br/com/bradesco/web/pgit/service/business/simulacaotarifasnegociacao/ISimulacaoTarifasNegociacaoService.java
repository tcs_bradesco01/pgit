/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ConsultarConfigPadraoAtribTipoServModEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ConsultarConfigPadraoAtribTipoServModSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ConsultarConfigTipoServicoModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ConsultarConfigTipoServicoModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.DetalharSimulacaoTarifaNegociacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.DetalharSimulacaoTarifaNegociacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.EfetuarValidacaoConfigAtribModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.EfetuarValidacaoConfigAtribModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: SimulacaoTarifasNegociacao
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISimulacaoTarifasNegociacaoService {

	
	/**
	 * Consultar lista operacao tarifa tipo servico.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< lista operacao tarifa tipo servico saida dt o>
	 */
	List<ListaOperacaoTarifaTipoServicoSaidaDTO> consultarListaOperacaoTarifaTipoServico(ListaOperacaoTarifaTipoServicoEntradaDTO entradaDTO);
	
	/**
	 * Detalhar simulacao tarifa negociacao.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar simulacao tarifa negociacao saida dto
	 */
	DetalharSimulacaoTarifaNegociacaoSaidaDTO detalharSimulacaoTarifaNegociacao(DetalharSimulacaoTarifaNegociacaoEntradaDTO entradaDTO);

	/**
	 * Efetuar Validacao Config Atrib Modalidade.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the efetuar validacao config atrib modalidade saida dto
	 */
	EfetuarValidacaoConfigAtribModalidadeSaidaDTO efetuarValidacaoConfigAtribModalidade(EfetuarValidacaoConfigAtribModalidadeEntradaDTO entradaDTO);

	/**
	 * Consultar Config Tipo Servico Modalidade.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the consultar config tipo servico modalidade saida dto
	 */
	ConsultarConfigTipoServicoModalidadeSaidaDTO consultarConfigTipoServicoModalidade(ConsultarConfigTipoServicoModalidadeEntradaDTO entradaDTO);
	
	/**
	 * Consultar Config Padrao Atrib Tipo Serv Mod.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the consultar config padrao atrib tipo serv mod saida dto
	 */
	ConsultarConfigPadraoAtribTipoServModSaidaDTO consultarConfigPadraoAtribTipoServMod(ConsultarConfigPadraoAtribTipoServModEntradaDTO entradaDTO);
	
}

