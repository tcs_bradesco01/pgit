/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

import br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO {
	
    /** Atributo cdTransmissaoAutomatica. */
    private String cdTransmissaoAutomatica;
    
    /** Atributo nrCpf. */
    private Long nrCpf;
    
    /** Atributo cdControleCpf. */
    private String cdControleCpf;
    
    /** Atributo cdDigitoCpf. */
    private Integer cdDigitoCpf;
    
    /** Atributo dsEmpresa. */
    private String dsEmpresa;
    
    /** Atributo cdTipoContaVinculo. */
    private String cdTipoContaVinculo;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExter. */
    private String cdUsuarioInclusaoExter;
    
    /** Atributo dtInclusa. */
    private String dtInclusa;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdOperCanalInclusao. */
    private String cdOperCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExter. */
    private String cdUsuarioManutencaoExter;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdOperCanalManutencao. */
    private String cdOperCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
	/**
	 * Consultar det vinculo cta salario empresa saida dto.
	 */
	public ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO() {
		super();
	}

	/**
	 * Consultar det vinculo cta salario empresa saida dto.
	 *
	 * @param ocorrencia the ocorrencia
	 */
	public ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO(Ocorrencias ocorrencia) {
		this.cdTransmissaoAutomatica = ocorrencia.getCdTransmissaoAutomatica();
		this.nrCpf = ocorrencia.getNrCpf();
		this.cdControleCpf = ocorrencia.getCdControleCpf();
		this.cdDigitoCpf = ocorrencia.getCdDigitoCpf();
		this.dsEmpresa = ocorrencia.getDsEmpresa();
		this.cdTipoContaVinculo = ocorrencia.getCdTipoContaVinculo();
		this.cdUsuarioInclusao = ocorrencia.getCdUsuarioInclusao();
		this.cdUsuarioInclusaoExter = ocorrencia.getCdUsuarioInclusaoExter();
		this.dtInclusa = ocorrencia.getDtInclusa();
		this.hrInclusao = ocorrencia.getHrInclusao();
		this.cdOperCanalInclusao = ocorrencia.getCdOperCanalInclusao();
		this.cdTipoCanalInclusao = ocorrencia.getCdTipoCanalInclusao();
		this.dsCanalInclusao = ocorrencia.getDsCanalInclusao();
		this.cdUsuarioManutencao = ocorrencia.getCdUsuarioManutencao();
		this.cdUsuarioManutencaoExter = ocorrencia.getCdUsuarioManutencaoExter();
		this.dtManutencao = ocorrencia.getDtManutencao();
		this.hrManutencao = ocorrencia.getHrManutencao();
		this.cdOperCanalManutencao = ocorrencia.getCdOperCanalManutencao();
		this.cdTipoCanalManutencao = ocorrencia.getCdTipoCanalManutencao();
		this.dsCanalManutencao = ocorrencia.getDsCanalManutencao();
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj(){		
		
		if (nrCpf == null || nrCpf.longValue() == 0l){
			return "";
		}
		
		if (cdControleCpf != null && cdControleCpf != null && !cdControleCpf.trim().equals("")) {
			
			return CpfCnpjUtils.formatCpfCnpjCompleto(nrCpf, Integer.valueOf(cdControleCpf), cdDigitoCpf);
		}else {
			
			return CpfCnpjUtils.formatCpfCnpjCompleto(nrCpf, 0, cdDigitoCpf);
		}
	}
	
	/**
	 * Get: canalInclusao.
	 *
	 * @return canalInclusao
	 */
	public String getCanalInclusao(){
		return cdTipoCanalInclusao == 0 ? dsCanalInclusao : cdTipoCanalInclusao + "-" + dsCanalInclusao;
	}
	
	/**
	 * Get: canalManutencao.
	 *
	 * @return canalManutencao
	 */
	public String getCanalManutencao(){
		return cdTipoCanalManutencao == 0 ? dsCanalManutencao : cdTipoCanalManutencao + "-" + dsCanalManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao(){
	    String temp = "";
	    temp = (dtInclusa == null || dtInclusa.equals("0. 0.   0")) ? "" : dtInclusa + " ";
	    temp += (hrInclusao == null || hrInclusao.equals("0: 0: 0")) ? "" : hrInclusao;
	    
	    return temp;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao(){
	    String temp = "";
	    temp = (dtManutencao == null || dtManutencao.equals("0. 0.   0")) ? "" : dtManutencao + " ";
	    temp += (hrManutencao == null || hrManutencao.equals("0: 0: 0")) ? "" : hrManutencao;
	    
	    return temp;
	}
	
	/**
	 * Get: cdControleCpf.
	 *
	 * @return cdControleCpf
	 */
	public String getCdControleCpf() {
		return cdControleCpf;
	}


	/**
	 * Set: cdControleCpf.
	 *
	 * @param cdControleCpf the cd controle cpf
	 */
	public void setCdControleCpf(String cdControleCpf) {
		this.cdControleCpf = cdControleCpf;
	}

	/**
	 * Get: cdDigitoCpf.
	 *
	 * @return cdDigitoCpf
	 */
	public Integer getCdDigitoCpf() {
		return cdDigitoCpf;
	}

	/**
	 * Set: cdDigitoCpf.
	 *
	 * @param cdDigitoCpf the cd digito cpf
	 */
	public void setCdDigitoCpf(Integer cdDigitoCpf) {
		this.cdDigitoCpf = cdDigitoCpf;
	}

	/**
	 * Get: cdOperCanalInclusao.
	 *
	 * @return cdOperCanalInclusao
	 */
	public String getCdOperCanalInclusao() {
		return cdOperCanalInclusao;
	}

	/**
	 * Set: cdOperCanalInclusao.
	 *
	 * @param cdOperCanalInclusao the cd oper canal inclusao
	 */
	public void setCdOperCanalInclusao(String cdOperCanalInclusao) {
		this.cdOperCanalInclusao = cdOperCanalInclusao;
	}

	/**
	 * Get: cdOperCanalManutencao.
	 *
	 * @return cdOperCanalManutencao
	 */
	public String getCdOperCanalManutencao() {
		return cdOperCanalManutencao;
	}

	/**
	 * Set: cdOperCanalManutencao.
	 *
	 * @param cdOperCanalManutencao the cd oper canal manutencao
	 */
	public void setCdOperCanalManutencao(String cdOperCanalManutencao) {
		this.cdOperCanalManutencao = cdOperCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTransmissaoAutomatica.
	 *
	 * @return cdTransmissaoAutomatica
	 */
	public String getCdTransmissaoAutomatica() {
		return cdTransmissaoAutomatica;
	}

	/**
	 * Set: cdTransmissaoAutomatica.
	 *
	 * @param cdTransmissaoAutomatica the cd transmissao automatica
	 */
	public void setCdTransmissaoAutomatica(String cdTransmissaoAutomatica) {
		this.cdTransmissaoAutomatica = cdTransmissaoAutomatica;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExter.
	 *
	 * @return cdUsuarioInclusaoExter
	 */
	public String getCdUsuarioInclusaoExter() {
		return cdUsuarioInclusaoExter;
	}

	/**
	 * Set: cdUsuarioInclusaoExter.
	 *
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 */
	public void setCdUsuarioInclusaoExter(String cdUsuarioInclusaoExter) {
		this.cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExter.
	 *
	 * @return cdUsuarioManutencaoExter
	 */
	public String getCdUsuarioManutencaoExter() {
		return cdUsuarioManutencaoExter;
	}

	/**
	 * Set: cdUsuarioManutencaoExter.
	 *
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 */
	public void setCdUsuarioManutencaoExter(String cdUsuarioManutencaoExter) {
		this.cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dtInclusa.
	 *
	 * @return dtInclusa
	 */
	public String getDtInclusa() {
		return dtInclusa;
	}

	/**
	 * Set: dtInclusa.
	 *
	 * @param dtInclusa the dt inclusa
	 */
	public void setDtInclusa(String dtInclusa) {
		this.dtInclusa = dtInclusa;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: nrCpf.
	 *
	 * @return nrCpf
	 */
	public Long getNrCpf() {
		return nrCpf;
	}

	/**
	 * Set: nrCpf.
	 *
	 * @param nrCpf the nr cpf
	 */
	public void setNrCpf(Long nrCpf) {
		this.nrCpf = nrCpf;
	}

	/**
	 * Get: cdTipoContaVinculo.
	 *
	 * @return cdTipoContaVinculo
	 */
	public String getCdTipoContaVinculo() {
		return cdTipoContaVinculo;
	}

	/**
	 * Set: cdTipoContaVinculo.
	 *
	 * @param cdTipoContaVinculo the cd tipo conta vinculo
	 */
	public void setCdTipoContaVinculo(String cdTipoContaVinculo) {
		this.cdTipoContaVinculo = cdTipoContaVinculo;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
}
