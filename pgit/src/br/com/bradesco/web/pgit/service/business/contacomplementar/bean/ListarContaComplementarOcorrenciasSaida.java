package br.com.bradesco.web.pgit.service.business.contacomplementar.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

public class ListarContaComplementarOcorrenciasSaida {
	
	private Integer cdContaComplementar = null;
	private Long cdCorpoCnpjCpf = null;
	private Integer cdFilialCpfCnpj = null;
	
	private Integer cdCtrlCpfCnpj = null;
	
	private String dsNomeRazaoSocial = null;
	private Long cdPessoaJuridicaContratoConta = null;
	private Integer cdTipoContratoConta = null;
	
	private Long nrSequenciaContratoConta = null;
	
	private Integer cdBanco = null;
	private String dsBanco = null;
	
	private Integer cdAgencia = null;
	private String dsAgencia = null;
	
	private Long cdConta = null;
	private String cdDigitoConta = null;
	
    /**
     * Instancia um novo ListarContaComplementarOcorrenciasSaida
     *
     * @see
     */
    public ListarContaComplementarOcorrenciasSaida() {
        super();
    }

	public Integer getCdContaComplementar() {
		return cdContaComplementar;
	}

	public void setCdContaComplementar(Integer cdContaComplementar) {
		this.cdContaComplementar = cdContaComplementar;
	}

	public Long getCdCorpoCnpjCpf() {
		return cdCorpoCnpjCpf;
	}

	public void setCdCorpoCnpjCpf(Long cdCorpoCnpjCpf) {
		this.cdCorpoCnpjCpf = cdCorpoCnpjCpf;
	}

	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}

	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}

	public Integer getCdCtrlCpfCnpj() {
		return cdCtrlCpfCnpj;
	}

	public void setCdCtrlCpfCnpj(Integer cdCtrlCpfCnpj) {
		this.cdCtrlCpfCnpj = cdCtrlCpfCnpj;
	}

	public String getDsNomeRazaoSocial() {
		return dsNomeRazaoSocial;
	}

	public void setDsNomeRazaoSocial(String dsNomeRazaoSocial) {
		this.dsNomeRazaoSocial = dsNomeRazaoSocial;
	}

	public Long getCdPessoaJuridicaContratoConta() {
		return cdPessoaJuridicaContratoConta;
	}

	public void setCdPessoaJuridicaContratoConta(Long cdPessoaJuridicaContratoConta) {
		this.cdPessoaJuridicaContratoConta = cdPessoaJuridicaContratoConta;
	}

	public Integer getCdTipoContratoConta() {
		return cdTipoContratoConta;
	}

	public void setCdTipoContratoConta(Integer cdTipoContratoConta) {
		this.cdTipoContratoConta = cdTipoContratoConta;
	}

	public Long getNrSequenciaContratoConta() {
		return nrSequenciaContratoConta;
	}

	public void setNrSequenciaContratoConta(Long nrSequenciaContratoConta) {
		this.nrSequenciaContratoConta = nrSequenciaContratoConta;
	}

	public Integer getCdBanco() {
		return cdBanco;
	}

	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	public String getDsBanco() {
		return dsBanco;
	}

	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}

	public Integer getCdAgencia() {
		return cdAgencia;
	}

	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	public String getDsAgencia() {
		return dsAgencia;
	}

	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	public Long getCdConta() {
		return cdConta;
	}

	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	public String getCnpjOuCpfFormatado() {
		return CpfCnpjUtils.formatCpfCnpjCompleto(cdCorpoCnpjCpf, cdFilialCpfCnpj, cdCtrlCpfCnpj);
	}

	public String getContaDigito() {
		return Long.toString(cdConta).trim()+ "-" + cdDigitoConta;
	}
}