/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarServicoPagtoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarServicoPagtoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maximoOcorrencias
     */
    private int _maximoOcorrencias = 0;

    /**
     * keeps track of state for field: _maximoOcorrencias
     */
    private boolean _has_maximoOcorrencias;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarServicoPagtoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.request.ConsultarServicoPagtoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteMaximoOcorrencias
     * 
     */
    public void deleteMaximoOcorrencias()
    {
        this._has_maximoOcorrencias= false;
    } //-- void deleteMaximoOcorrencias() 

    /**
     * Returns the value of field 'maximoOcorrencias'.
     * 
     * @return int
     * @return the value of field 'maximoOcorrencias'.
     */
    public int getMaximoOcorrencias()
    {
        return this._maximoOcorrencias;
    } //-- int getMaximoOcorrencias() 

    /**
     * Method hasMaximoOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaximoOcorrencias()
    {
        return this._has_maximoOcorrencias;
    } //-- boolean hasMaximoOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'maximoOcorrencias'.
     * 
     * @param maximoOcorrencias the value of field
     * 'maximoOcorrencias'.
     */
    public void setMaximoOcorrencias(int maximoOcorrencias)
    {
        this._maximoOcorrencias = maximoOcorrencias;
        this._has_maximoOcorrencias = true;
    } //-- void setMaximoOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarServicoPagtoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.request.ConsultarServicoPagtoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.request.ConsultarServicoPagtoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.request.ConsultarServicoPagtoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.request.ConsultarServicoPagtoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
