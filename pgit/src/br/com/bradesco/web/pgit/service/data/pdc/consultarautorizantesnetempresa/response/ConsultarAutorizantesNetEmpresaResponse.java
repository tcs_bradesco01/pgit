/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarAutorizantesNetEmpresaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarAutorizantesNetEmpresaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridCont
     */
    private long _cdPessoaJuridCont = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridCont
     */
    private boolean _has_cdPessoaJuridCont;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _dtEfetivacaoPagamento
     */
    private java.lang.String _dtEfetivacaoPagamento;

    /**
     * Field _cdBancoDescricao
     */
    private int _cdBancoDescricao = 0;

    /**
     * keeps track of state for field: _cdBancoDescricao
     */
    private boolean _has_cdBancoDescricao;

    /**
     * Field _cdAgenciaBancariaDescricao
     */
    private int _cdAgenciaBancariaDescricao = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancariaDescricao
     */
    private boolean _has_cdAgenciaBancariaDescricao;

    /**
     * Field _cdContaBancariaDescricao
     */
    private long _cdContaBancariaDescricao = 0;

    /**
     * keeps track of state for field: _cdContaBancariaDescricao
     */
    private boolean _has_cdContaBancariaDescricao;

    /**
     * Field _cdAgendadosPagoNaoPago
     */
    private int _cdAgendadosPagoNaoPago = 0;

    /**
     * keeps track of state for field: _cdAgendadosPagoNaoPago
     */
    private boolean _has_cdAgendadosPagoNaoPago;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _nrArquivoRemssaPagamento
     */
    private long _nrArquivoRemssaPagamento = 0;

    /**
     * keeps track of state for field: _nrArquivoRemssaPagamento
     */
    private boolean _has_nrArquivoRemssaPagamento;

    /**
     * Field _cdRejeicaoLoteComprovante
     */
    private long _cdRejeicaoLoteComprovante = 0;

    /**
     * keeps track of state for field: _cdRejeicaoLoteComprovante
     */
    private boolean _has_cdRejeicaoLoteComprovante;

    /**
     * Field _cdListaDebitoPagamento
     */
    private long _cdListaDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdListaDebitoPagamento
     */
    private boolean _has_cdListaDebitoPagamento;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _dsTipoCanal
     */
    private java.lang.String _dsTipoCanal;

    /**
     * Field _vlPagamento
     */
    private java.math.BigDecimal _vlPagamento = new java.math.BigDecimal("0");

    /**
     * Field _dsModalidade
     */
    private int _dsModalidade = 0;

    /**
     * keeps track of state for field: _dsModalidade
     */
    private boolean _has_dsModalidade;

    /**
     * Field _dsBancoDebito
     */
    private java.lang.String _dsBancoDebito;

    /**
     * Field _dsAgenciaDebito
     */
    private java.lang.String _dsAgenciaDebito;

    /**
     * Field _dsTipoContaDebito
     */
    private java.lang.String _dsTipoContaDebito;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _dsSituacaoContrato
     */
    private java.lang.String _dsSituacaoContrato;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _dtVencimento
     */
    private java.lang.String _dtVencimento;

    /**
     * Field _cdSituacaoPagamentoCliente
     */
    private int _cdSituacaoPagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdSituacaoPagamentoCliente
     */
    private boolean _has_cdSituacaoPagamentoCliente;

    /**
     * Field _dsSituacaoPagamentoCliente
     */
    private java.lang.String _dsSituacaoPagamentoCliente;

    /**
     * Field _cdMotivoPagamentoCliente
     */
    private int _cdMotivoPagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdMotivoPagamentoCliente
     */
    private boolean _has_cdMotivoPagamentoCliente;

    /**
     * Field _dsMotivoPagamentoCliente
     */
    private java.lang.String _dsMotivoPagamentoCliente;

    /**
     * Field _cdBancoCredito
     */
    private int _cdBancoCredito = 0;

    /**
     * keeps track of state for field: _cdBancoCredito
     */
    private boolean _has_cdBancoCredito;

    /**
     * Field _dsBancoCredito
     */
    private java.lang.String _dsBancoCredito;

    /**
     * Field _cdAgenciaBancariaCredito
     */
    private int _cdAgenciaBancariaCredito = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancariaCredito
     */
    private boolean _has_cdAgenciaBancariaCredito;

    /**
     * Field _cdDigitoAgenciaCredito
     */
    private int _cdDigitoAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaCredito
     */
    private boolean _has_cdDigitoAgenciaCredito;

    /**
     * Field _dsAgenciaCredito
     */
    private java.lang.String _dsAgenciaCredito;

    /**
     * Field _cdContaBancariaCredito
     */
    private long _cdContaBancariaCredito = 0;

    /**
     * keeps track of state for field: _cdContaBancariaCredito
     */
    private boolean _has_cdContaBancariaCredito;

    /**
     * Field _cdDigitoContaCredito
     */
    private java.lang.String _cdDigitoContaCredito;

    /**
     * Field _cdTipoContaCredito
     */
    private java.lang.String _cdTipoContaCredito;

    /**
     * Field _cdTipoIsncricaoFavorecido
     */
    private java.lang.String _cdTipoIsncricaoFavorecido;

    /**
     * Field _cdCtciaInscricaoFavorecido
     */
    private long _cdCtciaInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdCtciaInscricaoFavorecido
     */
    private boolean _has_cdCtciaInscricaoFavorecido;

    /**
     * Field _dtCreditoPagamento
     */
    private java.lang.String _dtCreditoPagamento;

    /**
     * Field _numeroConsulta
     */
    private int _numeroConsulta = 0;

    /**
     * keeps track of state for field: _numeroConsulta
     */
    private boolean _has_numeroConsulta;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarAutorizantesNetEmpresaResponse() 
     {
        super();
        setVlPagamento(new java.math.BigDecimal("0"));
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.ConsultarAutorizantesNetEmpresaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 100)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 100");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 100)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 100");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias) 

    /**
     * Method deleteCdAgenciaBancariaCredito
     * 
     */
    public void deleteCdAgenciaBancariaCredito()
    {
        this._has_cdAgenciaBancariaCredito= false;
    } //-- void deleteCdAgenciaBancariaCredito() 

    /**
     * Method deleteCdAgenciaBancariaDescricao
     * 
     */
    public void deleteCdAgenciaBancariaDescricao()
    {
        this._has_cdAgenciaBancariaDescricao= false;
    } //-- void deleteCdAgenciaBancariaDescricao() 

    /**
     * Method deleteCdAgendadosPagoNaoPago
     * 
     */
    public void deleteCdAgendadosPagoNaoPago()
    {
        this._has_cdAgendadosPagoNaoPago= false;
    } //-- void deleteCdAgendadosPagoNaoPago() 

    /**
     * Method deleteCdBancoCredito
     * 
     */
    public void deleteCdBancoCredito()
    {
        this._has_cdBancoCredito= false;
    } //-- void deleteCdBancoCredito() 

    /**
     * Method deleteCdBancoDescricao
     * 
     */
    public void deleteCdBancoDescricao()
    {
        this._has_cdBancoDescricao= false;
    } //-- void deleteCdBancoDescricao() 

    /**
     * Method deleteCdContaBancariaCredito
     * 
     */
    public void deleteCdContaBancariaCredito()
    {
        this._has_cdContaBancariaCredito= false;
    } //-- void deleteCdContaBancariaCredito() 

    /**
     * Method deleteCdContaBancariaDescricao
     * 
     */
    public void deleteCdContaBancariaDescricao()
    {
        this._has_cdContaBancariaDescricao= false;
    } //-- void deleteCdContaBancariaDescricao() 

    /**
     * Method deleteCdCtciaInscricaoFavorecido
     * 
     */
    public void deleteCdCtciaInscricaoFavorecido()
    {
        this._has_cdCtciaInscricaoFavorecido= false;
    } //-- void deleteCdCtciaInscricaoFavorecido() 

    /**
     * Method deleteCdDigitoAgenciaCredito
     * 
     */
    public void deleteCdDigitoAgenciaCredito()
    {
        this._has_cdDigitoAgenciaCredito= false;
    } //-- void deleteCdDigitoAgenciaCredito() 

    /**
     * Method deleteCdListaDebitoPagamento
     * 
     */
    public void deleteCdListaDebitoPagamento()
    {
        this._has_cdListaDebitoPagamento= false;
    } //-- void deleteCdListaDebitoPagamento() 

    /**
     * Method deleteCdMotivoPagamentoCliente
     * 
     */
    public void deleteCdMotivoPagamentoCliente()
    {
        this._has_cdMotivoPagamentoCliente= false;
    } //-- void deleteCdMotivoPagamentoCliente() 

    /**
     * Method deleteCdPessoaJuridCont
     * 
     */
    public void deleteCdPessoaJuridCont()
    {
        this._has_cdPessoaJuridCont= false;
    } //-- void deleteCdPessoaJuridCont() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdRejeicaoLoteComprovante
     * 
     */
    public void deleteCdRejeicaoLoteComprovante()
    {
        this._has_cdRejeicaoLoteComprovante= false;
    } //-- void deleteCdRejeicaoLoteComprovante() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdSituacaoPagamentoCliente
     * 
     */
    public void deleteCdSituacaoPagamentoCliente()
    {
        this._has_cdSituacaoPagamentoCliente= false;
    } //-- void deleteCdSituacaoPagamentoCliente() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteDsModalidade
     * 
     */
    public void deleteDsModalidade()
    {
        this._has_dsModalidade= false;
    } //-- void deleteDsModalidade() 

    /**
     * Method deleteNrArquivoRemssaPagamento
     * 
     */
    public void deleteNrArquivoRemssaPagamento()
    {
        this._has_nrArquivoRemssaPagamento= false;
    } //-- void deleteNrArquivoRemssaPagamento() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroConsulta
     * 
     */
    public void deleteNumeroConsulta()
    {
        this._has_numeroConsulta= false;
    } //-- void deleteNumeroConsulta() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdAgenciaBancariaCredito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancariaCredito'.
     */
    public int getCdAgenciaBancariaCredito()
    {
        return this._cdAgenciaBancariaCredito;
    } //-- int getCdAgenciaBancariaCredito() 

    /**
     * Returns the value of field 'cdAgenciaBancariaDescricao'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancariaDescricao'.
     */
    public int getCdAgenciaBancariaDescricao()
    {
        return this._cdAgenciaBancariaDescricao;
    } //-- int getCdAgenciaBancariaDescricao() 

    /**
     * Returns the value of field 'cdAgendadosPagoNaoPago'.
     * 
     * @return int
     * @return the value of field 'cdAgendadosPagoNaoPago'.
     */
    public int getCdAgendadosPagoNaoPago()
    {
        return this._cdAgendadosPagoNaoPago;
    } //-- int getCdAgendadosPagoNaoPago() 

    /**
     * Returns the value of field 'cdBancoCredito'.
     * 
     * @return int
     * @return the value of field 'cdBancoCredito'.
     */
    public int getCdBancoCredito()
    {
        return this._cdBancoCredito;
    } //-- int getCdBancoCredito() 

    /**
     * Returns the value of field 'cdBancoDescricao'.
     * 
     * @return int
     * @return the value of field 'cdBancoDescricao'.
     */
    public int getCdBancoDescricao()
    {
        return this._cdBancoDescricao;
    } //-- int getCdBancoDescricao() 

    /**
     * Returns the value of field 'cdContaBancariaCredito'.
     * 
     * @return long
     * @return the value of field 'cdContaBancariaCredito'.
     */
    public long getCdContaBancariaCredito()
    {
        return this._cdContaBancariaCredito;
    } //-- long getCdContaBancariaCredito() 

    /**
     * Returns the value of field 'cdContaBancariaDescricao'.
     * 
     * @return long
     * @return the value of field 'cdContaBancariaDescricao'.
     */
    public long getCdContaBancariaDescricao()
    {
        return this._cdContaBancariaDescricao;
    } //-- long getCdContaBancariaDescricao() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdCtciaInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdCtciaInscricaoFavorecido'.
     */
    public long getCdCtciaInscricaoFavorecido()
    {
        return this._cdCtciaInscricaoFavorecido;
    } //-- long getCdCtciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaCredito'.
     */
    public int getCdDigitoAgenciaCredito()
    {
        return this._cdDigitoAgenciaCredito;
    } //-- int getCdDigitoAgenciaCredito() 

    /**
     * Returns the value of field 'cdDigitoContaCredito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaCredito'.
     */
    public java.lang.String getCdDigitoContaCredito()
    {
        return this._cdDigitoContaCredito;
    } //-- java.lang.String getCdDigitoContaCredito() 

    /**
     * Returns the value of field 'cdListaDebitoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdListaDebitoPagamento'.
     */
    public long getCdListaDebitoPagamento()
    {
        return this._cdListaDebitoPagamento;
    } //-- long getCdListaDebitoPagamento() 

    /**
     * Returns the value of field 'cdMotivoPagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdMotivoPagamentoCliente'.
     */
    public int getCdMotivoPagamentoCliente()
    {
        return this._cdMotivoPagamentoCliente;
    } //-- int getCdMotivoPagamentoCliente() 

    /**
     * Returns the value of field 'cdPessoaJuridCont'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridCont'.
     */
    public long getCdPessoaJuridCont()
    {
        return this._cdPessoaJuridCont;
    } //-- long getCdPessoaJuridCont() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdRejeicaoLoteComprovante'.
     * 
     * @return long
     * @return the value of field 'cdRejeicaoLoteComprovante'.
     */
    public long getCdRejeicaoLoteComprovante()
    {
        return this._cdRejeicaoLoteComprovante;
    } //-- long getCdRejeicaoLoteComprovante() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdSituacaoPagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPagamentoCliente'.
     */
    public int getCdSituacaoPagamentoCliente()
    {
        return this._cdSituacaoPagamentoCliente;
    } //-- int getCdSituacaoPagamentoCliente() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoContaCredito'.
     * 
     * @return String
     * @return the value of field 'cdTipoContaCredito'.
     */
    public java.lang.String getCdTipoContaCredito()
    {
        return this._cdTipoContaCredito;
    } //-- java.lang.String getCdTipoContaCredito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdTipoIsncricaoFavorecido'.
     */
    public java.lang.String getCdTipoIsncricaoFavorecido()
    {
        return this._cdTipoIsncricaoFavorecido;
    } //-- java.lang.String getCdTipoIsncricaoFavorecido() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaCredito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaCredito'.
     */
    public java.lang.String getDsAgenciaCredito()
    {
        return this._dsAgenciaCredito;
    } //-- java.lang.String getDsAgenciaCredito() 

    /**
     * Returns the value of field 'dsAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDebito'.
     */
    public java.lang.String getDsAgenciaDebito()
    {
        return this._dsAgenciaDebito;
    } //-- java.lang.String getDsAgenciaDebito() 

    /**
     * Returns the value of field 'dsBancoCredito'.
     * 
     * @return String
     * @return the value of field 'dsBancoCredito'.
     */
    public java.lang.String getDsBancoCredito()
    {
        return this._dsBancoCredito;
    } //-- java.lang.String getDsBancoCredito() 

    /**
     * Returns the value of field 'dsBancoDebito'.
     * 
     * @return String
     * @return the value of field 'dsBancoDebito'.
     */
    public java.lang.String getDsBancoDebito()
    {
        return this._dsBancoDebito;
    } //-- java.lang.String getDsBancoDebito() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return int
     * @return the value of field 'dsModalidade'.
     */
    public int getDsModalidade()
    {
        return this._dsModalidade;
    } //-- int getDsModalidade() 

    /**
     * Returns the value of field 'dsMotivoPagamentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsMotivoPagamentoCliente'.
     */
    public java.lang.String getDsMotivoPagamentoCliente()
    {
        return this._dsMotivoPagamentoCliente;
    } //-- java.lang.String getDsMotivoPagamentoCliente() 

    /**
     * Returns the value of field 'dsSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContrato'.
     */
    public java.lang.String getDsSituacaoContrato()
    {
        return this._dsSituacaoContrato;
    } //-- java.lang.String getDsSituacaoContrato() 

    /**
     * Returns the value of field 'dsSituacaoPagamentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPagamentoCliente'.
     */
    public java.lang.String getDsSituacaoPagamentoCliente()
    {
        return this._dsSituacaoPagamentoCliente;
    } //-- java.lang.String getDsSituacaoPagamentoCliente() 

    /**
     * Returns the value of field 'dsTipoCanal'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanal'.
     */
    public java.lang.String getDsTipoCanal()
    {
        return this._dsTipoCanal;
    } //-- java.lang.String getDsTipoCanal() 

    /**
     * Returns the value of field 'dsTipoContaDebito'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaDebito'.
     */
    public java.lang.String getDsTipoContaDebito()
    {
        return this._dsTipoContaDebito;
    } //-- java.lang.String getDsTipoContaDebito() 

    /**
     * Returns the value of field 'dtCreditoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamento'.
     */
    public java.lang.String getDtCreditoPagamento()
    {
        return this._dtCreditoPagamento;
    } //-- java.lang.String getDtCreditoPagamento() 

    /**
     * Returns the value of field 'dtEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtEfetivacaoPagamento'.
     */
    public java.lang.String getDtEfetivacaoPagamento()
    {
        return this._dtEfetivacaoPagamento;
    } //-- java.lang.String getDtEfetivacaoPagamento() 

    /**
     * Returns the value of field 'dtVencimento'.
     * 
     * @return String
     * @return the value of field 'dtVencimento'.
     */
    public java.lang.String getDtVencimento()
    {
        return this._dtVencimento;
    } //-- java.lang.String getDtVencimento() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrArquivoRemssaPagamento'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemssaPagamento'.
     */
    public long getNrArquivoRemssaPagamento()
    {
        return this._nrArquivoRemssaPagamento;
    } //-- long getNrArquivoRemssaPagamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroConsulta'.
     * 
     * @return int
     * @return the value of field 'numeroConsulta'.
     */
    public int getNumeroConsulta()
    {
        return this._numeroConsulta;
    } //-- int getNumeroConsulta() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'vlPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamento'.
     */
    public java.math.BigDecimal getVlPagamento()
    {
        return this._vlPagamento;
    } //-- java.math.BigDecimal getVlPagamento() 

    /**
     * Method hasCdAgenciaBancariaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancariaCredito()
    {
        return this._has_cdAgenciaBancariaCredito;
    } //-- boolean hasCdAgenciaBancariaCredito() 

    /**
     * Method hasCdAgenciaBancariaDescricao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancariaDescricao()
    {
        return this._has_cdAgenciaBancariaDescricao;
    } //-- boolean hasCdAgenciaBancariaDescricao() 

    /**
     * Method hasCdAgendadosPagoNaoPago
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendadosPagoNaoPago()
    {
        return this._has_cdAgendadosPagoNaoPago;
    } //-- boolean hasCdAgendadosPagoNaoPago() 

    /**
     * Method hasCdBancoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoCredito()
    {
        return this._has_cdBancoCredito;
    } //-- boolean hasCdBancoCredito() 

    /**
     * Method hasCdBancoDescricao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDescricao()
    {
        return this._has_cdBancoDescricao;
    } //-- boolean hasCdBancoDescricao() 

    /**
     * Method hasCdContaBancariaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancariaCredito()
    {
        return this._has_cdContaBancariaCredito;
    } //-- boolean hasCdContaBancariaCredito() 

    /**
     * Method hasCdContaBancariaDescricao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancariaDescricao()
    {
        return this._has_cdContaBancariaDescricao;
    } //-- boolean hasCdContaBancariaDescricao() 

    /**
     * Method hasCdCtciaInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaInscricaoFavorecido()
    {
        return this._has_cdCtciaInscricaoFavorecido;
    } //-- boolean hasCdCtciaInscricaoFavorecido() 

    /**
     * Method hasCdDigitoAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaCredito()
    {
        return this._has_cdDigitoAgenciaCredito;
    } //-- boolean hasCdDigitoAgenciaCredito() 

    /**
     * Method hasCdListaDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdListaDebitoPagamento()
    {
        return this._has_cdListaDebitoPagamento;
    } //-- boolean hasCdListaDebitoPagamento() 

    /**
     * Method hasCdMotivoPagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoPagamentoCliente()
    {
        return this._has_cdMotivoPagamentoCliente;
    } //-- boolean hasCdMotivoPagamentoCliente() 

    /**
     * Method hasCdPessoaJuridCont
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridCont()
    {
        return this._has_cdPessoaJuridCont;
    } //-- boolean hasCdPessoaJuridCont() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdRejeicaoLoteComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoLoteComprovante()
    {
        return this._has_cdRejeicaoLoteComprovante;
    } //-- boolean hasCdRejeicaoLoteComprovante() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdSituacaoPagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPagamentoCliente()
    {
        return this._has_cdSituacaoPagamentoCliente;
    } //-- boolean hasCdSituacaoPagamentoCliente() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasDsModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsModalidade()
    {
        return this._has_dsModalidade;
    } //-- boolean hasDsModalidade() 

    /**
     * Method hasNrArquivoRemssaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemssaPagamento()
    {
        return this._has_nrArquivoRemssaPagamento;
    } //-- boolean hasNrArquivoRemssaPagamento() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroConsulta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroConsulta()
    {
        return this._has_numeroConsulta;
    } //-- boolean hasNumeroConsulta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdAgenciaBancariaCredito'.
     * 
     * @param cdAgenciaBancariaCredito the value of field
     * 'cdAgenciaBancariaCredito'.
     */
    public void setCdAgenciaBancariaCredito(int cdAgenciaBancariaCredito)
    {
        this._cdAgenciaBancariaCredito = cdAgenciaBancariaCredito;
        this._has_cdAgenciaBancariaCredito = true;
    } //-- void setCdAgenciaBancariaCredito(int) 

    /**
     * Sets the value of field 'cdAgenciaBancariaDescricao'.
     * 
     * @param cdAgenciaBancariaDescricao the value of field
     * 'cdAgenciaBancariaDescricao'.
     */
    public void setCdAgenciaBancariaDescricao(int cdAgenciaBancariaDescricao)
    {
        this._cdAgenciaBancariaDescricao = cdAgenciaBancariaDescricao;
        this._has_cdAgenciaBancariaDescricao = true;
    } //-- void setCdAgenciaBancariaDescricao(int) 

    /**
     * Sets the value of field 'cdAgendadosPagoNaoPago'.
     * 
     * @param cdAgendadosPagoNaoPago the value of field
     * 'cdAgendadosPagoNaoPago'.
     */
    public void setCdAgendadosPagoNaoPago(int cdAgendadosPagoNaoPago)
    {
        this._cdAgendadosPagoNaoPago = cdAgendadosPagoNaoPago;
        this._has_cdAgendadosPagoNaoPago = true;
    } //-- void setCdAgendadosPagoNaoPago(int) 

    /**
     * Sets the value of field 'cdBancoCredito'.
     * 
     * @param cdBancoCredito the value of field 'cdBancoCredito'.
     */
    public void setCdBancoCredito(int cdBancoCredito)
    {
        this._cdBancoCredito = cdBancoCredito;
        this._has_cdBancoCredito = true;
    } //-- void setCdBancoCredito(int) 

    /**
     * Sets the value of field 'cdBancoDescricao'.
     * 
     * @param cdBancoDescricao the value of field 'cdBancoDescricao'
     */
    public void setCdBancoDescricao(int cdBancoDescricao)
    {
        this._cdBancoDescricao = cdBancoDescricao;
        this._has_cdBancoDescricao = true;
    } //-- void setCdBancoDescricao(int) 

    /**
     * Sets the value of field 'cdContaBancariaCredito'.
     * 
     * @param cdContaBancariaCredito the value of field
     * 'cdContaBancariaCredito'.
     */
    public void setCdContaBancariaCredito(long cdContaBancariaCredito)
    {
        this._cdContaBancariaCredito = cdContaBancariaCredito;
        this._has_cdContaBancariaCredito = true;
    } //-- void setCdContaBancariaCredito(long) 

    /**
     * Sets the value of field 'cdContaBancariaDescricao'.
     * 
     * @param cdContaBancariaDescricao the value of field
     * 'cdContaBancariaDescricao'.
     */
    public void setCdContaBancariaDescricao(long cdContaBancariaDescricao)
    {
        this._cdContaBancariaDescricao = cdContaBancariaDescricao;
        this._has_cdContaBancariaDescricao = true;
    } //-- void setCdContaBancariaDescricao(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdCtciaInscricaoFavorecido'.
     * 
     * @param cdCtciaInscricaoFavorecido the value of field
     * 'cdCtciaInscricaoFavorecido'.
     */
    public void setCdCtciaInscricaoFavorecido(long cdCtciaInscricaoFavorecido)
    {
        this._cdCtciaInscricaoFavorecido = cdCtciaInscricaoFavorecido;
        this._has_cdCtciaInscricaoFavorecido = true;
    } //-- void setCdCtciaInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @param cdDigitoAgenciaCredito the value of field
     * 'cdDigitoAgenciaCredito'.
     */
    public void setCdDigitoAgenciaCredito(int cdDigitoAgenciaCredito)
    {
        this._cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
        this._has_cdDigitoAgenciaCredito = true;
    } //-- void setCdDigitoAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdDigitoContaCredito'.
     * 
     * @param cdDigitoContaCredito the value of field
     * 'cdDigitoContaCredito'.
     */
    public void setCdDigitoContaCredito(java.lang.String cdDigitoContaCredito)
    {
        this._cdDigitoContaCredito = cdDigitoContaCredito;
    } //-- void setCdDigitoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdListaDebitoPagamento'.
     * 
     * @param cdListaDebitoPagamento the value of field
     * 'cdListaDebitoPagamento'.
     */
    public void setCdListaDebitoPagamento(long cdListaDebitoPagamento)
    {
        this._cdListaDebitoPagamento = cdListaDebitoPagamento;
        this._has_cdListaDebitoPagamento = true;
    } //-- void setCdListaDebitoPagamento(long) 

    /**
     * Sets the value of field 'cdMotivoPagamentoCliente'.
     * 
     * @param cdMotivoPagamentoCliente the value of field
     * 'cdMotivoPagamentoCliente'.
     */
    public void setCdMotivoPagamentoCliente(int cdMotivoPagamentoCliente)
    {
        this._cdMotivoPagamentoCliente = cdMotivoPagamentoCliente;
        this._has_cdMotivoPagamentoCliente = true;
    } //-- void setCdMotivoPagamentoCliente(int) 

    /**
     * Sets the value of field 'cdPessoaJuridCont'.
     * 
     * @param cdPessoaJuridCont the value of field
     * 'cdPessoaJuridCont'.
     */
    public void setCdPessoaJuridCont(long cdPessoaJuridCont)
    {
        this._cdPessoaJuridCont = cdPessoaJuridCont;
        this._has_cdPessoaJuridCont = true;
    } //-- void setCdPessoaJuridCont(long) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdRejeicaoLoteComprovante'.
     * 
     * @param cdRejeicaoLoteComprovante the value of field
     * 'cdRejeicaoLoteComprovante'.
     */
    public void setCdRejeicaoLoteComprovante(long cdRejeicaoLoteComprovante)
    {
        this._cdRejeicaoLoteComprovante = cdRejeicaoLoteComprovante;
        this._has_cdRejeicaoLoteComprovante = true;
    } //-- void setCdRejeicaoLoteComprovante(long) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdSituacaoPagamentoCliente'.
     * 
     * @param cdSituacaoPagamentoCliente the value of field
     * 'cdSituacaoPagamentoCliente'.
     */
    public void setCdSituacaoPagamentoCliente(int cdSituacaoPagamentoCliente)
    {
        this._cdSituacaoPagamentoCliente = cdSituacaoPagamentoCliente;
        this._has_cdSituacaoPagamentoCliente = true;
    } //-- void setCdSituacaoPagamentoCliente(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoContaCredito'.
     * 
     * @param cdTipoContaCredito the value of field
     * 'cdTipoContaCredito'.
     */
    public void setCdTipoContaCredito(java.lang.String cdTipoContaCredito)
    {
        this._cdTipoContaCredito = cdTipoContaCredito;
    } //-- void setCdTipoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @param cdTipoIsncricaoFavorecido the value of field
     * 'cdTipoIsncricaoFavorecido'.
     */
    public void setCdTipoIsncricaoFavorecido(java.lang.String cdTipoIsncricaoFavorecido)
    {
        this._cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
    } //-- void setCdTipoIsncricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaCredito'.
     * 
     * @param dsAgenciaCredito the value of field 'dsAgenciaCredito'
     */
    public void setDsAgenciaCredito(java.lang.String dsAgenciaCredito)
    {
        this._dsAgenciaCredito = dsAgenciaCredito;
    } //-- void setDsAgenciaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDebito'.
     * 
     * @param dsAgenciaDebito the value of field 'dsAgenciaDebito'.
     */
    public void setDsAgenciaDebito(java.lang.String dsAgenciaDebito)
    {
        this._dsAgenciaDebito = dsAgenciaDebito;
    } //-- void setDsAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoCredito'.
     * 
     * @param dsBancoCredito the value of field 'dsBancoCredito'.
     */
    public void setDsBancoCredito(java.lang.String dsBancoCredito)
    {
        this._dsBancoCredito = dsBancoCredito;
    } //-- void setDsBancoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDebito'.
     * 
     * @param dsBancoDebito the value of field 'dsBancoDebito'.
     */
    public void setDsBancoDebito(java.lang.String dsBancoDebito)
    {
        this._dsBancoDebito = dsBancoDebito;
    } //-- void setDsBancoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(int dsModalidade)
    {
        this._dsModalidade = dsModalidade;
        this._has_dsModalidade = true;
    } //-- void setDsModalidade(int) 

    /**
     * Sets the value of field 'dsMotivoPagamentoCliente'.
     * 
     * @param dsMotivoPagamentoCliente the value of field
     * 'dsMotivoPagamentoCliente'.
     */
    public void setDsMotivoPagamentoCliente(java.lang.String dsMotivoPagamentoCliente)
    {
        this._dsMotivoPagamentoCliente = dsMotivoPagamentoCliente;
    } //-- void setDsMotivoPagamentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContrato'.
     * 
     * @param dsSituacaoContrato the value of field
     * 'dsSituacaoContrato'.
     */
    public void setDsSituacaoContrato(java.lang.String dsSituacaoContrato)
    {
        this._dsSituacaoContrato = dsSituacaoContrato;
    } //-- void setDsSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPagamentoCliente'.
     * 
     * @param dsSituacaoPagamentoCliente the value of field
     * 'dsSituacaoPagamentoCliente'.
     */
    public void setDsSituacaoPagamentoCliente(java.lang.String dsSituacaoPagamentoCliente)
    {
        this._dsSituacaoPagamentoCliente = dsSituacaoPagamentoCliente;
    } //-- void setDsSituacaoPagamentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanal'.
     * 
     * @param dsTipoCanal the value of field 'dsTipoCanal'.
     */
    public void setDsTipoCanal(java.lang.String dsTipoCanal)
    {
        this._dsTipoCanal = dsTipoCanal;
    } //-- void setDsTipoCanal(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaDebito'.
     * 
     * @param dsTipoContaDebito the value of field
     * 'dsTipoContaDebito'.
     */
    public void setDsTipoContaDebito(java.lang.String dsTipoContaDebito)
    {
        this._dsTipoContaDebito = dsTipoContaDebito;
    } //-- void setDsTipoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dtCreditoPagamento'.
     * 
     * @param dtCreditoPagamento the value of field
     * 'dtCreditoPagamento'.
     */
    public void setDtCreditoPagamento(java.lang.String dtCreditoPagamento)
    {
        this._dtCreditoPagamento = dtCreditoPagamento;
    } //-- void setDtCreditoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtEfetivacaoPagamento'.
     * 
     * @param dtEfetivacaoPagamento the value of field
     * 'dtEfetivacaoPagamento'.
     */
    public void setDtEfetivacaoPagamento(java.lang.String dtEfetivacaoPagamento)
    {
        this._dtEfetivacaoPagamento = dtEfetivacaoPagamento;
    } //-- void setDtEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtVencimento'.
     * 
     * @param dtVencimento the value of field 'dtVencimento'.
     */
    public void setDtVencimento(java.lang.String dtVencimento)
    {
        this._dtVencimento = dtVencimento;
    } //-- void setDtVencimento(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemssaPagamento'.
     * 
     * @param nrArquivoRemssaPagamento the value of field
     * 'nrArquivoRemssaPagamento'.
     */
    public void setNrArquivoRemssaPagamento(long nrArquivoRemssaPagamento)
    {
        this._nrArquivoRemssaPagamento = nrArquivoRemssaPagamento;
        this._has_nrArquivoRemssaPagamento = true;
    } //-- void setNrArquivoRemssaPagamento(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroConsulta'.
     * 
     * @param numeroConsulta the value of field 'numeroConsulta'.
     */
    public void setNumeroConsulta(int numeroConsulta)
    {
        this._numeroConsulta = numeroConsulta;
        this._has_numeroConsulta = true;
    } //-- void setNumeroConsulta(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 100)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 100");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.Ocorrencias) 

    /**
     * Sets the value of field 'vlPagamento'.
     * 
     * @param vlPagamento the value of field 'vlPagamento'.
     */
    public void setVlPagamento(java.math.BigDecimal vlPagamento)
    {
        this._vlPagamento = vlPagamento;
    } //-- void setVlPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarAutorizantesNetEmpresaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.ConsultarAutorizantesNetEmpresaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.ConsultarAutorizantesNetEmpresaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.ConsultarAutorizantesNetEmpresaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.ConsultarAutorizantesNetEmpresaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
