/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.NumberUtils;

/**
 * Nome: ListaOperacaoTarifaTipoServicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListaOperacaoTarifaTipoServicoSaidaDTO {
	
	
   /** Atributo FORMATO. */
   private static final String FORMATO =  "#,##0.00";

   /** Atributo cdProdutoServicoOperacao. */
   private Integer cdProdutoServicoOperacao;
   
   /** Atributo dsProdutoServicoOperacao. */
   private String dsProdutoServicoOperacao;
   
   /** Atributo cdProdutoOperacaoRelacionado. */
   private Integer cdProdutoOperacaoRelacionado;
   
   /** Atributo dsProdutoOperacaoRelacionado. */
   private String dsProdutoOperacaoRelacionado;
   
   /** Atributo cdRelacionamentoProdutoProduto. */
   private Integer cdRelacionamentoProdutoProduto;
   
   /** Atributo cdOperacaoProdutoServico. */
   private Integer cdOperacaoProdutoServico;
   
   /** Atributo dsOperacaoProdutoServico. */
   private String dsOperacaoProdutoServico;
   
   /** Atributo vlrTarifaContratoMinimo. */
   private BigDecimal vlrTarifaContratoMinimo;
   
   /** Atributo vlrTarifaContratoMaximo. */
   private BigDecimal vlrTarifaContratoMaximo;
   
   /** Atributo vlrTarifaContrato. */
   private BigDecimal vlrTarifaContrato;
   
   /** Atributo check. */
   private boolean check = false;
   
   // utilizado na grid
   /** Atributo vlrTarifaIndProposta. */
   private BigDecimal vlrTarifaIndProposta;
   
   /** Atributo percentualTarifaIndividual. */
   private BigDecimal percentualTarifaIndividual;
   
   /** Atributo qtdeEstimada. */
   private Integer qtdeEstimada;
   
   /** Atributo qtdeDiasFloating. */
   private Integer qtdeDiasFloating;
   
   /** Atributo vlrMedio. */
   private BigDecimal vlrMedio = BigDecimal.ZERO;
   
   /** Atributo vlrTotal. */
   private BigDecimal vlrTotal= BigDecimal.ZERO;
   
   /** Atributo vlrTotalPadrao. */
   private BigDecimal vlrTotalPadrao = BigDecimal.ZERO;
   
   /** Atributo vlrMedioPadrao. */
   private BigDecimal vlrMedioPadrao = BigDecimal.ZERO;

   /** Atributo mensagem. */
   private String mensagem;
   
   /** Atributo cdMensagem. */
   private String cdMensagem;
   
	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public Integer getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}
	
	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRelacionamentoProdutoProduto.
	 *
	 * @return cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProdutoProduto.
	 *
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: vlrTarifaContrato.
	 *
	 * @return vlrTarifaContrato
	 */
	public BigDecimal getVlrTarifaContrato() {
		return vlrTarifaContrato;
	}
	
	/**
	 * Set: vlrTarifaContrato.
	 *
	 * @param vlrTarifaContrato the vlr tarifa contrato
	 */
	public void setVlrTarifaContrato(BigDecimal vlrTarifaContrato) {
		this.vlrTarifaContrato = vlrTarifaContrato;
	}
	
	/**
	 * Get: vlrTarifaContratoMaximo.
	 *
	 * @return vlrTarifaContratoMaximo
	 */
	public BigDecimal getVlrTarifaContratoMaximo() {
		return vlrTarifaContratoMaximo;
	}
	
	/**
	 * Set: vlrTarifaContratoMaximo.
	 *
	 * @param vlrTarifaContratoMaximo the vlr tarifa contrato maximo
	 */
	public void setVlrTarifaContratoMaximo(BigDecimal vlrTarifaContratoMaximo) {
		this.vlrTarifaContratoMaximo = vlrTarifaContratoMaximo;
	}
	
	/**
	 * Get: vlrTarifaContratoMinimo.
	 *
	 * @return vlrTarifaContratoMinimo
	 */
	public BigDecimal getVlrTarifaContratoMinimo() {
		return vlrTarifaContratoMinimo;
	}
	
	/**
	 * Set: vlrTarifaContratoMinimo.
	 *
	 * @param vlrTarifaContratoMinimo the vlr tarifa contrato minimo
	 */
	public void setVlrTarifaContratoMinimo(BigDecimal vlrTarifaContratoMinimo) {
		this.vlrTarifaContratoMinimo = vlrTarifaContratoMinimo;
	}
   
   /**
    * Get: vlrTarifaContratoMinimoFormatado.
    *
    * @return vlrTarifaContratoMinimoFormatado
    */
   public String getVlrTarifaContratoMinimoFormatado(){
	   
	   if (getVlrTarifaContratoMinimo() == null){
		   return "";
	   }
	   
	   return NumberUtils.format(getVlrTarifaContratoMinimo(),FORMATO);
   }
   
   /**
    * Get: vlrTarifaContratoMaximoFormatado.
    *
    * @return vlrTarifaContratoMaximoFormatado
    */
   public String getVlrTarifaContratoMaximoFormatado(){
	   
	   if (getVlrTarifaContratoMaximo() == null){
		   return "";
	   }
	   
	   return NumberUtils.format(getVlrTarifaContratoMaximo(),FORMATO);
   }
   
   /**
    * Get: vlrTarifaContratoFormatado.
    *
    * @return vlrTarifaContratoFormatado
    */
   public String getVlrTarifaContratoFormatado(){
	   
	   if (getVlrTarifaContrato() == null){
		   return "";
	   }
	   
	   return NumberUtils.format(getVlrTarifaContrato(),FORMATO);
   }
   
   /**
    * Get: vlrTotalFormatado.
    *
    * @return vlrTotalFormatado
    */
   public String getVlrTotalFormatado(){
	   
	   if (getVlrTotal() == null){
		   return "0,00";
	   }
	   
	   return NumberUtils.format(getVlrTotal(),FORMATO);
   }
   
   /**
    * Get: vlrMedioFormatado.
    *
    * @return vlrMedioFormatado
    */
   public String getVlrMedioFormatado(){
	   
	   if (getVlrMedio() == null){
		   return "0,00";
	   }
	   
	   return NumberUtils.format(getVlrMedio(),FORMATO);
   }
   
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Get: percentualTarifaIndividual.
	 *
	 * @return percentualTarifaIndividual
	 */
	public BigDecimal getPercentualTarifaIndividual() {
		return percentualTarifaIndividual;
	}
	
	/**
	 * Set: percentualTarifaIndividual.
	 *
	 * @param percentualTarifaIndividual the percentual tarifa individual
	 */
	public void setPercentualTarifaIndividual(BigDecimal percentualTarifaIndividual) {
		this.percentualTarifaIndividual = percentualTarifaIndividual;
	}
	
	/**
	 * Get: qtdeDiasFloating.
	 *
	 * @return qtdeDiasFloating
	 */
	public Integer getQtdeDiasFloating() {
		return qtdeDiasFloating;
	}
	
	/**
	 * Set: qtdeDiasFloating.
	 *
	 * @param qtdeDiasFloating the qtde dias floating
	 */
	public void setQtdeDiasFloating(Integer qtdeDiasFloating) {
		this.qtdeDiasFloating = qtdeDiasFloating;
	}
	
	/**
	 * Get: qtdeEstimada.
	 *
	 * @return qtdeEstimada
	 */
	public Integer getQtdeEstimada() {
		return qtdeEstimada;
	}
	
	/**
	 * Set: qtdeEstimada.
	 *
	 * @param qtdeEstimada the qtde estimada
	 */
	public void setQtdeEstimada(Integer qtdeEstimada) {
		this.qtdeEstimada = qtdeEstimada;
	}
	
	/**
	 * Get: vlrMedio.
	 *
	 * @return vlrMedio
	 */
	public BigDecimal getVlrMedio() {
		return vlrMedio;
	}
	
	/**
	 * Set: vlrMedio.
	 *
	 * @param vlrMedio the vlr medio
	 */
	public void setVlrMedio(BigDecimal vlrMedio) {
		this.vlrMedio = vlrMedio;
	}
	
	/**
	 * Get: vlrTarifaIndProposta.
	 *
	 * @return vlrTarifaIndProposta
	 */
	public BigDecimal getVlrTarifaIndProposta() {
		return vlrTarifaIndProposta;
	}
	
	/**
	 * Set: vlrTarifaIndProposta.
	 *
	 * @param vlrTarifaIndProposta the vlr tarifa ind proposta
	 */
	public void setVlrTarifaIndProposta(BigDecimal vlrTarifaIndProposta) {
		this.vlrTarifaIndProposta = vlrTarifaIndProposta;
	}
	
	/**
	 * Get: vlrTotal.
	 *
	 * @return vlrTotal
	 */
	public BigDecimal getVlrTotal() {
		return vlrTotal;
	}
	
	/**
	 * Set: vlrTotal.
	 *
	 * @param vlrTotal the vlr total
	 */
	public void setVlrTotal(BigDecimal vlrTotal) {
		this.vlrTotal = vlrTotal;
	}
	
	/**
	 * Get: dsOperacaoProdutoServico.
	 *
	 * @return dsOperacaoProdutoServico
	 */
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}
	
	/**
	 * Set: dsOperacaoProdutoServico.
	 *
	 * @param dsOperacaoProdutoServico the ds operacao produto servico
	 */
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}
	
	/**
	 * Get: vlrTotalPadrao.
	 *
	 * @return vlrTotalPadrao
	 */
	public BigDecimal getVlrTotalPadrao() {
		return vlrTotalPadrao;
	}
	
	/**
	 * Set: vlrTotalPadrao.
	 *
	 * @param vlrTotalPadrao the vlr total padrao
	 */
	public void setVlrTotalPadrao(BigDecimal vlrTotalPadrao) {
		this.vlrTotalPadrao = vlrTotalPadrao;
	}
	
	/**
	 * Get: vlrMedioPadrao.
	 *
	 * @return vlrMedioPadrao
	 */
	public BigDecimal getVlrMedioPadrao() {
		return vlrMedioPadrao;
	}
	
	/**
	 * Set: vlrMedioPadrao.
	 *
	 * @param vlrMedioPadrao the vlr medio padrao
	 */
	public void setVlrMedioPadrao(BigDecimal vlrMedioPadrao) {
		this.vlrMedioPadrao = vlrMedioPadrao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdMensagem.
	 *
	 * @return cdMensagem
	 */
	public String getCdMensagem() {
		return cdMensagem;
	}
	
	/**
	 * Set: cdMensagem.
	 *
	 * @param cdMensagem the cd mensagem
	 */
	public void setCdMensagem(String cdMensagem) {
		this.cdMensagem = cdMensagem;
	}

	
   
}
