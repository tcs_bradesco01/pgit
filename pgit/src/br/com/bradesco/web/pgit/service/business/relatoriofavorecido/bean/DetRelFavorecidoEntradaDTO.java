/*
 * Nome: br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean;

/**
 * Nome: DetRelFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetRelFavorecidoEntradaDTO {
    
    /** Atributo tpSolicitacao. */
    private Integer tpSolicitacao;
    
    /** Atributo nrSolicitacao. */
    private Integer nrSolicitacao;
    

	/**
	 * Det rel favorecido entrada dto.
	 */
	public DetRelFavorecidoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}

	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}

	/**
	 * Get: tpSolicitacao.
	 *
	 * @return tpSolicitacao
	 */
	public Integer getTpSolicitacao() {
		return tpSolicitacao;
	}

	/**
	 * Set: tpSolicitacao.
	 *
	 * @param tpSolicitacao the tp solicitacao
	 */
	public void setTpSolicitacao(Integer tpSolicitacao) {
		this.tpSolicitacao = tpSolicitacao;
	}
}
