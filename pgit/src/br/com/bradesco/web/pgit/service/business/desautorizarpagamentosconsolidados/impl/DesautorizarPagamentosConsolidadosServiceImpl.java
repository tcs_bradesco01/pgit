/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.IDesautorizarPagamentosConsolidadosService;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.IDesautorizarPagamentosConsolidadosServiceConstants;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.ConsultarPagtosConsAutorizadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.ConsultarPagtosConsAutorizadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DetalharPagtosConsAutorizadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DetalharPagtosConsAutorizadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.OcorrenciasDetPagtosConsAutSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsautorizados.request.ConsultarPagtosConsAutorizadosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsautorizados.response.ConsultarPagtosConsAutorizadosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarintegral.request.DesautorizarIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarintegral.response.DesautorizarIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarparcial.request.DesautorizarParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarparcial.response.DesautorizarParcialResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.request.DetalharPagtosConsAutorizadosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.response.DetalharPagtosConsAutorizadosResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: DesautorizarPagamentosConsolidados
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class DesautorizarPagamentosConsolidadosServiceImpl implements IDesautorizarPagamentosConsolidadosService {

    /** Atributo nf. */
    private java.text.NumberFormat nf = java.text.NumberFormat.getInstance(new Locale("pt_br"));

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.IDesautorizarPagamentosConsolidadosService#desautorizarIntegral(br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarIntegralEntradaDTO)
     */
    public DesautorizarIntegralSaidaDTO desautorizarIntegral(DesautorizarIntegralEntradaDTO desautorizarIntegralEntradaDTO) {

        DesautorizarIntegralSaidaDTO saidaDTO = new DesautorizarIntegralSaidaDTO();
        DesautorizarIntegralRequest request = new DesautorizarIntegralRequest();
        DesautorizarIntegralResponse response = new DesautorizarIntegralResponse();

        request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(desautorizarIntegralEntradaDTO.getCdAgenciaDebito()));
        request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(desautorizarIntegralEntradaDTO.getCdBancoDebito()));
        request.setCdContaDebito(PgitUtil.verificaLongNulo(desautorizarIntegralEntradaDTO.getCdContaDebito()));
        request.setCdControleCnpjCpf(PgitUtil
            .verificaIntegerNulo(desautorizarIntegralEntradaDTO.getCdControleCnpjCpf()));
        request.setCdCorpoCnpjCpf(PgitUtil.verificaLongNulo(desautorizarIntegralEntradaDTO.getCdCorpoCnpjCpf()));
        request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(desautorizarIntegralEntradaDTO.getCdFilialCnpjCpf()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(desautorizarIntegralEntradaDTO
            .getCdPessoaJuridicaContrato()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(desautorizarIntegralEntradaDTO
            .getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(desautorizarIntegralEntradaDTO
            .getCdProdutoServicoRelacionado()));
        request.setCdSituacao(PgitUtil.verificaIntegerNulo(desautorizarIntegralEntradaDTO.getCdSituacao()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(desautorizarIntegralEntradaDTO
            .getCdTipoContratoNegocio()));
        request.setDtAgendamentoPagamento(PgitUtil.verificaStringNula(desautorizarIntegralEntradaDTO
            .getDtAgendamentoPagamento()));
        request.setHrInclusaoRemessaSistema(PgitUtil.verificaStringNula(desautorizarIntegralEntradaDTO
            .getHrInclusaoRemessaSistema()));
        request.setNrSequenciaArquivoRemessa(PgitUtil.verificaLongNulo(desautorizarIntegralEntradaDTO
            .getNrSequenciaArquivoRemessa()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(desautorizarIntegralEntradaDTO
            .getNrSequenciaContratoNegocio()));

        response = getFactoryAdapter().getDesautorizarIntegralPDCAdapter().invokeProcess(request);

        while ("PGIT0009".equals(response.getCodMensagem())) {
            response = getFactoryAdapter().getDesautorizarIntegralPDCAdapter().invokeProcess(request);
        }

        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());

        return saidaDTO;

    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.IDesautorizarPagamentosConsolidadosService#desautorizarParcial(java.util.List)
     */
    public DesautorizarParcialSaidaDTO desautorizarParcial(List<DesautorizarParcialEntradaDTO> listaEntradaDTO) {
	DesautorizarParcialSaidaDTO saidaDTO = new DesautorizarParcialSaidaDTO();
	DesautorizarParcialRequest request = new DesautorizarParcialRequest();
	DesautorizarParcialResponse response = new DesautorizarParcialResponse();

	request.setNumeroConsultas(listaEntradaDTO.size());
	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.desautorizarparcial.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.desautorizarparcial.request.Ocorrencias>();

	for (int i = 0; i < listaEntradaDTO.size(); i++) {
	    br.com.bradesco.web.pgit.service.data.pdc.desautorizarparcial.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.desautorizarparcial.request.Ocorrencias();
	    ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntradaDTO.get(i).getCdControlePagamento()));
	    ocorrencia.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdOperacaoProdutoServico()));
	    ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getCdPessoaJuridicaContrato()));
	    ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdProdutoServicoOperacao()));
	    ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoCanal()));
	    ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoContratoNegocio()));
	    ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getNrSequenciaContratoNegocio()));
	    ocorrencias.add(ocorrencia);
	}
	request.setOcorrencias(ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.desautorizarparcial.request.Ocorrencias[0]));

	response = getFactoryAdapter().getDesautorizarParcialPDCAdapter().invokeProcess(request);

	saidaDTO.setCodMensagem(response.getCodMensagem());
	saidaDTO.setMensagem(response.getMensagem());

	return saidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.IDesautorizarPagamentosConsolidadosService#detalharPagtosConsAutorizados(br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DetalharPagtosConsAutorizadosEntradaDTO)
     */
    public DetalharPagtosConsAutorizadosSaidaDTO detalharPagtosConsAutorizados(
	    DetalharPagtosConsAutorizadosEntradaDTO entradaDTO) {

	DetalharPagtosConsAutorizadosRequest request = new DetalharPagtosConsAutorizadosRequest();
	DetalharPagtosConsAutorizadosResponse response = new DetalharPagtosConsAutorizadosResponse();

	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	request.setNrOcorrencias(IDesautorizarPagamentosConsolidadosServiceConstants.NUMERO_OCORRENCIAS_DETALHAR_CONSULTA);
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
	request.setCdDigitoConta(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoConta()));
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
	request
		.setCdProdutoServicoRelacionado(PgitUtil
			.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
	request
		.setCdSituacaoOperacaoPagamento(PgitUtil
			.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
	request.setDtCreditoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamento()));
	request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
	request.setNrCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getNrCnpjCpf()));
	request.setNrControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrControleCnpjCpf()));
	request.setNrFilialcnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrFilialcnpjCpf()));
	request.setCdAgendadoPagaoNaoPago(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadoPagaoNaoPago()));
	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
	request.setCdTituloPgtoRastreado(0);
	request.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorAutorizacaoPagador()));
	request.setNrLoteInternoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrLoteInternoPagamento()));
	
	response = getFactoryAdapter().getDetalharPagtosConsAutorizadosPDCAdapter().invokeProcess(request);

	DetalharPagtosConsAutorizadosSaidaDTO saida = new DetalharPagtosConsAutorizadosSaidaDTO();

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());
	saida.setDsBancoDebito(response.getDsBancoDebito());
	saida.setDsAgenciaDebito(response.getDsAgenciaDebito());
	saida.setDsTipoContaDebito(response.getDsTipoContaDebito());
	saida.setDsContrato(response.getDsContrato());
	saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
	saida.setNmCliente(response.getNmCliente());

	List<OcorrenciasDetPagtosConsAutSaidaDTO> listaOcorrencias = new ArrayList<OcorrenciasDetPagtosConsAutSaidaDTO>();
	OcorrenciasDetPagtosConsAutSaidaDTO ocorrencia;
	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    ocorrencia = new OcorrenciasDetPagtosConsAutSaidaDTO();
	    ocorrencia.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
	    ocorrencia.setCdBanco(response.getOcorrencias(i).getCdBanco());
	    ocorrencia.setCdCnpjCpfFavorecido(response.getOcorrencias(i).getCdCnpjCpfFavorecido());
	    ocorrencia.setCdConta(response.getOcorrencias(i).getCdConta());
	    ocorrencia.setCdControleCnpjCpfFavorecido(response.getOcorrencias(i).getCdControleCnpjCpfFavorecido());
	    ocorrencia.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
	    ocorrencia.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	    ocorrencia.setCdFilialCnpjCpfFavorecido(response.getOcorrencias(i).getCdFilialCnpjCpfFavorecido());
	    ocorrencia.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
	    ocorrencia.setDsBeneficio(response.getOcorrencias(i).getDsBeneficio());
	    ocorrencia.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
	    ocorrencia.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
	    ocorrencia.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i)
		    .getCdFavorecido()));
	    ocorrencia.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());

	    ocorrencia.setBancoFormatado(PgitUtil.formatBanco(ocorrencia.getCdBanco(), false));
	    ocorrencia.setAgenciaFormatada(PgitUtil.formatAgencia(ocorrencia.getCdAgencia(), ocorrencia.getCdDigitoAgencia(), false));
	    ocorrencia.setContaFormatada(PgitUtil.formatConta(ocorrencia.getCdConta(), ocorrencia.getCdDigitoConta(), false));

	    ocorrencia.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response
		    .getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(),
		    response.getOcorrencias(i).getCdDigitoConta(), true));

	    ocorrencia.setFavorecidoBeneficiario(response.getOcorrencias(i).getDsBeneficio());
	    
	    ocorrencia.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
	    ocorrencia.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
	    ocorrencia.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
	    
	    listaOcorrencias.add(ocorrencia);
	}

	saida.setListaOcorrencias(listaOcorrencias);

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.IDesautorizarPagamentosConsolidadosService#consultarPagtosConsAutorizados(br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.ConsultarPagtosConsAutorizadosEntradaDTO)
     */
    public List<ConsultarPagtosConsAutorizadosSaidaDTO> consultarPagtosConsAutorizados(
	    ConsultarPagtosConsAutorizadosEntradaDTO consultarPagtosConsAutorizadosEntradaDTO) {

	List<ConsultarPagtosConsAutorizadosSaidaDTO> listaSaida = new ArrayList<ConsultarPagtosConsAutorizadosSaidaDTO>();
	ConsultarPagtosConsAutorizadosRequest request = new ConsultarPagtosConsAutorizadosRequest();
	ConsultarPagtosConsAutorizadosResponse response = new ConsultarPagtosConsAutorizadosResponse();

	request.setCdAgencia(PgitUtil.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdAgencia()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdBanco()));
	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdPessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdTipoContratoNegocio()));
	request.setNrOcorrencias(IDesautorizarPagamentosConsolidadosServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(consultarPagtosConsAutorizadosEntradaDTO.getNrSequenciaContratoNegocio()));
	request.setCdConta(PgitUtil.verificaLongNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdConta()));
	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdProdutoServicoOperacao()));
	request.setCdProdutoServicoRelacionado(PgitUtil
		.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdProdutoServicoRelacionado()));
	request.setCdSituacaoOperacaoPagamento(PgitUtil
		.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdSituacaoOperacaoPagamento()));
	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(consultarPagtosConsAutorizadosEntradaDTO.getDtCreditoPagamentoFim()));
	request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(consultarPagtosConsAutorizadosEntradaDTO.getNrArquivoRemessaPagamento()));
	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdTipoPesquisa()));
	request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdAgendadosPagosNaoPagos()));
	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(consultarPagtosConsAutorizadosEntradaDTO.getDtCreditoPagamentoInicio()));
	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(consultarPagtosConsAutorizadosEntradaDTO.getCdMotivoSituacaoPagamento()));
	request.setCdTituloPgtoRastreado(0);

	response = getFactoryAdapter().getConsultarPagtosConsAutorizadosPDCAdapter().invokeProcess(request);

	ConsultarPagtosConsAutorizadosSaidaDTO saida;
	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    saida = new ConsultarPagtosConsAutorizadosSaidaDTO();
	    saida.setCodMensagem(response.getCodMensagem());
	    saida.setMensagem(response.getMensagem());
	    saida.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
	    saida.setCdBanco(response.getOcorrencias(i).getCdBanco());
	    saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
	    saida.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
	    saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
	    saida.setNrContratoOrigem(response.getOcorrencias(i).getNrContratoOrigem());
	    saida.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
	    saida.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
	    saida.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
	    saida.setNrArquivoRemessaPagamento(response.getOcorrencias(i).getNrArquivoRemessaPagamento());
	    saida.setDtCreditoPagamento(DateUtils.trocarSeparadorDeData(response.getOcorrencias(i).getDtCreditoPagamento(), ".", "/"));
	    saida.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
	    saida.setCdConta(response.getOcorrencias(i).getCdConta());
	    saida.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	    saida.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
	    saida.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
	    saida.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
	    saida.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
	    saida.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
	    saida.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
	    saida.setQtPagamento(response.getOcorrencias(i).getQtPagamento());
	    saida.setVlEfetivoPagamentoCliente(response.getOcorrencias(i).getVlEfetivoPagamentoCliente());
	    java.math.BigDecimal num = new java.math.BigDecimal(response.getOcorrencias(i).getQtPagamento());
	    saida.setQtPagamentoFormatado(nf.format(num));

	    saida.setCdPessoaContratoDebito(response.getOcorrencias(i).getCdPessoaContratoDebito());
	    saida.setCdTipoContratoDebito(response.getOcorrencias(i).getCdTipoContratoDebito());
	    saida.setNrSequenciaContratoDebito(response.getOcorrencias(i).getNrSequenciaContratoDebito());

	    saida.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), response.getOcorrencias(i)
		    .getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
	    saida.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response
		    .getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(),
		    response.getOcorrencias(i).getCdDigitoConta(), true));
	    listaSaida.add(saida);
	}

	return listaSaida;
    }

}
