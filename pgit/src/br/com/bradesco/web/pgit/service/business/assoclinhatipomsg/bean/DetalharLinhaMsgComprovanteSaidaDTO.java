/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean;

/**
 * Nome: DetalharLinhaMsgComprovanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharLinhaMsgComprovanteSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoMensagem. */
	private int cdTipoMensagem;
	
	/** Atributo dsTipoMensagem. */
	private String dsTipoMensagem;
	
	/** Atributo prefixo. */
	private String prefixo;
	
	/** Atributo cdcodigoMensagem. */
	private int cdcodigoMensagem;
	
	/** Atributo dsCodigoMensagem. */
	private String dsCodigoMensagem;
	
	/** Atributo cdRecurso. */
	private int cdRecurso;
	
	/** Atributo dsRecurso. */
	private String dsRecurso;
	
	/** Atributo cdIdioma. */
	private int cdIdioma;
	
	/** Atributo dsIdioma. */
	private String dsIdioma;
	
	/** Atributo ordemLinhaMensagem. */
	private long ordemLinhaMensagem;
	
	/** Atributo cdTipoCanalInclusao. */
	private int cdTipoCanalInclusao;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo cdTipoCanalManutencao. */
	private int cdTipoCanalManutencao;
	
	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/**
	 * Get: cdcodigoMensagem.
	 *
	 * @return cdcodigoMensagem
	 */
	public int getCdcodigoMensagem() {
		return cdcodigoMensagem;
	}
	
	/**
	 * Set: cdcodigoMensagem.
	 *
	 * @param cdcodigoMensagem the cdcodigo mensagem
	 */
	public void setCdcodigoMensagem(int cdcodigoMensagem) {
		this.cdcodigoMensagem = cdcodigoMensagem;
	}
	
	/**
	 * Get: cdIdioma.
	 *
	 * @return cdIdioma
	 */
	public int getCdIdioma() {
		return cdIdioma;
	}
	
	/**
	 * Set: cdIdioma.
	 *
	 * @param cdIdioma the cd idioma
	 */
	public void setCdIdioma(int cdIdioma) {
		this.cdIdioma = cdIdioma;
	}
	
	/**
	 * Get: cdRecurso.
	 *
	 * @return cdRecurso
	 */
	public int getCdRecurso() {
		return cdRecurso;
	}
	
	/**
	 * Set: cdRecurso.
	 *
	 * @param cdRecurso the cd recurso
	 */
	public void setCdRecurso(int cdRecurso) {
		this.cdRecurso = cdRecurso;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public int getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(int cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public int getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(int cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCodigoMensagem.
	 *
	 * @return dsCodigoMensagem
	 */
	public String getDsCodigoMensagem() {
		return dsCodigoMensagem;
	}
	
	/**
	 * Set: dsCodigoMensagem.
	 *
	 * @param dsCodigoMensagem the ds codigo mensagem
	 */
	public void setDsCodigoMensagem(String dsCodigoMensagem) {
		this.dsCodigoMensagem = dsCodigoMensagem;
	}
	
	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}
	
	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}
	
	/**
	 * Get: dsRecurso.
	 *
	 * @return dsRecurso
	 */
	public String getDsRecurso() {
		return dsRecurso;
	}
	
	/**
	 * Set: dsRecurso.
	 *
	 * @param dsRecurso the ds recurso
	 */
	public void setDsRecurso(String dsRecurso) {
		this.dsRecurso = dsRecurso;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: ordemLinhaMensagem.
	 *
	 * @return ordemLinhaMensagem
	 */
	public long getOrdemLinhaMensagem() {
		return ordemLinhaMensagem;
	}
	
	/**
	 * Set: ordemLinhaMensagem.
	 *
	 * @param ordemLinhaMensagem the ordem linha mensagem
	 */
	public void setOrdemLinhaMensagem(long ordemLinhaMensagem) {
		this.ordemLinhaMensagem = ordemLinhaMensagem;
	}
	
	/**
	 * Get: prefixo.
	 *
	 * @return prefixo
	 */
	public String getPrefixo() {
		return prefixo;
	}
	
	/**
	 * Set: prefixo.
	 *
	 * @param prefixo the prefixo
	 */
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: cdTipoMensagem.
	 *
	 * @return cdTipoMensagem
	 */
	public int getCdTipoMensagem() {
		return cdTipoMensagem;
	}

	/**
	 * Set: cdTipoMensagem.
	 *
	 * @param cdTipoMensagem the cd tipo mensagem
	 */
	public void setCdTipoMensagem(int cdTipoMensagem) {
		this.cdTipoMensagem = cdTipoMensagem;
	}

	/**
	 * Get: dsTipoMensagem.
	 *
	 * @return dsTipoMensagem
	 */
	public String getDsTipoMensagem() {
		return dsTipoMensagem;
	}

	/**
	 * Set: dsTipoMensagem.
	 *
	 * @param dsTipoMensagem the ds tipo mensagem
	 */
	public void setDsTipoMensagem(String dsTipoMensagem) {
		this.dsTipoMensagem = dsTipoMensagem;
	}
	
	




}
