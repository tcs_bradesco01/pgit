package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean;


/**
 * Arquivo criado em 23/10/15.
 */
public class AlterarArquivoServicoSaidaDTO {

    private String codMensagem;

    private String mensagem;

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }
}