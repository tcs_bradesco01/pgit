/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdContratoSaida
     */
    private long _cdContratoSaida = 0;

    /**
     * keeps track of state for field: _cdContratoSaida
     */
    private boolean _has_cdContratoSaida;

    /**
     * Field _cdCpfCnpjSaida
     */
    private long _cdCpfCnpjSaida = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjSaida
     */
    private boolean _has_cdCpfCnpjSaida;

    /**
     * Field _cdFilialCpfCnpjSaida
     */
    private int _cdFilialCpfCnpjSaida = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjSaida
     */
    private boolean _has_cdFilialCpfCnpjSaida;

    /**
     * Field _cdCtrlCpfCnpjSaida
     */
    private int _cdCtrlCpfCnpjSaida = 0;

    /**
     * keeps track of state for field: _cdCtrlCpfCnpjSaida
     */
    private boolean _has_cdCtrlCpfCnpjSaida;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdCpfCnpjOriginal
     */
    private long _cdCpfCnpjOriginal = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjOriginal
     */
    private boolean _has_cdCpfCnpjOriginal;

    /**
     * Field _cdFilialCpfCnpjOriginal
     */
    private int _cdFilialCpfCnpjOriginal = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjOriginal
     */
    private boolean _has_cdFilialCpfCnpjOriginal;

    /**
     * Field _cdCtrlCpfCnpjOriginal
     */
    private int _cdCtrlCpfCnpjOriginal = 0;

    /**
     * keeps track of state for field: _cdCtrlCpfCnpjOriginal
     */
    private boolean _has_cdCtrlCpfCnpjOriginal;

    /**
     * Field _cdNumeroContrato
     */
    private long _cdNumeroContrato = 0;

    /**
     * keeps track of state for field: _cdNumeroContrato
     */
    private boolean _has_cdNumeroContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdContratoSaida
     * 
     */
    public void deleteCdContratoSaida()
    {
        this._has_cdContratoSaida= false;
    } //-- void deleteCdContratoSaida() 

    /**
     * Method deleteCdCpfCnpjOriginal
     * 
     */
    public void deleteCdCpfCnpjOriginal()
    {
        this._has_cdCpfCnpjOriginal= false;
    } //-- void deleteCdCpfCnpjOriginal() 

    /**
     * Method deleteCdCpfCnpjSaida
     * 
     */
    public void deleteCdCpfCnpjSaida()
    {
        this._has_cdCpfCnpjSaida= false;
    } //-- void deleteCdCpfCnpjSaida() 

    /**
     * Method deleteCdCtrlCpfCnpjOriginal
     * 
     */
    public void deleteCdCtrlCpfCnpjOriginal()
    {
        this._has_cdCtrlCpfCnpjOriginal= false;
    } //-- void deleteCdCtrlCpfCnpjOriginal() 

    /**
     * Method deleteCdCtrlCpfCnpjSaida
     * 
     */
    public void deleteCdCtrlCpfCnpjSaida()
    {
        this._has_cdCtrlCpfCnpjSaida= false;
    } //-- void deleteCdCtrlCpfCnpjSaida() 

    /**
     * Method deleteCdFilialCpfCnpjOriginal
     * 
     */
    public void deleteCdFilialCpfCnpjOriginal()
    {
        this._has_cdFilialCpfCnpjOriginal= false;
    } //-- void deleteCdFilialCpfCnpjOriginal() 

    /**
     * Method deleteCdFilialCpfCnpjSaida
     * 
     */
    public void deleteCdFilialCpfCnpjSaida()
    {
        this._has_cdFilialCpfCnpjSaida= false;
    } //-- void deleteCdFilialCpfCnpjSaida() 

    /**
     * Method deleteCdNumeroContrato
     * 
     */
    public void deleteCdNumeroContrato()
    {
        this._has_cdNumeroContrato= false;
    } //-- void deleteCdNumeroContrato() 

    /**
     * Returns the value of field 'cdContratoSaida'.
     * 
     * @return long
     * @return the value of field 'cdContratoSaida'.
     */
    public long getCdContratoSaida()
    {
        return this._cdContratoSaida;
    } //-- long getCdContratoSaida() 

    /**
     * Returns the value of field 'cdCpfCnpjOriginal'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjOriginal'.
     */
    public long getCdCpfCnpjOriginal()
    {
        return this._cdCpfCnpjOriginal;
    } //-- long getCdCpfCnpjOriginal() 

    /**
     * Returns the value of field 'cdCpfCnpjSaida'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjSaida'.
     */
    public long getCdCpfCnpjSaida()
    {
        return this._cdCpfCnpjSaida;
    } //-- long getCdCpfCnpjSaida() 

    /**
     * Returns the value of field 'cdCtrlCpfCnpjOriginal'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCpfCnpjOriginal'.
     */
    public int getCdCtrlCpfCnpjOriginal()
    {
        return this._cdCtrlCpfCnpjOriginal;
    } //-- int getCdCtrlCpfCnpjOriginal() 

    /**
     * Returns the value of field 'cdCtrlCpfCnpjSaida'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCpfCnpjSaida'.
     */
    public int getCdCtrlCpfCnpjSaida()
    {
        return this._cdCtrlCpfCnpjSaida;
    } //-- int getCdCtrlCpfCnpjSaida() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjOriginal'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjOriginal'.
     */
    public int getCdFilialCpfCnpjOriginal()
    {
        return this._cdFilialCpfCnpjOriginal;
    } //-- int getCdFilialCpfCnpjOriginal() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjSaida'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjSaida'.
     */
    public int getCdFilialCpfCnpjSaida()
    {
        return this._cdFilialCpfCnpjSaida;
    } //-- int getCdFilialCpfCnpjSaida() 

    /**
     * Returns the value of field 'cdNumeroContrato'.
     * 
     * @return long
     * @return the value of field 'cdNumeroContrato'.
     */
    public long getCdNumeroContrato()
    {
        return this._cdNumeroContrato;
    } //-- long getCdNumeroContrato() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Method hasCdContratoSaida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContratoSaida()
    {
        return this._has_cdContratoSaida;
    } //-- boolean hasCdContratoSaida() 

    /**
     * Method hasCdCpfCnpjOriginal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjOriginal()
    {
        return this._has_cdCpfCnpjOriginal;
    } //-- boolean hasCdCpfCnpjOriginal() 

    /**
     * Method hasCdCpfCnpjSaida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjSaida()
    {
        return this._has_cdCpfCnpjSaida;
    } //-- boolean hasCdCpfCnpjSaida() 

    /**
     * Method hasCdCtrlCpfCnpjOriginal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCpfCnpjOriginal()
    {
        return this._has_cdCtrlCpfCnpjOriginal;
    } //-- boolean hasCdCtrlCpfCnpjOriginal() 

    /**
     * Method hasCdCtrlCpfCnpjSaida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCpfCnpjSaida()
    {
        return this._has_cdCtrlCpfCnpjSaida;
    } //-- boolean hasCdCtrlCpfCnpjSaida() 

    /**
     * Method hasCdFilialCpfCnpjOriginal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjOriginal()
    {
        return this._has_cdFilialCpfCnpjOriginal;
    } //-- boolean hasCdFilialCpfCnpjOriginal() 

    /**
     * Method hasCdFilialCpfCnpjSaida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjSaida()
    {
        return this._has_cdFilialCpfCnpjSaida;
    } //-- boolean hasCdFilialCpfCnpjSaida() 

    /**
     * Method hasCdNumeroContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNumeroContrato()
    {
        return this._has_cdNumeroContrato;
    } //-- boolean hasCdNumeroContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdContratoSaida'.
     * 
     * @param cdContratoSaida the value of field 'cdContratoSaida'.
     */
    public void setCdContratoSaida(long cdContratoSaida)
    {
        this._cdContratoSaida = cdContratoSaida;
        this._has_cdContratoSaida = true;
    } //-- void setCdContratoSaida(long) 

    /**
     * Sets the value of field 'cdCpfCnpjOriginal'.
     * 
     * @param cdCpfCnpjOriginal the value of field
     * 'cdCpfCnpjOriginal'.
     */
    public void setCdCpfCnpjOriginal(long cdCpfCnpjOriginal)
    {
        this._cdCpfCnpjOriginal = cdCpfCnpjOriginal;
        this._has_cdCpfCnpjOriginal = true;
    } //-- void setCdCpfCnpjOriginal(long) 

    /**
     * Sets the value of field 'cdCpfCnpjSaida'.
     * 
     * @param cdCpfCnpjSaida the value of field 'cdCpfCnpjSaida'.
     */
    public void setCdCpfCnpjSaida(long cdCpfCnpjSaida)
    {
        this._cdCpfCnpjSaida = cdCpfCnpjSaida;
        this._has_cdCpfCnpjSaida = true;
    } //-- void setCdCpfCnpjSaida(long) 

    /**
     * Sets the value of field 'cdCtrlCpfCnpjOriginal'.
     * 
     * @param cdCtrlCpfCnpjOriginal the value of field
     * 'cdCtrlCpfCnpjOriginal'.
     */
    public void setCdCtrlCpfCnpjOriginal(int cdCtrlCpfCnpjOriginal)
    {
        this._cdCtrlCpfCnpjOriginal = cdCtrlCpfCnpjOriginal;
        this._has_cdCtrlCpfCnpjOriginal = true;
    } //-- void setCdCtrlCpfCnpjOriginal(int) 

    /**
     * Sets the value of field 'cdCtrlCpfCnpjSaida'.
     * 
     * @param cdCtrlCpfCnpjSaida the value of field
     * 'cdCtrlCpfCnpjSaida'.
     */
    public void setCdCtrlCpfCnpjSaida(int cdCtrlCpfCnpjSaida)
    {
        this._cdCtrlCpfCnpjSaida = cdCtrlCpfCnpjSaida;
        this._has_cdCtrlCpfCnpjSaida = true;
    } //-- void setCdCtrlCpfCnpjSaida(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjOriginal'.
     * 
     * @param cdFilialCpfCnpjOriginal the value of field
     * 'cdFilialCpfCnpjOriginal'.
     */
    public void setCdFilialCpfCnpjOriginal(int cdFilialCpfCnpjOriginal)
    {
        this._cdFilialCpfCnpjOriginal = cdFilialCpfCnpjOriginal;
        this._has_cdFilialCpfCnpjOriginal = true;
    } //-- void setCdFilialCpfCnpjOriginal(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjSaida'.
     * 
     * @param cdFilialCpfCnpjSaida the value of field
     * 'cdFilialCpfCnpjSaida'.
     */
    public void setCdFilialCpfCnpjSaida(int cdFilialCpfCnpjSaida)
    {
        this._cdFilialCpfCnpjSaida = cdFilialCpfCnpjSaida;
        this._has_cdFilialCpfCnpjSaida = true;
    } //-- void setCdFilialCpfCnpjSaida(int) 

    /**
     * Sets the value of field 'cdNumeroContrato'.
     * 
     * @param cdNumeroContrato the value of field 'cdNumeroContrato'
     */
    public void setCdNumeroContrato(long cdNumeroContrato)
    {
        this._cdNumeroContrato = cdNumeroContrato;
        this._has_cdNumeroContrato = true;
    } //-- void setCdNumeroContrato(long) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
