/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

import java.util.ArrayList;
import java.util.List;


/**
 * Nome: BloqDesbloqEmissaoAutAvisosComprvEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloqDesbloqEmissaoAutAvisosComprvEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdProdutoServicoVinculo. */
	private Integer cdProdutoServicoVinculo;
	
	/** Atributo listaOcorrencias. */
	private List<OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO> listaOcorrencias = new ArrayList<OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO>();
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdProdutoServicoVinculo.
	 *
	 * @return cdProdutoServicoVinculo
	 */
	public Integer getCdProdutoServicoVinculo() {
	    return cdProdutoServicoVinculo;
	}
	
	/**
	 * Set: cdProdutoServicoVinculo.
	 *
	 * @param cdProdutoServicoVinculo the cd produto servico vinculo
	 */
	public void setCdProdutoServicoVinculo(Integer cdProdutoServicoVinculo) {
	    this.cdProdutoServicoVinculo = cdProdutoServicoVinculo;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: listaOcorrencias.
	 *
	 * @return listaOcorrencias
	 */
	public List<OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO> getListaOcorrencias() {
		return listaOcorrencias;
	}
	
	/**
	 * Set: listaOcorrencias.
	 *
	 * @param listaOcorrencias the lista ocorrencias
	 */
	public void setListaOcorrencias(
			List<OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO> listaOcorrencias) {
		this.listaOcorrencias = listaOcorrencias;
	}
	
	

}
