/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.IAutorizarPagamentosConsolidadosService;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.IAutorizarPagamentosConsolidadosServiceConstants;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.ConsultarPagtosConsPendAutorizacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.ConsultarPagtosConsPendAutorizacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.DetalharPagtosConsPendAutorizacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.DetalharPagtosConsPendAutorizacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.OcorrenciasDetPagConsPendAutSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarintegral.request.AutorizarIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarintegral.response.AutorizarIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarparcial.request.AutorizarParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarparcial.response.AutorizarParcialResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconspendautorizacao.request.ConsultarPagtosConsPendAutorizacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconspendautorizacao.response.ConsultarPagtosConsPendAutorizacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconspendautorizacao.request.DetalharPagtosConsPendAutorizacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconspendautorizacao.response.DetalharPagtosConsPendAutorizacaoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AutorizarPagamentosConsolidados
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AutorizarPagamentosConsolidadosServiceImpl implements IAutorizarPagamentosConsolidadosService {
	
	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
		
	
	
   /**
    * Get: factoryAdapter.
    *
    * @return factoryAdapter
    */
   public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
    /**
     * Construtor.
     */
    public AutorizarPagamentosConsolidadosServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.IAutorizarPagamentosConsolidadosService#autorizarIntegral(br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarIntegralEntradaDTO)
     */
    public AutorizarIntegralSaidaDTO autorizarIntegral(AutorizarIntegralEntradaDTO entradaDTO) {
    	AutorizarIntegralSaidaDTO saidaDTO = new AutorizarIntegralSaidaDTO();
		AutorizarIntegralRequest request = new AutorizarIntegralRequest();
		AutorizarIntegralResponse response = new AutorizarIntegralResponse();
		
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
		request.setCdCorpoCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getCdCorpoCnpjCpf()));
		request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjCpf()));
		request.setCdControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjCpf()));
		request.setNrSequenciaArquivoRemessa(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaArquivoRemessa()));
		request.setHrInclusaoRemessaSistema(PgitUtil.verificaStringNula(entradaDTO.getHrInclusaoRemessaSistema()));
		request.setDtAgendamentoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtAgendamentoPagamento()));
		request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));
		request.setCdSituacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacao()));
		
		response = getFactoryAdapter().getAutorizarIntegralPDCAdapter().invokeProcess(request);
		
		while ("PGIT0009".equals(response.getCodMensagem())) {
			response = getFactoryAdapter().getAutorizarIntegralPDCAdapter().invokeProcess(request);
		}

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.IAutorizarPagamentosConsolidadosService#autorizarParcial(java.util.List)
     */
    public AutorizarParcialSaidaDTO autorizarParcial(List<AutorizarParcialEntradaDTO> listaEntradaDTO){
    	AutorizarParcialSaidaDTO saidaDTO = new AutorizarParcialSaidaDTO();
    	AutorizarParcialRequest request = new AutorizarParcialRequest();
    	AutorizarParcialResponse response = new AutorizarParcialResponse();
    	
    	request.setNumeroConsultas(listaEntradaDTO.size());
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.autorizarparcial.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.autorizarparcial.request.Ocorrencias>();
    	
    	for(int i=0; i<listaEntradaDTO.size(); i++){
    		br.com.bradesco.web.pgit.service.data.pdc.autorizarparcial.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.autorizarparcial.request.Ocorrencias();
    	
    		ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getCdPessoaJuridicaContrato()));
    		ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoContratoNegocio()));
    		ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getNrSequenciaContratoNegocio()));
    		ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoCanal())));
    		ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdProdutoServicoOperacao()));
    		ocorrencia.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdOperacaoProdutoServico()));
    		ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntradaDTO.get(i).getCdControlePagamento()));
    		
    		ocorrencias.add(ocorrencia);
    		
    	}
    	request.setOcorrencias(ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.autorizarparcial.request.Ocorrencias[0]));
    	
    	response = getFactoryAdapter().getAutorizarParcialPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
    	return saidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.IAutorizarPagamentosConsolidadosService#consultarPagtosConsPendAutorizacao(br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.ConsultarPagtosConsPendAutorizacaoEntradaDTO)
     */
    public List<ConsultarPagtosConsPendAutorizacaoSaidaDTO> consultarPagtosConsPendAutorizacao(ConsultarPagtosConsPendAutorizacaoEntradaDTO entradaDTO) {
    	List<ConsultarPagtosConsPendAutorizacaoSaidaDTO> listaSaidaDTO = new ArrayList<ConsultarPagtosConsPendAutorizacaoSaidaDTO>();
    	ConsultarPagtosConsPendAutorizacaoRequest request = new ConsultarPagtosConsPendAutorizacaoRequest();
    	ConsultarPagtosConsPendAutorizacaoResponse response = new ConsultarPagtosConsPendAutorizacaoResponse();
		
        request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
		request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
		request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
		request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
		request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
		request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);		
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
		request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
		request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
		request.setNrOcorrencias(IAutorizarPagamentosConsolidadosServiceConstants.NUMERO_OCORRENCIAS_50);
		request.setCdTituloPgtoRastreado(0);
		
		response = getFactoryAdapter().getConsultarPagtosConsPendAutorizacaoPDCAdapter().invokeProcess(request);
		
		ConsultarPagtosConsPendAutorizacaoSaidaDTO saidaDTO;
		
		for(int i=0; i<response.getOcorrenciasCount(); i++){
			saidaDTO = new ConsultarPagtosConsPendAutorizacaoSaidaDTO();
		
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			
			saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
			saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
			saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			saidaDTO.setNrContratoOrigem(response.getOcorrencias(i).getNrContratoOrigem());
			saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
			saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
			saidaDTO.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
			saidaDTO.setNrArquivoRemessaPagamento(response.getOcorrencias(i).getNrArquivoRemessaPagamento());
			saidaDTO.setDtCreditoPagamento(DateUtils.trocarSeparadorDeData(response.getOcorrencias(i).getDtCreditoPagamento(),".","/"));
			saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
			saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
			saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
			saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
			saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
			saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
			saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
			saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
			saidaDTO.setQtPagamento(response.getOcorrencias(i).getQtPagamento());
			java.math.BigDecimal num = new java.math.BigDecimal(response.getOcorrencias(i).getQtPagamento());
			saidaDTO.setQtPagamentoFormatado(nf.format(num));	
			saidaDTO.setCdPessoaContratoDebito(response.getOcorrencias(i).getCdPessoaContratoDebito());
			saidaDTO.setCdTipoContratoDebito(response.getOcorrencias(i).getCdTipoContratoDebito());
			saidaDTO.setNrSequenciaContratoDebito(response.getOcorrencias(i).getNrSequenciaContratoDebito());
			saidaDTO.setVlEfetivoPagamentoCliente(response.getOcorrencias(i).getVlEfetivoPagamentoCliente());
			saidaDTO.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), response.getOcorrencias(i).getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
			saidaDTO.setBancoAgenciaContaDebito(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(), true));
			
						
			listaSaidaDTO.add(saidaDTO);
		}
		
		return listaSaidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.IAutorizarPagamentosConsolidadosService#detalharPagtosConsPendAutorizacao(br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.DetalharPagtosConsPendAutorizacaoEntradaDTO)
     */
    public DetalharPagtosConsPendAutorizacaoSaidaDTO detalharPagtosConsPendAutorizacao(DetalharPagtosConsPendAutorizacaoEntradaDTO entradaDTO) {
		DetalharPagtosConsPendAutorizacaoRequest request = new DetalharPagtosConsPendAutorizacaoRequest();
		DetalharPagtosConsPendAutorizacaoResponse response = new DetalharPagtosConsPendAutorizacaoResponse();
		
		request.setNrOcorrencias(IAutorizarPagamentosConsolidadosServiceConstants.NUMERO_OCORRENCIAS);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setNrCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getNrCnpjCpf()));		
		request.setNrFilialcnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrFilialcnpjCpf()));
		request.setNrControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrControleCnpjCpf()));
		request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
		request.setDtCreditoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamento()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
		request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
		request.setCdDigitoConta(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoConta()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
		request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
		request.setCdAgendadoPagaoNaoPago(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadoPagaoNaoPago()));
		request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
		request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
		request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
		request.setCdTituloPgtoRastreado(0);
		request.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorAutorizacaoPagador()));
		request.setNrLoteInternoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrLoteInternoPagamento()));
		request.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorAutorizacaoPagador()));
		request.setNrLoteInternoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrLoteInternoPagamento()));

		response = getFactoryAdapter().getDetalharPagtosConsPendAutorizacaoPDCAdapter().invokeProcess(request);
		
		DetalharPagtosConsPendAutorizacaoSaidaDTO saidaDTO = new DetalharPagtosConsPendAutorizacaoSaidaDTO();
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setDsSituacaoContrato(response.getDsSituacaoContrato());
		saidaDTO.setNmCliente(response.getNmCliente());
		
		
		List<OcorrenciasDetPagConsPendAutSaidaDTO> listaDetPagtosConsPendAutorizacao = new ArrayList<OcorrenciasDetPagConsPendAutSaidaDTO>();
		OcorrenciasDetPagConsPendAutSaidaDTO ocorrencia;
		
		 for(int i=0;i<response.getOcorrenciasCount();i++){
			 ocorrencia = new OcorrenciasDetPagConsPendAutSaidaDTO();
			 ocorrencia.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
			 ocorrencia.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
			 ocorrencia.setVlPagamentoFormatado(NumberUtils.format(response.getOcorrencias(i).getVlPagamento(),"#,##0.00"));
			 ocorrencia.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(), true));
			 ocorrencia.setCdCnpjCpfFavorecido(response.getOcorrencias(i).getCdCnpjCpfFavorecido());
			 ocorrencia.setCdFilialCnpjCpfFavorecido(response.getOcorrencias(i).getCdFilialCnpjCpfFavorecido());
			 ocorrencia.setCdControleCnpjCpfFavorecido(response.getOcorrencias(i).getCdControleCnpjCpfFavorecido());
			 ocorrencia.setDsBeneficio(response.getOcorrencias(i).getDsBeneficio());
			 ocorrencia.setCdBanco(response.getOcorrencias(i).getCdBanco());
			 ocorrencia.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
			 ocorrencia.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
			 ocorrencia.setCdConta(response.getOcorrencias(i).getCdConta());
			 ocorrencia.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
			 ocorrencia.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
			 ocorrencia.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i).getCdFavorecido()));
			 ocorrencia.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());   
			 ocorrencia.setBancoFormatado(PgitUtil.formatBanco(ocorrencia.getCdBanco(), false));
			 ocorrencia.setAgenciaFormatada((PgitUtil.formatAgencia(ocorrencia.getCdAgencia(), ocorrencia.getCdDigitoAgencia(), false)));    	
			 ocorrencia.setContaFormatada(PgitUtil.formatConta(ocorrencia.getCdConta(), ocorrencia.getCdDigitoConta(), false));
			 ocorrencia.setFavorecidoBeneficiario(response.getOcorrencias(i).getDsBeneficio());
			 ocorrencia.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
			 ocorrencia.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
			 ocorrencia.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
			 ocorrencia.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
			 ocorrencia.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());		
			 
			 listaDetPagtosConsPendAutorizacao.add(ocorrencia);	    	
		   }
		   saidaDTO.setListaDetPagtosConsPendAutorizacao(listaDetPagtosConsPendAutorizacao);
		    
		return saidaDTO;
	}
}