/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimircontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimircontrato.bean;

/**
 * Nome: ImprimirContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoEntradaDTO {
	
	/** Atributo cdClubPessoa. */
	private Long cdClubPessoa;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdCorpoCpfCnpj. */
    private Long cdCorpoCpfCnpj;
    
    /** Atributo cdControleCpfCnpj. */
    private Integer cdControleCpfCnpj;
    
    /** Atributo cdDigitoCpfCnpj. */
    private Integer cdDigitoCpfCnpj;
    
    /** Atributo cdCpfCnpjParticipante. */
    private String cdCpfCnpjParticipante;
    
    /** Atributo codFuncionalBradesco. */
    private Long codFuncionalBradesco;
	
	/** Atributo nmParticipante. */
	private String nmParticipante;
	
	/** Atributo nmEmpresa. */
	private String nmEmpresa;
	
	/**
	 * Imprimir contrato entrada dto.
	 */
	public ImprimirContratoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Get: cdClubPessoa.
	 *
	 * @return cdClubPessoa
	 */
	public Long getCdClubPessoa() {
		return cdClubPessoa;
	}

	/**
	 * Set: cdClubPessoa.
	 *
	 * @param cdClubPessoa the cd club pessoa
	 */
	public void setCdClubPessoa(Long cdClubPessoa) {
		this.cdClubPessoa = cdClubPessoa;
	}

	/**
	 * Get: cdControleCpfCnpj.
	 *
	 * @return cdControleCpfCnpj
	 */
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}

	/**
	 * Set: cdControleCpfCnpj.
	 *
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 */
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}

	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public String getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}

	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(String cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}

	/**
	 * Get: cdDigitoCpfCnpj.
	 *
	 * @return cdDigitoCpfCnpj
	 */
	public Integer getCdDigitoCpfCnpj() {
		return cdDigitoCpfCnpj;
	}

	/**
	 * Set: cdDigitoCpfCnpj.
	 *
	 * @param cdDigitoCpfCnpj the cd digito cpf cnpj
	 */
	public void setCdDigitoCpfCnpj(Integer cdDigitoCpfCnpj) {
		this.cdDigitoCpfCnpj = cdDigitoCpfCnpj;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: codFuncionalBradesco.
	 *
	 * @return codFuncionalBradesco
	 */
	public Long getCodFuncionalBradesco() {
		return codFuncionalBradesco;
	}

	/**
	 * Set: codFuncionalBradesco.
	 *
	 * @param codFuncionalBradesco the cod funcional bradesco
	 */
	public void setCodFuncionalBradesco(Long codFuncionalBradesco) {
		this.codFuncionalBradesco = codFuncionalBradesco;
	}

	/**
	 * Get: nmEmpresa.
	 *
	 * @return nmEmpresa
	 */
	public String getNmEmpresa() {
		return nmEmpresa;
	}

	/**
	 * Set: nmEmpresa.
	 *
	 * @param nmEmpresa the nm empresa
	 */
	public void setNmEmpresa(String nmEmpresa) {
		this.nmEmpresa = nmEmpresa;
	}

	/**
	 * Get: nmParticipante.
	 *
	 * @return nmParticipante
	 */
	public String getNmParticipante() {
		return nmParticipante;
	}

	/**
	 * Set: nmParticipante.
	 *
	 * @param nmParticipante the nm participante
	 */
	public void setNmParticipante(String nmParticipante) {
		this.nmParticipante = nmParticipante;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
}
