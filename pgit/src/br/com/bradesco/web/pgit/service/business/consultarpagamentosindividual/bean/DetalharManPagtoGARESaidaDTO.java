/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Nome: DetalharManPagtoGARESaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class DetalharManPagtoGARESaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdDddContribuinte. */
	private String cdDddContribuinte;

	/** Atributo nrTelefoneContribuinte. */
	private String nrTelefoneContribuinte;

	/** Atributo cdInscricaoEstadualContribuinte. */
	private String cdInscricaoEstadualContribuinte;

	/** Atributo cdReceita. */
	private String cdReceita;

	/** Atributo nrReferencia. */
	private String nrReferencia;

	/** Atributo nrCota. */
	private String nrCota;

	/** Atributo cdPercentualReceita. */
	private BigDecimal cdPercentualReceita;

	/** Atributo dsApuracaoTributo. */
	private String dsApuracaoTributo;

	/** Atributo dtReferencia. */
	private String dtReferencia;

	/** Atributo cdCnae. */
	private String cdCnae;

	/** Atributo cdPlacaVeiculo. */
	private String cdPlacaVeiculo;

	/** Atributo nrParcela. */
	private String nrParcela;

	/** Atributo cdOrFavorecido. */
	private String cdOrFavorecido;

	/** Atributo dsOrFavorecido. */
	private String dsOrFavorecido;

	/** Atributo cdUfFavorecido. */
	private String cdUfFavorecido;

	/** Atributo dsUfFavorecido. */
	private String dsUfFavorecido;

	/** Atributo vlReceita. */
	private BigDecimal vlReceita;

	/** Atributo vlPrincipal. */
	private BigDecimal vlPrincipal;

	/** Atributo vlAtual. */
	private BigDecimal vlAtual;

	/** Atributo vlAcrescimo. */
	private BigDecimal vlAcrescimo;

	/** Atributo vlHonorario. */
	private BigDecimal vlHonorario;

	/** Atributo vlAbatimento. */
	private BigDecimal vlAbatimento;

	/** Atributo vlMulta. */
	private BigDecimal vlMulta;

	/** Atributo vlMora. */
	private BigDecimal vlMora;

	/** Atributo vlTotal. */
	private BigDecimal vlTotal;

	/** Atributo dsTributo. */
	private String dsTributo;

	/** Atributo cdIndicadorModalidadePgit. */
	private Integer cdIndicadorModalidadePgit;

	/** Atributo dsMoeda. */
	private String dsMoeda;

	/** Atributo dsPagamento. */
	private String dsPagamento;

	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;

	/** Atributo dsTipoContaDebito. */
	private String dsTipoContaDebito;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo dtAgendamento. */
	private String dtAgendamento;

	/** Atributo vlAgendado. */
	private BigDecimal vlAgendado;

	/** Atributo vlEfetivo. */
	private BigDecimal vlEfetivo;

	/** Atributo cdIndicadorEconomicoMoeda. */
	private Integer cdIndicadorEconomicoMoeda;

	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda;

	/** Atributo dtVencimento. */
	private String dtVencimento;

	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha;

	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha;

	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento;

	/** Atributo cdMotivoSituacaoPagamento. */
	private Integer cdMotivoSituacaoPagamento;

	/** Atributo cdListaDebito. */
	private String cdListaDebito;

	/** Atributo cdTipoInscricaoFavorecido. */
	private Integer cdTipoInscricaoFavorecido;

	/** Atributo nrDocumento. */
	private String nrDocumento;

	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento;

	/** Atributo cdTipoDocumento. */
	private Integer cdTipoDocumento;

	/** Atributo dsTipoDocumento. */
	private String dsTipoDocumento;

	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento;

	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento;

	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa;

	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa;

	/** Atributo cdFavorecido. */
	private String cdFavorecido;

	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;

	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;

	/** Atributo cdTipoContaFavorecido. */
	private Integer cdTipoContaFavorecido;

	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido;

	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;

	/** Atributo cdTipoManutencao. */
	private Integer cdTipoManutencao;

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;

	/** Atributo dtInclusao. */
	private String dtInclusao;

	/** Atributo hrInclusao. */
	private String hrInclusao;

	/** Atributo cdUsuarioInclusaoInterno. */
	private String cdUsuarioInclusaoInterno;

	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;

	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;

	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;

	/** Atributo cdFluxoInclusao. */
	private String cdFluxoInclusao;

	/** Atributo dtManutencao. */
	private String dtManutencao;

	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno;

	/** Atributo hrManutencao. */
	private String hrManutencao;

	/** Atributo cdUsuarioManutencaoInterno. */
	private String cdUsuarioManutencaoInterno;

	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;

	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;

	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;

	/** Atributo cdFluxoManutencao. */
	private String cdFluxoManutencao;

	/** Atributo dataHoraInclusaoFormatada. */
	private String dataHoraInclusaoFormatada;

	/** Atributo usuarioInclusaoFormatado. */
	private String usuarioInclusaoFormatado;

	/** Atributo tipoCanalInclusaoFormatado. */
	private String tipoCanalInclusaoFormatado;

	/** Atributo complementoInclusaoFormatado. */
	private String complementoInclusaoFormatado;

	/** Atributo dataHoraManutencaoFormatada. */
	private String dataHoraManutencaoFormatada;

	/** Atributo usuarioManutencaoFormatado. */
	private String usuarioManutencaoFormatado;

	/** Atributo tipoCanalManutencaoFormatado. */
	private String tipoCanalManutencaoFormatado;

	/** Atributo complementoManutencaoFormatado. */
	private String complementoManutencaoFormatado;

	/** Atributo dsPessoaJuridicaContrato. */
	private String dsPessoaJuridicaContrato;

	/** Atributo nrSequenciaContratoNegocio. */
	private String nrSequenciaContratoNegocio;

	/** Atributo bancoDebitoFormatado. */
	private String bancoDebitoFormatado;

	/** Atributo agenciaDebitoFormatada. */
	private String agenciaDebitoFormatada;

	/** Atributo contaDebitoFormatada. */
	private String contaDebitoFormatada;

	// Cliente
	/** Atributo cpfCnpjClienteFormatado. */
	private String cpfCnpjClienteFormatado;

	/** Atributo cdCpfCnpjCliente. */
	private Long cdCpfCnpjCliente;

	/** Atributo nomeCliente. */
	private String nomeCliente;

	// Conta de D�bito
	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;

	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;

	/** Atributo cdDigAgenciaDebto. */
	private String cdDigAgenciaDebto;

	/** Atributo cdContaDebito. */
	private Long cdContaDebito;

	/** Atributo dsDigAgenciaDebito. */
	private String dsDigAgenciaDebito;

	/** Atributo bancoCreditoFormatado. */
	private String bancoCreditoFormatado;

	// Favorecido
	/** Atributo nmInscriFavorecido. */
	private String nmInscriFavorecido;

	/** Atributo dsNomeFavorecido. */
	private String dsNomeFavorecido;

	/** Atributo cdBancoCredito. */
	private Integer cdBancoCredito;

	/** Atributo cdAgenciaCredito. */
	private Integer cdAgenciaCredito;

	/** Atributo cdDigAgenciaCredito. */
	private String cdDigAgenciaCredito;

	/** Atributo cdContaCredito. */
	private Long cdContaCredito;

	/** Atributo cdDigContaCredito. */
	private String cdDigContaCredito;

	/** Atributo numeroInscricaoFavorecidoFormatado. */
	private String numeroInscricaoFavorecidoFormatado;

	// Conta de Cr�dito
	/** Atributo agenciaCreditoFormatada. */
	private String agenciaCreditoFormatada;

	/** Atributo contaCreditoFormatada. */
	private String contaCreditoFormatada;

	// Pagamento
	/** Atributo dtPagamento. */
	private Date dtPagamento;

	/** Atributo cdSituacaoPagamento. */
	private String cdSituacaoPagamento;

	/** Atributo cdIndicadorAuto. */
	private Integer cdIndicadorAuto;

	/** Atributo cdIndicadorSemConsulta. */
	private Integer cdIndicadorSemConsulta;

	/** Atributo dsTipoServicoOperacao. */
	private String dsTipoServicoOperacao;

	/** Atributo dsModalidadeRelacionado. */
	private String dsModalidadeRelacionado;

	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;

	/** Atributo dsUnidadeAgencia. */
	private String dsUnidadeAgencia;

	/** Atributo dtFloatingPagamento. */
	private String dtFloatingPagamento;

	/** Atributo dtEfetivFloatPgto. */
	private String dtEfetivFloatPgto;

	/** Atributo vlFloatingPagamento. */
	private BigDecimal vlFloatingPagamento;

	private Long cdLoteInterno;
	private String dsIndicadorAutorizacao;
	private String dsTipoLayout;

	/**
	 * Get: complementoInclusaoFormatado.
	 * 
	 * @return complementoInclusaoFormatado
	 */
	public String getComplementoInclusaoFormatado() {
		return complementoInclusaoFormatado;
	}

	/**
	 * Set: complementoInclusaoFormatado.
	 * 
	 * @param complementoInclusaoFormatado
	 *            the complemento inclusao formatado
	 */
	public void setComplementoInclusaoFormatado(
			String complementoInclusaoFormatado) {
		this.complementoInclusaoFormatado = complementoInclusaoFormatado;
	}

	/**
	 * Get: complementoManutencaoFormatado.
	 * 
	 * @return complementoManutencaoFormatado
	 */
	public String getComplementoManutencaoFormatado() {
		return complementoManutencaoFormatado;
	}

	/**
	 * Set: complementoManutencaoFormatado.
	 * 
	 * @param complementoManutencaoFormatado
	 *            the complemento manutencao formatado
	 */
	public void setComplementoManutencaoFormatado(
			String complementoManutencaoFormatado) {
		this.complementoManutencaoFormatado = complementoManutencaoFormatado;
	}

	/**
	 * Get: dataHoraInclusaoFormatada.
	 * 
	 * @return dataHoraInclusaoFormatada
	 */
	public String getDataHoraInclusaoFormatada() {
		return dataHoraInclusaoFormatada;
	}

	/**
	 * Set: dataHoraInclusaoFormatada.
	 * 
	 * @param dataHoraInclusaoFormatada
	 *            the data hora inclusao formatada
	 */
	public void setDataHoraInclusaoFormatada(String dataHoraInclusaoFormatada) {
		this.dataHoraInclusaoFormatada = dataHoraInclusaoFormatada;
	}

	/**
	 * Get: dataHoraManutencaoFormatada.
	 * 
	 * @return dataHoraManutencaoFormatada
	 */
	public String getDataHoraManutencaoFormatada() {
		return dataHoraManutencaoFormatada;
	}

	/**
	 * Set: dataHoraManutencaoFormatada.
	 * 
	 * @param dataHoraManutencaoFormatada
	 *            the data hora manutencao formatada
	 */
	public void setDataHoraManutencaoFormatada(
			String dataHoraManutencaoFormatada) {
		this.dataHoraManutencaoFormatada = dataHoraManutencaoFormatada;
	}

	/**
	 * Get: tipoCanalInclusaoFormatado.
	 * 
	 * @return tipoCanalInclusaoFormatado
	 */
	public String getTipoCanalInclusaoFormatado() {
		return tipoCanalInclusaoFormatado;
	}

	/**
	 * Set: tipoCanalInclusaoFormatado.
	 * 
	 * @param tipoCanalInclusaoFormatado
	 *            the tipo canal inclusao formatado
	 */
	public void setTipoCanalInclusaoFormatado(String tipoCanalInclusaoFormatado) {
		this.tipoCanalInclusaoFormatado = tipoCanalInclusaoFormatado;
	}

	/**
	 * Get: tipoCanalManutencaoFormatado.
	 * 
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado() {
		return tipoCanalManutencaoFormatado;
	}

	/**
	 * Set: tipoCanalManutencaoFormatado.
	 * 
	 * @param tipoCanalManutencaoFormatado
	 *            the tipo canal manutencao formatado
	 */
	public void setTipoCanalManutencaoFormatado(
			String tipoCanalManutencaoFormatado) {
		this.tipoCanalManutencaoFormatado = tipoCanalManutencaoFormatado;
	}

	/**
	 * Get: usuarioInclusaoFormatado.
	 * 
	 * @return usuarioInclusaoFormatado
	 */
	public String getUsuarioInclusaoFormatado() {
		return usuarioInclusaoFormatado;
	}

	/**
	 * Set: usuarioInclusaoFormatado.
	 * 
	 * @param usuarioInclusaoFormatado
	 *            the usuario inclusao formatado
	 */
	public void setUsuarioInclusaoFormatado(String usuarioInclusaoFormatado) {
		this.usuarioInclusaoFormatado = usuarioInclusaoFormatado;
	}

	/**
	 * Get: usuarioManutencaoFormatado.
	 * 
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}

	/**
	 * Set: usuarioManutencaoFormatado.
	 * 
	 * @param usuarioManutencaoFormatado
	 *            the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}

	/**
	 * Get: cdCnae.
	 * 
	 * @return cdCnae
	 */
	public String getCdCnae() {
		return cdCnae;
	}

	/**
	 * Set: cdCnae.
	 * 
	 * @param cdCnae
	 *            the cd cnae
	 */
	public void setCdCnae(String cdCnae) {
		this.cdCnae = cdCnae;
	}

	/**
	 * Get: cdDddContribuinte.
	 * 
	 * @return cdDddContribuinte
	 */
	public String getCdDddContribuinte() {
		return cdDddContribuinte;
	}

	/**
	 * Set: cdDddContribuinte.
	 * 
	 * @param cdDddContribuinte
	 *            the cd ddd contribuinte
	 */
	public void setCdDddContribuinte(String cdDddContribuinte) {
		this.cdDddContribuinte = cdDddContribuinte;
	}

	/**
	 * Get: cdFavorecido.
	 * 
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 * 
	 * @param cdFavorecido
	 *            the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdFluxoInclusao.
	 * 
	 * @return cdFluxoInclusao
	 */
	public String getCdFluxoInclusao() {
		return cdFluxoInclusao;
	}

	/**
	 * Set: cdFluxoInclusao.
	 * 
	 * @param cdFluxoInclusao
	 *            the cd fluxo inclusao
	 */
	public void setCdFluxoInclusao(String cdFluxoInclusao) {
		this.cdFluxoInclusao = cdFluxoInclusao;
	}

	/**
	 * Get: cdFluxoManutencao.
	 * 
	 * @return cdFluxoManutencao
	 */
	public String getCdFluxoManutencao() {
		return cdFluxoManutencao;
	}

	/**
	 * Set: cdFluxoManutencao.
	 * 
	 * @param cdFluxoManutencao
	 *            the cd fluxo manutencao
	 */
	public void setCdFluxoManutencao(String cdFluxoManutencao) {
		this.cdFluxoManutencao = cdFluxoManutencao;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 * 
	 * @return cdIndicadorEconomicoMoeda
	 */
	public Integer getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 * 
	 * @param cdIndicadorEconomicoMoeda
	 *            the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(Integer cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdIndicadorModalidadePgit.
	 * 
	 * @return cdIndicadorModalidadePgit
	 */
	public Integer getCdIndicadorModalidadePgit() {
		return cdIndicadorModalidadePgit;
	}

	/**
	 * Set: cdIndicadorModalidadePgit.
	 * 
	 * @param cdIndicadorModalidadePgit
	 *            the cd indicador modalidade pgit
	 */
	public void setCdIndicadorModalidadePgit(Integer cdIndicadorModalidadePgit) {
		this.cdIndicadorModalidadePgit = cdIndicadorModalidadePgit;
	}

	/**
	 * Get: cdInscricaoEstadualContribuinte.
	 * 
	 * @return cdInscricaoEstadualContribuinte
	 */
	public String getCdInscricaoEstadualContribuinte() {
		return cdInscricaoEstadualContribuinte;
	}

	/**
	 * Set: cdInscricaoEstadualContribuinte.
	 * 
	 * @param cdInscricaoEstadualContribuinte
	 *            the cd inscricao estadual contribuinte
	 */
	public void setCdInscricaoEstadualContribuinte(
			String cdInscricaoEstadualContribuinte) {
		this.cdInscricaoEstadualContribuinte = cdInscricaoEstadualContribuinte;
	}

	/**
	 * Get: cdListaDebito.
	 * 
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 * 
	 * @param cdListaDebito
	 *            the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdMotivoSituacaoPagamento.
	 * 
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}

	/**
	 * Set: cdMotivoSituacaoPagamento.
	 * 
	 * @param cdMotivoSituacaoPagamento
	 *            the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}

	/**
	 * Get: cdOrFavorecido.
	 * 
	 * @return cdOrFavorecido
	 */
	public String getCdOrFavorecido() {
		return cdOrFavorecido;
	}

	/**
	 * Set: cdOrFavorecido.
	 * 
	 * @param cdOrFavorecido
	 *            the cd or favorecido
	 */
	public void setCdOrFavorecido(String cdOrFavorecido) {
		this.cdOrFavorecido = cdOrFavorecido;
	}

	/**
	 * Get: cdPercentualReceita.
	 * 
	 * @return cdPercentualReceita
	 */
	public BigDecimal getCdPercentualReceita() {
		return cdPercentualReceita;
	}

	/**
	 * Set: cdPercentualReceita.
	 * 
	 * @param cdPercentualReceita
	 *            the cd percentual receita
	 */
	public void setCdPercentualReceita(BigDecimal cdPercentualReceita) {
		this.cdPercentualReceita = cdPercentualReceita;
	}

	/**
	 * Get: cdPlacaVeiculo.
	 * 
	 * @return cdPlacaVeiculo
	 */
	public String getCdPlacaVeiculo() {
		return cdPlacaVeiculo;
	}

	/**
	 * Set: cdPlacaVeiculo.
	 * 
	 * @param cdPlacaVeiculo
	 *            the cd placa veiculo
	 */
	public void setCdPlacaVeiculo(String cdPlacaVeiculo) {
		this.cdPlacaVeiculo = cdPlacaVeiculo;
	}

	/**
	 * Get: cdReceita.
	 * 
	 * @return cdReceita
	 */
	public String getCdReceita() {
		return cdReceita;
	}

	/**
	 * Set: cdReceita.
	 * 
	 * @param cdReceita
	 *            the cd receita
	 */
	public void setCdReceita(String cdReceita) {
		this.cdReceita = cdReceita;
	}

	/**
	 * Get: cdSerieDocumento.
	 * 
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 * 
	 * @param cdSerieDocumento
	 *            the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoContrato.
	 * 
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 * 
	 * @param cdSituacaoContrato
	 *            the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 * 
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 * 
	 * @param cdSituacaoOperacaoPagamento
	 *            the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(
			Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 * 
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 * 
	 * @param cdTipoCanalInclusao
	 *            the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 * 
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 * 
	 * @param cdTipoCanalManutencao
	 *            the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoContaFavorecido.
	 * 
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}

	/**
	 * Set: cdTipoContaFavorecido.
	 * 
	 * @param cdTipoContaFavorecido
	 *            the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}

	/**
	 * Get: cdTipoDocumento.
	 * 
	 * @return cdTipoDocumento
	 */
	public Integer getCdTipoDocumento() {
		return cdTipoDocumento;
	}

	/**
	 * Set: cdTipoDocumento.
	 * 
	 * @param cdTipoDocumento
	 *            the cd tipo documento
	 */
	public void setCdTipoDocumento(Integer cdTipoDocumento) {
		this.cdTipoDocumento = cdTipoDocumento;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 * 
	 * @return cdTipoInscricaoFavorecido
	 */
	public Integer getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 * 
	 * @param cdTipoInscricaoPagador
	 *            the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(Integer cdTipoInscricaoPagador) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoPagador;
	}

	/**
	 * Get: cdTipoManutencao.
	 * 
	 * @return cdTipoManutencao
	 */
	public Integer getCdTipoManutencao() {
		return cdTipoManutencao;
	}

	/**
	 * Set: cdTipoManutencao.
	 * 
	 * @param cdTipoManutencao
	 *            the cd tipo manutencao
	 */
	public void setCdTipoManutencao(Integer cdTipoManutencao) {
		this.cdTipoManutencao = cdTipoManutencao;
	}

	/**
	 * Get: cdUfFavorecido.
	 * 
	 * @return cdUfFavorecido
	 */
	public String getCdUfFavorecido() {
		return cdUfFavorecido;
	}

	/**
	 * Set: cdUfFavorecido.
	 * 
	 * @param cdUfFavorecido
	 *            the cd uf favorecido
	 */
	public void setCdUfFavorecido(String cdUfFavorecido) {
		this.cdUfFavorecido = cdUfFavorecido;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 * 
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 * 
	 * @param cdUsuarioInclusaoExterno
	 *            the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: cdUsuarioInclusaoInterno.
	 * 
	 * @return cdUsuarioInclusaoInterno
	 */
	public String getCdUsuarioInclusaoInterno() {
		return cdUsuarioInclusaoInterno;
	}

	/**
	 * Set: cdUsuarioInclusaoInterno.
	 * 
	 * @param cdUsuarioInclusaoInterno
	 *            the cd usuario inclusao interno
	 */
	public void setCdUsuarioInclusaoInterno(String cdUsuarioInclusaoInterno) {
		this.cdUsuarioInclusaoInterno = cdUsuarioInclusaoInterno;
	}

	/**
	 * Get: cdUsuarioManutencaoExterno.
	 * 
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}

	/**
	 * Set: cdUsuarioManutencaoExterno.
	 * 
	 * @param cdUsuarioManutencaoExterno
	 *            the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	/**
	 * Get: cdUsuarioManutencaoInterno.
	 * 
	 * @return cdUsuarioManutencaoInterno
	 */
	public String getCdUsuarioManutencaoInterno() {
		return cdUsuarioManutencaoInterno;
	}

	/**
	 * Set: cdUsuarioManutencaoInterno.
	 * 
	 * @param cdUsuarioManutencaoInterno
	 *            the cd usuario manutencao interno
	 */
	public void setCdUsuarioManutencaoInterno(String cdUsuarioManutencaoInterno) {
		this.cdUsuarioManutencaoInterno = cdUsuarioManutencaoInterno;
	}

	/**
	 * Get: codMensagem.
	 * 
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 * 
	 * @param codMensagem
	 *            the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsAgenciaDebito.
	 * 
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 * 
	 * @param dsAgenciaDebito
	 *            the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 * 
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 * 
	 * @param dsAgenciaFavorecido
	 *            the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsApuracaoTributo.
	 * 
	 * @return dsApuracaoTributo
	 */
	public String getDsApuracaoTributo() {
		return dsApuracaoTributo;
	}

	/**
	 * Set: dsApuracaoTributo.
	 * 
	 * @param dsApuracaoTributo
	 *            the ds apuracao tributo
	 */
	public void setDsApuracaoTributo(String dsApuracaoTributo) {
		this.dsApuracaoTributo = dsApuracaoTributo;
	}

	/**
	 * Get: dsBancoDebito.
	 * 
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 * 
	 * @param dsBancoDebito
	 *            the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsBancoFavorecido.
	 * 
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 * 
	 * @param dsBancoFavorecido
	 *            the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContrato.
	 * 
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 * 
	 * @param dsContrato
	 *            the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 * 
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 * 
	 * @param dsMensagemPrimeiraLinha
	 *            the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 * 
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 * 
	 * @param dsMensagemSegundaLinha
	 *            the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}

	/**
	 * Get: dsMotivoSituacao.
	 * 
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 * 
	 * @param dsMotivoSituacao
	 *            the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsOrFavorecido.
	 * 
	 * @return dsOrFavorecido
	 */
	public String getDsOrFavorecido() {
		return dsOrFavorecido;
	}

	/**
	 * Set: dsOrFavorecido.
	 * 
	 * @param dsOrFavorecido
	 *            the ds or favorecido
	 */
	public void setDsOrFavorecido(String dsOrFavorecido) {
		this.dsOrFavorecido = dsOrFavorecido;
	}

	/**
	 * Get: dsPagamento.
	 * 
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 * 
	 * @param dsPagamento
	 *            the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 * 
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 * 
	 * @param dsTipoCanalInclusao
	 *            the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dsTipoCanalManutencao.
	 * 
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	/**
	 * Set: dsTipoCanalManutencao.
	 * 
	 * @param dsTipoCanalManutencao
	 *            the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	/**
	 * Get: dsTipoContaDebito.
	 * 
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}

	/**
	 * Set: dsTipoContaDebito.
	 * 
	 * @param dsTipoContaDebito
	 *            the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 * 
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 * 
	 * @param dsTipoContaFavorecido
	 *            the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsTipoDocumento.
	 * 
	 * @return dsTipoDocumento
	 */
	public String getDsTipoDocumento() {
		return dsTipoDocumento;
	}

	/**
	 * Set: dsTipoDocumento.
	 * 
	 * @param dsTipoDocumento
	 *            the ds tipo documento
	 */
	public void setDsTipoDocumento(String dsTipoDocumento) {
		this.dsTipoDocumento = dsTipoDocumento;
	}

	/**
	 * Get: dsTipoManutencao.
	 * 
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 * 
	 * @param dsTipoManutencao
	 *            the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: dsTributo.
	 * 
	 * @return dsTributo
	 */
	public String getDsTributo() {
		return dsTributo;
	}

	/**
	 * Set: dsTributo.
	 * 
	 * @param dsTributo
	 *            the ds tributo
	 */
	public void setDsTributo(String dsTributo) {
		this.dsTributo = dsTributo;
	}

	/**
	 * Get: dsUfFavorecido.
	 * 
	 * @return dsUfFavorecido
	 */
	public String getDsUfFavorecido() {
		return dsUfFavorecido;
	}

	/**
	 * Set: dsUfFavorecido.
	 * 
	 * @param dsUfFavorecido
	 *            the ds uf favorecido
	 */
	public void setDsUfFavorecido(String dsUfFavorecido) {
		this.dsUfFavorecido = dsUfFavorecido;
	}

	/**
	 * Get: dsUsoEmpresa.
	 * 
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 * 
	 * @param dsUsoEmpresa
	 *            the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtAgendamento.
	 * 
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 * 
	 * @param dtAgendamento
	 *            the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 * 
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 * 
	 * @param dtEmissaoDocumento
	 *            the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtInclusao.
	 * 
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 * 
	 * @param dtInclusao
	 *            the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 * 
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 * 
	 * @param dtManutencao
	 *            the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: dtReferencia.
	 * 
	 * @return dtReferencia
	 */
	public String getDtReferencia() {
		return dtReferencia;
	}

	/**
	 * Set: dtReferencia.
	 * 
	 * @param dtReferencia
	 *            the dt referencia
	 */
	public void setDtReferencia(String dtReferencia) {
		this.dtReferencia = dtReferencia;
	}

	/**
	 * Get: dtVencimento.
	 * 
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 * 
	 * @param dtVencimento
	 *            the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: hrInclusao.
	 * 
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 * 
	 * @param hrInclusao
	 *            the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 * 
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 * 
	 * @param hrManutencao
	 *            the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mensagem.
	 * 
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 * 
	 * @param mensagem
	 *            the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrCota.
	 * 
	 * @return nrCota
	 */
	public String getNrCota() {
		return nrCota;
	}

	/**
	 * Set: nrCota.
	 * 
	 * @param nrCota
	 *            the nr cota
	 */
	public void setNrCota(String nrCota) {
		this.nrCota = nrCota;
	}

	/**
	 * Get: nrDocumento.
	 * 
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 * 
	 * @param nrDocumento
	 *            the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 * 
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 * 
	 * @param nrLoteArquivoRemessa
	 *            the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nrParcela.
	 * 
	 * @return nrParcela
	 */
	public String getNrParcela() {
		return nrParcela;
	}

	/**
	 * Set: nrParcela.
	 * 
	 * @param nrParcela
	 *            the nr parcela
	 */
	public void setNrParcela(String nrParcela) {
		this.nrParcela = nrParcela;
	}

	/**
	 * Get: nrReferencia.
	 * 
	 * @return nrReferencia
	 */
	public String getNrReferencia() {
		return nrReferencia;
	}

	/**
	 * Set: nrReferencia.
	 * 
	 * @param nrReferencia
	 *            the nr referencia
	 */
	public void setNrReferencia(String nrReferencia) {
		this.nrReferencia = nrReferencia;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 * 
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 * 
	 * @param nrSequenciaArquivoRemessa
	 *            the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: nrTelefoneContribuinte.
	 * 
	 * @return nrTelefoneContribuinte
	 */
	public String getNrTelefoneContribuinte() {
		return nrTelefoneContribuinte;
	}

	/**
	 * Set: nrTelefoneContribuinte.
	 * 
	 * @param nrTelefoneContribuinte
	 *            the nr telefone contribuinte
	 */
	public void setNrTelefoneContribuinte(String nrTelefoneContribuinte) {
		this.nrTelefoneContribuinte = nrTelefoneContribuinte;
	}

	/**
	 * Get: qtMoeda.
	 * 
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 * 
	 * @param qtMoeda
	 *            the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: vlAbatimento.
	 * 
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}

	/**
	 * Set: vlAbatimento.
	 * 
	 * @param vlAbatimento
	 *            the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}

	/**
	 * Get: vlAcrescimo.
	 * 
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}

	/**
	 * Set: vlAcrescimo.
	 * 
	 * @param vlAcrescimo
	 *            the vl acrescimo
	 */
	public void setVlAcrescimo(BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	/**
	 * Get: vlAgendado.
	 * 
	 * @return vlAgendado
	 */
	public BigDecimal getVlAgendado() {
		return vlAgendado;
	}

	/**
	 * Set: vlAgendado.
	 * 
	 * @param vlAgendado
	 *            the vl agendado
	 */
	public void setVlAgendado(BigDecimal vlAgendado) {
		this.vlAgendado = vlAgendado;
	}

	/**
	 * Get: vlAtual.
	 * 
	 * @return vlAtual
	 */
	public BigDecimal getVlAtual() {
		return vlAtual;
	}

	/**
	 * Set: vlAtual.
	 * 
	 * @param vlAtual
	 *            the vl atual
	 */
	public void setVlAtual(BigDecimal vlAtual) {
		this.vlAtual = vlAtual;
	}

	/**
	 * Get: vlDocumento.
	 * 
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 * 
	 * @param vlDocumento
	 *            the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlEfetivo.
	 * 
	 * @return vlEfetivo
	 */
	public BigDecimal getVlEfetivo() {
		return vlEfetivo;
	}

	/**
	 * Set: vlEfetivo.
	 * 
	 * @param vlEfetivo
	 *            the vl efetivo
	 */
	public void setVlEfetivo(BigDecimal vlEfetivo) {
		this.vlEfetivo = vlEfetivo;
	}

	/**
	 * Get: vlHonorario.
	 * 
	 * @return vlHonorario
	 */
	public BigDecimal getVlHonorario() {
		return vlHonorario;
	}

	/**
	 * Set: vlHonorario.
	 * 
	 * @param vlHonorario
	 *            the vl honorario
	 */
	public void setVlHonorario(BigDecimal vlHonorario) {
		this.vlHonorario = vlHonorario;
	}

	/**
	 * Get: vlMora.
	 * 
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}

	/**
	 * Set: vlMora.
	 * 
	 * @param vlMora
	 *            the vl mora
	 */
	public void setVlMora(BigDecimal vlMora) {
		this.vlMora = vlMora;
	}

	/**
	 * Get: vlMulta.
	 * 
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}

	/**
	 * Set: vlMulta.
	 * 
	 * @param vlMulta
	 *            the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}

	/**
	 * Get: vlPrincipal.
	 * 
	 * @return vlPrincipal
	 */
	public BigDecimal getVlPrincipal() {
		return vlPrincipal;
	}

	/**
	 * Set: vlPrincipal.
	 * 
	 * @param vlPrincipal
	 *            the vl principal
	 */
	public void setVlPrincipal(BigDecimal vlPrincipal) {
		this.vlPrincipal = vlPrincipal;
	}

	/**
	 * Get: vlReceita.
	 * 
	 * @return vlReceita
	 */
	public BigDecimal getVlReceita() {
		return vlReceita;
	}

	/**
	 * Set: vlReceita.
	 * 
	 * @param vlReceita
	 *            the vl receita
	 */
	public void setVlReceita(BigDecimal vlReceita) {
		this.vlReceita = vlReceita;
	}

	/**
	 * Get: vlTotal.
	 * 
	 * @return vlTotal
	 */
	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	/**
	 * Set: vlTotal.
	 * 
	 * @param vlTotal
	 *            the vl total
	 */
	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	/**
	 * Get: dsMoeda.
	 * 
	 * @return dsMoeda
	 */
	public String getDsMoeda() {
		return dsMoeda;
	}

	/**
	 * Set: dsMoeda.
	 * 
	 * @param dsMoeda
	 *            the ds moeda
	 */
	public void setDsMoeda(String dsMoeda) {
		this.dsMoeda = dsMoeda;
	}

	/**
	 * Get: agenciaCreditoFormatada.
	 * 
	 * @return agenciaCreditoFormatada
	 */
	public String getAgenciaCreditoFormatada() {
		return agenciaCreditoFormatada;
	}

	/**
	 * Set: agenciaCreditoFormatada.
	 * 
	 * @param agenciaCreditoFormatada
	 *            the agencia credito formatada
	 */
	public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
		this.agenciaCreditoFormatada = agenciaCreditoFormatada;
	}

	/**
	 * Get: agenciaDebitoFormatada.
	 * 
	 * @return agenciaDebitoFormatada
	 */
	public String getAgenciaDebitoFormatada() {
		return agenciaDebitoFormatada;
	}

	/**
	 * Set: agenciaDebitoFormatada.
	 * 
	 * @param agenciaDebitoFormatada
	 *            the agencia debito formatada
	 */
	public void setAgenciaDebitoFormatada(String agenciaDebitoFormatada) {
		this.agenciaDebitoFormatada = agenciaDebitoFormatada;
	}

	/**
	 * Get: bancoCreditoFormatado.
	 * 
	 * @return bancoCreditoFormatado
	 */
	public String getBancoCreditoFormatado() {
		return bancoCreditoFormatado;
	}

	/**
	 * Set: bancoCreditoFormatado.
	 * 
	 * @param bancoCreditoFormatado
	 *            the banco credito formatado
	 */
	public void setBancoCreditoFormatado(String bancoCreditoFormatado) {
		this.bancoCreditoFormatado = bancoCreditoFormatado;
	}

	/**
	 * Get: bancoDebitoFormatado.
	 * 
	 * @return bancoDebitoFormatado
	 */
	public String getBancoDebitoFormatado() {
		return bancoDebitoFormatado;
	}

	/**
	 * Set: bancoDebitoFormatado.
	 * 
	 * @param bancoDebitoFormatado
	 *            the banco debito formatado
	 */
	public void setBancoDebitoFormatado(String bancoDebitoFormatado) {
		this.bancoDebitoFormatado = bancoDebitoFormatado;
	}

	/**
	 * Get: cdAgenciaCredito.
	 * 
	 * @return cdAgenciaCredito
	 */
	public Integer getCdAgenciaCredito() {
		return cdAgenciaCredito;
	}

	/**
	 * Set: cdAgenciaCredito.
	 * 
	 * @param cdAgenciaCredito
	 *            the cd agencia credito
	 */
	public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
		this.cdAgenciaCredito = cdAgenciaCredito;
	}

	/**
	 * Get: cdAgenciaDebito.
	 * 
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	/**
	 * Set: cdAgenciaDebito.
	 * 
	 * @param cdAgenciaDebito
	 *            the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	/**
	 * Get: cdBancoCredito.
	 * 
	 * @return cdBancoCredito
	 */
	public Integer getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 * 
	 * @param cdBancoCredito
	 *            the cd banco credito
	 */
	public void setCdBancoCredito(Integer cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdBancoDebito.
	 * 
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	/**
	 * Set: cdBancoDebito.
	 * 
	 * @param cdBancoDebito
	 *            the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	/**
	 * Get: cdContaCredito.
	 * 
	 * @return cdContaCredito
	 */
	public Long getCdContaCredito() {
		return cdContaCredito;
	}

	/**
	 * Set: cdContaCredito.
	 * 
	 * @param cdContaCredito
	 *            the cd conta credito
	 */
	public void setCdContaCredito(Long cdContaCredito) {
		this.cdContaCredito = cdContaCredito;
	}

	/**
	 * Get: cdContaDebito.
	 * 
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * Set: cdContaDebito.
	 * 
	 * @param cdContaDebito
	 *            the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	/**
	 * Get: cdControlePagamento.
	 * 
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}

	/**
	 * Set: cdControlePagamento.
	 * 
	 * @param cdControlePagamento
	 *            the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}

	/**
	 * Get: cdCpfCnpjCliente.
	 * 
	 * @return cdCpfCnpjCliente
	 */
	public Long getCdCpfCnpjCliente() {
		return cdCpfCnpjCliente;
	}

	/**
	 * Set: cdCpfCnpjCliente.
	 * 
	 * @param cdCpfCnpjCliente
	 *            the cd cpf cnpj cliente
	 */
	public void setCdCpfCnpjCliente(Long cdCpfCnpjCliente) {
		this.cdCpfCnpjCliente = cdCpfCnpjCliente;
	}

	/**
	 * Get: cdDigAgenciaCredito.
	 * 
	 * @return cdDigAgenciaCredito
	 */
	public String getCdDigAgenciaCredito() {
		return cdDigAgenciaCredito;
	}

	/**
	 * Set: cdDigAgenciaCredito.
	 * 
	 * @param cdDigAgenciaCredito
	 *            the cd dig agencia credito
	 */
	public void setCdDigAgenciaCredito(String cdDigAgenciaCredito) {
		this.cdDigAgenciaCredito = cdDigAgenciaCredito;
	}

	/**
	 * Get: cdDigAgenciaDebto.
	 * 
	 * @return cdDigAgenciaDebto
	 */
	public String getCdDigAgenciaDebto() {
		return cdDigAgenciaDebto;
	}

	/**
	 * Set: cdDigAgenciaDebto.
	 * 
	 * @param cdDigAgenciaDebto
	 *            the cd dig agencia debto
	 */
	public void setCdDigAgenciaDebto(String cdDigAgenciaDebto) {
		this.cdDigAgenciaDebto = cdDigAgenciaDebto;
	}

	/**
	 * Get: cdDigContaCredito.
	 * 
	 * @return cdDigContaCredito
	 */
	public String getCdDigContaCredito() {
		return cdDigContaCredito;
	}

	/**
	 * Set: cdDigContaCredito.
	 * 
	 * @param cdDigContaCredito
	 *            the cd dig conta credito
	 */
	public void setCdDigContaCredito(String cdDigContaCredito) {
		this.cdDigContaCredito = cdDigContaCredito;
	}

	/**
	 * Get: cdIndicadorAuto.
	 * 
	 * @return cdIndicadorAuto
	 */
	public Integer getCdIndicadorAuto() {
		return cdIndicadorAuto;
	}

	/**
	 * Set: cdIndicadorAuto.
	 * 
	 * @param cdIndicadorAuto
	 *            the cd indicador auto
	 */
	public void setCdIndicadorAuto(Integer cdIndicadorAuto) {
		this.cdIndicadorAuto = cdIndicadorAuto;
	}

	/**
	 * Get: cdIndicadorSemConsulta.
	 * 
	 * @return cdIndicadorSemConsulta
	 */
	public Integer getCdIndicadorSemConsulta() {
		return cdIndicadorSemConsulta;
	}

	/**
	 * Set: cdIndicadorSemConsulta.
	 * 
	 * @param cdIndicadorSemConsulta
	 *            the cd indicador sem consulta
	 */
	public void setCdIndicadorSemConsulta(Integer cdIndicadorSemConsulta) {
		this.cdIndicadorSemConsulta = cdIndicadorSemConsulta;
	}

	/**
	 * Get: cdSituacaoPagamento.
	 * 
	 * @return cdSituacaoPagamento
	 */
	public String getCdSituacaoPagamento() {
		return cdSituacaoPagamento;
	}

	/**
	 * Set: cdSituacaoPagamento.
	 * 
	 * @param cdSituacaoPagamento
	 *            the cd situacao pagamento
	 */
	public void setCdSituacaoPagamento(String cdSituacaoPagamento) {
		this.cdSituacaoPagamento = cdSituacaoPagamento;
	}

	/**
	 * Get: contaCreditoFormatada.
	 * 
	 * @return contaCreditoFormatada
	 */
	public String getContaCreditoFormatada() {
		return contaCreditoFormatada;
	}

	/**
	 * Set: contaCreditoFormatada.
	 * 
	 * @param contaCreditoFormatada
	 *            the conta credito formatada
	 */
	public void setContaCreditoFormatada(String contaCreditoFormatada) {
		this.contaCreditoFormatada = contaCreditoFormatada;
	}

	/**
	 * Get: contaDebitoFormatada.
	 * 
	 * @return contaDebitoFormatada
	 */
	public String getContaDebitoFormatada() {
		return contaDebitoFormatada;
	}

	/**
	 * Set: contaDebitoFormatada.
	 * 
	 * @param contaDebitoFormatada
	 *            the conta debito formatada
	 */
	public void setContaDebitoFormatada(String contaDebitoFormatada) {
		this.contaDebitoFormatada = contaDebitoFormatada;
	}

	/**
	 * Get: cpfCnpjClienteFormatado.
	 * 
	 * @return cpfCnpjClienteFormatado
	 */
	public String getCpfCnpjClienteFormatado() {
		return cpfCnpjClienteFormatado;
	}

	/**
	 * Set: cpfCnpjClienteFormatado.
	 * 
	 * @param cpfCnpjClienteFormatado
	 *            the cpf cnpj cliente formatado
	 */
	public void setCpfCnpjClienteFormatado(String cpfCnpjClienteFormatado) {
		this.cpfCnpjClienteFormatado = cpfCnpjClienteFormatado;
	}

	/**
	 * Get: dsDigAgenciaDebito.
	 * 
	 * @return dsDigAgenciaDebito
	 */
	public String getDsDigAgenciaDebito() {
		return dsDigAgenciaDebito;
	}

	/**
	 * Set: dsDigAgenciaDebito.
	 * 
	 * @param dsDigAgenciaDebito
	 *            the ds dig agencia debito
	 */
	public void setDsDigAgenciaDebito(String dsDigAgenciaDebito) {
		this.dsDigAgenciaDebito = dsDigAgenciaDebito;
	}

	/**
	 * Get: dsModalidadeRelacionado.
	 * 
	 * @return dsModalidadeRelacionado
	 */
	public String getDsModalidadeRelacionado() {
		return dsModalidadeRelacionado;
	}

	/**
	 * Set: dsModalidadeRelacionado.
	 * 
	 * @param dsModalidadeRelacionado
	 *            the ds modalidade relacionado
	 */
	public void setDsModalidadeRelacionado(String dsModalidadeRelacionado) {
		this.dsModalidadeRelacionado = dsModalidadeRelacionado;
	}

	/**
	 * Get: dsNomeFavorecido.
	 * 
	 * @return dsNomeFavorecido
	 */
	public String getDsNomeFavorecido() {
		return dsNomeFavorecido;
	}

	/**
	 * Set: dsNomeFavorecido.
	 * 
	 * @param dsNomeFavorecido
	 *            the ds nome favorecido
	 */
	public void setDsNomeFavorecido(String dsNomeFavorecido) {
		this.dsNomeFavorecido = dsNomeFavorecido;
	}

	/**
	 * Get: dsPessoaJuridicaContrato.
	 * 
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}

	/**
	 * Set: dsPessoaJuridicaContrato.
	 * 
	 * @param dsPessoaJuridicaContrato
	 *            the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}

	/**
	 * Get: dsTipoServicoOperacao.
	 * 
	 * @return dsTipoServicoOperacao
	 */
	public String getDsTipoServicoOperacao() {
		return dsTipoServicoOperacao;
	}

	/**
	 * Set: dsTipoServicoOperacao.
	 * 
	 * @param dsTipoServicoOperacao
	 *            the ds tipo servico operacao
	 */
	public void setDsTipoServicoOperacao(String dsTipoServicoOperacao) {
		this.dsTipoServicoOperacao = dsTipoServicoOperacao;
	}

	/**
	 * Get: dsUnidadeAgencia.
	 * 
	 * @return dsUnidadeAgencia
	 */
	public String getDsUnidadeAgencia() {
		return dsUnidadeAgencia;
	}

	/**
	 * Set: dsUnidadeAgencia.
	 * 
	 * @param dsUnidadeAgencia
	 *            the ds unidade agencia
	 */
	public void setDsUnidadeAgencia(String dsUnidadeAgencia) {
		this.dsUnidadeAgencia = dsUnidadeAgencia;
	}

	/**
	 * Get: dtPagamento.
	 * 
	 * @return dtPagamento
	 */
	public Date getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 * 
	 * @param dtPagamento
	 *            the dt pagamento
	 */
	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: nmInscriFavorecido.
	 * 
	 * @return nmInscriFavorecido
	 */
	public String getNmInscriFavorecido() {
		return nmInscriFavorecido;
	}

	/**
	 * Set: nmInscriFavorecido.
	 * 
	 * @param nmInscriFavorecido
	 *            the nm inscri favorecido
	 */
	public void setNmInscriFavorecido(String nmInscriFavorecido) {
		this.nmInscriFavorecido = nmInscriFavorecido;
	}

	/**
	 * Get: nomeCliente.
	 * 
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}

	/**
	 * Set: nomeCliente.
	 * 
	 * @param nomeCliente
	 *            the nome cliente
	 */
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public String getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(String nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: numeroInscricaoFavorecidoFormatado.
	 * 
	 * @return numeroInscricaoFavorecidoFormatado
	 */
	public String getNumeroInscricaoFavorecidoFormatado() {
		return numeroInscricaoFavorecidoFormatado;
	}

	/**
	 * Set: numeroInscricaoFavorecidoFormatado.
	 * 
	 * @param numeroInscricaoFavorecidoFormatado
	 *            the numero inscricao favorecido formatado
	 */
	public void setNumeroInscricaoFavorecidoFormatado(
			String numeroInscricaoFavorecidoFormatado) {
		this.numeroInscricaoFavorecidoFormatado = numeroInscricaoFavorecidoFormatado;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 * 
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 * 
	 * @param dtDevolucaoEstorno
	 *            the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

	/**
	 * Get: dtFloatingPagamento.
	 * 
	 * @return dtFloatingPagamento
	 */
	public String getDtFloatingPagamento() {
		return dtFloatingPagamento;
	}

	/**
	 * Set: dtFloatingPagamento.
	 * 
	 * @param dtFloatingPagamento
	 *            the dt floating pagamento
	 */
	public void setDtFloatingPagamento(String dtFloatingPagamento) {
		this.dtFloatingPagamento = dtFloatingPagamento;
	}

	/**
	 * Get: dtEfetivFloatPgto.
	 * 
	 * @return dtEfetivFloatPgto
	 */
	public String getDtEfetivFloatPgto() {
		return dtEfetivFloatPgto;
	}

	/**
	 * Set: dtEfetivFloatPgto.
	 * 
	 * @param dtEfetivFloatPgto
	 *            the dt efetiv float pgto
	 */
	public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
		this.dtEfetivFloatPgto = dtEfetivFloatPgto;
	}

	/**
	 * Get: vlFloatingPagamento.
	 * 
	 * @return vlFloatingPagamento
	 */
	public BigDecimal getVlFloatingPagamento() {
		return vlFloatingPagamento;
	}

	/**
	 * Set: vlFloatingPagamento.
	 * 
	 * @param vlFloatingPagamento
	 *            the vl floating pagamento
	 */
	public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
		this.vlFloatingPagamento = vlFloatingPagamento;
	}

	/**
	 * @return the cdLoteInterno
	 */
	public Long getCdLoteInterno() {
		return cdLoteInterno;
	}

	/**
	 * @param cdLoteInterno
	 *            the cdLoteInterno to set
	 */
	public void setCdLoteInterno(Long cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}

	/**
	 * @return the dsIndicadorAutorizacao
	 */
	public String getDsIndicadorAutorizacao() {
		return dsIndicadorAutorizacao;
	}

	/**
	 * @param dsIndicadorAutorizacao
	 *            the dsIndicadorAutorizacao to set
	 */
	public void setDsIndicadorAutorizacao(String dsIndicadorAutorizacao) {
		this.dsIndicadorAutorizacao = dsIndicadorAutorizacao;
	}

	public String getDsTipoLayout() {
		return dsTipoLayout;
	}

	public void setDsTipoLayout(String dsTipoLayout) {
		this.dsTipoLayout = dsTipoLayout;
	}

}
