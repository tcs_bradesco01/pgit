package br.com.bradesco.web.pgit.service.business.confcestastarifas.dto;

import java.io.Serializable;

public class ConfCestaTarifaEntradaDTO implements Serializable{
	
	private static final long serialVersionUID = -1337090749093308499L;
	
	private Integer maxOcorrencias; 
	private Long cdPessoasJuridicaContrato; 
	private Integer cdTipoContratoNegocio; 
	private Long numSequenciaNegocio;
	
	public ConfCestaTarifaEntradaDTO() {
		super();
	}

	public ConfCestaTarifaEntradaDTO(Integer maxOcorrencias,
			Long cdPessoasJuridicaContrato, Integer cdTipoContratoNegocio,
			Long numSequenciaNegocio) {
		super();
		this.maxOcorrencias = maxOcorrencias;
		this.cdPessoasJuridicaContrato = cdPessoasJuridicaContrato;
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		this.numSequenciaNegocio = numSequenciaNegocio;
	}

	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}

	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}

	public Long getCdPessoasJuridicaContrato() {
		return cdPessoasJuridicaContrato;
	}

	public void setCdPessoasJuridicaContrato(Long cdPessoasJuridicaContrato) {
		this.cdPessoasJuridicaContrato = cdPessoasJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNumSequenciaNegocio() {
		return numSequenciaNegocio;
	}

	public void setNumSequenciaNegocio(Long numSequenciaNegocio) {
		this.numSequenciaNegocio = numSequenciaNegocio;
	}

}
