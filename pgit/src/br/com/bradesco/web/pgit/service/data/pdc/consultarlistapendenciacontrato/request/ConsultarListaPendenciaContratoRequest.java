/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarListaPendenciaContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarListaPendenciaContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdAcesso
     */
    private int _cdAcesso = 0;

    /**
     * keeps track of state for field: _cdAcesso
     */
    private boolean _has_cdAcesso;

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdPendenciaPagamentoIntegrado
     */
    private long _cdPendenciaPagamentoIntegrado = 0;

    /**
     * keeps track of state for field: _cdPendenciaPagamentoIntegrad
     */
    private boolean _has_cdPendenciaPagamentoIntegrado;

    /**
     * Field _cdSituacaoPendenciaContrato
     */
    private int _cdSituacaoPendenciaContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoPendenciaContrato
     */
    private boolean _has_cdSituacaoPendenciaContrato;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoUnidade
     */
    private int _cdTipoUnidade = 0;

    /**
     * keeps track of state for field: _cdTipoUnidade
     */
    private boolean _has_cdTipoUnidade;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _cdTipoBaixa
     */
    private int _cdTipoBaixa = 0;

    /**
     * keeps track of state for field: _cdTipoBaixa
     */
    private boolean _has_cdTipoBaixa;

    /**
     * Field _dtInicioBaixa
     */
    private java.lang.String _dtInicioBaixa;

    /**
     * Field _dtFimBaixa
     */
    private java.lang.String _dtFimBaixa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarListaPendenciaContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.request.ConsultarListaPendenciaContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcesso
     * 
     */
    public void deleteCdAcesso()
    {
        this._has_cdAcesso= false;
    } //-- void deleteCdAcesso() 

    /**
     * Method deleteCdPendenciaPagamentoIntegrado
     * 
     */
    public void deleteCdPendenciaPagamentoIntegrado()
    {
        this._has_cdPendenciaPagamentoIntegrado= false;
    } //-- void deleteCdPendenciaPagamentoIntegrado() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoPendenciaContrato
     * 
     */
    public void deleteCdSituacaoPendenciaContrato()
    {
        this._has_cdSituacaoPendenciaContrato= false;
    } //-- void deleteCdSituacaoPendenciaContrato() 

    /**
     * Method deleteCdTipoBaixa
     * 
     */
    public void deleteCdTipoBaixa()
    {
        this._has_cdTipoBaixa= false;
    } //-- void deleteCdTipoBaixa() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoUnidade
     * 
     */
    public void deleteCdTipoUnidade()
    {
        this._has_cdTipoUnidade= false;
    } //-- void deleteCdTipoUnidade() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdAcesso'.
     * 
     * @return int
     * @return the value of field 'cdAcesso'.
     */
    public int getCdAcesso()
    {
        return this._cdAcesso;
    } //-- int getCdAcesso() 

    /**
     * Returns the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @return long
     * @return the value of field 'cdPendenciaPagamentoIntegrado'.
     */
    public long getCdPendenciaPagamentoIntegrado()
    {
        return this._cdPendenciaPagamentoIntegrado;
    } //-- long getCdPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoPendenciaContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPendenciaContrato'.
     */
    public int getCdSituacaoPendenciaContrato()
    {
        return this._cdSituacaoPendenciaContrato;
    } //-- int getCdSituacaoPendenciaContrato() 

    /**
     * Returns the value of field 'cdTipoBaixa'.
     * 
     * @return int
     * @return the value of field 'cdTipoBaixa'.
     */
    public int getCdTipoBaixa()
    {
        return this._cdTipoBaixa;
    } //-- int getCdTipoBaixa() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoUnidade'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidade'.
     */
    public int getCdTipoUnidade()
    {
        return this._cdTipoUnidade;
    } //-- int getCdTipoUnidade() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dtFimBaixa'.
     * 
     * @return String
     * @return the value of field 'dtFimBaixa'.
     */
    public java.lang.String getDtFimBaixa()
    {
        return this._dtFimBaixa;
    } //-- java.lang.String getDtFimBaixa() 

    /**
     * Returns the value of field 'dtInicioBaixa'.
     * 
     * @return String
     * @return the value of field 'dtInicioBaixa'.
     */
    public java.lang.String getDtInicioBaixa()
    {
        return this._dtInicioBaixa;
    } //-- java.lang.String getDtInicioBaixa() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcesso()
    {
        return this._has_cdAcesso;
    } //-- boolean hasCdAcesso() 

    /**
     * Method hasCdPendenciaPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPendenciaPagamentoIntegrado()
    {
        return this._has_cdPendenciaPagamentoIntegrado;
    } //-- boolean hasCdPendenciaPagamentoIntegrado() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoPendenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPendenciaContrato()
    {
        return this._has_cdSituacaoPendenciaContrato;
    } //-- boolean hasCdSituacaoPendenciaContrato() 

    /**
     * Method hasCdTipoBaixa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoBaixa()
    {
        return this._has_cdTipoBaixa;
    } //-- boolean hasCdTipoBaixa() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoUnidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidade()
    {
        return this._has_cdTipoUnidade;
    } //-- boolean hasCdTipoUnidade() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcesso'.
     * 
     * @param cdAcesso the value of field 'cdAcesso'.
     */
    public void setCdAcesso(int cdAcesso)
    {
        this._cdAcesso = cdAcesso;
        this._has_cdAcesso = true;
    } //-- void setCdAcesso(int) 

    /**
     * Sets the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @param cdPendenciaPagamentoIntegrado the value of field
     * 'cdPendenciaPagamentoIntegrado'.
     */
    public void setCdPendenciaPagamentoIntegrado(long cdPendenciaPagamentoIntegrado)
    {
        this._cdPendenciaPagamentoIntegrado = cdPendenciaPagamentoIntegrado;
        this._has_cdPendenciaPagamentoIntegrado = true;
    } //-- void setCdPendenciaPagamentoIntegrado(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoPendenciaContrato'.
     * 
     * @param cdSituacaoPendenciaContrato the value of field
     * 'cdSituacaoPendenciaContrato'.
     */
    public void setCdSituacaoPendenciaContrato(int cdSituacaoPendenciaContrato)
    {
        this._cdSituacaoPendenciaContrato = cdSituacaoPendenciaContrato;
        this._has_cdSituacaoPendenciaContrato = true;
    } //-- void setCdSituacaoPendenciaContrato(int) 

    /**
     * Sets the value of field 'cdTipoBaixa'.
     * 
     * @param cdTipoBaixa the value of field 'cdTipoBaixa'.
     */
    public void setCdTipoBaixa(int cdTipoBaixa)
    {
        this._cdTipoBaixa = cdTipoBaixa;
        this._has_cdTipoBaixa = true;
    } //-- void setCdTipoBaixa(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoUnidade'.
     * 
     * @param cdTipoUnidade the value of field 'cdTipoUnidade'.
     */
    public void setCdTipoUnidade(int cdTipoUnidade)
    {
        this._cdTipoUnidade = cdTipoUnidade;
        this._has_cdTipoUnidade = true;
    } //-- void setCdTipoUnidade(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'dtFimBaixa'.
     * 
     * @param dtFimBaixa the value of field 'dtFimBaixa'.
     */
    public void setDtFimBaixa(java.lang.String dtFimBaixa)
    {
        this._dtFimBaixa = dtFimBaixa;
    } //-- void setDtFimBaixa(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioBaixa'.
     * 
     * @param dtInicioBaixa the value of field 'dtInicioBaixa'.
     */
    public void setDtInicioBaixa(java.lang.String dtInicioBaixa)
    {
        this._dtInicioBaixa = dtInicioBaixa;
    } //-- void setDtInicioBaixa(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarListaPendenciaContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.request.ConsultarListaPendenciaContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.request.ConsultarListaPendenciaContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.request.ConsultarListaPendenciaContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.request.ConsultarListaPendenciaContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
