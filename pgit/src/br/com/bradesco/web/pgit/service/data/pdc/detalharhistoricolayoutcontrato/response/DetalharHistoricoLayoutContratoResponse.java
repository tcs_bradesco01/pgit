/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistoricoLayoutContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistoricoLayoutContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdArquivoRetornoPagamento
     */
    private int _cdArquivoRetornoPagamento = 0;

    /**
     * keeps track of state for field: _cdArquivoRetornoPagamento
     */
    private boolean _has_cdArquivoRetornoPagamento;

    /**
     * Field _dsArquivoRetornoPagamento
     */
    private java.lang.String _dsArquivoRetornoPagamento;

    /**
     * Field _cdTipoGeracaoRetorno
     */
    private int _cdTipoGeracaoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoGeracaoRetorno
     */
    private boolean _has_cdTipoGeracaoRetorno;

    /**
     * Field _dsTipoGeracaoRetorno
     */
    private java.lang.String _dsTipoGeracaoRetorno;

    /**
     * Field _cdTipoMontaRetorno
     */
    private int _cdTipoMontaRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoMontaRetorno
     */
    private boolean _has_cdTipoMontaRetorno;

    /**
     * Field _dsTipoMontaRetorno
     */
    private java.lang.String _dsTipoMontaRetorno;

    /**
     * Field _cdTipoOcorRetorno
     */
    private int _cdTipoOcorRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoOcorRetorno
     */
    private boolean _has_cdTipoOcorRetorno;

    /**
     * Field _dsTipoOcorRetorno
     */
    private java.lang.String _dsTipoOcorRetorno;

    /**
     * Field _cdTipoOrdemRegistroRetorno
     */
    private int _cdTipoOrdemRegistroRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoOrdemRegistroRetorno
     */
    private boolean _has_cdTipoOrdemRegistroRetorno;

    /**
     * Field _dsTipoOrdemRegistroRetorno
     */
    private java.lang.String _dsTipoOrdemRegistroRetorno;

    /**
     * Field _cdTipoClassificacaoRetorno
     */
    private int _cdTipoClassificacaoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoClassificacaoRetorno
     */
    private boolean _has_cdTipoClassificacaoRetorno;

    /**
     * Field _dsTipoClassificacaoRetorno
     */
    private java.lang.String _dsTipoClassificacaoRetorno;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistoricoLayoutContratoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.response.DetalharHistoricoLayoutContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdArquivoRetornoPagamento
     * 
     */
    public void deleteCdArquivoRetornoPagamento()
    {
        this._has_cdArquivoRetornoPagamento= false;
    } //-- void deleteCdArquivoRetornoPagamento() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoClassificacaoRetorno
     * 
     */
    public void deleteCdTipoClassificacaoRetorno()
    {
        this._has_cdTipoClassificacaoRetorno= false;
    } //-- void deleteCdTipoClassificacaoRetorno() 

    /**
     * Method deleteCdTipoGeracaoRetorno
     * 
     */
    public void deleteCdTipoGeracaoRetorno()
    {
        this._has_cdTipoGeracaoRetorno= false;
    } //-- void deleteCdTipoGeracaoRetorno() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoMontaRetorno
     * 
     */
    public void deleteCdTipoMontaRetorno()
    {
        this._has_cdTipoMontaRetorno= false;
    } //-- void deleteCdTipoMontaRetorno() 

    /**
     * Method deleteCdTipoOcorRetorno
     * 
     */
    public void deleteCdTipoOcorRetorno()
    {
        this._has_cdTipoOcorRetorno= false;
    } //-- void deleteCdTipoOcorRetorno() 

    /**
     * Method deleteCdTipoOrdemRegistroRetorno
     * 
     */
    public void deleteCdTipoOrdemRegistroRetorno()
    {
        this._has_cdTipoOrdemRegistroRetorno= false;
    } //-- void deleteCdTipoOrdemRegistroRetorno() 

    /**
     * Returns the value of field 'cdArquivoRetornoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdArquivoRetornoPagamento'.
     */
    public int getCdArquivoRetornoPagamento()
    {
        return this._cdArquivoRetornoPagamento;
    } //-- int getCdArquivoRetornoPagamento() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoClassificacaoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoClassificacaoRetorno'.
     */
    public int getCdTipoClassificacaoRetorno()
    {
        return this._cdTipoClassificacaoRetorno;
    } //-- int getCdTipoClassificacaoRetorno() 

    /**
     * Returns the value of field 'cdTipoGeracaoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoGeracaoRetorno'.
     */
    public int getCdTipoGeracaoRetorno()
    {
        return this._cdTipoGeracaoRetorno;
    } //-- int getCdTipoGeracaoRetorno() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoMontaRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoMontaRetorno'.
     */
    public int getCdTipoMontaRetorno()
    {
        return this._cdTipoMontaRetorno;
    } //-- int getCdTipoMontaRetorno() 

    /**
     * Returns the value of field 'cdTipoOcorRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoOcorRetorno'.
     */
    public int getCdTipoOcorRetorno()
    {
        return this._cdTipoOcorRetorno;
    } //-- int getCdTipoOcorRetorno() 

    /**
     * Returns the value of field 'cdTipoOrdemRegistroRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoOrdemRegistroRetorno'.
     */
    public int getCdTipoOrdemRegistroRetorno()
    {
        return this._cdTipoOrdemRegistroRetorno;
    } //-- int getCdTipoOrdemRegistroRetorno() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsArquivoRetornoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsArquivoRetornoPagamento'.
     */
    public java.lang.String getDsArquivoRetornoPagamento()
    {
        return this._dsArquivoRetornoPagamento;
    } //-- java.lang.String getDsArquivoRetornoPagamento() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoClassificacaoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoClassificacaoRetorno'.
     */
    public java.lang.String getDsTipoClassificacaoRetorno()
    {
        return this._dsTipoClassificacaoRetorno;
    } //-- java.lang.String getDsTipoClassificacaoRetorno() 

    /**
     * Returns the value of field 'dsTipoGeracaoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoGeracaoRetorno'.
     */
    public java.lang.String getDsTipoGeracaoRetorno()
    {
        return this._dsTipoGeracaoRetorno;
    } //-- java.lang.String getDsTipoGeracaoRetorno() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'dsTipoMontaRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoMontaRetorno'.
     */
    public java.lang.String getDsTipoMontaRetorno()
    {
        return this._dsTipoMontaRetorno;
    } //-- java.lang.String getDsTipoMontaRetorno() 

    /**
     * Returns the value of field 'dsTipoOcorRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoOcorRetorno'.
     */
    public java.lang.String getDsTipoOcorRetorno()
    {
        return this._dsTipoOcorRetorno;
    } //-- java.lang.String getDsTipoOcorRetorno() 

    /**
     * Returns the value of field 'dsTipoOrdemRegistroRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoOrdemRegistroRetorno'.
     */
    public java.lang.String getDsTipoOrdemRegistroRetorno()
    {
        return this._dsTipoOrdemRegistroRetorno;
    } //-- java.lang.String getDsTipoOrdemRegistroRetorno() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdArquivoRetornoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdArquivoRetornoPagamento()
    {
        return this._has_cdArquivoRetornoPagamento;
    } //-- boolean hasCdArquivoRetornoPagamento() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoClassificacaoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoClassificacaoRetorno()
    {
        return this._has_cdTipoClassificacaoRetorno;
    } //-- boolean hasCdTipoClassificacaoRetorno() 

    /**
     * Method hasCdTipoGeracaoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoGeracaoRetorno()
    {
        return this._has_cdTipoGeracaoRetorno;
    } //-- boolean hasCdTipoGeracaoRetorno() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoMontaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMontaRetorno()
    {
        return this._has_cdTipoMontaRetorno;
    } //-- boolean hasCdTipoMontaRetorno() 

    /**
     * Method hasCdTipoOcorRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoOcorRetorno()
    {
        return this._has_cdTipoOcorRetorno;
    } //-- boolean hasCdTipoOcorRetorno() 

    /**
     * Method hasCdTipoOrdemRegistroRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoOrdemRegistroRetorno()
    {
        return this._has_cdTipoOrdemRegistroRetorno;
    } //-- boolean hasCdTipoOrdemRegistroRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdArquivoRetornoPagamento'.
     * 
     * @param cdArquivoRetornoPagamento the value of field
     * 'cdArquivoRetornoPagamento'.
     */
    public void setCdArquivoRetornoPagamento(int cdArquivoRetornoPagamento)
    {
        this._cdArquivoRetornoPagamento = cdArquivoRetornoPagamento;
        this._has_cdArquivoRetornoPagamento = true;
    } //-- void setCdArquivoRetornoPagamento(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoClassificacaoRetorno'.
     * 
     * @param cdTipoClassificacaoRetorno the value of field
     * 'cdTipoClassificacaoRetorno'.
     */
    public void setCdTipoClassificacaoRetorno(int cdTipoClassificacaoRetorno)
    {
        this._cdTipoClassificacaoRetorno = cdTipoClassificacaoRetorno;
        this._has_cdTipoClassificacaoRetorno = true;
    } //-- void setCdTipoClassificacaoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoGeracaoRetorno'.
     * 
     * @param cdTipoGeracaoRetorno the value of field
     * 'cdTipoGeracaoRetorno'.
     */
    public void setCdTipoGeracaoRetorno(int cdTipoGeracaoRetorno)
    {
        this._cdTipoGeracaoRetorno = cdTipoGeracaoRetorno;
        this._has_cdTipoGeracaoRetorno = true;
    } //-- void setCdTipoGeracaoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoMontaRetorno'.
     * 
     * @param cdTipoMontaRetorno the value of field
     * 'cdTipoMontaRetorno'.
     */
    public void setCdTipoMontaRetorno(int cdTipoMontaRetorno)
    {
        this._cdTipoMontaRetorno = cdTipoMontaRetorno;
        this._has_cdTipoMontaRetorno = true;
    } //-- void setCdTipoMontaRetorno(int) 

    /**
     * Sets the value of field 'cdTipoOcorRetorno'.
     * 
     * @param cdTipoOcorRetorno the value of field
     * 'cdTipoOcorRetorno'.
     */
    public void setCdTipoOcorRetorno(int cdTipoOcorRetorno)
    {
        this._cdTipoOcorRetorno = cdTipoOcorRetorno;
        this._has_cdTipoOcorRetorno = true;
    } //-- void setCdTipoOcorRetorno(int) 

    /**
     * Sets the value of field 'cdTipoOrdemRegistroRetorno'.
     * 
     * @param cdTipoOrdemRegistroRetorno the value of field
     * 'cdTipoOrdemRegistroRetorno'.
     */
    public void setCdTipoOrdemRegistroRetorno(int cdTipoOrdemRegistroRetorno)
    {
        this._cdTipoOrdemRegistroRetorno = cdTipoOrdemRegistroRetorno;
        this._has_cdTipoOrdemRegistroRetorno = true;
    } //-- void setCdTipoOrdemRegistroRetorno(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsArquivoRetornoPagamento'.
     * 
     * @param dsArquivoRetornoPagamento the value of field
     * 'dsArquivoRetornoPagamento'.
     */
    public void setDsArquivoRetornoPagamento(java.lang.String dsArquivoRetornoPagamento)
    {
        this._dsArquivoRetornoPagamento = dsArquivoRetornoPagamento;
    } //-- void setDsArquivoRetornoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoClassificacaoRetorno'.
     * 
     * @param dsTipoClassificacaoRetorno the value of field
     * 'dsTipoClassificacaoRetorno'.
     */
    public void setDsTipoClassificacaoRetorno(java.lang.String dsTipoClassificacaoRetorno)
    {
        this._dsTipoClassificacaoRetorno = dsTipoClassificacaoRetorno;
    } //-- void setDsTipoClassificacaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoGeracaoRetorno'.
     * 
     * @param dsTipoGeracaoRetorno the value of field
     * 'dsTipoGeracaoRetorno'.
     */
    public void setDsTipoGeracaoRetorno(java.lang.String dsTipoGeracaoRetorno)
    {
        this._dsTipoGeracaoRetorno = dsTipoGeracaoRetorno;
    } //-- void setDsTipoGeracaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoMontaRetorno'.
     * 
     * @param dsTipoMontaRetorno the value of field
     * 'dsTipoMontaRetorno'.
     */
    public void setDsTipoMontaRetorno(java.lang.String dsTipoMontaRetorno)
    {
        this._dsTipoMontaRetorno = dsTipoMontaRetorno;
    } //-- void setDsTipoMontaRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoOcorRetorno'.
     * 
     * @param dsTipoOcorRetorno the value of field
     * 'dsTipoOcorRetorno'.
     */
    public void setDsTipoOcorRetorno(java.lang.String dsTipoOcorRetorno)
    {
        this._dsTipoOcorRetorno = dsTipoOcorRetorno;
    } //-- void setDsTipoOcorRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoOrdemRegistroRetorno'.
     * 
     * @param dsTipoOrdemRegistroRetorno the value of field
     * 'dsTipoOrdemRegistroRetorno'.
     */
    public void setDsTipoOrdemRegistroRetorno(java.lang.String dsTipoOrdemRegistroRetorno)
    {
        this._dsTipoOrdemRegistroRetorno = dsTipoOrdemRegistroRetorno;
    } //-- void setDsTipoOrdemRegistroRetorno(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistoricoLayoutContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.response.DetalharHistoricoLayoutContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.response.DetalharHistoricoLayoutContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.response.DetalharHistoricoLayoutContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.response.DetalharHistoricoLayoutContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
