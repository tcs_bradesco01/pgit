/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias3.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias3 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsServicoLayout
     */
    private java.lang.String _dsServicoLayout;

    /**
     * Field _cdLayout
     */
    private java.lang.String _cdLayout;

    /**
     * Field _cdPerfilTrocaArq
     */
    private long _cdPerfilTrocaArq = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArq
     */
    private boolean _has_cdPerfilTrocaArq;

    /**
     * Field _dsAplicativoTransmissao
     */
    private java.lang.String _dsAplicativoTransmissao;

    /**
     * Field _dsMeioTransmissao
     */
    private java.lang.String _dsMeioTransmissao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias3() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPerfilTrocaArq
     * 
     */
    public void deleteCdPerfilTrocaArq()
    {
        this._has_cdPerfilTrocaArq= false;
    } //-- void deleteCdPerfilTrocaArq() 

    /**
     * Returns the value of field 'cdLayout'.
     * 
     * @return String
     * @return the value of field 'cdLayout'.
     */
    public java.lang.String getCdLayout()
    {
        return this._cdLayout;
    } //-- java.lang.String getCdLayout() 

    /**
     * Returns the value of field 'cdPerfilTrocaArq'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArq'.
     */
    public long getCdPerfilTrocaArq()
    {
        return this._cdPerfilTrocaArq;
    } //-- long getCdPerfilTrocaArq() 

    /**
     * Returns the value of field 'dsAplicativoTransmissao'.
     * 
     * @return String
     * @return the value of field 'dsAplicativoTransmissao'.
     */
    public java.lang.String getDsAplicativoTransmissao()
    {
        return this._dsAplicativoTransmissao;
    } //-- java.lang.String getDsAplicativoTransmissao() 

    /**
     * Returns the value of field 'dsMeioTransmissao'.
     * 
     * @return String
     * @return the value of field 'dsMeioTransmissao'.
     */
    public java.lang.String getDsMeioTransmissao()
    {
        return this._dsMeioTransmissao;
    } //-- java.lang.String getDsMeioTransmissao() 

    /**
     * Returns the value of field 'dsServicoLayout'.
     * 
     * @return String
     * @return the value of field 'dsServicoLayout'.
     */
    public java.lang.String getDsServicoLayout()
    {
        return this._dsServicoLayout;
    } //-- java.lang.String getDsServicoLayout() 

    /**
     * Method hasCdPerfilTrocaArq
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArq()
    {
        return this._has_cdPerfilTrocaArq;
    } //-- boolean hasCdPerfilTrocaArq() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdLayout'.
     * 
     * @param cdLayout the value of field 'cdLayout'.
     */
    public void setCdLayout(java.lang.String cdLayout)
    {
        this._cdLayout = cdLayout;
    } //-- void setCdLayout(java.lang.String) 

    /**
     * Sets the value of field 'cdPerfilTrocaArq'.
     * 
     * @param cdPerfilTrocaArq the value of field 'cdPerfilTrocaArq'
     */
    public void setCdPerfilTrocaArq(long cdPerfilTrocaArq)
    {
        this._cdPerfilTrocaArq = cdPerfilTrocaArq;
        this._has_cdPerfilTrocaArq = true;
    } //-- void setCdPerfilTrocaArq(long) 

    /**
     * Sets the value of field 'dsAplicativoTransmissao'.
     * 
     * @param dsAplicativoTransmissao the value of field
     * 'dsAplicativoTransmissao'.
     */
    public void setDsAplicativoTransmissao(java.lang.String dsAplicativoTransmissao)
    {
        this._dsAplicativoTransmissao = dsAplicativoTransmissao;
    } //-- void setDsAplicativoTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioTransmissao'.
     * 
     * @param dsMeioTransmissao the value of field
     * 'dsMeioTransmissao'.
     */
    public void setDsMeioTransmissao(java.lang.String dsMeioTransmissao)
    {
        this._dsMeioTransmissao = dsMeioTransmissao;
    } //-- void setDsMeioTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoLayout'.
     * 
     * @param dsServicoLayout the value of field 'dsServicoLayout'.
     */
    public void setDsServicoLayout(java.lang.String dsServicoLayout)
    {
        this._dsServicoLayout = dsServicoLayout;
    } //-- void setDsServicoLayout(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias3
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
