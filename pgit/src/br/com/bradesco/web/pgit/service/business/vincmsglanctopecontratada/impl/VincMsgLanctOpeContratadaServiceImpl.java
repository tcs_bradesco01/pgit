/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.IVincMsgLanctOpeContratadaService;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.IVincMsgLanctOpeContratadaServiceConstants;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ExcluirVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ExcluirVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperEntradaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarTiposPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarTiposPagtoSaidaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsglancoper.request.DetalharVincMsgLancOperRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsglancoper.response.DetalharVincMsgLancOperResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincmsglancoper.request.ExcluirVincMsgLancOperRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincmsglancoper.response.ExcluirVincMsgLancOperResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsglancoper.request.IncluirVincMsgLancOperRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsglancoper.response.IncluirVincMsgLancOperResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.request.ListarTiposPagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.response.ListarTiposPagtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.request.ListarVincMsgLancOperRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.response.ListarVincMsgLancOperResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: VincMsgLanctOpeContratada
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class VincMsgLanctOpeContratadaServiceImpl implements IVincMsgLanctOpeContratadaService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.IVincMsgLanctOpeContratadaService#listarVincMsgLancOper(br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarVincMsgLancOperEntradaDTO)
	 */
	public List<ListarVincMsgLancOperSaidaDTO> listarVincMsgLancOper(ListarVincMsgLancOperEntradaDTO listarVincMsgLancOperEntradaDTO) {
		
		List<ListarVincMsgLancOperSaidaDTO> listaRetorno = new ArrayList<ListarVincMsgLancOperSaidaDTO>();		
		ListarVincMsgLancOperRequest request = new ListarVincMsgLancOperRequest();
		ListarVincMsgLancOperResponse response = new ListarVincMsgLancOperResponse();
		
		request.setCdMensagemLinhaExtrato(listarVincMsgLancOperEntradaDTO.getCodigoHistorico());
		request.setCdPessoaJuridicaContrato(listarVincMsgLancOperEntradaDTO.getPessoaJurContrato());
		request.setCdProdutoServicoOperacao(listarVincMsgLancOperEntradaDTO.getTipoServico());
		request.setCdProdutoOperacaoRelacionado(listarVincMsgLancOperEntradaDTO.getModalidadeServico());
		request.setCdRelacionamentoProduto(listarVincMsgLancOperEntradaDTO.getTipoRelacionamento());
		request.setCdTipoContratoNegocio(listarVincMsgLancOperEntradaDTO.getTipoContrato());
		request.setNrSequenciaContratoNegocio(listarVincMsgLancOperEntradaDTO.getNumeroContrato());
		request.setQtOcorrencias(IVincMsgLanctOpeContratadaServiceConstants.QTDE_CONSULTAS_LISTAR);
	
		response = getFactoryAdapter().getListarVincMsgLancOperPDCAdapter().invokeProcess(request);

		ListarVincMsgLancOperSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			
			saidaDTO = new ListarVincMsgLancOperSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodigoHistorico(response.getOcorrencias(i).getCdMensagemLinhaExtrato());
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			saidaDTO.setLancCredito(response.getOcorrencias(i).getCdIdentificadorLancamentoCredito());
			saidaDTO.setLancDebito(response.getOcorrencias(i).getCdIdentificadorLancamentoDebito() );
			saidaDTO.setDsLanctCredito(response.getOcorrencias(i).getCdIdentificadorLancamentoCredito()  !=0 ? response.getOcorrencias(i).getCdIdentificadorLancamentoCredito() + " - " + response.getOcorrencias(i).getDsIdentificadorLancamentoCredito():"");
			saidaDTO.setDsLanctDebito(response.getOcorrencias(i).getCdIdentificadorLancamentoDebito() !=0 ?response.getOcorrencias(i).getCdIdentificadorLancamentoDebito() + " - " + response.getOcorrencias(i).getDsIdentificadorLancamentoDebito():"");
			saidaDTO.setModalidadeServico(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			saidaDTO.setTipoServico(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saidaDTO.setTipoRelacionamento(response.getOcorrencias(i).getCdRelacionamentoProduto());
			
			
			listaRetorno.add(saidaDTO);
			
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.IVincMsgLanctOpeContratadaService#incluirVincMsgLancOper(br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperEntradaDTO)
	 */
	public IncluirVincMsgLancOperSaidaDTO incluirVincMsgLancOper(IncluirVincMsgLancOperEntradaDTO incluirVincMsgLancOperEntradaDTO) {
		
		IncluirVincMsgLancOperSaidaDTO incluirVincMsgLancOperSaidaDTO = new IncluirVincMsgLancOperSaidaDTO();
		IncluirVincMsgLancOperRequest request = new IncluirVincMsgLancOperRequest();
		IncluirVincMsgLancOperResponse response = null;
		
		request.setCdMensagemLinhaExtrato(incluirVincMsgLancOperEntradaDTO.getCodigoHistorico());
		request.setCdPessoaJuridica(incluirVincMsgLancOperEntradaDTO.getPessoaJurContrato());
		request.setCdProdutoServicoOperacao(incluirVincMsgLancOperEntradaDTO.getTipoServico());
		request.setCdprodutoOperacaoRelacionado(incluirVincMsgLancOperEntradaDTO.getModalidadeServico());
		request.setCdRelacionamentoProdutoProduto(incluirVincMsgLancOperEntradaDTO.getTipoRelacionamento());
		request.setCdTipoContratoNegocio(incluirVincMsgLancOperEntradaDTO.getTipoContrato());
		request.setSequenciaContratoNegocio(incluirVincMsgLancOperEntradaDTO.getNumeroContrato());
		
			
		
		List<IncluirVincMsgLancOperEntradaOcorrenciasDTO> listaIncluir = incluirVincMsgLancOperEntradaDTO.getOcorrencias();		
		List<List<IncluirVincMsgLancOperEntradaOcorrenciasDTO>> listOcorrencias = new ArrayList<List<IncluirVincMsgLancOperEntradaOcorrenciasDTO>>();
		
		if (!CollectionUtils.isEmpty(listaIncluir)) {
			listOcorrencias = mapearOcorrencias(listaIncluir, IVincMsgLanctOpeContratadaServiceConstants.MAX_CONSULTAS); 
			
			for (List<IncluirVincMsgLancOperEntradaOcorrenciasDTO> ocorrs : listOcorrencias) {
				response = incluirAutorizacoes(ocorrs, request);
			}
		}else {
			br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsglancoper.request.Ocorrencias ocorr = null;			
			ocorr = new br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsglancoper.request.Ocorrencias();			
			ocorr.setCdTipoPagamentoCliente(0);
			
			request.addOcorrencias(ocorr);			
			request.setQtOcorrencia(0);
			
			response = getFactoryAdapter().getIncluirVincMsgLancOperPDCAdapter().invokeProcess(request);
		}			
		
		
		
		incluirVincMsgLancOperSaidaDTO.setCodMensagem(response.getCodMensagem());
		incluirVincMsgLancOperSaidaDTO.setMensagem(response.getMensagem());
	
		return incluirVincMsgLancOperSaidaDTO;
	}
	
	private IncluirVincMsgLancOperResponse incluirAutorizacoes(List<IncluirVincMsgLancOperEntradaOcorrenciasDTO> autorizacoes, IncluirVincMsgLancOperRequest request){		
		
		br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsglancoper.request.Ocorrencias ocorr = null;
		
		for (int i = 0; i < autorizacoes.size(); i++) {
			IncluirVincMsgLancOperEntradaOcorrenciasDTO autorizacao = autorizacoes.get(i);
			
			ocorr = new br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsglancoper.request.Ocorrencias();			
			ocorr.setCdTipoPagamentoCliente(autorizacao.getCdTipoPagamentoCliente());
			
			request.addOcorrencias(ocorr);			
		}
		
		request.setQtOcorrencia(autorizacoes.size());
		
		IncluirVincMsgLancOperResponse response = getFactoryAdapter().getIncluirVincMsgLancOperPDCAdapter().invokeProcess(request);
		
		return response;
	}
	
	private List<List<IncluirVincMsgLancOperEntradaOcorrenciasDTO>> mapearOcorrencias(List<IncluirVincMsgLancOperEntradaOcorrenciasDTO> ocorrencias, int numeroMaxOcorrencias) {
		
		List<List<IncluirVincMsgLancOperEntradaOcorrenciasDTO>> listOcorrencias = null;
		listOcorrencias = new ArrayList<List<IncluirVincMsgLancOperEntradaOcorrenciasDTO>>();

		int size = ocorrencias.size(); 
		int chamadas = size / 100;
		if ((size % 100) != 0) { 
			chamadas += 1; 
		}
		
		int indexInicial = 0;
		int indexFinal = 0;
		
		if (chamadas <= 1) { 
			indexFinal = size;
			listOcorrencias.add(ocorrencias.subList(indexInicial, indexFinal));
		}else {
			indexFinal = numeroMaxOcorrencias;
			
			for (int i = 1; i <= chamadas; i++) {
				listOcorrencias.add(ocorrencias.subList(indexInicial, indexFinal));				
				
				indexInicial += numeroMaxOcorrencias;
				if (i == chamadas - 1) {
					indexFinal += size - i * indexInicial;
				} else {
					indexFinal += numeroMaxOcorrencias;
				}
			}			
		}
		
		return listOcorrencias;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.IVincMsgLanctOpeContratadaService#detalharVincMsgLancOper(br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperEntradaDTO)
	 */
	public DetalharVincMsgLancOperSaidaDTO detalharVincMsgLancOper(DetalharVincMsgLancOperEntradaDTO entrada) {
		
		DetalharVincMsgLancOperSaidaDTO saida = new DetalharVincMsgLancOperSaidaDTO();
		
		DetalharVincMsgLancOperRequest request = new DetalharVincMsgLancOperRequest();
		DetalharVincMsgLancOperResponse response = new DetalharVincMsgLancOperResponse();
		
		request.setCdMensagemLinhaExtrato(PgitUtil.verificaIntegerNulo(entrada.getCdMensagemLinhaExtrato()));
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
		request.setCdprodutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdprodutoOperacaoRelacionado()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdRelacionamentoProdutoProduto(PgitUtil.verificaIntegerNulo(entrada.getCdRelacionamentoProdutoProduto()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setQtOcorrencia(0);		
	
		response = getFactoryAdapter().getDetalharVincMsgLancOperPDCAdapter().invokeProcess(request);
	
		saida = new DetalharVincMsgLancOperSaidaDTO();
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		
		saida.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
		saida.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setCdIdentificadorLancamentoCredito(response.getCdIdentificadorLancamentoCredito());
		saida.setCdIdentificadorLancamentoDebito(response.getCdIdentificadorLancamentoDebito());
		saida.setCdIndicadorRestContrato(response.getCdIndicadorRestContrato());
		saida.setCdMensagemLinhaExtrato(response.getCdMensagemLinhaExtrato());
		saida.setCdPessoaJuridicaContrato(response.getCdPessoaJuridicaContrato());
		saida.setCdProdutoOperacaoRelacionado(response.getCdProdutoOperacaoRelacionado());
		saida.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
		saida.setCdRelacionamentoProdutoProduto(response.getCdRelacionamentoProdutoProduto());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setCdTipoMensagemExtrato(response.getCdTipoMensagemExtrato());
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setDsIdentificadorLancamentoCredito(response.getDsIdentificadorLancamentoCredito());
		saida.setDsIdentificadorLancamentoDebito(response.getDsIdentificadorLancamentoDebito());
		saida.setDsProdutoOperacaoRelacionado(response.getDsProdutoOperacaoRelacionado());
		saida.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
		saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saida.setMensagem(response.getMensagem());
		saida.setNmOperadorFluxoInclusao(response.getNmOperadorFluxoInclusao());
		saida.setNmOperadorFluxoManutencao(response.getNmOperadorFluxoManutencao());
		saida.setNrOcorrencias(response.getNrOcorrencias());
		saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
		
		List<DetalharVincMsgLancOperOcorrenciasSaidaDTO> listaSaida = new ArrayList<DetalharVincMsgLancOperOcorrenciasSaidaDTO>();
		
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			DetalharVincMsgLancOperOcorrenciasSaidaDTO ocorrencia = new DetalharVincMsgLancOperOcorrenciasSaidaDTO();
			ocorrencia.setDsTipoPagamentoCliente(response.getOcorrencias(i).getDsTipoPagamentoCliente());
			listaSaida.add(ocorrencia);
		}
		
		saida.setOcorrencias(listaSaida);
		
		
		return saida;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.IVincMsgLanctOpeContratadaService#excluirVincMsgLancOper(br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ExcluirVincMsgLancOperEntradaDTO)
	 */
	public ExcluirVincMsgLancOperSaidaDTO excluirVincMsgLancOper(ExcluirVincMsgLancOperEntradaDTO excluirVincMsgLancOperEntradaDTO) {
		
		ExcluirVincMsgLancOperSaidaDTO excluirVincMsgLancOperSaidaDTO = new ExcluirVincMsgLancOperSaidaDTO();
		ExcluirVincMsgLancOperRequest excluirVincMsgLancOperRequest = new ExcluirVincMsgLancOperRequest();
		ExcluirVincMsgLancOperResponse excluirVincMsgLancOperResponse = new ExcluirVincMsgLancOperResponse();
	
		excluirVincMsgLancOperRequest.setCdMensagemLinhaExtrato(excluirVincMsgLancOperEntradaDTO.getCodigoHistorico());
		excluirVincMsgLancOperRequest.setCdPessoaJuridicaContrato(excluirVincMsgLancOperEntradaDTO.getPessoaJurContrato());
		excluirVincMsgLancOperRequest.setCdProdutoServicoOperacao(excluirVincMsgLancOperEntradaDTO.getTipoServico());
		excluirVincMsgLancOperRequest.setCdProdutoOperacaoRelacionado(excluirVincMsgLancOperEntradaDTO.getModalidadeServico());
		excluirVincMsgLancOperRequest.setCdRelacionamentoProduto(excluirVincMsgLancOperEntradaDTO.getTipoRelacionamento());
		excluirVincMsgLancOperRequest.setCdTipoContratoNegocio(excluirVincMsgLancOperEntradaDTO.getTipoContrato());
		excluirVincMsgLancOperRequest.setNrSequenciaContratoNegocio(excluirVincMsgLancOperEntradaDTO.getNumeroContrato());
		excluirVincMsgLancOperRequest.setQtOcorrencias(0);
	
		excluirVincMsgLancOperResponse = getFactoryAdapter().getExcluirVincMsgLancOperPDCAdapter().invokeProcess(excluirVincMsgLancOperRequest);
	
		excluirVincMsgLancOperSaidaDTO.setCodMensagem(excluirVincMsgLancOperResponse.getCodMensagem());
		excluirVincMsgLancOperSaidaDTO.setMensagem(excluirVincMsgLancOperResponse.getMensagem());
	
		return excluirVincMsgLancOperSaidaDTO;
	}

	public ListarTiposPagtoSaidaDTO listarTiposPagto() {
		
		ListarTiposPagtoSaidaDTO saida = new ListarTiposPagtoSaidaDTO();
		ListarTiposPagtoRequest request = new ListarTiposPagtoRequest();
		ListarTiposPagtoResponse response = new ListarTiposPagtoResponse();
		
		request.setMaxOcorrencias(PgitUtil.verificaIntegerNulo(IVincMsgLanctOpeContratadaServiceConstants.MAX_CONSULTAS));
		
		response = getFactoryAdapter().getListarTiposPagtoPDCAdapter().invokeProcess(request);
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getCodMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());
		
		List<ListarTiposPagtoSaidaOcorrenciasDTO> listaOcorrencias = new ArrayList<ListarTiposPagtoSaidaOcorrenciasDTO>();
		
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ListarTiposPagtoSaidaOcorrenciasDTO ocorrencia = new ListarTiposPagtoSaidaOcorrenciasDTO();
			
			ocorrencia.setCdTipoPagamentoCliente(response.getOcorrencias(i).getCdTipoPagamentoCliente());
			ocorrencia.setDsTipoPagamentoCliente(response.getOcorrencias(i).getDsTipoPagamentoCliente());
			
			listaOcorrencias.add(ocorrencia);
		}
		
		saida.setOcorrencias(listaOcorrencias);
		
		return saida;
	}

}

