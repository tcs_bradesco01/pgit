package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

public class ValidarVinculacaoConvenioContaSalarioSaidaDTO {

	 private String codMensagem;
     private String mensagem;
     private Integer cdIndicadorAlcadaAgencia;
     private Integer cdProdutoServicoOperacao;
     
	/**
	 * @return the codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	/**
	 * @param codMensagem the codMensagem to set
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	/**
	 * @return the cdIndicadorAlcadaAgencia
	 */
	public Integer getCdIndicadorAlcadaAgencia() {
		return cdIndicadorAlcadaAgencia;
	}
	/**
	 * @param cdIndicadorAlcadaAgencia the cdIndicadorAlcadaAgencia to set
	 */
	public void setCdIndicadorAlcadaAgencia(Integer cdIndicadorAlcadaAgencia) {
		this.cdIndicadorAlcadaAgencia = cdIndicadorAlcadaAgencia;
	}
	/**
	 * @return the cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	/**
	 * @param cdProdutoServicoOperacao the cdProdutoServicoOperacao to set
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
     
}
