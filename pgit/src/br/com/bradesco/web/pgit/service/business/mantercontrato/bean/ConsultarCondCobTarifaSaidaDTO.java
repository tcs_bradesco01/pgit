/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarCondCobTarifaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarCondCobTarifaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPeriodicidadeCobrancaTarifa. */
	private int cdPeriodicidadeCobrancaTarifa;
	
	/** Atributo dsPeriodicidadeCobrancaTarifa. */
	private String dsPeriodicidadeCobrancaTarifa;
	
	/** Atributo qtDiaCobrancaTarifa. */
	private int qtDiaCobrancaTarifa;
	
	/** Atributo nrFechamentoApuracaoTarifa. */
	private int nrFechamentoApuracaoTarifa;
	
	/** Atributo cdTipoReajusteTarifa. */
	private int cdTipoReajusteTarifa;
	
	/** Atributo cdPeriodicidadeReajusteTarifa. */
	private int cdPeriodicidadeReajusteTarifa;
	
	/** Atributo qtMesReajusteTarifa. */
	private int qtMesReajusteTarifa;
	
	/** Atributo cdIndicadorEconomicoReajuste. */
	private int cdIndicadorEconomicoReajuste;
	
	/** Atributo cdPercentualIndice. */
	private BigDecimal cdPercentualIndice;
	
	/** Atributo cdPercentualFlexibilizacao. */
	private BigDecimal cdPercentualFlexibilizacao;
	
	/**
	 * Get: cdIndicadorEconomicoReajuste.
	 *
	 * @return cdIndicadorEconomicoReajuste
	 */
	public int getCdIndicadorEconomicoReajuste() {
		return cdIndicadorEconomicoReajuste;
	}
	
	/**
	 * Set: cdIndicadorEconomicoReajuste.
	 *
	 * @param cdIndicadorEconomicoReajuste the cd indicador economico reajuste
	 */
	public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste) {
		this.cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
	}
	
	/**
	 * Get: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @return cdPeriodicidadeCobrancaTarifa
	 */
	public int getCdPeriodicidadeCobrancaTarifa() {
		return cdPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Set: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @param cdPeriodicidadeCobrancaTarifa the cd periodicidade cobranca tarifa
	 */
	public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa) {
		this.cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Get: cdPeriodicidadeReajusteTarifa.
	 *
	 * @return cdPeriodicidadeReajusteTarifa
	 */
	public int getCdPeriodicidadeReajusteTarifa() {
		return cdPeriodicidadeReajusteTarifa;
	}
	
	/**
	 * Set: cdPeriodicidadeReajusteTarifa.
	 *
	 * @param cdPeriodicidadeReajusteTarifa the cd periodicidade reajuste tarifa
	 */
	public void setCdPeriodicidadeReajusteTarifa(int cdPeriodicidadeReajusteTarifa) {
		this.cdPeriodicidadeReajusteTarifa = cdPeriodicidadeReajusteTarifa;
	}
	
	/**
	 * Get: cdTipoReajusteTarifa.
	 *
	 * @return cdTipoReajusteTarifa
	 */
	public int getCdTipoReajusteTarifa() {
		return cdTipoReajusteTarifa;
	}
	
	/**
	 * Set: cdTipoReajusteTarifa.
	 *
	 * @param cdTipoReajusteTarifa the cd tipo reajuste tarifa
	 */
	public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa) {
		this.cdTipoReajusteTarifa = cdTipoReajusteTarifa;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsPeriodicidadeCobrancaTarifa.
	 *
	 * @return dsPeriodicidadeCobrancaTarifa
	 */
	public String getDsPeriodicidadeCobrancaTarifa() {
		return dsPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Set: dsPeriodicidadeCobrancaTarifa.
	 *
	 * @param dsPeriodicidadeCobrancaTarifa the ds periodicidade cobranca tarifa
	 */
	public void setDsPeriodicidadeCobrancaTarifa(
			String dsPeriodicidadeCobrancaTarifa) {
		this.dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrFechamentoApuracaoTarifa.
	 *
	 * @return nrFechamentoApuracaoTarifa
	 */
	public int getNrFechamentoApuracaoTarifa() {
		return nrFechamentoApuracaoTarifa;
	}
	
	/**
	 * Set: nrFechamentoApuracaoTarifa.
	 *
	 * @param nrFechamentoApuracaoTarifa the nr fechamento apuracao tarifa
	 */
	public void setNrFechamentoApuracaoTarifa(int nrFechamentoApuracaoTarifa) {
		this.nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
	}
	
	/**
	 * Get: qtDiaCobrancaTarifa.
	 *
	 * @return qtDiaCobrancaTarifa
	 */
	public int getQtDiaCobrancaTarifa() {
		return qtDiaCobrancaTarifa;
	}
	
	/**
	 * Set: qtDiaCobrancaTarifa.
	 *
	 * @param qtDiaCobrancaTarifa the qt dia cobranca tarifa
	 */
	public void setQtDiaCobrancaTarifa(int qtDiaCobrancaTarifa) {
		this.qtDiaCobrancaTarifa = qtDiaCobrancaTarifa;
	}
	
	/**
	 * Get: qtMesReajusteTarifa.
	 *
	 * @return qtMesReajusteTarifa
	 */
	public int getQtMesReajusteTarifa() {
		return qtMesReajusteTarifa;
	}
	
	/**
	 * Set: qtMesReajusteTarifa.
	 *
	 * @param qtMesReajusteTarifa the qt mes reajuste tarifa
	 */
	public void setQtMesReajusteTarifa(int qtMesReajusteTarifa) {
		this.qtMesReajusteTarifa = qtMesReajusteTarifa;
	}
	
	/**
	 * Get: cdPercentualFlexibilizacao.
	 *
	 * @return cdPercentualFlexibilizacao
	 */
	public BigDecimal getCdPercentualFlexibilizacao() {
		return cdPercentualFlexibilizacao;
	}
	
	/**
	 * Set: cdPercentualFlexibilizacao.
	 *
	 * @param cdPercentualFlexibilizacao the cd percentual flexibilizacao
	 */
	public void setCdPercentualFlexibilizacao(BigDecimal cdPercentualFlexibilizacao) {
		this.cdPercentualFlexibilizacao = cdPercentualFlexibilizacao;
	}
	
	/**
	 * Get: cdPercentualIndice.
	 *
	 * @return cdPercentualIndice
	 */
	public BigDecimal getCdPercentualIndice() {
		return cdPercentualIndice;
	}
	
	/**
	 * Set: cdPercentualIndice.
	 *
	 * @param cdPercentualIndice the cd percentual indice
	 */
	public void setCdPercentualIndice(BigDecimal cdPercentualIndice) {
		this.cdPercentualIndice = cdPercentualIndice;
	}

	
	
	
	

}
