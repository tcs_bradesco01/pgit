/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasAvisoFormularios
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasAvisoFormularios {
	
	/** Atributo dsRecTipoModalidade. */
	private String dsRecTipoModalidade;
    
    /** Atributo dsRecMomentoEnvio. */
    private String dsRecMomentoEnvio;
    
    /** Atributo dsRecDestino. */
    private String dsRecDestino;
    
    /** Atributo dsRecCadastroEnderecoUtilizado. */
    private String dsRecCadastroEnderecoUtilizado;
    
    /** Atributo qtRecDiaAvisoAntecipado. */
    private Integer qtRecDiaAvisoAntecipado;
    
    /** Atributo dsRecPermissaoAgrupamento. */
    private String dsRecPermissaoAgrupamento;
    
	/**
	 * Get: dsRecTipoModalidade.
	 *
	 * @return dsRecTipoModalidade
	 */
	public String getDsRecTipoModalidade() {
		return dsRecTipoModalidade;
	}
	
	/**
	 * Set: dsRecTipoModalidade.
	 *
	 * @param dsRecTipoModalidade the ds rec tipo modalidade
	 */
	public void setDsRecTipoModalidade(String dsRecTipoModalidade) {
		this.dsRecTipoModalidade = dsRecTipoModalidade;
	}
	
	/**
	 * Get: dsRecMomentoEnvio.
	 *
	 * @return dsRecMomentoEnvio
	 */
	public String getDsRecMomentoEnvio() {
		return dsRecMomentoEnvio;
	}
	
	/**
	 * Set: dsRecMomentoEnvio.
	 *
	 * @param dsRecMomentoEnvio the ds rec momento envio
	 */
	public void setDsRecMomentoEnvio(String dsRecMomentoEnvio) {
		this.dsRecMomentoEnvio = dsRecMomentoEnvio;
	}
	
	/**
	 * Get: dsRecDestino.
	 *
	 * @return dsRecDestino
	 */
	public String getDsRecDestino() {
		return dsRecDestino;
	}
	
	/**
	 * Set: dsRecDestino.
	 *
	 * @param dsRecDestino the ds rec destino
	 */
	public void setDsRecDestino(String dsRecDestino) {
		this.dsRecDestino = dsRecDestino;
	}
	
	/**
	 * Get: dsRecCadastroEnderecoUtilizado.
	 *
	 * @return dsRecCadastroEnderecoUtilizado
	 */
	public String getDsRecCadastroEnderecoUtilizado() {
		return dsRecCadastroEnderecoUtilizado;
	}
	
	/**
	 * Set: dsRecCadastroEnderecoUtilizado.
	 *
	 * @param dsRecCadastroEnderecoUtilizado the ds rec cadastro endereco utilizado
	 */
	public void setDsRecCadastroEnderecoUtilizado(
			String dsRecCadastroEnderecoUtilizado) {
		this.dsRecCadastroEnderecoUtilizado = dsRecCadastroEnderecoUtilizado;
	}
	
	/**
	 * Get: qtRecDiaAvisoAntecipado.
	 *
	 * @return qtRecDiaAvisoAntecipado
	 */
	public Integer getQtRecDiaAvisoAntecipado() {
		return qtRecDiaAvisoAntecipado;
	}
	
	/**
	 * Set: qtRecDiaAvisoAntecipado.
	 *
	 * @param qtRecDiaAvisoAntecipado the qt rec dia aviso antecipado
	 */
	public void setQtRecDiaAvisoAntecipado(Integer qtRecDiaAvisoAntecipado) {
		this.qtRecDiaAvisoAntecipado = qtRecDiaAvisoAntecipado;
	}
	
	/**
	 * Get: dsRecPermissaoAgrupamento.
	 *
	 * @return dsRecPermissaoAgrupamento
	 */
	public String getDsRecPermissaoAgrupamento() {
		return dsRecPermissaoAgrupamento;
	}
	
	/**
	 * Set: dsRecPermissaoAgrupamento.
	 *
	 * @param dsRecPermissaoAgrupamento the ds rec permissao agrupamento
	 */
	public void setDsRecPermissaoAgrupamento(String dsRecPermissaoAgrupamento) {
		this.dsRecPermissaoAgrupamento = dsRecPermissaoAgrupamento;
	}
    
}
