/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoremessa.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoremessa.bean;

/**
 * Nome: ConsultarSolicRecuperacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicRecuperacaoEntradaDTO {
	
	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;        
	
	/** Atributo dtInicioSolicitacao. */
	private String dtInicioSolicitacao;    
	
	/** Atributo dtFinalSolicitacao. */
	private String dtFinalSolicitacao;    
	
	/** Atributo cdUsuarioSolicitacao. */
	private String cdUsuarioSolicitacao;    
	
	/** Atributo cdSituacaoSolicitacaoRecuperacao. */
	private Integer cdSituacaoSolicitacaoRecuperacao;
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoSolicitacaoRecuperacao.
	 *
	 * @return cdSituacaoSolicitacaoRecuperacao
	 */
	public Integer getCdSituacaoSolicitacaoRecuperacao() {
		return cdSituacaoSolicitacaoRecuperacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacaoRecuperacao.
	 *
	 * @param cdSituacaoSolicitacaoRecuperacao the cd situacao solicitacao recuperacao
	 */
	public void setCdSituacaoSolicitacaoRecuperacao(
			Integer cdSituacaoSolicitacaoRecuperacao) {
		this.cdSituacaoSolicitacaoRecuperacao = cdSituacaoSolicitacaoRecuperacao;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdUsuarioSolicitacao.
	 *
	 * @return cdUsuarioSolicitacao
	 */
	public String getCdUsuarioSolicitacao() {
		return cdUsuarioSolicitacao;
	}
	
	/**
	 * Set: cdUsuarioSolicitacao.
	 *
	 * @param cdUsuarioSolicitacao the cd usuario solicitacao
	 */
	public void setCdUsuarioSolicitacao(String cdUsuarioSolicitacao) {
		this.cdUsuarioSolicitacao = cdUsuarioSolicitacao;
	}
	
	/**
	 * Get: dtFinalSolicitacao.
	 *
	 * @return dtFinalSolicitacao
	 */
	public String getDtFinalSolicitacao() {
		return dtFinalSolicitacao;
	}
	
	/**
	 * Set: dtFinalSolicitacao.
	 *
	 * @param dtFinalSolicitacao the dt final solicitacao
	 */
	public void setDtFinalSolicitacao(String dtFinalSolicitacao) {
		this.dtFinalSolicitacao = dtFinalSolicitacao;
	}
	
	/**
	 * Get: dtInicioSolicitacao.
	 *
	 * @return dtInicioSolicitacao
	 */
	public String getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}
	
	/**
	 * Set: dtInicioSolicitacao.
	 *
	 * @param dtInicioSolicitacao the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(String dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	

}
