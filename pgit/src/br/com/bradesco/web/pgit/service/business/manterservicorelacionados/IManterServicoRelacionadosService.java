/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterservicorelacionados;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.AlterarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.AlterarServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.DetalharServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.DetalharServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ExcluirServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ExcluirServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.IncluirServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.IncluirServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoSaidaDTO;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterServicoRelacionados
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterServicoRelacionadosService {

	/**
	 * Listar servico relacionado dto.
	 *
	 * @param listarServicoRelacionadoEntradaDTO the listar servico relacionado entrada dto
	 * @return the list< listar servico relacionado saida dt o>
	 */
	List<ListarServicoRelacionadoSaidaDTO> listarServicoRelacionadoDTO(ListarServicoRelacionadoEntradaDTO listarServicoRelacionadoEntradaDTO);
	
	/**
	 * Incluir servico relacionado dto.
	 *
	 * @param incluirServicoRelacionadoEntradaDTO the incluir servico relacionado entrada dto
	 * @return the incluir servico relacionado saida dto
	 */
	IncluirServicoRelacionadoSaidaDTO incluirServicoRelacionadoDTO(IncluirServicoRelacionadoEntradaDTO incluirServicoRelacionadoEntradaDTO);
	
	/**
	 * Excluir servico relacionado dto.
	 *
	 * @param excluirServicoRelacionadoEntradaDTO the excluir servico relacionado entrada dto
	 * @return the excluir servico relacionado saida dto
	 */
	ExcluirServicoRelacionadoSaidaDTO  excluirServicoRelacionadoDTO(ExcluirServicoRelacionadoEntradaDTO excluirServicoRelacionadoEntradaDTO);
	
	/**
	 * Detalhar servico relacionado dto.
	 *
	 * @param detalharServicoRelacionadoEntradaDTO the detalhar servico relacionado entrada dto
	 * @return the detalhar servico relacionado saida dto
	 */
	DetalharServicoRelacionadoSaidaDTO  detalharServicoRelacionadoDTO(DetalharServicoRelacionadoEntradaDTO detalharServicoRelacionadoEntradaDTO);
	
	AlterarServicoRelacionadoSaidaDTO alterarServicoRelacionado(AlterarServicoRelacionadoEntradaDTO entrada);
	

	

}

