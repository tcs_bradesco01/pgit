/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean;

/**
 * Nome: IncluirContrEnvioArqFormaLiquidacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirContrEnvioArqFormaLiquidacaoEntradaDTO{
	
	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo nrEnvioFormaLiquidacaoDoc. */
	private Integer nrEnvioFormaLiquidacaoDoc;
	
	/** Atributo hrEnvioFormaLiquidacaoDoc. */
	private String hrEnvioFormaLiquidacaoDoc;

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao(){
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao){
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: nrEnvioFormaLiquidacaoDoc.
	 *
	 * @return nrEnvioFormaLiquidacaoDoc
	 */
	public Integer getNrEnvioFormaLiquidacaoDoc(){
		return nrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Set: nrEnvioFormaLiquidacaoDoc.
	 *
	 * @param nrEnvioFormaLiquidacaoDoc the nr envio forma liquidacao doc
	 */
	public void setNrEnvioFormaLiquidacaoDoc(Integer nrEnvioFormaLiquidacaoDoc){
		this.nrEnvioFormaLiquidacaoDoc = nrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Get: hrEnvioFormaLiquidacaoDoc.
	 *
	 * @return hrEnvioFormaLiquidacaoDoc
	 */
	public String getHrEnvioFormaLiquidacaoDoc(){
		return hrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Set: hrEnvioFormaLiquidacaoDoc.
	 *
	 * @param hrEnvioFormaLiquidacaoDoc the hr envio forma liquidacao doc
	 */
	public void setHrEnvioFormaLiquidacaoDoc(String hrEnvioFormaLiquidacaoDoc){
		this.hrEnvioFormaLiquidacaoDoc = hrEnvioFormaLiquidacaoDoc;
	}
}