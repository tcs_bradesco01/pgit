/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean;

import java.math.BigDecimal;

/**
 * Nome: IncluirSolEmiAviMovtoCliePagadorEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSolEmiAviMovtoCliePagadorEntradaDTO {
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dtInicioPerMovimentacao. */
    private String dtInicioPerMovimentacao;
    
    /** Atributo dsFimPerMovimentacao. */
    private String dsFimPerMovimentacao;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo cdRecebedorCredito. */
    private Long cdRecebedorCredito;
    
    /** Atributo cdTipoInscricaoRecebedor. */
    private Integer cdTipoInscricaoRecebedor;
    
    /** Atributo cdCpfCnpjRecebedor. */
    private Long cdCpfCnpjRecebedor;
    
    /** Atributo cdFilialCnpjRecebedor. */
    private Integer cdFilialCnpjRecebedor;
    
    /** Atributo cdControleCpfRecebedor. */
    private Integer cdControleCpfRecebedor;
    
    /** Atributo cdDestinoCorrespondenciaSolic. */
    private Integer cdDestinoCorrespondenciaSolic;
    
    /** Atributo cdTipoPostagemSolicitacao. */
    private String cdTipoPostagemSolicitacao;
    
    /** Atributo dsLogradouroPagador. */
    private String dsLogradouroPagador;
    
    /** Atributo dsNumeroLogradouroPagador. */
    private String dsNumeroLogradouroPagador;
    
    /** Atributo dsComplementoLogradouroPagador. */
    private String dsComplementoLogradouroPagador;
    
    /** Atributo dsBairroClientePagador. */
    private String dsBairroClientePagador;
    
    /** Atributo dsMunicipioClientePagador. */
    private String dsMunicipioClientePagador;
    
    /** Atributo cdSiglaUfPagador. */
    private String cdSiglaUfPagador;
    
    /** Atributo cdCepPagador. */
    private Integer cdCepPagador;
    
    /** Atributo cdCepComplementoPagador. */
    private Integer cdCepComplementoPagador;
    
    /** Atributo dsEmailClientePagador. */
    private String dsEmailClientePagador;
    
    /** Atributo cdPessoaJuridicaDepto. */
    private Long cdPessoaJuridicaDepto;
    
    /** Atributo nrSequenciaUnidadeDepto. */
    private Integer nrSequenciaUnidadeDepto;
    
    /** Atributo vrTarifaPadraoSolic. */
    private BigDecimal vrTarifaPadraoSolic;
    
    /** Atributo cdPercentualTarifaSolic. */
    private BigDecimal cdPercentualTarifaSolic;
    
    /** Atributo vlTarifaNegocioSolic. */
    private BigDecimal vlTarifaNegocioSolic;
    
    /** Atributo cdDepartamentoUnidade. */
    private Integer cdDepartamentoUnidade;   
    
	/**
	 * Get: cdDepartamentoUnidade.
	 *
	 * @return cdDepartamentoUnidade
	 */
	public Integer getCdDepartamentoUnidade() {
		return cdDepartamentoUnidade;
	}
	
	/**
	 * Set: cdDepartamentoUnidade.
	 *
	 * @param cdDepartamentoUnidade the cd departamento unidade
	 */
	public void setCdDepartamentoUnidade(Integer cdDepartamentoUnidade) {
		this.cdDepartamentoUnidade = cdDepartamentoUnidade;
	}
	
	/**
	 * Get: cdCepComplementoPagador.
	 *
	 * @return cdCepComplementoPagador
	 */
	public Integer getCdCepComplementoPagador() {
		return cdCepComplementoPagador;
	}
	
	/**
	 * Set: cdCepComplementoPagador.
	 *
	 * @param cdCepComplementoPagador the cd cep complemento pagador
	 */
	public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
		this.cdCepComplementoPagador = cdCepComplementoPagador;
	}
	
	/**
	 * Get: cdCepPagador.
	 *
	 * @return cdCepPagador
	 */
	public Integer getCdCepPagador() {
		return cdCepPagador;
	}
	
	/**
	 * Set: cdCepPagador.
	 *
	 * @param cdCepPagador the cd cep pagador
	 */
	public void setCdCepPagador(Integer cdCepPagador) {
		this.cdCepPagador = cdCepPagador;
	}
	
	/**
	 * Get: cdControleCpfRecebedor.
	 *
	 * @return cdControleCpfRecebedor
	 */
	public Integer getCdControleCpfRecebedor() {
		return cdControleCpfRecebedor;
	}
	
	/**
	 * Set: cdControleCpfRecebedor.
	 *
	 * @param cdControleCpfRecebedor the cd controle cpf recebedor
	 */
	public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
		this.cdControleCpfRecebedor = cdControleCpfRecebedor;
	}
	
	/**
	 * Get: cdCpfCnpjRecebedor.
	 *
	 * @return cdCpfCnpjRecebedor
	 */
	public Long getCdCpfCnpjRecebedor() {
		return cdCpfCnpjRecebedor;
	}
	
	/**
	 * Set: cdCpfCnpjRecebedor.
	 *
	 * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
	 */
	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}
	
	/**
	 * Get: cdDestinoCorrespondenciaSolic.
	 *
	 * @return cdDestinoCorrespondenciaSolic
	 */
	public Integer getCdDestinoCorrespondenciaSolic() {
		return cdDestinoCorrespondenciaSolic;
	}
	
	/**
	 * Set: cdDestinoCorrespondenciaSolic.
	 *
	 * @param cdDestinoCorrespondenciaSolic the cd destino correspondencia solic
	 */
	public void setCdDestinoCorrespondenciaSolic(
			Integer cdDestinoCorrespondenciaSolic) {
		this.cdDestinoCorrespondenciaSolic = cdDestinoCorrespondenciaSolic;
	}
	
	/**
	 * Get: cdFilialCnpjRecebedor.
	 *
	 * @return cdFilialCnpjRecebedor
	 */
	public Integer getCdFilialCnpjRecebedor() {
		return cdFilialCnpjRecebedor;
	}
	
	/**
	 * Set: cdFilialCnpjRecebedor.
	 *
	 * @param cdFilialCnpjRecebedor the cd filial cnpj recebedor
	 */
	public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
		this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
	}
	
	/**
	 * Get: cdPercentualTarifaSolic.
	 *
	 * @return cdPercentualTarifaSolic
	 */
	public BigDecimal getCdPercentualTarifaSolic() {
		return cdPercentualTarifaSolic;
	}
	
	/**
	 * Set: cdPercentualTarifaSolic.
	 *
	 * @param cdPercentualTarifaSolic the cd percentual tarifa solic
	 */
	public void setCdPercentualTarifaSolic(BigDecimal cdPercentualTarifaSolic) {
		this.cdPercentualTarifaSolic = cdPercentualTarifaSolic;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaDepto.
	 *
	 * @return cdPessoaJuridicaDepto
	 */
	public Long getCdPessoaJuridicaDepto() {
		return cdPessoaJuridicaDepto;
	}
	
	/**
	 * Set: cdPessoaJuridicaDepto.
	 *
	 * @param cdPessoaJuridicaDepto the cd pessoa juridica depto
	 */
	public void setCdPessoaJuridicaDepto(Long cdPessoaJuridicaDepto) {
		this.cdPessoaJuridicaDepto = cdPessoaJuridicaDepto;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRecebedorCredito.
	 *
	 * @return cdRecebedorCredito
	 */
	public Long getCdRecebedorCredito() {
		return cdRecebedorCredito;
	}
	
	/**
	 * Set: cdRecebedorCredito.
	 *
	 * @param cdRecebedorCredito the cd recebedor credito
	 */
	public void setCdRecebedorCredito(Long cdRecebedorCredito) {
		this.cdRecebedorCredito = cdRecebedorCredito;
	}
	
	/**
	 * Get: cdSiglaUfPagador.
	 *
	 * @return cdSiglaUfPagador
	 */
	public String getCdSiglaUfPagador() {
		return cdSiglaUfPagador;
	}
	
	/**
	 * Set: cdSiglaUfPagador.
	 *
	 * @param cdSiglaUfPagador the cd sigla uf pagador
	 */
	public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
		this.cdSiglaUfPagador = cdSiglaUfPagador;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoInscricaoRecebedor.
	 *
	 * @return cdTipoInscricaoRecebedor
	 */
	public Integer getCdTipoInscricaoRecebedor() {
		return cdTipoInscricaoRecebedor;
	}
	
	/**
	 * Set: cdTipoInscricaoRecebedor.
	 *
	 * @param cdTipoInscricaoRecebedor the cd tipo inscricao recebedor
	 */
	public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
		this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
	}
	
	/**
	 * Get: cdTipoPostagemSolicitacao.
	 *
	 * @return cdTipoPostagemSolicitacao
	 */
	public String getCdTipoPostagemSolicitacao() {
		return cdTipoPostagemSolicitacao;
	}
	
	/**
	 * Set: cdTipoPostagemSolicitacao.
	 *
	 * @param cdTipoPostagemSolicitacao the cd tipo postagem solicitacao
	 */
	public void setCdTipoPostagemSolicitacao(String cdTipoPostagemSolicitacao) {
		this.cdTipoPostagemSolicitacao = cdTipoPostagemSolicitacao;
	}
	
	/**
	 * Get: dsBairroClientePagador.
	 *
	 * @return dsBairroClientePagador
	 */
	public String getDsBairroClientePagador() {
		return dsBairroClientePagador;
	}
	
	/**
	 * Set: dsBairroClientePagador.
	 *
	 * @param dsBairroClientePagador the ds bairro cliente pagador
	 */
	public void setDsBairroClientePagador(String dsBairroClientePagador) {
		this.dsBairroClientePagador = dsBairroClientePagador;
	}
	
	/**
	 * Get: dsComplementoLogradouroPagador.
	 *
	 * @return dsComplementoLogradouroPagador
	 */
	public String getDsComplementoLogradouroPagador() {
		return dsComplementoLogradouroPagador;
	}
	
	/**
	 * Set: dsComplementoLogradouroPagador.
	 *
	 * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
	 */
	public void setDsComplementoLogradouroPagador(
			String dsComplementoLogradouroPagador) {
		this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
	}
	
	/**
	 * Get: dsEmailClientePagador.
	 *
	 * @return dsEmailClientePagador
	 */
	public String getDsEmailClientePagador() {
		return dsEmailClientePagador;
	}
	
	/**
	 * Set: dsEmailClientePagador.
	 *
	 * @param dsEmailClientePagador the ds email cliente pagador
	 */
	public void setDsEmailClientePagador(String dsEmailClientePagador) {
		this.dsEmailClientePagador = dsEmailClientePagador;
	}
	
	/**
	 * Get: dsFimPerMovimentacao.
	 *
	 * @return dsFimPerMovimentacao
	 */
	public String getDsFimPerMovimentacao() {
		return dsFimPerMovimentacao;
	}
	
	/**
	 * Set: dsFimPerMovimentacao.
	 *
	 * @param dsFimPerMovimentacao the ds fim per movimentacao
	 */
	public void setDsFimPerMovimentacao(String dsFimPerMovimentacao) {
		this.dsFimPerMovimentacao = dsFimPerMovimentacao;
	}
	
	/**
	 * Get: dsLogradouroPagador.
	 *
	 * @return dsLogradouroPagador
	 */
	public String getDsLogradouroPagador() {
		return dsLogradouroPagador;
	}
	
	/**
	 * Set: dsLogradouroPagador.
	 *
	 * @param dsLogradouroPagador the ds logradouro pagador
	 */
	public void setDsLogradouroPagador(String dsLogradouroPagador) {
		this.dsLogradouroPagador = dsLogradouroPagador;
	}
	
	/**
	 * Get: dsMunicipioClientePagador.
	 *
	 * @return dsMunicipioClientePagador
	 */
	public String getDsMunicipioClientePagador() {
		return dsMunicipioClientePagador;
	}
	
	/**
	 * Set: dsMunicipioClientePagador.
	 *
	 * @param dsMunicipioClientePagador the ds municipio cliente pagador
	 */
	public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
		this.dsMunicipioClientePagador = dsMunicipioClientePagador;
	}
	
	/**
	 * Get: dsNumeroLogradouroPagador.
	 *
	 * @return dsNumeroLogradouroPagador
	 */
	public String getDsNumeroLogradouroPagador() {
		return dsNumeroLogradouroPagador;
	}
	
	/**
	 * Set: dsNumeroLogradouroPagador.
	 *
	 * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
	 */
	public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
		this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
	}
	
	/**
	 * Get: dtInicioPerMovimentacao.
	 *
	 * @return dtInicioPerMovimentacao
	 */
	public String getDtInicioPerMovimentacao() {
		return dtInicioPerMovimentacao;
	}
	
	/**
	 * Set: dtInicioPerMovimentacao.
	 *
	 * @param dtInicioPerMovimentacao the dt inicio per movimentacao
	 */
	public void setDtInicioPerMovimentacao(String dtInicioPerMovimentacao) {
		this.dtInicioPerMovimentacao = dtInicioPerMovimentacao;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaUnidadeDepto.
	 *
	 * @return nrSequenciaUnidadeDepto
	 */
	public Integer getNrSequenciaUnidadeDepto() {
		return nrSequenciaUnidadeDepto;
	}
	
	/**
	 * Set: nrSequenciaUnidadeDepto.
	 *
	 * @param nrSequenciaUnidadeDepto the nr sequencia unidade depto
	 */
	public void setNrSequenciaUnidadeDepto(Integer nrSequenciaUnidadeDepto) {
		this.nrSequenciaUnidadeDepto = nrSequenciaUnidadeDepto;
	}
	
	/**
	 * Get: vlTarifaNegocioSolic.
	 *
	 * @return vlTarifaNegocioSolic
	 */
	public BigDecimal getVlTarifaNegocioSolic() {
		return vlTarifaNegocioSolic;
	}
	
	/**
	 * Set: vlTarifaNegocioSolic.
	 *
	 * @param vlTarifaNegocioSolic the vl tarifa negocio solic
	 */
	public void setVlTarifaNegocioSolic(BigDecimal vlTarifaNegocioSolic) {
		this.vlTarifaNegocioSolic = vlTarifaNegocioSolic;
	}
	
	/**
	 * Get: vrTarifaPadraoSolic.
	 *
	 * @return vrTarifaPadraoSolic
	 */
	public BigDecimal getVrTarifaPadraoSolic() {
		return vrTarifaPadraoSolic;
	}
	
	/**
	 * Set: vrTarifaPadraoSolic.
	 *
	 * @param vrTarifaPadraoSolic the vr tarifa padrao solic
	 */
	public void setVrTarifaPadraoSolic(BigDecimal vrTarifaPadraoSolic) {
		this.vrTarifaPadraoSolic = vrTarifaPadraoSolic;
	}
    
    
}
