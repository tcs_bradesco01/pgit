/*
 * Nome: br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean;

/**
 * Nome: OcorrenciaBloquearDesbloquearProdServContDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciaBloquearDesbloquearProdServContDTO {
	
	/** Atributo cdServicoRelacionado. */
	private Integer cdServicoRelacionado;

	/**
	 * Get: cdServicoRelacionado.
	 *
	 * @return cdServicoRelacionado
	 */
	public Integer getCdServicoRelacionado() {
		return cdServicoRelacionado;
	}

	/**
	 * Set: cdServicoRelacionado.
	 *
	 * @param cdServicoRelacionado the cd servico relacionado
	 */
	public void setCdServicoRelacionado(Integer cdServicoRelacionado) {
		this.cdServicoRelacionado = cdServicoRelacionado;
	}
	
	

}
