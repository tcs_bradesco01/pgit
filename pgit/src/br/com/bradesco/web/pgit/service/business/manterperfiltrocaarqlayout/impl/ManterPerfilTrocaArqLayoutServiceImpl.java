/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutServiceConstants;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.AlterarPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.AlterarPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarHistoricoPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarHistoricoPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharHistoricoPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharHistoricoPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ExcluirPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ExcluirPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.IncluirPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.IncluirPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ListaContratosVinculadosPerfilEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ListaContratosVinculadosPerfilOcorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.request.AlterarPerfilTrocaArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.response.AlterarPerfilTrocaArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistoricoperfiltrocaarquivo.request.ConsultarHistoricoPerfilTrocaArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistoricoperfiltrocaarquivo.response.ConsultarHistoricoPerfilTrocaArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivo.request.ConsultarListaPerfilTrocaArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivo.response.ConsultarListaPerfilTrocaArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.request.DetalharHistoricoPerfilTrocaArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.response.DetalharHistoricoPerfilTrocaArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharperfiltrocaarquivo.request.DetalharPerfilTrocaArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharperfiltrocaarquivo.response.DetalharPerfilTrocaArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirperfiltrocaarquivo.request.ExcluirPerfilTrocaArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirperfiltrocaarquivo.response.ExcluirPerfilTrocaArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.IncluirPerfilTrocaArquivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.response.IncluirPerfilTrocaArquivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listacontratosvinculadosperfil.request.ListaContratosVinculadosPerfilRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listacontratosvinculadosperfil.response.ListaContratosVinculadosPerfilResponse;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterPerfilTrocaArqLayout
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterPerfilTrocaArqLayoutServiceImpl implements IManterPerfilTrocaArqLayoutService {
    
    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService#consultarListaPerfilTrocaArquivo(br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoEntradaDTO)
     */
    public ConsultarListaPerfilTrocaArquivoSaidaDTO consultarListaPerfilTrocaArquivo(ConsultarListaPerfilTrocaArquivoEntradaDTO entrada) {
	List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> listaSaida = new ArrayList<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO>();
	ConsultarListaPerfilTrocaArquivoRequest request = new ConsultarListaPerfilTrocaArquivoRequest();
	ConsultarListaPerfilTrocaArquivoResponse response = new ConsultarListaPerfilTrocaArquivoResponse();
	ConsultarListaPerfilTrocaArquivoSaidaDTO saida = new ConsultarListaPerfilTrocaArquivoSaidaDTO();

	request.setMaxOcorrencias(IManterPerfilTrocaArqLayoutServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
	request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	
	
	response = getFactoryAdapter().getConsultarListaPerfilTrocaArquivoPDCAdapter().invokeProcess(request);
	
	saida.setCodMensagem(response.getCodMensagem());
    saida.setMensagem(response.getMensagem());
    saida.setNumeroLinhas(response.getNumeroLinhas());
	
	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
		ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO ocorrencia = new ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO();

		ocorrencia.setCdAplicacaoTransmicaoPagamento(response.getOcorrencias(i).getCdAplicacaoTransmicaoPagamento());
		ocorrencia.setCdmeioAlternativoRemessa(response.getOcorrencias(i).getCdmeioAlternativoRemessa());
		ocorrencia.setCdMeioAlternativoRetorno(response.getOcorrencias(i).getCdMeioAlternativoRetorno());
		ocorrencia.setCdMeioPrincipalRemessa(response.getOcorrencias(i).getCdMeioPrincipalRemessa());
		ocorrencia.setCdMeioPrincipalRetorno(response.getOcorrencias(i).getCdMeioPrincipalRetorno());
		ocorrencia.setCdPerfilTrocaArquivo(response.getOcorrencias(i).getCdPerfilTrocaArquivo());
		ocorrencia.setCdPessoaJuridicaParceiro(response.getOcorrencias(i).getCdPessoaJuridicaParceiro());
		ocorrencia.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
		ocorrencia.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
		ocorrencia.setCdRelacionamentoProduto(response.getOcorrencias(i).getCdRelacionamentoProduto());
		ocorrencia.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
		ocorrencia.setDsAplicacaoTransmicaoPagamento(response.getOcorrencias(i).getDsAplicacaoTransmicaoPagamento());
		ocorrencia.setDsLayoutProprio(response.getOcorrencias(i).getDsLayoutProprio());
		ocorrencia.setDsMeioAltrnRemss(response.getOcorrencias(i).getDsMeioAltrnRemss());
		ocorrencia.setDsMeioAltrnRetor(response.getOcorrencias(i).getDsMeioAltrnRetor());
		ocorrencia.setDsMeioPrincRemss(response.getOcorrencias(i).getDsMeioPrincRemss());
		ocorrencia.setDsMeioPrincRetor(response.getOcorrencias(i).getDsMeioPrincRetor());
		ocorrencia.setDsProdutoServico(response.getOcorrencias(i).getDsProdutoServico());
		ocorrencia.setDsRzScialPcero(response.getOcorrencias(i).getDsRzScialPcero());
		ocorrencia.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());

		listaSaida.add(ocorrencia);
	}
	
	
	saida.setOcorrencia(listaSaida);

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService#detalharPerfilTrocaArquivo(br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoEntradaDTO)
     */
    public DetalharPerfilTrocaArquivoSaidaDTO detalharPerfilTrocaArquivo(DetalharPerfilTrocaArquivoEntradaDTO entrada) {
		DetalharPerfilTrocaArquivoSaidaDTO saida = new DetalharPerfilTrocaArquivoSaidaDTO();
		DetalharPerfilTrocaArquivoRequest request = new DetalharPerfilTrocaArquivoRequest();
		DetalharPerfilTrocaArquivoResponse response = new DetalharPerfilTrocaArquivoResponse();
	
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
		request.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(entrada.getCdRelacionamentoProduto()));
	
		response = getFactoryAdapter().getDetalharPerfilTrocaArquivoPDCAdapter().invokeProcess(request);
	
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdPerfilTrocaArq(response.getCdPerfilTrocaArq());
		saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saida.setDsCodTipoLayout(response.getDsCodTipoLayout());
		saida.setDsAplicFormat(response.getDsAplicFormat());
		saida.setCdSistemaOrigemArquivo(response.getCdSistemaOrigemArquivo());
		saida.setDsSistemaOrigemArquivo(response.getDsSistemaOrigemArquivo());
		saida.setDsEmpresa(response.getDsEmpresa());
		saida.setDsResponsavelCustoEmpresa(response.getDsResponsavelCustoEmpresa());

		if (response.getPcCustoOrganizacaoTransmissao() != null && response.getPcCustoOrganizacaoTransmissao().compareTo(BigDecimal.ZERO) == 0) {
			saida.setPcCustoOrganizacaoTransmissao(null);
		} else {
			saida.setPcCustoOrganizacaoTransmissao(response.getPcCustoOrganizacaoTransmissao());
		}

		saida.setDsNomeCliente(response.getDsNomeCliente());
		saida.setDsNomeArquivoRemessa(response.getDsNomeArquivoRemessa());
		saida.setDsNomeArquivoRetorno(response.getCdNomeArquivoRemessa());
		saida.setCdRejeicaoAcltoRemessa(response.getCdRejeicaoAcltoRemessa());
		saida.setDsRejeicaoAcltoRemessa(response.getDsRejeicaoAcltoRemessa());
		saida.setQtMaxIncotRemessa(response.getQtMaxIncotRemessa());

		if (response.getCdIncotRejeicaoRemessa() != null && response.getCdIncotRejeicaoRemessa().compareTo(BigDecimal.ZERO) == 0) {
			saida.setPercentualIncotRejeiRemessa(null);
		} else {
			saida.setPercentualIncotRejeiRemessa(response.getCdIncotRejeicaoRemessa());
		}

		saida.setCdNivelControleRemessa(response.getCdNivelControleRemessa());
		saida.setDsNivelControleRemessa(response.getDsNivelControleRemessa());
		saida.setCdControleNumeroRemessa(response.getCdControleNumeroRemessa());
		saida.setDsControleNumeroRemessa(response.getDsControleNumeroRemessa());
		saida.setCdPeriodicidadeContagemRemessa(response.getCdPeriodicidadeContagemRemessa());
		saida.setDsPeriodicidadeContagemRemessa(response.getDsPeriodicidadeContagemRemessa());
		saida.setNrMaxContagemRemessa(Long.valueOf(0).equals(response.getNrMaxContagemRemessa()) ? null : response.getNrMaxContagemRemessa());
		saida.setCdMeioPrincipalRemessa(response.getCdMeioPrincipalRemessa());
		saida.setDsCodMeioPrincipalRemessa(response.getDsCodMeioPrincipalRemessa());
		saida.setCdMeioAlternRemessa(response.getCdMeioAlternRemessa());
		saida.setDsCodMeioAlternRemessa(response.getDsCodMeioAlternRemessa());
		saida.setCdNivelControleRetorno(response.getCdNivelControleRetorno());
		saida.setDsNivelControleRetorno(response.getDsNivelControleRetorno());
		saida.setCdControleNumeroRetorno(response.getCdControleNumeroRetorno());
		saida.setDsControleNumeroRetorno(response.getDsControleNumeroRetorno());
		saida.setCdPeriodicodadeContagemRetorno(response.getCdPeriodicodadeContagemRetorno());
		saida.setDsPeriodicodadeContagemRetorno(response.getDsPeriodicodadeContagemRetorno());
		saida.setNrMaxContagemRetorno(Long.valueOf(0).equals(response.getNrMaximoContagemRetorno()) ? null : response.getNrMaximoContagemRetorno());
		saida.setCdMeioPrincipalRetorno(response.getDsCodMeioPrincipalRetorno());
		saida.setCdCodMeioPrincipalRetorno(response.getCdCodMeioPrincipalRetorno());
		saida.setCdMeioAltrnRetorno(response.getCdMeioAltrnRetorno());
		saida.setDsMeioAltrnRetorno(response.getDsMeioAltrnRetorno());
		saida.setCdSerieAplicTranmicao(response.getCdSerieAplicTranmicao());
		saida.setCdOperacaoCanalInclusao("0".equals(response.getCdOperacaoCanalInclusao()) ? "" : response.getCdOperacaoCanalInclusao());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
		saida.setCdOperacaoCanalManutencao("0".equals(response.getCdOperacaoCanalManutencao()) ? "" : response.getCdOperacaoCanalManutencao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoexterno(response.getCdUsuarioManutencaoexterno());
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
		saida.setDsResponsavelCustoEmpresa(response.getDsResponsavelCustoEmpresa());
		saida.setDsUtilizacaoEmpresaVan(response.getDsUtilizacaoEmpresaVan());
		saida.setCdPessoaJuridica(response.getCdPessoaJuridica());
	    saida.setCdUnidadeOrganizacional(Integer.valueOf(0).equals(response.getCdUnidadeOrganizacional()) ? null : response.getCdUnidadeOrganizacional());
	    saida.setDsNomeContatoCliente(response.getDsNomeContatoCliente());
	    saida.setCdAreaFoneCliente(response.getCdAreaFoneCliente());
	    saida.setCdFoneContatoCliente(NumberUtils.formatTelefoneSemDdd(response.getCdFoneContatoCliente()));
	    saida.setCdRamalContatoCliente(response.getCdRamalContatoCliente());
	    saida.setDsEmailContatoCliente(response.getDsEmailContatoCliente());
	    saida.setDsSolicitacaoPendente(response.getDsSolicitacaoPendente());
	    saida.setCdAreaFonePend(response.getCdAreaFonePend());
	    saida.setCdFoneSolicitacaoPend(NumberUtils.formatTelefoneSemDdd(response.getCdFoneSolicitacaoPend()));
	    saida.setCdRamalSolctPend(response.getCdRamalSolctPend());
	    saida.setDsEmailSolctPend(response.getDsEmailSolctPend());
	    saida.setQtMesRegistroTrafg(NumberUtils.formatVolume(response.getQtMesRegistroTrafg()).replace(",", "."));
	    saida.setDsObsGeralPerfil(response.getDsObsGeralPerfil());
	    saida.setCdAplicacaoTransmPagamento(response.getCdAplicacaoTransmPagamento());
	    saida.setCdResponsavelCustoEmpresa(response.getCdResponsavelCustoEmpresa());
	    saida.setCdPessoaJuridicaParceiro(response.getCdPessoaJuridicaParceiro());
		saida.setQtMaxIncotRemessaFormatada(PgitUtil.formatarNumero(response.getQtMaxIncotRemessa(), false));
		saida.setCdIndicadorLayoutProprio(response.getCdIndicadorLayoutProprio());
		saida.setDsIndicadorConsisteContaDebito(response.getDsIndicadorConsisteContaDebito());
		
		if (response.getCdTipoCanalInclusao() == 0) {
			saida.setCanalInclusaoFormatado("");
		} else {
			saida.setCanalInclusaoFormatado(PgitUtil.concatenarCampos(response.getCdTipoCanalInclusao(), response.getDsCanalInclusao()));
		}

		if (response.getCdTipoCanalManutencao() == 0) {
			saida.setCanalManutencaoFormatado("");
		} else {
			saida.setCanalManutencaoFormatado(PgitUtil.concatenarCampos(response.getCdTipoCanalManutencao(), response.getDsCanalManutencao()));
		}

		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setCdIndicadorGeracaoSegmentoB(response.getCdIndicadorGeracaoSegmentoB());
		saida.setDsSegmentoB(response.getDsSegmentoB());
		saida.setCdIndicadorGeracaoSegmentoZ(response.getCdIndicadorGeracaoSegmentoZ());
		saida.setDsSegmentoZ(response.getDsSegmentoZ());

		saida.setCdIndicadorCpfLayout(response.getCdIndicadorCpfLayout());
		saida.setDsIndicadorCpfLayout(response.getDsIndicadorCpfLayout());
		saida.setCdIndicadorAssociacaoLayout(response.getCdIndicadorAssociacaoLayout());
		saida.setDsIndicadorAssociacaoLayout(response.getDsIndicadorAssociacaoLayout());
		saida.setCdIndicadorContaComplementar(response.getCdIndicadorContaComplementar());
		saida.setDsIndicadorContaComplementar(response.getDsIndicadorContaComplementar());
		saida.setCdIndicadorConsisteContaDebito(response.getCdIndicadorConsisteContaDebito());
		
		saida.setCdTipoLayoutMae(response.getCdTipoLayoutMae());
	    saida.setCdTipoContratoMae(response.getCdTipoContratoMae());
	    saida.setCdTipoParticipanteMae(response.getCdTipoParticipanteMae());
	    saida.setCdFilialCnpj(response.getCdFilialCnpj());
	    saida.setCdControleCpfCnpj(response.getCdControleCpfCnpj());
	    saida.setDsNomeRazaoSocial(response.getDsNomeRazaoSocial());
	    saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
	    saida.setCdPerfilTrocaMae(response.getCdPerfilTrocaMae());
	    saida.setCdPessoaJuridicaMae(response.getCdPessoaJuridicaMae());
	    saida.setNrSequencialContratoMae(response.getNrSequencialContratoMae());
	    saida.setCdCpssoaMae(response.getCdCpssoaMae());
	    saida.setCdCpfCnpj(response.getCdCpfCnpj());
		
		return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService#incluirPerfilTrocaArquivo(br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.IncluirPerfilTrocaArquivoEntradaDTO)
     */
    public IncluirPerfilTrocaArquivoSaidaDTO incluirPerfilTrocaArquivo(IncluirPerfilTrocaArquivoEntradaDTO entrada) {
	IncluirPerfilTrocaArquivoSaidaDTO saida = new IncluirPerfilTrocaArquivoSaidaDTO();
	IncluirPerfilTrocaArquivoRequest request = new IncluirPerfilTrocaArquivoRequest();
	IncluirPerfilTrocaArquivoResponse response = new IncluirPerfilTrocaArquivoResponse();

	request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setCdSistemaOrigemArquivo(PgitUtil.verificaStringNula(entrada.getCdSistemaOrigemArquivo()));
	request.setDsUserNameCliente(PgitUtil.verificaStringNula(entrada.getDsUserNameCliente()));
	request.setDsNomeArquivoRemessa(PgitUtil.verificaStringNula(entrada.getDsNomeArquivoRemessa()));
	request.setDsNameArquivoRetorno(PgitUtil.verificaStringNula(entrada.getDsNameArquivoRetorno()));
	request.setCdRejeicaoAcltoRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdRejeicaoAcltoRemessa()));
	request.setQtMaxIncotRemessa(PgitUtil.verificaIntegerNulo(entrada.getQtMaxIncotRemessa()));
	request.setPercentualInconsistenciaRejeicaoRemessa(PgitUtil.verificaBigDecimalNulo(entrada.getPercentualInconsistenciaRejeicaoRemessa()));
	request.setCdNivelControleRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdNivelControleRemessa()));
	request.setCdControleNumeroRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdControleNumeroRemessa()));
	request.setCdPeriodicidadeContagemRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdPeriodicidadeContagemRemessa()));
	request.setNrMaximoContagemRemessa(PgitUtil.verificaLongNulo(entrada.getNrMaximoContagemRemessa()));
	request.setCdMeioPrincipalRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdMeioPrincipalRemessa()));
	request.setCdMeioAlternativoRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdMeioAlternativoRemessa()));
	request.setCdNivelControleRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdNivelControleRetorno()));
	request.setCdControleNumeroRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdControleNumeroRetorno()));
	request.setCdPerdcContagemRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdPerdcContagemRetorno()));
	request.setNrMaximoContagemRetorno(PgitUtil.verificaLongNulo(entrada.getNrMaximoContagemRetorno()));
	request.setCdMeioPrincipalRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdMeioPrincipalRetorno()));
	request.setCdMeioAlternativoRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdMeioAlternativoRetorno()));
	request.setCdSerieAplicacaoTransmissao(PgitUtil.verificaStringNula(entrada.getCdSerieAplicacaoTransmissao()));
	request.setCdPessoaJuridicaParceiro(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaParceiro()));
	request.setCdEmpresaResponsavel(PgitUtil.verificaIntegerNulo(entrada.getCdEmpresaResponsavel()));
	request.setCdCustoTransmicao(PgitUtil.verificaBigDecimalNulo(entrada.getCdCustoTransmicao()));
	request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
	request.setCdUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entrada.getCdUnidadeOrganizacional()));
	request.setDsNomeContatoCliente(PgitUtil.verificaStringNula(entrada.getDsNomeContatoCliente()));
	request.setCdAreaFone(PgitUtil.verificaIntegerNulo(entrada.getCdAreaFone()));
	request.setCdFoneContatoCliente(PgitUtil.verificaLongNulo(entrada.getCdFoneContatoCliente()));
	request.setCdRamalContatoCliente(PgitUtil.verificaStringNula(entrada.getCdRamalContatoCliente()));
	request.setDsEmailContatoCliente(PgitUtil.verificaStringNula(entrada.getDsEmailContatoCliente()));
	request.setDsSolicitacaoPendente(PgitUtil.verificaStringNula(entrada.getDsSolicitacaoPendente()));
	request.setCdAreaFonePend(PgitUtil.verificaIntegerNulo(entrada.getCdAreaFonePend()));
	request.setCdFoneSolicitacaoPend(PgitUtil.verificaLongNulo(entrada.getCdFoneSolicitacaoPend()));
	request.setCdRamalSolctPend(PgitUtil.verificaStringNula(entrada.getCdRamalSolctPend()));
	request.setDsEmailSolctPend(PgitUtil.verificaStringNula(entrada.getDsEmailSolctPend()));
	request.setQtMesRegistroTrafg(PgitUtil.verificaLongNulo(entrada.getQtMesRegistroTrafg()));
	request.setDsObsGeralPerfil(PgitUtil.verificaStringNula(entrada.getDsObsGeralPerfil()));
	request.setCdIndicadorGeracaoSegmentoB(verificaIntegerNulo(entrada.getCdIndicadorGeracaoSegmentoB()));
	request.setCdIndicadorGeracaoSegmentoZ(verificaIntegerNulo(entrada.getCdIndicadorGeracaoSegmentoZ()));

	request.setCdIndicadorCpfLayout(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorCpfLayout()));
	request.setCdIndicadorContaComplementar(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorContaComplementar()));
	request.setCdIndicadorConsisteContaDebito(verificaIntegerNulo(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorConsisteContaDebito())));
	request.setCdIndicadorAssociacaoLayout(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorAssociacaoLayout()));
	request.setCdIndicadorGeracaoSegmentoB(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorGeracaoSegmentoB()));
	request.setCdIndicadorGeracaoSegmentoZ(PgitUtil.verificaIntegerNulo(entrada.getCdIndicadorGeracaoSegmentoZ()));
	
	request.setMaxOcorrencias(verificaIntegerNulo(entrada.getMaximaOcorrencias()));
	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias> lista = 
		new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias>();
	
	br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias ocorrencia;

	
	if(entrada.getOcorrencias().isEmpty()){
		ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias();
		
		ocorrencia.setCdProdutoOperacaoRelacionado(0);
	    ocorrencia.setCdIndicadorLayoutProprio(0);
	    ocorrencia.setCdApliFormat(0);
		
		lista.add(ocorrencia);
	}else{
		// V�rias Ocorr�ncias para Antecipa��o
		for (int i = 0; i < entrada.getOcorrencias().size(); i++) {
		    ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias();
		    
		    ocorrencia.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getOcorrencias().get(i).getCdProdutoOperacaoRelacionado()));
		    ocorrencia.setCdIndicadorLayoutProprio(PgitUtil.verificaIntegerNulo(entrada.getOcorrencias().get(i).getCdIndicadorLayoutProprio()));
		    ocorrencia.setCdApliFormat(PgitUtil.verificaIntegerNulo(entrada.getOcorrencias().get(i).getCdApliFormat()));
		    lista.add(ocorrencia);
		}
	}
	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias[]) lista
		.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias[0]));
	
	response = getFactoryAdapter().getIncluirPerfilTrocaArquivoPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService#alterarPerfilTrocaArquivo(br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.AlterarPerfilTrocaArquivoEntradaDTO)
     */
    public AlterarPerfilTrocaArquivoSaidaDTO alterarPerfilTrocaArquivo(AlterarPerfilTrocaArquivoEntradaDTO entrada) {
	AlterarPerfilTrocaArquivoSaidaDTO saida = new AlterarPerfilTrocaArquivoSaidaDTO();
	AlterarPerfilTrocaArquivoRequest request = new AlterarPerfilTrocaArquivoRequest();
	AlterarPerfilTrocaArquivoResponse response = new AlterarPerfilTrocaArquivoResponse();

	request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setCdSistemaOrigemArquivo(PgitUtil.verificaStringNula(entrada.getCdSistemaOrigemArquivo()));
	request.setDsUserNameCliente(PgitUtil.verificaStringNula(entrada.getDsUserNameCliente()));
	request.setDsNomeArquivoRemessa(PgitUtil.verificaStringNula(entrada.getDsNomeArquivoRemessa()));
	request.setDsNameArquivoRetorno(PgitUtil.verificaStringNula(entrada.getDsNameArquivoRetorno()));
	request.setCdRejeicaoAcltoRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdRejeicaoAcltoRemessa()));
	request.setQtMaxIncotRemessa(PgitUtil.verificaIntegerNulo(entrada.getQtMaxIncotRemessa()));
	request.setPercentualInconsistenciaRejeicaoRemessa(PgitUtil.verificaBigDecimalNulo(entrada.getPercentualInconsistenciaRejeicaoRemessa()));
	request.setCdNivelControleRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdNivelControleRemessa()));
	request.setCdControleNumeroRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdControleNumeroRemessa()));
	request.setCdPeriodicidadeContagemRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdPeriodicidadeContagemRemessa()));
	request.setNrMaximoContagemRemessa(PgitUtil.verificaLongNulo(entrada.getNrMaximoContagemRemessa()));
	request.setCdMeioPrincipalRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdMeioPrincipalRemessa()));
	request.setCdMeioAlternativoRemessa(PgitUtil.verificaIntegerNulo(entrada.getCdMeioAlternativoRemessa()));
	request.setCdNivelControleRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdNivelControleRetorno()));
	request.setCdControleNumeroRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdControleNumeroRetorno()));
	request.setCdPerdcContagemRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdPerdcContagemRetorno()));
	request.setNrMaximoContagemRetorno(PgitUtil.verificaLongNulo(entrada.getNrMaximoContagemRetorno()));
	request.setCdMeioPrincipalRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdMeioPrincipalRetorno()));
	request.setCdMeioAlternativoRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdMeioAlternativoRetorno()));
	request.setCdSerieAplicacaoTransmissao(PgitUtil.verificaStringNula(entrada.getCdSerieAplicacaoTransmissao()));
	//request.setCdEmpresaResponsavelTransporte(PgitUtil.verificaLongNulo(entrada.getCdEmpresaResponsavelTransporte()));
	request.setCdApliFormat(PgitUtil.verificaLongNulo(entrada.getCdApliFormat()));
	
	request.setCdPessoaJuridicaParceiro(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaParceiro()));
	request.setCdEmpresaResponsavel(PgitUtil.verificaIntegerNulo(entrada.getCdEmpresaResponsavel()));
	request.setCdCustoTransmicao(PgitUtil.verificaBigDecimalNulo(entrada.getCdCustoTrasmicao()));
	
	request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
	request.setCdUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entrada.getCdUnidadeOrganizacional()));
	request.setDsNomeContatoCliente(PgitUtil.verificaStringNula(entrada.getDsNomeContatoCliente()));
	request.setCdAreaFone(PgitUtil.verificaIntegerNulo(entrada.getCdAreaFone()));
	request.setCdFoneContatoCliente(PgitUtil.verificaLongNulo(entrada.getCdFoneContatoCliente()));
	request.setCdRamalContatoCliente(PgitUtil.verificaStringNula(entrada.getCdRamalContatoCliente()));
	request.setDsEmailContatoCliente(PgitUtil.verificaStringNula(entrada.getDsEmailContatoCliente()));
	request.setDsSolicitacaoPendente(PgitUtil.verificaStringNula(entrada.getDsSolicitacaoPendente()));
	request.setCdAreaFonePend(PgitUtil.verificaIntegerNulo(entrada.getCdAreaFonePend()));
	request.setCdFoneSolicitacaoPend(PgitUtil.verificaLongNulo(entrada.getCdFoneSolicitacaoPend()));
	request.setCdRamalSolctPend(PgitUtil.verificaStringNula(entrada.getCdRamalSolctPend()));
	request.setDsEmailSolctPend(PgitUtil.verificaStringNula(entrada.getDsEmailSolctPend()));
	request.setQtMesRegistroTrafg(PgitUtil.verificaLongNulo(entrada.getQtMesRegistroTrafg()));
	request.setDsObsGeralPerfil(PgitUtil.verificaStringNula(entrada.getDsObsGeralPerfil()));
	request.setCdIndicadorGeracaoSegmentoB(verificaIntegerNulo(entrada.getCdIndicadorGeracaoSegmentoB()));
	request.setCdIndicadorGeracaoSegmentoZ(verificaIntegerNulo(entrada.getCdIndicadorGeracaoSegmentoZ()));
	request.setCdIndicadorCpfLayout(verificaIntegerNulo(entrada.getCdIndicadorCpfLayout()));
	request.setCdIndicadorContaComplementar(verificaIntegerNulo(entrada.getCdIndicadorContaComplementar()));
	request.setCdIndicadorAssociacaoLayout(verificaIntegerNulo(entrada.getCdIndicadorAssociacaoLayout()));
	request.setCdIndicadorConsisteContaDebito(verificaIntegerNulo(entrada.getCdContaDebito()));
	
	response = getFactoryAdapter().getAlterarPerfilTrocaArquivoPDCAdapter().invokeProcess(request);
	
	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService#excluirPerfilTrocaArquivo(br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ExcluirPerfilTrocaArquivoEntradaDTO)
     */
    public ExcluirPerfilTrocaArquivoSaidaDTO excluirPerfilTrocaArquivo(ExcluirPerfilTrocaArquivoEntradaDTO entrada) {
	ExcluirPerfilTrocaArquivoSaidaDTO saida = new ExcluirPerfilTrocaArquivoSaidaDTO();
	ExcluirPerfilTrocaArquivoRequest request = new ExcluirPerfilTrocaArquivoRequest();
	ExcluirPerfilTrocaArquivoResponse response = new ExcluirPerfilTrocaArquivoResponse();

	request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setCdPerfilTrocaArquivo(verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
	request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
	request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
	request.setCdRelacionamentoProduto(verificaIntegerNulo(entrada.getCdRelacionamentoProduto()));

	response = getFactoryAdapter().getExcluirPerfilTrocaArquivoPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService#consultarHistoricoPerfilTrocaArquivo(br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarHistoricoPerfilTrocaArquivoEntradaDTO)
     */
    public List<ConsultarHistoricoPerfilTrocaArquivoSaidaDTO> consultarHistoricoPerfilTrocaArquivo(
	    ConsultarHistoricoPerfilTrocaArquivoEntradaDTO entrada) {
	List<ConsultarHistoricoPerfilTrocaArquivoSaidaDTO> listaSaida = new ArrayList<ConsultarHistoricoPerfilTrocaArquivoSaidaDTO>();
	ConsultarHistoricoPerfilTrocaArquivoRequest request = new ConsultarHistoricoPerfilTrocaArquivoRequest();
	ConsultarHistoricoPerfilTrocaArquivoResponse response = new ConsultarHistoricoPerfilTrocaArquivoResponse();

	request.setMaxOcorrencias(IManterPerfilTrocaArqLayoutServiceConstants.NUMERO_OCORRENCIAS_HISTORICO);
	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
	request.setDtManutencaoInicio(PgitUtil.verificaStringNula(entrada.getDtManutencaoInicio()));
	request.setDtManutencaoFim(PgitUtil.verificaStringNula(entrada.getDtManutencaoFim()));

	response = getFactoryAdapter().getConsultarHistoricoPerfilTrocaArquivoPDCAdapter().invokeProcess(request);

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    ConsultarHistoricoPerfilTrocaArquivoSaidaDTO saida = new ConsultarHistoricoPerfilTrocaArquivoSaidaDTO();
	    saida.setCodMensagem(response.getCodMensagem());
	    saida.setMensagem(response.getMensagem());

	    saida.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
	    saida.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
	    saida.setCdPerfilTrocaArquivo(response.getOcorrencias(i).getCdPerfilTrocaArquivo());
	    saida.setHrManutencao(response.getOcorrencias(i).getHrManutencao());

	    saida.setDataHoraFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrManutencao()));

	    listaSaida.add(saida);
	}

	return listaSaida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService#detalharHistoricoPerfilTrocaArquivo(br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharHistoricoPerfilTrocaArquivoEntradaDTO)
     */
    public DetalharHistoricoPerfilTrocaArquivoSaidaDTO detalharHistoricoPerfilTrocaArquivo(DetalharHistoricoPerfilTrocaArquivoEntradaDTO entrada) {
	DetalharHistoricoPerfilTrocaArquivoSaidaDTO saida = new DetalharHistoricoPerfilTrocaArquivoSaidaDTO();
	DetalharHistoricoPerfilTrocaArquivoRequest request = new DetalharHistoricoPerfilTrocaArquivoRequest();
	DetalharHistoricoPerfilTrocaArquivoResponse response = new DetalharHistoricoPerfilTrocaArquivoResponse();

	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
	request.setHrInclusaoRegistroHist(PgitUtil.verificaStringNula(entrada.getHrInclusaoRegistroHist()));

	response = getFactoryAdapter().getDetalharHistoricoPerfilTrocaArquivoPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());
	saida.setCdPerfilTrocaArq(response.getCdPerfilTrocaArq());
	saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
	saida.setDsCodTipoLayout(response.getDsCodTipoLayout());
	saida.setDsAplicFormat(response.getDsAplicFormat());
	saida.setCdSistemaOrigemArquivo(response.getCdSistemaOrigemArquivo());
	saida.setDsSistemaOrigemArquivo(response.getDsSistemaOrigemArquivo());
	saida.setCdPessoaJuridicaParceiro(response.getCdPessoaJuridicaParceiro());
	saida.setDsEmpresa(response.getDsEmpresa());
	saida.setPcCustoOrganizacaoTransmissao(response.getPcCustoOrganizacaoTransmissao());
	saida.setDsResponsavelCustoEmpresa(response.getDsResponsavelCustoEmpresa());
	saida.setDsUtilizacaoEmpresaVan(response.getDsUtilizacaoEmpresaVan());
	saida.setDsNomeCliente(response.getDsNomeCliente());
	saida.setDsNomeArquivoRemessa(response.getDsNomeArquivoRemessa());
	saida.setDsNomeArquivoRetorno(response.getDsNomeArquivoRetorno());
	saida.setCdRejeicaoAcltoRemessa(response.getCdRejeicaoAcltoRemessa());
	saida.setDsRejeicaoAcltoRemessa(response.getDsRejeicaoAcltoRemessa());
	saida.setQtMaxIncotRemessa(response.getQtMaxIncotRemessa());
	saida.setPercentualIncotRejeiRemessa(response.getPercentualIncotRejeiRemessa());
	saida.setCdNivelControleRemessa(response.getCdNivelControleRemessa());
	saida.setDsNivelControleRemessa(response.getDsNivelControleRemessa());
	saida.setCdControleNumeroRemessa(response.getCdControleNumeroRemessa());
	saida.setDsControleNumeroRemessa(response.getDsControleNumeroRemessa());
	saida.setCdPeriodicidadeContagemRemessa(response.getCdPeriodicidadeContagemRemessa());
	saida.setDsPeriodicidadeContagemRemessa(response.getDsPeriodicidadeContagemRemessa());
	saida.setNrMaxContagemRemessa(response.getNrMaxContagemRemessa());
	saida.setCdMeioPrincipalRemessa(response.getCdMeioPrincipalRemessa());
	saida.setDsCodMeioPrincipalRemessa(response.getDsCodMeioPrincipalRemessa());
	saida.setCdMeioAlternRemessa(response.getCdMeioAlternRemessa());
	saida.setDsCodMeioAlternRemessa(response.getDsCodMeioAlternRemessa());
	saida.setCdNivelControleRetorno(response.getCdNivelControleRetorno());
	saida.setDsNivelControleRetorno(response.getDsNivelControleRetorno());
	saida.setCdControleNumeroRetorno(response.getCdControleNumeroRetorno());
	saida.setDsControleNumeroRetorno(response.getDsControleNumeroRetorno());
	saida.setCdPeriodicodadeContagemRetorno(response.getCdPeriodicodadeContagemRetorno());
	saida.setDsPeriodicodadeContagemRetorno(response.getDsPeriodicodadeContagemRetorno());
	saida.setNrMaxContagemRetorno(response.getNrMaxContagemRetorno());
	saida.setCdMeioPrincipalRetorno(response.getCdMeioPrincipalRetorno());
	//saida.setDsMeioPrincipalRetorno(response.getDsCodMeioPrincipalRemessa());
	saida.setCdMeioAltrnRetorno(response.getCdMeioAltrnRetorno());
	saida.setDsMeioAltrnRetorno(response.getDsMeioAltrnRetorno());
	saida.setCdSerieAplicTranmicao(response.getCdSerieAplicTranmicao());
	saida.setCdIndicadorTipoManutencao(response.getCdIndicadorTipoManutencao());
	saida.setDsIndicadorTipoManutencao(response.getDsIndicadorTipoManutencao());
	saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
	saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
	saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
	saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
	saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
	saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
	saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
	saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
	saida.setCdUsuarioManutencaoexterno(response.getCdUsuarioManutencaoexterno());
	saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
	saida.setDsCanalInclusao(response.getDsCanalInclusao());
	saida.setDsCanalManutencao(response.getDsCanalManutencao());
	
	saida.setCdPessoaJuridica(response.getCdPessoaJuridica());
    saida.setCdUnidadeOrganizacional(response.getCdUnidadeOrganizacional());
    saida.setDsNomeContatoCliente(response.getDsNomeContatoCliente());
    saida.setCdAreaFoneCliente(response.getCdAreaFoneCliente());
    saida.setCdFoneContatoCliente(response.getCdFoneContatoCliente());
    saida.setCdRamalContatoCliente(response.getCdRamalContatoCliente());
    saida.setDsEmailContatoCliente(response.getDsEmailContatoCliente());
    saida.setDsSolicitacaoPendente(response.getDsSolicitacaoPendente());
    saida.setCdAreaFonePend(response.getCdAreaFonePend());
    saida.setCdFoneSolicitacaoPend(response.getCdFoneSolicitacaoPend());
    saida.setCdRamalSolctPend(response.getCdRamalSolctPend());
    saida.setDsEmailSolctPend(response.getDsEmailSolctPend());
    saida.setQtMesRegistroTrafg(response.getQtMesRegistroTrafg());
    saida.setDsObsGeralPerfil(response.getDsObsGeralPerfil());
    saida.setCdCodMeioPrincipalRetorno(response.getCdCodMeioPrincipalRetorno());
    saida.setCdIndicadorGeracaoSegmentoB(response.getCdIndicadorGeracaoSegmentoB());
	saida.setDsSegmentoB(response.getDsSegmentoB());
    saida.setCdIndicadorGeracaoSegmentoZ(response.getCdIndicadorGeracaoSegmentoZ());
    saida.setDsSegmentoZ(response.getDsSegmentoZ());
    
	saida.setQtMaxIncotRemessaFormatada(PgitUtil.formatarNumero(response.getQtMaxIncotRemessa(), true));
	saida.setCanalInclusaoFormatado(PgitUtil.concatenarCampos(saida.getCdTipoCanalInclusao(), saida.getDsCanalInclusao()));
	saida.setCanalManutencaoFormatado(PgitUtil.concatenarCampos(saida.getCdTipoCanalManutencao(), saida.getDsCanalManutencao()));
	saida.setDsIndicadorConsisteContaDebito(response.getDsIndicadorConsisteContaDebito());
	
	saida.setCdIndicadorCpfLayout(response.getCdIndicadorCpfLayout());
	saida.setDsIndicadorCpfLayout(response.getDsIndicadorCpfLayout());
	saida.setCdIndicadorAssociacaoLayout(response.getCdIndicadorAssociacaoLayout());
	saida.setDsIndicadorAssociacaoLayout(response.getDsIndicadorAssociacaoLayout());
	saida.setCdIndicadorContaComplementar(response.getCdIndicadorContaComplementar());
	saida.setDsIndicadorContaComplementar(response.getDsIndicadorContaComplementar());
	
	return saida;
    }

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService#listarContratosVinculadosPerfil(br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ListaContratosVinculadosPerfilEntradaDTO)
	 */
	public List<ListaContratosVinculadosPerfilOcorrencias> listarContratosVinculadosPerfil(
			ListaContratosVinculadosPerfilEntradaDTO entrada) {
		
		ListaContratosVinculadosPerfilRequest request = new ListaContratosVinculadosPerfilRequest();
		ListaContratosVinculadosPerfilResponse response = null;		
		List<ListaContratosVinculadosPerfilOcorrencias> listaContratos = null;
		
		request.setNrOcorrencias(20);
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
		request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));
		
		response = getFactoryAdapter().getListaContratosVinculadosPerfilPDCAdapter().invokeProcess(request);
		
		if(response.getNumeroLinhas() != 0){
			listaContratos = new ArrayList<ListaContratosVinculadosPerfilOcorrencias>();
			ListaContratosVinculadosPerfilOcorrencias ocorrencia = null;
			
			for (int i = 0; i < response.getNumeroLinhas(); i++) {
				ocorrencia = new ListaContratosVinculadosPerfilOcorrencias();
				
				ocorrencia.setCdClubRepresentante(response.getOcorrencias(i).getCdClubRepresentante());
				
				ocorrencia.setCdCorpoCnpjRepresentante(response.getOcorrencias(i).getCdCorpoCnpjRepresentante());
				ocorrencia.setCdFilialCnpjRepresentante(response.getOcorrencias(i).getCdFilialCnpjRepresentante());
				ocorrencia.setCdControleCnpjRepresentante(response.getOcorrencias(i).getCdControleCnpjRepresentante());
				ocorrencia.setNmRazaoRepresentante(response.getOcorrencias(i).getNmRazaoRepresentante());
				ocorrencia.setCdPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
				ocorrencia.setDsPessoaJuridica(response.getOcorrencias(i).getDsPessoaJuridica());
				ocorrencia.setCdTipoContrato(response.getOcorrencias(i).getCdTipoContrato());
				ocorrencia.setDsTipoContrato(response.getOcorrencias(i).getDsTipoContrato());
				ocorrencia.setNrSequenciaContrato(response.getOcorrencias(i).getNrSequenciaContrato());
				ocorrencia.setDsContrato(response.getOcorrencias(i).getDsContrato());
				ocorrencia.setCdAgenciaOperadora(response.getOcorrencias(i).getCdAgenciaOperadora());
				ocorrencia.setCdDigitoAgenciaOperadora(response.getOcorrencias(i).getCdDigitoAgenciaOperadora());
				ocorrencia.setDsAgenciaOperadora(response.getOcorrencias(i).getDsAgenciaOperadora());
				ocorrencia.setCdFuncionarioBradesco(response.getOcorrencias(i).getCdFuncionarioBradesco());
				ocorrencia.setDsFuncionarioBradesco(response.getOcorrencias(i).getDsFuncionarioBradesco());
				ocorrencia.setCdSituacaoContrato(response.getOcorrencias(i).getCdSituacaoContrato());
				ocorrencia.setDsSituacaoContrato(response.getOcorrencias(i).getDsSituacaoContrato());
				ocorrencia.setCdMotivoSituacao(response.getOcorrencias(i).getCdMotivoSituacao());
				ocorrencia.setDsMotivoSituacao(response.getOcorrencias(i).getDsMotivoSituacao());
				ocorrencia.setCdAditivo(response.getOcorrencias(i).getCdAditivo());
				ocorrencia.setCdTipoParticipacao(response.getOcorrencias(i).getCdTipoParticipacao());
				ocorrencia.setCdSegmentoCliente(response.getOcorrencias(i).getCdSegmentoCliente());
				ocorrencia.setDsSegmentoCliente(response.getOcorrencias(i).getDsSegmentoCliente());
				ocorrencia.setCdSubSegmentoCliente(response.getOcorrencias(i).getCdSubSegmentoCliente());
				ocorrencia.setDsSubSegmentoCliente(response.getOcorrencias(i).getDsSubSegmentoCliente());
				ocorrencia.setCdAtividadeEconomica(response.getOcorrencias(i).getCdAtividadeEconomica());
				ocorrencia.setDsAtividadeEconomica(response.getOcorrencias(i).getDsAtividadeEconomica());
				ocorrencia.setCdGrupoEconomico(response.getOcorrencias(i).getCdGrupoEconomico());
				ocorrencia.setDsGrupoEconomico(response.getOcorrencias(i).getDsGrupoEconomico());
				ocorrencia.setDtAbertura(FormatarData.formataTimestampFromPdc(response.getOcorrencias(i).getDtAbertura()));
				ocorrencia.setDtEncerramento(FormatarData.formataTimestampFromPdc(response.getOcorrencias(i).getDtEncerramento()));
				ocorrencia.setDtInclusao(FormatarData.formataTimestampFromPdc(response.getOcorrencias(i).getDtInclusao()));
				
				listaContratos.add(ocorrencia);
			}
		}
		if(listaContratos == null){
			return Collections.EMPTY_LIST; 
		}
		return listaContratos;
	}
}