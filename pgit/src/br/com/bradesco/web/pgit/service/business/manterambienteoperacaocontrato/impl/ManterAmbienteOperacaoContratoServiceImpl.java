/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarContratoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbPartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.DetalharAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.DetalharAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontrato.request.ConsultarContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontrato.response.ConsultarContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambiente.request.ConsultarSolicitacaoAmbienteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambiente.response.ConsultarSolicitacaoAmbienteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.request.ConsultarSolicitacaoAmbientePartcRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.response.ConsultarSolicitacaoAmbientePartcResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambiente.request.ConsultarSolicitacaoMudancaAmbienteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambiente.response.ConsultarSolicitacaoMudancaAmbienteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.request.ConsultarSolicitacaoMudancaAmbPartcRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.response.ConsultarSolicitacaoMudancaAmbPartcResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.detalharambiente.request.DetalharAmbienteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharambiente.response.DetalharAmbienteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralteracaoambiente.request.ExcluirAlteracaoAmbienteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralteracaoambiente.response.ExcluirAlteracaoAmbienteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralteracaoambientepartc.request.ExcluirAlteracaoAmbientePartcRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralteracaoambientepartc.response.ExcluirAlteracaoAmbientePartcResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.request.IncluirAlteracaoAmbienteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.response.IncluirAlteracaoAmbienteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambientepartc.request.IncluirAlteracaoAmbientePartcRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambientepartc.response.IncluirAlteracaoAmbientePartcResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarambiente.request.ListarAmbienteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarambiente.response.ListarAmbienteResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterAmbienteOperacaoContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterAmbienteOperacaoContratoServiceImpl implements
		IManterAmbienteOperacaoContratoService {

	private static final int QTDE_OCORRENCIAS = 20;
	private static final int PARAMETRO_PROGRAMA = 6;
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 * 
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 * 
	 * @param factoryAdapter
	 *            the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#consultarSolicitacaoAmbiente(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbienteEntradaDTO)
	 */
	public ConsultarSolicitacaoAmbienteSaidaDTO consultarSolicitacaoAmbiente(
			ConsultarSolicitacaoAmbienteEntradaDTO consultarSolicitacaoAmbienteEntradaDTO) {

		ConsultarSolicitacaoAmbienteSaidaDTO saidaDTO = new ConsultarSolicitacaoAmbienteSaidaDTO();
		ConsultarSolicitacaoAmbienteRequest request = new ConsultarSolicitacaoAmbienteRequest();
		ConsultarSolicitacaoAmbienteResponse response = new ConsultarSolicitacaoAmbienteResponse();

		request
				.setCdSolicitacaoPagamentoIntegrado(consultarSolicitacaoAmbienteEntradaDTO
						.getCdSolicitacaoPagamentoIntegrado());
		request
				.setNrSolicitacaoPagamentoIntegrado(consultarSolicitacaoAmbienteEntradaDTO
						.getNrSolicitacaoPagamentoIntegrado());

		response = getFactoryAdapter()
				.getConsultarSolicitacaoAmbientePDCAdapter().invokeProcess(
						request);

		saidaDTO = new ConsultarSolicitacaoAmbienteSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDsServico(response.getDsServico());
		saidaDTO.setDsModalidade(response.getDsModalidade());
		saidaDTO.setCdTipoMudanca(response.getCdTipoMudanca());
		saidaDTO.setCdFormaEfetivacao(response.getCdFormaEfetivacao());
		saidaDTO.setDtProgramada(response.getDtProgramada());
		saidaDTO.setCdSituacao(response.getCdSituacao());
		saidaDTO.setCdManterOperacao(response.getCdManterOperacao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO
				.setCdUsuarioInclusaoExter(response.getCdUsuarioInclusaoExter());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExter(response
				.getCdUsuarioManutencaoExter());
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());

		saidaDTO.setDtInclusao(response.getDtInclusao() + " "
				+ response.getHrInclusao()); // Data + Hora da Trilha de
		// Auditoria Concatenados
		saidaDTO.setDtManutencao(response.getDtManutencao() + " "
				+ response.getHrManutencao()); // Data + Hora da Trilha de
		// Auditoria Concatenados

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#consultarSolicitacaoAmbientePartc(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbientePartcEntradaDTO)
	 */
	public ConsultarSolicitacaoAmbientePartcSaidaDTO consultarSolicitacaoAmbientePartc(
			ConsultarSolicitacaoAmbientePartcEntradaDTO consultarSolicitacaoAmbientePartcEntradaDTO) {

		ConsultarSolicitacaoAmbientePartcSaidaDTO saidaDTO = new ConsultarSolicitacaoAmbientePartcSaidaDTO();
		ConsultarSolicitacaoAmbientePartcRequest request = new ConsultarSolicitacaoAmbientePartcRequest();
		ConsultarSolicitacaoAmbientePartcResponse response = new ConsultarSolicitacaoAmbientePartcResponse();

		request
				.setCdSolicitacaoPagamentoIntegrado(PgitUtil
						.verificaIntegerNulo(consultarSolicitacaoAmbientePartcEntradaDTO
								.getCdSolicitacaoPagamentoIntegrado()));
		request
				.setNrSolicitacaoPagamentoIntegrado(PgitUtil
						.verificaIntegerNulo(consultarSolicitacaoAmbientePartcEntradaDTO
								.getNrSolicitacaoPagamentoIntegrado()));

		response = getFactoryAdapter()
				.getConsultarSolicitacaoAmbientePartcPDCAdapter()
				.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDsServico(response.getDsServico());
		saidaDTO.setDsModalidade(response.getDsModalidade());
		saidaDTO.setCdTipoMudanca(response.getCdTipoMudanca());
		saidaDTO.setCdFormaEfetivacao(response.getCdFormaEfetivacao());
		saidaDTO.setDtProgramada(response.getDtProgramada());
		saidaDTO.setCdSituacao(response.getCdSituacao());
		saidaDTO.setCdManterOperacao(response.getCdManterOperacao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());

		saidaDTO.setCdServico(response.getCdServico());
		saidaDTO.setCdModalidade(response.getCdModalidade());
		saidaDTO.setCdMotivoSituacao(response.getCdMotivoSituacao());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoParticipacaoPessoa(response
				.getCdTipoParticipacaoPessoa());
		saidaDTO.setCdPessoa(response.getCdPessoa());
		saidaDTO.setCdCnpjParticipante(response.getCdCnpjParticipante());
		saidaDTO.setCdFilialParticipante(response.getCdFilialParticipante());
		saidaDTO
				.setCdControlePArticipante(response.getCdControlePArticipante());
		saidaDTO.setDsNomeRazaoParticipante(response
				.getDsNomeRazaoParticipante());

		saidaDTO.setDtInclusao(response.getDtInclusao() + " "
				+ response.getHrInclusao()); // Data + Hora da Trilha de
		// Auditoria Concatenados
		saidaDTO.setDtManutencao(response.getDtManutencao() + " "
				+ response.getHrManutencao()); // Data + Hora da Trilha de
		// Auditoria Concatenados

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#consultarSolicitacaoMudancaAmbiente(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbienteEntradaDTO)
	 */
	public List<ConsultarSolicitacaoMudancaAmbienteSaidaDTO> consultarSolicitacaoMudancaAmbiente(
			ConsultarSolicitacaoMudancaAmbienteEntradaDTO consultarSolicitacaoMudancaAmbienteEntradaDTO) {

		List<ConsultarSolicitacaoMudancaAmbienteSaidaDTO> listaRetorno = new ArrayList<ConsultarSolicitacaoMudancaAmbienteSaidaDTO>();
		ConsultarSolicitacaoMudancaAmbienteRequest request = new ConsultarSolicitacaoMudancaAmbienteRequest();
		ConsultarSolicitacaoMudancaAmbienteResponse response = new ConsultarSolicitacaoMudancaAmbienteResponse();

		request
				.setCdPessoaJuridica(consultarSolicitacaoMudancaAmbienteEntradaDTO
						.getCdPessoaJuridica());
		request
				.setCdTipoContratoNegocio(consultarSolicitacaoMudancaAmbienteEntradaDTO
						.getCdTipoContratoNegocio());
		request
				.setNrSequenciaContratoNegocio(consultarSolicitacaoMudancaAmbienteEntradaDTO
						.getNrSequenciaContratoNegocio());
		request
				.setDtInicioSolicitacao(consultarSolicitacaoMudancaAmbienteEntradaDTO
						.getDtInicioSolicitacao());
		request
				.setDtFimSolicitacao(consultarSolicitacaoMudancaAmbienteEntradaDTO
						.getDtFimSolicitacao());
		request.setNumeroOcorrencias(30);

		response = getFactoryAdapter()
				.getConsultarSolicitacaoMudancaAmbientePDCAdapter()
				.invokeProcess(request);
		ConsultarSolicitacaoMudancaAmbienteSaidaDTO saidaDTO;

		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat formato3 = new SimpleDateFormat("dd.MM.yyyy");
		SimpleDateFormat formato4 = new SimpleDateFormat("dd/MM/yyyy");

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ConsultarSolicitacaoMudancaAmbienteSaidaDTO();

			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdSolicitacao(response.getOcorrencias(i)
					.getCdSolicitacao());
			saidaDTO.setNrSolicitacao(response.getOcorrencias(i)
					.getNrSolicitacao());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i)
					.getCdProdutoServicoOperacao());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i)
					.getDsProdutoServicoOperacao());
			saidaDTO.setCdAmbienteAtivoSolicitacao(response.getOcorrencias(i)
					.getCdAmbienteAtivoSolicitacao());

			// saidaDTO.setDtProgramadaMudancaAmbiente(response.getOcorrencias(i).getDtProgramadaMudancaAmbiente());
			saidaDTO.setCdSituacaoSolicitacaoPagamento(response.getOcorrencias(
					i).getCdSituacaoSolicitacaoPagamento());
			saidaDTO.setDsSituacaoSolicitacao(response.getOcorrencias(i)
					.getDsSituacaoSolicitacao());
			// saidaDTO.setDtSolicitacao(response.getOcorrencias(0).getDtSolicitacao());
			saidaDTO.setDsModalidade(response.getOcorrencias(i)
					.getDsProdutoServicoRelacionado());
			saidaDTO.setCdModalidade(response.getOcorrencias(i)
					.getCdProdutoServicoRelacionado());

			if (response.getOcorrencias(i).getDtSolicitacao() != null
					&& response.getOcorrencias(i).getDtSolicitacao().length() >= 19) {
				String stringData = response.getOcorrencias(i)
						.getDtSolicitacao().substring(0, 19);
				try {
					saidaDTO.setDtSolicitacao(formato2.format(formato1
							.parse(stringData)));
					saidaDTO.setDtProgramadaMudancaAmbiente(formato4
							.format(formato3.parse(response.getOcorrencias(i)
									.getDtProgramadaMudancaAmbiente())));
				} catch (ParseException e) {
					saidaDTO.setDtSolicitacao("");
					saidaDTO.setDtProgramadaMudancaAmbiente("");
				}
			}

			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#incluirAlteracaoAmbiente(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbienteEntradaDTO)
	 */
	public IncluirAlteracaoAmbienteSaidaDTO incluirAlteracaoAmbiente(
			IncluirAlteracaoAmbienteEntradaDTO incluirAlteracaoAmbienteEntradaDTO) {

		IncluirAlteracaoAmbienteSaidaDTO saidaDTO = new IncluirAlteracaoAmbienteSaidaDTO();
		IncluirAlteracaoAmbienteRequest request = new IncluirAlteracaoAmbienteRequest();
		IncluirAlteracaoAmbienteResponse response = new IncluirAlteracaoAmbienteResponse();

		request.setCdPessoaJuridica(PgitUtil
				.verificaLongNulo(incluirAlteracaoAmbienteEntradaDTO
						.getCdPessoaJuridicaNegocio()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(incluirAlteracaoAmbienteEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContrato(PgitUtil
				.verificaLongNulo(incluirAlteracaoAmbienteEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setCdServico(PgitUtil
				.verificaIntegerNulo(incluirAlteracaoAmbienteEntradaDTO
						.getCdServico()));
		request.setCdModalidade(PgitUtil
				.verificaIntegerNulo(incluirAlteracaoAmbienteEntradaDTO
						.getCdModalidade()));
		request.setCdRelacionamentoProduto(PgitUtil
				.verificaIntegerNulo(incluirAlteracaoAmbienteEntradaDTO
						.getCdRelacionamentoProduto()));
		request.setCdFormaMudanca(PgitUtil
				.verificaIntegerNulo(incluirAlteracaoAmbienteEntradaDTO
						.getCdFormaMudanca()));
		request.setDtMudancao(PgitUtil
				.verificaStringNula(incluirAlteracaoAmbienteEntradaDTO
						.getDtMudanca()));
		request.setCdIndicadorArmazenamentoInformacao(PgitUtil
				.verificaIntegerNulo(incluirAlteracaoAmbienteEntradaDTO
						.getCdIndicadorArmazenamentoInformacao()));
		request.setCdAmbienteAtivoSolicitacao(PgitUtil
				.verificaStringNula(incluirAlteracaoAmbienteEntradaDTO
						.getCdAmbienteAtivoSolicitacao()));

		response = getFactoryAdapter().getIncluirAlteracaoAmbientePDCAdapter()
				.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#incluirAlteracaoAmbientePartc(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbientePartcEntradaDTO)
	 */
	public IncluirAlteracaoAmbientePartcSaidaDTO incluirAlteracaoAmbientePartc(
			IncluirAlteracaoAmbientePartcEntradaDTO entrada) {

		IncluirAlteracaoAmbientePartcSaidaDTO saidaDTO = new IncluirAlteracaoAmbientePartcSaidaDTO();
		IncluirAlteracaoAmbientePartcRequest request = new IncluirAlteracaoAmbientePartcRequest();
		IncluirAlteracaoAmbientePartcResponse response = new IncluirAlteracaoAmbientePartcResponse();

		request.setCdAmbienteAtivoSolicitacao(PgitUtil
				.verificaStringNula(entrada.getCdAmbienteAtivoSolicitacao()));
		request.setCdFormaMudanca(PgitUtil.verificaIntegerNulo(entrada
				.getCdFormaMudanca()));
		request.setCdIndicadorArmazenamentoInformacao(PgitUtil
				.verificaIntegerNulo(entrada
						.getCdIndicadorArmazenamentoInformacao()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request
				.setCdProdutoOperacaoRelacionado(PgitUtil
						.verificaIntegerNulo(entrada
								.getCdProdutoOperacaoRelacionado()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(entrada
				.getCdRelacionamentoProduto()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil
				.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
		request.setDtMudancaAmbiente(PgitUtil.verificaStringNula(entrada
				.getDtMudancaAmbiente()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));

		response = getFactoryAdapter()
				.getIncluirAlteracaoAmbientePartcPDCAdapter().invokeProcess(
						request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#excluirAlteracaoAmbiente(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbienteEntradaDTO)
	 */
	public ExcluirAlteracaoAmbienteSaidaDTO excluirAlteracaoAmbiente(
			ExcluirAlteracaoAmbienteEntradaDTO excluirAlteracaoAmbienteEntradaDTO) {

		ExcluirAlteracaoAmbienteSaidaDTO saidaDTO = new ExcluirAlteracaoAmbienteSaidaDTO();
		ExcluirAlteracaoAmbienteRequest request = new ExcluirAlteracaoAmbienteRequest();
		ExcluirAlteracaoAmbienteResponse response = new ExcluirAlteracaoAmbienteResponse();

		request
				.setCdSolicitacaoPagamentoIntegrado(excluirAlteracaoAmbienteEntradaDTO
						.getCdSolicitacaoPagamentoIntegrado());
		request
				.setNrSolicitacaoPagamentoIntegrado(excluirAlteracaoAmbienteEntradaDTO
						.getNrSolicitacaoPagamentoIntegrado());
		request.setCdpessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(excluirAlteracaoAmbienteEntradaDTO
						.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(excluirAlteracaoAmbienteEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(excluirAlteracaoAmbienteEntradaDTO
						.getNrSequenciaContratoNegocio()));

		response = getFactoryAdapter().getExcluirAlteracaoAmbientePDCAdapter()
				.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#consultarSolicitacaoMudancaAmbPartc(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbPartcEntradaDTO)
	 */
	public List<ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO> consultarSolicitacaoMudancaAmbPartc(
			ConsultarSolicitacaoMudancaAmbPartcEntradaDTO entradaDTO) {
		ConsultarSolicitacaoMudancaAmbPartcRequest request = new ConsultarSolicitacaoMudancaAmbPartcRequest();
		List<ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO> saida = new ArrayList<ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO>();

		request
				.setCdPessoa(PgitUtil
						.verificaLongNulo(entradaDTO.getCdPessoa()));
		request.setCdpessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoParticipacaoPessoa()));
		request.setDtFimSolicitacao(PgitUtil.verificaStringNula(entradaDTO
				.getDtFimSolicitacao()));
		request.setDtInicioSolicitacao(PgitUtil.verificaStringNula(entradaDTO
				.getDtInicioSolicitacao()));
		request.setNrOcorrencias(PgitUtil.verificaIntegerNulo(entradaDTO
				.getNrOcorrencias()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setDsAmbiente(entradaDTO.getDsAmbiente());

		ConsultarSolicitacaoMudancaAmbPartcResponse response = getFactoryAdapter()
				.getConsultarSolicitacaoMudancaAmbPartcPDCAdapter()
				.invokeProcess(request);

		for (Ocorrencias occur : response.getOcorrencias()) {
			ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO item = new ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO();
			item.setCdAmbienteAtivoSolicitacao(occur
					.getCdAmbienteAtivoSolicitacao());
			item.setCdCnpjParticipante(occur.getCdCnpjParticipante());
			item.setCdControleCnpjParticipante(occur
					.getCdControleCnpjParticipante());
			item.setCdFilialParticipante(occur.getCdFilialParticipante());
			item.setCdPessoa(occur.getCdPessoa());
			item.setCdProdutoServicoOperacao(occur
					.getCdProdutoServicoOperacao());
			item.setCdProdutoServicoRelacionado(occur
					.getCdProdutoServicoRelacionado());
			item.setCdSituacaoSolicitacaoPagamento(occur
					.getCdSituacaoSolicitacaoPagamento());
			item.setCdSolicitacao(occur.getCdSolicitacao());
			item.setCdTipoParticipacaoPessoa(occur
					.getCdTipoParticipacaoPessoa());
			item.setDsNomeRazaoParticipante(occur.getDsNomeRazaoParticipante());
			item.setDsProdutoServicoOperacao(occur
					.getDsProdutoServicoOperacao());
			item.setDsProdutoServicoRelacionado(occur
					.getDsProdutoServicoRelacionado());
			item.setDsSituacaoSolicitacao(occur.getDsSituacaoSolicitacao());
			item.setDtProgramadaMudancaAmbiente(FormatarData
					.formataDiaMesAnoFromPdc(occur
							.getDtProgramadaMudancaAmbiente()));
			item.setDtSolicitacao(occur.getDtSolicitacao());
			item.setNrSolicitacao(occur.getNrSolicitacao());
			saida.add(item);
		}

		return saida;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#listarAmbiente(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteEntradaDTO)
	 */
	public List<ListarAmbienteOcorrenciaSaidaDTO> listarAmbiente(ListarAmbienteEntradaDTO entrada) {
		ListarAmbienteRequest request = new ListarAmbienteRequest();
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
		request.setCdProdutoOperacaoRelacionamento(PgitUtil.verificaIntegerNulo(entrada
						.getCdProdutoOperacaoRelacionamento()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
		request.setMaxOcorrencias(50);
		request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));
		request.setDsAmbiente(obterAmbiente(entrada.getDsAmbiente()));

		ListarAmbienteResponse response = getFactoryAdapter()
				.getListarAmbientePDCAdapter().invokeProcess(request);

		List<ListarAmbienteOcorrenciaSaidaDTO> ambientes = new ArrayList<ListarAmbienteOcorrenciaSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarambiente.response.Ocorrencias item : response
				.getOcorrencias()) {
			ListarAmbienteOcorrenciaSaidaDTO ocorrencia = new ListarAmbienteOcorrenciaSaidaDTO();
			ocorrencia.setCdControleCpfCnpj(item.getCdControleCpfCnpj());
			ocorrencia.setCdCorpoCpfCnpj(item.getCdCorpoCpfCnpj());
			ocorrencia.setCdFilialCnpj(item.getCdFilialCnpj());
			ocorrencia.setCdPessoa(item.getCdPessoa());
			ocorrencia.setCdProdutoOperacaoRelacionado(item.getCdProdutoOperacaoRelacionado());
			ocorrencia.setCdProdutoServicoOperacao(item.getCdProdutoServicoOperacao());
			ocorrencia.setCdRelacionamentoProdutoProduto(item.getCdRelacionamentoProdutoProduto());
			ocorrencia.setCdTipoParticipacaoPessoa(item.getCdTipoParticipacaoPessoa());
			ocorrencia.setCdTipoServico(item.getCdTipoServico());
			ocorrencia.setDsAmbienteOperacao(item.getDsAmbienteOperacao());
			ocorrencia.setDsNomeRazaoSocial(item.getDsNomeRazaoSocial());
			ocorrencia.setDsTipoServico(item.getDsTipoServico());
			ambientes.add(ocorrencia);
		}
		return ambientes;
	}

	/**
	 * Obter ambiente.
	 *
	 * @param ambienteParam the ds ambiente
	 * @return the string
	 */
	private String obterAmbiente(String ambienteParam) {
		String ambiente = PgitUtil.verificaStringNula(ambienteParam);
		if (ambiente.length() > 4) {
			ambiente = ambiente.substring(0, 4);
		}
		return ambiente;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#excluirAlteracaoAmbientePartc(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbientePartcEntradaDTO)
	 */
	public ExcluirAlteracaoAmbientePartcSaidaDTO excluirAlteracaoAmbientePartc(
			ExcluirAlteracaoAmbientePartcEntradaDTO entrada) {

		ExcluirAlteracaoAmbientePartcSaidaDTO saida = new ExcluirAlteracaoAmbientePartcSaidaDTO();
		ExcluirAlteracaoAmbientePartcRequest request = new ExcluirAlteracaoAmbientePartcRequest();

		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entrada
						.getCdSolicitacaoPagamentoIntegrado()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNrSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entrada
						.getNrSolicitacaoPagamentoIntegrado()));

		ExcluirAlteracaoAmbientePartcResponse response = getFactoryAdapter()
				.getExcluirAlteracaoAmbientePartcPDCAdapter().invokeProcess(
						request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService#detalharAmbiente(br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.DetalharAmbienteEntradaDTO)
	 */
	public DetalharAmbienteSaidaDTO detalharAmbiente(
			DetalharAmbienteEntradaDTO entrada) {
		DetalharAmbienteRequest request = new DetalharAmbienteRequest();

		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaJuridica()));
		request.setCdProdutoOperacaoRelacionamento(PgitUtil
				.verificaIntegerNulo(entrada
						.getCdProdutoOperacaoRelacionamento()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdRelacionamentoProdutoProduto(PgitUtil
				.verificaIntegerNulo(entrada
						.getCdRelacionamentoProdutoProduto()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContrato()));
		request.setCdTipoParticipacaoPessoa(PgitUtil
				.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
		request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContrato()));

		DetalharAmbienteResponse response = getFactoryAdapter()
				.getDetalharAmbientePDCAdapter().invokeProcess(request);

		DetalharAmbienteSaidaDTO saida = new DetalharAmbienteSaidaDTO();

		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setHrManutencaoRegistroManuntencao(response
				.getHrManutencaoRegistroManutencao());
		saida.setHrManutencaRegistroInclusao(response
				.getHrManutencaRegistroInclusao());

		saida.setMensagem(response.getMensagem());
		saida.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
		saida.setNmOperacaoFluxoManutencao(response
				.getNmOperacaoFluxoManutencao());

		return saida;

	}

	public List<ConsultarContratoOcorrenciasDTO> consultarContratos(
			ConsultarContratoEntradaDTO entrada) {

		List<ConsultarContratoOcorrenciasDTO> ocorrencias = new ArrayList<ConsultarContratoOcorrenciasDTO>();
		ConsultarContratoRequest request = new ConsultarContratoRequest();
		ConsultarContratoResponse response = null;

		request.setNrOcorrencias(QTDE_OCORRENCIAS);
		request.setCdClubPessoa(verificaLongNulo(entrada.getCdClubPessoa()));
		request.setCdPessoaJuridica(verificaLongNulo(entrada
				.getCdPessoaJuridica()));
		request.setCdTipoContrato(verificaIntegerNulo(entrada
				.getCdTipoContrato()));
		request.setNrSequenciaContrato(verificaLongNulo(entrada
				.getNrSequenciaContrato()));
		request.setCdAgencia(verificaIntegerNulo(entrada.getCdAgencia()));
		request.setCdFuncionarioBradesco(verificaLongNulo(entrada
				.getCdFuncionarioBradesco()));
		request.setCdSituacaoContrato(verificaIntegerNulo(entrada
				.getCdSituacaoContrato()));
		request.setCdParametroPrograma(PARAMETRO_PROGRAMA);
		request
				.setCdCpfCnpjPssoa(verificaLongNulo(entrada.getCdCpfCnpjPssoa()));
		request.setCdFilialCnpjPssoa(verificaIntegerNulo(entrada
				.getCdFilialCnpjPssoa()));
		request.setCdControleCpfPssoa(verificaIntegerNulo(entrada
				.getCdControleCpfPssoa()));
		request.setDsAmbiente(entrada.getDsAmbiente());

		response = factoryAdapter.getConsultarContratoPDCAdapter()
				.invokeProcess(request);

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ConsultarContratoOcorrenciasDTO ocorrencia = new ConsultarContratoOcorrenciasDTO();

			ocorrencia.setCdClubRepresentante(response.getOcorrencias(i)
					.getCdClubRepresentante());
			ocorrencia.setCdCorpoCnpjRepresentante(response.getOcorrencias(i)
					.getCdCorpoCnpjRepresentante());
			ocorrencia.setCdFilialCnpjRepresentante(response.getOcorrencias(i)
					.getCdFilialCnpjRepresentante());
			ocorrencia.setCdControleCnpjRepresentante(response
					.getOcorrencias(i).getCdControleCnpjRepresentante());
			ocorrencia.setNmRazaoRepresentante(response.getOcorrencias(i)
					.getNmRazaoRepresentante());
			ocorrencia.setCdPessoaJuridica(response.getOcorrencias(i)
					.getCdPessoaJuridica());
			ocorrencia.setDsPessoaJuridica(response.getOcorrencias(i)
					.getDsPessoaJuridica());
			ocorrencia.setCdTipoContrato(response.getOcorrencias(i)
					.getCdTipoContrato());
			ocorrencia.setDsTipoContrato(response.getOcorrencias(i)
					.getDsTipoContrato());
			ocorrencia.setNrSequenciaContrato(response.getOcorrencias(i)
					.getNrSequenciaContrato());
			ocorrencia
					.setDsContrato(response.getOcorrencias(i).getDsContrato());
			ocorrencia.setCdAgenciaOperadora(response.getOcorrencias(i)
					.getCdAgenciaOperadora());
			ocorrencia.setCdDigitoAgenciaOperadora(response.getOcorrencias(i)
					.getCdDigitoAgenciaOperadora());
			ocorrencia.setDsAgenciaOperadora(response.getOcorrencias(i)
					.getDsAgenciaOperadora());
			ocorrencia.setCdFuncionarioBradesco(response.getOcorrencias(i)
					.getCdFuncionarioBradesco());
			ocorrencia.setDsFuncionarioBradesco(response.getOcorrencias(i)
					.getDsFuncionarioBradesco());
			ocorrencia.setCdSituacaoContrato(response.getOcorrencias(i)
					.getCdSituacaoContrato());
			ocorrencia.setDsSituacaoContrato(response.getOcorrencias(i)
					.getDsSituacaoContrato());
			ocorrencia.setCdMotivoSituacao(response.getOcorrencias(i)
					.getCdMotivoSituacao());
			ocorrencia.setDsMotivoSituacao(response.getOcorrencias(i)
					.getDsMotivoSituacao());
			ocorrencia.setCdAditivo(response.getOcorrencias(i).getCdAditivo());
			ocorrencia.setCdTipoParticipacao(response.getOcorrencias(i)
					.getCdTipoParticipacao());
			ocorrencia.setCdSegmentoCliente(response.getOcorrencias(i)
					.getCdSegmentoCliente());
			ocorrencia.setDsSegmentoCliente(response.getOcorrencias(i)
					.getDsSegmentoCliente());
			ocorrencia.setCdSubSegmentoCliente(response.getOcorrencias(i)
					.getCdSubSegmentoCliente());
			ocorrencia.setDsSubSegmentoCliente(response.getOcorrencias(i)
					.getDsSubSegmentoCliente());
			ocorrencia.setCdAtividadeEconomica(response.getOcorrencias(i)
					.getCdAtividadeEconomica());
			ocorrencia.setDsAtividadeEconomica(response.getOcorrencias(i)
					.getDsAtividadeEconomica());
			ocorrencia.setCdGrupoEconomico(response.getOcorrencias(i)
					.getCdGrupoEconomico());
			ocorrencia.setDsGrupoEconomico(response.getOcorrencias(i)
					.getDsGrupoEconomico());
			ocorrencia
					.setDtAbertura(response.getOcorrencias(i).getDtAbertura());
			ocorrencia.setDtEncerramento(response.getOcorrencias(i)
					.getDtEncerramento());
			ocorrencia
					.setDtInclusao(response.getOcorrencias(i).getDtInclusao());

			ocorrencias.add(ocorrencia);

		}

		return ocorrencias;
	}

}
