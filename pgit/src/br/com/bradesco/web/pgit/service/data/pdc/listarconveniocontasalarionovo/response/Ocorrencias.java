/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codConvenio
     */
    private long _codConvenio = 0;

    /**
     * keeps track of state for field: _codConvenio
     */
    private boolean _has_codConvenio;

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filial
     */
    private int _filial = 0;

    /**
     * keeps track of state for field: _filial
     */
    private boolean _has_filial;

    /**
     * Field _controle
     */
    private int _controle = 0;

    /**
     * keeps track of state for field: _controle
     */
    private boolean _has_controle;

    /**
     * Field _banco
     */
    private int _banco = 0;

    /**
     * keeps track of state for field: _banco
     */
    private boolean _has_banco;

    /**
     * Field _agencia
     */
    private int _agencia = 0;

    /**
     * keeps track of state for field: _agencia
     */
    private boolean _has_agencia;

    /**
     * Field _digitoAgencia
     */
    private java.lang.String _digitoAgencia;

    /**
     * Field _conta
     */
    private long _conta = 0;

    /**
     * keeps track of state for field: _conta
     */
    private boolean _has_conta;

    /**
     * Field _digitoConta
     */
    private java.lang.String _digitoConta;

    /**
     * Field _codSituacaoConvenio
     */
    private int _codSituacaoConvenio = 0;

    /**
     * keeps track of state for field: _codSituacaoConvenio
     */
    private boolean _has_codSituacaoConvenio;

    /**
     * Field _descSituacaoConvenio
     */
    private java.lang.String _descSituacaoConvenio;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteAgencia
     * 
     */
    public void deleteAgencia()
    {
        this._has_agencia= false;
    } //-- void deleteAgencia() 

    /**
     * Method deleteBanco
     * 
     */
    public void deleteBanco()
    {
        this._has_banco= false;
    } //-- void deleteBanco() 

    /**
     * Method deleteCodConvenio
     * 
     */
    public void deleteCodConvenio()
    {
        this._has_codConvenio= false;
    } //-- void deleteCodConvenio() 

    /**
     * Method deleteCodSituacaoConvenio
     * 
     */
    public void deleteCodSituacaoConvenio()
    {
        this._has_codSituacaoConvenio= false;
    } //-- void deleteCodSituacaoConvenio() 

    /**
     * Method deleteConta
     * 
     */
    public void deleteConta()
    {
        this._has_conta= false;
    } //-- void deleteConta() 

    /**
     * Method deleteControle
     * 
     */
    public void deleteControle()
    {
        this._has_controle= false;
    } //-- void deleteControle() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilial
     * 
     */
    public void deleteFilial()
    {
        this._has_filial= false;
    } //-- void deleteFilial() 

    /**
     * Returns the value of field 'agencia'.
     * 
     * @return int
     * @return the value of field 'agencia'.
     */
    public int getAgencia()
    {
        return this._agencia;
    } //-- int getAgencia() 

    /**
     * Returns the value of field 'banco'.
     * 
     * @return int
     * @return the value of field 'banco'.
     */
    public int getBanco()
    {
        return this._banco;
    } //-- int getBanco() 

    /**
     * Returns the value of field 'codConvenio'.
     * 
     * @return long
     * @return the value of field 'codConvenio'.
     */
    public long getCodConvenio()
    {
        return this._codConvenio;
    } //-- long getCodConvenio() 

    /**
     * Returns the value of field 'codSituacaoConvenio'.
     * 
     * @return int
     * @return the value of field 'codSituacaoConvenio'.
     */
    public int getCodSituacaoConvenio()
    {
        return this._codSituacaoConvenio;
    } //-- int getCodSituacaoConvenio() 

    /**
     * Returns the value of field 'conta'.
     * 
     * @return long
     * @return the value of field 'conta'.
     */
    public long getConta()
    {
        return this._conta;
    } //-- long getConta() 

    /**
     * Returns the value of field 'controle'.
     * 
     * @return int
     * @return the value of field 'controle'.
     */
    public int getControle()
    {
        return this._controle;
    } //-- int getControle() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'descSituacaoConvenio'.
     * 
     * @return String
     * @return the value of field 'descSituacaoConvenio'.
     */
    public java.lang.String getDescSituacaoConvenio()
    {
        return this._descSituacaoConvenio;
    } //-- java.lang.String getDescSituacaoConvenio() 

    /**
     * Returns the value of field 'digitoAgencia'.
     * 
     * @return String
     * @return the value of field 'digitoAgencia'.
     */
    public java.lang.String getDigitoAgencia()
    {
        return this._digitoAgencia;
    } //-- java.lang.String getDigitoAgencia() 

    /**
     * Returns the value of field 'digitoConta'.
     * 
     * @return String
     * @return the value of field 'digitoConta'.
     */
    public java.lang.String getDigitoConta()
    {
        return this._digitoConta;
    } //-- java.lang.String getDigitoConta() 

    /**
     * Returns the value of field 'filial'.
     * 
     * @return int
     * @return the value of field 'filial'.
     */
    public int getFilial()
    {
        return this._filial;
    } //-- int getFilial() 

    /**
     * Method hasAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasAgencia()
    {
        return this._has_agencia;
    } //-- boolean hasAgencia() 

    /**
     * Method hasBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasBanco()
    {
        return this._has_banco;
    } //-- boolean hasBanco() 

    /**
     * Method hasCodConvenio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodConvenio()
    {
        return this._has_codConvenio;
    } //-- boolean hasCodConvenio() 

    /**
     * Method hasCodSituacaoConvenio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodSituacaoConvenio()
    {
        return this._has_codSituacaoConvenio;
    } //-- boolean hasCodSituacaoConvenio() 

    /**
     * Method hasConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasConta()
    {
        return this._has_conta;
    } //-- boolean hasConta() 

    /**
     * Method hasControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControle()
    {
        return this._has_controle;
    } //-- boolean hasControle() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilial()
    {
        return this._has_filial;
    } //-- boolean hasFilial() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'agencia'.
     * 
     * @param agencia the value of field 'agencia'.
     */
    public void setAgencia(int agencia)
    {
        this._agencia = agencia;
        this._has_agencia = true;
    } //-- void setAgencia(int) 

    /**
     * Sets the value of field 'banco'.
     * 
     * @param banco the value of field 'banco'.
     */
    public void setBanco(int banco)
    {
        this._banco = banco;
        this._has_banco = true;
    } //-- void setBanco(int) 

    /**
     * Sets the value of field 'codConvenio'.
     * 
     * @param codConvenio the value of field 'codConvenio'.
     */
    public void setCodConvenio(long codConvenio)
    {
        this._codConvenio = codConvenio;
        this._has_codConvenio = true;
    } //-- void setCodConvenio(long) 

    /**
     * Sets the value of field 'codSituacaoConvenio'.
     * 
     * @param codSituacaoConvenio the value of field
     * 'codSituacaoConvenio'.
     */
    public void setCodSituacaoConvenio(int codSituacaoConvenio)
    {
        this._codSituacaoConvenio = codSituacaoConvenio;
        this._has_codSituacaoConvenio = true;
    } //-- void setCodSituacaoConvenio(int) 

    /**
     * Sets the value of field 'conta'.
     * 
     * @param conta the value of field 'conta'.
     */
    public void setConta(long conta)
    {
        this._conta = conta;
        this._has_conta = true;
    } //-- void setConta(long) 

    /**
     * Sets the value of field 'controle'.
     * 
     * @param controle the value of field 'controle'.
     */
    public void setControle(int controle)
    {
        this._controle = controle;
        this._has_controle = true;
    } //-- void setControle(int) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'descSituacaoConvenio'.
     * 
     * @param descSituacaoConvenio the value of field
     * 'descSituacaoConvenio'.
     */
    public void setDescSituacaoConvenio(java.lang.String descSituacaoConvenio)
    {
        this._descSituacaoConvenio = descSituacaoConvenio;
    } //-- void setDescSituacaoConvenio(java.lang.String) 

    /**
     * Sets the value of field 'digitoAgencia'.
     * 
     * @param digitoAgencia the value of field 'digitoAgencia'.
     */
    public void setDigitoAgencia(java.lang.String digitoAgencia)
    {
        this._digitoAgencia = digitoAgencia;
    } //-- void setDigitoAgencia(java.lang.String) 

    /**
     * Sets the value of field 'digitoConta'.
     * 
     * @param digitoConta the value of field 'digitoConta'.
     */
    public void setDigitoConta(java.lang.String digitoConta)
    {
        this._digitoConta = digitoConta;
    } //-- void setDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'filial'.
     * 
     * @param filial the value of field 'filial'.
     */
    public void setFilial(int filial)
    {
        this._filial = filial;
        this._has_filial = true;
    } //-- void setFilial(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
