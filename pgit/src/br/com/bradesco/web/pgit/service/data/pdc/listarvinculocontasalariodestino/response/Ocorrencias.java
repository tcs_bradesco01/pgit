/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridVinc
     */
    private long _cdPessoaJuridVinc = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridVinc
     */
    private boolean _has_cdPessoaJuridVinc;

    /**
     * Field _cdTipoContratoVinc
     */
    private int _cdTipoContratoVinc = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinc
     */
    private boolean _has_cdTipoContratoVinc;

    /**
     * Field _nrSeqContratoVinc
     */
    private long _nrSeqContratoVinc = 0;

    /**
     * keeps track of state for field: _nrSeqContratoVinc
     */
    private boolean _has_nrSeqContratoVinc;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _cdBancoSalario
     */
    private int _cdBancoSalario = 0;

    /**
     * keeps track of state for field: _cdBancoSalario
     */
    private boolean _has_cdBancoSalario;

    /**
     * Field _dsBancoSalario
     */
    private java.lang.String _dsBancoSalario;

    /**
     * Field _cdAgenciaSalario
     */
    private int _cdAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdAgenciaSalario
     */
    private boolean _has_cdAgenciaSalario;

    /**
     * Field _cdDigitoAgenciaSalario
     */
    private int _cdDigitoAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaSalario
     */
    private boolean _has_cdDigitoAgenciaSalario;

    /**
     * Field _dsAgenciaSalario
     */
    private java.lang.String _dsAgenciaSalario;

    /**
     * Field _cdContaSalario
     */
    private long _cdContaSalario = 0;

    /**
     * keeps track of state for field: _cdContaSalario
     */
    private boolean _has_cdContaSalario;

    /**
     * Field _cdDigitoContaSalario
     */
    private java.lang.String _cdDigitoContaSalario;

    /**
     * Field _cdBancoDestino
     */
    private int _cdBancoDestino = 0;

    /**
     * keeps track of state for field: _cdBancoDestino
     */
    private boolean _has_cdBancoDestino;

    /**
     * Field _dsBancoDestino
     */
    private java.lang.String _dsBancoDestino;

    /**
     * Field _cdAgenciaDestino
     */
    private int _cdAgenciaDestino = 0;

    /**
     * keeps track of state for field: _cdAgenciaDestino
     */
    private boolean _has_cdAgenciaDestino;

    /**
     * Field _cdDigitoAgenciaDestino
     */
    private int _cdDigitoAgenciaDestino = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaDestino
     */
    private boolean _has_cdDigitoAgenciaDestino;

    /**
     * Field _dsAgenciaDestino
     */
    private java.lang.String _dsAgenciaDestino;

    /**
     * Field _cdContaDestino
     */
    private long _cdContaDestino = 0;

    /**
     * keeps track of state for field: _cdContaDestino
     */
    private boolean _has_cdContaDestino;

    /**
     * Field _cdDigitoContaDestino
     */
    private java.lang.String _cdDigitoContaDestino;

    /**
     * Field _cdTipoContaDestino
     */
    private java.lang.String _cdTipoContaDestino;

    /**
     * Field _dsSituacaoContaDestino
     */
    private java.lang.String _dsSituacaoContaDestino;

    /**
     * Field _cdContingenciaTed
     */
    private java.lang.String _cdContingenciaTed;

    /**
     * Field _nmParticipante
     */
    private java.lang.String _nmParticipante;

    /**
     * Field _cdCpfParticipante
     */
    private java.lang.String _cdCpfParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDestino
     * 
     */
    public void deleteCdAgenciaDestino()
    {
        this._has_cdAgenciaDestino= false;
    } //-- void deleteCdAgenciaDestino() 

    /**
     * Method deleteCdAgenciaSalario
     * 
     */
    public void deleteCdAgenciaSalario()
    {
        this._has_cdAgenciaSalario= false;
    } //-- void deleteCdAgenciaSalario() 

    /**
     * Method deleteCdBancoDestino
     * 
     */
    public void deleteCdBancoDestino()
    {
        this._has_cdBancoDestino= false;
    } //-- void deleteCdBancoDestino() 

    /**
     * Method deleteCdBancoSalario
     * 
     */
    public void deleteCdBancoSalario()
    {
        this._has_cdBancoSalario= false;
    } //-- void deleteCdBancoSalario() 

    /**
     * Method deleteCdContaDestino
     * 
     */
    public void deleteCdContaDestino()
    {
        this._has_cdContaDestino= false;
    } //-- void deleteCdContaDestino() 

    /**
     * Method deleteCdContaSalario
     * 
     */
    public void deleteCdContaSalario()
    {
        this._has_cdContaSalario= false;
    } //-- void deleteCdContaSalario() 

    /**
     * Method deleteCdDigitoAgenciaDestino
     * 
     */
    public void deleteCdDigitoAgenciaDestino()
    {
        this._has_cdDigitoAgenciaDestino= false;
    } //-- void deleteCdDigitoAgenciaDestino() 

    /**
     * Method deleteCdDigitoAgenciaSalario
     * 
     */
    public void deleteCdDigitoAgenciaSalario()
    {
        this._has_cdDigitoAgenciaSalario= false;
    } //-- void deleteCdDigitoAgenciaSalario() 

    /**
     * Method deleteCdPessoaJuridVinc
     * 
     */
    public void deleteCdPessoaJuridVinc()
    {
        this._has_cdPessoaJuridVinc= false;
    } //-- void deleteCdPessoaJuridVinc() 

    /**
     * Method deleteCdTipoContratoVinc
     * 
     */
    public void deleteCdTipoContratoVinc()
    {
        this._has_cdTipoContratoVinc= false;
    } //-- void deleteCdTipoContratoVinc() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSeqContratoVinc
     * 
     */
    public void deleteNrSeqContratoVinc()
    {
        this._has_nrSeqContratoVinc= false;
    } //-- void deleteNrSeqContratoVinc() 

    /**
     * Returns the value of field 'cdAgenciaDestino'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDestino'.
     */
    public int getCdAgenciaDestino()
    {
        return this._cdAgenciaDestino;
    } //-- int getCdAgenciaDestino() 

    /**
     * Returns the value of field 'cdAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaSalario'.
     */
    public int getCdAgenciaSalario()
    {
        return this._cdAgenciaSalario;
    } //-- int getCdAgenciaSalario() 

    /**
     * Returns the value of field 'cdBancoDestino'.
     * 
     * @return int
     * @return the value of field 'cdBancoDestino'.
     */
    public int getCdBancoDestino()
    {
        return this._cdBancoDestino;
    } //-- int getCdBancoDestino() 

    /**
     * Returns the value of field 'cdBancoSalario'.
     * 
     * @return int
     * @return the value of field 'cdBancoSalario'.
     */
    public int getCdBancoSalario()
    {
        return this._cdBancoSalario;
    } //-- int getCdBancoSalario() 

    /**
     * Returns the value of field 'cdContaDestino'.
     * 
     * @return long
     * @return the value of field 'cdContaDestino'.
     */
    public long getCdContaDestino()
    {
        return this._cdContaDestino;
    } //-- long getCdContaDestino() 

    /**
     * Returns the value of field 'cdContaSalario'.
     * 
     * @return long
     * @return the value of field 'cdContaSalario'.
     */
    public long getCdContaSalario()
    {
        return this._cdContaSalario;
    } //-- long getCdContaSalario() 

    /**
     * Returns the value of field 'cdContingenciaTed'.
     * 
     * @return String
     * @return the value of field 'cdContingenciaTed'.
     */
    public java.lang.String getCdContingenciaTed()
    {
        return this._cdContingenciaTed;
    } //-- java.lang.String getCdContingenciaTed() 

    /**
     * Returns the value of field 'cdCpfParticipante'.
     * 
     * @return String
     * @return the value of field 'cdCpfParticipante'.
     */
    public java.lang.String getCdCpfParticipante()
    {
        return this._cdCpfParticipante;
    } //-- java.lang.String getCdCpfParticipante() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDestino'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaDestino'.
     */
    public int getCdDigitoAgenciaDestino()
    {
        return this._cdDigitoAgenciaDestino;
    } //-- int getCdDigitoAgenciaDestino() 

    /**
     * Returns the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaSalario'.
     */
    public int getCdDigitoAgenciaSalario()
    {
        return this._cdDigitoAgenciaSalario;
    } //-- int getCdDigitoAgenciaSalario() 

    /**
     * Returns the value of field 'cdDigitoContaDestino'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDestino'.
     */
    public java.lang.String getCdDigitoContaDestino()
    {
        return this._cdDigitoContaDestino;
    } //-- java.lang.String getCdDigitoContaDestino() 

    /**
     * Returns the value of field 'cdDigitoContaSalario'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaSalario'.
     */
    public java.lang.String getCdDigitoContaSalario()
    {
        return this._cdDigitoContaSalario;
    } //-- java.lang.String getCdDigitoContaSalario() 

    /**
     * Returns the value of field 'cdPessoaJuridVinc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridVinc'.
     */
    public long getCdPessoaJuridVinc()
    {
        return this._cdPessoaJuridVinc;
    } //-- long getCdPessoaJuridVinc() 

    /**
     * Returns the value of field 'cdTipoContaDestino'.
     * 
     * @return String
     * @return the value of field 'cdTipoContaDestino'.
     */
    public java.lang.String getCdTipoContaDestino()
    {
        return this._cdTipoContaDestino;
    } //-- java.lang.String getCdTipoContaDestino() 

    /**
     * Returns the value of field 'cdTipoContratoVinc'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinc'.
     */
    public int getCdTipoContratoVinc()
    {
        return this._cdTipoContratoVinc;
    } //-- int getCdTipoContratoVinc() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'dsAgenciaDestino'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDestino'.
     */
    public java.lang.String getDsAgenciaDestino()
    {
        return this._dsAgenciaDestino;
    } //-- java.lang.String getDsAgenciaDestino() 

    /**
     * Returns the value of field 'dsAgenciaSalario'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaSalario'.
     */
    public java.lang.String getDsAgenciaSalario()
    {
        return this._dsAgenciaSalario;
    } //-- java.lang.String getDsAgenciaSalario() 

    /**
     * Returns the value of field 'dsBancoDestino'.
     * 
     * @return String
     * @return the value of field 'dsBancoDestino'.
     */
    public java.lang.String getDsBancoDestino()
    {
        return this._dsBancoDestino;
    } //-- java.lang.String getDsBancoDestino() 

    /**
     * Returns the value of field 'dsBancoSalario'.
     * 
     * @return String
     * @return the value of field 'dsBancoSalario'.
     */
    public java.lang.String getDsBancoSalario()
    {
        return this._dsBancoSalario;
    } //-- java.lang.String getDsBancoSalario() 

    /**
     * Returns the value of field 'dsSituacaoContaDestino'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContaDestino'.
     */
    public java.lang.String getDsSituacaoContaDestino()
    {
        return this._dsSituacaoContaDestino;
    } //-- java.lang.String getDsSituacaoContaDestino() 

    /**
     * Returns the value of field 'nmParticipante'.
     * 
     * @return String
     * @return the value of field 'nmParticipante'.
     */
    public java.lang.String getNmParticipante()
    {
        return this._nmParticipante;
    } //-- java.lang.String getNmParticipante() 

    /**
     * Returns the value of field 'nrSeqContratoVinc'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoVinc'.
     */
    public long getNrSeqContratoVinc()
    {
        return this._nrSeqContratoVinc;
    } //-- long getNrSeqContratoVinc() 

    /**
     * Method hasCdAgenciaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDestino()
    {
        return this._has_cdAgenciaDestino;
    } //-- boolean hasCdAgenciaDestino() 

    /**
     * Method hasCdAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaSalario()
    {
        return this._has_cdAgenciaSalario;
    } //-- boolean hasCdAgenciaSalario() 

    /**
     * Method hasCdBancoDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDestino()
    {
        return this._has_cdBancoDestino;
    } //-- boolean hasCdBancoDestino() 

    /**
     * Method hasCdBancoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoSalario()
    {
        return this._has_cdBancoSalario;
    } //-- boolean hasCdBancoSalario() 

    /**
     * Method hasCdContaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDestino()
    {
        return this._has_cdContaDestino;
    } //-- boolean hasCdContaDestino() 

    /**
     * Method hasCdContaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaSalario()
    {
        return this._has_cdContaSalario;
    } //-- boolean hasCdContaSalario() 

    /**
     * Method hasCdDigitoAgenciaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaDestino()
    {
        return this._has_cdDigitoAgenciaDestino;
    } //-- boolean hasCdDigitoAgenciaDestino() 

    /**
     * Method hasCdDigitoAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaSalario()
    {
        return this._has_cdDigitoAgenciaSalario;
    } //-- boolean hasCdDigitoAgenciaSalario() 

    /**
     * Method hasCdPessoaJuridVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridVinc()
    {
        return this._has_cdPessoaJuridVinc;
    } //-- boolean hasCdPessoaJuridVinc() 

    /**
     * Method hasCdTipoContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinc()
    {
        return this._has_cdTipoContratoVinc;
    } //-- boolean hasCdTipoContratoVinc() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSeqContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoVinc()
    {
        return this._has_nrSeqContratoVinc;
    } //-- boolean hasNrSeqContratoVinc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDestino'.
     * 
     * @param cdAgenciaDestino the value of field 'cdAgenciaDestino'
     */
    public void setCdAgenciaDestino(int cdAgenciaDestino)
    {
        this._cdAgenciaDestino = cdAgenciaDestino;
        this._has_cdAgenciaDestino = true;
    } //-- void setCdAgenciaDestino(int) 

    /**
     * Sets the value of field 'cdAgenciaSalario'.
     * 
     * @param cdAgenciaSalario the value of field 'cdAgenciaSalario'
     */
    public void setCdAgenciaSalario(int cdAgenciaSalario)
    {
        this._cdAgenciaSalario = cdAgenciaSalario;
        this._has_cdAgenciaSalario = true;
    } //-- void setCdAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdBancoDestino'.
     * 
     * @param cdBancoDestino the value of field 'cdBancoDestino'.
     */
    public void setCdBancoDestino(int cdBancoDestino)
    {
        this._cdBancoDestino = cdBancoDestino;
        this._has_cdBancoDestino = true;
    } //-- void setCdBancoDestino(int) 

    /**
     * Sets the value of field 'cdBancoSalario'.
     * 
     * @param cdBancoSalario the value of field 'cdBancoSalario'.
     */
    public void setCdBancoSalario(int cdBancoSalario)
    {
        this._cdBancoSalario = cdBancoSalario;
        this._has_cdBancoSalario = true;
    } //-- void setCdBancoSalario(int) 

    /**
     * Sets the value of field 'cdContaDestino'.
     * 
     * @param cdContaDestino the value of field 'cdContaDestino'.
     */
    public void setCdContaDestino(long cdContaDestino)
    {
        this._cdContaDestino = cdContaDestino;
        this._has_cdContaDestino = true;
    } //-- void setCdContaDestino(long) 

    /**
     * Sets the value of field 'cdContaSalario'.
     * 
     * @param cdContaSalario the value of field 'cdContaSalario'.
     */
    public void setCdContaSalario(long cdContaSalario)
    {
        this._cdContaSalario = cdContaSalario;
        this._has_cdContaSalario = true;
    } //-- void setCdContaSalario(long) 

    /**
     * Sets the value of field 'cdContingenciaTed'.
     * 
     * @param cdContingenciaTed the value of field
     * 'cdContingenciaTed'.
     */
    public void setCdContingenciaTed(java.lang.String cdContingenciaTed)
    {
        this._cdContingenciaTed = cdContingenciaTed;
    } //-- void setCdContingenciaTed(java.lang.String) 

    /**
     * Sets the value of field 'cdCpfParticipante'.
     * 
     * @param cdCpfParticipante the value of field
     * 'cdCpfParticipante'.
     */
    public void setCdCpfParticipante(java.lang.String cdCpfParticipante)
    {
        this._cdCpfParticipante = cdCpfParticipante;
    } //-- void setCdCpfParticipante(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDestino'.
     * 
     * @param cdDigitoAgenciaDestino the value of field
     * 'cdDigitoAgenciaDestino'.
     */
    public void setCdDigitoAgenciaDestino(int cdDigitoAgenciaDestino)
    {
        this._cdDigitoAgenciaDestino = cdDigitoAgenciaDestino;
        this._has_cdDigitoAgenciaDestino = true;
    } //-- void setCdDigitoAgenciaDestino(int) 

    /**
     * Sets the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @param cdDigitoAgenciaSalario the value of field
     * 'cdDigitoAgenciaSalario'.
     */
    public void setCdDigitoAgenciaSalario(int cdDigitoAgenciaSalario)
    {
        this._cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
        this._has_cdDigitoAgenciaSalario = true;
    } //-- void setCdDigitoAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdDigitoContaDestino'.
     * 
     * @param cdDigitoContaDestino the value of field
     * 'cdDigitoContaDestino'.
     */
    public void setCdDigitoContaDestino(java.lang.String cdDigitoContaDestino)
    {
        this._cdDigitoContaDestino = cdDigitoContaDestino;
    } //-- void setCdDigitoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaSalario'.
     * 
     * @param cdDigitoContaSalario the value of field
     * 'cdDigitoContaSalario'.
     */
    public void setCdDigitoContaSalario(java.lang.String cdDigitoContaSalario)
    {
        this._cdDigitoContaSalario = cdDigitoContaSalario;
    } //-- void setCdDigitoContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridVinc'.
     * 
     * @param cdPessoaJuridVinc the value of field
     * 'cdPessoaJuridVinc'.
     */
    public void setCdPessoaJuridVinc(long cdPessoaJuridVinc)
    {
        this._cdPessoaJuridVinc = cdPessoaJuridVinc;
        this._has_cdPessoaJuridVinc = true;
    } //-- void setCdPessoaJuridVinc(long) 

    /**
     * Sets the value of field 'cdTipoContaDestino'.
     * 
     * @param cdTipoContaDestino the value of field
     * 'cdTipoContaDestino'.
     */
    public void setCdTipoContaDestino(java.lang.String cdTipoContaDestino)
    {
        this._cdTipoContaDestino = cdTipoContaDestino;
    } //-- void setCdTipoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoContratoVinc'.
     * 
     * @param cdTipoContratoVinc the value of field
     * 'cdTipoContratoVinc'.
     */
    public void setCdTipoContratoVinc(int cdTipoContratoVinc)
    {
        this._cdTipoContratoVinc = cdTipoContratoVinc;
        this._has_cdTipoContratoVinc = true;
    } //-- void setCdTipoContratoVinc(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'dsAgenciaDestino'.
     * 
     * @param dsAgenciaDestino the value of field 'dsAgenciaDestino'
     */
    public void setDsAgenciaDestino(java.lang.String dsAgenciaDestino)
    {
        this._dsAgenciaDestino = dsAgenciaDestino;
    } //-- void setDsAgenciaDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaSalario'.
     * 
     * @param dsAgenciaSalario the value of field 'dsAgenciaSalario'
     */
    public void setDsAgenciaSalario(java.lang.String dsAgenciaSalario)
    {
        this._dsAgenciaSalario = dsAgenciaSalario;
    } //-- void setDsAgenciaSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDestino'.
     * 
     * @param dsBancoDestino the value of field 'dsBancoDestino'.
     */
    public void setDsBancoDestino(java.lang.String dsBancoDestino)
    {
        this._dsBancoDestino = dsBancoDestino;
    } //-- void setDsBancoDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoSalario'.
     * 
     * @param dsBancoSalario the value of field 'dsBancoSalario'.
     */
    public void setDsBancoSalario(java.lang.String dsBancoSalario)
    {
        this._dsBancoSalario = dsBancoSalario;
    } //-- void setDsBancoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContaDestino'.
     * 
     * @param dsSituacaoContaDestino the value of field
     * 'dsSituacaoContaDestino'.
     */
    public void setDsSituacaoContaDestino(java.lang.String dsSituacaoContaDestino)
    {
        this._dsSituacaoContaDestino = dsSituacaoContaDestino;
    } //-- void setDsSituacaoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'nmParticipante'.
     * 
     * @param nmParticipante the value of field 'nmParticipante'.
     */
    public void setNmParticipante(java.lang.String nmParticipante)
    {
        this._nmParticipante = nmParticipante;
    } //-- void setNmParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqContratoVinc'.
     * 
     * @param nrSeqContratoVinc the value of field
     * 'nrSeqContratoVinc'.
     */
    public void setNrSeqContratoVinc(long nrSeqContratoVinc)
    {
        this._nrSeqContratoVinc = nrSeqContratoVinc;
        this._has_nrSeqContratoVinc = true;
    } //-- void setNrSeqContratoVinc(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
