/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.io.Serializable;

/**
 * Nome: ListarDependenciaEmpresaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDependenciaEmpresaEntradaDTO implements Serializable {
	
	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -2995237772025257574L;
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
    
    /** Atributo cdTipoPessoaJuridica. */
    private Long cdTipoPessoaJuridica;
    
    /** Atributo cdTipoUnidadeOrganizacional. */
    private Integer cdTipoUnidadeOrganizacional;
    
    /**
     * Listar dependencia empresa entrada dto.
     */
    public ListarDependenciaEmpresaEntradaDTO() {
    	
    }

	/**
	 * Listar dependencia empresa entrada dto.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 * @param cdTipoPessoaJuridica the cd tipo pessoa juridica
	 * @param cdTipoUnidadeOrganizacional the cd tipo unidade organizacional
	 */
	public ListarDependenciaEmpresaEntradaDTO(Integer numeroOcorrencias, Long cdTipoPessoaJuridica, Integer cdTipoUnidadeOrganizacional) {
		super();
		this.numeroOcorrencias = numeroOcorrencias;
		this.cdTipoPessoaJuridica = cdTipoPessoaJuridica;
		this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
	}

	/**
	 * Get: cdTipoPessoaJuridica.
	 *
	 * @return cdTipoPessoaJuridica
	 */
	public Long getCdTipoPessoaJuridica() {
		return cdTipoPessoaJuridica;
	}

	/**
	 * Set: cdTipoPessoaJuridica.
	 *
	 * @param cdTipoPessoaJuridica the cd tipo pessoa juridica
	 */
	public void setCdTipoPessoaJuridica(Long cdTipoPessoaJuridica) {
		this.cdTipoPessoaJuridica = cdTipoPessoaJuridica;
	}

	/**
	 * Get: cdTipoUnidadeOrganizacional.
	 *
	 * @return cdTipoUnidadeOrganizacional
	 */
	public Integer getCdTipoUnidadeOrganizacional() {
		return cdTipoUnidadeOrganizacional;
	}

	/**
	 * Set: cdTipoUnidadeOrganizacional.
	 *
	 * @param cdTipoUnidadeOrganizacional the cd tipo unidade organizacional
	 */
	public void setCdTipoUnidadeOrganizacional(Integer cdTipoUnidadeOrganizacional) {
		this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
	}

	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	

}
