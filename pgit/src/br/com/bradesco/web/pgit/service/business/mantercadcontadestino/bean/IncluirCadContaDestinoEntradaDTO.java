/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;


/**
 * Nome: IncluirCadContaDestinoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirCadContaDestinoEntradaDTO {
	
	 /** Atributo cdPessoaJuridNegocio. */
 	private Long cdPessoaJuridNegocio;
     
     /** Atributo cdTipoContratoNegocio. */
     private Integer cdTipoContratoNegocio;
     
     /** Atributo nrSeqContratoNegocio. */
     private Long nrSeqContratoNegocio;
     
     /** Atributo cdTipoVinculoContrato. */
     private Integer cdTipoVinculoContrato;
     
     /** Atributo cdBancoSalario. */
     private Integer cdBancoSalario;
     
     /** Atributo cdAgenciaSalario. */
     private Integer cdAgenciaSalario;
     
     /** Atributo cdDigitoAgenciaSalario. */
     private Integer cdDigitoAgenciaSalario;
     
     /** Atributo cdContaSalario. */
     private Long cdContaSalario;
     
     /** Atributo cdDigitoContaSalario. */
     private String cdDigitoContaSalario;
     
     /** Atributo cdBanco. */
     private Integer cdBanco;
     
     /** Atributo cdAgencia. */
     private Integer cdAgencia;
     
     /** Atributo cdDigitoAgencia. */
     private Integer cdDigitoAgencia;
     
     /** Atributo cdConta. */
     private Long cdConta;
     
     /** Atributo cdDigitoConta. */
     private String cdDigitoConta;
     
     /** Atributo cdTipoConta. */
     private Integer cdTipoConta;
     
     /** Atributo cdContingenciaTed. */
     private Integer cdContingenciaTed;
     
     /** Atributo dsTipoConta. */
     private String dsTipoConta;
     
     /** Atributo dsBancoSalario. */
     private String dsBancoSalario;
     
     /** Atributo dsBancoDestino. */
     private String dsBancoDestino;
     
     /** Atributo dsAgenciaSalario. */
     private String dsAgenciaSalario;
     
     /** Atributo dsAgenciaDestino. */
     private String dsAgenciaDestino;
     
     /** Atributo dsContingenciaTed. */
     private String dsContingenciaTed;


	 /**
 	 * Incluir cad conta destino entrada dto.
 	 */
 	public IncluirCadContaDestinoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	 }
	 
	 /**
 	 * Get: bancoDestino.
 	 *
 	 * @return bancoDestino
 	 */
 	public String getBancoDestino() {
		 return dsBancoDestino == null ? String.valueOf(cdBanco) : cdBanco + "-" + dsBancoDestino;
	 }
	 
	 /**
 	 * Get: bancoSalario.
 	 *
 	 * @return bancoSalario
 	 */
 	public String getBancoSalario() {
		 return dsBancoSalario == null ? String.valueOf(cdBancoSalario) : cdBancoSalario + "-" + dsBancoSalario;
	 }
	 
	 /**
 	 * Get: agenciaDestino.
 	 *
 	 * @return agenciaDestino
 	 */
 	public String getAgenciaDestino() {
		 if (dsAgenciaDestino != null  && !dsAgenciaDestino.trim().equals("") ) {
			 return cdDigitoAgencia == null ? cdAgencia + " " + dsAgenciaDestino : cdAgencia + "-" + cdDigitoAgencia + "-"  + dsAgenciaDestino;
		 } else {
			 return cdDigitoAgencia == null ? String.valueOf(cdAgencia) : cdAgencia + "-" + cdDigitoAgencia; 
		 }
	 }
	
	 /**
 	 * Get: agenciaSalario.
 	 *
 	 * @return agenciaSalario
 	 */
 	public String getAgenciaSalario() {
		if (dsAgenciaSalario != null  && !dsAgenciaSalario.trim().equals("")) {
			return cdDigitoAgenciaSalario == null ? cdAgenciaSalario + "-" + dsAgenciaSalario : cdAgenciaSalario + "-" + cdDigitoAgenciaSalario + "-"  + dsAgenciaSalario;
		}else{
			return cdDigitoAgenciaSalario == null ? String.valueOf(cdAgenciaSalario) : cdAgenciaSalario + "-" + cdDigitoAgenciaSalario;
		}
	 }

	 /**
 	 * Get: contaDigito.
 	 *
 	 * @return contaDigito
 	 */
 	public String getContaDigito() {
		 if (cdDigitoConta != null && !cdDigitoConta.trim().equals("0")) {
			 return cdConta + "-" + cdDigitoConta;
		 } else {
			 return String.valueOf(cdConta);
		 }
	 }

	 /**
 	 * Get: contaDigitoSalario.
 	 *
 	 * @return contaDigitoSalario
 	 */
 	public String getContaDigitoSalario() {
		 if (cdDigitoContaSalario != null && !cdDigitoContaSalario.trim().equals("0")) {
			 return cdContaSalario + "-" + cdDigitoContaSalario;
		 } else{
			 return String.valueOf(cdContaSalario); 
		 }
	 }
     
	 /**
 	 * Get: cdAgenciaSalario.
 	 *
 	 * @return cdAgenciaSalario
 	 */
 	public Integer getCdAgenciaSalario() {
		return cdAgenciaSalario;
	 }
	 
	 /**
 	 * Set: cdAgenciaSalario.
 	 *
 	 * @param cdAgenciaSalario the cd agencia salario
 	 */
 	public void setCdAgenciaSalario(Integer cdAgenciaSalario) {
		this.cdAgenciaSalario = cdAgenciaSalario;
	 }
	 
	 /**
 	 * Get: cdBancoSalario.
 	 *
 	 * @return cdBancoSalario
 	 */
 	public Integer getCdBancoSalario() {
		return cdBancoSalario;
	 }
	 
	 /**
 	 * Set: cdBancoSalario.
 	 *
 	 * @param cdBancoSalario the cd banco salario
 	 */
 	public void setCdBancoSalario(Integer cdBancoSalario) {
		this.cdBancoSalario = cdBancoSalario;
	 }
	 
	 /**
 	 * Get: cdContaSalario.
 	 *
 	 * @return cdContaSalario
 	 */
 	public Long getCdContaSalario() {
		return cdContaSalario;
	 }
	 
	 /**
 	 * Set: cdContaSalario.
 	 *
 	 * @param cdContaSalario the cd conta salario
 	 */
 	public void setCdContaSalario(Long cdContaSalario) {
		this.cdContaSalario = cdContaSalario;
	 }

	 /**
 	 * Get: cdAgencia.
 	 *
 	 * @return cdAgencia
 	 */
 	public Integer getCdAgencia() {
		return cdAgencia;
	 }
	 
	 /**
 	 * Set: cdAgencia.
 	 *
 	 * @param cdAgencia the cd agencia
 	 */
 	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	 }
	 
	 /**
 	 * Get: cdBanco.
 	 *
 	 * @return cdBanco
 	 */
 	public Integer getCdBanco() {
		return cdBanco;
	 }
	
	 /**
 	 * Set: cdBanco.
 	 *
 	 * @param cdBanco the cd banco
 	 */
 	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	 }
	
	 /**
 	 * Get: cdConta.
 	 *
 	 * @return cdConta
 	 */
 	public Long getCdConta() {
		return cdConta;
	 }
	
	 /**
 	 * Set: cdConta.
 	 *
 	 * @param cdConta the cd conta
 	 */
 	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	 }

	 /**
 	 * Get: cdDigitoConta.
 	 *
 	 * @return cdDigitoConta
 	 */
 	public String getCdDigitoConta() {
		return cdDigitoConta;
	 }
	 
	 /**
 	 * Set: cdDigitoConta.
 	 *
 	 * @param cdDigitoConta the cd digito conta
 	 */
 	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	 }

	 /**
 	 * Get: cdTipoConta.
 	 *
 	 * @return cdTipoConta
 	 */
 	public Integer getCdTipoConta() {
		return cdTipoConta;
	 }
	
	 /**
 	 * Set: cdTipoConta.
 	 *
 	 * @param cdTipoConta the cd tipo conta
 	 */
 	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	 }
	
	 /**
 	 * Get: cdTipoContratoNegocio.
 	 *
 	 * @return cdTipoContratoNegocio
 	 */
 	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	 }
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}

	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	/**
	 * Get: cdDigitoAgenciaSalario.
	 *
	 * @return cdDigitoAgenciaSalario
	 */
	public Integer getCdDigitoAgenciaSalario() {
		return cdDigitoAgenciaSalario;
	}

	/**
	 * Set: cdDigitoAgenciaSalario.
	 *
	 * @param cdDigitoAgenciaSalario the cd digito agencia salario
	 */
	public void setCdDigitoAgenciaSalario(Integer cdDigitoAgenciaSalario) {
		this.cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
	}

	/**
	 * Get: cdDigitoContaSalario.
	 *
	 * @return cdDigitoContaSalario
	 */
	public String getCdDigitoContaSalario() {
		return cdDigitoContaSalario;
	}

	/**
	 * Set: cdDigitoContaSalario.
	 *
	 * @param cdDigitoContaSalario the cd digito conta salario
	 */
	public void setCdDigitoContaSalario(String cdDigitoContaSalario) {
		this.cdDigitoContaSalario = cdDigitoContaSalario;
	}

	/**
	 * Get: cdPessoaJuridNegocio.
	 *
	 * @return cdPessoaJuridNegocio
	 */
	public Long getCdPessoaJuridNegocio() {
		return cdPessoaJuridNegocio;
	}

	/**
	 * Set: cdPessoaJuridNegocio.
	 *
	 * @param cdPessoaJuridNegocio the cd pessoa jurid negocio
	 */
	public void setCdPessoaJuridNegocio(Long cdPessoaJuridNegocio) {
		this.cdPessoaJuridNegocio = cdPessoaJuridNegocio;
	}

	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}

	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}

	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}

	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}
	
    /**
     * Get: dsTipoConta.
     *
     * @return dsTipoConta
     */
    public String getDsTipoConta() {
		return dsTipoConta;
	}
    
 	/**
	  * Set: dsTipoConta.
	  *
	  * @param dsTipoConta the ds tipo conta
	  */
	 public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}
 	
	/**
	 * Get: cdContingenciaTed.
	 *
	 * @return cdContingenciaTed
	 */
	public Integer getCdContingenciaTed() {
		return cdContingenciaTed;
	}

	/**
	 * Set: cdContingenciaTed.
	 *
	 * @param cdContingenciaTed the cd contingencia ted
	 */
	public void setCdContingenciaTed(Integer cdContingenciaTed) {
		this.cdContingenciaTed = cdContingenciaTed;
	}

	/**
	 * Get: dsAgenciaDestino.
	 *
	 * @return dsAgenciaDestino
	 */
	public String getDsAgenciaDestino() {
		return dsAgenciaDestino;
	}

	/**
	 * Set: dsAgenciaDestino.
	 *
	 * @param dsAgenciaDestino the ds agencia destino
	 */
	public void setDsAgenciaDestino(String dsAgenciaDestino) {
		this.dsAgenciaDestino = dsAgenciaDestino;
	}

	/**
	 * Get: dsAgenciaSalario.
	 *
	 * @return dsAgenciaSalario
	 */
	public String getDsAgenciaSalario() {
		return dsAgenciaSalario;
	}

	/**
	 * Set: dsAgenciaSalario.
	 *
	 * @param dsAgenciaSalario the ds agencia salario
	 */
	public void setDsAgenciaSalario(String dsAgenciaSalario) {
		this.dsAgenciaSalario = dsAgenciaSalario;
	}

	/**
	 * Get: dsContingenciaTed.
	 *
	 * @return dsContingenciaTed
	 */
	public String getDsContingenciaTed() {
		return dsContingenciaTed;
	}

	/**
	 * Set: dsContingenciaTed.
	 *
	 * @param dsContingenciaTed the ds contingencia ted
	 */
	public void setDsContingenciaTed(String dsContingenciaTed) {
		this.dsContingenciaTed = dsContingenciaTed;
	}

	/**
	 * Get: dsBancoDestino.
	 *
	 * @return dsBancoDestino
	 */
	public String getDsBancoDestino() {
		return dsBancoDestino;
	}

	/**
	 * Set: dsBancoDestino.
	 *
	 * @param dsBancoDestino the ds banco destino
	 */
	public void setDsBancoDestino(String dsBancoDestino) {
		this.dsBancoDestino = dsBancoDestino;
	}

	/**
	 * Get: dsBancoSalario.
	 *
	 * @return dsBancoSalario
	 */
	public String getDsBancoSalario() {
		return dsBancoSalario;
	}

	/**
	 * Set: dsBancoSalario.
	 *
	 * @param dsBancoSalario the ds banco salario
	 */
	public void setDsBancoSalario(String dsBancoSalario) {
		this.dsBancoSalario = dsBancoSalario;
	}
}
