/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;

/**
 * Nome: AlterarLimiteContratoDiarioEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarLimiteContratoDiarioEntradaDTO {
	
	/** Atributo novoLimite. */
	private int novoLimite;

	/**
	 * Get: novoLimite.
	 *
	 * @return novoLimite
	 */
	public int getNovoLimite() {
		return novoLimite;
	}

	/**
	 * Set: novoLimite.
	 *
	 * @param novoLimite the novo limite
	 */
	public void setNovoLimite(int novoLimite) {
		this.novoLimite = novoLimite;
	}
	
	
}
