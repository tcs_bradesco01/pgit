/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

/**
 * Nome: ListarDadosCtaConvnEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDadosCtaConvnEntradaDTO {

	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdClubPessoaRepresentante. */
	private Long cdClubPessoaRepresentante;
	
	/** Atributo cdCpfCnpjRepresentante. */
	private Long cdCpfCnpjRepresentante;
	
	/** Atributo cdFilialCnpjRepresentante. */
	private Integer cdFilialCnpjRepresentante;
	
	/** Atributo cdControleCnpjRepresentante. */
	private Integer cdControleCnpjRepresentante;
	
	/** Atributo cdParmConvenio. */
    private Integer cdParmConvenio;

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdClubPessoaRepresentante.
	 *
	 * @return cdClubPessoaRepresentante
	 */
	public Long getCdClubPessoaRepresentante() {
		return cdClubPessoaRepresentante;
	}

	/**
	 * Set: cdClubPessoaRepresentante.
	 *
	 * @param cdClubPessoaRepresentante the cd club pessoa representante
	 */
	public void setCdClubPessoaRepresentante(Long cdClubPessoaRepresentante) {
		this.cdClubPessoaRepresentante = cdClubPessoaRepresentante;
	}

	/**
	 * Get: cdCpfCnpjRepresentante.
	 *
	 * @return cdCpfCnpjRepresentante
	 */
	public Long getCdCpfCnpjRepresentante() {
		return cdCpfCnpjRepresentante;
	}

	/**
	 * Set: cdCpfCnpjRepresentante.
	 *
	 * @param cdCpfCnpjRepresentante the cd cpf cnpj representante
	 */
	public void setCdCpfCnpjRepresentante(Long cdCpfCnpjRepresentante) {
		this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
	}

	/**
	 * Get: cdFilialCnpjRepresentante.
	 *
	 * @return cdFilialCnpjRepresentante
	 */
	public Integer getCdFilialCnpjRepresentante() {
		return cdFilialCnpjRepresentante;
	}

	/**
	 * Set: cdFilialCnpjRepresentante.
	 *
	 * @param cdFilialCnpjRepresentante the cd filial cnpj representante
	 */
	public void setCdFilialCnpjRepresentante(Integer cdFilialCnpjRepresentante) {
		this.cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
	}

	/**
	 * Get: cdControleCnpjRepresentante.
	 *
	 * @return cdControleCnpjRepresentante
	 */
	public Integer getCdControleCnpjRepresentante() {
		return cdControleCnpjRepresentante;
	}

	/**
	 * Set: cdControleCnpjRepresentante.
	 *
	 * @param cdControleCnpjRepresentante the cd controle cnpj representante
	 */
	public void setCdControleCnpjRepresentante(
			Integer cdControleCnpjRepresentante) {
		this.cdControleCnpjRepresentante = cdControleCnpjRepresentante;
	}

    /**
     * Nome: getCdParmConvenio
     *
     * @return cdParmConvenio
     */
    public Integer getCdParmConvenio() {
        return cdParmConvenio;
    }

    /**
     * Nome: setCdParmConvenio
     *
     * @param cdParmConvenio
     */
    public void setCdParmConvenio(Integer cdParmConvenio) {
        this.cdParmConvenio = cdParmConvenio;
    }
}