/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsSolicitacaoEstornoPagtosOPRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsSolicitacaoEstornoPagtosOPRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtSolicitacaoInicio
     */
    private java.lang.String _dtSolicitacaoInicio;

    /**
     * Field _dtSolicitacaoFim
     */
    private java.lang.String _dtSolicitacaoFim;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _cdMotivoSituacaoSolicitacao
     */
    private int _cdMotivoSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoSolicitacao
     */
    private boolean _has_cdMotivoSituacaoSolicitacao;

    /**
     * Field _cdModalidadePagamentoCliente
     */
    private int _cdModalidadePagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdModalidadePagamentoCliente
     */
    private boolean _has_cdModalidadePagamentoCliente;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsSolicitacaoEstornoPagtosOPRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.request.ConsSolicitacaoEstornoPagtosOPRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModalidadePagamentoCliente
     * 
     */
    public void deleteCdModalidadePagamentoCliente()
    {
        this._has_cdModalidadePagamentoCliente= false;
    } //-- void deleteCdModalidadePagamentoCliente() 

    /**
     * Method deleteCdMotivoSituacaoSolicitacao
     * 
     */
    public void deleteCdMotivoSituacaoSolicitacao()
    {
        this._has_cdMotivoSituacaoSolicitacao= false;
    } //-- void deleteCdMotivoSituacaoSolicitacao() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdModalidadePagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdModalidadePagamentoCliente'.
     */
    public int getCdModalidadePagamentoCliente()
    {
        return this._cdModalidadePagamentoCliente;
    } //-- int getCdModalidadePagamentoCliente() 

    /**
     * Returns the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoSolicitacao'.
     */
    public int getCdMotivoSituacaoSolicitacao()
    {
        return this._cdMotivoSituacaoSolicitacao;
    } //-- int getCdMotivoSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dtSolicitacaoFim'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacaoFim'.
     */
    public java.lang.String getDtSolicitacaoFim()
    {
        return this._dtSolicitacaoFim;
    } //-- java.lang.String getDtSolicitacaoFim() 

    /**
     * Returns the value of field 'dtSolicitacaoInicio'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacaoInicio'.
     */
    public java.lang.String getDtSolicitacaoInicio()
    {
        return this._dtSolicitacaoInicio;
    } //-- java.lang.String getDtSolicitacaoInicio() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdModalidadePagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidadePagamentoCliente()
    {
        return this._has_cdModalidadePagamentoCliente;
    } //-- boolean hasCdModalidadePagamentoCliente() 

    /**
     * Method hasCdMotivoSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoSolicitacao()
    {
        return this._has_cdMotivoSituacaoSolicitacao;
    } //-- boolean hasCdMotivoSituacaoSolicitacao() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModalidadePagamentoCliente'.
     * 
     * @param cdModalidadePagamentoCliente the value of field
     * 'cdModalidadePagamentoCliente'.
     */
    public void setCdModalidadePagamentoCliente(int cdModalidadePagamentoCliente)
    {
        this._cdModalidadePagamentoCliente = cdModalidadePagamentoCliente;
        this._has_cdModalidadePagamentoCliente = true;
    } //-- void setCdModalidadePagamentoCliente(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoSolicitacao'.
     * 
     * @param cdMotivoSituacaoSolicitacao the value of field
     * 'cdMotivoSituacaoSolicitacao'.
     */
    public void setCdMotivoSituacaoSolicitacao(int cdMotivoSituacaoSolicitacao)
    {
        this._cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
        this._has_cdMotivoSituacaoSolicitacao = true;
    } //-- void setCdMotivoSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dtSolicitacaoFim'.
     * 
     * @param dtSolicitacaoFim the value of field 'dtSolicitacaoFim'
     */
    public void setDtSolicitacaoFim(java.lang.String dtSolicitacaoFim)
    {
        this._dtSolicitacaoFim = dtSolicitacaoFim;
    } //-- void setDtSolicitacaoFim(java.lang.String) 

    /**
     * Sets the value of field 'dtSolicitacaoInicio'.
     * 
     * @param dtSolicitacaoInicio the value of field
     * 'dtSolicitacaoInicio'.
     */
    public void setDtSolicitacaoInicio(java.lang.String dtSolicitacaoInicio)
    {
        this._dtSolicitacaoInicio = dtSolicitacaoInicio;
    } //-- void setDtSolicitacaoInicio(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsSolicitacaoEstornoPagtosOPRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.request.ConsSolicitacaoEstornoPagtosOPRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.request.ConsSolicitacaoEstornoPagtosOPRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.request.ConsSolicitacaoEstornoPagtosOPRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.request.ConsSolicitacaoEstornoPagtosOPRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
