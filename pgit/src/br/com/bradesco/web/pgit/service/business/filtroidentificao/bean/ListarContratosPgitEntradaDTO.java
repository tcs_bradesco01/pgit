/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroidentificao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroidentificao.bean;

/**
 * Nome: ListarContratosPgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContratosPgitEntradaDTO {

	/** Atributo cdAgencia. */
	private Integer cdAgencia;
	
	/** Atributo cdClubPessoa. */
	private Long cdClubPessoa;
	
	/** Atributo cdFuncionario. */
	private Long cdFuncionario;
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdSituacaoContrato. */
	private Integer cdSituacaoContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;
	
	/** Atributo parametroPrograma. */
	private Integer parametroPrograma;
	
	/** Atributo cdCpfCnpjPssoa. */
	private Long cdCpfCnpjPssoa;
	
	/** Atributo cdFilialCnpjPssoa. */
	private Integer cdFilialCnpjPssoa;
	
	/** Atributo cdControleCpfPssoa. */
	private Integer cdControleCpfPssoa;

	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdClubPessoa.
	 *
	 * @return cdClubPessoa
	 */
	public Long getCdClubPessoa() {
		return cdClubPessoa;
	}

	/**
	 * Set: cdClubPessoa.
	 *
	 * @param cdClubPessoa the cd club pessoa
	 */
	public void setCdClubPessoa(Long cdClubPessoa) {
		this.cdClubPessoa = cdClubPessoa;
	}

	/**
	 * Get: cdFuncionario.
	 *
	 * @return cdFuncionario
	 */
	public Long getCdFuncionario() {
		return cdFuncionario;
	}

	/**
	 * Set: cdFuncionario.
	 *
	 * @param cdFuncionario the cd funcionario
	 */
	public void setCdFuncionario(Long cdFuncionario) {
		this.cdFuncionario = cdFuncionario;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Get: parametroPrograma.
	 *
	 * @return parametroPrograma
	 */
	public Integer getParametroPrograma() {
		return parametroPrograma;
	}

	/**
	 * Set: parametroPrograma.
	 *
	 * @param parametroPrograma the parametro programa
	 */
	public void setParametroPrograma(Integer parametroPrograma) {
		this.parametroPrograma = parametroPrograma;
	}

	/**
	 * Get: cdCpfCnpjPssoa.
	 *
	 * @return cdCpfCnpjPssoa
	 */
	public Long getCdCpfCnpjPssoa() {
		return cdCpfCnpjPssoa;
	}

	/**
	 * Set: cdCpfCnpjPssoa.
	 *
	 * @param cdCpfCnpjPssoa the cd cpf cnpj pssoa
	 */
	public void setCdCpfCnpjPssoa(Long cdCpfCnpjPssoa) {
		this.cdCpfCnpjPssoa = cdCpfCnpjPssoa;
	}

	/**
	 * Get: cdFilialCnpjPssoa.
	 *
	 * @return cdFilialCnpjPssoa
	 */
	public Integer getCdFilialCnpjPssoa() {
		return cdFilialCnpjPssoa;
	}

	/**
	 * Set: cdFilialCnpjPssoa.
	 *
	 * @param cdFilialCnpjPssoa the cd filial cnpj pssoa
	 */
	public void setCdFilialCnpjPssoa(Integer cdFilialCnpjPssoa) {
		this.cdFilialCnpjPssoa = cdFilialCnpjPssoa;
	}

	/**
	 * Get: cdControleCpfPssoa.
	 *
	 * @return cdControleCpfPssoa
	 */
	public Integer getCdControleCpfPssoa() {
		return cdControleCpfPssoa;
	}

	/**
	 * Set: cdControleCpfPssoa.
	 *
	 * @param cdControleCpfPssoa the cd controle cpf pssoa
	 */
	public void setCdControleCpfPssoa(Integer cdControleCpfPssoa) {
		this.cdControleCpfPssoa = cdControleCpfPssoa;
	}

}
