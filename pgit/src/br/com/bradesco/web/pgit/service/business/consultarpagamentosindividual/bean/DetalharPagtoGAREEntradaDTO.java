/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

/**
 * Nome: DetalharPagtoGAREEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPagtoGAREEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
	
	/** Atributo cdBancoDebito. */
	private int cdBancoDebito;
	
	/** Atributo cdAgenciaDebito. */
	private int cdAgenciaDebito;
	
	/** Atributo cdContaDebito. */
	private long cdContaDebito;
	
	/** Atributo cdBancoCredito. */
	private int cdBancoCredito;
	
	/** Atributo cdAgenciaCredito. */
	private int cdAgenciaCredito;
	
	/** Atributo cdContaCredito. */
	private long cdContaCredito;
	
	/** Atributo cdAgendadosPagoNaoPago. */
	private int cdAgendadosPagoNaoPago;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private int cdProdutoServicoRelacionado;
	
	/** Atributo dtHoraManutencao. */
	private String dtHoraManutencao;
	
	/** Atributo dsEfetivacaoPagamento. */
	private String dsEfetivacaoPagamento;
	
	/**
	 * Get: cdAgenciaCredito.
	 *
	 * @return cdAgenciaCredito
	 */
	public int getCdAgenciaCredito() {
		return cdAgenciaCredito;
	}
	
	/**
	 * Set: cdAgenciaCredito.
	 *
	 * @param cdAgenciaCredito the cd agencia credito
	 */
	public void setCdAgenciaCredito(int cdAgenciaCredito) {
		this.cdAgenciaCredito = cdAgenciaCredito;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public int getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(int cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdAgendadosPagoNaoPago.
	 *
	 * @return cdAgendadosPagoNaoPago
	 */
	public int getCdAgendadosPagoNaoPago() {
		return cdAgendadosPagoNaoPago;
	}
	
	/**
	 * Set: cdAgendadosPagoNaoPago.
	 *
	 * @param cdAgendadosPagoNaoPago the cd agendados pago nao pago
	 */
	public void setCdAgendadosPagoNaoPago(int cdAgendadosPagoNaoPago) {
		this.cdAgendadosPagoNaoPago = cdAgendadosPagoNaoPago;
	}
	
	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public int getCdBancoCredito() {
		return cdBancoCredito;
	}
	
	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(int cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public int getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(int cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaCredito.
	 *
	 * @return cdContaCredito
	 */
	public long getCdContaCredito() {
		return cdContaCredito;
	}
	
	/**
	 * Set: cdContaCredito.
	 *
	 * @param cdContaCredito the cd conta credito
	 */
	public void setCdContaCredito(long cdContaCredito) {
		this.cdContaCredito = cdContaCredito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public int getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtHoraManutencao.
	 *
	 * @return dtHoraManutencao
	 */
	public String getDtHoraManutencao() {
		return dtHoraManutencao;
	}
	
	/**
	 * Set: dtHoraManutencao.
	 *
	 * @param dtHoraManutencao the dt hora manutencao
	 */
	public void setDtHoraManutencao(String dtHoraManutencao) {
		this.dtHoraManutencao = dtHoraManutencao;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: dsEfetivacaoPagamento.
	 *
	 * @return dsEfetivacaoPagamento
	 */
	public String getDsEfetivacaoPagamento() {
		return dsEfetivacaoPagamento;
	}
	
	/**
	 * Set: dsEfetivacaoPagamento.
	 *
	 * @param dsEfetivacaoPagamento the ds efetivacao pagamento
	 */
	public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
		this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
	}
}