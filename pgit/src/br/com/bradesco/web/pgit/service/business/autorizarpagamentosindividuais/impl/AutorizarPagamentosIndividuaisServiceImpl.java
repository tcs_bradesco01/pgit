/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.IAutorizarPagamentosIndividuaisService;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.IAutorizarPagamentosIndividuaisServiceConstants;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.AutorizarPagtosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.AutorizarPagtosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.ConsultarPagtoIndPendAutorizacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.ConsultarPagtoIndPendAutorizacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.request.AutorizarPagtosIndividuaisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.response.AutorizarPagtosIndividuaisResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtoindpendautorizacao.request.ConsultarPagtoIndPendAutorizacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtoindpendautorizacao.response.ConsultarPagtoIndPendAutorizacaoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AutorizarPagamentosIndividuais
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AutorizarPagamentosIndividuaisServiceImpl implements IAutorizarPagamentosIndividuaisService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.IAutorizarPagamentosIndividuaisService#autorizarPagtosIndividuais(br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.AutorizarPagtosIndividuaisEntradaDTO)
     */
    public AutorizarPagtosIndividuaisSaidaDTO autorizarPagtosIndividuais(AutorizarPagtosIndividuaisEntradaDTO autorizarPagtosIndividuaisEntradaDTO) {
	AutorizarPagtosIndividuaisSaidaDTO saidaDTO = new AutorizarPagtosIndividuaisSaidaDTO();
	AutorizarPagtosIndividuaisRequest request = new AutorizarPagtosIndividuaisRequest();
	AutorizarPagtosIndividuaisResponse response = new AutorizarPagtosIndividuaisResponse();

	request.setNumeroConsultas(PgitUtil.verificaIntegerNulo(autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais().size()));
	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.request.Ocorrencias>();
	br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.request.Ocorrencias ocorrencia;

	// V�rias Ocorr�ncias para Autoriza��o
	for (int i = 0; i < autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais().size(); i++) {
	    ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.request.Ocorrencias();

	    ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais().get(i)
		    .getCdControlePagamento()));
	    ocorrencia.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais()
		    .get(i).getCdOperacaoProdutoServico()));
	    ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais().get(
		    i).getCdPessoaJuridicaContrato()));
	    ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais()
		    .get(i).getCdProdutoServicoOperacao()));
	    ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais().get(i)
		    .getCdTipoCanal()));
	    ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais().get(
		    i).getCdTipoContratoNegocio()));
	    ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(autorizarPagtosIndividuaisEntradaDTO.getListaAutorizarIndividuais()
		    .get(i).getNrSequenciaContratoNegocio()));

	    lista.add(ocorrencia);

	}

	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.request.Ocorrencias[]) lista
		.toArray(new br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.request.Ocorrencias[0]));

	response = getFactoryAdapter().getAutorizarPagtosIndividuaisPDCAdapter().invokeProcess(request);

	saidaDTO.setCodMensagem(response.getCodMensagem());
	saidaDTO.setMensagem(response.getMensagem());

	return saidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.IAutorizarPagamentosIndividuaisService#consultarPagtoIndPendAutorizacao(br.com.bradesco.web.pgit.service.business.autorizarpagamentosindividuais.bean.ConsultarPagtoIndPendAutorizacaoEntradaDTO)
     */
    public List<ConsultarPagtoIndPendAutorizacaoSaidaDTO> consultarPagtoIndPendAutorizacao(
	    ConsultarPagtoIndPendAutorizacaoEntradaDTO entradaDto) {

	List<ConsultarPagtoIndPendAutorizacaoSaidaDTO> listaRetorno = new ArrayList<ConsultarPagtoIndPendAutorizacaoSaidaDTO>();
	ConsultarPagtoIndPendAutorizacaoRequest request = new ConsultarPagtoIndPendAutorizacaoRequest();
	ConsultarPagtoIndPendAutorizacaoResponse response = new ConsultarPagtoIndPendAutorizacaoResponse();

	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDto.getCdAgencia()));
	request.setCdAgenciaBeneficiario(PgitUtil.verificaIntegerNulo(entradaDto.getCdAgenciaBeneficiario()));
	request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entradaDto.getCdAgendadosPagosNaoPagos()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDto.getCdBanco()));
	request.setCdBancoBeneficiario(PgitUtil.verificaIntegerNulo(entradaDto.getCdBancoBeneficiario()));
	request.setCdBarraDocumento(PgitUtil.verificaStringNula(entradaDto.getCdBarraDocumento()));
	request.setCdBeneficio(PgitUtil.verificaLongNulo(entradaDto.getCdBeneficio()));
	request.setCdClassificacao(PgitUtil.verificaIntegerNulo(entradaDto.getCdClassificacao()));
	request.setCdConta(PgitUtil.verificaLongNulo(entradaDto.getCdConta()));
	request.setCdContaBeneficiario(PgitUtil.verificaLongNulo(entradaDto.getCdContaBeneficiario()));
	request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entradaDto.getCdControlePagamentoAte()));
	request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDto.getCdControlePagamentoDe()));
	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
	request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
	request.setCdEspecieBeneficioInss(PgitUtil.verificaIntegerNulo(entradaDto.getCdEspecieBeneficioInss()));
	request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entradaDto.getCdFavorecidoClientePagador()));
	request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDto
		.getCdIdentificacaoInscricaoFavorecido()));
	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDto.getCdMotivoSituacaoPagamento()));
	request.setCdParticipante(PgitUtil.verificaLongNulo(entradaDto.getCdParticipante()));
	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDto.getCdPessoaJuridicaContrato()));
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDto.getCdProdutoServicoOperacao()));
	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDto
		.getCdProdutoServicoRelacionado()));
	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDto
		.getCdSituacaoOperacaoPagamento()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDto.getCdTipoContratoNegocio()));
	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDto.getCdTipoPesquisa()));
	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDto.getDtCreditoPagamentoFim()));
	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDto.getDtCreditoPagamentoInicio()));
	request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entradaDto.getNrArquivoRemssaPagamento()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDto.getNrSequenciaContratoNegocio()));
	request.setNrUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDto.getNrUnidadeOrganizacional()));
	request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDto.getVlPagamentoAte()));
	request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDto.getVlPagamentoDe()));
	request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDto.getCdInscricaoFavorecido()));
	request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entradaDto.getCdEmpresaContrato()));
	request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDto.getCdTipoConta()));
	request.setNrContrato(PgitUtil.verificaLongNulo(entradaDto.getNrContrato()));
	request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entradaDto.getCdCpfCnpjParticipante()));
	request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDto.getCdFilialCnpjParticipante()));
	request.setCdControleCnpjParticipante(PgitUtil
		.verificaIntegerNulo(entradaDto.getCdControleCnpjParticipante()));
	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDto.getCdPessoaContratoDebito()));
	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDto.getCdTipoContratoDebito()));
	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDto.getNrSequenciaContratoDebito()));
	request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entradaDto.getCdPessoaContratoCredt()));
	request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entradaDto.getCdTipoContratoCredt()));
	request.setNrSequenciaContratoCredt(verificaLongNulo(entradaDto.getNrSequenciaContratoCredt()));
	request.setCdIndiceSimulaPagamento(verificaIntegerNulo(entradaDto.getCdIndiceSimulaPagamento()));
	request.setCdOrigemRecebidoEfetivacao(PgitUtil
		.verificaIntegerNulo(entradaDto.getCdOrigemRecebidoEfetivacao()));
	request.setCdTituloPgtoRastreado(0);

	request.setCdBancoSalarial(verificaIntegerNulo(entradaDto.getCdBancoSalarial()));
	request.setCdAgencaSalarial(verificaIntegerNulo(entradaDto.getCdAgencaSalarial()));
	request.setCdContaSalarial(verificaLongNulo(entradaDto.getCdContaSalarial()));
	if("".equals(PgitUtil.verificaStringNula(entradaDto.getCdIspbPagtoDestino()))){
		request.setCdIspbPagtoDestino("");
	}else{
		request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDto.getCdIspbPagtoDestino(), 8));
	}
	request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDto.getContaPagtoDestino(), 20));
	
	request.setNrOcorrencias(IAutorizarPagamentosIndividuaisServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);

	response = getFactoryAdapter().getConsultarPagtoIndPendAutorizacaoPDCAdapter().invokeProcess(request);
	ConsultarPagtoIndPendAutorizacaoSaidaDTO saidaDTO;

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    saidaDTO = new ConsultarPagtoIndPendAutorizacaoSaidaDTO();
	    saidaDTO.setMensagem(response.getMensagem());
	    saidaDTO.setCdMensagem(response.getCodMensagem());
	    saidaDTO.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
	    saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
	    saidaDTO.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
	    saidaDTO.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
	    saidaDTO.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
	    saidaDTO.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
	    saidaDTO.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
	    saidaDTO.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
	    saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
	    saidaDTO.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
	    saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
	    saidaDTO.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
	    saidaDTO.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido() == 0L ? "" : String.valueOf(response
		    .getOcorrencias(i).getCdInscricaoFavorecido()));
	    saidaDTO.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
	    saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
	    saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
	    saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
	    saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
	    saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
	    saidaDTO.setNrContrato(response.getOcorrencias(i).getNrContrato());
	    saidaDTO.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
	    saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
	    saidaDTO.setVlEfetivoPagamento(response.getOcorrencias(i).getVlEfetivoPagamento());
	    saidaDTO.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), response.getOcorrencias(i)
		    .getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
	    saidaDTO.setFavorecidoBeneficiarioFormatado(response.getOcorrencias(i).getDsFavorecido());
	    saidaDTO.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(), response
		    .getOcorrencias(i).getCdAgenciaDebito(), response.getOcorrencias(i).getCdDigitoAgenciaDebito(), response.getOcorrencias(i)
		    .getCdContaDebito(), response.getOcorrencias(i).getCdDigitoContaDebito(), true));
	    saidaDTO.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoCredito(), response
		    .getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), response.getOcorrencias(i)
		    .getCdContaCredito(), response.getOcorrencias(i).getCdDigitoContaCredito(), true));
	    saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
	    saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
	    saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
	    saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
	    saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
	    saidaDTO.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
	    saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
	    saidaDTO.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
	    saidaDTO.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
	    saidaDTO.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
	    saidaDTO.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
	    saidaDTO.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());

	    // Formata Data
	    saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento() != null
		    && !response.getOcorrencias(i).getDtCreditoPagamento().equals("") ? FormatarData.formatarData(response.getOcorrencias(i)
		    .getDtCreditoPagamento()) : "");

	    // Formata Campo Monet�rio
	    saidaDTO.setVlEfetivoPagamentoFormatado(NumberUtils.format(response.getOcorrencias(i).getVlEfetivoPagamento(), "#,##0.00"));

	    saidaDTO.setCdBancoCreditoFormatado(PgitUtil.formatBanco(response.getOcorrencias(i).getCdBancoCredito(), false));
	    saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i)
		    .getCdDigitoAgenciaCredito(), true));
	    saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdContaCredito(), response.getOcorrencias(i)
		    .getCdDigitoContaCredito(), false));

	    saidaDTO.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
		saidaDTO.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
		
	    listaRetorno.add(saidaDTO);
	}
	return listaRetorno;
    }

}
