/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: BloquearDesbloquearContratoPgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloquearDesbloquearContratoPgitEntradaDTO{
    
    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;
    
    /** Atributo cdPessoa. */
    private Long cdPessoa;
    
    /** Atributo cdSituacaoParticipanteContrato. */
    private Integer cdSituacaoParticipanteContrato;
    
    /** Atributo cdMotivoSituacaoParticipante. */
    private Integer cdMotivoSituacaoParticipante;
    
	/**
	 * Get: cdMotivoSituacaoParticipante.
	 *
	 * @return cdMotivoSituacaoParticipante
	 */
	public Integer getCdMotivoSituacaoParticipante() {
		return cdMotivoSituacaoParticipante;
	}
	
	/**
	 * Set: cdMotivoSituacaoParticipante.
	 *
	 * @param cdMotivoSituacaoParticipante the cd motivo situacao participante
	 */
	public void setCdMotivoSituacaoParticipante(Integer cdMotivoSituacaoParticipante) {
		this.cdMotivoSituacaoParticipante = cdMotivoSituacaoParticipante;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoParticipanteContrato.
	 *
	 * @return cdSituacaoParticipanteContrato
	 */
	public Integer getCdSituacaoParticipanteContrato() {
		return cdSituacaoParticipanteContrato;
	}
	
	/**
	 * Set: cdSituacaoParticipanteContrato.
	 *
	 * @param cdSituacaoParticipanteContrato the cd situacao participante contrato
	 */
	public void setCdSituacaoParticipanteContrato(
			Integer cdSituacaoParticipanteContrato) {
		this.cdSituacaoParticipanteContrato = cdSituacaoParticipanteContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
    
    
}
