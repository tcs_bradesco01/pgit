/*
 * Nome: br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean;

/**
 * Nome: ListarAssRetLayArqProSerSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAssRetLayArqProSerSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLayoutArquivo. */
	private int cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdTipoArquivoRetorno. */
	private int cdTipoArquivoRetorno;
	
	/** Atributo dsTipoArquivoRetorno. */
	private String dsTipoArquivoRetorno;
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return cdTipoArquivoRetorno
	 */
	public int getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}
	
	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdTipoArquivoRetorno the cd tipo arquivo retorno
	 */
	public void setCdTipoArquivoRetorno(int cdTipoArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public int getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoArquivoRetorno.
	 *
	 * @return dsTipoArquivoRetorno
	 */
	public String getDsTipoArquivoRetorno() {
		return dsTipoArquivoRetorno;
	}
	
	/**
	 * Set: dsTipoArquivoRetorno.
	 *
	 * @param dsTipoArquivoRetorno the ds tipo arquivo retorno
	 */
	public void setDsTipoArquivoRetorno(String dsTipoArquivoRetorno) {
		this.dsTipoArquivoRetorno = dsTipoArquivoRetorno;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	

}
