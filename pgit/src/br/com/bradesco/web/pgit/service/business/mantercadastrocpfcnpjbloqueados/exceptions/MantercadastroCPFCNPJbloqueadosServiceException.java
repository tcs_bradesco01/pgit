/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.exceptions
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.exceptions;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * Nome: MantercadastroCPFCNPJbloqueadosServiceException
 * <p>
 * Prop�sito: Classe de exce��o do adaptador MantercadastroCPFCNPJbloqueados
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see BradescoApplicationException
 */
public class MantercadastroCPFCNPJbloqueadosServiceException extends BradescoApplicationException  {

}