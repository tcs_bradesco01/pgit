/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean;

// TODO: Auto-generated Javadoc
/**
 * Nome: ListarFuncoesAlcadaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHorarioLimiteEntradaDTO {
	
	/** Atributo centroCusto. */
	private Integer numeroOcorrencias;

	/**
	 * Sets the numero ocorrencias.
	 *
	 * @param numeroOcorrencias the numeroOcorrencias to set
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

	/**
	 * Gets the numero ocorrencias.
	 *
	 * @return the numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	

	

}
