/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean;

/**
 * Nome: ConsultarTercPartContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarTercPartContratoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo nrRastreaTerceiro. */
	private Integer nrRastreaTerceiro;
	
	/** Atributo cdCpfCnpjTerceiro. */
	private Long cdCpfCnpjTerceiro;
	
	/** Atributo cdFilialCnpjTerceiro. */
	private Integer cdFilialCnpjTerceiro;
	
	/** Atributo cdControleCnpjTerceiro. */
	private Integer cdControleCnpjTerceiro;
	
	/** Atributo cdIndicadorAgendaTitulo. */
	private Integer cdIndicadorAgendaTitulo;
	
	/** Atributo cdIndicadorNotaTerceiro. */
	private Integer cdIndicadorNotaTerceiro;
	
	/** Atributo cdIndicadorPapeletaTerceiro. */
	private Integer cdIndicadorPapeletaTerceiro;
	
	/** Atributo dtInicioRastreaTerceiro. */
	private String dtInicioRastreaTerceiro;
	
	/** Atributo dtInicioPapeletaTerceiro. */
	private String dtInicioPapeletaTerceiro;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo nmOperacaoFluxoInclusao. */
	private String nmOperacaoFluxoInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo cdIndicadorBloqueioRastreabilidade. */
	private String cdIndicadorBloqueioRastreabilidade;
	
	/** Atributo dsIndicadorBloqueioRastreabilidade. */
	private String dsIndicadorBloqueioRastreabilidade;

	/**
	 * Get: dsIndicadorBloqueioRastreabilidade.
	 *
	 * @return dsIndicadorBloqueioRastreabilidade
	 */
	public String getDsIndicadorBloqueioRastreabilidade() {
		return dsIndicadorBloqueioRastreabilidade;
	}

	/**
	 * Set: dsIndicadorBloqueioRastreabilidade.
	 *
	 * @param dsIndicadorBloqueioRastreabilidade the ds indicador bloqueio rastreabilidade
	 */
	public void setDsIndicadorBloqueioRastreabilidade(
			String dsIndicadorBloqueioRastreabilidade) {
		this.dsIndicadorBloqueioRastreabilidade = dsIndicadorBloqueioRastreabilidade;
	}

	/**
	 * Get: cdIndicadorBloqueioRastreabilidade.
	 *
	 * @return cdIndicadorBloqueioRastreabilidade
	 */
	public String getCdIndicadorBloqueioRastreabilidade() {
		return cdIndicadorBloqueioRastreabilidade;
	}

	/**
	 * Set: cdIndicadorBloqueioRastreabilidade.
	 *
	 * @param cdIndicadorBloqueioRastreabilidade the cd indicador bloqueio rastreabilidade
	 */
	public void setCdIndicadorBloqueioRastreabilidade(
			String cdIndicadorBloqueioRastreabilidade) {
		this.cdIndicadorBloqueioRastreabilidade = cdIndicadorBloqueioRastreabilidade;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato(){
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato){
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa(){
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa){
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa(){
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa){
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: nrRastreaTerceiro.
	 *
	 * @return nrRastreaTerceiro
	 */
	public Integer getNrRastreaTerceiro(){
		return nrRastreaTerceiro;
	}

	/**
	 * Set: nrRastreaTerceiro.
	 *
	 * @param nrRastreaTerceiro the nr rastrea terceiro
	 */
	public void setNrRastreaTerceiro(Integer nrRastreaTerceiro){
		this.nrRastreaTerceiro = nrRastreaTerceiro;
	}

	/**
	 * Get: cdCpfCnpjTerceiro.
	 *
	 * @return cdCpfCnpjTerceiro
	 */
	public Long getCdCpfCnpjTerceiro(){
		return cdCpfCnpjTerceiro;
	}

	/**
	 * Set: cdCpfCnpjTerceiro.
	 *
	 * @param cdCpfCnpjTerceiro the cd cpf cnpj terceiro
	 */
	public void setCdCpfCnpjTerceiro(Long cdCpfCnpjTerceiro){
		this.cdCpfCnpjTerceiro = cdCpfCnpjTerceiro;
	}

	/**
	 * Get: cdFilialCnpjTerceiro.
	 *
	 * @return cdFilialCnpjTerceiro
	 */
	public Integer getCdFilialCnpjTerceiro(){
		return cdFilialCnpjTerceiro;
	}

	/**
	 * Set: cdFilialCnpjTerceiro.
	 *
	 * @param cdFilialCnpjTerceiro the cd filial cnpj terceiro
	 */
	public void setCdFilialCnpjTerceiro(Integer cdFilialCnpjTerceiro){
		this.cdFilialCnpjTerceiro = cdFilialCnpjTerceiro;
	}

	/**
	 * Get: cdControleCnpjTerceiro.
	 *
	 * @return cdControleCnpjTerceiro
	 */
	public Integer getCdControleCnpjTerceiro(){
		return cdControleCnpjTerceiro;
	}

	/**
	 * Set: cdControleCnpjTerceiro.
	 *
	 * @param cdControleCnpjTerceiro the cd controle cnpj terceiro
	 */
	public void setCdControleCnpjTerceiro(Integer cdControleCnpjTerceiro){
		this.cdControleCnpjTerceiro = cdControleCnpjTerceiro;
	}

	/**
	 * Get: cdIndicadorAgendaTitulo.
	 *
	 * @return cdIndicadorAgendaTitulo
	 */
	public Integer getCdIndicadorAgendaTitulo(){
		return cdIndicadorAgendaTitulo;
	}

	/**
	 * Set: cdIndicadorAgendaTitulo.
	 *
	 * @param cdIndicadorAgendaTitulo the cd indicador agenda titulo
	 */
	public void setCdIndicadorAgendaTitulo(Integer cdIndicadorAgendaTitulo){
		this.cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
	}

	/**
	 * Get: cdIndicadorNotaTerceiro.
	 *
	 * @return cdIndicadorNotaTerceiro
	 */
	public Integer getCdIndicadorNotaTerceiro(){
		return cdIndicadorNotaTerceiro;
	}

	/**
	 * Set: cdIndicadorNotaTerceiro.
	 *
	 * @param cdIndicadorNotaTerceiro the cd indicador nota terceiro
	 */
	public void setCdIndicadorNotaTerceiro(Integer cdIndicadorNotaTerceiro){
		this.cdIndicadorNotaTerceiro = cdIndicadorNotaTerceiro;
	}

	/**
	 * Get: cdIndicadorPapeletaTerceiro.
	 *
	 * @return cdIndicadorPapeletaTerceiro
	 */
	public Integer getCdIndicadorPapeletaTerceiro(){
		return cdIndicadorPapeletaTerceiro;
	}

	/**
	 * Set: cdIndicadorPapeletaTerceiro.
	 *
	 * @param cdIndicadorPapeletaTerceiro the cd indicador papeleta terceiro
	 */
	public void setCdIndicadorPapeletaTerceiro(Integer cdIndicadorPapeletaTerceiro){
		this.cdIndicadorPapeletaTerceiro = cdIndicadorPapeletaTerceiro;
	}

	/**
	 * Get: dtInicioRastreaTerceiro.
	 *
	 * @return dtInicioRastreaTerceiro
	 */
	public String getDtInicioRastreaTerceiro(){
		return dtInicioRastreaTerceiro;
	}

	/**
	 * Set: dtInicioRastreaTerceiro.
	 *
	 * @param dtInicioRastreaTerceiro the dt inicio rastrea terceiro
	 */
	public void setDtInicioRastreaTerceiro(String dtInicioRastreaTerceiro){
		this.dtInicioRastreaTerceiro = dtInicioRastreaTerceiro;
	}

	/**
	 * Get: dtInicioPapeletaTerceiro.
	 *
	 * @return dtInicioPapeletaTerceiro
	 */
	public String getDtInicioPapeletaTerceiro(){
		return dtInicioPapeletaTerceiro;
	}

	/**
	 * Set: dtInicioPapeletaTerceiro.
	 *
	 * @param dtInicioPapeletaTerceiro the dt inicio papeleta terceiro
	 */
	public void setDtInicioPapeletaTerceiro(String dtInicioPapeletaTerceiro){
		this.dtInicioPapeletaTerceiro = dtInicioPapeletaTerceiro;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao(){
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao){
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao(){
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao){
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao(){
		return cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(String cdAutenticacaoSegurancaInclusao){
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao(){
		return nmOperacaoFluxoInclusao;
	}

	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao){
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro(){
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro){
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao(){
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao){
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao(){
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao){
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao(){
		return cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(String cdAutenticacaoSegurancaManutencao){
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao(){
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao){
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro(){
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro){
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
}