/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean;

/**
 * Nome: ConsutarContrEnvioFormaLiquidacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsutarContrEnvioFormaLiquidacaoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo nrEnvioFormaLiquidacaoDoc. */
	private Integer nrEnvioFormaLiquidacaoDoc;
	
	/** Atributo hrEnvioFormaLiquidacaoDoc. */
	private String hrEnvioFormaLiquidacaoDoc;
	
	/** Atributo dsFormaLiquidacao. */
	private String dsFormaLiquidacao;
	
	/** Atributo formaLiquidacaoFormatado. */
	private String formaLiquidacaoFormatado;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas(){
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas){
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao(){
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao){
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: nrEnvioFormaLiquidacaoDoc.
	 *
	 * @return nrEnvioFormaLiquidacaoDoc
	 */
	public Integer getNrEnvioFormaLiquidacaoDoc(){
		return nrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Set: nrEnvioFormaLiquidacaoDoc.
	 *
	 * @param nrEnvioFormaLiquidacaoDoc the nr envio forma liquidacao doc
	 */
	public void setNrEnvioFormaLiquidacaoDoc(Integer nrEnvioFormaLiquidacaoDoc){
		this.nrEnvioFormaLiquidacaoDoc = nrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Get: hrEnvioFormaLiquidacaoDoc.
	 *
	 * @return hrEnvioFormaLiquidacaoDoc
	 */
	public String getHrEnvioFormaLiquidacaoDoc(){
		return hrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Set: hrEnvioFormaLiquidacaoDoc.
	 *
	 * @param hrEnvioFormaLiquidacaoDoc the hr envio forma liquidacao doc
	 */
	public void setHrEnvioFormaLiquidacaoDoc(String hrEnvioFormaLiquidacaoDoc){
		this.hrEnvioFormaLiquidacaoDoc = hrEnvioFormaLiquidacaoDoc;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao(){
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao){
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: formaLiquidacaoFormatado.
	 *
	 * @return formaLiquidacaoFormatado
	 */
	public String getFormaLiquidacaoFormatado() {
		return formaLiquidacaoFormatado;
	}

	/**
	 * Set: formaLiquidacaoFormatado.
	 *
	 * @param formaLiquidacaoFormatado the forma liquidacao formatado
	 */
	public void setFormaLiquidacaoFormatado(String formaLiquidacaoFormatado) {
		this.formaLiquidacaoFormatado = formaLiquidacaoFormatado;
	}
	
}