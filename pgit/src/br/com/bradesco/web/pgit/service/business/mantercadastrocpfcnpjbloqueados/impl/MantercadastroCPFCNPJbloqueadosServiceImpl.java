/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.impl
 * Compilador: JDK 1.5
 * Propósito: INSERIR O PROPÓSITO DAS CLASSES DO PACOTE
 * Data da criação: <dd/MM/yyyy>
 * Parâmetros de compilação: -d
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ConsultarBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ConsultarBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioOcorrencias;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarIncluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarIncluirBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.request.ConsultarBloqueioRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.response.ConsultarBloqueioRastreamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.request.DetalharBloqueioRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.response.DetalharBloqueioRastreamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.request.DetalharCnpjFicticioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.response.DetalharCnpjFicticioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.ExcluirBloqueioRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.response.ExcluirBloqueioRastreamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluircnpjficticio.request.ExcluirCnpjFicticioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluircnpjficticio.response.ExcluirCnpjFicticioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirbloqueiorastreamento.request.IncluirBloqueioRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirbloqueiorastreamento.response.IncluirBloqueioRastreamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluircnpjficticio.request.IncluirCnpjFicticioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluircnpjficticio.response.IncluirCnpjFicticioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.request.ListarCnpjFicticioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.response.ListarCnpjFicticioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.request.ValidarCnpjFicticioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.response.ValidarCnpjFicticioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.request.ValidarInclusaoBloqueioRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.response.ValidarInclusaoBloqueioRastreamentoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: MantercadastroCPFCNPJbloqueadosServiceImpl
 * <p>
 * Propósito: Implementação do adaptador MantercadastroCPFCNPJbloqueados
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IMantercadastroCPFCNPJbloqueadosService
 */
public class MantercadastroCPFCNPJbloqueadosServiceImpl implements
		IMantercadastroCPFCNPJbloqueadosService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Construtor
	 */
	public MantercadastroCPFCNPJbloqueadosServiceImpl() {
		// TODO: Implementação
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#pesquisarCPFCNPJBloqueados(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ConsultarBloqueioRastreamentoEntradaDTO)
	 */
	public List<ConsultarBloqueioRastreamentoSaidaDTO> pesquisarCPFCNPJBloqueados(
			ConsultarBloqueioRastreamentoEntradaDTO entrada) {

		ConsultarBloqueioRastreamentoRequest request = new ConsultarBloqueioRastreamentoRequest();

		request.setCdpessoaJuridicaContrato(entrada
				.getCdpessoaJuridicaContrato() == null ? 0 : entrada
				.getCdpessoaJuridicaContrato());
		request
				.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0
						: entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio() == null ? 0 : entrada
				.getNrSequenciaContratoNegocio());
		request
				.setNumeroOcorrencias(IMantercadastroCPFCNPJbloqueadosServiceConstants.QTDE_CONSULTAS_LISTAR);

		ConsultarBloqueioRastreamentoResponse response = getFactoryAdapter()
				.getConsultarBloqueioRastreamentoPDCAdapter().invokeProcess(
						request);

		List<ConsultarBloqueioRastreamentoSaidaDTO> lista = new ArrayList<ConsultarBloqueioRastreamentoSaidaDTO>();

		ConsultarBloqueioRastreamentoSaidaDTO saida = null;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ConsultarBloqueioRastreamentoSaidaDTO();

			saida.setNmControleRastreabilidadeTitulo(response.getOcorrencias(i)
					.getNmControleRastreabilidadeTitulo());
			saida.setCdCpfCnpjBloqueio(response.getOcorrencias(i)
					.getCdCpfCnpjBloqueio());
			saida.setCdFilialCnpjBloqueio(response.getOcorrencias(i)
					.getCdFilialCnpjBloqueio());
			saida.setCdControleCnpjCpf(response.getOcorrencias(i)
					.getCdControleCnpjCpf());
			saida.setDsPessoa(response.getOcorrencias(i).getDsPessoa());

			lista.add(saida);
		}

		return lista;
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#incluirBloqueioRastreamento(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirBloqueioRastreamentoEntradaDTO)
	 */
	public IncluirBloqueioRastreamentoSaidaDTO incluirBloqueioRastreamento(
			IncluirBloqueioRastreamentoEntradaDTO entrada) {

		IncluirBloqueioRastreamentoRequest request = new IncluirBloqueioRastreamentoRequest();
		IncluirBloqueioRastreamentoResponse response = new IncluirBloqueioRastreamentoResponse();
		IncluirBloqueioRastreamentoSaidaDTO saida = new IncluirBloqueioRastreamentoSaidaDTO();

		request.setCdpessoaJuridicaContrato(entrada
				.getCdpessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setCdCpfCnpjBloqueio(entrada.getCdCpfCnpjBloqueio());
		request.setCdFilialCnpjBloqueio(entrada.getCdFilialCnpjBloqueio());
		request.setCdControleCnpjCpf(entrada.getCdControleCnpjCpf());

		response = getFactoryAdapter()
				.getIncluirBloqueioRastreamentoPDCAdapter().invokeProcess(
						request);
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#validarBloqueioRastreamento(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarIncluirBloqueioRastreamentoEntradaDTO)
	 */
	public ValidarIncluirBloqueioRastreamentoSaidaDTO validarBloqueioRastreamento(
			ValidarIncluirBloqueioRastreamentoEntradaDTO entrada) {

		ValidarInclusaoBloqueioRastreamentoRequest request = new ValidarInclusaoBloqueioRastreamentoRequest();
		ValidarInclusaoBloqueioRastreamentoResponse response = new ValidarInclusaoBloqueioRastreamentoResponse();
		ValidarIncluirBloqueioRastreamentoSaidaDTO saida = new ValidarIncluirBloqueioRastreamentoSaidaDTO();

		request.setCdpessoaJuridicaContrato(entrada
				.getCdpessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setCdCpfCnpjBloqueio(entrada.getCdCpfCnpjBloqueio());
		request.setCdFilialCnpjBloqueio(entrada.getCdFilialCnpjBloqueio());
		request.setCdControleCnpjCpf(entrada.getCdControleCnpjCpf());

		response = getFactoryAdapter()
				.getValidarInclusaoBloqueioRastreamentoPDCAdapter()
				.invokeProcess(request);
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setDsPessoa(response.getDsPessoa());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#excluirBloqueioRastreamento(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirBloqueioRastreamentoEntradaDTO)
	 */
	public ExcluirBloqueioRastreamentoSaidaDTO excluirBloqueioRastreamento(
			ExcluirBloqueioRastreamentoEntradaDTO entrada) {

		ExcluirBloqueioRastreamentoRequest request = new ExcluirBloqueioRastreamentoRequest();
		ExcluirBloqueioRastreamentoResponse response = new ExcluirBloqueioRastreamentoResponse();
		ExcluirBloqueioRastreamentoSaidaDTO saida = new ExcluirBloqueioRastreamentoSaidaDTO();
		br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias[] ocorrencias = new br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias[entrada
				.getQtdeRegistrosOcorrencias()];
		br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias ocurr = null;

		request.setCdpessoaJuridicaContrato(entrada
				.getCdpessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setQtdeRegistrosOcorrencias(entrada
				.getQtdeRegistrosOcorrencias());

		for (int i = 0; i < entrada.getOcorrencias().size(); i++) {
			ocurr = new br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.request.Ocorrencias();
			ocurr.setNmControleRastreabilidadeTitulo(entrada.getOcorrencias()
	.get(i));
			ocorrencias[i] = ocurr;
		}

		request.setOcorrencias(ocorrencias);

		response = getFactoryAdapter()
				.getExcluirBloqueioRastreamentoPDCAdapter().invokeProcess(
						request);
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#detalharBloqueioRastreamento(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharBloqueioRastreamentoEntradaDTO)
	 */
	public DetalharBloqueioRastreamentoSaidaDTO detalharBloqueioRastreamento(
			DetalharBloqueioRastreamentoEntradaDTO entrada) {

		DetalharBloqueioRastreamentoRequest request = new DetalharBloqueioRastreamentoRequest();
		DetalharBloqueioRastreamentoResponse response = new DetalharBloqueioRastreamentoResponse();
		DetalharBloqueioRastreamentoSaidaDTO saida = new DetalharBloqueioRastreamentoSaidaDTO();

		request.setCdpessoaJuridicaContrato(entrada
				.getCdpessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNmControleRastreabilidadeTitulo(entrada
				.getNmControleRastreabilidadeTitulo());

		response = getFactoryAdapter()
				.getDetalharBloqueioRastreamentoPDCAdapter().invokeProcess(
						request);

		saida.setNmControleRastreabilidadeTitulo(response
				.getNmControleRastreabilidadeTitulo());
		saida.setCdCpfCnpjBloqueio(response.getCdCpfCnpjBloqueio());
		saida.setCdFilialCnpjBloqueio(response.getCdFilialCnpjBloqueio());
		saida.setCdControleCnpjCpf(response.getCdControleCnpjCpf());
		saida.setDsPessoa(response.getDsPessoa());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setCdAutenticacaoSegurancaInclusao(response
				.getCdAutenticacaoSegurancaInclusao());
		saida.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
		saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setCdAutenticacaoSegurancaManutencao(response
				.getCdAutenticacaoSegurancaManutencao());
		saida.setNmOperacaoFluxoManutencao(response
				.getNmOperacaoFluxoManutencao());
		saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#IncluirCnpjFicticio(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirCnpjFicticioEntradaDTO)
	 */
	public MensagemSaida IncluirCnpjFicticio(IncluirCnpjFicticioEntradaDTO entrada) {
		IncluirCnpjFicticioRequest  request = new IncluirCnpjFicticioRequest();
		
		request.setCdCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCnpjCpf()));
		request.setCdCtrlCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdCtrlCpfCnpj()));
		request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjCpf()));

		IncluirCnpjFicticioResponse response = getFactoryAdapter().getIncluirCnpjFicticioPDCAdapter().invokeProcess(request);
		
		return new MensagemSaida(response.getCodMensagem(),response.getMensagem());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#detalharCnpjFicticio(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharCnpjFicticioEntradaDTO)
	 */
	public DetalharCnpjFicticioSaidaDTO detalharCnpjFicticio(DetalharCnpjFicticioEntradaDTO entrada) {
		DetalharCnpjFicticioRequest request = new DetalharCnpjFicticioRequest();
		DetalharCnpjFicticioSaidaDTO objSaida = new DetalharCnpjFicticioSaidaDTO();
		
		request.setCdCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCnpjCpf()));
		request.setCdCtrlCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdCtrlCpfCnpj()));
		request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjCpf()));
		request.setCdContrato(PgitUtil.verificaLongNulo(entrada.getCdContrato()));
		
		DetalharCnpjFicticioResponse response = getFactoryAdapter().getDetalharCnpjFicticioPDCAdapter().invokeProcess(request);
		
		objSaida.setCodMensagem(response.getCodMensagem());
		objSaida.setMensagem(response.getMensagem());
		objSaida.setCdCnpjCpf(response.getCdCnpjCpf());
		objSaida.setCdContratoPerfil(response.getCdContratoPerfil());
		objSaida.setCdCtrlCpfCnpj(response.getCdCtrlCpfCnpj());
		objSaida.setCdFilialCpfCnpj(response.getCdFilialCpfCnpj());
		objSaida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		objSaida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		objSaida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		objSaida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		objSaida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		objSaida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		objSaida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		objSaida.setCdUsuarioManutencaoexterno(response.getCdUsuarioManutencaoexterno());
		objSaida.setDsRazaoSocial(response.getDsRazaoSocial());
		objSaida.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		objSaida.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		objSaida.setDtInclusaoRegistro(response.getDtInclusaoRegistro());
		objSaida.setDtManutencaoRegistro(response.getDtManutencaoRegistro());
		objSaida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		objSaida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		
		return objSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#excluirCnpjFicticio(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirCnpjFicticioEntradaDTO)
	 */
	public MensagemSaida excluirCnpjFicticio(ExcluirCnpjFicticioEntradaDTO entrada) {
		ExcluirCnpjFicticioRequest  request = new ExcluirCnpjFicticioRequest();
		
		request.setCdCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCnpjCpf()));
		request.setCdCtrlCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdCtrlCpfCnpj()));
		request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjCpf()));
		request.setCdContrato(PgitUtil.verificaLongNulo(entrada.getCdContrato()));
		
		ExcluirCnpjFicticioResponse response = getFactoryAdapter().getExcluirCnpjFicticioPDCAdapter().invokeProcess(request);
		
		return new MensagemSaida(response.getCodMensagem(),response.getMensagem());
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#listarCnpjFicticio(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioEntradaDTO)
	 */
	public ListarCnpjFicticioSaidaDTO listarCnpjFicticio(ListarCnpjFicticioEntradaDTO entrada) {
		ListarCnpjFicticioRequest request = new ListarCnpjFicticioRequest();
		ListarCnpjFicticioSaidaDTO saida = new ListarCnpjFicticioSaidaDTO();
		List<ListarCnpjFicticioOcorrencias> listaSaida = new ArrayList<ListarCnpjFicticioOcorrencias>();

		request.setCdContrato(PgitUtil.verificaLongNulo(entrada.getCdContrato()));
		request.setCdCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCnpjCpf()));
		request.setCdFilialCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCpfCnpj()));
		request.setCdCtrlCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdCtrlCpfCnpj()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setNrOcorrencias(50);

		ListarCnpjFicticioResponse response = getFactoryAdapter().getListarCnpjFicticioPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ListarCnpjFicticioOcorrencias ocor = new ListarCnpjFicticioOcorrencias();
			ocor.setCdContratoSaida(response.getOcorrencias(i).getCdContratoSaida());
			ocor.setCdCpfCnpjSaida(response.getOcorrencias(i).getCdCpfCnpjSaida());
			ocor.setCdFilialCpfCnpjSaida(response.getOcorrencias(i).getCdFilialCpfCnpjSaida());
			ocor.setCdCtrlCpfCnpjSaida(response.getOcorrencias(i).getCdCtrlCpfCnpjSaida());
			ocor.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
			ocor.setCdCpfCnpjOriginal(response.getOcorrencias(i).getCdCpfCnpjOriginal());
			ocor.setCdFilialCpfCnpjOriginal(response.getOcorrencias(i).getCdFilialCpfCnpjOriginal());
			ocor.setCdCtrlCpfCnpjOriginal(response.getOcorrencias(i).getCdCtrlCpfCnpjOriginal());
			ocor.setCdNumeroContrato(response.getOcorrencias(i).getCdNumeroContrato() == 0l ? null : response.getOcorrencias(i).getCdNumeroContrato());

			if (response.getOcorrencias(i).getCdFilialCpfCnpjOriginal() !=0) {
				ocor.setCpfCnpjOriginalFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getCdCpfCnpjOriginal(), response.getOcorrencias(i).getCdFilialCpfCnpjOriginal(), response.getOcorrencias(i)
						.getCdCtrlCpfCnpjOriginal()));
			} else {
				ocor.setCpfCnpjOriginalFormatado("");
			}

			if (response.getOcorrencias(i).getCdFilialCpfCnpjSaida() !=0) {
				ocor.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getCdCpfCnpjSaida(), response.getOcorrencias(i).getCdFilialCpfCnpjSaida(), response.getOcorrencias(i)
						.getCdCtrlCpfCnpjSaida()));
			} else {
				ocor.setCpfCnpjFormatado("");
			}

			listaSaida.add(ocor);

		}

		saida.setOcorrencias(listaSaida);

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService#validarCnpjFicticio(br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarCnpjFicticioEntradaDTO)
	 */
	public ValidarCnpjFicticioSaidaDTO validarCnpjFicticio(ValidarCnpjFicticioEntradaDTO entrada) {
		ValidarCnpjFicticioRequest request = new ValidarCnpjFicticioRequest();
		ValidarCnpjFicticioSaidaDTO objSaida = new ValidarCnpjFicticioSaidaDTO();
		
		request.setCdCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCnpjCpf()));
		request.setCdCtrlCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdCtrlCpfCnpj()));
		request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjCpf()));
		
		ValidarCnpjFicticioResponse response = getFactoryAdapter().getValidarCnpjFicticioPDCAdapter().invokeProcess(request);
		
		objSaida.setCodMensagem(response.getCodMensagem());
		objSaida.setMensagem(response.getMensagem());
		objSaida.setCdContratoPerfil(response.getCdContratoPerfil());
		objSaida.setCdCnpjCpf(response.getCdCnpjCpf());
		objSaida.setCdFilialCpfCnpj(response.getCdFilialCpfCnpj());
		objSaida.setCdCtrlCpfCnpj(response.getCdCtrlCpfCnpj());
		objSaida.setDsRazaoSocial(response.getDsRazaoSocial());
		
		return objSaida;
	}

}