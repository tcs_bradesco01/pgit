/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExcluirPrioridadeTipoCompromissoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExcluirPrioridadeTipoCompromissoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCompromissoPrioridadeProduto
     */
    private int _cdCompromissoPrioridadeProduto = 0;

    /**
     * keeps track of state for field:
     * _cdCompromissoPrioridadeProduto
     */
    private boolean _has_cdCompromissoPrioridadeProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExcluirPrioridadeTipoCompromissoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.request.ExcluirPrioridadeTipoCompromissoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCompromissoPrioridadeProduto
     * 
     */
    public void deleteCdCompromissoPrioridadeProduto()
    {
        this._has_cdCompromissoPrioridadeProduto= false;
    } //-- void deleteCdCompromissoPrioridadeProduto() 

    /**
     * Returns the value of field 'cdCompromissoPrioridadeProduto'.
     * 
     * @return int
     * @return the value of field 'cdCompromissoPrioridadeProduto'.
     */
    public int getCdCompromissoPrioridadeProduto()
    {
        return this._cdCompromissoPrioridadeProduto;
    } //-- int getCdCompromissoPrioridadeProduto() 

    /**
     * Method hasCdCompromissoPrioridadeProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCompromissoPrioridadeProduto()
    {
        return this._has_cdCompromissoPrioridadeProduto;
    } //-- boolean hasCdCompromissoPrioridadeProduto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCompromissoPrioridadeProduto'.
     * 
     * @param cdCompromissoPrioridadeProduto the value of field
     * 'cdCompromissoPrioridadeProduto'.
     */
    public void setCdCompromissoPrioridadeProduto(int cdCompromissoPrioridadeProduto)
    {
        this._cdCompromissoPrioridadeProduto = cdCompromissoPrioridadeProduto;
        this._has_cdCompromissoPrioridadeProduto = true;
    } //-- void setCdCompromissoPrioridadeProduto(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExcluirPrioridadeTipoCompromissoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.request.ExcluirPrioridadeTipoCompromissoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.request.ExcluirPrioridadeTipoCompromissoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.request.ExcluirPrioridadeTipoCompromissoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.request.ExcluirPrioridadeTipoCompromissoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
