/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetHistCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetHistCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ExcluirVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ExcluirVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.IncluirVinculoEmprPagContSalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.IncluirVinculoEmprPagContSalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoHistCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoHistCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.SubstituirVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.SubstituirVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.request.ConsultarBancoAgenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response.ConsultarBancoAgenciaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.request.ConsultarDetHistCtaSalarioEmpresaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.response.ConsultarDetHistCtaSalarioEmpresaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.request.ConsultarDetVinculoCtaSalarioEmpresaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.response.ConsultarDetVinculoCtaSalarioEmpresaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculoctasalarioempresa.request.ExcluirVinculoCtaSalarioEmpresaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculoctasalarioempresa.response.ExcluirVinculoCtaSalarioEmpresaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.request.IncluirVinculoEmprPagContSalRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.response.IncluirVinculoEmprPagContSalResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.request.ListarVinculoCtaSalarioEmpresaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.response.ListarVinculoCtaSalarioEmpresaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.request.ListarVinculoHistCtaSalarioEmpresaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response.ListarVinculoHistCtaSalarioEmpresaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.request.SubstituirVinculoCtaSalarioEmpresaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.response.SubstituirVinculoCtaSalarioEmpresaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterSolicitacaoRastFav
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterVinculoEmpresaSalarioServiceImpl implements IManterVinculoEmpresaSalarioService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Manter vinculo empresa salario service impl.
     */
    public ManterVinculoEmpresaSalarioServiceImpl() {
    }

    /*
         * Consultar
         */
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService#pesquisaManterVinculoEmpresaSalario(br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoCtaSalarioEmpresaEntradaDTO)
     */
    public List<ListarVinculoCtaSalarioEmpresaSaidaDTO> pesquisaManterVinculoEmpresaSalario(ListarVinculoCtaSalarioEmpresaEntradaDTO entradaLista) {

	List<ListarVinculoCtaSalarioEmpresaSaidaDTO> lista = new ArrayList<ListarVinculoCtaSalarioEmpresaSaidaDTO>();
	ListarVinculoCtaSalarioEmpresaRequest request = new ListarVinculoCtaSalarioEmpresaRequest();

	request.setCdAgenciaSalario(entradaLista.getCdAgenciaSalario() == null ? 0 : entradaLista.getCdAgenciaSalario());
	if (entradaLista.getCdBancoSalario() != null) {
	    request.setCdBancoSalario(entradaLista.getCdAgenciaSalario() == null ? 0 : entradaLista.getCdBancoSalario());
	} else {
	    request.setCdBancoSalario(entradaLista.getCdBancoSalario() == null ? 0 : entradaLista.getCdBancoSalario());
	}
	request.setCdContaSalario(entradaLista.getCdContaSalario() == null ? 0 : entradaLista.getCdContaSalario());
	request.setCdDigitoAgenciaSalario(entradaLista.getCdDigitoAgenciaSalario() == null ? 0 : entradaLista.getCdDigitoAgenciaSalario());// Verificar
	// o
	// que
	// enviar
	request.setCdDigitoContaSalario(entradaLista.getCdDigitoContaSalario() == null ? "" : entradaLista.getCdDigitoContaSalario());
	request.setCdPessoaJuridNegocio(entradaLista.getCdPessoaJuridNegocio());
	request.setCdTipoContratoNegocio(entradaLista.getCdTipoContratoNegocio());
	request.setNrOcorrencias(entradaLista.getNrOcorrencias());
	request.setNrSeqContratoNegocio(entradaLista.getNrSeqContratoNegocio());

	ListarVinculoCtaSalarioEmpresaResponse response = getFactoryAdapter().getListarVinculoCtaSalarioEmpresaPDCAdapter().invokeProcess(request);

	if (response == null) {
	    return null;
	}

	for (Ocorrencias ocorrencia : response.getOcorrencias()) {
	    lista.add(new ListarVinculoCtaSalarioEmpresaSaidaDTO(ocorrencia));
	}

	return lista;
    }

    /*
         * Detalhar
         */
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService#detalheContaSalarioEmp(br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO)
     */
    public ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO detalheContaSalarioEmp(ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO entradaConsulta) {

	ConsultarDetVinculoCtaSalarioEmpresaRequest request = new ConsultarDetVinculoCtaSalarioEmpresaRequest();

	request.setCdPessoaJuridNegocio(entradaConsulta.getCdPessoaJuridNegocio());
	request.setCdPessoaJuridVinc(entradaConsulta.getCdPessoaJuridVinc());
	request.setCdTipoContratoNegocio(entradaConsulta.getCdTipoContratoNegocio());
	request.setCdTipoContratoVinc(entradaConsulta.getCdTipoContratoVinc());
	request.setCdTipoVinculoContrato(entradaConsulta.getCdTipoVinculoContrato());
	request.setNrSeqContratoNegocio(entradaConsulta.getNrSeqContratoNegocio());
	request.setNrSeqContratoVinc(entradaConsulta.getNrSeqContratoVinc());

	ConsultarDetVinculoCtaSalarioEmpresaResponse response = getFactoryAdapter().getConsultarDetVinculoCtaSalarioEmpresaPDCAdapter()
		.invokeProcess(request);

	if (response == null) {
	    return null;
	}

	return new ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO(response.getOcorrencias(0));
    }

    /*
         * Incluir
         */
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService#incluirVinculoEmprPagContSal(br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.IncluirVinculoEmprPagContSalEntradaDTO)
     */
    public IncluirVinculoEmprPagContSalSaidaDTO incluirVinculoEmprPagContSal(IncluirVinculoEmprPagContSalEntradaDTO entrada) {
	IncluirVinculoEmprPagContSalSaidaDTO saida = new IncluirVinculoEmprPagContSalSaidaDTO();
	IncluirVinculoEmprPagContSalRequest request = new IncluirVinculoEmprPagContSalRequest();
	IncluirVinculoEmprPagContSalResponse response = new IncluirVinculoEmprPagContSalResponse();

	request.setCdPessoaJuridicaNegocio(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaNegocio()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	request.setCdBancoSalario(PgitUtil.verificaIntegerNulo(entrada.getCdBancoSalario()));
	request.setCdAgenciaSalario(PgitUtil.verificaIntegerNulo(entrada.getCdAgenciaSalario()));
	request.setCdDigitoAgenciaSalario(PgitUtil.verificaIntegerNulo(entrada.getCdDigitoAgenciaSalario()));
	request.setCdContaSalario(PgitUtil.verificaLongNulo(entrada.getCdContaSalario()));
	request.setDsDigitoContaSalario(PgitUtil.verificaStringNula(entrada.getDsDigitoContaSalario()));

	response = getFactoryAdapter().getIncluirVinculoEmprPagContSalPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

    /*
         * Substituir
         */
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService#substituirManterVincCtaSalarioEmpr(br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.SubstituirVinculoCtaSalarioEmpresaEntradaDTO)
     */
    public SubstituirVinculoCtaSalarioEmpresaSaidaDTO substituirManterVincCtaSalarioEmpr(SubstituirVinculoCtaSalarioEmpresaEntradaDTO entrada) {

	SubstituirVinculoCtaSalarioEmpresaRequest request = new SubstituirVinculoCtaSalarioEmpresaRequest();

	request.setCdAgenciaAtualizada(entrada.getCdAgenciaAtualizada());
	request.setCdBancoAtualizado(entrada.getCdBancoAtualizado());
	request.setCdContaAtualizada(entrada.getCdContaAtualizada());
	request.setCdDigitoAgenciaAtualizada(entrada.getCdDigitoAgenciaAtualizada() == null ? 0 : entrada.getCdDigitoAgenciaAtualizada());
	request.setCdDigitoContaAtualizada(entrada.getCdDigitoContaAtualizada());
	request.setCdPessoa(entrada.getCdPessoa());
	request.setCdPessoaJuridicaVinculo(entrada.getCdPessoaJuridicaVinculo());
	request.setCdTipoContrato(entrada.getCdTipoContrato());
	request.setCdTipoContratoVinculo(entrada.getCdTipoContratoVinculo());
	request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
	request.setNrSeqContrato(entrada.getNrSeqContrato());
	request.setNrSeqContratoVinculo(entrada.getNrSeqContratoVinculo());

	SubstituirVinculoCtaSalarioEmpresaResponse response = getFactoryAdapter().getSubstituirVinculoCtaSalarioEmpresaPDCAdapter().invokeProcess(
		request);

	if (response == null) {
	    return null;
	}

	return new SubstituirVinculoCtaSalarioEmpresaSaidaDTO(response.getCodMensagem(), response.getMensagem());
    }

    /*
         * Excluir
         */
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService#excluirManterVincCtaSalarioEmp(br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ExcluirVinculoCtaSalarioEmpresaEntradaDTO)
     */
    public ExcluirVinculoCtaSalarioEmpresaSaidaDTO excluirManterVincCtaSalarioEmp(ExcluirVinculoCtaSalarioEmpresaEntradaDTO entrada) {

	ExcluirVinculoCtaSalarioEmpresaRequest request = new ExcluirVinculoCtaSalarioEmpresaRequest();

	request.setCdPessoa(entrada.getCdPessoa());
	request.setCdTipoPessoa(entrada.getCdTipoPessoa());
	request.setNrSeqContrato(entrada.getNrSeqContrato());
	request.setCdPessoVinc(entrada.getCdPessoVinc());
	request.setCdTipoContratoVinc(entrada.getCdTipoContratoVinc());
	request.setNrSeqContratoVinc(entrada.getNrSeqContratoVinc());
	request.setCdTipoVincContrato(entrada.getCdTipoVincContrato());

	ExcluirVinculoCtaSalarioEmpresaResponse response = getFactoryAdapter().getExcluirVinculoCtaSalarioEmpresaPDCAdapter().invokeProcess(request);

	return new ExcluirVinculoCtaSalarioEmpresaSaidaDTO(response.getCodMensagem(), response.getMensagem());
    }

    /*
         * Consultar Historico
         */
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService#pesquisarHistoricoVincCtaSalarioEmp(br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoHistCtaSalarioEmpresaEntradaDTO)
     */
    public List<ListarVinculoHistCtaSalarioEmpresaSaidaDTO> pesquisarHistoricoVincCtaSalarioEmp(
	    ListarVinculoHistCtaSalarioEmpresaEntradaDTO entradaListarHistorico) {

	ListarVinculoHistCtaSalarioEmpresaRequest request = new ListarVinculoHistCtaSalarioEmpresaRequest();

	request.setCdPessoaJuridContrato(PgitUtil.verificaLongNulo(entradaListarHistorico.getCdPessoaJuridContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaListarHistorico.getCdTipoContratoNegocio()));
	request.setCdTipoVinculoContrato(PgitUtil.verificaIntegerNulo(entradaListarHistorico.getCdTipoVinculoContrato()));
	request.setDtAte(PgitUtil.verificaStringNula(entradaListarHistorico.getDtAte()));
	request.setDtDe(PgitUtil.verificaStringNula(entradaListarHistorico.getDtDe()));
	request.setNrOcorrencias(50);
	request.setNrSeqContratoNegocio(PgitUtil.verificaLongNulo(entradaListarHistorico.getNrSeqContratoNegocio()));

	request.setCdPessoaJuridVinc(PgitUtil.verificaLongNulo(entradaListarHistorico.getCdPessoaJuridVinc()));
	request.setCdTipoContratoVinc(PgitUtil.verificaIntegerNulo(entradaListarHistorico.getCdTipoContratoVinc()));
	request.setNrSeqContratoVinc(PgitUtil.verificaLongNulo(entradaListarHistorico.getNrSeqContratoVinc()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaListarHistorico.getCdBanco()));
	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaListarHistorico.getCdAgencia()));
	request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entradaListarHistorico.getCdDigitoAgencia()));
	request.setCdConta(PgitUtil.verificaLongNulo(entradaListarHistorico.getCdConta()));
	request.setCdDigitoConta(PgitUtil.verificaStringNula(entradaListarHistorico.getCdDigitoConta()));
	request.setCdPesquisaLista(PgitUtil.verificaIntegerNulo(entradaListarHistorico.getCdPesquisaLista()));

	List<ListarVinculoHistCtaSalarioEmpresaSaidaDTO> listaHistoricoVinculo = new ArrayList<ListarVinculoHistCtaSalarioEmpresaSaidaDTO>();

	ListarVinculoHistCtaSalarioEmpresaResponse response = getFactoryAdapter().getListarVinculoHistCtaSalarioEmpresaPDCAdapter().invokeProcess(
		request);

	for (br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response.Ocorrencias ocorrencia : response.getOcorrencias()) {
	    listaHistoricoVinculo.add(new ListarVinculoHistCtaSalarioEmpresaSaidaDTO(ocorrencia));
	}

	return listaHistoricoVinculo;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService#detalharHistoricoVincCtaSalarioEmp(br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetHistCtaSalarioEmpresaEntradaDTO)
     */
    public ConsultarDetHistCtaSalarioEmpresaSaidaDTO detalharHistoricoVincCtaSalarioEmp(
	    ConsultarDetHistCtaSalarioEmpresaEntradaDTO entradaDetalheHistorico) {

	ConsultarDetHistCtaSalarioEmpresaRequest request = new ConsultarDetHistCtaSalarioEmpresaRequest();

	request.setCdPessoa(entradaDetalheHistorico.getCdPessoa());
	request.setCdPessoaVinc(entradaDetalheHistorico.getCdPessoaVinc());
	request.setCdTipoContrato(entradaDetalheHistorico.getCdTipoContrato());
	request.setCdTipoContratoVinc(entradaDetalheHistorico.getCdTipoContratoVinc());
	request.setCdTipoVincContrato(entradaDetalheHistorico.getCdTipoVincContrato());
	request.setNrSeqContrato(entradaDetalheHistorico.getNrSeqContrato());
	request.setNrSeqContratoVinc(entradaDetalheHistorico.getNrSeqContratoVinc());
	request.setHrInclusaoRegistroHist(entradaDetalheHistorico.getHrInclusaoRegistro());

	ConsultarDetHistCtaSalarioEmpresaResponse response = getFactoryAdapter().getConsultarDetHistCtaSalarioEmpresaPDCAdapter().invokeProcess(
		request);

	return new ConsultarDetHistCtaSalarioEmpresaSaidaDTO(response);
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService#consultarDescricaoBancoAgencia(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO)
     */
    public ConsultarBancoAgenciaSaidaDTO consultarDescricaoBancoAgencia(ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia) {

	ConsultarBancoAgenciaRequest request = new ConsultarBancoAgenciaRequest();

	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaConsultarBancoAgencia.getCdAgencia()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaConsultarBancoAgencia.getCdBanco()));
	request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entradaConsultarBancoAgencia.getCdDigitoAgencia()));
	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaConsultarBancoAgencia.getCdPessoaJuridicaContrato()));

	ConsultarBancoAgenciaResponse response = getFactoryAdapter().getConsultarBancoAgenciaPDCAdapter().invokeProcess(request);

	if (response == null) {
	    return null;
	}

	ConsultarBancoAgenciaSaidaDTO saidaRetorno = new ConsultarBancoAgenciaSaidaDTO();

	saidaRetorno.setDsBanco(response.getDsBanco());
	saidaRetorno.setDsAgencia(response.getDsAgencia());

	saidaRetorno.setCodMensagem(response.getCodMensagem());
	saidaRetorno.setMensagem(response.getMensagem());

	saidaRetorno.setCdEndereco(response.getCdEndereco());
	saidaRetorno.setLogradouro(response.getLogradouro());
	saidaRetorno.setNumero(response.getNumero());
	saidaRetorno.setBairro(response.getBairro());
	saidaRetorno.setComplemento(response.getComplemento());
	saidaRetorno.setCidade(response.getCidade());
	saidaRetorno.setEstado(response.getEstado());
	saidaRetorno.setPais(response.getPais());
	saidaRetorno.setContato(response.getContato());
	saidaRetorno.setEmail(response.getEmail());
	saidaRetorno.setCep(response.getCep());
	saidaRetorno.setComplementoCep(response.getComplementoCep());

	return saidaRetorno;
    }

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /**
     * Pesquisar manter vinculo empresa salario.
     *
     * @param entradaConsultarDetVinculoCtaSalarioEmpresa the entrada consultar det vinculo cta salario empresa
     * @return the list< consultar det vinculo cta salario empresa saida dt o>
     */
    public List<ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO> pesquisarManterVinculoEmpresaSalario(
	    ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO entradaConsultarDetVinculoCtaSalarioEmpresa) {
	return null;
    }

    /**
     * Pesquisar solic rast favorecidos.
     *
     * @param entradaSubstituirVinculoCtaSalarioEmpresa the entrada substituir vinculo cta salario empresa
     * @return the string
     */
    public String pesquisarSolicRastFavorecidos(SubstituirVinculoCtaSalarioEmpresaEntradaDTO entradaSubstituirVinculoCtaSalarioEmpresa) {
	return null;
    }

}