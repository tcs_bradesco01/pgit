/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: FolhaPagtoContratadaReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FolhaPagtoContratadaReport extends BasicPdfReport {
	
	/** Atributo fTextoTabelaNegrito. */
	private Font fTextoTabelaNegrito = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
	
	/** Atributo fTextoTabelaNormal. */
	private Font fTextoTabelaNormal = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoCinzaNegrito. */
	private Font fTextoCinzaNegrito = new Font(Font.TIMES_ROMAN, 8, Font.BOLD, Color.GRAY);
	
	/** Atributo saidaImprimirContrato. */
	private ImprimirContratoSaidaDTO saidaImprimirContrato;
	
	/** Atributo imagemRelatorio. */
	private Image imagemRelatorio = null;
	
	/**
	 * Folha pagto contratada report.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public FolhaPagtoContratadaReport(ImprimirContratoSaidaDTO saidaImprimirContrato) {
		super();
		this.saidaImprimirContrato = saidaImprimirContrato;
		this.imagemRelatorio = getImagemRelatorio(); 
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	public void popularPdf(Document document) throws DocumentException {
		ManterContratoReport reportManterContrato = new ManterContratoReport();
		 
		preencherCabec(document);
		preencherInicioDocumento(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaPrimeira(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaSegunda(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafoPrimeiro(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafoSegundo(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceira(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaQuinta(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaSexta(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaSetima(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafoUnico(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaOitava(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaNona(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaDez(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaOnze(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaDoze(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaTreze(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaQuatorze(document);
		
		document.add(Chunk.NEXTPAGE);
		document.add(Chunk.NEWLINE);
		reportManterContrato.preencherData(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		reportManterContrato.assinaturaUm(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		reportManterContrato.preencherAssinaturas3(document);
		
		document.add(Chunk.NEWLINE);
		reportManterContrato.assinaturaDois(document);
				
		
	}
	
	/**
	 * Preencher cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		tabela.setWidths(new float[] {30.0f, 70.0f});
		
		Paragraph pLinha1Esq = new Paragraph(new Chunk("CONTRATO DE PRESTA��O DE SERVI�OS DE PAGAMENTOS.", fTextoCinzaNegrito));
		pLinha1Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfPCell.NO_BORDER);
		celula1.addElement(imagemRelatorio);
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher inicio documento.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherInicioDocumento(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Titulo 1 */
		Paragraph pTitulo1 = new Paragraph();
		pTitulo1.add(new Chunk("ANEXO 1", fTextoCinzaNegrito));
		pTitulo1.add(Chunk.NEWLINE);
		pTitulo1.add(Chunk.NEWLINE);
		pTitulo1.setAlignment(Element.ALIGN_CENTER);
		
		/* Titulo 2 */
		Paragraph pTitulo2 = new Paragraph();
		pTitulo2.add(new Chunk("Cr�dito em Conta Folha de Pagamento", fTextoTabelaNegrito));
		pTitulo2.add(Chunk.NEWLINE);
		pTitulo2.setAlignment(Element.ALIGN_CENTER);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("As partes nomeadas e qualificadas no pre�mbulo do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Contrato", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", ao qual este Anexo faz parte, denominadas ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco e Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", resolvem estabelecer as condi��es operacionais e comerciais espec�ficas � presta��o dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("servi�os contratados.", fTextoTabelaNegrito));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pTitulo1);
		celula.addElement(pTitulo2);
		celula.addElement(pLinha);
		
		tabela.addCell(celula);

		document.add(tabela);
	}
	
	/**
	 * Preencher clausula primeira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaPrimeira(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Primeira: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("prestar� ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("os servi�os de pagamento aos seus empregados, mediante ", fTextoTabelaNormal));
		pLinha.add(new Chunk("d�bito na conta corrente ", fTextoTabelaNormal));
		pLinha.add(new Chunk(saidaImprimirContrato.getCdContaAgenciaGestor().toString() + "-" + saidaImprimirContrato.getCdDigitoContaGestor(), fTextoTabelaNegrito));
		pLinha.add(new Chunk(", mantida na ag�ncia  ", fTextoTabelaNormal));
		pLinha.add(new Chunk(saidaImprimirContrato.getCdAgenciaGestorContrato().toString() + "-" + saidaImprimirContrato.getCdDigitoAgenciaGestor(), fTextoTabelaNegrito));
		pLinha.add(new Chunk(" do Banco Bradesco S.A. e cr�dito em conta ", fTextoTabelaNormal));
		pLinha.add(new Chunk("sal�rio dos empregados por transmiss�o de dados, via computador, que conecta (liga��o do seu computador a ", fTextoTabelaNormal));
		pLinha.add(new Chunk("um computador remoto) ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("diretamente ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", nos termos das cl�usulas e condi��es abaixo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("descritas, as quais ficam expressamente ratificadas e aceitas pelas partes para todos os fins e efeitos de direito.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula segunda.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaSegunda(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Segunda: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Os servi�os prestados pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", em conformidade com a cl�usula primeira, ser�o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("realizados por transmiss�o de arquivos em meios magn�ticos, contendo todos os dados necess�rios � ", fTextoTabelaNormal));
		pLinha.add(new Chunk("consecu��o dos servi�os ora contratados, os quais encontram-se mencionados no �lay-out� e/ou �Software� ", fTextoTabelaNormal));
		pLinha.add(new Chunk("que, eventualmente, poder�o ser fornecidos ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente.", fTextoTabelaNegrito));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}

	/**
	 * Preencher paragrafo primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherParagrafoPrimeiro(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Para a execu��o dos servi�os objeto deste Anexo, os arquivos dever�o ser transmitidos, ", fTextoTabelaNormal));
		pLinha.add(new Chunk("e estarem em poder do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" no Centro de Processamento de Dados - na Cidade de Deus, Osasco - S�o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Paulo, doravante denominado �Centro�, com 48 (quarenta e oito) horas de anteced�ncia da data dos d�bitos, ", fTextoTabelaNormal));
		pLinha.add(new Chunk("para que se viabilize o cumprimento do cr�dito.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherParagrafoSegundo(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Os arquivos devem conter todas as informa��es/dados necess�rios � realiza��o dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("pagamentos aos Empregados, os quais encontram-se mencionados no �lay-out� e/ou �Software�.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula terceira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaTerceira(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", ap�s conclu�da a transmiss�o/processamento dos dados pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", tornar� ", fTextoTabelaNormal));
		pLinha.add(new Chunk("dispon�vel o arquivo retorno contendo as consist�ncias, ficando sob a responsabilidade do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" a ", fTextoTabelaNormal));
		pLinha.add(new Chunk("constata��o, confer�ncia e confirma��o das informa��es contidas nesses arquivos, no mesmo dia do envio dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("arquivos.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quinta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaQuinta(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Quinta: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" responsabiliza-se pelo correto conte�do das informa��es contidas nos arquivos, ", fTextoTabelaNormal));
		pLinha.add(new Chunk("os quais ser�o fornecidos ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente.", fTextoTabelaNegrito));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula sexta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaSexta(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Sexta: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" obriga-se a tomar todas as cautelas necess�rias para a correta transcri��o dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("dados dos pagamentos a serem realizados com base neste Anexo, isentando o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", neste ato, de toda e ", fTextoTabelaNormal));
		pLinha.add(new Chunk("qualquer responsabilidade relativa a eventuais reclama��es, preju�zos, perdas e danos, lucros cessantes e/ou ", fTextoTabelaNormal));
		pLinha.add(new Chunk("emergentes, inclusive perante a terceiros, decorrentes de erros, falhas, irregularidades e omiss�es dos dados ", fTextoTabelaNormal));
		pLinha.add(new Chunk("constantes de cada pagamento.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula setima.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaSetima(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula S�tima: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Desde que cumpridas todas as obriga��es assumidas pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" neste Anexo, o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("obriga-se a efetuar os cr�ditos, observadas as seguintes condi��es:", fTextoTabelaNormal));
		
		/* Linha 2 */
		Paragraph pLinha2 = new Paragraph();
		
		pLinha2.add(new Chunk("a)	abertura de conta para cada um dos empregados, destinada exclusivamente para cr�ditos decorrentes da ", fTextoTabelaNormal));
		pLinha2.add(new Chunk("folha de pagamento, sendo proibido o envio pelo ", fTextoTabelaNormal));
		pLinha2.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha2.add(new Chunk(" e o acolhimento pelo ", fTextoTabelaNormal));
		pLinha2.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha2.add(new Chunk(" de cr�ditos de qualquer ", fTextoTabelaNormal));
		pLinha2.add(new Chunk("outra origem ou natureza e a movimenta��o por meio de cheques;", fTextoTabelaNormal));
		pLinha2.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 3 */
		Paragraph pLinha3 = new Paragraph();
		
		pLinha3.add(new Chunk("b) as isen��es e cobran�as de tarifas da conta sal�rio observar�o as regras previstas no Adendo A, denominado ", fTextoTabelaNormal));
		pLinha3.add(new Chunk("�Pacote de Tarifas�, que faz parte integrante deste Anexo para todos os efeitos; e,", fTextoTabelaNormal));
		pLinha3.setAlignment(Element.ALIGN_LEFT);
		
		/* Linha 4 */
		Paragraph pLinha4 = new Paragraph();
		
		pLinha4.add(new Chunk("c) os cr�ditos realizados na conta sal�rio poder�o ser transferidos para contas de dep�sitos de titularidade dos ", fTextoTabelaNormal));
		pLinha4.add(new Chunk("empregados no Banco Bradesco S.A. ou em outras institui��es financeiras.", fTextoTabelaNormal));
		pLinha4.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 5 */
		Paragraph pLinha5 = new Paragraph();
		
		pLinha5.add(new Chunk("c.1.)  para o empregado que j� possua conta de dep�sitos no ", fTextoTabelaNormal));
		pLinha5.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha5.add(new Chunk(" destinada ao recebimento de sal�rios, a ", fTextoTabelaNormal));
		pLinha5.add(new Chunk("transfer�ncia do saldo da conta sal�rio para a conta de dep�sitos ser� autom�tica, ficando dispensada a ", fTextoTabelaNormal));
		pLinha5.add(new Chunk("necessidade de pr�via indica��o pelo empregado;", fTextoTabelaNormal));
		pLinha5.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 6 */
		Paragraph pLinha6 = new Paragraph();
		
		pLinha6.add(new Chunk("c.2.) os empregados que n�o se enquadrem na hip�tese prevista no item acima ou que desejarem realizar a ", fTextoTabelaNormal));
		pLinha6.add(new Chunk("transfer�ncia do saldo da conta sal�rio para conta de dep�sitos em outra institui��o financeira dever�o ", fTextoTabelaNormal));
		pLinha6.add(new Chunk("comunicar o ", fTextoTabelaNormal));
		pLinha6.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha6.add(new Chunk(", por escrito ou por meio eletr�nico eventualmente aceito pelo ", fTextoTabelaNormal));
		pLinha6.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha6.add(new Chunk(", a inten��o de ", fTextoTabelaNormal));
		pLinha6.add(new Chunk("transferir os cr�ditos de sua conta sal�rio para outra conta de dep�sitos, com a indica��o detalhada dos dados ", fTextoTabelaNormal));
		pLinha6.add(new Chunk("identificadores da conta de dep�sitos destinat�ria do cr�dito a ser transferido;e,", fTextoTabelaNormal));
		pLinha6.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 7 */
		Paragraph pLinha7 = new Paragraph();
		
		pLinha7.add(new Chunk("c.3.) a transfer�ncia dos cr�ditos da conta sal�rio para outra conta de dep�sitos mantida no ", fTextoTabelaNormal));
		pLinha7.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha7.add(new Chunk(" ou em outra ", fTextoTabelaNormal));
		pLinha7.add(new Chunk("institui��o financeira somente cessar� por solicita��o do empregado, a partir do m�s de refer�ncia ", fTextoTabelaNormal));
		pLinha7.add(new Chunk("imediatamente posterior ao pedido, desde que a formaliza��o dessa solicita��o tenha sido feita com, no ", fTextoTabelaNormal));
		pLinha7.add(new Chunk("m�nimo, 5 (cinco) dias �teis de anteced�ncia � data de efetiva��o dos cr�ditos, voltando os recursos a ser ", fTextoTabelaNormal));
		pLinha7.add(new Chunk("mantidos na conta sal�rio.", fTextoTabelaNormal));
		pLinha7.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 8 */
		Paragraph pLinha8 = new Paragraph();
		
		pLinha8.add(new Chunk("d) independentemente da conta sal�rio que ser� aberta para o cr�dito dos  pagamentos salariais, o empregado ", fTextoTabelaNormal));
		pLinha8.add(new Chunk("tamb�m poder� ter outras contas de movimenta��o, desde que preenchidos os requisitos da legisla��o em ", fTextoTabelaNormal));
		pLinha8.add(new Chunk("vigor, para as quais incidir�o o pacote de tarifas espec�fico.", fTextoTabelaNormal));
		pLinha8.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		celula.addElement(Chunk.NEWLINE);
		celula.addElement(pLinha2);
		celula.addElement(Chunk.NEWLINE);
		celula.addElement(pLinha3);
		celula.addElement(Chunk.NEWLINE);
		celula.addElement(pLinha4);
		celula.addElement(Chunk.NEWLINE);
		celula.addElement(pLinha5);
		celula.addElement(Chunk.NEWLINE);
		celula.addElement(pLinha6);
		celula.addElement(Chunk.NEWLINE);
		celula.addElement(pLinha7);
		celula.addElement(Chunk.NEWLINE);
		celula.addElement(pLinha8);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo unico.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherParagrafoUnico(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Se no arquivo remessa de que trata o par�grafo primeiro da cl�usula segunda, constar como ", fTextoTabelaNormal));
		pLinha.add(new Chunk("destino do sal�rio ou de cr�dito similar uma conta corrente e/ou poupan�a em nome de seu empregado, o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" autoriza, em car�ter irretrat�vel e irrevog�vel, que o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" credite a conta sal�rio aberta em nome de ", fTextoTabelaNormal));
		pLinha.add(new Chunk("seu empregado, e n�o aquela conta corrente e/ou poupan�a, uma vez que, em decorr�ncia de normas do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Conselho Monet�rio Nacional e do Banco Central, o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" est� impedido de prestar servi�os de folha de ", fTextoTabelaNormal));
		pLinha.add(new Chunk("pagamento de sal�rios e similares por meios outros que n�o o cr�dito em conta sal�rio.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);

		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);

		celula.addElement(pLinha);

		tabela.addCell(celula);

		document.add(tabela);
	}

	/**
	 * Preencher clausula oitava.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaOitava(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(new Locale ("pt", "BR"));

		df.applyPattern("#,##0.00");
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Oitava: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("No caso de impossibilidade da transmiss�o dos arquivos, eles poder�o ser entregues ", fTextoTabelaNormal));
		pLinha.add(new Chunk("fisicamente, com anteced�ncia m�nima de 02 (dois) dias �teis da data do pagamento, na ag�ncia onde o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" mant�m sua conta corrente, mediante cobran�a da tarifa de R$ " + df.format(saidaImprimirContrato.getVlTarifaFolha()) + "(" + saidaImprimirContrato.getDsTarifa() + "), conforme o servi�o prestado.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);

		celula.addElement(pLinha);
		
		tabela.addCell(celula);

		document.add(tabela);
	}

	/**
	 * Preencher clausula nona.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaNona(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Nona: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" obriga-se a provisionar em sua conta corrente, os recursos que permitam o integral ", fTextoTabelaNormal));
		pLinha.add(new Chunk("acolhimento dos d�bitos relativos ao montante da folha de pagamento dos seus empregados e suas respectivas ", fTextoTabelaNormal));
		pLinha.add(new Chunk("tarifas, sendo que o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" n�o se responsabiliza pela n�o realiza��o dos pagamentos nos seguintes casos:", fTextoTabelaNormal));
		
		/* Linha 2 */
		Paragraph pLinha2 = new Paragraph();

		pLinha2.add(new Chunk("a) insufici�ncia de provis�o de fundos em conta corrente;", fTextoTabelaNormal));
		pLinha2.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 3 */
		Paragraph pLinha3 = new Paragraph();
		
		pLinha3.add(new Chunk("b) falhas ou omiss�es nas informa��es prestadas pelo Cliente; e,", fTextoTabelaNormal));
		pLinha3.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 4 */
		Paragraph pLinha4 = new Paragraph();
		
		pLinha4.add(new Chunk("c) atraso na entrega das informa��es pelo Cliente.", fTextoTabelaNormal));
		pLinha4.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfPCell.NO_BORDER);
		
		celula.addElement(pLinha);
		celula.addElement(pLinha2);
		celula.addElement(pLinha3);
		celula.addElement(pLinha4);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}

	/**
	 * Preencher clausula dez.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaDez(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Dez: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" autoriza o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" a efetuar em sua conta corrente, o d�bito dos valores relativos ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("montante dos pagamentos aos seus empregados e suas respectivas tarifas, com  " + saidaImprimirContrato.getQtDiaAviso() + "(" + saidaImprimirContrato.getDsDiaAviso() + ") dia(s) �til ", fTextoTabelaNormal));
		pLinha.add(new Chunk("de anteced�ncia da data dos referidos pagamentos.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfPCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula onze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaOnze(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Onze: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Em nenhuma hip�tese o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" est� obrigado a efetuar pagamentos em montante superior � ", fTextoTabelaNormal));
		pLinha.add(new Chunk("import�ncia dispon�vel na conta corrente do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(".  Entretanto, caso, o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" venha a efetuar pagamentos e ", fTextoTabelaNormal));
		pLinha.add(new Chunk("em conseq��ncia a conta corrente apresentar saldo devedor, o ",  fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" obriga-se a efetuar imediata cobertura ", fTextoTabelaNormal));
		pLinha.add(new Chunk("do d�bito, acrescido do respectivo valor do imposto sobre opera��es de cr�dito e dos juros de mora devidos, ", fTextoTabelaNormal));
		pLinha.add(new Chunk("sob pena de rescis�o deste Anexo, independentemente de qualquer aviso ou notifica��o.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfPCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula doze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaDoze(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Doze: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" dever� agendar os pagamentos sempre para data de d�bito em dia �til. Os arquivos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("ser�o invalidados na hip�tese dos agendamentos reca�rem em dia n�o �til (s�bado, domingo ou feriado), e, em conseq��ncia os cr�ditos aos empregados n�o ser�o efetivados.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfPCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula treze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaTreze(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Treze: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Para todos os fins e efeitos de direito, o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" reconhecer� como l�quido e certo o valor de todos os lan�amentos efetuados na conta corrente, decorrentes dos pagamentos efetuados nos termos deste Anexo.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfPCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quatorze.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaQuatorze(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Quatorze: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" obriga-se a identificar os seus empregados para o correto processamento da folha de pagamento, que dever� conter, os respectivos n�meros do documento de identidade e de inscri��o no ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cadastro de Pessoas F�sicas (CPF), data de nascimento, filia��o e endere�o, sendo vedada a utiliza��o de nome abreviado ou de qualquer forma alterado, inclusive pela supress�o de parte ou partes do nome do empregado.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 2 */
		Paragraph pLinha2 = new Paragraph();
		
		pLinha2.add(new Chunk("Por se acharem de pleno acordo, com tudo aqui pactuado, as partes firmam este Anexo em 03 (tr�s) vias de ", fTextoTabelaNormal));
		pLinha2.add(new Chunk("igual teor e forma, juntamente com as testemunhas abaixo:", fTextoTabelaNormal));
		pLinha2.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfPCell.NO_BORDER);
		
		celula.addElement(pLinha);
		celula.addElement(Chunk.NEWLINE);
		celula.addElement(pLinha2);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
}

