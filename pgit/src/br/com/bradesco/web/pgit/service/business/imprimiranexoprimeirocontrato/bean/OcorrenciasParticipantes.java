/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean;

/**
 * Nome: OcorrenciasParticipantes
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasParticipantes {
	
	/** Atributo cdCpfCnpjParticipante. */
	private String cdCpfCnpjParticipante;
	
	/** Atributo nmParticipante. */
	private String nmParticipante;
	
	/** Atributo cdGrupoEconomicoParticipante. */
	private Long cdGrupoEconomicoParticipante;
	
	/** Atributo dsGrupoEconomicoParticipante. */
	private String dsGrupoEconomicoParticipante;
	
	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public String getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(String cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}
	
	/**
	 * Get: nmParticipante.
	 *
	 * @return nmParticipante
	 */
	public String getNmParticipante() {
		return nmParticipante;
	}
	
	/**
	 * Set: nmParticipante.
	 *
	 * @param nmParticipante the nm participante
	 */
	public void setNmParticipante(String nmParticipante) {
		this.nmParticipante = nmParticipante;
	}
	
	/**
	 * Get: cdGrupoEconomicoParticipante.
	 *
	 * @return cdGrupoEconomicoParticipante
	 */
	public Long getCdGrupoEconomicoParticipante() {
		return cdGrupoEconomicoParticipante;
	}
	
	/**
	 * Set: cdGrupoEconomicoParticipante.
	 *
	 * @param cdGrupoEconomicoParticipante the cd grupo economico participante
	 */
	public void setCdGrupoEconomicoParticipante(Long cdGrupoEconomicoParticipante) {
		this.cdGrupoEconomicoParticipante = cdGrupoEconomicoParticipante;
	}
	
	/**
	 * Get: dsGrupoEconomicoParticipante.
	 *
	 * @return dsGrupoEconomicoParticipante
	 */
	public String getDsGrupoEconomicoParticipante() {
		return dsGrupoEconomicoParticipante;
	}
	
	/**
	 * Set: dsGrupoEconomicoParticipante.
	 *
	 * @param dsGrupoEconomicoParticipante the ds grupo economico participante
	 */
	public void setDsGrupoEconomicoParticipante(String dsGrupoEconomicoParticipante) {
		this.dsGrupoEconomicoParticipante = dsGrupoEconomicoParticipante;
	}
	
	
}
