package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import br.com.bradesco.web.pgit.utils.SiteUtil;

public class ListarVincConvnCtaSalarioOcorrenciasDTO {
	
	private Long codConvenioContaSalario;
	private Long cpfCnpj;
	private int filialCpfCnpj;
	private int controleCpfCnpj;
	private String filialAux;
	private String controleAux;
	private int banco;
	private int agencia;
	private String digAgencia;
	private Long conta;
	private String digConta;
	private int codSituacaoConvenio;
	private String situacaoConvenio;	
	private static final int FILIAL_ZERO = 0;
	private static final int NUMERO_DOIS = 2;
	private static final int NUMERO_QUATRO = 4;
	private static final int NUMERO_NOVE = 9;
	public Long getCodConvenioContaSalario() {
		return codConvenioContaSalario;
	}
	public void setCodConvenioContaSalario(Long codConvenioContaSalario) {
		this.codConvenioContaSalario = codConvenioContaSalario;
	}
	public Long getCpfCnpj() {		
		return cpfCnpj;
	}
	public String getCpfCnpjFormatado(){		
		if(filialCpfCnpj == FILIAL_ZERO){
			return SiteUtil.formatNumber(Long.toString(cpfCnpj), NUMERO_NOVE) +"-"+SiteUtil.formatNumber(Integer.toString(controleCpfCnpj), NUMERO_DOIS);			
		}else{
			return SiteUtil.formatNumber(Long.toString(cpfCnpj), NUMERO_NOVE)+"/"+SiteUtil.formatNumber(Integer.toString(filialCpfCnpj), NUMERO_QUATRO)+"-"+SiteUtil.formatNumber(Integer.toString(controleCpfCnpj), NUMERO_DOIS);
		}
	}
	public void setCpfCnpj(Long cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public int getFilialCpfCnpj() {
		return filialCpfCnpj;
	}
	public void setFilialCpfCnpj(int filialCpfCnpj) {
		this.filialCpfCnpj = filialCpfCnpj;
	}
	public int getControleCpfCnpj() {
		return controleCpfCnpj;
	}
	public void setControleCpfCnpj(int controleCpfCnpj) {
		this.controleCpfCnpj = controleCpfCnpj;
	}
	public int getBanco() {
		return banco;
	}
	public void setBanco(int banco) {
		this.banco = banco;
	}
	public int getAgencia() {
		return agencia;
	}
	public String getAgenciaFormatado(){
		if(null != digAgencia && !"".equals(digAgencia)){
			return agencia +"-"+digAgencia;
		}else{
			return Integer.toString(agencia);
		}
	}
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	public String getDigAgencia() {
		return digAgencia;
	}
	public void setDigAgencia(String digAgencia) {
		this.digAgencia = digAgencia;
	}
	public Long getConta() {
		return conta;
	}
	public String getContaFormatado(){
		if(null != digConta && !"".equals(digConta)){
			return conta +"-"+digConta;
		}else{
			return Long.toString(conta);
		}
	}
	public void setConta(Long conta) {
		this.conta = conta;
	}
	public String getDigConta() {
		return digConta;
	}
	public void setDigConta(String digConta) {
		this.digConta = digConta;
	}
	public int getCodSituacaoConvenio() {
		return codSituacaoConvenio;
	}
	public void setCodSituacaoConvenio(int codSituacaoConvenio) {
		this.codSituacaoConvenio = codSituacaoConvenio;
	}
	public String getSituacaoConvenio() {
		return situacaoConvenio;
	}
	public void setSituacaoConvenio(String situacaoConvenio) {
		this.situacaoConvenio = situacaoConvenio;
	}
	public String getFilialAux() {
		return filialAux;
	}
	public void setFilialAux(String filialAux) {
		this.filialAux = filialAux;
	}
	public String getControleAux() {
		return controleAux;
	}
	public void setControleAux(String controleAux) {
		this.controleAux = controleAux;
	}
	

}
