/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _dsCdSistema
     */
    private java.lang.String _dsCdSistema;

    /**
     * Field _cdProcessoSistema
     */
    private int _cdProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdProcessoSistema
     */
    private boolean _has_cdProcessoSistema;

    /**
     * Field _cdTipoProcessoSistema
     */
    private int _cdTipoProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdTipoProcessoSistema
     */
    private boolean _has_cdTipoProcessoSistema;

    /**
     * Field _dsTipoProcessoSistema
     */
    private java.lang.String _dsTipoProcessoSistema;

    /**
     * Field _cdPeriodicidade
     */
    private int _cdPeriodicidade = 0;

    /**
     * keeps track of state for field: _cdPeriodicidade
     */
    private boolean _has_cdPeriodicidade;

    /**
     * Field _dsCodigoPeriodicidade
     */
    private java.lang.String _dsCodigoPeriodicidade;

    /**
     * Field _cdSituacaoProcessoSistema
     */
    private int _cdSituacaoProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdSituacaoProcessoSistema
     */
    private boolean _has_cdSituacaoProcessoSistema;

    /**
     * Field _dsSituacaoProcessoSistema
     */
    private java.lang.String _dsSituacaoProcessoSistema;

    /**
     * Field _cdNetProcessamentoPagamento
     */
    private java.lang.String _cdNetProcessamentoPagamento;

    /**
     * Field _cdJobProcessamentoPagamento
     */
    private java.lang.String _cdJobProcessamentoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPeriodicidade
     * 
     */
    public void deleteCdPeriodicidade()
    {
        this._has_cdPeriodicidade= false;
    } //-- void deleteCdPeriodicidade() 

    /**
     * Method deleteCdProcessoSistema
     * 
     */
    public void deleteCdProcessoSistema()
    {
        this._has_cdProcessoSistema= false;
    } //-- void deleteCdProcessoSistema() 

    /**
     * Method deleteCdSituacaoProcessoSistema
     * 
     */
    public void deleteCdSituacaoProcessoSistema()
    {
        this._has_cdSituacaoProcessoSistema= false;
    } //-- void deleteCdSituacaoProcessoSistema() 

    /**
     * Method deleteCdTipoProcessoSistema
     * 
     */
    public void deleteCdTipoProcessoSistema()
    {
        this._has_cdTipoProcessoSistema= false;
    } //-- void deleteCdTipoProcessoSistema() 

    /**
     * Returns the value of field 'cdJobProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdJobProcessamentoPagamento'.
     */
    public java.lang.String getCdJobProcessamentoPagamento()
    {
        return this._cdJobProcessamentoPagamento;
    } //-- java.lang.String getCdJobProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdNetProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdNetProcessamentoPagamento'.
     */
    public java.lang.String getCdNetProcessamentoPagamento()
    {
        return this._cdNetProcessamentoPagamento;
    } //-- java.lang.String getCdNetProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdPeriodicidade'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidade'.
     */
    public int getCdPeriodicidade()
    {
        return this._cdPeriodicidade;
    } //-- int getCdPeriodicidade() 

    /**
     * Returns the value of field 'cdProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcessoSistema'.
     */
    public int getCdProcessoSistema()
    {
        return this._cdProcessoSistema;
    } //-- int getCdProcessoSistema() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdSituacaoProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoProcessoSistema'.
     */
    public int getCdSituacaoProcessoSistema()
    {
        return this._cdSituacaoProcessoSistema;
    } //-- int getCdSituacaoProcessoSistema() 

    /**
     * Returns the value of field 'cdTipoProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcessoSistema'.
     */
    public int getCdTipoProcessoSistema()
    {
        return this._cdTipoProcessoSistema;
    } //-- int getCdTipoProcessoSistema() 

    /**
     * Returns the value of field 'dsCdSistema'.
     * 
     * @return String
     * @return the value of field 'dsCdSistema'.
     */
    public java.lang.String getDsCdSistema()
    {
        return this._dsCdSistema;
    } //-- java.lang.String getDsCdSistema() 

    /**
     * Returns the value of field 'dsCodigoPeriodicidade'.
     * 
     * @return String
     * @return the value of field 'dsCodigoPeriodicidade'.
     */
    public java.lang.String getDsCodigoPeriodicidade()
    {
        return this._dsCodigoPeriodicidade;
    } //-- java.lang.String getDsCodigoPeriodicidade() 

    /**
     * Returns the value of field 'dsSituacaoProcessoSistema'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoProcessoSistema'.
     */
    public java.lang.String getDsSituacaoProcessoSistema()
    {
        return this._dsSituacaoProcessoSistema;
    } //-- java.lang.String getDsSituacaoProcessoSistema() 

    /**
     * Returns the value of field 'dsTipoProcessoSistema'.
     * 
     * @return String
     * @return the value of field 'dsTipoProcessoSistema'.
     */
    public java.lang.String getDsTipoProcessoSistema()
    {
        return this._dsTipoProcessoSistema;
    } //-- java.lang.String getDsTipoProcessoSistema() 

    /**
     * Method hasCdPeriodicidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidade()
    {
        return this._has_cdPeriodicidade;
    } //-- boolean hasCdPeriodicidade() 

    /**
     * Method hasCdProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcessoSistema()
    {
        return this._has_cdProcessoSistema;
    } //-- boolean hasCdProcessoSistema() 

    /**
     * Method hasCdSituacaoProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoProcessoSistema()
    {
        return this._has_cdSituacaoProcessoSistema;
    } //-- boolean hasCdSituacaoProcessoSistema() 

    /**
     * Method hasCdTipoProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcessoSistema()
    {
        return this._has_cdTipoProcessoSistema;
    } //-- boolean hasCdTipoProcessoSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdJobProcessamentoPagamento'.
     * 
     * @param cdJobProcessamentoPagamento the value of field
     * 'cdJobProcessamentoPagamento'.
     */
    public void setCdJobProcessamentoPagamento(java.lang.String cdJobProcessamentoPagamento)
    {
        this._cdJobProcessamentoPagamento = cdJobProcessamentoPagamento;
    } //-- void setCdJobProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdNetProcessamentoPagamento'.
     * 
     * @param cdNetProcessamentoPagamento the value of field
     * 'cdNetProcessamentoPagamento'.
     */
    public void setCdNetProcessamentoPagamento(java.lang.String cdNetProcessamentoPagamento)
    {
        this._cdNetProcessamentoPagamento = cdNetProcessamentoPagamento;
    } //-- void setCdNetProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdPeriodicidade'.
     * 
     * @param cdPeriodicidade the value of field 'cdPeriodicidade'.
     */
    public void setCdPeriodicidade(int cdPeriodicidade)
    {
        this._cdPeriodicidade = cdPeriodicidade;
        this._has_cdPeriodicidade = true;
    } //-- void setCdPeriodicidade(int) 

    /**
     * Sets the value of field 'cdProcessoSistema'.
     * 
     * @param cdProcessoSistema the value of field
     * 'cdProcessoSistema'.
     */
    public void setCdProcessoSistema(int cdProcessoSistema)
    {
        this._cdProcessoSistema = cdProcessoSistema;
        this._has_cdProcessoSistema = true;
    } //-- void setCdProcessoSistema(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoProcessoSistema'.
     * 
     * @param cdSituacaoProcessoSistema the value of field
     * 'cdSituacaoProcessoSistema'.
     */
    public void setCdSituacaoProcessoSistema(int cdSituacaoProcessoSistema)
    {
        this._cdSituacaoProcessoSistema = cdSituacaoProcessoSistema;
        this._has_cdSituacaoProcessoSistema = true;
    } //-- void setCdSituacaoProcessoSistema(int) 

    /**
     * Sets the value of field 'cdTipoProcessoSistema'.
     * 
     * @param cdTipoProcessoSistema the value of field
     * 'cdTipoProcessoSistema'.
     */
    public void setCdTipoProcessoSistema(int cdTipoProcessoSistema)
    {
        this._cdTipoProcessoSistema = cdTipoProcessoSistema;
        this._has_cdTipoProcessoSistema = true;
    } //-- void setCdTipoProcessoSistema(int) 

    /**
     * Sets the value of field 'dsCdSistema'.
     * 
     * @param dsCdSistema the value of field 'dsCdSistema'.
     */
    public void setDsCdSistema(java.lang.String dsCdSistema)
    {
        this._dsCdSistema = dsCdSistema;
    } //-- void setDsCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoPeriodicidade'.
     * 
     * @param dsCodigoPeriodicidade the value of field
     * 'dsCodigoPeriodicidade'.
     */
    public void setDsCodigoPeriodicidade(java.lang.String dsCodigoPeriodicidade)
    {
        this._dsCodigoPeriodicidade = dsCodigoPeriodicidade;
    } //-- void setDsCodigoPeriodicidade(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoProcessoSistema'.
     * 
     * @param dsSituacaoProcessoSistema the value of field
     * 'dsSituacaoProcessoSistema'.
     */
    public void setDsSituacaoProcessoSistema(java.lang.String dsSituacaoProcessoSistema)
    {
        this._dsSituacaoProcessoSistema = dsSituacaoProcessoSistema;
    } //-- void setDsSituacaoProcessoSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoProcessoSistema'.
     * 
     * @param dsTipoProcessoSistema the value of field
     * 'dsTipoProcessoSistema'.
     */
    public void setDsTipoProcessoSistema(java.lang.String dsTipoProcessoSistema)
    {
        this._dsTipoProcessoSistema = dsTipoProcessoSistema;
    } //-- void setDsTipoProcessoSistema(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
