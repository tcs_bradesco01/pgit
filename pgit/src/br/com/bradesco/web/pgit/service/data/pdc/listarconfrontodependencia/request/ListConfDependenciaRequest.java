/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id: ListConfDependenciaRequest.java,v 1.2 2009/05/13 19:39:18 cpm.com.br\heslei.silva Exp $
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ListConfDependenciaRequest.
 * 
 * @version $Revision: 1.2 $ $Date: 2009/05/13 19:39:18 $
 */
public class ListConfDependenciaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maxOcorrencia
     */
    private int _maxOcorrencia = 00;

    /**
     * keeps track of state for field: _maxOcorrencia
     */
    private boolean _has_maxOcorrencia;

    /**
     * Field _codEmpresa
     */
    private long _codEmpresa = 0000000000;

    /**
     * keeps track of state for field: _codEmpresa
     */
    private boolean _has_codEmpresa;

    /**
     * Field _codDependencia
     */
    private int _codDependencia = 00000000;

    /**
     * keeps track of state for field: _codDependencia
     */
    private boolean _has_codDependencia;

    /**
     * Field _dtReferenciaDe
     */
    private java.lang.String _dtReferenciaDe = "0000000000";

    /**
     * Field _dtReferenciaAte
     */
    private java.lang.String _dtReferenciaAte = "0000000000";

    /**
     * Field _tipoConta
     */
    private int _tipoConta = 0;

    /**
     * keeps track of state for field: _tipoConta
     */
    private boolean _has_tipoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListConfDependenciaRequest() 
     {
        super();
        setDtReferenciaDe("0000000000");
        setDtReferenciaAte("0000000000");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.request.ListConfDependenciaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodDependencia
     * 
     */
    public void deleteCodDependencia()
    {
        this._has_codDependencia= false;
    } //-- void deleteCodDependencia() 

    /**
     * Method deleteCodEmpresa
     * 
     */
    public void deleteCodEmpresa()
    {
        this._has_codEmpresa= false;
    } //-- void deleteCodEmpresa() 

    /**
     * Method deleteMaxOcorrencia
     * 
     */
    public void deleteMaxOcorrencia()
    {
        this._has_maxOcorrencia= false;
    } //-- void deleteMaxOcorrencia() 

    /**
     * Method deleteTipoConta
     * 
     */
    public void deleteTipoConta()
    {
        this._has_tipoConta= false;
    } //-- void deleteTipoConta() 

    /**
     * Returns the value of field 'codDependencia'.
     * 
     * @return int
     * @return the value of field 'codDependencia'.
     */
    public int getCodDependencia()
    {
        return this._codDependencia;
    } //-- int getCodDependencia() 

    /**
     * Returns the value of field 'codEmpresa'.
     * 
     * @return long
     * @return the value of field 'codEmpresa'.
     */
    public long getCodEmpresa()
    {
        return this._codEmpresa;
    } //-- long getCodEmpresa() 

    /**
     * Returns the value of field 'dtReferenciaAte'.
     * 
     * @return String
     * @return the value of field 'dtReferenciaAte'.
     */
    public java.lang.String getDtReferenciaAte()
    {
        return this._dtReferenciaAte;
    } //-- java.lang.String getDtReferenciaAte() 

    /**
     * Returns the value of field 'dtReferenciaDe'.
     * 
     * @return String
     * @return the value of field 'dtReferenciaDe'.
     */
    public java.lang.String getDtReferenciaDe()
    {
        return this._dtReferenciaDe;
    } //-- java.lang.String getDtReferenciaDe() 

    /**
     * Returns the value of field 'maxOcorrencia'.
     * 
     * @return int
     * @return the value of field 'maxOcorrencia'.
     */
    public int getMaxOcorrencia()
    {
        return this._maxOcorrencia;
    } //-- int getMaxOcorrencia() 

    /**
     * Returns the value of field 'tipoConta'.
     * 
     * @return int
     * @return the value of field 'tipoConta'.
     */
    public int getTipoConta()
    {
        return this._tipoConta;
    } //-- int getTipoConta() 

    /**
     * Method hasCodDependencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodDependencia()
    {
        return this._has_codDependencia;
    } //-- boolean hasCodDependencia() 

    /**
     * Method hasCodEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodEmpresa()
    {
        return this._has_codEmpresa;
    } //-- boolean hasCodEmpresa() 

    /**
     * Method hasMaxOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaxOcorrencia()
    {
        return this._has_maxOcorrencia;
    } //-- boolean hasMaxOcorrencia() 

    /**
     * Method hasTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasTipoConta()
    {
        return this._has_tipoConta;
    } //-- boolean hasTipoConta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codDependencia'.
     * 
     * @param codDependencia the value of field 'codDependencia'.
     */
    public void setCodDependencia(int codDependencia)
    {
        this._codDependencia = codDependencia;
        this._has_codDependencia = true;
    } //-- void setCodDependencia(int) 

    /**
     * Sets the value of field 'codEmpresa'.
     * 
     * @param codEmpresa the value of field 'codEmpresa'.
     */
    public void setCodEmpresa(long codEmpresa)
    {
        this._codEmpresa = codEmpresa;
        this._has_codEmpresa = true;
    } //-- void setCodEmpresa(long) 

    /**
     * Sets the value of field 'dtReferenciaAte'.
     * 
     * @param dtReferenciaAte the value of field 'dtReferenciaAte'.
     */
    public void setDtReferenciaAte(java.lang.String dtReferenciaAte)
    {
        this._dtReferenciaAte = dtReferenciaAte;
    } //-- void setDtReferenciaAte(java.lang.String) 

    /**
     * Sets the value of field 'dtReferenciaDe'.
     * 
     * @param dtReferenciaDe the value of field 'dtReferenciaDe'.
     */
    public void setDtReferenciaDe(java.lang.String dtReferenciaDe)
    {
        this._dtReferenciaDe = dtReferenciaDe;
    } //-- void setDtReferenciaDe(java.lang.String) 

    /**
     * Sets the value of field 'maxOcorrencia'.
     * 
     * @param maxOcorrencia the value of field 'maxOcorrencia'.
     */
    public void setMaxOcorrencia(int maxOcorrencia)
    {
        this._maxOcorrencia = maxOcorrencia;
        this._has_maxOcorrencia = true;
    } //-- void setMaxOcorrencia(int) 

    /**
     * Sets the value of field 'tipoConta'.
     * 
     * @param tipoConta the value of field 'tipoConta'.
     */
    public void setTipoConta(int tipoConta)
    {
        this._tipoConta = tipoConta;
        this._has_tipoConta = true;
    } //-- void setTipoConta(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListConfDependenciaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.request.ListConfDependenciaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.request.ListConfDependenciaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.request.ListConfDependenciaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconfrontodependencia.request.ListConfDependenciaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
