/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasModFornecedores
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasModFornecedores {

	/** Atributo dsModalidadeFornecedor. */
	private String dsModalidadeFornecedor;

	/**
	 * Ocorrencias mod fornecedores.
	 */
	public OcorrenciasModFornecedores() {
		super();
	}

	/**
	 * Ocorrencias mod fornecedores.
	 *
	 * @param dsModalidadeFornecedor the ds modalidade fornecedor
	 */
	public OcorrenciasModFornecedores(String dsModalidadeFornecedor) {
		super();
		this.dsModalidadeFornecedor = dsModalidadeFornecedor;
	}

	/**
	 * Get: dsModalidadeFornecedor.
	 *
	 * @return dsModalidadeFornecedor
	 */
	public String getDsModalidadeFornecedor() {
		return dsModalidadeFornecedor;
	}

	/**
	 * Set: dsModalidadeFornecedor.
	 *
	 * @param dsModalidadeFornecedor the ds modalidade fornecedor
	 */
	public void setDsModalidadeFornecedor(String dsModalidadeFornecedor) {
		this.dsModalidadeFornecedor = dsModalidadeFornecedor;
	}
}