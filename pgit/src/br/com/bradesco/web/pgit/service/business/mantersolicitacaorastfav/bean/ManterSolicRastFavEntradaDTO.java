/**
 * 
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.utils.DateUtils;

/**
 * @author tcordeiro
 *
 */
public class ManterSolicRastFavEntradaDTO {
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdSituacaoSolicitacaoPagamento. */
    private Integer cdSituacaoSolicitacaoPagamento;
    
    /** Atributo dtInicioRastreabilidade. */
    private Date dtInicioRastreabilidade;
    
    /** Atributo dtFimRastreabilidade. */
    private Date dtFimRastreabilidade;
    
    
	/**
	 * Manter solic rast fav entrada dto.
	 */
	public ManterSolicRastFavEntradaDTO() {
		super();
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoSolicitacaoPagamento.
	 *
	 * @return cdSituacaoSolicitacaoPagamento
	 */
	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoSolicitacaoPagamento.
	 *
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 */
	public void setCdSituacaoSolicitacaoPagamento(
			Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

	/**
	 * Get: dtFimRastreabilidade.
	 *
	 * @return dtFimRastreabilidade
	 */
	public Date getDtFimRastreabilidade() {
		return dtFimRastreabilidade;
	}

	/**
	 * Set: dtFimRastreabilidade.
	 *
	 * @param dtFimRastreabilidade the dt fim rastreabilidade
	 */
	public void setDtFimRastreabilidade(Date dtFimRastreabilidade) {
		this.dtFimRastreabilidade = dtFimRastreabilidade;
	}

	/**
	 * Get: dtInicioRastreabilidade.
	 *
	 * @return dtInicioRastreabilidade
	 */
	public Date getDtInicioRastreabilidade() {
		return dtInicioRastreabilidade;
	}

	/**
	 * Set: dtInicioRastreabilidade.
	 *
	 * @param dtInicioRastreabilidade the dt inicio rastreabilidade
	 */
	public void setDtInicioRastreabilidade(Date dtInicioRastreabilidade) {
		this.dtInicioRastreabilidade = dtInicioRastreabilidade;
	}
	
	/**
	 * Get: dtFimRastreabilidadeConv.
	 *
	 * @return dtFimRastreabilidadeConv
	 */
	public String getDtFimRastreabilidadeConv() {
		return DateUtils.formartarData(dtFimRastreabilidade, "dd.MM.yyyy");
	}

	/**
	 * Get: dtInicioRastreabilidadeConv.
	 *
	 * @return dtInicioRastreabilidadeConv
	 */
	public String getDtInicioRastreabilidadeConv() {
		return DateUtils.formartarData(dtInicioRastreabilidade, "dd.MM.yyyy");
	}
}
