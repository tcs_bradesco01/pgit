/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;


/**
 * Nome: ListarHistAssocTrocaArqParticipanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistAssocTrocaArqParticipanteSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdClub. */
	private Long cdClub;
    
    /** Atributo cdCorpoCpfCnpj. */
    private Long cdCorpoCpfCnpj;
    
    /** Atributo cdCnpjContrato. */
    private Integer cdCnpjContrato;
    
    /** Atributo cdCpfCnpjDigito. */
    private Integer cdCpfCnpjDigito;    
	
	/** Atributo cdRazaoSocial. */
	private String cdRazaoSocial;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo dsTipoParticipante. */
	private String dsTipoParticipante;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo dsLayout. */
	private String dsLayout;
	
	/** Atributo cpfCnpjFormatado. */
	private String cpfCnpjFormatado;
	
	/** Atributo dataHoraFormatada. */
	private String dataHoraFormatada;
	
	/**
	 * Get: dataHoraFormatada.
	 *
	 * @return dataHoraFormatada
	 */
	public String getDataHoraFormatada() {
		return dataHoraFormatada;
	}

	/**
	 * Set: dataHoraFormatada.
	 *
	 * @param dataHoraFormatada the data hora formatada
	 */
	public void setDataHoraFormatada(String dataHoraFormatada) {
		this.dataHoraFormatada = dataHoraFormatada;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}

	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub(){
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub){
		this.cdClub = cdClub;
	}
	
	/**
	 * Get: cdRazaoSocial.
	 *
	 * @return cdRazaoSocial
	 */
	public String getCdRazaoSocial(){
		return cdRazaoSocial;
	}

	/**
	 * Set: cdRazaoSocial.
	 *
	 * @param cdRazaoSocial the cd razao social
	 */
	public void setCdRazaoSocial(String cdRazaoSocial){
		this.cdRazaoSocial = cdRazaoSocial;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa(){
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa){
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo(){
		return cdPerfilTrocaArquivo;
	}

	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo){
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro(){
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro){
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro(){
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro){
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdCnpjContrato.
	 *
	 * @return cdCnpjContrato
	 */
	public Integer getCdCnpjContrato() {
		return cdCnpjContrato;
	}

	/**
	 * Set: cdCnpjContrato.
	 *
	 * @param cdCnpjContrato the cd cnpj contrato
	 */
	public void setCdCnpjContrato(Integer cdCnpjContrato) {
		this.cdCnpjContrato = cdCnpjContrato;
	}

	/**
	 * Get: cdCpfCnpjDigito.
	 *
	 * @return cdCpfCnpjDigito
	 */
	public Integer getCdCpfCnpjDigito() {
		return cdCpfCnpjDigito;
	}

	/**
	 * Set: cdCpfCnpjDigito.
	 *
	 * @param cdCpfCnpjDigito the cd cpf cnpj digito
	 */
	public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito) {
		this.cdCpfCnpjDigito = cdCpfCnpjDigito;
	}

	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}

	/**
	 * Get: dsLayout.
	 *
	 * @return dsLayout
	 */
	public String getDsLayout() {
		return dsLayout;
	}

	/**
	 * Set: dsLayout.
	 *
	 * @param dsLayout the ds layout
	 */
	public void setDsLayout(String dsLayout) {
		this.dsLayout = dsLayout;
	}

	/**
	 * Get: dsTipoParticipante.
	 *
	 * @return dsTipoParticipante
	 */
	public String getDsTipoParticipante() {
		return dsTipoParticipante;
	}

	/**
	 * Set: dsTipoParticipante.
	 *
	 * @param dsTipoParticipante the ds tipo participante
	 */
	public void setDsTipoParticipante(String dsTipoParticipante) {
		this.dsTipoParticipante = dsTipoParticipante;
	}

	/**
	 * Get: cdDsTipoParticipante.
	 *
	 * @return cdDsTipoParticipante
	 */
	public String getCdDsTipoParticipante() {
		StringBuilder cdDsTipoParticipante = new StringBuilder();
		if (cdTipoLayoutArquivo != null) {
			cdDsTipoParticipante.append(cdTipoParticipacaoPessoa);
		}
		cdDsTipoParticipante.append(" - ");
		if (dsTipoParticipante != null) {
			cdDsTipoParticipante.append(dsTipoParticipante);
		}
		return cdDsTipoParticipante.toString();
	}
}