/*
 * Nome: br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPosDiaPagtoAgenciaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPosDiaPagtoAgenciaSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;
    
    /** Atributo cdCorpoCnpjCpf. */
    private Long cdCorpoCnpjCpf;
    
    /** Atributo cdCnpjCpfFilial. */
    private Integer cdCnpjCpfFilial;
    
    /** Atributo cdCnpjCpfDigito. */
    private Integer cdCnpjCpfDigito;
    
    /** Atributo cdPessoa. */
    private Long cdPessoa;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
    
    /** Atributo cdEmpresa. */
    private Long cdEmpresa;
    
    /** Atributo dsEmpresa. */
    private String dsEmpresa;
    
    /** Atributo cdTipoContratoRec. */
    private Integer cdTipoContratoRec;
    
    /** Atributo dsTipoContratoNegocio. */
    private String dsTipoContratoNegocio;
    
    /** Atributo cdContratoOrigemRec. */
    private Long cdContratoOrigemRec;
    
    /** Atributo cdServico. */
    private Integer cdServico;
    
    /** Atributo dsServico. */
    private String dsServico;
    
    /** Atributo cdModalidade. */
    private Integer cdModalidade;
    
    /** Atributo dsModalidade. */
    private String dsModalidade;
    
    /** Atributo vlPagamentoAgendado. */
    private BigDecimal vlPagamentoAgendado;
    
    /** Atributo vlPagamentoPendente. */
    private BigDecimal vlPagamentoPendente;
    
    /** Atributo vlPagamentoSemSaldo. */
    private BigDecimal vlPagamentoSemSaldo;
    
    /** Atributo vlPagamentoEfetivado. */
    private BigDecimal vlPagamentoEfetivado;
    
    /** Atributo vlTotalPagamentos. */
    private BigDecimal vlTotalPagamentos;
    
    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;
    
    /** Atributo vlPagtAgenFormatado. */
    private String vlPagtAgenFormatado;
    
    /** Atributo vlPagtPendFormatado. */
    private String vlPagtPendFormatado;
    
    /** Atributo vlPagtSemSaldoFormatado. */
    private String vlPagtSemSaldoFormatado;
    
    /** Atributo vlPagtEfetFormatado. */
    private String vlPagtEfetFormatado;
    
    /** Atributo vlTotalPagtFormatado. */
    private String vlTotalPagtFormatado;
    
	/**
	 * Get: vlPagtEfetFormatado.
	 *
	 * @return vlPagtEfetFormatado
	 */
	public String getVlPagtEfetFormatado() {
		return vlPagtEfetFormatado;
	}
	
	/**
	 * Set: vlPagtEfetFormatado.
	 *
	 * @param vlPagtEfetFormatado the vl pagt efet formatado
	 */
	public void setVlPagtEfetFormatado(String vlPagtEfetFormatado) {
		this.vlPagtEfetFormatado = vlPagtEfetFormatado;
	}
	
	/**
	 * Get: vlPagtSemSaldoFormatado.
	 *
	 * @return vlPagtSemSaldoFormatado
	 */
	public String getVlPagtSemSaldoFormatado() {
		return vlPagtSemSaldoFormatado;
	}
	
	/**
	 * Set: vlPagtSemSaldoFormatado.
	 *
	 * @param vlPagtSemSaldoFormatado the vl pagt sem saldo formatado
	 */
	public void setVlPagtSemSaldoFormatado(String vlPagtSemSaldoFormatado) {
		this.vlPagtSemSaldoFormatado = vlPagtSemSaldoFormatado;
	}
	
	/**
	 * Get: vlTotalPagtFormatado.
	 *
	 * @return vlTotalPagtFormatado
	 */
	public String getVlTotalPagtFormatado() {
		return vlTotalPagtFormatado;
	}
	
	/**
	 * Set: vlTotalPagtFormatado.
	 *
	 * @param vlTotalPagtFormatado the vl total pagt formatado
	 */
	public void setVlTotalPagtFormatado(String vlTotalPagtFormatado) {
		this.vlTotalPagtFormatado = vlTotalPagtFormatado;
	}
	
	/**
	 * Get: vlPagtPendFormatado.
	 *
	 * @return vlPagtPendFormatado
	 */
	public String getVlPagtPendFormatado() {
		return vlPagtPendFormatado;
	}
	
	/**
	 * Set: vlPagtPendFormatado.
	 *
	 * @param vlPagtPendFormatado the vl pagt pend formatado
	 */
	public void setVlPagtPendFormatado(String vlPagtPendFormatado) {
		this.vlPagtPendFormatado = vlPagtPendFormatado;
	}
	
	/**
	 * Get: vlPagtAgenFormatado.
	 *
	 * @return vlPagtAgenFormatado
	 */
	public String getVlPagtAgenFormatado() {
		return vlPagtAgenFormatado;
	}
	
	/**
	 * Set: vlPagtAgenFormatado.
	 *
	 * @param vlPagtAgenFormatado the vl pagt agen formatado
	 */
	public void setVlPagtAgenFormatado(String vlPagtAgenFormatado) {
		this.vlPagtAgenFormatado = vlPagtAgenFormatado;
	}
	
	/**
	 * Get: cdCnpjCpfDigito.
	 *
	 * @return cdCnpjCpfDigito
	 */
	public Integer getCdCnpjCpfDigito() {
		return cdCnpjCpfDigito;
	}
	
	/**
	 * Set: cdCnpjCpfDigito.
	 *
	 * @param cdCnpjCpfDigito the cd cnpj cpf digito
	 */
	public void setCdCnpjCpfDigito(Integer cdCnpjCpfDigito) {
		this.cdCnpjCpfDigito = cdCnpjCpfDigito;
	}
	
	/**
	 * Get: cdCnpjCpfFilial.
	 *
	 * @return cdCnpjCpfFilial
	 */
	public Integer getCdCnpjCpfFilial() {
		return cdCnpjCpfFilial;
	}
	
	/**
	 * Set: cdCnpjCpfFilial.
	 *
	 * @param cdCnpjCpfFilial the cd cnpj cpf filial
	 */
	public void setCdCnpjCpfFilial(Integer cdCnpjCpfFilial) {
		this.cdCnpjCpfFilial = cdCnpjCpfFilial;
	}
	
	/**
	 * Get: cdContratoOrigemRec.
	 *
	 * @return cdContratoOrigemRec
	 */
	public Long getCdContratoOrigemRec() {
		return cdContratoOrigemRec;
	}
	
	/**
	 * Set: cdContratoOrigemRec.
	 *
	 * @param cdContratoOrigemRec the cd contrato origem rec
	 */
	public void setCdContratoOrigemRec(Long cdContratoOrigemRec) {
		this.cdContratoOrigemRec = cdContratoOrigemRec;
	}
	
	/**
	 * Get: cdCorpoCnpjCpf.
	 *
	 * @return cdCorpoCnpjCpf
	 */
	public Long getCdCorpoCnpjCpf() {
		return cdCorpoCnpjCpf;
	}
	
	/**
	 * Set: cdCorpoCnpjCpf.
	 *
	 * @param cdCorpoCnpjCpf the cd corpo cnpj cpf
	 */
	public void setCdCorpoCnpjCpf(Long cdCorpoCnpjCpf) {
		this.cdCorpoCnpjCpf = cdCorpoCnpjCpf;
	}
	
	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}
	
	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: cdTipoContratoRec.
	 *
	 * @return cdTipoContratoRec
	 */
	public Integer getCdTipoContratoRec() {
		return cdTipoContratoRec;
	}
	
	/**
	 * Set: cdTipoContratoRec.
	 *
	 * @param cdTipoContratoRec the cd tipo contrato rec
	 */
	public void setCdTipoContratoRec(Integer cdTipoContratoRec) {
		this.cdTipoContratoRec = cdTipoContratoRec;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}
	
	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}
	
	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}
	
	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}
	
	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}
	
	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}
	
	/**
	 * Get: vlPagamentoAgendado.
	 *
	 * @return vlPagamentoAgendado
	 */
	public BigDecimal getVlPagamentoAgendado() {
		return vlPagamentoAgendado;
	}
	
	/**
	 * Set: vlPagamentoAgendado.
	 *
	 * @param vlPagamentoAgendado the vl pagamento agendado
	 */
	public void setVlPagamentoAgendado(BigDecimal vlPagamentoAgendado) {
		this.vlPagamentoAgendado = vlPagamentoAgendado;
	}
	
	/**
	 * Get: vlPagamentoEfetivado.
	 *
	 * @return vlPagamentoEfetivado
	 */
	public BigDecimal getVlPagamentoEfetivado() {
		return vlPagamentoEfetivado;
	}
	
	/**
	 * Set: vlPagamentoEfetivado.
	 *
	 * @param vlPagamentoEfetivado the vl pagamento efetivado
	 */
	public void setVlPagamentoEfetivado(BigDecimal vlPagamentoEfetivado) {
		this.vlPagamentoEfetivado = vlPagamentoEfetivado;
	}
	
	/**
	 * Get: vlPagamentoPendente.
	 *
	 * @return vlPagamentoPendente
	 */
	public BigDecimal getVlPagamentoPendente() {
		return vlPagamentoPendente;
	}
	
	/**
	 * Set: vlPagamentoPendente.
	 *
	 * @param vlPagamentoPendente the vl pagamento pendente
	 */
	public void setVlPagamentoPendente(BigDecimal vlPagamentoPendente) {
		this.vlPagamentoPendente = vlPagamentoPendente;
	}
	
	/**
	 * Get: vlPagamentoSemSaldo.
	 *
	 * @return vlPagamentoSemSaldo
	 */
	public BigDecimal getVlPagamentoSemSaldo() {
		return vlPagamentoSemSaldo;
	}
	
	/**
	 * Set: vlPagamentoSemSaldo.
	 *
	 * @param vlPagamentoSemSaldo the vl pagamento sem saldo
	 */
	public void setVlPagamentoSemSaldo(BigDecimal vlPagamentoSemSaldo) {
		this.vlPagamentoSemSaldo = vlPagamentoSemSaldo;
	}
	
	/**
	 * Get: vlTotalPagamentos.
	 *
	 * @return vlTotalPagamentos
	 */
	public BigDecimal getVlTotalPagamentos() {
		return vlTotalPagamentos;
	}
	
	/**
	 * Set: vlTotalPagamentos.
	 *
	 * @param vlTotalPagamentos the vl total pagamentos
	 */
	public void setVlTotalPagamentos(BigDecimal vlTotalPagamentos) {
		this.vlTotalPagamentos = vlTotalPagamentos;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}
	
	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}
    
    
}
