/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean;

/**
 * Nome: ListarServicoEquiparacaoContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicoEquiparacaoContratoEntradaDTO {
	
	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;
    
    /** Atributo cdPessoaJuridicaContrato1. */
    private Long cdPessoaJuridicaContrato1;
    
    /** Atributo cdTipoContratoNegocio1. */
    private Integer cdTipoContratoNegocio1;
    
    /** Atributo nrSequenciaContratoNegocio1. */
    private Long nrSequenciaContratoNegocio1;
    
    /** Atributo cdPessoaJuridicaContrato2. */
    private Long cdPessoaJuridicaContrato2;
    
    /** Atributo cdTipoContratoNegocio2. */
    private Integer cdTipoContratoNegocio2;
    
    /** Atributo nrSequenciaContratoNegocio2. */
    private Long nrSequenciaContratoNegocio2;
	
    /**
     * Get: nrOcorrencias.
     *
     * @return nrOcorrencias
     */
    public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato1.
	 *
	 * @return cdPessoaJuridicaContrato1
	 */
	public Long getCdPessoaJuridicaContrato1() {
		return cdPessoaJuridicaContrato1;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato1.
	 *
	 * @param cdPessoaJuridicaContrato1 the cd pessoa juridica contrato1
	 */
	public void setCdPessoaJuridicaContrato1(Long cdPessoaJuridicaContrato1) {
		this.cdPessoaJuridicaContrato1 = cdPessoaJuridicaContrato1;
	}
	
	/**
	 * Get: cdTipoContratoNegocio1.
	 *
	 * @return cdTipoContratoNegocio1
	 */
	public Integer getCdTipoContratoNegocio1() {
		return cdTipoContratoNegocio1;
	}
	
	/**
	 * Set: cdTipoContratoNegocio1.
	 *
	 * @param cdTipoContratoNegocio1 the cd tipo contrato negocio1
	 */
	public void setCdTipoContratoNegocio1(Integer cdTipoContratoNegocio1) {
		this.cdTipoContratoNegocio1 = cdTipoContratoNegocio1;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio1.
	 *
	 * @return nrSequenciaContratoNegocio1
	 */
	public Long getNrSequenciaContratoNegocio1() {
		return nrSequenciaContratoNegocio1;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio1.
	 *
	 * @param nrSequenciaContratoNegocio1 the nr sequencia contrato negocio1
	 */
	public void setNrSequenciaContratoNegocio1(Long nrSequenciaContratoNegocio1) {
		this.nrSequenciaContratoNegocio1 = nrSequenciaContratoNegocio1;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato2.
	 *
	 * @return cdPessoaJuridicaContrato2
	 */
	public Long getCdPessoaJuridicaContrato2() {
		return cdPessoaJuridicaContrato2;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato2.
	 *
	 * @param cdPessoaJuridicaContrato2 the cd pessoa juridica contrato2
	 */
	public void setCdPessoaJuridicaContrato2(Long cdPessoaJuridicaContrato2) {
		this.cdPessoaJuridicaContrato2 = cdPessoaJuridicaContrato2;
	}
	
	/**
	 * Get: cdTipoContratoNegocio2.
	 *
	 * @return cdTipoContratoNegocio2
	 */
	public Integer getCdTipoContratoNegocio2() {
		return cdTipoContratoNegocio2;
	}
	
	/**
	 * Set: cdTipoContratoNegocio2.
	 *
	 * @param cdTipoContratoNegocio2 the cd tipo contrato negocio2
	 */
	public void setCdTipoContratoNegocio2(Integer cdTipoContratoNegocio2) {
		this.cdTipoContratoNegocio2 = cdTipoContratoNegocio2;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio2.
	 *
	 * @return nrSequenciaContratoNegocio2
	 */
	public Long getNrSequenciaContratoNegocio2() {
		return nrSequenciaContratoNegocio2;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio2.
	 *
	 * @param nrSequenciaContratoNegocio2 the nr sequencia contrato negocio2
	 */
	public void setNrSequenciaContratoNegocio2(Long nrSequenciaContratoNegocio2) {
		this.nrSequenciaContratoNegocio2 = nrSequenciaContratoNegocio2;
	}
    
}
