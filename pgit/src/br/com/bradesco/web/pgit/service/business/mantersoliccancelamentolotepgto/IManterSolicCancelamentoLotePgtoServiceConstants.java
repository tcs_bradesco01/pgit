/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersoliccancelamentolotepgto
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersoliccancelamentolotepgto;

/**
 * Nome: IManterSolicCancelamentoLotePgtoServiceConstants
 * <p>
 * Prop�sito: Interface de constantes do adaptador ManterSolicCancelamentoLotePgto
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IManterSolicCancelamentoLotePgtoServiceConstants {

	static final int QTDE_MAXIMA_OCORRENCIAS = 40;
}