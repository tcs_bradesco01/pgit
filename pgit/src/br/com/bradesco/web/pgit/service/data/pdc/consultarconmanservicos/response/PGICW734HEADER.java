/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class PGICW734HEADER.
 * 
 * @version $Revision$ $Date$
 */
public class PGICW734HEADER implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _PGICW734CODLAYOUT
     */
    private java.lang.String _PGICW734CODLAYOUT = "PGICW734";

    /**
     * Field _PGICW734TAMLAYOUT
     */
    private int _PGICW734TAMLAYOUT = 6048;

    /**
     * keeps track of state for field: _PGICW734TAMLAYOUT
     */
    private boolean _has_PGICW734TAMLAYOUT;


      //----------------/
     //- Constructors -/
    //----------------/

    public PGICW734HEADER() 
     {
        super();
        setPGICW734CODLAYOUT("PGICW734");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.PGICW734HEADER()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deletePGICW734TAMLAYOUT
     * 
     */
    public void deletePGICW734TAMLAYOUT()
    {
        this._has_PGICW734TAMLAYOUT= false;
    } //-- void deletePGICW734TAMLAYOUT() 

    /**
     * Returns the value of field 'PGICW734CODLAYOUT'.
     * 
     * @return String
     * @return the value of field 'PGICW734CODLAYOUT'.
     */
    public java.lang.String getPGICW734CODLAYOUT()
    {
        return this._PGICW734CODLAYOUT;
    } //-- java.lang.String getPGICW734CODLAYOUT() 

    /**
     * Returns the value of field 'PGICW734TAMLAYOUT'.
     * 
     * @return int
     * @return the value of field 'PGICW734TAMLAYOUT'.
     */
    public int getPGICW734TAMLAYOUT()
    {
        return this._PGICW734TAMLAYOUT;
    } //-- int getPGICW734TAMLAYOUT() 

    /**
     * Method hasPGICW734TAMLAYOUT
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPGICW734TAMLAYOUT()
    {
        return this._has_PGICW734TAMLAYOUT;
    } //-- boolean hasPGICW734TAMLAYOUT() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'PGICW734CODLAYOUT'.
     * 
     * @param PGICW734CODLAYOUT the value of field
     * 'PGICW734CODLAYOUT'.
     */
    public void setPGICW734CODLAYOUT(java.lang.String PGICW734CODLAYOUT)
    {
        this._PGICW734CODLAYOUT = PGICW734CODLAYOUT;
    } //-- void setPGICW734CODLAYOUT(java.lang.String) 

    /**
     * Sets the value of field 'PGICW734TAMLAYOUT'.
     * 
     * @param PGICW734TAMLAYOUT the value of field
     * 'PGICW734TAMLAYOUT'.
     */
    public void setPGICW734TAMLAYOUT(int PGICW734TAMLAYOUT)
    {
        this._PGICW734TAMLAYOUT = PGICW734TAMLAYOUT;
        this._has_PGICW734TAMLAYOUT = true;
    } //-- void setPGICW734TAMLAYOUT(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return PGICW734HEADER
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.PGICW734HEADER unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.PGICW734HEADER) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.PGICW734HEADER.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.PGICW734HEADER unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
