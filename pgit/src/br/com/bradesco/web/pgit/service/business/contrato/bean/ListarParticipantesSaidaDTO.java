/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

import java.util.List;

/**
 * Nome: ListarParticipantesSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipantesSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem = null;

    /** The mensagem. */
    private String mensagem = null;

    /** The nr participantes. */
    private Integer nrParticipantes = null;

    /** The ocorrencias. */
    private List<ListarParticipantesOcorrenciasSaidaDTO> ocorrencias = null;

    /**
     * Instancia um novo ListarParticipantesSaidaDTO
     *
     * @see
     */
    public ListarParticipantesSaidaDTO() {
        super();
    }

    /**
     * Sets the cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Gets the cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Sets the mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Gets the mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Sets the nr participantes.
     *
     * @param nrParticipantes the nr participantes
     */
    public void setNrParticipantes(Integer nrParticipantes) {
        this.nrParticipantes = nrParticipantes;
    }

    /**
     * Gets the nr participantes.
     *
     * @return the nr participantes
     */
    public Integer getNrParticipantes() {
        return this.nrParticipantes;
    }

    /**
     * Sets the ocorrencias.
     *
     * @param ocorrencias the ocorrencias
     */
    public void setOcorrencias(List<ListarParticipantesOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    /**
     * Gets the ocorrencias.
     *
     * @return the ocorrencias
     */
    public List<ListarParticipantesOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}