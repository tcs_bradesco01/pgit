/*
 * Nome: br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean;

/**
 * Nome: DesautorizarIntegralEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DesautorizarIntegralEntradaDTO {
	
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
    
    /** Atributo cdCorpoCnpjCpf. */
    private Long cdCorpoCnpjCpf;
    
    /** Atributo cdFilialCnpjCpf. */
    private Integer cdFilialCnpjCpf;
    
    /** Atributo cdControleCnpjCpf. */
    private Integer cdControleCnpjCpf;
    
    /** Atributo nrSequenciaArquivoRemessa. */
    private Long nrSequenciaArquivoRemessa;
    
    /** Atributo hrInclusaoRemessaSistema. */
    private String hrInclusaoRemessaSistema;
    
    /** Atributo dtAgendamentoPagamento. */
    private String dtAgendamentoPagamento;
    
    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo cdSituacao. */
    private Integer cdSituacao;
    
    
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdControleCnpjCpf.
	 *
	 * @return cdControleCnpjCpf
	 */
	public Integer getCdControleCnpjCpf() {
		return cdControleCnpjCpf;
	}
	
	/**
	 * Set: cdControleCnpjCpf.
	 *
	 * @param cdControleCnpjCpf the cd controle cnpj cpf
	 */
	public void setCdControleCnpjCpf(Integer cdControleCnpjCpf) {
		this.cdControleCnpjCpf = cdControleCnpjCpf;
	}
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdCorpoCnpjCpf.
	 *
	 * @return cdCorpoCnpjCpf
	 */
	public Long getCdCorpoCnpjCpf() {
		return cdCorpoCnpjCpf;
	}
	
	/**
	 * Set: cdCorpoCnpjCpf.
	 *
	 * @param cdCorpoCnpjCpf the cd corpo cnpj cpf
	 */
	public void setCdCorpoCnpjCpf(Long cdCorpoCnpjCpf) {
		this.cdCorpoCnpjCpf = cdCorpoCnpjCpf;
	}
	
	/**
	 * Get: cdFilialCnpjCpf.
	 *
	 * @return cdFilialCnpjCpf
	 */
	public Integer getCdFilialCnpjCpf() {
		return cdFilialCnpjCpf;
	}
	
	/**
	 * Set: cdFilialCnpjCpf.
	 *
	 * @param cdFilialCnpjCpf the cd filial cnpj cpf
	 */
	public void setCdFilialCnpjCpf(Integer cdFilialCnpjCpf) {
		this.cdFilialCnpjCpf = cdFilialCnpjCpf;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}
	
	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtAgendamentoPagamento.
	 *
	 * @return dtAgendamentoPagamento
	 */
	public String getDtAgendamentoPagamento() {
		return dtAgendamentoPagamento;
	}
	
	/**
	 * Set: dtAgendamentoPagamento.
	 *
	 * @param dtAgendamentoPagamento the dt agendamento pagamento
	 */
	public void setDtAgendamentoPagamento(String dtAgendamentoPagamento) {
		this.dtAgendamentoPagamento = dtAgendamentoPagamento;
	}
	
	/**
	 * Get: hrInclusaoRemessaSistema.
	 *
	 * @return hrInclusaoRemessaSistema
	 */
	public String getHrInclusaoRemessaSistema() {
		return hrInclusaoRemessaSistema;
	}
	
	/**
	 * Set: hrInclusaoRemessaSistema.
	 *
	 * @param hrInclusaoRemessaSistema the hr inclusao remessa sistema
	 */
	public void setHrInclusaoRemessaSistema(String hrInclusaoRemessaSistema) {
		this.hrInclusaoRemessaSistema = hrInclusaoRemessaSistema;
	}
	
	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public Long getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}
	
	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(Long nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
    
    

}
