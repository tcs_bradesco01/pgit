/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.IManterSolicGeracaoArquivoRetBaseFavService;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.IManterSolicGeracaoArquivoRetBaseFavServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.DetalharSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.DetalharSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ExcluirSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ExcluirSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.IncluirSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.IncluirSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ListarSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ListarSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.request.DetalharSolBaseFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.response.DetalharSolBaseFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasefavorecido.request.ExcluirSolBaseFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasefavorecido.response.ExcluirSolBaseFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasefavorecido.request.IncluirSolBaseFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasefavorecido.response.IncluirSolBaseFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasefavorecido.request.ListarSolBaseFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasefavorecido.response.ListarSolBaseFavorecidoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterSolicGeracaoArquivoRetBaseFav
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterSolicGeracaoArquivoRetBaseFavServiceImpl implements IManterSolicGeracaoArquivoRetBaseFavService {
	
	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;		
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public ManterSolicGeracaoArquivoRetBaseFavServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.IManterSolicGeracaoArquivoRetBaseFavService#listarSolBaseFavorecido(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ListarSolBaseFavorecidoEntradaDTO)
     */
    public List<ListarSolBaseFavorecidoSaidaDTO> listarSolBaseFavorecido (ListarSolBaseFavorecidoEntradaDTO entradaDTO){
    	List<ListarSolBaseFavorecidoSaidaDTO> listaRetorno = new ArrayList<ListarSolBaseFavorecidoSaidaDTO>();
    	ListarSolBaseFavorecidoRequest request = new ListarSolBaseFavorecidoRequest();
    	ListarSolBaseFavorecidoResponse response = new ListarSolBaseFavorecidoResponse();
    	    	
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setDtSolicitacaoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtSolicitacaoInicio()));
        request.setDtSolicitacaoFim(PgitUtil.verificaStringNula(entradaDTO.getDtSolicitacaoFim()));
        request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
        request.setCdSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacao()));
        request.setCdOrigemSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemSolicitacao()));        
        request.setNumeroOcorrencias(IManterSolicGeracaoArquivoRetBaseFavServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);                
        
        response = getFactoryAdapter().getListarSolBaseFavorecidoPDCAdapter().invokeProcess(request);
        
        ListarSolBaseFavorecidoSaidaDTO saidaDTO;
        for(int i = 0;i<response.getOcorrenciasCount();i++){
        	saidaDTO = new ListarSolBaseFavorecidoSaidaDTO();
        	
        	saidaDTO.setCodMensagem(response.getCodMensagem());
        	saidaDTO.setMensagem(response.getMensagem());        	
        	saidaDTO.setNrSolicitacaoPagamento(response.getOcorrencias(i).getNrSolicitacaoPagamento());
        	saidaDTO.setHrSolicitacao(response.getOcorrencias(i).getHrSolicitacao());
        	saidaDTO.setHrSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrSolicitacao()));
        	saidaDTO.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
        	saidaDTO.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
        	saidaDTO.setResultadoProcessamento(response.getOcorrencias(i).getResultadoProcessamento());
        	saidaDTO.setHrAtendimentoSolicitacao(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrAtendimentoSolicitacao()));
        	saidaDTO.setHrAtendimentoSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrAtendimentoSolicitacao()));
        	saidaDTO.setQtdeRegistrosSolicitacao(response.getOcorrencias(i).getQtdeRegistrosSolicitacao());	
        	saidaDTO.setQtdeRegistrosSolicitacaoFormatada(nf.format(saidaDTO.getQtdeRegistrosSolicitacao()));        	
            
            listaRetorno.add(saidaDTO);
        }
    	
    	return listaRetorno;    	
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.IManterSolicGeracaoArquivoRetBaseFavService#detalharSolBaseFavorecido(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.DetalharSolBaseFavorecidoEntradaDTO)
     */
    public DetalharSolBaseFavorecidoSaidaDTO detalharSolBaseFavorecido(DetalharSolBaseFavorecidoEntradaDTO entradaDTO) {
		
    	DetalharSolBaseFavorecidoRequest request = new DetalharSolBaseFavorecidoRequest();
    	DetalharSolBaseFavorecidoResponse response = new DetalharSolBaseFavorecidoResponse();		
		
    	request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
				
    	DetalharSolBaseFavorecidoSaidaDTO saidaDTO = new DetalharSolBaseFavorecidoSaidaDTO();
    	
		response = getFactoryAdapter().getDetalharSolBaseFavorecidoPDCAdapter().invokeProcess(request);				
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
	    saidaDTO.setDsTipoFavorecido(response.getDsTipoFavorecido());
	    saidaDTO.setDsSituacaoFavorecido(response.getDsSituacaoFavorecido());
	    saidaDTO.setCdDestino(response.getCdDestino());
	    saidaDTO.setNrSolicitacaoPagamento(response.getNrSolicitacaoPagamento());
	    saidaDTO.setHrSolicitacao(response.getHrSolicitacao());
	    saidaDTO.setHrSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getHrSolicitacao()));
	    saidaDTO.setDsSituacaoSolicitacao(response.getDsSituacaoSolicitacao());
	    saidaDTO.setResultadoProcessamento(response.getResultadoProcessamento());
	    saidaDTO.setHrAtendimentoSolicitacao(response.getHrAtendimentoSolicitacao());
	    saidaDTO.setHrAtendimentoSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getHrAtendimentoSolicitacao()));	    
	    saidaDTO.setQtdeRegistroSolicitacao(response.getQtdeRegistroSolicitacao());
	    saidaDTO.setTarifaPadrao(response.getVlTarifaPadrao());
	    saidaDTO.setDescontoTarifa(response.getNumPercentualDescTarifa());
	    saidaDTO.setTarifaAtual(response.getVlrTarifaAtual());	    
	    saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
	    saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
	    saidaDTO.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
	    saidaDTO.setNmOperacaoFluxo(response.getNmOperacaoFluxo());
	    saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
	    saidaDTO.setHrInclusaoRegistroFormatada(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
	    saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
	    saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
	    saidaDTO.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
	    saidaDTO.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
	    saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());   
	    saidaDTO.setHrManutencaoRegistroFormatada(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
	    
	    return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.IManterSolicGeracaoArquivoRetBaseFavService#incluirSolBaseFavorecido(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.IncluirSolBaseFavorecidoEntradaDTO)
     */
    public IncluirSolBaseFavorecidoSaidaDTO incluirSolBaseFavorecido(IncluirSolBaseFavorecidoEntradaDTO entradaDTO) {
		
    	IncluirSolBaseFavorecidoRequest request = new IncluirSolBaseFavorecidoRequest();
    	IncluirSolBaseFavorecidoResponse response = new IncluirSolBaseFavorecidoResponse();		
		    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
	    request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	    request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	    request.setCdTipoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoFavorecido()));
	    request.setCdSituacaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoFavorecido()));
	    request.setCdDestino(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDestino()));
	    request.setVlTarifaPadrao(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlTarifaPadrao()));
	    request.setNumPercentualDescTarifa(PgitUtil.verificaBigDecimalNulo(entradaDTO.getNumPercentualDescTarifa()));
	    request.setVlrTarifaAtual(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlrTarifaAtual()));
    					
	    IncluirSolBaseFavorecidoSaidaDTO saidaDTO = new IncluirSolBaseFavorecidoSaidaDTO();
    	
		response = getFactoryAdapter().getIncluirSolBaseFavorecidoPDCAdapter().invokeProcess(request);	
			
		saidaDTO.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
		saidaDTO.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));		
	    
	    return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.IManterSolicGeracaoArquivoRetBaseFavService#excluirSolBaseFavorecido(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ExcluirSolBaseFavorecidoEntradaDTO)
     */
    public ExcluirSolBaseFavorecidoSaidaDTO excluirSolBaseFavorecido(ExcluirSolBaseFavorecidoEntradaDTO entradaDTO) {
		
    	ExcluirSolBaseFavorecidoRequest request = new ExcluirSolBaseFavorecidoRequest();
    	ExcluirSolBaseFavorecidoResponse response = new ExcluirSolBaseFavorecidoResponse();		
    	
    	request.setNrSolicitacaoGeracaoRetorno(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoGeracaoRetorno()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		    	    					
    	ExcluirSolBaseFavorecidoSaidaDTO saidaDTO = new ExcluirSolBaseFavorecidoSaidaDTO();
    	
		response = getFactoryAdapter().getExcluirSolBaseFavorecidoPDCAdapter().invokeProcess(request);	
			
		saidaDTO.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
		saidaDTO.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));		
	    
	    return saidaDTO;
	}    
}

