/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.relatoriofavorecido.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.IRelatorioFavorecidoService;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.IRelatorioFavorecidoServiceConstants;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ConRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ConRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.DetRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.DetRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ExcRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.IncRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.IncRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.request.ConsultarDescricaoUnidadeOrganizacionalRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.response.ConsultarDescricaoUnidadeOrganizacionalResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.request.DetalharSolicitacaoRelatorioFavorecidosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.response.DetalharSolicitacaoRelatorioFavorecidosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaorelatoriofavorecidos.request.ExcluirSolicitacaoRelatorioFavorecidosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaorelatoriofavorecidos.response.ExcluirSolicitacaoRelatorioFavorecidosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.request.IncluirSolicitacaoRelatorioFavorecidosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.response.IncluirSolicitacaoRelatorioFavorecidosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.request.ListarSolicitacaoRelatorioFavorecidosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.response.ListarSolicitacaoRelatorioFavorecidosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: RelatorioFavorecido
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class RelatorioFavorecidoServiceImpl implements IRelatorioFavorecidoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Relatorio favorecido service impl.
     */
    public RelatorioFavorecidoServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.relatoriofavorecido.IRelatorioFavorecidoService#pesquisaRelFavorecido(br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ConRelFavorecidoEntradaDTO)
     */
    public List <ConRelFavorecidoSaidaDTO> pesquisaRelFavorecido(ConRelFavorecidoEntradaDTO entrada)
    {
    	ListarSolicitacaoRelatorioFavorecidosRequest request = new ListarSolicitacaoRelatorioFavorecidosRequest();
    	
    	request.setTpSolicitacao(entrada.getTpSolicitacao());
    	
    	if(entrada.getDtInicioSolicitacao() == null){
    		request.setDtInicioSolicitacao(IRelatorioFavorecidoServiceConstants.EMPTY);
    	}else{
    		request.setDtInicioSolicitacao(entrada.getDtInicioSolicitacaoConv());
    	}
    	
    	if(entrada.getDtFimSolicitacao() == null){
    		request.setDtFimSolicitacao(IRelatorioFavorecidoServiceConstants.EMPTY);
    	}else{
    		request.setDtFimSolicitacao(entrada.getDtFimSolicitacaoConv());
    	}
    	
    	request.setNumeroOcorrencias(50);
    	
    	ListarSolicitacaoRelatorioFavorecidosResponse response = getFactoryAdapter().getListarSolicitacaoRelatorioFavorecidosPDCAdapter().invokeProcess(request);
    	List <ConRelFavorecidoSaidaDTO> list = new ArrayList<ConRelFavorecidoSaidaDTO>(); 
    	
    	for (Ocorrencias saida : response.getOcorrencias()) {
			list.add(new ConRelFavorecidoSaidaDTO(saida));
		}
    	
    	return list;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.relatoriofavorecido.IRelatorioFavorecidoService#pesquisarDetalheRelFavorecido(br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.DetRelFavorecidoEntradaDTO)
     */
    public DetRelFavorecidoSaidaDTO pesquisarDetalheRelFavorecido(DetRelFavorecidoEntradaDTO entrada) {
		
    	DetalharSolicitacaoRelatorioFavorecidosRequest request = new DetalharSolicitacaoRelatorioFavorecidosRequest();
    	
    	request.setNrSolicitacao(entrada.getNrSolicitacao());
    	request.setCdTipoSolicitacao(entrada.getTpSolicitacao());
    	
    	DetalharSolicitacaoRelatorioFavorecidosResponse response = getFactoryAdapter().getDetalharSolicitacaoRelatorioFavorecidosPDCAdapter().invokeProcess(request);
    	
    	DetRelFavorecidoSaidaDTO retorno = new DetRelFavorecidoSaidaDTO();
    
    	
    	retorno.setCdAtividadeEconomica(response.getCdAtividadeEconomica());
    	retorno.setCdClassAtividade(response.getCdClassAtividade());
    	retorno.setCdGrupoEconomico(response.getCdGrupoEconomico());
    	retorno.setCdMotivoSituacaoSolicitacao(response.getCdMotivoSituacaoSolicitacao());
    	retorno.setCdOperCanalInclusao(response.getCdOperCanalInclusao());
    	retorno.setCdOperCanalManutencao(response.getCdOperCanalManutencao());
    	retorno.setCdPessoaJuridica(response.getCdPessoaJuridica());
    	retorno.setCdPessoaJuridicaDir(response.getCdPessoaJuridicaDir());
    	retorno.setCdPessoaJuridicaGerc(response.getCdPessoaJuridicaGerc());
    	retorno.setCdProdutoOperRelacionado(response.getCdProdutoOperRelacionado());
    	retorno.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
    	retorno.setCdRamoAtividade(response.getCdRamoAtividade());
    	retorno.setCdRamoAtividadeEconomica(response.getCdRamoAtividadeEconomica());
    	retorno.setCdRelacionamentoProduto(response.getCdRelacionamentoProduto());
    	retorno.setCdSegmentoCliente(response.getCdSegmentoCliente());
    	retorno.setCdSituacaoSolicitacaoPagamento(response.getCdSituacaoSolicitacaoPagamento());
    	retorno.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
    	retorno.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
    	retorno.setCdTipoRelatorio(response.getCdTipoRelatorio());
    	retorno.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
    	retorno.setCdUsuarioInclusaoExter(response.getCdUsuarioInclusaoExter());
    	retorno.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
    	retorno.setCdUsuarioManutencaoExter(response.getCdUsuarioManutencaoExter());
    	retorno.setDsAtividadeEconomica(response.getDsAtividadeEconomica());
    	retorno.setDsCanalInclusao(response.getDsCanalInclusao());
    	retorno.setDsCanalManutencao(response.getDsCanalManutencao());
    	retorno.setDsClassAtividade(response.getDsClassAtividade());
    	retorno.setDsGrupoEconomico(response.getDsGrupoEconomico());
    	retorno.setDsMotivoSolicitacao(response.getDsMotivoSolicitacao());
    	retorno.setDsOrganizacaoOperacional(response.getDsOrganizacaoOperacional());
    	retorno.setDsOrganizacionalGerc(response.getDsOrganizacionalGerc());
    	retorno.setDsProdutoOperRelacionado(response.getDsProdutoOperRelacionado());
    	retorno.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
    	retorno.setDsRamoAtividade(response.getDsRamoAtividade());
    	retorno.setDsRamoAtividadeEconomica(response.getDsRamoAtividadeEconomica());
    	retorno.setDsSegmentoCliente(response.getDsSegmentoCliente());    	
    	retorno.setDsSolicitacaoPagamento(response.getDsSolicitacaoPagamento());
    	retorno.setDsTipoRelatorio(response.getDsTipoRelatorio());
    	retorno.setDsUnidadeOrganizacionalDir(response.getDsUnidadeOrganizacionalDir());
    	retorno.setDtAtendimento(response.getDtAtendimento());
    	retorno.setDtInclusao(response.getDtInclusao());
    	retorno.setDtManutencao(response.getDtManutencao());
    	retorno.setDtSolicitacao(response.getDtSolicitacao());
    	retorno.setHrAtendimento(response.getHrAtendimento());
    	retorno.setHrInclusao(response.getHrInclusao());
    	retorno.setHrManutencao(response.getHrManutencao());
    	retorno.setHrSolicitacao(response.getHrSolicitacao());
    	retorno.setNrSeqUnidadeOrganizacionalDir(response.getNrSeqUnidadeOrganizacionalDir());
    	retorno.setNrTarifaBonificacao(response.getNrTarifaBonificacao());
    	retorno.setNrUnidadeOrganizacional(response.getNrUnidadeOrganizacional());
    	retorno.setNrUnidadeOrganizacionalGerc(response.getNrUnidadeOrganizacionalGerc());
    	
    	retorno.setDsClasseRamo(PgitUtil.concatenarCampos(response.getCdClassAtividade(), PgitUtil.concatenarCampos(response.getCdRamoAtividadeEconomica(), response.getDsRamoAtividadeEconomica()), "/"));
    	retorno.setDsSubRamoAtividade(PgitUtil.concatenarCampos(response.getCdRamoAtividade(), PgitUtil.concatenarCampos(response.getCdAtividadeEconomica(), response.getDsAtividadeEconomica()), "/"));
    	
    	retorno.setDsEmpresa(response.getDsEmpresa());
    	
    	return  retorno;
	}
    
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.relatoriofavorecido.IRelatorioFavorecidoService#excluirMotivoBloqueioFavorecido(java.lang.Integer)
     */
    public ExcRelFavorecidoSaidaDTO excluirMotivoBloqueioFavorecido(Integer numSimulacao) {
		
    	ExcluirSolicitacaoRelatorioFavorecidosRequest request = new ExcluirSolicitacaoRelatorioFavorecidosRequest();
    	
    	request.setNrSolicitacao(numSimulacao);
    	request.setCdSolicitacaoPagamento(3);
    	
    	ExcluirSolicitacaoRelatorioFavorecidosResponse response = getFactoryAdapter().getExcluirSolicitacaoRelatorioFavorecidosPDCAdapter().invokeProcess(request);
    	
    	return new ExcRelFavorecidoSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.relatoriofavorecido.IRelatorioFavorecidoService#incluirMotivoBloqueioFavorecido(br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.IncRelFavorecidoEntradaDTO)
     */
    public IncRelFavorecidoSaidaDTO incluirMotivoBloqueioFavorecido(IncRelFavorecidoEntradaDTO inclusao) {
		
    	IncluirSolicitacaoRelatorioFavorecidosRequest request = new IncluirSolicitacaoRelatorioFavorecidosRequest();
    	
    	request.setTpRelatorio(inclusao.getTpRelatorio() == null ? 0 :inclusao.getTpRelatorio());
    	request.setCdEmpresaConglomerado(inclusao.getTpEmpresaConglomerada() == null ? 0 :inclusao.getTpEmpresaConglomerada());
    	request.setCdDiretoriaRegional(inclusao.getTpDiretoriaRegional() == null ? 0 :inclusao.getTpDiretoriaRegional());
    	request.setCdGerenciaRegional(inclusao.getTpGerenciaRegional() == null ? 0 :inclusao.getTpGerenciaRegional());
    	request.setCdAgenciaOperadora(inclusao.getCdAgenciaOperadora() == null ? 0 : inclusao.getCdAgenciaOperadora()); 
    	request.setCdSegmento(inclusao.getCdSegmento() == null ? 0 :inclusao.getCdSegmento());
    	request.setCdGrupoEconomico(inclusao.getCdParticipanteGrupoEconomico() == null ? 0 :inclusao.getCdParticipanteGrupoEconomico());
    	request.setCdClassAtividade(inclusao.getCdClasse() == null ? "" :inclusao.getCdClasse());
    	request.setCdRamoAtividade(inclusao.getCdRamo() == null ? 0 :inclusao.getCdRamo());
    	request.setCdSubRamoAtividade(inclusao.getCdSubramo() == null ? 0 :inclusao.getCdSubramo());
    	request.setCdAtividadeEconomica(inclusao.getCdAtividadeEconomica() == null ? 0 :inclusao.getCdAtividadeEconomica());
    	
    	IncluirSolicitacaoRelatorioFavorecidosResponse response = getFactoryAdapter().getIncluirSolicitacaoRelatorioFavorecidosPDCAdapter().invokeProcess(request);
    	
    	return new IncRelFavorecidoSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.relatoriofavorecido.IRelatorioFavorecidoService#descricaoUnidadeOrganizacional(br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalEntradaDTO)
	 */
	public ConsultarDescricaoUnidOrganizacionalSaidaDTO descricaoUnidadeOrganizacional(ConsultarDescricaoUnidOrganizacionalEntradaDTO descricaoUnidOrgEntradaDTO) {
		ConsultarDescricaoUnidadeOrganizacionalRequest detalharUnidOrgRequest = new ConsultarDescricaoUnidadeOrganizacionalRequest();
		ConsultarDescricaoUnidadeOrganizacionalResponse detalharUnidOrgResponse = new ConsultarDescricaoUnidadeOrganizacionalResponse();
		
		detalharUnidOrgRequest.setCdPessoaJuridica(descricaoUnidOrgEntradaDTO.getCdPessoaJuridica());		
		detalharUnidOrgRequest.setCdTipoUnidadeOrganizacional(descricaoUnidOrgEntradaDTO.getCdTipoUnidadeOrganizacional());
		detalharUnidOrgRequest.setNrSequenciaUnidadeOrganizacional(descricaoUnidOrgEntradaDTO.getNrSequenciaUnidadeOrganizacional());
		
		detalharUnidOrgResponse = getFactoryAdapter().getConsultarDescricaoUnidadeOrganizacionalPDCAdapter().invokeProcess(detalharUnidOrgRequest);

		ConsultarDescricaoUnidOrganizacionalSaidaDTO detalharUnidOrgSaidaDTO = new ConsultarDescricaoUnidOrganizacionalSaidaDTO();
			
		detalharUnidOrgSaidaDTO.setCodMensagem(detalharUnidOrgResponse.getCodMensagem());
		detalharUnidOrgSaidaDTO.setMensagem(detalharUnidOrgResponse.getMensagem());
		detalharUnidOrgSaidaDTO.setDsNumeroSeqUnidadeOrganizacional(detalharUnidOrgResponse.getDsNumeroSeqUnidadeOrganizacional());
		
		
		return detalharUnidOrgSaidaDTO;	

	}
    
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	
    
    
}

