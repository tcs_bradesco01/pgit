/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdIndicadorParticipante
     */
    private int _cdIndicadorParticipante = 0;

    /**
     * keeps track of state for field: _cdIndicadorParticipante
     */
    private boolean _has_cdIndicadorParticipante;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _cdParticipante
     */
    private long _cdParticipante = 0;

    /**
     * keeps track of state for field: _cdParticipante
     */
    private boolean _has_cdParticipante;

    /**
     * Field _dsNomeParticipante
     */
    private java.lang.String _dsNomeParticipante;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _dsLogradouroParticipante
     */
    private java.lang.String _dsLogradouroParticipante;

    /**
     * Field _nrLogradouroParticipante
     */
    private java.lang.String _nrLogradouroParticipante;

    /**
     * Field _dsBairroClienteParticipante
     */
    private java.lang.String _dsBairroClienteParticipante;

    /**
     * Field _dsMunicipioClienteParticipante
     */
    private java.lang.String _dsMunicipioClienteParticipante;

    /**
     * Field _dsSiglaUfParticipante
     */
    private java.lang.String _dsSiglaUfParticipante;

    /**
     * Field _cdCep
     */
    private int _cdCep = 0;

    /**
     * keeps track of state for field: _cdCep
     */
    private boolean _has_cdCep;

    /**
     * Field _cdCepComplemento
     */
    private int _cdCepComplemento = 0;

    /**
     * keeps track of state for field: _cdCepComplemento
     */
    private boolean _has_cdCepComplemento;

    /**
     * Field _cdIndicadorQuebra
     */
    private java.lang.String _cdIndicadorQuebra;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdCep
     * 
     */
    public void deleteCdCep()
    {
        this._has_cdCep= false;
    } //-- void deleteCdCep() 

    /**
     * Method deleteCdCepComplemento
     * 
     */
    public void deleteCdCepComplemento()
    {
        this._has_cdCepComplemento= false;
    } //-- void deleteCdCepComplemento() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdIndicadorParticipante
     * 
     */
    public void deleteCdIndicadorParticipante()
    {
        this._has_cdIndicadorParticipante= false;
    } //-- void deleteCdIndicadorParticipante() 

    /**
     * Method deleteCdParticipante
     * 
     */
    public void deleteCdParticipante()
    {
        this._has_cdParticipante= false;
    } //-- void deleteCdParticipante() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdCep'.
     * 
     * @return int
     * @return the value of field 'cdCep'.
     */
    public int getCdCep()
    {
        return this._cdCep;
    } //-- int getCdCep() 

    /**
     * Returns the value of field 'cdCepComplemento'.
     * 
     * @return int
     * @return the value of field 'cdCepComplemento'.
     */
    public int getCdCepComplemento()
    {
        return this._cdCepComplemento;
    } //-- int getCdCepComplemento() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdIndicadorParticipante'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorParticipante'.
     */
    public int getCdIndicadorParticipante()
    {
        return this._cdIndicadorParticipante;
    } //-- int getCdIndicadorParticipante() 

    /**
     * Returns the value of field 'cdIndicadorQuebra'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorQuebra'.
     */
    public java.lang.String getCdIndicadorQuebra()
    {
        return this._cdIndicadorQuebra;
    } //-- java.lang.String getCdIndicadorQuebra() 

    /**
     * Returns the value of field 'cdParticipante'.
     * 
     * @return long
     * @return the value of field 'cdParticipante'.
     */
    public long getCdParticipante()
    {
        return this._cdParticipante;
    } //-- long getCdParticipante() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBairroClienteParticipante'.
     * 
     * @return String
     * @return the value of field 'dsBairroClienteParticipante'.
     */
    public java.lang.String getDsBairroClienteParticipante()
    {
        return this._dsBairroClienteParticipante;
    } //-- java.lang.String getDsBairroClienteParticipante() 

    /**
     * Returns the value of field 'dsLogradouroParticipante'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroParticipante'.
     */
    public java.lang.String getDsLogradouroParticipante()
    {
        return this._dsLogradouroParticipante;
    } //-- java.lang.String getDsLogradouroParticipante() 

    /**
     * Returns the value of field 'dsMunicipioClienteParticipante'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioClienteParticipante'.
     */
    public java.lang.String getDsMunicipioClienteParticipante()
    {
        return this._dsMunicipioClienteParticipante;
    } //-- java.lang.String getDsMunicipioClienteParticipante() 

    /**
     * Returns the value of field 'dsNomeParticipante'.
     * 
     * @return String
     * @return the value of field 'dsNomeParticipante'.
     */
    public java.lang.String getDsNomeParticipante()
    {
        return this._dsNomeParticipante;
    } //-- java.lang.String getDsNomeParticipante() 

    /**
     * Returns the value of field 'dsSiglaUfParticipante'.
     * 
     * @return String
     * @return the value of field 'dsSiglaUfParticipante'.
     */
    public java.lang.String getDsSiglaUfParticipante()
    {
        return this._dsSiglaUfParticipante;
    } //-- java.lang.String getDsSiglaUfParticipante() 

    /**
     * Returns the value of field 'nrLogradouroParticipante'.
     * 
     * @return String
     * @return the value of field 'nrLogradouroParticipante'.
     */
    public java.lang.String getNrLogradouroParticipante()
    {
        return this._nrLogradouroParticipante;
    } //-- java.lang.String getNrLogradouroParticipante() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdCep
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCep()
    {
        return this._has_cdCep;
    } //-- boolean hasCdCep() 

    /**
     * Method hasCdCepComplemento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplemento()
    {
        return this._has_cdCepComplemento;
    } //-- boolean hasCdCepComplemento() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdIndicadorParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorParticipante()
    {
        return this._has_cdIndicadorParticipante;
    } //-- boolean hasCdIndicadorParticipante() 

    /**
     * Method hasCdParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParticipante()
    {
        return this._has_cdParticipante;
    } //-- boolean hasCdParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdCep'.
     * 
     * @param cdCep the value of field 'cdCep'.
     */
    public void setCdCep(int cdCep)
    {
        this._cdCep = cdCep;
        this._has_cdCep = true;
    } //-- void setCdCep(int) 

    /**
     * Sets the value of field 'cdCepComplemento'.
     * 
     * @param cdCepComplemento the value of field 'cdCepComplemento'
     */
    public void setCdCepComplemento(int cdCepComplemento)
    {
        this._cdCepComplemento = cdCepComplemento;
        this._has_cdCepComplemento = true;
    } //-- void setCdCepComplemento(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorParticipante'.
     * 
     * @param cdIndicadorParticipante the value of field
     * 'cdIndicadorParticipante'.
     */
    public void setCdIndicadorParticipante(int cdIndicadorParticipante)
    {
        this._cdIndicadorParticipante = cdIndicadorParticipante;
        this._has_cdIndicadorParticipante = true;
    } //-- void setCdIndicadorParticipante(int) 

    /**
     * Sets the value of field 'cdIndicadorQuebra'.
     * 
     * @param cdIndicadorQuebra the value of field
     * 'cdIndicadorQuebra'.
     */
    public void setCdIndicadorQuebra(java.lang.String cdIndicadorQuebra)
    {
        this._cdIndicadorQuebra = cdIndicadorQuebra;
    } //-- void setCdIndicadorQuebra(java.lang.String) 

    /**
     * Sets the value of field 'cdParticipante'.
     * 
     * @param cdParticipante the value of field 'cdParticipante'.
     */
    public void setCdParticipante(long cdParticipante)
    {
        this._cdParticipante = cdParticipante;
        this._has_cdParticipante = true;
    } //-- void setCdParticipante(long) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroClienteParticipante'.
     * 
     * @param dsBairroClienteParticipante the value of field
     * 'dsBairroClienteParticipante'.
     */
    public void setDsBairroClienteParticipante(java.lang.String dsBairroClienteParticipante)
    {
        this._dsBairroClienteParticipante = dsBairroClienteParticipante;
    } //-- void setDsBairroClienteParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroParticipante'.
     * 
     * @param dsLogradouroParticipante the value of field
     * 'dsLogradouroParticipante'.
     */
    public void setDsLogradouroParticipante(java.lang.String dsLogradouroParticipante)
    {
        this._dsLogradouroParticipante = dsLogradouroParticipante;
    } //-- void setDsLogradouroParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioClienteParticipante'.
     * 
     * @param dsMunicipioClienteParticipante the value of field
     * 'dsMunicipioClienteParticipante'.
     */
    public void setDsMunicipioClienteParticipante(java.lang.String dsMunicipioClienteParticipante)
    {
        this._dsMunicipioClienteParticipante = dsMunicipioClienteParticipante;
    } //-- void setDsMunicipioClienteParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeParticipante'.
     * 
     * @param dsNomeParticipante the value of field
     * 'dsNomeParticipante'.
     */
    public void setDsNomeParticipante(java.lang.String dsNomeParticipante)
    {
        this._dsNomeParticipante = dsNomeParticipante;
    } //-- void setDsNomeParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsSiglaUfParticipante'.
     * 
     * @param dsSiglaUfParticipante the value of field
     * 'dsSiglaUfParticipante'.
     */
    public void setDsSiglaUfParticipante(java.lang.String dsSiglaUfParticipante)
    {
        this._dsSiglaUfParticipante = dsSiglaUfParticipante;
    } //-- void setDsSiglaUfParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nrLogradouroParticipante'.
     * 
     * @param nrLogradouroParticipante the value of field
     * 'nrLogradouroParticipante'.
     */
    public void setNrLogradouroParticipante(java.lang.String nrLogradouroParticipante)
    {
        this._nrLogradouroParticipante = nrLogradouroParticipante;
    } //-- void setNrLogradouroParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
