/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.ICancelarPagamentosIndividuaisService;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.ICancelarPagamentosIndividuaisServiceConstants;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.CancelarPagtosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.CancelarPagtosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.ConsultarPagtosIndParaCancelamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.ConsultarPagtosIndParaCancelamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.request.CancelarPagtosIndividuaisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.response.CancelarPagtosIndividuaisResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindparacancelamento.request.ConsultarPagtosIndParaCancelamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindparacancelamento.response.ConsultarPagtosIndParaCancelamentoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: CancelarPagamentosIndividuais
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class CancelarPagamentosIndividuaisServiceImpl implements ICancelarPagamentosIndividuaisService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

    /**
     * Construtor.
     */
    public CancelarPagamentosIndividuaisServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.ICancelarPagamentosIndividuaisService#consultarPagtosIndParaCancelamento(br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean.ConsultarPagtosIndParaCancelamentoEntradaDTO)
     */
    public List<ConsultarPagtosIndParaCancelamentoSaidaDTO> consultarPagtosIndParaCancelamento (ConsultarPagtosIndParaCancelamentoEntradaDTO entrada){
    	List<ConsultarPagtosIndParaCancelamentoSaidaDTO> listaRetorno = new ArrayList<ConsultarPagtosIndParaCancelamentoSaidaDTO>();
    	ConsultarPagtosIndParaCancelamentoRequest request = new ConsultarPagtosIndParaCancelamentoRequest();
    	ConsultarPagtosIndParaCancelamentoResponse response = new ConsultarPagtosIndParaCancelamentoResponse();
    	
        request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoPesquisa()));
        request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entrada.getCdAgendadosPagosNaoPagos()));
        request.setNrOcorrencias(ICancelarPagamentosIndividuaisServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
        request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
        request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
        request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entrada.getDtCreditoPagamentoInicio()));
        request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entrada.getDtCreditoPagamentoFim()));
        request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entrada.getNrArquivoRemssaPagamento()));
        request.setCdParticipante(PgitUtil.verificaLongNulo(entrada.getCdParticipante()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entrada.getCdControlePagamentoDe()));
        request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entrada.getCdControlePagamentoAte()));
        request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entrada.getVlPagamentoDe()));
        request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entrada.getVlPagamentoAte()));
        request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoOperacaoPagamento()));
        request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdMotivoSituacaoPagamento()));
        request.setCdBarraDocumento(PgitUtil.verificaStringNula(entrada.getCdBarraDocumento()));
        request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entrada.getCdFavorecidoClientePagador()));
        request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entrada.getCdInscricaoFavorecido()));
        request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entrada.getCdIdentificacaoInscricaoFavorecido()));
        request.setCdBancoBeneficiario(PgitUtil.verificaIntegerNulo(entrada.getCdBancoBeneficiario()));
        request.setCdAgenciaBeneficiario(PgitUtil.verificaIntegerNulo(entrada.getCdAgenciaBeneficiario()));
        request.setCdContaBeneficiario(PgitUtil.verificaLongNulo(entrada.getCdContaBeneficiario()));
        request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
        request.setNrUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entrada.getNrUnidadeOrganizacional()));
        request.setCdBeneficio(PgitUtil.verificaLongNulo(entrada.getCdBeneficio()));
        request.setCdEspecieBeneficioInss(PgitUtil.verificaIntegerNulo(entrada.getCdEspecieBeneficioInss()));
        request.setCdClassificacao(PgitUtil.verificaIntegerNulo(entrada.getCdClassificacao()));
        //Novos Campos
        request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entrada.getCdEmpresaContrato()));
        request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entrada.getCdTipoConta()));
        request.setNrContrato(PgitUtil.verificaLongNulo(entrada.getNrContrato()));
        request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjParticipante()));
        request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjParticipante()));
        request.setCdControleCnpjParticipante(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjParticipante()));
        request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entrada.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoDebito()));
        request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entrada.getCdPessoaContratoCredt()));
        request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoCredt()));
        request.setNrSequenciaContratoCredt(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoCredt()));
        request.setCdIndiceSimulaPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdIndiceSimulaPagamento()));
        request.setCdOrigemRecebidoEfetivacao(PgitUtil.verificaIntegerNulo(entrada.getCdOrigemRecebidoEfetivacao()));         
        request.setCdTituloPgtoRastreado(0); 
        request.setCdBancoSalarial(PgitUtil.verificaIntegerNulo(entrada.getCdBancoSalarial()));
        request.setCdAgencaSalarial(PgitUtil.verificaIntegerNulo(entrada.getCdAgencaSalarial()));
        request.setCdContaSalarial(PgitUtil.verificaLongNulo(entrada.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entrada.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entrada.getCdIspbPagtoDestino(), 8));
		}
		
		if(entrada.getContaPagtoDestino() != null){
			request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entrada.getContaPagtoDestino().toString(), 20));
		}else{
			request.setContaPagtoDestino("");
		}
		

        response = getFactoryAdapter().getConsultarPagtosIndParaCancelamentoPDCAdapter().invokeProcess(request);
        
        ConsultarPagtosIndParaCancelamentoSaidaDTO saida;
        
        
        for(int i=0;i<response.getOcorrenciasCount();i++){
        	saida = new ConsultarPagtosIndParaCancelamentoSaidaDTO();
            saida.setCodMensagem(response.getCodMensagem());
            saida.setMensagem(response.getMensagem());
            
            
            
            saida.setNrCnpjCpf(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getNrCnpjCpf()));
            saida.setNrFilialCnpjCpf(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getNrFilialCnpjCpf()));
            saida.setNrDigitoCnpjCpf(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getNrDigitoCnpjCpf()));
            saida.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdPessoaJuridicaContrato()));
            saida.setDsRazaoSocial(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsRazaoSocial()));
            saida.setCdEmpresa(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdEmpresa()));
            saida.setDsEmpresa(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsEmpresa()));
            saida.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdTipoContratoNegocio()));
            saida.setNrContrato(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getNrContrato()));
            saida.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdProdutoServicoOperacao()));
            saida.setDsResumoProdutoServico(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsResumoProdutoServico()));
            saida.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdProdutoServicoRelacionado()));
            saida.setDsOperacaoProdutoServico(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsOperacaoProdutoServico()));
            saida.setCdControlePagamento(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdControlePagamento()));
            
            saida.setDtCreditoPagamento( response.getOcorrencias(i).getDtCreditoPagamento() != null && !response.getOcorrencias(i).getDtCreditoPagamento().equals("")
 	    		   ? FormatarData.formatarData(response.getOcorrencias(i).getDtCreditoPagamento()) : "");
            
            saida.setVlEfetivoPagamento(PgitUtil.verificaBigDecimalNulo(response.getOcorrencias(i).getVlEfetivoPagamento()));
            saida.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i).getCdInscricaoFavorecido()));
            saida.setDsFavorecido(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsFavorecido()));
            saida.setCdBancoDebito(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdBancoDebito()));
            saida.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdAgenciaDebito()));
            saida.setCdDigitoAgenciaDebito(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdDigitoAgenciaDebito()));
            saida.setCdContaDebito(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdContaDebito()));
            saida.setCdDigitoContaDebito(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdDigitoContaDebito()));
            saida.setCdBancoCredito(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdBancoCredito()));
            saida.setCdAgenciaCredito(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdAgenciaCredito()));
            saida.setCdDigitoAgenciaCredito(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdDigitoAgenciaCredito()));
            saida.setCdContaCredito(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdContaCredito()));
            saida.setCdDigitoContaCredito(PgitUtil.verificaStringNula(response.getOcorrencias(i).getCdDigitoContaCredito()));
            saida.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento()));
            saida.setDsSituacaoOperacaoPagamento(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento()));
            saida.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdMotivoSituacaoPagamento()));
            saida.setDsMotivoSituacaoPagamento(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsMotivoSituacaoPagamento()));
            saida.setCdTipoCanal(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdTipoCanal()));
            saida.setCdTipoTela(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdTipoTela()));
            saida.setCdBancoCreditoFormatado(response.getOcorrencias(i).getCdBancoCredito() == 0 ? "" : SiteUtil.formatNumber(String.valueOf(response.getOcorrencias(i).getCdBancoCredito()), 3));
            saida.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(saida.getNrCnpjCpf(), saida.getNrFilialCnpjCpf(), saida.getNrDigitoCnpjCpf()));
            saida.setFavorecidoBeneficiarioFormatado(saida.getDsFavorecido());
            saida.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), false));
    	    saida.setContaCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdContaCredito(), response.getOcorrencias(i).getCdDigitoContaCredito(), false));
    
   	    //Formatar Conta Cr�dito/D�bito
			saida.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(), response.getOcorrencias(i).getCdAgenciaDebito(), response.getOcorrencias(i).getCdDigitoAgenciaDebito(), response.getOcorrencias(i).getCdContaDebito(), response.getOcorrencias(i).getCdDigitoContaDebito(), true));
	        saida.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoCredito(), response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), response.getOcorrencias(i).getCdContaCredito(), response.getOcorrencias(i).getCdDigitoContaCredito(), true));
    	    saida.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
    	    saida.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
    	
    	    saida.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
    	    saida.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
    	    	
            listaRetorno.add(saida);
            

            }
    	return listaRetorno;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.ICancelarPagamentosIndividuaisService#cancelarPagtosIndividuais(java.util.List)
     */
    public CancelarPagtosIndividuaisSaidaDTO cancelarPagtosIndividuais (List<CancelarPagtosIndividuaisEntradaDTO> listaEntrada){
    	CancelarPagtosIndividuaisSaidaDTO saidaDTO = new CancelarPagtosIndividuaisSaidaDTO();
    	CancelarPagtosIndividuaisRequest request = new CancelarPagtosIndividuaisRequest();
    	CancelarPagtosIndividuaisResponse response = new CancelarPagtosIndividuaisResponse();
    	
    	request.setNumeroConsultas(listaEntrada.size());  
    	
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.request.Ocorrencias>();
    	br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.request.Ocorrencias ocorrencia;
    	
    	for(int i=0;i<listaEntrada.size();i++){
    		ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.request.Ocorrencias();
    		ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntrada.get(i).getCdControlePagamento()));    		
    		ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntrada.get(i).getCdPessoaJuridicaContrato()));
    		ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdProdutoServicoOperacao()));
    		ocorrencia.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdProdutoServicoRelacionado()));
    		ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(listaEntrada.get(i).getCdTipoCanal()));
    		ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaEntrada.get(i).getNrSequenciaContratoNegocio())); 
    		
    		ocorrencias.add(ocorrencia);    
    		
    	}
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.request.Ocorrencias[]) ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.request.Ocorrencias[0]));
    	
    	
    	response = getFactoryAdapter().getCancelarPagtosIndividuaisPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
    	saidaDTO.setMensagem(response.getMensagem());
    	
    	return saidaDTO;
    }
}