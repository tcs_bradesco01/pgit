/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;
import java.math.BigDecimal;

/**
 * Nome: AlterarLimDIaOpePgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarLimDIaOpePgitEntradaDTO{
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo vlLimiteDiaPagamento. */
	private BigDecimal vlLimiteDiaPagamento;
	
	/** Atributo vlLimiteIndvdPagamento. */
	private BigDecimal vlLimiteIndvdPagamento;
	
	/** Atributo cdusuario. */
	private String cdusuario;
	
	/** Atributo cdUsuarioExterno. */
	private String cdUsuarioExterno;
	
	/** Atributo cdCanal. */
	private Integer cdCanal;
	
	/** Atributo nmOperacaoFluxo. */
	private String nmOperacaoFluxo;
	
	/** Atributo cdEmpresaOperante. */
	private Long cdEmpresaOperante;
	
	/** Atributo cdDependenteOperante. */
	private Integer cdDependenteOperante;

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato(){
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato){
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao(){
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao){
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado(){
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado){
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: vlLimiteDiaPagamento.
	 *
	 * @return vlLimiteDiaPagamento
	 */
	public BigDecimal getVlLimiteDiaPagamento(){
		return vlLimiteDiaPagamento;
	}

	/**
	 * Set: vlLimiteDiaPagamento.
	 *
	 * @param vlLimiteDiaPagamento the vl limite dia pagamento
	 */
	public void setVlLimiteDiaPagamento(BigDecimal vlLimiteDiaPagamento){
		this.vlLimiteDiaPagamento = vlLimiteDiaPagamento;
	}

	/**
	 * Get: vlLimiteIndvdPagamento.
	 *
	 * @return vlLimiteIndvdPagamento
	 */
	public BigDecimal getVlLimiteIndvdPagamento(){
		return vlLimiteIndvdPagamento;
	}

	/**
	 * Set: vlLimiteIndvdPagamento.
	 *
	 * @param vlLimiteIndvdPagamento the vl limite indvd pagamento
	 */
	public void setVlLimiteIndvdPagamento(BigDecimal vlLimiteIndvdPagamento){
		this.vlLimiteIndvdPagamento = vlLimiteIndvdPagamento;
	}

	/**
	 * Get: cdusuario.
	 *
	 * @return cdusuario
	 */
	public String getCdusuario(){
		return cdusuario;
	}

	/**
	 * Set: cdusuario.
	 *
	 * @param cdusuario the cdusuario
	 */
	public void setCdusuario(String cdusuario){
		this.cdusuario = cdusuario;
	}

	/**
	 * Get: cdUsuarioExterno.
	 *
	 * @return cdUsuarioExterno
	 */
	public String getCdUsuarioExterno(){
		return cdUsuarioExterno;
	}

	/**
	 * Set: cdUsuarioExterno.
	 *
	 * @param cdUsuarioExterno the cd usuario externo
	 */
	public void setCdUsuarioExterno(String cdUsuarioExterno){
		this.cdUsuarioExterno = cdUsuarioExterno;
	}

	/**
	 * Get: cdCanal.
	 *
	 * @return cdCanal
	 */
	public Integer getCdCanal(){
		return cdCanal;
	}

	/**
	 * Set: cdCanal.
	 *
	 * @param cdCanal the cd canal
	 */
	public void setCdCanal(Integer cdCanal){
		this.cdCanal = cdCanal;
	}

	/**
	 * Get: nmOperacaoFluxo.
	 *
	 * @return nmOperacaoFluxo
	 */
	public String getNmOperacaoFluxo(){
		return nmOperacaoFluxo;
	}

	/**
	 * Set: nmOperacaoFluxo.
	 *
	 * @param nmOperacaoFluxo the nm operacao fluxo
	 */
	public void setNmOperacaoFluxo(String nmOperacaoFluxo){
		this.nmOperacaoFluxo = nmOperacaoFluxo;
	}

	/**
	 * Get: cdEmpresaOperante.
	 *
	 * @return cdEmpresaOperante
	 */
	public Long getCdEmpresaOperante(){
		return cdEmpresaOperante;
	}

	/**
	 * Set: cdEmpresaOperante.
	 *
	 * @param cdEmpresaOperante the cd empresa operante
	 */
	public void setCdEmpresaOperante(Long cdEmpresaOperante){
		this.cdEmpresaOperante = cdEmpresaOperante;
	}

	/**
	 * Get: cdDependenteOperante.
	 *
	 * @return cdDependenteOperante
	 */
	public Integer getCdDependenteOperante(){
		return cdDependenteOperante;
	}

	/**
	 * Set: cdDependenteOperante.
	 *
	 * @param cdDependenteOperante the cd dependente operante
	 */
	public void setCdDependenteOperante(Integer cdDependenteOperante){
		this.cdDependenteOperante = cdDependenteOperante;
	}
}