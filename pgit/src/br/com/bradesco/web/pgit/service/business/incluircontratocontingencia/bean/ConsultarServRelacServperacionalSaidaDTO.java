/*
 * Nome: br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean;



/**
 * Nome: ConsultarServRelacServperacionalSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarServRelacServperacionalSaidaDTO {

		/** Atributo codMensagem. */
		private String codMensagem;
	    
    	/** Atributo mensagem. */
    	private String mensagem;
	    
    	/** Atributo numeroLinhas. */
    	private Integer numeroLinhas;
	    
    	/** Atributo cdProdutoOperacaoRelacionado. */
    	private Integer cdProdutoOperacaoRelacionado;
		
		/** Atributo cdProdutoServicoRelacionado. */
		private String cdProdutoServicoRelacionado;
	    
    	/** Atributo cdRelacionamentoProdutoProduto. */
    	private Integer cdRelacionamentoProdutoProduto;
		
		/** Atributo dsRelacionamentoProdutoProduto. */
		private String dsRelacionamentoProdutoProduto;
		
		/** Atributo cdTipoProdutoServico. */
		private Integer cdTipoProdutoServico;
		
		/** Atributo dsTipoProdutoServico. */
		private String dsTipoProdutoServico;
		
		/** Atributo check. */
		private boolean check;
		   
		   
		/**
		 * Get: cdProdutoOperacaoRelacionado.
		 *
		 * @return cdProdutoOperacaoRelacionado
		 */
		public Integer getCdProdutoOperacaoRelacionado() {
			return cdProdutoOperacaoRelacionado;
		}
		
		/**
		 * Set: cdProdutoOperacaoRelacionado.
		 *
		 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
		 */
		public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
			this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
		}
		
		/**
		 * Get: cdProdutoServicoRelacionado.
		 *
		 * @return cdProdutoServicoRelacionado
		 */
		public String getCdProdutoServicoRelacionado() {
			return cdProdutoServicoRelacionado;
		}
		
		/**
		 * Set: cdProdutoServicoRelacionado.
		 *
		 * @param cdprodutoServicoRelacionado the cd produto servico relacionado
		 */
		public void setCdProdutoServicoRelacionado(String cdprodutoServicoRelacionado) {
			this.cdProdutoServicoRelacionado = cdprodutoServicoRelacionado;
		}
		
		/**
		 * Get: cdRelacionamentoProdutoProduto.
		 *
		 * @return cdRelacionamentoProdutoProduto
		 */
		public Integer getCdRelacionamentoProdutoProduto() {
			return cdRelacionamentoProdutoProduto;
		}
		
		/**
		 * Set: cdRelacionamentoProdutoProduto.
		 *
		 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
		 */
		public void setCdRelacionamentoProdutoProduto(
				Integer cdRelacionamentoProdutoProduto) {
			this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
		}
		
		/**
		 * Get: cdTipoProdutoServico.
		 *
		 * @return cdTipoProdutoServico
		 */
		public Integer getCdTipoProdutoServico() {
			return cdTipoProdutoServico;
		}
		
		/**
		 * Set: cdTipoProdutoServico.
		 *
		 * @param cdTipoProdutoServico the cd tipo produto servico
		 */
		public void setCdTipoProdutoServico(Integer cdTipoProdutoServico) {
			this.cdTipoProdutoServico = cdTipoProdutoServico;
		}
		
		/**
		 * Get: dsRelacionamentoProdutoProduto.
		 *
		 * @return dsRelacionamentoProdutoProduto
		 */
		public String getDsRelacionamentoProdutoProduto() {
			return dsRelacionamentoProdutoProduto;
		}
		
		/**
		 * Set: dsRelacionamentoProdutoProduto.
		 *
		 * @param dsRelacionamentoProdutoProduto the ds relacionamento produto produto
		 */
		public void setDsRelacionamentoProdutoProduto(
				String dsRelacionamentoProdutoProduto) {
			this.dsRelacionamentoProdutoProduto = dsRelacionamentoProdutoProduto;
		}
		
		/**
		 * Get: dsTipoProdutoServico.
		 *
		 * @return dsTipoProdutoServico
		 */
		public String getDsTipoProdutoServico() {
			return dsTipoProdutoServico;
		}
		
		/**
		 * Set: dsTipoProdutoServico.
		 *
		 * @param dsTipoProdutoServico the ds tipo produto servico
		 */
		public void setDsTipoProdutoServico(String dsTipoProdutoServico) {
			this.dsTipoProdutoServico = dsTipoProdutoServico;
		}
	    
	    
		/**
		 * Get: codMensagem.
		 *
		 * @return codMensagem
		 */
		public String getCodMensagem() {
			return codMensagem;
		}
		
		/**
		 * Set: codMensagem.
		 *
		 * @param codMensagem the cod mensagem
		 */
		public void setCodMensagem(String codMensagem) {
			this.codMensagem = codMensagem;
		}
		
		/**
		 * Get: mensagem.
		 *
		 * @return mensagem
		 */
		public String getMensagem() {
			return mensagem;
		}
		
		/**
		 * Set: mensagem.
		 *
		 * @param mensagem the mensagem
		 */
		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
		
		/**
		 * Get: numeroLinhas.
		 *
		 * @return numeroLinhas
		 */
		public Integer getNumeroLinhas() {
			return numeroLinhas;
		}
		
		/**
		 * Set: numeroLinhas.
		 *
		 * @param numeroLinhas the numero linhas
		 */
		public void setNumeroLinhas(Integer numeroLinhas) {
			this.numeroLinhas = numeroLinhas;
		}
		
		/**
		 * Is check.
		 *
		 * @return true, if is check
		 */
		public boolean isCheck() {
			return check;
		}
		
		/**
		 * Set: check.
		 *
		 * @param check the check
		 */
		public void setCheck(boolean check) {
			this.check = check;
		}
}