/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean;


/**
 * Nome: ListarSolBaseSistemaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSolBaseSistemaSaidaDTO {
	
	    /** Atributo codMensagem. */
    	private String codMensagem;
	    
    	/** Atributo mensagem. */
    	private String mensagem;
	    
    	/** Atributo nrSolicitacaoPagamento. */
    	private Integer nrSolicitacaoPagamento;
	    
    	/** Atributo hrSolicitacao. */
    	private String hrSolicitacao;
	    
    	/** Atributo cdSituacaoSolicitacao. */
    	private Integer cdSituacaoSolicitacao;
	    
    	/** Atributo dsSituacaoSolicitacao. */
    	private String dsSituacaoSolicitacao;
	    
    	/** Atributo resultadoProcessamento. */
    	private String resultadoProcessamento;
	    
    	/** Atributo hrAtendimentoSolicitacao. */
    	private String hrAtendimentoSolicitacao;
	    
    	/** Atributo qtdeRegistroSolicitacao. */
    	private String qtdeRegistroSolicitacao;
	    
    	/** Atributo hrSolicitacaoFormatada. */
    	private String hrSolicitacaoFormatada;
	    
		/**
		 * Get: codMensagem.
		 *
		 * @return codMensagem
		 */
		public String getCodMensagem() {
			return codMensagem;
		}
		
		/**
		 * Set: codMensagem.
		 *
		 * @param codMensagem the cod mensagem
		 */
		public void setCodMensagem(String codMensagem) {
			this.codMensagem = codMensagem;
		}
		
		/**
		 * Get: mensagem.
		 *
		 * @return mensagem
		 */
		public String getMensagem() {
			return mensagem;
		}
		
		/**
		 * Set: mensagem.
		 *
		 * @param mensagem the mensagem
		 */
		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
		
		/**
		 * Get: cdSituacaoSolicitacao.
		 *
		 * @return cdSituacaoSolicitacao
		 */
		public Integer getCdSituacaoSolicitacao() {
			return cdSituacaoSolicitacao;
		}
		
		/**
		 * Set: cdSituacaoSolicitacao.
		 *
		 * @param cdSituacaoSolicitacao the cd situacao solicitacao
		 */
		public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
			this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
		}
		
		/**
		 * Get: dsSituacaoSolicitacao.
		 *
		 * @return dsSituacaoSolicitacao
		 */
		public String getDsSituacaoSolicitacao() {
			return dsSituacaoSolicitacao;
		}
		
		/**
		 * Set: dsSituacaoSolicitacao.
		 *
		 * @param dsSituacaoSolicitacao the ds situacao solicitacao
		 */
		public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
			this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
		}
		
		/**
		 * Get: hrAtendimentoSolicitacao.
		 *
		 * @return hrAtendimentoSolicitacao
		 */
		public String getHrAtendimentoSolicitacao() {
			return hrAtendimentoSolicitacao;
		}
		
		/**
		 * Set: hrAtendimentoSolicitacao.
		 *
		 * @param hrAtendimentoSolicitacao the hr atendimento solicitacao
		 */
		public void setHrAtendimentoSolicitacao(String hrAtendimentoSolicitacao) {
			this.hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
		}
		
		/**
		 * Get: hrSolicitacao.
		 *
		 * @return hrSolicitacao
		 */
		public String getHrSolicitacao() {
			return hrSolicitacao;
		}
		
		/**
		 * Set: hrSolicitacao.
		 *
		 * @param hrSolicitacao the hr solicitacao
		 */
		public void setHrSolicitacao(String hrSolicitacao) {
			this.hrSolicitacao = hrSolicitacao;
		}
		
		/**
		 * Get: nrSolicitacaoPagamento.
		 *
		 * @return nrSolicitacaoPagamento
		 */
		public Integer getNrSolicitacaoPagamento() {
			return nrSolicitacaoPagamento;
		}
		
		/**
		 * Set: nrSolicitacaoPagamento.
		 *
		 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
		 */
		public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
			this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
		}
		
		/**
		 * Get: resultadoProcessamento.
		 *
		 * @return resultadoProcessamento
		 */
		public String getResultadoProcessamento() {
			return resultadoProcessamento;
		}
		
		/**
		 * Set: resultadoProcessamento.
		 *
		 * @param resultadoProcessamento the resultado processamento
		 */
		public void setResultadoProcessamento(String resultadoProcessamento) {
			this.resultadoProcessamento = resultadoProcessamento;
		}
		
		/**
		 * Get: hrSolicitacaoFormatada.
		 *
		 * @return hrSolicitacaoFormatada
		 */
		public String getHrSolicitacaoFormatada() {
			return hrSolicitacaoFormatada;
		}
		
		/**
		 * Set: hrSolicitacaoFormatada.
		 *
		 * @param hrSolicitacaoFormatada the hr solicitacao formatada
		 */
		public void setHrSolicitacaoFormatada(String hrSolicitacaoFormatada) {
			this.hrSolicitacaoFormatada = hrSolicitacaoFormatada;
		}
		
		/**
		 * Get: qtdeRegistroSolicitacao.
		 *
		 * @return qtdeRegistroSolicitacao
		 */
		public String getQtdeRegistroSolicitacao() {
			return qtdeRegistroSolicitacao;
		}
		
		/**
		 * Set: qtdeRegistroSolicitacao.
		 *
		 * @param qtdeRegistroSolicitacao the qtde registro solicitacao
		 */
		public void setQtdeRegistroSolicitacao(String qtdeRegistroSolicitacao) {
			this.qtdeRegistroSolicitacao = qtdeRegistroSolicitacao;
		}
		
	    
	    
	    
}
