/*
 * Nome: br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.impl;

import java.util.ArrayList;

import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.IEstornoOrdensPagamentosService;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.IEstornoOrdensPagamentosServiceConstants;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.EstornoOrdensPagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.EstornoOrdensPagamentosListaDTO;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.EstornoOrdensPagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.IncluirEstornoOrdensPagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.IncluirEstornoOrdensPagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestornoop.request.ConsultarPagtosEstornoOPRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestornoop.response.ConsultarPagtosEstornoOPResponse;
import br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.EstornarPagamentosOPRequest;
import br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.response.EstornarPagamentosOPResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: EstornoOrdensPagamentosServiceImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EstornoOrdensPagamentosServiceImpl implements IEstornoOrdensPagamentosService {

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.IEstornoOrdensPagamentosService#consultarPagtosEstornoOP(br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.EstornoOrdensPagamentosEntradaDTO)
	 */
	public EstornoOrdensPagamentosSaidaDTO consultarPagtosEstornoOP(EstornoOrdensPagamentosEntradaDTO entrada) {
		ConsultarPagtosEstornoOPRequest request = new ConsultarPagtosEstornoOPRequest();
		ConsultarPagtosEstornoOPResponse response = new ConsultarPagtosEstornoOPResponse();
		EstornoOrdensPagamentosSaidaDTO saida = new EstornoOrdensPagamentosSaidaDTO();

		request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoPesquisa()));
		request.setNumeroOcorrencias(IEstornoOrdensPagamentosServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
		request.setDgAgenciaBancaria(PgitUtil.verificaIntegerNulo(entrada.getDgAgenciaBancaria()));
		request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
		request.setCdDigitoConta(PgitUtil.verificaStringNula(entrada.getCdDigitoConta()));
		request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entrada.getNrArquivoRemssaPagamento()));
		request.setCdParticipante(PgitUtil.verificaLongNulo(entrada.getCdParticipante()));
		request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
		request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entrada.getCdControlePagamentoDe()));
		request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entrada.getVlPagamentoDe()));
		request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entrada.getVlPagamentoAte()));
		request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entrada.getCdFavorecidoClientePagador()));
		request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entrada.getCdInscricaoFavorecido()));
		request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entrada.getCdIdentificacaoInscricaoFavorecido()));
		request.setDtEstornoInicio(FormatarData.formataDiaMesAnoToPdc(entrada.getDtEstornoInicio()));
		request.setDtEstornoFim(FormatarData.formataDiaMesAnoToPdc(entrada.getDtEstornoFim()));

		response = getFactoryAdapter().getConsultarPagtosEstornoOPPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());

		EstornoOrdensPagamentosListaDTO dto = new EstornoOrdensPagamentosListaDTO();
		saida.setOcorrencias(new ArrayList<EstornoOrdensPagamentosListaDTO>());
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			dto = new EstornoOrdensPagamentosListaDTO();

			dto.setNrCnpjCpf(response.getOcorrencias(i).getCorpoCfpCnpj());
			dto.setNrFilialCnpjCpf(response.getOcorrencias(i).getFilialCfpCnpj());
			dto.setNrDigitoCnpjCpf(response.getOcorrencias(i).getControleCfpCnpj());
			dto.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
			dto.setDsRazaoSocial(response.getOcorrencias(i).getNmRazaoSocial());
			dto.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
			dto.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
			dto.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			dto.setNrContrato(response.getOcorrencias(i).getNrContrato());
			dto.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			dto.setDsResumoProdutoServico(response.getOcorrencias(i).getCdResumoProdutoServico());
			dto.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
			dto.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
			dto.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
			dto.setDtCreditoPagamento(response.getOcorrencias(i).getDtCraditoPagamento());
			dto.setVlEfetivoPagamento(response.getOcorrencias(i).getVlrEfetivacaoPagamentoCliente());
			dto.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido());
			dto.setDsFavorecido(response.getOcorrencias(i).getDsAbrevFavorecido());
			dto.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
			dto.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
			dto.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
			dto.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
			dto.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
			dto.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
			dto.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
			dto.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
			dto.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
			dto.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
			dto.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
			dto.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
			dto.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
			dto.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
			dto.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
			dto.setCdTipoTela(response.getOcorrencias(i).getDsTipoTela());
			dto.setDtEfetivacao(response.getOcorrencias(i).getDtEfetivacao());
			dto.setCdCpfCnpjRecebedor(response.getOcorrencias(i).getCdCpfCnpjRecebedor());
			dto.setCdFilialRecebedor(response.getOcorrencias(i).getCdFilialRecebedor());
			dto.setCdDigitoCpfRecebedor(response.getOcorrencias(i).getCdDigitoCpfRecebedor());
			dto.setDsCododigoTabelaConsulta(response.getOcorrencias(i).getDsCodigoTabelaConsulta());
			dto.setNrArquivoRemessa(response.getOcorrencias(i).getNrArquivoRemessa());
			dto.setCdFormaLiquidacao(response.getOcorrencias(i).getCdFormaLiquidacao());

			saida.getOcorrencias().add(dto);
		}

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.IEstornoOrdensPagamentosService#estornarPagamentosOP(br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.IncluirEstornoOrdensPagamentosEntradaDTO)
	 */
	public IncluirEstornoOrdensPagamentosSaidaDTO estornarPagamentosOP(IncluirEstornoOrdensPagamentosEntradaDTO entrada) {
		EstornarPagamentosOPRequest request = new EstornarPagamentosOPRequest();
		EstornarPagamentosOPResponse response = new EstornarPagamentosOPResponse();
		IncluirEstornoOrdensPagamentosSaidaDTO saida = new IncluirEstornoOrdensPagamentosSaidaDTO();

		request.setCdPrimeiraSolicitacao(PgitUtil.verificaStringNula(entrada.getCdPrimeiraSolicitacao()));
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamento()));
		request.setVlrPrevistoEstornoPagamento(PgitUtil.verificaBigDecimalNulo(entrada.getVlrPrevistoEstornoPagamento()));
		request.setDsObservacaoAutorizacaoEstorno(PgitUtil.verificaStringNula(entrada.getDsObservacaoAutorizacaoEstorno()));
		request.setCdUsuarioAutorizacaoEstorno(PgitUtil.verificaStringNula(entrada.getCdUsuarioAutorizacaoEstorno()));
		request.setNrPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrPagamento()));

		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias> listaOcorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias>();
		for (int i = 0; i < entrada.getOcorrencias().size(); i++) {
			br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias ocorrencias = new br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias();
			ocorrencias.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getOcorrencias().get(i).getCdpessoaJuridicaContrato()));
			ocorrencias.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getOcorrencias().get(i).getCdTipoContratoNegocio()));
			ocorrencias.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getOcorrencias().get(i).getNrSequenciaContratoNegocio()));
			ocorrencias.setCdControlePagamento(PgitUtil.verificaStringNula(entrada.getOcorrencias().get(i).getCdControlePagamento()));
			ocorrencias.setDtEfetivacao(PgitUtil.verificaStringNula(entrada.getOcorrencias().get(i).getDtEfetivacao()));
			ocorrencias.setCdModalidadePagamento(PgitUtil.verificaIntegerNulo(entrada.getOcorrencias().get(i).getCdModalidadePagamento()));
			ocorrencias.setCdValorEstorno(PgitUtil.verificaBigDecimalNulo(entrada.getOcorrencias().get(i).getCdValorEstorno()));
			listaOcorrencias.add(ocorrencias);
		}
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias[]) listaOcorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias[0]));

		response = getFactoryAdapter().getEstornarPagamentosOPPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdSolicitacaoPagamento(response.getCdSolicitacaoPagamento());
		saida.setNrSolicitacaoPagamento(response.getNrSolicitacaoPagamento());
		saida.setDsValorEstornoTotal(response.getDsValorEstornoTotal());

		return saida;
	}

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}
