/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

/**
 * Nome: AnexoTributosContas
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AnexoTributosContas {

    /** Atributo modalidadeDarf. */
    private ModalidadeCondicao modalidadeDarf = null;

    /** Atributo modalidadeGare. */
    private ModalidadeCondicao modalidadeGare = null;

    /** Atributo modalidadeGps. */
    private ModalidadeCondicao modalidadeGps = null;

    /** Atributo modalidadeCodigoBarra. */
    private ModalidadeCondicao modalidadeCodigoBarra = null;
    
    /** The hr limite pagamento remessa. */
    private String hrLimitePagamentoRemessa = null; 

    /**
     * Instancia um novo AnexoTributosContas
     * 
     * @see
     */
    public AnexoTributosContas() {
        super();
    }

    /**
     * Nome: getModalidadeDarf.
     *
     * @return modalidadeDarf
     */
    public ModalidadeCondicao getModalidadeDarf() {
        return modalidadeDarf;
    }

    /**
     * Nome: setModalidadeDarf.
     *
     * @param modalidadeDarf the modalidade darf
     */
    public void setModalidadeDarf(ModalidadeCondicao modalidadeDarf) {
        this.modalidadeDarf = modalidadeDarf;
    }

    /**
     * Nome: getModalidadeGare.
     *
     * @return modalidadeGare
     */
    public ModalidadeCondicao getModalidadeGare() {
        return modalidadeGare;
    }

    /**
     * Nome: setModalidadeGare.
     *
     * @param modalidadeGare the modalidade gare
     */
    public void setModalidadeGare(ModalidadeCondicao modalidadeGare) {
        this.modalidadeGare = modalidadeGare;
    }

    /**
     * Nome: getModalidadeGps.
     *
     * @return modalidadeGps
     */
    public ModalidadeCondicao getModalidadeGps() {
        return modalidadeGps;
    }

    /**
     * Nome: setModalidadeGps.
     *
     * @param modalidadeGps the modalidade gps
     */
    public void setModalidadeGps(ModalidadeCondicao modalidadeGps) {
        this.modalidadeGps = modalidadeGps;
    }

    /**
     * Nome: getModalidadeCodigoBarra.
     *
     * @return modalidadeCodigoBarra
     */
    public ModalidadeCondicao getModalidadeCodigoBarra() {
        return modalidadeCodigoBarra;
    }

    /**
     * Nome: setModalidadeCodigoBarra.
     *
     * @param modalidadeCodigoBarra the modalidade codigo barra
     */
    public void setModalidadeCodigoBarra(ModalidadeCondicao modalidadeCodigoBarra) {
        this.modalidadeCodigoBarra = modalidadeCodigoBarra;
    }

	/**
	 * Gets the hr limite pagamento remessa.
	 *
	 * @return the hr limite pagamento remessa
	 */
	public String getHrLimitePagamentoRemessa() {
		return hrLimitePagamentoRemessa;
	}

	/**
	 * Sets the hr limite pagamento remessa.
	 *
	 * @param hrLimitePagamentoRemessa the new hr limite pagamento remessa
	 */
	public void setHrLimitePagamentoRemessa(String hrLimitePagamentoRemessa) {
		this.hrLimitePagamentoRemessa = hrLimitePagamentoRemessa;
	}
}
