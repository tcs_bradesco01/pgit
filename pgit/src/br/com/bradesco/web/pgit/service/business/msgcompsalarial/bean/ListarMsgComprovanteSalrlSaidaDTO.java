/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean;

/**
 * Nome: ListarMsgComprovanteSalrlSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMsgComprovanteSalrlSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/** Atributo nivelMensagem. */
	private Integer nivelMensagem;
	
	/** Atributo codigo. */
	private Integer codigo;
	
	/** Atributo descricao. */
	private String descricao;
	
	/** Atributo restricao. */
	private Integer restricao;
	
	/** Atributo descTipoComprovanteSalarial. */
	private String descTipoComprovanteSalarial;
	
	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}
	
	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public Integer getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(Integer restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: nivelMensagem.
	 *
	 * @return nivelMensagem
	 */
	public Integer getNivelMensagem() {
		return nivelMensagem;
	}
	
	/**
	 * Set: nivelMensagem.
	 *
	 * @param nivelMensagem the nivel mensagem
	 */
	public void setNivelMensagem(Integer nivelMensagem) {
		this.nivelMensagem = nivelMensagem;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: descTipoComprovanteSalarial.
	 *
	 * @return descTipoComprovanteSalarial
	 */
	public String getDescTipoComprovanteSalarial() {
		return descTipoComprovanteSalarial;
	}
	
	/**
	 * Set: descTipoComprovanteSalarial.
	 *
	 * @param descTipoComprovanteSalarial the desc tipo comprovante salarial
	 */
	public void setDescTipoComprovanteSalarial(String descTipoComprovanteSalarial) {
		this.descTipoComprovanteSalarial = descTipoComprovanteSalarial;
	}
	

}
