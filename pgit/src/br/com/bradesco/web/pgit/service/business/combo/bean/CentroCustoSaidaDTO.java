/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;


/**
 * Nome: CentroCustoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CentroCustoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdCentroCusto. */
	private String cdCentroCusto;
	
	/** Atributo dsCentroCusto. */
	private String dsCentroCusto;
	
	/**
	 * Get: cdCentroCusto.
	 *
	 * @return cdCentroCusto
	 */
	public String getCdCentroCusto() {
		return cdCentroCusto;
	}
	
	/**
	 * Set: cdCentroCusto.
	 *
	 * @param cdCentroCusto the cd centro custo
	 */
	public void setCdCentroCusto(String cdCentroCusto) {
		this.cdCentroCusto = cdCentroCusto;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsCentroCusto.
	 *
	 * @return dsCentroCusto
	 */
	public String getDsCentroCusto() {
		return dsCentroCusto;
	}
	
	/**
	 * Set: dsCentroCusto.
	 *
	 * @param dsCentroCusto the ds centro custo
	 */
	public void setDsCentroCusto(String dsCentroCusto) {
		this.dsCentroCusto = dsCentroCusto;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: codigoDescricao.
	 *
	 * @return codigoDescricao
	 */
	public String getCodigoDescricao() {
		return getCdCentroCusto() + " - " + getDsCentroCusto();
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CentroCustoSaidaDTO)) {
			return false;
		}

		CentroCustoSaidaDTO other = (CentroCustoSaidaDTO) obj;
		return this.getCdCentroCusto().equals(other.getCdCentroCusto());
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hashCode = 7;
		hashCode = hashCode + getCdCentroCusto().hashCode();
		return hashCode;
	}
}
