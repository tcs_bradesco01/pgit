/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrCpfCnpjConta
     */
    private java.lang.String _nrCpfCnpjConta;

    /**
     * Field _nmRazaoSocialConta
     */
    private java.lang.String _nmRazaoSocialConta;

    /**
     * Field _dsTipoParticipacaoPessoa
     */
    private java.lang.String _dsTipoParticipacaoPessoa;

    /**
     * Field _dsAcaoManutencaoPerfil
     */
    private java.lang.String _dsAcaoManutencaoPerfil;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'dsAcaoManutencaoPerfil'.
     * 
     * @return String
     * @return the value of field 'dsAcaoManutencaoPerfil'.
     */
    public java.lang.String getDsAcaoManutencaoPerfil()
    {
        return this._dsAcaoManutencaoPerfil;
    } //-- java.lang.String getDsAcaoManutencaoPerfil() 

    /**
     * Returns the value of field 'dsTipoParticipacaoPessoa'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipacaoPessoa'.
     */
    public java.lang.String getDsTipoParticipacaoPessoa()
    {
        return this._dsTipoParticipacaoPessoa;
    } //-- java.lang.String getDsTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'nmRazaoSocialConta'.
     * 
     * @return String
     * @return the value of field 'nmRazaoSocialConta'.
     */
    public java.lang.String getNmRazaoSocialConta()
    {
        return this._nmRazaoSocialConta;
    } //-- java.lang.String getNmRazaoSocialConta() 

    /**
     * Returns the value of field 'nrCpfCnpjConta'.
     * 
     * @return String
     * @return the value of field 'nrCpfCnpjConta'.
     */
    public java.lang.String getNrCpfCnpjConta()
    {
        return this._nrCpfCnpjConta;
    } //-- java.lang.String getNrCpfCnpjConta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsAcaoManutencaoPerfil'.
     * 
     * @param dsAcaoManutencaoPerfil the value of field
     * 'dsAcaoManutencaoPerfil'.
     */
    public void setDsAcaoManutencaoPerfil(java.lang.String dsAcaoManutencaoPerfil)
    {
        this._dsAcaoManutencaoPerfil = dsAcaoManutencaoPerfil;
    } //-- void setDsAcaoManutencaoPerfil(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipacaoPessoa'.
     * 
     * @param dsTipoParticipacaoPessoa the value of field
     * 'dsTipoParticipacaoPessoa'.
     */
    public void setDsTipoParticipacaoPessoa(java.lang.String dsTipoParticipacaoPessoa)
    {
        this._dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
    } //-- void setDsTipoParticipacaoPessoa(java.lang.String) 

    /**
     * Sets the value of field 'nmRazaoSocialConta'.
     * 
     * @param nmRazaoSocialConta the value of field
     * 'nmRazaoSocialConta'.
     */
    public void setNmRazaoSocialConta(java.lang.String nmRazaoSocialConta)
    {
        this._nmRazaoSocialConta = nmRazaoSocialConta;
    } //-- void setNmRazaoSocialConta(java.lang.String) 

    /**
     * Sets the value of field 'nrCpfCnpjConta'.
     * 
     * @param nrCpfCnpjConta the value of field 'nrCpfCnpjConta'.
     */
    public void setNrCpfCnpjConta(java.lang.String nrCpfCnpjConta)
    {
        this._nrCpfCnpjConta = nrCpfCnpjConta;
    } //-- void setNrCpfCnpjConta(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
