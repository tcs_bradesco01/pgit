/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.validarparametrotela.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.validarparametrotela.IValidarParametroTelaService;
import br.com.bradesco.web.pgit.service.business.validarparametrotela.bean.ValidarParametroTelaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.validarparametrotela.bean.ValidarParametroTelaOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.validarparametrotela.bean.ValidarParametroTelaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContServiceConstants;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsghistoricocomplementar.request.DetalharMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsghistoricocomplementar.response.DetalharMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.request.DetalharVincMsgHistComplementarOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.response.DetalharVincMsgHistComplementarOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincmsghistcomplementaroperacao.request.ExcluirVincMsgHistComplementarOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincmsghistcomplementaroperacao.response.ExcluirVincMsgHistComplementarOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsghistcomplementaroperacao.request.IncluirVincMsgHistComplementarOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsghistcomplementaroperacao.response.IncluirVincMsgHistComplementarOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.request.ListarMsgHistoricoComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response.ListarMsgHistoricoComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincmsghistcomplementaroperacao.request.ListarVincMsgHistComplementarOperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincmsghistcomplementaroperacao.response.ListarVincMsgHistComplementarOperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validarparametrotela.request.ValidarParametroTelaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarparametrotela.response.ValidarParametroTelaResponse;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: VincMsgHistCompOpeCont
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ValidarParametroTelaServiceImpl implements
		IValidarParametroTelaService {

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.validarparametrotela.IValidarParametroTelaService#listarValidarParametroTela(br.com.bradesco.web.pgit.service.business.validarparametrotela.bean.ValidarParametroTelaEntradaDTO)
	 */
	public ValidarParametroTelaSaidaDTO listarValidarParametroTela(
			ValidarParametroTelaEntradaDTO entradaDTO) {
		ValidarParametroTelaSaidaDTO saidaDTO = new ValidarParametroTelaSaidaDTO();
		ValidarParametroTelaRequest request = new ValidarParametroTelaRequest();
		ValidarParametroTelaResponse listarValidarParametroTelaResponse = new ValidarParametroTelaResponse();
		List<ValidarParametroTelaOcorrenciaSaidaDTO> listaValidarParametroTelaOcorrenciaSaidaDTO = new ArrayList<ValidarParametroTelaOcorrenciaSaidaDTO>();
		ValidarParametroTelaOcorrenciaSaidaDTO ocorrenciaSaidaDTO;

		request.setCdProdutoOperacaoRelacionado(entradaDTO
				.getCdProdutoOperacaoRelacionado());
		request.setCdProdutoServicoOperacao(entradaDTO
				.getCdProdutoOperacaoRelacionado());
		request.setCdRelacionamentoProduto(entradaDTO
				.getCdRelacionamentoProduto());
		request.setNrOcorrencias(entradaDTO.getNrOcorrencias());

		listarValidarParametroTelaResponse = getFactoryAdapter()
				.getValidarParametroTelaPDCAdapter().invokeProcess(request);

		saidaDTO.setCodMensagem(listarValidarParametroTelaResponse
				.getCodMensagem());
		saidaDTO.setMensagem(listarValidarParametroTelaResponse.getMensagem());
		saidaDTO.setNumeroLinhas(listarValidarParametroTelaResponse
				.getNumeroLinhas());

		for (int i = 0; i < listarValidarParametroTelaResponse
				.getOcorrenciasCount(); i++) {

			ocorrenciaSaidaDTO = new ValidarParametroTelaOcorrenciaSaidaDTO();

			ocorrenciaSaidaDTO.setCdParametroTela(listarValidarParametroTelaResponse.getOcorrencias(i).getCdParametroTela());
			ocorrenciaSaidaDTO
					.setCdProdutoOperacaoRelacionado(listarValidarParametroTelaResponse
							.getOcorrencias(i)
							.getCdProdutoOperacaoRelacionado());
			ocorrenciaSaidaDTO
					.setCdProdutoServicoRelacionado(listarValidarParametroTelaResponse
							.getOcorrencias(i).getCdprodutoServicoRelacionado());
			ocorrenciaSaidaDTO
					.setCdRelacionamentoProdutoProduto(listarValidarParametroTelaResponse
							.getOcorrencias(i)
							.getCdRelacionamentoProdutoProduto());
			ocorrenciaSaidaDTO
					.setDsRelacionamentoProdutoProduto(listarValidarParametroTelaResponse
							.getOcorrencias(i)
							.getDsRelacionamentoProdutoProduto());
			ocorrenciaSaidaDTO
					.setCdTipoProdutoServico(listarValidarParametroTelaResponse
							.getOcorrencias(i).getCdTipoProdutoServico());
			ocorrenciaSaidaDTO
					.setDsTipoProdutoServico(listarValidarParametroTelaResponse
							.getOcorrencias(i).getDsTipoProdutoServico());

			listaValidarParametroTelaOcorrenciaSaidaDTO.add(ocorrenciaSaidaDTO);
		}
		saidaDTO.setOcorrencias(listaValidarParametroTelaOcorrenciaSaidaDTO);

		return saidaDTO;

	}

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

}
