/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean;

/**
 * Nome: DetalharSolEmiAviMovtoFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolEmiAviMovtoFavorecidoEntradaDTO{
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo cdPessoaJuridicaEmpr. */
    private Long cdPessoaJuridicaEmpr;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoSolicitacaoPagamento. */
    private Integer cdTipoSolicitacaoPagamento;
    
    /** Atributo cdSolicitacao. */
    private Integer cdSolicitacao;
    
	/**
	 * Get: cdPessoaJuridicaEmpr.
	 *
	 * @return cdPessoaJuridicaEmpr
	 */
	public Long getCdPessoaJuridicaEmpr() {
		return cdPessoaJuridicaEmpr;
	}
	
	/**
	 * Set: cdPessoaJuridicaEmpr.
	 *
	 * @param cdPessoaJuridicaEmpr the cd pessoa juridica empr
	 */
	public void setCdPessoaJuridicaEmpr(Long cdPessoaJuridicaEmpr) {
		this.cdPessoaJuridicaEmpr = cdPessoaJuridicaEmpr;
	}
	
	/**
	 * Get: cdSolicitacao.
	 *
	 * @return cdSolicitacao
	 */
	public Integer getCdSolicitacao() {
		return cdSolicitacao;
	}
	
	/**
	 * Set: cdSolicitacao.
	 *
	 * @param cdSolicitacao the cd solicitacao
	 */
	public void setCdSolicitacao(Integer cdSolicitacao) {
		this.cdSolicitacao = cdSolicitacao;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoSolicitacaoPagamento.
	 *
	 * @return cdTipoSolicitacaoPagamento
	 */
	public Integer getCdTipoSolicitacaoPagamento() {
		return cdTipoSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdTipoSolicitacaoPagamento.
	 *
	 * @param cdTipoSolicitacaoPagamento the cd tipo solicitacao pagamento
	 */
	public void setCdTipoSolicitacaoPagamento(Integer cdTipoSolicitacaoPagamento) {
		this.cdTipoSolicitacaoPagamento = cdTipoSolicitacaoPagamento;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
}