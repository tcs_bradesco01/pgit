/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: DetalharCnpjFicticioSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharCnpjFicticioSaidaDTO {
	
    /** Atributo codMensagem. */
    private String  codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdContratoPerfil. */
    private Long cdContratoPerfil;
    
    /** Atributo cdCnpjCpf. */
    private Long cdCnpjCpf;
    
    /** Atributo cdFilialCpfCnpj. */
    private Integer cdFilialCpfCnpj;
    
    /** Atributo cdCtrlCpfCnpj. */
    private Integer cdCtrlCpfCnpj;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo dtInclusaoRegistro. */
    private String dtInclusaoRegistro;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsTipoCanalInclusao. */
    private String dsTipoCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoexterno. */
    private String cdUsuarioManutencaoexterno;
    
    /** Atributo dtManutencaoRegistro. */
    private String dtManutencaoRegistro;
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsTipoCanalManutencao. */
    private String dsTipoCanalManutencao;

    /**
     * Get: cpfCnpjFormatado.
     *
     * @return cpfCnpjFormatado
     */
    public String getCpfCnpjFormatado(){
    	return CpfCnpjUtils.formatarCpfCnpj(cdCnpjCpf, cdFilialCpfCnpj,cdCtrlCpfCnpj);
    }
    
    /**
     * Get: canalManutencaoFormatado.
     *
     * @return canalManutencaoFormatado
     */
    public String getCanalManutencaoFormatado(){
    	return PgitUtil.concatenarCampos(cdTipoCanalManutencao, dsTipoCanalManutencao,"-");
    }
    
    /**
     * Get: canalInclusaoFormatado.
     *
     * @return canalInclusaoFormatado
     */
    public String getCanalInclusaoFormatado(){
    	return PgitUtil.concatenarCampos(cdTipoCanalInclusao, dsTipoCanalInclusao,"-");
    }
    
    /**
     * Get: usuarioInclusaoFormatado.
     *
     * @return usuarioInclusaoFormatado
     */
    public String getUsuarioInclusaoFormatado(){
    	return PgitUtil.isStringVazio(cdUsuarioInclusao) ? cdUsuarioInclusaoExterno : cdUsuarioInclusao;
    }
    
    /**
     * Get: usuarioManutencaoFormatado.
     *
     * @return usuarioManutencaoFormatado
     */
    public String getUsuarioManutencaoFormatado(){
    	return PgitUtil.isStringVazio(cdUsuarioManutencao) ? cdUsuarioManutencaoexterno : cdUsuarioManutencao;
    }
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdContratoPerfil.
	 *
	 * @return cdContratoPerfil
	 */
	public Long getCdContratoPerfil() {
		return cdContratoPerfil;
	}
	
	/**
	 * Set: cdContratoPerfil.
	 *
	 * @param cdContratoPerfil the cd contrato perfil
	 */
	public void setCdContratoPerfil(Long cdContratoPerfil) {
		this.cdContratoPerfil = cdContratoPerfil;
	}
	
	/**
	 * Get: cdCnpjCpf.
	 *
	 * @return cdCnpjCpf
	 */
	public Long getCdCnpjCpf() {
		return cdCnpjCpf;
	}
	
	/**
	 * Set: cdCnpjCpf.
	 *
	 * @param cdCnpjCpf the cd cnpj cpf
	 */
	public void setCdCnpjCpf(Long cdCnpjCpf) {
		this.cdCnpjCpf = cdCnpjCpf;
	}
	
	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	
	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	
	/**
	 * Get: cdCtrlCpfCnpj.
	 *
	 * @return cdCtrlCpfCnpj
	 */
	public Integer getCdCtrlCpfCnpj() {
		return cdCtrlCpfCnpj;
	}
	
	/**
	 * Set: cdCtrlCpfCnpj.
	 *
	 * @param cdCtrlCpfCnpj the cd ctrl cpf cnpj
	 */
	public void setCdCtrlCpfCnpj(Integer cdCtrlCpfCnpj) {
		this.cdCtrlCpfCnpj = cdCtrlCpfCnpj;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: dtInclusaoRegistro.
	 *
	 * @return dtInclusaoRegistro
	 */
	public String getDtInclusaoRegistro() {
		return dtInclusaoRegistro;
	}
	
	/**
	 * Set: dtInclusaoRegistro.
	 *
	 * @param dtInclusaoRegistro the dt inclusao registro
	 */
	public void setDtInclusaoRegistro(String dtInclusaoRegistro) {
		this.dtInclusaoRegistro = dtInclusaoRegistro;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoexterno.
	 *
	 * @return cdUsuarioManutencaoexterno
	 */
	public String getCdUsuarioManutencaoexterno() {
		return cdUsuarioManutencaoexterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoexterno.
	 *
	 * @param cdUsuarioManutencaoexterno the cd usuario manutencaoexterno
	 */
	public void setCdUsuarioManutencaoexterno(String cdUsuarioManutencaoexterno) {
		this.cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
	}
	
	/**
	 * Get: dtManutencaoRegistro.
	 *
	 * @return dtManutencaoRegistro
	 */
	public String getDtManutencaoRegistro() {
		return dtManutencaoRegistro;
	}
	
	/**
	 * Set: dtManutencaoRegistro.
	 *
	 * @param dtManutencaoRegistro the dt manutencao registro
	 */
	public void setDtManutencaoRegistro(String dtManutencaoRegistro) {
		this.dtManutencaoRegistro = dtManutencaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

}
