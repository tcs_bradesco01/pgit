/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento.impl;

import static br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento.IManterSolicAntecipacaoPostergarLotePagamentoServiceConstants.QUANTIDADE_OCORRENCIAS;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaBigDecimalNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataData;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataTimestampFromPdc;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.OcorrenciasLotePagamentosDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento.IManterSolicAntecipacaoPostergarLotePagamentoService;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicantposdtpgto.request.ConsultarSolicAntPosDtPgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicantposdtpgto.response.ConsultarSolicAntPosDtPgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.request.DetalharSolicAntPosDtPgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.response.DetalharSolicAntPosDtPgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicantposdtpgto.request.ExcluirSolicAntPosDtPgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicantposdtpgto.response.ExcluirSolicAntPosDtPgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicantposdtpgto.request.IncluirSolicAntPosDtPgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicantposdtpgto.response.IncluirSolicAntPosDtPgtoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterSolicAntecipacaoPostergarLotePagamentoServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador
 * ManterSolicAntecipacaoPostergarLotePagamento
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IManterSolicAntecipacaoPostergarLotePagamentoService
 */
public class ManterSolicAntecipacaoPostergarLotePagamentoServiceImpl implements
		IManterSolicAntecipacaoPostergarLotePagamentoService {

	private FactoryAdapter factoryAdapter;

	public ConsultarLotePagamentosSaidaDTO consultarSolicAntPosDtPgto(
			ConsultarLotePagamentosEntradaDTO entrada) {

		ConsultarLotePagamentosSaidaDTO saida = new ConsultarLotePagamentosSaidaDTO();
		ConsultarSolicAntPosDtPgtoRequest request = new ConsultarSolicAntPosDtPgtoRequest();
		ConsultarSolicAntPosDtPgtoResponse response = null;

		request.setNrOcorrencias(QUANTIDADE_OCORRENCIAS);
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request.setDtPgtoInicial(verificaStringNula(entrada.getDtPgtoInicial()));
		request.setDtPgtoFinal(verificaStringNula(entrada.getDtPgtoFinal()));
		request.setCdSituacaoSolicitacaoPagamento(verificaIntegerNulo(entrada
				.getCdSituacaoSolicitacaoPagamento()));
		request.setCdMotivoSolicitacao(verificaIntegerNulo(entrada
				.getCdMotivoSolicitacao()));

		response = getFactoryAdapter()
				.getConsultarSolicAntPosDtPgtoPDCAdapter().invokeProcess(
						request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());

		List<OcorrenciasLotePagamentosDTO> ocorrencias = new ArrayList<OcorrenciasLotePagamentosDTO>();

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			OcorrenciasLotePagamentosDTO ocorrencia = new OcorrenciasLotePagamentosDTO();

			ocorrencia.setCdSolicitacaoPagamentoIntegrado(response
					.getOcorrencias(i).getCdSolicitacaoPagamentoIntegrado());
			ocorrencia.setNrSolicitacaoPagamentoIntegrado(response
					.getOcorrencias(i).getNrSolicitacaoPagamentoIntegrado());
			ocorrencia.setCdpessoaJuridicaContrato(response.getOcorrencias(i)
					.getCdpessoaJuridicaContrato());
			ocorrencia.setCdTipoContratoNegocio(response.getOcorrencias(i)
					.getCdTipoContratoNegocio());
			ocorrencia.setNrSequenciaContratoNegocio(response.getOcorrencias(i)
					.getNrSequenciaContratoNegocio());
			ocorrencia.setNrLoteInterno(response.getOcorrencias(i)
					.getNrLoteInterno());
			ocorrencia.setCdTipoLayoutArquivo(response.getOcorrencias(i)
					.getCdTipoLayoutArquivo());
			ocorrencia.setDsTipoLayoutArquivo(response.getOcorrencias(i)
					.getDsTipoLayoutArquivo());
			ocorrencia.setCdSituacaoSolicitacaoPagamento(response
					.getOcorrencias(i).getCdSituacaoSolicitacaoPagamento());
			ocorrencia.setDsSituacaoSolicitaoPgto(response.getOcorrencias(i)
					.getDsSituacaoSolicitaoPgto());
			ocorrencia.setCdMotivoSolicitacao(response.getOcorrencias(i)
					.getCdMotivoSolicitacao());
			ocorrencia.setDsMotivoSolicitacao(response.getOcorrencias(i)
					.getDsMotivoSolicitacao());
			ocorrencia.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
			ocorrencia.setDsSolicitacao(response.getOcorrencias(i)
					.getDsSolicitacao());
			ocorrencia.setHrSolicitacao(response.getOcorrencias(i)
					.getHrSolicitacao());
			ocorrencia.setDsTipoSolicitacaoAntecipacaoPostergacao(response.getOcorrencias(i)
					.getDsTipoSolicitacaoAntecipacaoPostergacao());
			ocorrencia.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			
			ocorrencia.setDataHoraFormatada(PgitUtil.concatenarCampos(formataData(FormatarData.formataDiaMesAnoFromPdc(response.getOcorrencias(i).getDsSolicitacao()), "dd/MM/yyyy"),
					response.getOcorrencias(i).getHrSolicitacao(), "-"));

			ocorrencias.add(ocorrencia);
		}

		saida.setOcorrencias(ocorrencias);

		return saida;
	}

	public DetalharLotePagamentosSaidaDTO detalharSolicAntPosDtPgto(
			DetalharLotePagamentosEntradaDTO entrada) {

		DetalharLotePagamentosSaidaDTO saida = new DetalharLotePagamentosSaidaDTO();
		DetalharSolicAntPosDtPgtoRequest request = new DetalharSolicAntPosDtPgtoRequest();
		DetalharSolicAntPosDtPgtoResponse response = null;

		request.setCdSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entrada
						.getCdSolicitacaoPagamentoIntegrado()));
		request.setNrSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entrada
						.getNrSolicitacaoPagamentoIntegrado()));

		response = getFactoryAdapter().getDetalharSolicAntPosDtPgtoPDCAdapter()
				.invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdpessoaJuridicaContrato(response
				.getCdpessoaJuridicaContrato());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio());
		saida.setCdSolicitacaoPagamentoIntegrado(response
				.getCdSolicitacaoPagamentoIntegrado());
		saida.setNrSolicitacaoPagamentoIntegrado(response
				.getNrSolicitacaoPagamentoIntegrado());
		saida.setDsSituacaoSolicitaoPgto(response.getDsSituacaoSolicitaoPgto());
		saida.setDsMotivoSolicitacao(response.getDsMotivoSolicitacao());
		saida.setDsSolicitacao(response.getDsSolicitacao());
		saida.setHrSolicitacao(response.getHrSolicitacao());		
		saida.setFormatDataHora(response.getDsSolicitacao() + response.getHrSolicitacao());
        if(saida.getDsSituacaoSolicitaoPgto().toUpperCase().equals("PENDENTE")){
        	saida.setFormatDataHora(null);        	
        }    
        if(StringUtils.isNotEmpty(saida.getFormatDataHora())){
        	saida.setFormatDataHora(String.format("%s - %s", response.getDsSolicitacao(), response.getHrSolicitacao()));	
        }
		saida.setNrLoteInterno(response.getNrLoteInterno());
		saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saida.setDtPgtoInicial(response.getDtPgtoInicial());
		saida.setDtPgtoFinal(response.getDtPgtoFinal());
		saida.setCdBancoDebito(response.getCdBancoDebito());
		saida.setCdAgenciaDebito(0);
		saida.setCdContaDebito(response.getCdContaDebito());
		saida.setDsProdutoServicoOperacao(response
				.getDsProdutoServicoOperacao());
		saida.setDsModalidadePgtoCliente(response.getDsModalidadePgtoCliente());

		saida.setQtdeTotalPagtoPrevistoSoltc(response
				.getQtdeTotalPagtoPrevistoSoltc());
		saida.setVlrTotPagtoPrevistoSolicitacao(response
				.getVlrTotPagtoPrevistoSolicitacao());

		saida.setQtTotalPgtoEfetivadoSolicitacao(response
				.getQtTotalPgtoEfetivadoSolicitacao());
		saida.setVlTotalPgtoEfetuadoSolicitacao(response
				.getVlTotalPgtoEfetuadoSolicitacao());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setCdAutenticacaoSegurancaInclusao(response
				.getCdAutenticacaoSegurancaInclusao());
		saida.setHrInclusaoRegistro(formataData(formataTimestampFromPdc(response.getHrInclusaoRegistro()), "dd/MM/yyyy - HH:mm:ss"));
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setCdAutenticacaoSegurancaManutencao(response
				.getCdAutenticacaoSegurancaManutencao());
		saida.setNmOperacaoFluxoManutencao(response
				.getNmOperacaoFluxoManutencao());
		saida.setDsTipoSolicitacaoAntecipacaoPostergacao(response
				.getDsTipoSolicitacaoAntecipacaoPostergacao());

		saida.setHrManutencaoRegistro(formataData(formataTimestampFromPdc(response.getHrManutencaoRegistro()), "dd/MM/yyyy - HH:mm:ss"));

		saida.setCdIndicadorAntecipPosterg(response
				.getCdIndicadorAntecipPosterg());
		saida.setDtNovaDataAgenda(response.getDtNovaDataAgenda());

		return saida;
	}

	public ExcluirLotePagamentosSaidaDTO excluirSolicAntPosDtPgto(
			ExcluirLotePagamentosEntradaDTO entrada) {

		ExcluirLotePagamentosSaidaDTO saida = new ExcluirLotePagamentosSaidaDTO();
		ExcluirSolicAntPosDtPgtoRequest request = new ExcluirSolicAntPosDtPgtoRequest();
		ExcluirSolicAntPosDtPgtoResponse response = null;

		request.setCdSolicitacaoPagamentoIntegrado(entrada
				.getCdSolicitacaoPagamentoIntegrado());
		request.setNrSolicitacaoPagamentoIntegrado(entrada
				.getNrSolicitacaoPagamentoIntegrado());

		response = getFactoryAdapter().getExcluirSolicAntPosDtPgtoPDCAdapter()
				.invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	public IncluirLotePagamentosSaidaDTO incluirSolicAntPosDtPgto(
			IncluirLotePagamentosEntradaDTO entrada) {

		IncluirLotePagamentosSaidaDTO saida = new IncluirLotePagamentosSaidaDTO();
		
		IncluirSolicAntPosDtPgtoRequest request = new IncluirSolicAntPosDtPgtoRequest();
		IncluirSolicAntPosDtPgtoResponse response = null;
		
		
		request.setCdIndicadorFase(entrada.getCdIndicadorFase());
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setDtPgtoInicial(verificaStringNula(entrada.getDtPgtoInicial()));
		request.setDtPgtoFinal(verificaStringNula(entrada.getDtPgtoFinal()));
		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(0);
		request.setCdBancoDebito(0);
		request.setCdAgenciaDebito(verificaIntegerNulo(entrada.getCdAgenciaDebito()));
		request.setCdContaDebito(0);
		request.setQtdeTotalPagtoPrevistoSoltc(verificaLongNulo(entrada.getQtdeTotalPagtoPrevistoSoltc()));
		request.setVlrTotPagtoPrevistoSolicitacao(verificaBigDecimalNulo(entrada.getVlrTotPagtoPrevistoSolicitacao()));
		request.setCdTipoInscricaoPagador(0);
		request.setCdCpfCnpjPagador(0);
		request.setCdFilialCpfCnpjPagador(0);
		request.setCdControleCpfCnpjPagador(0);
		request.setCdTipoCtaRecebedor(0);
		request.setCdBcoRecebedor(0);
		request.setCdAgenciaRecebedor(0);
		request.setCdContaRecebedor(0);
		request.setCdTipoInscricaoRecebedor(0);
		request.setCdCpfCnpjRecebedor(0);
		request.setCdFilialCnpjRecebedor(0);
		request.setCdControleCpfRecebedor(0);

		request.setCdIndicadorAntecipPosterg(entrada.getCdIndicadorAntecipPosterg());
		request.setDtNovaDataAgenda(entrada.getDtNovaDataAgenda());

		response = getFactoryAdapter().getIncluirSolicAntPosDtPgtoPDCAdapter()
				.invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}