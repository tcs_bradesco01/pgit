/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarEnderecoEmailRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarEnderecoEmailRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdCnpjContrato
     */
    private int _cdCnpjContrato = 0;

    /**
     * keeps track of state for field: _cdCnpjContrato
     */
    private boolean _has_cdCnpjContrato;

    /**
     * Field _cdCpfCnpjDigito
     */
    private int _cdCpfCnpjDigito = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjDigito
     */
    private boolean _has_cdCpfCnpjDigito;

    /**
     * Field _cdTipoPessoa
     */
    private java.lang.String _cdTipoPessoa;

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdEmpresaContrato
     */
    private long _cdEmpresaContrato = 0;

    /**
     * keeps track of state for field: _cdEmpresaContrato
     */
    private boolean _has_cdEmpresaContrato;

    /**
     * Field _cdEspecieEndereco
     */
    private int _cdEspecieEndereco = 0;

    /**
     * keeps track of state for field: _cdEspecieEndereco
     */
    private boolean _has_cdEspecieEndereco;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarEnderecoEmailRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.request.ListarEnderecoEmailRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdCnpjContrato
     * 
     */
    public void deleteCdCnpjContrato()
    {
        this._has_cdCnpjContrato= false;
    } //-- void deleteCdCnpjContrato() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdCpfCnpjDigito
     * 
     */
    public void deleteCdCpfCnpjDigito()
    {
        this._has_cdCpfCnpjDigito= false;
    } //-- void deleteCdCpfCnpjDigito() 

    /**
     * Method deleteCdEmpresaContrato
     * 
     */
    public void deleteCdEmpresaContrato()
    {
        this._has_cdEmpresaContrato= false;
    } //-- void deleteCdEmpresaContrato() 

    /**
     * Method deleteCdEspecieEndereco
     * 
     */
    public void deleteCdEspecieEndereco()
    {
        this._has_cdEspecieEndereco= false;
    } //-- void deleteCdEspecieEndereco() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdCnpjContrato'.
     * 
     * @return int
     * @return the value of field 'cdCnpjContrato'.
     */
    public int getCdCnpjContrato()
    {
        return this._cdCnpjContrato;
    } //-- int getCdCnpjContrato() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpjDigito'.
     * 
     * @return int
     * @return the value of field 'cdCpfCnpjDigito'.
     */
    public int getCdCpfCnpjDigito()
    {
        return this._cdCpfCnpjDigito;
    } //-- int getCdCpfCnpjDigito() 

    /**
     * Returns the value of field 'cdEmpresaContrato'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaContrato'.
     */
    public long getCdEmpresaContrato()
    {
        return this._cdEmpresaContrato;
    } //-- long getCdEmpresaContrato() 

    /**
     * Returns the value of field 'cdEspecieEndereco'.
     * 
     * @return int
     * @return the value of field 'cdEspecieEndereco'.
     */
    public int getCdEspecieEndereco()
    {
        return this._cdEspecieEndereco;
    } //-- int getCdEspecieEndereco() 

    /**
     * Returns the value of field 'cdTipoPessoa'.
     * 
     * @return String
     * @return the value of field 'cdTipoPessoa'.
     */
    public java.lang.String getCdTipoPessoa()
    {
        return this._cdTipoPessoa;
    } //-- java.lang.String getCdTipoPessoa() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdCnpjContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjContrato()
    {
        return this._has_cdCnpjContrato;
    } //-- boolean hasCdCnpjContrato() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdCpfCnpjDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjDigito()
    {
        return this._has_cdCpfCnpjDigito;
    } //-- boolean hasCdCpfCnpjDigito() 

    /**
     * Method hasCdEmpresaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaContrato()
    {
        return this._has_cdEmpresaContrato;
    } //-- boolean hasCdEmpresaContrato() 

    /**
     * Method hasCdEspecieEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEspecieEndereco()
    {
        return this._has_cdEspecieEndereco;
    } //-- boolean hasCdEspecieEndereco() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdCnpjContrato'.
     * 
     * @param cdCnpjContrato the value of field 'cdCnpjContrato'.
     */
    public void setCdCnpjContrato(int cdCnpjContrato)
    {
        this._cdCnpjContrato = cdCnpjContrato;
        this._has_cdCnpjContrato = true;
    } //-- void setCdCnpjContrato(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdCpfCnpjDigito'.
     * 
     * @param cdCpfCnpjDigito the value of field 'cdCpfCnpjDigito'.
     */
    public void setCdCpfCnpjDigito(int cdCpfCnpjDigito)
    {
        this._cdCpfCnpjDigito = cdCpfCnpjDigito;
        this._has_cdCpfCnpjDigito = true;
    } //-- void setCdCpfCnpjDigito(int) 

    /**
     * Sets the value of field 'cdEmpresaContrato'.
     * 
     * @param cdEmpresaContrato the value of field
     * 'cdEmpresaContrato'.
     */
    public void setCdEmpresaContrato(long cdEmpresaContrato)
    {
        this._cdEmpresaContrato = cdEmpresaContrato;
        this._has_cdEmpresaContrato = true;
    } //-- void setCdEmpresaContrato(long) 

    /**
     * Sets the value of field 'cdEspecieEndereco'.
     * 
     * @param cdEspecieEndereco the value of field
     * 'cdEspecieEndereco'.
     */
    public void setCdEspecieEndereco(int cdEspecieEndereco)
    {
        this._cdEspecieEndereco = cdEspecieEndereco;
        this._has_cdEspecieEndereco = true;
    } //-- void setCdEspecieEndereco(int) 

    /**
     * Sets the value of field 'cdTipoPessoa'.
     * 
     * @param cdTipoPessoa the value of field 'cdTipoPessoa'.
     */
    public void setCdTipoPessoa(java.lang.String cdTipoPessoa)
    {
        this._cdTipoPessoa = cdTipoPessoa;
    } //-- void setCdTipoPessoa(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarEnderecoEmailRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.request.ListarEnderecoEmailRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.request.ListarEnderecoEmailRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.request.ListarEnderecoEmailRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.request.ListarEnderecoEmailRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
