/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 10/03/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

import java.util.Date;

/**
 * Nome: ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public class ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO {

    /**
     * Atributo Integer
     */
    private Integer qtMaximaRegistro = null;

    /**
     * Atributo Integer
     */
    private Integer cdProdutoServicoOperacional = null;

    /**
     * Atributo Integer
     */
    private Integer cdProdutoOperacionalRelacionado = null;

    /**
     * Atributo Integer
     */
    private Integer cdOperacaoProdutoServico = null;

    /**
     * Atributo Date
     */
    private Date dtInicialPesquisaHistorico = null;

    /**
     * Atributo Date
     */
    private Date dtFinalPesquisaHistorico = null;

    /**
     * Nome: getQtMaximaRegistro
     * 
     * @return qtMaximaRegistro
     */
    public Integer getQtMaximaRegistro() {
        return qtMaximaRegistro;
    }

    /**
     * Nome: setQtMaximaRegistro
     * 
     * @param qtMaximaRegistro
     */
    public void setQtMaximaRegistro(Integer qtMaximaRegistro) {
        this.qtMaximaRegistro = qtMaximaRegistro;
    }

    /**
     * Nome: getCdProdutoServicoOperacional
     * 
     * @return cdProdutoServicoOperacional
     */
    public Integer getCdProdutoServicoOperacional() {
        return cdProdutoServicoOperacional;
    }

    /**
     * Nome: setCdProdutoServicoOperacional
     * 
     * @param cdProdutoServicoOperacional
     */
    public void setCdProdutoServicoOperacional(Integer cdProdutoServicoOperacional) {
        this.cdProdutoServicoOperacional = cdProdutoServicoOperacional;
    }

    /**
     * Nome: getCdProdutoOperacionalRelacionado
     * 
     * @return cdProdutoOperacionalRelacionado
     */
    public Integer getCdProdutoOperacionalRelacionado() {
        return cdProdutoOperacionalRelacionado;
    }

    /**
     * Nome: setCdProdutoOperacionalRelacionado
     * 
     * @param cdProdutoOperacionalRelacionado
     */
    public void setCdProdutoOperacionalRelacionado(Integer cdProdutoOperacionalRelacionado) {
        this.cdProdutoOperacionalRelacionado = cdProdutoOperacionalRelacionado;
    }

    /**
     * Nome: getCdOperacaoProdutoServico
     * 
     * @return cdOperacaoProdutoServico
     */
    public Integer getCdOperacaoProdutoServico() {
        return cdOperacaoProdutoServico;
    }

    /**
     * Nome: setCdOperacaoProdutoServico
     * 
     * @param cdOperacaoProdutoServico
     */
    public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
        this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
    }

    /**
     * Nome: getDtInicialPesquisaHistorico
     * 
     * @return dtInicialPesquisaHistorico
     */
    public Date getDtInicialPesquisaHistorico() {
        return dtInicialPesquisaHistorico;
    }

    /**
     * Nome: setDtInicialPesquisaHistorico
     * 
     * @param dtInicialPesquisaHistorico
     */
    public void setDtInicialPesquisaHistorico(Date dtInicialPesquisaHistorico) {
        this.dtInicialPesquisaHistorico = dtInicialPesquisaHistorico;
    }

    /**
     * Nome: getDtFinalPesquisaHistorico
     * 
     * @return dtFinalPesquisaHistorico
     */
    public Date getDtFinalPesquisaHistorico() {
        return dtFinalPesquisaHistorico;
    }

    /**
     * Nome: setDtFinalPesquisaHistorico
     * 
     * @param dtFinalPesquisaHistorico
     */
    public void setDtFinalPesquisaHistorico(Date dtFinalPesquisaHistorico) {
        this.dtFinalPesquisaHistorico = dtFinalPesquisaHistorico;
    }

}
