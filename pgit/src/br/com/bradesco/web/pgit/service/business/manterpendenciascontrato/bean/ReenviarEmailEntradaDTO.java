/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean;

/**
 * Nome: ReenviarEmailEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ReenviarEmailEntradaDTO {
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdPendenciaPagamentoIntegrado. */
    private Long cdPendenciaPagamentoIntegrado;
    
    /** Atributo nrPendenciaContratoNegocio. */
    private Long nrPendenciaContratoNegocio;
	
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdPendenciaPagamentoIntegrado.
	 *
	 * @return cdPendenciaPagamentoIntegrado
	 */
	public Long getCdPendenciaPagamentoIntegrado() {
		return cdPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: cdPendenciaPagamentoIntegrado.
	 *
	 * @param cdPendenciaPagamentoIntegrado the cd pendencia pagamento integrado
	 */
	public void setCdPendenciaPagamentoIntegrado(Long cdPendenciaPagamentoIntegrado) {
		this.cdPendenciaPagamentoIntegrado = cdPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: nrPendenciaContratoNegocio.
	 *
	 * @return nrPendenciaContratoNegocio
	 */
	public Long getNrPendenciaContratoNegocio() {
		return nrPendenciaContratoNegocio;
	}
	
	/**
	 * Set: nrPendenciaContratoNegocio.
	 *
	 * @param nrPendenciaContratoNegocio the nr pendencia contrato negocio
	 */
	public void setNrPendenciaContratoNegocio(Long nrPendenciaContratoNegocio) {
		this.nrPendenciaContratoNegocio = nrPendenciaContratoNegocio;
	}
	
	
	

}
