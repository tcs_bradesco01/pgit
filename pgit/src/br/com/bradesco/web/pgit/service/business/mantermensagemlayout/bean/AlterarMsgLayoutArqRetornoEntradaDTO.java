/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean;

/**
 * Nome: AlterarMsgLayoutArqRetornoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarMsgLayoutArqRetornoEntradaDTO{
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo nrMensagemArquivoRetorno. */
	private Integer nrMensagemArquivoRetorno;
	
	/** Atributo cdMensagemArquivoRetorno. */
	private String cdMensagemArquivoRetorno;
	
	/** Atributo cdTipoMensagemRetorno. */
	private Integer cdTipoMensagemRetorno;
	
	/** Atributo nrPosicaoInicialMensagem. */
	private Integer nrPosicaoInicialMensagem;
	
	/** Atributo nrPosicaoFinalMensagem. */
	private Integer nrPosicaoFinalMensagem;
	
	/** Atributo cdNivelMensagemLayout. */
	private Integer cdNivelMensagemLayout;
	
	/** Atributo dsMensagemLayoutRetorno. */
	private String dsMensagemLayoutRetorno;

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: nrMensagemArquivoRetorno.
	 *
	 * @return nrMensagemArquivoRetorno
	 */
	public Integer getNrMensagemArquivoRetorno(){
		return nrMensagemArquivoRetorno;
	}

	/**
	 * Set: nrMensagemArquivoRetorno.
	 *
	 * @param nrMensagemArquivoRetorno the nr mensagem arquivo retorno
	 */
	public void setNrMensagemArquivoRetorno(Integer nrMensagemArquivoRetorno){
		this.nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
	}

	/**
	 * Get: cdMensagemArquivoRetorno.
	 *
	 * @return cdMensagemArquivoRetorno
	 */
	public String getCdMensagemArquivoRetorno(){
		return cdMensagemArquivoRetorno;
	}

	/**
	 * Set: cdMensagemArquivoRetorno.
	 *
	 * @param cdMensagemArquivoRetorno the cd mensagem arquivo retorno
	 */
	public void setCdMensagemArquivoRetorno(String cdMensagemArquivoRetorno){
		this.cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
	}

	/**
	 * Get: cdTipoMensagemRetorno.
	 *
	 * @return cdTipoMensagemRetorno
	 */
	public Integer getCdTipoMensagemRetorno(){
		return cdTipoMensagemRetorno;
	}

	/**
	 * Set: cdTipoMensagemRetorno.
	 *
	 * @param cdTipoMensagemRetorno the cd tipo mensagem retorno
	 */
	public void setCdTipoMensagemRetorno(Integer cdTipoMensagemRetorno){
		this.cdTipoMensagemRetorno = cdTipoMensagemRetorno;
	}

	/**
	 * Get: nrPosicaoInicialMensagem.
	 *
	 * @return nrPosicaoInicialMensagem
	 */
	public Integer getNrPosicaoInicialMensagem(){
		return nrPosicaoInicialMensagem;
	}

	/**
	 * Set: nrPosicaoInicialMensagem.
	 *
	 * @param nrPosicaoInicialMensagem the nr posicao inicial mensagem
	 */
	public void setNrPosicaoInicialMensagem(Integer nrPosicaoInicialMensagem){
		this.nrPosicaoInicialMensagem = nrPosicaoInicialMensagem;
	}

	/**
	 * Get: nrPosicaoFinalMensagem.
	 *
	 * @return nrPosicaoFinalMensagem
	 */
	public Integer getNrPosicaoFinalMensagem(){
		return nrPosicaoFinalMensagem;
	}

	/**
	 * Set: nrPosicaoFinalMensagem.
	 *
	 * @param nrPosicaoFinalMensagem the nr posicao final mensagem
	 */
	public void setNrPosicaoFinalMensagem(Integer nrPosicaoFinalMensagem){
		this.nrPosicaoFinalMensagem = nrPosicaoFinalMensagem;
	}

	/**
	 * Get: cdNivelMensagemLayout.
	 *
	 * @return cdNivelMensagemLayout
	 */
	public Integer getCdNivelMensagemLayout(){
		return cdNivelMensagemLayout;
	}

	/**
	 * Set: cdNivelMensagemLayout.
	 *
	 * @param cdNivelMensagemLayout the cd nivel mensagem layout
	 */
	public void setCdNivelMensagemLayout(Integer cdNivelMensagemLayout){
		this.cdNivelMensagemLayout = cdNivelMensagemLayout;
	}

	/**
	 * Get: dsMensagemLayoutRetorno.
	 *
	 * @return dsMensagemLayoutRetorno
	 */
	public String getDsMensagemLayoutRetorno(){
		return dsMensagemLayoutRetorno;
	}

	/**
	 * Set: dsMensagemLayoutRetorno.
	 *
	 * @param dsMensagemLayoutRetorno the ds mensagem layout retorno
	 */
	public void setDsMensagemLayoutRetorno(String dsMensagemLayoutRetorno){
		this.dsMensagemLayoutRetorno = dsMensagemLayoutRetorno;
	}
}