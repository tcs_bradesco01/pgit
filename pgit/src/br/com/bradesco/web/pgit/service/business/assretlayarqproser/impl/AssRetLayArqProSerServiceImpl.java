/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.assretlayarqproser.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerServiceConstants;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ConsultarAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ConsultarAssVersaoLayoutLoteServicoSaidaDto;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.DetalharAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.DetalharAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssVersaoLayoutLoteServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssVersaoLayoutLoteServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.
bean.ListarAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.
bean.ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssVersaoLayoutLoteServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.
request.ConsultarAssVersaoLayoutLoteServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.
response.ConsultarAssVersaoLayoutLoteServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharassretornolayoutservico.
request.DetalharAssRetornoLayoutServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharassretornolayoutservico.
response.DetalharAssRetornoLayoutServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassretornolayoutservico.
request.ExcluirAssRetornoLayoutServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassretornolayoutservico.
response.ExcluirAssRetornoLayoutServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassversaolayoutloteservico.
request.ExcluirAssVersaoLayoutLoteServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassversaolayoutloteservico.
response.ExcluirAssVersaoLayoutLoteServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.
request.IncluirAssRetornoLayoutServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.
response.IncluirAssRetornoLayoutServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassversaolayoutloteservico.
request.IncluirAssVersaoLayoutLoteServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassversaolayoutloteservico.
response.IncluirAssVersaoLayoutLoteServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarassretornolayoutservico.
request.ListarAssRetornoLayoutServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarassretornolayoutservico.
response.ListarAssRetornoLayoutServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.
request.ListarAssVersaoLayoutLoteServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.
response.ListarAssVersaoLayoutLoteServicoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AssRetLayArqProSer
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AssRetLayArqProSerServiceImpl implements IAssRetLayArqProSerService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
     * Construtor.
     */
    public AssRetLayArqProSerServiceImpl() {
        // TODO: Implementa��o
    }

    /**
     * M�todo de exemplo.
     * 
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSer#sampleAssRetLayArqProSer()
     */
    public void sampleAssRetLayArqProSer() {
        // TODO: Implementa�ao
    }

    /**
     * Get: factoryAdapter.
     * 
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     * 
     * @param factoryAdapter
     *            the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
        this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService#
     * listarAssRetLayArqProSer(br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.
     * ListarAssRetLayArqProSerEntradaDTO)
     * * @param entrada
     */
    public List<ListarAssRetLayArqProSerSaidaDTO> listarAssRetLayArqProSer(
        ListarAssRetLayArqProSerEntradaDTO listarAssRetLayArqProSerEntradaDTO) {

        List<ListarAssRetLayArqProSerSaidaDTO> listaRetorno = new ArrayList<ListarAssRetLayArqProSerSaidaDTO>();
        ListarAssRetornoLayoutServicoRequest request = new ListarAssRetornoLayoutServicoRequest();
        ListarAssRetornoLayoutServicoResponse response = new ListarAssRetornoLayoutServicoResponse();

        request.setCdProdutoServicoOper(listarAssRetLayArqProSerEntradaDTO.getTipoServico());
        request.setCdTipoRetorno(listarAssRetLayArqProSerEntradaDTO.getTipoArquivoRetorno());
        request.setCdTipoLayoutArquivo(listarAssRetLayArqProSerEntradaDTO.getTipoLayoutArquivo());
        request.setQtConsultas(IAssRetLayArqProSerServiceConstants.QTDE_CONSULTAS_LISTAR);

        response = getFactoryAdapter().getListarAssRetornoLayoutServicoPDCAdapter().invokeProcess(request);

        ListarAssRetLayArqProSerSaidaDTO saidaDTO;

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            saidaDTO = new ListarAssRetLayArqProSerSaidaDTO();
            saidaDTO.setCodMensagem(response.getCodMensagem());
            saidaDTO.setMensagem(response.getMensagem());
            saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServicoOper());
            saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServicoOper());
            saidaDTO.setCdTipoArquivoRetorno(response.getOcorrencias(i).getCdTipoRetorno());
            saidaDTO.setDsTipoArquivoRetorno(response.getOcorrencias(i).getDsTipoRetorno());
            saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
            saidaDTO.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());

            listaRetorno.add(saidaDTO);
        }

        return listaRetorno;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService#
     * detalharAssRetLayArqProSer(br.com.bradesco.web.pgit.service.business.assretlayarqproser.
     * bean.DetalharAssRetLayArqProSerEntradaDTO)
     * * @param entrada
     */
    public DetalharAssRetLayArqProSerSaidaDTO detalharAssRetLayArqProSer(
        DetalharAssRetLayArqProSerEntradaDTO detalharAssRetLayArqProSerEntradaDTO) {

        DetalharAssRetLayArqProSerSaidaDTO saidaDTO = new DetalharAssRetLayArqProSerSaidaDTO();
        DetalharAssRetornoLayoutServicoRequest request = new DetalharAssRetornoLayoutServicoRequest();
        DetalharAssRetornoLayoutServicoResponse response = new DetalharAssRetornoLayoutServicoResponse();

        request.setCdProdutoServicoOper(detalharAssRetLayArqProSerEntradaDTO.getTipoServico());
        request.setCdTipoRetorno(detalharAssRetLayArqProSerEntradaDTO.getTipoArquivoRetorno());
        request.setCdTipoLayoutArquivo(detalharAssRetLayArqProSerEntradaDTO.getTipoLayoutArquivo());
        request.setQtConsultas(0);

        response = getFactoryAdapter().getDetalharAssRetornoLayoutServicoPDCAdapter().invokeProcess(request);

        saidaDTO = new DetalharAssRetLayArqProSerSaidaDTO();
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());
        saidaDTO.setCdTipoServico(response.getOcorrencias(0).getCdProdutoServicoOper());
        saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOper());
        saidaDTO.setCdTipoArquivoRetorno(response.getOcorrencias(0).getCdTipoRetorno());
        saidaDTO.setDsTipoArquivoRetorno(response.getOcorrencias(0).getDsTipoRetorno());
        saidaDTO.setCdLayoutArquivo(response.getOcorrencias(0).getCdTipoLayoutArquivo());
        saidaDTO.setDsLayoutArquivo(response.getOcorrencias(0).getDsTipoLayoutArquivo());
        saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
        saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
        saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
        saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegrcInclusao());
        saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
        saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
        saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
        saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
        saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
        saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());

        return saidaDTO;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService#
     * incluirAssRetLayArqProSer(br.com.bradesco.web.pgit.service.business.assretlayarqproser.
     * bean.IncluirAssRetLayArqProSerEntradaDTO)
     * * @param entrada
     */
    public IncluirAssRetLayArqProSerSaidaDTO incluirAssRetLayArqProSer(
        IncluirAssRetLayArqProSerEntradaDTO incluirAssRetLayArqProSerEntradaDTO) {

        IncluirAssRetLayArqProSerSaidaDTO incluirAssRetLayArqProSerSaidaDTO = new IncluirAssRetLayArqProSerSaidaDTO();
        IncluirAssRetornoLayoutServicoRequest incluirAssRetornoLayoutServicoRequest =
            new IncluirAssRetornoLayoutServicoRequest();
        IncluirAssRetornoLayoutServicoResponse incluirAssRetornoLayoutServicoResponse =
            new IncluirAssRetornoLayoutServicoResponse();

        incluirAssRetornoLayoutServicoRequest.setCdProdutoServicoOper(incluirAssRetLayArqProSerEntradaDTO
            .getTipoServico());
        incluirAssRetornoLayoutServicoRequest.setCdTipoRetorno(incluirAssRetLayArqProSerEntradaDTO
            .getTipoArquivoRetorno());
        incluirAssRetornoLayoutServicoRequest.setCdTipoLayoutArquivo(incluirAssRetLayArqProSerEntradaDTO
            .getTipoLayoutArquivo());
        incluirAssRetornoLayoutServicoRequest.setQtConsultas(0);

        incluirAssRetornoLayoutServicoResponse =
            getFactoryAdapter().getIncluirAssRetornoLayoutServicoPDCAdapter().invokeProcess(
                incluirAssRetornoLayoutServicoRequest);

        incluirAssRetLayArqProSerSaidaDTO.setCodMensagem(incluirAssRetornoLayoutServicoResponse.getCodMensagem());
        incluirAssRetLayArqProSerSaidaDTO.setMensagem(incluirAssRetornoLayoutServicoResponse.getMensagem());

        return incluirAssRetLayArqProSerSaidaDTO;
    }

    /**
     * (non-Javadoc)
     * 
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService#
     * excluirAssRetLayArqProSer(br.com.bradesco.web.pgit.service.business.assretlayarqproser.
     * bean.ExcluirAssRetLayArqProSerEntradaDTO)
     * * @param entrada
     */
    public ExcluirAssRetLayArqProSerSaidaDTO excluirAssRetLayArqProSer(
        ExcluirAssRetLayArqProSerEntradaDTO excluirAssRetLayArqProSerEntradaDTO) {

        ExcluirAssRetLayArqProSerSaidaDTO excluirAssRetLayArqProSerSaidaDTO = new ExcluirAssRetLayArqProSerSaidaDTO();
        ExcluirAssRetornoLayoutServicoRequest excluirAssRetornoLayoutServicoRequest =
            new ExcluirAssRetornoLayoutServicoRequest();
        ExcluirAssRetornoLayoutServicoResponse excluirAssRetornoLayoutServicoResponse =
            new ExcluirAssRetornoLayoutServicoResponse();

        excluirAssRetornoLayoutServicoRequest.setCdProdutoServicoOper(excluirAssRetLayArqProSerEntradaDTO
            .getTipoServico());
        excluirAssRetornoLayoutServicoRequest.setCdTipoRetorno(excluirAssRetLayArqProSerEntradaDTO
            .getTipoArquivoRetorno());
        excluirAssRetornoLayoutServicoRequest.setCdTipoLayoutArquivo(excluirAssRetLayArqProSerEntradaDTO
            .getTipoLayoutArquivo());
        excluirAssRetornoLayoutServicoRequest.setQtConsultas(0);

        excluirAssRetornoLayoutServicoResponse =
            getFactoryAdapter().getExcluirAssRetornoLayoutServicoPDCAdapter().invokeProcess(
                excluirAssRetornoLayoutServicoRequest);

        excluirAssRetLayArqProSerSaidaDTO.setCodMensagem(excluirAssRetornoLayoutServicoResponse.getCodMensagem());
        excluirAssRetLayArqProSerSaidaDTO.setMensagem(excluirAssRetornoLayoutServicoResponse.getMensagem());

        return excluirAssRetLayArqProSerSaidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService#
     * excluirAssVersaoLayoutLoteServico(br.com.bradesco.web.pgit.
     * service.business.assretlayarqproser.bean.ExcluirAssVersaoLayoutLoteServicoEntradaDTO)
     * @param entrada
     */
    public ExcluirAssVersaoLayoutLoteServicoSaidaDTO excluirAssVersaoLayoutLoteServico(
        ExcluirAssVersaoLayoutLoteServicoEntradaDTO entrada) {
        ExcluirAssVersaoLayoutLoteServicoRequest request = new ExcluirAssVersaoLayoutLoteServicoRequest();
        ExcluirAssVersaoLayoutLoteServicoResponse response = new ExcluirAssVersaoLayoutLoteServicoResponse();
        ExcluirAssVersaoLayoutLoteServicoSaidaDTO saida = new ExcluirAssVersaoLayoutLoteServicoSaidaDTO();

        request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setNrVersaoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getNrVersaoLayoutArquivo()));
        request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLoteLayout()));
        request.setNrVersaoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getNrVersaoLoteLayout()));
        request.setCdTipoServicoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdTipoServicoCnab()));

        response = getFactoryAdapter().getExcluirAssVersaoLayoutLoteServicoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService#
     * incluirAssVersaoLayoutLoteServico(br.com.bradesco.web.pgit.service.business.
     * assretlayarqproser.bean.IncluirAssVersaoLayoutLoteServicoEntradaDTO)
     * @param entrada
     */
    public IncluirAssVersaoLayoutLoteServicoSaidaDTO incluirAssVersaoLayoutLoteServico(
        IncluirAssVersaoLayoutLoteServicoEntradaDTO entrada) {
        IncluirAssVersaoLayoutLoteServicoRequest request = new IncluirAssVersaoLayoutLoteServicoRequest();
        IncluirAssVersaoLayoutLoteServicoResponse responseIncluir = new IncluirAssVersaoLayoutLoteServicoResponse();
        IncluirAssVersaoLayoutLoteServicoSaidaDTO saida = new IncluirAssVersaoLayoutLoteServicoSaidaDTO();

        request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setNrVersaoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getNrVersaoLayoutArquivo()));
        request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLoteLayout()));
        request.setNrVersaoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getNrVersaoLoteLayout()));
        request.setCdTipoServicoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdTipoServicoCnab()));

        responseIncluir = getFactoryAdapter().getIncluirAssVersaoLayoutLoteServicoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(responseIncluir.getCodMensagem());
        saida.setMensagem(responseIncluir.getMensagem());

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService#
     * listarAssVersaoLayoutLoteServico(br.com.bradesco.web.pgit.service.business.
     * assretlayarqproser.bean.ListarAssVersaoLayoutLoteServicoEntradaDTO)
     * @param entrada
     */
    public ListarAssVersaoLayoutLoteServicoSaidaDTO listarAssVersaoLayoutLoteServico(
        ListarAssVersaoLayoutLoteServicoEntradaDTO entrada) {
        ListarAssVersaoLayoutLoteServicoRequest request = new ListarAssVersaoLayoutLoteServicoRequest();
        ListarAssVersaoLayoutLoteServicoResponse response = new ListarAssVersaoLayoutLoteServicoResponse();
        List<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO> listaRetorno =
            new ArrayList<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO>();

        request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setNumeroVersaoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getNrVersaoLayoutArquivo()));
        request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLoteLayout()));
        request.setNrVersaoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getNrVersaoLoteLayout()));
        request.setCdTipoServicoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdTipoServicoCnab()));

        response = getFactoryAdapter().getListarAssVersaoLayoutLoteServicoPDCAdapter().invokeProcess(request);

        ListarAssVersaoLayoutLoteServicoSaidaDTO saida = new ListarAssVersaoLayoutLoteServicoSaidaDTO();
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNumeroLinhas(response.getNumeroLinhas());

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO obj =
                new ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO();
            obj.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
            obj.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
            obj.setNrVersaoLayoutArquivo(response.getOcorrencias(i).getNumeroVersaoLayoutArquivo());
            obj.setCdTipoLoteLayout(response.getOcorrencias(i).getCdTipoLoteLayout());
            obj.setDsTipoLoteLayout(response.getOcorrencias(i).getDsTipoLoteLayout());
            obj.setNrVersaoLoteLayout(response.getOcorrencias(i).getNrVersaoLoteLayout());
            obj.setCdTipoServicoCnab(response.getOcorrencias(i).getCdTipoServicoCnab());
            obj.setDsTipoServicoCnab(response.getOcorrencias(i).getDsTipoServicoCnab());

            listaRetorno.add(obj);
        }

        saida.setOcorrencias(listaRetorno);

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService#
     * ConsultarAssVersaoLayoutLoteServico(br.com.bradesco.web.pgit.service.business.
     * assretlayarqproser.bean.ConsultarAssVersaoLayoutLoteServicoEntradaDTO)
     * @param entrada
     */
    public ConsultarAssVersaoLayoutLoteServicoSaidaDto ConsultarAssVersaoLayoutLoteServico(
        ConsultarAssVersaoLayoutLoteServicoEntradaDTO entrada) {
        ConsultarAssVersaoLayoutLoteServicoRequest request = new ConsultarAssVersaoLayoutLoteServicoRequest();
        ConsultarAssVersaoLayoutLoteServicoResponse response = new ConsultarAssVersaoLayoutLoteServicoResponse();
        ConsultarAssVersaoLayoutLoteServicoSaidaDto saida = new ConsultarAssVersaoLayoutLoteServicoSaidaDto();

        request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setNrVersaoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getNrVersaoLayoutArquivo()));
        request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLoteLayout()));
        request.setNrVersaoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getNrVersaoLoteLayout()));
        request.setCdTipoServicoCnab(PgitUtil.verificaIntegerNulo(entrada.getCdTipoServicoCnab()));

        response = getFactoryAdapter().getConsultarAssVersaoLayoutLoteServicoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
        saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
        saida.setNrVersaoLayoutArquivo(response.getNrVersaoLayoutArquivo());
        saida.setCdTipoLoteLayout(response.getCdTipoLoteLayout());
        saida.setDsTipoLoteLayout(response.getDsTipoLoteLayout());
        saida.setNrVersaoLoteLayout(response.getNrVersaoLoteLayout());
        saida.setCdTipoServicoCnab(response.getCdTipoServicoCnab());
        saida.setDsTipoServicoCnab(response.getDsTipoServicoCnab());
        saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
        saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
        saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
        saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());

        return saida;
    }

}
