/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.teste;

import br.com.bradesco.web.pgit.service.business.teste.bean.DadosFinanceirosTesteDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterSolicitacaoReplicacao
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IDadosFinanceirosTesteService {

	  /**
  	 * Consultar.
  	 *
  	 * @param cnpj the cnpj
  	 * @param cnpj2 the cnpj2
  	 * @param cnpj3 the cnpj3
  	 * @param cpf the cpf
  	 * @param cpf2 the cpf2
  	 * @param dtSist the dt sist
  	 * @param tipoPesquisaSelecionada the tipo pesquisa selecionada
  	 * @return the dados financeiros teste dto
  	 */
  	DadosFinanceirosTesteDTO consultar(Long cnpj, Integer cnpj2, Integer cnpj3, Long cpf, Integer cpf2, Integer dtSist, Integer tipoPesquisaSelecionada);
}