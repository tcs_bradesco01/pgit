/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ConsultarListaValoresDiscretosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaValoresDiscretosSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdCombo. */
	private Long cdCombo;
	
	/** Atributo dsCombo. */
	private String dsCombo;
	
	/**
	 * Consultar lista valores discretos saida dto.
	 *
	 * @param cdCombo the cd combo
	 * @param dsCombo the ds combo
	 */
	public ConsultarListaValoresDiscretosSaidaDTO(Long cdCombo, String dsCombo) {
		super();
		this.cdCombo = cdCombo;
		this.dsCombo = dsCombo;
	}
	
	/**
	 * Get: cdCombo.
	 *
	 * @return cdCombo
	 */
	public Long getCdCombo() {
		return cdCombo;
	}
	
	/**
	 * Set: cdCombo.
	 *
	 * @param cdCombo the cd combo
	 */
	public void setCdCombo(Long cdCombo) {
		this.cdCombo = cdCombo;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsCombo.
	 *
	 * @return dsCombo
	 */
	public String getDsCombo() {
		return dsCombo;
	}
	
	/**
	 * Set: dsCombo.
	 *
	 * @param dsCombo the ds combo
	 */
	public void setDsCombo(String dsCombo) {
		this.dsCombo = dsCombo;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	

}
