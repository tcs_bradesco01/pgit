/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdOrgaoPagadorOrganizacao
     */
    private int _cdOrgaoPagadorOrganizacao = 0;

    /**
     * keeps track of state for field: _cdOrgaoPagadorOrganizacao
     */
    private boolean _has_cdOrgaoPagadorOrganizacao;

    /**
     * Field _cdTipoUnidadeOrganizacao
     */
    private int _cdTipoUnidadeOrganizacao = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacao
     */
    private boolean _has_cdTipoUnidadeOrganizacao;

    /**
     * Field _dsTipoUnidadeOrganizacao
     */
    private java.lang.String _dsTipoUnidadeOrganizacao;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _dsPessoaJuridica
     */
    private java.lang.String _dsPessoaJuridica;

    /**
     * Field _nrSequenciaUnidadeOrganizacao
     */
    private int _nrSequenciaUnidadeOrganizacao = 0;

    /**
     * keeps track of state for field: _nrSequenciaUnidadeOrganizaca
     */
    private boolean _has_nrSequenciaUnidadeOrganizacao;

    /**
     * Field _dsNumeroSequenciaUnidadeOrganizacao
     */
    private java.lang.String _dsNumeroSequenciaUnidadeOrganizacao;

    /**
     * Field _cdSituacaoOrganizacaoPagador
     */
    private int _cdSituacaoOrganizacaoPagador = 0;

    /**
     * keeps track of state for field: _cdSituacaoOrganizacaoPagador
     */
    private boolean _has_cdSituacaoOrganizacaoPagador;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOrgaoPagadorOrganizacao
     * 
     */
    public void deleteCdOrgaoPagadorOrganizacao()
    {
        this._has_cdOrgaoPagadorOrganizacao= false;
    } //-- void deleteCdOrgaoPagadorOrganizacao() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdSituacaoOrganizacaoPagador
     * 
     */
    public void deleteCdSituacaoOrganizacaoPagador()
    {
        this._has_cdSituacaoOrganizacaoPagador= false;
    } //-- void deleteCdSituacaoOrganizacaoPagador() 

    /**
     * Method deleteCdTipoUnidadeOrganizacao
     * 
     */
    public void deleteCdTipoUnidadeOrganizacao()
    {
        this._has_cdTipoUnidadeOrganizacao= false;
    } //-- void deleteCdTipoUnidadeOrganizacao() 

    /**
     * Method deleteNrSequenciaUnidadeOrganizacao
     * 
     */
    public void deleteNrSequenciaUnidadeOrganizacao()
    {
        this._has_nrSequenciaUnidadeOrganizacao= false;
    } //-- void deleteNrSequenciaUnidadeOrganizacao() 

    /**
     * Returns the value of field 'cdOrgaoPagadorOrganizacao'.
     * 
     * @return int
     * @return the value of field 'cdOrgaoPagadorOrganizacao'.
     */
    public int getCdOrgaoPagadorOrganizacao()
    {
        return this._cdOrgaoPagadorOrganizacao;
    } //-- int getCdOrgaoPagadorOrganizacao() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdSituacaoOrganizacaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOrganizacaoPagador'.
     */
    public int getCdSituacaoOrganizacaoPagador()
    {
        return this._cdSituacaoOrganizacaoPagador;
    } //-- int getCdSituacaoOrganizacaoPagador() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacao'.
     */
    public int getCdTipoUnidadeOrganizacao()
    {
        return this._cdTipoUnidadeOrganizacao;
    } //-- int getCdTipoUnidadeOrganizacao() 

    /**
     * Returns the value of field
     * 'dsNumeroSequenciaUnidadeOrganizacao'.
     * 
     * @return String
     * @return the value of field
     * 'dsNumeroSequenciaUnidadeOrganizacao'.
     */
    public java.lang.String getDsNumeroSequenciaUnidadeOrganizacao()
    {
        return this._dsNumeroSequenciaUnidadeOrganizacao;
    } //-- java.lang.String getDsNumeroSequenciaUnidadeOrganizacao() 

    /**
     * Returns the value of field 'dsPessoaJuridica'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridica'.
     */
    public java.lang.String getDsPessoaJuridica()
    {
        return this._dsPessoaJuridica;
    } //-- java.lang.String getDsPessoaJuridica() 

    /**
     * Returns the value of field 'dsTipoUnidadeOrganizacao'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeOrganizacao'.
     */
    public java.lang.String getDsTipoUnidadeOrganizacao()
    {
        return this._dsTipoUnidadeOrganizacao;
    } //-- java.lang.String getDsTipoUnidadeOrganizacao() 

    /**
     * Returns the value of field 'nrSequenciaUnidadeOrganizacao'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeOrganizacao'.
     */
    public int getNrSequenciaUnidadeOrganizacao()
    {
        return this._nrSequenciaUnidadeOrganizacao;
    } //-- int getNrSequenciaUnidadeOrganizacao() 

    /**
     * Method hasCdOrgaoPagadorOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrgaoPagadorOrganizacao()
    {
        return this._has_cdOrgaoPagadorOrganizacao;
    } //-- boolean hasCdOrgaoPagadorOrganizacao() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdSituacaoOrganizacaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOrganizacaoPagador()
    {
        return this._has_cdSituacaoOrganizacaoPagador;
    } //-- boolean hasCdSituacaoOrganizacaoPagador() 

    /**
     * Method hasCdTipoUnidadeOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacao()
    {
        return this._has_cdTipoUnidadeOrganizacao;
    } //-- boolean hasCdTipoUnidadeOrganizacao() 

    /**
     * Method hasNrSequenciaUnidadeOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeOrganizacao()
    {
        return this._has_nrSequenciaUnidadeOrganizacao;
    } //-- boolean hasNrSequenciaUnidadeOrganizacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOrgaoPagadorOrganizacao'.
     * 
     * @param cdOrgaoPagadorOrganizacao the value of field
     * 'cdOrgaoPagadorOrganizacao'.
     */
    public void setCdOrgaoPagadorOrganizacao(int cdOrgaoPagadorOrganizacao)
    {
        this._cdOrgaoPagadorOrganizacao = cdOrgaoPagadorOrganizacao;
        this._has_cdOrgaoPagadorOrganizacao = true;
    } //-- void setCdOrgaoPagadorOrganizacao(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdSituacaoOrganizacaoPagador'.
     * 
     * @param cdSituacaoOrganizacaoPagador the value of field
     * 'cdSituacaoOrganizacaoPagador'.
     */
    public void setCdSituacaoOrganizacaoPagador(int cdSituacaoOrganizacaoPagador)
    {
        this._cdSituacaoOrganizacaoPagador = cdSituacaoOrganizacaoPagador;
        this._has_cdSituacaoOrganizacaoPagador = true;
    } //-- void setCdSituacaoOrganizacaoPagador(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacao'.
     * 
     * @param cdTipoUnidadeOrganizacao the value of field
     * 'cdTipoUnidadeOrganizacao'.
     */
    public void setCdTipoUnidadeOrganizacao(int cdTipoUnidadeOrganizacao)
    {
        this._cdTipoUnidadeOrganizacao = cdTipoUnidadeOrganizacao;
        this._has_cdTipoUnidadeOrganizacao = true;
    } //-- void setCdTipoUnidadeOrganizacao(int) 

    /**
     * Sets the value of field
     * 'dsNumeroSequenciaUnidadeOrganizacao'.
     * 
     * @param dsNumeroSequenciaUnidadeOrganizacao the value of
     * field 'dsNumeroSequenciaUnidadeOrganizacao'.
     */
    public void setDsNumeroSequenciaUnidadeOrganizacao(java.lang.String dsNumeroSequenciaUnidadeOrganizacao)
    {
        this._dsNumeroSequenciaUnidadeOrganizacao = dsNumeroSequenciaUnidadeOrganizacao;
    } //-- void setDsNumeroSequenciaUnidadeOrganizacao(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridica'.
     * 
     * @param dsPessoaJuridica the value of field 'dsPessoaJuridica'
     */
    public void setDsPessoaJuridica(java.lang.String dsPessoaJuridica)
    {
        this._dsPessoaJuridica = dsPessoaJuridica;
    } //-- void setDsPessoaJuridica(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeOrganizacao'.
     * 
     * @param dsTipoUnidadeOrganizacao the value of field
     * 'dsTipoUnidadeOrganizacao'.
     */
    public void setDsTipoUnidadeOrganizacao(java.lang.String dsTipoUnidadeOrganizacao)
    {
        this._dsTipoUnidadeOrganizacao = dsTipoUnidadeOrganizacao;
    } //-- void setDsTipoUnidadeOrganizacao(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeOrganizacao'.
     * 
     * @param nrSequenciaUnidadeOrganizacao the value of field
     * 'nrSequenciaUnidadeOrganizacao'.
     */
    public void setNrSequenciaUnidadeOrganizacao(int nrSequenciaUnidadeOrganizacao)
    {
        this._nrSequenciaUnidadeOrganizacao = nrSequenciaUnidadeOrganizacao;
        this._has_nrSequenciaUnidadeOrganizacao = true;
    } //-- void setNrSequenciaUnidadeOrganizacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
