/*
 * Nome: br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.response.Ocorrencias;

/**
 * Nome: ConRelFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConRelFavorecidoSaidaDTO {
    
    /** Atributo cdSolicitacaoPagamento. */
    private Integer cdSolicitacaoPagamento;
    
    /** Atributo nrSolicitacao. */
    private Integer nrSolicitacao;
    
    /** Atributo dsTipoRelatorio. */
    private String dsTipoRelatorio;
    
    /** Atributo dsDataSolicitacao. */
    private String dsDataSolicitacao;
    
    /** Atributo dsHoraSolicitacao. */
    private String dsHoraSolicitacao;
    
    /** Atributo dsSituacao. */
    private String dsSituacao;
    
	/**
	 * Con rel favorecido saida dto.
	 */
	public ConRelFavorecidoSaidaDTO() {
		super();
	}
	
	/**
	 * Con rel favorecido saida dto.
	 *
	 * @param saida the saida
	 */
	public ConRelFavorecidoSaidaDTO(Ocorrencias saida) {
	    this.cdSolicitacaoPagamento = saida.getCdSolicitacaoPagamento();
	    this.nrSolicitacao = saida.getNrSolicitacao();
	    this.dsTipoRelatorio = saida.getDsTipoRelatorio();
	    this.dsDataSolicitacao = saida.getDsDataSolicitacao();
	    this.dsHoraSolicitacao = saida.getDsHoraSolicitacao();
	    this.dsSituacao = saida.getDsSituacao();
	}
	
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}

	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}

	/**
	 * Get: dsDataSolicitacao.
	 *
	 * @return dsDataSolicitacao
	 */
	public String getDsDataSolicitacao() {
		return dsDataSolicitacao;
	}

	/**
	 * Set: dsDataSolicitacao.
	 *
	 * @param dsDataSolicitacao the ds data solicitacao
	 */
	public void setDsDataSolicitacao(String dsDataSolicitacao) {
		this.dsDataSolicitacao = dsDataSolicitacao;
	}

	/**
	 * Get: dsHoraSolicitacao.
	 *
	 * @return dsHoraSolicitacao
	 */
	public String getDsHoraSolicitacao() {
		return dsHoraSolicitacao;
	}

	/**
	 * Set: dsHoraSolicitacao.
	 *
	 * @param dsHoraSolicitacao the ds hora solicitacao
	 */
	public void setDsHoraSolicitacao(String dsHoraSolicitacao) {
		this.dsHoraSolicitacao = dsHoraSolicitacao;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: dsTipoRelatorio.
	 *
	 * @return dsTipoRelatorio
	 */
	public String getDsTipoRelatorio() {
		return dsTipoRelatorio;
	}

	/**
	 * Set: dsTipoRelatorio.
	 *
	 * @param dsTipoRelatorio the ds tipo relatorio
	 */
	public void setDsTipoRelatorio(String dsTipoRelatorio) {
		this.dsTipoRelatorio = dsTipoRelatorio;
	}

	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}

	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}   
}
