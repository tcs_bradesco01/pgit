package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

public class ListarTiposPagtoSaidaOcorrenciasDTO {

	/** Atributo cdTipoPagamentoCliente. */
	private Integer cdTipoPagamentoCliente;

	/** Atributo dsTipoPagamentoCliente. */
	private String dsTipoPagamentoCliente;
	
	/** Atributo checkEvento. */
	public Boolean checkEvento = false;
	
	public Integer getCdTipoPagamentoCliente() {
		return cdTipoPagamentoCliente;
	}	

	public void setCdTipoPagamentoCliente(Integer cdTipoPagamentoCliente) {
		this.cdTipoPagamentoCliente = cdTipoPagamentoCliente;
	}

	public String getDsTipoPagamentoCliente() {
		return dsTipoPagamentoCliente;
	}

	public void setDsTipoPagamentoCliente(String dsTipoPagamentoCliente) {
		this.dsTipoPagamentoCliente = dsTipoPagamentoCliente;
	}

	public Boolean getCheckEvento() {
		return checkEvento;
	}

	public void setCheckEvento(Boolean checkEvento) {
		this.checkEvento = checkEvento;
	}

}
