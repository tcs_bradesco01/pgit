/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoFinalidade
     */
    private int _cdTipoFinalidade = 0;

    /**
     * keeps track of state for field: _cdTipoFinalidade
     */
    private boolean _has_cdTipoFinalidade;

    /**
     * Field _dsTipoFinalidade
     */
    private java.lang.String _dsTipoFinalidade;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoFinalidade
     * 
     */
    public void deleteCdTipoFinalidade()
    {
        this._has_cdTipoFinalidade= false;
    } //-- void deleteCdTipoFinalidade() 

    /**
     * Returns the value of field 'cdTipoFinalidade'.
     * 
     * @return int
     * @return the value of field 'cdTipoFinalidade'.
     */
    public int getCdTipoFinalidade()
    {
        return this._cdTipoFinalidade;
    } //-- int getCdTipoFinalidade() 

    /**
     * Returns the value of field 'dsTipoFinalidade'.
     * 
     * @return String
     * @return the value of field 'dsTipoFinalidade'.
     */
    public java.lang.String getDsTipoFinalidade()
    {
        return this._dsTipoFinalidade;
    } //-- java.lang.String getDsTipoFinalidade() 

    /**
     * Method hasCdTipoFinalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFinalidade()
    {
        return this._has_cdTipoFinalidade;
    } //-- boolean hasCdTipoFinalidade() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoFinalidade'.
     * 
     * @param cdTipoFinalidade the value of field 'cdTipoFinalidade'
     */
    public void setCdTipoFinalidade(int cdTipoFinalidade)
    {
        this._cdTipoFinalidade = cdTipoFinalidade;
        this._has_cdTipoFinalidade = true;
    } //-- void setCdTipoFinalidade(int) 

    /**
     * Sets the value of field 'dsTipoFinalidade'.
     * 
     * @param dsTipoFinalidade the value of field 'dsTipoFinalidade'
     */
    public void setDsTipoFinalidade(java.lang.String dsTipoFinalidade)
    {
        this._dsTipoFinalidade = dsTipoFinalidade;
    } //-- void setDsTipoFinalidade(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
