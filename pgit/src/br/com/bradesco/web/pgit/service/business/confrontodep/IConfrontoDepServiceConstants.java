/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitetura Bradesco Canal Internet
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/pgitIntranet/JavaSource/br/com/bradesco/web/pgit/service/business/confrontodependencia/IConfrontoDepServiceConstants.java,v $
 * $Id: IConfrontoDepServiceConstants.java,v 1.2 2009/05/13 19:39:28 cpm.com.br\heslei.silva Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: IConfrontoDepServiceConstants.java,v $
 * Revision 1.2  2009/05/13 19:39:28  cpm.com.br\heslei.silva
 * voltando para vers�o do branch 2.0
 *
 * Revision 1.1.2.1  2009/05/05 14:24:35  corporate\marcio.alves
 * Adicionando pagina��o ao pgit intranet
 *
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.confrontodep;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface de constantes do adaptador: ConfrontoDependencia
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConfrontoDepServiceConstants {

}

