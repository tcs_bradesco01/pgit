/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMotivoSituacaoParticipante
     */
    private int _cdMotivoSituacaoParticipante = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoParticipante
     */
    private boolean _has_cdMotivoSituacaoParticipante;

    /**
     * Field _dsMotivoSituacaoParticipante
     */
    private java.lang.String _dsMotivoSituacaoParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoSituacaoParticipante
     * 
     */
    public void deleteCdMotivoSituacaoParticipante()
    {
        this._has_cdMotivoSituacaoParticipante= false;
    } //-- void deleteCdMotivoSituacaoParticipante() 

    /**
     * Returns the value of field 'cdMotivoSituacaoParticipante'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoParticipante'.
     */
    public int getCdMotivoSituacaoParticipante()
    {
        return this._cdMotivoSituacaoParticipante;
    } //-- int getCdMotivoSituacaoParticipante() 

    /**
     * Returns the value of field 'dsMotivoSituacaoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoParticipante'.
     */
    public java.lang.String getDsMotivoSituacaoParticipante()
    {
        return this._dsMotivoSituacaoParticipante;
    } //-- java.lang.String getDsMotivoSituacaoParticipante() 

    /**
     * Method hasCdMotivoSituacaoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoParticipante()
    {
        return this._has_cdMotivoSituacaoParticipante;
    } //-- boolean hasCdMotivoSituacaoParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoSituacaoParticipante'.
     * 
     * @param cdMotivoSituacaoParticipante the value of field
     * 'cdMotivoSituacaoParticipante'.
     */
    public void setCdMotivoSituacaoParticipante(int cdMotivoSituacaoParticipante)
    {
        this._cdMotivoSituacaoParticipante = cdMotivoSituacaoParticipante;
        this._has_cdMotivoSituacaoParticipante = true;
    } //-- void setCdMotivoSituacaoParticipante(int) 

    /**
     * Sets the value of field 'dsMotivoSituacaoParticipante'.
     * 
     * @param dsMotivoSituacaoParticipante the value of field
     * 'dsMotivoSituacaoParticipante'.
     */
    public void setDsMotivoSituacaoParticipante(java.lang.String dsMotivoSituacaoParticipante)
    {
        this._dsMotivoSituacaoParticipante = dsMotivoSituacaoParticipante;
    } //-- void setDsMotivoSituacaoParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
