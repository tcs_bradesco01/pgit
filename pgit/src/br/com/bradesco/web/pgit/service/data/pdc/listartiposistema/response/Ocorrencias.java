/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdBaseSistema
     */
    private int _cdBaseSistema = 0;

    /**
     * keeps track of state for field: _cdBaseSistema
     */
    private boolean _has_cdBaseSistema;

    /**
     * Field _dsBaseSistema
     */
    private java.lang.String _dsBaseSistema;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdBaseSistema
     * 
     */
    public void deleteCdBaseSistema()
    {
        this._has_cdBaseSistema= false;
    } //-- void deleteCdBaseSistema() 

    /**
     * Returns the value of field 'cdBaseSistema'.
     * 
     * @return int
     * @return the value of field 'cdBaseSistema'.
     */
    public int getCdBaseSistema()
    {
        return this._cdBaseSistema;
    } //-- int getCdBaseSistema() 

    /**
     * Returns the value of field 'dsBaseSistema'.
     * 
     * @return String
     * @return the value of field 'dsBaseSistema'.
     */
    public java.lang.String getDsBaseSistema()
    {
        return this._dsBaseSistema;
    } //-- java.lang.String getDsBaseSistema() 

    /**
     * Method hasCdBaseSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBaseSistema()
    {
        return this._has_cdBaseSistema;
    } //-- boolean hasCdBaseSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdBaseSistema'.
     * 
     * @param cdBaseSistema the value of field 'cdBaseSistema'.
     */
    public void setCdBaseSistema(int cdBaseSistema)
    {
        this._cdBaseSistema = cdBaseSistema;
        this._has_cdBaseSistema = true;
    } //-- void setCdBaseSistema(int) 

    /**
     * Sets the value of field 'dsBaseSistema'.
     * 
     * @param dsBaseSistema the value of field 'dsBaseSistema'.
     */
    public void setDsBaseSistema(java.lang.String dsBaseSistema)
    {
        this._dsBaseSistema = dsBaseSistema;
    } //-- void setDsBaseSistema(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
