/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarSolicitacaoAmbientePartcResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarSolicitacaoAmbientePartcResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _dsServico
     */
    private java.lang.String _dsServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _dsModalidade
     */
    private java.lang.String _dsModalidade;

    /**
     * Field _cdTipoMudanca
     */
    private java.lang.String _cdTipoMudanca;

    /**
     * Field _cdFormaEfetivacao
     */
    private java.lang.String _cdFormaEfetivacao;

    /**
     * Field _dtProgramada
     */
    private java.lang.String _dtProgramada;

    /**
     * Field _cdSituacao
     */
    private java.lang.String _cdSituacao;

    /**
     * Field _cdMotivoSituacao
     */
    private int _cdMotivoSituacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacao
     */
    private boolean _has_cdMotivoSituacao;

    /**
     * Field _dsMotivoSituacao
     */
    private java.lang.String _dsMotivoSituacao;

    /**
     * Field _cdManterOperacao
     */
    private java.lang.String _cdManterOperacao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCnpjParticipante
     */
    private long _cdCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCnpjParticipante
     */
    private boolean _has_cdCnpjParticipante;

    /**
     * Field _cdFilialParticipante
     */
    private int _cdFilialParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialParticipante
     */
    private boolean _has_cdFilialParticipante;

    /**
     * Field _cdControlePArticipante
     */
    private int _cdControlePArticipante = 0;

    /**
     * keeps track of state for field: _cdControlePArticipante
     */
    private boolean _has_cdControlePArticipante;

    /**
     * Field _dsNomeRazaoParticipante
     */
    private java.lang.String _dsNomeRazaoParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarSolicitacaoAmbientePartcResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.response.ConsultarSolicitacaoAmbientePartcResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjParticipante
     * 
     */
    public void deleteCdCnpjParticipante()
    {
        this._has_cdCnpjParticipante= false;
    } //-- void deleteCdCnpjParticipante() 

    /**
     * Method deleteCdControlePArticipante
     * 
     */
    public void deleteCdControlePArticipante()
    {
        this._has_cdControlePArticipante= false;
    } //-- void deleteCdControlePArticipante() 

    /**
     * Method deleteCdFilialParticipante
     * 
     */
    public void deleteCdFilialParticipante()
    {
        this._has_cdFilialParticipante= false;
    } //-- void deleteCdFilialParticipante() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdMotivoSituacao
     * 
     */
    public void deleteCdMotivoSituacao()
    {
        this._has_cdMotivoSituacao= false;
    } //-- void deleteCdMotivoSituacao() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCnpjParticipante'.
     */
    public long getCdCnpjParticipante()
    {
        return this._cdCnpjParticipante;
    } //-- long getCdCnpjParticipante() 

    /**
     * Returns the value of field 'cdControlePArticipante'.
     * 
     * @return int
     * @return the value of field 'cdControlePArticipante'.
     */
    public int getCdControlePArticipante()
    {
        return this._cdControlePArticipante;
    } //-- int getCdControlePArticipante() 

    /**
     * Returns the value of field 'cdFilialParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialParticipante'.
     */
    public int getCdFilialParticipante()
    {
        return this._cdFilialParticipante;
    } //-- int getCdFilialParticipante() 

    /**
     * Returns the value of field 'cdFormaEfetivacao'.
     * 
     * @return String
     * @return the value of field 'cdFormaEfetivacao'.
     */
    public java.lang.String getCdFormaEfetivacao()
    {
        return this._cdFormaEfetivacao;
    } //-- java.lang.String getCdFormaEfetivacao() 

    /**
     * Returns the value of field 'cdManterOperacao'.
     * 
     * @return String
     * @return the value of field 'cdManterOperacao'.
     */
    public java.lang.String getCdManterOperacao()
    {
        return this._cdManterOperacao;
    } //-- java.lang.String getCdManterOperacao() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdMotivoSituacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacao'.
     */
    public int getCdMotivoSituacao()
    {
        return this._cdMotivoSituacao;
    } //-- int getCdMotivoSituacao() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdSituacao'.
     * 
     * @return String
     * @return the value of field 'cdSituacao'.
     */
    public java.lang.String getCdSituacao()
    {
        return this._cdSituacao;
    } //-- java.lang.String getCdSituacao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoMudanca'.
     * 
     * @return String
     * @return the value of field 'cdTipoMudanca'.
     */
    public java.lang.String getCdTipoMudanca()
    {
        return this._cdTipoMudanca;
    } //-- java.lang.String getCdTipoMudanca() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModalidade'.
     */
    public java.lang.String getDsModalidade()
    {
        return this._dsModalidade;
    } //-- java.lang.String getDsModalidade() 

    /**
     * Returns the value of field 'dsMotivoSituacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacao'.
     */
    public java.lang.String getDsMotivoSituacao()
    {
        return this._dsMotivoSituacao;
    } //-- java.lang.String getDsMotivoSituacao() 

    /**
     * Returns the value of field 'dsNomeRazaoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazaoParticipante'.
     */
    public java.lang.String getDsNomeRazaoParticipante()
    {
        return this._dsNomeRazaoParticipante;
    } //-- java.lang.String getDsNomeRazaoParticipante() 

    /**
     * Returns the value of field 'dsServico'.
     * 
     * @return String
     * @return the value of field 'dsServico'.
     */
    public java.lang.String getDsServico()
    {
        return this._dsServico;
    } //-- java.lang.String getDsServico() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtProgramada'.
     * 
     * @return String
     * @return the value of field 'dtProgramada'.
     */
    public java.lang.String getDtProgramada()
    {
        return this._dtProgramada;
    } //-- java.lang.String getDtProgramada() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjParticipante()
    {
        return this._has_cdCnpjParticipante;
    } //-- boolean hasCdCnpjParticipante() 

    /**
     * Method hasCdControlePArticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControlePArticipante()
    {
        return this._has_cdControlePArticipante;
    } //-- boolean hasCdControlePArticipante() 

    /**
     * Method hasCdFilialParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialParticipante()
    {
        return this._has_cdFilialParticipante;
    } //-- boolean hasCdFilialParticipante() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdMotivoSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacao()
    {
        return this._has_cdMotivoSituacao;
    } //-- boolean hasCdMotivoSituacao() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjParticipante'.
     * 
     * @param cdCnpjParticipante the value of field
     * 'cdCnpjParticipante'.
     */
    public void setCdCnpjParticipante(long cdCnpjParticipante)
    {
        this._cdCnpjParticipante = cdCnpjParticipante;
        this._has_cdCnpjParticipante = true;
    } //-- void setCdCnpjParticipante(long) 

    /**
     * Sets the value of field 'cdControlePArticipante'.
     * 
     * @param cdControlePArticipante the value of field
     * 'cdControlePArticipante'.
     */
    public void setCdControlePArticipante(int cdControlePArticipante)
    {
        this._cdControlePArticipante = cdControlePArticipante;
        this._has_cdControlePArticipante = true;
    } //-- void setCdControlePArticipante(int) 

    /**
     * Sets the value of field 'cdFilialParticipante'.
     * 
     * @param cdFilialParticipante the value of field
     * 'cdFilialParticipante'.
     */
    public void setCdFilialParticipante(int cdFilialParticipante)
    {
        this._cdFilialParticipante = cdFilialParticipante;
        this._has_cdFilialParticipante = true;
    } //-- void setCdFilialParticipante(int) 

    /**
     * Sets the value of field 'cdFormaEfetivacao'.
     * 
     * @param cdFormaEfetivacao the value of field
     * 'cdFormaEfetivacao'.
     */
    public void setCdFormaEfetivacao(java.lang.String cdFormaEfetivacao)
    {
        this._cdFormaEfetivacao = cdFormaEfetivacao;
    } //-- void setCdFormaEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'cdManterOperacao'.
     * 
     * @param cdManterOperacao the value of field 'cdManterOperacao'
     */
    public void setCdManterOperacao(java.lang.String cdManterOperacao)
    {
        this._cdManterOperacao = cdManterOperacao;
    } //-- void setCdManterOperacao(java.lang.String) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdMotivoSituacao'.
     * 
     * @param cdMotivoSituacao the value of field 'cdMotivoSituacao'
     */
    public void setCdMotivoSituacao(int cdMotivoSituacao)
    {
        this._cdMotivoSituacao = cdMotivoSituacao;
        this._has_cdMotivoSituacao = true;
    } //-- void setCdMotivoSituacao(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdSituacao'.
     * 
     * @param cdSituacao the value of field 'cdSituacao'.
     */
    public void setCdSituacao(java.lang.String cdSituacao)
    {
        this._cdSituacao = cdSituacao;
    } //-- void setCdSituacao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoMudanca'.
     * 
     * @param cdTipoMudanca the value of field 'cdTipoMudanca'.
     */
    public void setCdTipoMudanca(java.lang.String cdTipoMudanca)
    {
        this._cdTipoMudanca = cdTipoMudanca;
    } //-- void setCdTipoMudanca(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(java.lang.String dsModalidade)
    {
        this._dsModalidade = dsModalidade;
    } //-- void setDsModalidade(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacao'.
     * 
     * @param dsMotivoSituacao the value of field 'dsMotivoSituacao'
     */
    public void setDsMotivoSituacao(java.lang.String dsMotivoSituacao)
    {
        this._dsMotivoSituacao = dsMotivoSituacao;
    } //-- void setDsMotivoSituacao(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeRazaoParticipante'.
     * 
     * @param dsNomeRazaoParticipante the value of field
     * 'dsNomeRazaoParticipante'.
     */
    public void setDsNomeRazaoParticipante(java.lang.String dsNomeRazaoParticipante)
    {
        this._dsNomeRazaoParticipante = dsNomeRazaoParticipante;
    } //-- void setDsNomeRazaoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsServico'.
     * 
     * @param dsServico the value of field 'dsServico'.
     */
    public void setDsServico(java.lang.String dsServico)
    {
        this._dsServico = dsServico;
    } //-- void setDsServico(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtProgramada'.
     * 
     * @param dtProgramada the value of field 'dtProgramada'.
     */
    public void setDtProgramada(java.lang.String dtProgramada)
    {
        this._dtProgramada = dtProgramada;
    } //-- void setDtProgramada(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarSolicitacaoAmbientePartcResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.response.ConsultarSolicitacaoAmbientePartcResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.response.ConsultarSolicitacaoAmbientePartcResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.response.ConsultarSolicitacaoAmbientePartcResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.response.ConsultarSolicitacaoAmbientePartcResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
