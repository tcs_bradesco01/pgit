/*
 * Nome: br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean;

/**
 * Nome: BloquearDesbloquearContaContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloquearDesbloquearContaContratoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdBanco. */
	private int cdBanco;
	
	/** Atributo cdTipoContratoConta. */
	private int cdTipoContratoConta;
	
	/** Atributo cdComercialAgenciaContabil. */
	private int cdComercialAgenciaContabil;
	
	/** Atributo cdConta. */
	private long cdConta;
	
	/** Atributo cdDigitoConta. */
	private int cdDigitoConta;
	
	/** Atributo cdSituacaoContrato. */
	private int cdSituacaoContrato;
	
	/** Atributo cdPessoaJuridicaVinculo. */
	private long cdPessoaJuridicaVinculo;
	
	/** Atributo cdTipoContratoVinculo. */
	private int cdTipoContratoVinculo;
	
	/** Atributo nrSequenciaContratoVinculo. */
	private long nrSequenciaContratoVinculo;
	
	/** Atributo cdTipoVinculoContrato. */
	private int cdTipoVinculoContrato;
	
	/** Atributo cdMotivoBloqueioConta. */
	private int cdMotivoBloqueioConta;
	
	/** Atributo cdTipoAcao. */
	private String cdTipoAcao;
	
	/** Atributo dtInicioVinculacaoContrato. */
	private String dtInicioVinculacaoContrato;
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public int getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(int cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdComercialAgenciaContabil.
	 *
	 * @return cdComercialAgenciaContabil
	 */
	public int getCdComercialAgenciaContabil() {
		return cdComercialAgenciaContabil;
	}
	
	/**
	 * Set: cdComercialAgenciaContabil.
	 *
	 * @param cdComercialAgenciaContabil the cd comercial agencia contabil
	 */
	public void setCdComercialAgenciaContabil(int cdComercialAgenciaContabil) {
		this.cdComercialAgenciaContabil = cdComercialAgenciaContabil;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public int getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(int cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdMotivoBloqueioConta.
	 *
	 * @return cdMotivoBloqueioConta
	 */
	public int getCdMotivoBloqueioConta() {
		return cdMotivoBloqueioConta;
	}
	
	/**
	 * Set: cdMotivoBloqueioConta.
	 *
	 * @param cdMotivoBloqueioConta the cd motivo bloqueio conta
	 */
	public void setCdMotivoBloqueioConta(int cdMotivoBloqueioConta) {
		this.cdMotivoBloqueioConta = cdMotivoBloqueioConta;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public int getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(int cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: cdTipoAcao.
	 *
	 * @return cdTipoAcao
	 */
	public String getCdTipoAcao() {
		return cdTipoAcao;
	}
	
	/**
	 * Set: cdTipoAcao.
	 *
	 * @param cdTipoAcao the cd tipo acao
	 */
	public void setCdTipoAcao(String cdTipoAcao) {
		this.cdTipoAcao = cdTipoAcao;
	}
	
	/**
	 * Get: cdTipoContratoConta.
	 *
	 * @return cdTipoContratoConta
	 */
	public int getCdTipoContratoConta() {
		return cdTipoContratoConta;
	}
	
	/**
	 * Set: cdTipoContratoConta.
	 *
	 * @param cdTipoContratoConta the cd tipo contrato conta
	 */
	public void setCdTipoContratoConta(int cdTipoContratoConta) {
		this.cdTipoContratoConta = cdTipoContratoConta;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTIpoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTIpoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTIpoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public int getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}
	
	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(int cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}
	
	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public int getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	
	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(int cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	
	/**
	 * Get: dtInicioVinculacaoContrato.
	 *
	 * @return dtInicioVinculacaoContrato
	 */
	public String getDtInicioVinculacaoContrato() {
		return dtInicioVinculacaoContrato;
	}
	
	/**
	 * Set: dtInicioVinculacaoContrato.
	 *
	 * @param dtInicioVinculacaoContrato the dt inicio vinculacao contrato
	 */
	public void setDtInicioVinculacaoContrato(String dtInicioVinculacaoContrato) {
		this.dtInicioVinculacaoContrato = dtInicioVinculacaoContrato;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}
	
	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}
}
