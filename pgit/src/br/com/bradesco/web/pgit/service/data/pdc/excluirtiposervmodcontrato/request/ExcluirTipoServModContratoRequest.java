/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExcluirTipoServModContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExcluirTipoServModContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdTipoServico
     */
    private int _cdTipoServico = 0;

    /**
     * keeps track of state for field: _cdTipoServico
     */
    private boolean _has_cdTipoServico;

    /**
     * Field _cdModalidade1
     */
    private int _cdModalidade1 = 0;

    /**
     * keeps track of state for field: _cdModalidade1
     */
    private boolean _has_cdModalidade1;

    /**
     * Field _cdModalidade2
     */
    private int _cdModalidade2 = 0;

    /**
     * keeps track of state for field: _cdModalidade2
     */
    private boolean _has_cdModalidade2;

    /**
     * Field _cdModalidade3
     */
    private int _cdModalidade3 = 0;

    /**
     * keeps track of state for field: _cdModalidade3
     */
    private boolean _has_cdModalidade3;

    /**
     * Field _cdModalidade4
     */
    private int _cdModalidade4 = 0;

    /**
     * keeps track of state for field: _cdModalidade4
     */
    private boolean _has_cdModalidade4;

    /**
     * Field _cdModalidade5
     */
    private int _cdModalidade5 = 0;

    /**
     * keeps track of state for field: _cdModalidade5
     */
    private boolean _has_cdModalidade5;

    /**
     * Field _cdModalidade6
     */
    private int _cdModalidade6 = 0;

    /**
     * keeps track of state for field: _cdModalidade6
     */
    private boolean _has_cdModalidade6;

    /**
     * Field _cdModalidade7
     */
    private int _cdModalidade7 = 0;

    /**
     * keeps track of state for field: _cdModalidade7
     */
    private boolean _has_cdModalidade7;

    /**
     * Field _cdModalidade8
     */
    private int _cdModalidade8 = 0;

    /**
     * keeps track of state for field: _cdModalidade8
     */
    private boolean _has_cdModalidade8;

    /**
     * Field _cdModalidade9
     */
    private int _cdModalidade9 = 0;

    /**
     * keeps track of state for field: _cdModalidade9
     */
    private boolean _has_cdModalidade9;

    /**
     * Field _cdModalidade10
     */
    private int _cdModalidade10 = 0;

    /**
     * keeps track of state for field: _cdModalidade10
     */
    private boolean _has_cdModalidade10;

    /**
     * Field _cdExerServicoModalidade
     */
    private int _cdExerServicoModalidade = 0;

    /**
     * keeps track of state for field: _cdExerServicoModalidade
     */
    private boolean _has_cdExerServicoModalidade;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExcluirTipoServModContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.request.ExcluirTipoServModContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdExerServicoModalidade
     * 
     */
    public void deleteCdExerServicoModalidade()
    {
        this._has_cdExerServicoModalidade= false;
    } //-- void deleteCdExerServicoModalidade() 

    /**
     * Method deleteCdModalidade1
     * 
     */
    public void deleteCdModalidade1()
    {
        this._has_cdModalidade1= false;
    } //-- void deleteCdModalidade1() 

    /**
     * Method deleteCdModalidade10
     * 
     */
    public void deleteCdModalidade10()
    {
        this._has_cdModalidade10= false;
    } //-- void deleteCdModalidade10() 

    /**
     * Method deleteCdModalidade2
     * 
     */
    public void deleteCdModalidade2()
    {
        this._has_cdModalidade2= false;
    } //-- void deleteCdModalidade2() 

    /**
     * Method deleteCdModalidade3
     * 
     */
    public void deleteCdModalidade3()
    {
        this._has_cdModalidade3= false;
    } //-- void deleteCdModalidade3() 

    /**
     * Method deleteCdModalidade4
     * 
     */
    public void deleteCdModalidade4()
    {
        this._has_cdModalidade4= false;
    } //-- void deleteCdModalidade4() 

    /**
     * Method deleteCdModalidade5
     * 
     */
    public void deleteCdModalidade5()
    {
        this._has_cdModalidade5= false;
    } //-- void deleteCdModalidade5() 

    /**
     * Method deleteCdModalidade6
     * 
     */
    public void deleteCdModalidade6()
    {
        this._has_cdModalidade6= false;
    } //-- void deleteCdModalidade6() 

    /**
     * Method deleteCdModalidade7
     * 
     */
    public void deleteCdModalidade7()
    {
        this._has_cdModalidade7= false;
    } //-- void deleteCdModalidade7() 

    /**
     * Method deleteCdModalidade8
     * 
     */
    public void deleteCdModalidade8()
    {
        this._has_cdModalidade8= false;
    } //-- void deleteCdModalidade8() 

    /**
     * Method deleteCdModalidade9
     * 
     */
    public void deleteCdModalidade9()
    {
        this._has_cdModalidade9= false;
    } //-- void deleteCdModalidade9() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoServico
     * 
     */
    public void deleteCdTipoServico()
    {
        this._has_cdTipoServico= false;
    } //-- void deleteCdTipoServico() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Returns the value of field 'cdExerServicoModalidade'.
     * 
     * @return int
     * @return the value of field 'cdExerServicoModalidade'.
     */
    public int getCdExerServicoModalidade()
    {
        return this._cdExerServicoModalidade;
    } //-- int getCdExerServicoModalidade() 

    /**
     * Returns the value of field 'cdModalidade1'.
     * 
     * @return int
     * @return the value of field 'cdModalidade1'.
     */
    public int getCdModalidade1()
    {
        return this._cdModalidade1;
    } //-- int getCdModalidade1() 

    /**
     * Returns the value of field 'cdModalidade10'.
     * 
     * @return int
     * @return the value of field 'cdModalidade10'.
     */
    public int getCdModalidade10()
    {
        return this._cdModalidade10;
    } //-- int getCdModalidade10() 

    /**
     * Returns the value of field 'cdModalidade2'.
     * 
     * @return int
     * @return the value of field 'cdModalidade2'.
     */
    public int getCdModalidade2()
    {
        return this._cdModalidade2;
    } //-- int getCdModalidade2() 

    /**
     * Returns the value of field 'cdModalidade3'.
     * 
     * @return int
     * @return the value of field 'cdModalidade3'.
     */
    public int getCdModalidade3()
    {
        return this._cdModalidade3;
    } //-- int getCdModalidade3() 

    /**
     * Returns the value of field 'cdModalidade4'.
     * 
     * @return int
     * @return the value of field 'cdModalidade4'.
     */
    public int getCdModalidade4()
    {
        return this._cdModalidade4;
    } //-- int getCdModalidade4() 

    /**
     * Returns the value of field 'cdModalidade5'.
     * 
     * @return int
     * @return the value of field 'cdModalidade5'.
     */
    public int getCdModalidade5()
    {
        return this._cdModalidade5;
    } //-- int getCdModalidade5() 

    /**
     * Returns the value of field 'cdModalidade6'.
     * 
     * @return int
     * @return the value of field 'cdModalidade6'.
     */
    public int getCdModalidade6()
    {
        return this._cdModalidade6;
    } //-- int getCdModalidade6() 

    /**
     * Returns the value of field 'cdModalidade7'.
     * 
     * @return int
     * @return the value of field 'cdModalidade7'.
     */
    public int getCdModalidade7()
    {
        return this._cdModalidade7;
    } //-- int getCdModalidade7() 

    /**
     * Returns the value of field 'cdModalidade8'.
     * 
     * @return int
     * @return the value of field 'cdModalidade8'.
     */
    public int getCdModalidade8()
    {
        return this._cdModalidade8;
    } //-- int getCdModalidade8() 

    /**
     * Returns the value of field 'cdModalidade9'.
     * 
     * @return int
     * @return the value of field 'cdModalidade9'.
     */
    public int getCdModalidade9()
    {
        return this._cdModalidade9;
    } //-- int getCdModalidade9() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoServico'.
     * 
     * @return int
     * @return the value of field 'cdTipoServico'.
     */
    public int getCdTipoServico()
    {
        return this._cdTipoServico;
    } //-- int getCdTipoServico() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Method hasCdExerServicoModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdExerServicoModalidade()
    {
        return this._has_cdExerServicoModalidade;
    } //-- boolean hasCdExerServicoModalidade() 

    /**
     * Method hasCdModalidade1
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade1()
    {
        return this._has_cdModalidade1;
    } //-- boolean hasCdModalidade1() 

    /**
     * Method hasCdModalidade10
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade10()
    {
        return this._has_cdModalidade10;
    } //-- boolean hasCdModalidade10() 

    /**
     * Method hasCdModalidade2
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade2()
    {
        return this._has_cdModalidade2;
    } //-- boolean hasCdModalidade2() 

    /**
     * Method hasCdModalidade3
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade3()
    {
        return this._has_cdModalidade3;
    } //-- boolean hasCdModalidade3() 

    /**
     * Method hasCdModalidade4
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade4()
    {
        return this._has_cdModalidade4;
    } //-- boolean hasCdModalidade4() 

    /**
     * Method hasCdModalidade5
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade5()
    {
        return this._has_cdModalidade5;
    } //-- boolean hasCdModalidade5() 

    /**
     * Method hasCdModalidade6
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade6()
    {
        return this._has_cdModalidade6;
    } //-- boolean hasCdModalidade6() 

    /**
     * Method hasCdModalidade7
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade7()
    {
        return this._has_cdModalidade7;
    } //-- boolean hasCdModalidade7() 

    /**
     * Method hasCdModalidade8
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade8()
    {
        return this._has_cdModalidade8;
    } //-- boolean hasCdModalidade8() 

    /**
     * Method hasCdModalidade9
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade9()
    {
        return this._has_cdModalidade9;
    } //-- boolean hasCdModalidade9() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServico()
    {
        return this._has_cdTipoServico;
    } //-- boolean hasCdTipoServico() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdExerServicoModalidade'.
     * 
     * @param cdExerServicoModalidade the value of field
     * 'cdExerServicoModalidade'.
     */
    public void setCdExerServicoModalidade(int cdExerServicoModalidade)
    {
        this._cdExerServicoModalidade = cdExerServicoModalidade;
        this._has_cdExerServicoModalidade = true;
    } //-- void setCdExerServicoModalidade(int) 

    /**
     * Sets the value of field 'cdModalidade1'.
     * 
     * @param cdModalidade1 the value of field 'cdModalidade1'.
     */
    public void setCdModalidade1(int cdModalidade1)
    {
        this._cdModalidade1 = cdModalidade1;
        this._has_cdModalidade1 = true;
    } //-- void setCdModalidade1(int) 

    /**
     * Sets the value of field 'cdModalidade10'.
     * 
     * @param cdModalidade10 the value of field 'cdModalidade10'.
     */
    public void setCdModalidade10(int cdModalidade10)
    {
        this._cdModalidade10 = cdModalidade10;
        this._has_cdModalidade10 = true;
    } //-- void setCdModalidade10(int) 

    /**
     * Sets the value of field 'cdModalidade2'.
     * 
     * @param cdModalidade2 the value of field 'cdModalidade2'.
     */
    public void setCdModalidade2(int cdModalidade2)
    {
        this._cdModalidade2 = cdModalidade2;
        this._has_cdModalidade2 = true;
    } //-- void setCdModalidade2(int) 

    /**
     * Sets the value of field 'cdModalidade3'.
     * 
     * @param cdModalidade3 the value of field 'cdModalidade3'.
     */
    public void setCdModalidade3(int cdModalidade3)
    {
        this._cdModalidade3 = cdModalidade3;
        this._has_cdModalidade3 = true;
    } //-- void setCdModalidade3(int) 

    /**
     * Sets the value of field 'cdModalidade4'.
     * 
     * @param cdModalidade4 the value of field 'cdModalidade4'.
     */
    public void setCdModalidade4(int cdModalidade4)
    {
        this._cdModalidade4 = cdModalidade4;
        this._has_cdModalidade4 = true;
    } //-- void setCdModalidade4(int) 

    /**
     * Sets the value of field 'cdModalidade5'.
     * 
     * @param cdModalidade5 the value of field 'cdModalidade5'.
     */
    public void setCdModalidade5(int cdModalidade5)
    {
        this._cdModalidade5 = cdModalidade5;
        this._has_cdModalidade5 = true;
    } //-- void setCdModalidade5(int) 

    /**
     * Sets the value of field 'cdModalidade6'.
     * 
     * @param cdModalidade6 the value of field 'cdModalidade6'.
     */
    public void setCdModalidade6(int cdModalidade6)
    {
        this._cdModalidade6 = cdModalidade6;
        this._has_cdModalidade6 = true;
    } //-- void setCdModalidade6(int) 

    /**
     * Sets the value of field 'cdModalidade7'.
     * 
     * @param cdModalidade7 the value of field 'cdModalidade7'.
     */
    public void setCdModalidade7(int cdModalidade7)
    {
        this._cdModalidade7 = cdModalidade7;
        this._has_cdModalidade7 = true;
    } //-- void setCdModalidade7(int) 

    /**
     * Sets the value of field 'cdModalidade8'.
     * 
     * @param cdModalidade8 the value of field 'cdModalidade8'.
     */
    public void setCdModalidade8(int cdModalidade8)
    {
        this._cdModalidade8 = cdModalidade8;
        this._has_cdModalidade8 = true;
    } //-- void setCdModalidade8(int) 

    /**
     * Sets the value of field 'cdModalidade9'.
     * 
     * @param cdModalidade9 the value of field 'cdModalidade9'.
     */
    public void setCdModalidade9(int cdModalidade9)
    {
        this._cdModalidade9 = cdModalidade9;
        this._has_cdModalidade9 = true;
    } //-- void setCdModalidade9(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoServico'.
     * 
     * @param cdTipoServico the value of field 'cdTipoServico'.
     */
    public void setCdTipoServico(int cdTipoServico)
    {
        this._cdTipoServico = cdTipoServico;
        this._has_cdTipoServico = true;
    } //-- void setCdTipoServico(int) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExcluirTipoServModContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.request.ExcluirTipoServModContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.request.ExcluirTipoServModContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.request.ExcluirTipoServModContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.request.ExcluirTipoServModContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
