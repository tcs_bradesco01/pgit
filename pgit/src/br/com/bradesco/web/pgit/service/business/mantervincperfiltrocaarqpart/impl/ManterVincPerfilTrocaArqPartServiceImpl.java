/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaBigDecimalNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.AlterarPerfilContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.AlterarPerfilContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharHistAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharHistAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ExcluirVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ExcluirVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirPagamentoSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirPagamentoSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincContMaeFilhoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincContMaeFilhoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarContratoMaeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarContratoMaeOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarContratoMaeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarHistAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarHistAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.request.AlterarPerfilContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.response.AlterarPerfilContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.request.DetalharHistAssocTrocaArqParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.response.DetalharHistAssocTrocaArqParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvinctrocaarqparticipante.request.DetalharVincTrocaArqParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvinctrocaarqparticipante.response.DetalharVincTrocaArqParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinctrocaarqparticipante.request.ExcluirVincTrocaArqParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinctrocaarqparticipante.response.ExcluirVincTrocaArqParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalario.request.IncluirPagamentoSalarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalario.response.IncluirPagamentoSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinccontratomaecontratofilho.request.IncluirVincContratoMaeContratoFilhoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinccontratomaecontratofilho.response.IncluirVincContratoMaeContratoFilhoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.request.IncluirVincTrocaArqParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.response.IncluirVincTrocaArqParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.request.ListarAssocTrocaArqParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.response.ListarAssocTrocaArqParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.request.ListarContratoMaeRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.response.ListarContratoMaeResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistassoctrocaarqparticipante.request.ListarHistAssocTrocaArqParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistassoctrocaarqparticipante.response.ListarHistAssocTrocaArqParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipante.request.ListarParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipante.response.ListarParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.request.ListarParticipantesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.response.ListarParticipantesResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterVincPerfilTrocaArqPart
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterVincPerfilTrocaArqPartServiceImpl implements IManterVincPerfilTrocaArqPartService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#listarParticipantes(br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesEntradaDTO)
	 */
	public List<ListarParticipantesSaidaDTO> listarParticipantes(ListarParticipantesEntradaDTO listarParticipantesEntradaDTO) {

		List<ListarParticipantesSaidaDTO> listaRetorno = new ArrayList<ListarParticipantesSaidaDTO>();
		ListarParticipantesRequest request = new ListarParticipantesRequest();
		ListarParticipantesResponse response = new ListarParticipantesResponse();

		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(listarParticipantesEntradaDTO.getCdPessoaJuridica()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(listarParticipantesEntradaDTO.getCdTipoContrato()));
		request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(listarParticipantesEntradaDTO.getNrSequenciaContrato()));
		request.setNrOcorrencias(25);

		response = getFactoryAdapter().getListarParticipantesPDCAdapter().invokeProcess(request);

		ListarParticipantesSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ListarParticipantesSaidaDTO();
			saidaDTO.setCdAtividadeEconomica(response.getOcorrencias(i).getCdAtividadeEconomica());
			saidaDTO.setDsAtividadeEconomica(response.getOcorrencias(i).getDsAtividadeEconomica());
			saidaDTO.setCpf(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj(), response.getOcorrencias(i).getCdFilialCnpj(), response.getOcorrencias(i)
					.getCdControleCnpj()));
			saidaDTO.setCdGrupoEconomico(response.getOcorrencias(i).getCdGrupoEconomico());
			saidaDTO.setCdParticipante(response.getOcorrencias(i).getCdPessoa());
			saidaDTO.setDsGrupoEconomico(response.getOcorrencias(i).getDsGrupoEconomico());
			saidaDTO.setNomeRazao(response.getOcorrencias(i).getNmRazao());
			saidaDTO.setCdParticipante(response.getOcorrencias(i).getCdPessoa());
			saidaDTO.setTipoParticipacaoFormatador(response.getOcorrencias(i).getCdTipoParticipacao() + " - " + response.getOcorrencias(i).getDsTipoParticipacao());
			saidaDTO.setDsSituacaoParticipacao(response.getOcorrencias(i).getDsSituacaoParticipacao());
			saidaDTO.setTipoParticipacao(response.getOcorrencias(i).getCdTipoParticipacao());

			saidaDTO.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			saidaDTO.setCdFilialCnpj((response.getOcorrencias(i).getCdFilialCnpj()));
			saidaDTO.setCdControleCnpj(response.getOcorrencias(i).getCdControleCnpj());

			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;
	}
	
	

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#listarAssocTrocaArqParticipante(br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarAssocTrocaArqParticipanteEntradaDTO)
	 */
	public List<ListarAssocTrocaArqParticipanteSaidaDTO> listarAssocTrocaArqParticipante(ListarAssocTrocaArqParticipanteEntradaDTO entrada) {
		ListarAssocTrocaArqParticipanteRequest request = new ListarAssocTrocaArqParticipanteRequest();
		request.setMaxOcorrencias(IManterVincPerfilTrocaArqPartServiceConstants.MAX_OCORRENCIAS_40);
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));

		ListarAssocTrocaArqParticipanteResponse response = getFactoryAdapter().getListarAssocTrocaArqParticipantePDCAdapter().invokeProcess(request);

		List<ListarAssocTrocaArqParticipanteSaidaDTO> listaSaida = new ArrayList<ListarAssocTrocaArqParticipanteSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.response.Ocorrencias ocorrencia : response.getOcorrencias()) {
			ListarAssocTrocaArqParticipanteSaidaDTO saida = new ListarAssocTrocaArqParticipanteSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdClub(ocorrencia.getCdClub());
			saida.setCdCpfCnpj(ocorrencia.getCdCpfCnpj());
			saida.setCdCnpjContrato(ocorrencia.getCdCnpjContrato());
			saida.setCdCpfCnpjDigito(ocorrencia.getCdCpfCnpjDigito());
			saida.setCdRazaoSocial(ocorrencia.getCdRazaoSocial());
			saida.setCdTipoLayoutArquivo(ocorrencia.getCdTipoLayoutArquivo());
			saida.setCdPerfilTrocaArq(ocorrencia.getCdPerfilTrocaArq());
			saida.setCdTipoParticipacaoPessoa(ocorrencia.getCdTipoParticipacaoPessoa());
			saida.setDsTipoParticipante(ocorrencia.getDsTipoParticipante());
			saida.setDsTipoLayoutArquivo(ocorrencia.getDsLayout());
			saida.setDsLayoutProprio(ocorrencia.getDsLayoutProprio());
            saida.setCdProdutoServicoOperacao(ocorrencia.getCdProdutoServicoOperacao());
            saida.setCdAplicacaoTransmPagamento(ocorrencia.getCdAplicacaoTransmPagamento());
            saida.setDsProdutoServico(ocorrencia.getDsProdutoServico());
            saida.setDsAplicacaoTransmicaoPagamento(ocorrencia.getDsAplicacaoTransmicaoPagamento());
            saida.setCdMeioPrincipalRemessa(ocorrencia.getCdMeioPrincipalRemessa());
            saida.setDsMeioPrincRemss(ocorrencia.getDsMeioPrincRemss());
            saida.setCdMeioAlternRemessa(ocorrencia.getCdMeioAlternRemessa());
            saida.setDsMeioAltrnRemss(ocorrencia.getDsMeioAltrnRemss());
            saida.setCdMeioPrincipalRetorno(ocorrencia.getCdMeioPrincipalRetorno());
            saida.setDsMeioPrincRetor(ocorrencia.getDsMeioPrincRetor());
            saida.setCdMeioAltrnRetorno(ocorrencia.getCdMeioAltrnRetorno());
            saida.setDsMeioAltrnRetor(ocorrencia.getDsMeioAltrnRetor());
            saida.setCdPessoaJuridicaParceiro(ocorrencia.getCdPessoaJuridicaParceiro());
            saida.setDsRzScialPcero(ocorrencia.getDsRzScialPcero());

			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#listarParticipante(br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteEntradaDTO)
	 */
	public List<ListarParticipanteSaidaDTO> listarParticipante(ListarParticipanteEntradaDTO entrada) {
		ListarParticipanteRequest request = new ListarParticipanteRequest();
		request.setMaxOcorrencias(50);
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));

		ListarParticipanteResponse response = getFactoryAdapter().getListarParticipantePDCAdapter().invokeProcess(request);

		List<ListarParticipanteSaidaDTO> listaSaida = new ArrayList<ListarParticipanteSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarparticipante.response.Ocorrencias ocorrencia : response.getOcorrencias()) {
			ListarParticipanteSaidaDTO saida = new ListarParticipanteSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdClub(ocorrencia.getCdClub());
	 		saida.setCdCorpoCpfCnpj(ocorrencia.getCdCorpoCpfCnpj());
			saida.setCdCnpjContrato(ocorrencia.getCdCnpjContrato());
			saida.setCdCpfCnpjDigito(ocorrencia.getCdCpfCnpjDigito());
			saida.setCdRazaoSocial(ocorrencia.getCdRazaoSocial());
			saida.setCdTipoLayoutArquivo(ocorrencia.getCdTipoLayoutArquivo());
			saida.setCdPerfilTrocaArquivo(ocorrencia.getCdPerfilTrocaArquivo());
			saida.setCdTipoParticipacaoPessoa(ocorrencia.getCdTipoParticipacaoPessoa());
			saida.setDsTipoParticipante(ocorrencia.getDsTipoParticipante());

			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#incluirVincTrocaArqParticipante(br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincTrocaArqParticipanteEntradaDTO)
	 */
	public IncluirVincTrocaArqParticipanteSaidaDTO incluirVincTrocaArqParticipante(IncluirVincTrocaArqParticipanteEntradaDTO entrada) {
		IncluirVincTrocaArqParticipanteSaidaDTO saida = new IncluirVincTrocaArqParticipanteSaidaDTO();
		IncluirVincTrocaArqParticipanteRequest request = new IncluirVincTrocaArqParticipanteRequest();
		IncluirVincTrocaArqParticipanteResponse response = new IncluirVincTrocaArqParticipanteResponse();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));

		// campos adicionados 05/09
		request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entrada.getCdCorpoCpfCnpj()));
		request.setNrFilialCnpj(PgitUtil.verificaIntegerNulo(entrada.getNrFilialCnpj()));
		request.setCdDigitoCpfCnpj(PgitUtil.verificaStringNula(entrada.getCdDigitoCpfCnpj()));
		
		request.setCdTipoLayoutMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutMae()));
		request.setCdPerfilTrocaMae(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaMae()));
		request.setCdPessoaJuridicaMae(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaMae()));
		request.setCdTipoContratoMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoMae()));
		request.setNrSequencialContratoMae(PgitUtil.verificaLongNulo(entrada.getNrSequencialContratoMae()));
		request.setCdTipoParticipanteMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipanteMae()));
		request.setCdCpessoaMae(PgitUtil.verificaLongNulo(entrada.getCdCpessoaMae()));
		request.setCdCpfCnpjMae(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjMae()));
		request.setCdCnpjFilialMae(PgitUtil.verificaIntegerNulo(entrada.getCdCnpjFilialMae()));
		request.setCdDigitoCpfCnpjMae(PgitUtil.verificaStringNula(entrada.getCdDigitoCpfCnpjMae()));
		
		response = getFactoryAdapter().getIncluirVincTrocaArqParticipantePDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#excluirVincTrocaArqParticipante(br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ExcluirVincTrocaArqParticipanteEntradaDTO)
	 */
	public ExcluirVincTrocaArqParticipanteSaidaDTO excluirVincTrocaArqParticipante(ExcluirVincTrocaArqParticipanteEntradaDTO entrada) {
		ExcluirVincTrocaArqParticipanteSaidaDTO saida = new ExcluirVincTrocaArqParticipanteSaidaDTO();
		ExcluirVincTrocaArqParticipanteRequest request = new ExcluirVincTrocaArqParticipanteRequest();
		ExcluirVincTrocaArqParticipanteResponse response = new ExcluirVincTrocaArqParticipanteResponse();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdTipoLayoutMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutMae()));
		request.setCdPerfilTrocaMae(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaMae()));
		request.setCdPessoaJuridicaMae(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaMae()));
		request.setCdTipoContratoMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoMae()));
		request.setNrSequencialContratoMae(PgitUtil.verificaLongNulo(entrada.getNrSequencialContratoMae()));
		request.setCdTipoParticipanteMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipanteMae()));
		request.setCdCpessoaMae(PgitUtil.verificaLongNulo(entrada.getCdCpessoaMae()));

		response = getFactoryAdapter().getExcluirVincTrocaArqParticipantePDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#detalharVincTrocaArqParticipante(br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharVincTrocaArqParticipanteEntradaDTO)
	 */
	public DetalharVincTrocaArqParticipanteSaidaDTO detalharVincTrocaArqParticipante(DetalharVincTrocaArqParticipanteEntradaDTO entrada) {
		DetalharVincTrocaArqParticipanteSaidaDTO saida = new DetalharVincTrocaArqParticipanteSaidaDTO();
		DetalharVincTrocaArqParticipanteRequest request = new DetalharVincTrocaArqParticipanteRequest();
		DetalharVincTrocaArqParticipanteResponse response = new DetalharVincTrocaArqParticipanteResponse();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));

		response = getFactoryAdapter().getDetalharVincTrocaArqParticipantePDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saida.setDsCodigoTipoLayout(response.getDsCodigoTipoLayout());
		saida.setCdPerfilTrocaArquivo(response.getCdPerfilTrocaArquivo());
		saida.setCdCorpoCpfCnpj(response.getCdCorpoCpfCnpj());
		saida.setCdCnpjContrato(String.valueOf(response.getCdCnpjContrato()));
		saida.setCdCpfCnpjDigito(response.getCdCpfCnpjDigito());
		saida.setDsRazaoSocial(response.getDsRazaoSocial());
		saida.setCdTipoParticipante(response.getCdTipoParticipante());
		saida.setDsTipoParticipante(response.getDsTipoParticipante());
		saida.setCdSituacaoParticipante(response.getCdSituacaoParticipante());
		saida.setCdDescricaoSituacaoParticapante(response.getCdDescricaoSituacaoParticapante());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
		saida.setCdTipoParticipacaoPessoa(response.getCdTipoParticipacaoPessoa());
		saida.setCdClub(response.getCdClub());
		saida.setCdCanal(response.getCdCanal());
		saida.setDsCanal(response.getDsCanalInclusao());
		saida.setCdOperacaoCanalIncusao(response.getCdOperacaoCanalIncusao());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setDsTipoCanalInclusao(response.getDsCanalInclusao());
		saida.setCdUsuarioInclusao(PgitUtil.verificaStringNula(response.getCdUsuarioInclusao()));
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setDsTipoCanalManutencao(response.getDsCanalManutencao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
		saida.setCpfFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getCdCorpoCpfCnpj(), response.getCdCnpjContrato(), response.getCdCpfCnpjDigito()));
		saida.setCanalInclusaoFormatado(PgitUtil.concatenarCampos(response.getCdTipoCanalInclusao(), response.getDsCanalInclusao()));
		saida.setCanalManutencaoFormatado(PgitUtil.concatenarCampos(response.getCdTipoCanalManutencao(), response.getDsCanalManutencao()));
		saida.setCdTipoLayoutMae(response.getCdTipoLayoutMae());
		saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saida.setCdPerfilTrocaMae(response.getCdPerfilTrocaMae());
		saida.setCdPessoaJuridicaMae(response.getCdPessoaJuridicaMae());
		saida.setCdTipoContratoMae(response.getCdTipoContratoMae());
		saida.setNrSequencialContratoMae(response.getNrSequencialContratoMae());
		saida.setCdTipoParticipanteMae(response.getCdTipoParticipanteMae());
		saida.setCdCpessoaMae(response.getCdCpessoaMae());
		saida.setCdCpfCnpj(response.getCdCpfCnpj());
		saida.setCdFilialCnpj(response.getCdFilialCnpj());
		saida.setCdControleCnpj(response.getCdControleCnpj());
		saida.setDsNomeRazao(response.getDsNomeRazao());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#listarHistAssocTrocaArqParticipante(br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarHistAssocTrocaArqParticipanteEntradaDTO)
	 */
	public List<ListarHistAssocTrocaArqParticipanteSaidaDTO> listarHistAssocTrocaArqParticipante(ListarHistAssocTrocaArqParticipanteEntradaDTO entrada) {
		List<ListarHistAssocTrocaArqParticipanteSaidaDTO> listaSaida = new ArrayList<ListarHistAssocTrocaArqParticipanteSaidaDTO>();
		ListarHistAssocTrocaArqParticipanteRequest request = new ListarHistAssocTrocaArqParticipanteRequest();
		ListarHistAssocTrocaArqParticipanteResponse response = new ListarHistAssocTrocaArqParticipanteResponse();

		request.setMaxOcorrencias(IManterVincPerfilTrocaArqPartServiceConstants.NUMERO_OCORRENCIAS_HISTORICO);
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));
		request.setDtManutencaoInicio(PgitUtil.verificaStringNula(entrada.getDtManutencaoInicio()));
		request.setDtManutencaoFim(PgitUtil.verificaStringNula(entrada.getDtManutencaoFim()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));

		response = getFactoryAdapter().getListarHistAssocTrocaArqParticipantePDCAdapter().invokeProcess(request);

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ListarHistAssocTrocaArqParticipanteSaidaDTO saida = new ListarHistAssocTrocaArqParticipanteSaidaDTO();

			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());

			saida.setCdClub(response.getOcorrencias(i).getCdClub());
			saida.setCdCorpoCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
			saida.setCdCnpjContrato(response.getOcorrencias(i).getCdCnpjContrato());
			saida.setCdCpfCnpjDigito(response.getOcorrencias(i).getCdCpfCnpjDigito());
			saida.setCdRazaoSocial(response.getOcorrencias(i).getCdRazaoSocial());
			saida.setCdTipoParticipacaoPessoa(response.getOcorrencias(i).getCdTipoParticipacaoPessoa());
			saida.setDsTipoParticipante(response.getOcorrencias(i).getDsTipoParticipante());
			saida.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
			saida.setCdPerfilTrocaArquivo(response.getOcorrencias(i).getCdPerfilTrocaArquivo());
			saida.setHrManutencaoRegistro(response.getOcorrencias(i).getHrManutencaoRegistro());
			saida.setHrInclusaoRegistro(response.getOcorrencias(i).getHrInclusaoRegistro());
			saida.setDsLayout(response.getOcorrencias(i).getDsLayout());

			saida.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(saida.getCdCorpoCpfCnpj(), saida.getCdCnpjContrato(), saida.getCdCpfCnpjDigito()));
			saida.setDataHoraFormatada(FormatarData.formatarDataTrilha(saida.getHrManutencaoRegistro()));

			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#detalharHistAssocTrocaArqParticipante(br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharHistAssocTrocaArqParticipanteEntradaDTO)
	 */
	public DetalharHistAssocTrocaArqParticipanteSaidaDTO detalharHistAssocTrocaArqParticipante(DetalharHistAssocTrocaArqParticipanteEntradaDTO entrada) {
		DetalharHistAssocTrocaArqParticipanteSaidaDTO saida = new DetalharHistAssocTrocaArqParticipanteSaidaDTO();
		DetalharHistAssocTrocaArqParticipanteRequest request = new DetalharHistAssocTrocaArqParticipanteRequest();
		DetalharHistAssocTrocaArqParticipanteResponse response = new DetalharHistAssocTrocaArqParticipanteResponse();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setHrInclusaoRegistroHist(PgitUtil.verificaStringNula(entrada.getHrInclusaoRegistroHist()));

		response = getFactoryAdapter().getDetalharHistAssocTrocaArqParticipantePDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saida.setDsCodigoTipoLayout(response.getDsCodigoTipoLayout());
		saida.setCdPerfilTrocaArquivo(response.getCdPerfilTrocaArquivo());
		saida.setCdCorpoCpfCnpj(response.getCdCorpoCpfCnpj());
		saida.setCdCnpjContrato(response.getCdCnpjContrato());
		saida.setCdCpfCnpjDigito(response.getCdCpfCnpjDigito());
		saida.setDsRazaoSocial(response.getDsRazaoSocial());
		saida.setCdTipoParticipante(response.getCdTipoParticipante());
		saida.setDsTipoParticipante(response.getDsTipoParticipante());
		saida.setCdSituacaoParticipante(response.getCdSituacaoParticipante());
		saida.setCdDescricaoSituacaoParticapante(response.getCdDescricaoSituacaoParticapante());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
		saida.setCdTipoParticipacaoPessoa(response.getCdTipoParticipacaoPessoa());
		saida.setDsClub(response.getDsClub());
		saida.setCdCanal(response.getCdCanal());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsIndicadorTipoManutencao(response.getDsIndicadorTipoManutencao());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
		saida.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setCdContratoMae(response.getNrSequencialContratoMae());
		saida.setCdCpfCnpj(response.getCdCpfCnpj());
		saida.setCdFilialCnpj(response.getCdFilialCnpj());
		saida.setCdControleCnpj(response.getCdControleCnpj());
		saida.setDsRazaoSocialContratoMae(response.getDsRazaoSocial());
		saida.setCdPerfilContratoMae(response.getCdPerfilTrocaMae());
		saida.setDsTipoLayoutArquivoContratoMae(response.getDsTipoLayoutArquivo());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService#alterarPerfilContrato(br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.AlterarPerfilContratoEntradaDTO)
	 */
	public AlterarPerfilContratoSaidaDTO alterarPerfilContrato(AlterarPerfilContratoEntradaDTO entrada) {

		AlterarPerfilContratoRequest request = new AlterarPerfilContratoRequest();
		AlterarPerfilContratoResponse response = null;
		AlterarPerfilContratoSaidaDTO saida = new AlterarPerfilContratoSaidaDTO();

		request.setCdPerfilTrocaArquivo(verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdMeioPrincipalRemessa(verificaIntegerNulo(entrada.getCdMeioPrincipalRemessa()));
		request.setCdmeioAlternativoRemessa(verificaIntegerNulo(entrada.getCodigoMeioAlternativoRemessa()));
		request.setCdMeioPrincipalRetorno(verificaIntegerNulo(entrada.getCdMeioPrincipalRetorno()));
		request.setCdMeioAlternativoRetorno(verificaIntegerNulo(entrada.getCdMeioAlternativoRetorno()));
		request.setCdPessoaJuridicaParceiro(verificaLongNulo(entrada.getCdPessoaJuridicaParceiro()));
		request.setCdEmpresaResponsavel(verificaIntegerNulo(entrada.getCdEmpresaResponsavel()));
		request.setCdCustoTransmicao(verificaBigDecimalNulo(entrada.getCdCustoTransmicao()));
		request.setCdApliFormat(PgitUtil.verificaLongNulo(entrada.getCdApliFormat()));
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
		request.setCdUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entrada.getCdUnidadeOrganizacional()));
		request.setDsNomeContatoCliente(verificaStringNula(entrada.getDsNomeContatoCliente()));
		request.setCdAreaFone(PgitUtil.verificaIntegerNulo(entrada.getCdAreaFone()));
		request.setCdFoneContatoCliente(PgitUtil.verificaLongNulo(entrada.getCdFoneContatoCliente()));
		request.setCdRamalContatoCliente(verificaStringNula(entrada.getCdRamalContatoCliente()));
		request.setDsEmailContatoCliente(PgitUtil.verificaStringNula(entrada.getDsEmailContatoCliente()));
		request.setDsSolicitacaoPendente(PgitUtil.verificaStringNula(entrada.getDsSolicitacaoPendente()));
		request.setCdAreaFonePend(PgitUtil.verificaIntegerNulo(entrada.getCdAreaFonePend()));
		request.setCdFoneSolicitacaoPend(PgitUtil.verificaLongNulo(entrada.getCdFoneSolicitacaoPend()));
		request.setCdRamalSolctPend(PgitUtil.verificaStringNula(entrada.getCdRamalSolctPend()));
		request.setDsEmailSolctPend(PgitUtil.verificaStringNula(entrada.getDsEmailSolctPend()));
		request.setQtMesRegistroTrafg(PgitUtil.verificaLongNulo(entrada.getQtMesRegistroTrafg()));
		request.setDsObsGeralPerfil(PgitUtil.verificaStringNula(entrada.getDsObsGeralPerfil()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
	    request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
	    request.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(entrada.getCdRelacionamentoProduto()));

		response = getFactoryAdapter().getAlterarPerfilContratoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	public IncluirPagamentoSalarioSaidaDTO incluirPagamentoSalario(IncluirPagamentoSalarioEntradaDTO entrada) {
		
		IncluirPagamentoSalarioRequest request = new IncluirPagamentoSalarioRequest();
		
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato())); 
	    request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	    request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	    request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
	    
	    request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));  
	    request.setCdClubRepresentante(verificaLongNulo(entrada.getCdClubRepresentante()));           
	    request.setCdCpfCnpjRepresentante(verificaLongNulo(entrada.getCdCpfCnpjRepresentante()));
	    
	    request.setCdFilialCnpjRepresentante(verificaIntegerNulo(entrada.getCdFilialCnpjRepresentante()));     
	    request.setCdControleCpfRepresentante(verificaIntegerNulo(entrada.getCdControleCpfRepresentante())); 
	    
	    request.setNmRazaoRepresentante(verificaStringNula(entrada.getNmRazaoRepresentante()));          
	    request.setCdPessoaJuridicaConta(verificaLongNulo(entrada.getCdPessoaJuridicaConta()));
	    
	    request.setCdTipoContratoConta(verificaIntegerNulo(entrada.getCdTipoContratoConta()));           
	    request.setNrSequenciaContratoConta(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));      
	    request.setCdInstituicaoBancaria(verificaIntegerNulo(entrada.getCdInstituicaoBancaria()));         
	    request.setCdUnidadeOrganizacional(verificaIntegerNulo(entrada.getCdUnidadeOrganizacional()));       
	    request.setNrConta(verificaIntegerNulo(entrada.getNrConta()));                       
	    request.setNrContaDigito(verificaStringNula(entrada.getNrContaDigito()));                 
	    request.setCdIndicadorNumConve(verificaIntegerNulo(entrada.getCdIndicadorNumConve()));           
	    request.setCdConveCtaSalarial(verificaLongNulo(entrada.getCdConveCtaSalarial()));            
	    request.setVlPagamentoMes(verificaBigDecimalNulo(entrada.getVlPagamentoMes()));                
	    request.setQtdFuncionarioFlPgto(verificaIntegerNulo(entrada.getQtdFuncionarioFlPgto()));          
	    request.setVlMediaSalarialMes(verificaBigDecimalNulo(entrada.getVlMediaSalarialMes()));            
	    request.setCdPagamentoSalarialMes(verificaIntegerNulo(entrada.getCdPagamentoSalarialMes()));        
	    request.setCdPagamentoSalarialQzena(verificaIntegerNulo(entrada.getCdPagamentoSalarialQzena()));
	    request.setCdLocalContaSalarial(verificaIntegerNulo(entrada.getCdLocalContaSalarial()));
	    request.setDtReftFlPgto(verificaIntegerNulo(entrada.getDtReftFlPgto()));
	    request.setCdIndicadorAlcadaAgencia(verificaIntegerNulo(entrada.getCdIndicadorAlcadaAgencia()));
	    request.setCdFidelize(verificaIntegerNulo(entrada.getCdFidelize()));
	    request.setDsEmailEmpresa(verificaStringNula(entrada.getDsEmailEmpresa()));
	    request.setDsEmailAgencia(verificaStringNula(entrada.getDsEmailAgencia()));
	    
	    request.setCdAltoRotvo(verificaIntegerNulo(entrada.getCdAltoTurnover()));
	    request.setCdOfertCatao(verificaIntegerNulo(entrada.getCdCartaoPre()));
	    request.setCdAbertAplic(verificaIntegerNulo(entrada.getCdPeloApp()));
	    
	    IncluirPagamentoSalarioResponse response = getFactoryAdapter().getIncluirPagamentoSalarioPDCAdapter().invokeProcess(request);
	    
	    IncluirPagamentoSalarioSaidaDTO saida = new IncluirPagamentoSalarioSaidaDTO();
	    
	    saida.setCodMensagem(response.getCodMensagem());
	    saida.setMensagem(response.getMensagem());

		return saida;
	}

	public ListarContratoMaeSaidaDTO listarContratoMae(ListarContratoMaeEntradaDTO entrada) {
		ListarContratoMaeSaidaDTO saida = new ListarContratoMaeSaidaDTO();
		ListarContratoMaeRequest request = new ListarContratoMaeRequest();
		
		request.setCdPessoaJuridicaMae(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaMae()));
		request.setMaxOcorrencias(50);
		
		ListarContratoMaeResponse response = getFactoryAdapter().getListarContratoMaePDCAdapter().invokeProcess(request);
		
		saida.setCdpessoaJuridicaContrato(response.getCdpessoaJuridicaContrato());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNrLinhas(response.getNrLinhas());
		saida.setOcorrencias(new ArrayList<ListarContratoMaeOcorrenciasSaidaDTO>());
		
		ListarContratoMaeOcorrenciasSaidaDTO ocorrencias = new ListarContratoMaeOcorrenciasSaidaDTO();
		for (int i = 0; i < response.getOcorrencias().length; i++) {
			ocorrencias = new ListarContratoMaeOcorrenciasSaidaDTO();
			
			ocorrencias.setCdControleCnpj(response.getOcorrencias(i).getCdControleCnpj());
			ocorrencias.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
			ocorrencias.setCdFilialCnpj(response.getOcorrencias(i).getCdFilialCnpj());
			ocorrencias.setCdPerfilTrocaArquivo(response.getOcorrencias(i).getCdPerfilTrocaArquivo());
			ocorrencias.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
			ocorrencias.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
			ocorrencias.setCdTipoParticipacaoPessoa(response.getOcorrencias(i).getCdTipoParticipacaoPessoa());
			ocorrencias.setDsNomeRazao(response.getOcorrencias(i).getDsNomeRazao());
			ocorrencias.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
			
			saida.getOcorrencias().add(ocorrencias);
		}
		
		return saida;
	}

	public IncluirVincContMaeFilhoSaidaDTO incluirVincContratoMaeContratoFilho(
			IncluirVincContMaeFilhoEntradaDTO entrada) {
		
		IncluirVincContratoMaeContratoFilhoRequest request = new IncluirVincContratoMaeContratoFilhoRequest();
		
		request.setCdCpessoaMae(PgitUtil.verificaLongNulo(entrada.getCdCpessoaMae()));
		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
		request.setCdPerfilTrocaMae(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaMae()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdPessoaJuridicaMae(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaMae()));
		request.setCdTipoContratoMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoMae()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdTipoLayoutMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutMae()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipacaoPessoa()));
		request.setCdTipoParticipanteMae(PgitUtil.verificaIntegerNulo(entrada.getCdTipoParticipanteMae()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setNrSequencialContratoMae(PgitUtil.verificaLongNulo(entrada.getNrSequencialContratoMae()));
		
		IncluirVincContratoMaeContratoFilhoResponse response = getFactoryAdapter().getIncluirVincContratoMaeContratoFilhoPDCAdapter().invokeProcess(request);
		
		IncluirVincContMaeFilhoSaidaDTO saida = new IncluirVincContMaeFilhoSaidaDTO();
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());		
		
		return saida;
	}
}
