/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgalertagestor.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgalertagestor.bean;

/**
 * Nome: IncluirMsgAlertaGestorEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirMsgAlertaGestorEntradaDTO {
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo tipoProcesso. */
	private int tipoProcesso;
	
	/** Atributo nrSeqMensagemAlerta. */
	private int nrSeqMensagemAlerta;
	
	/** Atributo cdFuncionario. */
	private long cdFuncionario;
	
	/** Atributo prefixo. */
	private String prefixo;
	
	/** Atributo tipoMensagem. */
	private int tipoMensagem;
	
	/** Atributo idioma. */
	private int idioma;
	
	/** Atributo recurso. */
	private int recurso;
	
	/** Atributo meioTransmissao. */
	private int meioTransmissao;
	
	/**
	 * Get: cdFuncionario.
	 *
	 * @return cdFuncionario
	 */
	public long getCdFuncionario() {
		return cdFuncionario;
	}
	
	/**
	 * Set: cdFuncionario.
	 *
	 * @param cdFuncionario the cd funcionario
	 */
	public void setCdFuncionario(long cdFuncionario) {
		this.cdFuncionario = cdFuncionario;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public int getIdioma() {
		return idioma;
	}
	
	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(int idioma) {
		this.idioma = idioma;
	}
	
	/**
	 * Get: meioTransmissao.
	 *
	 * @return meioTransmissao
	 */
	public int getMeioTransmissao() {
		return meioTransmissao;
	}
	
	/**
	 * Set: meioTransmissao.
	 *
	 * @param meioTransmissao the meio transmissao
	 */
	public void setMeioTransmissao(int meioTransmissao) {
		this.meioTransmissao = meioTransmissao;
	}
	
	/**
	 * Get: nrSeqMensagemAlerta.
	 *
	 * @return nrSeqMensagemAlerta
	 */
	public int getNrSeqMensagemAlerta() {
		return nrSeqMensagemAlerta;
	}
	
	/**
	 * Set: nrSeqMensagemAlerta.
	 *
	 * @param nrSeqMensagemAlerta the nr seq mensagem alerta
	 */
	public void setNrSeqMensagemAlerta(int nrSeqMensagemAlerta) {
		this.nrSeqMensagemAlerta = nrSeqMensagemAlerta;
	}
	
	/**
	 * Get: prefixo.
	 *
	 * @return prefixo
	 */
	public String getPrefixo() {
		return prefixo;
	}
	
	/**
	 * Set: prefixo.
	 *
	 * @param prefixo the prefixo
	 */
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	
	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public int getTipoMensagem() {
		return tipoMensagem;
	}
	
	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(int tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	/**
	 * Get: tipoProcesso.
	 *
	 * @return tipoProcesso
	 */
	public int getTipoProcesso() {
		return tipoProcesso;
	}
	
	/**
	 * Set: tipoProcesso.
	 *
	 * @param tipoProcesso the tipo processo
	 */
	public void setTipoProcesso(int tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public int getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(int recurso) {
		this.recurso = recurso;
	}

}
