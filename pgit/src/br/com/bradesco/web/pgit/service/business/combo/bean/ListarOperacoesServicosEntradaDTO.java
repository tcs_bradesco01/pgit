/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarOperacoesServicosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarOperacoesServicosEntradaDTO {
	
	/** Atributo cdProduto. */
	private int cdProduto;

	/**
	 * Listar operacoes servicos entrada dto.
	 */
	public ListarOperacoesServicosEntradaDTO() {
		super();
	}

	/**
	 * Listar operacoes servicos entrada dto.
	 *
	 * @param cdProduto the cd produto
	 */
	public ListarOperacoesServicosEntradaDTO(int cdProduto) {
		super();
		this.cdProduto = cdProduto;
	}

	/**
	 * Get: cdProduto.
	 *
	 * @return cdProduto
	 */
	public int getCdProduto() {
		return cdProduto;
	}

	/**
	 * Set: cdProduto.
	 *
	 * @param cdProduto the cd produto
	 */
	public void setCdProduto(int cdProduto) {
		this.cdProduto = cdProduto;
	}
	

}
