/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;


/**
 * Nome: IdiomaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IdiomaSaidaDTO {
	
	/** Atributo dsIdioma. */
	private String dsIdioma;
	
	/** Atributo cdIdioma. */
	private Integer cdIdioma;
	
	/**
	 * Get: cdIdioma.
	 *
	 * @return cdIdioma
	 */
	public Integer getCdIdioma() {
		return cdIdioma;
	}
	
	/**
	 * Set: cdIdioma.
	 *
	 * @param cdIdioma the cd idioma
	 */
	public void setCdIdioma(Integer cdIdioma) {
		this.cdIdioma = cdIdioma;
	}
	
	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}
	
	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}
	
    /**
     * Idioma saida dto.
     *
     * @param cdIdioma the cd idioma
     * @param dsIdioma the ds idioma
     */
    public IdiomaSaidaDTO(Integer cdIdioma, String dsIdioma) {
		super();
		this.dsIdioma = dsIdioma;
		this.cdIdioma = cdIdioma;
	}
	
    /**
     * Idioma saida dto.
     */
    public IdiomaSaidaDTO() {
		super();
	}    
	
	
}
