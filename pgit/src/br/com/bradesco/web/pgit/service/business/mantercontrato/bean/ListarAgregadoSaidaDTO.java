/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ListarAgregadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAgregadoSaidaDTO {

	/** Atributo cdCorpoCpfCnpj. */
	private Long cdCorpoCpfCnpj;
	
	/** Atributo cdControleCpfCnpj. */
	private Integer cdControleCpfCnpj;
	
	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;
	
	/** Atributo dsNomeAgregado. */
	private String dsNomeAgregado;

	/**
	 * Listar agregado saida dto.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 * @param cdFilialCnpj the cd filial cnpj
	 * @param dsNomeAgregado the ds nome agregado
	 */
	public ListarAgregadoSaidaDTO(Long cdCorpoCpfCnpj,
			Integer cdControleCpfCnpj, Integer cdFilialCnpj,
			String dsNomeAgregado) {
		super();
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
		this.cdControleCpfCnpj = cdControleCpfCnpj;
		this.cdFilialCnpj = cdFilialCnpj;
		this.dsNomeAgregado = dsNomeAgregado;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils.formatarCpfCnpj(cdCorpoCpfCnpj, cdFilialCnpj, cdControleCpfCnpj);
	}

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	
	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	
	/**
	 * Get: cdControleCpfCnpj.
	 *
	 * @return cdControleCpfCnpj
	 */
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}
	
	/**
	 * Set: cdControleCpfCnpj.
	 *
	 * @param cdControleCpfCnpj the cd controle cpf cnpj
	 */
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}
	
	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}
	
	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}
	
	/**
	 * Get: dsNomeAgregado.
	 *
	 * @return dsNomeAgregado
	 */
	public String getDsNomeAgregado() {
		return dsNomeAgregado;
	}
	
	/**
	 * Set: dsNomeAgregado.
	 *
	 * @param dsNomeAgregado the ds nome agregado
	 */
	public void setDsNomeAgregado(String dsNomeAgregado) {
		this.dsNomeAgregado = dsNomeAgregado;
	}
}