/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMotivoSituacaoServContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoSituacaoServContratoSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdMotivoSituacaoProduto. */
	private int cdMotivoSituacaoProduto;
	
	/** Atributo dsMotivoSituacaoProduto. */
	private String dsMotivoSituacaoProduto;
	
	/**
	 * Get: cdMotivoSituacaoProduto.
	 *
	 * @return cdMotivoSituacaoProduto
	 */
	public int getCdMotivoSituacaoProduto() {
		return cdMotivoSituacaoProduto;
	}
	
	/**
	 * Set: cdMotivoSituacaoProduto.
	 *
	 * @param cdMotivoSituacaoProduto the cd motivo situacao produto
	 */
	public void setCdMotivoSituacaoProduto(int cdMotivoSituacaoProduto) {
		this.cdMotivoSituacaoProduto = cdMotivoSituacaoProduto;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsMotivoSituacaoProduto.
	 *
	 * @return dsMotivoSituacaoProduto
	 */
	public String getDsMotivoSituacaoProduto() {
		return dsMotivoSituacaoProduto;
	}
	
	/**
	 * Set: dsMotivoSituacaoProduto.
	 *
	 * @param dsMotivoSituacaoProduto the ds motivo situacao produto
	 */
	public void setDsMotivoSituacaoProduto(String dsMotivoSituacaoProduto) {
		this.dsMotivoSituacaoProduto = dsMotivoSituacaoProduto;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Listar motivo situacao serv contrato saida dto.
	 *
	 * @param cdMotivoSituacaoProduto the cd motivo situacao produto
	 * @param dsMotivoSituacaoProduto the ds motivo situacao produto
	 */
	public ListarMotivoSituacaoServContratoSaidaDTO(int cdMotivoSituacaoProduto, String dsMotivoSituacaoProduto) {
		super();
		this.cdMotivoSituacaoProduto = cdMotivoSituacaoProduto;
		this.dsMotivoSituacaoProduto = dsMotivoSituacaoProduto;
	}
	
	
}
