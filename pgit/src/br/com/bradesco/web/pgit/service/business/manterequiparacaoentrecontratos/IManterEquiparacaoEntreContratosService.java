/**
 * Nome: br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos;

import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.IncluirEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.IncluirEquiparacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarServicoEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarServicoEquiparacaoContratoSaidaDTO;

/**
 * Nome: IManterEquiparacaoEntreContratosService
 * <p>
 * Prop�sito: Interface do adaptador ManterEquiparacaoEntreContratos
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IManterEquiparacaoEntreContratosService {
   
    /**
     * Listar servico equiparacao contrato.
     *
     * @param entrada the entrada
     * @return the listar servico equiparacao contrato saida dto
     */
    public ListarServicoEquiparacaoContratoSaidaDTO listarServicoEquiparacaoContrato(ListarServicoEquiparacaoContratoEntradaDTO entrada);
    
    /**
     * Listar modalidade equiparacao contrato.
     *
     * @param entrada the entrada
     * @return the listar modalidade equiparacao contrato saida dto
     */
    public ListarModalidadeEquiparacaoContratoSaidaDTO listarModalidadeEquiparacaoContrato(ListarModalidadeEquiparacaoContratoEntradaDTO entrada);
    
    /**
     * Incluir equiparacao contrato.
     *
     * @param entrada the entrada
     * @return the incluir equiparacao contrato saida dto
     */
    public IncluirEquiparacaoContratoSaidaDTO incluirEquiparacaoContrato(IncluirEquiparacaoContratoEntradaDTO entrada);
}