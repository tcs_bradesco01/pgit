package br.com.bradesco.web.pgit.service.business.consultaautorizante.bean;

import java.math.BigDecimal;
import java.util.List;

/**
 * Nome: ConsultarAutorizantesNetEmpresaSaidaDTO
 * <p>
 * Propůsito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ConsultarAutorizantesNetEmpresaSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdPessoaJuridCont. */
    private Long cdPessoaJuridCont;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;
    
    /** Atributo dtEfetivacaoPagamento. */
    private String dtEfetivacaoPagamento;
    
    /** Atributo cdBancoDescricao. */
    private Integer cdBancoDescricao;
    
    /** Atributo cdAgenciaBancariaDescricao. */
    private Integer cdAgenciaBancariaDescricao;
    
    /** Atributo cdContaBancariaDescricao. */
    private Long cdContaBancariaDescricao;
    
    /** Atributo cdAgendadosPagoNaoPago. */
    private Integer cdAgendadosPagoNaoPago;
    
    /** Atributo nrArquivoRemssaPagamento. */
    private Long nrArquivoRemssaPagamento;
    
    /** Atributo cdRejeicaoLoteComprovante. */
    private Long cdRejeicaoLoteComprovante;
    
    /** Atributo cdListaDebitoPagamento. */
    private Long cdListaDebitoPagamento;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo dsTipoCanal. */
    private String dsTipoCanal;
    
    /** Atributo vlPagamento. */
    private BigDecimal vlPagamento;
    
    /** Atributo dsModalidade. */
    private Integer dsModalidade;
    
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;
    
    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;
    
    /** Atributo dsTipoContaDebito. */
    private String dsTipoContaDebito;
    
    /** Atributo cdSituacaoContrato. */
    private Integer cdSituacaoContrato;
    
    /** Atributo dsSituacaoContrato. */
    private String dsSituacaoContrato;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo dtVencimento. */
    private String dtVencimento;
    
    /** Atributo cdSituacaoPagamentoCliente. */
    private Integer cdSituacaoPagamentoCliente;
    
    /** Atributo dsSituacaoPagamentoCliente. */
    private String dsSituacaoPagamentoCliente;
    
    /** Atributo cdMotivoPagamentoCliente. */
    private Integer cdMotivoPagamentoCliente;
    
    /** Atributo dsMotivoPagamentoCliente. */
    private String dsMotivoPagamentoCliente;
    
    /** Atributo cdBancoCredito. */
    private Integer cdBancoCredito;
    
    /** Atributo dsBancoCredito. */
    private String dsBancoCredito;
    
    /** Atributo cdAgenciaBancariaCredito. */
    private Integer cdAgenciaBancariaCredito;
    
    /** Atributo cdDigitoAgenciaCredito. */
    private Integer cdDigitoAgenciaCredito;
    
    /** Atributo dsAgenciaCredito. */
    private String dsAgenciaCredito;
    
    /** Atributo cdContaBancariaCredito. */
    private Long cdContaBancariaCredito;
    
    /** Atributo cdDigitoContaCredito. */
    private String cdDigitoContaCredito;
    
    /** Atributo cdTipoContaCredito. */
    private String cdTipoContaCredito;
    
    /** Atributo cdTipoIsncricaoFavorecido. */
    private String cdTipoIsncricaoFavorecido;
    
    /** Atributo cdCtciaInscricaoFavorecido. */
    private Long cdCtciaInscricaoFavorecido;
    
    /** Atributo dtCreditoPagamento. */
    private String dtCreditoPagamento;
    
    /** Atributo numeroConsulta. */
    private Integer numeroConsulta;
    
    /** Atributo ocorrencias. */
    private List<ConsultarAutorizantesOcorrenciaSaidaDTO> ocorrencias;

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get: cdPessoaJuridCont.
     *
     * @return cdPessoaJuridCont
     */
    public Long getCdPessoaJuridCont() {
        return cdPessoaJuridCont;
    }

    /**
     * Set: cdPessoaJuridCont.
     *
     * @param cdPessoaJuridCont the cd pessoa jurid cont
     */
    public void setCdPessoaJuridCont(Long cdPessoaJuridCont) {
        this.cdPessoaJuridCont = cdPessoaJuridCont;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return nrSequenciaContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: cdControlePagamento.
     *
     * @return cdControlePagamento
     */
    public String getCdControlePagamento() {
        return cdControlePagamento;
    }

    /**
     * Set: cdControlePagamento.
     *
     * @param cdControlePagamento the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
        this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get: dtEfetivacaoPagamento.
     *
     * @return dtEfetivacaoPagamento
     */
    public String getDtEfetivacaoPagamento() {
        return dtEfetivacaoPagamento;
    }

    /**
     * Set: dtEfetivacaoPagamento.
     *
     * @param dtEfetivacaoPagamento the dt efetivacao pagamento
     */
    public void setDtEfetivacaoPagamento(String dtEfetivacaoPagamento) {
        this.dtEfetivacaoPagamento = dtEfetivacaoPagamento;
    }

    /**
     * Get: cdBancoDescricao.
     *
     * @return cdBancoDescricao
     */
    public Integer getCdBancoDescricao() {
        return cdBancoDescricao;
    }

    /**
     * Set: cdBancoDescricao.
     *
     * @param cdBancoDescricao the cd banco descricao
     */
    public void setCdBancoDescricao(Integer cdBancoDescricao) {
        this.cdBancoDescricao = cdBancoDescricao;
    }

    /**
     * Get: cdAgenciaBancariaDescricao.
     *
     * @return cdAgenciaBancariaDescricao
     */
    public Integer getCdAgenciaBancariaDescricao() {
        return cdAgenciaBancariaDescricao;
    }

    /**
     * Set: cdAgenciaBancariaDescricao.
     *
     * @param cdAgenciaBancariaDescricao the cd agencia bancaria descricao
     */
    public void setCdAgenciaBancariaDescricao(Integer cdAgenciaBancariaDescricao) {
        this.cdAgenciaBancariaDescricao = cdAgenciaBancariaDescricao;
    }

    /**
     * Get: cdContaBancariaDescricao.
     *
     * @return cdContaBancariaDescricao
     */
    public Long getCdContaBancariaDescricao() {
        return cdContaBancariaDescricao;
    }

    /**
     * Set: cdContaBancariaDescricao.
     *
     * @param cdContaBancariaDescricao the cd conta bancaria descricao
     */
    public void setCdContaBancariaDescricao(Long cdContaBancariaDescricao) {
        this.cdContaBancariaDescricao = cdContaBancariaDescricao;
    }

    /**
     * Get: cdAgendadosPagoNaoPago.
     *
     * @return cdAgendadosPagoNaoPago
     */
    public Integer getCdAgendadosPagoNaoPago() {
        return cdAgendadosPagoNaoPago;
    }

    /**
     * Set: cdAgendadosPagoNaoPago.
     *
     * @param cdAgendadosPagoNaoPago the cd agendados pago nao pago
     */
    public void setCdAgendadosPagoNaoPago(Integer cdAgendadosPagoNaoPago) {
        this.cdAgendadosPagoNaoPago = cdAgendadosPagoNaoPago;
    }

    /**
     * Get: nrArquivoRemssaPagamento.
     *
     * @return nrArquivoRemssaPagamento
     */
    public Long getNrArquivoRemssaPagamento() {
        return nrArquivoRemssaPagamento;
    }

    /**
     * Set: nrArquivoRemssaPagamento.
     *
     * @param nrArquivoRemssaPagamento the nr arquivo remssa pagamento
     */
    public void setNrArquivoRemssaPagamento(Long nrArquivoRemssaPagamento) {
        this.nrArquivoRemssaPagamento = nrArquivoRemssaPagamento;
    }

    /**
     * Get: cdRejeicaoLoteComprovante.
     *
     * @return cdRejeicaoLoteComprovante
     */
    public Long getCdRejeicaoLoteComprovante() {
        return cdRejeicaoLoteComprovante;
    }

    /**
     * Set: cdRejeicaoLoteComprovante.
     *
     * @param cdRejeicaoLoteComprovante the cd rejeicao lote comprovante
     */
    public void setCdRejeicaoLoteComprovante(Long cdRejeicaoLoteComprovante) {
        this.cdRejeicaoLoteComprovante = cdRejeicaoLoteComprovante;
    }

    /**
     * Get: cdListaDebitoPagamento.
     *
     * @return cdListaDebitoPagamento
     */
    public Long getCdListaDebitoPagamento() {
        return cdListaDebitoPagamento;
    }

    /**
     * Set: cdListaDebitoPagamento.
     *
     * @param cdListaDebitoPagamento the cd lista debito pagamento
     */
    public void setCdListaDebitoPagamento(Long cdListaDebitoPagamento) {
        this.cdListaDebitoPagamento = cdListaDebitoPagamento;
    }

    /**
     * Get: cdTipoCanal.
     *
     * @return cdTipoCanal
     */
    public Integer getCdTipoCanal() {
        return cdTipoCanal;
    }

    /**
     * Set: cdTipoCanal.
     *
     * @param cdTipoCanal the cd tipo canal
     */
    public void setCdTipoCanal(Integer cdTipoCanal) {
        this.cdTipoCanal = cdTipoCanal;
    }

    /**
     * Get: dsTipoCanal.
     *
     * @return dsTipoCanal
     */
    public String getDsTipoCanal() {
        return dsTipoCanal;
    }

    /**
     * Set: dsTipoCanal.
     *
     * @param dsTipoCanal the ds tipo canal
     */
    public void setDsTipoCanal(String dsTipoCanal) {
        this.dsTipoCanal = dsTipoCanal;
    }

    /**
     * Get: vlPagamento.
     *
     * @return vlPagamento
     */
    public BigDecimal getVlPagamento() {
        return vlPagamento;
    }

    /**
     * Set: vlPagamento.
     *
     * @param vlPagamento the vl pagamento
     */
    public void setVlPagamento(BigDecimal vlPagamento) {
        this.vlPagamento = vlPagamento;
    }

    /**
     * Get: dsModalidade.
     *
     * @return dsModalidade
     */
    public Integer getDsModalidade() {
        return dsModalidade;
    }

    /**
     * Set: dsModalidade.
     *
     * @param dsModalidade the ds modalidade
     */
    public void setDsModalidade(Integer dsModalidade) {
        this.dsModalidade = dsModalidade;
    }

    /**
     * Get: dsBancoDebito.
     *
     * @return dsBancoDebito
     */
    public String getDsBancoDebito() {
        return dsBancoDebito;
    }

    /**
     * Set: dsBancoDebito.
     *
     * @param dsBancoDebito the ds banco debito
     */
    public void setDsBancoDebito(String dsBancoDebito) {
        this.dsBancoDebito = dsBancoDebito;
    }

    /**
     * Get: dsAgenciaDebito.
     *
     * @return dsAgenciaDebito
     */
    public String getDsAgenciaDebito() {
        return dsAgenciaDebito;
    }

    /**
     * Set: dsAgenciaDebito.
     *
     * @param dsAgenciaDebito the ds agencia debito
     */
    public void setDsAgenciaDebito(String dsAgenciaDebito) {
        this.dsAgenciaDebito = dsAgenciaDebito;
    }

    /**
     * Get: dsTipoContaDebito.
     *
     * @return dsTipoContaDebito
     */
    public String getDsTipoContaDebito() {
        return dsTipoContaDebito;
    }

    /**
     * Set: dsTipoContaDebito.
     *
     * @param dsTipoContaDebito the ds tipo conta debito
     */
    public void setDsTipoContaDebito(String dsTipoContaDebito) {
        this.dsTipoContaDebito = dsTipoContaDebito;
    }

    /**
     * Get: cdSituacaoContrato.
     *
     * @return cdSituacaoContrato
     */
    public Integer getCdSituacaoContrato() {
        return cdSituacaoContrato;
    }

    /**
     * Set: cdSituacaoContrato.
     *
     * @param cdSituacaoContrato the cd situacao contrato
     */
    public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
        this.cdSituacaoContrato = cdSituacaoContrato;
    }

    /**
     * Get: dsSituacaoContrato.
     *
     * @return dsSituacaoContrato
     */
    public String getDsSituacaoContrato() {
        return dsSituacaoContrato;
    }

    /**
     * Set: dsSituacaoContrato.
     *
     * @param dsSituacaoContrato the ds situacao contrato
     */
    public void setDsSituacaoContrato(String dsSituacaoContrato) {
        this.dsSituacaoContrato = dsSituacaoContrato;
    }

    /**
     * Get: dsContrato.
     *
     * @return dsContrato
     */
    public String getDsContrato() {
        return dsContrato;
    }

    /**
     * Set: dsContrato.
     *
     * @param dsContrato the ds contrato
     */
    public void setDsContrato(String dsContrato) {
        this.dsContrato = dsContrato;
    }

    /**
     * Get: dtVencimento.
     *
     * @return dtVencimento
     */
    public String getDtVencimento() {
        return dtVencimento;
    }

    /**
     * Set: dtVencimento.
     *
     * @param dtVencimento the dt vencimento
     */
    public void setDtVencimento(String dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    /**
     * Get: cdSituacaoPagamentoCliente.
     *
     * @return cdSituacaoPagamentoCliente
     */
    public Integer getCdSituacaoPagamentoCliente() {
        return cdSituacaoPagamentoCliente;
    }

    /**
     * Set: cdSituacaoPagamentoCliente.
     *
     * @param cdSituacaoPagamentoCliente the cd situacao pagamento cliente
     */
    public void setCdSituacaoPagamentoCliente(Integer cdSituacaoPagamentoCliente) {
        this.cdSituacaoPagamentoCliente = cdSituacaoPagamentoCliente;
    }

    /**
     * Get: dsSituacaoPagamentoCliente.
     *
     * @return dsSituacaoPagamentoCliente
     */
    public String getDsSituacaoPagamentoCliente() {
        return dsSituacaoPagamentoCliente;
    }

    /**
     * Set: dsSituacaoPagamentoCliente.
     *
     * @param dsSituacaoPagamentoCliente the ds situacao pagamento cliente
     */
    public void setDsSituacaoPagamentoCliente(String dsSituacaoPagamentoCliente) {
        this.dsSituacaoPagamentoCliente = dsSituacaoPagamentoCliente;
    }

    /**
     * Get: cdMotivoPagamentoCliente.
     *
     * @return cdMotivoPagamentoCliente
     */
    public Integer getCdMotivoPagamentoCliente() {
        return cdMotivoPagamentoCliente;
    }

    /**
     * Set: cdMotivoPagamentoCliente.
     *
     * @param cdMotivoPagamentoCliente the cd motivo pagamento cliente
     */
    public void setCdMotivoPagamentoCliente(Integer cdMotivoPagamentoCliente) {
        this.cdMotivoPagamentoCliente = cdMotivoPagamentoCliente;
    }

    /**
     * Get: dsMotivoPagamentoCliente.
     *
     * @return dsMotivoPagamentoCliente
     */
    public String getDsMotivoPagamentoCliente() {
        return dsMotivoPagamentoCliente;
    }

    /**
     * Set: dsMotivoPagamentoCliente.
     *
     * @param dsMotivoPagamentoCliente the ds motivo pagamento cliente
     */
    public void setDsMotivoPagamentoCliente(String dsMotivoPagamentoCliente) {
        this.dsMotivoPagamentoCliente = dsMotivoPagamentoCliente;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public Integer getCdBancoCredito() {
        return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(Integer cdBancoCredito) {
        this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: dsBancoCredito.
     *
     * @return dsBancoCredito
     */
    public String getDsBancoCredito() {
        return dsBancoCredito;
    }

    /**
     * Set: dsBancoCredito.
     *
     * @param dsBancoCredito the ds banco credito
     */
    public void setDsBancoCredito(String dsBancoCredito) {
        this.dsBancoCredito = dsBancoCredito;
    }

    /**
     * Get: cdAgenciaBancariaCredito.
     *
     * @return cdAgenciaBancariaCredito
     */
    public Integer getCdAgenciaBancariaCredito() {
        return cdAgenciaBancariaCredito;
    }

    /**
     * Set: cdAgenciaBancariaCredito.
     *
     * @param cdAgenciaBancariaCredito the cd agencia bancaria credito
     */
    public void setCdAgenciaBancariaCredito(Integer cdAgenciaBancariaCredito) {
        this.cdAgenciaBancariaCredito = cdAgenciaBancariaCredito;
    }

    /**
     * Get: cdDigitoAgenciaCredito.
     *
     * @return cdDigitoAgenciaCredito
     */
    public Integer getCdDigitoAgenciaCredito() {
        return cdDigitoAgenciaCredito;
    }

    /**
     * Set: cdDigitoAgenciaCredito.
     *
     * @param cdDigitoAgenciaCredito the cd digito agencia credito
     */
    public void setCdDigitoAgenciaCredito(Integer cdDigitoAgenciaCredito) {
        this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
    }

    /**
     * Get: dsAgenciaCredito.
     *
     * @return dsAgenciaCredito
     */
    public String getDsAgenciaCredito() {
        return dsAgenciaCredito;
    }

    /**
     * Set: dsAgenciaCredito.
     *
     * @param dsAgenciaCredito the ds agencia credito
     */
    public void setDsAgenciaCredito(String dsAgenciaCredito) {
        this.dsAgenciaCredito = dsAgenciaCredito;
    }

    /**
     * Get: cdContaBancariaCredito.
     *
     * @return cdContaBancariaCredito
     */
    public Long getCdContaBancariaCredito() {
        return cdContaBancariaCredito;
    }

    /**
     * Set: cdContaBancariaCredito.
     *
     * @param cdContaBancariaCredito the cd conta bancaria credito
     */
    public void setCdContaBancariaCredito(Long cdContaBancariaCredito) {
        this.cdContaBancariaCredito = cdContaBancariaCredito;
    }

    /**
     * Get: cdDigitoContaCredito.
     *
     * @return cdDigitoContaCredito
     */
    public String getCdDigitoContaCredito() {
        return cdDigitoContaCredito;
    }

    /**
     * Set: cdDigitoContaCredito.
     *
     * @param cdDigitoContaCredito the cd digito conta credito
     */
    public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
        this.cdDigitoContaCredito = cdDigitoContaCredito;
    }

    /**
     * Get: cdTipoContaCredito.
     *
     * @return cdTipoContaCredito
     */
    public String getCdTipoContaCredito() {
        return cdTipoContaCredito;
    }

    /**
     * Set: cdTipoContaCredito.
     *
     * @param cdTipoContaCredito the cd tipo conta credito
     */
    public void setCdTipoContaCredito(String cdTipoContaCredito) {
        this.cdTipoContaCredito = cdTipoContaCredito;
    }

    /**
     * Get: cdTipoIsncricaoFavorecido.
     *
     * @return cdTipoIsncricaoFavorecido
     */
    public String getCdTipoIsncricaoFavorecido() {
        return cdTipoIsncricaoFavorecido;
    }

    /**
     * Set: cdTipoIsncricaoFavorecido.
     *
     * @param cdTipoIsncricaoFavorecido the cd tipo isncricao favorecido
     */
    public void setCdTipoIsncricaoFavorecido(String cdTipoIsncricaoFavorecido) {
        this.cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
    }

    /**
     * Get: cdCtciaInscricaoFavorecido.
     *
     * @return cdCtciaInscricaoFavorecido
     */
    public Long getCdCtciaInscricaoFavorecido() {
        return cdCtciaInscricaoFavorecido;
    }

    /**
     * Set: cdCtciaInscricaoFavorecido.
     *
     * @param cdCtciaInscricaoFavorecido the cd ctcia inscricao favorecido
     */
    public void setCdCtciaInscricaoFavorecido(Long cdCtciaInscricaoFavorecido) {
        this.cdCtciaInscricaoFavorecido = cdCtciaInscricaoFavorecido;
    }

    /**
     * Get: dtCreditoPagamento.
     *
     * @return dtCreditoPagamento
     */
    public String getDtCreditoPagamento() {
        return dtCreditoPagamento;
    }

    /**
     * Set: dtCreditoPagamento.
     *
     * @param dtCreditoPagamento the dt credito pagamento
     */
    public void setDtCreditoPagamento(String dtCreditoPagamento) {
        this.dtCreditoPagamento = dtCreditoPagamento;
    }

    /**
     * Get: numeroConsulta.
     *
     * @return numeroConsulta
     */
    public Integer getNumeroConsulta() {
        return numeroConsulta;
    }

    /**
     * Set: numeroConsulta.
     *
     * @param numeroConsulta the numero consulta
     */
    public void setNumeroConsulta(Integer numeroConsulta) {
        this.numeroConsulta = numeroConsulta;
    }

    /**
     * Get: ocorrencias.
     *
     * @return ocorrencias
     */
    public List<ConsultarAutorizantesOcorrenciaSaidaDTO> getOcorrencias() {
        return ocorrencias;
    }

    /**
     * Set: ocorrencias.
     *
     * @param ocorrencias the ocorrencias
     */
    public void setOcorrencias(List<ConsultarAutorizantesOcorrenciaSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

}