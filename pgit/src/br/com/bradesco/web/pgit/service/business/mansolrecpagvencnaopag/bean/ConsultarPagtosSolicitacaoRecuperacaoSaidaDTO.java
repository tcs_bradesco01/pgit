/*
 * Nome: br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroOcorrencias. */
    private Integer numeroOcorrencias;
    
    /** Atributo cdCorpoCpfCnpj. */
    private Long cdCorpoCpfCnpj;
    
    /** Atributo cdFilialCpfCnpj. */
    private Integer cdFilialCpfCnpj;
    
    /** Atributo cdDigitoCpfCnpj. */
    private Integer cdDigitoCpfCnpj;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
    
    /** Atributo cdEmpresa. */
    private Long cdEmpresa;
    
    /** Atributo dsEmpresa. */
    private String dsEmpresa;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo dsTipoContrato. */
    private String dsTipoContrato;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdProdutoServicoOper. */
    private Integer cdProdutoServicoOper;
    
    /** Atributo dsOperacaoProdutoServico. */
    private String dsOperacaoProdutoServico;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo dsOperacaoProdutoRelacionado. */
    private String dsOperacaoProdutoRelacionado;
    
    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;
    
    /** Atributo dtCredito. */
    private String dtCredito;
    
    /** Atributo vlrEfetivacaoPagamento. */
    private BigDecimal vlrEfetivacaoPagamento;
    
    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;
    
    /** Atributo dsFavorecido. */
    private String dsFavorecido;
    
    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo cdDigitoAgenciaDebito. */
    private Integer cdDigitoAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;
    
    /** Atributo cdSituacaoOperPagamento. */
    private Integer cdSituacaoOperPagamento;
    
    /** Atributo dsSituacaoOperacaoPagamento. */
    private String dsSituacaoOperacaoPagamento;
    
    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;
    
    /** Atributo contaDebito. */
    private String contaDebito;
    
    /**
     * Get: favorecidoFormat.
     *
     * @return favorecidoFormat
     */
    public String getFavorecidoFormat(){
    	if(cdInscricaoFavorecido!= null &&  cdInscricaoFavorecido != 0){
    		StringBuilder format = new StringBuilder();
    		format.append(cdInscricaoFavorecido);
    		format.append("-");
    		format.append(dsFavorecido);
    		return format.toString();
    	}else{
    		return dsFavorecido;
    	}
    }
    
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	
	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	
	/**
	 * Get: cdDigitoAgenciaDebito.
	 *
	 * @return cdDigitoAgenciaDebito
	 */
	public Integer getCdDigitoAgenciaDebito() {
		return cdDigitoAgenciaDebito;
	}
	
	/**
	 * Set: cdDigitoAgenciaDebito.
	 *
	 * @param cdDigitoAgenciaDebito the cd digito agencia debito
	 */
	public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
		this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
	}
	
	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public String getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}
	
	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}
	
	/**
	 * Get: cdDigitoCpfCnpj.
	 *
	 * @return cdDigitoCpfCnpj
	 */
	public Integer getCdDigitoCpfCnpj() {
		return cdDigitoCpfCnpj;
	}
	
	/**
	 * Set: cdDigitoCpfCnpj.
	 *
	 * @param cdDigitoCpfCnpj the cd digito cpf cnpj
	 */
	public void setCdDigitoCpfCnpj(Integer cdDigitoCpfCnpj) {
		this.cdDigitoCpfCnpj = cdDigitoCpfCnpj;
	}
	
	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}
	
	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	
	/**
	 * Get: cdFilialCpfCnpj.
	 *
	 * @return cdFilialCpfCnpj
	 */
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	
	/**
	 * Set: cdFilialCpfCnpj.
	 *
	 * @param cdFilialCpfCnpj the cd filial cpf cnpj
	 */
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	
	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public Long getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}
	
	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOper.
	 *
	 * @return cdProdutoServicoOper
	 */
	public Integer getCdProdutoServicoOper() {
		return cdProdutoServicoOper;
	}
	
	/**
	 * Set: cdProdutoServicoOper.
	 *
	 * @param cdProdutoServicoOper the cd produto servico oper
	 */
	public void setCdProdutoServicoOper(Integer cdProdutoServicoOper) {
		this.cdProdutoServicoOper = cdProdutoServicoOper;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoOperPagamento.
	 *
	 * @return cdSituacaoOperPagamento
	 */
	public Integer getCdSituacaoOperPagamento() {
		return cdSituacaoOperPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperPagamento.
	 *
	 * @param cdSituacaoOperPagamento the cd situacao oper pagamento
	 */
	public void setCdSituacaoOperPagamento(Integer cdSituacaoOperPagamento) {
		this.cdSituacaoOperPagamento = cdSituacaoOperPagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}
	
	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}
	
	/**
	 * Get: dsOperacaoProdutoRelacionado.
	 *
	 * @return dsOperacaoProdutoRelacionado
	 */
	public String getDsOperacaoProdutoRelacionado() {
		return dsOperacaoProdutoRelacionado;
	}
	
	/**
	 * Set: dsOperacaoProdutoRelacionado.
	 *
	 * @param dsOperacaoProdutoRelacionado the ds operacao produto relacionado
	 */
	public void setDsOperacaoProdutoRelacionado(String dsOperacaoProdutoRelacionado) {
		this.dsOperacaoProdutoRelacionado = dsOperacaoProdutoRelacionado;
	}
	
	/**
	 * Get: dsOperacaoProdutoServico.
	 *
	 * @return dsOperacaoProdutoServico
	 */
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}
	
	/**
	 * Set: dsOperacaoProdutoServico.
	 *
	 * @param dsOperacaoProdutoServico the ds operacao produto servico
	 */
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dsSituacaoOperacaoPagamento.
	 *
	 * @return dsSituacaoOperacaoPagamento
	 */
	public String getDsSituacaoOperacaoPagamento() {
		return dsSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: dsSituacaoOperacaoPagamento.
	 *
	 * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
	 */
	public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
		this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: dsTipoContrato.
	 *
	 * @return dsTipoContrato
	 */
	public String getDsTipoContrato() {
		return dsTipoContrato;
	}
	
	/**
	 * Set: dsTipoContrato.
	 *
	 * @param dsTipoContrato the ds tipo contrato
	 */
	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}
	
	/**
	 * Get: dtCredito.
	 *
	 * @return dtCredito
	 */
	public String getDtCredito() {
		return dtCredito;
	}
	
	/**
	 * Set: dtCredito.
	 *
	 * @param dtCredito the dt credito
	 */
	public void setDtCredito(String dtCredito) {
		this.dtCredito = dtCredito;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	/**
	 * Get: vlrEfetivacaoPagamento.
	 *
	 * @return vlrEfetivacaoPagamento
	 */
	public BigDecimal getVlrEfetivacaoPagamento() {
		return vlrEfetivacaoPagamento;
	}
	
	/**
	 * Set: vlrEfetivacaoPagamento.
	 *
	 * @param vlrEfetivacaoPagamento the vlr efetivacao pagamento
	 */
	public void setVlrEfetivacaoPagamento(BigDecimal vlrEfetivacaoPagamento) {
		this.vlrEfetivacaoPagamento = vlrEfetivacaoPagamento;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}
	
	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}
	
	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}
	
	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}
    
    
}
