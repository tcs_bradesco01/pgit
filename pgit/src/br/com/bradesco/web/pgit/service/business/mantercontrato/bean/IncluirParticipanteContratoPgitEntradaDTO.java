/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: IncluirParticipanteContratoPgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirParticipanteContratoPgitEntradaDTO {
	

	/** Atributo codEmpresaContrato. */
	private long  codEmpresaContrato;
	
	/** Atributo numSequenciaContrato. */
	private long numSequenciaContrato;
	
	/** Atributo tipoParticipacao. */
	private int tipoParticipacao;
	
	/** Atributo codParticipante. */
	private long codParticipante;
	
	/** Atributo cpfCnpjParticipante. */
	private long cpfCnpjParticipante;
	
	/** Atributo codFilialCNPJ. */
	private int codFilialCNPJ;
	
	/** Atributo codControleCpfCnpj. */
	private int codControleCpfCnpj;
	
	/** Atributo codTipoContrato. */
	private int codTipoContrato;
	
	private Long cdPessoaJuridica;
	
	/**
	 * Get: codTipoContrato.
	 *
	 * @return codTipoContrato
	 */
	public int getCodTipoContrato() {
		return codTipoContrato;
	}
	
	/**
	 * Set: codTipoContrato.
	 *
	 * @param codTipoContrato the cod tipo contrato
	 */
	public void setCodTipoContrato(int codTipoContrato) {
		this.codTipoContrato = codTipoContrato;
	}
	
	/**
	 * Get: codControleCpfCnpj.
	 *
	 * @return codControleCpfCnpj
	 */
	public int getCodControleCpfCnpj() {
		return codControleCpfCnpj;
	}
	
	/**
	 * Set: codControleCpfCnpj.
	 *
	 * @param codControleCpfCnpj the cod controle cpf cnpj
	 */
	public void setCodControleCpfCnpj(int codControleCpfCnpj) {
		this.codControleCpfCnpj = codControleCpfCnpj;
	}
	
	/**
	 * Get: codEmpresaContrato.
	 *
	 * @return codEmpresaContrato
	 */
	public long getCodEmpresaContrato() {
		return codEmpresaContrato;
	}
	
	/**
	 * Set: codEmpresaContrato.
	 *
	 * @param codEmpresaContrato the cod empresa contrato
	 */
	public void setCodEmpresaContrato(long codEmpresaContrato) {
		this.codEmpresaContrato = codEmpresaContrato;
	}
	
	/**
	 * Get: codFilialCNPJ.
	 *
	 * @return codFilialCNPJ
	 */
	public int getCodFilialCNPJ() {
		return codFilialCNPJ;
	}
	
	/**
	 * Set: codFilialCNPJ.
	 *
	 * @param codFilialCNPJ the cod filial cnpj
	 */
	public void setCodFilialCNPJ(int codFilialCNPJ) {
		this.codFilialCNPJ = codFilialCNPJ;
	}
	
	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public long getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}
	
	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(long cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}
	
	/**
	 * Get: numSequenciaContrato.
	 *
	 * @return numSequenciaContrato
	 */
	public long getNumSequenciaContrato() {
		return numSequenciaContrato;
	}
	
	/**
	 * Set: numSequenciaContrato.
	 *
	 * @param numSequenciaContrato the num sequencia contrato
	 */
	public void setNumSequenciaContrato(long numSequenciaContrato) {
		this.numSequenciaContrato = numSequenciaContrato;
	}

	/**
	 * Get: tipoParticipacao.
	 *
	 * @return tipoParticipacao
	 */
	public int getTipoParticipacao() {
		return tipoParticipacao;
	}
	
	/**
	 * Set: tipoParticipacao.
	 *
	 * @param tipoParticipacao the tipo participacao
	 */
	public void setTipoParticipacao(int tipoParticipacao) {
		this.tipoParticipacao = tipoParticipacao;
	}
	
	/**
	 * Get: codParticipante.
	 *
	 * @return codParticipante
	 */
	public long getCodParticipante() {
		return codParticipante;
	}
	
	/**
	 * Set: codParticipante.
	 *
	 * @param codParticipante the cod participante
	 */
	public void setCodParticipante(long codParticipante) {
		this.codParticipante = codParticipante;
	}

    public Long getCdPessoaJuridica() {
        return cdPessoaJuridica;
    }

    public void setCdPessoaJuridica(Long cdPessoaJuridica) {
        this.cdPessoaJuridica = cdPessoaJuridica;
    }

}
