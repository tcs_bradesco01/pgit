/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProdutoProduto
     */
    private int _cdRelacionamentoProdutoProduto = 0;

    /**
     * keeps track of state for field:
     * _cdRelacionamentoProdutoProduto
     */
    private boolean _has_cdRelacionamentoProdutoProduto;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;

    /**
     * Field _cdCondicaoEconomica
     */
    private int _cdCondicaoEconomica = 0;

    /**
     * keeps track of state for field: _cdCondicaoEconomica
     */
    private boolean _has_cdCondicaoEconomica;

    /**
     * Field _vlTarifaContratoMinimo
     */
    private java.math.BigDecimal _vlTarifaContratoMinimo = new java.math.BigDecimal("0");

    /**
     * Field _vlTarifaContratoMaximo
     */
    private java.math.BigDecimal _vlTarifaContratoMaximo = new java.math.BigDecimal("0");

    /**
     * Field _vlTarifaContratoNegocio
     */
    private java.math.BigDecimal _vlTarifaContratoNegocio = new java.math.BigDecimal("0");

    /**
     * Field _cdAlcada
     */
    private int _cdAlcada = 0;

    /**
     * keeps track of state for field: _cdAlcada
     */
    private boolean _has_cdAlcada;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlTarifaContratoMinimo(new java.math.BigDecimal("0"));
        setVlTarifaContratoMaximo(new java.math.BigDecimal("0"));
        setVlTarifaContratoNegocio(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAlcada
     * 
     */
    public void deleteCdAlcada()
    {
        this._has_cdAlcada= false;
    } //-- void deleteCdAlcada() 

    /**
     * Method deleteCdCondicaoEconomica
     * 
     */
    public void deleteCdCondicaoEconomica()
    {
        this._has_cdCondicaoEconomica= false;
    } //-- void deleteCdCondicaoEconomica() 

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProdutoProduto
     * 
     */
    public void deleteCdRelacionamentoProdutoProduto()
    {
        this._has_cdRelacionamentoProdutoProduto= false;
    } //-- void deleteCdRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'cdAlcada'.
     * 
     * @return int
     * @return the value of field 'cdAlcada'.
     */
    public int getCdAlcada()
    {
        return this._cdAlcada;
    } //-- int getCdAlcada() 

    /**
     * Returns the value of field 'cdCondicaoEconomica'.
     * 
     * @return int
     * @return the value of field 'cdCondicaoEconomica'.
     */
    public int getCdCondicaoEconomica()
    {
        return this._cdCondicaoEconomica;
    } //-- int getCdCondicaoEconomica() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProdutoProduto'.
     */
    public int getCdRelacionamentoProdutoProduto()
    {
        return this._cdRelacionamentoProdutoProduto;
    } //-- int getCdRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'vlTarifaContratoMaximo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaContratoMaximo'.
     */
    public java.math.BigDecimal getVlTarifaContratoMaximo()
    {
        return this._vlTarifaContratoMaximo;
    } //-- java.math.BigDecimal getVlTarifaContratoMaximo() 

    /**
     * Returns the value of field 'vlTarifaContratoMinimo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaContratoMinimo'.
     */
    public java.math.BigDecimal getVlTarifaContratoMinimo()
    {
        return this._vlTarifaContratoMinimo;
    } //-- java.math.BigDecimal getVlTarifaContratoMinimo() 

    /**
     * Returns the value of field 'vlTarifaContratoNegocio'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaContratoNegocio'.
     */
    public java.math.BigDecimal getVlTarifaContratoNegocio()
    {
        return this._vlTarifaContratoNegocio;
    } //-- java.math.BigDecimal getVlTarifaContratoNegocio() 

    /**
     * Method hasCdAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAlcada()
    {
        return this._has_cdAlcada;
    } //-- boolean hasCdAlcada() 

    /**
     * Method hasCdCondicaoEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCondicaoEconomica()
    {
        return this._has_cdCondicaoEconomica;
    } //-- boolean hasCdCondicaoEconomica() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProdutoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProdutoProduto()
    {
        return this._has_cdRelacionamentoProdutoProduto;
    } //-- boolean hasCdRelacionamentoProdutoProduto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAlcada'.
     * 
     * @param cdAlcada the value of field 'cdAlcada'.
     */
    public void setCdAlcada(int cdAlcada)
    {
        this._cdAlcada = cdAlcada;
        this._has_cdAlcada = true;
    } //-- void setCdAlcada(int) 

    /**
     * Sets the value of field 'cdCondicaoEconomica'.
     * 
     * @param cdCondicaoEconomica the value of field
     * 'cdCondicaoEconomica'.
     */
    public void setCdCondicaoEconomica(int cdCondicaoEconomica)
    {
        this._cdCondicaoEconomica = cdCondicaoEconomica;
        this._has_cdCondicaoEconomica = true;
    } //-- void setCdCondicaoEconomica(int) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @param cdRelacionamentoProdutoProduto the value of field
     * 'cdRelacionamentoProdutoProduto'.
     */
    public void setCdRelacionamentoProdutoProduto(int cdRelacionamentoProdutoProduto)
    {
        this._cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
        this._has_cdRelacionamentoProdutoProduto = true;
    } //-- void setCdRelacionamentoProdutoProduto(int) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'vlTarifaContratoMaximo'.
     * 
     * @param vlTarifaContratoMaximo the value of field
     * 'vlTarifaContratoMaximo'.
     */
    public void setVlTarifaContratoMaximo(java.math.BigDecimal vlTarifaContratoMaximo)
    {
        this._vlTarifaContratoMaximo = vlTarifaContratoMaximo;
    } //-- void setVlTarifaContratoMaximo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaContratoMinimo'.
     * 
     * @param vlTarifaContratoMinimo the value of field
     * 'vlTarifaContratoMinimo'.
     */
    public void setVlTarifaContratoMinimo(java.math.BigDecimal vlTarifaContratoMinimo)
    {
        this._vlTarifaContratoMinimo = vlTarifaContratoMinimo;
    } //-- void setVlTarifaContratoMinimo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaContratoNegocio'.
     * 
     * @param vlTarifaContratoNegocio the value of field
     * 'vlTarifaContratoNegocio'.
     */
    public void setVlTarifaContratoNegocio(java.math.BigDecimal vlTarifaContratoNegocio)
    {
        this._vlTarifaContratoNegocio = vlTarifaContratoNegocio;
    } //-- void setVlTarifaContratoNegocio(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
