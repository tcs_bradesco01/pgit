/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelcontratos.bean;

/**
 * Nome: DetalharSolRelContratosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolRelContratosEntradaDTO {

	
	/** Atributo cdSolicitacaoPagamentoIntegrado. */
	private int cdSolicitacaoPagamentoIntegrado;
	
	/** Atributo nrSolicitacaoPagamentoIntegrado. */
	private int nrSolicitacaoPagamentoIntegrado;
	
	/** Atributo tipoRelatorio. */
	private int tipoRelatorio;
	
	/** Atributo dataInicio. */
	private String dataInicio;
	
	/** Atributo dataFim. */
	private String dataFim;
	

	
	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public int getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			int cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public int getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			int nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: tipoRelatorio.
	 *
	 * @return tipoRelatorio
	 */
	public int getTipoRelatorio() {
		return tipoRelatorio;
	}
	
	/**
	 * Set: tipoRelatorio.
	 *
	 * @param tipoRelatorio the tipo relatorio
	 */
	public void setTipoRelatorio(int tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}
	
	/**
	 * Get: dataFim.
	 *
	 * @return dataFim
	 */
	public String getDataFim() {
		return dataFim;
	}
	
	/**
	 * Set: dataFim.
	 *
	 * @param dataFim the data fim
	 */
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	
	/**
	 * Get: dataInicio.
	 *
	 * @return dataInicio
	 */
	public String getDataInicio() {
		return dataInicio;
	}
	
	/**
	 * Set: dataInicio.
	 *
	 * @param dataInicio the data inicio
	 */
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	
	
	
}
