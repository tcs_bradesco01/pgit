/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.impl;

import br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.IAtivarContratoPendenteAssinaturaService;
import br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.bean.AtivarContratoPendAssinaturaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.bean.AtivarContratoPendAssinaturaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.ativarcontratopendassinatura.request.AtivarContratoPendAssinaturaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.ativarcontratopendassinatura.response.AtivarContratoPendAssinaturaResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AtivarContratoPendenteAssinatura
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AtivarContratoPendenteAssinaturaServiceImpl implements IAtivarContratoPendenteAssinaturaService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter; 
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.IAtivarContratoPendenteAssinaturaService#ativarContratoPendAssinatura(br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.bean.AtivarContratoPendAssinaturaEntradaDTO)
	 */
	public AtivarContratoPendAssinaturaSaidaDTO ativarContratoPendAssinatura(AtivarContratoPendAssinaturaEntradaDTO ativarContratoPendAssinaturaEntradaDTO) {
		
		AtivarContratoPendAssinaturaSaidaDTO ativarContratoPendAssinaturaSaidaDTO = new AtivarContratoPendAssinaturaSaidaDTO();
		AtivarContratoPendAssinaturaRequest request = new AtivarContratoPendAssinaturaRequest();
		AtivarContratoPendAssinaturaResponse response = new AtivarContratoPendAssinaturaResponse();
		
		request.setCdMotivoSituacaoContrato(ativarContratoPendAssinaturaEntradaDTO.getCdMotivoSituacaoContrato());
		request.setCdPessoaJuridicaContrato(ativarContratoPendAssinaturaEntradaDTO.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(ativarContratoPendAssinaturaEntradaDTO.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(ativarContratoPendAssinaturaEntradaDTO.getNrSequenciaContratoNegocio());
		
		response = getFactoryAdapter().getAtivarContratoPendAssinaturaPDCAdapter().invokeProcess(request);
		
		ativarContratoPendAssinaturaSaidaDTO.setCodMensagem(response.getCodMensagem());
		ativarContratoPendAssinaturaSaidaDTO.setMensagem(response.getMensagem());
	
		return ativarContratoPendAssinaturaSaidaDTO;
	}

}

