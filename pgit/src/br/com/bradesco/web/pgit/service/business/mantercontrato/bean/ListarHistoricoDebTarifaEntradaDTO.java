package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

public class ListarHistoricoDebTarifaEntradaDTO {
	
    private Integer nrOcorrencias;
    private Long cdPessoaJuridicaNegocio;
    private Integer cdTipoContratoNegocio;
    private Long nrSequenciaContratoNegocio;
    private String dataInicio;
    private String dataFim;
    
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

}
