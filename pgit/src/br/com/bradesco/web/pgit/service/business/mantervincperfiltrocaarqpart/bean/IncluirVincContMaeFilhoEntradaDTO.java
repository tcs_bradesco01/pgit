package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

public class IncluirVincContMaeFilhoEntradaDTO {
	
	private Integer cdTipoLayoutArquivo;
	private Long cdPerfilTrocaArquivo;
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Integer cdTipoParticipacaoPessoa;
	private Long cdPessoa;
	private Integer cdTipoLayoutMae;
	private Long cdPerfilTrocaMae;
	private Long cdPessoaJuridicaMae;
	private Integer cdTipoContratoMae;
	private Long nrSequencialContratoMae;
	private Integer cdTipoParticipanteMae;
	private Long cdCpessoaMae;

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public Long getCdPerfilTrocaArquivo() {
		return cdPerfilTrocaArquivo;
	}

	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	public Long getCdPessoa() {
		return cdPessoa;
	}

	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	public Integer getCdTipoLayoutMae() {
		return cdTipoLayoutMae;
	}

	public void setCdTipoLayoutMae(Integer cdTipoLayoutMae) {
		this.cdTipoLayoutMae = cdTipoLayoutMae;
	}

	public Long getCdPerfilTrocaMae() {
		return cdPerfilTrocaMae;
	}

	public void setCdPerfilTrocaMae(Long cdPerfilTrocaMae) {
		this.cdPerfilTrocaMae = cdPerfilTrocaMae;
	}

	public Long getCdPessoaJuridicaMae() {
		return cdPessoaJuridicaMae;
	}

	public void setCdPessoaJuridicaMae(Long cdPessoaJuridicaMae) {
		this.cdPessoaJuridicaMae = cdPessoaJuridicaMae;
	}

	public Integer getCdTipoContratoMae() {
		return cdTipoContratoMae;
	}

	public void setCdTipoContratoMae(Integer cdTipoContratoMae) {
		this.cdTipoContratoMae = cdTipoContratoMae;
	}

	public Long getNrSequencialContratoMae() {
		return nrSequencialContratoMae;
	}

	public void setNrSequencialContratoMae(Long nrSequencialContratoMae) {
		this.nrSequencialContratoMae = nrSequencialContratoMae;
	}

	public Integer getCdTipoParticipanteMae() {
		return cdTipoParticipanteMae;
	}

	public void setCdTipoParticipanteMae(Integer cdTipoParticipanteMae) {
		this.cdTipoParticipanteMae = cdTipoParticipanteMae;
	}

	public Long getCdCpessoaMae() {
		return cdCpessoaMae;
	}

	public void setCdCpessoaMae(Long cdCpessoaMae) {
		this.cdCpessoaMae = cdCpessoaMae;
	}
}
