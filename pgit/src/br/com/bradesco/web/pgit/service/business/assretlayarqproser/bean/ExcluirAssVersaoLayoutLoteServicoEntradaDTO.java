package br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean;


/**
 * Arquivo criado em 09/02/15.
 */
public class ExcluirAssVersaoLayoutLoteServicoEntradaDTO {

    private Integer cdTipoLayoutArquivo;

    private Integer nrVersaoLayoutArquivo;

    private Integer cdTipoLoteLayout;

    private Integer nrVersaoLoteLayout;

    private Integer cdTipoServicoCnab;

    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
        this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    public Integer getCdTipoLayoutArquivo() {
        return this.cdTipoLayoutArquivo;
    }

    public void setNrVersaoLayoutArquivo(Integer nrVersaoLayoutArquivo) {
        this.nrVersaoLayoutArquivo = nrVersaoLayoutArquivo;
    }

    public Integer getNrVersaoLayoutArquivo() {
        return this.nrVersaoLayoutArquivo;
    }

    public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
        this.cdTipoLoteLayout = cdTipoLoteLayout;
    }

    public Integer getCdTipoLoteLayout() {
        return this.cdTipoLoteLayout;
    }

    public void setNrVersaoLoteLayout(Integer nrVersaoLoteLayout) {
        this.nrVersaoLoteLayout = nrVersaoLoteLayout;
    }

    public Integer getNrVersaoLoteLayout() {
        return this.nrVersaoLoteLayout;
    }

    public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
        this.cdTipoServicoCnab = cdTipoServicoCnab;
    }

    public Integer getCdTipoServicoCnab() {
        return this.cdTipoServicoCnab;
    }
}