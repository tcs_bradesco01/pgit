/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoParticipacao
     */
    private int _cdTipoParticipacao = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacao
     */
    private boolean _has_cdTipoParticipacao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _nomeParticipante
     */
    private java.lang.String _nomeParticipante;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdusuarioManutencao
     */
    private java.lang.String _cdusuarioManutencao;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private java.lang.String _cdIndicadorTipoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoParticipacao
     * 
     */
    public void deleteCdTipoParticipacao()
    {
        this._has_cdTipoParticipacao= false;
    } //-- void deleteCdTipoParticipacao() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public java.lang.String getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- java.lang.String getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoParticipacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacao'.
     */
    public int getCdTipoParticipacao()
    {
        return this._cdTipoParticipacao;
    } //-- int getCdTipoParticipacao() 

    /**
     * Returns the value of field 'cdusuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdusuarioManutencao'.
     */
    public java.lang.String getCdusuarioManutencao()
    {
        return this._cdusuarioManutencao;
    } //-- java.lang.String getCdusuarioManutencao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'nomeParticipante'.
     * 
     * @return String
     * @return the value of field 'nomeParticipante'.
     */
    public java.lang.String getNomeParticipante()
    {
        return this._nomeParticipante;
    } //-- java.lang.String getNomeParticipante() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacao()
    {
        return this._has_cdTipoParticipacao;
    } //-- boolean hasCdTipoParticipacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(java.lang.String cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
    } //-- void setCdIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoParticipacao'.
     * 
     * @param cdTipoParticipacao the value of field
     * 'cdTipoParticipacao'.
     */
    public void setCdTipoParticipacao(int cdTipoParticipacao)
    {
        this._cdTipoParticipacao = cdTipoParticipacao;
        this._has_cdTipoParticipacao = true;
    } //-- void setCdTipoParticipacao(int) 

    /**
     * Sets the value of field 'cdusuarioManutencao'.
     * 
     * @param cdusuarioManutencao the value of field
     * 'cdusuarioManutencao'.
     */
    public void setCdusuarioManutencao(java.lang.String cdusuarioManutencao)
    {
        this._cdusuarioManutencao = cdusuarioManutencao;
    } //-- void setCdusuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nomeParticipante'.
     * 
     * @param nomeParticipante the value of field 'nomeParticipante'
     */
    public void setNomeParticipante(java.lang.String nomeParticipante)
    {
        this._nomeParticipante = nomeParticipante;
    } //-- void setNomeParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
