package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarRepresentanteEntradaDTO
 * <p>
 * Propůsito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarRepresentanteEntradaDTO {

	/**
     * Atributo _maxOcorrencias
     */
    private Integer maxOcorrencias;

    /**
     * Atributo _cdpessoaJuridicaContrato
     */
    private Long cdpessoaJuridicaContrato;

    /**
     * Atributo _cdTipoContratoNegocio
     */
    private Integer cdTipoContratoNegocio;

    /**
     * Atributo _nrSequenciaContratoNegocio
     */
    private Long nrSequenciaContratoNegocio;

    /**
     * Atributo _cdFlagRepresentante
     */
    private String cdFlagRepresentante;

	/**
	 * @return the maxOcorrencias
	 */
	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}

	/**
	 * @param maxOcorrencias the maxOcorrencias to set
	 */
	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}

	/**
	 * @return the cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	/**
	 * @param cdpessoaJuridicaContrato the cdpessoaJuridicaContrato to set
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * @return the cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * @param cdTipoContratoNegocio the cdTipoContratoNegocio to set
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * @return the nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * @param nrSequenciaContratoNegocio the nrSequenciaContratoNegocio to set
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * @return the cdFlagRepresentante
	 */
	public String getCdFlagRepresentante() {
		return cdFlagRepresentante;
	}

	/**
	 * @param cdFlagRepresentante the cdFlagRepresentante to set
	 */
	public void setCdFlagRepresentante(String cdFlagRepresentante) {
		this.cdFlagRepresentante = cdFlagRepresentante;
	}
}
