/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetalhesSolicitacaoOPResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalhesSolicitacaoOPResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdBancoEstorno
     */
    private int _cdBancoEstorno = 0;

    /**
     * keeps track of state for field: _cdBancoEstorno
     */
    private boolean _has_cdBancoEstorno;

    /**
     * Field _dsBancoEstorno
     */
    private java.lang.String _dsBancoEstorno;

    /**
     * Field _cdAgenciaEstorno
     */
    private int _cdAgenciaEstorno = 0;

    /**
     * keeps track of state for field: _cdAgenciaEstorno
     */
    private boolean _has_cdAgenciaEstorno;

    /**
     * Field _cdDigitoAgenciaEstorno
     */
    private int _cdDigitoAgenciaEstorno = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaEstorno
     */
    private boolean _has_cdDigitoAgenciaEstorno;

    /**
     * Field _dsAgenciaEstorno
     */
    private java.lang.String _dsAgenciaEstorno;

    /**
     * Field _cdContaEstorno
     */
    private long _cdContaEstorno = 0;

    /**
     * keeps track of state for field: _cdContaEstorno
     */
    private boolean _has_cdContaEstorno;

    /**
     * Field _cdDigitoContaEstorno
     */
    private java.lang.String _cdDigitoContaEstorno;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;

    /**
     * Field _situacaoSolicitacao
     */
    private java.lang.String _situacaoSolicitacao;

    /**
     * Field _cdMotivoSolicitacao
     */
    private java.lang.String _cdMotivoSolicitacao;

    /**
     * Field _dsObservacao
     */
    private java.lang.String _dsObservacao;

    /**
     * Field _dsDataSolicitacao
     */
    private java.lang.String _dsDataSolicitacao;

    /**
     * Field _dsHoraSolicitacao
     */
    private java.lang.String _dsHoraSolicitacao;

    /**
     * Field _dtAutorizacao
     */
    private java.lang.String _dtAutorizacao;

    /**
     * Field _hrAutorizacao
     */
    private java.lang.String _hrAutorizacao;

    /**
     * Field _cdUsuarioAutorizacao
     */
    private java.lang.String _cdUsuarioAutorizacao;

    /**
     * Field _dtPrevistaEstorno
     */
    private java.lang.String _dtPrevistaEstorno;

    /**
     * Field _cdValorPrevistoEstorno
     */
    private java.math.BigDecimal _cdValorPrevistoEstorno = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalhesSolicitacaoOPResponse() 
     {
        super();
        setCdValorPrevistoEstorno(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.response.ConsultarDetalhesSolicitacaoOPResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaEstorno
     * 
     */
    public void deleteCdAgenciaEstorno()
    {
        this._has_cdAgenciaEstorno= false;
    } //-- void deleteCdAgenciaEstorno() 

    /**
     * Method deleteCdBancoEstorno
     * 
     */
    public void deleteCdBancoEstorno()
    {
        this._has_cdBancoEstorno= false;
    } //-- void deleteCdBancoEstorno() 

    /**
     * Method deleteCdContaEstorno
     * 
     */
    public void deleteCdContaEstorno()
    {
        this._has_cdContaEstorno= false;
    } //-- void deleteCdContaEstorno() 

    /**
     * Method deleteCdDigitoAgenciaEstorno
     * 
     */
    public void deleteCdDigitoAgenciaEstorno()
    {
        this._has_cdDigitoAgenciaEstorno= false;
    } //-- void deleteCdDigitoAgenciaEstorno() 

    /**
     * Returns the value of field 'cdAgenciaEstorno'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaEstorno'.
     */
    public int getCdAgenciaEstorno()
    {
        return this._cdAgenciaEstorno;
    } //-- int getCdAgenciaEstorno() 

    /**
     * Returns the value of field 'cdBancoEstorno'.
     * 
     * @return int
     * @return the value of field 'cdBancoEstorno'.
     */
    public int getCdBancoEstorno()
    {
        return this._cdBancoEstorno;
    } //-- int getCdBancoEstorno() 

    /**
     * Returns the value of field 'cdContaEstorno'.
     * 
     * @return long
     * @return the value of field 'cdContaEstorno'.
     */
    public long getCdContaEstorno()
    {
        return this._cdContaEstorno;
    } //-- long getCdContaEstorno() 

    /**
     * Returns the value of field 'cdDigitoAgenciaEstorno'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaEstorno'.
     */
    public int getCdDigitoAgenciaEstorno()
    {
        return this._cdDigitoAgenciaEstorno;
    } //-- int getCdDigitoAgenciaEstorno() 

    /**
     * Returns the value of field 'cdDigitoContaEstorno'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaEstorno'.
     */
    public java.lang.String getCdDigitoContaEstorno()
    {
        return this._cdDigitoContaEstorno;
    } //-- java.lang.String getCdDigitoContaEstorno() 

    /**
     * Returns the value of field 'cdMotivoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdMotivoSolicitacao'.
     */
    public java.lang.String getCdMotivoSolicitacao()
    {
        return this._cdMotivoSolicitacao;
    } //-- java.lang.String getCdMotivoSolicitacao() 

    /**
     * Returns the value of field 'cdUsuarioAutorizacao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioAutorizacao'.
     */
    public java.lang.String getCdUsuarioAutorizacao()
    {
        return this._cdUsuarioAutorizacao;
    } //-- java.lang.String getCdUsuarioAutorizacao() 

    /**
     * Returns the value of field 'cdValorPrevistoEstorno'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdValorPrevistoEstorno'.
     */
    public java.math.BigDecimal getCdValorPrevistoEstorno()
    {
        return this._cdValorPrevistoEstorno;
    } //-- java.math.BigDecimal getCdValorPrevistoEstorno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaEstorno'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaEstorno'.
     */
    public java.lang.String getDsAgenciaEstorno()
    {
        return this._dsAgenciaEstorno;
    } //-- java.lang.String getDsAgenciaEstorno() 

    /**
     * Returns the value of field 'dsBancoEstorno'.
     * 
     * @return String
     * @return the value of field 'dsBancoEstorno'.
     */
    public java.lang.String getDsBancoEstorno()
    {
        return this._dsBancoEstorno;
    } //-- java.lang.String getDsBancoEstorno() 

    /**
     * Returns the value of field 'dsDataSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsDataSolicitacao'.
     */
    public java.lang.String getDsDataSolicitacao()
    {
        return this._dsDataSolicitacao;
    } //-- java.lang.String getDsDataSolicitacao() 

    /**
     * Returns the value of field 'dsHoraSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsHoraSolicitacao'.
     */
    public java.lang.String getDsHoraSolicitacao()
    {
        return this._dsHoraSolicitacao;
    } //-- java.lang.String getDsHoraSolicitacao() 

    /**
     * Returns the value of field 'dsObservacao'.
     * 
     * @return String
     * @return the value of field 'dsObservacao'.
     */
    public java.lang.String getDsObservacao()
    {
        return this._dsObservacao;
    } //-- java.lang.String getDsObservacao() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'dtAutorizacao'.
     * 
     * @return String
     * @return the value of field 'dtAutorizacao'.
     */
    public java.lang.String getDtAutorizacao()
    {
        return this._dtAutorizacao;
    } //-- java.lang.String getDtAutorizacao() 

    /**
     * Returns the value of field 'dtPrevistaEstorno'.
     * 
     * @return String
     * @return the value of field 'dtPrevistaEstorno'.
     */
    public java.lang.String getDtPrevistaEstorno()
    {
        return this._dtPrevistaEstorno;
    } //-- java.lang.String getDtPrevistaEstorno() 

    /**
     * Returns the value of field 'hrAutorizacao'.
     * 
     * @return String
     * @return the value of field 'hrAutorizacao'.
     */
    public java.lang.String getHrAutorizacao()
    {
        return this._hrAutorizacao;
    } //-- java.lang.String getHrAutorizacao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'situacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'situacaoSolicitacao'.
     */
    public java.lang.String getSituacaoSolicitacao()
    {
        return this._situacaoSolicitacao;
    } //-- java.lang.String getSituacaoSolicitacao() 

    /**
     * Method hasCdAgenciaEstorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaEstorno()
    {
        return this._has_cdAgenciaEstorno;
    } //-- boolean hasCdAgenciaEstorno() 

    /**
     * Method hasCdBancoEstorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoEstorno()
    {
        return this._has_cdBancoEstorno;
    } //-- boolean hasCdBancoEstorno() 

    /**
     * Method hasCdContaEstorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaEstorno()
    {
        return this._has_cdContaEstorno;
    } //-- boolean hasCdContaEstorno() 

    /**
     * Method hasCdDigitoAgenciaEstorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaEstorno()
    {
        return this._has_cdDigitoAgenciaEstorno;
    } //-- boolean hasCdDigitoAgenciaEstorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaEstorno'.
     * 
     * @param cdAgenciaEstorno the value of field 'cdAgenciaEstorno'
     */
    public void setCdAgenciaEstorno(int cdAgenciaEstorno)
    {
        this._cdAgenciaEstorno = cdAgenciaEstorno;
        this._has_cdAgenciaEstorno = true;
    } //-- void setCdAgenciaEstorno(int) 

    /**
     * Sets the value of field 'cdBancoEstorno'.
     * 
     * @param cdBancoEstorno the value of field 'cdBancoEstorno'.
     */
    public void setCdBancoEstorno(int cdBancoEstorno)
    {
        this._cdBancoEstorno = cdBancoEstorno;
        this._has_cdBancoEstorno = true;
    } //-- void setCdBancoEstorno(int) 

    /**
     * Sets the value of field 'cdContaEstorno'.
     * 
     * @param cdContaEstorno the value of field 'cdContaEstorno'.
     */
    public void setCdContaEstorno(long cdContaEstorno)
    {
        this._cdContaEstorno = cdContaEstorno;
        this._has_cdContaEstorno = true;
    } //-- void setCdContaEstorno(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaEstorno'.
     * 
     * @param cdDigitoAgenciaEstorno the value of field
     * 'cdDigitoAgenciaEstorno'.
     */
    public void setCdDigitoAgenciaEstorno(int cdDigitoAgenciaEstorno)
    {
        this._cdDigitoAgenciaEstorno = cdDigitoAgenciaEstorno;
        this._has_cdDigitoAgenciaEstorno = true;
    } //-- void setCdDigitoAgenciaEstorno(int) 

    /**
     * Sets the value of field 'cdDigitoContaEstorno'.
     * 
     * @param cdDigitoContaEstorno the value of field
     * 'cdDigitoContaEstorno'.
     */
    public void setCdDigitoContaEstorno(java.lang.String cdDigitoContaEstorno)
    {
        this._cdDigitoContaEstorno = cdDigitoContaEstorno;
    } //-- void setCdDigitoContaEstorno(java.lang.String) 

    /**
     * Sets the value of field 'cdMotivoSolicitacao'.
     * 
     * @param cdMotivoSolicitacao the value of field
     * 'cdMotivoSolicitacao'.
     */
    public void setCdMotivoSolicitacao(java.lang.String cdMotivoSolicitacao)
    {
        this._cdMotivoSolicitacao = cdMotivoSolicitacao;
    } //-- void setCdMotivoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioAutorizacao'.
     * 
     * @param cdUsuarioAutorizacao the value of field
     * 'cdUsuarioAutorizacao'.
     */
    public void setCdUsuarioAutorizacao(java.lang.String cdUsuarioAutorizacao)
    {
        this._cdUsuarioAutorizacao = cdUsuarioAutorizacao;
    } //-- void setCdUsuarioAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'cdValorPrevistoEstorno'.
     * 
     * @param cdValorPrevistoEstorno the value of field
     * 'cdValorPrevistoEstorno'.
     */
    public void setCdValorPrevistoEstorno(java.math.BigDecimal cdValorPrevistoEstorno)
    {
        this._cdValorPrevistoEstorno = cdValorPrevistoEstorno;
    } //-- void setCdValorPrevistoEstorno(java.math.BigDecimal) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaEstorno'.
     * 
     * @param dsAgenciaEstorno the value of field 'dsAgenciaEstorno'
     */
    public void setDsAgenciaEstorno(java.lang.String dsAgenciaEstorno)
    {
        this._dsAgenciaEstorno = dsAgenciaEstorno;
    } //-- void setDsAgenciaEstorno(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoEstorno'.
     * 
     * @param dsBancoEstorno the value of field 'dsBancoEstorno'.
     */
    public void setDsBancoEstorno(java.lang.String dsBancoEstorno)
    {
        this._dsBancoEstorno = dsBancoEstorno;
    } //-- void setDsBancoEstorno(java.lang.String) 

    /**
     * Sets the value of field 'dsDataSolicitacao'.
     * 
     * @param dsDataSolicitacao the value of field
     * 'dsDataSolicitacao'.
     */
    public void setDsDataSolicitacao(java.lang.String dsDataSolicitacao)
    {
        this._dsDataSolicitacao = dsDataSolicitacao;
    } //-- void setDsDataSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsHoraSolicitacao'.
     * 
     * @param dsHoraSolicitacao the value of field
     * 'dsHoraSolicitacao'.
     */
    public void setDsHoraSolicitacao(java.lang.String dsHoraSolicitacao)
    {
        this._dsHoraSolicitacao = dsHoraSolicitacao;
    } //-- void setDsHoraSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsObservacao'.
     * 
     * @param dsObservacao the value of field 'dsObservacao'.
     */
    public void setDsObservacao(java.lang.String dsObservacao)
    {
        this._dsObservacao = dsObservacao;
    } //-- void setDsObservacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dtAutorizacao'.
     * 
     * @param dtAutorizacao the value of field 'dtAutorizacao'.
     */
    public void setDtAutorizacao(java.lang.String dtAutorizacao)
    {
        this._dtAutorizacao = dtAutorizacao;
    } //-- void setDtAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'dtPrevistaEstorno'.
     * 
     * @param dtPrevistaEstorno the value of field
     * 'dtPrevistaEstorno'.
     */
    public void setDtPrevistaEstorno(java.lang.String dtPrevistaEstorno)
    {
        this._dtPrevistaEstorno = dtPrevistaEstorno;
    } //-- void setDtPrevistaEstorno(java.lang.String) 

    /**
     * Sets the value of field 'hrAutorizacao'.
     * 
     * @param hrAutorizacao the value of field 'hrAutorizacao'.
     */
    public void setHrAutorizacao(java.lang.String hrAutorizacao)
    {
        this._hrAutorizacao = hrAutorizacao;
    } //-- void setHrAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'situacaoSolicitacao'.
     * 
     * @param situacaoSolicitacao the value of field
     * 'situacaoSolicitacao'.
     */
    public void setSituacaoSolicitacao(java.lang.String situacaoSolicitacao)
    {
        this._situacaoSolicitacao = situacaoSolicitacao;
    } //-- void setSituacaoSolicitacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalhesSolicitacaoOPResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.response.ConsultarDetalhesSolicitacaoOPResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.response.ConsultarDetalhesSolicitacaoOPResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.response.ConsultarDetalhesSolicitacaoOPResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.response.ConsultarDetalhesSolicitacaoOPResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
