/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdOrgaoPagador
     */
    private int _cdOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdOrgaoPagador
     */
    private boolean _has_cdOrgaoPagador;

    /**
     * Field _horario
     */
    private java.lang.String _horario;

    /**
     * Field _cdTipoUnidadeOrganizacional
     */
    private int _cdTipoUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacional
     */
    private boolean _has_cdTipoUnidadeOrganizacional;

    /**
     * Field _dsTipoUnidadeOrganizacional
     */
    private java.lang.String _dsTipoUnidadeOrganizacional;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _dsPessoJuridica
     */
    private java.lang.String _dsPessoJuridica;

    /**
     * Field _nrSeqUnidadeOrganizacional
     */
    private int _nrSeqUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _nrSeqUnidadeOrganizacional
     */
    private boolean _has_nrSeqUnidadeOrganizacional;

    /**
     * Field _dsSeqUnidadeOrganizacional
     */
    private java.lang.String _dsSeqUnidadeOrganizacional;

    /**
     * Field _dsSituacao
     */
    private int _dsSituacao = 0;

    /**
     * keeps track of state for field: _dsSituacao
     */
    private boolean _has_dsSituacao;

    /**
     * Field _dsUsuarioResponsavel
     */
    private java.lang.String _dsUsuarioResponsavel;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdOrgaoPagador
     * 
     */
    public void deleteCdOrgaoPagador()
    {
        this._has_cdOrgaoPagador= false;
    } //-- void deleteCdOrgaoPagador() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdTipoUnidadeOrganizacional
     * 
     */
    public void deleteCdTipoUnidadeOrganizacional()
    {
        this._has_cdTipoUnidadeOrganizacional= false;
    } //-- void deleteCdTipoUnidadeOrganizacional() 

    /**
     * Method deleteDsSituacao
     * 
     */
    public void deleteDsSituacao()
    {
        this._has_dsSituacao= false;
    } //-- void deleteDsSituacao() 

    /**
     * Method deleteNrSeqUnidadeOrganizacional
     * 
     */
    public void deleteNrSeqUnidadeOrganizacional()
    {
        this._has_nrSeqUnidadeOrganizacional= false;
    } //-- void deleteNrSeqUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdOrgaoPagador'.
     */
    public int getCdOrgaoPagador()
    {
        return this._cdOrgaoPagador;
    } //-- int getCdOrgaoPagador() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacional'.
     */
    public int getCdTipoUnidadeOrganizacional()
    {
        return this._cdTipoUnidadeOrganizacional;
    } //-- int getCdTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsPessoJuridica'.
     * 
     * @return String
     * @return the value of field 'dsPessoJuridica'.
     */
    public java.lang.String getDsPessoJuridica()
    {
        return this._dsPessoJuridica;
    } //-- java.lang.String getDsPessoJuridica() 

    /**
     * Returns the value of field 'dsSeqUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsSeqUnidadeOrganizacional'.
     */
    public java.lang.String getDsSeqUnidadeOrganizacional()
    {
        return this._dsSeqUnidadeOrganizacional;
    } //-- java.lang.String getDsSeqUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsSituacao'.
     * 
     * @return int
     * @return the value of field 'dsSituacao'.
     */
    public int getDsSituacao()
    {
        return this._dsSituacao;
    } //-- int getDsSituacao() 

    /**
     * Returns the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeOrganizacional'.
     */
    public java.lang.String getDsTipoUnidadeOrganizacional()
    {
        return this._dsTipoUnidadeOrganizacional;
    } //-- java.lang.String getDsTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsUsuarioResponsavel'.
     * 
     * @return String
     * @return the value of field 'dsUsuarioResponsavel'.
     */
    public java.lang.String getDsUsuarioResponsavel()
    {
        return this._dsUsuarioResponsavel;
    } //-- java.lang.String getDsUsuarioResponsavel() 

    /**
     * Returns the value of field 'horario'.
     * 
     * @return String
     * @return the value of field 'horario'.
     */
    public java.lang.String getHorario()
    {
        return this._horario;
    } //-- java.lang.String getHorario() 

    /**
     * Returns the value of field 'nrSeqUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSeqUnidadeOrganizacional'.
     */
    public int getNrSeqUnidadeOrganizacional()
    {
        return this._nrSeqUnidadeOrganizacional;
    } //-- int getNrSeqUnidadeOrganizacional() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrgaoPagador()
    {
        return this._has_cdOrgaoPagador;
    } //-- boolean hasCdOrgaoPagador() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdTipoUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacional()
    {
        return this._has_cdTipoUnidadeOrganizacional;
    } //-- boolean hasCdTipoUnidadeOrganizacional() 

    /**
     * Method hasDsSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsSituacao()
    {
        return this._has_dsSituacao;
    } //-- boolean hasDsSituacao() 

    /**
     * Method hasNrSeqUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqUnidadeOrganizacional()
    {
        return this._has_nrSeqUnidadeOrganizacional;
    } //-- boolean hasNrSeqUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdOrgaoPagador'.
     * 
     * @param cdOrgaoPagador the value of field 'cdOrgaoPagador'.
     */
    public void setCdOrgaoPagador(int cdOrgaoPagador)
    {
        this._cdOrgaoPagador = cdOrgaoPagador;
        this._has_cdOrgaoPagador = true;
    } //-- void setCdOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @param cdTipoUnidadeOrganizacional the value of field
     * 'cdTipoUnidadeOrganizacional'.
     */
    public void setCdTipoUnidadeOrganizacional(int cdTipoUnidadeOrganizacional)
    {
        this._cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
        this._has_cdTipoUnidadeOrganizacional = true;
    } //-- void setCdTipoUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'dsPessoJuridica'.
     * 
     * @param dsPessoJuridica the value of field 'dsPessoJuridica'.
     */
    public void setDsPessoJuridica(java.lang.String dsPessoJuridica)
    {
        this._dsPessoJuridica = dsPessoJuridica;
    } //-- void setDsPessoJuridica(java.lang.String) 

    /**
     * Sets the value of field 'dsSeqUnidadeOrganizacional'.
     * 
     * @param dsSeqUnidadeOrganizacional the value of field
     * 'dsSeqUnidadeOrganizacional'.
     */
    public void setDsSeqUnidadeOrganizacional(java.lang.String dsSeqUnidadeOrganizacional)
    {
        this._dsSeqUnidadeOrganizacional = dsSeqUnidadeOrganizacional;
    } //-- void setDsSeqUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacao'.
     * 
     * @param dsSituacao the value of field 'dsSituacao'.
     */
    public void setDsSituacao(int dsSituacao)
    {
        this._dsSituacao = dsSituacao;
        this._has_dsSituacao = true;
    } //-- void setDsSituacao(int) 

    /**
     * Sets the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @param dsTipoUnidadeOrganizacional the value of field
     * 'dsTipoUnidadeOrganizacional'.
     */
    public void setDsTipoUnidadeOrganizacional(java.lang.String dsTipoUnidadeOrganizacional)
    {
        this._dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
    } //-- void setDsTipoUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'dsUsuarioResponsavel'.
     * 
     * @param dsUsuarioResponsavel the value of field
     * 'dsUsuarioResponsavel'.
     */
    public void setDsUsuarioResponsavel(java.lang.String dsUsuarioResponsavel)
    {
        this._dsUsuarioResponsavel = dsUsuarioResponsavel;
    } //-- void setDsUsuarioResponsavel(java.lang.String) 

    /**
     * Sets the value of field 'horario'.
     * 
     * @param horario the value of field 'horario'.
     */
    public void setHorario(java.lang.String horario)
    {
        this._horario = horario;
    } //-- void setHorario(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqUnidadeOrganizacional'.
     * 
     * @param nrSeqUnidadeOrganizacional the value of field
     * 'nrSeqUnidadeOrganizacional'.
     */
    public void setNrSeqUnidadeOrganizacional(int nrSeqUnidadeOrganizacional)
    {
        this._nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
        this._has_nrSeqUnidadeOrganizacional = true;
    } //-- void setNrSeqUnidadeOrganizacional(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
