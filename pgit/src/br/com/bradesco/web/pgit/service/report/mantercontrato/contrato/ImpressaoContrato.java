/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato;

import java.io.OutputStream;

import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.ContratoPrestacaoServico;

/**
 * Nome: ImpressaoContrato
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public interface ImpressaoContrato {

    /**
     * Imprimir contrato prestacao servico.
     * 
     * @param contrato
     *            the contrato
     * @param caminhoWeb
     *            the caminho web
     * @param outputStream
     *            the output stream
     */
    void imprimirContratoPrestacaoServico(ContratoPrestacaoServico contrato, String caminhoWeb,
        OutputStream outputStream);
}
