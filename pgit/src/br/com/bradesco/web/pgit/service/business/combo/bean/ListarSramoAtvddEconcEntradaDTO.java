/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarSramoAtvddEconcEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSramoAtvddEconcEntradaDTO {
    
    /** Atributo cdRamoAtividade. */
    private Integer cdRamoAtividade;
    
    /** Atributo cdClassAtividade. */
    private String cdClassAtividade;
    
    /**
     * Get: cdClassAtividade.
     *
     * @return cdClassAtividade
     */
    public String getCdClassAtividade() {
        return cdClassAtividade;
    }
    
    /**
     * Set: cdClassAtividade.
     *
     * @param cdClassAtividade the cd class atividade
     */
    public void setCdClassAtividade(String cdClassAtividade) {
        this.cdClassAtividade = cdClassAtividade;
    }
    
    /**
     * Get: cdRamoAtividade.
     *
     * @return cdRamoAtividade
     */
    public Integer getCdRamoAtividade() {
        return cdRamoAtividade;
    }
    
    /**
     * Set: cdRamoAtividade.
     *
     * @param cdRamoAtividade the cd ramo atividade
     */
    public void setCdRamoAtividade(Integer cdRamoAtividade) {
        this.cdRamoAtividade = cdRamoAtividade;
    }
   
}
