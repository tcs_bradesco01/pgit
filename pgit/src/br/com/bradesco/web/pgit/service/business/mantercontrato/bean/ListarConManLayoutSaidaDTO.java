/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarConManLayoutSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarConManLayoutSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;

	/** Atributo cdTipoLayout. */
	private int cdTipoLayout;

	/** Atributo dsLayout. */
	private String dsLayout;

	/** Atributo dtManutencao. */
	private String dtManutencao;

	/** Atributo hrManutencao. */
	private String hrManutencao;

	/** Atributo cdUsuario. */
	private String cdUsuario;

	/** Atributo dtHoraManutencaoDesc. */
	private String dtHoraManutencaoDesc;

	/** Atributo dtHoraInclusaoDesc. */
	private String dtHoraInclusaoDesc;

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;

	/**
	 * Get: cdTipoLayout.
	 *
	 * @return cdTipoLayout
	 */
	public int getCdTipoLayout() {
		return cdTipoLayout;
	}

	/**
	 * Set: cdTipoLayout.
	 *
	 * @param cdTipoLayout the cd tipo layout
	 */
	public void setCdTipoLayout(int cdTipoLayout) {
		this.cdTipoLayout = cdTipoLayout;
	}

	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsLayout.
	 *
	 * @return dsLayout
	 */
	public String getDsLayout() {
		return dsLayout;
	}

	/**
	 * Set: dsLayout.
	 *
	 * @param dsLayout the ds layout
	 */
	public void setDsLayout(String dsLayout) {
		this.dsLayout = dsLayout;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: dtHoraInclusaoDesc.
	 *
	 * @return dtHoraInclusaoDesc
	 */
	public String getDtHoraInclusaoDesc() {
		return dtHoraInclusaoDesc;
	}

	/**
	 * Set: dtHoraInclusaoDesc.
	 *
	 * @param dtHoraInclusaoDesc the dt hora inclusao desc
	 */
	public void setDtHoraInclusaoDesc(String dtHoraInclusaoDesc) {
		this.dtHoraInclusaoDesc = dtHoraInclusaoDesc;
	}

	/**
	 * Get: dtHoraManutencaoDesc.
	 *
	 * @return dtHoraManutencaoDesc
	 */
	public String getDtHoraManutencaoDesc() {
		return dtHoraManutencaoDesc;
	}

	/**
	 * Set: dtHoraManutencaoDesc.
	 *
	 * @param dtHoraManutencaoDesc the dt hora manutencao desc
	 */
	public void setDtHoraManutencaoDesc(String dtHoraManutencaoDesc) {
		this.dtHoraManutencaoDesc = dtHoraManutencaoDesc;
	}

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}
}
