/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.math.BigDecimal;

/**
 * The Class DetalharPiramideOcorrencias4SaidaDTO.
 */
public class DetalharPiramideOcorrencias4SaidaDTO {

    /** The cd banco credt csignst. */
    private Integer cdBancoCredtCsignst;

    /** The ds banco credt csignst. */
    private String dsBancoCredtCsignst;

    /** The vl repas mes consign. */
    private BigDecimal vlRepasMesConsign;

    /** The vl cart csign. */
    private BigDecimal vlCartCsign;

    /**
     * Set cd banco credt csignst.
     * 
     * @param cdBancoCredtCsignst
     *            the cd banco credt csignst
     */
    public void setCdBancoCredtCsignst(Integer cdBancoCredtCsignst) {
        this.cdBancoCredtCsignst = cdBancoCredtCsignst;
    }

    /**
     * Get cd banco credt csignst.
     * 
     * @return the cd banco credt csignst
     */
    public Integer getCdBancoCredtCsignst() {
        return this.cdBancoCredtCsignst;
    }

    /**
     * Set ds banco credt csignst.
     * 
     * @param dsBancoCredtCsignst
     *            the ds banco credt csignst
     */
    public void setDsBancoCredtCsignst(String dsBancoCredtCsignst) {
        this.dsBancoCredtCsignst = dsBancoCredtCsignst;
    }

    /**
     * Get ds banco credt csignst.
     * 
     * @return the ds banco credt csignst
     */
    public String getDsBancoCredtCsignst() {
        return this.dsBancoCredtCsignst;
    }

    /**
     * Set vl repas mes consign.
     * 
     * @param vlRepasMesConsign
     *            the vl repas mes consign
     */
    public void setVlRepasMesConsign(BigDecimal vlRepasMesConsign) {
        this.vlRepasMesConsign = vlRepasMesConsign;
    }

    /**
     * Get vl repas mes consign.
     * 
     * @return the vl repas mes consign
     */
    public BigDecimal getVlRepasMesConsign() {
        return this.vlRepasMesConsign;
    }

    /**
     * Set vl cart csign.
     * 
     * @param vlCartCsign
     *            the vl cart csign
     */
    public void setVlCartCsign(BigDecimal vlCartCsign) {
        this.vlCartCsign = vlCartCsign;
    }

    /**
     * Get vl cart csign.
     * 
     * @return the vl cart csign
     */
    public BigDecimal getVlCartCsign() {
        return this.vlCartCsign;
    }
}