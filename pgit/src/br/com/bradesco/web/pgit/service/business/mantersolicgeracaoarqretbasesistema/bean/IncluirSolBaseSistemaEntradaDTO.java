/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean;

import java.math.BigDecimal;

/**
 * Nome: IncluirSolBaseSistemaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSolBaseSistemaEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdBaseSistema. */
    private Integer cdBaseSistema;
    
    /** Atributo vlTarifaPadrao. */
    private BigDecimal vlTarifaPadrao;
    
    /** Atributo numPercentualDescTarifa. */
    private BigDecimal numPercentualDescTarifa;
    
    /** Atributo vlrTarifaAtual. */
    private BigDecimal vlrTarifaAtual;
    
    
	/**
	 * Get: cdBaseSistema.
	 *
	 * @return cdBaseSistema
	 */
	public Integer getCdBaseSistema() {
		return cdBaseSistema;
	}
	
	/**
	 * Set: cdBaseSistema.
	 *
	 * @param cdBaseSistema the cd base sistema
	 */
	public void setCdBaseSistema(Integer cdBaseSistema) {
		this.cdBaseSistema = cdBaseSistema;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}
	
	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}
	
	/**
	 * Get: numPercentualDescTarifa.
	 *
	 * @return numPercentualDescTarifa
	 */
	public BigDecimal getNumPercentualDescTarifa() {
		return numPercentualDescTarifa;
	}
	
	/**
	 * Set: numPercentualDescTarifa.
	 *
	 * @param numPercentualDescTarifa the num percentual desc tarifa
	 */
	public void setNumPercentualDescTarifa(BigDecimal numPercentualDescTarifa) {
		this.numPercentualDescTarifa = numPercentualDescTarifa;
	}
	
	/**
	 * Get: vlrTarifaAtual.
	 *
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}
	
	/**
	 * Set: vlrTarifaAtual.
	 *
	 * @param vlrTarifaAtual the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}
}
