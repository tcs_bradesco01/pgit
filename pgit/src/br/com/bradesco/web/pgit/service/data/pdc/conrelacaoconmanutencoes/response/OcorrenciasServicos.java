/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class OcorrenciasServicos.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasServicos implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoRelacionado
     */
    private java.lang.String _dsProdutoServicoRelacionado;

    /**
     * Field _dsAcaoManutencaoServico
     */
    private java.lang.String _dsAcaoManutencaoServico;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasServicos() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasServicos()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'dsAcaoManutencaoServico'.
     * 
     * @return String
     * @return the value of field 'dsAcaoManutencaoServico'.
     */
    public java.lang.String getDsAcaoManutencaoServico()
    {
        return this._dsAcaoManutencaoServico;
    } //-- java.lang.String getDsAcaoManutencaoServico() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsProdutoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoRelacionado'.
     */
    public java.lang.String getDsProdutoServicoRelacionado()
    {
        return this._dsProdutoServicoRelacionado;
    } //-- java.lang.String getDsProdutoServicoRelacionado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsAcaoManutencaoServico'.
     * 
     * @param dsAcaoManutencaoServico the value of field
     * 'dsAcaoManutencaoServico'.
     */
    public void setDsAcaoManutencaoServico(java.lang.String dsAcaoManutencaoServico)
    {
        this._dsAcaoManutencaoServico = dsAcaoManutencaoServico;
    } //-- void setDsAcaoManutencaoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoRelacionado'.
     * 
     * @param dsProdutoServicoRelacionado the value of field
     * 'dsProdutoServicoRelacionado'.
     */
    public void setDsProdutoServicoRelacionado(java.lang.String dsProdutoServicoRelacionado)
    {
        this._dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
    } //-- void setDsProdutoServicoRelacionado(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasServicos
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasServicos unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasServicos) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasServicos.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasServicos unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
