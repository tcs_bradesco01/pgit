/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoFavorecidoSaidaDTO {

	/** Atributo cdTipoFavorecidos. */
	private Integer cdTipoFavorecidos;
	
	/** Atributo dsTipoFavorecidos. */
	private String dsTipoFavorecidos;
	
	/**
	 * Tipo favorecido saida dto.
	 */
	public TipoFavorecidoSaidaDTO() {
	}

	/**
	 * Tipo favorecido saida dto.
	 *
	 * @param cdTipoFavorecidos the cd tipo favorecidos
	 * @param dsTipoFavorecidos the ds tipo favorecidos
	 */
	public TipoFavorecidoSaidaDTO(Integer cdTipoFavorecidos, String dsTipoFavorecidos) {
		super();
		this.cdTipoFavorecidos = cdTipoFavorecidos;
		this.dsTipoFavorecidos = dsTipoFavorecidos;
	}

	/**
	 * Get: cdTipoFavorecidos.
	 *
	 * @return cdTipoFavorecidos
	 */
	public Integer getCdTipoFavorecidos() {
		return cdTipoFavorecidos;
	}

	/**
	 * Set: cdTipoFavorecidos.
	 *
	 * @param cdTipoFavorecidos the cd tipo favorecidos
	 */
	public void setCdTipoFavorecidos(Integer cdTipoFavorecidos) {
		this.cdTipoFavorecidos = cdTipoFavorecidos;
	}

	/**
	 * Get: dsTipoFavorecidos.
	 *
	 * @return dsTipoFavorecidos
	 */
	public String getDsTipoFavorecidos() {
		return dsTipoFavorecidos;
	}

	/**
	 * Set: dsTipoFavorecidos.
	 *
	 * @param dsTipoFavorecidos the ds tipo favorecidos
	 */
	public void setDsTipoFavorecidos(String dsTipoFavorecidos) {
		this.dsTipoFavorecidos = dsTipoFavorecidos;
	}
	
}
