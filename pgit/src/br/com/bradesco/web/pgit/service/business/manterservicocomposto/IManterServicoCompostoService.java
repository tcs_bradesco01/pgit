/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterservicocomposto;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.AlterarServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.AlterarServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.DetalharServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.DetalharServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ExcluirServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ExcluirServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.IncluirServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.IncluirServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ListarServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ListarServicoCompostoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterServicoComposto
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterServicoCompostoService {

	/**
	 * Consultar servico composto.
	 *
	 * @param entrada the entrada
	 * @return the list< listar servico composto saida dt o>
	 */
	List<ListarServicoCompostoSaidaDTO> consultarServicoComposto(ListarServicoCompostoEntradaDTO entrada);
	
	/**
	 * Incluir servico composto.
	 *
	 * @param entrada the entrada
	 * @return the incluir servico composto saida dto
	 */
	IncluirServicoCompostoSaidaDTO incluirServicoComposto(IncluirServicoCompostoEntradaDTO entrada);
	
	/**
	 * Excluir servico composto.
	 *
	 * @param entrada the entrada
	 * @return the excluir servico composto saida dto
	 */
	ExcluirServicoCompostoSaidaDTO excluirServicoComposto(ExcluirServicoCompostoEntradaDTO entrada);
	
	/**
	 * Detalhar servico composto.
	 *
	 * @param entrada the entrada
	 * @return the detalhar servico composto saida dto
	 */
	DetalharServicoCompostoSaidaDTO detalharServicoComposto(DetalharServicoCompostoEntradaDTO entrada);
	
	/**
	 * Alterar servico composto.
	 *
	 * @param entrada the entrada
	 * @return the alterar servico composto saida dto
	 */
	AlterarServicoCompostoSaidaDTO alterarServicoComposto(AlterarServicoCompostoEntradaDTO entrada);
}

