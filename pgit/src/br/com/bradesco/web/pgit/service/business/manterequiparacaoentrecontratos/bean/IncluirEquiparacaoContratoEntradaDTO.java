/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean;

import java.util.List;

/**
 * Nome: IncluirEquiparacaoContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirEquiparacaoContratoEntradaDTO {
	
	/** Atributo maxOcorrencias. */
	private Integer maxOcorrencias;
    
    /** Atributo ocorrencias. */
    private List<IncluirEquiparacaoContratoOcorrenciasDTO> ocorrencias;
	
    /**
     * Get: maxOcorrencias.
     *
     * @return maxOcorrencias
     */
    public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}
	
	/**
	 * Set: maxOcorrencias.
	 *
	 * @param maxOcorrencias the max ocorrencias
	 */
	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}
	
	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<IncluirEquiparacaoContratoOcorrenciasDTO> getOcorrencias() {
		return ocorrencias;
	}
	
	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(List<IncluirEquiparacaoContratoOcorrenciasDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
    
}
