/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarUltimoNumeroRemessaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarUltimoNumeroRemessaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _dsNivelControle
     */
    private java.lang.String _dsNivelControle;

    /**
     * Field _dsTipoControle
     */
    private java.lang.String _dsTipoControle;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdTipoLoteLayout
     */
    private int _cdTipoLoteLayout = 0;

    /**
     * keeps track of state for field: _cdTipoLoteLayout
     */
    private boolean _has_cdTipoLoteLayout;

    /**
     * Field _dsTipoLoteLayout
     */
    private java.lang.String _dsTipoLoteLayout;

    /**
     * Field _nrUltimoNumeroArquivoRemessa
     */
    private long _nrUltimoNumeroArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrUltimoNumeroArquivoRemessa
     */
    private boolean _has_nrUltimoNumeroArquivoRemessa;

    /**
     * Field _nrMaximoArquivoRemessa
     */
    private long _nrMaximoArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrMaximoArquivoRemessa
     */
    private boolean _has_nrMaximoArquivoRemessa;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private java.lang.String _cdCorpoCpfCnpj;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarUltimoNumeroRemessaResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.response.ConsultarUltimoNumeroRemessaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoLoteLayout
     * 
     */
    public void deleteCdTipoLoteLayout()
    {
        this._has_cdTipoLoteLayout= false;
    } //-- void deleteCdTipoLoteLayout() 

    /**
     * Method deleteNrMaximoArquivoRemessa
     * 
     */
    public void deleteNrMaximoArquivoRemessa()
    {
        this._has_nrMaximoArquivoRemessa= false;
    } //-- void deleteNrMaximoArquivoRemessa() 

    /**
     * Method deleteNrUltimoNumeroArquivoRemessa
     * 
     */
    public void deleteNrUltimoNumeroArquivoRemessa()
    {
        this._has_nrUltimoNumeroArquivoRemessa= false;
    } //-- void deleteNrUltimoNumeroArquivoRemessa() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public java.lang.String getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- java.lang.String getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoLoteLayout'.
     * 
     * @return int
     * @return the value of field 'cdTipoLoteLayout'.
     */
    public int getCdTipoLoteLayout()
    {
        return this._cdTipoLoteLayout;
    } //-- int getCdTipoLoteLayout() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsNivelControle'.
     * 
     * @return String
     * @return the value of field 'dsNivelControle'.
     */
    public java.lang.String getDsNivelControle()
    {
        return this._dsNivelControle;
    } //-- java.lang.String getDsNivelControle() 

    /**
     * Returns the value of field 'dsTipoControle'.
     * 
     * @return String
     * @return the value of field 'dsTipoControle'.
     */
    public java.lang.String getDsTipoControle()
    {
        return this._dsTipoControle;
    } //-- java.lang.String getDsTipoControle() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoLoteLayout'.
     * 
     * @return String
     * @return the value of field 'dsTipoLoteLayout'.
     */
    public java.lang.String getDsTipoLoteLayout()
    {
        return this._dsTipoLoteLayout;
    } //-- java.lang.String getDsTipoLoteLayout() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrMaximoArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrMaximoArquivoRemessa'.
     */
    public long getNrMaximoArquivoRemessa()
    {
        return this._nrMaximoArquivoRemessa;
    } //-- long getNrMaximoArquivoRemessa() 

    /**
     * Returns the value of field 'nrUltimoNumeroArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrUltimoNumeroArquivoRemessa'.
     */
    public long getNrUltimoNumeroArquivoRemessa()
    {
        return this._nrUltimoNumeroArquivoRemessa;
    } //-- long getNrUltimoNumeroArquivoRemessa() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoLoteLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLoteLayout()
    {
        return this._has_cdTipoLoteLayout;
    } //-- boolean hasCdTipoLoteLayout() 

    /**
     * Method hasNrMaximoArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMaximoArquivoRemessa()
    {
        return this._has_nrMaximoArquivoRemessa;
    } //-- boolean hasNrMaximoArquivoRemessa() 

    /**
     * Method hasNrUltimoNumeroArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUltimoNumeroArquivoRemessa()
    {
        return this._has_nrUltimoNumeroArquivoRemessa;
    } //-- boolean hasNrUltimoNumeroArquivoRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(java.lang.String cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
    } //-- void setCdCorpoCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoLoteLayout'.
     * 
     * @param cdTipoLoteLayout the value of field 'cdTipoLoteLayout'
     */
    public void setCdTipoLoteLayout(int cdTipoLoteLayout)
    {
        this._cdTipoLoteLayout = cdTipoLoteLayout;
        this._has_cdTipoLoteLayout = true;
    } //-- void setCdTipoLoteLayout(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsNivelControle'.
     * 
     * @param dsNivelControle the value of field 'dsNivelControle'.
     */
    public void setDsNivelControle(java.lang.String dsNivelControle)
    {
        this._dsNivelControle = dsNivelControle;
    } //-- void setDsNivelControle(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoControle'.
     * 
     * @param dsTipoControle the value of field 'dsTipoControle'.
     */
    public void setDsTipoControle(java.lang.String dsTipoControle)
    {
        this._dsTipoControle = dsTipoControle;
    } //-- void setDsTipoControle(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLoteLayout'.
     * 
     * @param dsTipoLoteLayout the value of field 'dsTipoLoteLayout'
     */
    public void setDsTipoLoteLayout(java.lang.String dsTipoLoteLayout)
    {
        this._dsTipoLoteLayout = dsTipoLoteLayout;
    } //-- void setDsTipoLoteLayout(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrMaximoArquivoRemessa'.
     * 
     * @param nrMaximoArquivoRemessa the value of field
     * 'nrMaximoArquivoRemessa'.
     */
    public void setNrMaximoArquivoRemessa(long nrMaximoArquivoRemessa)
    {
        this._nrMaximoArquivoRemessa = nrMaximoArquivoRemessa;
        this._has_nrMaximoArquivoRemessa = true;
    } //-- void setNrMaximoArquivoRemessa(long) 

    /**
     * Sets the value of field 'nrUltimoNumeroArquivoRemessa'.
     * 
     * @param nrUltimoNumeroArquivoRemessa the value of field
     * 'nrUltimoNumeroArquivoRemessa'.
     */
    public void setNrUltimoNumeroArquivoRemessa(long nrUltimoNumeroArquivoRemessa)
    {
        this._nrUltimoNumeroArquivoRemessa = nrUltimoNumeroArquivoRemessa;
        this._has_nrUltimoNumeroArquivoRemessa = true;
    } //-- void setNrUltimoNumeroArquivoRemessa(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarUltimoNumeroRemessaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.response.ConsultarUltimoNumeroRemessaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.response.ConsultarUltimoNumeroRemessaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.response.ConsultarUltimoNumeroRemessaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.response.ConsultarUltimoNumeroRemessaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
