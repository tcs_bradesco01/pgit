/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoManutencaoContrato
     */
    private int _cdTipoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoManutencaoContrato
     */
    private boolean _has_cdTipoManutencaoContrato;

    /**
     * Field _dsTipoManutencaoContrato
     */
    private java.lang.String _dsTipoManutencaoContrato;

    /**
     * Field _cdManutencao
     */
    private long _cdManutencao = 0;

    /**
     * keeps track of state for field: _cdManutencao
     */
    private boolean _has_cdManutencao;

    /**
     * Field _dsManutencaoContrato
     */
    private java.lang.String _dsManutencaoContrato;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdSituacaoManutencaoContrato
     */
    private int _cdSituacaoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoManutencaoContrato
     */
    private boolean _has_cdSituacaoManutencaoContrato;

    /**
     * Field _dsSituacaoManutencaoContrato
     */
    private java.lang.String _dsSituacaoManutencaoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdManutencao
     * 
     */
    public void deleteCdManutencao()
    {
        this._has_cdManutencao= false;
    } //-- void deleteCdManutencao() 

    /**
     * Method deleteCdSituacaoManutencaoContrato
     * 
     */
    public void deleteCdSituacaoManutencaoContrato()
    {
        this._has_cdSituacaoManutencaoContrato= false;
    } //-- void deleteCdSituacaoManutencaoContrato() 

    /**
     * Method deleteCdTipoManutencaoContrato
     * 
     */
    public void deleteCdTipoManutencaoContrato()
    {
        this._has_cdTipoManutencaoContrato= false;
    } //-- void deleteCdTipoManutencaoContrato() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdManutencao'.
     * 
     * @return long
     * @return the value of field 'cdManutencao'.
     */
    public long getCdManutencao()
    {
        return this._cdManutencao;
    } //-- long getCdManutencao() 

    /**
     * Returns the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoManutencaoContrato'.
     */
    public int getCdSituacaoManutencaoContrato()
    {
        return this._cdSituacaoManutencaoContrato;
    } //-- int getCdSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'cdTipoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencaoContrato'.
     */
    public int getCdTipoManutencaoContrato()
    {
        return this._cdTipoManutencaoContrato;
    } //-- int getCdTipoManutencaoContrato() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsManutencaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsManutencaoContrato'.
     */
    public java.lang.String getDsManutencaoContrato()
    {
        return this._dsManutencaoContrato;
    } //-- java.lang.String getDsManutencaoContrato() 

    /**
     * Returns the value of field 'dsSituacaoManutencaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoManutencaoContrato'.
     */
    public java.lang.String getDsSituacaoManutencaoContrato()
    {
        return this._dsSituacaoManutencaoContrato;
    } //-- java.lang.String getDsSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'dsTipoManutencaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencaoContrato'.
     */
    public java.lang.String getDsTipoManutencaoContrato()
    {
        return this._dsTipoManutencaoContrato;
    } //-- java.lang.String getDsTipoManutencaoContrato() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdManutencao()
    {
        return this._has_cdManutencao;
    } //-- boolean hasCdManutencao() 

    /**
     * Method hasCdSituacaoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoManutencaoContrato()
    {
        return this._has_cdSituacaoManutencaoContrato;
    } //-- boolean hasCdSituacaoManutencaoContrato() 

    /**
     * Method hasCdTipoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencaoContrato()
    {
        return this._has_cdTipoManutencaoContrato;
    } //-- boolean hasCdTipoManutencaoContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdManutencao'.
     * 
     * @param cdManutencao the value of field 'cdManutencao'.
     */
    public void setCdManutencao(long cdManutencao)
    {
        this._cdManutencao = cdManutencao;
        this._has_cdManutencao = true;
    } //-- void setCdManutencao(long) 

    /**
     * Sets the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @param cdSituacaoManutencaoContrato the value of field
     * 'cdSituacaoManutencaoContrato'.
     */
    public void setCdSituacaoManutencaoContrato(int cdSituacaoManutencaoContrato)
    {
        this._cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
        this._has_cdSituacaoManutencaoContrato = true;
    } //-- void setCdSituacaoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoManutencaoContrato'.
     * 
     * @param cdTipoManutencaoContrato the value of field
     * 'cdTipoManutencaoContrato'.
     */
    public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato)
    {
        this._cdTipoManutencaoContrato = cdTipoManutencaoContrato;
        this._has_cdTipoManutencaoContrato = true;
    } //-- void setCdTipoManutencaoContrato(int) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsManutencaoContrato'.
     * 
     * @param dsManutencaoContrato the value of field
     * 'dsManutencaoContrato'.
     */
    public void setDsManutencaoContrato(java.lang.String dsManutencaoContrato)
    {
        this._dsManutencaoContrato = dsManutencaoContrato;
    } //-- void setDsManutencaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoManutencaoContrato'.
     * 
     * @param dsSituacaoManutencaoContrato the value of field
     * 'dsSituacaoManutencaoContrato'.
     */
    public void setDsSituacaoManutencaoContrato(java.lang.String dsSituacaoManutencaoContrato)
    {
        this._dsSituacaoManutencaoContrato = dsSituacaoManutencaoContrato;
    } //-- void setDsSituacaoManutencaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencaoContrato'.
     * 
     * @param dsTipoManutencaoContrato the value of field
     * 'dsTipoManutencaoContrato'.
     */
    public void setDsTipoManutencaoContrato(java.lang.String dsTipoManutencaoContrato)
    {
        this._dsTipoManutencaoContrato = dsTipoManutencaoContrato;
    } //-- void setDsTipoManutencaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
