/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdFilialCnpj
     */
    private int _cdFilialCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCnpj
     */
    private boolean _has_cdFilialCnpj;

    /**
     * Field _cdControleCnpj
     */
    private int _cdControleCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCnpj
     */
    private boolean _has_cdControleCnpj;

    /**
     * Field _dsNomeRazao
     */
    private java.lang.String _dsNomeRazao;

    /**
     * Field _cdPerfilTrocaArquivo
     */
    private long _cdPerfilTrocaArquivo = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArquivo
     */
    private boolean _has_cdPerfilTrocaArquivo;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCnpj
     * 
     */
    public void deleteCdControleCnpj()
    {
        this._has_cdControleCnpj= false;
    } //-- void deleteCdControleCnpj() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdFilialCnpj
     * 
     */
    public void deleteCdFilialCnpj()
    {
        this._has_cdFilialCnpj= false;
    } //-- void deleteCdFilialCnpj() 

    /**
     * Method deleteCdPerfilTrocaArquivo
     * 
     */
    public void deleteCdPerfilTrocaArquivo()
    {
        this._has_cdPerfilTrocaArquivo= false;
    } //-- void deleteCdPerfilTrocaArquivo() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdControleCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpj'.
     */
    public int getCdControleCnpj()
    {
        return this._cdControleCnpj;
    } //-- int getCdControleCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpj'.
     */
    public int getCdFilialCnpj()
    {
        return this._cdFilialCnpj;
    } //-- int getCdFilialCnpj() 

    /**
     * Returns the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArquivo'.
     */
    public long getCdPerfilTrocaArquivo()
    {
        return this._cdPerfilTrocaArquivo;
    } //-- long getCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'dsNomeRazao'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazao'.
     */
    public java.lang.String getDsNomeRazao()
    {
        return this._dsNomeRazao;
    } //-- java.lang.String getDsNomeRazao() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Method hasCdControleCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpj()
    {
        return this._has_cdControleCnpj;
    } //-- boolean hasCdControleCnpj() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpj()
    {
        return this._has_cdFilialCnpj;
    } //-- boolean hasCdFilialCnpj() 

    /**
     * Method hasCdPerfilTrocaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArquivo()
    {
        return this._has_cdPerfilTrocaArquivo;
    } //-- boolean hasCdPerfilTrocaArquivo() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCnpj'.
     * 
     * @param cdControleCnpj the value of field 'cdControleCnpj'.
     */
    public void setCdControleCnpj(int cdControleCnpj)
    {
        this._cdControleCnpj = cdControleCnpj;
        this._has_cdControleCnpj = true;
    } //-- void setCdControleCnpj(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdFilialCnpj'.
     * 
     * @param cdFilialCnpj the value of field 'cdFilialCnpj'.
     */
    public void setCdFilialCnpj(int cdFilialCnpj)
    {
        this._cdFilialCnpj = cdFilialCnpj;
        this._has_cdFilialCnpj = true;
    } //-- void setCdFilialCnpj(int) 

    /**
     * Sets the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @param cdPerfilTrocaArquivo the value of field
     * 'cdPerfilTrocaArquivo'.
     */
    public void setCdPerfilTrocaArquivo(long cdPerfilTrocaArquivo)
    {
        this._cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
        this._has_cdPerfilTrocaArquivo = true;
    } //-- void setCdPerfilTrocaArquivo(long) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'dsNomeRazao'.
     * 
     * @param dsNomeRazao the value of field 'dsNomeRazao'.
     */
    public void setDsNomeRazao(java.lang.String dsNomeRazao)
    {
        this._dsNomeRazao = dsNomeRazao;
    } //-- void setDsNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
