/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.AlterarOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.AlterarOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ConsultarTarifaCatalogoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ConsultarTarifaCatalogoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ExcluirOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ExcluirOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.IncluirOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.IncluirOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarHistoricoOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoSaidaDTO;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterOperacoesServRelacionado
 * </p>.
 *
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterOperacoesServRelacionadoService {

    /**
     * Listar operacao servico pagto integrado.
     *
     * @param entrada the entrada
     * @return the list< listar operacao servico pagto integrado saida dt o>
     */
    List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> listarOperacaoServicoPagtoIntegrado(ListarOperacaoServicoPagtoIntegradoEntradaDTO entrada);
    
    /**
     * Detalhar operacao servico pagto integrado.
     *
     * @param entrada the entrada
     * @return the detalhar operacao servico pagto integrado saida dto
     */
    DetalharOperacaoServicoPagtoIntegradoSaidaDTO detalharOperacaoServicoPagtoIntegrado(DetalharOperacaoServicoPagtoIntegradoEntradaDTO entrada);
    
    /**
     * Incluir operacao servico pagto integrado.
     *
     * @param entrada the entrada
     * @return the incluir operacao servico pagto integrado saida dto
     */
    IncluirOperacaoServicoPagtoIntegradoSaidaDTO incluirOperacaoServicoPagtoIntegrado(IncluirOperacaoServicoPagtoIntegradoEntradaDTO entrada);
    
    /**
     * Excluir operacao servico pagto integrado.
     *
     * @param entrada the entrada
     * @return the excluir operacao servico pagto integrado saida dto
     */
    ExcluirOperacaoServicoPagtoIntegradoSaidaDTO excluirOperacaoServicoPagtoIntegrado(ExcluirOperacaoServicoPagtoIntegradoEntradaDTO entrada);
    
    /**
     * Nome: consultarTarifaCatalogo.
     *
     * @param entrada the entrada
     * @return the consultar tarifa catalogo saida dto
     */
    ConsultarTarifaCatalogoSaidaDTO consultarTarifaCatalogo(ConsultarTarifaCatalogoEntradaDTO entrada);
    
    /**
     * Nome: detalharHistoricoOperacaoServicoPagtoIntegrado.
     *
     * @param entrada the entrada
     * @return the detalhar historico operacao servico pagto integrado saida dto
     */
    DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO detalharHistoricoOperacaoServicoPagtoIntegrado(DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO entrada);

    /**
     * Nome: listarHistoricoOperacaoServicoPagtoIntegrado.
     *
     * @param entrada the entrada
     * @return the listar historico operacao servico pagto integrado saida dto
     */
    ListarHistoricoOperacaoServicoPagtoIntegradoSaidaDTO listarHistoricoOperacaoServicoPagtoIntegrado(ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO entrada);
    
    /**
     * Nome: alterarOperacaoServicoPagtoIntegrado.
     *
     * @param entrada the entrada
     * @return the alterar operacao servico pagto integrado saida dto
     */
    AlterarOperacaoServicoPagtoIntegradoSaidaDTO alterarOperacaoServicoPagtoIntegrado(AlterarOperacaoServicoPagtoIntegradoEntradaDTO entrada);
    
    /**
     * Listar servico relacionado dto.
     *
     * @param entradaDto the entrada dto
     * @return the listar servico relacionado saida dto
     */
    ListarServicoRelacionadoSaidaDTO listarServicoRelacionadoDTO(ListarServicoRelacionadoEntradaDTO entradaDto);
}

