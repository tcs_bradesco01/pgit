/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean;

/**
 * Nome: ListarTipoSistemaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoSistemaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo qtdeRegistros. */
	private int qtdeRegistros;
	
	/** Atributo cdBaseSistema. */
	private int cdBaseSistema;
	
	/** Atributo dsBaseSistema. */
	private String dsBaseSistema;
	
	
	/**
	 * Get: cdBaseSistema.
	 *
	 * @return cdBaseSistema
	 */
	public int getCdBaseSistema() {
		return cdBaseSistema;
	}
	
	/**
	 * Set: cdBaseSistema.
	 *
	 * @param cdBaseSistema the cd base sistema
	 */
	public void setCdBaseSistema(int cdBaseSistema) {
		this.cdBaseSistema = cdBaseSistema;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsBaseSistema.
	 *
	 * @return dsBaseSistema
	 */
	public String getDsBaseSistema() {
		return dsBaseSistema;
	}
	
	/**
	 * Set: dsBaseSistema.
	 *
	 * @param dsBaseSistema the ds base sistema
	 */
	public void setDsBaseSistema(String dsBaseSistema) {
		this.dsBaseSistema = dsBaseSistema;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: qtdeRegistros.
	 *
	 * @return qtdeRegistros
	 */
	public int getQtdeRegistros() {
		return qtdeRegistros;
	}
	
	/**
	 * Set: qtdeRegistros.
	 *
	 * @param qtdeRegistros the qtde registros
	 */
	public void setQtdeRegistros(int qtdeRegistros) {
		this.qtdeRegistros = qtdeRegistros;
	}
	
	

}
