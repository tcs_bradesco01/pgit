/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdOperacaoProduto
     */
    private int _cdOperacaoProduto = 0;

    /**
     * keeps track of state for field: _cdOperacaoProduto
     */
    private boolean _has_cdOperacaoProduto;

    /**
     * Field _dsOperacaoProduto
     */
    private java.lang.String _dsOperacaoProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOperacaoProduto
     * 
     */
    public void deleteCdOperacaoProduto()
    {
        this._has_cdOperacaoProduto= false;
    } //-- void deleteCdOperacaoProduto() 

    /**
     * Returns the value of field 'cdOperacaoProduto'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProduto'.
     */
    public int getCdOperacaoProduto()
    {
        return this._cdOperacaoProduto;
    } //-- int getCdOperacaoProduto() 

    /**
     * Returns the value of field 'dsOperacaoProduto'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProduto'.
     */
    public java.lang.String getDsOperacaoProduto()
    {
        return this._dsOperacaoProduto;
    } //-- java.lang.String getDsOperacaoProduto() 

    /**
     * Method hasCdOperacaoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProduto()
    {
        return this._has_cdOperacaoProduto;
    } //-- boolean hasCdOperacaoProduto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOperacaoProduto'.
     * 
     * @param cdOperacaoProduto the value of field
     * 'cdOperacaoProduto'.
     */
    public void setCdOperacaoProduto(int cdOperacaoProduto)
    {
        this._cdOperacaoProduto = cdOperacaoProduto;
        this._has_cdOperacaoProduto = true;
    } //-- void setCdOperacaoProduto(int) 

    /**
     * Sets the value of field 'dsOperacaoProduto'.
     * 
     * @param dsOperacaoProduto the value of field
     * 'dsOperacaoProduto'.
     */
    public void setDsOperacaoProduto(java.lang.String dsOperacaoProduto)
    {
        this._dsOperacaoProduto = dsOperacaoProduto;
    } //-- void setDsOperacaoProduto(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
