/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.io.Serializable;

/**
 * Nome: EmpresaSegmentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmpresaSegmentoEntradaDTO implements Serializable {
	
	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -2995237772025257574L;
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;

    /** Atributo cdSituacaoVinculacaoConta. */
    private Integer cdSituacaoVinculacaoConta;
    
    /**
     * Empresa segmento entrada dto.
     */
    public EmpresaSegmentoEntradaDTO() {
    	
    }
    
    /**
     * Empresa segmento entrada dto.
     *
     * @param numeroOcorrencias the numero ocorrencias
     * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
     */
    public EmpresaSegmentoEntradaDTO(Integer numeroOcorrencias,Integer cdSituacaoVinculacaoConta) {
		super();
		this.numeroOcorrencias = numeroOcorrencias;
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}

	/**
	 * Get: cdSituacaoVinculacaoConta.
	 *
	 * @return cdSituacaoVinculacaoConta
	 */
	public Integer getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}

	/**
	 * Set: cdSituacaoVinculacaoConta.
	 *
	 * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(Integer cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}

	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
    
}
