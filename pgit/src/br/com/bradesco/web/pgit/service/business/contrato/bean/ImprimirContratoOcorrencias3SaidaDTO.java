/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

/**
 * Nome: ImprimirContratoOcorrencias3SaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoOcorrencias3SaidaDTO {

    /** The ds produto salario funcionarios. */
    private String dsProdutoSalarioFuncionarios = null;

    /** The ds servico salario funcional. */
    private String dsServicoSalarioFuncional = null;

    /**
     * Instancia um novo ImprimirContratoOcorrencias3SaidaDTO.
     * 
     * @see
     */
    public ImprimirContratoOcorrencias3SaidaDTO() {
        super();
    }

    /**
     * Sets the ds produto salario funcionarios.
     *
     * @param dsProdutoSalarioFuncionarios the ds produto salario funcionarios
     */
    public void setDsProdutoSalarioFuncionarios(String dsProdutoSalarioFuncionarios) {
        this.dsProdutoSalarioFuncionarios = dsProdutoSalarioFuncionarios;
    }

    /**
     * Gets the ds produto salario funcionarios.
     *
     * @return the ds produto salario funcionarios
     */
    public String getDsProdutoSalarioFuncionarios() {
        return this.dsProdutoSalarioFuncionarios;
    }

    /**
     * Sets the ds servico salario funcional.
     *
     * @param dsServicoSalarioFuncional the ds servico salario funcional
     */
    public void setDsServicoSalarioFuncional(String dsServicoSalarioFuncional) {
        this.dsServicoSalarioFuncional = dsServicoSalarioFuncional;
    }

    /**
     * Gets the ds servico salario funcional.
     *
     * @return the ds servico salario funcional
     */
    public String getDsServicoSalarioFuncional() {
        return this.dsServicoSalarioFuncional;
    }
}