/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.ConsultarPagamentosConsolidadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.ConsultarPagamentosConsolidadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ConsultarPagamentosConsolidado
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConsultarPagamentosConsolidadoService {

    /**
     * Consultar pagamentos consolidados.
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the list< consultar pagamentos consolidados saida dt o>
     */
    List<ConsultarPagamentosConsolidadosSaidaDTO> consultarPagamentosConsolidados(
        ConsultarPagamentosConsolidadosEntradaDTO entradaDTO);

    /**
     * Detalhar pagamentos consolidados.
     * 
     * @param entradaDTO
     *            the entrada dto
     * @return the detalhar pagamentos consolidados saida dto
     */
    DetalharPagamentosConsolidadosSaidaDTO detalharPagamentosConsolidados(
        DetalharPagamentosConsolidadosEntradaDTO entradaDTO);
    
    /**
     * Nome: consultarImprimirPgtoConsolidado
     * 
     * @param entradaDTO
     */
    void consultarImprimirPgtoConsolidado(DetalharPagamentosConsolidadosEntradaDTO entrada);
    
    /**
     * Nome: imprimirPagamentosConsolidados
     * 
     * @param entradaDTO
     * @return
     */
    DetalharPagamentosConsolidadosSaidaDTO imprimirPagamentosConsolidados(DetalharPagamentosConsolidadosEntradaDTO entrada);

}
