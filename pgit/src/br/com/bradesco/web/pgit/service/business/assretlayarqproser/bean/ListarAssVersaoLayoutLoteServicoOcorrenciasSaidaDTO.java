package br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean;


/**
 * Arquivo criado em 09/02/15.
 */
public class ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO {

    private Integer cdTipoLayoutArquivo;

    private String dsTipoLayoutArquivo;

    private Integer nrVersaoLayoutArquivo;

    private Integer cdTipoLoteLayout;

    private String dsTipoLoteLayout;

    private Integer nrVersaoLoteLayout;

    private Integer cdTipoServicoCnab;

    private String dsTipoServicoCnab;

    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
        this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    public Integer getCdTipoLayoutArquivo() {
        return this.cdTipoLayoutArquivo;
    }

    public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
        this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    }

    public String getDsTipoLayoutArquivo() {
        return this.dsTipoLayoutArquivo;
    }

    public void setNrVersaoLayoutArquivo(Integer nrVersaoLayoutArquivo) {
        this.nrVersaoLayoutArquivo = nrVersaoLayoutArquivo;
    }

    public Integer getNrVersaoLayoutArquivo() {
        return this.nrVersaoLayoutArquivo;
    }

    public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
        this.cdTipoLoteLayout = cdTipoLoteLayout;
    }

    public Integer getCdTipoLoteLayout() {
        return this.cdTipoLoteLayout;
    }

    public void setDsTipoLoteLayout(String dsTipoLoteLayout) {
        this.dsTipoLoteLayout = dsTipoLoteLayout;
    }

    public String getDsTipoLoteLayout() {
        return this.dsTipoLoteLayout;
    }

    public void setNrVersaoLoteLayout(Integer nrVersaoLoteLayout) {
        this.nrVersaoLoteLayout = nrVersaoLoteLayout;
    }

    public Integer getNrVersaoLoteLayout() {
        return this.nrVersaoLoteLayout;
    }

    public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
        this.cdTipoServicoCnab = cdTipoServicoCnab;
    }

    public Integer getCdTipoServicoCnab() {
        return this.cdTipoServicoCnab;
    }

    public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
        this.dsTipoServicoCnab = dsTipoServicoCnab;
    }

    public String getDsTipoServicoCnab() {
        return this.dsTipoServicoCnab;
    }
}