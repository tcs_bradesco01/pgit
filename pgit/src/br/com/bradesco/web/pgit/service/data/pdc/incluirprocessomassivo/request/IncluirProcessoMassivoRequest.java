/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirProcessoMassivoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirProcessoMassivoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdProcessoSistema
     */
    private int _cdProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdProcessoSistema
     */
    private boolean _has_cdProcessoSistema;

    /**
     * Field _cdTipoProcessoSistema
     */
    private int _cdTipoProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdTipoProcessoSistema
     */
    private boolean _has_cdTipoProcessoSistema;

    /**
     * Field _cdPeriodicidade
     */
    private int _cdPeriodicidade = 0;

    /**
     * keeps track of state for field: _cdPeriodicidade
     */
    private boolean _has_cdPeriodicidade;

    /**
     * Field _dsProcessoSistema
     */
    private java.lang.String _dsProcessoSistema;

    /**
     * Field _cdNetProcessamentoPagamento
     */
    private java.lang.String _cdNetProcessamentoPagamento;

    /**
     * Field _cdJobProcessamentoPagamento
     */
    private java.lang.String _cdJobProcessamentoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirProcessoMassivoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.request.IncluirProcessoMassivoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPeriodicidade
     * 
     */
    public void deleteCdPeriodicidade()
    {
        this._has_cdPeriodicidade= false;
    } //-- void deleteCdPeriodicidade() 

    /**
     * Method deleteCdProcessoSistema
     * 
     */
    public void deleteCdProcessoSistema()
    {
        this._has_cdProcessoSistema= false;
    } //-- void deleteCdProcessoSistema() 

    /**
     * Method deleteCdTipoProcessoSistema
     * 
     */
    public void deleteCdTipoProcessoSistema()
    {
        this._has_cdTipoProcessoSistema= false;
    } //-- void deleteCdTipoProcessoSistema() 

    /**
     * Returns the value of field 'cdJobProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdJobProcessamentoPagamento'.
     */
    public java.lang.String getCdJobProcessamentoPagamento()
    {
        return this._cdJobProcessamentoPagamento;
    } //-- java.lang.String getCdJobProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdNetProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdNetProcessamentoPagamento'.
     */
    public java.lang.String getCdNetProcessamentoPagamento()
    {
        return this._cdNetProcessamentoPagamento;
    } //-- java.lang.String getCdNetProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdPeriodicidade'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidade'.
     */
    public int getCdPeriodicidade()
    {
        return this._cdPeriodicidade;
    } //-- int getCdPeriodicidade() 

    /**
     * Returns the value of field 'cdProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcessoSistema'.
     */
    public int getCdProcessoSistema()
    {
        return this._cdProcessoSistema;
    } //-- int getCdProcessoSistema() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdTipoProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcessoSistema'.
     */
    public int getCdTipoProcessoSistema()
    {
        return this._cdTipoProcessoSistema;
    } //-- int getCdTipoProcessoSistema() 

    /**
     * Returns the value of field 'dsProcessoSistema'.
     * 
     * @return String
     * @return the value of field 'dsProcessoSistema'.
     */
    public java.lang.String getDsProcessoSistema()
    {
        return this._dsProcessoSistema;
    } //-- java.lang.String getDsProcessoSistema() 

    /**
     * Method hasCdPeriodicidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidade()
    {
        return this._has_cdPeriodicidade;
    } //-- boolean hasCdPeriodicidade() 

    /**
     * Method hasCdProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcessoSistema()
    {
        return this._has_cdProcessoSistema;
    } //-- boolean hasCdProcessoSistema() 

    /**
     * Method hasCdTipoProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcessoSistema()
    {
        return this._has_cdTipoProcessoSistema;
    } //-- boolean hasCdTipoProcessoSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdJobProcessamentoPagamento'.
     * 
     * @param cdJobProcessamentoPagamento the value of field
     * 'cdJobProcessamentoPagamento'.
     */
    public void setCdJobProcessamentoPagamento(java.lang.String cdJobProcessamentoPagamento)
    {
        this._cdJobProcessamentoPagamento = cdJobProcessamentoPagamento;
    } //-- void setCdJobProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdNetProcessamentoPagamento'.
     * 
     * @param cdNetProcessamentoPagamento the value of field
     * 'cdNetProcessamentoPagamento'.
     */
    public void setCdNetProcessamentoPagamento(java.lang.String cdNetProcessamentoPagamento)
    {
        this._cdNetProcessamentoPagamento = cdNetProcessamentoPagamento;
    } //-- void setCdNetProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdPeriodicidade'.
     * 
     * @param cdPeriodicidade the value of field 'cdPeriodicidade'.
     */
    public void setCdPeriodicidade(int cdPeriodicidade)
    {
        this._cdPeriodicidade = cdPeriodicidade;
        this._has_cdPeriodicidade = true;
    } //-- void setCdPeriodicidade(int) 

    /**
     * Sets the value of field 'cdProcessoSistema'.
     * 
     * @param cdProcessoSistema the value of field
     * 'cdProcessoSistema'.
     */
    public void setCdProcessoSistema(int cdProcessoSistema)
    {
        this._cdProcessoSistema = cdProcessoSistema;
        this._has_cdProcessoSistema = true;
    } //-- void setCdProcessoSistema(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoProcessoSistema'.
     * 
     * @param cdTipoProcessoSistema the value of field
     * 'cdTipoProcessoSistema'.
     */
    public void setCdTipoProcessoSistema(int cdTipoProcessoSistema)
    {
        this._cdTipoProcessoSistema = cdTipoProcessoSistema;
        this._has_cdTipoProcessoSistema = true;
    } //-- void setCdTipoProcessoSistema(int) 

    /**
     * Sets the value of field 'dsProcessoSistema'.
     * 
     * @param dsProcessoSistema the value of field
     * 'dsProcessoSistema'.
     */
    public void setDsProcessoSistema(java.lang.String dsProcessoSistema)
    {
        this._dsProcessoSistema = dsProcessoSistema;
    } //-- void setDsProcessoSistema(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirProcessoMassivoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.request.IncluirProcessoMassivoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.request.IncluirProcessoMassivoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.request.IncluirProcessoMassivoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.request.IncluirProcessoMassivoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
