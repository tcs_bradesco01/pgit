/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias11.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias11 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsCretipoServico
     */
    private java.lang.String _dsCretipoServico;

    /**
     * Field _dsCreTipoConsultaSaldo
     */
    private java.lang.String _dsCreTipoConsultaSaldo;

    /**
     * Field _dsCreTipoProcessamento
     */
    private java.lang.String _dsCreTipoProcessamento;

    /**
     * Field _dsCreTipoTratamentoFeriado
     */
    private java.lang.String _dsCreTipoTratamentoFeriado;

    /**
     * Field _dsCreTipoRejeicaoEfetivacao
     */
    private java.lang.String _dsCreTipoRejeicaoEfetivacao;

    /**
     * Field _dsCreTipoRejeicaoAgendada
     */
    private java.lang.String _dsCreTipoRejeicaoAgendada;

    /**
     * Field _cdCreIndicadorPercentualMaximo
     */
    private java.lang.String _cdCreIndicadorPercentualMaximo;

    /**
     * Field _pcCreMaximoInconsistente
     */
    private int _pcCreMaximoInconsistente = 0;

    /**
     * keeps track of state for field: _pcCreMaximoInconsistente
     */
    private boolean _has_pcCreMaximoInconsistente;

    /**
     * Field _cdCreIndicadorQuantidadeMaxima
     */
    private java.lang.String _cdCreIndicadorQuantidadeMaxima;

    /**
     * Field _qtCreMaximaInconsistencia
     */
    private int _qtCreMaximaInconsistencia = 0;

    /**
     * keeps track of state for field: _qtCreMaximaInconsistencia
     */
    private boolean _has_qtCreMaximaInconsistencia;

    /**
     * Field _cdCreIndicadorValorMaximo
     */
    private java.lang.String _cdCreIndicadorValorMaximo;

    /**
     * Field _vlCreMaximoFavorecidoNao
     */
    private java.math.BigDecimal _vlCreMaximoFavorecidoNao = new java.math.BigDecimal("0");

    /**
     * Field _dsCrePrioridadeDebito
     */
    private java.lang.String _dsCrePrioridadeDebito;

    /**
     * Field _cdCreIndicadorValorDia
     */
    private java.lang.String _cdCreIndicadorValorDia;

    /**
     * Field _vlCreLimiteDiario
     */
    private java.math.BigDecimal _vlCreLimiteDiario = new java.math.BigDecimal("0");

    /**
     * Field _cdCreIndicadorValorIndividual
     */
    private java.lang.String _cdCreIndicadorValorIndividual;

    /**
     * Field _vlCreLimiteIndividual
     */
    private java.math.BigDecimal _vlCreLimiteIndividual = new java.math.BigDecimal("0");

    /**
     * Field _cdCreIndicadorQuantidadeFloating
     */
    private java.lang.String _cdCreIndicadorQuantidadeFloating;

    /**
     * Field _qtCreDiaFloating
     */
    private int _qtCreDiaFloating = 0;

    /**
     * keeps track of state for field: _qtCreDiaFloating
     */
    private boolean _has_qtCreDiaFloating;

    /**
     * Field _cdCreIndicadorCadastroFavorecido
     */
    private java.lang.String _cdCreIndicadorCadastroFavorecido;

    /**
     * Field _cdCreUtilizacaoCadastroFavorecido
     */
    private java.lang.String _cdCreUtilizacaoCadastroFavorecido;

    /**
     * Field _cdCreIndicadorQuantidadeRepique
     */
    private java.lang.String _cdCreIndicadorQuantidadeRepique;

    /**
     * Field _qtCreDiaRepique
     */
    private int _qtCreDiaRepique = 0;

    /**
     * keeps track of state for field: _qtCreDiaRepique
     */
    private boolean _has_qtCreDiaRepique;

    /**
     * Field _dsCreTipoConsistenciaFavorecido
     */
    private java.lang.String _dsCreTipoConsistenciaFavorecido;

    /**
     * Field _cdCreCferiLoc
     */
    private java.lang.String _cdCreCferiLoc;

    /**
     * Field _cdCreContratoTransf
     */
    private java.lang.String _cdCreContratoTransf;

    /**
     * Field _cdLctoPgmd
     */
    private java.lang.String _cdLctoPgmd;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias11() 
     {
        super();
        setVlCreMaximoFavorecidoNao(new java.math.BigDecimal("0"));
        setVlCreLimiteDiario(new java.math.BigDecimal("0"));
        setVlCreLimiteIndividual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deletePcCreMaximoInconsistente
     * 
     */
    public void deletePcCreMaximoInconsistente()
    {
        this._has_pcCreMaximoInconsistente= false;
    } //-- void deletePcCreMaximoInconsistente() 

    /**
     * Method deleteQtCreDiaFloating
     * 
     */
    public void deleteQtCreDiaFloating()
    {
        this._has_qtCreDiaFloating= false;
    } //-- void deleteQtCreDiaFloating() 

    /**
     * Method deleteQtCreDiaRepique
     * 
     */
    public void deleteQtCreDiaRepique()
    {
        this._has_qtCreDiaRepique= false;
    } //-- void deleteQtCreDiaRepique() 

    /**
     * Method deleteQtCreMaximaInconsistencia
     * 
     */
    public void deleteQtCreMaximaInconsistencia()
    {
        this._has_qtCreMaximaInconsistencia= false;
    } //-- void deleteQtCreMaximaInconsistencia() 

    /**
     * Returns the value of field 'cdCreCferiLoc'.
     * 
     * @return String
     * @return the value of field 'cdCreCferiLoc'.
     */
    public java.lang.String getCdCreCferiLoc()
    {
        return this._cdCreCferiLoc;
    } //-- java.lang.String getCdCreCferiLoc() 

    /**
     * Returns the value of field 'cdCreContratoTransf'.
     * 
     * @return String
     * @return the value of field 'cdCreContratoTransf'.
     */
    public java.lang.String getCdCreContratoTransf()
    {
        return this._cdCreContratoTransf;
    } //-- java.lang.String getCdCreContratoTransf() 

    /**
     * Returns the value of field
     * 'cdCreIndicadorCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdCreIndicadorCadastroFavorecido'
     */
    public java.lang.String getCdCreIndicadorCadastroFavorecido()
    {
        return this._cdCreIndicadorCadastroFavorecido;
    } //-- java.lang.String getCdCreIndicadorCadastroFavorecido() 

    /**
     * Returns the value of field 'cdCreIndicadorPercentualMaximo'.
     * 
     * @return String
     * @return the value of field 'cdCreIndicadorPercentualMaximo'.
     */
    public java.lang.String getCdCreIndicadorPercentualMaximo()
    {
        return this._cdCreIndicadorPercentualMaximo;
    } //-- java.lang.String getCdCreIndicadorPercentualMaximo() 

    /**
     * Returns the value of field
     * 'cdCreIndicadorQuantidadeFloating'.
     * 
     * @return String
     * @return the value of field 'cdCreIndicadorQuantidadeFloating'
     */
    public java.lang.String getCdCreIndicadorQuantidadeFloating()
    {
        return this._cdCreIndicadorQuantidadeFloating;
    } //-- java.lang.String getCdCreIndicadorQuantidadeFloating() 

    /**
     * Returns the value of field 'cdCreIndicadorQuantidadeMaxima'.
     * 
     * @return String
     * @return the value of field 'cdCreIndicadorQuantidadeMaxima'.
     */
    public java.lang.String getCdCreIndicadorQuantidadeMaxima()
    {
        return this._cdCreIndicadorQuantidadeMaxima;
    } //-- java.lang.String getCdCreIndicadorQuantidadeMaxima() 

    /**
     * Returns the value of field
     * 'cdCreIndicadorQuantidadeRepique'.
     * 
     * @return String
     * @return the value of field 'cdCreIndicadorQuantidadeRepique'.
     */
    public java.lang.String getCdCreIndicadorQuantidadeRepique()
    {
        return this._cdCreIndicadorQuantidadeRepique;
    } //-- java.lang.String getCdCreIndicadorQuantidadeRepique() 

    /**
     * Returns the value of field 'cdCreIndicadorValorDia'.
     * 
     * @return String
     * @return the value of field 'cdCreIndicadorValorDia'.
     */
    public java.lang.String getCdCreIndicadorValorDia()
    {
        return this._cdCreIndicadorValorDia;
    } //-- java.lang.String getCdCreIndicadorValorDia() 

    /**
     * Returns the value of field 'cdCreIndicadorValorIndividual'.
     * 
     * @return String
     * @return the value of field 'cdCreIndicadorValorIndividual'.
     */
    public java.lang.String getCdCreIndicadorValorIndividual()
    {
        return this._cdCreIndicadorValorIndividual;
    } //-- java.lang.String getCdCreIndicadorValorIndividual() 

    /**
     * Returns the value of field 'cdCreIndicadorValorMaximo'.
     * 
     * @return String
     * @return the value of field 'cdCreIndicadorValorMaximo'.
     */
    public java.lang.String getCdCreIndicadorValorMaximo()
    {
        return this._cdCreIndicadorValorMaximo;
    } //-- java.lang.String getCdCreIndicadorValorMaximo() 

    /**
     * Returns the value of field
     * 'cdCreUtilizacaoCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'cdCreUtilizacaoCadastroFavorecido'.
     */
    public java.lang.String getCdCreUtilizacaoCadastroFavorecido()
    {
        return this._cdCreUtilizacaoCadastroFavorecido;
    } //-- java.lang.String getCdCreUtilizacaoCadastroFavorecido() 

    /**
     * Returns the value of field 'cdLctoPgmd'.
     * 
     * @return String
     * @return the value of field 'cdLctoPgmd'.
     */
    public java.lang.String getCdLctoPgmd()
    {
        return this._cdLctoPgmd;
    } //-- java.lang.String getCdLctoPgmd() 

    /**
     * Returns the value of field 'dsCrePrioridadeDebito'.
     * 
     * @return String
     * @return the value of field 'dsCrePrioridadeDebito'.
     */
    public java.lang.String getDsCrePrioridadeDebito()
    {
        return this._dsCrePrioridadeDebito;
    } //-- java.lang.String getDsCrePrioridadeDebito() 

    /**
     * Returns the value of field
     * 'dsCreTipoConsistenciaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsCreTipoConsistenciaFavorecido'.
     */
    public java.lang.String getDsCreTipoConsistenciaFavorecido()
    {
        return this._dsCreTipoConsistenciaFavorecido;
    } //-- java.lang.String getDsCreTipoConsistenciaFavorecido() 

    /**
     * Returns the value of field 'dsCreTipoConsultaSaldo'.
     * 
     * @return String
     * @return the value of field 'dsCreTipoConsultaSaldo'.
     */
    public java.lang.String getDsCreTipoConsultaSaldo()
    {
        return this._dsCreTipoConsultaSaldo;
    } //-- java.lang.String getDsCreTipoConsultaSaldo() 

    /**
     * Returns the value of field 'dsCreTipoProcessamento'.
     * 
     * @return String
     * @return the value of field 'dsCreTipoProcessamento'.
     */
    public java.lang.String getDsCreTipoProcessamento()
    {
        return this._dsCreTipoProcessamento;
    } //-- java.lang.String getDsCreTipoProcessamento() 

    /**
     * Returns the value of field 'dsCreTipoRejeicaoAgendada'.
     * 
     * @return String
     * @return the value of field 'dsCreTipoRejeicaoAgendada'.
     */
    public java.lang.String getDsCreTipoRejeicaoAgendada()
    {
        return this._dsCreTipoRejeicaoAgendada;
    } //-- java.lang.String getDsCreTipoRejeicaoAgendada() 

    /**
     * Returns the value of field 'dsCreTipoRejeicaoEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dsCreTipoRejeicaoEfetivacao'.
     */
    public java.lang.String getDsCreTipoRejeicaoEfetivacao()
    {
        return this._dsCreTipoRejeicaoEfetivacao;
    } //-- java.lang.String getDsCreTipoRejeicaoEfetivacao() 

    /**
     * Returns the value of field 'dsCreTipoTratamentoFeriado'.
     * 
     * @return String
     * @return the value of field 'dsCreTipoTratamentoFeriado'.
     */
    public java.lang.String getDsCreTipoTratamentoFeriado()
    {
        return this._dsCreTipoTratamentoFeriado;
    } //-- java.lang.String getDsCreTipoTratamentoFeriado() 

    /**
     * Returns the value of field 'dsCretipoServico'.
     * 
     * @return String
     * @return the value of field 'dsCretipoServico'.
     */
    public java.lang.String getDsCretipoServico()
    {
        return this._dsCretipoServico;
    } //-- java.lang.String getDsCretipoServico() 

    /**
     * Returns the value of field 'pcCreMaximoInconsistente'.
     * 
     * @return int
     * @return the value of field 'pcCreMaximoInconsistente'.
     */
    public int getPcCreMaximoInconsistente()
    {
        return this._pcCreMaximoInconsistente;
    } //-- int getPcCreMaximoInconsistente() 

    /**
     * Returns the value of field 'qtCreDiaFloating'.
     * 
     * @return int
     * @return the value of field 'qtCreDiaFloating'.
     */
    public int getQtCreDiaFloating()
    {
        return this._qtCreDiaFloating;
    } //-- int getQtCreDiaFloating() 

    /**
     * Returns the value of field 'qtCreDiaRepique'.
     * 
     * @return int
     * @return the value of field 'qtCreDiaRepique'.
     */
    public int getQtCreDiaRepique()
    {
        return this._qtCreDiaRepique;
    } //-- int getQtCreDiaRepique() 

    /**
     * Returns the value of field 'qtCreMaximaInconsistencia'.
     * 
     * @return int
     * @return the value of field 'qtCreMaximaInconsistencia'.
     */
    public int getQtCreMaximaInconsistencia()
    {
        return this._qtCreMaximaInconsistencia;
    } //-- int getQtCreMaximaInconsistencia() 

    /**
     * Returns the value of field 'vlCreLimiteDiario'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlCreLimiteDiario'.
     */
    public java.math.BigDecimal getVlCreLimiteDiario()
    {
        return this._vlCreLimiteDiario;
    } //-- java.math.BigDecimal getVlCreLimiteDiario() 

    /**
     * Returns the value of field 'vlCreLimiteIndividual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlCreLimiteIndividual'.
     */
    public java.math.BigDecimal getVlCreLimiteIndividual()
    {
        return this._vlCreLimiteIndividual;
    } //-- java.math.BigDecimal getVlCreLimiteIndividual() 

    /**
     * Returns the value of field 'vlCreMaximoFavorecidoNao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlCreMaximoFavorecidoNao'.
     */
    public java.math.BigDecimal getVlCreMaximoFavorecidoNao()
    {
        return this._vlCreMaximoFavorecidoNao;
    } //-- java.math.BigDecimal getVlCreMaximoFavorecidoNao() 

    /**
     * Method hasPcCreMaximoInconsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPcCreMaximoInconsistente()
    {
        return this._has_pcCreMaximoInconsistente;
    } //-- boolean hasPcCreMaximoInconsistente() 

    /**
     * Method hasQtCreDiaFloating
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtCreDiaFloating()
    {
        return this._has_qtCreDiaFloating;
    } //-- boolean hasQtCreDiaFloating() 

    /**
     * Method hasQtCreDiaRepique
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtCreDiaRepique()
    {
        return this._has_qtCreDiaRepique;
    } //-- boolean hasQtCreDiaRepique() 

    /**
     * Method hasQtCreMaximaInconsistencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtCreMaximaInconsistencia()
    {
        return this._has_qtCreMaximaInconsistencia;
    } //-- boolean hasQtCreMaximaInconsistencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCreCferiLoc'.
     * 
     * @param cdCreCferiLoc the value of field 'cdCreCferiLoc'.
     */
    public void setCdCreCferiLoc(java.lang.String cdCreCferiLoc)
    {
        this._cdCreCferiLoc = cdCreCferiLoc;
    } //-- void setCdCreCferiLoc(java.lang.String) 

    /**
     * Sets the value of field 'cdCreContratoTransf'.
     * 
     * @param cdCreContratoTransf the value of field
     * 'cdCreContratoTransf'.
     */
    public void setCdCreContratoTransf(java.lang.String cdCreContratoTransf)
    {
        this._cdCreContratoTransf = cdCreContratoTransf;
    } //-- void setCdCreContratoTransf(java.lang.String) 

    /**
     * Sets the value of field 'cdCreIndicadorCadastroFavorecido'.
     * 
     * @param cdCreIndicadorCadastroFavorecido the value of field
     * 'cdCreIndicadorCadastroFavorecido'.
     */
    public void setCdCreIndicadorCadastroFavorecido(java.lang.String cdCreIndicadorCadastroFavorecido)
    {
        this._cdCreIndicadorCadastroFavorecido = cdCreIndicadorCadastroFavorecido;
    } //-- void setCdCreIndicadorCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdCreIndicadorPercentualMaximo'.
     * 
     * @param cdCreIndicadorPercentualMaximo the value of field
     * 'cdCreIndicadorPercentualMaximo'.
     */
    public void setCdCreIndicadorPercentualMaximo(java.lang.String cdCreIndicadorPercentualMaximo)
    {
        this._cdCreIndicadorPercentualMaximo = cdCreIndicadorPercentualMaximo;
    } //-- void setCdCreIndicadorPercentualMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdCreIndicadorQuantidadeFloating'.
     * 
     * @param cdCreIndicadorQuantidadeFloating the value of field
     * 'cdCreIndicadorQuantidadeFloating'.
     */
    public void setCdCreIndicadorQuantidadeFloating(java.lang.String cdCreIndicadorQuantidadeFloating)
    {
        this._cdCreIndicadorQuantidadeFloating = cdCreIndicadorQuantidadeFloating;
    } //-- void setCdCreIndicadorQuantidadeFloating(java.lang.String) 

    /**
     * Sets the value of field 'cdCreIndicadorQuantidadeMaxima'.
     * 
     * @param cdCreIndicadorQuantidadeMaxima the value of field
     * 'cdCreIndicadorQuantidadeMaxima'.
     */
    public void setCdCreIndicadorQuantidadeMaxima(java.lang.String cdCreIndicadorQuantidadeMaxima)
    {
        this._cdCreIndicadorQuantidadeMaxima = cdCreIndicadorQuantidadeMaxima;
    } //-- void setCdCreIndicadorQuantidadeMaxima(java.lang.String) 

    /**
     * Sets the value of field 'cdCreIndicadorQuantidadeRepique'.
     * 
     * @param cdCreIndicadorQuantidadeRepique the value of field
     * 'cdCreIndicadorQuantidadeRepique'.
     */
    public void setCdCreIndicadorQuantidadeRepique(java.lang.String cdCreIndicadorQuantidadeRepique)
    {
        this._cdCreIndicadorQuantidadeRepique = cdCreIndicadorQuantidadeRepique;
    } //-- void setCdCreIndicadorQuantidadeRepique(java.lang.String) 

    /**
     * Sets the value of field 'cdCreIndicadorValorDia'.
     * 
     * @param cdCreIndicadorValorDia the value of field
     * 'cdCreIndicadorValorDia'.
     */
    public void setCdCreIndicadorValorDia(java.lang.String cdCreIndicadorValorDia)
    {
        this._cdCreIndicadorValorDia = cdCreIndicadorValorDia;
    } //-- void setCdCreIndicadorValorDia(java.lang.String) 

    /**
     * Sets the value of field 'cdCreIndicadorValorIndividual'.
     * 
     * @param cdCreIndicadorValorIndividual the value of field
     * 'cdCreIndicadorValorIndividual'.
     */
    public void setCdCreIndicadorValorIndividual(java.lang.String cdCreIndicadorValorIndividual)
    {
        this._cdCreIndicadorValorIndividual = cdCreIndicadorValorIndividual;
    } //-- void setCdCreIndicadorValorIndividual(java.lang.String) 

    /**
     * Sets the value of field 'cdCreIndicadorValorMaximo'.
     * 
     * @param cdCreIndicadorValorMaximo the value of field
     * 'cdCreIndicadorValorMaximo'.
     */
    public void setCdCreIndicadorValorMaximo(java.lang.String cdCreIndicadorValorMaximo)
    {
        this._cdCreIndicadorValorMaximo = cdCreIndicadorValorMaximo;
    } //-- void setCdCreIndicadorValorMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdCreUtilizacaoCadastroFavorecido'.
     * 
     * @param cdCreUtilizacaoCadastroFavorecido the value of field
     * 'cdCreUtilizacaoCadastroFavorecido'.
     */
    public void setCdCreUtilizacaoCadastroFavorecido(java.lang.String cdCreUtilizacaoCadastroFavorecido)
    {
        this._cdCreUtilizacaoCadastroFavorecido = cdCreUtilizacaoCadastroFavorecido;
    } //-- void setCdCreUtilizacaoCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdLctoPgmd'.
     * 
     * @param cdLctoPgmd the value of field 'cdLctoPgmd'.
     */
    public void setCdLctoPgmd(java.lang.String cdLctoPgmd)
    {
        this._cdLctoPgmd = cdLctoPgmd;
    } //-- void setCdLctoPgmd(java.lang.String) 

    /**
     * Sets the value of field 'dsCrePrioridadeDebito'.
     * 
     * @param dsCrePrioridadeDebito the value of field
     * 'dsCrePrioridadeDebito'.
     */
    public void setDsCrePrioridadeDebito(java.lang.String dsCrePrioridadeDebito)
    {
        this._dsCrePrioridadeDebito = dsCrePrioridadeDebito;
    } //-- void setDsCrePrioridadeDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsCreTipoConsistenciaFavorecido'.
     * 
     * @param dsCreTipoConsistenciaFavorecido the value of field
     * 'dsCreTipoConsistenciaFavorecido'.
     */
    public void setDsCreTipoConsistenciaFavorecido(java.lang.String dsCreTipoConsistenciaFavorecido)
    {
        this._dsCreTipoConsistenciaFavorecido = dsCreTipoConsistenciaFavorecido;
    } //-- void setDsCreTipoConsistenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsCreTipoConsultaSaldo'.
     * 
     * @param dsCreTipoConsultaSaldo the value of field
     * 'dsCreTipoConsultaSaldo'.
     */
    public void setDsCreTipoConsultaSaldo(java.lang.String dsCreTipoConsultaSaldo)
    {
        this._dsCreTipoConsultaSaldo = dsCreTipoConsultaSaldo;
    } //-- void setDsCreTipoConsultaSaldo(java.lang.String) 

    /**
     * Sets the value of field 'dsCreTipoProcessamento'.
     * 
     * @param dsCreTipoProcessamento the value of field
     * 'dsCreTipoProcessamento'.
     */
    public void setDsCreTipoProcessamento(java.lang.String dsCreTipoProcessamento)
    {
        this._dsCreTipoProcessamento = dsCreTipoProcessamento;
    } //-- void setDsCreTipoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'dsCreTipoRejeicaoAgendada'.
     * 
     * @param dsCreTipoRejeicaoAgendada the value of field
     * 'dsCreTipoRejeicaoAgendada'.
     */
    public void setDsCreTipoRejeicaoAgendada(java.lang.String dsCreTipoRejeicaoAgendada)
    {
        this._dsCreTipoRejeicaoAgendada = dsCreTipoRejeicaoAgendada;
    } //-- void setDsCreTipoRejeicaoAgendada(java.lang.String) 

    /**
     * Sets the value of field 'dsCreTipoRejeicaoEfetivacao'.
     * 
     * @param dsCreTipoRejeicaoEfetivacao the value of field
     * 'dsCreTipoRejeicaoEfetivacao'.
     */
    public void setDsCreTipoRejeicaoEfetivacao(java.lang.String dsCreTipoRejeicaoEfetivacao)
    {
        this._dsCreTipoRejeicaoEfetivacao = dsCreTipoRejeicaoEfetivacao;
    } //-- void setDsCreTipoRejeicaoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'dsCreTipoTratamentoFeriado'.
     * 
     * @param dsCreTipoTratamentoFeriado the value of field
     * 'dsCreTipoTratamentoFeriado'.
     */
    public void setDsCreTipoTratamentoFeriado(java.lang.String dsCreTipoTratamentoFeriado)
    {
        this._dsCreTipoTratamentoFeriado = dsCreTipoTratamentoFeriado;
    } //-- void setDsCreTipoTratamentoFeriado(java.lang.String) 

    /**
     * Sets the value of field 'dsCretipoServico'.
     * 
     * @param dsCretipoServico the value of field 'dsCretipoServico'
     */
    public void setDsCretipoServico(java.lang.String dsCretipoServico)
    {
        this._dsCretipoServico = dsCretipoServico;
    } //-- void setDsCretipoServico(java.lang.String) 

    /**
     * Sets the value of field 'pcCreMaximoInconsistente'.
     * 
     * @param pcCreMaximoInconsistente the value of field
     * 'pcCreMaximoInconsistente'.
     */
    public void setPcCreMaximoInconsistente(int pcCreMaximoInconsistente)
    {
        this._pcCreMaximoInconsistente = pcCreMaximoInconsistente;
        this._has_pcCreMaximoInconsistente = true;
    } //-- void setPcCreMaximoInconsistente(int) 

    /**
     * Sets the value of field 'qtCreDiaFloating'.
     * 
     * @param qtCreDiaFloating the value of field 'qtCreDiaFloating'
     */
    public void setQtCreDiaFloating(int qtCreDiaFloating)
    {
        this._qtCreDiaFloating = qtCreDiaFloating;
        this._has_qtCreDiaFloating = true;
    } //-- void setQtCreDiaFloating(int) 

    /**
     * Sets the value of field 'qtCreDiaRepique'.
     * 
     * @param qtCreDiaRepique the value of field 'qtCreDiaRepique'.
     */
    public void setQtCreDiaRepique(int qtCreDiaRepique)
    {
        this._qtCreDiaRepique = qtCreDiaRepique;
        this._has_qtCreDiaRepique = true;
    } //-- void setQtCreDiaRepique(int) 

    /**
     * Sets the value of field 'qtCreMaximaInconsistencia'.
     * 
     * @param qtCreMaximaInconsistencia the value of field
     * 'qtCreMaximaInconsistencia'.
     */
    public void setQtCreMaximaInconsistencia(int qtCreMaximaInconsistencia)
    {
        this._qtCreMaximaInconsistencia = qtCreMaximaInconsistencia;
        this._has_qtCreMaximaInconsistencia = true;
    } //-- void setQtCreMaximaInconsistencia(int) 

    /**
     * Sets the value of field 'vlCreLimiteDiario'.
     * 
     * @param vlCreLimiteDiario the value of field
     * 'vlCreLimiteDiario'.
     */
    public void setVlCreLimiteDiario(java.math.BigDecimal vlCreLimiteDiario)
    {
        this._vlCreLimiteDiario = vlCreLimiteDiario;
    } //-- void setVlCreLimiteDiario(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlCreLimiteIndividual'.
     * 
     * @param vlCreLimiteIndividual the value of field
     * 'vlCreLimiteIndividual'.
     */
    public void setVlCreLimiteIndividual(java.math.BigDecimal vlCreLimiteIndividual)
    {
        this._vlCreLimiteIndividual = vlCreLimiteIndividual;
    } //-- void setVlCreLimiteIndividual(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlCreMaximoFavorecidoNao'.
     * 
     * @param vlCreMaximoFavorecidoNao the value of field
     * 'vlCreMaximoFavorecidoNao'.
     */
    public void setVlCreMaximoFavorecidoNao(java.math.BigDecimal vlCreMaximoFavorecidoNao)
    {
        this._vlCreMaximoFavorecidoNao = vlCreMaximoFavorecidoNao;
    } //-- void setVlCreMaximoFavorecidoNao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias11
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
