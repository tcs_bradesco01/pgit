/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.pgic0000.impl;

import br.com.bradesco.web.pgit.service.business.pgic0000.IPgic0000Service;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Pgic0000
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class Pgic0000ServiceImpl implements IPgic0000Service {

    /**
     * Construtor.
     */
    public Pgic0000ServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.pgic0000.IPgic0000#samplePgic0000()
     */
    public void samplePgic0000() {
        // TODO: Implementa�ao
    }
    
}

