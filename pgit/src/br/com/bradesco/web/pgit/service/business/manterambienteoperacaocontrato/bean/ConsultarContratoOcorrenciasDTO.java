package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

import static br.com.bradesco.web.pgit.utils.CpfCnpjUtils.formatCpfCnpjCompleto;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

public class ConsultarContratoOcorrenciasDTO {

	private Long cdClubRepresentante;
	private Long cdCorpoCnpjRepresentante;
	private Integer cdFilialCnpjRepresentante;
	private Integer cdControleCnpjRepresentante;
	private String nmRazaoRepresentante;
	private Long cdPessoaJuridica;
	private String dsPessoaJuridica;
	private Integer cdTipoContrato;
	private String dsTipoContrato;
	private Long nrSequenciaContrato;
	private String dsContrato;
	private Integer cdAgenciaOperadora;
	private String cdDigitoAgenciaOperadora;
	private String dsAgenciaOperadora;
	private Long cdFuncionarioBradesco;
	private String dsFuncionarioBradesco;
	private Integer cdSituacaoContrato;
	private String dsSituacaoContrato;
	private Integer cdMotivoSituacao;
	private String dsMotivoSituacao;
	private String cdAditivo;
	private String cdTipoParticipacao;
	private Integer cdSegmentoCliente;
	private String dsSegmentoCliente;
	private Integer cdSubSegmentoCliente;
	private String dsSubSegmentoCliente;
	private Integer cdAtividadeEconomica;
	private String dsAtividadeEconomica;
	private Long cdGrupoEconomico;
	private String dsGrupoEconomico;
	private String dtAbertura;
	private String dtEncerramento;
	private String dtInclusao;

	public Long getCdClubRepresentante() {
		return cdClubRepresentante;
	}

	public void setCdClubRepresentante(Long cdClubRepresentante) {
		this.cdClubRepresentante = cdClubRepresentante;
	}

	public Long getCdCorpoCnpjRepresentante() {
		return cdCorpoCnpjRepresentante;
	}

	public void setCdCorpoCnpjRepresentante(Long cdCorpoCnpjRepresentante) {
		this.cdCorpoCnpjRepresentante = cdCorpoCnpjRepresentante;
	}

	public Integer getCdFilialCnpjRepresentante() {
		return cdFilialCnpjRepresentante;
	}

	public void setCdFilialCnpjRepresentante(Integer cdFilialCnpjRepresentante) {
		this.cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
	}

	public Integer getCdControleCnpjRepresentante() {
		return cdControleCnpjRepresentante;
	}

	public void setCdControleCnpjRepresentante(
			Integer cdControleCnpjRepresentante) {
		this.cdControleCnpjRepresentante = cdControleCnpjRepresentante;
	}

	public String getNmRazaoRepresentante() {
		return nmRazaoRepresentante;
	}

	public void setNmRazaoRepresentante(String nmRazaoRepresentante) {
		this.nmRazaoRepresentante = nmRazaoRepresentante;
	}

	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	public String getDsPessoaJuridica() {
		return dsPessoaJuridica;
	}

	public void setDsPessoaJuridica(String dsPessoaJuridica) {
		this.dsPessoaJuridica = dsPessoaJuridica;
	}

	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	public String getDsTipoContrato() {
		return dsTipoContrato;
	}

	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}

	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	public String getDsContrato() {
		return dsContrato;
	}

	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	public Integer getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}

	public void setCdAgenciaOperadora(Integer cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}

	public String getCdDigitoAgenciaOperadora() {
		return cdDigitoAgenciaOperadora;
	}

	public void setCdDigitoAgenciaOperadora(String cdDigitoAgenciaOperadora) {
		this.cdDigitoAgenciaOperadora = cdDigitoAgenciaOperadora;
	}

	public String getDsAgenciaOperadora() {
		return dsAgenciaOperadora;
	}

	public void setDsAgenciaOperadora(String dsAgenciaOperadora) {
		this.dsAgenciaOperadora = dsAgenciaOperadora;
	}

	public Long getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}

	public void setCdFuncionarioBradesco(Long cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}

	public String getDsFuncionarioBradesco() {
		return dsFuncionarioBradesco;
	}

	public void setDsFuncionarioBradesco(String dsFuncionarioBradesco) {
		this.dsFuncionarioBradesco = dsFuncionarioBradesco;
	}

	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}

	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}

	public Integer getCdMotivoSituacao() {
		return cdMotivoSituacao;
	}

	public void setCdMotivoSituacao(Integer cdMotivoSituacao) {
		this.cdMotivoSituacao = cdMotivoSituacao;
	}

	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	public String getCdAditivo() {
		return cdAditivo;
	}

	public void setCdAditivo(String cdAditivo) {
		this.cdAditivo = cdAditivo;
	}

	public String getCdTipoParticipacao() {
		return cdTipoParticipacao;
	}

	public void setCdTipoParticipacao(String cdTipoParticipacao) {
		this.cdTipoParticipacao = cdTipoParticipacao;
	}

	public Integer getCdSegmentoCliente() {
		return cdSegmentoCliente;
	}

	public void setCdSegmentoCliente(Integer cdSegmentoCliente) {
		this.cdSegmentoCliente = cdSegmentoCliente;
	}

	public String getDsSegmentoCliente() {
		return dsSegmentoCliente;
	}

	public void setDsSegmentoCliente(String dsSegmentoCliente) {
		this.dsSegmentoCliente = dsSegmentoCliente;
	}

	public Integer getCdSubSegmentoCliente() {
		return cdSubSegmentoCliente;
	}

	public void setCdSubSegmentoCliente(Integer cdSubSegmentoCliente) {
		this.cdSubSegmentoCliente = cdSubSegmentoCliente;
	}

	public String getDsSubSegmentoCliente() {
		return dsSubSegmentoCliente;
	}

	public void setDsSubSegmentoCliente(String dsSubSegmentoCliente) {
		this.dsSubSegmentoCliente = dsSubSegmentoCliente;
	}

	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}

	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}

	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}

	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}

	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}

	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}

	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}

	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}

	public String getDtAbertura() {
		return dtAbertura;
	}

	public void setDtAbertura(String dtAbertura) {
		this.dtAbertura = dtAbertura;
	}

	public String getDtEncerramento() {
		return dtEncerramento;
	}

	public void setDtEncerramento(String dtEncerramento) {
		this.dtEncerramento = dtEncerramento;
	}

	public String getDtInclusao() {
		return dtInclusao;
	}

	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	public String getCnpjOuCpfFormatado() {
		return formatCpfCnpjCompleto(cdCorpoCnpjRepresentante,
				cdFilialCnpjRepresentante, cdControleCnpjRepresentante);
	}

}
