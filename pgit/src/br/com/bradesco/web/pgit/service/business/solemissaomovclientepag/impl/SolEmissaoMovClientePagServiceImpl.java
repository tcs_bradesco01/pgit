/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.ISolEmissaoMovClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.ISolEmissaoMovClientePagServiceConstants;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ConsultarSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ConsultarSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.DetalharSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.DetalharSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ExcluirSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ExcluirSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.IncluirSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.IncluirSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.OcorrenciasDetalharSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtocliepagador.request.ConsultarSolEmiAviMovtoCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtocliepagador.response.ConsultarSolEmiAviMovtoCliePagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtocliepagador.request.DetalharSolEmiAviMovtoCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtocliepagador.response.DetalharSolEmiAviMovtoCliePagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.request.ExcluirSolEmiAviMovtoCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.response.ExcluirSolEmiAviMovtoCliePagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.IncluirSolEmiAviMovtoCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.response.IncluirSolEmiAviMovtoCliePagadorResponse;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: SolEmissaoMovClientePag
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SolEmissaoMovClientePagServiceImpl implements ISolEmissaoMovClientePagService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
     * Construtor.
     */
    public SolEmissaoMovClientePagServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.ISolEmissaoMovClientePagService#consultarSolEmiAviMovtoCliePagador(br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ConsultarSolEmiAviMovtoCliePagadorEntradaDTO)
     */
    public List<ConsultarSolEmiAviMovtoCliePagadorSaidaDTO> consultarSolEmiAviMovtoCliePagador (ConsultarSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO){
    	
    	List<ConsultarSolEmiAviMovtoCliePagadorSaidaDTO> listaRetorno = new ArrayList<ConsultarSolEmiAviMovtoCliePagadorSaidaDTO>();
    	ConsultarSolEmiAviMovtoCliePagadorRequest request = new ConsultarSolEmiAviMovtoCliePagadorRequest();
    	ConsultarSolEmiAviMovtoCliePagadorResponse response = new ConsultarSolEmiAviMovtoCliePagadorResponse();
    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setDtInicioSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getDtInicioSolicitacao()));
    	request.setDtFimSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getDtFimSolicitacao()));
    	request.setCdSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacao()));
    	request.setNrOcorrencias(ISolEmissaoMovClientePagServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	
    	response = getFactoryAdapter().getConsultarSolEmiAviMovtoCliePagadorPDCAdapter().invokeProcess(request);
    	
    	ConsultarSolEmiAviMovtoCliePagadorSaidaDTO saidaDTO;
    	
    	for(int i = 0; i < response.getOcorrenciasCount(); i++){
    		saidaDTO = new ConsultarSolEmiAviMovtoCliePagadorSaidaDTO();
    		
    		saidaDTO.setCodMensagem(response.getCodMensagem());
    		saidaDTO.setMensagem(response.getMensagem());
    		saidaDTO.setCdTipoSolicitacao(response.getOcorrencias(i).getCdTipoSolicitacao());
    		saidaDTO.setNrSolicitacao(response.getOcorrencias(i).getNrSolicitacao());
    		saidaDTO.setDtHoraSolicitacao(response.getOcorrencias(i).getDtHoraSolicitacao());
    		saidaDTO.setDtHoraAtendimento(response.getOcorrencias(i).getDtHoraAtendimento());
    		saidaDTO.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
    		saidaDTO.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
    	    saidaDTO.setCdMotivoSituacao(response.getOcorrencias(i).getCdMotivoSituacao());
    	    saidaDTO.setDsMotivoSituacao(response.getOcorrencias(i).getDsMotivoSituacao());
    	    saidaDTO.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
    	    saidaDTO.setDsTipoCanal(response.getOcorrencias(i).getDsTipoCanal());
    	    saidaDTO.setCdPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
    	    saidaDTO.setCdTipoContrato(response.getOcorrencias(i).getCdTipoContrato());
    	    saidaDTO.setNrSequenciaContrato(response.getOcorrencias(i).getNrSequenciaContrato());
    	    saidaDTO.setCdUsuarioInclusao(response.getOcorrencias(i).getCdUsuarioInclusao());
    	  
    	    saidaDTO.setContratoFormatado(saidaDTO.getCdPessoaJuridica() + "/" + saidaDTO.getCdTipoContrato() + "/" + saidaDTO.getNrSequenciaContrato());
    	    saidaDTO.setSituacaoFormatada(PgitUtil.concatenarCampos(saidaDTO.getCdSituacaoSolicitacao(), saidaDTO.getDsSituacaoSolicitacao()));
    	    saidaDTO.setMotivoFormatado(PgitUtil.concatenarCampos(saidaDTO.getCdMotivoSituacao(), saidaDTO.getDsMotivoSituacao()));
    	    saidaDTO.setDtHoraSolFormatada(FormatarData.formatarDataTrilha(saidaDTO.getDtHoraSolicitacao()));
    	    
    	    listaRetorno.add(saidaDTO);
    	    
    	  
    	}
    	
    	return listaRetorno;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.ISolEmissaoMovClientePagService#detalharSolEmiAviMovtoCliePagador(br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.DetalharSolEmiAviMovtoCliePagadorEntradaDTO)
     */
    public DetalharSolEmiAviMovtoCliePagadorSaidaDTO detalharSolEmiAviMovtoCliePagador (DetalharSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO){
    	DetalharSolEmiAviMovtoCliePagadorRequest request = new DetalharSolEmiAviMovtoCliePagadorRequest();
    	DetalharSolEmiAviMovtoCliePagadorResponse response = new DetalharSolEmiAviMovtoCliePagadorResponse();
    	DetalharSolEmiAviMovtoCliePagadorSaidaDTO saidaDTO = new DetalharSolEmiAviMovtoCliePagadorSaidaDTO();
    	
    	request.setCdPessoaJuridicaEmpr(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaEmpr()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdTipoSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoSolicitacaoPagamento()));
    	request.setCdSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacao()));
    	request.setNrOcorrencias(ISolEmissaoMovClientePagServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
    	
    	response = getFactoryAdapter().getDetalharSolEmiAviMovtoCliePagadorPDCAdapter().invokeProcess(request);
    	
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());
        saidaDTO.setCdSolicitacaoPagamentoIntegrado(response.getCdSolicitacaoPagamentoIntegrado());
        saidaDTO.setNrSolicitacaoPagamentoIntegrado(response.getNrSolicitacaoPagamentoIntegrado());
        saidaDTO.setCdSituacaoSolicitacao(response.getCdSituacaoSolicitacao());
        saidaDTO.setDsSituacaoSolicitacao(response.getDsSituacaoSolicitacao());
        saidaDTO.setDtInicioPeriodoMovimentacao(FormatarData.formatarDataFromPdc(response.getDtInicioPeriodoMovimentacao()));
        saidaDTO.setDtFimPeriodoMovimentacao(FormatarData.formatarDataFromPdc(response.getDtFimPeriodoMovimentacao()));
        saidaDTO.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
        saidaDTO.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
        saidaDTO.setCdProdutoOperacaoRelacionado(response.getCdProdutoOperacaoRelacionado());
        saidaDTO.setDsProdutoOperacaoRelacionado(response.getDsProdutoOperacaoRelacionado());
        saidaDTO.setCdRecebedorCredito(response.getCdRecebedorCredito());
        saidaDTO.setCdTipoInscricaoRecebedor(response.getCdTipoInscricaoRecebedor());
        saidaDTO.setCdCpfCnpjRecebedor(response.getCdCpfCnpjRecebedor());
        saidaDTO.setCdFilialCnpjRecebedor(response.getCdFilialCnpjRecebedor());
        saidaDTO.setCdControleCpfRecebedor(response.getCdControleCpfRecebedor());
        saidaDTO.setCdDestinoCorrespSolicitacao(response.getCdDestinoCorrespSolicitacao());
        saidaDTO.setCdTipoPostagemSolicitacao(response.getCdTipoPostagemSolicitacao());
        saidaDTO.setDsLogradouroPagador(response.getDsLogradouroPagador());
        saidaDTO.setDsNumeroLogradouroPagador(response.getDsNumeroLogradouroPagador());
        saidaDTO.setDsComplementoLogradouroPagador(response.getDsComplementoLogradouroPagador());
        saidaDTO.setDsBairroClientePagador(response.getDsBairroClientePagador());
        saidaDTO.setDsMunicipioClientePagador(response.getDsMunicipioClientePagador());
        saidaDTO.setCdSiglaUfPagador(response.getCdSiglaUfPagador());
        saidaDTO.setCdCepPagador(response.getCdCepPagador());
        saidaDTO.setCdCepComplementoPagador(response.getCdCepComplementoPagador());
        saidaDTO.setDsEmailClientePagador(response.getDsEmailClientePagador());
        saidaDTO.setCdAgenciaOperadora(response.getCdAgenciaOperadora());
        saidaDTO.setDsAgencia(response.getDsAgencia());
        saidaDTO.setVlTarifa(response.getVlrTarifaAtual());
        saidaDTO.setVlTarifaPadrao(response.getVlTarifaPadrao());
        saidaDTO.setVlrTarifaAtual(response.getVlrTarifaAtual());
        saidaDTO.setCdPercentualDescTarifa(response.getCdPercentualDescTarifa());
        saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
        saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
        saidaDTO.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao().equals("0") ? "" : response.getCdOperacaoCanalInclusao());
        saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
        saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
        saidaDTO.setCdDepartamentoUnidade(response.getCdDepartamentoUnidade());
        
        List<OcorrenciasDetalharSaidaDTO> listaOcorrencias = new ArrayList<OcorrenciasDetalharSaidaDTO>();
        OcorrenciasDetalharSaidaDTO ocorrencias;
        for(int i = 0; i< response.getOcorrenciasCount();i ++){
        	ocorrencias = new OcorrenciasDetalharSaidaDTO();
     	   ocorrencias.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
    	   ocorrencias.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
    	   ocorrencias.setDtPagamento(response.getOcorrencias(i).getDtPagamento());
    	   ocorrencias.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
    	   ocorrencias.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido());
    	   ocorrencias.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
    	   ocorrencias.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
    	   ocorrencias.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
    	   ocorrencias.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
    	   ocorrencias.setCdDigitoContaDebito(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdDigitoContaDebito()));
    	   ocorrencias.setCdProdutoServico(response.getOcorrencias(i).getCdProdutoServico());
    	   ocorrencias.setCdSituacaoPagamento(response.getOcorrencias(i).getCdSituacaoPagamento());
    	   ocorrencias.setDsSituacaoPagamento(response.getOcorrencias(i).getDsSituacaoPagamento());
    	   ocorrencias.setDsCodigoProdutoServicoOperacao(response.getOcorrencias(i).getDsCodigoProdutoServicoOperacao());
    	   ocorrencias.setCdProdutoRelacionado(response.getOcorrencias(i).getCdProdutoRelacionado());
    	   ocorrencias.setDsCodigoProdutoServicoRelacionado(response.getOcorrencias(i).getDsCodigoProdutoServicoRelacionado());
    	   
    	   ocorrencias.setFavorecidoFormatado(PgitUtil.concatenarCampos(ocorrencias.getCdFavorecido(), ocorrencias.getDsFavorecido()));
    	   ocorrencias.setDataFormatada(DateUtils.formartarDataPDCparaPadraPtBr(ocorrencias.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
    	   ocorrencias.setContaDebitoFormatada(PgitUtil.formatBancoAgenciaConta(ocorrencias.getCdBancoDebito(), ocorrencias.getCdAgenciaDebito(), null, ocorrencias.getCdContaDebito(), String.valueOf(PgitUtil.verificaIntegerNulo(ocorrencias.getCdDigitoContaDebito())), true));
    	   
    	   listaOcorrencias.add(ocorrencias);
        }
        
        saidaDTO.setListaOcorrencias(listaOcorrencias);
    	
    	return saidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.ISolEmissaoMovClientePagService#excluirSolEmiAviMovtoCliePagador(br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ExcluirSolEmiAviMovtoCliePagadorEntradaDTO)
     */
    public ExcluirSolEmiAviMovtoCliePagadorSaidaDTO excluirSolEmiAviMovtoCliePagador (ExcluirSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO){
    	ExcluirSolEmiAviMovtoCliePagadorRequest request = new ExcluirSolEmiAviMovtoCliePagadorRequest();
    	ExcluirSolEmiAviMovtoCliePagadorResponse response = new ExcluirSolEmiAviMovtoCliePagadorResponse();
    	ExcluirSolEmiAviMovtoCliePagadorSaidaDTO saidaDTO = new ExcluirSolEmiAviMovtoCliePagadorSaidaDTO();
    	
    	request.setCdSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamentoIntegrado()));
    	request.setNrSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamentoIntegrado()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setNrRegistrosEntrada(ISolEmissaoMovClientePagServiceConstants.NUMERO_REGISTRO_EXCLUIR);
    	
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.request.Ocorrencias>();
    	br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.request.Ocorrencias();
    	
    	ocorrencia.setCdControlePagamento("");
    	ocorrencia.setCdTipoCanal(0);
    	lista.add(ocorrencia);
    	
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.request.Ocorrencias[0]));
    	
    	response = getFactoryAdapter().getExcluirSolEmiAviMovtoCliePagadorPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
    	saidaDTO.setMensagem(response.getMensagem());
    	
    	return saidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.ISolEmissaoMovClientePagService#incluirSolEmiAviMovtoCliePagador(br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.IncluirSolEmiAviMovtoCliePagadorEntradaDTO)
     */
    public IncluirSolEmiAviMovtoCliePagadorSaidaDTO incluirSolEmiAviMovtoCliePagador (IncluirSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO){
    	IncluirSolEmiAviMovtoCliePagadorRequest request = new IncluirSolEmiAviMovtoCliePagadorRequest();
    	IncluirSolEmiAviMovtoCliePagadorResponse response = new IncluirSolEmiAviMovtoCliePagadorResponse();
    	IncluirSolEmiAviMovtoCliePagadorSaidaDTO saidaDTO = new IncluirSolEmiAviMovtoCliePagadorSaidaDTO();
    	
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setDtInicioPerMovimentacao(PgitUtil.verificaStringNula(entradaDTO.getDtInicioPerMovimentacao()));
        request.setDsFimPerMovimentacao(PgitUtil.verificaStringNula(entradaDTO.getDsFimPerMovimentacao()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoRelacionado()));
        request.setCdRecebedorCredito(PgitUtil.verificaLongNulo(entradaDTO.getCdRecebedorCredito()));
        request.setCdTipoInscricaoRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoInscricaoRecebedor()));
        request.setCdCpfCnpjRecebedor(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjRecebedor()));
        request.setCdFilialCnpjRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjRecebedor()));
        request.setCdControleCpfRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCpfRecebedor()));
        request.setCdDestinoCorrespondenciaSolic(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDestinoCorrespondenciaSolic()));
        request.setCdTipoPostagemSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getCdTipoPostagemSolicitacao()));
        request.setDsLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsLogradouroPagador()));
        request.setDsNumeroLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsNumeroLogradouroPagador()));
        request.setDsComplementoLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsComplementoLogradouroPagador()));
        request.setDsBairroClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsBairroClientePagador()));
        request.setDsMunicipioClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsMunicipioClientePagador()));
        request.setCdSiglaUfPagador(PgitUtil.verificaStringNula(entradaDTO.getCdSiglaUfPagador()));
        request.setCdCepPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCepPagador()));
        request.setCdCepComplementoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCepComplementoPagador()));
        request.setDsEmailClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsEmailClientePagador()));
        request.setCdPessoaJuridicaDepto(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaDepto()));
        request.setNrSequenciaUnidadeDepto(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSequenciaUnidadeDepto()));
        request.setVrTarifaPadraoSolic(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVrTarifaPadraoSolic()));
        request.setCdPercentualTarifaSolic(PgitUtil.verificaBigDecimalNulo(entradaDTO.getCdPercentualTarifaSolic()));
        request.setVlTarifaNegocioSolic(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlTarifaNegocioSolic()));
        request.setNrRegistrosEntrada(ISolEmissaoMovClientePagServiceConstants.NUMERO_REGISTRO_INCLUIR);
        request.setCdDepartamentoUnidade(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDepartamentoUnidade()));
 
        ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias>();
        br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias ocorrencias = new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias();
        
        ocorrencias.setCdControlePagamento("");
        ocorrencias.setCdTipoCanal(0);
        lista.add(ocorrencias);
        
        request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias[])lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias[0]));
        
        response  = getFactoryAdapter().getIncluirSolEmiAviMovtoCliePagadorPDCAdapter().invokeProcess(request);
        
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());
    	
    	return saidaDTO;
    }
}

