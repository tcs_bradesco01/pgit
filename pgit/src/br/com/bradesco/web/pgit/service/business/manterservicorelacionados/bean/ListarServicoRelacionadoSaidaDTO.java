/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean;

/**
 * Nome: ListarServicoRelacionadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicoRelacionadoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/** Atributo codTipoServico. */
	private int codTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo codModalidadeServico. */
	private int codModalidadeServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo codRelacionamentoProd. */
	private int codRelacionamentoProd;
	
	/** Atributo codServicoComposto. */
	private long codServicoComposto;
	
	/** Atributo dsServicoComposto. */
	private String dsServicoComposto;
	
	/** Atributo codNaturezaServico. */
	private int codNaturezaServico;
	
	/** Atributo check. */
	private boolean check;
	
	/** Atributo tipoServicoFormatado. */
	private String tipoServicoFormatado;
	
	/** Atributo modalidadeFormatado. */
	private String modalidadeFormatado;
	
	/** Atributo servicoCompostoFormatado. */
	private String servicoCompostoFormatado;
	
	
	
	/**
	 * Get: modalidadeFormatado.
	 *
	 * @return modalidadeFormatado
	 */
	public String getModalidadeFormatado() {
		return modalidadeFormatado;
	}
	
	/**
	 * Set: modalidadeFormatado.
	 *
	 * @param modalidadeFormatado the modalidade formatado
	 */
	public void setModalidadeFormatado(String modalidadeFormatado) {
		this.modalidadeFormatado = modalidadeFormatado;
	}
	
	/**
	 * Get: servicoCompostoFormatado.
	 *
	 * @return servicoCompostoFormatado
	 */
	public String getServicoCompostoFormatado() {
		return servicoCompostoFormatado;
	}
	
	/**
	 * Set: servicoCompostoFormatado.
	 *
	 * @param servicoCompostoFormatado the servico composto formatado
	 */
	public void setServicoCompostoFormatado(String servicoCompostoFormatado) {
		this.servicoCompostoFormatado = servicoCompostoFormatado;
	}
	
	/**
	 * Get: tipoServicoFormatado.
	 *
	 * @return tipoServicoFormatado
	 */
	public String getTipoServicoFormatado() {
		return tipoServicoFormatado;
	}
	
	/**
	 * Set: tipoServicoFormatado.
	 *
	 * @param tipoServicoFormatado the tipo servico formatado
	 */
	public void setTipoServicoFormatado(String tipoServicoFormatado) {
		this.tipoServicoFormatado = tipoServicoFormatado;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Get: codModalidadeServico.
	 *
	 * @return codModalidadeServico
	 */
	public int getCodModalidadeServico() {
		return codModalidadeServico;
	}
	
	/**
	 * Set: codModalidadeServico.
	 *
	 * @param codModalidadeServico the cod modalidade servico
	 */
	public void setCodModalidadeServico(int codModalidadeServico) {
		this.codModalidadeServico = codModalidadeServico;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: codNaturezaServico.
	 *
	 * @return codNaturezaServico
	 */
	public int getCodNaturezaServico() {
		return codNaturezaServico;
	}
	
	/**
	 * Set: codNaturezaServico.
	 *
	 * @param codNaturezaServico the cod natureza servico
	 */
	public void setCodNaturezaServico(int codNaturezaServico) {
		this.codNaturezaServico = codNaturezaServico;
	}
	
	/**
	 * Get: codRelacionamentoProd.
	 *
	 * @return codRelacionamentoProd
	 */
	public int getCodRelacionamentoProd() {
		return codRelacionamentoProd;
	}
	
	/**
	 * Set: codRelacionamentoProd.
	 *
	 * @param codRelacionamentoProd the cod relacionamento prod
	 */
	public void setCodRelacionamentoProd(int codRelacionamentoProd) {
		this.codRelacionamentoProd = codRelacionamentoProd;
	}

	/**
	 * Get: codServicoComposto.
	 *
	 * @return codServicoComposto
	 */
	public long getCodServicoComposto() {
		return codServicoComposto;
	}
	
	/**
	 * Set: codServicoComposto.
	 *
	 * @param codServicoComposto the cod servico composto
	 */
	public void setCodServicoComposto(long codServicoComposto) {
		this.codServicoComposto = codServicoComposto;
	}
	
	/**
	 * Get: codTipoServico.
	 *
	 * @return codTipoServico
	 */
	public int getCodTipoServico() {
		return codTipoServico;
	}
	
	/**
	 * Set: codTipoServico.
	 *
	 * @param codTipoServico the cod tipo servico
	 */
	public void setCodTipoServico(int codTipoServico) {
		this.codTipoServico = codTipoServico;
	}
	
	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}
	
	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}
	
	/**
	 * Get: dsServicoComposto.
	 *
	 * @return dsServicoComposto
	 */
	public String getDsServicoComposto() {
		return dsServicoComposto;
	}
	
	/**
	 * Set: dsServicoComposto.
	 *
	 * @param dsServicoComposto the ds servico composto
	 */
	public void setDsServicoComposto(String dsServicoComposto) {
		this.dsServicoComposto = dsServicoComposto;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	

	



}
