/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantercontrato;

import java.io.OutputStream;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasRelacionadasParaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasRelacionadasParaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServModEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.*;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoSaidaDTO;

import com.lowagie.text.DocumentException;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterContrato
 * </p>.
 *
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterContratoService {

	/**
	 * Alterar dados basico contrato.
	 * 
	 * @param alterarDadosBasicoContratoEntradaDTO
	 *            the alterar dados basico contrato entrada dto
	 * @return the alterar dados basico contrato saida dto
	 */
	AlterarDadosBasicoContratoSaidaDTO alterarDadosBasicoContrato(
			AlterarDadosBasicoContratoEntradaDTO alterarDadosBasicoContratoEntradaDTO);

	/**
	 * Consultar dados basico contrato.
	 * 
	 * @param consultarDadosBasicoContratoEntradaDTO
	 *            the consultar dados basico contrato entrada dto
	 * @return the consultar dados basico contrato saida dto
	 */
	ConsultarDadosBasicoContratoSaidaDTO consultarDadosBasicoContrato(
			ConsultarDadosBasicoContratoEntradaDTO consultarDadosBasicoContratoEntradaDTO);

	/**
	 * Encerrar contrato.
	 * 
	 * @param encerrarContratoEntradaDTO
	 *            the encerrar contrato entrada dto
	 * @return the encerrar contrato saida dto
	 */
	EncerrarContratoSaidaDTO encerrarContrato(
			EncerrarContratoEntradaDTO encerrarContratoEntradaDTO);

	/**
	 * Recuperar contrato.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the recuperar contrato saida dto
	 */
	RecuperarContratoSaidaDTO recuperarContrato(
			RecuperarContratoEntradaDTO entradaDTO);

	/**
	 * Listar participantes.
	 * 
	 * @param listarParticipantesEntradaDTO
	 *            the listar participantes entrada dto
	 * @return the list< listar participantes saida dt o>
	 */
	List<ListarParticipantesSaidaDTO> listarParticipantes(
			ListarParticipantesEntradaDTO listarParticipantesEntradaDTO);
	
	List<ListarParticipantesSaidaDTO> listarParticipantesSemLoop(
			ListarParticipantesEntradaDTO listarParticipantesEntradaDTO);

	/**
	 * Incluir participante contrato pgit.
	 *
	 * @param EntradaDTO the Entrada dto
	 * @param listaContasParaInclusao the lista contas para inclusao
	 * @return the incluir participante contrato pgit saida dto
	 */
	IncluirParticipanteContratoPgitSaidaDTO incluirParticipanteContratoPgit(
			IncluirParticipanteContratoPgitEntradaDTO EntradaDTO, 
			List<ListarContasPossiveisInclusaoSaidaDTO> listaContasParaInclusao);

	/**
	 * Excluir participante contrato pgit.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the excluir participante contrato pgit saida dto
	 */
	ExcluirParticipanteContratoPgitSaidaDTO excluirParticipanteContratoPgit(
			ExcluirParticipanteContratoPgitEntradaDTO entradaDTO);

	/**
	 * Listar contas vinculadas contrato.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar contas vinc cont saida dt o>
	 */
	List<ListarContasVincContSaidaDTO> listarContasVinculadasContrato(
			ListarContasVincContEntradaDTO entrada);

	/**
	 * Listar contas relacionadas.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar contas rel conta saida dt o>
	 */
	List<ListarContasRelContaSaidaDTO> listarContasRelacionadas(
			ListarContasRelContaEntradaDTO entrada);

	/**
	 * Consultar manutencao participante.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< consultar man participante saida dt o>
	 */
	List<ConsultarManParticipanteSaidaDTO> consultarManutencaoParticipante(
			ConsultarManParticipanteEntradaDTO entrada);

	/**
	 * Listar contas relacionadas hist.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the list< listar contas relacionadas hist saida dt o>
	 */
	List<ListarContasRelacionadasHistSaidaDTO> listarContasRelacionadasHist(
			ListarContasRelacionadasHistEntradaDTO entradaDTO);

	/**
	 * Alterar participante contrato pgit.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the alterar participante contrato pgit saida dto
	 */
	AlterarParticipanteContratoPgitSaidaDTO alterarParticipanteContratoPgit(
			AlterarParticipanteContratoPgitEntradaDTO entradaDTO);

	// Layouts
	/**
	 * Consultar lista layout arquivo contrato.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the list< consultar lista layout arquivo contrato saida dt o>
	 */
	List<ConsultarListaLayoutArquivoContratoSaidaDTO> consultarListaLayoutArquivoContrato(
			ConsultarListaLayoutArquivoContratoEntradaDTO entradaDTO);

	/**
	 * Consultar lista tipo retorno layout.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the list< consultar lista tipo retorno layout saida dt o>
	 */
	List<ConsultarListaTipoRetornoLayoutSaidaDTO> consultarListaTipoRetornoLayout(
			ConsultarListaTipoRetornoLayoutEntradaDTO entradaDTO);

	/**
	 * Consultar lista tipo retorno layout.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the list< consultar lista tipo retorno layout saida dt o>
	 */
	List<ListarContasRelacionadasParaContratoSaidaDTO> listarContasRelacionadasContrato(
			ListarContasRelacionadasParaContratoEntradaDTO entradaDTO);

	/**
	 * Incluir tipo retorno layout.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the incluir tipo retorno layout saida dto
	 */
	IncluirTipoRetornoLayoutSaidaDTO incluirTipoRetornoLayout(
			IncluirTipoRetornoLayoutEntradaDTO entradaDTO);

	/**
	 * Listar con man layout.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar con man layout saida dt o>
	 */
	List<ListarConManLayoutSaidaDTO> listarConManLayout(
			ListarConManLayoutEntradaDTO entrada);

	/**
	 * Listar historico layout contrato.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the list< listar historico layout contrato saida dt o>
	 */
	List<ListarHistoricoLayoutContratoSaidaDTO> listarHistoricoLayoutContrato(
			ListarHistoricoLayoutContratoEntradaDTO entradaDTO);

	/**
	 * Consultar con man layout.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the consultar con man layout saida dto
	 */
	ConsultarConManLayoutSaidaDTO consultarConManLayout(
			ConsultarConManLayoutEntradaDTO entrada);

	/**
	 * Incluir layout.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the incluir layout saida dto
	 */
	IncluirLayoutSaidaDTO incluirLayout(IncluirLayoutEntradaDTO entrada);

	/**
	 * Excluir layout.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the excluir layout saida dto
	 */
	ExcluirLayoutSaidaDTO excluirLayout(ExcluirLayoutEntradaDTO entrada);

	/**
	 * Detalhar historico layout contrato.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the detalhar historico layout contrato saida dto
	 */
	DetalharHistoricoLayoutContratoSaidaDTO detalharHistoricoLayoutContrato(
			DetalharHistoricoLayoutContratoEntradaDTO entradaDTO);

	/**
	 * Alterar configuracao layout arq contrato.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the alterar configuracao layout arq contrato saida dto
	 */
	AlterarConfiguracaoLayoutArqContratoSaidaDTO alterarConfiguracaoLayoutArqContrato(
			ConfiguracaoLayoutArquivoContratoEntradaDto entradaDTO);

	/**
	 * Incluir conta contrato.
	 * 
	 * @param incluir
	 *            the incluir
	 * @return the incluir conta contrato saida dto
	 */
	IncluirContaContratoSaidaDTO incluirContaContrato(
			IncluirContaContratoEntradaDTO incluir);

	/**
	 * Alterar configuracao contra contrato.
	 * 
	 * @param alterar
	 *            the alterar
	 * @return the alterar conf conta cont saida dto
	 */
	AlterarConfContaContSaidaDTO alterarConfiguracaoContraContrato(
			AlterarConfContaContEntradaDTO alterar);

	/**
	 * Incluir relacionamento conta contrato.
	 * 
	 * @param incluir
	 *            the incluir
	 * @return the incluir relac conta cont saida dto
	 */
	IncluirRelacContaContSaidaDTO incluirRelacionamentoContaContrato(
			IncluirRelacContaContEntradaDTO incluir);

	/**
	 * Excluir conta contrato.
	 * 
	 * @param excluir
	 *            the excluir
	 * @return the excluir conta contrato saida dto
	 */
	ExcluirContaContratoSaidaDTO excluirContaContrato(
			ExcluirContaContratoEntradaDTO excluir);

	/**
	 * Substituir relacionamento conta contrato.
	 * 
	 * @param substituir
	 *            the substituir
	 * @return the subs relac conta contrato saida dto
	 */
	SubsRelacContaContratoSaidaDTO substituirRelacionamentoContaContrato(
			SubsRelacContaContratoEntradaDTO substituir);

	/**
	 * Consultar conta contrato.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the consultar conta contrato saida dto
	 */
	ConsultarContaContratoSaidaDTO consultarContaContrato(
			ConsultarContaContratoEntradaDTO entrada);

	/**
	 * Listar con man tarifa.
	 * 
	 * @param listaConManTarifaEntradaDTO
	 *            the lista con man tarifa entrada dto
	 * @return the list< listar con man tarifa saida dt o>
	 */
	List<ListarConManTarifaSaidaDTO> listarConManTarifa(
			ListarConManTarifaEntradaDTO listaConManTarifaEntradaDTO);

	/**
	 * Listar hist cond cobr reajuste.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar con man servico saida dt o>
	 */
	List<ListarConManServicoSaidaDTO> listarHistCondCobrReajuste(
			ListarConManServicoEntradaDTO entrada);

	/**
	 * Validar qtd dias cobranca aps apuracao.
	 * 
	 * @param validarQtdDiasCobrancaApsApuracaoEntradaDTO
	 *            the validar qtd dias cobranca aps apuracao entrada dto
	 * @return the validar qtd dias cobranca aps apuracao saida dto
	 */
	ValidarQtdDiasCobrancaApsApuracaoSaidaDTO validarQtdDiasCobrancaApsApuracao(
			ValidarQtdDiasCobrancaApsApuracaoEntradaDTO validarQtdDiasCobrancaApsApuracaoEntradaDTO);

	/**
	 * Alterar cond cob tarifa.
	 * 
	 * @param alterarCondCobTarifaEntradaDTO
	 *            the alterar cond cob tarifa entrada dto
	 * @return the alterar cond cob tarifa saida dto
	 */
	AlterarCondCobTarifaSaidaDTO alterarCondCobTarifa(
			AlterarCondCobTarifaEntradaDTO alterarCondCobTarifaEntradaDTO);

	/**
	 * Alterar cond reajuste tarifa.
	 * 
	 * @param alterarCondReajusteTarifaEntradaDTO
	 *            the alterar cond reajuste tarifa entrada dto
	 * @return the alterar cond reajuste tarifa saida dto
	 */
	AlterarCondReajusteTarifaSaidaDTO alterarCondReajusteTarifa(
			AlterarCondReajusteTarifaEntradaDTO alterarCondReajusteTarifaEntradaDTO);

	/**
	 * Consultar cond cob tarifa.
	 * 
	 * @param consultarCondCobTarifaEntradaDTO
	 *            the consultar cond cob tarifa entrada dto
	 * @return the consultar cond cob tarifa saida dto
	 */
	ConsultarCondCobTarifaSaidaDTO consultarCondCobTarifa(
			ConsultarCondCobTarifaEntradaDTO consultarCondCobTarifaEntradaDTO);

	/**
	 * Consultar cond reajuste tarifa.
	 * 
	 * @param consultarCondReajusteTarifaEntradaDTO
	 *            the consultar cond reajuste tarifa entrada dto
	 * @return the consultar cond reajuste tarifa saida dto
	 */
	ConsultarCondReajusteTarifaSaidaDTO consultarCondReajusteTarifa(
			ConsultarCondReajusteTarifaEntradaDTO consultarCondReajusteTarifaEntradaDTO);

	/**
	 * Consultar tarifa contrato.
	 * 
	 * @param consultarTarifaContratoEntradaDTO
	 *            the consultar tarifa contrato entrada dto
	 * @return the list< consultar tarifa contrato saida dt o>
	 */
	List<ConsultarTarifaContratoSaidaDTO> consultarTarifaContrato(
			ConsultarTarifaContratoEntradaDTO consultarTarifaContratoEntradaDTO);
	
	/**
	 * Consultar tarifa contrato loop.
	 * 
	 * @param consultarTarifaContratoEntradaDTO
	 *            the consultar tarifa contrato entrada dto
	 * @return the list< consultar tarifa contrato saida dt o>
	 */
	List<ConsultarTarifaContratoSaidaDTO> consultarTarifaContratoLoop(
			ConsultarTarifaContratoEntradaDTO consultarTarifaContratoEntradaDTO);
	


	/**
	 * Listar tarifas contrato.
	 * 
	 * @param listarTarifasContratoEntradaDTO
	 *            the listar tarifas contrato entrada dto
	 * @return the list< listar tarifas contrato saida dt o>
	 */
	List<ListarTarifasContratoSaidaDTO> listarTarifasContrato(
			ListarTarifasContratoEntradaDTO listarTarifasContratoEntradaDTO);

	/**
	 * Consultar con man tarifas.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the consultar con man tarifas saida dto
	 */
	ConsultarConManTarifasSaidaDTO consultarConManTarifas(
			ConsultarConManTarifasEntradaDTO entrada);

	/**
	 * Listar con man conta vinculada historico.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar con man conta vinculada saida dt o>
	 */
	List<ListarConManContaVinculadaSaidaDTO> listarConManContaVinculadaHistorico(
			ListarConManContaVinculadaEntradaDTO entrada);

	/**
	 * Consultar con man conta historico.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the consultar con man conta saida dto
	 */
	ConsultarConManContaSaidaDTO consultarConManContaHistorico(
			ConsultarConManContaEntradaDTO entrada);

	/**
	 * Consultar con man participantes.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the consultar con man participantes saida dto
	 */
	ConsultarConManParticipantesSaidaDTO consultarConManParticipantes(
			ConsultarConManParticipantesEntradaDTO entradaDTO);

	/**
	 * Listar con man participante.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the list< listar con man participante saida dt o>
	 */
	List<ListarConManParticipanteSaidaDTO> listarConManParticipante(
			ListarConManParticipanteEntradaDTO entradaDTO);

	/**
	 * Consultar layout arquivo contrato.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the consultar layout arquivo contrato saida dto
	 */
	ConsultarLayoutArquivoContratoSaidaDTO consultarLayoutArquivoContrato(
			ConsultarLayoutArquivoContratoEntradaDTO entradaDTO);

	/**
	 * Listar contas possiveis inclusao.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar contas possiveis inclusao saida dt o>
	 */
	List<ListarContasPossiveisInclusaoSaidaDTO> listarContasPossiveisInclusao(
			ListarContasPossiveisInclusaoEntradaDTO entrada);

	/**
	 * Alterar tipo retorno layout.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the alterar tipo retorno layout saida dto
	 */
	AlterarTipoRetornoLayoutSaidaDTO alterarTipoRetornoLayout(
			AlterarTipoRetornoLayoutEntradaDTO entradaDTO);

	/**
	 * Excluir tipo retorno layout.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the excluir tipo retorno layout saida dto
	 */
	ExcluirTipoRetornoLayoutSaidaDTO excluirTipoRetornoLayout(
			ExcluirTipoRetornoLayoutEntradaDTO entradaDTO);

	/**
	 * Detalhar tipo retorno layout.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the detalhar tipo retorno layout saida dto
	 */
	DetalharTipoRetornoLayoutSaidaDTO detalharTipoRetornoLayout(
			DetalharTipoRetornoLayoutEntradaDTO entradaDTO);

	/**
	 * Listar con man dados basicos.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar con man dados basicos saida dt o>
	 */
	List<ListarConManDadosBasicosSaidaDTO> listarConManDadosBasicos(
			ListarConManDadosBasicosEntradaDTO entrada);

	/**
	 * Consultar con man dados basicos.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the consultar con man dados basicos saida dto
	 */
	ConsultarConManDadosBasicosSaidaDTO consultarConManDadosBasicos(
			ConsultarConManDadosBasicosEntradaDTO entrada);

	/**
	 * Bloquear desbloquear contrato pgit.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the bloquear desbloquear contrato pgit saida dto
	 */
	BloquearDesbloquearContratoPgitSaidaDTO bloquearDesbloquearContratoPgit(
			BloquearDesbloquearContratoPgitEntradaDTO entradaDTO);

	// Servicos
	/**
	 * Listar modalidade tipo servico.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar modalidade tipo servico saida dt o>
	 */
	List<ListarModalidadeTipoServicoSaidaDTO> listarModalidadeTipoServico(
			ListarModalidadeTipoServicoEntradaDTO entrada);

	/**
	 * Listar servico relacionado cdps.
	 * 
	 * @param listarServicoRelacionadoCdpsEntradaDTO
	 *            the listar servico relacionado cdps entrada dto
	 * @return the list< listar servico relacionado cdps saida dt o>
	 */
	List<ListarServicoRelacionadoCdpsSaidaDTO> listarServicoRelacionadoCdps(
			ListarServicoRelacionadoCdpsEntradaDTO listarServicoRelacionadoCdpsEntradaDTO);

	/**
	 * Detalhar tipo serv mod contrato.
	 * 
	 * @param detalharTipoServModContratoEntradaDTO
	 *            the detalhar tipo serv mod contrato entrada dto
	 * @return the detalhar tipo serv mod contrato saida dto
	 */
	DetalharTipoServModContratoSaidaDTO detalharTipoServModContrato(
			DetalharTipoServModContratoEntradaDTO detalharTipoServModContratoEntradaDTO);

	/**
	 * Incluir tipo serv mod contrato.
	 * 
	 * @param incluir
	 *            the incluir
	 * @return the incluir tipo serv mod contrato saida dto
	 */
	IncluirTipoServModContratoSaidaDTO incluirTipoServModContrato(
			IncluirTipoServModContratoEntradaDTO incluir);

	/**
	 * Excluir tipo serv mod contrato.
	 * 
	 * @param excluir
	 *            the excluir
	 * @return the excluir tipo serv mod contrato saida dto
	 */
	ExcluirTipoServModContratoSaidaDTO excluirTipoServModContrato(
			ExcluirTipoServModContratoEntradaDTO excluir);

	/**
	 * Alterar configuracao tipo serv mod contrato.
	 * 
	 * @param alterarConfiguracaoTipoServModContratoEntradaDTO
	 *            the alterar configuracao tipo serv mod contrato entrada dto
	 * @return the alterar configuracao tipo serv mod contrato saida dto
	 */
	AlterarConfiguracaoTipoServModContratoSaidaDTO alterarConfiguracaoTipoServModContrato(
			AlterarConfiguracaoTipoServModContratoEntradaDTO 
			alterarConfiguracaoTipoServModContratoEntradaDTO);

	/**
	 * Listar con man servico.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar con man servico saida dt o>
	 */
	List<ListarConManServicoSaidaDTO> listarConManServico(
			ListarConManServicoEntradaDTO entrada);

	/**
	 * Consultar con man servico.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the consultar con man servicos saida dto
	 */
	ConsultarConManServicosSaidaDTO consultarConManServico(
			ConsultarConManServicosEntradaDTO entrada);

	/**
	 * Alterar layout servico.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the alterar layout servico saida dto
	 */
	AlterarLayoutServicoSaidaDTO alterarLayoutServico(
			AlterarLayoutServicoEntradaDTO entrada);

	/**
	 * Listar tipo layout contrato.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar tipo layout contrato saida dt o>
	 */
	List<ListarTipoLayoutContratoSaidaDTO> listarTipoLayoutContrato(
			ListarTipoLayoutContratoEntradaDTO entrada);

	/**
	 * Detalhar conta relacionada hist.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the detalhar conta relacionada hist saida dto
	 */
	DetalharContaRelacionadaHistSaidaDTO detalharContaRelacionadaHist(
			DetalharContaRelacionadaHistEntradaDTO entradaDTO);

	/**
	 * Listar contas vinculadas contrato cinquenta.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar contas vinc cont saida dt o>
	 */
	List<ListarContasVincContSaidaDTO> listarContasVinculadasContratoCinquenta(
			ListarContasVincContEntradaDTO entrada);

	/**
	 * Gerar manter contrato.
	 * 
	 * @param saidaImprimirContrato
	 *            the saida imprimir contrato
	 * @param outputStream
	 *            the output stream
	 * @throws DocumentException
	 *             the document exception
	 */
	void gerarManterContrato(ImprimirContratoSaidaDTO saidaImprimirContrato,
			OutputStream outputStream) throws DocumentException;

	/**
	 * Listar participantes contrato.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar participantes contrato saida dt o>
	 */
	List<ListarParticipantesContratoSaidaDTO> listarParticipantesContrato(
			ListarParticipantesContratoEntradaDTO entrada);

	/**
	 * Alterar representante contrato pgit.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the alterar representante contrato pgit saida dto
	 */
	AlterarRepresentanteContratoPgitSaidaDTO alterarRepresentanteContratoPgit(
			AlterarRepresentanteContratoPgitEntradaDTO entrada);

	/**
	 * Excluir relaci assoc.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the excluir relaci assoc saida dto
	 */
	ExcluirRelaciAssocSaidaDTO excluirRelaciAssoc(
			ExcluirRelaciAssocEntradaDTO entrada);

	/**
	 * Verificar atributo servico modalidade.
	 * 
	 * @param entradaVerificarAtributo
	 *            the entrada verificar atributo
	 * @return the verificar atributos servico modalidade saida dto
	 */
	VerificarAtributosServicoModalidadeSaidaDTO verificarAtributoServicoModalidade(
			VerificarAtributosServicoModalidadeEntradaDTO entradaVerificarAtributo);

	/**
	 * Listar tipo servicos relacionados incluir.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar servicos saida dt o>
	 */
	List<ListarServicosSaidaDTO> listarTipoServicosRelacionadosIncluir(
			ListarServModEntradaDTO entrada);

	/**
	 * Listar tipo modalidades relacionadas incluir.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar servico relacionado cdps saida dt o>
	 */
	List<ListarServicoRelacionadoCdpsSaidaDTO> listarTipoModalidadesRelacionadasIncluir(
			ListarServModEntradaDTO entrada);

	/**
	 * Listar participante agregado.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar participante agregado saida dt o>
	 */
	List<ListarParticipanteAgregadoSaidaDTO> listarParticipanteAgregado(
			ListarParticipanteAgregadoEntradaDTO entrada);

	/**
	 * Listar agregado.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar agregado saida dt o>
	 */
	List<ListarAgregadoSaidaDTO> listarAgregado(ListarAgregadoEntradaDTO entrada);

	/**
	 * Consultar lista operacao tarifa tipo servico.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the lista operacao tarifa tipo servico saida dto
	 */
	ListaOperacaoTarifaTipoServicoSaidaDTO consultarListaOperacaoTarifaTipoServico(
			ListaOperacaoTarifaTipoServicoEntradaDTO entradaDTO);

	/**
	 * Validar proposta andamento.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the validar proposta andamento saida dto
	 */
	ValidarPropostaAndamentoSaidaDTO validarPropostaAndamento(
			ValidarPropostaAndamentoEntradaDTO entrada);

	/**
	 * Consultar con man amb part.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the consultar con man amb part saida dto
	 */
	ConsultarConManAmbPartSaidaDTO consultarConManAmbPart(
			ConsultarConManAmbPartEntradaDTO entrada);

	/**
	 * Listar con man amb part.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the list< listar con man amb part ocorrenciat saida dt o>
	 */
	List<ListarConManAmbPartOcorrenciatSaidaDTO> listarConManAmbPart(
			ListarConManAmbPartEntradaDTO entrada);

	/**
	 * Alterar tarifa acima padrao.
	 * 
	 * @param entradaDTO
	 *            the entrada dto
	 * @return the alterar tarifa acima padrao saida dto
	 */
	AlterarTarifaAcimaPadraoSaidaDTO alterarTarifaAcimaPadrao(
			AlterarTarifaAcimaPadraoEntradaDTO entradaDTO);

	/**
	 * Alterar agencia gestora.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the alterar agencia gestora saida dto
	 */
	AlterarAgenciaGestoraSaidaDTO alterarAgenciaGestora(
			AlterarAgenciaGestoraEntradaDTO entrada);

	/**
	 * consultar Conta Deb Tarifa.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the consultar Conta Deb Tarifa saida dto
	 */
	ConsultarContaDebTarifaSaidaDTO consultarContaDebTarifa(
			ConsultarContaDebTarifaEntradaDTO entrada);

	/**
	 * detalhar Historico Deb Tarifa.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the detalhar Historico Deb Tarifa saida dto
	 */
	DetalharHistoricoDebTarifaSaidaDTO detalharHistoricoDebTarifa(
			DetalharHistoricoDebTarifaEntradaDTO entrada);

	/**
	 * incluir Conta Deb Tarifa.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the Mensagem Saida
	 */
	MensagemSaida incluirContaDebTarifa(IncluirContaDebTarifaEntradaDTO entrada);

	/**
	 * listar Conta Deb Tarifa.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the Listar Conta Deb Tarifa saida dto
	 */
	ListarContaDebTarifaSaidaDTO listarContaDebTarifa(
			ListarContaDebTarifaEntradaDTO entrada);

	/**
	 * listar Historico Deb Tarifa.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the listar Historico Deb Tarifa saida dto
	 */
	ListarHistoricoDebTarifaSaidaDTO listarHistoricoDebTarifa(
			ListarHistoricoDebTarifaEntradaDTO entrada);

	/**
	 * listar Operacao Conta Deb Tarifa.
	 * 
	 * @param entrada
	 *            the entrada
	 * @return the listar Operacao Conta Deb Tarifa saida dto
	 */
	ListarOperacaoContaDebTarifaSaidaDTO listarOperacaoContaDebTarifa(
			ListarOperacaoContaDebTarifaEntradaDTO entrada);
	
	/**
	 * listar Representantes para inclus�o de servi�o de comprovante de pagamento.
	 *
	 * @param entrada the entrada
	 * @return the Representante saida dto
	 */
	ListarRepresentanteSaidaDTO listarRepresentantesComprovante(ListarRepresentanteEntradaDTO entrada);
	
	/**
	 * incluir Representantes para de servi�o de comprovante de pagamento.
	 *
	 * @param entrada the entrada
	 * @return the Incluir Representante saida dto
	 */
	IncluirRepresentanteSaidaDTO incluirRepresentantesComprovante(IncluirRepresentanteEntradaDTO entrada);
	
	/**
	 * validar Usuario.
	 *
	 * @param entrada the entrada
	 * @return the Validar Usuario Entrada DTO
	 */
	ValidarUsuarioSaidaDTO validarUsuario(ValidarUsuarioEntradaDTO entrada);
	
    /**
     * validar Vinc Convenio Cta Salario.
     *
     * @param entrada the entrada
     * @return the ValidarVinculacaoConvenioContaSalarioEntradaDTO
     */
    ValidarVinculacaoConvenioContaSalarioSaidaDTO validarVincConvenioCtaSalario(
        ValidarVinculacaoConvenioContaSalarioEntradaDTO entrada);

    /**
     * Verificar atributos dados basicos.
     * 
     * @param entrada
     *            the entrada
     * @return the verificar atributos dados basicos saida dto
     */
    VerificarAtributosDadosBasicosSaidaDto verificarAtributosDadosBasicos(
        VerificarAtributosDadosBasicosEntradaDto entrada);
    
    /**
     * Listar contas exclusao.
     *
     * @param entrada the entrada
     * @return the listar contas exclusao saida dto
     */
    ListarContasExclusaoSaidaDTO listarContasExclusao(ListarContasExclusaoEntradaDTO entrada);
}