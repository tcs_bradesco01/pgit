package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

public class ValidarVinculacaoConvenioContaSalarioEntradaDTO {
	                                           
	 private Long cdpessoaJuridicaContrato;
     private Integer cdTipoContratoNegocio;
     private Long nrSequenciaContratoNegocio;
     private Long cdClubRepresentante;
     private Long cdCpfCnpjRepresentante;
     private Integer cdFilialCnpjRepresentante;
     private Integer cdControleCpfRepresentante;
     private Long cdPessoaJuridicaConta;
     private Integer cdTipoContratoConta;
     private Long nrSequenciaContratoConta;
     private Integer cdInstituicaoBancaria;
     private Integer cdUnidadeOrganizacional;
     private Integer nrConta;
     private String nrContaDigito;
     private Integer cdIndicadorNumConve;
     private Long cdConveCtaSalarial;
     private BigDecimal vlPagamentoMes;
     private Integer qtdFuncionarioFlPgto;
     private BigDecimal vlMediaSalarialMes;
     private Integer cdPagamentoSalarialMes;
     private Integer cdPagamentoSalarialQzena;
     private Integer cdLocalContaSalarial;
     private Integer dtReftFlPgto;
     
	/**
	 * @return the cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	/**
	 * @param cdpessoaJuridicaContrato the cdpessoaJuridicaContrato to set
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	/**
	 * @return the cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	/**
	 * @param cdTipoContratoNegocio the cdTipoContratoNegocio to set
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	/**
	 * @return the nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	/**
	 * @param nrSequenciaContratoNegocio the nrSequenciaContratoNegocio to set
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	/**
	 * @return the cdClubRepresentante
	 */
	public Long getCdClubRepresentante() {
		return cdClubRepresentante;
	}
	/**
	 * @param cdClubRepresentante the cdClubRepresentante to set
	 */
	public void setCdClubRepresentante(Long cdClubRepresentante) {
		this.cdClubRepresentante = cdClubRepresentante;
	}
	/**
	 * @return the cdCpfCnpjRepresentante
	 */
	public Long getCdCpfCnpjRepresentante() {
		return cdCpfCnpjRepresentante;
	}
	/**
	 * @param cdCpfCnpjRepresentante the cdCpfCnpjRepresentante to set
	 */
	public void setCdCpfCnpjRepresentante(Long cdCpfCnpjRepresentante) {
		this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
	}
	/**
	 * @return the cdFilialCnpjRepresentante
	 */
	public Integer getCdFilialCnpjRepresentante() {
		return cdFilialCnpjRepresentante;
	}
	/**
	 * @param cdFilialCnpjRepresentante the cdFilialCnpjRepresentante to set
	 */
	public void setCdFilialCnpjRepresentante(Integer cdFilialCnpjRepresentante) {
		this.cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
	}
	/**
	 * @return the cdControleCpfRepresentante
	 */
	public Integer getCdControleCpfRepresentante() {
		return cdControleCpfRepresentante;
	}
	/**
	 * @param cdControleCpfRepresentante the cdControleCpfRepresentante to set
	 */
	public void setCdControleCpfRepresentante(Integer cdControleCpfRepresentante) {
		this.cdControleCpfRepresentante = cdControleCpfRepresentante;
	}
	/**
	 * @return the cdPessoaJuridicaConta
	 */
	public Long getCdPessoaJuridicaConta() {
		return cdPessoaJuridicaConta;
	}
	/**
	 * @param cdPessoaJuridicaConta the cdPessoaJuridicaConta to set
	 */
	public void setCdPessoaJuridicaConta(Long cdPessoaJuridicaConta) {
		this.cdPessoaJuridicaConta = cdPessoaJuridicaConta;
	}
	/**
	 * @return the cdTipoContratoConta
	 */
	public Integer getCdTipoContratoConta() {
		return cdTipoContratoConta;
	}
	/**
	 * @param cdTipoContratoConta the cdTipoContratoConta to set
	 */
	public void setCdTipoContratoConta(Integer cdTipoContratoConta) {
		this.cdTipoContratoConta = cdTipoContratoConta;
	}
	/**
	 * @return the nrSequenciaContratoConta
	 */
	public Long getNrSequenciaContratoConta() {
		return nrSequenciaContratoConta;
	}
	/**
	 * @param nrSequenciaContratoConta the nrSequenciaContratoConta to set
	 */
	public void setNrSequenciaContratoConta(Long nrSequenciaContratoConta) {
		this.nrSequenciaContratoConta = nrSequenciaContratoConta;
	}
	/**
	 * @return the cdInstituicaoBancaria
	 */
	public Integer getCdInstituicaoBancaria() {
		return cdInstituicaoBancaria;
	}
	/**
	 * @param cdInstituicaoBancaria the cdInstituicaoBancaria to set
	 */
	public void setCdInstituicaoBancaria(Integer cdInstituicaoBancaria) {
		this.cdInstituicaoBancaria = cdInstituicaoBancaria;
	}
	/**
	 * @return the cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}
	/**
	 * @param cdUnidadeOrganizacional the cdUnidadeOrganizacional to set
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}
	/**
	 * @return the nrConta
	 */
	public Integer getNrConta() {
		return nrConta;
	}
	/**
	 * @param nrConta the nrConta to set
	 */
	public void setNrConta(Integer nrConta) {
		this.nrConta = nrConta;
	}
	/**
	 * @return the nrContaDigito
	 */
	public String getNrContaDigito() {
		return nrContaDigito;
	}
	/**
	 * @param nrContaDigito the nrContaDigito to set
	 */
	public void setNrContaDigito(String nrContaDigito) {
		this.nrContaDigito = nrContaDigito;
	}
	/**
	 * @return the cdIndicadorNumConve
	 */
	public Integer getCdIndicadorNumConve() {
		return cdIndicadorNumConve;
	}
	/**
	 * @param cdIndicadorNumConve the cdIndicadorNumConve to set
	 */
	public void setCdIndicadorNumConve(Integer cdIndicadorNumConve) {
		this.cdIndicadorNumConve = cdIndicadorNumConve;
	}
	/**
	 * @return the cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}
	/**
	 * @param cdConveCtaSalarial the cdConveCtaSalarial to set
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}
	/**
	 * @return the vlPagamentoMes
	 */
	public BigDecimal getVlPagamentoMes() {
		return vlPagamentoMes;
	}
	/**
	 * @param vlPagamentoMes the vlPagamentoMes to set
	 */
	public void setVlPagamentoMes(BigDecimal vlPagamentoMes) {
		this.vlPagamentoMes = vlPagamentoMes;
	}
	/**
	 * @return the qtdFuncionarioFlPgto
	 */
	public Integer getQtdFuncionarioFlPgto() {
		return qtdFuncionarioFlPgto;
	}
	/**
	 * @param qtdFuncionarioFlPgto the qtdFuncionarioFlPgto to set
	 */
	public void setQtdFuncionarioFlPgto(Integer qtdFuncionarioFlPgto) {
		this.qtdFuncionarioFlPgto = qtdFuncionarioFlPgto;
	}
	/**
	 * @return the vlMediaSalarialMes
	 */
	public BigDecimal getVlMediaSalarialMes() {
		return vlMediaSalarialMes;
	}
	/**
	 * @param vlMediaSalarialMes the vlMediaSalarialMes to set
	 */
	public void setVlMediaSalarialMes(BigDecimal vlMediaSalarialMes) {
		this.vlMediaSalarialMes = vlMediaSalarialMes;
	}
	/**
	 * @return the cdPagamentoSalarialMes
	 */
	public Integer getCdPagamentoSalarialMes() {
		return cdPagamentoSalarialMes;
	}
	/**
	 * @param cdPagamentoSalarialMes the cdPagamentoSalarialMes to set
	 */
	public void setCdPagamentoSalarialMes(Integer cdPagamentoSalarialMes) {
		this.cdPagamentoSalarialMes = cdPagamentoSalarialMes;
	}
	/**
	 * @return the cdPagamentoSalarialQzena
	 */
	public Integer getCdPagamentoSalarialQzena() {
		return cdPagamentoSalarialQzena;
	}
	/**
	 * @param cdPagamentoSalarialQzena the cdPagamentoSalarialQzena to set
	 */
	public void setCdPagamentoSalarialQzena(Integer cdPagamentoSalarialQzena) {
		this.cdPagamentoSalarialQzena = cdPagamentoSalarialQzena;
	}
	/**
	 * @return the cdLocalContaSalarial
	 */
	public Integer getCdLocalContaSalarial() {
		return cdLocalContaSalarial;
	}
	/**
	 * @param cdLocalContaSalarial the cdLocalContaSalarial to set
	 */
	public void setCdLocalContaSalarial(Integer cdLocalContaSalarial) {
		this.cdLocalContaSalarial = cdLocalContaSalarial;
	}
	/**
	 * @return the dtReftFlPgto
	 */
	public Integer getDtReftFlPgto() {
		return dtReftFlPgto;
	}
	/**
	 * @param dtReftFlPgto the dtReftFlPgto to set
	 */
	public void setDtReftFlPgto(Integer dtReftFlPgto) {
		this.dtReftFlPgto = dtReftFlPgto;
	}
                                
}
