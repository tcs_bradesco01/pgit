/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarListaFuncionarioBradescoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarListaFuncionarioBradescoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdCriptografadoFuncionarioBradesco
     */
    private long _cdCriptografadoFuncionarioBradesco = 0;

    /**
     * keeps track of state for field:
     * _cdCriptografadoFuncionarioBradesco
     */
    private boolean _has_cdCriptografadoFuncionarioBradesco;

    /**
     * Field _dsCriptografadoFuncionarioBradesco
     */
    private java.lang.String _dsCriptografadoFuncionarioBradesco;

    /**
     * Field _cdJuncaoPertencente
     */
    private int _cdJuncaoPertencente = 0;

    /**
     * keeps track of state for field: _cdJuncaoPertencente
     */
    private boolean _has_cdJuncaoPertencente;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarListaFuncionarioBradescoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.ConsultarListaFuncionarioBradescoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCriptografadoFuncionarioBradesco
     * 
     */
    public void deleteCdCriptografadoFuncionarioBradesco()
    {
        this._has_cdCriptografadoFuncionarioBradesco= false;
    } //-- void deleteCdCriptografadoFuncionarioBradesco() 

    /**
     * Method deleteCdJuncaoPertencente
     * 
     */
    public void deleteCdJuncaoPertencente()
    {
        this._has_cdJuncaoPertencente= false;
    } //-- void deleteCdJuncaoPertencente() 

    /**
     * Returns the value of field
     * 'cdCriptografadoFuncionarioBradesco'.
     * 
     * @return long
     * @return the value of field
     * 'cdCriptografadoFuncionarioBradesco'.
     */
    public long getCdCriptografadoFuncionarioBradesco()
    {
        return this._cdCriptografadoFuncionarioBradesco;
    } //-- long getCdCriptografadoFuncionarioBradesco() 

    /**
     * Returns the value of field 'cdJuncaoPertencente'.
     * 
     * @return int
     * @return the value of field 'cdJuncaoPertencente'.
     */
    public int getCdJuncaoPertencente()
    {
        return this._cdJuncaoPertencente;
    } //-- int getCdJuncaoPertencente() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field
     * 'dsCriptografadoFuncionarioBradesco'.
     * 
     * @return String
     * @return the value of field
     * 'dsCriptografadoFuncionarioBradesco'.
     */
    public java.lang.String getDsCriptografadoFuncionarioBradesco()
    {
        return this._dsCriptografadoFuncionarioBradesco;
    } //-- java.lang.String getDsCriptografadoFuncionarioBradesco() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdCriptografadoFuncionarioBradesco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriptografadoFuncionarioBradesco()
    {
        return this._has_cdCriptografadoFuncionarioBradesco;
    } //-- boolean hasCdCriptografadoFuncionarioBradesco() 

    /**
     * Method hasCdJuncaoPertencente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdJuncaoPertencente()
    {
        return this._has_cdJuncaoPertencente;
    } //-- boolean hasCdJuncaoPertencente() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field
     * 'cdCriptografadoFuncionarioBradesco'.
     * 
     * @param cdCriptografadoFuncionarioBradesco the value of field
     * 'cdCriptografadoFuncionarioBradesco'.
     */
    public void setCdCriptografadoFuncionarioBradesco(long cdCriptografadoFuncionarioBradesco)
    {
        this._cdCriptografadoFuncionarioBradesco = cdCriptografadoFuncionarioBradesco;
        this._has_cdCriptografadoFuncionarioBradesco = true;
    } //-- void setCdCriptografadoFuncionarioBradesco(long) 

    /**
     * Sets the value of field 'cdJuncaoPertencente'.
     * 
     * @param cdJuncaoPertencente the value of field
     * 'cdJuncaoPertencente'.
     */
    public void setCdJuncaoPertencente(int cdJuncaoPertencente)
    {
        this._cdJuncaoPertencente = cdJuncaoPertencente;
        this._has_cdJuncaoPertencente = true;
    } //-- void setCdJuncaoPertencente(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsCriptografadoFuncionarioBradesco'.
     * 
     * @param dsCriptografadoFuncionarioBradesco the value of field
     * 'dsCriptografadoFuncionarioBradesco'.
     */
    public void setDsCriptografadoFuncionarioBradesco(java.lang.String dsCriptografadoFuncionarioBradesco)
    {
        this._dsCriptografadoFuncionarioBradesco = dsCriptografadoFuncionarioBradesco;
    } //-- void setDsCriptografadoFuncionarioBradesco(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarListaFuncionarioBradescoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.ConsultarListaFuncionarioBradescoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.ConsultarListaFuncionarioBradescoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.ConsultarListaFuncionarioBradescoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.ConsultarListaFuncionarioBradescoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
