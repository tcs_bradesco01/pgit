/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class EfetuarCalcOperReceitaTarifasResponse.
 * 
 * @version $Revision$ $Date$
 */
public class EfetuarCalcOperReceitaTarifasResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _vlTarifaTotalPad
     */
    private java.math.BigDecimal _vlTarifaTotalPad = new java.math.BigDecimal("0");

    /**
     * Field _vlTarifaTotalPro
     */
    private java.math.BigDecimal _vlTarifaTotalPro = new java.math.BigDecimal("0");

    /**
     * Field _vlTarifaDesconto
     */
    private java.math.BigDecimal _vlTarifaDesconto = new java.math.BigDecimal("0");

    /**
     * Field _vlFloat
     */
    private java.math.BigDecimal _vlFloat = new java.math.BigDecimal("0");

    /**
     * Field _percentualFlexibilidade
     */
    private java.math.BigDecimal _percentualFlexibilidade = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public EfetuarCalcOperReceitaTarifasResponse() 
     {
        super();
        setVlTarifaTotalPad(new java.math.BigDecimal("0"));
        setVlTarifaTotalPro(new java.math.BigDecimal("0"));
        setVlTarifaDesconto(new java.math.BigDecimal("0"));
        setVlFloat(new java.math.BigDecimal("0"));
        setPercentualFlexibilidade(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.response.EfetuarCalcOperReceitaTarifasResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'percentualFlexibilidade'.
     * 
     * @return BigDecimal
     * @return the value of field 'percentualFlexibilidade'.
     */
    public java.math.BigDecimal getPercentualFlexibilidade()
    {
        return this._percentualFlexibilidade;
    } //-- java.math.BigDecimal getPercentualFlexibilidade() 

    /**
     * Returns the value of field 'vlFloat'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFloat'.
     */
    public java.math.BigDecimal getVlFloat()
    {
        return this._vlFloat;
    } //-- java.math.BigDecimal getVlFloat() 

    /**
     * Returns the value of field 'vlTarifaDesconto'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaDesconto'.
     */
    public java.math.BigDecimal getVlTarifaDesconto()
    {
        return this._vlTarifaDesconto;
    } //-- java.math.BigDecimal getVlTarifaDesconto() 

    /**
     * Returns the value of field 'vlTarifaTotalPad'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaTotalPad'.
     */
    public java.math.BigDecimal getVlTarifaTotalPad()
    {
        return this._vlTarifaTotalPad;
    } //-- java.math.BigDecimal getVlTarifaTotalPad() 

    /**
     * Returns the value of field 'vlTarifaTotalPro'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaTotalPro'.
     */
    public java.math.BigDecimal getVlTarifaTotalPro()
    {
        return this._vlTarifaTotalPro;
    } //-- java.math.BigDecimal getVlTarifaTotalPro() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'percentualFlexibilidade'.
     * 
     * @param percentualFlexibilidade the value of field
     * 'percentualFlexibilidade'.
     */
    public void setPercentualFlexibilidade(java.math.BigDecimal percentualFlexibilidade)
    {
        this._percentualFlexibilidade = percentualFlexibilidade;
    } //-- void setPercentualFlexibilidade(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlFloat'.
     * 
     * @param vlFloat the value of field 'vlFloat'.
     */
    public void setVlFloat(java.math.BigDecimal vlFloat)
    {
        this._vlFloat = vlFloat;
    } //-- void setVlFloat(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaDesconto'.
     * 
     * @param vlTarifaDesconto the value of field 'vlTarifaDesconto'
     */
    public void setVlTarifaDesconto(java.math.BigDecimal vlTarifaDesconto)
    {
        this._vlTarifaDesconto = vlTarifaDesconto;
    } //-- void setVlTarifaDesconto(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaTotalPad'.
     * 
     * @param vlTarifaTotalPad the value of field 'vlTarifaTotalPad'
     */
    public void setVlTarifaTotalPad(java.math.BigDecimal vlTarifaTotalPad)
    {
        this._vlTarifaTotalPad = vlTarifaTotalPad;
    } //-- void setVlTarifaTotalPad(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaTotalPro'.
     * 
     * @param vlTarifaTotalPro the value of field 'vlTarifaTotalPro'
     */
    public void setVlTarifaTotalPro(java.math.BigDecimal vlTarifaTotalPro)
    {
        this._vlTarifaTotalPro = vlTarifaTotalPro;
    } //-- void setVlTarifaTotalPro(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return EfetuarCalcOperReceitaTarifasResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.response.EfetuarCalcOperReceitaTarifasResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.response.EfetuarCalcOperReceitaTarifasResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.response.EfetuarCalcOperReceitaTarifasResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.response.EfetuarCalcOperReceitaTarifasResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
