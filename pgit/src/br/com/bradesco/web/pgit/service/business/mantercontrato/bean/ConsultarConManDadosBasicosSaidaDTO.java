/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarConManDadosBasicosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConManDadosBasicosSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo hrInclusaoRegistroHistorico. */
	private String hrInclusaoRegistroHistorico;

	/** Atributo cdSituacaoContrato. */
	private Integer cdSituacaoContrato;

	/** Atributo cdMotivoSituacaoContrato. */
	private Integer cdMotivoSituacaoContrato;

	/** Atributo cdAcaoRelacionamento. */
	private Long cdAcaoRelacionamento;

	/** Atributo cdContratoNegocio. */
	private String cdContratoNegocio;

	/** Atributo cdOrigemContrato. */
	private Integer cdOrigemContrato;

	/** Atributo hrAssinaturaContrato. */
	private String hrAssinaturaContrato;

	/** Atributo hrSituacaoContrato. */
	private String hrSituacaoContrato;

	/** Atributo cdIndicadorTipoManutencao. */
	private Integer cdIndicadorTipoManutencao;

	/** Atributo dsIndicadorTipoManutencao. */
	private String dsIndicadorTipoManutencao;

	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;

	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;

	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;

	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;

	/** Atributo nmOperacaoFluxoInclusao. */
	private String nmOperacaoFluxoInclusao;

	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;

	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;

	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;

	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;

	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;

	/** Atributo dsContratoNegocio. */
	private String dsContratoNegocio;

	/** Atributo cdIndicadorMoeda. */
	private Integer cdIndicadorMoeda;

	/** Atributo dsMoeda. */
	private String dsMoeda;

	/** Atributo cdIdentificadorIdioma. */
	private Integer cdIdentificadorIdioma;

	/** Atributo dsIdioma. */
	private String dsIdioma;

	/** Atributo dtInicioVigencia. */
	private String dtInicioVigencia;

	/** Atributo dtFimVigencia. */
	private String dtFimVigencia;

	/** Atributo dsSituacaoContrato. */
	private String dsSituacaoContrato;

	/** Atributo dsMotivoSituacaoContrato. */
	private String dsMotivoSituacaoContrato;

	/** Atributo dsOrigemContrato. */
	private String dsOrigemContrato;

	/** Atributo dsIndicadorParticipante. */
	private String dsIndicadorParticipante;

	/** Atributo dsIndicadorAditivo. */
	private String dsIndicadorAditivo;

	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;

	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;

	/** Atributo dsTipoContratoNegocio. */
	private String dsTipoContratoNegocio;

	/** Atributo nmPessoaJuridica. */
	private String nmPessoaJuridica;

	/** Atributo cdDeptoGestor. */
	private Integer cdDeptoGestor;

	/** Atributo nmDepto. */
	private String nmDepto;

	/** Atributo cdFuncionarioBradesco. */
	private Long cdFuncionarioBradesco;

	/** Atributo nmFuncionarioBradesco. */
	private String nmFuncionarioBradesco;

	/** Atributo cdSetorContratoPagamento. */
	private Integer cdSetorContratoPagamento;

	/** Atributo dsSetorContratoPagamento. */
	private String dsSetorContratoPagamento;

	/** Atributo vlSaldoVirtualTeste. */
	private BigDecimal vlSaldoVirtualTeste;
	
	/** Atributo cdPessoaJuridicaProposta. */
	private Long cdPessoaJuridicaProposta;
	
    /** Atributo cdTipoContratoProposta. */
    private Integer cdTipoContratoProposta;
    
    /** Atributo nrContratoProposta. */
    private Long nrContratoProposta;
    
    /** Atributo dsPessoaJuridicaProposta. */
    private String dsPessoaJuridicaProposta;
    
    /** Atributo dsTipoContratoProposta. */
    private String dsTipoContratoProposta; 
    
	/** Atributo nrSequenciaContratoOutros. */
	private long nrSequenciaContratoOutros;
	
	/** Atributo nrSequenciaContratoPagamentoSalario. */
	private long nrSequenciaContratoPagamentoSalario;

	/** Atributo cdFormaAutorizacaoPagamento. */
    private Integer cdFormaAutorizacaoPagamento = null;

    /** Atributo dsFormaAutorizacaoPagamento. */
    private String dsFormaAutorizacaoPagamento = null;
    
	/**
	 * Get: vlSaldoVirtualTeste.
	 *
	 * @return vlSaldoVirtualTeste
	 */
	public BigDecimal getVlSaldoVirtualTeste() {
		return vlSaldoVirtualTeste;
	}

	/**
	 * Set: vlSaldoVirtualTeste.
	 *
	 * @param vlSaldoVirtualTeste the vl saldo virtual teste
	 */
	public void setVlSaldoVirtualTeste(BigDecimal vlSaldoVirtualTeste) {
		this.vlSaldoVirtualTeste = vlSaldoVirtualTeste;
	}

	/**
	 * Get: cdAcaoRelacionamento.
	 *
	 * @return cdAcaoRelacionamento
	 */
	public Long getCdAcaoRelacionamento() {
		return cdAcaoRelacionamento;
	}

	/**
	 * Set: cdAcaoRelacionamento.
	 *
	 * @param cdAcaoRelacionamento the cd acao relacionamento
	 */
	public void setCdAcaoRelacionamento(Long cdAcaoRelacionamento) {
		this.cdAcaoRelacionamento = cdAcaoRelacionamento;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: cdContratoNegocio.
	 *
	 * @return cdContratoNegocio
	 */
	public String getCdContratoNegocio() {
		return cdContratoNegocio;
	}

	/**
	 * Set: cdContratoNegocio.
	 *
	 * @param cdContratoNegocio the cd contrato negocio
	 */
	public void setCdContratoNegocio(String cdContratoNegocio) {
		this.cdContratoNegocio = cdContratoNegocio;
	}

	/**
	 * Get: cdDeptoGestor.
	 *
	 * @return cdDeptoGestor
	 */
	public Integer getCdDeptoGestor() {
		return cdDeptoGestor;
	}

	/**
	 * Set: cdDeptoGestor.
	 *
	 * @param cdDeptoGestor the cd depto gestor
	 */
	public void setCdDeptoGestor(Integer cdDeptoGestor) {
		this.cdDeptoGestor = cdDeptoGestor;
	}

	/**
	 * Get: cdFuncionarioBradesco.
	 *
	 * @return cdFuncionarioBradesco
	 */
	public Long getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}

	/**
	 * Set: cdFuncionarioBradesco.
	 *
	 * @param cdFuncionarioBradesco the cd funcionario bradesco
	 */
	public void setCdFuncionarioBradesco(Long cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}

	/**
	 * Get: cdIdentificadorIdioma.
	 *
	 * @return cdIdentificadorIdioma
	 */
	public Integer getCdIdentificadorIdioma() {
		return cdIdentificadorIdioma;
	}

	/**
	 * Set: cdIdentificadorIdioma.
	 *
	 * @param cdIdentificadorIdioma the cd identificador idioma
	 */
	public void setCdIdentificadorIdioma(Integer cdIdentificadorIdioma) {
		this.cdIdentificadorIdioma = cdIdentificadorIdioma;
	}

	/**
	 * Get: cdIndicadorMoeda.
	 *
	 * @return cdIndicadorMoeda
	 */
	public Integer getCdIndicadorMoeda() {
		return cdIndicadorMoeda;
	}

	/**
	 * Set: cdIndicadorMoeda.
	 *
	 * @param cdIndicadorMoeda the cd indicador moeda
	 */
	public void setCdIndicadorMoeda(Integer cdIndicadorMoeda) {
		this.cdIndicadorMoeda = cdIndicadorMoeda;
	}

	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}

	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}

	/**
	 * Get: cdMotivoSituacaoContrato.
	 *
	 * @return cdMotivoSituacaoContrato
	 */
	public Integer getCdMotivoSituacaoContrato() {
		return cdMotivoSituacaoContrato;
	}

	/**
	 * Set: cdMotivoSituacaoContrato.
	 *
	 * @param cdMotivoSituacaoContrato the cd motivo situacao contrato
	 */
	public void setCdMotivoSituacaoContrato(Integer cdMotivoSituacaoContrato) {
		this.cdMotivoSituacaoContrato = cdMotivoSituacaoContrato;
	}

	/**
	 * Get: cdOrigemContrato.
	 *
	 * @return cdOrigemContrato
	 */
	public Integer getCdOrigemContrato() {
		return cdOrigemContrato;
	}

	/**
	 * Set: cdOrigemContrato.
	 *
	 * @param cdOrigemContrato the cd origem contrato
	 */
	public void setCdOrigemContrato(Integer cdOrigemContrato) {
		this.cdOrigemContrato = cdOrigemContrato;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}

	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsContratoNegocio.
	 *
	 * @return dsContratoNegocio
	 */
	public String getDsContratoNegocio() {
		return dsContratoNegocio;
	}

	/**
	 * Set: dsContratoNegocio.
	 *
	 * @param dsContratoNegocio the ds contrato negocio
	 */
	public void setDsContratoNegocio(String dsContratoNegocio) {
		this.dsContratoNegocio = dsContratoNegocio;
	}

	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}

	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}

	/**
	 * Get: dsIndicadorAditivo.
	 *
	 * @return dsIndicadorAditivo
	 */
	public String getDsIndicadorAditivo() {
		return dsIndicadorAditivo;
	}

	/**
	 * Set: dsIndicadorAditivo.
	 *
	 * @param dsIndicadorAditivo the ds indicador aditivo
	 */
	public void setDsIndicadorAditivo(String dsIndicadorAditivo) {
		this.dsIndicadorAditivo = dsIndicadorAditivo;
	}

	/**
	 * Get: dsIndicadorParticipante.
	 *
	 * @return dsIndicadorParticipante
	 */
	public String getDsIndicadorParticipante() {
		return dsIndicadorParticipante;
	}

	/**
	 * Set: dsIndicadorParticipante.
	 *
	 * @param dsIndicadorParticipante the ds indicador participante
	 */
	public void setDsIndicadorParticipante(String dsIndicadorParticipante) {
		this.dsIndicadorParticipante = dsIndicadorParticipante;
	}

	/**
	 * Get: dsMoeda.
	 *
	 * @return dsMoeda
	 */
	public String getDsMoeda() {
		return dsMoeda;
	}

	/**
	 * Set: dsMoeda.
	 *
	 * @param dsMoeda the ds moeda
	 */
	public void setDsMoeda(String dsMoeda) {
		this.dsMoeda = dsMoeda;
	}

	/**
	 * Get: dsMotivoSituacaoContrato.
	 *
	 * @return dsMotivoSituacaoContrato
	 */
	public String getDsMotivoSituacaoContrato() {
		return dsMotivoSituacaoContrato;
	}

	/**
	 * Set: dsMotivoSituacaoContrato.
	 *
	 * @param dsMotivoSituacaoContrato the ds motivo situacao contrato
	 */
	public void setDsMotivoSituacaoContrato(String dsMotivoSituacaoContrato) {
		this.dsMotivoSituacaoContrato = dsMotivoSituacaoContrato;
	}

	/**
	 * Get: dsOrigemContrato.
	 *
	 * @return dsOrigemContrato
	 */
	public String getDsOrigemContrato() {
		return dsOrigemContrato;
	}

	/**
	 * Set: dsOrigemContrato.
	 *
	 * @param dsOrigemContrato the ds origem contrato
	 */
	public void setDsOrigemContrato(String dsOrigemContrato) {
		this.dsOrigemContrato = dsOrigemContrato;
	}

	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}

	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}

	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}

	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}

	/**
	 * Get: dtFimVigencia.
	 *
	 * @return dtFimVigencia
	 */
	public String getDtFimVigencia() {
		return dtFimVigencia;
	}

	/**
	 * Set: dtFimVigencia.
	 *
	 * @param dtFimVigencia the dt fim vigencia
	 */
	public void setDtFimVigencia(String dtFimVigencia) {
		this.dtFimVigencia = dtFimVigencia;
	}

	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}

	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}

	/**
	 * Get: hrAssinaturaContrato.
	 *
	 * @return hrAssinaturaContrato
	 */
	public String getHrAssinaturaContrato() {
		return hrAssinaturaContrato;
	}

	/**
	 * Set: hrAssinaturaContrato.
	 *
	 * @param hrAssinaturaContrato the hr assinatura contrato
	 */
	public void setHrAssinaturaContrato(String hrAssinaturaContrato) {
		this.hrAssinaturaContrato = hrAssinaturaContrato;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrInclusaoRegistroHistorico.
	 *
	 * @return hrInclusaoRegistroHistorico
	 */
	public String getHrInclusaoRegistroHistorico() {
		return hrInclusaoRegistroHistorico;
	}

	/**
	 * Set: hrInclusaoRegistroHistorico.
	 *
	 * @param hrInclusaoRegistroHistorico the hr inclusao registro historico
	 */
	public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
		this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: hrSituacaoContrato.
	 *
	 * @return hrSituacaoContrato
	 */
	public String getHrSituacaoContrato() {
		return hrSituacaoContrato;
	}

	/**
	 * Set: hrSituacaoContrato.
	 *
	 * @param hrSituacaoContrato the hr situacao contrato
	 */
	public void setHrSituacaoContrato(String hrSituacaoContrato) {
		this.hrSituacaoContrato = hrSituacaoContrato;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nmDepto.
	 *
	 * @return nmDepto
	 */
	public String getNmDepto() {
		return nmDepto;
	}

	/**
	 * Set: nmDepto.
	 *
	 * @param nmDepto the nm depto
	 */
	public void setNmDepto(String nmDepto) {
		this.nmDepto = nmDepto;
	}

	/**
	 * Get: nmFuncionarioBradesco.
	 *
	 * @return nmFuncionarioBradesco
	 */
	public String getNmFuncionarioBradesco() {
		return nmFuncionarioBradesco;
	}

	/**
	 * Set: nmFuncionarioBradesco.
	 *
	 * @param nmFuncionarioBradesco the nm funcionario bradesco
	 */
	public void setNmFuncionarioBradesco(String nmFuncionarioBradesco) {
		this.nmFuncionarioBradesco = nmFuncionarioBradesco;
	}

	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}

	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: nmPessoaJuridica.
	 *
	 * @return nmPessoaJuridica
	 */
	public String getNmPessoaJuridica() {
		return nmPessoaJuridica;
	}

	/**
	 * Set: nmPessoaJuridica.
	 *
	 * @param nmPessoaJuridica the nm pessoa juridica
	 */
	public void setNmPessoaJuridica(String nmPessoaJuridica) {
		this.nmPessoaJuridica = nmPessoaJuridica;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}

	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}

	/**
	 * Get: cdSetorContratoPagamento.
	 *
	 * @return cdSetorContratoPagamento
	 */
	public Integer getCdSetorContratoPagamento() {
		return cdSetorContratoPagamento;
	}

	/**
	 * Set: cdSetorContratoPagamento.
	 *
	 * @param cdSetorContratoPagamento the cd setor contrato pagamento
	 */
	public void setCdSetorContratoPagamento(Integer cdSetorContratoPagamento) {
		this.cdSetorContratoPagamento = cdSetorContratoPagamento;
	}

	/**
	 * Get: dsSetorContratoPagamento.
	 *
	 * @return dsSetorContratoPagamento
	 */
	public String getDsSetorContratoPagamento() {
		return dsSetorContratoPagamento;
	}

	/**
	 * Set: dsSetorContratoPagamento.
	 *
	 * @param dsSetorContratoPagamento the ds setor contrato pagamento
	 */
	public void setDsSetorContratoPagamento(String dsSetorContratoPagamento) {
		this.dsSetorContratoPagamento = dsSetorContratoPagamento;
	}

	/**
	 * Get: cdPessoaJuridicaProposta.
	 *
	 * @return cdPessoaJuridicaProposta
	 */
	public Long getCdPessoaJuridicaProposta() {
		return cdPessoaJuridicaProposta;
	}

	/**
	 * Set: cdPessoaJuridicaProposta.
	 *
	 * @param cdPessoaJuridicaProposta the cd pessoa juridica proposta
	 */
	public void setCdPessoaJuridicaProposta(Long cdPessoaJuridicaProposta) {
		this.cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
	}

	/**
	 * Get: cdTipoContratoProposta.
	 *
	 * @return cdTipoContratoProposta
	 */
	public Integer getCdTipoContratoProposta() {
		return cdTipoContratoProposta;
	}

	/**
	 * Set: cdTipoContratoProposta.
	 *
	 * @param cdTipoContratoProposta the cd tipo contrato proposta
	 */
	public void setCdTipoContratoProposta(Integer cdTipoContratoProposta) {
		this.cdTipoContratoProposta = cdTipoContratoProposta;
	}

	/**
	 * Get: nrContratoProposta.
	 *
	 * @return nrContratoProposta
	 */
	public Long getNrContratoProposta() {
		return nrContratoProposta;
	}

	/**
	 * Set: nrContratoProposta.
	 *
	 * @param nrContratoProposta the nr contrato proposta
	 */
	public void setNrContratoProposta(Long nrContratoProposta) {
		this.nrContratoProposta = nrContratoProposta;
	}

	/**
	 * Get: dsPessoaJuridicaProposta.
	 *
	 * @return dsPessoaJuridicaProposta
	 */
	public String getDsPessoaJuridicaProposta() {
		return dsPessoaJuridicaProposta;
	}

	/**
	 * Set: dsPessoaJuridicaProposta.
	 *
	 * @param dsPessoaJuridicaProposta the ds pessoa juridica proposta
	 */
	public void setDsPessoaJuridicaProposta(String dsPessoaJuridicaProposta) {
		this.dsPessoaJuridicaProposta = dsPessoaJuridicaProposta;
	}

	/**
	 * Get: dsTipoContratoProposta.
	 *
	 * @return dsTipoContratoProposta
	 */
	public String getDsTipoContratoProposta() {
		return dsTipoContratoProposta;
	}

	/**
	 * Set: dsTipoContratoProposta.
	 *
	 * @param dsTipoContratoProposta the ds tipo contrato proposta
	 */
	public void setDsTipoContratoProposta(String dsTipoContratoProposta) {
		this.dsTipoContratoProposta = dsTipoContratoProposta;
	}

	/**
	 * @return the nrSequenciaContratoOutros
	 */
	public long getNrSequenciaContratoOutros() {
		return nrSequenciaContratoOutros;
	}

	/**
	 * @param nrSequenciaContratoOutros the nrSequenciaContratoOutros to set
	 */
	public void setNrSequenciaContratoOutros(long nrSequenciaContratoOutros) {
		this.nrSequenciaContratoOutros = nrSequenciaContratoOutros;
	}

	/**
	 * @return the nrSequenciaContratoPagamentoSalario
	 */
	public long getNrSequenciaContratoPagamentoSalario() {
		return nrSequenciaContratoPagamentoSalario;
	}

	/**
	 * @param nrSequenciaContratoPagamentoSalario the nrSequenciaContratoPagamentoSalario to set
	 */
	public void setNrSequenciaContratoPagamentoSalario(
			long nrSequenciaContratoPagamentoSalario) {
		this.nrSequenciaContratoPagamentoSalario = nrSequenciaContratoPagamentoSalario;
	}

    /**
     * Nome: getCdFormaAutorizacaoPagamento
     *
     * @return cdFormaAutorizacaoPagamento
     */
    public Integer getCdFormaAutorizacaoPagamento() {
        return cdFormaAutorizacaoPagamento;
    }

    /**
     * Nome: setCdFormaAutorizacaoPagamento
     *
     * @param cdFormaAutorizacaoPagamento
     */
    public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
        this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
    }

    /**
     * Nome: getDsFormaAutorizacaoPagamento
     *
     * @return dsFormaAutorizacaoPagamento
     */
    public String getDsFormaAutorizacaoPagamento() {
        return dsFormaAutorizacaoPagamento;
    }

    /**
     * Nome: setDsFormaAutorizacaoPagamento
     *
     * @param dsFormaAutorizacaoPagamento
     */
    public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
        this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    }
}
