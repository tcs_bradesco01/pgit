/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Nome: DetalharSolBaseRetransmissaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolBaseRetransmissaoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo nrSolicitacao. */
	private Integer nrSolicitacao;
	
	/** Atributo hrSolicitacao. */
	private String hrSolicitacao;
	
	/** Atributo dsSituacaoRecuperacao. */
	private String dsSituacaoRecuperacao;
	
	/** Atributo dsMotivoSituacaoRecuperacao. */
	private String dsMotivoSituacaoRecuperacao;
	
	/** Atributo hrAtendimentoSolicitacao. */
	private String hrAtendimentoSolicitacao;
	
	/** Atributo qtdeRegistros. */
	private Long qtdeRegistros;
	
	/** Atributo vlrTarifaPadrao. */
	private BigDecimal vlrTarifaPadrao;
	
	/** Atributo descontoTarifa. */
	private BigDecimal descontoTarifa;
	
	/** Atributo vlrTarifaAtual. */
	private BigDecimal vlrTarifaAtual;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo nmOperacaoFluxoInclusao. */
	private String nmOperacaoFluxoInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo nrSolicitacao2. */
	private Integer nrSolicitacao2;
		
	/** Atributo hrSolicitacaoFormatada. */
	private String hrSolicitacaoFormatada;
	
	/** Atributo hrAtendimentoSolicitacaoFormatada. */
	private String hrAtendimentoSolicitacaoFormatada;
	
	/** Atributo hrInclusaoRegistroFormatada. */
	private String hrInclusaoRegistroFormatada;
	
	/** Atributo hrManutencaoRegistroFormatada. */
	private String hrManutencaoRegistroFormatada;
	
	/** Atributo qtdeRegistrosFormatada. */
	private String qtdeRegistrosFormatada;	
	
	/** Atributo listaDetalharSolBaseRetransmissao. */
	private List<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO> listaDetalharSolBaseRetransmissao = new ArrayList<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO>();
	
	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: descontoTarifa.
	 *
	 * @return descontoTarifa
	 */
	public BigDecimal getDescontoTarifa() {
		return descontoTarifa;
	}
	
	/**
	 * Set: descontoTarifa.
	 *
	 * @param descontoTarifa the desconto tarifa
	 */
	public void setDescontoTarifa(BigDecimal descontoTarifa) {
		this.descontoTarifa = descontoTarifa;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsMotivoSituacaoRecuperacao.
	 *
	 * @return dsMotivoSituacaoRecuperacao
	 */
	public String getDsMotivoSituacaoRecuperacao() {
		return dsMotivoSituacaoRecuperacao;
	}
	
	/**
	 * Set: dsMotivoSituacaoRecuperacao.
	 *
	 * @param dsMotivoSituacaoRecuperacao the ds motivo situacao recuperacao
	 */
	public void setDsMotivoSituacaoRecuperacao(String dsMotivoSituacaoRecuperacao) {
		this.dsMotivoSituacaoRecuperacao = dsMotivoSituacaoRecuperacao;
	}
	
	/**
	 * Get: dsSituacaoRecuperacao.
	 *
	 * @return dsSituacaoRecuperacao
	 */
	public String getDsSituacaoRecuperacao() {
		return dsSituacaoRecuperacao;
	}
	
	/**
	 * Set: dsSituacaoRecuperacao.
	 *
	 * @param dsSituacaoRecuperacao the ds situacao recuperacao
	 */
	public void setDsSituacaoRecuperacao(String dsSituacaoRecuperacao) {
		this.dsSituacaoRecuperacao = dsSituacaoRecuperacao;
	}
	
	/**
	 * Get: hrAtendimentoSolicitacao.
	 *
	 * @return hrAtendimentoSolicitacao
	 */
	public String getHrAtendimentoSolicitacao() {
		return hrAtendimentoSolicitacao;
	}
	
	/**
	 * Set: hrAtendimentoSolicitacao.
	 *
	 * @param hrAtendimentoSolicitacao the hr atendimento solicitacao
	 */
	public void setHrAtendimentoSolicitacao(String hrAtendimentoSolicitacao) {
		this.hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: hrSolicitacao.
	 *
	 * @return hrSolicitacao
	 */
	public String getHrSolicitacao() {
		return hrSolicitacao;
	}
	
	/**
	 * Set: hrSolicitacao.
	 *
	 * @param hrSolicitacao the hr solicitacao
	 */
	public void setHrSolicitacao(String hrSolicitacao) {
		this.hrSolicitacao = hrSolicitacao;
	}
	
	/**
	 * Get: listaDetalharSolBaseRetransmissao.
	 *
	 * @return listaDetalharSolBaseRetransmissao
	 */
	public List<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO> getListaDetalharSolBaseRetransmissao() {
		return listaDetalharSolBaseRetransmissao;
	}
	
	/**
	 * Set: listaDetalharSolBaseRetransmissao.
	 *
	 * @param listaDetalharSolBaseRetransmissao the lista detalhar sol base retransmissao
	 */
	public void setListaDetalharSolBaseRetransmissao(
			List<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO> listaDetalharSolBaseRetransmissao) {
		this.listaDetalharSolBaseRetransmissao = listaDetalharSolBaseRetransmissao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: nrSolicitacao2.
	 *
	 * @return nrSolicitacao2
	 */
	public Integer getNrSolicitacao2() {
		return nrSolicitacao2;
	}
	
	/**
	 * Set: nrSolicitacao2.
	 *
	 * @param nrSolicitacao2 the nr solicitacao2
	 */
	public void setNrSolicitacao2(Integer nrSolicitacao2) {
		this.nrSolicitacao2 = nrSolicitacao2;
	}
	
	/**
	 * Get: qtdeRegistros.
	 *
	 * @return qtdeRegistros
	 */
	public Long getQtdeRegistros() {
		return qtdeRegistros;
	}
	
	/**
	 * Set: qtdeRegistros.
	 *
	 * @param qtdeRegistros the qtde registros
	 */
	public void setQtdeRegistros(Long qtdeRegistros) {
		this.qtdeRegistros = qtdeRegistros;
	}
	
	/**
	 * Get: vlrTarifaAtual.
	 *
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}
	
	/**
	 * Set: vlrTarifaAtual.
	 *
	 * @param vlrTarifaAtual the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}
	
	/**
	 * Get: vlrTarifaPadrao.
	 *
	 * @return vlrTarifaPadrao
	 */
	public BigDecimal getVlrTarifaPadrao() {
		return vlrTarifaPadrao;
	}
	
	/**
	 * Set: vlrTarifaPadrao.
	 *
	 * @param vlrTarifaPadrao the vlr tarifa padrao
	 */
	public void setVlrTarifaPadrao(BigDecimal vlrTarifaPadrao) {
		this.vlrTarifaPadrao = vlrTarifaPadrao;
	}
	
	/**
	 * Get: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @return hrAtendimentoSolicitacaoFormatada
	 */
	public String getHrAtendimentoSolicitacaoFormatada() {
		return hrAtendimentoSolicitacaoFormatada;
	}
	
	/**
	 * Set: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @param hrAtendimentoSolicitacaoFormatada the hr atendimento solicitacao formatada
	 */
	public void setHrAtendimentoSolicitacaoFormatada(
			String hrAtendimentoSolicitacaoFormatada) {
		this.hrAtendimentoSolicitacaoFormatada = hrAtendimentoSolicitacaoFormatada;
	}
	
	/**
	 * Get: hrInclusaoRegistroFormatada.
	 *
	 * @return hrInclusaoRegistroFormatada
	 */
	public String getHrInclusaoRegistroFormatada() {
		return hrInclusaoRegistroFormatada;
	}
	
	/**
	 * Set: hrInclusaoRegistroFormatada.
	 *
	 * @param hrInclusaoRegistroFormatada the hr inclusao registro formatada
	 */
	public void setHrInclusaoRegistroFormatada(String hrInclusaoRegistroFormatada) {
		this.hrInclusaoRegistroFormatada = hrInclusaoRegistroFormatada;
	}
	
	/**
	 * Get: hrManutencaoRegistroFormatada.
	 *
	 * @return hrManutencaoRegistroFormatada
	 */
	public String getHrManutencaoRegistroFormatada() {
		return hrManutencaoRegistroFormatada;
	}
	
	/**
	 * Set: hrManutencaoRegistroFormatada.
	 *
	 * @param hrManutencaoRegistroFormatada the hr manutencao registro formatada
	 */
	public void setHrManutencaoRegistroFormatada(
			String hrManutencaoRegistroFormatada) {
		this.hrManutencaoRegistroFormatada = hrManutencaoRegistroFormatada;
	}
	
	/**
	 * Get: hrSolicitacaoFormatada.
	 *
	 * @return hrSolicitacaoFormatada
	 */
	public String getHrSolicitacaoFormatada() {
		return hrSolicitacaoFormatada;
	}
	
	/**
	 * Set: hrSolicitacaoFormatada.
	 *
	 * @param hrSolicitacaoFormatada the hr solicitacao formatada
	 */
	public void setHrSolicitacaoFormatada(String hrSolicitacaoFormatada) {
		this.hrSolicitacaoFormatada = hrSolicitacaoFormatada;
	}
	
	/**
	 * Get: qtdeRegistrosFormatada.
	 *
	 * @return qtdeRegistrosFormatada
	 */
	public String getQtdeRegistrosFormatada() {
		return qtdeRegistrosFormatada;
	}
	
	/**
	 * Set: qtdeRegistrosFormatada.
	 *
	 * @param qtdeRegistrosFormatada the qtde registros formatada
	 */
	public void setQtdeRegistrosFormatada(String qtdeRegistrosFormatada) {
		this.qtdeRegistrosFormatada = qtdeRegistrosFormatada;
	}
	
	
}
