/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: DetalharHistEmissaoAutCompSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistEmissaoAutCompSaidaDTO {

	/** Atributo cdServicoEmissaoContratado. */
	private int cdServicoEmissaoContratado;
	
	/** Atributo dsServicoEmissaoContratado. */
	private String dsServicoEmissaoContratado;
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdModalidadeRelacionamento. */
	private int cdModalidadeRelacionamento;
	
	/** Atributo dsModalidadeRelacionamento. */
	private String dsModalidadeRelacionamento;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo situacaoVinculacao. */
	private int situacaoVinculacao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;	
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dsSituacaoVinculacao. */
	private String dsSituacaoVinculacao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdModalidadeRelacionamento.
	 *
	 * @return cdModalidadeRelacionamento
	 */
	public int getCdModalidadeRelacionamento() {
		return cdModalidadeRelacionamento;
	}
	
	/**
	 * Set: cdModalidadeRelacionamento.
	 *
	 * @param cdModalidadeRelacionamento the cd modalidade relacionamento
	 */
	public void setCdModalidadeRelacionamento(int cdModalidadeRelacionamento) {
		this.cdModalidadeRelacionamento = cdModalidadeRelacionamento;
	}
	
	/**
	 * Get: cdServicoEmissaoContratado.
	 *
	 * @return cdServicoEmissaoContratado
	 */
	public int getCdServicoEmissaoContratado() {
		return cdServicoEmissaoContratado;
	}
	
	/**
	 * Set: cdServicoEmissaoContratado.
	 *
	 * @param cdServicoEmissaoContratado the cd servico emissao contratado
	 */
	public void setCdServicoEmissaoContratado(int cdServicoEmissaoContratado) {
		this.cdServicoEmissaoContratado = cdServicoEmissaoContratado;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsModalidadeRelacionamento.
	 *
	 * @return dsModalidadeRelacionamento
	 */
	public String getDsModalidadeRelacionamento() {
		return dsModalidadeRelacionamento;
	}
	
	/**
	 * Set: dsModalidadeRelacionamento.
	 *
	 * @param dsModalidadeRelacionamento the ds modalidade relacionamento
	 */
	public void setDsModalidadeRelacionamento(String dsModalidadeRelacionamento) {
		this.dsModalidadeRelacionamento = dsModalidadeRelacionamento;
	}
	
	/**
	 * Get: dsServicoEmissaoContratado.
	 *
	 * @return dsServicoEmissaoContratado
	 */
	public String getDsServicoEmissaoContratado() {
		return dsServicoEmissaoContratado;
	}
	
	/**
	 * Set: dsServicoEmissaoContratado.
	 *
	 * @param dsServicoEmissaoContratado the ds servico emissao contratado
	 */
	public void setDsServicoEmissaoContratado(String dsServicoEmissaoContratado) {
		this.dsServicoEmissaoContratado = dsServicoEmissaoContratado;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: situacaoVinculacao.
	 *
	 * @return situacaoVinculacao
	 */
	public int getSituacaoVinculacao() {
		return situacaoVinculacao;
	}
	
	/**
	 * Set: situacaoVinculacao.
	 *
	 * @param situacaoVinculacao the situacao vinculacao
	 */
	public void setSituacaoVinculacao(int situacaoVinculacao) {
		this.situacaoVinculacao = situacaoVinculacao;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: dsSituacaoVinculacao.
	 *
	 * @return dsSituacaoVinculacao
	 */
	public String getDsSituacaoVinculacao() {
	    return dsSituacaoVinculacao;
	}
	
	/**
	 * Set: dsSituacaoVinculacao.
	 *
	 * @param dsSituacaoVinculacao the ds situacao vinculacao
	 */
	public void setDsSituacaoVinculacao(String dsSituacaoVinculacao) {
	    this.dsSituacaoVinculacao = dsSituacaoVinculacao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
}
