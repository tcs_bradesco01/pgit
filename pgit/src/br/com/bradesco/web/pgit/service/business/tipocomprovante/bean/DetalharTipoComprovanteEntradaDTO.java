/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipocomprovante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipocomprovante.bean;

/**
 * Nome: DetalharTipoComprovanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharTipoComprovanteEntradaDTO {
	
	/** Atributo cdComprovanteSalarial. */
	private int cdComprovanteSalarial;

	/**
	 * Get: cdComprovanteSalarial.
	 *
	 * @return cdComprovanteSalarial
	 */
	public int getCdComprovanteSalarial() {
		return cdComprovanteSalarial;
	}

	/**
	 * Set: cdComprovanteSalarial.
	 *
	 * @param cdComprovanteSalarial the cd comprovante salarial
	 */
	public void setCdComprovanteSalarial(int cdComprovanteSalarial) {
		this.cdComprovanteSalarial = cdComprovanteSalarial;
	}
	
	

}
