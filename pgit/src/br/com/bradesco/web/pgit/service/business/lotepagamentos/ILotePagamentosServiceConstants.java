/**
 * Nome: br.com.bradesco.web.pgit.service.business.lotepagamentos
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.lotepagamentos;

/**
 * Nome: ILotePagamentosServiceConstants
 * <p>
 * Prop�sito: Interface de constantes do adaptador LotePagamentos
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface ILotePagamentosServiceConstants {

}