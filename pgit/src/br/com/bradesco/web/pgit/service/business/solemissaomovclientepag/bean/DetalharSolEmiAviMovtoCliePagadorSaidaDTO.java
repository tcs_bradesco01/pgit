/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Nome: DetalharSolEmiAviMovtoCliePagadorSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolEmiAviMovtoCliePagadorSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdSolicitacaoPagamentoIntegrado. */
    private Integer cdSolicitacaoPagamentoIntegrado;
    
    /** Atributo nrSolicitacaoPagamentoIntegrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo dtInicioPeriodoMovimentacao. */
    private String dtInicioPeriodoMovimentacao;
    
    /** Atributo dtFimPeriodoMovimentacao. */
    private String dtFimPeriodoMovimentacao;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo dsProdutoServicoOperacao. */
    private String dsProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo dsProdutoOperacaoRelacionado. */
    private String dsProdutoOperacaoRelacionado;
    
    /** Atributo cdRecebedorCredito. */
    private Long cdRecebedorCredito;
    
    /** Atributo cdTipoInscricaoRecebedor. */
    private Integer cdTipoInscricaoRecebedor;
    
    /** Atributo cdCpfCnpjRecebedor. */
    private Long cdCpfCnpjRecebedor;
    
    /** Atributo cdFilialCnpjRecebedor. */
    private Integer cdFilialCnpjRecebedor;
    
    /** Atributo cdControleCpfRecebedor. */
    private Integer cdControleCpfRecebedor;
    
    /** Atributo cdDestinoCorrespSolicitacao. */
    private Integer cdDestinoCorrespSolicitacao;
    
    /** Atributo cdTipoPostagemSolicitacao. */
    private String cdTipoPostagemSolicitacao;
    
    /** Atributo dsLogradouroPagador. */
    private String dsLogradouroPagador;
    
    /** Atributo dsNumeroLogradouroPagador. */
    private String dsNumeroLogradouroPagador;
    
    /** Atributo dsComplementoLogradouroPagador. */
    private String dsComplementoLogradouroPagador;
    
    /** Atributo dsBairroClientePagador. */
    private String dsBairroClientePagador;
    
    /** Atributo dsMunicipioClientePagador. */
    private String dsMunicipioClientePagador;
    
    /** Atributo cdSiglaUfPagador. */
    private String cdSiglaUfPagador;
    
    /** Atributo cdCepPagador. */
    private Integer cdCepPagador;
    
    /** Atributo cdCepComplementoPagador. */
    private Integer cdCepComplementoPagador;
    
    /** Atributo dsEmailClientePagador. */
    private String dsEmailClientePagador;
    
    /** Atributo cdAgenciaOperadora. */
    private Integer cdAgenciaOperadora;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo vlTarifaPadrao. */
    private BigDecimal vlTarifaPadrao;
    
    /** Atributo vlTarifa. */
    private BigDecimal vlTarifa;
    
    /** Atributo vlrTarifaAtual. */
    private BigDecimal vlrTarifaAtual;
    
    /** Atributo cdPercentualDescTarifa. */
    private BigDecimal cdPercentualDescTarifa;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsTipoCanalInclusao. */
    private String dsTipoCanalInclusao;
    
    /** Atributo cdDepartamentoUnidade. */
    private Integer cdDepartamentoUnidade;
    
    /** Atributo listaOcorrencias. */
    private List<OcorrenciasDetalharSaidaDTO> listaOcorrencias = new ArrayList<OcorrenciasDetalharSaidaDTO>();
    
	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public Integer getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}
	
	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(Integer cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}
	
	/**
	 * Get: cdCepComplementoPagador.
	 *
	 * @return cdCepComplementoPagador
	 */
	public Integer getCdCepComplementoPagador() {
		return cdCepComplementoPagador;
	}
	
	/**
	 * Set: cdCepComplementoPagador.
	 *
	 * @param cdCepComplementoPagador the cd cep complemento pagador
	 */
	public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
		this.cdCepComplementoPagador = cdCepComplementoPagador;
	}
	
	/**
	 * Get: cdCepPagador.
	 *
	 * @return cdCepPagador
	 */
	public Integer getCdCepPagador() {
		return cdCepPagador;
	}
	
	/**
	 * Set: cdCepPagador.
	 *
	 * @param cdCepPagador the cd cep pagador
	 */
	public void setCdCepPagador(Integer cdCepPagador) {
		this.cdCepPagador = cdCepPagador;
	}
	
	/**
	 * Get: cdControleCpfRecebedor.
	 *
	 * @return cdControleCpfRecebedor
	 */
	public Integer getCdControleCpfRecebedor() {
		return cdControleCpfRecebedor;
	}
	
	/**
	 * Set: cdControleCpfRecebedor.
	 *
	 * @param cdControleCpfRecebedor the cd controle cpf recebedor
	 */
	public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
		this.cdControleCpfRecebedor = cdControleCpfRecebedor;
	}
	
	/**
	 * Get: cdCpfCnpjRecebedor.
	 *
	 * @return cdCpfCnpjRecebedor
	 */
	public Long getCdCpfCnpjRecebedor() {
		return cdCpfCnpjRecebedor;
	}
	
	/**
	 * Set: cdCpfCnpjRecebedor.
	 *
	 * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
	 */
	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}
	
	/**
	 * Get: cdDestinoCorrespSolicitacao.
	 *
	 * @return cdDestinoCorrespSolicitacao
	 */
	public Integer getCdDestinoCorrespSolicitacao() {
		return cdDestinoCorrespSolicitacao;
	}
	
	/**
	 * Set: cdDestinoCorrespSolicitacao.
	 *
	 * @param cdDestinoCorrespSolicitacao the cd destino corresp solicitacao
	 */
	public void setCdDestinoCorrespSolicitacao(Integer cdDestinoCorrespSolicitacao) {
		this.cdDestinoCorrespSolicitacao = cdDestinoCorrespSolicitacao;
	}
	
	/**
	 * Get: cdFilialCnpjRecebedor.
	 *
	 * @return cdFilialCnpjRecebedor
	 */
	public Integer getCdFilialCnpjRecebedor() {
		return cdFilialCnpjRecebedor;
	}
	
	/**
	 * Set: cdFilialCnpjRecebedor.
	 *
	 * @param cdFilialCnpjRecebedor the cd filial cnpj recebedor
	 */
	public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
		this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdPercentualDescTarifa.
	 *
	 * @return cdPercentualDescTarifa
	 */
	public BigDecimal getCdPercentualDescTarifa() {
		return cdPercentualDescTarifa;
	}
	
	/**
	 * Set: cdPercentualDescTarifa.
	 *
	 * @param cdPercentualDescTarifa the cd percentual desc tarifa
	 */
	public void setCdPercentualDescTarifa(BigDecimal cdPercentualDescTarifa) {
		this.cdPercentualDescTarifa = cdPercentualDescTarifa;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRecebedorCredito.
	 *
	 * @return cdRecebedorCredito
	 */
	public Long getCdRecebedorCredito() {
		return cdRecebedorCredito;
	}
	
	/**
	 * Set: cdRecebedorCredito.
	 *
	 * @param cdRecebedorCredito the cd recebedor credito
	 */
	public void setCdRecebedorCredito(Long cdRecebedorCredito) {
		this.cdRecebedorCredito = cdRecebedorCredito;
	}
	
	/**
	 * Get: cdSiglaUfPagador.
	 *
	 * @return cdSiglaUfPagador
	 */
	public String getCdSiglaUfPagador() {
		return cdSiglaUfPagador;
	}
	
	/**
	 * Set: cdSiglaUfPagador.
	 *
	 * @param cdSiglaUfPagador the cd sigla uf pagador
	 */
	public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
		this.cdSiglaUfPagador = cdSiglaUfPagador;
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoInscricaoRecebedor.
	 *
	 * @return cdTipoInscricaoRecebedor
	 */
	public Integer getCdTipoInscricaoRecebedor() {
		return cdTipoInscricaoRecebedor;
	}
	
	/**
	 * Set: cdTipoInscricaoRecebedor.
	 *
	 * @param cdTipoInscricaoRecebedor the cd tipo inscricao recebedor
	 */
	public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
		this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
	}
	
	/**
	 * Get: cdTipoPostagemSolicitacao.
	 *
	 * @return cdTipoPostagemSolicitacao
	 */
	public String getCdTipoPostagemSolicitacao() {
		return cdTipoPostagemSolicitacao;
	}
	
	/**
	 * Set: cdTipoPostagemSolicitacao.
	 *
	 * @param cdTipoPostagemSolicitacao the cd tipo postagem solicitacao
	 */
	public void setCdTipoPostagemSolicitacao(String cdTipoPostagemSolicitacao) {
		this.cdTipoPostagemSolicitacao = cdTipoPostagemSolicitacao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: dsBairroClientePagador.
	 *
	 * @return dsBairroClientePagador
	 */
	public String getDsBairroClientePagador() {
		return dsBairroClientePagador;
	}
	
	/**
	 * Set: dsBairroClientePagador.
	 *
	 * @param dsBairroClientePagador the ds bairro cliente pagador
	 */
	public void setDsBairroClientePagador(String dsBairroClientePagador) {
		this.dsBairroClientePagador = dsBairroClientePagador;
	}
	
	/**
	 * Get: dsComplementoLogradouroPagador.
	 *
	 * @return dsComplementoLogradouroPagador
	 */
	public String getDsComplementoLogradouroPagador() {
		return dsComplementoLogradouroPagador;
	}
	
	/**
	 * Set: dsComplementoLogradouroPagador.
	 *
	 * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
	 */
	public void setDsComplementoLogradouroPagador(
			String dsComplementoLogradouroPagador) {
		this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
	}
	
	/**
	 * Get: dsEmailClientePagador.
	 *
	 * @return dsEmailClientePagador
	 */
	public String getDsEmailClientePagador() {
		return dsEmailClientePagador;
	}
	
	/**
	 * Set: dsEmailClientePagador.
	 *
	 * @param dsEmailClientePagador the ds email cliente pagador
	 */
	public void setDsEmailClientePagador(String dsEmailClientePagador) {
		this.dsEmailClientePagador = dsEmailClientePagador;
	}
	
	/**
	 * Get: dsLogradouroPagador.
	 *
	 * @return dsLogradouroPagador
	 */
	public String getDsLogradouroPagador() {
		return dsLogradouroPagador;
	}
	
	/**
	 * Set: dsLogradouroPagador.
	 *
	 * @param dsLogradouroPagador the ds logradouro pagador
	 */
	public void setDsLogradouroPagador(String dsLogradouroPagador) {
		this.dsLogradouroPagador = dsLogradouroPagador;
	}
	
	/**
	 * Get: dsMunicipioClientePagador.
	 *
	 * @return dsMunicipioClientePagador
	 */
	public String getDsMunicipioClientePagador() {
		return dsMunicipioClientePagador;
	}
	
	/**
	 * Set: dsMunicipioClientePagador.
	 *
	 * @param dsMunicipioClientePagador the ds municipio cliente pagador
	 */
	public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
		this.dsMunicipioClientePagador = dsMunicipioClientePagador;
	}
	
	/**
	 * Get: dsNumeroLogradouroPagador.
	 *
	 * @return dsNumeroLogradouroPagador
	 */
	public String getDsNumeroLogradouroPagador() {
		return dsNumeroLogradouroPagador;
	}
	
	/**
	 * Set: dsNumeroLogradouroPagador.
	 *
	 * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
	 */
	public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
		this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dtFimPeriodoMovimentacao.
	 *
	 * @return dtFimPeriodoMovimentacao
	 */
	public String getDtFimPeriodoMovimentacao() {
		return dtFimPeriodoMovimentacao;
	}
	
	/**
	 * Set: dtFimPeriodoMovimentacao.
	 *
	 * @param dtFimPeriodoMovimentacao the dt fim periodo movimentacao
	 */
	public void setDtFimPeriodoMovimentacao(String dtFimPeriodoMovimentacao) {
		this.dtFimPeriodoMovimentacao = dtFimPeriodoMovimentacao;
	}
	
	/**
	 * Get: dtInicioPeriodoMovimentacao.
	 *
	 * @return dtInicioPeriodoMovimentacao
	 */
	public String getDtInicioPeriodoMovimentacao() {
		return dtInicioPeriodoMovimentacao;
	}
	
	/**
	 * Set: dtInicioPeriodoMovimentacao.
	 *
	 * @param dtInicioPeriodoMovimentacao the dt inicio periodo movimentacao
	 */
	public void setDtInicioPeriodoMovimentacao(String dtInicioPeriodoMovimentacao) {
		this.dtInicioPeriodoMovimentacao = dtInicioPeriodoMovimentacao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: listaOcorrencias.
	 *
	 * @return listaOcorrencias
	 */
	public List<OcorrenciasDetalharSaidaDTO> getListaOcorrencias() {
		return listaOcorrencias;
	}
	
	/**
	 * Set: listaOcorrencias.
	 *
	 * @param listaOcorrencias the lista ocorrencias
	 */
	public void setListaOcorrencias(
			List<OcorrenciasDetalharSaidaDTO> listaOcorrencias) {
		this.listaOcorrencias = listaOcorrencias;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: vlTarifa.
	 *
	 * @return vlTarifa
	 */
	public BigDecimal getVlTarifa() {
		return vlTarifa;
	}
	
	/**
	 * Set: vlTarifa.
	 *
	 * @param vlTarifa the vl tarifa
	 */
	public void setVlTarifa(BigDecimal vlTarifa) {
		this.vlTarifa = vlTarifa;
	}
	
	/**
	 * Get: cdDepartamentoUnidade.
	 *
	 * @return cdDepartamentoUnidade
	 */
	public Integer getCdDepartamentoUnidade() {
		return cdDepartamentoUnidade;
	}
	
	/**
	 * Set: cdDepartamentoUnidade.
	 *
	 * @param cdDepartamentoUnidade the cd departamento unidade
	 */
	public void setCdDepartamentoUnidade(Integer cdDepartamentoUnidade) {
		this.cdDepartamentoUnidade = cdDepartamentoUnidade;
	}
	
	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}
	
	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}
	
	/**
	 * Get: vlrTarifaAtual.
	 *
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}
	
	/**
	 * Set: vlrTarifaAtual.
	 *
	 * @param vlrTarifaAtual the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}
    
    
    
    
}
