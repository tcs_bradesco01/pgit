/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.contrato.bean.NumerosEmissoesPagasEnum;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.ExcluirVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.ExcluirVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.AlterarVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.AlterarVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideOcorrencias1SaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideOcorrencias2SaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideOcorrencias3SaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideOcorrencias4SaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideOcorrencias5SaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideOcorrencias6SaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.FaixasRenda;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.FolhaPagamentoEntradaDto;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.FolhaPagamentoSaidaDto;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.IncluirVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.IncluirVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosConvenioContaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosConvenioContaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnListaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarVincConvnCtaSalarioOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarvincconvnctasalario.request.AlterarVincConvnCtaSalarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarvincconvnctasalario.response.AlterarVincConvnCtaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.request.DetalharFolhaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.response.DetalharFolhaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.request.DetalharPiramideRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.response.DetalharPiramideResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.request.DetalharVincConvnCtaSalarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.response.DetalharVincConvnCtaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincconvnctasalario.request.ExcluirVincConvnCtaSalarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincconvnctasalario.response.ExcluirVincConvnCtaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.request.IncluirVincConvnCtaSalarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.response.IncluirVincConvnCtaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.request.ListarConvenioContaSalarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.request.ListarConvenioContaSalarioNovoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.ListarConvenioContaSalarioNovoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.request.ListarDadosCtaConvnRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response.ListarDadosCtaConvnResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.request.ListarFaixaSalarialPropostaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.response.ListarFaixaSalarialPropostaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.request.ListarVincConvnCtaSalarioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.response.ListarVincConvnCtaSalarioResponse;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.model.CalculadorValorPercentual;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterVinculacaoConvenioContaSalarioServiceImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterVinculacaoConvenioContaSalarioServiceImpl implements IManterVinculacaoConvenioContaSalarioService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService#listarDadosContaConvenio(br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnEntradaDTO)
	 */
	public ListarDadosCtaConvnSaidaDTO listarDadosContaConvenio(ListarDadosCtaConvnEntradaDTO entrada) {
//		ListarDadosConvenioRequest request = new ListarDadosConvenioRequest();
//		ListarDadosConvenioResponse response = new ListarDadosConvenioResponse();
		ListarDadosCtaConvnRequest request = new ListarDadosCtaConvnRequest();
		ListarDadosCtaConvnResponse response = new ListarDadosCtaConvnResponse();

		request.setCdClubPessoaRepresentante(PgitUtil.verificaLongNulo(entrada.getCdClubPessoaRepresentante()));
		request.setCdControleCnpjRepresentante(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjRepresentante()));
		request.setCdCpfCnpjRepresentante(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjRepresentante()));
		request.setCdFilialCnpjRepresentante(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjRepresentante()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdParmConvenio(PgitUtil.verificaIntegerNulo(entrada.getCdParmConvenio()));
		request.setNrOcorrencias(50);

		response = getFactoryAdapter().getListarDadosCtaConvnPDCAdapter().invokeProcess(request);
		
		ListarDadosCtaConvnSaidaDTO saida = new ListarDadosCtaConvnSaidaDTO();
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());
		
		List<ListarDadosCtaConvnListaOcorrenciasDTO> lista = new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();

		for (Ocorrencias o : response.getOcorrencias()) {
			ListarDadosCtaConvnListaOcorrenciasDTO dto = new ListarDadosCtaConvnListaOcorrenciasDTO();
			dto.setCdAgencia(o.getCdAgencia());
			dto.setCdBanco(o.getCdBanco());
			dto.setCdControleCnpjEmp(o.getCdControleCnpjEmp());
			dto.setCdConveCtaSalarial(o.getCdConveCtaSalarial());
			dto.setCdConvnNovo(o.getCdConvnNovo());
			dto.setCdCpfCnpjEmp(o.getCdCpfCnpjEmp());
			dto.setCdCta(o.getCdCta());
			dto.setCdDigitoAgencia(o.getCdDigitoAgencia());
			dto.setCdDigitoConta(o.getCdDigitoConta());
			dto.setCdFilialCnpjEmp(o.getCdFilialCnpjEmp());
			dto.setDsConvn(o.getDsConvn());

			lista.add(dto);
		}

		return new ListarDadosCtaConvnSaidaDTO(response, lista);
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService#incluirVincConvnCtaSalario(br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.IncluirVincConvnCtaSalarioEntradaDTO)
	 */
	public IncluirVincConvnCtaSalarioSaidaDTO incluirVincConvnCtaSalario(IncluirVincConvnCtaSalarioEntradaDTO entrada) {
		IncluirVincConvnCtaSalarioRequest request = new IncluirVincConvnCtaSalarioRequest();
		IncluirVincConvnCtaSalarioResponse response = new IncluirVincConvnCtaSalarioResponse();

		request.setCdAgencCtaEmp(entrada.getCdAgencCtaEmp());
		request.setCdBcoCtaEmp(entrada.getCdBcoCtaEmp());
		request.setCdCnpjEmp(entrada.getCdCnpjEmp());
		request.setCdConveCtaSalarial(entrada.getCdConveCtaSalarial());
		request.setCdConvenNovo(entrada.getCdConvenNovo());
		request.setCdCtrlCnpjEmp(entrada.getCdCtrlCnpjEmp());
		request.setCdDigitoCtaEmp(entrada.getCdDigitoCtaEmp());
		request.setCdFilialEmp(entrada.getCdFilialEmp());
		request.setCdNumeroCtaDest(entrada.getCdNumeroCtaDest());
		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());
		request.setDsConvCtaEmp(entrada.getDsConvCtaEmp());

		response = getFactoryAdapter().getIncluirVincConvnCtaSalarioPDCAdapter().invokeProcess(request);

		if (response == null) {
			return new IncluirVincConvnCtaSalarioSaidaDTO();
		}

		return new IncluirVincConvnCtaSalarioSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService#detalharVincConvnCtaSalario(br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharVincConvnCtaSalarioEntradaDTO)
	 */
	public DetalharVincConvnCtaSalarioSaidaDTO detalharVincConvnCtaSalario(DetalharVincConvnCtaSalarioEntradaDTO entrada) {
		DetalharVincConvnCtaSalarioRequest request = new DetalharVincConvnCtaSalarioRequest();
		DetalharVincConvnCtaSalarioResponse response = new DetalharVincConvnCtaSalarioResponse();
		DetalharVincConvnCtaSalarioSaidaDTO saidaDTO = new DetalharVincConvnCtaSalarioSaidaDTO();

		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridica()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContrato()));
		request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContrato()));

		response = getFactoryAdapter().getDetalharVincConvnCtaSalarioPDCAdapter().invokeProcess(request);

		if (response == null) {
			return new DetalharVincConvnCtaSalarioSaidaDTO();
		}

		saidaDTO.setCdAgenciaEmprConvn(response.getCdAgenciaEmprConvn());
		saidaDTO.setCdBcoEmprConvn(response.getCdBcoEmprConvn());
		saidaDTO.setCdConveCtaSalarial(response.getCdConveCtaSalarial());
		saidaDTO.setCdCtaEmprConvn(response.getCdCtaEmprConvn());
		saidaDTO.setCdDigitoEmprConvn(response.getCdDigitoEmprConvn());
		saidaDTO.setCdSitConvCta(response.getCdSitConvCta());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setDsConvCtaSalarial(response.getDsConvCtaSalarial());
		saidaDTO.setDtHoraInclusao(response.getDtInclusao());
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdOperacaoFluxoInclusao(response.getCdOperacaoFluxoInclusao());
		saidaDTO.setDtManutencao(response.getDtManutencao());
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdOperacaoFluxoManutencao(response.getCdOperacaoFluxoManutencao());
		saidaDTO.setDtInclusao(response.getDtInclusao());
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService#alterarVincConvnCtaSalario(br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.AlterarVincConvnCtaSalarioEntradaDTO)
	 */
	public AlterarVincConvnCtaSalarioSaidaDTO alterarVincConvnCtaSalario(AlterarVincConvnCtaSalarioEntradaDTO entrada) {
		AlterarVincConvnCtaSalarioRequest request = new AlterarVincConvnCtaSalarioRequest();
		AlterarVincConvnCtaSalarioResponse response = new AlterarVincConvnCtaSalarioResponse();

		request.setCdAgencCtaEmp(entrada.getCdAgencCtaEmp());
		request.setCdBcoCtaEmp(entrada.getCdBcoCtaEmp());
		request.setCdCnpjEmp(entrada.getCdCnpjEmp());
		request.setCdConveCtaSalarial(entrada.getCdConveCtaSalarial());
		request.setCdConvenNovo(entrada.getCdConvenNovo());
		request.setCdCtrlCnpjEmp(entrada.getCdCtrlCnpjEmp());
		request.setCdDigitoCtaEmp(entrada.getCdDigitoCtaEmp());
		request.setCdFilialEmp(entrada.getCdFilialEmp());
		request.setCdNumeroCtaDest(entrada.getCdNumeroCtaDest());
		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setDsConvCtaEmp(entrada.getDsConvCtaEmp());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());

		response = getFactoryAdapter().getAlterarVincConvnCtaSalarioPDCAdapter().invokeProcess(request);

		if (response == null) {
			return new AlterarVincConvnCtaSalarioSaidaDTO();
		}

		return new AlterarVincConvnCtaSalarioSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService#excluirVincConvnCtaSalario(br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.ExcluirVincConvnCtaSalarioEntradaDTO)
	 */
	public ExcluirVincConvnCtaSalarioSaidaDTO excluirVincConvnCtaSalario(ExcluirVincConvnCtaSalarioEntradaDTO entrada) {
		ExcluirVincConvnCtaSalarioRequest request = new ExcluirVincConvnCtaSalarioRequest();
		ExcluirVincConvnCtaSalarioResponse response = new ExcluirVincConvnCtaSalarioResponse();

		request.setCdPessoaJuridica(entrada.getCdPessoaJuridica());
		request.setCdTipoContrato(entrada.getCdTipoContrato());
		request.setNrSequenciaContrato(entrada.getNrSequenciaContrato());

		response = getFactoryAdapter().getExcluirVincConvnCtaSalarioPDCAdapter().invokeProcess(request);

		if (response == null) {
			return new ExcluirVincConvnCtaSalarioSaidaDTO();
		}

		return new ExcluirVincConvnCtaSalarioSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}

	public FolhaPagamentoSaidaDto detalharFolhaPagamento(FolhaPagamentoEntradaDto entrada) {
		DetalharFolhaRequest request = new DetalharFolhaRequest();
		DetalharFolhaResponse response = new DetalharFolhaResponse();
		FolhaPagamentoSaidaDto saidaDTO = new FolhaPagamentoSaidaDto();
		
		request.setCdpessoaJuridicaContrato(entrada.getCdpessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
		
		response = getFactoryAdapter().getDetalharFolhaPDCAdapter().invokeProcess(request);
		
		if (response == null) {
			return new FolhaPagamentoSaidaDto();
		}
		
		saidaDTO.setCdAberturaContaSalario(response.getCdAberturaContaSalario());
		saidaDTO.setCdIndicadorPagamentoMes(response.getCdIndicadorPagamentoMes());
		saidaDTO.setCdIndicadorPagamentoQuinzena(response.getCdIndicadorPagamentoQuinzena());
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setQtFuncionarioEmpresaPagadora(response.getQtFuncionarioEmpresaPagadora());
		saidaDTO.setVlFolhaPagamentoEmpresa(response.getVlFolhaPagamentoEmpresa());
		saidaDTO.setVlMediaSalarialEmpresa(response.getVlMediaSalarialEmpresa());
		
		return saidaDTO;
	}

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.
     * IManterVinculacaoConvenioContaSalarioService#detalharPiramideSalarial(
     * br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideEntradaDTO)
     */
    public DetalharPiramideSaidaDTO detalharPiramideSalarial(DetalharPiramideEntradaDTO entrada) {
        DetalharPiramideRequest request = new DetalharPiramideRequest();
        request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        
        List<FaixasRenda> faixas = listarFaixasSalariais();
        
        DetalharPiramideResponse response = getFactoryAdapter().getDetalharPiramidePDCAdapter().invokeProcess(request);

        DetalharPiramideSaidaDTO saida = new DetalharPiramideSaidaDTO();
        saida.setVlPagamentoMes(response.getVlPagamentoMes());
        saida.setQtdFuncionarioFlPgto(response.getQtdFuncionarioFlPgto());
        saida.setVlMediaSalarialMes(response.getVlMediaSalarialMes());
        saida.setDtMesAnoCorrespondente(response.getDtMesAnoCorrespondente());
        saida.setCdPagamentoSalarialMes(response.getCdPagamentoSalarialMes());
        saida.setCdPagamentoSalarialQzena(response.getCdPagamentoSalarialQzena());
        saida.setDsLocCtaSalarial(response.getDsLocCtaSalarial());
        saida.setCdIndicadorGrpQuest(response.getCdIndicadorGrpQuest());
        saida.setDsGrupoEconmQuest(response.getDsGrupoEconmQuest());
        saida.setNroGrupoEconmQuest(response.getNroGrupoEconmQuest());
        saida.setCdUdistcAgEmpr(response.getCdUdistcAgEmpr());
        saida.setDsLogradouroEmprQuest(response.getDsLogradouroEmprQuest());
        saida.setDsBairroEmprQuest(response.getDsBairroEmprQuest());
        saida.setDsMuncEmpreQuest(response.getDsMuncEmpreQuest());
        saida.setCdCepEmprQuest(response.getCdCepEmprQuest());
        saida.setCdCepComplemento(response.getCdCepComplemento());
        saida.setDsUfEmprQuest(response.getDsUfEmprQuest());
        saida.setDsRamoAtividadeQuest(response.getDsRamoAtividadeQuest());
        saida.setCdDiscagemDiretaDistEmpr(response.getCdDiscagemDiretaDistEmpr());
        saida.setNumFoneEmprQuest(response.getNumFoneEmprQuest());
        saida.setDsSiteEmprQuest(response.getDsSiteEmprQuest());
        saida.setDsGerRelacionamentoQuest(response.getDsGerRelacionamentoQuest());
        saida.setCdDiscagemDiretaDistanGer(response.getCdDiscagemDiretaDistanGer());
        saida.setNumFoneGerQuest(response.getNumFoneGerQuest());
        saida.setDsAbrevSegmentoEmpr(response.getDsAbrevSegmentoEmpr());
        saida.setDsPagamentoSalDivers(response.getDsPagamentoSalDivers());
        saida.setPctgmPreNovAnoQuest(response.getPctgmPreNovAnoQuest());
        saida.setDsTextoJustfConc(response.getDsTextoJustfConc());
        saida.setCdprefeCsign(response.getCdprefeCsign());
        saida.setDsTextoJustfRecip(response.getDsTextoJustfRecip());
        saida.setCdIndicadorInstaEstrt(response.getCdIndicadorInstaEstrt());
        saida.setDsEstruturaInstaEmpr(response.getDsEstruturaInstaEmpr());
        saida.setDsAgenciaVincInsta(response.getDsAgenciaVincInsta());
        saida.setCdAgenciaVincInsta(response.getCdAgenciaVincInsta());
        saida.setDsIndicadorVisitaInsta(response.getDsIndicadorVisitaInsta());
        saida.setDsGerVisitanteInsta(response.getDsGerVisitanteInsta());
        saida.setCdUespacConcedidoInsta(response.getCdUespacConcedidoInsta());
        saida.setDsTipoNecessInsta(response.getDsTipoNecessInsta());
        saida.setDsIndicadorPerdarelacionamento(response.getDsIndicadorPerdarelacionamento());
        saida.setDsTextoJustfCompl(response.getDsTextoJustfCompl());
        saida.setCdprefePabCren(response.getCdprefePabCren());
        saida.setCdIndicadorPaeCcren(response.getCdIndicadorPaeCcren());
        saida.setCdIndicadorAgCcren(response.getCdIndicadorAgCcren());
        saida.setCdPagamentoSalCredt(response.getCdPagamentoSalCredt());
        saida.setCdPagamentoSalCreque(response.getCdPagamentoSalCreque());
        saida.setCdPagamentoSalDinheiro(response.getCdPagamentoSalDinheiro());
        saida.setCdPagamentoSalDivers(response.getCdPagamentoSalDivers());
        saida.setDsPagamentoSalDivers(response.getDsPagamentoSalDivers());

        saida.setOcorrencias5(new ArrayList<DetalharPiramideOcorrencias5SaidaDTO>());
        //for (Ocorrencias5 o : response.getOcorrencias5()) {
        for(int i = 0; i <  response.getCdFolMaxOcorr(); i++){
            DetalharPiramideOcorrencias5SaidaDTO instituicaoCredito = new DetalharPiramideOcorrencias5SaidaDTO();
            instituicaoCredito.setCdBancoFlQuest(response.getOcorrencias5(i).getCdBancoFlQuest());
            instituicaoCredito.setDsBancoFlQuest(response.getOcorrencias5(i).getDsBancoFlQuest());
            instituicaoCredito.setPctBancoFlQuest(response.getOcorrencias5(i).getPctBancoFlQuest());
            
            saida.getOcorrencias5().add(instituicaoCredito);
        }

        saida.setOcorrencias1(new ArrayList<DetalharPiramideOcorrencias1SaidaDTO>());
        //for (Ocorrencias1 o : response.getOcorrencias1()) {
        for(int i = 0; i <  response.getCdFaiMaxOcorr(); i++){
            DetalharPiramideOcorrencias1SaidaDTO piramide = new DetalharPiramideOcorrencias1SaidaDTO();
            piramide.setNSeqFaixaSalarial(faixas.get(i).getNuSequencialFaixaSalarial());
            piramide.setVlInicialFaixaSalarial(faixas.get(i).getVrInicialFaixaSalarial());
            piramide.setVlFinalFaixaSalarial(faixas.get(i).getVrFinalFaixaSalarial());
            piramide.setQtdFuncionarioFilial01(response.getOcorrencias1(i).getQtdFuncionarioFilial01());
            piramide.setQtdFuncionarioFilial02(response.getOcorrencias1(i).getQtdFuncionarioFilial02());
            piramide.setQtdFuncionarioFilial03(response.getOcorrencias1(i).getQtdFuncionarioFilial03());
            piramide.setQtdFuncionarioFilial04(response.getOcorrencias1(i).getQtdFuncionarioFilial04());
            piramide.setQtdFuncionarioFilial05(response.getOcorrencias1(i).getQtdFuncionarioFilial05());
            piramide.setQtdFuncionarioFilial06(response.getOcorrencias1(i).getQtdFuncionarioFilial06());
            piramide.setQtdFuncionarioFilial07(response.getOcorrencias1(i).getQtdFuncionarioFilial07());
            piramide.setQtdFuncionarioFilial08(response.getOcorrencias1(i).getQtdFuncionarioFilial08());
            piramide.setQtdFuncionarioFilial09(response.getOcorrencias1(i).getQtdFuncionarioFilial09());
            piramide.setQtdFuncionarioFilial10(response.getOcorrencias1(i).getQtdFuncionarioFilial10());

            saida.getOcorrencias1().add(piramide);
            saida
                .setTotalQtdFuncionarioFilial01(saida.getTotalQtdFuncionarioFilial01() + response.getOcorrencias1(i).getQtdFuncionarioFilial01());
            saida
                .setTotalQtdFuncionarioFilial02(saida.getTotalQtdFuncionarioFilial02() + response.getOcorrencias1(i).getQtdFuncionarioFilial02());
            saida
                .setTotalQtdFuncionarioFilial03(saida.getTotalQtdFuncionarioFilial03() + response.getOcorrencias1(i).getQtdFuncionarioFilial03());
            saida
                .setTotalQtdFuncionarioFilial04(saida.getTotalQtdFuncionarioFilial04() + response.getOcorrencias1(i).getQtdFuncionarioFilial04());
            saida
                .setTotalQtdFuncionarioFilial05(saida.getTotalQtdFuncionarioFilial05() + response.getOcorrencias1(i).getQtdFuncionarioFilial05());
            saida
                .setTotalQtdFuncionarioFilial06(saida.getTotalQtdFuncionarioFilial06() + response.getOcorrencias1(i).getQtdFuncionarioFilial06());
            saida
                .setTotalQtdFuncionarioFilial07(saida.getTotalQtdFuncionarioFilial07() + response.getOcorrencias1(i).getQtdFuncionarioFilial07());
            saida
                .setTotalQtdFuncionarioFilial08(saida.getTotalQtdFuncionarioFilial08() + response.getOcorrencias1(i).getQtdFuncionarioFilial08());
            saida
                .setTotalQtdFuncionarioFilial09(saida.getTotalQtdFuncionarioFilial09() + response.getOcorrencias1(i).getQtdFuncionarioFilial09());
            saida
                .setTotalQtdFuncionarioFilial10(saida.getTotalQtdFuncionarioFilial10() + response.getOcorrencias1(i).getQtdFuncionarioFilial10());
            
        }

        saida.setOcorrencias2(new ArrayList<DetalharPiramideOcorrencias2SaidaDTO>());
        //for (Ocorrencias2 o : response.getOcorrencias2()) {
        for(int i = 0; i <  response.getOcorrencias2Count(); i++){
            DetalharPiramideOcorrencias2SaidaDTO piramideRotatividade = new DetalharPiramideOcorrencias2SaidaDTO();
            piramideRotatividade.setCdFaixaRenovQuest(response.getOcorrencias2(i).getCdFaixaRenovQuest());
            piramideRotatividade.setDsFaixaRenovQuest(response.getOcorrencias2(i).getDsFaixaRenovQuest());
            
            Long qtdFuncCasa = response.getOcorrencias2(i).getQtdFuncCasaQuest();
            Long qtdDesligamentoFunc = response.getOcorrencias2(i).getQtdFuncDeslgQuest();
            
            // verifica se qtdFuncCasa � diferente de 0 e qtdFuncDesligado � diferente de 0
            if (NumberUtils.isNumberNotZero(qtdFuncCasa) && NumberUtils.isNumberNotZero(qtdDesligamentoFunc)) {
                piramideRotatividade.setPtcRenovFaixaQuest(calcularPercentual(qtdFuncCasa, qtdDesligamentoFunc));
            }
            
            piramideRotatividade.setQtdFuncCasaQuest(response.getOcorrencias2(i).getQtdFuncCasaQuest());
            piramideRotatividade.setQtdFuncDeslgQuest(response.getOcorrencias2(i).getQtdFuncDeslgQuest());

            saida.getOcorrencias2().add(piramideRotatividade);
            
            Long qtdTotalFuncCasa = saida.getTotalQtdFuncCasaQuest() + response.getOcorrencias2(i).getQtdFuncCasaQuest();
            Long qtdTotalDesligamentoFunc = saida.getTotalQtdFuncDeslgQuest() + response.getOcorrencias2(i).getQtdFuncDeslgQuest();
            
            // verifica se qtdTotalFuncCasa � diferente de 0 e qtdTotalDesligamentoFunc � diferente de 0
            if (NumberUtils.isNumberNotZero(qtdTotalFuncCasa) && NumberUtils.isNumberNotZero(qtdTotalDesligamentoFunc)) {
                saida.setTotalPtcRenovFaixaQuest(calcularPercentual(qtdTotalFuncCasa, qtdTotalDesligamentoFunc));
            }
            
            saida.setTotalQtdFuncCasaQuest(saida.getTotalQtdFuncCasaQuest() + response.getOcorrencias2(i).getQtdFuncCasaQuest());
            saida.setTotalQtdFuncDeslgQuest(saida.getTotalQtdFuncDeslgQuest() + response.getOcorrencias2(i).getQtdFuncDeslgQuest());
        }

        saida.setOcorrencias3(new ArrayList<DetalharPiramideOcorrencias3SaidaDTO>());
//        for (Ocorrencias3 o : response.getOcorrencias3()) {
        for(int i = 0; i <  response.getCdFilMaxOcorr(); i++){
            DetalharPiramideOcorrencias3SaidaDTO filial = new DetalharPiramideOcorrencias3SaidaDTO();
            filial.setCdCepComplQuesf(response.getOcorrencias3(i).getCdCepComplQuesf());
            filial.setCdCepFilialQuest(response.getOcorrencias3(i).getCdCepFilialQuest());
            filial.setCdMunicipioFilialQuest(response.getOcorrencias3(i).getCdMunicipioFilialQuest());
            filial.setCdSegmentoFilialQuest(response.getOcorrencias3(i).getCdSegmentoFilialQuest());
            filial.setCdUfFilialQuest(response.getOcorrencias3(i).getCdUfFilialQuest());
            filial.setDsBairroFilialQuest(response.getOcorrencias3(i).getDsBairroFilialQuest());
            filial.setDsLogradouroFilialQuest(response.getOcorrencias3(i).getDsLogradouroFilialQuest());
            filial.setDsMuncFilialQuest(response.getOcorrencias3(i).getDsMuncFilialQuest());
            filial.setQtdFuncFilalQuest(response.getOcorrencias3(i).getQtdFuncFilalQuest());

            saida.getOcorrencias3().add(filial);
        }

        saida.setOcorrencias4(new ArrayList<DetalharPiramideOcorrencias4SaidaDTO>());
//        for (Ocorrencias4 o : response.getOcorrencias4()) {
        for(int i = 0; i <  response.getCdConMaxOcorr(); i++){
            DetalharPiramideOcorrencias4SaidaDTO banco = new DetalharPiramideOcorrencias4SaidaDTO();
            banco.setCdBancoCredtCsignst(response.getOcorrencias4(i).getCdBancoCredtCsignst());
            banco.setDsBancoCredtCsignst(response.getOcorrencias4(i).getDsBancoCredtCsignst());
            banco.setVlCartCsign(response.getOcorrencias4(i).getVlCartCsign());
            banco.setVlRepasMesConsign(response.getOcorrencias4(i).getVlRepasMesConsign());

            saida.getOcorrencias4().add(banco);
        }

        saida.setOcorrencias6(new ArrayList<DetalharPiramideOcorrencias6SaidaDTO>());
//       for (Ocorrencias6 o : response.getOcorrencias6()) {
        for(int i = 0; i <  response.getCdCocMaxOcorr(); i++){
            DetalharPiramideOcorrencias6SaidaDTO atendimento = new DetalharPiramideOcorrencias6SaidaDTO();
            atendimento.setCdCrenAtendimentoQuest(response.getOcorrencias6(i).getCdCrenAtendimentoQuest());
            atendimento.setCdDistcCrenQuest(response.getOcorrencias6(i).getCdDistcCrenQuest());
            atendimento.setCdUnidadeMedAgQuest(response.getOcorrencias6(i).getCdUnidadeMedAgQuest());
            atendimento.setDsCrenAtendimentoQuest(response.getOcorrencias6(i).getDsCrenAtendimentoQuest());
            atendimento.setQtdCrenQuest(response.getOcorrencias6(i).getQtdCrenQuest());
            atendimento.setQtdFuncCrenQuest(response.getOcorrencias6(i).getQtdFuncCrenQuest());

            saida.getOcorrencias6().add(atendimento);
        }

        return saida;
    }
    
    /* (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.piramidesalarial.IPiramideSalarialService#listarFaixasSalariais()
     * @param
     * @return
     */
    public List < FaixasRenda > listarFaixasSalariais() {
        ListarFaixaSalarialPropostaRequest request = new ListarFaixaSalarialPropostaRequest();
        request.setCdDependencia(0);
        request.setCdUsuario("");
    
        ListarFaixaSalarialPropostaResponse response = factoryAdapter
        .getListarFaixaSalarialPropostaPDCAdapter().invokeProcess(request);
    
        int numeroLinhas = response.getNumeroLinhas();
        List < FaixasRenda > faixasRenda = new ArrayList < FaixasRenda > (numeroLinhas);
        // Popula lista FaixaRenda
        for (int i = 0; i < numeroLinhas; i++) {
            br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.response.Ocorrencias o = response.getOcorrencias(i);
    
            FaixasRenda faixaRenda = new FaixasRenda();
            faixaRenda.setNuSequencialFaixaSalarial(o.getNrFaixa());
            faixaRenda.setVrInicialFaixaSalarial(o.getVlInicialFaixaSalarial());
            faixaRenda.setVrFinalFaixaSalarial(o.getVlFinalFaixaSalarial());
            faixasRenda.add(faixaRenda);
        }
        return faixasRenda;
    }
    
    /**
     * Calcular percentual.
     *
     * @param funcCasaParam the funcionarios casa param
     * @param totalDesligamentoFuncParam the funcionarios desligados param
     * @return the big decimal
     */
    private BigDecimal calcularPercentual(long funcCasaParam, long totalDesligamentoFuncParam) {
        BigDecimal funcCasa = new BigDecimal(funcCasaParam);
        BigDecimal funcDesligados = new BigDecimal(totalDesligamentoFuncParam);
        return CalculadorValorPercentual.calcularPercentual(funcCasa, funcDesligados);
    }

	public ListarDadosConvenioContaSalarioSaidaDTO listarDadosConvenioContaSalario(ListarDadosConvenioContaSalarioEntradaDTO entrada) {
		
		ListarConvenioContaSalarioRequest request = new ListarConvenioContaSalarioRequest();
		request.setCdPessoaJuridicaProposta(entrada.getCodPessoaJuridica());
		request.setCdTipoContratoProposta(entrada.getCodTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada.getNumSeqContratoNegocio());
		
		ListarConvenioContaSalarioResponse response = new ListarConvenioContaSalarioResponse();

		response = getFactoryAdapter().getListarConvenioContaSalarioPDCAdapter().invokeProcess(request);
		
		ListarDadosConvenioContaSalarioSaidaDTO saidaDTO = new ListarDadosConvenioContaSalarioSaidaDTO(response);
		
		return saidaDTO;
	}

	public ListarVincConvnCtaSalarioSaidaDTO listarConvenioContaSalarioNovo(ListarVincConvnCtaSalarioEntradaDTO entrada) {
		
		ListarVincConvnCtaSalarioSaidaDTO saida = new ListarVincConvnCtaSalarioSaidaDTO();
		ListarVincConvnCtaSalarioOcorrenciasDTO ocorrencias;
		ListarConvenioContaSalarioNovoRequest request = new ListarConvenioContaSalarioNovoRequest();
		request.setCodPessoaJuridicaContrato(entrada.getCodPessoaJuridica());
		request.setCodTipoContratoNegocio(entrada.getCodTipoContratoNegocio());
		request.setMaxOcorrencias(entrada.getMaxOcorrencias());
		request.setNumSeqContratoNegocio(entrada.getNumSeqContratoNegocio());
		
		ListarConvenioContaSalarioNovoResponse response = new ListarConvenioContaSalarioNovoResponse();
		
		response = getFactoryAdapter().getListarConvenioContaSalarioNovoPDCAdapter().invokeProcess(request);
		
		saida.setIndicadorConvenioNovo(response.getIndicadorConvenioNovo());
		saida.setUsuarioInclusao(response.getUsuarioInclusao());
		saida.setHorarioInclusao(FormatarData.formatarDataTrilha(response.getHorarioInclusao()));
		saida.setDescCanalInclusao(response.getDescCanalInclusao());
		saida.setDescCanalManutencao(response.getDescCanalManutencao());
		saida.setTipoCanalInclusao(response.getTipoCanalInclusao());
		saida.setTipoCanalManutencao(response.getTipoCanalManutencao());
		saida.setUsuarioManutencao(response.getUsuarioManutencao());
		saida.setHorarioManutencao(FormatarData.formatarDataTrilha(response.getHorarioManutencao()));
		saida.setNumOcorrencias(response.getNumOcorrencias());
		
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ocorrencias = new ListarVincConvnCtaSalarioOcorrenciasDTO();
			ocorrencias.setCodConvenioContaSalario(response.getOcorrencias(i).getCodConvenio());
			ocorrencias.setCpfCnpj(response.getOcorrencias(i).getCpfCnpj());
			ocorrencias.setFilialCpfCnpj(response.getOcorrencias(i).getFilial());
			ocorrencias.setControleCpfCnpj(response.getOcorrencias(i).getControle());
			ocorrencias.setBanco(response.getOcorrencias(i).getBanco());
			ocorrencias.setAgencia(response.getOcorrencias(i).getAgencia());
			ocorrencias.setDigAgencia(response.getOcorrencias(i).getDigitoAgencia());
			ocorrencias.setConta(response.getOcorrencias(i).getConta());
			ocorrencias.setDigConta(response.getOcorrencias(i).getDigitoConta());
			ocorrencias.setCodSituacaoConvenio(response.getOcorrencias(i).getCodSituacaoConvenio());
			ocorrencias.setSituacaoConvenio(response.getOcorrencias(i).getDescSituacaoConvenio());
			
			saida.getListaOcorrencias().add(ocorrencias);			
			
		}
		
		return saida;
	}
}