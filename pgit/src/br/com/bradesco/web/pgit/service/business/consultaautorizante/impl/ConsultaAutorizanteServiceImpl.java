/**
 * Nome: br.com.bradesco.web.pgit.service.business.consultaautorizante.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */ 
package br.com.bradesco.web.pgit.service.business.consultaautorizante.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultaautorizante.IConsultaAutorizanteService;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.IConsultaAutorizanteServiceConstants;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.request.ConsultarAutorizantesNetEmpresaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.response.ConsultarAutorizantesNetEmpresaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ConsultaAutorizanteServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador ConsultaAutorizante
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IConsultaAutorizanteService
 */
public class ConsultaAutorizanteServiceImpl implements IConsultaAutorizanteService {
    
    private FactoryAdapter factoryAdapter;
    
    /**
     * Nome: setFactoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
        this.factoryAdapter = factoryAdapter;
    }
    
    /**
     * Nome: getFactoryAdapter
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }
    
    public ConsultaAutorizanteServiceImpl() {
        
    }

    public ConsultarAutorizantesNetEmpresaSaidaDTO consultarAutorizantesNetEmpresa(
        ConsultarAutorizantesNetEmpresaEntradaDTO entradaDTO) {
        
        ConsultarAutorizantesNetEmpresaRequest request = new ConsultarAutorizantesNetEmpresaRequest();
        request.setMaxOcorrencias(IConsultaAutorizanteServiceConstants.MAX_OCORRENCIAS_80);
        String retiraChaves = PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamento());
		retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
		request.setCdControlePagamento(retiraChaves);
        request.setCdModalidade(PgitUtil.verificaIntegerNulo(entradaDTO.getCdModalidade()));
        request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoCanal()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdAgendadosPagoNaoPago(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadosPagoNaoPago()));
        request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));
        request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));
        request.setCdContaBancariaDestino(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));
        request.setCdDigitoAgenciaDebito(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoAgenciaDebito()));
        
        ConsultarAutorizantesNetEmpresaResponse response = getFactoryAdapter().getConsultarAutorizantesNetEmpresaPDCAdapter().invokeProcess(request);
        
        ConsultarAutorizantesNetEmpresaSaidaDTO saidaDTO = new ConsultarAutorizantesNetEmpresaSaidaDTO();
      
                saidaDTO.setCodMensagem(response.getCodMensagem());
                saidaDTO.setMensagem(response.getMensagem());
                saidaDTO.setCdPessoaJuridCont(response.getCdPessoaJuridCont());
                saidaDTO.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
                saidaDTO.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
                saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
                saidaDTO.setDtEfetivacaoPagamento(response.getDtEfetivacaoPagamento());
                saidaDTO.setCdBancoDescricao(response.getCdBancoDescricao());
                saidaDTO.setCdAgenciaBancariaDescricao(response.getCdAgenciaBancariaDescricao());
                saidaDTO.setCdContaBancariaDescricao(response.getCdContaBancariaDescricao());
                saidaDTO.setCdAgendadosPagoNaoPago(response.getCdAgendadosPagoNaoPago());
                saidaDTO.setNrArquivoRemssaPagamento(response.getNrArquivoRemssaPagamento());
                saidaDTO.setCdRejeicaoLoteComprovante(response.getCdRejeicaoLoteComprovante());
                saidaDTO.setCdListaDebitoPagamento(response.getCdListaDebitoPagamento());
                saidaDTO.setCdTipoCanal(response.getCdTipoCanal());
                saidaDTO.setDsTipoCanal(response.getDsTipoCanal());
                saidaDTO.setVlPagamento(response.getVlPagamento());
                saidaDTO.setDsModalidade(response.getDsModalidade());
                saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
                saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
                saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
                saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
                saidaDTO.setDsSituacaoContrato(response.getDsSituacaoContrato());
                saidaDTO.setDsContrato(response.getDsContrato());
                saidaDTO.setDtVencimento(response.getDtVencimento());
                saidaDTO.setCdSituacaoPagamentoCliente(response.getCdSituacaoPagamentoCliente());
                saidaDTO.setDsSituacaoPagamentoCliente(response.getDsSituacaoPagamentoCliente());
                saidaDTO.setCdMotivoPagamentoCliente(response.getCdMotivoPagamentoCliente());
                saidaDTO.setDsMotivoPagamentoCliente(response.getDsMotivoPagamentoCliente());
                saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
                saidaDTO.setDsBancoCredito(response.getDsBancoCredito());
                saidaDTO.setCdAgenciaBancariaCredito(response.getCdAgenciaBancariaCredito());
                saidaDTO.setCdDigitoAgenciaCredito(response.getCdDigitoAgenciaCredito());
                saidaDTO.setDsAgenciaCredito(response.getDsAgenciaCredito());
                saidaDTO.setCdContaBancariaCredito(response.getCdContaBancariaCredito());
                saidaDTO.setCdDigitoContaCredito(response.getCdDigitoContaCredito());
                saidaDTO.setCdTipoContaCredito(response.getCdTipoContaCredito());
                saidaDTO.setCdTipoIsncricaoFavorecido(response.getCdTipoIsncricaoFavorecido());
                saidaDTO.setCdCtciaInscricaoFavorecido(response.getCdCtciaInscricaoFavorecido());
                saidaDTO.setDtCreditoPagamento(response.getDtCreditoPagamento());
                saidaDTO.setNumeroConsulta(response.getNumeroConsulta());
                
                List<ConsultarAutorizantesOcorrenciaSaidaDTO> lista = new ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO>();
                
                for (int i = 0; i < response.getOcorrenciasCount(); i++) {
                    ConsultarAutorizantesOcorrenciaSaidaDTO ocorrencia = new ConsultarAutorizantesOcorrenciaSaidaDTO();
                    ocorrencia.setCdAcaoEvento(response.getOcorrencias(i).getCdAcaoEvento());
                    ocorrencia.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
                    ocorrencia.setCdTipoCpfCnpj(response.getOcorrencias(i).getCdTipocpfcnpj());
                    ocorrencia.setDsAcaoEvento(response.getOcorrencias(i).getDsAcaoEvento());
                    ocorrencia.setDsAutorizante(response.getOcorrencias(i).getDsAutorizante());
                    ocorrencia.setDtHoraManutencao(response.getOcorrencias(i).getDtHoraManutencao());
                    ocorrencia.setQtdAutorizadoPagamento(response.getOcorrencias(i).getQtdAutorizadoPagamento());
                    lista.add(ocorrencia);
                }
                saidaDTO.setOcorrencias(lista);
                      
        return saidaDTO;

    }
}