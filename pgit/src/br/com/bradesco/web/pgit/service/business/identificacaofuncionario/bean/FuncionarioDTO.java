/*
 * Nome: br.com.bradesco.web.pgit.service.business.identificacaofuncionario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.identificacaofuncionario.bean;

/**
 * Nome: FuncionarioDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FuncionarioDTO {
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo nome. */
	private String nome;
	
	/** Atributo codigoJuncao. */
	private String codigoJuncao;
	
	/** Atributo codigoSecao. */
	private String codigoSecao;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/**
	 * Get: codigoJuncao.
	 *
	 * @return codigoJuncao
	 */
	public String getCodigoJuncao() {
		return codigoJuncao;
	}
	
	/**
	 * Set: codigoJuncao.
	 *
	 * @param codigoJuncao the codigo juncao
	 */
	public void setCodigoJuncao(String codigoJuncao) {
		this.codigoJuncao = codigoJuncao;
	}
	
	/**
	 * Get: codigoSecao.
	 *
	 * @return codigoSecao
	 */
	public String getCodigoSecao() {
		return codigoSecao;
	}
	
	/**
	 * Set: codigoSecao.
	 *
	 * @param codigoSecao the codigo secao
	 */
	public void setCodigoSecao(String codigoSecao) {
		this.codigoSecao = codigoSecao;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	
	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Get: nome.
	 *
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Set: nome.
	 *
	 * @param nome the nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	
	

}
