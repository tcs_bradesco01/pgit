/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarMotivoBloqueioContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarMotivoBloqueioContaSaidaDTO {
	
	/** Atributo cdMotivoBloqueioConta. */
	private Integer cdMotivoBloqueioConta;
    
    /** Atributo dsMotivoBloqueioConta. */
    private String dsMotivoBloqueioConta;
    
	/**
	 * Consultar motivo bloqueio conta saida dto.
	 */
	public ConsultarMotivoBloqueioContaSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	/**
	 * Consultar motivo bloqueio conta saida dto.
	 *
	 * @param cdMotivoBloqueioConta the cd motivo bloqueio conta
	 * @param dsMotivoBloqueioConta the ds motivo bloqueio conta
	 */
	public ConsultarMotivoBloqueioContaSaidaDTO(Integer cdMotivoBloqueioConta, String dsMotivoBloqueioConta) {
		super();
		this.cdMotivoBloqueioConta = cdMotivoBloqueioConta;
		this.dsMotivoBloqueioConta = dsMotivoBloqueioConta;
	}

	/**
	 * Get: cdMotivoBloqueioConta.
	 *
	 * @return cdMotivoBloqueioConta
	 */
	public Integer getCdMotivoBloqueioConta() {
		return cdMotivoBloqueioConta;
	}
	
	/**
	 * Set: cdMotivoBloqueioConta.
	 *
	 * @param cdMotivoBloqueioConta the cd motivo bloqueio conta
	 */
	public void setCdMotivoBloqueioConta(Integer cdMotivoBloqueioConta) {
		this.cdMotivoBloqueioConta = cdMotivoBloqueioConta;
	}
	
	/**
	 * Get: dsMotivoBloqueioConta.
	 *
	 * @return dsMotivoBloqueioConta
	 */
	public String getDsMotivoBloqueioConta() {
		return dsMotivoBloqueioConta;
	}
	
	/**
	 * Set: dsMotivoBloqueioConta.
	 *
	 * @param dsMotivoBloqueioConta the ds motivo bloqueio conta
	 */
	public void setDsMotivoBloqueioConta(String dsMotivoBloqueioConta) {
		this.dsMotivoBloqueioConta = dsMotivoBloqueioConta;
	}

}
