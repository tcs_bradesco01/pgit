/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoservicocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSituacaoProdutoServico
     */
    private int _cdSituacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdSituacaoProdutoServico
     */
    private boolean _has_cdSituacaoProdutoServico;

    /**
     * Field _rsSituacaoProdutoServico
     */
    private java.lang.String _rsSituacaoProdutoServico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoservicocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoProdutoServico
     * 
     */
    public void deleteCdSituacaoProdutoServico()
    {
        this._has_cdSituacaoProdutoServico= false;
    } //-- void deleteCdSituacaoProdutoServico() 

    /**
     * Returns the value of field 'cdSituacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoProdutoServico'.
     */
    public int getCdSituacaoProdutoServico()
    {
        return this._cdSituacaoProdutoServico;
    } //-- int getCdSituacaoProdutoServico() 

    /**
     * Returns the value of field 'rsSituacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'rsSituacaoProdutoServico'.
     */
    public java.lang.String getRsSituacaoProdutoServico()
    {
        return this._rsSituacaoProdutoServico;
    } //-- java.lang.String getRsSituacaoProdutoServico() 

    /**
     * Method hasCdSituacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoProdutoServico()
    {
        return this._has_cdSituacaoProdutoServico;
    } //-- boolean hasCdSituacaoProdutoServico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoProdutoServico'.
     * 
     * @param cdSituacaoProdutoServico the value of field
     * 'cdSituacaoProdutoServico'.
     */
    public void setCdSituacaoProdutoServico(int cdSituacaoProdutoServico)
    {
        this._cdSituacaoProdutoServico = cdSituacaoProdutoServico;
        this._has_cdSituacaoProdutoServico = true;
    } //-- void setCdSituacaoProdutoServico(int) 

    /**
     * Sets the value of field 'rsSituacaoProdutoServico'.
     * 
     * @param rsSituacaoProdutoServico the value of field
     * 'rsSituacaoProdutoServico'.
     */
    public void setRsSituacaoProdutoServico(java.lang.String rsSituacaoProdutoServico)
    {
        this._rsSituacaoProdutoServico = rsSituacaoProdutoServico;
    } //-- void setRsSituacaoProdutoServico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoservicocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoservicocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoservicocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoservicocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
