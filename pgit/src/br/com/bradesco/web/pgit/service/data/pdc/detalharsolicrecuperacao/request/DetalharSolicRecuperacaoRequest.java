/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolicRecuperacaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolicRecuperacaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrSolicitacaoRecuperacao
     */
    private int _nrSolicitacaoRecuperacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacaoRecuperacao
     */
    private boolean _has_nrSolicitacaoRecuperacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolicRecuperacaoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.request.DetalharSolicRecuperacaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNrSolicitacaoRecuperacao
     * 
     */
    public void deleteNrSolicitacaoRecuperacao()
    {
        this._has_nrSolicitacaoRecuperacao= false;
    } //-- void deleteNrSolicitacaoRecuperacao() 

    /**
     * Returns the value of field 'nrSolicitacaoRecuperacao'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoRecuperacao'.
     */
    public int getNrSolicitacaoRecuperacao()
    {
        return this._nrSolicitacaoRecuperacao;
    } //-- int getNrSolicitacaoRecuperacao() 

    /**
     * Method hasNrSolicitacaoRecuperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoRecuperacao()
    {
        return this._has_nrSolicitacaoRecuperacao;
    } //-- boolean hasNrSolicitacaoRecuperacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'nrSolicitacaoRecuperacao'.
     * 
     * @param nrSolicitacaoRecuperacao the value of field
     * 'nrSolicitacaoRecuperacao'.
     */
    public void setNrSolicitacaoRecuperacao(int nrSolicitacaoRecuperacao)
    {
        this._nrSolicitacaoRecuperacao = nrSolicitacaoRecuperacao;
        this._has_nrSolicitacaoRecuperacao = true;
    } //-- void setNrSolicitacaoRecuperacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolicRecuperacaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.request.DetalharSolicRecuperacaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.request.DetalharSolicRecuperacaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.request.DetalharSolicRecuperacaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.request.DetalharSolicRecuperacaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
