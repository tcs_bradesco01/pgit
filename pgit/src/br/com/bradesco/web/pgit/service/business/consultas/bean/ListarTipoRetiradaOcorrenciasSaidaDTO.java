/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/10/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

import java.math.BigDecimal;

/**
 * Nome: ListarTipoRetiradaOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarTipoRetiradaOcorrenciasSaidaDTO {

    /** Atributo cdFormaPagamento. */
    private Integer cdFormaPagamento = null;

    /** Atributo dsFormaPagamento. */
    private String dsFormaPagamento = null;

    /** Atributo qtPagamento. */
    private Integer qtPagamento = null;

    /** Atributo vlPagamento. */
    private BigDecimal vlPagamento = null;

    /**
     * Set: cdFormaPagamento.
     * 
     * @param cdFormaPagamento
     *            the cd forma pagamento
     */
    public void setCdFormaPagamento(Integer cdFormaPagamento) {
        this.cdFormaPagamento = cdFormaPagamento;
    }

    /**
     * Get: cdFormaPagamento.
     * 
     * @return cdFormaPagamento
     */
    public Integer getCdFormaPagamento() {
        return this.cdFormaPagamento;
    }

    /**
     * Set: dsFormaPagamento.
     * 
     * @param dsFormaPagamento
     *            the ds forma pagamento
     */
    public void setDsFormaPagamento(String dsFormaPagamento) {
        this.dsFormaPagamento = dsFormaPagamento;
    }

    /**
     * Get: dsFormaPagamento.
     * 
     * @return dsFormaPagamento
     */
    public String getDsFormaPagamento() {
        return this.dsFormaPagamento;
    }

    /**
     * Set: qtPagamento.
     * 
     * @param qtPagamento
     *            the qt pagamento
     */
    public void setQtPagamento(Integer qtPagamento) {
        this.qtPagamento = qtPagamento;
    }

    /**
     * Get: qtPagamento.
     * 
     * @return qtPagamento
     */
    public Integer getQtPagamento() {
        return this.qtPagamento;
    }

    /**
     * Set: vlPagamento.
     * 
     * @param vlPagamento
     *            the vl pagamento
     */
    public void setVlPagamento(BigDecimal vlPagamento) {
        this.vlPagamento = vlPagamento;
    }

    /**
     * Get: vlPagamento.
     * 
     * @return vlPagamento
     */
    public BigDecimal getVlPagamento() {
        return this.vlPagamento;
    }
}