/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarSoliRelatorioEmpresaAcompanhadaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarSoliRelatorioEmpresaAcompanhadaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maxOcorrencias
     */
    private int _maxOcorrencias = 0;

    /**
     * keeps track of state for field: _maxOcorrencias
     */
    private boolean _has_maxOcorrencias;

    /**
     * Field _dtInicio
     */
    private java.lang.String _dtInicio;

    /**
     * Field _dtFim
     */
    private java.lang.String _dtFim;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarSoliRelatorioEmpresaAcompanhadaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.request.ListarSoliRelatorioEmpresaAcompanhadaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteMaxOcorrencias
     * 
     */
    public void deleteMaxOcorrencias()
    {
        this._has_maxOcorrencias= false;
    } //-- void deleteMaxOcorrencias() 

    /**
     * Returns the value of field 'dtFim'.
     * 
     * @return String
     * @return the value of field 'dtFim'.
     */
    public java.lang.String getDtFim()
    {
        return this._dtFim;
    } //-- java.lang.String getDtFim() 

    /**
     * Returns the value of field 'dtInicio'.
     * 
     * @return String
     * @return the value of field 'dtInicio'.
     */
    public java.lang.String getDtInicio()
    {
        return this._dtInicio;
    } //-- java.lang.String getDtInicio() 

    /**
     * Returns the value of field 'maxOcorrencias'.
     * 
     * @return int
     * @return the value of field 'maxOcorrencias'.
     */
    public int getMaxOcorrencias()
    {
        return this._maxOcorrencias;
    } //-- int getMaxOcorrencias() 

    /**
     * Method hasMaxOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaxOcorrencias()
    {
        return this._has_maxOcorrencias;
    } //-- boolean hasMaxOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dtFim'.
     * 
     * @param dtFim the value of field 'dtFim'.
     */
    public void setDtFim(java.lang.String dtFim)
    {
        this._dtFim = dtFim;
    } //-- void setDtFim(java.lang.String) 

    /**
     * Sets the value of field 'dtInicio'.
     * 
     * @param dtInicio the value of field 'dtInicio'.
     */
    public void setDtInicio(java.lang.String dtInicio)
    {
        this._dtInicio = dtInicio;
    } //-- void setDtInicio(java.lang.String) 

    /**
     * Sets the value of field 'maxOcorrencias'.
     * 
     * @param maxOcorrencias the value of field 'maxOcorrencias'.
     */
    public void setMaxOcorrencias(int maxOcorrencias)
    {
        this._maxOcorrencias = maxOcorrencias;
        this._has_maxOcorrencias = true;
    } //-- void setMaxOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarSoliRelatorioEmpresaAcompanhadaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.request.ListarSoliRelatorioEmpresaAcompanhadaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.request.ListarSoliRelatorioEmpresaAcompanhadaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.request.ListarSoliRelatorioEmpresaAcompanhadaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.request.ListarSoliRelatorioEmpresaAcompanhadaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
