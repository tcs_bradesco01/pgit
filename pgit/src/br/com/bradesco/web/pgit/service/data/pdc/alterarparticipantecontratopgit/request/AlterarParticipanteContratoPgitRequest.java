/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarParticipanteContratoPgitRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarParticipanteContratoPgitRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdTipoParticipante
     */
    private int _cdTipoParticipante = 0;

    /**
     * keeps track of state for field: _cdTipoParticipante
     */
    private boolean _has_cdTipoParticipante;

    /**
     * Field _cdPessoaParticipacao
     */
    private long _cdPessoaParticipacao = 0;

    /**
     * keeps track of state for field: _cdPessoaParticipacao
     */
    private boolean _has_cdPessoaParticipacao;

    /**
     * Field _cdclassificacaoAreaParticipante
     */
    private int _cdclassificacaoAreaParticipante = 0;

    /**
     * keeps track of state for field:
     * _cdclassificacaoAreaParticipante
     */
    private boolean _has_cdclassificacaoAreaParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarParticipanteContratoPgitRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.request.AlterarParticipanteContratoPgitRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaParticipacao
     * 
     */
    public void deleteCdPessoaParticipacao()
    {
        this._has_cdPessoaParticipacao= false;
    } //-- void deleteCdPessoaParticipacao() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoParticipante
     * 
     */
    public void deleteCdTipoParticipante()
    {
        this._has_cdTipoParticipante= false;
    } //-- void deleteCdTipoParticipante() 

    /**
     * Method deleteCdclassificacaoAreaParticipante
     * 
     */
    public void deleteCdclassificacaoAreaParticipante()
    {
        this._has_cdclassificacaoAreaParticipante= false;
    } //-- void deleteCdclassificacaoAreaParticipante() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaParticipacao'.
     * 
     * @return long
     * @return the value of field 'cdPessoaParticipacao'.
     */
    public long getCdPessoaParticipacao()
    {
        return this._cdPessoaParticipacao;
    } //-- long getCdPessoaParticipacao() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoParticipante'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipante'.
     */
    public int getCdTipoParticipante()
    {
        return this._cdTipoParticipante;
    } //-- int getCdTipoParticipante() 

    /**
     * Returns the value of field
     * 'cdclassificacaoAreaParticipante'.
     * 
     * @return int
     * @return the value of field 'cdclassificacaoAreaParticipante'.
     */
    public int getCdclassificacaoAreaParticipante()
    {
        return this._cdclassificacaoAreaParticipante;
    } //-- int getCdclassificacaoAreaParticipante() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaParticipacao()
    {
        return this._has_cdPessoaParticipacao;
    } //-- boolean hasCdPessoaParticipacao() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipante()
    {
        return this._has_cdTipoParticipante;
    } //-- boolean hasCdTipoParticipante() 

    /**
     * Method hasCdclassificacaoAreaParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdclassificacaoAreaParticipante()
    {
        return this._has_cdclassificacaoAreaParticipante;
    } //-- boolean hasCdclassificacaoAreaParticipante() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaParticipacao'.
     * 
     * @param cdPessoaParticipacao the value of field
     * 'cdPessoaParticipacao'.
     */
    public void setCdPessoaParticipacao(long cdPessoaParticipacao)
    {
        this._cdPessoaParticipacao = cdPessoaParticipacao;
        this._has_cdPessoaParticipacao = true;
    } //-- void setCdPessoaParticipacao(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoParticipante'.
     * 
     * @param cdTipoParticipante the value of field
     * 'cdTipoParticipante'.
     */
    public void setCdTipoParticipante(int cdTipoParticipante)
    {
        this._cdTipoParticipante = cdTipoParticipante;
        this._has_cdTipoParticipante = true;
    } //-- void setCdTipoParticipante(int) 

    /**
     * Sets the value of field 'cdclassificacaoAreaParticipante'.
     * 
     * @param cdclassificacaoAreaParticipante the value of field
     * 'cdclassificacaoAreaParticipante'.
     */
    public void setCdclassificacaoAreaParticipante(int cdclassificacaoAreaParticipante)
    {
        this._cdclassificacaoAreaParticipante = cdclassificacaoAreaParticipante;
        this._has_cdclassificacaoAreaParticipante = true;
    } //-- void setCdclassificacaoAreaParticipante(int) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarParticipanteContratoPgitRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.request.AlterarParticipanteContratoPgitRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.request.AlterarParticipanteContratoPgitRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.request.AlterarParticipanteContratoPgitRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.request.AlterarParticipanteContratoPgitRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
