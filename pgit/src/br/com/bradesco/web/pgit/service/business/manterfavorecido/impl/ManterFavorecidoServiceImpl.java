/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterfavorecido.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheHistoricoContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheHistoricoContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.DetalharHistoricoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.DetalharHistoricoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearcontafavorecido.request.BloquearContaFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearcontafavorecido.response.BloquearContaFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.request.BloquearFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.response.BloquearFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.request.ConsultarDetalheContaFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.response.ConsultarDetalheContaFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.request.ConsultarDetalheFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.response.ConsultarDetalheFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.request.ConsultarDetalheHistoricoContaFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.response.ConsultarDetalheHistoricoContaFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.request.ConsultarListaContaFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response.ConsultarListaContaFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistahistoricocontafavorecido.request.ConsultarListaHistoricoContaFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistahistoricocontafavorecido.response.ConsultarListaHistoricoContaFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistahistoricofavorecido.request.ConsultarListaHistoricoFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistahistoricofavorecido.response.ConsultarListaHistoricoFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricofavorecido.request.DetalharHistoricoFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricofavorecido.response.DetalharHistoricoFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.request.ListarFavorecidosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response.ListarFavorecidosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.CamposUtils;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.constants.DataConstants;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterFavorecido
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterFavorecidoServiceImpl implements IManterFavorecidoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Manter favorecido service impl.
	 */
	public ManterFavorecidoServiceImpl() {
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#pesquisarFavorecidos(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarFavorecidoEntradaDTO)
	 */
	public List<ListarFavorecidoSaidaDTO> pesquisarFavorecidos(ListarFavorecidoEntradaDTO entrada) {

		ListarFavorecidosRequest request = new ListarFavorecidosRequest();

		request.setCdFavorecidoClientePagador(entrada.getCdFavorecidoClientePagador() == null ? 0L : entrada.getCdFavorecidoClientePagador());
		request.setCdInscricaoFavorecidoCliente(entrada.getCdInscricaoFavorecidoCliente() == null ? 0 : entrada.getCdInscricaoFavorecidoCliente());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0 : entrada.getCdPessoaJuridicaContrato());
		request.setCdSituacaoFavorecidoCliente(entrada.getCdSituacaoFavorecidoCliente() == null ? 0 : entrada.getCdSituacaoFavorecidoCliente());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada.getCdTipoContratoNegocio());
		request.setCdTipoFavorecido(entrada.getCdTipoFavorecido() == null ? 0 : entrada.getCdTipoFavorecido());
		request.setCdTipoInscricaoFavorecido(entrada.getCdTipoInscricaoFavorecido() == null ? 0 : entrada.getCdTipoInscricaoFavorecido());
		request.setNmFavorecido(entrada.getNmFavorecido() == null ? "" : entrada.getNmFavorecido());
		request.setNrSequencialContratoNegocio(entrada.getNrSequencialContratoNegocio() == null ? 0 : entrada.getNrSequencialContratoNegocio());
		request.setNumeroOcorrencias(50);

		ListarFavorecidosResponse response = getFactoryAdapter().getListarFavorecidosPDCAdapter().invokeProcess(request);

		List<ListarFavorecidoSaidaDTO> list = new ArrayList<ListarFavorecidoSaidaDTO>();

		for (Ocorrencias saida : response.getOcorrencias()) {
			list.add(new ListarFavorecidoSaidaDTO(saida));
		}

		return list;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#confirmarBloquearFavorecido(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearFavorecidoEntradaDTO)
	 */
	public BloquearFavorecidoSaidaDTO confirmarBloquearFavorecido(BloquearFavorecidoEntradaDTO entrada) {

		BloquearFavorecidoRequest request = new BloquearFavorecidoRequest();

		request.setCdFavoritoClientePagador(entrada.getCdFavoritoClientePagador() == null ? 0L : entrada.getCdFavoritoClientePagador());
		request.setCdMotivoBloqueioFavorecido(entrada.getCdMotivoBloqueioFavorecido() == null ? 0 : entrada.getCdMotivoBloqueioFavorecido());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0 : entrada.getCdPessoaJuridicaContrato());
		request.setCdSituacaoFavorecido(entrada.getCdSituacaoFavorecido() == null ? 0 : entrada.getCdSituacaoFavorecido());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada.getCdTipoContratoNegocio());
		request.setNrSequencialContratoNegocio(entrada.getNrSequencialContratoNegocio() == null ? 0 : entrada.getNrSequencialContratoNegocio());
		
		BloquearFavorecidoResponse reponse = getFactoryAdapter().getBloquearFavorecidoPDCAdapter().invokeProcess(request);

		return new BloquearFavorecidoSaidaDTO(reponse.getCodMensagem(), reponse.getMensagem());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#confirmarBloqueoContaFavorecido(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearContaFavorecidoEntradaDTO)
	 */
	public BloquearContaFavorecidoSaidaDTO confirmarBloqueoContaFavorecido(BloquearContaFavorecidoEntradaDTO entrada) {

		BloquearContaFavorecidoRequest request = new BloquearContaFavorecidoRequest();

		request.setCdFavorecidoClientePagador(entrada.getCdFavorecidoClientePagador() == null ? 0L : entrada.getCdFavorecidoClientePagador());
		request.setCdMotivoBloqueioContrato(entrada.getCdMotivoBloqueioContrato() == null ? 0 : entrada.getCdMotivoBloqueioContrato());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0L : entrada.getCdPessoaJuridicaContrato());
		request.setCdSituacaoContrato(entrada.getCdSituacaoContrato() == null ? 0 : entrada.getCdSituacaoContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio() == null ? 0 : entrada.getNrSequenciaContratoNegocio());
		request.setNrOcorrenciaContaFavorecido(entrada.getNrOcorrenciaContaFavorecido() == null ? 0 : entrada.getNrOcorrenciaContaFavorecido());
		
		BloquearContaFavorecidoResponse response = getFactoryAdapter().getBloquearContaFavorecidoPDCAdapter().invokeProcess(request);

		return new BloquearContaFavorecidoSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#consultarContaFavorecido(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaContaFavorecidoEntradaDTO)
	 */
	public List<ConsultarListaContaFavorecidoSaidaDTO> consultarContaFavorecido(ConsultarListaContaFavorecidoEntradaDTO entrada) {

		ConsultarListaContaFavorecidoRequest request = new ConsultarListaContaFavorecidoRequest();

		request.setCdFavorecidoClientePagador(entrada.getCdFavorecidoClientePagador() == null ? 0L : entrada.getCdFavorecidoClientePagador());
		request.setCdPessoaJuridContrato(entrada.getCdPessoaJuridContrato() == null ? 0L : entrada.getCdPessoaJuridContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada.getCdTipoContratoNegocio());
		request.setNrSeqContratoNegocio(entrada.getNrSeqContratoNegocio() == null ? 0L : entrada.getNrSeqContratoNegocio());
		request.setNrOcorrencias(50);

		ConsultarListaContaFavorecidoResponse response = getFactoryAdapter().getConsultarListaContaFavorecidoPDCAdapter().invokeProcess(request);

		List<ConsultarListaContaFavorecidoSaidaDTO> lista = new ArrayList<ConsultarListaContaFavorecidoSaidaDTO>();

		for (br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response.Ocorrencias saida : response.getOcorrencias()) {
			lista.add(new ConsultarListaContaFavorecidoSaidaDTO(saida));
		}

		return lista;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#consultarDetalheContaFavorecido(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheContaFavorecidoEntradaDTO)
	 */
	public ConsultarDetalheContaFavorecidoSaidaDTO consultarDetalheContaFavorecido(ConsultarDetalheContaFavorecidoEntradaDTO entrada) {

		ConsultarDetalheContaFavorecidoRequest request = new ConsultarDetalheContaFavorecidoRequest();

		request.setCdFavorecidoClientePagador(entrada.getCdFavorecidoClientePagador() == null ? 0L : entrada.getCdFavorecidoClientePagador());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0L : entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContaFavorecido(entrada.getCdTipoContaFavorecido() == null ? 0 : entrada.getCdTipoContaFavorecido());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada.getCdTipoContratoNegocio());
		request.setNrOcorrenciaContaFavorecido(entrada.getNrOcorrenciaContaFavorecido() == null ? 0 : entrada.getNrOcorrenciaContaFavorecido());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio() == null ? 0 : entrada.getNrSequenciaContratoNegocio());

		ConsultarDetalheContaFavorecidoResponse response = getFactoryAdapter().getConsultarDetalheContaFavorecidoPDCAdapter().invokeProcess(request);

		ConsultarDetalheContaFavorecidoSaidaDTO saida = new ConsultarDetalheContaFavorecidoSaidaDTO();

		saida.setTpConta(response.getTpConta());
		saida.setDsBloqueio(response.getDsBloqueio());
		saida.setCdBloqueioContaFavorecido(response.getCdBloqueioContaFavorecido());
		saida.setDsObservacaoContaFavorecida(response.getDsObservacaoContaFavorecida());
		saida.setCdIndicadorRetornoCliente(response.getCdIndicadorRetornoCliente());
		saida.setCdSituacaoContaFavorecido(response.getCdSituacaoContaFavorecido());
		saida.setHrBloqueioContaFavorecido(DateUtils.formartarDataPDCparaPadraPtBr(response.getHrBloqueioContaFavorecido(), DataConstants.DATA_PDC, DataConstants.DATA_PADRA));
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setDtInclusao(DateUtils.trocarSeparadorDeData(response.getDtInclusao(),".","/"));
		saida.setHrInclusao(response.getHrInclusao());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setDsCanalInclusao(CamposUtils.formatadorCampos(response.getCdTipoCanalInclusao(), response.getDsCanalInclusao()));
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterna(response.getCdUsuarioManutencaoExterna());
		saida.setDtManutencao(DateUtils.trocarSeparadorDeData(response.getDtManutencao(),".","/"));
		saida.setHrManutencao(response.getHrManutencao());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setDsCanalManutencao(CamposUtils.formatadorCampos(response.getCdTipoCanalManutencao(), response.getDsCanalManutencao()));

		return saida;

	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#consultarDetalheFavorecido(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheFavorecidoEntradaDTO)
	 */
	public ConsultarDetalheFavorecidoSaidaDTO consultarDetalheFavorecido(ConsultarDetalheFavorecidoEntradaDTO entrada) {

		ConsultarDetalheFavorecidoRequest request = new ConsultarDetalheFavorecidoRequest();

		request.setCdFavorecidoClientePagador(entrada.getCdFavorecidoClientePagador() == null ? 0L : entrada.getCdFavorecidoClientePagador());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0 : entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio() == null ? 0L : entrada.getNrSequenciaContratoNegocio());

		ConsultarDetalheFavorecidoResponse response = getFactoryAdapter().getConsultarDetalheFavorecidoPDCAdapter().invokeProcess(request);

		ConsultarDetalheFavorecidoSaidaDTO saida = new ConsultarDetalheFavorecidoSaidaDTO();

		saida.setDsTipoFavorecido(response.getDsTipoFavorecido());
		saida.setDsInscricaoFavorecido(response.getDsInscricaoFavorecido());
		saida.setMtBloqueio(response.getMtBloqueio());
		saida.setCdTipoFavorecido(response.getCdTipoFavorecido());
		saida.setCdTipoInscricaoFavorecido(response.getCdTipoInscricaoFavorecido());
		saida.setCdMotivoBloqueioFavorecido(response.getCdMotivoBloqueioFavorecido());
		saida.setCdInscricaoFavorecidoCliente(response.getCdInscricaoFavorecidoCliente());
		saida.setDsCompletaFavorecidoCliente(response.getDsCompletaFavorecidoCliente());
		saida.setDsAbreviacaoFavorecidoCliente(response.getDsAbreviacaoFavorecidoCliente());
		saida.setDsOrganizacaoEmissorDocumento(response.getDsOrganizacaoEmissorDocumento());
		saida.setDtExpedicaoDocumentoFavorecido(DateUtils.trocarSeparadorDeData(response.getDtExpedicaoDocumentoFavorecido(),".","/"));
		saida.setCdSexoFavorecidoCliente(response.getCdSexoFavorecidoCliente());
		saida.setCdAreaFavorecidoCliente(response.getCdAreaFavorecidoCliente());
		saida.setCdFoneFavorecidoCliente(response.getCdFoneFavorecidoCliente());
		saida.setCdAreaFaxFavorecido(response.getCdAreaFaxFavorecido());
		saida.setCdFaxFavorecidoCliente(response.getCdFaxFavorecidoCliente());
		saida.setCdRamalFaxFavorecido(response.getCdRamalFaxFavorecido());
		saida.setCdAreaCelularFavorecido(response.getCdAreaCelularFavorecido());
		saida.setCdCelularFavorecidoCliente(response.getCdCelularFavorecidoCliente());
		saida.setCdAreaFoneComercial(response.getCdAreaFoneComercial());
		saida.setCdFoneComercialFavorecido(response.getCdFoneComercialFavorecido());
		saida.setCdRamalComercialFavorecido(response.getCdRamalComercialFavorecido());
		saida.setEnEmailFavorecidoCliente(response.getEnEmailFavorecidoCliente());
		saida.setEnEmailComercialFavorecido(response.getEnEmailComercialFavorecido());
		saida.setCdSmsFavorecidoCliente(NumberUtils.formtarFoneSemZero(response.getCdSmsFavorecidoCliente()));
		saida.setEnLogradouroFavorecidoCliente(response.getEnLogradouroFavorecidoCliente());
		saida.setEnNumeroLogradouroFavorecido(response.getEnNumeroLogradouroFavorecido());
		saida.setEnComplementoLogradouroFavorecido(response.getEnComplementoLogradouroFavorecido());
		saida.setEnBairroFavorecidoCliente(response.getEnBairroFavorecidoCliente());
		saida.setDsEstadoFavorecidoCliente(response.getDsEstadoFavorecidoCliente());
		saida.setCdSiglaEstadoFavorecidoCliente(response.getCdSiglaEstadoFavorecidoCliente());
		saida.setCdCepFavorecidoCliente(response.getCdCepFavorecidoCliente());
		saida.setCdComplementoCepFavorecido(response.getCdComplementoCepFavorecido());
		saida.setEnLogradrouroCorrespondenciaFavorecido(response.getEnLogradrouroCorrespondenciaFavorecido());
		saida.setEnNumeroLogradouroCorrespondencia(response.getEnNumeroLogradouroCorrespondencia());
		saida.setEnComplementoLogradouroCorrespondencia(response.getEnComplementoLogradouroCorrespondencia());
		saida.setEnBairroCorrespondenciaFavorecido(response.getEnBairroCorrespondenciaFavorecido());
		saida.setDsEstadoCorrespondenciaFavorecido(response.getDsEstadoCorrespondenciaFavorecido());
		saida.setCdSiglaEstadoCorrespondencia(response.getCdSiglaEstadoCorrespondencia());
		saida.setCdCepCorrespondenciaFavorecido(response.getCdCepCorrespondenciaFavorecido());
		saida.setCdComplementoCepCorrespondencia(response.getCdComplementoCepCorrespondencia());
		saida.setCdNitFavorecidoCliente(response.getCdNitFavorecidoCliente());
		saida.setDsMunicipioFavorecidoCliente(response.getDsMunicipioFavorecidoCliente());
		saida.setCdInscricaoEstadoFavorecido(response.getCdInscricaoEstadoFavorecido());
		saida.setCdSiglaUfInscricaoEstadual(response.getCdSiglaUfInscricaoEstadual());
		saida.setDsMunicipalInscricaoEstado(response.getDsMunicipalInscricaoEstado());
		saida.setCdSituacaoFavorecidoCliente(response.getCdSituacaoFavorecidoCliente());
		saida.setHrBloqueioFavorecidoCliente(DateUtils.formartarDataPDCparaPadraPtBr(response.getHrBloqueioFavorecidoCliente(), DataConstants.DATA_PDC, DataConstants.DATA_PADRA));
		saida.setDsObservacaoControleFavorecido(response.getDsObservacaoControleFavorecido());
		saida.setCdIndicadorRetornoCliente(response.getCdIndicadorRetornoCliente());
		saida.setDsRetornoCliente(response.getDsRetornoCliente());
		saida.setDtUltimaMovimentacaoFavorecido(DateUtils.trocarSeparadorDeData(response.getDtUltimaMovimentacaoFavorecido(), ".", "/"));
		saida.setCdIndicadorFavorecidoInativo(response.getCdIndicadorFavorecidoInativo());
		saida.setCdEstadoCivilFavorecido(response.getCdEstadoCivilFavorecido());
		saida.setCdUfEmpresaFavorecido(response.getCdUfEmpresaFavorecido());
		saida.setDtNascimentoFavorecidoCliente(DateUtils.trocarSeparadorDeData(response.getDtNascimentoFavorecidoCliente(), ".", "/"));
		saida.setDsEmpresaFavorecidoCliente(response.getDsEmpresaFavorecidoCliente());
		saida.setDsMunicipioEmpresaFavorecido(response.getDsMunicipioEmpresaFavorecido());
		saida.setDsConjugeFavorecidoCliente(response.getDsConjugeFavorecidoCliente());
		saida.setDsNacionalidadeFavorecidoCliente(response.getDsNacionalidadeFavorecidoCliente());
		saida.setDsProfissaoFavorecidoCliente(response.getDsProfissaoFavorecidoCliente());
		saida.setDsCargoFavorecidoCliente(response.getDsCargoFavorecidoCliente());
		saida.setDsUfEmpreFavorecido(response.getDsUfEmpreFavorecido());
		saida.setDsMaeFavorecidoCliente(response.getDsMaeFavorecidoCliente());
		saida.setDsPaiFavorecidoCliente(response.getDsPaiFavorecidoCliente());
		saida.setDsMunicipioNascimentoFavorecido(response.getDsMunicipioNascimentoFavorecido());
		saida.setDsUfNascimentoFavorecido(response.getDsUfNascimentoFavorecido());
		saida.setVlRendaMensalFavorecido(response.getVlRendaMensalFavorecido());
		saida.setDtAtualizarEnderecoResidencial(DateUtils.trocarSeparadorDeData(response.getDtAtualizarEnderecoResidencial(),".","/"));
		saida.setDtAtualizarEnderecoCorrespondencia(DateUtils.trocarSeparadorDeData(response.getDtAtualizarEnderecoCorrespondencia(),".","/"));
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterna(response.getCdUsuarioInclusaoExterna());
		saida.setDtInclusao(DateUtils.trocarSeparadorDeData(response.getDtInclusao(), ".", "/"));
		saida.setHrInclusao(response.getHrInclusao());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setDsCanalinclusao(CamposUtils.formatadorCampos(response.getCdTipoCanalInclusao(), response.getDsCanalinclusao()));
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterna(response.getCdUsuarioManutencaoExterna());
		saida.setDtManutencao(DateUtils.trocarSeparadorDeData(response.getDtManutencao(), ".", "/"));
		saida.setHrManutencao(response.getHrManutencao());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setDsCanalManutencao(CamposUtils.formatadorCampos(response.getCdTipoCanalManutencao(), response.getDsCanalManutencao()));
		saida.setCdAcaoManutencao(response.getCdAcaoManutencao());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#consultarDetalheHistoricoContaFavorecido(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheHistoricoContaFavorecidoEntradaDTO)
	 */
	public ConsultarDetalheHistoricoContaFavorecidoSaidaDTO consultarDetalheHistoricoContaFavorecido(ConsultarDetalheHistoricoContaFavorecidoEntradaDTO entrada) {

		ConsultarDetalheHistoricoContaFavorecidoRequest request = new ConsultarDetalheHistoricoContaFavorecidoRequest();

		request.setCdFavorecidoClientePagador(entrada.getCdFavorecidoClientePagador() == null ? 0L : entrada.getCdFavorecidoClientePagador());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0 : entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada.getCdTipoContratoNegocio());
		request.setHrInclusaoRegistro(entrada.getHrInclusaoRegistro() == null ? "" : entrada.getHrInclusaoRegistro());
		request.setNrOcorrenciaContaFavorecido(entrada.getNrOcorrenciaContaFavorecido() == null ? 0 : entrada.getNrOcorrenciaContaFavorecido());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio() == null ? 0L : entrada.getNrSequenciaContratoNegocio());

		ConsultarDetalheHistoricoContaFavorecidoResponse response = getFactoryAdapter().getConsultarDetalheHistoricoContaFavorecidoPDCAdapter().invokeProcess(request);

		ConsultarDetalheHistoricoContaFavorecidoSaidaDTO saida = new ConsultarDetalheHistoricoContaFavorecidoSaidaDTO();
		saida.setDsBloqueioConta(CamposUtils.formatadorCampos(response.getCdBloqueioContaFavorecido(), response.getDsBloqueioConta()));
		saida.setDsObservacaoContaFavorecido(response.getDsObservacaoContaFavorecido());
		saida.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saida.setCdIndicadorRetornoCliente(response.getCdIndicadorRetornoCliente());
		saida.setCdSituacaoContaFavorecido(response.getCdSituacaoContaFavorecido());
		saida.setHrBloqueioContaFavorecido(DateUtils.formartarDataPDCparaPadraPtBr(response.getHrBloqueioContaFavorecido(), DataConstants.DATA_PDC, DataConstants.DATA_PADRA));
		saida.setCdIndicadorTipoManutencao(response.getCdIndicadorTipoManutencao());
		saida.setDsTipoManutencao(response.getDsTipoManutencao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setDtInclusao(DateUtils.trocarSeparadorDeData(response.getDtInclusao(),".","/"));
		saida.setHrInclusao(response.getHrInclusao());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setDsCanalInclusao(CamposUtils.formatadorCampos(response.getCdTipoCanalInclusao(), response.getDsCanalInclusao()));
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterna(response.getCdUsuarioManutencaoExterna());
		saida.setDtManutencao(DateUtils.trocarSeparadorDeData(response.getDtManutencao(),".","/"));
		saida.setHrManutencao(response.getHrManutencao());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setDsCanalManutencao(CamposUtils.formatadorCampos(response.getCdTipoCanalManutencao(), response.getDsCanalManutencao()));
		
		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#consultarDetalheHistoricoFavorecido(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.DetalharHistoricoFavorecidoEntradaDTO)
	 */
	public DetalharHistoricoFavorecidoSaidaDTO consultarDetalheHistoricoFavorecido(DetalharHistoricoFavorecidoEntradaDTO entrada) {

		DetalharHistoricoFavorecidoRequest request = new DetalharHistoricoFavorecidoRequest();

		request.setCdFavorecidoClientePagador(entrada.getCdFavorecidoClientePagador() == null ? 0L : entrada.getCdFavorecidoClientePagador());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0 : entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0 : entrada.getCdTipoContratoNegocio());
		request.setHrInclusaoRegistro(entrada.getHrInclusaoRegistro() == null ? "" : entrada.getHrInclusaoRegistro());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio() == null ? 0L : entrada.getNrSequenciaContratoNegocio());

		DetalharHistoricoFavorecidoResponse response = getFactoryAdapter().getDetalharHistoricoFavorecidoPDCAdapter().invokeProcess(request);

		DetalharHistoricoFavorecidoSaidaDTO saida = new DetalharHistoricoFavorecidoSaidaDTO();

		saida.setCdTipoFavorecido(response.getCdTipoFavorecido());
		saida.setDsCodigoTipoFavorecido(response.getDsCodigoTipoFavorecido());
		saida.setCdTipoInscricaoFavorecido(response.getCdTipoInscricaoFavorecido());
		saida.setDsCodigoTipoInscricao(response.getDsCodigoTipoInscricao());
		saida.setCdMotivoBloqueioFavorecido(response.getCdMotivoBloqueioFavorecido());
		saida.setDsCodigoMotivoBloqueio(response.getDsCodigoMotivoBloqueio());
		saida.setCdInscricaoFavorecidoCliente(response.getCdInscricaoFavorecidoCliente());
		saida.setDsComplementoFavorecidoCliente(response.getDsComplementoFavorecidoCliente());
		saida.setDsAbreviacaoFavorecidoCliente(response.getDsAbreviacaoFavorecidoCliente());
		saida.setDsOrgaoEmissorDocumento(response.getDsOrgaoEmissorDocumento());
		saida.setDtExpedicaoDocumentoFavorecido(DateUtils.trocarSeparadorDeData(response.getDtExpedicaoDocumentoFavorecido(),".","/"));
		saida.setCdSexoFavorecidoCliente(response.getCdSexoFavorecidoCliente());
		saida.setCdAreaFavorecidoCliente(response.getCdAreaFavorecidoCliente());
		saida.setCdFoneFavorecidoCliente(response.getCdFoneFavorecidoCliente());
		saida.setCdAreaFaxFavorecido(response.getCdAreaFaxFavorecido());
		saida.setCdFaxFavorecidoCliente(response.getCdFaxFavorecidoCliente());
		saida.setCdRamalFaxFavorecido(response.getCdRamalFaxFavorecido());
		saida.setCdAreaCelularFavorecido(response.getCdAreaCelularFavorecido());
		saida.setCdCelularFavorecidoCliente(response.getCdCelularFavorecidoCliente());
		saida.setCdAreaFoneComercial(response.getCdAreaFoneComercial());
		saida.setCdFoneComercialFavorecido(response.getCdFoneComercialFavorecido());
		saida.setCdRamalComercialFavorecido(response.getCdRamalComercialFavorecido());
		saida.setEnEmailFavorecidoCliente(response.getEnEmailFavorecidoCliente());
		saida.setEnEmailComercialFavorecido(response.getEnEmailComercialFavorecido());
		saida.setCdSmsFavorecidoCliente(NumberUtils.formatarFone(response.getCdSmsFavorecidoCliente()));
		saida.setEnLogradouroFavorecidoCliente(response.getEnLogradouroFavorecidoCliente());
		saida.setEnNumeroLogradouroFavorecido(response.getEnNumeroLogradouroFavorecido());
		saida.setEnComplementoLogradouroFavorecido(response.getEnComplementoLogradouroFavorecido());
		saida.setEnBairroFavorecidoCliente(response.getEnBairroFavorecidoCliente());
		saida.setDsEstadoFavorecidoCliente(response.getDsEstadoFavorecidoCliente());
		saida.setCdSiglaEstadoFavorecidoCliente(response.getCdSiglaEstadoFavorecidoCliente());
		saida.setCdCepFavorecidoCliente(response.getCdCepFavorecidoCliente());
		saida.setCdComplementoCepFavorecido(response.getCdComplementoCepFavorecido());
		saida.setEnLogradouroCorrespondenciaFavorecido(response.getEnLogradouroCorrespondenciaFavorecido());
		saida.setEnNumeroLogradouroCorrespondencia(response.getEnNumeroLogradouroCorrespondencia());
		saida.setEnComplementoLogradouroCorrespondencia(response.getEnComplementoLogradouroCorrespondencia());
		saida.setEnBairroCorrespondenciaFavorecido(response.getEnBairroCorrespondenciaFavorecido());
		saida.setDsEstadoCorrespondenciaFavorecido(response.getDsEstadoCorrespondenciaFavorecido());
		saida.setCdSiglaEstadoCorrespondencia(response.getCdSiglaEstadoCorrespondencia());
		saida.setCdCepCorrespondenciaFavorecido(response.getCdCepCorrespondenciaFavorecido());
		saida.setCdComplementoCepCorrespondencia(response.getCdComplementoCepCorrespondencia());
		saida.setCdNitFavorecidoCliente(response.getCdNitFavorecidoCliente());
		saida.setDsMunicipioFavorecidoCliente(response.getDsMunicipioFavorecidoCliente());
		saida.setCdInscricaoEstadualFavorecido(response.getCdInscricaoEstadualFavorecido());
		saida.setCdSiglaUfInscricaoEstadual(response.getCdSiglaUfInscricaoEstadual());
		saida.setDsMunicipioInscricaoEstadual(response.getDsMunicipioInscricaoEstadual());
		saida.setCdSituacaoFavorecidoCliente(response.getCdSituacaoFavorecidoCliente());
		saida.setHrBloqueioFavorecidoCliente(DateUtils.formartarDataPDCparaPadraPtBr(response.getHrBloqueioFavorecidoCliente(), DataConstants.DATA_PDC, DataConstants.DATA_PADRA));
		saida.setDsObservacaoControleFavorecido(response.getDsObservacaoControleFavorecido());
		saida.setCdIndicadorRetornoCliente(response.getCdIndicadorRetornoCliente());
		saida.setDsRetornoCliente(response.getDsRetornoCliente());
		saida.setDsUltimaMovimentacaoFavorecido(DateUtils.trocarSeparadorDeData(response.getDsUltimaMovimentacaoFavorecido(),".","/"));
		saida.setCdIndicadorFavorecidoInativo(response.getCdIndicadorFavorecidoInativo());
		saida.setCdEstadoCivilFavorecido(response.getCdEstadoCivilFavorecido());
		saida.setCdUfEmpresaFavorecido(response.getCdUfEmpresaFavorecido());
		saida.setDtNascimentoFavorecidoCliente(DateUtils.trocarSeparadorDeData(response.getDtNascimentoFavorecidoCliente(), ".", "/"));
		saida.setDsEmpresaFavorecidoCliente(response.getDsEmpresaFavorecidoCliente());
		saida.setDsMunicipioEmpresaFavorecido(response.getDsMunicipioEmpresaFavorecido());
		saida.setDsConjugeFavorecidoCliente(response.getDsConjugeFavorecidoCliente());
		saida.setDsNacionalidadeFavorecidoCliente(response.getDsNacionalidadeFavorecidoCliente());
		saida.setDsProfissaoFavorecidoCliente(response.getDsProfissaoFavorecidoCliente());
		saida.setDsCargoFavorecidoCliente(response.getDsCargoFavorecidoCliente());
		saida.setDsUfEmpresaFavorecido(response.getDsUfEmpresaFavorecido());
		saida.setDsMaeFavorecidoCliente(response.getDsMaeFavorecidoCliente());
		saida.setDsPaiFavorecidoCliente(response.getDsPaiFavorecidoCliente());
		saida.setDsMunicipalNascimentoFavorecido(response.getDsMunicipalNascimentoFavorecido());
		saida.setDsUfNacionalidadeFavorecido(response.getDsUfNacionalidadeFavorecido());
		saida.setCdUfNascimentoFavorecido(response.getCdUfNascimentoFavorecido());
		saida.setVlRendaMensalFavorecido(response.getVlRendaMensalFavorecido());
		saida.setDtAtualizacaoEnderecoResidencial(DateUtils.trocarSeparadorDeData(response.getDtAtualizacaoEnderecoResidencial(), ".", "/"));
		saida.setDtAtualizacaoEnderecoCorrespondencia(DateUtils.trocarSeparadorDeData(response.getDtAtualizacaoEnderecoCorrespondencia(), ".", "/"));
		saida.setCdIndicadorTipoManutencao(response.getCdIndicadorTipoManutencao());
		saida.setDsTipoManutencao(response.getDsTipoManutencao());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setDtInclusao(response.getDtInclusao());
		saida.setHrInclusao(response.getHrInclusao());
		saida.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		saida.setDsCanalInclusao(CamposUtils.formatadorCampos(response.getCdTipoCanalInclusao(), response.getDsCanalInclusao()));
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoEsteno(response.getCdUsuarioManutencaoEsteno());
		saida.setDtManutencao(response.getDtManutencao());
		saida.setHrManutencao(response.getHrManutencao());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setDsCanalManutencao(CamposUtils.formatadorCampos(response.getCdTipoCanalManutencao(), response.getDsCanalManutencao()));
		saida.setCdAcaoManutencao(response.getCdAcaoManutencao());

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#consultarHistoricosContaFavorecido(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoContaFavorecidoEntradaDTO)
	 */
	public List<ConsultarListaHistoricoContaFavorecidoSaidaDTO> consultarHistoricosContaFavorecido(ConsultarListaHistoricoContaFavorecidoEntradaDTO entrada) {
		
		ConsultarListaHistoricoContaFavorecidoRequest request = new ConsultarListaHistoricoContaFavorecidoRequest();
		
		request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entrada.getCdFavorecidoClientePagador()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrOcorrenciaContaFavorecido(PgitUtil.verificaIntegerNulo(entrada.getNrOcorrenciaContaFavorecido()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setNumeroOcorrencias(50);
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
		request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdDigitoAgencia()));
		request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
		request.setCdDigitoConta(PgitUtil.verificaStringNula(entrada.getCdDigitoConta()));
		request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entrada.getCdTipoConta()));
		request.setDataInicio(PgitUtil.verificaStringNula(entrada.getDataInicio()));
		request.setDataFim(PgitUtil.verificaStringNula(entrada.getDataFim()));
		request.setCdPesquisaLista(PgitUtil.verificaIntegerNulo(entrada.getCdPesquisaLista()));		
		
		ConsultarListaHistoricoContaFavorecidoResponse response = getFactoryAdapter().getConsultarListaHistoricoContaFavorecidoPDCAdapter().invokeProcess(request);
		
		List<ConsultarListaHistoricoContaFavorecidoSaidaDTO> list = new ArrayList<ConsultarListaHistoricoContaFavorecidoSaidaDTO>();
		
		for (br.com.bradesco.web.pgit.service.data.pdc.consultarlistahistoricocontafavorecido.response.Ocorrencias o : response.getOcorrencias()) {
			ConsultarListaHistoricoContaFavorecidoSaidaDTO saida = new ConsultarListaHistoricoContaFavorecidoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroConsultas(response.getNumeroConsultas());
			saida.setNrOcorrenciaConta(o.getNrOcorrenciaConta());
			saida.setHrInclusaoRegistro(o.getHrInclusaoRegistro());
			saida.setCdBanco(o.getCdBanco());
			saida.setDsBanco(o.getDsBanco());
			saida.setCdAgencia(o.getCdAgencia());
			saida.setDsAgencia(o.getDsAgencia());
			saida.setCdDigitoAgencia(o.getCdDigitoAgencia());
			saida.setCdConta(o.getCdConta());
			saida.setCdDigitoConta(o.getCdDigitoConta());
			saida.setCdTipoConta(o.getCdTipoConta());
			saida.setCdSituacaoConta(o.getCdSituacaoConta());
			saida.setDtManutencao(o.getDtManutencao());
			saida.setHrManutencao(o.getHrManutencao());
			saida.setCdUsuarioManutencao(o.getCdUsuarioManutencao());
			saida.setBancoFormatado(PgitUtil.formatBanco(o.getCdBanco(), o.getDsBanco(), false));
			saida.setAgenciaFormatada(PgitUtil.formatAgencia(o.getCdAgencia(), o.getCdDigitoAgencia(), o.getDsAgencia(), false));
			saida.setContaFormatada(PgitUtil.formatConta(o.getCdConta(), o.getCdDigitoConta(), false));
		    	saida.setTpManutencao(o.getCdTipoManutencao());
			list.add(saida);
		}
		
		return list;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService#consultarHistoricosFavorecidos(br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoFavorecidoEntradaDTO)
	 */
	public List<ConsultarListaHistoricoFavorecidoSaidaDTO> consultarHistoricosFavorecidos(ConsultarListaHistoricoFavorecidoEntradaDTO entrada) {
		
		ConsultarListaHistoricoFavorecidoRequest request = new ConsultarListaHistoricoFavorecidoRequest();
		
		request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo( entrada.getCdFavorecidoClientePagador()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo( entrada.getNrSequenciaContratoNegocio()));
		request.setCdTipoIsncricaoFavorecido(PgitUtil.verificaIntegerNulo(entrada.getCdTipoIsncricaoFavorecido()));
		request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entrada.getCdInscricaoFavorecido()));
		request.setDataInicio(PgitUtil.verificaStringNula(entrada.getDataInicio()));
		request.setDataFim(PgitUtil.verificaStringNula(entrada.getDataFim()));
		request.setCdPesquisaLista(PgitUtil.verificaIntegerNulo(entrada.getCdPesquisaLista()));
		request.setNumeroOcorrencias(50);
		
		ConsultarListaHistoricoFavorecidoResponse response = getFactoryAdapter().getConsultarListaHistoricoFavorecidoPDCAdapter().invokeProcess(request);
		
		List<ConsultarListaHistoricoFavorecidoSaidaDTO> list = new ArrayList<ConsultarListaHistoricoFavorecidoSaidaDTO>();
		ConsultarListaHistoricoFavorecidoSaidaDTO saidaDTO;
		for (br.com.bradesco.web.pgit.service.data.pdc.consultarlistahistoricofavorecido.response.Ocorrencias saida : response.getOcorrencias()) {
			saidaDTO = new ConsultarListaHistoricoFavorecidoSaidaDTO();
			saidaDTO.setCdFavorecidoClientePagador(saida.getCdFavorecidoClientePagador());
			saidaDTO.setCdHora(saida.getCdHora());
			saidaDTO.setCdInscricaoFavorecido(saida.getCdInscricaoFavorecido());
			saidaDTO.setCdSituacaoFavorecido(saida.getCdSituacaoFavorecido());
			saidaDTO.setCdTipoIsncricaoFavorecido(saida.getCdTipoIsncricaoFavorecido());
			saidaDTO.setCdUsuario(saida.getCdUsuario());
			saidaDTO.setDsComplementoFavorecido(saida.getDsComplementoFavorecido());
			saidaDTO.setDsTipoFavorecido(saida.getDsTipoFavorecido());
			saidaDTO.setDtSistema(saida.getDtSistema());
			saidaDTO.setHrInclusaoRegistro(saida.getHrInclusaoRegistro());
			saidaDTO.setFavorecidoFormatado(PgitUtil.concatenarCampos(saida.getCdFavorecidoClientePagador(), saida.getDsComplementoFavorecido()));
			saidaDTO.setCpfCnpjFavorecidoFormatado(CpfCnpjUtils.formatCpfCnpj(saida.getCdInscricaoFavorecido(), saida.getCdTipoIsncricaoFavorecido()));
			saidaDTO.setTpManutencao(saida.getTpManutencao());
			list.add(saidaDTO);
		}
		
		return list;
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

}
