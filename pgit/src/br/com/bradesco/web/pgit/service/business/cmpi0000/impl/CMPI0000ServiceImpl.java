/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.cmpi0000.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.cmpi0000.ICMPI0000Service;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.IListarClientesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request.ListarClientesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.response.ListarClientesResponse;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.utils.exception.PgitFormatException;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: CMPI0000
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class CMPI0000ServiceImpl implements ICMPI0000Service {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Construtor.
	 */
	public CMPI0000ServiceImpl() {
		// TODO: Implementa��o
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * M�todo de exemplo.
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.cmpi0000.ICMPI0000#sampleCMPI0000()
	 */
	public void sampleCMPI0000() {
		// TODO: Implementa�ao
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cmpi0000.ICMPI0000Service#listaGridClientes(br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO)
	 */
	public List<ListarClientesDTO> listaGridClientes(ListarClientesDTO pListarClientesDTO) {
		List<ListarClientesDTO> lListarClientes = new ArrayList<ListarClientesDTO>();
		ListarClientesRequest listarClientesRequest = new ListarClientesRequest();
		ListarClientesResponse listarClientesResponse = new ListarClientesResponse();

		// CPF/CNPJ
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO
				.getCodigoCpfCnpjCliente()))) {
			listarClientesRequest.setCpfCnpj(new Long(pListarClientesDTO
					.getCodigoCpfCnpjCliente()));
		} else {
			listarClientesRequest.setCpfCnpj(0);
		}
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getCodigoFilialCnpj()))) {
			listarClientesRequest.setFilial(Integer.parseInt(pListarClientesDTO
					.getCodigoFilialCnpj()));
		} else {
			listarClientesRequest.setFilial(0);
		}
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getControleCpfCnpj()))) {
			listarClientesRequest.setControle(Integer
					.parseInt(pListarClientesDTO.getControleCpfCnpj()));
		} else {
			listarClientesRequest.setControle(0);
		}
		// Nome Raz�o
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getNomeRazao()))) {
			listarClientesRequest.setNomeRazao(pListarClientesDTO
					.getNomeRazao());
		} else {
			listarClientesRequest.setNomeRazao("");
		}
		// DataNascimentoFundacao
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO
				.getDataNascimentoFundacao()))) {
			try {
				listarClientesRequest.setDataNascimentoFundacao(SiteUtil
						.changeStringDateFormat(pListarClientesDTO
								.getDataNascimentoFundacao(), "dd/MM/yyyy",
								"dd.MM.yyyy"));
			} catch (ParseException e) {
				throw new PgitFormatException(e.getMessage(), e);
			}
		} else {
			listarClientesRequest.setDataNascimentoFundacao("");
		}

		// Codigo Banco
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getCodigoBancoDebito()))) {
			listarClientesRequest.setBanco(Integer.parseInt(pListarClientesDTO
					.getCodigoBancoDebito()));
		} else {
			listarClientesRequest.setBanco(0);
		}
		// Agencia
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getAgenciaBancaria()))) {
			listarClientesRequest.setAgencia(Integer
					.parseInt(pListarClientesDTO.getCodigoBancoDebito()));
		} else {
			listarClientesRequest.setAgencia(0);
		}
		// Conta
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getContaBancaria()))) {
			listarClientesRequest.setConta(Integer.parseInt(pListarClientesDTO
					.getContaBancaria()));
		} else {
			listarClientesRequest.setConta(0);
		}
		// Fazemos a chamada do processo.
		IListarClientesPDCAdapter listarClientesPDCAdapter = factoryAdapter
				.getListarClientesPDCAdapter();

		listarClientesResponse = listarClientesPDCAdapter
				.invokeProcess(listarClientesRequest);

		StringBuffer bcoAgeConta = null;
		// Para cada item retornado do processo, adicionamos a lista de
		// retorno.

		String cpf = "";
		String cpfCliente = "";
		String cpfFilial = "";
		String cpfControle = "";

		for (int i = 0; i < listarClientesResponse.getNumeroLinhas(); i++) {
			ListarClientesDTO listarClientesDTO = new ListarClientesDTO();

			String campoFormatado = "";

			if (listarClientesResponse.getOcorrencias(i).getFilial() == 0) {

				cpfCliente = String.valueOf(listarClientesResponse
						.getOcorrencias(i).getCpfCnpj());

				while (cpfCliente.length() < 9) {
					cpfCliente = "@" + cpfCliente;
				}

				campoFormatado = cpfCliente.substring(0, 3) + "."
						+ cpfCliente.substring(3, 6) + "."
						+ cpfCliente.substring(6, 9);

				cpfControle = String.valueOf(listarClientesResponse
						.getOcorrencias(i).getControle());

				while (cpfControle.length() < 2) {
					cpfControle = "0" + cpfControle;
				}

				cpf = campoFormatado + "-" + cpfControle;

			} else {

				cpfCliente = String.valueOf(listarClientesResponse
						.getOcorrencias(i).getCpfCnpj());

				while (cpfCliente.length() < 9) {
					cpfCliente = "@" + cpfCliente;
				}

				campoFormatado = cpfCliente.substring(0, 3) + "."
						+ cpfCliente.substring(3, 6) + "."
						+ cpfCliente.substring(6, 9);

				cpfFilial = String.valueOf(listarClientesResponse
						.getOcorrencias(i).getFilial());

				while (cpfFilial.length() < 4) {
					cpfFilial = "0" + cpfFilial;
				}

				cpfControle = String.valueOf(listarClientesResponse
						.getOcorrencias(i).getControle());

				while (cpfControle.length() < 2) {
					cpfControle = "0" + cpfControle;
				}

				cpf = campoFormatado + "/" + cpfFilial + "-" + cpfControle;
			}

			listarClientesDTO.setCpf(cpf.replace("@", "").trim());
			listarClientesDTO.setCpfCnpjCliente(cpfCliente.replace(".", "")
					.replace("@", "").trim());
			listarClientesDTO.setFilialCnpj(cpfFilial);
			listarClientesDTO.setControleCpfCnpj(cpfControle);
			listarClientesDTO.setNomeRazao(listarClientesResponse
					.getOcorrencias(i).getNomeRazao());
			try {
				listarClientesDTO.setDataNascimentoFundacao(SiteUtil
						.changeStringDateFormat(listarClientesResponse
								.getOcorrencias(i).getDataNacscimentoFundacao(),
								"yyyy-MM-dd-hh.mm.ss", "dd/MM/yyyy hh:mm:ss"));
			} catch (ParseException e) {
				throw new PgitFormatException(e.getMessage(), e);
			}
			bcoAgeConta = new StringBuffer();
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse
					.getOcorrencias(i).getBanco());
			bcoAgeConta = bcoAgeConta.append("/");
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse
					.getOcorrencias(i).getAgencia());
			bcoAgeConta = bcoAgeConta.append("/");
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse
					.getOcorrencias(i).getConta());
			listarClientesDTO.setBcoAgeConta(bcoAgeConta.toString());
			listarClientesDTO.setTotalRegistros(i);
			lListarClientes.add(listarClientesDTO);
		}

		return lListarClientes;
	}
	
	/**
	 * Formatar.
	 *
	 * @param valor the valor
	 * @param mascara the mascara
	 * @return the string
	 */
	public static String formatar(String valor, String mascara) {
		String dado = "";
		// remove caracteres nao numericos
		for (int i = 0; i < valor.length(); i++) {
			char c = valor.charAt(i);
			if (Character.isDigit(c)) {
				dado += c;
			}
		}
		int indMascara = mascara.length();
		int indCampo = dado.length();
		for (; indCampo > 0 && indMascara > 0;) {
			if (mascara.charAt(--indMascara) == '#') {
				indCampo--;
			}
		}
		String saida = "";
		for (; indMascara < mascara.length(); indMascara++) {
			saida += ((mascara.charAt(indMascara) == '#') ? dado.charAt(indCampo++) : mascara.charAt(indMascara));
		}

		return saida;
	}

	/**
	 * Formatar cpf.
	 *
	 * @param cpf the cpf
	 * @return the string
	 */
	public static String formatarCpf(String cpf) {
		return formatar(cpf, "###.###.###-##");
	}

	/**
	 * Formatar cnpj.
	 *
	 * @param cnpj the cnpj
	 * @return the string
	 */
	public static String formatarCnpj(String cnpj) {
		return formatar(cnpj, "###.###.###/#####-##");
	}
}