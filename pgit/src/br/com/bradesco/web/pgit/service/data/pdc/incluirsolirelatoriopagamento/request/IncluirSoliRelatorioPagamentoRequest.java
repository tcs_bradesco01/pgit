/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirSoliRelatorioPagamentoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirSoliRelatorioPagamentoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoRelatorio
     */
    private int _cdTipoRelatorio = 0;

    /**
     * keeps track of state for field: _cdTipoRelatorio
     */
    private boolean _has_cdTipoRelatorio;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdPessoaParticipante
     */
    private long _cdPessoaParticipante = 0;

    /**
     * keeps track of state for field: _cdPessoaParticipante
     */
    private boolean _has_cdPessoaParticipante;

    /**
     * Field _cdCpfCnpjParticipante
     */
    private long _cdCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjParticipante
     */
    private boolean _has_cdCpfCnpjParticipante;

    /**
     * Field _cdFilialCnpjParticipante
     */
    private int _cdFilialCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjParticipante
     */
    private boolean _has_cdFilialCnpjParticipante;

    /**
     * Field _cdControleCpfParticipante
     */
    private int _cdControleCpfParticipante = 0;

    /**
     * keeps track of state for field: _cdControleCpfParticipante
     */
    private boolean _has_cdControleCpfParticipante;

    /**
     * Field _cdPessoaJuridicaDebito
     */
    private long _cdPessoaJuridicaDebito = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaDebito
     */
    private boolean _has_cdPessoaJuridicaDebito;

    /**
     * Field _cdTipoContratoDebito
     */
    private int _cdTipoContratoDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContratoDebito
     */
    private boolean _has_cdTipoContratoDebito;

    /**
     * Field _nrSequenciaContratoDebito
     */
    private long _nrSequenciaContratoDebito = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoDebito
     */
    private boolean _has_nrSequenciaContratoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _dtIniPagamento
     */
    private java.lang.String _dtIniPagamento;

    /**
     * Field _dtFimPagamento
     */
    private java.lang.String _dtFimPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirSoliRelatorioPagamentoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.request.IncluirSoliRelatorioPagamentoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdControleCpfParticipante
     * 
     */
    public void deleteCdControleCpfParticipante()
    {
        this._has_cdControleCpfParticipante= false;
    } //-- void deleteCdControleCpfParticipante() 

    /**
     * Method deleteCdCpfCnpjParticipante
     * 
     */
    public void deleteCdCpfCnpjParticipante()
    {
        this._has_cdCpfCnpjParticipante= false;
    } //-- void deleteCdCpfCnpjParticipante() 

    /**
     * Method deleteCdFilialCnpjParticipante
     * 
     */
    public void deleteCdFilialCnpjParticipante()
    {
        this._has_cdFilialCnpjParticipante= false;
    } //-- void deleteCdFilialCnpjParticipante() 

    /**
     * Method deleteCdPessoaJuridicaDebito
     * 
     */
    public void deleteCdPessoaJuridicaDebito()
    {
        this._has_cdPessoaJuridicaDebito= false;
    } //-- void deleteCdPessoaJuridicaDebito() 

    /**
     * Method deleteCdPessoaParticipante
     * 
     */
    public void deleteCdPessoaParticipante()
    {
        this._has_cdPessoaParticipante= false;
    } //-- void deleteCdPessoaParticipante() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdTipoContratoDebito
     * 
     */
    public void deleteCdTipoContratoDebito()
    {
        this._has_cdTipoContratoDebito= false;
    } //-- void deleteCdTipoContratoDebito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoRelatorio
     * 
     */
    public void deleteCdTipoRelatorio()
    {
        this._has_cdTipoRelatorio= false;
    } //-- void deleteCdTipoRelatorio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrSequenciaContratoDebito
     * 
     */
    public void deleteNrSequenciaContratoDebito()
    {
        this._has_nrSequenciaContratoDebito= false;
    } //-- void deleteNrSequenciaContratoDebito() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdControleCpfParticipante'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfParticipante'.
     */
    public int getCdControleCpfParticipante()
    {
        return this._cdControleCpfParticipante;
    } //-- int getCdControleCpfParticipante() 

    /**
     * Returns the value of field 'cdCpfCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjParticipante'.
     */
    public long getCdCpfCnpjParticipante()
    {
        return this._cdCpfCnpjParticipante;
    } //-- long getCdCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdFilialCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjParticipante'.
     */
    public int getCdFilialCnpjParticipante()
    {
        return this._cdFilialCnpjParticipante;
    } //-- int getCdFilialCnpjParticipante() 

    /**
     * Returns the value of field 'cdPessoaJuridicaDebito'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaDebito'.
     */
    public long getCdPessoaJuridicaDebito()
    {
        return this._cdPessoaJuridicaDebito;
    } //-- long getCdPessoaJuridicaDebito() 

    /**
     * Returns the value of field 'cdPessoaParticipante'.
     * 
     * @return long
     * @return the value of field 'cdPessoaParticipante'.
     */
    public long getCdPessoaParticipante()
    {
        return this._cdPessoaParticipante;
    } //-- long getCdPessoaParticipante() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdTipoContratoDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoDebito'.
     */
    public int getCdTipoContratoDebito()
    {
        return this._cdTipoContratoDebito;
    } //-- int getCdTipoContratoDebito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoRelatorio'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelatorio'.
     */
    public int getCdTipoRelatorio()
    {
        return this._cdTipoRelatorio;
    } //-- int getCdTipoRelatorio() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dtFimPagamento'.
     * 
     * @return String
     * @return the value of field 'dtFimPagamento'.
     */
    public java.lang.String getDtFimPagamento()
    {
        return this._dtFimPagamento;
    } //-- java.lang.String getDtFimPagamento() 

    /**
     * Returns the value of field 'dtIniPagamento'.
     * 
     * @return String
     * @return the value of field 'dtIniPagamento'.
     */
    public java.lang.String getDtIniPagamento()
    {
        return this._dtIniPagamento;
    } //-- java.lang.String getDtIniPagamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoDebito'.
     */
    public long getNrSequenciaContratoDebito()
    {
        return this._nrSequenciaContratoDebito;
    } //-- long getNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdControleCpfParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfParticipante()
    {
        return this._has_cdControleCpfParticipante;
    } //-- boolean hasCdControleCpfParticipante() 

    /**
     * Method hasCdCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjParticipante()
    {
        return this._has_cdCpfCnpjParticipante;
    } //-- boolean hasCdCpfCnpjParticipante() 

    /**
     * Method hasCdFilialCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjParticipante()
    {
        return this._has_cdFilialCnpjParticipante;
    } //-- boolean hasCdFilialCnpjParticipante() 

    /**
     * Method hasCdPessoaJuridicaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaDebito()
    {
        return this._has_cdPessoaJuridicaDebito;
    } //-- boolean hasCdPessoaJuridicaDebito() 

    /**
     * Method hasCdPessoaParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaParticipante()
    {
        return this._has_cdPessoaParticipante;
    } //-- boolean hasCdPessoaParticipante() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdTipoContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoDebito()
    {
        return this._has_cdTipoContratoDebito;
    } //-- boolean hasCdTipoContratoDebito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoRelatorio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelatorio()
    {
        return this._has_cdTipoRelatorio;
    } //-- boolean hasCdTipoRelatorio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrSequenciaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoDebito()
    {
        return this._has_nrSequenciaContratoDebito;
    } //-- boolean hasNrSequenciaContratoDebito() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdControleCpfParticipante'.
     * 
     * @param cdControleCpfParticipante the value of field
     * 'cdControleCpfParticipante'.
     */
    public void setCdControleCpfParticipante(int cdControleCpfParticipante)
    {
        this._cdControleCpfParticipante = cdControleCpfParticipante;
        this._has_cdControleCpfParticipante = true;
    } //-- void setCdControleCpfParticipante(int) 

    /**
     * Sets the value of field 'cdCpfCnpjParticipante'.
     * 
     * @param cdCpfCnpjParticipante the value of field
     * 'cdCpfCnpjParticipante'.
     */
    public void setCdCpfCnpjParticipante(long cdCpfCnpjParticipante)
    {
        this._cdCpfCnpjParticipante = cdCpfCnpjParticipante;
        this._has_cdCpfCnpjParticipante = true;
    } //-- void setCdCpfCnpjParticipante(long) 

    /**
     * Sets the value of field 'cdFilialCnpjParticipante'.
     * 
     * @param cdFilialCnpjParticipante the value of field
     * 'cdFilialCnpjParticipante'.
     */
    public void setCdFilialCnpjParticipante(int cdFilialCnpjParticipante)
    {
        this._cdFilialCnpjParticipante = cdFilialCnpjParticipante;
        this._has_cdFilialCnpjParticipante = true;
    } //-- void setCdFilialCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaDebito'.
     * 
     * @param cdPessoaJuridicaDebito the value of field
     * 'cdPessoaJuridicaDebito'.
     */
    public void setCdPessoaJuridicaDebito(long cdPessoaJuridicaDebito)
    {
        this._cdPessoaJuridicaDebito = cdPessoaJuridicaDebito;
        this._has_cdPessoaJuridicaDebito = true;
    } //-- void setCdPessoaJuridicaDebito(long) 

    /**
     * Sets the value of field 'cdPessoaParticipante'.
     * 
     * @param cdPessoaParticipante the value of field
     * 'cdPessoaParticipante'.
     */
    public void setCdPessoaParticipante(long cdPessoaParticipante)
    {
        this._cdPessoaParticipante = cdPessoaParticipante;
        this._has_cdPessoaParticipante = true;
    } //-- void setCdPessoaParticipante(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdTipoContratoDebito'.
     * 
     * @param cdTipoContratoDebito the value of field
     * 'cdTipoContratoDebito'.
     */
    public void setCdTipoContratoDebito(int cdTipoContratoDebito)
    {
        this._cdTipoContratoDebito = cdTipoContratoDebito;
        this._has_cdTipoContratoDebito = true;
    } //-- void setCdTipoContratoDebito(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoRelatorio'.
     * 
     * @param cdTipoRelatorio the value of field 'cdTipoRelatorio'.
     */
    public void setCdTipoRelatorio(int cdTipoRelatorio)
    {
        this._cdTipoRelatorio = cdTipoRelatorio;
        this._has_cdTipoRelatorio = true;
    } //-- void setCdTipoRelatorio(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dtFimPagamento'.
     * 
     * @param dtFimPagamento the value of field 'dtFimPagamento'.
     */
    public void setDtFimPagamento(java.lang.String dtFimPagamento)
    {
        this._dtFimPagamento = dtFimPagamento;
    } //-- void setDtFimPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtIniPagamento'.
     * 
     * @param dtIniPagamento the value of field 'dtIniPagamento'.
     */
    public void setDtIniPagamento(java.lang.String dtIniPagamento)
    {
        this._dtIniPagamento = dtIniPagamento;
    } //-- void setDtIniPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoDebito'.
     * 
     * @param nrSequenciaContratoDebito the value of field
     * 'nrSequenciaContratoDebito'.
     */
    public void setNrSequenciaContratoDebito(long nrSequenciaContratoDebito)
    {
        this._nrSequenciaContratoDebito = nrSequenciaContratoDebito;
        this._has_nrSequenciaContratoDebito = true;
    } //-- void setNrSequenciaContratoDebito(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirSoliRelatorioPagamentoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.request.IncluirSoliRelatorioPagamentoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.request.IncluirSoliRelatorioPagamentoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.request.IncluirSoliRelatorioPagamentoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.request.IncluirSoliRelatorioPagamentoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
