/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

/**
 * Nome: Conta
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class Conta {

    /** Atributo codigo. */
    private String codigo = null;

    /** Atributo agencia. */
    private String agencia = null;

    /** Atributo contaDebito. */
    private String contaDebito = null;

    /** Atributo digito. */
    private String digito = null;

    /** Atributo razaoSocialNome. */
    private String razaoSocialNome = null;
    
    /** Atributo cnpjCpf. */
    private String cnpjCpf = null;
    
    /**
     * Instancia um novo Conta
     *
     * @see
     */
    public Conta() {
        super();
    }

    /**
     * Nome: getCodigo
     *
     * @return codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Nome: setCodigo
     *
     * @param codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Nome: getAgencia
     *
     * @return agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Nome: setAgencia
     *
     * @param agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Nome: getContaDebito
     *
     * @return contaDebito
     */
    public String getContaDebito() {
        return contaDebito;
    }

    /**
     * Nome: setContaDebito
     *
     * @param contaDebito
     */
    public void setContaDebito(String contaDebito) {
        this.contaDebito = contaDebito;
    }

    /**
     * Nome: getDigito
     *
     * @return digito
     */
    public String getDigito() {
        return digito;
    }

    /**
     * Nome: setDigito
     *
     * @param digito
     */
    public void setDigito(String digito) {
        this.digito = digito;
    }

    /**
     * Nome: getRazaoSocialNome
     *
     * @return razaoSocialNome
     */
    public String getRazaoSocialNome() {
        return razaoSocialNome;
    }

    /**
     * Nome: setRazaoSocialNome
     *
     * @param razaoSocialNome
     */
    public void setRazaoSocialNome(String razaoSocialNome) {
        this.razaoSocialNome = razaoSocialNome;
    }

    /**
     * Nome: getCnpjCpf
     *
     * @return cnpjCpf
     */
    public String getCnpjCpf() {
        return cnpjCpf;
    }

    /**
     * Nome: setCnpjCpf
     *
     * @param cnpjCpf
     */
    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

}
