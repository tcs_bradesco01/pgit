/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.lanctopersonalizado.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonServiceConstants;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.AlterarLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.AlterarLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharHistoricoLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharHistoricoLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ExcluirLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ExcluirLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.HistoricoLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.HistoricoLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.IncluirLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.IncluirLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.request.AlterarMsgLancamentoPersonalizadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.response.AlterarMsgLancamentoPersonalizadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.request.ConsultarHistManutMsgLancPersonalizadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.response.ConsultarHistManutMsgLancPersonalizadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.request.ConsultarMsgLancamentoPersonalizadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.response.ConsultarMsgLancamentoPersonalizadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistmanutmsglancpersonalizado.request.DetalharHistManutMsgLancPersonalizadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistmanutmsglancpersonalizado.response.DetalharHistManutMsgLancPersonalizadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsglancamentopersonalizado.request.DetalharMsgLancamentoPersonalizadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsglancamentopersonalizado.response.DetalharMsgLancamentoPersonalizadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsglancamentopersonalizado.request.ExluirMsgLancamentoPersonalizadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsglancamentopersonalizado.response.ExluirMsgLancamentoPersonalizadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsglancamentopersonalizado.request.IncluirMsgLancamentoPersonalizadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsglancamentopersonalizado.response.IncluirMsgLancamentoPersonalizadoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterLancamentoPersonalizado
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterLancamentoPersonalizadoServiceImpl implements ILanctoPersonService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter; 
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public ManterLancamentoPersonalizadoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.lanctopersonalizado.IManterLancamentoPersonalizado#sampleManterLancamentoPersonalizado()
     */
    public void sampleManterLancamentoPersonalizado() {
        // TODO: Implementa�ao
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService#listarLanctoPersonalizado(br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoEntradaDTO)
     */
    public List<ListarLanctoPersonalizadoSaidaDTO> listarLanctoPersonalizado(ListarLanctoPersonalizadoEntradaDTO listarLanctoPersonalizadoEntradaDTO) {
		
		List<ListarLanctoPersonalizadoSaidaDTO> listaRetorno = new ArrayList<ListarLanctoPersonalizadoSaidaDTO>();		
		ConsultarMsgLancamentoPersonalizadoRequest request = new ConsultarMsgLancamentoPersonalizadoRequest();
		ConsultarMsgLancamentoPersonalizadoResponse response = new ConsultarMsgLancamentoPersonalizadoResponse();
		
		request.setCdIdentificadorLancamentoCredito(0);
		request.setCdIdentificadorLancamentoDebito(0);
		request.setCdIndicadorRestricaoContrato(0);
		request.setCdMensagemLinhasExtrato(listarLanctoPersonalizadoEntradaDTO.getCodLancamento());
		request.setCdProdutoServicoOperacao(listarLanctoPersonalizadoEntradaDTO.getTipoServico());
		request.setCdProdutoOperacaoRelacionado(listarLanctoPersonalizadoEntradaDTO.getModalidadeServico());
		request.setCdRelacionadoProduto(listarLanctoPersonalizadoEntradaDTO.getTipoRelacionamento());
		request.setQtOcorrencias(ILanctoPersonServiceConstants.QTDE_CONSULTAS_LISTAR);
	
		response = getFactoryAdapter().getConsultarMsgLancamentoPersonalizadoPDCAdapter().invokeProcess(request);

		ListarLanctoPersonalizadoSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarLanctoPersonalizadoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdLancCredito(response.getOcorrencias(i).getCdIdentificadorLancamentoCredito());
			saidaDTO.setCdLancDebito(response.getOcorrencias(i).getCdIdentificadorLancamentoDebito());
			saidaDTO.setCdTipoModalidade(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			saidaDTO.setCdTipoRelacionamento(response.getOcorrencias(i).getCdRelacionadoProduto());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServidoOperacao());
			saidaDTO.setCodLancamento(response.getOcorrencias(i).getCdMensagemLinhasExtrato());
			saidaDTO.setDsLancCredito(response.getOcorrencias(i).getCdIdentificadorLancamentoCredito() != 0 ? response.getOcorrencias(i).getCdIdentificadorLancamentoCredito() + " - " + response.getOcorrencias(i).getDsIdentificadorLancamentoCredito(): "");
			saidaDTO.setDsLancDebito(response.getOcorrencias(i).getCdIdentificadorLancamentoDebito() != 0 ?response.getOcorrencias(i).getCdIdentificadorLancamentoDebito() + " - " + response.getOcorrencias(i).getDsIdentificadorLancamentoDebito():"");
			saidaDTO.setDsTipoModalidade(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServidoOperacao());
			saidaDTO.setRestricao(response.getOcorrencias(i).getCdIndicadorRestricaoContrato());
			saidaDTO.setTipoLancamento(response.getOcorrencias(i).getCdTipoMensagemExtrato());
			
			listaRetorno.add(saidaDTO);
			
		}
		
		return listaRetorno;
	}
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService#incluirLanctoPersonalizado(br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.IncluirLanctoPersonalizadoEntradaDTO)
	 */
	public IncluirLanctoPersonalizadoSaidaDTO incluirLanctoPersonalizado(IncluirLanctoPersonalizadoEntradaDTO incluirLanctoPersonalizadoEntradaDTO) {
		
		IncluirLanctoPersonalizadoSaidaDTO incluirLanctoPersonalizadoSaidaDTO = new IncluirLanctoPersonalizadoSaidaDTO();
		IncluirMsgLancamentoPersonalizadoRequest request = new IncluirMsgLancamentoPersonalizadoRequest();
		IncluirMsgLancamentoPersonalizadoResponse response = new IncluirMsgLancamentoPersonalizadoResponse();
		
		request.setCdIdentificadorLancamentoCredito(incluirLanctoPersonalizadoEntradaDTO.getCodLancamentoCredito());
		request.setCdIdentificadorLancamentoDebito(incluirLanctoPersonalizadoEntradaDTO.getCodLancamentoDebito());
		request.setCdIndicadorRestricaoContrato(incluirLanctoPersonalizadoEntradaDTO.getRestricao());
		request.setCdMensagemLinhasExtrato(incluirLanctoPersonalizadoEntradaDTO.getCodLancamento());
		request.setCdProdutoOperacaoRelacionado(incluirLanctoPersonalizadoEntradaDTO.getModalidadeServico());
		request.setCdProdutoServicoOperacao(incluirLanctoPersonalizadoEntradaDTO.getTipoServico());
		request.setCdRelacionadoProduto(incluirLanctoPersonalizadoEntradaDTO.getTipoRelacionamento());
		request.setCdTipoMensagemExtrato(incluirLanctoPersonalizadoEntradaDTO.getTipoLancamento());
		
		response = getFactoryAdapter().getIncluirMsgLancamentoPersonalizadoPDCAdapter().invokeProcess(request);
		
		incluirLanctoPersonalizadoSaidaDTO.setCodMensagem(response.getCodMensagem());
		incluirLanctoPersonalizadoSaidaDTO.setMensagem(response.getMensagem());
	
		return incluirLanctoPersonalizadoSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService#detalharLanctoPersonalizado(br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharLanctoPersonalizadoEntradaDTO)
	 */
	public DetalharLanctoPersonalizadoSaidaDTO detalharLanctoPersonalizado(DetalharLanctoPersonalizadoEntradaDTO detalharLanctoPersonalizadoEntradaDTO) {
		
		DetalharLanctoPersonalizadoSaidaDTO saidaDTO = new DetalharLanctoPersonalizadoSaidaDTO();
		
		DetalharMsgLancamentoPersonalizadoRequest request = new DetalharMsgLancamentoPersonalizadoRequest();
		DetalharMsgLancamentoPersonalizadoResponse response = new DetalharMsgLancamentoPersonalizadoResponse();
	
		request.setCdMensagemLinhasExtrato(detalharLanctoPersonalizadoEntradaDTO.getCodLancamento());
		request.setCdProdutoServicoOperacao(detalharLanctoPersonalizadoEntradaDTO.getTipoServico());
		request.setCdProdutoOperacaoRelacionado(detalharLanctoPersonalizadoEntradaDTO.getModalidadeServico());
		request.setCdRelacionadoProduto(detalharLanctoPersonalizadoEntradaDTO.getTipoRelacionamento());
	
		response = getFactoryAdapter().getDetalharMsgLancamentoPersonalizadoPDCAdapter().invokeProcess(request);
	
		saidaDTO = new DetalharLanctoPersonalizadoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
	
		saidaDTO.setCdLancCredito(response.getOcorrencias(0).getCdIdentificadorLancamentoCredito());
		saidaDTO.setCdLancDebito(response.getOcorrencias(0).getCdIdentificadorLancamentoDebito());
		saidaDTO.setCdTipoModalidade(response.getOcorrencias(0).getCdProdutoOperacaoRelacionado());
		saidaDTO.setCdTipoRelacionamento(response.getOcorrencias(0).getCdRelacionadoProduto());
		saidaDTO.setCdTipoServico(response.getOcorrencias(0).getCdProdutoServicoOperacao());
		saidaDTO.setCodLancamento(response.getOcorrencias(0).getCdMensagemLinhasExtrato());
		saidaDTO.setDsLancCredito(response.getOcorrencias(0).getDsIdentificadorLancamentoCredito());
		saidaDTO.setDsLancDebito(response.getOcorrencias(0).getDsIdentificadorLancamentoDebito());
		saidaDTO.setDsTipoModalidade(response.getOcorrencias(0).getDsProdutoOperacaoRelacionado());
		saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOperacao());
		saidaDTO.setRestricao(response.getOcorrencias(0).getCdIndicadorRestricaoContrato());
		saidaDTO.setTipoLancamento(response.getOcorrencias(0).getCdTipoMensagemExtrato());
		
		
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao()); 
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegurancaInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaRegistro());
		
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao()); 
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegurancaManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		
		return saidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService#excluirLanctoPersonalizado(br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ExcluirLanctoPersonalizadoEntradaDTO)
	 */
	public ExcluirLanctoPersonalizadoSaidaDTO excluirLanctoPersonalizado(ExcluirLanctoPersonalizadoEntradaDTO excluirLanctoPersonalizadoEntradaDTO) {
		
		ExcluirLanctoPersonalizadoSaidaDTO excluirLanctoPersonalizadoSaidaDTO = new ExcluirLanctoPersonalizadoSaidaDTO();
		ExluirMsgLancamentoPersonalizadoRequest exluirMsgLancamentoPersonalizadoRequest = new ExluirMsgLancamentoPersonalizadoRequest();
		ExluirMsgLancamentoPersonalizadoResponse exluirMsgLancamentoPersonalizadoResponse = new ExluirMsgLancamentoPersonalizadoResponse();
	
		exluirMsgLancamentoPersonalizadoRequest.setCdMensagemLinhasExtratos(excluirLanctoPersonalizadoEntradaDTO.getCodLancamento());
		exluirMsgLancamentoPersonalizadoRequest.setCdProdutoServicoOperacao(excluirLanctoPersonalizadoEntradaDTO.getTipoServico());
		exluirMsgLancamentoPersonalizadoRequest.setCdProdutoOperacaoRelacionado(excluirLanctoPersonalizadoEntradaDTO.getModalidadeServico());
		exluirMsgLancamentoPersonalizadoRequest.setCdRelacionadoProduto(excluirLanctoPersonalizadoEntradaDTO.getTipoRelacionamento());
	
		exluirMsgLancamentoPersonalizadoResponse = getFactoryAdapter().getExcluirMsgLancamentoPersonalizadoPDCAdapter().invokeProcess(exluirMsgLancamentoPersonalizadoRequest);
	
		excluirLanctoPersonalizadoSaidaDTO.setCodMensagem(exluirMsgLancamentoPersonalizadoResponse.getCodMensagem());
		excluirLanctoPersonalizadoSaidaDTO.setMensagem(exluirMsgLancamentoPersonalizadoResponse.getMensagem());
	
		return excluirLanctoPersonalizadoSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService#alterarLanctoPersonalizado(br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.AlterarLanctoPersonalizadoEntradaDTO)
	 */
	public AlterarLanctoPersonalizadoSaidaDTO alterarLanctoPersonalizado(AlterarLanctoPersonalizadoEntradaDTO alterarLanctoPersonalizadoEntradaDTO) {
		
		
		AlterarMsgLancamentoPersonalizadoRequest alterarMsgLancamentoPersonalizadoRequest = new AlterarMsgLancamentoPersonalizadoRequest();
		AlterarMsgLancamentoPersonalizadoResponse alterarMsgLancamentoPersonalizadoResponse = new AlterarMsgLancamentoPersonalizadoResponse();
		AlterarLanctoPersonalizadoSaidaDTO alterarLanctoPersonalizadoSaidaDTO = new AlterarLanctoPersonalizadoSaidaDTO();
		
		alterarMsgLancamentoPersonalizadoRequest.setCdMensagemLinhasExtrato(alterarLanctoPersonalizadoEntradaDTO.getCodLancamento());
		alterarMsgLancamentoPersonalizadoRequest.setCdProdutoServicoOperacao(alterarLanctoPersonalizadoEntradaDTO.getTipoServico());
		alterarMsgLancamentoPersonalizadoRequest.setCdProdutoOperRelacionado(alterarLanctoPersonalizadoEntradaDTO.getModalidadeServico());
		alterarMsgLancamentoPersonalizadoRequest.setCdTipoMensagemExtrato(alterarLanctoPersonalizadoEntradaDTO.getTipoLancamento());
		alterarMsgLancamentoPersonalizadoRequest.setCdIdentificadorLancamentoCredito(alterarLanctoPersonalizadoEntradaDTO.getCodLancamentoCredito());
		alterarMsgLancamentoPersonalizadoRequest.setCdIdentificadorLancamentoDebito(alterarLanctoPersonalizadoEntradaDTO.getCodLancamentoDebito());
		alterarMsgLancamentoPersonalizadoRequest.setCdIndicadorRestricaoContrato(alterarLanctoPersonalizadoEntradaDTO.getRestricao());
		alterarMsgLancamentoPersonalizadoRequest.setCdRelacionadoProduto(alterarLanctoPersonalizadoEntradaDTO.getTipoRelacionamento());
		
		alterarMsgLancamentoPersonalizadoResponse = getFactoryAdapter().getAlterarMsgLancamentoPersonalizadoPDCAdapter().invokeProcess(alterarMsgLancamentoPersonalizadoRequest);
					
		alterarLanctoPersonalizadoSaidaDTO.setCodMensagem(alterarMsgLancamentoPersonalizadoResponse.getCodMensagem());
		alterarLanctoPersonalizadoSaidaDTO.setMensagem(alterarMsgLancamentoPersonalizadoResponse.getMensagem());
				
		return alterarLanctoPersonalizadoSaidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService#historicoLanctoPersonalizado(br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.HistoricoLanctoPersonalizadoEntradaDTO)
	 */
	public List<HistoricoLanctoPersonalizadoSaidaDTO> historicoLanctoPersonalizado(HistoricoLanctoPersonalizadoEntradaDTO historicoLanctoPersonalizadoEntradaDTO) {
		
		List<HistoricoLanctoPersonalizadoSaidaDTO> listaRetorno = new ArrayList<HistoricoLanctoPersonalizadoSaidaDTO>();		
		ConsultarHistManutMsgLancPersonalizadoRequest request = new ConsultarHistManutMsgLancPersonalizadoRequest();
		ConsultarHistManutMsgLancPersonalizadoResponse response = new ConsultarHistManutMsgLancPersonalizadoResponse();
		
		request.setCdMensagemLinhasExtrato(historicoLanctoPersonalizadoEntradaDTO.getCodLancamentoPersonalizado());
		request.setDtFim(historicoLanctoPersonalizadoEntradaDTO.getDataFinal());
		request.setDtInicio(historicoLanctoPersonalizadoEntradaDTO.getDataInicial());
		request.setHrInclusaoRegistroHist("");
		request.setQtOcorrencias(ILanctoPersonServiceConstants.QTDE_CONSULTAS_LISTAR);
	
		response = getFactoryAdapter().getConsultarHistManutMsgLancPersonalizadoPDCAdapter().invokeProcess(request);

		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");		
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		HistoricoLanctoPersonalizadoSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new HistoricoLanctoPersonalizadoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodigoLancamentoPersonalizado(response.getOcorrencias(i).getCdMensagemLinhasExtrato());
			saidaDTO.setDataManutencao(response.getOcorrencias(i).getHrInclusaoRegistroHist());
			
			if (response.getOcorrencias(i).getHrInclusaoRegistroHist() != null && response.getOcorrencias(i).getHrInclusaoRegistroHist().length() >=19 ){
				
				String stringData = response.getOcorrencias(i).getHrInclusaoRegistroHist().substring(0, 19);
				
				try {
					saidaDTO.setDataManutencaoDesc(formato2.format(formato1.parse(stringData)));
				}  catch (ParseException e) {
					saidaDTO.setDataManutencaoDesc("");
				}
			}else{
				saidaDTO.setDataManutencaoDesc("");
			}

		
			saidaDTO.setResponsavel(response.getOcorrencias(i).getCdUsuarioManutencao());
			saidaDTO.setTipoManutencao(response.getOcorrencias(i).getCdIndicadorTipoManutencao());
			
			listaRetorno.add(saidaDTO);
			
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService#detalharHistoricoLanctoPersonalizado(br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharHistoricoLanctoPersonalizadoEntradaDTO)
	 */
	public DetalharHistoricoLanctoPersonalizadoSaidaDTO detalharHistoricoLanctoPersonalizado(DetalharHistoricoLanctoPersonalizadoEntradaDTO detalharHistoricoLanctoPersonalizadoEntradaDTO) {
		
		DetalharHistoricoLanctoPersonalizadoSaidaDTO saidaDTO = new DetalharHistoricoLanctoPersonalizadoSaidaDTO();
		
		DetalharHistManutMsgLancPersonalizadoRequest request = new DetalharHistManutMsgLancPersonalizadoRequest();
		DetalharHistManutMsgLancPersonalizadoResponse response = new DetalharHistManutMsgLancPersonalizadoResponse();
	
		request.setCdMensagemLinhasExtrato(detalharHistoricoLanctoPersonalizadoEntradaDTO.getCodLancamentoPersonalizado());
		request.setDtFim(detalharHistoricoLanctoPersonalizadoEntradaDTO.getDataFinal());
		request.setDtInicio(detalharHistoricoLanctoPersonalizadoEntradaDTO.getDataInicial());
		request.setHrInclusaoRegistroHist(detalharHistoricoLanctoPersonalizadoEntradaDTO.getDataManutencao());
		request.setQtOcorrencias(0);
		
		response = getFactoryAdapter().getDetalharHistManutMsgLancPersonalizadoPDCAdapter().invokeProcess(request);
	
		saidaDTO = new DetalharHistoricoLanctoPersonalizadoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
	
		saidaDTO.setCdLancCredito(response.getOcorrencias(0).getCdIdentificadorLancamentoCredito());
		saidaDTO.setCdLancDebito(response.getOcorrencias(0).getCdIdentificadorLancamentoDebito());
		saidaDTO.setCdTipoModalidade(response.getOcorrencias(0).getCdProdutoOperacaoRelacionado());
		saidaDTO.setCdTipoServico(response.getOcorrencias(0).getCdProdutoServicoOperacao());
		saidaDTO.setCodLancamento(response.getOcorrencias(0).getCdMensagemLinhasExtrato());
		saidaDTO.setDsLancCredito(response.getOcorrencias(0).getDsIdentificadorLancamentoCredito());
		saidaDTO.setDsLancDebito(response.getOcorrencias(0).getDsIdentificadorLancamentoDebito());
		saidaDTO.setDsTipoModalidade(response.getOcorrencias(0).getDsProdutoOperacaoRelacionado());
		saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOperacao());
		saidaDTO.setRestricao(response.getOcorrencias(0).getCdIndicadorRestricaoContrato());
		saidaDTO.setTipoLancamento(response.getOcorrencias(0).getCdTipoMensagemExtrato());
		
		
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao()); 
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegurancaInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao()); 
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		
		return saidaDTO;
		
	}


    
}

