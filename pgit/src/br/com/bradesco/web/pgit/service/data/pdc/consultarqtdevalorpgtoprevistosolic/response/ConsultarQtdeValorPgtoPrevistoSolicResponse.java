/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarQtdeValorPgtoPrevistoSolicResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarQtdeValorPgtoPrevistoSolicResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _qtdeTotalPagtoPrevistoSoltc
     */
    private long _qtdeTotalPagtoPrevistoSoltc = 0;

    /**
     * keeps track of state for field: _qtdeTotalPagtoPrevistoSoltc
     */
    private boolean _has_qtdeTotalPagtoPrevistoSoltc;

    /**
     * Field _vlrTotPagtoPrevistoSolicitacao
     */
    private java.math.BigDecimal _vlrTotPagtoPrevistoSolicitacao = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarQtdeValorPgtoPrevistoSolicResponse() 
     {
        super();
        setVlrTotPagtoPrevistoSolicitacao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.response.ConsultarQtdeValorPgtoPrevistoSolicResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteQtdeTotalPagtoPrevistoSoltc
     * 
     */
    public void deleteQtdeTotalPagtoPrevistoSoltc()
    {
        this._has_qtdeTotalPagtoPrevistoSoltc= false;
    } //-- void deleteQtdeTotalPagtoPrevistoSoltc() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'qtdeTotalPagtoPrevistoSoltc'.
     * 
     * @return long
     * @return the value of field 'qtdeTotalPagtoPrevistoSoltc'.
     */
    public long getQtdeTotalPagtoPrevistoSoltc()
    {
        return this._qtdeTotalPagtoPrevistoSoltc;
    } //-- long getQtdeTotalPagtoPrevistoSoltc() 

    /**
     * Returns the value of field 'vlrTotPagtoPrevistoSolicitacao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTotPagtoPrevistoSolicitacao'.
     */
    public java.math.BigDecimal getVlrTotPagtoPrevistoSolicitacao()
    {
        return this._vlrTotPagtoPrevistoSolicitacao;
    } //-- java.math.BigDecimal getVlrTotPagtoPrevistoSolicitacao() 

    /**
     * Method hasQtdeTotalPagtoPrevistoSoltc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeTotalPagtoPrevistoSoltc()
    {
        return this._has_qtdeTotalPagtoPrevistoSoltc;
    } //-- boolean hasQtdeTotalPagtoPrevistoSoltc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'qtdeTotalPagtoPrevistoSoltc'.
     * 
     * @param qtdeTotalPagtoPrevistoSoltc the value of field
     * 'qtdeTotalPagtoPrevistoSoltc'.
     */
    public void setQtdeTotalPagtoPrevistoSoltc(long qtdeTotalPagtoPrevistoSoltc)
    {
        this._qtdeTotalPagtoPrevistoSoltc = qtdeTotalPagtoPrevistoSoltc;
        this._has_qtdeTotalPagtoPrevistoSoltc = true;
    } //-- void setQtdeTotalPagtoPrevistoSoltc(long) 

    /**
     * Sets the value of field 'vlrTotPagtoPrevistoSolicitacao'.
     * 
     * @param vlrTotPagtoPrevistoSolicitacao the value of field
     * 'vlrTotPagtoPrevistoSolicitacao'.
     */
    public void setVlrTotPagtoPrevistoSolicitacao(java.math.BigDecimal vlrTotPagtoPrevistoSolicitacao)
    {
        this._vlrTotPagtoPrevistoSolicitacao = vlrTotPagtoPrevistoSolicitacao;
    } //-- void setVlrTotPagtoPrevistoSolicitacao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarQtdeValorPgtoPrevistoSolicResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.response.ConsultarQtdeValorPgtoPrevistoSolicResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.response.ConsultarQtdeValorPgtoPrevistoSolicResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.response.ConsultarQtdeValorPgtoPrevistoSolicResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.response.ConsultarQtdeValorPgtoPrevistoSolicResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
