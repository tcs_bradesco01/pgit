/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: EmpresaSegmentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmpresaSegmentoSaidaDTO {
	
	/** Atributo cdSegmento. */
	private Integer cdSegmento;
    
    /** Atributo dsSegmento. */
    private String dsSegmento;
    
    /** Atributo cdSituacaoSegmento. */
    private Integer cdSituacaoSegmento;
    
    /** Atributo dsSituacaoSegmento. */
    private String dsSituacaoSegmento;

	/**
	 * Empresa segmento saida dto.
	 *
	 * @param cdSegmento the cd segmento
	 * @param dsSegmento the ds segmento
	 * @param cdSituacaoSegmento the cd situacao segmento
	 * @param dsSituacaoSegmento the ds situacao segmento
	 */
	public EmpresaSegmentoSaidaDTO(Integer cdSegmento, String dsSegmento, Integer cdSituacaoSegmento, String dsSituacaoSegmento) {
		super();
		this.cdSegmento = cdSegmento;
		this.dsSegmento = dsSegmento;
		this.cdSituacaoSegmento = cdSituacaoSegmento;
		this.dsSituacaoSegmento = dsSituacaoSegmento;
	}
	

	/**
	 * Get: cdSegmento.
	 *
	 * @return cdSegmento
	 */
	public Integer getCdSegmento() {
		return cdSegmento;
	}

	/**
	 * Set: cdSegmento.
	 *
	 * @param cdSegmento the cd segmento
	 */
	public void setCdSegmento(Integer cdSegmento) {
		this.cdSegmento = cdSegmento;
	}

	/**
	 * Get: cdSituacaoSegmento.
	 *
	 * @return cdSituacaoSegmento
	 */
	public int getCdSituacaoSegmento() {
		return cdSituacaoSegmento;
	}

	/**
	 * Set: cdSituacaoSegmento.
	 *
	 * @param cdSituacaoSegmento the cd situacao segmento
	 */
	public void setCdSituacaoSegmento(Integer cdSituacaoSegmento) {
		this.cdSituacaoSegmento = cdSituacaoSegmento;
	}

	/**
	 * Get: dsSegmento.
	 *
	 * @return dsSegmento
	 */
	public String getDsSegmento() {
		return dsSegmento;
	}

	/**
	 * Set: dsSegmento.
	 *
	 * @param dsSegmento the ds segmento
	 */
	public void setDsSegmento(String dsSegmento) {
		this.dsSegmento = dsSegmento;
	}

	/**
	 * Get: dsSituacaoSegmento.
	 *
	 * @return dsSituacaoSegmento
	 */
	public String getDsSituacaoSegmento() {
		return dsSituacaoSegmento;
	}

	/**
	 * Set: dsSituacaoSegmento.
	 *
	 * @param dsSituacaoSegmento the ds situacao segmento
	 */
	public void setDsSituacaoSegmento(String dsSituacaoSegmento) {
		this.dsSituacaoSegmento = dsSituacaoSegmento;
	}
    
}
