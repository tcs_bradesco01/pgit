/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: MoedaEconomicaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MoedaEconomicaSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
    
    /** Atributo numeroLinhas. */
    private Integer numeroLinhas;
    
    /** Atributo cdIndicadorEconomico. */
    private Integer cdIndicadorEconomico;
    
    /** Atributo dsIndicadorEconomico. */
    private String dsIndicadorEconomico;
    
    /** Atributo dsSiglaEconomico. */
    private String dsSiglaEconomico;
    
	/**
	 * Get: cdIndicadorEconomico.
	 *
	 * @return cdIndicadorEconomico
	 */
	public Integer getCdIndicadorEconomico() {
		return cdIndicadorEconomico;
	}
	
	/**
	 * Set: cdIndicadorEconomico.
	 *
	 * @param cdIndicadorEconomico the cd indicador economico
	 */
	public void setCdIndicadorEconomico(Integer cdIndicadorEconomico) {
		this.cdIndicadorEconomico = cdIndicadorEconomico;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsIndicadorEconomico.
	 *
	 * @return dsIndicadorEconomico
	 */
	public String getDsIndicadorEconomico() {
		return dsIndicadorEconomico;
	}
	
	/**
	 * Set: dsIndicadorEconomico.
	 *
	 * @param dsIndicadorEconomico the ds indicador economico
	 */
	public void setDsIndicadorEconomico(String dsIndicadorEconomico) {
		this.dsIndicadorEconomico = dsIndicadorEconomico;
	}
	
	/**
	 * Get: dsSiglaEconomico.
	 *
	 * @return dsSiglaEconomico
	 */
	public String getDsSiglaEconomico() {
		return dsSiglaEconomico;
	}
	
	/**
	 * Set: dsSiglaEconomico.
	 *
	 * @param dsSiglaEconomico the ds sigla economico
	 */
	public void setDsSiglaEconomico(String dsSiglaEconomico) {
		this.dsSiglaEconomico = dsSiglaEconomico;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	
	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
    
    
	
}
