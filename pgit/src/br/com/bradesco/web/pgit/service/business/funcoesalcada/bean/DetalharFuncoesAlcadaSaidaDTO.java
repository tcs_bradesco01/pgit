/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.funcoesalcada.bean;

/**
 * Nome: DetalharFuncoesAlcadaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharFuncoesAlcadaSaidaDTO {
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo dsCentroCusto. */
	private String dsCentroCusto;
	
	/** Atributo codigoAplicacao. */
	private String codigoAplicacao;
	
	/** Atributo cdAcao. */
	private int cdAcao;
	
	/** Atributo dsAcao. */
	private String dsAcao;
	
	/** Atributo indicadorAcesso. */
	private int indicadorAcesso;
	
	/** Atributo tipoRegraAlcada. */
	private int tipoRegraAlcada;
	
	/** Atributo tipoTratamento. */
	private int tipoTratamento;
	
	/** Atributo centroCustoRetorno. */
	private String centroCustoRetorno;
	
	/** Atributo dsCentroCustoRetorno. */
	private String dsCentroCustoRetorno;
	
	/** Atributo codigoAplicacaoRetorno. */
	private String codigoAplicacaoRetorno;
	
	/** Atributo cdServico. */
	private int cdServico;
	
	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo cdOperacao. */
	private int cdOperacao;
	
	/** Atributo dsOperacao. */
	private String dsOperacao;
	
	/** Atributo cdRegra. */
	private int cdRegra;
	
	/** Atributo dsRegra. */
	private String dsRegra;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;	
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;

	
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdAcao.
	 *
	 * @return cdAcao
	 */
	public int getCdAcao() {
		return cdAcao;
	}
	
	/**
	 * Set: cdAcao.
	 *
	 * @param cdAcao the cd acao
	 */
	public void setCdAcao(int cdAcao) {
		this.cdAcao = cdAcao;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: codigoAplicacao.
	 *
	 * @return codigoAplicacao
	 */
	public String getCodigoAplicacao() {
		return codigoAplicacao;
	}
	
	/**
	 * Set: codigoAplicacao.
	 *
	 * @param codigoAplicacao the codigo aplicacao
	 */
	public void setCodigoAplicacao(String codigoAplicacao) {
		this.codigoAplicacao = codigoAplicacao;
	}
	
	/**
	 * Get: dsAcao.
	 *
	 * @return dsAcao
	 */
	public String getDsAcao() {
		return dsAcao;
	}
	
	/**
	 * Set: dsAcao.
	 *
	 * @param dsAcao the ds acao
	 */
	public void setDsAcao(String dsAcao) {
		this.dsAcao = dsAcao;
	}
	
	/**
	 * Get: indicadorAcesso.
	 *
	 * @return indicadorAcesso
	 */
	public int getIndicadorAcesso() {
		return indicadorAcesso;
	}
	
	/**
	 * Set: indicadorAcesso.
	 *
	 * @param indicadorAcesso the indicador acesso
	 */
	public void setIndicadorAcesso(int indicadorAcesso) {
		this.indicadorAcesso = indicadorAcesso;
	}
	
	/**
	 * Get: tipoRegraAlcada.
	 *
	 * @return tipoRegraAlcada
	 */
	public int getTipoRegraAlcada() {
		return tipoRegraAlcada;
	}
	
	/**
	 * Set: tipoRegraAlcada.
	 *
	 * @param tipoRegraAlcada the tipo regra alcada
	 */
	public void setTipoRegraAlcada(int tipoRegraAlcada) {
		this.tipoRegraAlcada = tipoRegraAlcada;
	}
	
	/**
	 * Get: tipoTratamento.
	 *
	 * @return tipoTratamento
	 */
	public int getTipoTratamento() {
		return tipoTratamento;
	}
	
	/**
	 * Set: tipoTratamento.
	 *
	 * @param tipoTratamento the tipo tratamento
	 */
	public void setTipoTratamento(int tipoTratamento) {
		this.tipoTratamento = tipoTratamento;
	}
	
	/**
	 * Get: centroCustoRetorno.
	 *
	 * @return centroCustoRetorno
	 */
	public String getCentroCustoRetorno() {
		return centroCustoRetorno;
	}
	
	/**
	 * Set: centroCustoRetorno.
	 *
	 * @param centroCustoRetorno the centro custo retorno
	 */
	public void setCentroCustoRetorno(String centroCustoRetorno) {
		this.centroCustoRetorno = centroCustoRetorno;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdOperacao.
	 *
	 * @return cdOperacao
	 */
	public int getCdOperacao() {
		return cdOperacao;
	}
	
	/**
	 * Set: cdOperacao.
	 *
	 * @param cdOperacao the cd operacao
	 */
	public void setCdOperacao(int cdOperacao) {
		this.cdOperacao = cdOperacao;
	}
	
	/**
	 * Get: cdRegra.
	 *
	 * @return cdRegra
	 */
	public int getCdRegra() {
		return cdRegra;
	}
	
	/**
	 * Set: cdRegra.
	 *
	 * @param cdRegra the cd regra
	 */
	public void setCdRegra(int cdRegra) {
		this.cdRegra = cdRegra;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public int getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(int cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: codigoAplicacaoRetorno.
	 *
	 * @return codigoAplicacaoRetorno
	 */
	public String getCodigoAplicacaoRetorno() {
		return codigoAplicacaoRetorno;
	}
	
	/**
	 * Set: codigoAplicacaoRetorno.
	 *
	 * @param codigoAplicacaoRetorno the codigo aplicacao retorno
	 */
	public void setCodigoAplicacaoRetorno(String codigoAplicacaoRetorno) {
		this.codigoAplicacaoRetorno = codigoAplicacaoRetorno;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsOperacao.
	 *
	 * @return dsOperacao
	 */
	public String getDsOperacao() {
		return dsOperacao;
	}
	
	/**
	 * Set: dsOperacao.
	 *
	 * @param dsOperacao the ds operacao
	 */
	public void setDsOperacao(String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}
	
	/**
	 * Get: dsRegra.
	 *
	 * @return dsRegra
	 */
	public String getDsRegra() {
		return dsRegra;
	}
	
	/**
	 * Set: dsRegra.
	 *
	 * @param dsRegra the ds regra
	 */
	public void setDsRegra(String dsRegra) {
		this.dsRegra = dsRegra;
	}
	
	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}
	
	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: dsCentroCusto.
	 *
	 * @return dsCentroCusto
	 */
	public String getDsCentroCusto() {
		return dsCentroCusto;
	}
	
	/**
	 * Set: dsCentroCusto.
	 *
	 * @param dsCentroCusto the ds centro custo
	 */
	public void setDsCentroCusto(String dsCentroCusto) {
		this.dsCentroCusto = dsCentroCusto;
	}
	
	/**
	 * Get: dsCentroCustoRetorno.
	 *
	 * @return dsCentroCustoRetorno
	 */
	public String getDsCentroCustoRetorno() {
		return dsCentroCustoRetorno;
	}
	
	/**
	 * Set: dsCentroCustoRetorno.
	 *
	 * @param dsCentroCustoRetorno the ds centro custo retorno
	 */
	public void setDsCentroCustoRetorno(String dsCentroCustoRetorno) {
		this.dsCentroCustoRetorno = dsCentroCustoRetorno;
	}

}
