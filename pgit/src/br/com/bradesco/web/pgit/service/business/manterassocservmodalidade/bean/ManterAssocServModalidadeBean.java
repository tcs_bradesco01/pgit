/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.IManterAssocServModalidadeService;


/**
 * Nome: ManterAssocServModalidadeBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterAssocServModalidadeBean {
	
	
	
	/** Atributo cboTipoServico. */
	private Integer cboTipoServico;
	
	/** Atributo cboServRelacionado. */
	private Integer cboServRelacionado;
	
	/** Atributo cboTipoLote. */
	private Integer  cboTipoLote;
	
	/** Atributo cboTipoLayoutArquivo. */
	private Integer  cboTipoLayoutArquivo;
	
	/** Atributo filtroTipoServico. */
	private Integer filtroTipoServico;
	
	/** Atributo filtroServRelacionado. */
	private Integer filtroServRelacionado;
	
	/** Atributo filtroTipoLote. */
	private Integer  filtroTipoLote;
	
	/** Atributo filtroTipoLayoutArquivo. */
	private Integer  filtroTipoLayoutArquivo;
	
	
	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;
	
	/** Atributo modalidadeServicoDesc. */
	private String modalidadeServicoDesc;
	
	/** Atributo tipoLoteDesc. */
	private String  tipoLoteDesc;
	
	/** Atributo tipoLayoutArquivoDesc. */
	private String  tipoLayoutArquivoDesc;
	
	/** Atributo listaPesquisa. */
	private List<ListarAssocServModalidadeSaidaDTO> listaPesquisa;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionado. */
	private Integer itemSelecionado ;
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico =  new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico =  new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLote. */
	private List<SelectItem> listaTipoLote =  new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo =  new ArrayList<SelectItem>();
	
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuario. */
	private String usuario;
	
	/** Atributo tipoCanal. */
	private String tipoCanal;
	
	/** Atributo complemento. */
	private String complemento;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo listaModalidadeServicoIncluir. */
	private List<SelectItem> listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo assocServModalidadeService. */
	private IManterAssocServModalidadeService assocServModalidadeService;
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaModalidadeServicoIncluirHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaTipoLoteHash. */
	private Map<Integer, String> listaTipoLoteHash = new HashMap<Integer, String>();
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();
	
	/** Atributo hiddenObrigatoriedade. */
	private String hiddenObrigatoriedade;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;
	
	
	
	/**
	 * Carrega lista tipo servico.
	 */
	public void carregaListaTipoServico(){
		
		try{
			this.listaTipoServico = new ArrayList<SelectItem>();
			
			List<ListarServicosSaidaDTO> listaServico = new ArrayList<ListarServicosSaidaDTO>();
			
			listaServico = comboService.listarTipoServicos(1);
			
			listaTipoServicoHash.clear();
			for(ListarServicosSaidaDTO combo : listaServico){
				listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoServico = new ArrayList<SelectItem>();
		}
			
	}
	
	/**
	 * Carrega lista tipo lote.
	 */
	public void carregaListaTipoLote(){
		try{
			this.listaTipoLote = new ArrayList<SelectItem>();
			
			List<TipoLoteSaidaDTO> listaTipoLoteSaida = new ArrayList<TipoLoteSaidaDTO>();
			TipoLoteEntradaDTO tipoLoteEntrada = new TipoLoteEntradaDTO();
			tipoLoteEntrada.setCdSituacaoVinculacaoConta(0);
			
			listaTipoLoteSaida = comboService.listarTipoLote(tipoLoteEntrada);
			
			listaTipoLoteHash.clear();
			for(TipoLoteSaidaDTO combo : listaTipoLoteSaida){
				listaTipoLoteHash.put(combo.getCdTipoLoteLayout(), combo.getDsTipoLoteLayout());
				listaTipoLote.add(new SelectItem(combo.getCdTipoLoteLayout(), combo.getDsTipoLoteLayout()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLote = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Carrega lista modalidade servico.
	 */
	public void carregaListaModalidadeServico(){
		
		try{
			listaModalidadeServico = new ArrayList<SelectItem>();
			
			if ( getFiltroTipoServico() != null &&  getFiltroTipoServico()!=0){
				
				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
				
				listarModalidadeEntradaDTO.setCdServico(getFiltroTipoServico());
			
				listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
	
				
				listaModalidadeServicoHash.clear();
				for(ListarModalidadeSaidaDTO combo : listaModalidades){
					listaModalidadeServicoHash.put(combo.getCdModalidade(),combo);
					this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
				}
			}else{
				
				listaModalidadeServicoHash.clear();
				listaModalidadeServico.clear();
				setFiltroTipoServico(0);
			}
		}catch(PdcAdapterFunctionalException e){
			listaModalidadeServico = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Carrega lista tipo layout.
	 */
	public void carregaListaTipoLayout(){
		try{
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();		
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);
		
			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(tipoLoteEntradaDTO);
			listaTipoLayout = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			listaTipoLayoutArquivoHash.clear();
			for(TipoLayoutArquivoSaidaDTO combo : listaTipoLayout){
				listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo.getCdTipoLayoutArquivo(),combo.getDsTipoLayoutArquivo()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
		
	}
	
	

	/**
	 * Get: listaModalidadeServicoHash.
	 *
	 * @return listaModalidadeServicoHash
	 */
	public Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}
	
	/**
	 * Set lista modalidade servico hash.
	 *
	 * @param listaModalidadeServicoHash the lista modalidade servico hash
	 */
	public void setListaModalidadeServicoHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}
	
	/**
	 * Get: listaModalidadeServicoIncluirHash.
	 *
	 * @return listaModalidadeServicoIncluirHash
	 */
	public Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoIncluirHash() {
		return listaModalidadeServicoIncluirHash;
	}
	
	/**
	 * Set lista modalidade servico incluir hash.
	 *
	 * @param listaModalidadeServicoIncluirHash the lista modalidade servico incluir hash
	 */
	public void setListaModalidadeServicoIncluirHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoIncluirHash) {
		this.listaModalidadeServicoIncluirHash = listaModalidadeServicoIncluirHash;
	}
	
	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}
	
	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}
	
	/**
	 * Get: listaTipoLoteHash.
	 *
	 * @return listaTipoLoteHash
	 */
	public Map<Integer, String> getListaTipoLoteHash() {
		return listaTipoLoteHash;
	}
	
	/**
	 * Set lista tipo lote hash.
	 *
	 * @param listaTipoLoteHash the lista tipo lote hash
	 */
	public void setListaTipoLoteHash(Map<Integer, String> listaTipoLoteHash) {
		this.listaTipoLoteHash = listaTipoLoteHash;
	}
	
	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Get: complemento.
	 *
	 * @return complemento
	 */
	public String getComplemento() {
		return complemento;
	}

	/**
	 * Set: complemento.
	 *
	 * @param complemento the complemento
	 */
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Get: filtroServRelacionado.
	 *
	 * @return filtroServRelacionado
	 */
	public Integer getFiltroServRelacionado() {
		return filtroServRelacionado;
	}
	
	/**
	 * Set: filtroServRelacionado.
	 *
	 * @param filtroServRelacionado the filtro serv relacionado
	 */
	public void setFiltroServRelacionado(Integer filtroServRelacionado) {
		this.filtroServRelacionado = filtroServRelacionado;
	}
	
	/**
	 * Get: filtroTipoLayoutArquivo.
	 *
	 * @return filtroTipoLayoutArquivo
	 */
	public Integer getFiltroTipoLayoutArquivo() {
		return filtroTipoLayoutArquivo;
	}
	
	/**
	 * Set: filtroTipoLayoutArquivo.
	 *
	 * @param filtroTipoLayoutArquivo the filtro tipo layout arquivo
	 */
	public void setFiltroTipoLayoutArquivo(Integer filtroTipoLayoutArquivo) {
		this.filtroTipoLayoutArquivo = filtroTipoLayoutArquivo;
	}
	
	/**
	 * Get: filtroTipoLote.
	 *
	 * @return filtroTipoLote
	 */
	public Integer getFiltroTipoLote() {
		return filtroTipoLote;
	}
	
	/**
	 * Set: filtroTipoLote.
	 *
	 * @param filtroTipoLote the filtro tipo lote
	 */
	public void setFiltroTipoLote(Integer filtroTipoLote) {
		this.filtroTipoLote = filtroTipoLote;
	}
	
	/**
	 * Get: filtroTipoServico.
	 *
	 * @return filtroTipoServico
	 */
	public Integer getFiltroTipoServico() {
		return filtroTipoServico;
	}
	
	/**
	 * Set: filtroTipoServico.
	 *
	 * @param filtroTipoServico the filtro tipo servico
	 */
	public void setFiltroTipoServico(Integer filtroTipoServico) {
		this.filtroTipoServico = filtroTipoServico;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanal.
	 *
	 * @return tipoCanal
	 */
	public String getTipoCanal() {
		return tipoCanal;
	}

	/**
	 * Set: tipoCanal.
	 *
	 * @param tipoCanal the tipo canal
	 */
	public void setTipoCanal(String tipoCanal) {
		this.tipoCanal = tipoCanal;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuario.
	 *
	 * @return usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Set: usuario.
	 *
	 * @param usuario the usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	
	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: listaPesquisa.
	 *
	 * @return listaPesquisa
	 */
	public List<ListarAssocServModalidadeSaidaDTO> getListaPesquisa() {
		return listaPesquisa;
	}


	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}
	

	/**
	 * Set: listaPesquisa.
	 *
	 * @param listaPesquisa the lista pesquisa
	 */
	public void setListaPesquisa(List<ListarAssocServModalidadeSaidaDTO> listaPesquisa) {
		this.listaPesquisa = listaPesquisa;
	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		
	/*	if (getTipoServico()!=null && getTipoServico()!=0){
			
			
			
			this.listaModalidadeServico = new ArrayList<SelectItem>();
			 listaModalidadeServico.add(new SelectItem(new Integer("1"),"Limite Di�rio"));
			 listaModalidadeServico.add(new SelectItem(new Integer("2"),"Limite Mensal"));
								
		
	}else{
		listaModalidadeServico.clear();
	}*/
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidadeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLote.
	 *
	 * @return listaTipoLote
	 */
	public List<SelectItem> getListaTipoLote() {
		return listaTipoLote;
	}

	/**
	 * Set: listaTipoLote.
	 *
	 * @param listaTipoLote the lista tipo lote
	 */
	public void setListaTipoLote(List<SelectItem> listaTipoLote) {
		this.listaTipoLote = listaTipoLote;
	}


	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		
		setFiltroTipoServico(0);
		setFiltroServRelacionado(0);		
		setFiltroTipoLote(0);
		setFiltroTipoLayoutArquivo(0);		
		carregaListaModalidadeServico();
		setListaPesquisa(null);
		setItemSelecionado(null);
		setBtoAcionado(false);
		
		return "";
				
	}
	
	/**
	 * Carrega lista.
	 */
	public void carregaLista(){
		
		
	try{
			
			ListarAssocServModalidadeEntradaDTO entradaDTO = new ListarAssocServModalidadeEntradaDTO();
			
			entradaDTO.setTipoLayoutArquivo( getFiltroTipoLayoutArquivo() != null ?getFiltroTipoLayoutArquivo() : 0 );
			entradaDTO.setTipoLote(getFiltroTipoLote() !=null ? getFiltroTipoLote(): 0);
			entradaDTO.setTipoServico(getFiltroTipoServico() != null ? getFiltroTipoServico() : 0);
			entradaDTO.setModalidadeServico(getFiltroServRelacionado() != null ?getFiltroServRelacionado() : 0);
			entradaDTO.setTipoRelacionamento( getFiltroServRelacionado() != null && getFiltroServRelacionado() != 0 && listaModalidadeServicoHash !=null && !listaModalidadeServicoHash.isEmpty() ? listaModalidadeServicoHash.get(getFiltroServRelacionado()).getCdRelacionamentoProduto() : 0 );
			
			
			setListaPesquisa(getAssocServModalidadeService().listarAssocServModalidade(entradaDTO));
			setBtoAcionado(true);
			listaControle = new ArrayList<SelectItem>();
			
			for(int i = 0; i < getListaPesquisa().size(); i++){
				listaControle.add(new SelectItem(i,""));
			}
			
			setItemSelecionado(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaPesquisa(null);

		}
		
		
	}


	
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt){
		
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		setCboTipoLayoutArquivo(0);
		setCboTipoLote(0);
		setCboTipoServico(0);
		setCboServRelacionado(0);
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try {
			carregaVariaveis();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterMoedasSistema", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "AVANCAR_DETALHAR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {
			carregaVariaveis();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterMoedasSistema", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "AVANCAR_EXCLUIR";
	}
	
	/**
	 * Voltar detalhar.
	 *
	 * @return the string
	 */
	public String voltarDetalhar(){
		setItemSelecionado(null);
		return "VOLTAR_DETALHAR";
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		setItemSelecionado(null);
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		if (getHiddenObrigatoriedade() != null && getHiddenObrigatoriedade().equals("T")){
			setTipoServicoDesc((String) listaTipoServicoHash.get(getCboTipoServico()));
			setModalidadeServicoDesc(listaModalidadeServicoIncluirHash.get(getCboServRelacionado()).getDsModalidade());
			setTipoLoteDesc((String) listaTipoLoteHash.get(getCboTipoLote()));
			setTipoLayoutArquivoDesc((String) listaTipoLayoutArquivoHash.get(getCboTipoLayoutArquivo()));
			return "AVANCAR_INCLUIR";
		}
		return "";
		
	}
	
	/**
	 * Voltar incluir2.
	 *
	 * @return the string
	 */
	public String voltarIncluir2(){
		
		return "VOLTAR_INCLUIR2";
	}
	
	/**
	 * Voltar excluir.
	 *
	 * @return the string
	 */
	public String voltarExcluir(){
		setItemSelecionado(null);
		return "VOLTAR_EXCLUIR";
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		
		limparDados();		
		carregaListaTipoLayout();
		carregaListaTipoLote();
		carregaListaTipoServico();
		carregaListaModalidadeServico();
		setBtoAcionado(false);
	}

	/**
	 * Get: cboServRelacionado.
	 *
	 * @return cboServRelacionado
	 */
	public Integer getCboServRelacionado() {
		return cboServRelacionado;
	}

	/**
	 * Set: cboServRelacionado.
	 *
	 * @param cboServRelacionado the cbo serv relacionado
	 */
	public void setCboServRelacionado(Integer cboServRelacionado) {
		this.cboServRelacionado = cboServRelacionado;
	}

	/**
	 * Get: cboTipoLayoutArquivo.
	 *
	 * @return cboTipoLayoutArquivo
	 */
	public Integer getCboTipoLayoutArquivo() {
		return cboTipoLayoutArquivo;
	}

	/**
	 * Set: cboTipoLayoutArquivo.
	 *
	 * @param cboTipoLayoutArquivo the cbo tipo layout arquivo
	 */
	public void setCboTipoLayoutArquivo(Integer cboTipoLayoutArquivo) {
		this.cboTipoLayoutArquivo = cboTipoLayoutArquivo;
	}

	/**
	 * Get: cboTipoLote.
	 *
	 * @return cboTipoLote
	 */
	public Integer getCboTipoLote() {
		return cboTipoLote;
	}

	/**
	 * Set: cboTipoLote.
	 *
	 * @param cboTipoLote the cbo tipo lote
	 */
	public void setCboTipoLote(Integer cboTipoLote) {
		this.cboTipoLote = cboTipoLote;
	}

	/**
	 * Get: cboTipoServico.
	 *
	 * @return cboTipoServico
	 */
	public Integer getCboTipoServico() {
		return cboTipoServico;
	}

	/**
	 * Set: cboTipoServico.
	 *
	 * @param cboTipoServico the cbo tipo servico
	 */
	public void setCboTipoServico(Integer cboTipoServico) {
		this.cboTipoServico = cboTipoServico;
	}

	/**
	 * Get: modalidadeServicoDesc.
	 *
	 * @return modalidadeServicoDesc
	 */
	public String getModalidadeServicoDesc() {
		return modalidadeServicoDesc;
	}

	/**
	 * Set: modalidadeServicoDesc.
	 *
	 * @param modalidadeServicoDesc the modalidade servico desc
	 */
	public void setModalidadeServicoDesc(String modalidadeServicoDesc) {
		this.modalidadeServicoDesc = modalidadeServicoDesc;
	}

	/**
	 * Get: tipoLayoutArquivoDesc.
	 *
	 * @return tipoLayoutArquivoDesc
	 */
	public String getTipoLayoutArquivoDesc() {
		return tipoLayoutArquivoDesc;
	}

	/**
	 * Set: tipoLayoutArquivoDesc.
	 *
	 * @param tipoLayoutArquivoDesc the tipo layout arquivo desc
	 */
	public void setTipoLayoutArquivoDesc(String tipoLayoutArquivoDesc) {
		this.tipoLayoutArquivoDesc = tipoLayoutArquivoDesc;
	}

	/**
	 * Get: tipoLoteDesc.
	 *
	 * @return tipoLoteDesc
	 */
	public String getTipoLoteDesc() {
		return tipoLoteDesc;
	}

	/**
	 * Set: tipoLoteDesc.
	 *
	 * @param tipoLoteDesc the tipo lote desc
	 */
	public void setTipoLoteDesc(String tipoLoteDesc) {
		this.tipoLoteDesc = tipoLoteDesc;
	}

	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}

	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param tipoServicoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String tipoServicoDesc) {
		this.tipoServicoDesc = tipoServicoDesc;
	}
	
	/**
	 * Get: listaModalidadeServicoIncluir.
	 *
	 * @return listaModalidadeServicoIncluir
	 */
	public List<SelectItem> getListaModalidadeServicoIncluir() {
		
		if ( getCboTipoServico()!= null &&  getCboTipoServico()!=0){	
			 
				listaModalidadeServicoIncluir = new ArrayList<SelectItem>();		
				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
				
				listarModalidadeEntradaDTO.setCdServico(getCboTipoServico());
			
				listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
				
				
				listaModalidadeServicoIncluirHash.clear();
				for(ListarModalidadeSaidaDTO combo : listaModalidades){
					listaModalidadeServicoIncluirHash.put(combo.getCdModalidade(), combo);
					this.listaModalidadeServicoIncluir.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
					
					
				}
			
				
		}else{
			listaModalidadeServicoIncluirHash.clear();
			listaModalidadeServicoIncluir.clear();
		}
		
		return listaModalidadeServicoIncluir;
	}
	
	
	/**
	 * Carrega lista modalidade servico incluir.
	 */
	public void carregaListaModalidadeServicoIncluir(){
		
		getListaModalidadeServicoIncluir();
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}
	
	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}
	
	/**
	 * Set: listaModalidadeServicoIncluir.
	 *
	 * @param listaModalidadeServicoIncluir the lista modalidade servico incluir
	 */
	public void setListaModalidadeServicoIncluir(
			List<SelectItem> listaModalidadeServicoIncluir) {
		this.listaModalidadeServicoIncluir = listaModalidadeServicoIncluir;
	}
	
	/**
	 * Get: assocServModalidadeService.
	 *
	 * @return assocServModalidadeService
	 */
	public IManterAssocServModalidadeService getAssocServModalidadeService() {
		return assocServModalidadeService;
	}
	
	/**
	 * Set: assocServModalidadeService.
	 *
	 * @param assocServModalidadeService the assoc serv modalidade service
	 */
	public void setAssocServModalidadeService(
			IManterAssocServModalidadeService assocServModalidadeService) {
		this.assocServModalidadeService = assocServModalidadeService;
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		
		try {
				
				IncluirAssocServModalidadeEntradaDTO incluirAssocServModalidadeEntradaDTO = new IncluirAssocServModalidadeEntradaDTO();
				
				incluirAssocServModalidadeEntradaDTO.setTipoServico(getCboTipoServico());
				incluirAssocServModalidadeEntradaDTO.setModalidadeServico(getCboServRelacionado());
				incluirAssocServModalidadeEntradaDTO.setTipoRelacionamento(listaModalidadeServicoIncluirHash.get(getCboServRelacionado()).getCdRelacionamentoProduto());
				incluirAssocServModalidadeEntradaDTO.setTipoLayoutArquivo(getCboTipoLayoutArquivo());
				incluirAssocServModalidadeEntradaDTO.setTipoLote(getCboTipoLote());
				
				IncluirAssocServModalidadeSaidaDTO incluirAssocServModalidadeSaidaDTO = getAssocServModalidadeService().incluirAssocServModalidade(incluirAssocServModalidadeEntradaDTO);
				
				BradescoFacesUtils.addInfoModalMessage("(" +incluirAssocServModalidadeSaidaDTO.getCodMensagem() + ") " + incluirAssocServModalidadeSaidaDTO.getMensagem(), "conAssocLoteServModalidade", BradescoViewExceptionActionType.ACTION, false);			
				carregaLista();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			return "";
	}
	
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		
		try {
			
				ListarAssocServModalidadeSaidaDTO  listarAssocServModalidadeSaidaDTO =  getListaPesquisa().get(getItemSelecionado());
			
				ExcluirAssocServModalidadeEntradaDTO excluirAssocServModalidadeEntradaDTO = new ExcluirAssocServModalidadeEntradaDTO();
				
				excluirAssocServModalidadeEntradaDTO.setTipoServico(listarAssocServModalidadeSaidaDTO.getCdTipoServico());
				excluirAssocServModalidadeEntradaDTO.setModalidadeServico(listarAssocServModalidadeSaidaDTO.getCdModalidadeServico());
				excluirAssocServModalidadeEntradaDTO.setTipoRelacionamento(listarAssocServModalidadeSaidaDTO.getCdTipoRelacionamento());
				excluirAssocServModalidadeEntradaDTO.setTipoLayoutArquivo(listarAssocServModalidadeSaidaDTO.getCdTipoLayoutArquivo());
				excluirAssocServModalidadeEntradaDTO.setTipoLote(listarAssocServModalidadeSaidaDTO.getCdTipoLote());
				
				ExcluirAssocServModalidadeSaidaDTO excluirAssocServModalidadeSaidaDTO = getAssocServModalidadeService().excluirAssocServModalidade(excluirAssocServModalidadeEntradaDTO);
				
				BradescoFacesUtils.addInfoModalMessage("(" +excluirAssocServModalidadeSaidaDTO.getCodMensagem() + ") " + excluirAssocServModalidadeSaidaDTO.getMensagem(), "conAssocLoteServModalidade", BradescoViewExceptionActionType.ACTION, false);			
				carregaLista();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			
			return "";
	}		
	
	/**
	 * Carrega variaveis.
	 */
	public void carregaVariaveis(){	
		
		ListarAssocServModalidadeSaidaDTO listarAssocServModalidadeSaidaDTO =  getListaPesquisa().get(getItemSelecionado());
		
		DetalharAssocServModalidadeEntradaDTO detalharAssocServModalidadeEntradaDTO = new DetalharAssocServModalidadeEntradaDTO();
		
		detalharAssocServModalidadeEntradaDTO.setTipoServico(listarAssocServModalidadeSaidaDTO.getCdTipoServico());
		detalharAssocServModalidadeEntradaDTO.setModalidadeServico(listarAssocServModalidadeSaidaDTO.getCdModalidadeServico());
		detalharAssocServModalidadeEntradaDTO.setTipoRelacionamento(listarAssocServModalidadeSaidaDTO.getCdTipoRelacionamento());
		detalharAssocServModalidadeEntradaDTO.setTipoLayoutArquivo(listarAssocServModalidadeSaidaDTO.getCdTipoLayoutArquivo());
		detalharAssocServModalidadeEntradaDTO.setTipoLote(listarAssocServModalidadeSaidaDTO.getCdTipoLote());
		
		DetalharAssocServModalidadeSaidaDTO detalharAssocServModalidadeSaidaDTO = getAssocServModalidadeService().detalharAssocServModalidade(detalharAssocServModalidadeEntradaDTO);
		
		
		setTipoServicoDesc(detalharAssocServModalidadeSaidaDTO.getDsTipoServico());
		setModalidadeServicoDesc(detalharAssocServModalidadeSaidaDTO.getDsModalidadeServico());
		setTipoLoteDesc(detalharAssocServModalidadeSaidaDTO.getDsTipoLote());
		setTipoLayoutArquivoDesc(detalharAssocServModalidadeSaidaDTO.getDsTipoLayoutArquivo());		
		setUsuario(detalharAssocServModalidadeSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharAssocServModalidadeSaidaDTO.getUsuarioManutencao());		
		setComplemento(detalharAssocServModalidadeSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharAssocServModalidadeSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharAssocServModalidadeSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharAssocServModalidadeSaidaDTO.getComplementoManutencao());		
		setTipoCanal(detalharAssocServModalidadeSaidaDTO.getCdCanalInclusao()==0? "" : detalharAssocServModalidadeSaidaDTO.getCdCanalInclusao() + " - " +  detalharAssocServModalidadeSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharAssocServModalidadeSaidaDTO.getCdCanalManutencao()==0? "" : detalharAssocServModalidadeSaidaDTO.getCdCanalManutencao() + " - " + detalharAssocServModalidadeSaidaDTO.getDsCanalManutencao());
		setDataHoraInclusao(detalharAssocServModalidadeSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharAssocServModalidadeSaidaDTO.getDataHoraManutencao());
		
		
		
	}
	
	/**
	 * Get: hiddenObrigatoriedade.
	 *
	 * @return hiddenObrigatoriedade
	 */
	public String getHiddenObrigatoriedade() {
		return hiddenObrigatoriedade;
	}
	
	/**
	 * Set: hiddenObrigatoriedade.
	 *
	 * @param hiddenObrigatoriedade the hidden obrigatoriedade
	 */
	public void setHiddenObrigatoriedade(String hiddenObrigatoriedade) {
		this.hiddenObrigatoriedade = hiddenObrigatoriedade;
	}
	
	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}
	
	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}
	
	
}
