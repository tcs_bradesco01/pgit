/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

/**
 * Nome: DetalharHistLiquidacaoPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistLiquidacaoPagamentoSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdFormaLiquidacao. */
    private Integer cdFormaLiquidacao;
    
    /** Atributo dsFormaLiquidacao. */
    private String dsFormaLiquidacao;
    
    /** Atributo hrLimiteProcedimentoPagamento. */
    private String hrLimiteProcedimentoPagamento;
    
    /** Atributo cdSistema. */
    private String cdSistema;
    
    /** Atributo qtMinutoMargemSeguranca. */
    private Integer qtMinutoMargemSeguranca;
    
    /** Atributo cdPrioridadeDebitoForma. */
    private Integer cdPrioridadeDebitoForma;
    
    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdAutenticacaoSegurancaInclusao. */
    private String cdAutenticacaoSegurancaInclusao;
    
    /** Atributo nrOperacaoFluxoInclusao. */
    private String nrOperacaoFluxoInclusao;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo cdAutenticacaoSegurancaManutencao. */
    private String cdAutenticacaoSegurancaManutencao;
    
    /** Atributo nrOperacaoFluxoManutencao. */
    private String nrOperacaoFluxoManutencao;
    
    /** Atributo hrManutencaoRegistroManutencao. */
    private String hrManutencaoRegistroManutencao;
    
    /** Atributo dsSistema. */
    private String dsSistema;
    
    /** Atributo hrConsultasSaldoPagamento. */
    private String hrConsultasSaldoPagamento;
    
    /** Atributo hrConsultaFolhaPgto. */
    private String hrConsultaFolhaPgto;
    
    /** Atributo qtMinutosValorSuperior. */
    private Integer qtMinutosValorSuperior;
    
    /** Atributo hrLimiteValorSuperior. */
    private String hrLimiteValorSuperior;

    /** Atributo qtdTempoAgendamentoCobranca. */
    private Integer qtdTempoAgendamentoCobranca = null;

    /** Atributo qtdTempoEfetivacaoCiclicaCobranca. */
    private Integer qtdTempoEfetivacaoCiclicaCobranca = null;

    /** Atributo qtdTempoEfetivacaoDiariaCobranca. */
    private Integer qtdTempoEfetivacaoDiariaCobranca = null;       
    
    /** Atributo qtdLimiteSemResposta. */
    private Integer qtdLimiteSemResposta = null;
   
    /** Atributo qtdLimiteProcessamentoExcedido. */
    private Integer qtdLimiteProcessamentoExcedido = null;
        

	/**
	 * Detalhar hist liquidacao pagamento saida dto.
	 */
	public DetalharHistLiquidacaoPagamentoSaidaDTO() {
		super();
	}

	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return cdSistema + " - " + dsSistema;
	}
	
	/**
	 * Get: canalInclusao.
	 *
	 * @return canalInclusao
	 */
	public String getCanalInclusao() {
		return  String.valueOf(cdCanalInclusao).equals("0") ? "" :  cdCanalInclusao + "-" + dsCanalInclusao;
	}
	
	/**
	 * Get: canalManutencao.
	 *
	 * @return canalManutencao
	 */
	public String getCanalManutencao() {
		return  String.valueOf(cdCanalManutencao).equals("0") ? "" :  cdCanalManutencao + "-" + dsCanalManutencao;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao.trim().equals("0") ? "" : cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao.trim().equals("0") ? "" : cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: cdPrioridadeDebitoForma.
	 *
	 * @return cdPrioridadeDebitoForma
	 */
	public Integer getCdPrioridadeDebitoForma() {
		return cdPrioridadeDebitoForma;
	}

	/**
	 * Set: cdPrioridadeDebitoForma.
	 *
	 * @param cdPrioridadeDebitoForma the cd prioridade debito forma
	 */
	public void setCdPrioridadeDebitoForma(Integer cdPrioridadeDebitoForma) {
		this.cdPrioridadeDebitoForma = cdPrioridadeDebitoForma;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrLimiteProcedimentoPagamento.
	 *
	 * @return hrLimiteProcedimentoPagamento
	 */
	public String getHrLimiteProcedimentoPagamento() {
		return hrLimiteProcedimentoPagamento;
	}

	/**
	 * Set: hrLimiteProcedimentoPagamento.
	 *
	 * @param hrLimiteProcedimentoPagamento the hr limite procedimento pagamento
	 */
	public void setHrLimiteProcedimentoPagamento(String hrLimiteProcedimentoPagamento) {
		this.hrLimiteProcedimentoPagamento = hrLimiteProcedimentoPagamento;
	}

	/**
	 * Get: hrManutencaoRegistroManutencao.
	 *
	 * @return hrManutencaoRegistroManutencao
	 */
	public String getHrManutencaoRegistroManutencao() {
		return hrManutencaoRegistroManutencao;
	}

	/**
	 * Set: hrManutencaoRegistroManutencao.
	 *
	 * @param hrManutencaoRegistroManutencao the hr manutencao registro manutencao
	 */
	public void setHrManutencaoRegistroManutencao(
			String hrManutencaoRegistroManutencao) {
		this.hrManutencaoRegistroManutencao = hrManutencaoRegistroManutencao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrOperacaoFluxoInclusao.
	 *
	 * @return nrOperacaoFluxoInclusao
	 */
	public String getNrOperacaoFluxoInclusao() {
		return nrOperacaoFluxoInclusao.trim().equals("0") ? "" : nrOperacaoFluxoInclusao;
	}

	/**
	 * Set: nrOperacaoFluxoInclusao.
	 *
	 * @param nrOperacaoFluxoInclusao the nr operacao fluxo inclusao
	 */
	public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
		this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
	}

	/**
	 * Get: nrOperacaoFluxoManutencao.
	 *
	 * @return nrOperacaoFluxoManutencao
	 */
	public String getNrOperacaoFluxoManutencao() {
		return nrOperacaoFluxoManutencao.trim().equals("0") ? "" : nrOperacaoFluxoManutencao;
	}

	/**
	 * Set: nrOperacaoFluxoManutencao.
	 *
	 * @param nrOperacaoFluxoManutencao the nr operacao fluxo manutencao
	 */
	public void setNrOperacaoFluxoManutencao(String nrOperacaoFluxoManutencao) {
		this.nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
	}

	/**
	 * Get: qtMinutoMargemSeguranca.
	 *
	 * @return qtMinutoMargemSeguranca
	 */
	public Integer getQtMinutoMargemSeguranca() {
		return qtMinutoMargemSeguranca;
	}

	/**
	 * Set: qtMinutoMargemSeguranca.
	 *
	 * @param qtMinutoMargemSeguranca the qt minuto margem seguranca
	 */
	public void setQtMinutoMargemSeguranca(Integer qtMinutoMargemSeguranca) {
		this.qtMinutoMargemSeguranca = qtMinutoMargemSeguranca;
	}

	/**
	 * Get: dsSistema.
	 *
	 * @return dsSistema
	 */
	public String getDsSistema() {
		return dsSistema;
	}

	/**
	 * Set: dsSistema.
	 *
	 * @param dsSistema the ds sistema
	 */
	public void setDsSistema(String dsSistema) {
		this.dsSistema = dsSistema;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: hrConsultasSaldoPagamento.
	 *
	 * @return hrConsultasSaldoPagamento
	 */
	public String getHrConsultasSaldoPagamento() {
		return hrConsultasSaldoPagamento;
	}

	/**
	 * Set: hrConsultasSaldoPagamento.
	 *
	 * @param hrConsultasSaldoPagamento the hr consultas saldo pagamento
	 */
	public void setHrConsultasSaldoPagamento(String hrConsultasSaldoPagamento) {
		this.hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
	}

	/**
	 * Get: hrConsultaFolhaPgto.
	 *
	 * @return hrConsultaFolhaPgto
	 */
	public String getHrConsultaFolhaPgto() {
		return hrConsultaFolhaPgto;
	}

	/**
	 * Set: hrConsultaFolhaPgto.
	 *
	 * @param hrConsultaFolhaPgto the hr consulta folha pgto
	 */
	public void setHrConsultaFolhaPgto(String hrConsultaFolhaPgto) {
		this.hrConsultaFolhaPgto = hrConsultaFolhaPgto;
	}

	/**
	 * Get: qtMinutosValorSuperior.
	 *
	 * @return qtMinutosValorSuperior
	 */
	public Integer getQtMinutosValorSuperior() {
		return qtMinutosValorSuperior;
	}

	/**
	 * Set: qtMinutosValorSuperior.
	 *
	 * @param qtMinutosValorSuperior the qt minutos valor superior
	 */
	public void setQtMinutosValorSuperior(Integer qtMinutosValorSuperior) {
		this.qtMinutosValorSuperior = qtMinutosValorSuperior;
	}

	/**
	 * Get: hrLimiteValorSuperior.
	 *
	 * @return hrLimiteValorSuperior
	 */
	public String getHrLimiteValorSuperior() {
		return hrLimiteValorSuperior;
	}

	/**
	 * Set: hrLimiteValorSuperior.
	 *
	 * @param hrLimiteValorSuperior the hr limite valor superior
	 */
	public void setHrLimiteValorSuperior(String hrLimiteValorSuperior) {
		this.hrLimiteValorSuperior = hrLimiteValorSuperior;
	}

    /**
     * Nome: getQtdTempoAgendamentoCobranca
     *
     * @return qtdTempoAgendamentoCobranca
     */
    public Integer getQtdTempoAgendamentoCobranca() {
        return qtdTempoAgendamentoCobranca;
    }

    /**
     * Nome: setQtdTempoAgendamentoCobranca
     *
     * @param qtdTempoAgendamentoCobranca
     */
    public void setQtdTempoAgendamentoCobranca(Integer qtdTempoAgendamentoCobranca) {
        this.qtdTempoAgendamentoCobranca = qtdTempoAgendamentoCobranca;
    }

    /**
     * Nome: getQtdTempoEfetivacaoCiclicaCobranca
     *
     * @return qtdTempoEfetivacaoCiclicaCobranca
     */
    public Integer getQtdTempoEfetivacaoCiclicaCobranca() {
        return qtdTempoEfetivacaoCiclicaCobranca;
    }

    /**
     * Nome: setQtdTempoEfetivacaoCiclicaCobranca
     *
     * @param qtdTempoEfetivacaoCiclicaCobranca
     */
    public void setQtdTempoEfetivacaoCiclicaCobranca(Integer qtdTempoEfetivacaoCiclicaCobranca) {
        this.qtdTempoEfetivacaoCiclicaCobranca = qtdTempoEfetivacaoCiclicaCobranca;
    }

    /**
     * Nome: getQtdTempoEfetivacaoDiariaCobranca
     *
     * @return qtdTempoEfetivacaoDiariaCobranca
     */
    public Integer getQtdTempoEfetivacaoDiariaCobranca() {
        return qtdTempoEfetivacaoDiariaCobranca;
    }

    /**
     * Nome: setQtdTempoEfetivacaoDiariaCobranca
     *
     * @param qtdTempoEfetivacaoDiariaCobranca
     */
    public void setQtdTempoEfetivacaoDiariaCobranca(Integer qtdTempoEfetivacaoDiariaCobranca) {
        this.qtdTempoEfetivacaoDiariaCobranca = qtdTempoEfetivacaoDiariaCobranca;
    }

	public Integer getQtdLimiteSemResposta() {
		return qtdLimiteSemResposta;
	}

	public void setQtdLimiteSemResposta(Integer qtdLimiteSemResposta) {
		this.qtdLimiteSemResposta = qtdLimiteSemResposta;
	}

	public Integer getQtdLimiteProcessamentoExcedido() {
		return qtdLimiteProcessamentoExcedido;
	}

	public void setQtdLimiteProcessamentoExcedido(
			Integer qtdLimiteProcessamentoExcedido) {
		this.qtdLimiteProcessamentoExcedido = qtdLimiteProcessamentoExcedido;
	}
    
    
}