/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarTercPartContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarTercPartContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _nrRastreaTerceiro
     */
    private int _nrRastreaTerceiro = 0;

    /**
     * keeps track of state for field: _nrRastreaTerceiro
     */
    private boolean _has_nrRastreaTerceiro;

    /**
     * Field _cdCpfCnpjTerceiro
     */
    private long _cdCpfCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjTerceiro
     */
    private boolean _has_cdCpfCnpjTerceiro;

    /**
     * Field _cdFilialCnpjTerceiro
     */
    private int _cdFilialCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjTerceiro
     */
    private boolean _has_cdFilialCnpjTerceiro;

    /**
     * Field _cdControleCnpjTerceiro
     */
    private int _cdControleCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdControleCnpjTerceiro
     */
    private boolean _has_cdControleCnpjTerceiro;

    /**
     * Field _cdIndicadorAgendaTitulo
     */
    private int _cdIndicadorAgendaTitulo = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaTitulo
     */
    private boolean _has_cdIndicadorAgendaTitulo;

    /**
     * Field _cdIndicadorNotaTerceiro
     */
    private int _cdIndicadorNotaTerceiro = 0;

    /**
     * keeps track of state for field: _cdIndicadorNotaTerceiro
     */
    private boolean _has_cdIndicadorNotaTerceiro;

    /**
     * Field _cdIndicadorPapeletaTerceiro
     */
    private int _cdIndicadorPapeletaTerceiro = 0;

    /**
     * keeps track of state for field: _cdIndicadorPapeletaTerceiro
     */
    private boolean _has_cdIndicadorPapeletaTerceiro;

    /**
     * Field _dtInicioRastreaTerceiro
     */
    private java.lang.String _dtInicioRastreaTerceiro;

    /**
     * Field _cdIndicadorBloqueioRastreabilidade
     */
    private java.lang.String _cdIndicadorBloqueioRastreabilidade;

    /**
     * Field _dtInicioPapeletaTerceiro
     */
    private java.lang.String _dtInicioPapeletaTerceiro;

    /**
     * Field _dsIndicadorBloqueioRastreabilidade
     */
    private java.lang.String _dsIndicadorBloqueioRastreabilidade;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarTercPartContratoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.response.ConsultarTercPartContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdControleCnpjTerceiro
     * 
     */
    public void deleteCdControleCnpjTerceiro()
    {
        this._has_cdControleCnpjTerceiro= false;
    } //-- void deleteCdControleCnpjTerceiro() 

    /**
     * Method deleteCdCpfCnpjTerceiro
     * 
     */
    public void deleteCdCpfCnpjTerceiro()
    {
        this._has_cdCpfCnpjTerceiro= false;
    } //-- void deleteCdCpfCnpjTerceiro() 

    /**
     * Method deleteCdFilialCnpjTerceiro
     * 
     */
    public void deleteCdFilialCnpjTerceiro()
    {
        this._has_cdFilialCnpjTerceiro= false;
    } //-- void deleteCdFilialCnpjTerceiro() 

    /**
     * Method deleteCdIndicadorAgendaTitulo
     * 
     */
    public void deleteCdIndicadorAgendaTitulo()
    {
        this._has_cdIndicadorAgendaTitulo= false;
    } //-- void deleteCdIndicadorAgendaTitulo() 

    /**
     * Method deleteCdIndicadorNotaTerceiro
     * 
     */
    public void deleteCdIndicadorNotaTerceiro()
    {
        this._has_cdIndicadorNotaTerceiro= false;
    } //-- void deleteCdIndicadorNotaTerceiro() 

    /**
     * Method deleteCdIndicadorPapeletaTerceiro
     * 
     */
    public void deleteCdIndicadorPapeletaTerceiro()
    {
        this._has_cdIndicadorPapeletaTerceiro= false;
    } //-- void deleteCdIndicadorPapeletaTerceiro() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrRastreaTerceiro
     * 
     */
    public void deleteNrRastreaTerceiro()
    {
        this._has_nrRastreaTerceiro= false;
    } //-- void deleteNrRastreaTerceiro() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdControleCnpjTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjTerceiro'.
     */
    public int getCdControleCnpjTerceiro()
    {
        return this._cdControleCnpjTerceiro;
    } //-- int getCdControleCnpjTerceiro() 

    /**
     * Returns the value of field 'cdCpfCnpjTerceiro'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjTerceiro'.
     */
    public long getCdCpfCnpjTerceiro()
    {
        return this._cdCpfCnpjTerceiro;
    } //-- long getCdCpfCnpjTerceiro() 

    /**
     * Returns the value of field 'cdFilialCnpjTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjTerceiro'.
     */
    public int getCdFilialCnpjTerceiro()
    {
        return this._cdFilialCnpjTerceiro;
    } //-- int getCdFilialCnpjTerceiro() 

    /**
     * Returns the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaTitulo'.
     */
    public int getCdIndicadorAgendaTitulo()
    {
        return this._cdIndicadorAgendaTitulo;
    } //-- int getCdIndicadorAgendaTitulo() 

    /**
     * Returns the value of field
     * 'cdIndicadorBloqueioRastreabilidade'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorBloqueioRastreabilidade'.
     */
    public java.lang.String getCdIndicadorBloqueioRastreabilidade()
    {
        return this._cdIndicadorBloqueioRastreabilidade;
    } //-- java.lang.String getCdIndicadorBloqueioRastreabilidade() 

    /**
     * Returns the value of field 'cdIndicadorNotaTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorNotaTerceiro'.
     */
    public int getCdIndicadorNotaTerceiro()
    {
        return this._cdIndicadorNotaTerceiro;
    } //-- int getCdIndicadorNotaTerceiro() 

    /**
     * Returns the value of field 'cdIndicadorPapeletaTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorPapeletaTerceiro'.
     */
    public int getCdIndicadorPapeletaTerceiro()
    {
        return this._cdIndicadorPapeletaTerceiro;
    } //-- int getCdIndicadorPapeletaTerceiro() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field
     * 'dsIndicadorBloqueioRastreabilidade'.
     * 
     * @return String
     * @return the value of field
     * 'dsIndicadorBloqueioRastreabilidade'.
     */
    public java.lang.String getDsIndicadorBloqueioRastreabilidade()
    {
        return this._dsIndicadorBloqueioRastreabilidade;
    } //-- java.lang.String getDsIndicadorBloqueioRastreabilidade() 

    /**
     * Returns the value of field 'dtInicioPapeletaTerceiro'.
     * 
     * @return String
     * @return the value of field 'dtInicioPapeletaTerceiro'.
     */
    public java.lang.String getDtInicioPapeletaTerceiro()
    {
        return this._dtInicioPapeletaTerceiro;
    } //-- java.lang.String getDtInicioPapeletaTerceiro() 

    /**
     * Returns the value of field 'dtInicioRastreaTerceiro'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreaTerceiro'.
     */
    public java.lang.String getDtInicioRastreaTerceiro()
    {
        return this._dtInicioRastreaTerceiro;
    } //-- java.lang.String getDtInicioRastreaTerceiro() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrRastreaTerceiro'.
     * 
     * @return int
     * @return the value of field 'nrRastreaTerceiro'.
     */
    public int getNrRastreaTerceiro()
    {
        return this._nrRastreaTerceiro;
    } //-- int getNrRastreaTerceiro() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdControleCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjTerceiro()
    {
        return this._has_cdControleCnpjTerceiro;
    } //-- boolean hasCdControleCnpjTerceiro() 

    /**
     * Method hasCdCpfCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjTerceiro()
    {
        return this._has_cdCpfCnpjTerceiro;
    } //-- boolean hasCdCpfCnpjTerceiro() 

    /**
     * Method hasCdFilialCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjTerceiro()
    {
        return this._has_cdFilialCnpjTerceiro;
    } //-- boolean hasCdFilialCnpjTerceiro() 

    /**
     * Method hasCdIndicadorAgendaTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaTitulo()
    {
        return this._has_cdIndicadorAgendaTitulo;
    } //-- boolean hasCdIndicadorAgendaTitulo() 

    /**
     * Method hasCdIndicadorNotaTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorNotaTerceiro()
    {
        return this._has_cdIndicadorNotaTerceiro;
    } //-- boolean hasCdIndicadorNotaTerceiro() 

    /**
     * Method hasCdIndicadorPapeletaTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorPapeletaTerceiro()
    {
        return this._has_cdIndicadorPapeletaTerceiro;
    } //-- boolean hasCdIndicadorPapeletaTerceiro() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrRastreaTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrRastreaTerceiro()
    {
        return this._has_nrRastreaTerceiro;
    } //-- boolean hasNrRastreaTerceiro() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdControleCnpjTerceiro'.
     * 
     * @param cdControleCnpjTerceiro the value of field
     * 'cdControleCnpjTerceiro'.
     */
    public void setCdControleCnpjTerceiro(int cdControleCnpjTerceiro)
    {
        this._cdControleCnpjTerceiro = cdControleCnpjTerceiro;
        this._has_cdControleCnpjTerceiro = true;
    } //-- void setCdControleCnpjTerceiro(int) 

    /**
     * Sets the value of field 'cdCpfCnpjTerceiro'.
     * 
     * @param cdCpfCnpjTerceiro the value of field
     * 'cdCpfCnpjTerceiro'.
     */
    public void setCdCpfCnpjTerceiro(long cdCpfCnpjTerceiro)
    {
        this._cdCpfCnpjTerceiro = cdCpfCnpjTerceiro;
        this._has_cdCpfCnpjTerceiro = true;
    } //-- void setCdCpfCnpjTerceiro(long) 

    /**
     * Sets the value of field 'cdFilialCnpjTerceiro'.
     * 
     * @param cdFilialCnpjTerceiro the value of field
     * 'cdFilialCnpjTerceiro'.
     */
    public void setCdFilialCnpjTerceiro(int cdFilialCnpjTerceiro)
    {
        this._cdFilialCnpjTerceiro = cdFilialCnpjTerceiro;
        this._has_cdFilialCnpjTerceiro = true;
    } //-- void setCdFilialCnpjTerceiro(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @param cdIndicadorAgendaTitulo the value of field
     * 'cdIndicadorAgendaTitulo'.
     */
    public void setCdIndicadorAgendaTitulo(int cdIndicadorAgendaTitulo)
    {
        this._cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
        this._has_cdIndicadorAgendaTitulo = true;
    } //-- void setCdIndicadorAgendaTitulo(int) 

    /**
     * Sets the value of field
     * 'cdIndicadorBloqueioRastreabilidade'.
     * 
     * @param cdIndicadorBloqueioRastreabilidade the value of field
     * 'cdIndicadorBloqueioRastreabilidade'.
     */
    public void setCdIndicadorBloqueioRastreabilidade(java.lang.String cdIndicadorBloqueioRastreabilidade)
    {
        this._cdIndicadorBloqueioRastreabilidade = cdIndicadorBloqueioRastreabilidade;
    } //-- void setCdIndicadorBloqueioRastreabilidade(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorNotaTerceiro'.
     * 
     * @param cdIndicadorNotaTerceiro the value of field
     * 'cdIndicadorNotaTerceiro'.
     */
    public void setCdIndicadorNotaTerceiro(int cdIndicadorNotaTerceiro)
    {
        this._cdIndicadorNotaTerceiro = cdIndicadorNotaTerceiro;
        this._has_cdIndicadorNotaTerceiro = true;
    } //-- void setCdIndicadorNotaTerceiro(int) 

    /**
     * Sets the value of field 'cdIndicadorPapeletaTerceiro'.
     * 
     * @param cdIndicadorPapeletaTerceiro the value of field
     * 'cdIndicadorPapeletaTerceiro'.
     */
    public void setCdIndicadorPapeletaTerceiro(int cdIndicadorPapeletaTerceiro)
    {
        this._cdIndicadorPapeletaTerceiro = cdIndicadorPapeletaTerceiro;
        this._has_cdIndicadorPapeletaTerceiro = true;
    } //-- void setCdIndicadorPapeletaTerceiro(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsIndicadorBloqueioRastreabilidade'.
     * 
     * @param dsIndicadorBloqueioRastreabilidade the value of field
     * 'dsIndicadorBloqueioRastreabilidade'.
     */
    public void setDsIndicadorBloqueioRastreabilidade(java.lang.String dsIndicadorBloqueioRastreabilidade)
    {
        this._dsIndicadorBloqueioRastreabilidade = dsIndicadorBloqueioRastreabilidade;
    } //-- void setDsIndicadorBloqueioRastreabilidade(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioPapeletaTerceiro'.
     * 
     * @param dtInicioPapeletaTerceiro the value of field
     * 'dtInicioPapeletaTerceiro'.
     */
    public void setDtInicioPapeletaTerceiro(java.lang.String dtInicioPapeletaTerceiro)
    {
        this._dtInicioPapeletaTerceiro = dtInicioPapeletaTerceiro;
    } //-- void setDtInicioPapeletaTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreaTerceiro'.
     * 
     * @param dtInicioRastreaTerceiro the value of field
     * 'dtInicioRastreaTerceiro'.
     */
    public void setDtInicioRastreaTerceiro(java.lang.String dtInicioRastreaTerceiro)
    {
        this._dtInicioRastreaTerceiro = dtInicioRastreaTerceiro;
    } //-- void setDtInicioRastreaTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrRastreaTerceiro'.
     * 
     * @param nrRastreaTerceiro the value of field
     * 'nrRastreaTerceiro'.
     */
    public void setNrRastreaTerceiro(int nrRastreaTerceiro)
    {
        this._nrRastreaTerceiro = nrRastreaTerceiro;
        this._has_nrRastreaTerceiro = true;
    } //-- void setNrRastreaTerceiro(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarTercPartContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.response.ConsultarTercPartContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.response.ConsultarTercPartContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.response.ConsultarTercPartContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.response.ConsultarTercPartContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
