/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMotivoBloqueioDesbSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoBloqueioDesbSaidaDTO {
	
	/** Atributo cdMotivoSituacaoParticipante. */
	private Integer cdMotivoSituacaoParticipante;
	
	/** Atributo dsMotivoSituacaoParticipante. */
	private String dsMotivoSituacaoParticipante;
	
	/**
	 * Listar motivo bloqueio desb saida dto.
	 *
	 * @param cdMotivoSituacaoParticipante the cd motivo situacao participante
	 * @param dsMotivoSituacaoParticipante the ds motivo situacao participante
	 */
	public ListarMotivoBloqueioDesbSaidaDTO(Integer cdMotivoSituacaoParticipante, String dsMotivoSituacaoParticipante) {
		super();
		this.cdMotivoSituacaoParticipante = cdMotivoSituacaoParticipante;
		this.dsMotivoSituacaoParticipante = dsMotivoSituacaoParticipante;
	}
	
	/**
	 * Get: cdMotivoSituacaoParticipante.
	 *
	 * @return cdMotivoSituacaoParticipante
	 */
	public Integer getCdMotivoSituacaoParticipante() {
		return cdMotivoSituacaoParticipante;
	}
	
	/**
	 * Set: cdMotivoSituacaoParticipante.
	 *
	 * @param cdMotivoSituacaoParticipante the cd motivo situacao participante
	 */
	public void setCdMotivoSituacaoParticipante(Integer cdMotivoSituacaoParticipante) {
		this.cdMotivoSituacaoParticipante = cdMotivoSituacaoParticipante;
	}
	
	/**
	 * Get: dsMotivoSituacaoParticipante.
	 *
	 * @return dsMotivoSituacaoParticipante
	 */
	public String getDsMotivoSituacaoParticipante() {
		return dsMotivoSituacaoParticipante;
	}
	
	/**
	 * Set: dsMotivoSituacaoParticipante.
	 *
	 * @param dsMotivoSituacaoParticipante the ds motivo situacao participante
	 */
	public void setDsMotivoSituacaoParticipante(String dsMotivoSituacaoParticipante) {
		this.dsMotivoSituacaoParticipante = dsMotivoSituacaoParticipante;
	}
	
	
}
