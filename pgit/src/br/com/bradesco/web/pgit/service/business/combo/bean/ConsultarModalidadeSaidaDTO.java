/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ConsultarModalidadeSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarModalidadeSaidaDTO {
	
	/** Atributo cdProdutoOperacaoRelacionada. */
	private Integer cdProdutoOperacaoRelacionada;
	
	/** Atributo dsProdutoOperacaoRelacionada. */
	private String dsProdutoOperacaoRelacionada;
	
	/** Atributo cdRelacionamentoProdutoProduto. */
	private Integer cdRelacionamentoProdutoProduto;
	
	/**
	 * Consultar modalidade saida dto.
	 *
	 * @param cdProdutoOperacaoRelacionada the cd produto operacao relacionada
	 * @param dsProdutoOperacaoRelacionada the ds produto operacao relacionada
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public ConsultarModalidadeSaidaDTO(Integer cdProdutoOperacaoRelacionada, String dsProdutoOperacaoRelacionada, Integer cdRelacionamentoProdutoProduto){
		this.cdProdutoOperacaoRelacionada = cdProdutoOperacaoRelacionada;
		this.dsProdutoOperacaoRelacionada = dsProdutoOperacaoRelacionada;
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionada.
	 *
	 * @return cdProdutoOperacaoRelacionada
	 */
	public Integer getCdProdutoOperacaoRelacionada() {
		return cdProdutoOperacaoRelacionada;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionada.
	 *
	 * @param cdProdutoOperacaoRelacionada the cd produto operacao relacionada
	 */
	public void setCdProdutoOperacaoRelacionada(Integer cdProdutoOperacaoRelacionada) {
		this.cdProdutoOperacaoRelacionada = cdProdutoOperacaoRelacionada;
	}
	
	/**
	 * Get: cdRelacionamentoProdutoProduto.
	 *
	 * @return cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProdutoProduto.
	 *
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionada.
	 *
	 * @return dsProdutoOperacaoRelacionada
	 */
	public String getDsProdutoOperacaoRelacionada() {
		return dsProdutoOperacaoRelacionada;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionada.
	 *
	 * @param dsProdutoOperacaoRelacionada the ds produto operacao relacionada
	 */
	public void setDsProdutoOperacaoRelacionada(String dsProdutoOperacaoRelacionada) {
		this.dsProdutoOperacaoRelacionada = dsProdutoOperacaoRelacionada;
	}
}
