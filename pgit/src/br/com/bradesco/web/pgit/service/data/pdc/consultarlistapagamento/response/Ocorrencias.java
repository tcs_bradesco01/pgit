/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _nrPagamento
     */
    private java.lang.String _nrPagamento;

    /**
     * Field _vlrPagamento
     */
    private java.math.BigDecimal _vlrPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdCorpoCpfCnpjFavorecido
     */
    private long _cdCorpoCpfCnpjFavorecido = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpjFavorecido
     */
    private boolean _has_cdCorpoCpfCnpjFavorecido;

    /**
     * Field _cdFilialCpfCnpjFavorecido
     */
    private int _cdFilialCpfCnpjFavorecido = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjFavorecido
     */
    private boolean _has_cdFilialCpfCnpjFavorecido;

    /**
     * Field _cdControleCpfCnpjFavorecido
     */
    private int _cdControleCpfCnpjFavorecido = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpjFavorecido
     */
    private boolean _has_cdControleCpfCnpjFavorecido;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _dsBeneficiario
     */
    private java.lang.String _dsBeneficiario;

    /**
     * Field _cdBancoFavorecido
     */
    private int _cdBancoFavorecido = 0;

    /**
     * keeps track of state for field: _cdBancoFavorecido
     */
    private boolean _has_cdBancoFavorecido;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgenciaFavorecido
     */
    private int _cdAgenciaFavorecido = 0;

    /**
     * keeps track of state for field: _cdAgenciaFavorecido
     */
    private boolean _has_cdAgenciaFavorecido;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdContaFavorecido
     */
    private long _cdContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdContaFavorecido
     */
    private boolean _has_cdContaFavorecido;

    /**
     * Field _cdDigitoContaFavorecido
     */
    private java.lang.String _cdDigitoContaFavorecido;

    /**
     * Field _cdTipoTela
     */
    private int _cdTipoTela = 0;

    /**
     * keeps track of state for field: _cdTipoTela
     */
    private boolean _has_cdTipoTela;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlrPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaFavorecido
     * 
     */
    public void deleteCdAgenciaFavorecido()
    {
        this._has_cdAgenciaFavorecido= false;
    } //-- void deleteCdAgenciaFavorecido() 

    /**
     * Method deleteCdBancoFavorecido
     * 
     */
    public void deleteCdBancoFavorecido()
    {
        this._has_cdBancoFavorecido= false;
    } //-- void deleteCdBancoFavorecido() 

    /**
     * Method deleteCdContaFavorecido
     * 
     */
    public void deleteCdContaFavorecido()
    {
        this._has_cdContaFavorecido= false;
    } //-- void deleteCdContaFavorecido() 

    /**
     * Method deleteCdControleCpfCnpjFavorecido
     * 
     */
    public void deleteCdControleCpfCnpjFavorecido()
    {
        this._has_cdControleCpfCnpjFavorecido= false;
    } //-- void deleteCdControleCpfCnpjFavorecido() 

    /**
     * Method deleteCdCorpoCpfCnpjFavorecido
     * 
     */
    public void deleteCdCorpoCpfCnpjFavorecido()
    {
        this._has_cdCorpoCpfCnpjFavorecido= false;
    } //-- void deleteCdCorpoCpfCnpjFavorecido() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdFilialCpfCnpjFavorecido
     * 
     */
    public void deleteCdFilialCpfCnpjFavorecido()
    {
        this._has_cdFilialCpfCnpjFavorecido= false;
    } //-- void deleteCdFilialCpfCnpjFavorecido() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoTela
     * 
     */
    public void deleteCdTipoTela()
    {
        this._has_cdTipoTela= false;
    } //-- void deleteCdTipoTela() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaFavorecido'.
     */
    public int getCdAgenciaFavorecido()
    {
        return this._cdAgenciaFavorecido;
    } //-- int getCdAgenciaFavorecido() 

    /**
     * Returns the value of field 'cdBancoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdBancoFavorecido'.
     */
    public int getCdBancoFavorecido()
    {
        return this._cdBancoFavorecido;
    } //-- int getCdBancoFavorecido() 

    /**
     * Returns the value of field 'cdContaFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdContaFavorecido'.
     */
    public long getCdContaFavorecido()
    {
        return this._cdContaFavorecido;
    } //-- long getCdContaFavorecido() 

    /**
     * Returns the value of field 'cdControleCpfCnpjFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpjFavorecido'.
     */
    public int getCdControleCpfCnpjFavorecido()
    {
        return this._cdControleCpfCnpjFavorecido;
    } //-- int getCdControleCpfCnpjFavorecido() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpjFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpjFavorecido'.
     */
    public long getCdCorpoCpfCnpjFavorecido()
    {
        return this._cdCorpoCpfCnpjFavorecido;
    } //-- long getCdCorpoCpfCnpjFavorecido() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoContaFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaFavorecido'.
     */
    public java.lang.String getCdDigitoContaFavorecido()
    {
        return this._cdDigitoContaFavorecido;
    } //-- java.lang.String getCdDigitoContaFavorecido() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjFavorecido'.
     */
    public int getCdFilialCpfCnpjFavorecido()
    {
        return this._cdFilialCpfCnpjFavorecido;
    } //-- int getCdFilialCpfCnpjFavorecido() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoTela'.
     * 
     * @return int
     * @return the value of field 'cdTipoTela'.
     */
    public int getCdTipoTela()
    {
        return this._cdTipoTela;
    } //-- int getCdTipoTela() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsBeneficiario'.
     * 
     * @return String
     * @return the value of field 'dsBeneficiario'.
     */
    public java.lang.String getDsBeneficiario()
    {
        return this._dsBeneficiario;
    } //-- java.lang.String getDsBeneficiario() 

    /**
     * Returns the value of field 'nrPagamento'.
     * 
     * @return String
     * @return the value of field 'nrPagamento'.
     */
    public java.lang.String getNrPagamento()
    {
        return this._nrPagamento;
    } //-- java.lang.String getNrPagamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'vlrPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrPagamento'.
     */
    public java.math.BigDecimal getVlrPagamento()
    {
        return this._vlrPagamento;
    } //-- java.math.BigDecimal getVlrPagamento() 

    /**
     * Method hasCdAgenciaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaFavorecido()
    {
        return this._has_cdAgenciaFavorecido;
    } //-- boolean hasCdAgenciaFavorecido() 

    /**
     * Method hasCdBancoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoFavorecido()
    {
        return this._has_cdBancoFavorecido;
    } //-- boolean hasCdBancoFavorecido() 

    /**
     * Method hasCdContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaFavorecido()
    {
        return this._has_cdContaFavorecido;
    } //-- boolean hasCdContaFavorecido() 

    /**
     * Method hasCdControleCpfCnpjFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpjFavorecido()
    {
        return this._has_cdControleCpfCnpjFavorecido;
    } //-- boolean hasCdControleCpfCnpjFavorecido() 

    /**
     * Method hasCdCorpoCpfCnpjFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpjFavorecido()
    {
        return this._has_cdCorpoCpfCnpjFavorecido;
    } //-- boolean hasCdCorpoCpfCnpjFavorecido() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdFilialCpfCnpjFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjFavorecido()
    {
        return this._has_cdFilialCpfCnpjFavorecido;
    } //-- boolean hasCdFilialCpfCnpjFavorecido() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoTela()
    {
        return this._has_cdTipoTela;
    } //-- boolean hasCdTipoTela() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaFavorecido'.
     * 
     * @param cdAgenciaFavorecido the value of field
     * 'cdAgenciaFavorecido'.
     */
    public void setCdAgenciaFavorecido(int cdAgenciaFavorecido)
    {
        this._cdAgenciaFavorecido = cdAgenciaFavorecido;
        this._has_cdAgenciaFavorecido = true;
    } //-- void setCdAgenciaFavorecido(int) 

    /**
     * Sets the value of field 'cdBancoFavorecido'.
     * 
     * @param cdBancoFavorecido the value of field
     * 'cdBancoFavorecido'.
     */
    public void setCdBancoFavorecido(int cdBancoFavorecido)
    {
        this._cdBancoFavorecido = cdBancoFavorecido;
        this._has_cdBancoFavorecido = true;
    } //-- void setCdBancoFavorecido(int) 

    /**
     * Sets the value of field 'cdContaFavorecido'.
     * 
     * @param cdContaFavorecido the value of field
     * 'cdContaFavorecido'.
     */
    public void setCdContaFavorecido(long cdContaFavorecido)
    {
        this._cdContaFavorecido = cdContaFavorecido;
        this._has_cdContaFavorecido = true;
    } //-- void setCdContaFavorecido(long) 

    /**
     * Sets the value of field 'cdControleCpfCnpjFavorecido'.
     * 
     * @param cdControleCpfCnpjFavorecido the value of field
     * 'cdControleCpfCnpjFavorecido'.
     */
    public void setCdControleCpfCnpjFavorecido(int cdControleCpfCnpjFavorecido)
    {
        this._cdControleCpfCnpjFavorecido = cdControleCpfCnpjFavorecido;
        this._has_cdControleCpfCnpjFavorecido = true;
    } //-- void setCdControleCpfCnpjFavorecido(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpjFavorecido'.
     * 
     * @param cdCorpoCpfCnpjFavorecido the value of field
     * 'cdCorpoCpfCnpjFavorecido'.
     */
    public void setCdCorpoCpfCnpjFavorecido(long cdCorpoCpfCnpjFavorecido)
    {
        this._cdCorpoCpfCnpjFavorecido = cdCorpoCpfCnpjFavorecido;
        this._has_cdCorpoCpfCnpjFavorecido = true;
    } //-- void setCdCorpoCpfCnpjFavorecido(long) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoContaFavorecido'.
     * 
     * @param cdDigitoContaFavorecido the value of field
     * 'cdDigitoContaFavorecido'.
     */
    public void setCdDigitoContaFavorecido(java.lang.String cdDigitoContaFavorecido)
    {
        this._cdDigitoContaFavorecido = cdDigitoContaFavorecido;
    } //-- void setCdDigitoContaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjFavorecido'.
     * 
     * @param cdFilialCpfCnpjFavorecido the value of field
     * 'cdFilialCpfCnpjFavorecido'.
     */
    public void setCdFilialCpfCnpjFavorecido(int cdFilialCpfCnpjFavorecido)
    {
        this._cdFilialCpfCnpjFavorecido = cdFilialCpfCnpjFavorecido;
        this._has_cdFilialCpfCnpjFavorecido = true;
    } //-- void setCdFilialCpfCnpjFavorecido(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoTela'.
     * 
     * @param cdTipoTela the value of field 'cdTipoTela'.
     */
    public void setCdTipoTela(int cdTipoTela)
    {
        this._cdTipoTela = cdTipoTela;
        this._has_cdTipoTela = true;
    } //-- void setCdTipoTela(int) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsBeneficiario'.
     * 
     * @param dsBeneficiario the value of field 'dsBeneficiario'.
     */
    public void setDsBeneficiario(java.lang.String dsBeneficiario)
    {
        this._dsBeneficiario = dsBeneficiario;
    } //-- void setDsBeneficiario(java.lang.String) 

    /**
     * Sets the value of field 'nrPagamento'.
     * 
     * @param nrPagamento the value of field 'nrPagamento'.
     */
    public void setNrPagamento(java.lang.String nrPagamento)
    {
        this._nrPagamento = nrPagamento;
    } //-- void setNrPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'vlrPagamento'.
     * 
     * @param vlrPagamento the value of field 'vlrPagamento'.
     */
    public void setVlrPagamento(java.math.BigDecimal vlrPagamento)
    {
        this._vlrPagamento = vlrPagamento;
    } //-- void setVlrPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
