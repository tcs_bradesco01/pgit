/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.impdiaad.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ImpdiaadRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ImpdiaadRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _idDocumentoGerado
     */
    private long _idDocumentoGerado = 0;

    /**
     * keeps track of state for field: _idDocumentoGerado
     */
    private boolean _has_idDocumentoGerado;

    /**
     * Field _paginaDocumento
     */
    private int _paginaDocumento = 0;

    /**
     * keeps track of state for field: _paginaDocumento
     */
    private boolean _has_paginaDocumento;

    /**
     * Field _tipoImpressao
     */
    private java.lang.String _tipoImpressao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImpdiaadRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.impdiaad.request.ImpdiaadRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteIdDocumentoGerado
     * 
     */
    public void deleteIdDocumentoGerado()
    {
        this._has_idDocumentoGerado= false;
    } //-- void deleteIdDocumentoGerado() 

    /**
     * Method deletePaginaDocumento
     * 
     */
    public void deletePaginaDocumento()
    {
        this._has_paginaDocumento= false;
    } //-- void deletePaginaDocumento() 

    /**
     * Returns the value of field 'idDocumentoGerado'.
     * 
     * @return long
     * @return the value of field 'idDocumentoGerado'.
     */
    public long getIdDocumentoGerado()
    {
        return this._idDocumentoGerado;
    } //-- long getIdDocumentoGerado() 

    /**
     * Returns the value of field 'paginaDocumento'.
     * 
     * @return int
     * @return the value of field 'paginaDocumento'.
     */
    public int getPaginaDocumento()
    {
        return this._paginaDocumento;
    } //-- int getPaginaDocumento() 

    /**
     * Returns the value of field 'tipoImpressao'.
     * 
     * @return String
     * @return the value of field 'tipoImpressao'.
     */
    public java.lang.String getTipoImpressao()
    {
        return this._tipoImpressao;
    } //-- java.lang.String getTipoImpressao() 

    /**
     * Method hasIdDocumentoGerado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasIdDocumentoGerado()
    {
        return this._has_idDocumentoGerado;
    } //-- boolean hasIdDocumentoGerado() 

    /**
     * Method hasPaginaDocumento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPaginaDocumento()
    {
        return this._has_paginaDocumento;
    } //-- boolean hasPaginaDocumento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'idDocumentoGerado'.
     * 
     * @param idDocumentoGerado the value of field
     * 'idDocumentoGerado'.
     */
    public void setIdDocumentoGerado(long idDocumentoGerado)
    {
        this._idDocumentoGerado = idDocumentoGerado;
        this._has_idDocumentoGerado = true;
    } //-- void setIdDocumentoGerado(long) 

    /**
     * Sets the value of field 'paginaDocumento'.
     * 
     * @param paginaDocumento the value of field 'paginaDocumento'.
     */
    public void setPaginaDocumento(int paginaDocumento)
    {
        this._paginaDocumento = paginaDocumento;
        this._has_paginaDocumento = true;
    } //-- void setPaginaDocumento(int) 

    /**
     * Sets the value of field 'tipoImpressao'.
     * 
     * @param tipoImpressao the value of field 'tipoImpressao'.
     */
    public void setTipoImpressao(java.lang.String tipoImpressao)
    {
        this._tipoImpressao = tipoImpressao;
    } //-- void setTipoImpressao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ImpdiaadRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.impdiaad.request.ImpdiaadRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.impdiaad.request.ImpdiaadRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.impdiaad.request.ImpdiaadRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.impdiaad.request.ImpdiaadRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
