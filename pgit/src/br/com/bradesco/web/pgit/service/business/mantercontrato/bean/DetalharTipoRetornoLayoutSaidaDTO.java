/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: DetalharTipoRetornoLayoutSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharTipoRetornoLayoutSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoGeracaoRetorno. */
	private Integer cdTipoGeracaoRetorno;
	
	/** Atributo cdTipoOcorrenciaRetorno. */
	private Integer cdTipoOcorrenciaRetorno;
	
	/** Atributo cdTipoMontaRetorno. */
	private Integer cdTipoMontaRetorno;
	
	/** Atributo cdTipoClassificacaoRetorno. */
	private Integer cdTipoClassificacaoRetorno;
	
	/** Atributo cdTipoOrdemRegistroRetorno. */
	private Integer cdTipoOrdemRegistroRetorno;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExter. */
	private String cdUsuarioInclusaoExter;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoExter. */
	private String cdUsuarioManutencaoExter;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;
	
	/** Atributo dsTipoGeracaoRetorno. */
	private String dsTipoGeracaoRetorno;
	
	/** Atributo dsTipoOcorrenciaRetorno. */
	private String dsTipoOcorrenciaRetorno;
	
	/** Atributo dsTipoMontaRetorno. */
	private String dsTipoMontaRetorno;
	
	/** Atributo dsTipoClassificacaoRetorno. */
	private String dsTipoClassificacaoRetorno;
	
	/** Atributo dsTipoOrdemRegistroRetorno. */
	private String dsTipoOrdemRegistroRetorno;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoClassificacaoRetorno.
	 *
	 * @return cdTipoClassificacaoRetorno
	 */
	public Integer getCdTipoClassificacaoRetorno() {
		return cdTipoClassificacaoRetorno;
	}
	
	/**
	 * Set: cdTipoClassificacaoRetorno.
	 *
	 * @param cdTipoClassificacaoRetorno the cd tipo classificacao retorno
	 */
	public void setCdTipoClassificacaoRetorno(Integer cdTipoClassificacaoRetorno) {
		this.cdTipoClassificacaoRetorno = cdTipoClassificacaoRetorno;
	}
	
	/**
	 * Get: cdTipoGeracaoRetorno.
	 *
	 * @return cdTipoGeracaoRetorno
	 */
	public Integer getCdTipoGeracaoRetorno() {
		return cdTipoGeracaoRetorno;
	}
	
	/**
	 * Set: cdTipoGeracaoRetorno.
	 *
	 * @param cdTipoGeracaoRetorno the cd tipo geracao retorno
	 */
	public void setCdTipoGeracaoRetorno(Integer cdTipoGeracaoRetorno) {
		this.cdTipoGeracaoRetorno = cdTipoGeracaoRetorno;
	}
	
	/**
	 * Get: cdTipoMontaRetorno.
	 *
	 * @return cdTipoMontaRetorno
	 */
	public Integer getCdTipoMontaRetorno() {
		return cdTipoMontaRetorno;
	}
	
	/**
	 * Set: cdTipoMontaRetorno.
	 *
	 * @param cdTipoMontaRetorno the cd tipo monta retorno
	 */
	public void setCdTipoMontaRetorno(Integer cdTipoMontaRetorno) {
		this.cdTipoMontaRetorno = cdTipoMontaRetorno;
	}
	
	/**
	 * Get: cdTipoOcorrenciaRetorno.
	 *
	 * @return cdTipoOcorrenciaRetorno
	 */
	public Integer getCdTipoOcorrenciaRetorno() {
		return cdTipoOcorrenciaRetorno;
	}
	
	/**
	 * Set: cdTipoOcorrenciaRetorno.
	 *
	 * @param cdTipoOcorrenciaRetorno the cd tipo ocorrencia retorno
	 */
	public void setCdTipoOcorrenciaRetorno(Integer cdTipoOcorrenciaRetorno) {
		this.cdTipoOcorrenciaRetorno = cdTipoOcorrenciaRetorno;
	}
	
	/**
	 * Get: cdTipoOrdemRegistroRetorno.
	 *
	 * @return cdTipoOrdemRegistroRetorno
	 */
	public Integer getCdTipoOrdemRegistroRetorno() {
		return cdTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Set: cdTipoOrdemRegistroRetorno.
	 *
	 * @param cdTipoOrdemRegistroRetorno the cd tipo ordem registro retorno
	 */
	public void setCdTipoOrdemRegistroRetorno(Integer cdTipoOrdemRegistroRetorno) {
		this.cdTipoOrdemRegistroRetorno = cdTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExter.
	 *
	 * @return cdUsuarioInclusaoExter
	 */
	public String getCdUsuarioInclusaoExter() {
		return cdUsuarioInclusaoExter;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExter.
	 *
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 */
	public void setCdUsuarioInclusaoExter(String cdUsuarioInclusaoExter) {
		this.cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExter.
	 *
	 * @return cdUsuarioManutencaoExter
	 */
	public String getCdUsuarioManutencaoExter() {
		return cdUsuarioManutencaoExter;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExter.
	 *
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 */
	public void setCdUsuarioManutencaoExter(String cdUsuarioManutencaoExter) {
		this.cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: dsTipoClassificacaoRetorno.
	 *
	 * @return dsTipoClassificacaoRetorno
	 */
	public String getDsTipoClassificacaoRetorno() {
		return dsTipoClassificacaoRetorno;
	}
	
	/**
	 * Set: dsTipoClassificacaoRetorno.
	 *
	 * @param dsTipoClassificacaoRetorno the ds tipo classificacao retorno
	 */
	public void setDsTipoClassificacaoRetorno(String dsTipoClassificacaoRetorno) {
		this.dsTipoClassificacaoRetorno = dsTipoClassificacaoRetorno;
	}
	
	/**
	 * Get: dsTipoGeracaoRetorno.
	 *
	 * @return dsTipoGeracaoRetorno
	 */
	public String getDsTipoGeracaoRetorno() {
		return dsTipoGeracaoRetorno;
	}
	
	/**
	 * Set: dsTipoGeracaoRetorno.
	 *
	 * @param dsTipoGeracaoRetorno the ds tipo geracao retorno
	 */
	public void setDsTipoGeracaoRetorno(String dsTipoGeracaoRetorno) {
		this.dsTipoGeracaoRetorno = dsTipoGeracaoRetorno;
	}
	
	/**
	 * Get: dsTipoMontaRetorno.
	 *
	 * @return dsTipoMontaRetorno
	 */
	public String getDsTipoMontaRetorno() {
		return dsTipoMontaRetorno;
	}
	
	/**
	 * Set: dsTipoMontaRetorno.
	 *
	 * @param dsTipoMontaRetorno the ds tipo monta retorno
	 */
	public void setDsTipoMontaRetorno(String dsTipoMontaRetorno) {
		this.dsTipoMontaRetorno = dsTipoMontaRetorno;
	}
	
	/**
	 * Get: dsTipoOcorrenciaRetorno.
	 *
	 * @return dsTipoOcorrenciaRetorno
	 */
	public String getDsTipoOcorrenciaRetorno() {
		return dsTipoOcorrenciaRetorno;
	}
	
	/**
	 * Set: dsTipoOcorrenciaRetorno.
	 *
	 * @param dsTipoOcorrenciaRetorno the ds tipo ocorrencia retorno
	 */
	public void setDsTipoOcorrenciaRetorno(String dsTipoOcorrenciaRetorno) {
		this.dsTipoOcorrenciaRetorno = dsTipoOcorrenciaRetorno;
	}
	
	/**
	 * Get: dsTipoOrdemRegistroRetorno.
	 *
	 * @return dsTipoOrdemRegistroRetorno
	 */
	public String getDsTipoOrdemRegistroRetorno() {
		return dsTipoOrdemRegistroRetorno;
	}
	
	/**
	 * Set: dsTipoOrdemRegistroRetorno.
	 *
	 * @param dsTipoOrdemRegistroRetorno the ds tipo ordem registro retorno
	 */
	public void setDsTipoOrdemRegistroRetorno(String dsTipoOrdemRegistroRetorno) {
		this.dsTipoOrdemRegistroRetorno = dsTipoOrdemRegistroRetorno;
	}
	
	
}
