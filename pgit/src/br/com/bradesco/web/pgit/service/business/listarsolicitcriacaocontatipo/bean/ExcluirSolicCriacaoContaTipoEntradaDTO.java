/*
 * Nome: br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean;

/**
 * Nome: ExcluirSolicCriacaoContaTipoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirSolicCriacaoContaTipoEntradaDTO {

	/** Atributo nrSolicitContaDesp. */
	private Long nrSolicitContaDesp;
    
    /** Atributo cdPessoa. */
    private Long cdPessoa;
    
    /** Atributo cdCpfCnpjPssoa. */
    private Long cdCpfCnpjPssoa;
    
    /** Atributo cdFilialCnpjPssoa. */
    private Integer cdFilialCnpjPssoa;
    
    /** Atributo cdControleCnpjPssoa. */
    private Integer cdControleCnpjPssoa;
    
	/**
	 * Get: cdControleCnpjPssoa.
	 *
	 * @return cdControleCnpjPssoa
	 */
	public Integer getCdControleCnpjPssoa() {
		return cdControleCnpjPssoa;
	}
	
	/**
	 * Set: cdControleCnpjPssoa.
	 *
	 * @param cdControleCnpjPssoa the cd controle cnpj pssoa
	 */
	public void setCdControleCnpjPssoa(Integer cdControleCnpjPssoa) {
		this.cdControleCnpjPssoa = cdControleCnpjPssoa;
	}
	
	/**
	 * Get: cdCpfCnpjPssoa.
	 *
	 * @return cdCpfCnpjPssoa
	 */
	public Long getCdCpfCnpjPssoa() {
		return cdCpfCnpjPssoa;
	}
	
	/**
	 * Set: cdCpfCnpjPssoa.
	 *
	 * @param cdCpfCnpjPssoa the cd cpf cnpj pssoa
	 */
	public void setCdCpfCnpjPssoa(Long cdCpfCnpjPssoa) {
		this.cdCpfCnpjPssoa = cdCpfCnpjPssoa;
	}
	
	/**
	 * Get: cdFilialCnpjPssoa.
	 *
	 * @return cdFilialCnpjPssoa
	 */
	public Integer getCdFilialCnpjPssoa() {
		return cdFilialCnpjPssoa;
	}
	
	/**
	 * Set: cdFilialCnpjPssoa.
	 *
	 * @param cdFilialCnpjPssoa the cd filial cnpj pssoa
	 */
	public void setCdFilialCnpjPssoa(Integer cdFilialCnpjPssoa) {
		this.cdFilialCnpjPssoa = cdFilialCnpjPssoa;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: nrSolicitContaDesp.
	 *
	 * @return nrSolicitContaDesp
	 */
	public Long getNrSolicitContaDesp() {
		return nrSolicitContaDesp;
	}
	
	/**
	 * Set: nrSolicitContaDesp.
	 *
	 * @param nrSolicitContaDesp the nr solicit conta desp
	 */
	public void setNrSolicitContaDesp(Long nrSolicitContaDesp) {
		this.nrSolicitContaDesp = nrSolicitContaDesp;
	}

}