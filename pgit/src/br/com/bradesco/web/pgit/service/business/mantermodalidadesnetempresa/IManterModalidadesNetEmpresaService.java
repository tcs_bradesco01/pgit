/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.AlterarHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.AlterarHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.DetalharHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.DetalharHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ExcluirHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ExcluirHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ListarHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ListarHorarioLimiteSaidaDTO;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterOperacoesServRelacionado
 * </p>.
 *
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterModalidadesNetEmpresaService {
	
	 
    /**
     * Listar funcoes alcadas.
     *
     * @param listarHorarioLimiteEntradaDTO the listar horario limite entrada dto
     * @return the list< listar funcoes alcada saida dt o>
     */
    ListarHorarioLimiteSaidaDTO listarHorarioLimite(ListarHorarioLimiteEntradaDTO listarHorarioLimiteEntradaDTO);
    
    /**
     * Detalhar horario limite.
     *
     * @param detalharHorarioLimiteEntradaDTO the detalhar horario limite entrada dto
     * @return the detalhar horario limite saida dto
     */
    DetalharHorarioLimiteSaidaDTO detalharHorarioLimite(DetalharHorarioLimiteEntradaDTO detalharHorarioLimiteEntradaDTO);
    
    /**
     * Alterar horario limite.
     *
     * @param alterarHorarioLimiteEntradaDTO the alterar horario limite entrada dto
     * @return the alterar horario limite saida dto
     */
    AlterarHorarioLimiteSaidaDTO alterarHorarioLimite(AlterarHorarioLimiteEntradaDTO alterarHorarioLimiteEntradaDTO);
    
    /**
     * Excluir horario limite.
     *
     * @param excluirHorarioLimiteEntradaDTO the excluir horario limite entrada dto
     * @return the excluir horario limite saida dto
     */
    ExcluirHorarioLimiteSaidaDTO excluirHorarioLimite(ExcluirHorarioLimiteEntradaDTO excluirHorarioLimiteEntradaDTO);
    
    
   
}

