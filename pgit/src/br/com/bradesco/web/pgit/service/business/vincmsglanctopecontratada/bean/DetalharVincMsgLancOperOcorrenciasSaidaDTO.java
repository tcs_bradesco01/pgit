package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

public class DetalharVincMsgLancOperOcorrenciasSaidaDTO {
	
	/** Atributo dsTipoPagamentoCliente. */
	private String dsTipoPagamentoCliente;

	public String getDsTipoPagamentoCliente() {
		return dsTipoPagamentoCliente;
	}

	public void setDsTipoPagamentoCliente(String dsTipoPagamentoCliente) {
		this.dsTipoPagamentoCliente = dsTipoPagamentoCliente;
	}	
	
}
