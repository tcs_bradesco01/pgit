/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarConfiguracaoLayoutArqContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarConfiguracaoLayoutArqContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdSituacaoLayoutContrato
     */
    private int _cdSituacaoLayoutContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoLayoutContrato
     */
    private boolean _has_cdSituacaoLayoutContrato;

    /**
     * Field _cdResponsavelCustoEmpresa
     */
    private int _cdResponsavelCustoEmpresa = 0;

    /**
     * keeps track of state for field: _cdResponsavelCustoEmpresa
     */
    private boolean _has_cdResponsavelCustoEmpresa;

    /**
     * Field _percentualCustoOrganizacaoTransmissao
     */
    private java.math.BigDecimal _percentualCustoOrganizacaoTransmissao = new java.math.BigDecimal("0");

    /**
     * Field _cdMensagemLinhaExtrato
     */
    private int _cdMensagemLinhaExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhaExtrato
     */
    private boolean _has_cdMensagemLinhaExtrato;

    /**
     * Field _numeroVersaoLayoutArquivo
     */
    private int _numeroVersaoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _numeroVersaoLayoutArquivo
     */
    private boolean _has_numeroVersaoLayoutArquivo;

    /**
     * Field _cdTipoControlePagamento
     */
    private int _cdTipoControlePagamento = 0;

    /**
     * keeps track of state for field: _cdTipoControlePagamento
     */
    private boolean _has_cdTipoControlePagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarConfiguracaoLayoutArqContratoRequest() 
     {
        super();
        setPercentualCustoOrganizacaoTransmissao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.request.AlterarConfiguracaoLayoutArqContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMensagemLinhaExtrato
     * 
     */
    public void deleteCdMensagemLinhaExtrato()
    {
        this._has_cdMensagemLinhaExtrato= false;
    } //-- void deleteCdMensagemLinhaExtrato() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdResponsavelCustoEmpresa
     * 
     */
    public void deleteCdResponsavelCustoEmpresa()
    {
        this._has_cdResponsavelCustoEmpresa= false;
    } //-- void deleteCdResponsavelCustoEmpresa() 

    /**
     * Method deleteCdSituacaoLayoutContrato
     * 
     */
    public void deleteCdSituacaoLayoutContrato()
    {
        this._has_cdSituacaoLayoutContrato= false;
    } //-- void deleteCdSituacaoLayoutContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoControlePagamento
     * 
     */
    public void deleteCdTipoControlePagamento()
    {
        this._has_cdTipoControlePagamento= false;
    } //-- void deleteCdTipoControlePagamento() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroVersaoLayoutArquivo
     * 
     */
    public void deleteNumeroVersaoLayoutArquivo()
    {
        this._has_numeroVersaoLayoutArquivo= false;
    } //-- void deleteNumeroVersaoLayoutArquivo() 

    /**
     * Returns the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhaExtrato'.
     */
    public int getCdMensagemLinhaExtrato()
    {
        return this._cdMensagemLinhaExtrato;
    } //-- int getCdMensagemLinhaExtrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdResponsavelCustoEmpresa'.
     * 
     * @return int
     * @return the value of field 'cdResponsavelCustoEmpresa'.
     */
    public int getCdResponsavelCustoEmpresa()
    {
        return this._cdResponsavelCustoEmpresa;
    } //-- int getCdResponsavelCustoEmpresa() 

    /**
     * Returns the value of field 'cdSituacaoLayoutContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoLayoutContrato'.
     */
    public int getCdSituacaoLayoutContrato()
    {
        return this._cdSituacaoLayoutContrato;
    } //-- int getCdSituacaoLayoutContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoControlePagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoControlePagamento'.
     */
    public int getCdTipoControlePagamento()
    {
        return this._cdTipoControlePagamento;
    } //-- int getCdTipoControlePagamento() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroVersaoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'numeroVersaoLayoutArquivo'.
     */
    public int getNumeroVersaoLayoutArquivo()
    {
        return this._numeroVersaoLayoutArquivo;
    } //-- int getNumeroVersaoLayoutArquivo() 

    /**
     * Returns the value of field
     * 'percentualCustoOrganizacaoTransmissao'.
     * 
     * @return BigDecimal
     * @return the value of field
     * 'percentualCustoOrganizacaoTransmissao'.
     */
    public java.math.BigDecimal getPercentualCustoOrganizacaoTransmissao()
    {
        return this._percentualCustoOrganizacaoTransmissao;
    } //-- java.math.BigDecimal getPercentualCustoOrganizacaoTransmissao() 

    /**
     * Method hasCdMensagemLinhaExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhaExtrato()
    {
        return this._has_cdMensagemLinhaExtrato;
    } //-- boolean hasCdMensagemLinhaExtrato() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdResponsavelCustoEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdResponsavelCustoEmpresa()
    {
        return this._has_cdResponsavelCustoEmpresa;
    } //-- boolean hasCdResponsavelCustoEmpresa() 

    /**
     * Method hasCdSituacaoLayoutContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoLayoutContrato()
    {
        return this._has_cdSituacaoLayoutContrato;
    } //-- boolean hasCdSituacaoLayoutContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoControlePagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoControlePagamento()
    {
        return this._has_cdTipoControlePagamento;
    } //-- boolean hasCdTipoControlePagamento() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroVersaoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroVersaoLayoutArquivo()
    {
        return this._has_numeroVersaoLayoutArquivo;
    } //-- boolean hasNumeroVersaoLayoutArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @param cdMensagemLinhaExtrato the value of field
     * 'cdMensagemLinhaExtrato'.
     */
    public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato)
    {
        this._cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
        this._has_cdMensagemLinhaExtrato = true;
    } //-- void setCdMensagemLinhaExtrato(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdResponsavelCustoEmpresa'.
     * 
     * @param cdResponsavelCustoEmpresa the value of field
     * 'cdResponsavelCustoEmpresa'.
     */
    public void setCdResponsavelCustoEmpresa(int cdResponsavelCustoEmpresa)
    {
        this._cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
        this._has_cdResponsavelCustoEmpresa = true;
    } //-- void setCdResponsavelCustoEmpresa(int) 

    /**
     * Sets the value of field 'cdSituacaoLayoutContrato'.
     * 
     * @param cdSituacaoLayoutContrato the value of field
     * 'cdSituacaoLayoutContrato'.
     */
    public void setCdSituacaoLayoutContrato(int cdSituacaoLayoutContrato)
    {
        this._cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
        this._has_cdSituacaoLayoutContrato = true;
    } //-- void setCdSituacaoLayoutContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoControlePagamento'.
     * 
     * @param cdTipoControlePagamento the value of field
     * 'cdTipoControlePagamento'.
     */
    public void setCdTipoControlePagamento(int cdTipoControlePagamento)
    {
        this._cdTipoControlePagamento = cdTipoControlePagamento;
        this._has_cdTipoControlePagamento = true;
    } //-- void setCdTipoControlePagamento(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroVersaoLayoutArquivo'.
     * 
     * @param numeroVersaoLayoutArquivo the value of field
     * 'numeroVersaoLayoutArquivo'.
     */
    public void setNumeroVersaoLayoutArquivo(int numeroVersaoLayoutArquivo)
    {
        this._numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
        this._has_numeroVersaoLayoutArquivo = true;
    } //-- void setNumeroVersaoLayoutArquivo(int) 

    /**
     * Sets the value of field
     * 'percentualCustoOrganizacaoTransmissao'.
     * 
     * @param percentualCustoOrganizacaoTransmissao the value of
     * field 'percentualCustoOrganizacaoTransmissao'.
     */
    public void setPercentualCustoOrganizacaoTransmissao(java.math.BigDecimal percentualCustoOrganizacaoTransmissao)
    {
        this._percentualCustoOrganizacaoTransmissao = percentualCustoOrganizacaoTransmissao;
    } //-- void setPercentualCustoOrganizacaoTransmissao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarConfiguracaoLayoutArqContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.request.AlterarConfiguracaoLayoutArqContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.request.AlterarConfiguracaoLayoutArqContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.request.AlterarConfiguracaoLayoutArqContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.request.AlterarConfiguracaoLayoutArqContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
