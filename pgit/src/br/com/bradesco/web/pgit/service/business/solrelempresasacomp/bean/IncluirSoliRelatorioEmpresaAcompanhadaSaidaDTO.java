/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean;

/**
 * Nome: IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dtPagtoIntegrado. */
	private String dtPagtoIntegrado;
	
	/** Atributo dtHoraPagamentoIntegrado. */
	private String dtHoraPagamentoIntegrado;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
     
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: dtPagtoIntegrado.
	 *
	 * @return dtPagtoIntegrado
	 */
	public String getDtPagtoIntegrado() {
		return dtPagtoIntegrado;
	}
	
	/**
	 * Set: dtPagtoIntegrado.
	 *
	 * @param dtPagtoIntegrado the dt pagto integrado
	 */
	public void setDtPagtoIntegrado(String dtPagtoIntegrado) {
		this.dtPagtoIntegrado = dtPagtoIntegrado;
	}
	
	/**
	 * Get: dtHoraPagamentoIntegrado.
	 *
	 * @return dtHoraPagamentoIntegrado
	 */
	public String getDtHoraPagamentoIntegrado() {
		return dtHoraPagamentoIntegrado;
	}
	
	/**
	 * Set: dtHoraPagamentoIntegrado.
	 *
	 * @param dtHoraPagamentoIntegrado the dt hora pagamento integrado
	 */
	public void setDtHoraPagamentoIntegrado(String dtHoraPagamentoIntegrado) {
		this.dtHoraPagamentoIntegrado = dtHoraPagamentoIntegrado;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
} 

