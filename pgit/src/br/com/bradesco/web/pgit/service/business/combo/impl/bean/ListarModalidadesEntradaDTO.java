/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.impl.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.impl.bean;

/**
 * Nome: ListarModalidadesEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarModalidadesEntradaDTO {

	
	/** Atributo cdServico. */
	private Integer cdServico;
	
	/** Atributo numeroOcorrencias. */
	private int numeroOcorrencias;

	/**
	 * Listar modalidades entrada dto.
	 */
	public ListarModalidadesEntradaDTO() {
		super();
	}

	/**
	 * Listar modalidades entrada dto.
	 *
	 * @param cdServico the cd servico
	 */
	public ListarModalidadesEntradaDTO(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public int getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(int numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	
	
}
