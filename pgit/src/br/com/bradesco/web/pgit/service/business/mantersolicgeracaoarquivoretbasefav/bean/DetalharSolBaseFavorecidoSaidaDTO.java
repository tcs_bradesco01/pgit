/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharSolBaseFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolBaseFavorecidoSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo dsTipoFavorecido. */
    private String dsTipoFavorecido;
    
    /** Atributo dsSituacaoFavorecido. */
    private String dsSituacaoFavorecido;
    
    /** Atributo cdDestino. */
    private Integer cdDestino;
    
    /** Atributo nrSolicitacaoPagamento. */
    private Integer nrSolicitacaoPagamento;
    
    /** Atributo hrSolicitacao. */
    private String hrSolicitacao;
    
    /** Atributo hrSolicitacaoFormatada. */
    private String hrSolicitacaoFormatada;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo resultadoProcessamento. */
    private String resultadoProcessamento;
    
    /** Atributo hrAtendimentoSolicitacao. */
    private String hrAtendimentoSolicitacao;
    
    /** Atributo hrAtendimentoSolicitacaoFormatada. */
    private String hrAtendimentoSolicitacaoFormatada;
    
    /** Atributo qtdeRegistroSolicitacao. */
    private Long qtdeRegistroSolicitacao;
    
    /** Atributo tarifaPadrao. */
    private BigDecimal tarifaPadrao;
    
    /** Atributo descontoTarifa. */
    private BigDecimal descontoTarifa;
    
    /** Atributo tarifaAtual. */
    private BigDecimal tarifaAtual;
    
    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdAutenticacaoSegurancaInclusao. */
    private String cdAutenticacaoSegurancaInclusao;
    
    /** Atributo nmOperacaoFluxo. */
    private String nmOperacaoFluxo;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo hrInclusaoRegistroFormatada. */
    private String hrInclusaoRegistroFormatada;
    
    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo cdAutenticacaoSegurancaManutencao. */
    private String cdAutenticacaoSegurancaManutencao;
    
    /** Atributo nmOperacaoFluxoManutencao. */
    private String nmOperacaoFluxoManutencao;
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;   
    
    /** Atributo hrManutencaoRegistroFormatada. */
    private String hrManutencaoRegistroFormatada;   
	    
	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdDestino.
	 *
	 * @return cdDestino
	 */
	public Integer getCdDestino() {
		return cdDestino;
	}
	
	/**
	 * Set: cdDestino.
	 *
	 * @param cdDestino the cd destino
	 */
	public void setCdDestino(Integer cdDestino) {
		this.cdDestino = cdDestino;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: descontoTarifa.
	 *
	 * @return descontoTarifa
	 */
	public BigDecimal getDescontoTarifa() {
		return descontoTarifa;
	}
	
	/**
	 * Set: descontoTarifa.
	 *
	 * @param descontoTarifa the desconto tarifa
	 */
	public void setDescontoTarifa(BigDecimal descontoTarifa) {
		this.descontoTarifa = descontoTarifa;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsSituacaoFavorecido.
	 *
	 * @return dsSituacaoFavorecido
	 */
	public String getDsSituacaoFavorecido() {
		return dsSituacaoFavorecido;
	}
	
	/**
	 * Set: dsSituacaoFavorecido.
	 *
	 * @param dsSituacaoFavorecido the ds situacao favorecido
	 */
	public void setDsSituacaoFavorecido(String dsSituacaoFavorecido) {
		this.dsSituacaoFavorecido = dsSituacaoFavorecido;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: dsTipoFavorecido.
	 *
	 * @return dsTipoFavorecido
	 */
	public String getDsTipoFavorecido() {
		return dsTipoFavorecido;
	}
	
	/**
	 * Set: dsTipoFavorecido.
	 *
	 * @param dsTipoFavorecido the ds tipo favorecido
	 */
	public void setDsTipoFavorecido(String dsTipoFavorecido) {
		this.dsTipoFavorecido = dsTipoFavorecido;
	}
	
	/**
	 * Get: hrAtendimentoSolicitacao.
	 *
	 * @return hrAtendimentoSolicitacao
	 */
	public String getHrAtendimentoSolicitacao() {
		return hrAtendimentoSolicitacao;
	}
	
	/**
	 * Set: hrAtendimentoSolicitacao.
	 *
	 * @param hrAtendimentoSolicitacao the hr atendimento solicitacao
	 */
	public void setHrAtendimentoSolicitacao(String hrAtendimentoSolicitacao) {
		this.hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: hrSolicitacao.
	 *
	 * @return hrSolicitacao
	 */
	public String getHrSolicitacao() {
		return hrSolicitacao;
	}
	
	/**
	 * Set: hrSolicitacao.
	 *
	 * @param hrSolicitacao the hr solicitacao
	 */
	public void setHrSolicitacao(String hrSolicitacao) {
		this.hrSolicitacao = hrSolicitacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nmOperacaoFluxo.
	 *
	 * @return nmOperacaoFluxo
	 */
	public String getNmOperacaoFluxo() {
		return nmOperacaoFluxo;
	}
	
	/**
	 * Set: nmOperacaoFluxo.
	 *
	 * @param nmOperacaoFluxo the nm operacao fluxo
	 */
	public void setNmOperacaoFluxo(String nmOperacaoFluxo) {
		this.nmOperacaoFluxo = nmOperacaoFluxo;
	}
	
	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}
	
	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}
	
	/**
	 * Get: qtdeRegistroSolicitacao.
	 *
	 * @return qtdeRegistroSolicitacao
	 */
	public Long getQtdeRegistroSolicitacao() {
		return qtdeRegistroSolicitacao;
	}
	
	/**
	 * Set: qtdeRegistroSolicitacao.
	 *
	 * @param qtdeRegistroSolicitacao the qtde registro solicitacao
	 */
	public void setQtdeRegistroSolicitacao(Long qtdeRegistroSolicitacao) {
		this.qtdeRegistroSolicitacao = qtdeRegistroSolicitacao;
	}
	
	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}
	
	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}
	
	/**
	 * Get: tarifaAtual.
	 *
	 * @return tarifaAtual
	 */
	public BigDecimal getTarifaAtual() {
		return tarifaAtual;
	}
	
	/**
	 * Set: tarifaAtual.
	 *
	 * @param tarifaAtual the tarifa atual
	 */
	public void setTarifaAtual(BigDecimal tarifaAtual) {
		this.tarifaAtual = tarifaAtual;
	}
	
	/**
	 * Get: tarifaPadrao.
	 *
	 * @return tarifaPadrao
	 */
	public BigDecimal getTarifaPadrao() {
		return tarifaPadrao;
	}
	
	/**
	 * Set: tarifaPadrao.
	 *
	 * @param tarifaPadrao the tarifa padrao
	 */
	public void setTarifaPadrao(BigDecimal tarifaPadrao) {
		this.tarifaPadrao = tarifaPadrao;
	}
	
	/**
	 * Get: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @return hrAtendimentoSolicitacaoFormatada
	 */
	public String getHrAtendimentoSolicitacaoFormatada() {
		return hrAtendimentoSolicitacaoFormatada;
	}
	
	/**
	 * Set: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @param hrAtendimentoSolicitacaoFormatada the hr atendimento solicitacao formatada
	 */
	public void setHrAtendimentoSolicitacaoFormatada(
			String hrAtendimentoSolicitacaoFormatada) {
		this.hrAtendimentoSolicitacaoFormatada = hrAtendimentoSolicitacaoFormatada;
	}
	
	/**
	 * Get: hrInclusaoRegistroFormatada.
	 *
	 * @return hrInclusaoRegistroFormatada
	 */
	public String getHrInclusaoRegistroFormatada() {
		return hrInclusaoRegistroFormatada;
	}
	
	/**
	 * Set: hrInclusaoRegistroFormatada.
	 *
	 * @param hrInclusaoRegistroFormatada the hr inclusao registro formatada
	 */
	public void setHrInclusaoRegistroFormatada(String hrInclusaoRegistroFormatada) {
		this.hrInclusaoRegistroFormatada = hrInclusaoRegistroFormatada;
	}
	
	/**
	 * Get: hrManutencaoRegistroFormatada.
	 *
	 * @return hrManutencaoRegistroFormatada
	 */
	public String getHrManutencaoRegistroFormatada() {
		return hrManutencaoRegistroFormatada;
	}
	
	/**
	 * Set: hrManutencaoRegistroFormatada.
	 *
	 * @param hrManutencaoRegistroFormatada the hr manutencao registro formatada
	 */
	public void setHrManutencaoRegistroFormatada(
			String hrManutencaoRegistroFormatada) {
		this.hrManutencaoRegistroFormatada = hrManutencaoRegistroFormatada;
	}
	
	/**
	 * Get: hrSolicitacaoFormatada.
	 *
	 * @return hrSolicitacaoFormatada
	 */
	public String getHrSolicitacaoFormatada() {
		return hrSolicitacaoFormatada;
	}
	
	/**
	 * Set: hrSolicitacaoFormatada.
	 *
	 * @param hrSolicitacaoFormatada the hr solicitacao formatada
	 */
	public void setHrSolicitacaoFormatada(String hrSolicitacaoFormatada) {
		this.hrSolicitacaoFormatada = hrSolicitacaoFormatada;
	}	
	
}
