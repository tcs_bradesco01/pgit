/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarSramoAtvddEconcRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarSramoAtvddEconcRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _ocorrencias
     */
    private int _ocorrencias = 0;

    /**
     * keeps track of state for field: _ocorrencias
     */
    private boolean _has_ocorrencias;

    /**
     * Field _cdRamoAtividade
     */
    private int _cdRamoAtividade = 0;

    /**
     * keeps track of state for field: _cdRamoAtividade
     */
    private boolean _has_cdRamoAtividade;

    /**
     * Field _cdClassAtividade
     */
    private java.lang.String _cdClassAtividade;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarSramoAtvddEconcRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.request.ListarSramoAtvddEconcRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdRamoAtividade
     * 
     */
    public void deleteCdRamoAtividade()
    {
        this._has_cdRamoAtividade= false;
    } //-- void deleteCdRamoAtividade() 

    /**
     * Method deleteOcorrencias
     * 
     */
    public void deleteOcorrencias()
    {
        this._has_ocorrencias= false;
    } //-- void deleteOcorrencias() 

    /**
     * Returns the value of field 'cdClassAtividade'.
     * 
     * @return String
     * @return the value of field 'cdClassAtividade'.
     */
    public java.lang.String getCdClassAtividade()
    {
        return this._cdClassAtividade;
    } //-- java.lang.String getCdClassAtividade() 

    /**
     * Returns the value of field 'cdRamoAtividade'.
     * 
     * @return int
     * @return the value of field 'cdRamoAtividade'.
     */
    public int getCdRamoAtividade()
    {
        return this._cdRamoAtividade;
    } //-- int getCdRamoAtividade() 

    /**
     * Returns the value of field 'ocorrencias'.
     * 
     * @return int
     * @return the value of field 'ocorrencias'.
     */
    public int getOcorrencias()
    {
        return this._ocorrencias;
    } //-- int getOcorrencias() 

    /**
     * Method hasCdRamoAtividade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRamoAtividade()
    {
        return this._has_cdRamoAtividade;
    } //-- boolean hasCdRamoAtividade() 

    /**
     * Method hasOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasOcorrencias()
    {
        return this._has_ocorrencias;
    } //-- boolean hasOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClassAtividade'.
     * 
     * @param cdClassAtividade the value of field 'cdClassAtividade'
     */
    public void setCdClassAtividade(java.lang.String cdClassAtividade)
    {
        this._cdClassAtividade = cdClassAtividade;
    } //-- void setCdClassAtividade(java.lang.String) 

    /**
     * Sets the value of field 'cdRamoAtividade'.
     * 
     * @param cdRamoAtividade the value of field 'cdRamoAtividade'.
     */
    public void setCdRamoAtividade(int cdRamoAtividade)
    {
        this._cdRamoAtividade = cdRamoAtividade;
        this._has_cdRamoAtividade = true;
    } //-- void setCdRamoAtividade(int) 

    /**
     * Sets the value of field 'ocorrencias'.
     * 
     * @param ocorrencias the value of field 'ocorrencias'.
     */
    public void setOcorrencias(int ocorrencias)
    {
        this._ocorrencias = ocorrencias;
        this._has_ocorrencias = true;
    } //-- void setOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarSramoAtvddEconcRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.request.ListarSramoAtvddEconcRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.request.ListarSramoAtvddEconcRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.request.ListarSramoAtvddEconcRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.request.ListarSramoAtvddEconcRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
