/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirSolEmiAviMovtoCliePagadorRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirSolEmiAviMovtoCliePagadorRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtInicioPerMovimentacao
     */
    private java.lang.String _dtInicioPerMovimentacao;

    /**
     * Field _dsFimPerMovimentacao
     */
    private java.lang.String _dsFimPerMovimentacao;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRecebedorCredito
     */
    private long _cdRecebedorCredito = 0;

    /**
     * keeps track of state for field: _cdRecebedorCredito
     */
    private boolean _has_cdRecebedorCredito;

    /**
     * Field _cdTipoInscricaoRecebedor
     */
    private int _cdTipoInscricaoRecebedor = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoRecebedor
     */
    private boolean _has_cdTipoInscricaoRecebedor;

    /**
     * Field _cdCpfCnpjRecebedor
     */
    private long _cdCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRecebedor
     */
    private boolean _has_cdCpfCnpjRecebedor;

    /**
     * Field _cdFilialCnpjRecebedor
     */
    private int _cdFilialCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRecebedor
     */
    private boolean _has_cdFilialCnpjRecebedor;

    /**
     * Field _cdControleCpfRecebedor
     */
    private int _cdControleCpfRecebedor = 0;

    /**
     * keeps track of state for field: _cdControleCpfRecebedor
     */
    private boolean _has_cdControleCpfRecebedor;

    /**
     * Field _cdDestinoCorrespondenciaSolic
     */
    private int _cdDestinoCorrespondenciaSolic = 0;

    /**
     * keeps track of state for field: _cdDestinoCorrespondenciaSoli
     */
    private boolean _has_cdDestinoCorrespondenciaSolic;

    /**
     * Field _cdTipoPostagemSolicitacao
     */
    private java.lang.String _cdTipoPostagemSolicitacao;

    /**
     * Field _dsLogradouroPagador
     */
    private java.lang.String _dsLogradouroPagador;

    /**
     * Field _dsNumeroLogradouroPagador
     */
    private java.lang.String _dsNumeroLogradouroPagador;

    /**
     * Field _dsComplementoLogradouroPagador
     */
    private java.lang.String _dsComplementoLogradouroPagador;

    /**
     * Field _dsBairroClientePagador
     */
    private java.lang.String _dsBairroClientePagador;

    /**
     * Field _dsMunicipioClientePagador
     */
    private java.lang.String _dsMunicipioClientePagador;

    /**
     * Field _cdSiglaUfPagador
     */
    private java.lang.String _cdSiglaUfPagador;

    /**
     * Field _cdCepPagador
     */
    private int _cdCepPagador = 0;

    /**
     * keeps track of state for field: _cdCepPagador
     */
    private boolean _has_cdCepPagador;

    /**
     * Field _cdCepComplementoPagador
     */
    private int _cdCepComplementoPagador = 0;

    /**
     * keeps track of state for field: _cdCepComplementoPagador
     */
    private boolean _has_cdCepComplementoPagador;

    /**
     * Field _dsEmailClientePagador
     */
    private java.lang.String _dsEmailClientePagador;

    /**
     * Field _cdPessoaJuridicaDepto
     */
    private long _cdPessoaJuridicaDepto = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaDepto
     */
    private boolean _has_cdPessoaJuridicaDepto;

    /**
     * Field _nrSequenciaUnidadeDepto
     */
    private int _nrSequenciaUnidadeDepto = 0;

    /**
     * keeps track of state for field: _nrSequenciaUnidadeDepto
     */
    private boolean _has_nrSequenciaUnidadeDepto;

    /**
     * Field _cdDepartamentoUnidade
     */
    private int _cdDepartamentoUnidade = 0;

    /**
     * keeps track of state for field: _cdDepartamentoUnidade
     */
    private boolean _has_cdDepartamentoUnidade;

    /**
     * Field _vrTarifaPadraoSolic
     */
    private java.math.BigDecimal _vrTarifaPadraoSolic = new java.math.BigDecimal("0");

    /**
     * Field _cdPercentualTarifaSolic
     */
    private java.math.BigDecimal _cdPercentualTarifaSolic = new java.math.BigDecimal("0");

    /**
     * Field _vlTarifaNegocioSolic
     */
    private java.math.BigDecimal _vlTarifaNegocioSolic = new java.math.BigDecimal("0");

    /**
     * Field _nrRegistrosEntrada
     */
    private int _nrRegistrosEntrada = 0;

    /**
     * keeps track of state for field: _nrRegistrosEntrada
     */
    private boolean _has_nrRegistrosEntrada;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirSolEmiAviMovtoCliePagadorRequest() 
     {
        super();
        setVrTarifaPadraoSolic(new java.math.BigDecimal("0"));
        setCdPercentualTarifaSolic(new java.math.BigDecimal("0"));
        setVlTarifaNegocioSolic(new java.math.BigDecimal("0"));
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.IncluirSolEmiAviMovtoCliePagadorRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias) 

    /**
     * Method deleteCdCepComplementoPagador
     * 
     */
    public void deleteCdCepComplementoPagador()
    {
        this._has_cdCepComplementoPagador= false;
    } //-- void deleteCdCepComplementoPagador() 

    /**
     * Method deleteCdCepPagador
     * 
     */
    public void deleteCdCepPagador()
    {
        this._has_cdCepPagador= false;
    } //-- void deleteCdCepPagador() 

    /**
     * Method deleteCdControleCpfRecebedor
     * 
     */
    public void deleteCdControleCpfRecebedor()
    {
        this._has_cdControleCpfRecebedor= false;
    } //-- void deleteCdControleCpfRecebedor() 

    /**
     * Method deleteCdCpfCnpjRecebedor
     * 
     */
    public void deleteCdCpfCnpjRecebedor()
    {
        this._has_cdCpfCnpjRecebedor= false;
    } //-- void deleteCdCpfCnpjRecebedor() 

    /**
     * Method deleteCdDepartamentoUnidade
     * 
     */
    public void deleteCdDepartamentoUnidade()
    {
        this._has_cdDepartamentoUnidade= false;
    } //-- void deleteCdDepartamentoUnidade() 

    /**
     * Method deleteCdDestinoCorrespondenciaSolic
     * 
     */
    public void deleteCdDestinoCorrespondenciaSolic()
    {
        this._has_cdDestinoCorrespondenciaSolic= false;
    } //-- void deleteCdDestinoCorrespondenciaSolic() 

    /**
     * Method deleteCdFilialCnpjRecebedor
     * 
     */
    public void deleteCdFilialCnpjRecebedor()
    {
        this._has_cdFilialCnpjRecebedor= false;
    } //-- void deleteCdFilialCnpjRecebedor() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaDepto
     * 
     */
    public void deleteCdPessoaJuridicaDepto()
    {
        this._has_cdPessoaJuridicaDepto= false;
    } //-- void deleteCdPessoaJuridicaDepto() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRecebedorCredito
     * 
     */
    public void deleteCdRecebedorCredito()
    {
        this._has_cdRecebedorCredito= false;
    } //-- void deleteCdRecebedorCredito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoInscricaoRecebedor
     * 
     */
    public void deleteCdTipoInscricaoRecebedor()
    {
        this._has_cdTipoInscricaoRecebedor= false;
    } //-- void deleteCdTipoInscricaoRecebedor() 

    /**
     * Method deleteNrRegistrosEntrada
     * 
     */
    public void deleteNrRegistrosEntrada()
    {
        this._has_nrRegistrosEntrada= false;
    } //-- void deleteNrRegistrosEntrada() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaUnidadeDepto
     * 
     */
    public void deleteNrSequenciaUnidadeDepto()
    {
        this._has_nrSequenciaUnidadeDepto= false;
    } //-- void deleteNrSequenciaUnidadeDepto() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdCepComplementoPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepComplementoPagador'.
     */
    public int getCdCepComplementoPagador()
    {
        return this._cdCepComplementoPagador;
    } //-- int getCdCepComplementoPagador() 

    /**
     * Returns the value of field 'cdCepPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepPagador'.
     */
    public int getCdCepPagador()
    {
        return this._cdCepPagador;
    } //-- int getCdCepPagador() 

    /**
     * Returns the value of field 'cdControleCpfRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfRecebedor'.
     */
    public int getCdControleCpfRecebedor()
    {
        return this._cdControleCpfRecebedor;
    } //-- int getCdControleCpfRecebedor() 

    /**
     * Returns the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRecebedor'.
     */
    public long getCdCpfCnpjRecebedor()
    {
        return this._cdCpfCnpjRecebedor;
    } //-- long getCdCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'cdDepartamentoUnidade'.
     * 
     * @return int
     * @return the value of field 'cdDepartamentoUnidade'.
     */
    public int getCdDepartamentoUnidade()
    {
        return this._cdDepartamentoUnidade;
    } //-- int getCdDepartamentoUnidade() 

    /**
     * Returns the value of field 'cdDestinoCorrespondenciaSolic'.
     * 
     * @return int
     * @return the value of field 'cdDestinoCorrespondenciaSolic'.
     */
    public int getCdDestinoCorrespondenciaSolic()
    {
        return this._cdDestinoCorrespondenciaSolic;
    } //-- int getCdDestinoCorrespondenciaSolic() 

    /**
     * Returns the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRecebedor'.
     */
    public int getCdFilialCnpjRecebedor()
    {
        return this._cdFilialCnpjRecebedor;
    } //-- int getCdFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'cdPercentualTarifaSolic'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualTarifaSolic'.
     */
    public java.math.BigDecimal getCdPercentualTarifaSolic()
    {
        return this._cdPercentualTarifaSolic;
    } //-- java.math.BigDecimal getCdPercentualTarifaSolic() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaDepto'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaDepto'.
     */
    public long getCdPessoaJuridicaDepto()
    {
        return this._cdPessoaJuridicaDepto;
    } //-- long getCdPessoaJuridicaDepto() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRecebedorCredito'.
     * 
     * @return long
     * @return the value of field 'cdRecebedorCredito'.
     */
    public long getCdRecebedorCredito()
    {
        return this._cdRecebedorCredito;
    } //-- long getCdRecebedorCredito() 

    /**
     * Returns the value of field 'cdSiglaUfPagador'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUfPagador'.
     */
    public java.lang.String getCdSiglaUfPagador()
    {
        return this._cdSiglaUfPagador;
    } //-- java.lang.String getCdSiglaUfPagador() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoInscricaoRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoRecebedor'.
     */
    public int getCdTipoInscricaoRecebedor()
    {
        return this._cdTipoInscricaoRecebedor;
    } //-- int getCdTipoInscricaoRecebedor() 

    /**
     * Returns the value of field 'cdTipoPostagemSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdTipoPostagemSolicitacao'.
     */
    public java.lang.String getCdTipoPostagemSolicitacao()
    {
        return this._cdTipoPostagemSolicitacao;
    } //-- java.lang.String getCdTipoPostagemSolicitacao() 

    /**
     * Returns the value of field 'dsBairroClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsBairroClientePagador'.
     */
    public java.lang.String getDsBairroClientePagador()
    {
        return this._dsBairroClientePagador;
    } //-- java.lang.String getDsBairroClientePagador() 

    /**
     * Returns the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsComplementoLogradouroPagador'.
     */
    public java.lang.String getDsComplementoLogradouroPagador()
    {
        return this._dsComplementoLogradouroPagador;
    } //-- java.lang.String getDsComplementoLogradouroPagador() 

    /**
     * Returns the value of field 'dsEmailClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsEmailClientePagador'.
     */
    public java.lang.String getDsEmailClientePagador()
    {
        return this._dsEmailClientePagador;
    } //-- java.lang.String getDsEmailClientePagador() 

    /**
     * Returns the value of field 'dsFimPerMovimentacao'.
     * 
     * @return String
     * @return the value of field 'dsFimPerMovimentacao'.
     */
    public java.lang.String getDsFimPerMovimentacao()
    {
        return this._dsFimPerMovimentacao;
    } //-- java.lang.String getDsFimPerMovimentacao() 

    /**
     * Returns the value of field 'dsLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroPagador'.
     */
    public java.lang.String getDsLogradouroPagador()
    {
        return this._dsLogradouroPagador;
    } //-- java.lang.String getDsLogradouroPagador() 

    /**
     * Returns the value of field 'dsMunicipioClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioClientePagador'.
     */
    public java.lang.String getDsMunicipioClientePagador()
    {
        return this._dsMunicipioClientePagador;
    } //-- java.lang.String getDsMunicipioClientePagador() 

    /**
     * Returns the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsNumeroLogradouroPagador'.
     */
    public java.lang.String getDsNumeroLogradouroPagador()
    {
        return this._dsNumeroLogradouroPagador;
    } //-- java.lang.String getDsNumeroLogradouroPagador() 

    /**
     * Returns the value of field 'dtInicioPerMovimentacao'.
     * 
     * @return String
     * @return the value of field 'dtInicioPerMovimentacao'.
     */
    public java.lang.String getDtInicioPerMovimentacao()
    {
        return this._dtInicioPerMovimentacao;
    } //-- java.lang.String getDtInicioPerMovimentacao() 

    /**
     * Returns the value of field 'nrRegistrosEntrada'.
     * 
     * @return int
     * @return the value of field 'nrRegistrosEntrada'.
     */
    public int getNrRegistrosEntrada()
    {
        return this._nrRegistrosEntrada;
    } //-- int getNrRegistrosEntrada() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaUnidadeDepto'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeDepto'.
     */
    public int getNrSequenciaUnidadeDepto()
    {
        return this._nrSequenciaUnidadeDepto;
    } //-- int getNrSequenciaUnidadeDepto() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'vlTarifaNegocioSolic'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaNegocioSolic'.
     */
    public java.math.BigDecimal getVlTarifaNegocioSolic()
    {
        return this._vlTarifaNegocioSolic;
    } //-- java.math.BigDecimal getVlTarifaNegocioSolic() 

    /**
     * Returns the value of field 'vrTarifaPadraoSolic'.
     * 
     * @return BigDecimal
     * @return the value of field 'vrTarifaPadraoSolic'.
     */
    public java.math.BigDecimal getVrTarifaPadraoSolic()
    {
        return this._vrTarifaPadraoSolic;
    } //-- java.math.BigDecimal getVrTarifaPadraoSolic() 

    /**
     * Method hasCdCepComplementoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplementoPagador()
    {
        return this._has_cdCepComplementoPagador;
    } //-- boolean hasCdCepComplementoPagador() 

    /**
     * Method hasCdCepPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepPagador()
    {
        return this._has_cdCepPagador;
    } //-- boolean hasCdCepPagador() 

    /**
     * Method hasCdControleCpfRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfRecebedor()
    {
        return this._has_cdControleCpfRecebedor;
    } //-- boolean hasCdControleCpfRecebedor() 

    /**
     * Method hasCdCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRecebedor()
    {
        return this._has_cdCpfCnpjRecebedor;
    } //-- boolean hasCdCpfCnpjRecebedor() 

    /**
     * Method hasCdDepartamentoUnidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDepartamentoUnidade()
    {
        return this._has_cdDepartamentoUnidade;
    } //-- boolean hasCdDepartamentoUnidade() 

    /**
     * Method hasCdDestinoCorrespondenciaSolic
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoCorrespondenciaSolic()
    {
        return this._has_cdDestinoCorrespondenciaSolic;
    } //-- boolean hasCdDestinoCorrespondenciaSolic() 

    /**
     * Method hasCdFilialCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRecebedor()
    {
        return this._has_cdFilialCnpjRecebedor;
    } //-- boolean hasCdFilialCnpjRecebedor() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaDepto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaDepto()
    {
        return this._has_cdPessoaJuridicaDepto;
    } //-- boolean hasCdPessoaJuridicaDepto() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRecebedorCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecebedorCredito()
    {
        return this._has_cdRecebedorCredito;
    } //-- boolean hasCdRecebedorCredito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoInscricaoRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoRecebedor()
    {
        return this._has_cdTipoInscricaoRecebedor;
    } //-- boolean hasCdTipoInscricaoRecebedor() 

    /**
     * Method hasNrRegistrosEntrada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrRegistrosEntrada()
    {
        return this._has_nrRegistrosEntrada;
    } //-- boolean hasNrRegistrosEntrada() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaUnidadeDepto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeDepto()
    {
        return this._has_nrSequenciaUnidadeDepto;
    } //-- boolean hasNrSequenciaUnidadeDepto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdCepComplementoPagador'.
     * 
     * @param cdCepComplementoPagador the value of field
     * 'cdCepComplementoPagador'.
     */
    public void setCdCepComplementoPagador(int cdCepComplementoPagador)
    {
        this._cdCepComplementoPagador = cdCepComplementoPagador;
        this._has_cdCepComplementoPagador = true;
    } //-- void setCdCepComplementoPagador(int) 

    /**
     * Sets the value of field 'cdCepPagador'.
     * 
     * @param cdCepPagador the value of field 'cdCepPagador'.
     */
    public void setCdCepPagador(int cdCepPagador)
    {
        this._cdCepPagador = cdCepPagador;
        this._has_cdCepPagador = true;
    } //-- void setCdCepPagador(int) 

    /**
     * Sets the value of field 'cdControleCpfRecebedor'.
     * 
     * @param cdControleCpfRecebedor the value of field
     * 'cdControleCpfRecebedor'.
     */
    public void setCdControleCpfRecebedor(int cdControleCpfRecebedor)
    {
        this._cdControleCpfRecebedor = cdControleCpfRecebedor;
        this._has_cdControleCpfRecebedor = true;
    } //-- void setCdControleCpfRecebedor(int) 

    /**
     * Sets the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @param cdCpfCnpjRecebedor the value of field
     * 'cdCpfCnpjRecebedor'.
     */
    public void setCdCpfCnpjRecebedor(long cdCpfCnpjRecebedor)
    {
        this._cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
        this._has_cdCpfCnpjRecebedor = true;
    } //-- void setCdCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'cdDepartamentoUnidade'.
     * 
     * @param cdDepartamentoUnidade the value of field
     * 'cdDepartamentoUnidade'.
     */
    public void setCdDepartamentoUnidade(int cdDepartamentoUnidade)
    {
        this._cdDepartamentoUnidade = cdDepartamentoUnidade;
        this._has_cdDepartamentoUnidade = true;
    } //-- void setCdDepartamentoUnidade(int) 

    /**
     * Sets the value of field 'cdDestinoCorrespondenciaSolic'.
     * 
     * @param cdDestinoCorrespondenciaSolic the value of field
     * 'cdDestinoCorrespondenciaSolic'.
     */
    public void setCdDestinoCorrespondenciaSolic(int cdDestinoCorrespondenciaSolic)
    {
        this._cdDestinoCorrespondenciaSolic = cdDestinoCorrespondenciaSolic;
        this._has_cdDestinoCorrespondenciaSolic = true;
    } //-- void setCdDestinoCorrespondenciaSolic(int) 

    /**
     * Sets the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @param cdFilialCnpjRecebedor the value of field
     * 'cdFilialCnpjRecebedor'.
     */
    public void setCdFilialCnpjRecebedor(int cdFilialCnpjRecebedor)
    {
        this._cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
        this._has_cdFilialCnpjRecebedor = true;
    } //-- void setCdFilialCnpjRecebedor(int) 

    /**
     * Sets the value of field 'cdPercentualTarifaSolic'.
     * 
     * @param cdPercentualTarifaSolic the value of field
     * 'cdPercentualTarifaSolic'.
     */
    public void setCdPercentualTarifaSolic(java.math.BigDecimal cdPercentualTarifaSolic)
    {
        this._cdPercentualTarifaSolic = cdPercentualTarifaSolic;
    } //-- void setCdPercentualTarifaSolic(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaDepto'.
     * 
     * @param cdPessoaJuridicaDepto the value of field
     * 'cdPessoaJuridicaDepto'.
     */
    public void setCdPessoaJuridicaDepto(long cdPessoaJuridicaDepto)
    {
        this._cdPessoaJuridicaDepto = cdPessoaJuridicaDepto;
        this._has_cdPessoaJuridicaDepto = true;
    } //-- void setCdPessoaJuridicaDepto(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRecebedorCredito'.
     * 
     * @param cdRecebedorCredito the value of field
     * 'cdRecebedorCredito'.
     */
    public void setCdRecebedorCredito(long cdRecebedorCredito)
    {
        this._cdRecebedorCredito = cdRecebedorCredito;
        this._has_cdRecebedorCredito = true;
    } //-- void setCdRecebedorCredito(long) 

    /**
     * Sets the value of field 'cdSiglaUfPagador'.
     * 
     * @param cdSiglaUfPagador the value of field 'cdSiglaUfPagador'
     */
    public void setCdSiglaUfPagador(java.lang.String cdSiglaUfPagador)
    {
        this._cdSiglaUfPagador = cdSiglaUfPagador;
    } //-- void setCdSiglaUfPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoRecebedor'.
     * 
     * @param cdTipoInscricaoRecebedor the value of field
     * 'cdTipoInscricaoRecebedor'.
     */
    public void setCdTipoInscricaoRecebedor(int cdTipoInscricaoRecebedor)
    {
        this._cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
        this._has_cdTipoInscricaoRecebedor = true;
    } //-- void setCdTipoInscricaoRecebedor(int) 

    /**
     * Sets the value of field 'cdTipoPostagemSolicitacao'.
     * 
     * @param cdTipoPostagemSolicitacao the value of field
     * 'cdTipoPostagemSolicitacao'.
     */
    public void setCdTipoPostagemSolicitacao(java.lang.String cdTipoPostagemSolicitacao)
    {
        this._cdTipoPostagemSolicitacao = cdTipoPostagemSolicitacao;
    } //-- void setCdTipoPostagemSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroClientePagador'.
     * 
     * @param dsBairroClientePagador the value of field
     * 'dsBairroClientePagador'.
     */
    public void setDsBairroClientePagador(java.lang.String dsBairroClientePagador)
    {
        this._dsBairroClientePagador = dsBairroClientePagador;
    } //-- void setDsBairroClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @param dsComplementoLogradouroPagador the value of field
     * 'dsComplementoLogradouroPagador'.
     */
    public void setDsComplementoLogradouroPagador(java.lang.String dsComplementoLogradouroPagador)
    {
        this._dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
    } //-- void setDsComplementoLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailClientePagador'.
     * 
     * @param dsEmailClientePagador the value of field
     * 'dsEmailClientePagador'.
     */
    public void setDsEmailClientePagador(java.lang.String dsEmailClientePagador)
    {
        this._dsEmailClientePagador = dsEmailClientePagador;
    } //-- void setDsEmailClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsFimPerMovimentacao'.
     * 
     * @param dsFimPerMovimentacao the value of field
     * 'dsFimPerMovimentacao'.
     */
    public void setDsFimPerMovimentacao(java.lang.String dsFimPerMovimentacao)
    {
        this._dsFimPerMovimentacao = dsFimPerMovimentacao;
    } //-- void setDsFimPerMovimentacao(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroPagador'.
     * 
     * @param dsLogradouroPagador the value of field
     * 'dsLogradouroPagador'.
     */
    public void setDsLogradouroPagador(java.lang.String dsLogradouroPagador)
    {
        this._dsLogradouroPagador = dsLogradouroPagador;
    } //-- void setDsLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioClientePagador'.
     * 
     * @param dsMunicipioClientePagador the value of field
     * 'dsMunicipioClientePagador'.
     */
    public void setDsMunicipioClientePagador(java.lang.String dsMunicipioClientePagador)
    {
        this._dsMunicipioClientePagador = dsMunicipioClientePagador;
    } //-- void setDsMunicipioClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @param dsNumeroLogradouroPagador the value of field
     * 'dsNumeroLogradouroPagador'.
     */
    public void setDsNumeroLogradouroPagador(java.lang.String dsNumeroLogradouroPagador)
    {
        this._dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
    } //-- void setDsNumeroLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioPerMovimentacao'.
     * 
     * @param dtInicioPerMovimentacao the value of field
     * 'dtInicioPerMovimentacao'.
     */
    public void setDtInicioPerMovimentacao(java.lang.String dtInicioPerMovimentacao)
    {
        this._dtInicioPerMovimentacao = dtInicioPerMovimentacao;
    } //-- void setDtInicioPerMovimentacao(java.lang.String) 

    /**
     * Sets the value of field 'nrRegistrosEntrada'.
     * 
     * @param nrRegistrosEntrada the value of field
     * 'nrRegistrosEntrada'.
     */
    public void setNrRegistrosEntrada(int nrRegistrosEntrada)
    {
        this._nrRegistrosEntrada = nrRegistrosEntrada;
        this._has_nrRegistrosEntrada = true;
    } //-- void setNrRegistrosEntrada(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeDepto'.
     * 
     * @param nrSequenciaUnidadeDepto the value of field
     * 'nrSequenciaUnidadeDepto'.
     */
    public void setNrSequenciaUnidadeDepto(int nrSequenciaUnidadeDepto)
    {
        this._nrSequenciaUnidadeDepto = nrSequenciaUnidadeDepto;
        this._has_nrSequenciaUnidadeDepto = true;
    } //-- void setNrSequenciaUnidadeDepto(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.Ocorrencias) 

    /**
     * Sets the value of field 'vlTarifaNegocioSolic'.
     * 
     * @param vlTarifaNegocioSolic the value of field
     * 'vlTarifaNegocioSolic'.
     */
    public void setVlTarifaNegocioSolic(java.math.BigDecimal vlTarifaNegocioSolic)
    {
        this._vlTarifaNegocioSolic = vlTarifaNegocioSolic;
    } //-- void setVlTarifaNegocioSolic(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vrTarifaPadraoSolic'.
     * 
     * @param vrTarifaPadraoSolic the value of field
     * 'vrTarifaPadraoSolic'.
     */
    public void setVrTarifaPadraoSolic(java.math.BigDecimal vrTarifaPadraoSolic)
    {
        this._vrTarifaPadraoSolic = vrTarifaPadraoSolic;
    } //-- void setVrTarifaPadraoSolic(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirSolEmiAviMovtoCliePagadorRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.IncluirSolEmiAviMovtoCliePagadorRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.IncluirSolEmiAviMovtoCliePagadorRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.IncluirSolEmiAviMovtoCliePagadorRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.request.IncluirSolEmiAviMovtoCliePagadorRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
