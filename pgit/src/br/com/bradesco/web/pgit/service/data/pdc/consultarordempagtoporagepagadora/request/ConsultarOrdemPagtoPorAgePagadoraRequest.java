/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarOrdemPagtoPorAgePagadoraRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarOrdemPagtoPorAgePagadoraRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _dataInicio
     */
    private java.lang.String _dataInicio;

    /**
     * Field _dataFim
     */
    private java.lang.String _dataFim;

    /**
     * Field _cdTipoInscricao
     */
    private int _cdTipoInscricao = 0;

    /**
     * keeps track of state for field: _cdTipoInscricao
     */
    private boolean _has_cdTipoInscricao;

    /**
     * Field _cdCnpjCpf
     */
    private long _cdCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCnpjCpf
     */
    private boolean _has_cdCnpjCpf;

    /**
     * Field _cdFilialCnpjCpf
     */
    private int _cdFilialCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjCpf
     */
    private boolean _has_cdFilialCnpjCpf;

    /**
     * Field _cdControleCnpjCpf
     */
    private int _cdControleCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdControleCnpjCpf
     */
    private boolean _has_cdControleCnpjCpf;

    /**
     * Field _vlPagamentoClienteInicio
     */
    private java.math.BigDecimal _vlPagamentoClienteInicio = new java.math.BigDecimal("0");

    /**
     * Field _vlPagamentoClienteFim
     */
    private java.math.BigDecimal _vlPagamentoClienteFim = new java.math.BigDecimal("0");

    /**
     * Field _cdSituacaoPagamento
     */
    private int _cdSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoPagamento
     */
    private boolean _has_cdSituacaoPagamento;

    /**
     * Field _cdTipoRetornoPagamento
     */
    private int _cdTipoRetornoPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoRetornoPagamento
     */
    private boolean _has_cdTipoRetornoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarOrdemPagtoPorAgePagadoraRequest() 
     {
        super();
        setVlPagamentoClienteInicio(new java.math.BigDecimal("0"));
        setVlPagamentoClienteFim(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.request.ConsultarOrdemPagtoPorAgePagadoraRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdCnpjCpf
     * 
     */
    public void deleteCdCnpjCpf()
    {
        this._has_cdCnpjCpf= false;
    } //-- void deleteCdCnpjCpf() 

    /**
     * Method deleteCdControleCnpjCpf
     * 
     */
    public void deleteCdControleCnpjCpf()
    {
        this._has_cdControleCnpjCpf= false;
    } //-- void deleteCdControleCnpjCpf() 

    /**
     * Method deleteCdFilialCnpjCpf
     * 
     */
    public void deleteCdFilialCnpjCpf()
    {
        this._has_cdFilialCnpjCpf= false;
    } //-- void deleteCdFilialCnpjCpf() 

    /**
     * Method deleteCdSituacaoPagamento
     * 
     */
    public void deleteCdSituacaoPagamento()
    {
        this._has_cdSituacaoPagamento= false;
    } //-- void deleteCdSituacaoPagamento() 

    /**
     * Method deleteCdTipoInscricao
     * 
     */
    public void deleteCdTipoInscricao()
    {
        this._has_cdTipoInscricao= false;
    } //-- void deleteCdTipoInscricao() 

    /**
     * Method deleteCdTipoRetornoPagamento
     * 
     */
    public void deleteCdTipoRetornoPagamento()
    {
        this._has_cdTipoRetornoPagamento= false;
    } //-- void deleteCdTipoRetornoPagamento() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCnpjCpf'.
     */
    public long getCdCnpjCpf()
    {
        return this._cdCnpjCpf;
    } //-- long getCdCnpjCpf() 

    /**
     * Returns the value of field 'cdControleCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjCpf'.
     */
    public int getCdControleCnpjCpf()
    {
        return this._cdControleCnpjCpf;
    } //-- int getCdControleCnpjCpf() 

    /**
     * Returns the value of field 'cdFilialCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjCpf'.
     */
    public int getCdFilialCnpjCpf()
    {
        return this._cdFilialCnpjCpf;
    } //-- int getCdFilialCnpjCpf() 

    /**
     * Returns the value of field 'cdSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPagamento'.
     */
    public int getCdSituacaoPagamento()
    {
        return this._cdSituacaoPagamento;
    } //-- int getCdSituacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoInscricao'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricao'.
     */
    public int getCdTipoInscricao()
    {
        return this._cdTipoInscricao;
    } //-- int getCdTipoInscricao() 

    /**
     * Returns the value of field 'cdTipoRetornoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRetornoPagamento'.
     */
    public int getCdTipoRetornoPagamento()
    {
        return this._cdTipoRetornoPagamento;
    } //-- int getCdTipoRetornoPagamento() 

    /**
     * Returns the value of field 'dataFim'.
     * 
     * @return String
     * @return the value of field 'dataFim'.
     */
    public java.lang.String getDataFim()
    {
        return this._dataFim;
    } //-- java.lang.String getDataFim() 

    /**
     * Returns the value of field 'dataInicio'.
     * 
     * @return String
     * @return the value of field 'dataInicio'.
     */
    public java.lang.String getDataInicio()
    {
        return this._dataInicio;
    } //-- java.lang.String getDataInicio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Returns the value of field 'vlPagamentoClienteFim'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoClienteFim'.
     */
    public java.math.BigDecimal getVlPagamentoClienteFim()
    {
        return this._vlPagamentoClienteFim;
    } //-- java.math.BigDecimal getVlPagamentoClienteFim() 

    /**
     * Returns the value of field 'vlPagamentoClienteInicio'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoClienteInicio'.
     */
    public java.math.BigDecimal getVlPagamentoClienteInicio()
    {
        return this._vlPagamentoClienteInicio;
    } //-- java.math.BigDecimal getVlPagamentoClienteInicio() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpf()
    {
        return this._has_cdCnpjCpf;
    } //-- boolean hasCdCnpjCpf() 

    /**
     * Method hasCdControleCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjCpf()
    {
        return this._has_cdControleCnpjCpf;
    } //-- boolean hasCdControleCnpjCpf() 

    /**
     * Method hasCdFilialCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjCpf()
    {
        return this._has_cdFilialCnpjCpf;
    } //-- boolean hasCdFilialCnpjCpf() 

    /**
     * Method hasCdSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPagamento()
    {
        return this._has_cdSituacaoPagamento;
    } //-- boolean hasCdSituacaoPagamento() 

    /**
     * Method hasCdTipoInscricao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricao()
    {
        return this._has_cdTipoInscricao;
    } //-- boolean hasCdTipoInscricao() 

    /**
     * Method hasCdTipoRetornoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRetornoPagamento()
    {
        return this._has_cdTipoRetornoPagamento;
    } //-- boolean hasCdTipoRetornoPagamento() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdCnpjCpf'.
     * 
     * @param cdCnpjCpf the value of field 'cdCnpjCpf'.
     */
    public void setCdCnpjCpf(long cdCnpjCpf)
    {
        this._cdCnpjCpf = cdCnpjCpf;
        this._has_cdCnpjCpf = true;
    } //-- void setCdCnpjCpf(long) 

    /**
     * Sets the value of field 'cdControleCnpjCpf'.
     * 
     * @param cdControleCnpjCpf the value of field
     * 'cdControleCnpjCpf'.
     */
    public void setCdControleCnpjCpf(int cdControleCnpjCpf)
    {
        this._cdControleCnpjCpf = cdControleCnpjCpf;
        this._has_cdControleCnpjCpf = true;
    } //-- void setCdControleCnpjCpf(int) 

    /**
     * Sets the value of field 'cdFilialCnpjCpf'.
     * 
     * @param cdFilialCnpjCpf the value of field 'cdFilialCnpjCpf'.
     */
    public void setCdFilialCnpjCpf(int cdFilialCnpjCpf)
    {
        this._cdFilialCnpjCpf = cdFilialCnpjCpf;
        this._has_cdFilialCnpjCpf = true;
    } //-- void setCdFilialCnpjCpf(int) 

    /**
     * Sets the value of field 'cdSituacaoPagamento'.
     * 
     * @param cdSituacaoPagamento the value of field
     * 'cdSituacaoPagamento'.
     */
    public void setCdSituacaoPagamento(int cdSituacaoPagamento)
    {
        this._cdSituacaoPagamento = cdSituacaoPagamento;
        this._has_cdSituacaoPagamento = true;
    } //-- void setCdSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoInscricao'.
     * 
     * @param cdTipoInscricao the value of field 'cdTipoInscricao'.
     */
    public void setCdTipoInscricao(int cdTipoInscricao)
    {
        this._cdTipoInscricao = cdTipoInscricao;
        this._has_cdTipoInscricao = true;
    } //-- void setCdTipoInscricao(int) 

    /**
     * Sets the value of field 'cdTipoRetornoPagamento'.
     * 
     * @param cdTipoRetornoPagamento the value of field
     * 'cdTipoRetornoPagamento'.
     */
    public void setCdTipoRetornoPagamento(int cdTipoRetornoPagamento)
    {
        this._cdTipoRetornoPagamento = cdTipoRetornoPagamento;
        this._has_cdTipoRetornoPagamento = true;
    } //-- void setCdTipoRetornoPagamento(int) 

    /**
     * Sets the value of field 'dataFim'.
     * 
     * @param dataFim the value of field 'dataFim'.
     */
    public void setDataFim(java.lang.String dataFim)
    {
        this._dataFim = dataFim;
    } //-- void setDataFim(java.lang.String) 

    /**
     * Sets the value of field 'dataInicio'.
     * 
     * @param dataInicio the value of field 'dataInicio'.
     */
    public void setDataInicio(java.lang.String dataInicio)
    {
        this._dataInicio = dataInicio;
    } //-- void setDataInicio(java.lang.String) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Sets the value of field 'vlPagamentoClienteFim'.
     * 
     * @param vlPagamentoClienteFim the value of field
     * 'vlPagamentoClienteFim'.
     */
    public void setVlPagamentoClienteFim(java.math.BigDecimal vlPagamentoClienteFim)
    {
        this._vlPagamentoClienteFim = vlPagamentoClienteFim;
    } //-- void setVlPagamentoClienteFim(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoClienteInicio'.
     * 
     * @param vlPagamentoClienteInicio the value of field
     * 'vlPagamentoClienteInicio'.
     */
    public void setVlPagamentoClienteInicio(java.math.BigDecimal vlPagamentoClienteInicio)
    {
        this._vlPagamentoClienteInicio = vlPagamentoClienteInicio;
    } //-- void setVlPagamentoClienteInicio(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarOrdemPagtoPorAgePagadoraRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.request.ConsultarOrdemPagtoPorAgePagadoraRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.request.ConsultarOrdemPagtoPorAgePagadoraRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.request.ConsultarOrdemPagtoPorAgePagadoraRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.request.ConsultarOrdemPagtoPorAgePagadoraRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
