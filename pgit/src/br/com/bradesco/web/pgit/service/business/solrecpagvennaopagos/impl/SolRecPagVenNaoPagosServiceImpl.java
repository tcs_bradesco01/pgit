/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.ISolRecPagVenNaoPagosService;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.ConsultarPagotsVencNaoPagosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.ConsultarPagotsVencNaoPagosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.SolicitarRecPagtosVencidosNaoPagosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.SolicitarRecPagtosVencidosNaoPagosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrecupcompexp.ISolRecupCompExpConstants;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagotsvencnaopagos.request.ConsultarPagotsVencNaoPagosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagotsvencnaopagos.response.ConsultarPagotsVencNaoPagosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosvencidosnaopagos.request.SolicitarRecPagtosVencidosNaoPagosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosvencidosnaopagos.response.SolicitarRecPagtosVencidosNaoPagosResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: SolRecPagVenNaoPagos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SolRecPagVenNaoPagosServiceImpl implements ISolRecPagVenNaoPagosService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.ISolRecPagVenNaoPagosService#consultarPagotsVencNaoPagos(br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.ConsultarPagotsVencNaoPagosEntradaDTO)
	 */
	public List<ConsultarPagotsVencNaoPagosSaidaDTO> consultarPagotsVencNaoPagos(ConsultarPagotsVencNaoPagosEntradaDTO entradaDTO) {
		List<ConsultarPagotsVencNaoPagosSaidaDTO> listaRetorno = new ArrayList<ConsultarPagotsVencNaoPagosSaidaDTO>();		
		
		ConsultarPagotsVencNaoPagosSaidaDTO saidaDTO;
		
	
		 ConsultarPagotsVencNaoPagosRequest request = new ConsultarPagotsVencNaoPagosRequest();
        request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
        request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
        
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
        request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
        request.setCdDigitoConta(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoConta()));
        request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
        request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
        request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemssaPagamento()));
        request.setCdParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdParticipante()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
        request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
        request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
        request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
        request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
        request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
        request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
        request.setCdBarraDocumento(PgitUtil.verificaStringNula(entradaDTO.getCdBarraDocumento()));
        request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
        request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
        request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
        request.setCdBancoBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
        request.setCdAgenciaBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
        request.setCdContaBeneficiario(PgitUtil.verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
        request.setCdDigitoContaBeneficiario(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoContaBeneficiario()));
        request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
        request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoConta()));
        request.setNrContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrContrato()));
        request.setNrUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
        request.setCdBeneficio(PgitUtil.verificaLongNulo(entradaDTO.getCdBeneficio()));
        request.setCdEspecieBeneficioInss(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
        request.setCdClassificacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdClassificacao()));
        request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
        request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
        request.setCdControleCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
        request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
        request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
        request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
        request.setNrSequenciaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
        request.setCdIndiceSimulaPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
        request.setCdOrigemRecebidoEfetivacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));
        request.setCdTituloPgtoRastreado(0);
        request.setCdBancoSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
        request.setCdAgencaSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
        request.setCdContaSalarial(PgitUtil.verificaLongNulo(entradaDTO.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getCdIspbPagtoDestino(), 8));
		}
		request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getContaPagtoDestino(), 20));

        
        request.setNrOcorrencias(ISolRecupCompExpConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        
        ConsultarPagotsVencNaoPagosResponse response = getFactoryAdapter().getConsultarPagotsVencNaoPagosPDCAdapter().invokeProcess(request);
        
        for (int i = 0; i < response.getNumeroConsultas(); i++) {
        	saidaDTO = new ConsultarPagotsVencNaoPagosSaidaDTO();
        	 saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
             saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
             saidaDTO.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
             saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
             saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
             saidaDTO.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
             saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
             saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
             saidaDTO.setNrContrato(response.getOcorrencias(i).getNrContrato());
             saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
             saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
             saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
             saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
             saidaDTO.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
             saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento());
             saidaDTO.setDtCreditoPagamentoFormatada(FormatarData.formatarDataFromPdc(response.getOcorrencias(i).getDtCreditoPagamento()));
             saidaDTO.setVlEfetivoPagamento(response.getOcorrencias(i).getVlEfetivoPagamento());
             saidaDTO.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido());
             saidaDTO.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
             saidaDTO.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
             saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
             saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
             saidaDTO.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
             saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
             saidaDTO.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
             saidaDTO.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
             saidaDTO.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
             saidaDTO.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
             saidaDTO.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
             saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
             saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
             saidaDTO.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
             saidaDTO.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
             saidaDTO.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
             saidaDTO.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
             
     	     saidaDTO.setCdCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), 
     	    		 response.getOcorrencias(i).getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
     	
     	     
     	    saidaDTO.setFavorecidoBeneficiarioFormatado(PgitUtil.concatenarCampos(saidaDTO.getCdInscricaoFavorecido(), saidaDTO.getDsFavorecido(), "-"));
     	    saidaDTO.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBancoCredito(), saidaDTO.getCdAgenciaCredito(), saidaDTO.getCdDigitoAgenciaCredito(), saidaDTO.getCdContaCredito(), saidaDTO.getCdDigitoContaCredito(), true));
            saidaDTO.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBancoDebito(), saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigitoAgenciaDebito(), saidaDTO.getCdContaDebito(), saidaDTO.getCdDigitoContaDebito(), true));
     	   	saidaDTO.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
     	   	saidaDTO.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
     	   	saidaDTO.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
     	   	saidaDTO.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
     	 	
     	   	listaRetorno.add(saidaDTO);
			
		}
		
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.ISolRecPagVenNaoPagosService#solicitarRecPagtosVencidosNaoPagos(br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.SolicitarRecPagtosVencidosNaoPagosEntradaDTO)
	 */
	public SolicitarRecPagtosVencidosNaoPagosSaidaDTO solicitarRecPagtosVencidosNaoPagos(SolicitarRecPagtosVencidosNaoPagosEntradaDTO entradaDTO) {
		SolicitarRecPagtosVencidosNaoPagosSaidaDTO saidaDTO = new SolicitarRecPagtosVencidosNaoPagosSaidaDTO();
		SolicitarRecPagtosVencidosNaoPagosRequest request = new SolicitarRecPagtosVencidosNaoPagosRequest();
		SolicitarRecPagtosVencidosNaoPagosResponse response = new SolicitarRecPagtosVencidosNaoPagosResponse();
		
		request.setDtQuitacao(PgitUtil.verificaStringNula(entradaDTO.getDtQuitacao()));
		request.setNrPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrPagamento()));
		request.setCdPrimeiraSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getFlagPrimeiraSolicitacao()));
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
		request.setCdProduto(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProduto()));
		request.setCdProdutoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoRelacionado()));
		
		

		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosvencidosnaopagos.request.Ocorrencias> ocorrencias  = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosvencidosnaopagos.request.Ocorrencias>();
    	br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosvencidosnaopagos.request.Ocorrencias ocorrencia;
    	
    	for(int i=0;i<entradaDTO.getOcorrencias().size();i++){
    	   ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosvencidosnaopagos.request.Ocorrencias();
 		   
 		   ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(entradaDTO.getOcorrencias().get(i).getCdControlePagamento()));
 		   ocorrencia.setCdModalidadePagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getOcorrencias().get(i).getCdModalidadePagamento()));
 		   ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getOcorrencias().get(i).getCdPessoaJuridicaContrato())); 		 
 		   ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getOcorrencias().get(i).getCdTipoContratoNegocio()));
 		   ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getOcorrencias().get(i).getNrSequenciaContratoNegocio()));
 		   ocorrencia.setDsEfetivacaoPagamento(PgitUtil.verificaStringNula(entradaDTO.getOcorrencias().get(i).getDsEfetivacaoPagamento()));
 		     
 		  ocorrencias.add(ocorrencia);
    	}
    	
    	request.setOcorrencias(ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosvencidosnaopagos.request.Ocorrencias[0]));
    	
    	response = getFactoryAdapter().getSolicitarRecPagtosVencidosNaoPagosPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
    	saidaDTO.setMensagem(response.getMensagem());		
    	/*saidaDTO.setCdSolicitacaoPagamento(response.getCdSolicitacaoPagamento());
    	saidaDTO.setNrSolicitacaoPagamento(response.getNrSolicitacaoPagamento());  */	
		

        return saidaDTO;
	}

}

