/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricofavorecido.response.DetalharHistoricoFavorecidoResponse;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: DetalharHistoricoFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistoricoFavorecidoSaidaDTO {

    /** Atributo cdTipoFavorecido. */
    private Integer cdTipoFavorecido;
    
    /** Atributo dsCodigoTipoFavorecido. */
    private String dsCodigoTipoFavorecido;
    
    /** Atributo cdTipoInscricaoFavorecido. */
    private Integer cdTipoInscricaoFavorecido;
    
    /** Atributo dsCodigoTipoInscricao. */
    private String  dsCodigoTipoInscricao;
    
    /** Atributo cdMotivoBloqueioFavorecido. */
    private Integer cdMotivoBloqueioFavorecido;
    
    /** Atributo dsCodigoMotivoBloqueio. */
    private String dsCodigoMotivoBloqueio;
    
    /** Atributo cdInscricaoFavorecidoCliente. */
    private Long cdInscricaoFavorecidoCliente;
    
    /** Atributo dsComplementoFavorecidoCliente. */
    private String dsComplementoFavorecidoCliente;
    
    /** Atributo dsAbreviacaoFavorecidoCliente. */
    private String dsAbreviacaoFavorecidoCliente;
    
    /** Atributo dsOrgaoEmissorDocumento. */
    private String dsOrgaoEmissorDocumento;
    
    /** Atributo dtExpedicaoDocumentoFavorecido. */
    private String dtExpedicaoDocumentoFavorecido;
    
    /** Atributo cdSexoFavorecidoCliente. */
    private String cdSexoFavorecidoCliente;
    
    /** Atributo cdAreaFavorecidoCliente. */
    private Integer cdAreaFavorecidoCliente;
    
    /** Atributo cdFoneFavorecidoCliente. */
    private String cdFoneFavorecidoCliente;
    
    /** Atributo cdAreaFaxFavorecido. */
    private Integer cdAreaFaxFavorecido;
    
    /** Atributo cdFaxFavorecidoCliente. */
    private String cdFaxFavorecidoCliente;
    
    /** Atributo cdRamalFaxFavorecido. */
    private Integer cdRamalFaxFavorecido;
    
    /** Atributo cdAreaCelularFavorecido. */
    private Integer cdAreaCelularFavorecido;
    
    /** Atributo cdCelularFavorecidoCliente. */
    private String cdCelularFavorecidoCliente;
    
    /** Atributo cdAreaFoneComercial. */
    private Integer cdAreaFoneComercial;
    
    /** Atributo cdFoneComercialFavorecido. */
    private String cdFoneComercialFavorecido;
    
    /** Atributo cdRamalComercialFavorecido. */
    private Integer cdRamalComercialFavorecido;
    
    /** Atributo enEmailFavorecidoCliente. */
    private String enEmailFavorecidoCliente;
    
    /** Atributo enEmailComercialFavorecido. */
    private String enEmailComercialFavorecido;
    
    /** Atributo cdSmsFavorecidoCliente. */
    private String cdSmsFavorecidoCliente;
    
    /** Atributo enLogradouroFavorecidoCliente. */
    private String enLogradouroFavorecidoCliente;
    
    /** Atributo enNumeroLogradouroFavorecido. */
    private String enNumeroLogradouroFavorecido;
    
    /** Atributo enComplementoLogradouroFavorecido. */
    private String enComplementoLogradouroFavorecido;
    
    /** Atributo enBairroFavorecidoCliente. */
    private String enBairroFavorecidoCliente;
    
    /** Atributo dsEstadoFavorecidoCliente. */
    private String dsEstadoFavorecidoCliente;
    
    /** Atributo cdSiglaEstadoFavorecidoCliente. */
    private String cdSiglaEstadoFavorecidoCliente;
    
    /** Atributo cdCepFavorecidoCliente. */
    private Integer cdCepFavorecidoCliente;
    
    /** Atributo cdComplementoCepFavorecido. */
    private Integer cdComplementoCepFavorecido;
    
    /** Atributo enLogradouroCorrespondenciaFavorecido. */
    private String enLogradouroCorrespondenciaFavorecido;
    
    /** Atributo enNumeroLogradouroCorrespondencia. */
    private String enNumeroLogradouroCorrespondencia;
    
    /** Atributo enComplementoLogradouroCorrespondencia. */
    private String enComplementoLogradouroCorrespondencia;
    
    /** Atributo enBairroCorrespondenciaFavorecido. */
    private String enBairroCorrespondenciaFavorecido;
    
    /** Atributo dsEstadoCorrespondenciaFavorecido. */
    private String dsEstadoCorrespondenciaFavorecido;
    
    /** Atributo cdSiglaEstadoCorrespondencia. */
    private String cdSiglaEstadoCorrespondencia;
    
    /** Atributo cdCepCorrespondenciaFavorecido. */
    private Integer cdCepCorrespondenciaFavorecido;
    
    /** Atributo cdComplementoCepCorrespondencia. */
    private Integer cdComplementoCepCorrespondencia;
    
    /** Atributo cdNitFavorecidoCliente. */
    private Long cdNitFavorecidoCliente;
    
    /** Atributo dsMunicipioFavorecidoCliente. */
    private String dsMunicipioFavorecidoCliente;
    
    /** Atributo cdInscricaoEstadualFavorecido. */
    private Long cdInscricaoEstadualFavorecido;
    
    /** Atributo cdSiglaUfInscricaoEstadual. */
    private String cdSiglaUfInscricaoEstadual;
    
    /** Atributo dsMunicipioInscricaoEstadual. */
    private String dsMunicipioInscricaoEstadual;
    
    /** Atributo cdSituacaoFavorecidoCliente. */
    private Integer cdSituacaoFavorecidoCliente;
    
    /** Atributo hrBloqueioFavorecidoCliente. */
    private String hrBloqueioFavorecidoCliente;
    
    /** Atributo dsObservacaoControleFavorecido. */
    private String dsObservacaoControleFavorecido;
    
    /** Atributo cdIndicadorRetornoCliente. */
    private Integer cdIndicadorRetornoCliente;
    
    /** Atributo dsRetornoCliente. */
    private String dsRetornoCliente;
    
    /** Atributo dsUltimaMovimentacaoFavorecido. */
    private String dsUltimaMovimentacaoFavorecido;
    
    /** Atributo cdIndicadorFavorecidoInativo. */
    private Integer cdIndicadorFavorecidoInativo;
    
    /** Atributo cdEstadoCivilFavorecido. */
    private Integer cdEstadoCivilFavorecido;
    
    /** Atributo cdUfEmpresaFavorecido. */
    private String cdUfEmpresaFavorecido;
    
    /** Atributo dtNascimentoFavorecidoCliente. */
    private String dtNascimentoFavorecidoCliente;
    
    /** Atributo dsEmpresaFavorecidoCliente. */
    private String dsEmpresaFavorecidoCliente;
    
    /** Atributo dsMunicipioEmpresaFavorecido. */
    private String dsMunicipioEmpresaFavorecido;
    
    /** Atributo dsConjugeFavorecidoCliente. */
    private String dsConjugeFavorecidoCliente;
    
    /** Atributo dsNacionalidadeFavorecidoCliente. */
    private String dsNacionalidadeFavorecidoCliente;
    
    /** Atributo dsProfissaoFavorecidoCliente. */
    private String dsProfissaoFavorecidoCliente;
    
    /** Atributo dsCargoFavorecidoCliente. */
    private String dsCargoFavorecidoCliente;
    
    /** Atributo dsUfEmpresaFavorecido. */
    private String dsUfEmpresaFavorecido;
    
    /** Atributo dsMaeFavorecidoCliente. */
    private String dsMaeFavorecidoCliente;
    
    /** Atributo dsPaiFavorecidoCliente. */
    private String dsPaiFavorecidoCliente;
    
    /** Atributo dsMunicipalNascimentoFavorecido. */
    private String dsMunicipalNascimentoFavorecido;
    
    /** Atributo dsUfNacionalidadeFavorecido. */
    private String dsUfNacionalidadeFavorecido;
    
    /** Atributo cdUfNascimentoFavorecido. */
    private String cdUfNascimentoFavorecido;
    
    /** Atributo vlRendaMensalFavorecido. */
    private Double vlRendaMensalFavorecido;
    
    /** Atributo dtAtualizacaoEnderecoResidencial. */
    private String dtAtualizacaoEnderecoResidencial;
    
    /** Atributo dtAtualizacaoEnderecoCorrespondencia. */
    private String dtAtualizacaoEnderecoCorrespondencia;
    
    /** Atributo cdIndicadorTipoManutencao. */
    private Integer cdIndicadorTipoManutencao;
    
    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo dtInclusao. */
    private String dtInclusao;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoEsteno. */
    private String cdUsuarioManutencaoEsteno;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo cdAcaoManutencao. */
    private Integer cdAcaoManutencao;
	
    /**
     * Detalhar historico favorecido saida dto.
     */
    public DetalharHistoricoFavorecidoSaidaDTO() {
		super();
	}

	/**
	 * Detalhar historico favorecido saida dto.
	 *
	 * @param saida the saida
	 */
	public DetalharHistoricoFavorecidoSaidaDTO(DetalharHistoricoFavorecidoResponse saida) {
		super();
		this.cdTipoFavorecido = saida.getCdTipoFavorecido();
		this.dsCodigoTipoFavorecido = saida.getDsCodigoTipoFavorecido();
		this.cdTipoInscricaoFavorecido = saida.getCdTipoInscricaoFavorecido();
		this.dsCodigoTipoInscricao = saida.getDsCodigoTipoInscricao();
		this.cdMotivoBloqueioFavorecido = saida.getCdMotivoBloqueioFavorecido();
		this.dsCodigoMotivoBloqueio = saida.getDsCodigoMotivoBloqueio();
		this.cdInscricaoFavorecidoCliente = saida.getCdInscricaoFavorecidoCliente();
		this.dsComplementoFavorecidoCliente = saida.getDsComplementoFavorecidoCliente();
		this.dsAbreviacaoFavorecidoCliente = saida.getDsAbreviacaoFavorecidoCliente();
		this.dsOrgaoEmissorDocumento = saida.getDsOrgaoEmissorDocumento();
		this.dtExpedicaoDocumentoFavorecido = saida.getDtExpedicaoDocumentoFavorecido();
		this.cdSexoFavorecidoCliente = saida.getCdSexoFavorecidoCliente();
		this.cdAreaFavorecidoCliente = saida.getCdAreaFavorecidoCliente();
		this.cdFoneFavorecidoCliente = saida.getCdFoneFavorecidoCliente();
		this.cdAreaFaxFavorecido = saida.getCdAreaFaxFavorecido();
		this.cdFaxFavorecidoCliente = saida.getCdFaxFavorecidoCliente();
		this.cdRamalFaxFavorecido = saida.getCdRamalFaxFavorecido();
		this.cdAreaCelularFavorecido = saida.getCdAreaCelularFavorecido();
		this.cdCelularFavorecidoCliente = saida.getCdCelularFavorecidoCliente();
		this.cdAreaFoneComercial = saida.getCdAreaFoneComercial();
		this.cdFoneComercialFavorecido = saida.getCdFoneComercialFavorecido();
		this.cdRamalComercialFavorecido = saida.getCdRamalComercialFavorecido();
		this.enEmailFavorecidoCliente = saida.getEnEmailFavorecidoCliente();
		this.enEmailComercialFavorecido = saida.getEnEmailComercialFavorecido();
		this.cdSmsFavorecidoCliente = saida.getCdSmsFavorecidoCliente();
		this.enLogradouroFavorecidoCliente = saida.getEnLogradouroFavorecidoCliente();
		this.enNumeroLogradouroFavorecido = saida.getEnNumeroLogradouroFavorecido();
		this.enComplementoLogradouroFavorecido = saida.getEnComplementoLogradouroFavorecido();
		this.enBairroFavorecidoCliente = saida.getEnBairroFavorecidoCliente();
		this.dsEstadoFavorecidoCliente = saida.getDsEstadoFavorecidoCliente();
		this.cdSiglaEstadoFavorecidoCliente = saida.getCdSiglaEstadoFavorecidoCliente();
		this.cdCepFavorecidoCliente = saida.getCdCepFavorecidoCliente();
		this.cdComplementoCepFavorecido = saida.getCdComplementoCepFavorecido();
		this.enLogradouroCorrespondenciaFavorecido = saida.getEnLogradouroCorrespondenciaFavorecido();
		this.enNumeroLogradouroCorrespondencia = saida.getEnNumeroLogradouroCorrespondencia();
		this.enComplementoLogradouroCorrespondencia = saida.getEnComplementoLogradouroCorrespondencia();
		this.enBairroCorrespondenciaFavorecido = saida.getEnBairroCorrespondenciaFavorecido();
		this.dsEstadoCorrespondenciaFavorecido = saida.getDsEstadoCorrespondenciaFavorecido();
		this.cdSiglaEstadoCorrespondencia = saida.getCdSiglaEstadoCorrespondencia();
		this.cdCepCorrespondenciaFavorecido = saida.getCdCepCorrespondenciaFavorecido();
		this.cdComplementoCepCorrespondencia = saida.getCdComplementoCepCorrespondencia();
		this.cdNitFavorecidoCliente = saida.getCdNitFavorecidoCliente();
		this.dsMunicipioFavorecidoCliente = saida.getDsMunicipioFavorecidoCliente();
		this.cdInscricaoEstadualFavorecido = saida.getCdInscricaoEstadualFavorecido();
		this.cdSiglaUfInscricaoEstadual = saida.getCdSiglaUfInscricaoEstadual();
		this.dsMunicipioInscricaoEstadual = saida.getDsMunicipioInscricaoEstadual();
		this.cdSituacaoFavorecidoCliente = saida.getCdSituacaoFavorecidoCliente();
		this.hrBloqueioFavorecidoCliente = saida.getHrBloqueioFavorecidoCliente();
		this.dsObservacaoControleFavorecido = saida.getDsObservacaoControleFavorecido();
		this.cdIndicadorRetornoCliente = saida.getCdIndicadorRetornoCliente();
		this.dsRetornoCliente = saida.getDsRetornoCliente();
		this.dsUltimaMovimentacaoFavorecido = saida.getDsUltimaMovimentacaoFavorecido();
		this.cdIndicadorFavorecidoInativo = saida.getCdIndicadorFavorecidoInativo();
		this.cdEstadoCivilFavorecido = saida.getCdEstadoCivilFavorecido();
		this.cdUfEmpresaFavorecido = saida.getCdUfEmpresaFavorecido();
		this.dtNascimentoFavorecidoCliente = saida.getDtNascimentoFavorecidoCliente();
		this.dsEmpresaFavorecidoCliente = saida.getDsEmpresaFavorecidoCliente();
		this.dsMunicipioEmpresaFavorecido = saida.getDsMunicipioEmpresaFavorecido();
		this.dsConjugeFavorecidoCliente = saida.getDsConjugeFavorecidoCliente();
		this.dsNacionalidadeFavorecidoCliente = saida.getDsNacionalidadeFavorecidoCliente();
		this.dsProfissaoFavorecidoCliente = saida.getDsProfissaoFavorecidoCliente();
		this.dsCargoFavorecidoCliente = saida.getDsCargoFavorecidoCliente();
		this.dsUfEmpresaFavorecido = saida.getDsUfEmpresaFavorecido();
		this.dsMaeFavorecidoCliente = saida.getDsMaeFavorecidoCliente();
		this.dsPaiFavorecidoCliente = saida.getDsPaiFavorecidoCliente();
		this.dsMunicipalNascimentoFavorecido = saida.getDsMunicipalNascimentoFavorecido();
		this.dsUfNacionalidadeFavorecido = saida.getDsUfNacionalidadeFavorecido();
		this.cdUfNascimentoFavorecido = saida.getCdUfNascimentoFavorecido();
		this.vlRendaMensalFavorecido = saida.getVlRendaMensalFavorecido();
		this.dtAtualizacaoEnderecoResidencial = saida.getDtAtualizacaoEnderecoResidencial();
		this.dtAtualizacaoEnderecoCorrespondencia = saida.getDtAtualizacaoEnderecoCorrespondencia();
		this.cdIndicadorTipoManutencao = saida.getCdIndicadorTipoManutencao();
		this.dsTipoManutencao = saida.getDsTipoManutencao();
		this.cdUsuarioInclusao = saida.getCdUsuarioInclusao();
		this.cdUsuarioInclusaoExterno = saida.getCdUsuarioInclusaoExterno();
		this.dtInclusao = saida.getDtInclusao();
		this.hrInclusao = saida.getHrInclusao();
		this.cdOperacaoCanalInclusao = saida.getCdOperacaoCanalInclusao();
		this.cdTipoCanalInclusao = saida.getCdTipoCanalInclusao();
		this.dsCanalInclusao = saida.getDsCanalInclusao();
		this.cdUsuarioManutencao = saida.getCdUsuarioManutencao();
		this.cdUsuarioManutencaoEsteno = saida.getCdUsuarioManutencaoEsteno();
		this.dtManutencao = saida.getDtManutencao();
		this.hrManutencao = saida.getHrManutencao();
		this.cdOperacaoCanalManutencao = saida.getCdOperacaoCanalManutencao();
		this.cdTipoCanalManutencao = saida.getCdTipoCanalManutencao();
		this.dsCanalManutencao = saida.getDsCanalManutencao();
		this.cdAcaoManutencao = saida.getCdAcaoManutencao();
	}

	/**
	 * Get: sexo.
	 *
	 * @return sexo
	 */
	public String getSexo() {
		if (PgitUtil.verificaStringNula(getCdSexoFavorecidoCliente()).equalsIgnoreCase("M")) {
			return "MASCULINO";
		} else if (PgitUtil.verificaStringNula(getCdSexoFavorecidoCliente()).equalsIgnoreCase("F")) {
			return "FEMININO";
		} else {
			return " ";
		}
	}
	
	/**
	 * Get: telefoneResidencial.
	 *
	 * @return telefoneResidencial
	 */
	public String getTelefoneResidencial() {
		return NumberUtils.formatarFone(this.getCdAreaFavorecidoCliente(), this.getCdFoneFavorecidoCliente());
	}

	/**
	 * Get: telefoneFax.
	 *
	 * @return telefoneFax
	 */
	public String getTelefoneFax() {
		return NumberUtils.formatarFone(this.getCdAreaFaxFavorecido(), this.getCdFaxFavorecidoCliente());
	}

	/**
	 * Get: telefoneComercial.
	 *
	 * @return telefoneComercial
	 */
	public String getTelefoneComercial() {
		return NumberUtils.formatarFone(this.getCdAreaFoneComercial(), this.getCdFoneComercialFavorecido());
	}

	/**
	 * Get: telefoneCelular.
	 *
	 * @return telefoneCelular
	 */
	public String getTelefoneCelular() {
		return NumberUtils.formatarFone(this.getCdAreaCelularFavorecido(), this.getCdCelularFavorecidoCliente());
	}

	/**
	 * Get: indicadorRetorno.
	 *
	 * @return indicadorRetorno
	 */
	public String getIndicadorRetorno() {
		return cdIndicadorRetornoCliente == null ? "" :cdIndicadorRetornoCliente.equals(1) ? "RETORNO PENDENTE DE ENVIO" : "RETORNO ENVIADO";
	}

	/**
	 * Get: indicadorFavorecido.
	 *
	 * @return indicadorFavorecido
	 */
	public String getIndicadorFavorecido() {
		return cdIndicadorFavorecidoInativo == null ? "" : cdIndicadorFavorecidoInativo.equals(1) ? "SIM" : "N�O";
	}

	/**
	 * Get: estadoCivil.
	 *
	 * @return estadoCivil
	 */
	public String getEstadoCivil() {
	    	if (cdEstadoCivilFavorecido == null ){
	    	    return "";
	    	}
		if (cdEstadoCivilFavorecido.equals(1)) {
			return "SOLTEIRO";
		} else 	if (cdEstadoCivilFavorecido.equals(2)) {
			return "CASADO";
		} else 	if (cdEstadoCivilFavorecido.equals(3)) {
			return "DIVORCIADO";
		} else {
			return "VI�VO";
		}
	}

	/**
	 * Get: rendaMensal.
	 *
	 * @return rendaMensal
	 */
	public String getRendaMensal() {
		return NumberUtils.format(this.getVlRendaMensalFavorecido());
	}
	
	/**
	 * Get: cdAcaoManutencao.
	 *
	 * @return cdAcaoManutencao
	 */
	public Integer getCdAcaoManutencao() {
		return cdAcaoManutencao;
	}

	/**
	 * Set: cdAcaoManutencao.
	 *
	 * @param cdAcaoManutencao the cd acao manutencao
	 */
	public void setCdAcaoManutencao(Integer cdAcaoManutencao) {
		this.cdAcaoManutencao = cdAcaoManutencao;
	}

	/**
	 * Get: cdAreaCelularFavorecido.
	 *
	 * @return cdAreaCelularFavorecido
	 */
	public Integer getCdAreaCelularFavorecido() {
		return cdAreaCelularFavorecido;
	}

	/**
	 * Set: cdAreaCelularFavorecido.
	 *
	 * @param cdAreaCelularFavorecido the cd area celular favorecido
	 */
	public void setCdAreaCelularFavorecido(Integer cdAreaCelularFavorecido) {
		this.cdAreaCelularFavorecido = cdAreaCelularFavorecido;
	}

	/**
	 * Get: cdAreaFavorecidoCliente.
	 *
	 * @return cdAreaFavorecidoCliente
	 */
	public Integer getCdAreaFavorecidoCliente() {
		return cdAreaFavorecidoCliente;
	}

	/**
	 * Set: cdAreaFavorecidoCliente.
	 *
	 * @param cdAreaFavorecidoCliente the cd area favorecido cliente
	 */
	public void setCdAreaFavorecidoCliente(Integer cdAreaFavorecidoCliente) {
		this.cdAreaFavorecidoCliente = cdAreaFavorecidoCliente;
	}

	/**
	 * Get: cdAreaFaxFavorecido.
	 *
	 * @return cdAreaFaxFavorecido
	 */
	public Integer getCdAreaFaxFavorecido() {
		return cdAreaFaxFavorecido;
	}

	/**
	 * Set: cdAreaFaxFavorecido.
	 *
	 * @param cdAreaFaxFavorecido the cd area fax favorecido
	 */
	public void setCdAreaFaxFavorecido(Integer cdAreaFaxFavorecido) {
		this.cdAreaFaxFavorecido = cdAreaFaxFavorecido;
	}

	/**
	 * Get: cdAreaFoneComercial.
	 *
	 * @return cdAreaFoneComercial
	 */
	public Integer getCdAreaFoneComercial() {
		return cdAreaFoneComercial;
	}

	/**
	 * Set: cdAreaFoneComercial.
	 *
	 * @param cdAreaFoneComercial the cd area fone comercial
	 */
	public void setCdAreaFoneComercial(Integer cdAreaFoneComercial) {
		this.cdAreaFoneComercial = cdAreaFoneComercial;
	}

	/**
	 * Get: cdCelularFavorecidoCliente.
	 *
	 * @return cdCelularFavorecidoCliente
	 */
	public String getCdCelularFavorecidoCliente() {
		return cdCelularFavorecidoCliente;
	}

	/**
	 * Set: cdCelularFavorecidoCliente.
	 *
	 * @param cdCelularFavorecidoCliente the cd celular favorecido cliente
	 */
	public void setCdCelularFavorecidoCliente(String cdCelularFavorecidoCliente) {
		this.cdCelularFavorecidoCliente = cdCelularFavorecidoCliente;
	}

	/**
	 * Get: cdCepCorrespondenciaFavorecido.
	 *
	 * @return cdCepCorrespondenciaFavorecido
	 */
	public Integer getCdCepCorrespondenciaFavorecido() {
		return cdCepCorrespondenciaFavorecido;
	}

	/**
	 * Set: cdCepCorrespondenciaFavorecido.
	 *
	 * @param cdCepCorrespondenciaFavorecido the cd cep correspondencia favorecido
	 */
	public void setCdCepCorrespondenciaFavorecido(Integer cdCepCorrespondenciaFavorecido) {
		this.cdCepCorrespondenciaFavorecido = cdCepCorrespondenciaFavorecido;
	}

	/**
	 * Get: cdCepFavorecidoCliente.
	 *
	 * @return cdCepFavorecidoCliente
	 */
	public Integer getCdCepFavorecidoCliente() {
		return cdCepFavorecidoCliente;
	}

	/**
	 * Set: cdCepFavorecidoCliente.
	 *
	 * @param cdCepFavorecidoCliente the cd cep favorecido cliente
	 */
	public void setCdCepFavorecidoCliente(Integer cdCepFavorecidoCliente) {
		this.cdCepFavorecidoCliente = cdCepFavorecidoCliente;
	}

	/**
	 * Get: cdComplementoCepCorrespondencia.
	 *
	 * @return cdComplementoCepCorrespondencia
	 */
	public Integer getCdComplementoCepCorrespondencia() {
		return cdComplementoCepCorrespondencia;
	}

	/**
	 * Set: cdComplementoCepCorrespondencia.
	 *
	 * @param cdComplementoCepCorrespondencia the cd complemento cep correspondencia
	 */
	public void setCdComplementoCepCorrespondencia(Integer cdComplementoCepCorrespondencia) {
		this.cdComplementoCepCorrespondencia = cdComplementoCepCorrespondencia;
	}

	/**
	 * Get: cdComplementoCepFavorecido.
	 *
	 * @return cdComplementoCepFavorecido
	 */
	public Integer getCdComplementoCepFavorecido() {
		return cdComplementoCepFavorecido;
	}

	/**
	 * Set: cdComplementoCepFavorecido.
	 *
	 * @param cdComplementoCepFavorecido the cd complemento cep favorecido
	 */
	public void setCdComplementoCepFavorecido(Integer cdComplementoCepFavorecido) {
		this.cdComplementoCepFavorecido = cdComplementoCepFavorecido;
	}

	/**
	 * Get: cdEstadoCivilFavorecido.
	 *
	 * @return cdEstadoCivilFavorecido
	 */
	public Integer getCdEstadoCivilFavorecido() {
		return cdEstadoCivilFavorecido;
	}

	/**
	 * Set: cdEstadoCivilFavorecido.
	 *
	 * @param cdEstadoCivilFavorecido the cd estado civil favorecido
	 */
	public void setCdEstadoCivilFavorecido(Integer cdEstadoCivilFavorecido) {
		this.cdEstadoCivilFavorecido = cdEstadoCivilFavorecido;
	}

	/**
	 * Get: cdFaxFavorecidoCliente.
	 *
	 * @return cdFaxFavorecidoCliente
	 */
	public String getCdFaxFavorecidoCliente() {
		return cdFaxFavorecidoCliente;
	}

	/**
	 * Set: cdFaxFavorecidoCliente.
	 *
	 * @param cdFaxFavorecidoCliente the cd fax favorecido cliente
	 */
	public void setCdFaxFavorecidoCliente(String cdFaxFavorecidoCliente) {
		this.cdFaxFavorecidoCliente = cdFaxFavorecidoCliente;
	}

	/**
	 * Get: cdFoneComercialFavorecido.
	 *
	 * @return cdFoneComercialFavorecido
	 */
	public String getCdFoneComercialFavorecido() {
		return cdFoneComercialFavorecido;
	}

	/**
	 * Set: cdFoneComercialFavorecido.
	 *
	 * @param cdFoneComercialFavorecido the cd fone comercial favorecido
	 */
	public void setCdFoneComercialFavorecido(String cdFoneComercialFavorecido) {
		this.cdFoneComercialFavorecido = cdFoneComercialFavorecido;
	}

	/**
	 * Get: cdFoneFavorecidoCliente.
	 *
	 * @return cdFoneFavorecidoCliente
	 */
	public String getCdFoneFavorecidoCliente() {
		return cdFoneFavorecidoCliente;
	}

	/**
	 * Set: cdFoneFavorecidoCliente.
	 *
	 * @param cdFoneFavorecidoCliente the cd fone favorecido cliente
	 */
	public void setCdFoneFavorecidoCliente(String cdFoneFavorecidoCliente) {
		this.cdFoneFavorecidoCliente = cdFoneFavorecidoCliente;
	}

	/**
	 * Get: cdIndicadorFavorecidoInativo.
	 *
	 * @return cdIndicadorFavorecidoInativo
	 */
	public Integer getCdIndicadorFavorecidoInativo() {
		return cdIndicadorFavorecidoInativo;
	}

	/**
	 * Set: cdIndicadorFavorecidoInativo.
	 *
	 * @param cdIndicadorFavorecidoInativo the cd indicador favorecido inativo
	 */
	public void setCdIndicadorFavorecidoInativo(Integer cdIndicadorFavorecidoInativo) {
		this.cdIndicadorFavorecidoInativo = cdIndicadorFavorecidoInativo;
	}

	/**
	 * Get: cdIndicadorRetornoCliente.
	 *
	 * @return cdIndicadorRetornoCliente
	 */
	public Integer getCdIndicadorRetornoCliente() {
		return cdIndicadorRetornoCliente;
	}

	/**
	 * Set: cdIndicadorRetornoCliente.
	 *
	 * @param cdIndicadorRetornoCliente the cd indicador retorno cliente
	 */
	public void setCdIndicadorRetornoCliente(Integer cdIndicadorRetornoCliente) {
		this.cdIndicadorRetornoCliente = cdIndicadorRetornoCliente;
	}

	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}

	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}

	/**
	 * Get: cdInscricaoEstadualFavorecido.
	 *
	 * @return cdInscricaoEstadualFavorecido
	 */
	public Long getCdInscricaoEstadualFavorecido() {
		return cdInscricaoEstadualFavorecido;
	}

	/**
	 * Set: cdInscricaoEstadualFavorecido.
	 *
	 * @param cdInscricaoEstadualFavorecido the cd inscricao estadual favorecido
	 */
	public void setCdInscricaoEstadualFavorecido(Long cdInscricaoEstadualFavorecido) {
		this.cdInscricaoEstadualFavorecido = cdInscricaoEstadualFavorecido;
	}

	/**
	 * Get: cdInscricaoFavorecidoCliente.
	 *
	 * @return cdInscricaoFavorecidoCliente
	 */
	public Long getCdInscricaoFavorecidoCliente() {
		return cdInscricaoFavorecidoCliente;
	}

	/**
	 * Set: cdInscricaoFavorecidoCliente.
	 *
	 * @param cdInscricaoFavorecidoCliente the cd inscricao favorecido cliente
	 */
	public void setCdInscricaoFavorecidoCliente(Long cdInscricaoFavorecidoCliente) {
		this.cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
	}

	/**
	 * Get: cdMotivoBloqueioFavorecido.
	 *
	 * @return cdMotivoBloqueioFavorecido
	 */
	public Integer getCdMotivoBloqueioFavorecido() {
		return cdMotivoBloqueioFavorecido;
	}

	/**
	 * Set: cdMotivoBloqueioFavorecido.
	 *
	 * @param cdMotivoBloqueioFavorecido the cd motivo bloqueio favorecido
	 */
	public void setCdMotivoBloqueioFavorecido(Integer cdMotivoBloqueioFavorecido) {
		this.cdMotivoBloqueioFavorecido = cdMotivoBloqueioFavorecido;
	}

	/**
	 * Get: cdNitFavorecidoCliente.
	 *
	 * @return cdNitFavorecidoCliente
	 */
	public Long getCdNitFavorecidoCliente() {
		return cdNitFavorecidoCliente;
	}

	/**
	 * Set: cdNitFavorecidoCliente.
	 *
	 * @param cdNitFavorecidoCliente the cd nit favorecido cliente
	 */
	public void setCdNitFavorecidoCliente(Long cdNitFavorecidoCliente) {
		this.cdNitFavorecidoCliente = cdNitFavorecidoCliente;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdRamalComercialFavorecido.
	 *
	 * @return cdRamalComercialFavorecido
	 */
	public Integer getCdRamalComercialFavorecido() {
		return cdRamalComercialFavorecido;
	}

	/**
	 * Set: cdRamalComercialFavorecido.
	 *
	 * @param cdRamalComercialFavorecido the cd ramal comercial favorecido
	 */
	public void setCdRamalComercialFavorecido(Integer cdRamalComercialFavorecido) {
		this.cdRamalComercialFavorecido = cdRamalComercialFavorecido;
	}

	/**
	 * Get: cdRamalFaxFavorecido.
	 *
	 * @return cdRamalFaxFavorecido
	 */
	public Integer getCdRamalFaxFavorecido() {
		return cdRamalFaxFavorecido;
	}

	/**
	 * Set: cdRamalFaxFavorecido.
	 *
	 * @param cdRamalFaxFavorecido the cd ramal fax favorecido
	 */
	public void setCdRamalFaxFavorecido(Integer cdRamalFaxFavorecido) {
		this.cdRamalFaxFavorecido = cdRamalFaxFavorecido;
	}

	/**
	 * Get: cdSexoFavorecidoCliente.
	 *
	 * @return cdSexoFavorecidoCliente
	 */
	public String getCdSexoFavorecidoCliente() {
		return cdSexoFavorecidoCliente;
	}

	/**
	 * Set: cdSexoFavorecidoCliente.
	 *
	 * @param cdSexoFavorecidoCliente the cd sexo favorecido cliente
	 */
	public void setCdSexoFavorecidoCliente(String cdSexoFavorecidoCliente) {
		this.cdSexoFavorecidoCliente = cdSexoFavorecidoCliente;
	}

	/**
	 * Get: cdSiglaEstadoCorrespondencia.
	 *
	 * @return cdSiglaEstadoCorrespondencia
	 */
	public String getCdSiglaEstadoCorrespondencia() {
		return cdSiglaEstadoCorrespondencia;
	}

	/**
	 * Set: cdSiglaEstadoCorrespondencia.
	 *
	 * @param cdSiglaEstadoCorrespondencia the cd sigla estado correspondencia
	 */
	public void setCdSiglaEstadoCorrespondencia(String cdSiglaEstadoCorrespondencia) {
		this.cdSiglaEstadoCorrespondencia = cdSiglaEstadoCorrespondencia;
	}

	/**
	 * Get: cdSiglaEstadoFavorecidoCliente.
	 *
	 * @return cdSiglaEstadoFavorecidoCliente
	 */
	public String getCdSiglaEstadoFavorecidoCliente() {
		return cdSiglaEstadoFavorecidoCliente;
	}

	/**
	 * Set: cdSiglaEstadoFavorecidoCliente.
	 *
	 * @param cdSiglaEstadoFavorecidoCliente the cd sigla estado favorecido cliente
	 */
	public void setCdSiglaEstadoFavorecidoCliente(String cdSiglaEstadoFavorecidoCliente) {
		this.cdSiglaEstadoFavorecidoCliente = cdSiglaEstadoFavorecidoCliente;
	}

	/**
	 * Get: cdSiglaUfInscricaoEstadual.
	 *
	 * @return cdSiglaUfInscricaoEstadual
	 */
	public String getCdSiglaUfInscricaoEstadual() {
		return cdSiglaUfInscricaoEstadual;
	}

	/**
	 * Set: cdSiglaUfInscricaoEstadual.
	 *
	 * @param cdSiglaUfInscricaoEstadual the cd sigla uf inscricao estadual
	 */
	public void setCdSiglaUfInscricaoEstadual(String cdSiglaUfInscricaoEstadual) {
		this.cdSiglaUfInscricaoEstadual = cdSiglaUfInscricaoEstadual;
	}

	/**
	 * Get: cdSituacaoFavorecidoCliente.
	 *
	 * @return cdSituacaoFavorecidoCliente
	 */
	public Integer getCdSituacaoFavorecidoCliente() {
		return cdSituacaoFavorecidoCliente;
	}

	/**
	 * Set: cdSituacaoFavorecidoCliente.
	 *
	 * @param cdSituacaoFavorecidoCliente the cd situacao favorecido cliente
	 */
	public void setCdSituacaoFavorecidoCliente(Integer cdSituacaoFavorecidoCliente) {
		this.cdSituacaoFavorecidoCliente = cdSituacaoFavorecidoCliente;
	}

	/**
	 * Get: cdSmsFavorecidoCliente.
	 *
	 * @return cdSmsFavorecidoCliente
	 */
	public String getCdSmsFavorecidoCliente() {
		return cdSmsFavorecidoCliente;
	}

	/**
	 * Set: cdSmsFavorecidoCliente.
	 *
	 * @param cdSmsFavorecidoCliente the cd sms favorecido cliente
	 */
	public void setCdSmsFavorecidoCliente(String cdSmsFavorecidoCliente) {
		this.cdSmsFavorecidoCliente = cdSmsFavorecidoCliente;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoFavorecido.
	 *
	 * @return cdTipoFavorecido
	 */
	public Integer getCdTipoFavorecido() {
		return cdTipoFavorecido;
	}

	/**
	 * Set: cdTipoFavorecido.
	 *
	 * @param cdTipoFavorecido the cd tipo favorecido
	 */
	public void setCdTipoFavorecido(Integer cdTipoFavorecido) {
		this.cdTipoFavorecido = cdTipoFavorecido;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public Integer getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(Integer cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}

	/**
	 * Get: cdUfEmpresaFavorecido.
	 *
	 * @return cdUfEmpresaFavorecido
	 */
	public String getCdUfEmpresaFavorecido() {
		return cdUfEmpresaFavorecido;
	}

	/**
	 * Set: cdUfEmpresaFavorecido.
	 *
	 * @param cdUfEmpresaFavorecido the cd uf empresa favorecido
	 */
	public void setCdUfEmpresaFavorecido(String cdUfEmpresaFavorecido) {
		this.cdUfEmpresaFavorecido = cdUfEmpresaFavorecido;
	}

	/**
	 * Get: cdUfNascimentoFavorecido.
	 *
	 * @return cdUfNascimentoFavorecido
	 */
	public String getCdUfNascimentoFavorecido() {
		return cdUfNascimentoFavorecido;
	}

	/**
	 * Set: cdUfNascimentoFavorecido.
	 *
	 * @param cdUfNascimentoFavorecido the cd uf nascimento favorecido
	 */
	public void setCdUfNascimentoFavorecido(String cdUfNascimentoFavorecido) {
		this.cdUfNascimentoFavorecido = cdUfNascimentoFavorecido;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoEsteno.
	 *
	 * @return cdUsuarioManutencaoEsteno
	 */
	public String getCdUsuarioManutencaoEsteno() {
		return cdUsuarioManutencaoEsteno;
	}

	/**
	 * Set: cdUsuarioManutencaoEsteno.
	 *
	 * @param cdUsuarioManutencaoEsteno the cd usuario manutencao esteno
	 */
	public void setCdUsuarioManutencaoEsteno(String cdUsuarioManutencaoEsteno) {
		this.cdUsuarioManutencaoEsteno = cdUsuarioManutencaoEsteno;
	}

	/**
	 * Get: dsAbreviacaoFavorecidoCliente.
	 *
	 * @return dsAbreviacaoFavorecidoCliente
	 */
	public String getDsAbreviacaoFavorecidoCliente() {
		return dsAbreviacaoFavorecidoCliente;
	}

	/**
	 * Set: dsAbreviacaoFavorecidoCliente.
	 *
	 * @param dsAbreviacaoFavorecidoCliente the ds abreviacao favorecido cliente
	 */
	public void setDsAbreviacaoFavorecidoCliente(String dsAbreviacaoFavorecidoCliente) {
		this.dsAbreviacaoFavorecidoCliente = dsAbreviacaoFavorecidoCliente;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsCargoFavorecidoCliente.
	 *
	 * @return dsCargoFavorecidoCliente
	 */
	public String getDsCargoFavorecidoCliente() {
		return dsCargoFavorecidoCliente;
	}

	/**
	 * Set: dsCargoFavorecidoCliente.
	 *
	 * @param dsCargoFavorecidoCliente the ds cargo favorecido cliente
	 */
	public void setDsCargoFavorecidoCliente(String dsCargoFavorecidoCliente) {
		this.dsCargoFavorecidoCliente = dsCargoFavorecidoCliente;
	}

	/**
	 * Get: dsCodigoMotivoBloqueio.
	 *
	 * @return dsCodigoMotivoBloqueio
	 */
	public String getDsCodigoMotivoBloqueio() {
		return dsCodigoMotivoBloqueio;
	}

	/**
	 * Set: dsCodigoMotivoBloqueio.
	 *
	 * @param dsCodigoMotivoBloqueio the ds codigo motivo bloqueio
	 */
	public void setDsCodigoMotivoBloqueio(String dsCodigoMotivoBloqueio) {
		this.dsCodigoMotivoBloqueio = dsCodigoMotivoBloqueio;
	}

	/**
	 * Get: dsCodigoTipoFavorecido.
	 *
	 * @return dsCodigoTipoFavorecido
	 */
	public String getDsCodigoTipoFavorecido() {
		return dsCodigoTipoFavorecido;
	}

	/**
	 * Set: dsCodigoTipoFavorecido.
	 *
	 * @param dsCodigoTipoFavorecido the ds codigo tipo favorecido
	 */
	public void setDsCodigoTipoFavorecido(String dsCodigoTipoFavorecido) {
		this.dsCodigoTipoFavorecido = dsCodigoTipoFavorecido;
	}

	/**
	 * Get: dsCodigoTipoInscricao.
	 *
	 * @return dsCodigoTipoInscricao
	 */
	public String getDsCodigoTipoInscricao() {
		return dsCodigoTipoInscricao;
	}

	/**
	 * Set: dsCodigoTipoInscricao.
	 *
	 * @param dsCodigoTipoInscricao the ds codigo tipo inscricao
	 */
	public void setDsCodigoTipoInscricao(String dsCodigoTipoInscricao) {
		this.dsCodigoTipoInscricao = dsCodigoTipoInscricao;
	}

	/**
	 * Get: dsComplementoFavorecidoCliente.
	 *
	 * @return dsComplementoFavorecidoCliente
	 */
	public String getDsComplementoFavorecidoCliente() {
		return dsComplementoFavorecidoCliente;
	}

	/**
	 * Set: dsComplementoFavorecidoCliente.
	 *
	 * @param dsComplementoFavorecidoCliente the ds complemento favorecido cliente
	 */
	public void setDsComplementoFavorecidoCliente(String dsComplementoFavorecidoCliente) {
		this.dsComplementoFavorecidoCliente = dsComplementoFavorecidoCliente;
	}

	/**
	 * Get: dsConjugeFavorecidoCliente.
	 *
	 * @return dsConjugeFavorecidoCliente
	 */
	public String getDsConjugeFavorecidoCliente() {
		return dsConjugeFavorecidoCliente;
	}

	/**
	 * Set: dsConjugeFavorecidoCliente.
	 *
	 * @param dsConjugeFavorecidoCliente the ds conjuge favorecido cliente
	 */
	public void setDsConjugeFavorecidoCliente(String dsConjugeFavorecidoCliente) {
		this.dsConjugeFavorecidoCliente = dsConjugeFavorecidoCliente;
	}

	/**
	 * Get: dsEmpresaFavorecidoCliente.
	 *
	 * @return dsEmpresaFavorecidoCliente
	 */
	public String getDsEmpresaFavorecidoCliente() {
		return dsEmpresaFavorecidoCliente;
	}

	/**
	 * Set: dsEmpresaFavorecidoCliente.
	 *
	 * @param dsEmpresaFavorecidoCliente the ds empresa favorecido cliente
	 */
	public void setDsEmpresaFavorecidoCliente(String dsEmpresaFavorecidoCliente) {
		this.dsEmpresaFavorecidoCliente = dsEmpresaFavorecidoCliente;
	}

	/**
	 * Get: dsEstadoCorrespondenciaFavorecido.
	 *
	 * @return dsEstadoCorrespondenciaFavorecido
	 */
	public String getDsEstadoCorrespondenciaFavorecido() {
		return dsEstadoCorrespondenciaFavorecido;
	}

	/**
	 * Set: dsEstadoCorrespondenciaFavorecido.
	 *
	 * @param dsEstadoCorrespondenciaFavorecido the ds estado correspondencia favorecido
	 */
	public void setDsEstadoCorrespondenciaFavorecido(String dsEstadoCorrespondenciaFavorecido) {
		this.dsEstadoCorrespondenciaFavorecido = dsEstadoCorrespondenciaFavorecido;
	}

	/**
	 * Get: dsEstadoFavorecidoCliente.
	 *
	 * @return dsEstadoFavorecidoCliente
	 */
	public String getDsEstadoFavorecidoCliente() {
		return dsEstadoFavorecidoCliente;
	}

	/**
	 * Set: dsEstadoFavorecidoCliente.
	 *
	 * @param dsEstadoFavorecidoCliente the ds estado favorecido cliente
	 */
	public void setDsEstadoFavorecidoCliente(String dsEstadoFavorecidoCliente) {
		this.dsEstadoFavorecidoCliente = dsEstadoFavorecidoCliente;
	}

	/**
	 * Get: dsMaeFavorecidoCliente.
	 *
	 * @return dsMaeFavorecidoCliente
	 */
	public String getDsMaeFavorecidoCliente() {
		return dsMaeFavorecidoCliente;
	}

	/**
	 * Set: dsMaeFavorecidoCliente.
	 *
	 * @param dsMaeFavorecidoCliente the ds mae favorecido cliente
	 */
	public void setDsMaeFavorecidoCliente(String dsMaeFavorecidoCliente) {
		this.dsMaeFavorecidoCliente = dsMaeFavorecidoCliente;
	}

	/**
	 * Get: dsMunicipalNascimentoFavorecido.
	 *
	 * @return dsMunicipalNascimentoFavorecido
	 */
	public String getDsMunicipalNascimentoFavorecido() {
		return dsMunicipalNascimentoFavorecido;
	}

	/**
	 * Set: dsMunicipalNascimentoFavorecido.
	 *
	 * @param dsMunicipalNascimentoFavorecido the ds municipal nascimento favorecido
	 */
	public void setDsMunicipalNascimentoFavorecido(String dsMunicipalNascimentoFavorecido) {
		this.dsMunicipalNascimentoFavorecido = dsMunicipalNascimentoFavorecido;
	}

	/**
	 * Get: dsMunicipioEmpresaFavorecido.
	 *
	 * @return dsMunicipioEmpresaFavorecido
	 */
	public String getDsMunicipioEmpresaFavorecido() {
		return dsMunicipioEmpresaFavorecido;
	}

	/**
	 * Set: dsMunicipioEmpresaFavorecido.
	 *
	 * @param dsMunicipioEmpresaFavorecido the ds municipio empresa favorecido
	 */
	public void setDsMunicipioEmpresaFavorecido(String dsMunicipioEmpresaFavorecido) {
		this.dsMunicipioEmpresaFavorecido = dsMunicipioEmpresaFavorecido;
	}

	/**
	 * Get: dsMunicipioFavorecidoCliente.
	 *
	 * @return dsMunicipioFavorecidoCliente
	 */
	public String getDsMunicipioFavorecidoCliente() {
		return dsMunicipioFavorecidoCliente;
	}

	/**
	 * Set: dsMunicipioFavorecidoCliente.
	 *
	 * @param dsMunicipioFavorecidoCliente the ds municipio favorecido cliente
	 */
	public void setDsMunicipioFavorecidoCliente(String dsMunicipioFavorecidoCliente) {
		this.dsMunicipioFavorecidoCliente = dsMunicipioFavorecidoCliente;
	}

	/**
	 * Get: dsMunicipioInscricaoEstadual.
	 *
	 * @return dsMunicipioInscricaoEstadual
	 */
	public String getDsMunicipioInscricaoEstadual() {
		return dsMunicipioInscricaoEstadual;
	}

	/**
	 * Set: dsMunicipioInscricaoEstadual.
	 *
	 * @param dsMunicipioInscricaoEstadual the ds municipio inscricao estadual
	 */
	public void setDsMunicipioInscricaoEstadual(String dsMunicipioInscricaoEstadual) {
		this.dsMunicipioInscricaoEstadual = dsMunicipioInscricaoEstadual;
	}

	/**
	 * Get: dsNacionalidadeFavorecidoCliente.
	 *
	 * @return dsNacionalidadeFavorecidoCliente
	 */
	public String getDsNacionalidadeFavorecidoCliente() {
		return dsNacionalidadeFavorecidoCliente;
	}

	/**
	 * Set: dsNacionalidadeFavorecidoCliente.
	 *
	 * @param dsNacionalidadeFavorecidoCliente the ds nacionalidade favorecido cliente
	 */
	public void setDsNacionalidadeFavorecidoCliente(String dsNacionalidadeFavorecidoCliente) {
		this.dsNacionalidadeFavorecidoCliente = dsNacionalidadeFavorecidoCliente;
	}

	/**
	 * Get: dsObservacaoControleFavorecido.
	 *
	 * @return dsObservacaoControleFavorecido
	 */
	public String getDsObservacaoControleFavorecido() {
		return dsObservacaoControleFavorecido;
	}

	/**
	 * Set: dsObservacaoControleFavorecido.
	 *
	 * @param dsObservacaoControleFavorecido the ds observacao controle favorecido
	 */
	public void setDsObservacaoControleFavorecido(String dsObservacaoControleFavorecido) {
		this.dsObservacaoControleFavorecido = dsObservacaoControleFavorecido;
	}

	/**
	 * Get: dsOrgaoEmissorDocumento.
	 *
	 * @return dsOrgaoEmissorDocumento
	 */
	public String getDsOrgaoEmissorDocumento() {
		return dsOrgaoEmissorDocumento;
	}

	/**
	 * Set: dsOrgaoEmissorDocumento.
	 *
	 * @param dsOrgaoEmissorDocumento the ds orgao emissor documento
	 */
	public void setDsOrgaoEmissorDocumento(String dsOrgaoEmissorDocumento) {
		this.dsOrgaoEmissorDocumento = dsOrgaoEmissorDocumento;
	}

	/**
	 * Get: dsPaiFavorecidoCliente.
	 *
	 * @return dsPaiFavorecidoCliente
	 */
	public String getDsPaiFavorecidoCliente() {
		return dsPaiFavorecidoCliente;
	}

	/**
	 * Set: dsPaiFavorecidoCliente.
	 *
	 * @param dsPaiFavorecidoCliente the ds pai favorecido cliente
	 */
	public void setDsPaiFavorecidoCliente(String dsPaiFavorecidoCliente) {
		this.dsPaiFavorecidoCliente = dsPaiFavorecidoCliente;
	}

	/**
	 * Get: dsProfissaoFavorecidoCliente.
	 *
	 * @return dsProfissaoFavorecidoCliente
	 */
	public String getDsProfissaoFavorecidoCliente() {
		return dsProfissaoFavorecidoCliente;
	}

	/**
	 * Set: dsProfissaoFavorecidoCliente.
	 *
	 * @param dsProfissaoFavorecidoCliente the ds profissao favorecido cliente
	 */
	public void setDsProfissaoFavorecidoCliente(String dsProfissaoFavorecidoCliente) {
		this.dsProfissaoFavorecidoCliente = dsProfissaoFavorecidoCliente;
	}

	/**
	 * Get: dsRetornoCliente.
	 *
	 * @return dsRetornoCliente
	 */
	public String getDsRetornoCliente() {
		return dsRetornoCliente;
	}

	/**
	 * Set: dsRetornoCliente.
	 *
	 * @param dsRetornoCliente the ds retorno cliente
	 */
	public void setDsRetornoCliente(String dsRetornoCliente) {
		this.dsRetornoCliente = dsRetornoCliente;
	}

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: dsUfEmpresaFavorecido.
	 *
	 * @return dsUfEmpresaFavorecido
	 */
	public String getDsUfEmpresaFavorecido() {
		return dsUfEmpresaFavorecido;
	}

	/**
	 * Set: dsUfEmpresaFavorecido.
	 *
	 * @param dsUfEmpresaFavorecido the ds uf empresa favorecido
	 */
	public void setDsUfEmpresaFavorecido(String dsUfEmpresaFavorecido) {
		this.dsUfEmpresaFavorecido = dsUfEmpresaFavorecido;
	}

	/**
	 * Get: dsUfNacionalidadeFavorecido.
	 *
	 * @return dsUfNacionalidadeFavorecido
	 */
	public String getDsUfNacionalidadeFavorecido() {
		return dsUfNacionalidadeFavorecido;
	}

	/**
	 * Set: dsUfNacionalidadeFavorecido.
	 *
	 * @param dsUfNacionalidadeFavorecido the ds uf nacionalidade favorecido
	 */
	public void setDsUfNacionalidadeFavorecido(String dsUfNacionalidadeFavorecido) {
		this.dsUfNacionalidadeFavorecido = dsUfNacionalidadeFavorecido;
	}

	/**
	 * Get: dsUltimaMovimentacaoFavorecido.
	 *
	 * @return dsUltimaMovimentacaoFavorecido
	 */
	public String getDsUltimaMovimentacaoFavorecido() {
		return dsUltimaMovimentacaoFavorecido;
	}

	/**
	 * Set: dsUltimaMovimentacaoFavorecido.
	 *
	 * @param dsUltimaMovimentacaoFavorecido the ds ultima movimentacao favorecido
	 */
	public void setDsUltimaMovimentacaoFavorecido(String dsUltimaMovimentacaoFavorecido) {
		this.dsUltimaMovimentacaoFavorecido = dsUltimaMovimentacaoFavorecido;
	}

	/**
	 * Get: dtAtualizacaoEnderecoCorrespondencia.
	 *
	 * @return dtAtualizacaoEnderecoCorrespondencia
	 */
	public String getDtAtualizacaoEnderecoCorrespondencia() {
		return dtAtualizacaoEnderecoCorrespondencia;
	}

	/**
	 * Set: dtAtualizacaoEnderecoCorrespondencia.
	 *
	 * @param dtAtualizacaoEnderecoCorrespondencia the dt atualizacao endereco correspondencia
	 */
	public void setDtAtualizacaoEnderecoCorrespondencia(String dtAtualizacaoEnderecoCorrespondencia) {
		this.dtAtualizacaoEnderecoCorrespondencia = dtAtualizacaoEnderecoCorrespondencia;
	}

	/**
	 * Get: dtAtualizacaoEnderecoResidencial.
	 *
	 * @return dtAtualizacaoEnderecoResidencial
	 */
	public String getDtAtualizacaoEnderecoResidencial() {
		return dtAtualizacaoEnderecoResidencial;
	}

	/**
	 * Set: dtAtualizacaoEnderecoResidencial.
	 *
	 * @param dtAtualizacaoEnderecoResidencial the dt atualizacao endereco residencial
	 */
	public void setDtAtualizacaoEnderecoResidencial(String dtAtualizacaoEnderecoResidencial) {
		this.dtAtualizacaoEnderecoResidencial = dtAtualizacaoEnderecoResidencial;
	}

	/**
	 * Get: dtExpedicaoDocumentoFavorecido.
	 *
	 * @return dtExpedicaoDocumentoFavorecido
	 */
	public String getDtExpedicaoDocumentoFavorecido() {
		return dtExpedicaoDocumentoFavorecido;
	}

	/**
	 * Set: dtExpedicaoDocumentoFavorecido.
	 *
	 * @param dtExpedicaoDocumentoFavorecido the dt expedicao documento favorecido
	 */
	public void setDtExpedicaoDocumentoFavorecido(String dtExpedicaoDocumentoFavorecido) {
		this.dtExpedicaoDocumentoFavorecido = dtExpedicaoDocumentoFavorecido;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: dtNascimentoFavorecidoCliente.
	 *
	 * @return dtNascimentoFavorecidoCliente
	 */
	public String getDtNascimentoFavorecidoCliente() {
		return dtNascimentoFavorecidoCliente;
	}

	/**
	 * Set: dtNascimentoFavorecidoCliente.
	 *
	 * @param dtNascimentoFavorecidoCliente the dt nascimento favorecido cliente
	 */
	public void setDtNascimentoFavorecidoCliente(String dtNascimentoFavorecidoCliente) {
		this.dtNascimentoFavorecidoCliente = dtNascimentoFavorecidoCliente;
	}

	/**
	 * Get: enBairroCorrespondenciaFavorecido.
	 *
	 * @return enBairroCorrespondenciaFavorecido
	 */
	public String getEnBairroCorrespondenciaFavorecido() {
		return enBairroCorrespondenciaFavorecido;
	}

	/**
	 * Set: enBairroCorrespondenciaFavorecido.
	 *
	 * @param enBairroCorrespondenciaFavorecido the en bairro correspondencia favorecido
	 */
	public void setEnBairroCorrespondenciaFavorecido(String enBairroCorrespondenciaFavorecido) {
		this.enBairroCorrespondenciaFavorecido = enBairroCorrespondenciaFavorecido;
	}

	/**
	 * Get: enBairroFavorecidoCliente.
	 *
	 * @return enBairroFavorecidoCliente
	 */
	public String getEnBairroFavorecidoCliente() {
		return enBairroFavorecidoCliente;
	}

	/**
	 * Set: enBairroFavorecidoCliente.
	 *
	 * @param enBairroFavorecidoCliente the en bairro favorecido cliente
	 */
	public void setEnBairroFavorecidoCliente(String enBairroFavorecidoCliente) {
		this.enBairroFavorecidoCliente = enBairroFavorecidoCliente;
	}

	/**
	 * Get: enComplementoLogradouroCorrespondencia.
	 *
	 * @return enComplementoLogradouroCorrespondencia
	 */
	public String getEnComplementoLogradouroCorrespondencia() {
		return enComplementoLogradouroCorrespondencia;
	}

	/**
	 * Set: enComplementoLogradouroCorrespondencia.
	 *
	 * @param enComplementoLogradouroCorrespondencia the en complemento logradouro correspondencia
	 */
	public void setEnComplementoLogradouroCorrespondencia(String enComplementoLogradouroCorrespondencia) {
		this.enComplementoLogradouroCorrespondencia = enComplementoLogradouroCorrespondencia;
	}

	/**
	 * Get: enComplementoLogradouroFavorecido.
	 *
	 * @return enComplementoLogradouroFavorecido
	 */
	public String getEnComplementoLogradouroFavorecido() {
		return enComplementoLogradouroFavorecido;
	}

	/**
	 * Set: enComplementoLogradouroFavorecido.
	 *
	 * @param enComplementoLogradouroFavorecido the en complemento logradouro favorecido
	 */
	public void setEnComplementoLogradouroFavorecido(String enComplementoLogradouroFavorecido) {
		this.enComplementoLogradouroFavorecido = enComplementoLogradouroFavorecido;
	}

	/**
	 * Get: enEmailComercialFavorecido.
	 *
	 * @return enEmailComercialFavorecido
	 */
	public String getEnEmailComercialFavorecido() {
		return enEmailComercialFavorecido;
	}

	/**
	 * Set: enEmailComercialFavorecido.
	 *
	 * @param enEmailComercialFavorecido the en email comercial favorecido
	 */
	public void setEnEmailComercialFavorecido(String enEmailComercialFavorecido) {
		this.enEmailComercialFavorecido = enEmailComercialFavorecido;
	}

	/**
	 * Get: enEmailFavorecidoCliente.
	 *
	 * @return enEmailFavorecidoCliente
	 */
	public String getEnEmailFavorecidoCliente() {
		return enEmailFavorecidoCliente;
	}

	/**
	 * Set: enEmailFavorecidoCliente.
	 *
	 * @param enEmailFavorecidoCliente the en email favorecido cliente
	 */
	public void setEnEmailFavorecidoCliente(String enEmailFavorecidoCliente) {
		this.enEmailFavorecidoCliente = enEmailFavorecidoCliente;
	}

	/**
	 * Get: enLogradouroCorrespondenciaFavorecido.
	 *
	 * @return enLogradouroCorrespondenciaFavorecido
	 */
	public String getEnLogradouroCorrespondenciaFavorecido() {
		return enLogradouroCorrespondenciaFavorecido;
	}

	/**
	 * Set: enLogradouroCorrespondenciaFavorecido.
	 *
	 * @param enLogradouroCorrespondenciaFavorecido the en logradouro correspondencia favorecido
	 */
	public void setEnLogradouroCorrespondenciaFavorecido(String enLogradouroCorrespondenciaFavorecido) {
		this.enLogradouroCorrespondenciaFavorecido = enLogradouroCorrespondenciaFavorecido;
	}

	/**
	 * Get: enLogradouroFavorecidoCliente.
	 *
	 * @return enLogradouroFavorecidoCliente
	 */
	public String getEnLogradouroFavorecidoCliente() {
		return enLogradouroFavorecidoCliente;
	}

	/**
	 * Set: enLogradouroFavorecidoCliente.
	 *
	 * @param enLogradouroFavorecidoCliente the en logradouro favorecido cliente
	 */
	public void setEnLogradouroFavorecidoCliente(String enLogradouroFavorecidoCliente) {
		this.enLogradouroFavorecidoCliente = enLogradouroFavorecidoCliente;
	}

	/**
	 * Get: enNumeroLogradouroCorrespondencia.
	 *
	 * @return enNumeroLogradouroCorrespondencia
	 */
	public String getEnNumeroLogradouroCorrespondencia() {
		return enNumeroLogradouroCorrespondencia;
	}

	/**
	 * Set: enNumeroLogradouroCorrespondencia.
	 *
	 * @param enNumeroLogradouroCorrespondencia the en numero logradouro correspondencia
	 */
	public void setEnNumeroLogradouroCorrespondencia(String enNumeroLogradouroCorrespondencia) {
		this.enNumeroLogradouroCorrespondencia = enNumeroLogradouroCorrespondencia;
	}

	/**
	 * Get: enNumeroLogradouroFavorecido.
	 *
	 * @return enNumeroLogradouroFavorecido
	 */
	public String getEnNumeroLogradouroFavorecido() {
		return enNumeroLogradouroFavorecido;
	}

	/**
	 * Set: enNumeroLogradouroFavorecido.
	 *
	 * @param enNumeroLogradouroFavorecido the en numero logradouro favorecido
	 */
	public void setEnNumeroLogradouroFavorecido(String enNumeroLogradouroFavorecido) {
		this.enNumeroLogradouroFavorecido = enNumeroLogradouroFavorecido;
	}

	/**
	 * Get: hrBloqueioFavorecidoCliente.
	 *
	 * @return hrBloqueioFavorecidoCliente
	 */
	public String getHrBloqueioFavorecidoCliente() {
		return hrBloqueioFavorecidoCliente;
	}

	/**
	 * Set: hrBloqueioFavorecidoCliente.
	 *
	 * @param hrBloqueioFavorecidoCliente the hr bloqueio favorecido cliente
	 */
	public void setHrBloqueioFavorecidoCliente(String hrBloqueioFavorecidoCliente) {
		this.hrBloqueioFavorecidoCliente = hrBloqueioFavorecidoCliente;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: vlRendaMensalFavorecido.
	 *
	 * @return vlRendaMensalFavorecido
	 */
	public Double getVlRendaMensalFavorecido() {
		return vlRendaMensalFavorecido;
	}

	/**
	 * Set: vlRendaMensalFavorecido.
	 *
	 * @param vlRendaMensalFavorecido the vl renda mensal favorecido
	 */
	public void setVlRendaMensalFavorecido(Double vlRendaMensalFavorecido) {
		this.vlRendaMensalFavorecido = vlRendaMensalFavorecido;
	}
	
	// Segue o exemplo, por�m a l�gica varia de campo para campo
	
    /**
	 * Get: dsCepFavorecidoCliente.
	 *
	 * @return dsCepFavorecidoCliente
	 */
	public String getDsCepFavorecidoCliente(){
	
    	return getCdCepFavorecidoCliente() == null || getCdComplementoCepFavorecido() == null ? "" :this.getCdCepFavorecidoCliente().equals(0) &&  this.getCdComplementoCepFavorecido().equals(0) ? ""  : this.getCdCepFavorecidoCliente().toString() + " - " + this.getCdComplementoCepFavorecido().toString();
    }
    
    
}
