/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadproccontrole.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadproccontrole.bean;

import java.io.Serializable;

/**
 * Nome: ProcessoMassivoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ProcessoMassivoEntradaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -5543441235697104247L;

	/** Atributo cdSistema. */
	private String cdSistema;
	
	/** Atributo dsSistema. */
	private String dsSistema;

    /** Atributo cdTipoProcessoSistema. */
    private Integer cdTipoProcessoSistema;
    
    /** Atributo dsTipoProcessoSistema. */
    private String dsTipoProcessoSistema;

    /** Atributo cdPeriodicidade. */
    private Integer cdPeriodicidade;
    
    /** Atributo cdProcessoSistema. */
    private Integer cdProcessoSistema;
    
    /** Atributo dsProcessoSistema. */
    private String dsProcessoSistema;
    
    /** Atributo cdNetProcessamentoPgto. */
    private String cdNetProcessamentoPgto;
    
    /** Atributo cdJobProcessamentoPgto. */
    private String cdJobProcessamentoPgto;

    /** Atributo dsPeriodicidade. */
    private String dsPeriodicidade;

    /** Atributo cdSituacaoProcessoSistema. */
    private Integer cdSituacaoProcessoSistema;

    /** Atributo dsBloqueioProcessoSistema. */
    private String dsBloqueioProcessoSistema;

	/**
	 * Get: cdPeriodicidade.
	 *
	 * @return cdPeriodicidade
	 */
	public Integer getCdPeriodicidade() {
		return cdPeriodicidade;
	}

	/**
	 * Set: cdPeriodicidade.
	 *
	 * @param cdPeriodicidade the cd periodicidade
	 */
	public void setCdPeriodicidade(Integer cdPeriodicidade) {
		this.cdPeriodicidade = cdPeriodicidade;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: cdTipoProcessoSistema.
	 *
	 * @return cdTipoProcessoSistema
	 */
	public Integer getCdTipoProcessoSistema() {
		return cdTipoProcessoSistema;
	}

	/**
	 * Set: cdTipoProcessoSistema.
	 *
	 * @param cdTipoProcessoSistema the cd tipo processo sistema
	 */
	public void setCdTipoProcessoSistema(Integer cdTipoProcessoSistema) {
		this.cdTipoProcessoSistema = cdTipoProcessoSistema;
	}

	/**
	 * Get: cdJobProcessamentoPgto.
	 *
	 * @return cdJobProcessamentoPgto
	 */
	public String getCdJobProcessamentoPgto() {
		return cdJobProcessamentoPgto;
	}

	/**
	 * Set: cdJobProcessamentoPgto.
	 *
	 * @param cdJobProcessamentoPgto the cd job processamento pgto
	 */
	public void setCdJobProcessamentoPgto(String cdJobProcessamentoPgto) {
		this.cdJobProcessamentoPgto = cdJobProcessamentoPgto;
	}

	/**
	 * Get: cdNetProcessamentoPgto.
	 *
	 * @return cdNetProcessamentoPgto
	 */
	public String getCdNetProcessamentoPgto() {
		return cdNetProcessamentoPgto;
	}

	/**
	 * Set: cdNetProcessamentoPgto.
	 *
	 * @param cdNetProcessamentoPgto the cd net processamento pgto
	 */
	public void setCdNetProcessamentoPgto(String cdNetProcessamentoPgto) {
		this.cdNetProcessamentoPgto = cdNetProcessamentoPgto;
	}

	/**
	 * Get: cdProcessoSistema.
	 *
	 * @return cdProcessoSistema
	 */
	public Integer getCdProcessoSistema() {
		return cdProcessoSistema;
	}

	/**
	 * Set: cdProcessoSistema.
	 *
	 * @param cdProcessoSistema the cd processo sistema
	 */
	public void setCdProcessoSistema(Integer cdProcessoSistema) {
		this.cdProcessoSistema = cdProcessoSistema;
	}

	/**
	 * Get: dsProcessoSistema.
	 *
	 * @return dsProcessoSistema
	 */
	public String getDsProcessoSistema() {
		return dsProcessoSistema;
	}

	/**
	 * Set: dsProcessoSistema.
	 *
	 * @param dsProcessoSistema the ds processo sistema
	 */
	public void setDsProcessoSistema(String dsProcessoSistema) {
		this.dsProcessoSistema = dsProcessoSistema;
	}

	/**
	 * Get: dsPeriodicidade.
	 *
	 * @return dsPeriodicidade
	 */
	public String getDsPeriodicidade() {
		return dsPeriodicidade;
	}

	/**
	 * Set: dsPeriodicidade.
	 *
	 * @param dsPeriodicidade the ds periodicidade
	 */
	public void setDsPeriodicidade(String dsPeriodicidade) {
		this.dsPeriodicidade = dsPeriodicidade;
	}

	/**
	 * Get: dsTipoProcessoSistema.
	 *
	 * @return dsTipoProcessoSistema
	 */
	public String getDsTipoProcessoSistema() {
		return dsTipoProcessoSistema;
	}

	/**
	 * Set: dsTipoProcessoSistema.
	 *
	 * @param dsTipoProcessoSistema the ds tipo processo sistema
	 */
	public void setDsTipoProcessoSistema(String dsTipoProcessoSistema) {
		this.dsTipoProcessoSistema = dsTipoProcessoSistema;
	}

	/**
	 * Get: cdSituacaoProcessoSistema.
	 *
	 * @return cdSituacaoProcessoSistema
	 */
	public Integer getCdSituacaoProcessoSistema() {
		return cdSituacaoProcessoSistema;
	}

	/**
	 * Set: cdSituacaoProcessoSistema.
	 *
	 * @param cdSituacaoProcessoSistema the cd situacao processo sistema
	 */
	public void setCdSituacaoProcessoSistema(Integer cdSituacaoProcessoSistema) {
		this.cdSituacaoProcessoSistema = cdSituacaoProcessoSistema;
	}

	/**
	 * Get: dsBloqueioProcessoSistema.
	 *
	 * @return dsBloqueioProcessoSistema
	 */
	public String getDsBloqueioProcessoSistema() {
		return dsBloqueioProcessoSistema;
	}

	/**
	 * Set: dsBloqueioProcessoSistema.
	 *
	 * @param dsBloqueioProcessoSistema the ds bloqueio processo sistema
	 */
	public void setDsBloqueioProcessoSistema(String dsBloqueioProcessoSistema) {
		this.dsBloqueioProcessoSistema = dsBloqueioProcessoSistema;
	}

	/**
	 * Get: dsSistema.
	 *
	 * @return dsSistema
	 */
	public String getDsSistema() {
		return dsSistema;
	}

	/**
	 * Set: dsSistema.
	 *
	 * @param dsSistema the ds sistema
	 */
	public void setDsSistema(String dsSistema) {
		this.dsSistema = dsSistema;
	}

}
