/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean;

import java.math.BigDecimal;

/**
 * Nome: AlterarLiberacaoPreviaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarLiberacaoPreviaEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoSolicitacao. */
	private Integer cdTipoSolicitacao;
	
	/** Atributo nrSolicitacao. */
	private Long nrSolicitacao;
	
	/** Atributo dtProgramada. */
	private String dtProgramada;
	
	/** Atributo vlAutorizado. */
	private BigDecimal vlAutorizado;
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoSolicitacao.
	 *
	 * @return cdTipoSolicitacao
	 */
	public Integer getCdTipoSolicitacao() {
		return cdTipoSolicitacao;
	}
	
	/**
	 * Set: cdTipoSolicitacao.
	 *
	 * @param cdTipoSolicitacao the cd tipo solicitacao
	 */
	public void setCdTipoSolicitacao(Integer cdTipoSolicitacao) {
		this.cdTipoSolicitacao = cdTipoSolicitacao;
	}
	
	/**
	 * Get: dtProgramada.
	 *
	 * @return dtProgramada
	 */
	public String getDtProgramada() {
		return dtProgramada;
	}
	
	/**
	 * Set: dtProgramada.
	 *
	 * @param dtProgramada the dt programada
	 */
	public void setDtProgramada(String dtProgramada) {
		this.dtProgramada = dtProgramada;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Long getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Long nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: vlAutorizado.
	 *
	 * @return vlAutorizado
	 */
	public BigDecimal getVlAutorizado() {
		return vlAutorizado;
	}
	
	/**
	 * Set: vlAutorizado.
	 *
	 * @param vlAutorizado the vl autorizado
	 */
	public void setVlAutorizado(BigDecimal vlAutorizado) {
		this.vlAutorizado = vlAutorizado;
	}
	
	
	


}
