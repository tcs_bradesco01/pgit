/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.ConsultarPagtoPendIndividualEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.ConsultarPagtoPendIndividualSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoIndividualPendenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoIndividualPendenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoPendIndividualEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoPendIndividualSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: LiberarPagtosIndividuaisPend
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ILiberarPagtosIndividuaisPendService {

	/**
	 * Consultar pagto pend individual.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar pagto pend individual saida dt o>
	 */
	List<ConsultarPagtoPendIndividualSaidaDTO> consultarPagtoPendIndividual(ConsultarPagtoPendIndividualEntradaDTO entradaDTO);
	
	/**
	 * Liberar pagto individual pendencia.
	 *
	 * @param listaEntrada the lista entrada
	 * @return the liberar pagto individual pendencia saida dto
	 */
	LiberarPagtoIndividualPendenciaSaidaDTO liberarPagtoIndividualPendencia(List<LiberarPagtoIndividualPendenciaEntradaDTO> listaEntrada);
	
	/**
	 * Liberar pagto pend individual.
	 *
	 * @param listaEntrada the lista entrada
	 * @return the liberar pagto pend individual saida dto
	 */
	LiberarPagtoPendIndividualSaidaDTO liberarPagtoPendIndividual(List<LiberarPagtoPendIndividualEntradaDTO> listaEntrada);
}