/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoProcsSistema
     */
    private int _cdTipoProcsSistema = 0;

    /**
     * keeps track of state for field: _cdTipoProcsSistema
     */
    private boolean _has_cdTipoProcsSistema;

    /**
     * Field _dsTipoProcsSistema
     */
    private java.lang.String _dsTipoProcsSistema;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoProcsSistema
     * 
     */
    public void deleteCdTipoProcsSistema()
    {
        this._has_cdTipoProcsSistema= false;
    } //-- void deleteCdTipoProcsSistema() 

    /**
     * Returns the value of field 'cdTipoProcsSistema'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcsSistema'.
     */
    public int getCdTipoProcsSistema()
    {
        return this._cdTipoProcsSistema;
    } //-- int getCdTipoProcsSistema() 

    /**
     * Returns the value of field 'dsTipoProcsSistema'.
     * 
     * @return String
     * @return the value of field 'dsTipoProcsSistema'.
     */
    public java.lang.String getDsTipoProcsSistema()
    {
        return this._dsTipoProcsSistema;
    } //-- java.lang.String getDsTipoProcsSistema() 

    /**
     * Method hasCdTipoProcsSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcsSistema()
    {
        return this._has_cdTipoProcsSistema;
    } //-- boolean hasCdTipoProcsSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoProcsSistema'.
     * 
     * @param cdTipoProcsSistema the value of field
     * 'cdTipoProcsSistema'.
     */
    public void setCdTipoProcsSistema(int cdTipoProcsSistema)
    {
        this._cdTipoProcsSistema = cdTipoProcsSistema;
        this._has_cdTipoProcsSistema = true;
    } //-- void setCdTipoProcsSistema(int) 

    /**
     * Sets the value of field 'dsTipoProcsSistema'.
     * 
     * @param dsTipoProcsSistema the value of field
     * 'dsTipoProcsSistema'.
     */
    public void setDsTipoProcsSistema(java.lang.String dsTipoProcsSistema)
    {
        this._dsTipoProcsSistema = dsTipoProcsSistema;
    } //-- void setDsTipoProcsSistema(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
