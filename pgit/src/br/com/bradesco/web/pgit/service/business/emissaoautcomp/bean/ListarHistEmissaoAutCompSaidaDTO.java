/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: ListarHistEmissaoAutCompSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistEmissaoAutCompSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo dataManutencao. */
	private String dataManutencao;
	
	/** Atributo dataManutencaoDesc. */
	private String dataManutencaoDesc;
	
	/** Atributo responsavel. */
	private String responsavel;
	
	/** Atributo tipoManutencao. */
	private int tipoManutencao;
	
	/** Atributo descTipoManutencao. */
	private String descTipoManutencao;
	
	/**
	 * Get: descTipoManutencao.
	 *
	 * @return descTipoManutencao
	 */
	public String getDescTipoManutencao() {
		return descTipoManutencao;
	}
	
	/**
	 * Set: descTipoManutencao.
	 *
	 * @param descTipoManutencao the desc tipo manutencao
	 */
	public void setDescTipoManutencao(String descTipoManutencao) {
		this.descTipoManutencao = descTipoManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dataManutencao.
	 *
	 * @return dataManutencao
	 */
	public String getDataManutencao() {
		return dataManutencao;
	}
	
	/**
	 * Set: dataManutencao.
	 *
	 * @param dataManutencao the data manutencao
	 */
	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: responsavel.
	 *
	 * @return responsavel
	 */
	public String getResponsavel() {
		return responsavel;
	}
	
	/**
	 * Set: responsavel.
	 *
	 * @param responsavel the responsavel
	 */
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	/**
	 * Get: tipoManutencao.
	 *
	 * @return tipoManutencao
	 */
	public int getTipoManutencao() {
		return tipoManutencao;
	}
	
	/**
	 * Set: tipoManutencao.
	 *
	 * @param tipoManutencao the tipo manutencao
	 */
	public void setTipoManutencao(int tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}
	
	/**
	 * Get: dataManutencaoDesc.
	 *
	 * @return dataManutencaoDesc
	 */
	public String getDataManutencaoDesc() {
		return dataManutencaoDesc;
	}
	
	/**
	 * Set: dataManutencaoDesc.
	 *
	 * @param dataManutencaoDesc the data manutencao desc
	 */
	public void setDataManutencaoDesc(String dataManutencaoDesc) {
		this.dataManutencaoDesc = dataManutencaoDesc;
	}
	
	
}
