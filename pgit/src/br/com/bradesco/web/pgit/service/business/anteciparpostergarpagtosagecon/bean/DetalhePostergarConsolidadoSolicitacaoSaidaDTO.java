package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 23/05/17.
 *
 * @author : todo!
 * @version :
 */
public class DetalhePostergarConsolidadoSolicitacaoSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem;

    /** The mensagem. */
    private String mensagem;

    /** The cd solicitacao pagamento integrado. */
    private Integer cdSolicitacaoPagamentoIntegrado;

    /** The nr solicitacao pagamento integrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;

    /** The cdpessoa juridica contrato. */
    private Long cdpessoaJuridicaContrato;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio;

    /** The cd situacao solicitacao pagamento. */
    private Integer cdSituacaoSolicitacaoPagamento;

    /** The ds situacao solicitacao pagamento. */
    private String dsSituacaoSolicitacaoPagamento;

    /** The cd motivo solicitacao. */
    private Integer cdMotivoSolicitacao;

    /** The ds motivo solicitacao. */
    private String dsMotivoSolicitacao;

    /** The ds tipo solicitacao. */
    private String dsTipoSolicitacao;

    /** The dt agendamento pagamento. */
    private String dtAgendamentoPagamento;

    /** The dt prevista agenda pagamento. */
    private String dtPrevistaAgendaPagamento;

    /** The cd produto servico operacao. */
    private Integer cdProdutoServicoOperacao;

    /** The ds produto servico operacao. */
    private String dsProdutoServicoOperacao;

    /** The cd produto operacao relacionado. */
    private Integer cdProdutoOperacaoRelacionado;

    /** The ds produto oper relacionado. */
    private String dsProdutoOperRelacionado;

    /** The nr arquivo remssa pagamento. */
    private Long nrArquivoRemssaPagamento;

    /** The cd situacao pagamento cliente. */
    private Integer cdSituacaoPagamentoCliente;

    /** The ds situacao pagamento cliente. */
    private String dsSituacaoPagamentoCliente;

    /** The cd tipo conta pagador. */
    private Integer cdTipoContaPagador;

    /** The ds tipo conta pagador. */
    private String dsTipoContaPagador;

    /** The cd banco pagador. */
    private Integer cdBancoPagador;

    /** The ds banco pagador. */
    private String dsBancoPagador;

    /** The cd agencia bancaria pagador. */
    private Integer cdAgenciaBancariaPagador;

    /** The ds agencia bancaria pagador. */
    private String dsAgenciaBancariaPagador;

    /** The cd digito agencia pagador. */
    private String cdDigitoAgenciaPagador;

    /** The cd conta bancaria pagador. */
    private Long cdContaBancariaPagador;

    /** The cd digito conta pagador. */
    private String cdDigitoContaPagador;

    /** The cd tipo inscricao pagador. */
    private Integer cdTipoInscricaoPagador;

    /** The cd cpf cnpj pagador. */
    private Long cdCpfCnpjPagador;

    /** The cd filial cpf cnpj pagador. */
    private Integer cdFilialCpfCnpjPagador;

    /** The cd controle cpf cnpj pagador. */
    private Integer cdControleCpfCnpjPagador;

    /** The ds complemento pagador. */
    private String dsComplementoPagador;

    /** The qtd pagamento prevt solicitacao. */
    private Long qtdPagamentoPrevtSolicitacao;

    /** The qtd pagamento efetv solicitacao. */
    private Long qtdPagamentoEfetvSolicitacao;

    /** The qtde pagamentos processados. */
    private Long qtdePagamentosProcessados;

    /** The vl pagamento prevt solicitacao. */
    private BigDecimal vlPagamentoPrevtSolicitacao;

    /** The vl pagamento efetivo solicitacao. */
    private BigDecimal vlPagamentoEfetivoSolicitacao;

    /** The vlr pagamentos processados. */
    private BigDecimal vlrPagamentosProcessados;

    /** The dt atendimento solicitacao pagamento. */
    private String dtAtendimentoSolicitacaoPagamento;

    /** The hr atendimento solicitacao pagamento. */
    private String hrAtendimentoSolicitacaoPagamento;

    /** The cd canal inclusao. */
    private Integer cdCanalInclusao;

    /** The ds canal inclusao. */
    private String dsCanalInclusao;

    /** The cd autenticacao seguranca inclusao. */
    private String cdAutenticacaoSegurancaInclusao;

    /** The nr operacao fluxo inclusao. */
    private String nrOperacaoFluxoInclusao;

    /** The hr inclusao registro. */
    private String hrInclusaoRegistro;

    /** The cd canal manutencao. */
    private Integer cdCanalManutencao;

    /** The ds canal manutencao. */
    private String dsCanalManutencao;

    /** The cd autenticacao seguranca manutencao. */
    private String cdAutenticacaoSegurancaManutencao;

    /** The nm operacao fluxo manutencao. */
    private String nmOperacaoFluxoManutencao;

    /** The hr manutencao registro. */
    private String hrManutencaoRegistro;

    public String getCpfCnpjFormatado() {
        return CpfCnpjUtils.formatCpfCnpjCompleto(getCdCpfCnpjPagador(), getCdFilialCpfCnpjPagador(), getCdControleCpfCnpjPagador());
    }
    
    /**
     * Agencia formatada.
     *
     * @return the string
     */
    public String getAgenciaFormatada() {
        return getCdAgenciaBancariaPagador() + " - " + getCdDigitoAgenciaPagador() + " - "
            + getDsAgenciaBancariaPagador();
    }

    /**
     * Banco formatado.
     *
     * @return the string
     */
    public String getBancoFormatado() {
        return getCdBancoPagador() + " - " + getDsBancoPagador();
    }

    /**
     * Conta formatada.
     *
     * @return the string
     */
    public String getContaFormatada() {
        return getCdContaBancariaPagador() + " - " + getCdDigitoContaPagador();
    }

    /**
     * Data hora atendimento.
     *
     * @return the string
     */
    public String getDataHoraAtendimento() {
        if(getDtAtendimentoSolicitacaoPagamento().equals("") || getHrAtendimentoSolicitacaoPagamento().equals("")){
            return getDtAtendimentoSolicitacaoPagamento().replace(".", "/") 
            + getHrAtendimentoSolicitacaoPagamento();
        }
        
        return getDtAtendimentoSolicitacaoPagamento().replace(".", "/") + " - "
            + getHrAtendimentoSolicitacaoPagamento();
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
        if(getCdCanalInclusao() == 0 && getDsCanalInclusao().equals("")){
            return "";
        }
        
        if(getCdCanalInclusao().equals("") || getDsCanalInclusao().equals("")){
            return getCdCanalInclusao() + getDsCanalInclusao();
        }
        
        return getCdCanalInclusao() + " - " + getDsCanalInclusao();
    }
    
    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
        if(getCdCanalManutencao() == 0 && getDsCanalManutencao().equals("")){
            return "";
        }
        
        if(getCdCanalManutencao().equals("") || getDsCanalManutencao().equals("")){
            return getCdCanalManutencao() + getDsCanalManutencao();
        }
        
        return getCdCanalManutencao() + " - " + getDsCanalManutencao();
    }
    /**
     * Set cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Set mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Set cd solicitacao pagamento integrado.
     *
     * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
     */
    public void setCdSolicitacaoPagamentoIntegrado(Integer cdSolicitacaoPagamentoIntegrado) {
        this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get cd solicitacao pagamento integrado.
     *
     * @return the cd solicitacao pagamento integrado
     */
    public Integer getCdSolicitacaoPagamentoIntegrado() {
        return this.cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set nr solicitacao pagamento integrado.
     *
     * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
     */
    public void setNrSolicitacaoPagamentoIntegrado(Integer nrSolicitacaoPagamentoIntegrado) {
        this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get nr solicitacao pagamento integrado.
     *
     * @return the nr solicitacao pagamento integrado
     */
    public Integer getNrSolicitacaoPagamentoIntegrado() {
        return this.nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set cdpessoa juridica contrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get cdpessoa juridica contrato.
     *
     * @return the cdpessoa juridica contrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set cd situacao solicitacao pagamento.
     *
     * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
     */
    public void setCdSituacaoSolicitacaoPagamento(Integer cdSituacaoSolicitacaoPagamento) {
        this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
    }

    /**
     * Get cd situacao solicitacao pagamento.
     *
     * @return the cd situacao solicitacao pagamento
     */
    public Integer getCdSituacaoSolicitacaoPagamento() {
        return this.cdSituacaoSolicitacaoPagamento;
    }

    /**
     * Set ds situacao solicitacao pagamento.
     *
     * @param dsSituacaoSolicitacaoPagamento the ds situacao solicitacao pagamento
     */
    public void setDsSituacaoSolicitacaoPagamento(String dsSituacaoSolicitacaoPagamento) {
        this.dsSituacaoSolicitacaoPagamento = dsSituacaoSolicitacaoPagamento;
    }

    /**
     * Get ds situacao solicitacao pagamento.
     *
     * @return the ds situacao solicitacao pagamento
     */
    public String getDsSituacaoSolicitacaoPagamento() {
        return this.dsSituacaoSolicitacaoPagamento;
    }

    /**
     * Set cd motivo solicitacao.
     *
     * @param cdMotivoSolicitacao the cd motivo solicitacao
     */
    public void setCdMotivoSolicitacao(Integer cdMotivoSolicitacao) {
        this.cdMotivoSolicitacao = cdMotivoSolicitacao;
    }

    /**
     * Get cd motivo solicitacao.
     *
     * @return the cd motivo solicitacao
     */
    public Integer getCdMotivoSolicitacao() {
        return this.cdMotivoSolicitacao;
    }

    /**
     * Set ds motivo solicitacao.
     *
     * @param dsMotivoSolicitacao the ds motivo solicitacao
     */
    public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
        this.dsMotivoSolicitacao = dsMotivoSolicitacao;
    }

    /**
     * Get ds motivo solicitacao.
     *
     * @return the ds motivo solicitacao
     */
    public String getDsMotivoSolicitacao() {
        return this.dsMotivoSolicitacao;
    }

    /**
     * Set ds tipo solicitacao.
     *
     * @param dsTipoSolicitacao the ds tipo solicitacao
     */
    public void setDsTipoSolicitacao(String dsTipoSolicitacao) {
        this.dsTipoSolicitacao = dsTipoSolicitacao;
    }

    /**
     * Get ds tipo solicitacao.
     *
     * @return the ds tipo solicitacao
     */
    public String getDsTipoSolicitacao() {
        return this.dsTipoSolicitacao;
    }

    /**
     * Set dt agendamento pagamento.
     *
     * @param dtAgendamentoPagamento the dt agendamento pagamento
     */
    public void setDtAgendamentoPagamento(String dtAgendamentoPagamento) {
        this.dtAgendamentoPagamento = dtAgendamentoPagamento;
    }

    /**
     * Get dt agendamento pagamento.
     *
     * @return the dt agendamento pagamento
     */
    public String getDtAgendamentoPagamento() {
        return this.dtAgendamentoPagamento;
    }

    /**
     * Set dt prevista agenda pagamento.
     *
     * @param dtPrevistaAgendaPagamento the dt prevista agenda pagamento
     */
    public void setDtPrevistaAgendaPagamento(String dtPrevistaAgendaPagamento) {
        this.dtPrevistaAgendaPagamento = dtPrevistaAgendaPagamento;
    }

    /**
     * Get dt prevista agenda pagamento.
     *
     * @return the dt prevista agenda pagamento
     */
    public String getDtPrevistaAgendaPagamento() {
        return this.dtPrevistaAgendaPagamento;
    }

    /**
     * Set cd produto servico operacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get cd produto servico operacao.
     *
     * @return the cd produto servico operacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    /**
     * Set ds produto servico operacao.
     *
     * @param dsProdutoServicoOperacao the ds produto servico operacao
     */
    public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
        this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    }

    /**
     * Get ds produto servico operacao.
     *
     * @return the ds produto servico operacao
     */
    public String getDsProdutoServicoOperacao() {
        return this.dsProdutoServicoOperacao;
    }

    /**
     * Set cd produto operacao relacionado.
     *
     * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Get cd produto operacao relacionado.
     *
     * @return the cd produto operacao relacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }

    /**
     * Set ds produto oper relacionado.
     *
     * @param dsProdutoOperRelacionado the ds produto oper relacionado
     */
    public void setDsProdutoOperRelacionado(String dsProdutoOperRelacionado) {
        this.dsProdutoOperRelacionado = dsProdutoOperRelacionado;
    }

    /**
     * Get ds produto oper relacionado.
     *
     * @return the ds produto oper relacionado
     */
    public String getDsProdutoOperRelacionado() {
        return this.dsProdutoOperRelacionado;
    }

    /**
     * Set nr arquivo remssa pagamento.
     *
     * @param nrArquivoRemssaPagamento the nr arquivo remssa pagamento
     */
    public void setNrArquivoRemssaPagamento(Long nrArquivoRemssaPagamento) {
        this.nrArquivoRemssaPagamento = nrArquivoRemssaPagamento;
    }

    /**
     * Get nr arquivo remssa pagamento.
     *
     * @return the nr arquivo remssa pagamento
     */
    public Long getNrArquivoRemssaPagamento() {
        return this.nrArquivoRemssaPagamento;
    }

    /**
     * Set cd situacao pagamento cliente.
     *
     * @param cdSituacaoPagamentoCliente the cd situacao pagamento cliente
     */
    public void setCdSituacaoPagamentoCliente(Integer cdSituacaoPagamentoCliente) {
        this.cdSituacaoPagamentoCliente = cdSituacaoPagamentoCliente;
    }

    /**
     * Get cd situacao pagamento cliente.
     *
     * @return the cd situacao pagamento cliente
     */
    public Integer getCdSituacaoPagamentoCliente() {
        return this.cdSituacaoPagamentoCliente;
    }

    /**
     * Set ds situacao pagamento cliente.
     *
     * @param dsSituacaoPagamentoCliente the ds situacao pagamento cliente
     */
    public void setDsSituacaoPagamentoCliente(String dsSituacaoPagamentoCliente) {
        this.dsSituacaoPagamentoCliente = dsSituacaoPagamentoCliente;
    }

    /**
     * Get ds situacao pagamento cliente.
     *
     * @return the ds situacao pagamento cliente
     */
    public String getDsSituacaoPagamentoCliente() {
        return this.dsSituacaoPagamentoCliente;
    }

    /**
     * Set cd tipo conta pagador.
     *
     * @param cdTipoContaPagador the cd tipo conta pagador
     */
    public void setCdTipoContaPagador(Integer cdTipoContaPagador) {
        this.cdTipoContaPagador = cdTipoContaPagador;
    }

    /**
     * Get cd tipo conta pagador.
     *
     * @return the cd tipo conta pagador
     */
    public Integer getCdTipoContaPagador() {
        return this.cdTipoContaPagador;
    }

    /**
     * Set ds tipo conta pagador.
     *
     * @param dsTipoContaPagador the ds tipo conta pagador
     */
    public void setDsTipoContaPagador(String dsTipoContaPagador) {
        this.dsTipoContaPagador = dsTipoContaPagador;
    }

    /**
     * Get ds tipo conta pagador.
     *
     * @return the ds tipo conta pagador
     */
    public String getDsTipoContaPagador() {
        return this.dsTipoContaPagador;
    }

    /**
     * Set cd banco pagador.
     *
     * @param cdBancoPagador the cd banco pagador
     */
    public void setCdBancoPagador(Integer cdBancoPagador) {
        this.cdBancoPagador = cdBancoPagador;
    }

    /**
     * Get cd banco pagador.
     *
     * @return the cd banco pagador
     */
    public Integer getCdBancoPagador() {
        return this.cdBancoPagador;
    }

    /**
     * Set ds banco pagador.
     *
     * @param dsBancoPagador the ds banco pagador
     */
    public void setDsBancoPagador(String dsBancoPagador) {
        this.dsBancoPagador = dsBancoPagador;
    }

    /**
     * Get ds banco pagador.
     *
     * @return the ds banco pagador
     */
    public String getDsBancoPagador() {
        return this.dsBancoPagador;
    }

    /**
     * Set cd agencia bancaria pagador.
     *
     * @param cdAgenciaBancariaPagador the cd agencia bancaria pagador
     */
    public void setCdAgenciaBancariaPagador(Integer cdAgenciaBancariaPagador) {
        this.cdAgenciaBancariaPagador = cdAgenciaBancariaPagador;
    }

    /**
     * Get cd agencia bancaria pagador.
     *
     * @return the cd agencia bancaria pagador
     */
    public Integer getCdAgenciaBancariaPagador() {
        return this.cdAgenciaBancariaPagador;
    }

    /**
     * Set ds agencia bancaria pagador.
     *
     * @param dsAgenciaBancariaPagador the ds agencia bancaria pagador
     */
    public void setDsAgenciaBancariaPagador(String dsAgenciaBancariaPagador) {
        this.dsAgenciaBancariaPagador = dsAgenciaBancariaPagador;
    }

    /**
     * Get ds agencia bancaria pagador.
     *
     * @return the ds agencia bancaria pagador
     */
    public String getDsAgenciaBancariaPagador() {
        return this.dsAgenciaBancariaPagador;
    }

    /**
     * Set cd digito agencia pagador.
     *
     * @param cdDigitoAgenciaPagador the cd digito agencia pagador
     */
    public void setCdDigitoAgenciaPagador(String cdDigitoAgenciaPagador) {
        this.cdDigitoAgenciaPagador = cdDigitoAgenciaPagador;
    }

    /**
     * Get cd digito agencia pagador.
     *
     * @return the cd digito agencia pagador
     */
    public String getCdDigitoAgenciaPagador() {
        return this.cdDigitoAgenciaPagador;
    }

    /**
     * Set cd conta bancaria pagador.
     *
     * @param cdContaBancariaPagador the cd conta bancaria pagador
     */
    public void setCdContaBancariaPagador(Long cdContaBancariaPagador) {
        this.cdContaBancariaPagador = cdContaBancariaPagador;
    }

    /**
     * Get cd conta bancaria pagador.
     *
     * @return the cd conta bancaria pagador
     */
    public Long getCdContaBancariaPagador() {
        return this.cdContaBancariaPagador;
    }

    /**
     * Set cd digito conta pagador.
     *
     * @param cdDigitoContaPagador the cd digito conta pagador
     */
    public void setCdDigitoContaPagador(String cdDigitoContaPagador) {
        this.cdDigitoContaPagador = cdDigitoContaPagador;
    }

    /**
     * Get cd digito conta pagador.
     *
     * @return the cd digito conta pagador
     */
    public String getCdDigitoContaPagador() {
        return this.cdDigitoContaPagador;
    }

    /**
     * Set cd tipo inscricao pagador.
     *
     * @param cdTipoInscricaoPagador the cd tipo inscricao pagador
     */
    public void setCdTipoInscricaoPagador(Integer cdTipoInscricaoPagador) {
        this.cdTipoInscricaoPagador = cdTipoInscricaoPagador;
    }

    /**
     * Get cd tipo inscricao pagador.
     *
     * @return the cd tipo inscricao pagador
     */
    public Integer getCdTipoInscricaoPagador() {
        return this.cdTipoInscricaoPagador;
    }

    /**
     * Set cd cpf cnpj pagador.
     *
     * @param cdCpfCnpjPagador the cd cpf cnpj pagador
     */
    public void setCdCpfCnpjPagador(Long cdCpfCnpjPagador) {
        this.cdCpfCnpjPagador = cdCpfCnpjPagador;
    }

    /**
     * Get cd cpf cnpj pagador.
     *
     * @return the cd cpf cnpj pagador
     */
    public Long getCdCpfCnpjPagador() {
        return this.cdCpfCnpjPagador;
    }

    /**
     * Set cd filial cpf cnpj pagador.
     *
     * @param cdFilialCpfCnpjPagador the cd filial cpf cnpj pagador
     */
    public void setCdFilialCpfCnpjPagador(Integer cdFilialCpfCnpjPagador) {
        this.cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
    }

    /**
     * Get cd filial cpf cnpj pagador.
     *
     * @return the cd filial cpf cnpj pagador
     */
    public Integer getCdFilialCpfCnpjPagador() {
        return this.cdFilialCpfCnpjPagador;
    }

    /**
     * Set cd controle cpf cnpj pagador.
     *
     * @param cdControleCpfCnpjPagador the cd controle cpf cnpj pagador
     */
    public void setCdControleCpfCnpjPagador(Integer cdControleCpfCnpjPagador) {
        this.cdControleCpfCnpjPagador = cdControleCpfCnpjPagador;
    }

    /**
     * Get cd controle cpf cnpj pagador.
     *
     * @return the cd controle cpf cnpj pagador
     */
    public Integer getCdControleCpfCnpjPagador() {
        return this.cdControleCpfCnpjPagador;
    }

    /**
     * Set ds complemento pagador.
     *
     * @param dsComplementoPagador the ds complemento pagador
     */
    public void setDsComplementoPagador(String dsComplementoPagador) {
        this.dsComplementoPagador = dsComplementoPagador;
    }

    /**
     * Get ds complemento pagador.
     *
     * @return the ds complemento pagador
     */
    public String getDsComplementoPagador() {
        return this.dsComplementoPagador;
    }

    /**
     * Set qtd pagamento prevt solicitacao.
     *
     * @param qtdPagamentoPrevtSolicitacao the qtd pagamento prevt solicitacao
     */
    public void setQtdPagamentoPrevtSolicitacao(Long qtdPagamentoPrevtSolicitacao) {
        this.qtdPagamentoPrevtSolicitacao = qtdPagamentoPrevtSolicitacao;
    }

    /**
     * Get qtd pagamento prevt solicitacao.
     *
     * @return the qtd pagamento prevt solicitacao
     */
    public Long getQtdPagamentoPrevtSolicitacao() {
        return this.qtdPagamentoPrevtSolicitacao;
    }

    /**
     * Set qtd pagamento efetv solicitacao.
     *
     * @param qtdPagamentoEfetvSolicitacao the qtd pagamento efetv solicitacao
     */
    public void setQtdPagamentoEfetvSolicitacao(Long qtdPagamentoEfetvSolicitacao) {
        this.qtdPagamentoEfetvSolicitacao = qtdPagamentoEfetvSolicitacao;
    }

    /**
     * Get qtd pagamento efetv solicitacao.
     *
     * @return the qtd pagamento efetv solicitacao
     */
    public Long getQtdPagamentoEfetvSolicitacao() {
        return this.qtdPagamentoEfetvSolicitacao;
    }

    /**
     * Set qtde pagamentos processados.
     *
     * @param qtdePagamentosProcessados the qtde pagamentos processados
     */
    public void setQtdePagamentosProcessados(Long qtdePagamentosProcessados) {
        this.qtdePagamentosProcessados = qtdePagamentosProcessados;
    }

    /**
     * Get qtde pagamentos processados.
     *
     * @return the qtde pagamentos processados
     */
    public Long getQtdePagamentosProcessados() {
        return this.qtdePagamentosProcessados;
    }

    /**
     * Set vl pagamento prevt solicitacao.
     *
     * @param vlPagamentoPrevtSolicitacao the vl pagamento prevt solicitacao
     */
    public void setVlPagamentoPrevtSolicitacao(BigDecimal vlPagamentoPrevtSolicitacao) {
        this.vlPagamentoPrevtSolicitacao = vlPagamentoPrevtSolicitacao;
    }

    /**
     * Get vl pagamento prevt solicitacao.
     *
     * @return the vl pagamento prevt solicitacao
     */
    public BigDecimal getVlPagamentoPrevtSolicitacao() {
        return this.vlPagamentoPrevtSolicitacao;
    }

    /**
     * Set vl pagamento efetivo solicitacao.
     *
     * @param vlPagamentoEfetivoSolicitacao the vl pagamento efetivo solicitacao
     */
    public void setVlPagamentoEfetivoSolicitacao(BigDecimal vlPagamentoEfetivoSolicitacao) {
        this.vlPagamentoEfetivoSolicitacao = vlPagamentoEfetivoSolicitacao;
    }

    /**
     * Get vl pagamento efetivo solicitacao.
     *
     * @return the vl pagamento efetivo solicitacao
     */
    public BigDecimal getVlPagamentoEfetivoSolicitacao() {
        return this.vlPagamentoEfetivoSolicitacao;
    }

    /**
     * Set vlr pagamentos processados.
     *
     * @param vlrPagamentosProcessados the vlr pagamentos processados
     */
    public void setVlrPagamentosProcessados(BigDecimal vlrPagamentosProcessados) {
        this.vlrPagamentosProcessados = vlrPagamentosProcessados;
    }

    /**
     * Get vlr pagamentos processados.
     *
     * @return the vlr pagamentos processados
     */
    public BigDecimal getVlrPagamentosProcessados() {
        return this.vlrPagamentosProcessados;
    }

    /**
     * Set dt atendimento solicitacao pagamento.
     *
     * @param dtAtendimentoSolicitacaoPagamento the dt atendimento solicitacao pagamento
     */
    public void setDtAtendimentoSolicitacaoPagamento(String dtAtendimentoSolicitacaoPagamento) {
        this.dtAtendimentoSolicitacaoPagamento = dtAtendimentoSolicitacaoPagamento;
    }

    /**
     * Get dt atendimento solicitacao pagamento.
     *
     * @return the dt atendimento solicitacao pagamento
     */
    public String getDtAtendimentoSolicitacaoPagamento() {
        return this.dtAtendimentoSolicitacaoPagamento;
    }

    /**
     * Set hr atendimento solicitacao pagamento.
     *
     * @param hrAtendimentoSolicitacaoPagamento the hr atendimento solicitacao pagamento
     */
    public void setHrAtendimentoSolicitacaoPagamento(String hrAtendimentoSolicitacaoPagamento) {
        this.hrAtendimentoSolicitacaoPagamento = hrAtendimentoSolicitacaoPagamento;
    }

    /**
     * Get hr atendimento solicitacao pagamento.
     *
     * @return the hr atendimento solicitacao pagamento
     */
    public String getHrAtendimentoSolicitacaoPagamento() {
        return this.hrAtendimentoSolicitacaoPagamento;
    }

    /**
     * Set cd canal inclusao.
     *
     * @param cdCanalInclusao the cd canal inclusao
     */
    public void setCdCanalInclusao(Integer cdCanalInclusao) {
        this.cdCanalInclusao = cdCanalInclusao;
    }

    /**
     * Get cd canal inclusao.
     *
     * @return the cd canal inclusao
     */
    public Integer getCdCanalInclusao() {
        return this.cdCanalInclusao;
    }

    /**
     * Set ds canal inclusao.
     *
     * @param dsCanalInclusao the ds canal inclusao
     */
    public void setDsCanalInclusao(String dsCanalInclusao) {
        this.dsCanalInclusao = dsCanalInclusao;
    }

    /**
     * Get ds canal inclusao.
     *
     * @return the ds canal inclusao
     */
    public String getDsCanalInclusao() {
        return this.dsCanalInclusao;
    }

    /**
     * Set cd autenticacao seguranca inclusao.
     *
     * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
     */
    public void setCdAutenticacaoSegurancaInclusao(String cdAutenticacaoSegurancaInclusao) {
        this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    }

    /**
     * Get cd autenticacao seguranca inclusao.
     *
     * @return the cd autenticacao seguranca inclusao
     */
    public String getCdAutenticacaoSegurancaInclusao() {
        return this.cdAutenticacaoSegurancaInclusao;
    }

    /**
     * Set nr operacao fluxo inclusao.
     *
     * @param nrOperacaoFluxoInclusao the nr operacao fluxo inclusao
     */
    public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
        this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    }

    /**
     * Get nr operacao fluxo inclusao.
     *
     * @return the nr operacao fluxo inclusao
     */
    public String getNrOperacaoFluxoInclusao() {
        return this.nrOperacaoFluxoInclusao;
    }

    /**
     * Set hr inclusao registro.
     *
     * @param hrInclusaoRegistro the hr inclusao registro
     */
    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
        this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    /**
     * Get hr inclusao registro.
     *
     * @return the hr inclusao registro
     */
    public String getHrInclusaoRegistro() {
        return this.hrInclusaoRegistro;
    }

    /**
     * Set cd canal manutencao.
     *
     * @param cdCanalManutencao the cd canal manutencao
     */
    public void setCdCanalManutencao(Integer cdCanalManutencao) {
        this.cdCanalManutencao = cdCanalManutencao;
    }

    /**
     * Get cd canal manutencao.
     *
     * @return the cd canal manutencao
     */
    public Integer getCdCanalManutencao() {
        return this.cdCanalManutencao;
    }

    /**
     * Set ds canal manutencao.
     *
     * @param dsCanalManutencao the ds canal manutencao
     */
    public void setDsCanalManutencao(String dsCanalManutencao) {
        this.dsCanalManutencao = dsCanalManutencao;
    }

    /**
     * Get ds canal manutencao.
     *
     * @return the ds canal manutencao
     */
    public String getDsCanalManutencao() {
        return this.dsCanalManutencao;
    }

    /**
     * Set cd autenticacao seguranca manutencao.
     *
     * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
     */
    public void setCdAutenticacaoSegurancaManutencao(String cdAutenticacaoSegurancaManutencao) {
        this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    }

    /**
     * Get cd autenticacao seguranca manutencao.
     *
     * @return the cd autenticacao seguranca manutencao
     */
    public String getCdAutenticacaoSegurancaManutencao() {
        return this.cdAutenticacaoSegurancaManutencao;
    }

    /**
     * Set nm operacao fluxo manutencao.
     *
     * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
     */
    public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
        this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    }

    /**
     * Get nm operacao fluxo manutencao.
     *
     * @return the nm operacao fluxo manutencao
     */
    public String getNmOperacaoFluxoManutencao() {
        return this.nmOperacaoFluxoManutencao;
    }

    /**
     * Set hr manutencao registro.
     *
     * @param hrManutencaoRegistro the hr manutencao registro
     */
    public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
        this.hrManutencaoRegistro = hrManutencaoRegistro;
    }

    /**
     * Get hr manutencao registro.
     *
     * @return the hr manutencao registro
     */
    public String getHrManutencaoRegistro() {
        return this.hrManutencaoRegistro;
    }
}