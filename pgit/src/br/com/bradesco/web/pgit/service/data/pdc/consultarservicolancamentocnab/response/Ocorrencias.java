/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFormaLancamentoCnab
     */
    private int _cdFormaLancamentoCnab = 0;

    /**
     * keeps track of state for field: _cdFormaLancamentoCnab
     */
    private boolean _has_cdFormaLancamentoCnab;

    /**
     * Field _dsFormaLancamentoCnab
     */
    private java.lang.String _dsFormaLancamentoCnab;

    /**
     * Field _cdTipoServicoCnab
     */
    private int _cdTipoServicoCnab = 0;

    /**
     * keeps track of state for field: _cdTipoServicoCnab
     */
    private boolean _has_cdTipoServicoCnab;

    /**
     * Field _dsTipoServicoCnab
     */
    private java.lang.String _dsTipoServicoCnab;

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _dsFormaLiquidacao
     */
    private java.lang.String _dsFormaLiquidacao;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoServicoRelacionado
     */
    private java.lang.String _dsProdutoServicoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaLancamentoCnab
     * 
     */
    public void deleteCdFormaLancamentoCnab()
    {
        this._has_cdFormaLancamentoCnab= false;
    } //-- void deleteCdFormaLancamentoCnab() 

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoServicoCnab
     * 
     */
    public void deleteCdTipoServicoCnab()
    {
        this._has_cdTipoServicoCnab= false;
    } //-- void deleteCdTipoServicoCnab() 

    /**
     * Returns the value of field 'cdFormaLancamentoCnab'.
     * 
     * @return int
     * @return the value of field 'cdFormaLancamentoCnab'.
     */
    public int getCdFormaLancamentoCnab()
    {
        return this._cdFormaLancamentoCnab;
    } //-- int getCdFormaLancamentoCnab() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoServicoCnab'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoCnab'.
     */
    public int getCdTipoServicoCnab()
    {
        return this._cdTipoServicoCnab;
    } //-- int getCdTipoServicoCnab() 

    /**
     * Returns the value of field 'dsFormaLancamentoCnab'.
     * 
     * @return String
     * @return the value of field 'dsFormaLancamentoCnab'.
     */
    public java.lang.String getDsFormaLancamentoCnab()
    {
        return this._dsFormaLancamentoCnab;
    } //-- java.lang.String getDsFormaLancamentoCnab() 

    /**
     * Returns the value of field 'dsFormaLiquidacao'.
     * 
     * @return String
     * @return the value of field 'dsFormaLiquidacao'.
     */
    public java.lang.String getDsFormaLiquidacao()
    {
        return this._dsFormaLiquidacao;
    } //-- java.lang.String getDsFormaLiquidacao() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsProdutoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoRelacionado'.
     */
    public java.lang.String getDsProdutoServicoRelacionado()
    {
        return this._dsProdutoServicoRelacionado;
    } //-- java.lang.String getDsProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'dsTipoServicoCnab'.
     * 
     * @return String
     * @return the value of field 'dsTipoServicoCnab'.
     */
    public java.lang.String getDsTipoServicoCnab()
    {
        return this._dsTipoServicoCnab;
    } //-- java.lang.String getDsTipoServicoCnab() 

    /**
     * Method hasCdFormaLancamentoCnab
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLancamentoCnab()
    {
        return this._has_cdFormaLancamentoCnab;
    } //-- boolean hasCdFormaLancamentoCnab() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoServicoCnab
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoCnab()
    {
        return this._has_cdTipoServicoCnab;
    } //-- boolean hasCdTipoServicoCnab() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaLancamentoCnab'.
     * 
     * @param cdFormaLancamentoCnab the value of field
     * 'cdFormaLancamentoCnab'.
     */
    public void setCdFormaLancamentoCnab(int cdFormaLancamentoCnab)
    {
        this._cdFormaLancamentoCnab = cdFormaLancamentoCnab;
        this._has_cdFormaLancamentoCnab = true;
    } //-- void setCdFormaLancamentoCnab(int) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoServicoCnab'.
     * 
     * @param cdTipoServicoCnab the value of field
     * 'cdTipoServicoCnab'.
     */
    public void setCdTipoServicoCnab(int cdTipoServicoCnab)
    {
        this._cdTipoServicoCnab = cdTipoServicoCnab;
        this._has_cdTipoServicoCnab = true;
    } //-- void setCdTipoServicoCnab(int) 

    /**
     * Sets the value of field 'dsFormaLancamentoCnab'.
     * 
     * @param dsFormaLancamentoCnab the value of field
     * 'dsFormaLancamentoCnab'.
     */
    public void setDsFormaLancamentoCnab(java.lang.String dsFormaLancamentoCnab)
    {
        this._dsFormaLancamentoCnab = dsFormaLancamentoCnab;
    } //-- void setDsFormaLancamentoCnab(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaLiquidacao'.
     * 
     * @param dsFormaLiquidacao the value of field
     * 'dsFormaLiquidacao'.
     */
    public void setDsFormaLiquidacao(java.lang.String dsFormaLiquidacao)
    {
        this._dsFormaLiquidacao = dsFormaLiquidacao;
    } //-- void setDsFormaLiquidacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoRelacionado'.
     * 
     * @param dsProdutoServicoRelacionado the value of field
     * 'dsProdutoServicoRelacionado'.
     */
    public void setDsProdutoServicoRelacionado(java.lang.String dsProdutoServicoRelacionado)
    {
        this._dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
    } //-- void setDsProdutoServicoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoServicoCnab'.
     * 
     * @param dsTipoServicoCnab the value of field
     * 'dsTipoServicoCnab'.
     */
    public void setDsTipoServicoCnab(java.lang.String dsTipoServicoCnab)
    {
        this._dsTipoServicoCnab = dsTipoServicoCnab;
    } //-- void setDsTipoServicoCnab(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
