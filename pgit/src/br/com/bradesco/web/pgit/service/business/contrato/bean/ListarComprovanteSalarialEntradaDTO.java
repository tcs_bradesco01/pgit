/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

/**
 * Nome: ListarComprovanteSalarialEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarComprovanteSalarialEntradaDTO {

    /** The max ocorrencias. */
    private Integer maxOcorrencias = null;

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato = null;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio = null;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio = null;
    
    /** The nr aditivo contrato negocio. */
    private Long nrAditivoContratoNegocio = null;

    /**
     * Listar comprovante salarial entrada dto.
     * 
     * @see
     */
    public ListarComprovanteSalarialEntradaDTO() {
        super();
    }

    /**
     * Sets the max ocorrencias.
     *
     * @param maxOcorrencias the max ocorrencias
     */
    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    /**
     * Gets the max ocorrencias.
     *
     * @return the max ocorrencias
     */
    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
        this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
        return this.cdPessoaJuridicaContrato;
    }

    /**
     * Sets the cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Gets the cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Sets the nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Gets the nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Nome: setNrAditivoContratoNegocio
     *
     * @param nrAditivoContratoNegocio
     */
    public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
        this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
    }

    /**
     * Nome: getNrAditivoContratoNegocio
     *
     * @return nrAditivoContratoNegocio
     */
    public Long getNrAditivoContratoNegocio() {
        return nrAditivoContratoNegocio;
    }
}