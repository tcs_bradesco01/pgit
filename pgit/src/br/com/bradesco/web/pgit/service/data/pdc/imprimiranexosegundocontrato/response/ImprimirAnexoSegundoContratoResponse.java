/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ImprimirAnexoSegundoContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ImprimirAnexoSegundoContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdServicoFornecedor
     */
    private java.lang.String _cdServicoFornecedor;

    /**
     * Field _qtModalidadeFornecedor
     */
    private int _qtModalidadeFornecedor = 0;

    /**
     * keeps track of state for field: _qtModalidadeFornecedor
     */
    private boolean _has_qtModalidadeFornecedor;

    /**
     * Field _ocorrencias1List
     */
    private java.util.Vector _ocorrencias1List;

    /**
     * Field _cdServicoTributos
     */
    private java.lang.String _cdServicoTributos;

    /**
     * Field _qtModalidadeTributos
     */
    private int _qtModalidadeTributos = 0;

    /**
     * keeps track of state for field: _qtModalidadeTributos
     */
    private boolean _has_qtModalidadeTributos;

    /**
     * Field _ocorrencias2List
     */
    private java.util.Vector _ocorrencias2List;

    /**
     * Field _cdServicoSalarial
     */
    private java.lang.String _cdServicoSalarial;

    /**
     * Field _qtModalidadeSalario
     */
    private int _qtModalidadeSalario = 0;

    /**
     * keeps track of state for field: _qtModalidadeSalario
     */
    private boolean _has_qtModalidadeSalario;

    /**
     * Field _ocorrencias3List
     */
    private java.util.Vector _ocorrencias3List;

    /**
     * Field _cdServicoBeneficio
     */
    private java.lang.String _cdServicoBeneficio;

    /**
     * Field _qtModalidadeBeneficio
     */
    private int _qtModalidadeBeneficio = 0;

    /**
     * keeps track of state for field: _qtModalidadeBeneficio
     */
    private boolean _has_qtModalidadeBeneficio;

    /**
     * Field _ocorrencias4List
     */
    private java.util.Vector _ocorrencias4List;

    /**
     * Field _cdIndicadorAviso
     */
    private java.lang.String _cdIndicadorAviso;

    /**
     * Field _cdIndicadorAvisoPagador
     */
    private java.lang.String _cdIndicadorAvisoPagador;

    /**
     * Field _cdIndicadorAvisoFavorecido
     */
    private java.lang.String _cdIndicadorAvisoFavorecido;

    /**
     * Field _cdIndicadorComplemento
     */
    private java.lang.String _cdIndicadorComplemento;

    /**
     * Field _cdIndicadorComplementoPagador
     */
    private java.lang.String _cdIndicadorComplementoPagador;

    /**
     * Field _cdIndicadorComplementoFavorecido
     */
    private java.lang.String _cdIndicadorComplementoFavorecido;

    /**
     * Field _cdIndicadorComplementoSalarial
     */
    private java.lang.String _cdIndicadorComplementoSalarial;

    /**
     * Field _cdIndicadorComplementoDivergente
     */
    private java.lang.String _cdIndicadorComplementoDivergente;

    /**
     * Field _cdIndcadorCadastroFavorecido
     */
    private java.lang.String _cdIndcadorCadastroFavorecido;

    /**
     * Field _dsServicoRecadastro
     */
    private java.lang.String _dsServicoRecadastro;

    /**
     * Field _qtModalidadeRecadastro
     */
    private int _qtModalidadeRecadastro = 0;

    /**
     * keeps track of state for field: _qtModalidadeRecadastro
     */
    private boolean _has_qtModalidadeRecadastro;

    /**
     * Field _ocorrencias5List
     */
    private java.util.Vector _ocorrencias5List;

    /**
     * Field _dsSalarioEmissaoAntecipadoCartao
     */
    private java.lang.String _dsSalarioEmissaoAntecipadoCartao;

    /**
     * Field _cdSalarioQuantidadeLimiteSolicitacaoCartao
     */
    private int _cdSalarioQuantidadeLimiteSolicitacaoCartao = 0;

    /**
     * keeps track of state for field:
     * _cdSalarioQuantidadeLimiteSolicitacaoCartao
     */
    private boolean _has_cdSalarioQuantidadeLimiteSolicitacaoCartao;

    /**
     * Field _dsSalarioAberturaContaExpressa
     */
    private java.lang.String _dsSalarioAberturaContaExpressa;

    /**
     * Field _dsTibutoAgendadoDebitoVeicular
     */
    private java.lang.String _dsTibutoAgendadoDebitoVeicular;

    /**
     * Field _dsBeneficioInformadoAgenciaConta
     */
    private java.lang.String _dsBeneficioInformadoAgenciaConta;

    /**
     * Field _dsBeneficioTipoIdentificacaoBeneficio
     */
    private java.lang.String _dsBeneficioTipoIdentificacaoBeneficio;

    /**
     * Field _dsBeneficioUtilizacaoCadastroOrganizacao
     */
    private java.lang.String _dsBeneficioUtilizacaoCadastroOrganizacao;

    /**
     * Field _dsBeneficioUtilizacaoCadastroProcuradores
     */
    private java.lang.String _dsBeneficioUtilizacaoCadastroProcuradores;

    /**
     * Field _dsBeneficioPossuiExpiracaoCredito
     */
    private java.lang.String _dsBeneficioPossuiExpiracaoCredito;

    /**
     * Field _cdBeneficioQuantidadeDiasExpiracaoCredito
     */
    private int _cdBeneficioQuantidadeDiasExpiracaoCredito = 0;

    /**
     * keeps track of state for field:
     * _cdBeneficioQuantidadeDiasExpiracaoCredito
     */
    private boolean _has_cdBeneficioQuantidadeDiasExpiracaoCredito;

    /**
     * Field _cdServicoQuantidadeServicoPagamento
     */
    private int _cdServicoQuantidadeServicoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdServicoQuantidadeServicoPagamento
     */
    private boolean _has_cdServicoQuantidadeServicoPagamento;

    /**
     * Field _ocorrencias6List
     */
    private java.util.Vector _ocorrencias6List;

    /**
     * Field _qtAviso
     */
    private int _qtAviso = 0;

    /**
     * keeps track of state for field: _qtAviso
     */
    private boolean _has_qtAviso;

    /**
     * Field _ocorrencias7List
     */
    private java.util.Vector _ocorrencias7List;

    /**
     * Field _qtComprovante
     */
    private int _qtComprovante = 0;

    /**
     * keeps track of state for field: _qtComprovante
     */
    private boolean _has_qtComprovante;

    /**
     * Field _ocorrencias8List
     */
    private java.util.Vector _ocorrencias8List;

    /**
     * Field _qtComprovanteSalarioDiversos
     */
    private int _qtComprovanteSalarioDiversos = 0;

    /**
     * keeps track of state for field: _qtComprovanteSalarioDiversos
     */
    private boolean _has_qtComprovanteSalarioDiversos;

    /**
     * Field _ocorrencias9List
     */
    private java.util.Vector _ocorrencias9List;

    /**
     * Field _dsCadastroFormaManutencao
     */
    private java.lang.String _dsCadastroFormaManutencao;

    /**
     * Field _cdCadastroQuantidadeDiasInativos
     */
    private int _cdCadastroQuantidadeDiasInativos = 0;

    /**
     * keeps track of state for field:
     * _cdCadastroQuantidadeDiasInativos
     */
    private boolean _has_cdCadastroQuantidadeDiasInativos;

    /**
     * Field _dsCadastroTipoConsistenciaInscricao
     */
    private java.lang.String _dsCadastroTipoConsistenciaInscricao;

    /**
     * Field _dsRecTipoIdentificacaoBeneficio
     */
    private java.lang.String _dsRecTipoIdentificacaoBeneficio;

    /**
     * Field _dsRecTipoConsistenciaIdentificacao
     */
    private java.lang.String _dsRecTipoConsistenciaIdentificacao;

    /**
     * Field _dsRecCondicaoEnquadramento
     */
    private java.lang.String _dsRecCondicaoEnquadramento;

    /**
     * Field _dsRecCriterioPrincipalEnquadramento
     */
    private java.lang.String _dsRecCriterioPrincipalEnquadramento;

    /**
     * Field _dsRecTipoCriterioCompostoEnquadramento
     */
    private java.lang.String _dsRecTipoCriterioCompostoEnquadramento;

    /**
     * Field _qtRecEtapaRecadastramento
     */
    private int _qtRecEtapaRecadastramento = 0;

    /**
     * keeps track of state for field: _qtRecEtapaRecadastramento
     */
    private boolean _has_qtRecEtapaRecadastramento;

    /**
     * Field _qtRecFaseEtapaRecadastramento
     */
    private int _qtRecFaseEtapaRecadastramento = 0;

    /**
     * keeps track of state for field: _qtRecFaseEtapaRecadastrament
     */
    private boolean _has_qtRecFaseEtapaRecadastramento;

    /**
     * Field _dtRecInicioRecadastramento
     */
    private java.lang.String _dtRecInicioRecadastramento;

    /**
     * Field _dtRecFinalRecadastramento
     */
    private java.lang.String _dtRecFinalRecadastramento;

    /**
     * Field _dsRecGeradorRetornoInternet
     */
    private java.lang.String _dsRecGeradorRetornoInternet;

    /**
     * Field _qtForTitulo
     */
    private int _qtForTitulo = 0;

    /**
     * keeps track of state for field: _qtForTitulo
     */
    private boolean _has_qtForTitulo;

    /**
     * Field _ocorrencias10List
     */
    private java.util.Vector _ocorrencias10List;

    /**
     * Field _qtCredito
     */
    private int _qtCredito = 0;

    /**
     * keeps track of state for field: _qtCredito
     */
    private boolean _has_qtCredito;

    /**
     * Field _ocorrencias11List
     */
    private java.util.Vector _ocorrencias11List;

    /**
     * Field _qtOperacao
     */
    private int _qtOperacao = 0;

    /**
     * keeps track of state for field: _qtOperacao
     */
    private boolean _has_qtOperacao;

    /**
     * Field _ocorrencias12List
     */
    private java.util.Vector _ocorrencias12List;

    /**
     * Field _qtModularidade
     */
    private int _qtModularidade = 0;

    /**
     * keeps track of state for field: _qtModularidade
     */
    private boolean _has_qtModularidade;

    /**
     * Field _ocorrencias13List
     */
    private java.util.Vector _ocorrencias13List;

    /**
     * Field _dsFraTipoRastreabilidade
     */
    private java.lang.String _dsFraTipoRastreabilidade;

    /**
     * Field _dsFraRastreabilidadeTerceiro
     */
    private java.lang.String _dsFraRastreabilidadeTerceiro;

    /**
     * Field _dsFraRastreabilidadeNota
     */
    private java.lang.String _dsFraRastreabilidadeNota;

    /**
     * Field _dsFraCaptacaoTitulo
     */
    private java.lang.String _dsFraCaptacaoTitulo;

    /**
     * Field _dsFraAgendaCliente
     */
    private java.lang.String _dsFraAgendaCliente;

    /**
     * Field _dsFraAgendaFilial
     */
    private java.lang.String _dsFraAgendaFilial;

    /**
     * Field _dsFraBloqueioEmissao
     */
    private java.lang.String _dsFraBloqueioEmissao;

    /**
     * Field _dtFraInicioBloqueio
     */
    private java.lang.String _dtFraInicioBloqueio;

    /**
     * Field _dtFraInicioRastreabilidade
     */
    private java.lang.String _dtFraInicioRastreabilidade;

    /**
     * Field _dsFraAdesaoSacado
     */
    private java.lang.String _dsFraAdesaoSacado;

    /**
     * Field _dsTdvTipoConsultaProposta
     */
    private java.lang.String _dsTdvTipoConsultaProposta;

    /**
     * Field _dsTdvTipoTratamentoValor
     */
    private java.lang.String _dsTdvTipoTratamentoValor;

    /**
     * Field _qtTriTributo
     */
    private int _qtTriTributo = 0;

    /**
     * keeps track of state for field: _qtTriTributo
     */
    private boolean _has_qtTriTributo;

    /**
     * Field _ocorrencias14List
     */
    private java.util.Vector _ocorrencias14List;

    /**
     * Field _dsBprTipoAcao
     */
    private java.lang.String _dsBprTipoAcao;

    /**
     * Field _qtBprMesProvisorio
     */
    private int _qtBprMesProvisorio = 0;

    /**
     * keeps track of state for field: _qtBprMesProvisorio
     */
    private boolean _has_qtBprMesProvisorio;

    /**
     * Field _cdBprIndicadorEmissaoAviso
     */
    private java.lang.String _cdBprIndicadorEmissaoAviso;

    /**
     * Field _qtBprDiaAnteriorAviso
     */
    private int _qtBprDiaAnteriorAviso = 0;

    /**
     * keeps track of state for field: _qtBprDiaAnteriorAviso
     */
    private boolean _has_qtBprDiaAnteriorAviso;

    /**
     * Field _qtBprDiaAnteriorVencimento
     */
    private int _qtBprDiaAnteriorVencimento = 0;

    /**
     * keeps track of state for field: _qtBprDiaAnteriorVencimento
     */
    private boolean _has_qtBprDiaAnteriorVencimento;

    /**
     * Field _cdBprUtilizadoMensagemPersonalizada
     */
    private java.lang.String _cdBprUtilizadoMensagemPersonalizada;

    /**
     * Field _dsBprDestino
     */
    private java.lang.String _dsBprDestino;

    /**
     * Field _dsBprCadastroEnderecoUtilizado
     */
    private java.lang.String _dsBprCadastroEnderecoUtilizado;

    /**
     * Field _qtAvisoFormulario
     */
    private int _qtAvisoFormulario = 0;

    /**
     * keeps track of state for field: _qtAvisoFormulario
     */
    private boolean _has_qtAvisoFormulario;

    /**
     * Field _ocorrencias15List
     */
    private java.util.Vector _ocorrencias15List;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImprimirAnexoSegundoContratoResponse() 
     {
        super();
        _ocorrencias1List = new Vector();
        _ocorrencias2List = new Vector();
        _ocorrencias3List = new Vector();
        _ocorrencias4List = new Vector();
        _ocorrencias5List = new Vector();
        _ocorrencias6List = new Vector();
        _ocorrencias7List = new Vector();
        _ocorrencias8List = new Vector();
        _ocorrencias9List = new Vector();
        _ocorrencias10List = new Vector();
        _ocorrencias11List = new Vector();
        _ocorrencias12List = new Vector();
        _ocorrencias13List = new Vector();
        _ocorrencias14List = new Vector();
        _ocorrencias15List = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.ImprimirAnexoSegundoContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param vOcorrencias1
     */
    public void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias1List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias1 has a maximum of 10");
        }
        _ocorrencias1List.addElement(vOcorrencias1);
    } //-- void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void addOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias1List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias1 has a maximum of 10");
        }
        _ocorrencias1List.insertElementAt(vOcorrencias1, index);
    } //-- void addOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1) 

    /**
     * Method addOcorrencias10
     * 
     * 
     * 
     * @param vOcorrencias10
     */
    public void addOcorrencias10(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 vOcorrencias10)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias10List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias10 has a maximum of 2");
        }
        _ocorrencias10List.addElement(vOcorrencias10);
    } //-- void addOcorrencias10(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10) 

    /**
     * Method addOcorrencias10
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias10
     */
    public void addOcorrencias10(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 vOcorrencias10)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias10List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias10 has a maximum of 2");
        }
        _ocorrencias10List.insertElementAt(vOcorrencias10, index);
    } //-- void addOcorrencias10(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10) 

    /**
     * Method addOcorrencias11
     * 
     * 
     * 
     * @param vOcorrencias11
     */
    public void addOcorrencias11(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 vOcorrencias11)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias11List.size() < 3)) {
            throw new IndexOutOfBoundsException("addOcorrencias11 has a maximum of 3");
        }
        _ocorrencias11List.addElement(vOcorrencias11);
    } //-- void addOcorrencias11(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11) 

    /**
     * Method addOcorrencias11
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias11
     */
    public void addOcorrencias11(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 vOcorrencias11)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias11List.size() < 3)) {
            throw new IndexOutOfBoundsException("addOcorrencias11 has a maximum of 3");
        }
        _ocorrencias11List.insertElementAt(vOcorrencias11, index);
    } //-- void addOcorrencias11(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11) 

    /**
     * Method addOcorrencias12
     * 
     * 
     * 
     * @param vOcorrencias12
     */
    public void addOcorrencias12(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 vOcorrencias12)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias12List.size() < 3)) {
            throw new IndexOutOfBoundsException("addOcorrencias12 has a maximum of 3");
        }
        _ocorrencias12List.addElement(vOcorrencias12);
    } //-- void addOcorrencias12(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12) 

    /**
     * Method addOcorrencias12
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias12
     */
    public void addOcorrencias12(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 vOcorrencias12)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias12List.size() < 3)) {
            throw new IndexOutOfBoundsException("addOcorrencias12 has a maximum of 3");
        }
        _ocorrencias12List.insertElementAt(vOcorrencias12, index);
    } //-- void addOcorrencias12(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12) 

    /**
     * Method addOcorrencias13
     * 
     * 
     * 
     * @param vOcorrencias13
     */
    public void addOcorrencias13(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 vOcorrencias13)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias13List.size() < 9)) {
            throw new IndexOutOfBoundsException("addOcorrencias13 has a maximum of 9");
        }
        _ocorrencias13List.addElement(vOcorrencias13);
    } //-- void addOcorrencias13(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13) 

    /**
     * Method addOcorrencias13
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias13
     */
    public void addOcorrencias13(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 vOcorrencias13)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias13List.size() < 9)) {
            throw new IndexOutOfBoundsException("addOcorrencias13 has a maximum of 9");
        }
        _ocorrencias13List.insertElementAt(vOcorrencias13, index);
    } //-- void addOcorrencias13(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13) 

    /**
     * Method addOcorrencias14
     * 
     * 
     * 
     * @param vOcorrencias14
     */
    public void addOcorrencias14(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 vOcorrencias14)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias14List.size() < 5)) {
            throw new IndexOutOfBoundsException("addOcorrencias14 has a maximum of 5");
        }
        _ocorrencias14List.addElement(vOcorrencias14);
    } //-- void addOcorrencias14(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14) 

    /**
     * Method addOcorrencias14
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias14
     */
    public void addOcorrencias14(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 vOcorrencias14)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias14List.size() < 5)) {
            throw new IndexOutOfBoundsException("addOcorrencias14 has a maximum of 5");
        }
        _ocorrencias14List.insertElementAt(vOcorrencias14, index);
    } //-- void addOcorrencias14(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14) 

    /**
     * Method addOcorrencias15
     * 
     * 
     * 
     * @param vOcorrencias15
     */
    public void addOcorrencias15(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 vOcorrencias15)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias15List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias15 has a maximum of 2");
        }
        _ocorrencias15List.addElement(vOcorrencias15);
    } //-- void addOcorrencias15(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15) 

    /**
     * Method addOcorrencias15
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias15
     */
    public void addOcorrencias15(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 vOcorrencias15)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias15List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias15 has a maximum of 2");
        }
        _ocorrencias15List.insertElementAt(vOcorrencias15, index);
    } //-- void addOcorrencias15(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param vOcorrencias2
     */
    public void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias2List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias2 has a maximum of 10");
        }
        _ocorrencias2List.addElement(vOcorrencias2);
    } //-- void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void addOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias2List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias2 has a maximum of 10");
        }
        _ocorrencias2List.insertElementAt(vOcorrencias2, index);
    } //-- void addOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2) 

    /**
     * Method addOcorrencias3
     * 
     * 
     * 
     * @param vOcorrencias3
     */
    public void addOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias3List.size() < 4)) {
            throw new IndexOutOfBoundsException("addOcorrencias3 has a maximum of 4");
        }
        _ocorrencias3List.addElement(vOcorrencias3);
    } //-- void addOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3) 

    /**
     * Method addOcorrencias3
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias3
     */
    public void addOcorrencias3(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias3List.size() < 4)) {
            throw new IndexOutOfBoundsException("addOcorrencias3 has a maximum of 4");
        }
        _ocorrencias3List.insertElementAt(vOcorrencias3, index);
    } //-- void addOcorrencias3(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3) 

    /**
     * Method addOcorrencias4
     * 
     * 
     * 
     * @param vOcorrencias4
     */
    public void addOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias4List.size() < 5)) {
            throw new IndexOutOfBoundsException("addOcorrencias4 has a maximum of 5");
        }
        _ocorrencias4List.addElement(vOcorrencias4);
    } //-- void addOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4) 

    /**
     * Method addOcorrencias4
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias4
     */
    public void addOcorrencias4(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias4List.size() < 5)) {
            throw new IndexOutOfBoundsException("addOcorrencias4 has a maximum of 5");
        }
        _ocorrencias4List.insertElementAt(vOcorrencias4, index);
    } //-- void addOcorrencias4(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4) 

    /**
     * Method addOcorrencias5
     * 
     * 
     * 
     * @param vOcorrencias5
     */
    public void addOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias5List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias5 has a maximum of 2");
        }
        _ocorrencias5List.addElement(vOcorrencias5);
    } //-- void addOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5) 

    /**
     * Method addOcorrencias5
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias5
     */
    public void addOcorrencias5(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias5List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias5 has a maximum of 2");
        }
        _ocorrencias5List.insertElementAt(vOcorrencias5, index);
    } //-- void addOcorrencias5(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5) 

    /**
     * Method addOcorrencias6
     * 
     * 
     * 
     * @param vOcorrencias6
     */
    public void addOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias6List.size() < 4)) {
            throw new IndexOutOfBoundsException("addOcorrencias6 has a maximum of 4");
        }
        _ocorrencias6List.addElement(vOcorrencias6);
    } //-- void addOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6) 

    /**
     * Method addOcorrencias6
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias6
     */
    public void addOcorrencias6(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias6List.size() < 4)) {
            throw new IndexOutOfBoundsException("addOcorrencias6 has a maximum of 4");
        }
        _ocorrencias6List.insertElementAt(vOcorrencias6, index);
    } //-- void addOcorrencias6(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6) 

    /**
     * Method addOcorrencias7
     * 
     * 
     * 
     * @param vOcorrencias7
     */
    public void addOcorrencias7(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 vOcorrencias7)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias7List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias7 has a maximum of 2");
        }
        _ocorrencias7List.addElement(vOcorrencias7);
    } //-- void addOcorrencias7(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7) 

    /**
     * Method addOcorrencias7
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias7
     */
    public void addOcorrencias7(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 vOcorrencias7)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias7List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias7 has a maximum of 2");
        }
        _ocorrencias7List.insertElementAt(vOcorrencias7, index);
    } //-- void addOcorrencias7(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7) 

    /**
     * Method addOcorrencias8
     * 
     * 
     * 
     * @param vOcorrencias8
     */
    public void addOcorrencias8(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 vOcorrencias8)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias8List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias8 has a maximum of 2");
        }
        _ocorrencias8List.addElement(vOcorrencias8);
    } //-- void addOcorrencias8(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8) 

    /**
     * Method addOcorrencias8
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias8
     */
    public void addOcorrencias8(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 vOcorrencias8)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias8List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias8 has a maximum of 2");
        }
        _ocorrencias8List.insertElementAt(vOcorrencias8, index);
    } //-- void addOcorrencias8(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8) 

    /**
     * Method addOcorrencias9
     * 
     * 
     * 
     * @param vOcorrencias9
     */
    public void addOcorrencias9(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 vOcorrencias9)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias9List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias9 has a maximum of 2");
        }
        _ocorrencias9List.addElement(vOcorrencias9);
    } //-- void addOcorrencias9(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9) 

    /**
     * Method addOcorrencias9
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias9
     */
    public void addOcorrencias9(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 vOcorrencias9)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias9List.size() < 2)) {
            throw new IndexOutOfBoundsException("addOcorrencias9 has a maximum of 2");
        }
        _ocorrencias9List.insertElementAt(vOcorrencias9, index);
    } //-- void addOcorrencias9(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9) 

    /**
     * Method deleteCdBeneficioQuantidadeDiasExpiracaoCredito
     * 
     */
    public void deleteCdBeneficioQuantidadeDiasExpiracaoCredito()
    {
        this._has_cdBeneficioQuantidadeDiasExpiracaoCredito= false;
    } //-- void deleteCdBeneficioQuantidadeDiasExpiracaoCredito() 

    /**
     * Method deleteCdCadastroQuantidadeDiasInativos
     * 
     */
    public void deleteCdCadastroQuantidadeDiasInativos()
    {
        this._has_cdCadastroQuantidadeDiasInativos= false;
    } //-- void deleteCdCadastroQuantidadeDiasInativos() 

    /**
     * Method deleteCdSalarioQuantidadeLimiteSolicitacaoCartao
     * 
     */
    public void deleteCdSalarioQuantidadeLimiteSolicitacaoCartao()
    {
        this._has_cdSalarioQuantidadeLimiteSolicitacaoCartao= false;
    } //-- void deleteCdSalarioQuantidadeLimiteSolicitacaoCartao() 

    /**
     * Method deleteCdServicoQuantidadeServicoPagamento
     * 
     */
    public void deleteCdServicoQuantidadeServicoPagamento()
    {
        this._has_cdServicoQuantidadeServicoPagamento= false;
    } //-- void deleteCdServicoQuantidadeServicoPagamento() 

    /**
     * Method deleteQtAviso
     * 
     */
    public void deleteQtAviso()
    {
        this._has_qtAviso= false;
    } //-- void deleteQtAviso() 

    /**
     * Method deleteQtAvisoFormulario
     * 
     */
    public void deleteQtAvisoFormulario()
    {
        this._has_qtAvisoFormulario= false;
    } //-- void deleteQtAvisoFormulario() 

    /**
     * Method deleteQtBprDiaAnteriorAviso
     * 
     */
    public void deleteQtBprDiaAnteriorAviso()
    {
        this._has_qtBprDiaAnteriorAviso= false;
    } //-- void deleteQtBprDiaAnteriorAviso() 

    /**
     * Method deleteQtBprDiaAnteriorVencimento
     * 
     */
    public void deleteQtBprDiaAnteriorVencimento()
    {
        this._has_qtBprDiaAnteriorVencimento= false;
    } //-- void deleteQtBprDiaAnteriorVencimento() 

    /**
     * Method deleteQtBprMesProvisorio
     * 
     */
    public void deleteQtBprMesProvisorio()
    {
        this._has_qtBprMesProvisorio= false;
    } //-- void deleteQtBprMesProvisorio() 

    /**
     * Method deleteQtComprovante
     * 
     */
    public void deleteQtComprovante()
    {
        this._has_qtComprovante= false;
    } //-- void deleteQtComprovante() 

    /**
     * Method deleteQtComprovanteSalarioDiversos
     * 
     */
    public void deleteQtComprovanteSalarioDiversos()
    {
        this._has_qtComprovanteSalarioDiversos= false;
    } //-- void deleteQtComprovanteSalarioDiversos() 

    /**
     * Method deleteQtCredito
     * 
     */
    public void deleteQtCredito()
    {
        this._has_qtCredito= false;
    } //-- void deleteQtCredito() 

    /**
     * Method deleteQtForTitulo
     * 
     */
    public void deleteQtForTitulo()
    {
        this._has_qtForTitulo= false;
    } //-- void deleteQtForTitulo() 

    /**
     * Method deleteQtModalidadeBeneficio
     * 
     */
    public void deleteQtModalidadeBeneficio()
    {
        this._has_qtModalidadeBeneficio= false;
    } //-- void deleteQtModalidadeBeneficio() 

    /**
     * Method deleteQtModalidadeFornecedor
     * 
     */
    public void deleteQtModalidadeFornecedor()
    {
        this._has_qtModalidadeFornecedor= false;
    } //-- void deleteQtModalidadeFornecedor() 

    /**
     * Method deleteQtModalidadeRecadastro
     * 
     */
    public void deleteQtModalidadeRecadastro()
    {
        this._has_qtModalidadeRecadastro= false;
    } //-- void deleteQtModalidadeRecadastro() 

    /**
     * Method deleteQtModalidadeSalario
     * 
     */
    public void deleteQtModalidadeSalario()
    {
        this._has_qtModalidadeSalario= false;
    } //-- void deleteQtModalidadeSalario() 

    /**
     * Method deleteQtModalidadeTributos
     * 
     */
    public void deleteQtModalidadeTributos()
    {
        this._has_qtModalidadeTributos= false;
    } //-- void deleteQtModalidadeTributos() 

    /**
     * Method deleteQtModularidade
     * 
     */
    public void deleteQtModularidade()
    {
        this._has_qtModularidade= false;
    } //-- void deleteQtModularidade() 

    /**
     * Method deleteQtOperacao
     * 
     */
    public void deleteQtOperacao()
    {
        this._has_qtOperacao= false;
    } //-- void deleteQtOperacao() 

    /**
     * Method deleteQtRecEtapaRecadastramento
     * 
     */
    public void deleteQtRecEtapaRecadastramento()
    {
        this._has_qtRecEtapaRecadastramento= false;
    } //-- void deleteQtRecEtapaRecadastramento() 

    /**
     * Method deleteQtRecFaseEtapaRecadastramento
     * 
     */
    public void deleteQtRecFaseEtapaRecadastramento()
    {
        this._has_qtRecFaseEtapaRecadastramento= false;
    } //-- void deleteQtRecFaseEtapaRecadastramento() 

    /**
     * Method deleteQtTriTributo
     * 
     */
    public void deleteQtTriTributo()
    {
        this._has_qtTriTributo= false;
    } //-- void deleteQtTriTributo() 

    /**
     * Method enumerateOcorrencias1
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias1()
    {
        return _ocorrencias1List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias1() 

    /**
     * Method enumerateOcorrencias10
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias10()
    {
        return _ocorrencias10List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias10() 

    /**
     * Method enumerateOcorrencias11
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias11()
    {
        return _ocorrencias11List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias11() 

    /**
     * Method enumerateOcorrencias12
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias12()
    {
        return _ocorrencias12List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias12() 

    /**
     * Method enumerateOcorrencias13
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias13()
    {
        return _ocorrencias13List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias13() 

    /**
     * Method enumerateOcorrencias14
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias14()
    {
        return _ocorrencias14List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias14() 

    /**
     * Method enumerateOcorrencias15
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias15()
    {
        return _ocorrencias15List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias15() 

    /**
     * Method enumerateOcorrencias2
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias2()
    {
        return _ocorrencias2List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias2() 

    /**
     * Method enumerateOcorrencias3
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias3()
    {
        return _ocorrencias3List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias3() 

    /**
     * Method enumerateOcorrencias4
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias4()
    {
        return _ocorrencias4List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias4() 

    /**
     * Method enumerateOcorrencias5
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias5()
    {
        return _ocorrencias5List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias5() 

    /**
     * Method enumerateOcorrencias6
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias6()
    {
        return _ocorrencias6List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias6() 

    /**
     * Method enumerateOcorrencias7
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias7()
    {
        return _ocorrencias7List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias7() 

    /**
     * Method enumerateOcorrencias8
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias8()
    {
        return _ocorrencias8List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias8() 

    /**
     * Method enumerateOcorrencias9
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias9()
    {
        return _ocorrencias9List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias9() 

    /**
     * Returns the value of field
     * 'cdBeneficioQuantidadeDiasExpiracaoCredito'.
     * 
     * @return int
     * @return the value of field
     * 'cdBeneficioQuantidadeDiasExpiracaoCredito'.
     */
    public int getCdBeneficioQuantidadeDiasExpiracaoCredito()
    {
        return this._cdBeneficioQuantidadeDiasExpiracaoCredito;
    } //-- int getCdBeneficioQuantidadeDiasExpiracaoCredito() 

    /**
     * Returns the value of field 'cdBprIndicadorEmissaoAviso'.
     * 
     * @return String
     * @return the value of field 'cdBprIndicadorEmissaoAviso'.
     */
    public java.lang.String getCdBprIndicadorEmissaoAviso()
    {
        return this._cdBprIndicadorEmissaoAviso;
    } //-- java.lang.String getCdBprIndicadorEmissaoAviso() 

    /**
     * Returns the value of field
     * 'cdBprUtilizadoMensagemPersonalizada'.
     * 
     * @return String
     * @return the value of field
     * 'cdBprUtilizadoMensagemPersonalizada'.
     */
    public java.lang.String getCdBprUtilizadoMensagemPersonalizada()
    {
        return this._cdBprUtilizadoMensagemPersonalizada;
    } //-- java.lang.String getCdBprUtilizadoMensagemPersonalizada() 

    /**
     * Returns the value of field
     * 'cdCadastroQuantidadeDiasInativos'.
     * 
     * @return int
     * @return the value of field 'cdCadastroQuantidadeDiasInativos'
     */
    public int getCdCadastroQuantidadeDiasInativos()
    {
        return this._cdCadastroQuantidadeDiasInativos;
    } //-- int getCdCadastroQuantidadeDiasInativos() 

    /**
     * Returns the value of field 'cdIndcadorCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdIndcadorCadastroFavorecido'.
     */
    public java.lang.String getCdIndcadorCadastroFavorecido()
    {
        return this._cdIndcadorCadastroFavorecido;
    } //-- java.lang.String getCdIndcadorCadastroFavorecido() 

    /**
     * Returns the value of field 'cdIndicadorAviso'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAviso'.
     */
    public java.lang.String getCdIndicadorAviso()
    {
        return this._cdIndicadorAviso;
    } //-- java.lang.String getCdIndicadorAviso() 

    /**
     * Returns the value of field 'cdIndicadorAvisoFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAvisoFavorecido'.
     */
    public java.lang.String getCdIndicadorAvisoFavorecido()
    {
        return this._cdIndicadorAvisoFavorecido;
    } //-- java.lang.String getCdIndicadorAvisoFavorecido() 

    /**
     * Returns the value of field 'cdIndicadorAvisoPagador'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAvisoPagador'.
     */
    public java.lang.String getCdIndicadorAvisoPagador()
    {
        return this._cdIndicadorAvisoPagador;
    } //-- java.lang.String getCdIndicadorAvisoPagador() 

    /**
     * Returns the value of field 'cdIndicadorComplemento'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorComplemento'.
     */
    public java.lang.String getCdIndicadorComplemento()
    {
        return this._cdIndicadorComplemento;
    } //-- java.lang.String getCdIndicadorComplemento() 

    /**
     * Returns the value of field
     * 'cdIndicadorComplementoDivergente'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorComplementoDivergente'
     */
    public java.lang.String getCdIndicadorComplementoDivergente()
    {
        return this._cdIndicadorComplementoDivergente;
    } //-- java.lang.String getCdIndicadorComplementoDivergente() 

    /**
     * Returns the value of field
     * 'cdIndicadorComplementoFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorComplementoFavorecido'
     */
    public java.lang.String getCdIndicadorComplementoFavorecido()
    {
        return this._cdIndicadorComplementoFavorecido;
    } //-- java.lang.String getCdIndicadorComplementoFavorecido() 

    /**
     * Returns the value of field 'cdIndicadorComplementoPagador'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorComplementoPagador'.
     */
    public java.lang.String getCdIndicadorComplementoPagador()
    {
        return this._cdIndicadorComplementoPagador;
    } //-- java.lang.String getCdIndicadorComplementoPagador() 

    /**
     * Returns the value of field 'cdIndicadorComplementoSalarial'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorComplementoSalarial'.
     */
    public java.lang.String getCdIndicadorComplementoSalarial()
    {
        return this._cdIndicadorComplementoSalarial;
    } //-- java.lang.String getCdIndicadorComplementoSalarial() 

    /**
     * Returns the value of field
     * 'cdSalarioQuantidadeLimiteSolicitacaoCartao'.
     * 
     * @return int
     * @return the value of field
     * 'cdSalarioQuantidadeLimiteSolicitacaoCartao'.
     */
    public int getCdSalarioQuantidadeLimiteSolicitacaoCartao()
    {
        return this._cdSalarioQuantidadeLimiteSolicitacaoCartao;
    } //-- int getCdSalarioQuantidadeLimiteSolicitacaoCartao() 

    /**
     * Returns the value of field 'cdServicoBeneficio'.
     * 
     * @return String
     * @return the value of field 'cdServicoBeneficio'.
     */
    public java.lang.String getCdServicoBeneficio()
    {
        return this._cdServicoBeneficio;
    } //-- java.lang.String getCdServicoBeneficio() 

    /**
     * Returns the value of field 'cdServicoFornecedor'.
     * 
     * @return String
     * @return the value of field 'cdServicoFornecedor'.
     */
    public java.lang.String getCdServicoFornecedor()
    {
        return this._cdServicoFornecedor;
    } //-- java.lang.String getCdServicoFornecedor() 

    /**
     * Returns the value of field
     * 'cdServicoQuantidadeServicoPagamento'.
     * 
     * @return int
     * @return the value of field
     * 'cdServicoQuantidadeServicoPagamento'.
     */
    public int getCdServicoQuantidadeServicoPagamento()
    {
        return this._cdServicoQuantidadeServicoPagamento;
    } //-- int getCdServicoQuantidadeServicoPagamento() 

    /**
     * Returns the value of field 'cdServicoSalarial'.
     * 
     * @return String
     * @return the value of field 'cdServicoSalarial'.
     */
    public java.lang.String getCdServicoSalarial()
    {
        return this._cdServicoSalarial;
    } //-- java.lang.String getCdServicoSalarial() 

    /**
     * Returns the value of field 'cdServicoTributos'.
     * 
     * @return String
     * @return the value of field 'cdServicoTributos'.
     */
    public java.lang.String getCdServicoTributos()
    {
        return this._cdServicoTributos;
    } //-- java.lang.String getCdServicoTributos() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field
     * 'dsBeneficioInformadoAgenciaConta'.
     * 
     * @return String
     * @return the value of field 'dsBeneficioInformadoAgenciaConta'
     */
    public java.lang.String getDsBeneficioInformadoAgenciaConta()
    {
        return this._dsBeneficioInformadoAgenciaConta;
    } //-- java.lang.String getDsBeneficioInformadoAgenciaConta() 

    /**
     * Returns the value of field
     * 'dsBeneficioPossuiExpiracaoCredito'.
     * 
     * @return String
     * @return the value of field
     * 'dsBeneficioPossuiExpiracaoCredito'.
     */
    public java.lang.String getDsBeneficioPossuiExpiracaoCredito()
    {
        return this._dsBeneficioPossuiExpiracaoCredito;
    } //-- java.lang.String getDsBeneficioPossuiExpiracaoCredito() 

    /**
     * Returns the value of field
     * 'dsBeneficioTipoIdentificacaoBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'dsBeneficioTipoIdentificacaoBeneficio'.
     */
    public java.lang.String getDsBeneficioTipoIdentificacaoBeneficio()
    {
        return this._dsBeneficioTipoIdentificacaoBeneficio;
    } //-- java.lang.String getDsBeneficioTipoIdentificacaoBeneficio() 

    /**
     * Returns the value of field
     * 'dsBeneficioUtilizacaoCadastroOrganizacao'.
     * 
     * @return String
     * @return the value of field
     * 'dsBeneficioUtilizacaoCadastroOrganizacao'.
     */
    public java.lang.String getDsBeneficioUtilizacaoCadastroOrganizacao()
    {
        return this._dsBeneficioUtilizacaoCadastroOrganizacao;
    } //-- java.lang.String getDsBeneficioUtilizacaoCadastroOrganizacao() 

    /**
     * Returns the value of field
     * 'dsBeneficioUtilizacaoCadastroProcuradores'.
     * 
     * @return String
     * @return the value of field
     * 'dsBeneficioUtilizacaoCadastroProcuradores'.
     */
    public java.lang.String getDsBeneficioUtilizacaoCadastroProcuradores()
    {
        return this._dsBeneficioUtilizacaoCadastroProcuradores;
    } //-- java.lang.String getDsBeneficioUtilizacaoCadastroProcuradores() 

    /**
     * Returns the value of field 'dsBprCadastroEnderecoUtilizado'.
     * 
     * @return String
     * @return the value of field 'dsBprCadastroEnderecoUtilizado'.
     */
    public java.lang.String getDsBprCadastroEnderecoUtilizado()
    {
        return this._dsBprCadastroEnderecoUtilizado;
    } //-- java.lang.String getDsBprCadastroEnderecoUtilizado() 

    /**
     * Returns the value of field 'dsBprDestino'.
     * 
     * @return String
     * @return the value of field 'dsBprDestino'.
     */
    public java.lang.String getDsBprDestino()
    {
        return this._dsBprDestino;
    } //-- java.lang.String getDsBprDestino() 

    /**
     * Returns the value of field 'dsBprTipoAcao'.
     * 
     * @return String
     * @return the value of field 'dsBprTipoAcao'.
     */
    public java.lang.String getDsBprTipoAcao()
    {
        return this._dsBprTipoAcao;
    } //-- java.lang.String getDsBprTipoAcao() 

    /**
     * Returns the value of field 'dsCadastroFormaManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCadastroFormaManutencao'.
     */
    public java.lang.String getDsCadastroFormaManutencao()
    {
        return this._dsCadastroFormaManutencao;
    } //-- java.lang.String getDsCadastroFormaManutencao() 

    /**
     * Returns the value of field
     * 'dsCadastroTipoConsistenciaInscricao'.
     * 
     * @return String
     * @return the value of field
     * 'dsCadastroTipoConsistenciaInscricao'.
     */
    public java.lang.String getDsCadastroTipoConsistenciaInscricao()
    {
        return this._dsCadastroTipoConsistenciaInscricao;
    } //-- java.lang.String getDsCadastroTipoConsistenciaInscricao() 

    /**
     * Returns the value of field 'dsFraAdesaoSacado'.
     * 
     * @return String
     * @return the value of field 'dsFraAdesaoSacado'.
     */
    public java.lang.String getDsFraAdesaoSacado()
    {
        return this._dsFraAdesaoSacado;
    } //-- java.lang.String getDsFraAdesaoSacado() 

    /**
     * Returns the value of field 'dsFraAgendaCliente'.
     * 
     * @return String
     * @return the value of field 'dsFraAgendaCliente'.
     */
    public java.lang.String getDsFraAgendaCliente()
    {
        return this._dsFraAgendaCliente;
    } //-- java.lang.String getDsFraAgendaCliente() 

    /**
     * Returns the value of field 'dsFraAgendaFilial'.
     * 
     * @return String
     * @return the value of field 'dsFraAgendaFilial'.
     */
    public java.lang.String getDsFraAgendaFilial()
    {
        return this._dsFraAgendaFilial;
    } //-- java.lang.String getDsFraAgendaFilial() 

    /**
     * Returns the value of field 'dsFraBloqueioEmissao'.
     * 
     * @return String
     * @return the value of field 'dsFraBloqueioEmissao'.
     */
    public java.lang.String getDsFraBloqueioEmissao()
    {
        return this._dsFraBloqueioEmissao;
    } //-- java.lang.String getDsFraBloqueioEmissao() 

    /**
     * Returns the value of field 'dsFraCaptacaoTitulo'.
     * 
     * @return String
     * @return the value of field 'dsFraCaptacaoTitulo'.
     */
    public java.lang.String getDsFraCaptacaoTitulo()
    {
        return this._dsFraCaptacaoTitulo;
    } //-- java.lang.String getDsFraCaptacaoTitulo() 

    /**
     * Returns the value of field 'dsFraRastreabilidadeNota'.
     * 
     * @return String
     * @return the value of field 'dsFraRastreabilidadeNota'.
     */
    public java.lang.String getDsFraRastreabilidadeNota()
    {
        return this._dsFraRastreabilidadeNota;
    } //-- java.lang.String getDsFraRastreabilidadeNota() 

    /**
     * Returns the value of field 'dsFraRastreabilidadeTerceiro'.
     * 
     * @return String
     * @return the value of field 'dsFraRastreabilidadeTerceiro'.
     */
    public java.lang.String getDsFraRastreabilidadeTerceiro()
    {
        return this._dsFraRastreabilidadeTerceiro;
    } //-- java.lang.String getDsFraRastreabilidadeTerceiro() 

    /**
     * Returns the value of field 'dsFraTipoRastreabilidade'.
     * 
     * @return String
     * @return the value of field 'dsFraTipoRastreabilidade'.
     */
    public java.lang.String getDsFraTipoRastreabilidade()
    {
        return this._dsFraTipoRastreabilidade;
    } //-- java.lang.String getDsFraTipoRastreabilidade() 

    /**
     * Returns the value of field 'dsRecCondicaoEnquadramento'.
     * 
     * @return String
     * @return the value of field 'dsRecCondicaoEnquadramento'.
     */
    public java.lang.String getDsRecCondicaoEnquadramento()
    {
        return this._dsRecCondicaoEnquadramento;
    } //-- java.lang.String getDsRecCondicaoEnquadramento() 

    /**
     * Returns the value of field
     * 'dsRecCriterioPrincipalEnquadramento'.
     * 
     * @return String
     * @return the value of field
     * 'dsRecCriterioPrincipalEnquadramento'.
     */
    public java.lang.String getDsRecCriterioPrincipalEnquadramento()
    {
        return this._dsRecCriterioPrincipalEnquadramento;
    } //-- java.lang.String getDsRecCriterioPrincipalEnquadramento() 

    /**
     * Returns the value of field 'dsRecGeradorRetornoInternet'.
     * 
     * @return String
     * @return the value of field 'dsRecGeradorRetornoInternet'.
     */
    public java.lang.String getDsRecGeradorRetornoInternet()
    {
        return this._dsRecGeradorRetornoInternet;
    } //-- java.lang.String getDsRecGeradorRetornoInternet() 

    /**
     * Returns the value of field
     * 'dsRecTipoConsistenciaIdentificacao'.
     * 
     * @return String
     * @return the value of field
     * 'dsRecTipoConsistenciaIdentificacao'.
     */
    public java.lang.String getDsRecTipoConsistenciaIdentificacao()
    {
        return this._dsRecTipoConsistenciaIdentificacao;
    } //-- java.lang.String getDsRecTipoConsistenciaIdentificacao() 

    /**
     * Returns the value of field
     * 'dsRecTipoCriterioCompostoEnquadramento'.
     * 
     * @return String
     * @return the value of field
     * 'dsRecTipoCriterioCompostoEnquadramento'.
     */
    public java.lang.String getDsRecTipoCriterioCompostoEnquadramento()
    {
        return this._dsRecTipoCriterioCompostoEnquadramento;
    } //-- java.lang.String getDsRecTipoCriterioCompostoEnquadramento() 

    /**
     * Returns the value of field
     * 'dsRecTipoIdentificacaoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsRecTipoIdentificacaoBeneficio'.
     */
    public java.lang.String getDsRecTipoIdentificacaoBeneficio()
    {
        return this._dsRecTipoIdentificacaoBeneficio;
    } //-- java.lang.String getDsRecTipoIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'dsSalarioAberturaContaExpressa'.
     * 
     * @return String
     * @return the value of field 'dsSalarioAberturaContaExpressa'.
     */
    public java.lang.String getDsSalarioAberturaContaExpressa()
    {
        return this._dsSalarioAberturaContaExpressa;
    } //-- java.lang.String getDsSalarioAberturaContaExpressa() 

    /**
     * Returns the value of field
     * 'dsSalarioEmissaoAntecipadoCartao'.
     * 
     * @return String
     * @return the value of field 'dsSalarioEmissaoAntecipadoCartao'
     */
    public java.lang.String getDsSalarioEmissaoAntecipadoCartao()
    {
        return this._dsSalarioEmissaoAntecipadoCartao;
    } //-- java.lang.String getDsSalarioEmissaoAntecipadoCartao() 

    /**
     * Returns the value of field 'dsServicoRecadastro'.
     * 
     * @return String
     * @return the value of field 'dsServicoRecadastro'.
     */
    public java.lang.String getDsServicoRecadastro()
    {
        return this._dsServicoRecadastro;
    } //-- java.lang.String getDsServicoRecadastro() 

    /**
     * Returns the value of field 'dsTdvTipoConsultaProposta'.
     * 
     * @return String
     * @return the value of field 'dsTdvTipoConsultaProposta'.
     */
    public java.lang.String getDsTdvTipoConsultaProposta()
    {
        return this._dsTdvTipoConsultaProposta;
    } //-- java.lang.String getDsTdvTipoConsultaProposta() 

    /**
     * Returns the value of field 'dsTdvTipoTratamentoValor'.
     * 
     * @return String
     * @return the value of field 'dsTdvTipoTratamentoValor'.
     */
    public java.lang.String getDsTdvTipoTratamentoValor()
    {
        return this._dsTdvTipoTratamentoValor;
    } //-- java.lang.String getDsTdvTipoTratamentoValor() 

    /**
     * Returns the value of field 'dsTibutoAgendadoDebitoVeicular'.
     * 
     * @return String
     * @return the value of field 'dsTibutoAgendadoDebitoVeicular'.
     */
    public java.lang.String getDsTibutoAgendadoDebitoVeicular()
    {
        return this._dsTibutoAgendadoDebitoVeicular;
    } //-- java.lang.String getDsTibutoAgendadoDebitoVeicular() 

    /**
     * Returns the value of field 'dtFraInicioBloqueio'.
     * 
     * @return String
     * @return the value of field 'dtFraInicioBloqueio'.
     */
    public java.lang.String getDtFraInicioBloqueio()
    {
        return this._dtFraInicioBloqueio;
    } //-- java.lang.String getDtFraInicioBloqueio() 

    /**
     * Returns the value of field 'dtFraInicioRastreabilidade'.
     * 
     * @return String
     * @return the value of field 'dtFraInicioRastreabilidade'.
     */
    public java.lang.String getDtFraInicioRastreabilidade()
    {
        return this._dtFraInicioRastreabilidade;
    } //-- java.lang.String getDtFraInicioRastreabilidade() 

    /**
     * Returns the value of field 'dtRecFinalRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dtRecFinalRecadastramento'.
     */
    public java.lang.String getDtRecFinalRecadastramento()
    {
        return this._dtRecFinalRecadastramento;
    } //-- java.lang.String getDtRecFinalRecadastramento() 

    /**
     * Returns the value of field 'dtRecInicioRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dtRecInicioRecadastramento'.
     */
    public java.lang.String getDtRecInicioRecadastramento()
    {
        return this._dtRecInicioRecadastramento;
    } //-- java.lang.String getDtRecInicioRecadastramento() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1 getOcorrencias1(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias1: Index value '"+index+"' not in range [0.."+(_ocorrencias1List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1 getOcorrencias1(int) 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1[] getOcorrencias1()
    {
        int size = _ocorrencias1List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1[] getOcorrencias1() 

    /**
     * Method getOcorrencias10
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias10
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 getOcorrencias10(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias10List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias10: Index value '"+index+"' not in range [0.."+(_ocorrencias10List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10) _ocorrencias10List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 getOcorrencias10(int) 

    /**
     * Method getOcorrencias10
     * 
     * 
     * 
     * @return Ocorrencias10
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10[] getOcorrencias10()
    {
        int size = _ocorrencias10List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10) _ocorrencias10List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10[] getOcorrencias10() 

    /**
     * Method getOcorrencias10Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias10Count()
    {
        return _ocorrencias10List.size();
    } //-- int getOcorrencias10Count() 

    /**
     * Method getOcorrencias11
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias11
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 getOcorrencias11(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias11List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias11: Index value '"+index+"' not in range [0.."+(_ocorrencias11List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11) _ocorrencias11List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 getOcorrencias11(int) 

    /**
     * Method getOcorrencias11
     * 
     * 
     * 
     * @return Ocorrencias11
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11[] getOcorrencias11()
    {
        int size = _ocorrencias11List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11) _ocorrencias11List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11[] getOcorrencias11() 

    /**
     * Method getOcorrencias11Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias11Count()
    {
        return _ocorrencias11List.size();
    } //-- int getOcorrencias11Count() 

    /**
     * Method getOcorrencias12
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias12
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 getOcorrencias12(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias12List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias12: Index value '"+index+"' not in range [0.."+(_ocorrencias12List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12) _ocorrencias12List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 getOcorrencias12(int) 

    /**
     * Method getOcorrencias12
     * 
     * 
     * 
     * @return Ocorrencias12
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12[] getOcorrencias12()
    {
        int size = _ocorrencias12List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12) _ocorrencias12List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12[] getOcorrencias12() 

    /**
     * Method getOcorrencias12Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias12Count()
    {
        return _ocorrencias12List.size();
    } //-- int getOcorrencias12Count() 

    /**
     * Method getOcorrencias13
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias13
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 getOcorrencias13(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias13List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias13: Index value '"+index+"' not in range [0.."+(_ocorrencias13List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13) _ocorrencias13List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 getOcorrencias13(int) 

    /**
     * Method getOcorrencias13
     * 
     * 
     * 
     * @return Ocorrencias13
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13[] getOcorrencias13()
    {
        int size = _ocorrencias13List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13) _ocorrencias13List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13[] getOcorrencias13() 

    /**
     * Method getOcorrencias13Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias13Count()
    {
        return _ocorrencias13List.size();
    } //-- int getOcorrencias13Count() 

    /**
     * Method getOcorrencias14
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias14
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 getOcorrencias14(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias14List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias14: Index value '"+index+"' not in range [0.."+(_ocorrencias14List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14) _ocorrencias14List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 getOcorrencias14(int) 

    /**
     * Method getOcorrencias14
     * 
     * 
     * 
     * @return Ocorrencias14
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14[] getOcorrencias14()
    {
        int size = _ocorrencias14List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14) _ocorrencias14List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14[] getOcorrencias14() 

    /**
     * Method getOcorrencias14Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias14Count()
    {
        return _ocorrencias14List.size();
    } //-- int getOcorrencias14Count() 

    /**
     * Method getOcorrencias15
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias15
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 getOcorrencias15(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias15List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias15: Index value '"+index+"' not in range [0.."+(_ocorrencias15List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15) _ocorrencias15List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 getOcorrencias15(int) 

    /**
     * Method getOcorrencias15
     * 
     * 
     * 
     * @return Ocorrencias15
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15[] getOcorrencias15()
    {
        int size = _ocorrencias15List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15) _ocorrencias15List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15[] getOcorrencias15() 

    /**
     * Method getOcorrencias15Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias15Count()
    {
        return _ocorrencias15List.size();
    } //-- int getOcorrencias15Count() 

    /**
     * Method getOcorrencias1Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias1Count()
    {
        return _ocorrencias1List.size();
    } //-- int getOcorrencias1Count() 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2 getOcorrencias2(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias2: Index value '"+index+"' not in range [0.."+(_ocorrencias2List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2 getOcorrencias2(int) 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2[] getOcorrencias2()
    {
        int size = _ocorrencias2List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2[] getOcorrencias2() 

    /**
     * Method getOcorrencias2Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias2Count()
    {
        return _ocorrencias2List.size();
    } //-- int getOcorrencias2Count() 

    /**
     * Method getOcorrencias3
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3 getOcorrencias3(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias3List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias3: Index value '"+index+"' not in range [0.."+(_ocorrencias3List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3) _ocorrencias3List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3 getOcorrencias3(int) 

    /**
     * Method getOcorrencias3
     * 
     * 
     * 
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3[] getOcorrencias3()
    {
        int size = _ocorrencias3List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3) _ocorrencias3List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3[] getOcorrencias3() 

    /**
     * Method getOcorrencias3Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias3Count()
    {
        return _ocorrencias3List.size();
    } //-- int getOcorrencias3Count() 

    /**
     * Method getOcorrencias4
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4 getOcorrencias4(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias4List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias4: Index value '"+index+"' not in range [0.."+(_ocorrencias4List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4) _ocorrencias4List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4 getOcorrencias4(int) 

    /**
     * Method getOcorrencias4
     * 
     * 
     * 
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4[] getOcorrencias4()
    {
        int size = _ocorrencias4List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4) _ocorrencias4List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4[] getOcorrencias4() 

    /**
     * Method getOcorrencias4Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias4Count()
    {
        return _ocorrencias4List.size();
    } //-- int getOcorrencias4Count() 

    /**
     * Method getOcorrencias5
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5 getOcorrencias5(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias5List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias5: Index value '"+index+"' not in range [0.."+(_ocorrencias5List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5) _ocorrencias5List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5 getOcorrencias5(int) 

    /**
     * Method getOcorrencias5
     * 
     * 
     * 
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5[] getOcorrencias5()
    {
        int size = _ocorrencias5List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5) _ocorrencias5List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5[] getOcorrencias5() 

    /**
     * Method getOcorrencias5Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias5Count()
    {
        return _ocorrencias5List.size();
    } //-- int getOcorrencias5Count() 

    /**
     * Method getOcorrencias6
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 getOcorrencias6(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias6List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias6: Index value '"+index+"' not in range [0.."+(_ocorrencias6List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6) _ocorrencias6List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 getOcorrencias6(int) 

    /**
     * Method getOcorrencias6
     * 
     * 
     * 
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6[] getOcorrencias6()
    {
        int size = _ocorrencias6List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6) _ocorrencias6List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6[] getOcorrencias6() 

    /**
     * Method getOcorrencias6Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias6Count()
    {
        return _ocorrencias6List.size();
    } //-- int getOcorrencias6Count() 

    /**
     * Method getOcorrencias7
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias7
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 getOcorrencias7(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias7List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias7: Index value '"+index+"' not in range [0.."+(_ocorrencias7List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7) _ocorrencias7List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 getOcorrencias7(int) 

    /**
     * Method getOcorrencias7
     * 
     * 
     * 
     * @return Ocorrencias7
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7[] getOcorrencias7()
    {
        int size = _ocorrencias7List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7) _ocorrencias7List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7[] getOcorrencias7() 

    /**
     * Method getOcorrencias7Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias7Count()
    {
        return _ocorrencias7List.size();
    } //-- int getOcorrencias7Count() 

    /**
     * Method getOcorrencias8
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias8
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 getOcorrencias8(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias8List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias8: Index value '"+index+"' not in range [0.."+(_ocorrencias8List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8) _ocorrencias8List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 getOcorrencias8(int) 

    /**
     * Method getOcorrencias8
     * 
     * 
     * 
     * @return Ocorrencias8
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8[] getOcorrencias8()
    {
        int size = _ocorrencias8List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8) _ocorrencias8List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8[] getOcorrencias8() 

    /**
     * Method getOcorrencias8Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias8Count()
    {
        return _ocorrencias8List.size();
    } //-- int getOcorrencias8Count() 

    /**
     * Method getOcorrencias9
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias9
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 getOcorrencias9(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias9List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias9: Index value '"+index+"' not in range [0.."+(_ocorrencias9List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9) _ocorrencias9List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 getOcorrencias9(int) 

    /**
     * Method getOcorrencias9
     * 
     * 
     * 
     * @return Ocorrencias9
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9[] getOcorrencias9()
    {
        int size = _ocorrencias9List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9) _ocorrencias9List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9[] getOcorrencias9() 

    /**
     * Method getOcorrencias9Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias9Count()
    {
        return _ocorrencias9List.size();
    } //-- int getOcorrencias9Count() 

    /**
     * Returns the value of field 'qtAviso'.
     * 
     * @return int
     * @return the value of field 'qtAviso'.
     */
    public int getQtAviso()
    {
        return this._qtAviso;
    } //-- int getQtAviso() 

    /**
     * Returns the value of field 'qtAvisoFormulario'.
     * 
     * @return int
     * @return the value of field 'qtAvisoFormulario'.
     */
    public int getQtAvisoFormulario()
    {
        return this._qtAvisoFormulario;
    } //-- int getQtAvisoFormulario() 

    /**
     * Returns the value of field 'qtBprDiaAnteriorAviso'.
     * 
     * @return int
     * @return the value of field 'qtBprDiaAnteriorAviso'.
     */
    public int getQtBprDiaAnteriorAviso()
    {
        return this._qtBprDiaAnteriorAviso;
    } //-- int getQtBprDiaAnteriorAviso() 

    /**
     * Returns the value of field 'qtBprDiaAnteriorVencimento'.
     * 
     * @return int
     * @return the value of field 'qtBprDiaAnteriorVencimento'.
     */
    public int getQtBprDiaAnteriorVencimento()
    {
        return this._qtBprDiaAnteriorVencimento;
    } //-- int getQtBprDiaAnteriorVencimento() 

    /**
     * Returns the value of field 'qtBprMesProvisorio'.
     * 
     * @return int
     * @return the value of field 'qtBprMesProvisorio'.
     */
    public int getQtBprMesProvisorio()
    {
        return this._qtBprMesProvisorio;
    } //-- int getQtBprMesProvisorio() 

    /**
     * Returns the value of field 'qtComprovante'.
     * 
     * @return int
     * @return the value of field 'qtComprovante'.
     */
    public int getQtComprovante()
    {
        return this._qtComprovante;
    } //-- int getQtComprovante() 

    /**
     * Returns the value of field 'qtComprovanteSalarioDiversos'.
     * 
     * @return int
     * @return the value of field 'qtComprovanteSalarioDiversos'.
     */
    public int getQtComprovanteSalarioDiversos()
    {
        return this._qtComprovanteSalarioDiversos;
    } //-- int getQtComprovanteSalarioDiversos() 

    /**
     * Returns the value of field 'qtCredito'.
     * 
     * @return int
     * @return the value of field 'qtCredito'.
     */
    public int getQtCredito()
    {
        return this._qtCredito;
    } //-- int getQtCredito() 

    /**
     * Returns the value of field 'qtForTitulo'.
     * 
     * @return int
     * @return the value of field 'qtForTitulo'.
     */
    public int getQtForTitulo()
    {
        return this._qtForTitulo;
    } //-- int getQtForTitulo() 

    /**
     * Returns the value of field 'qtModalidadeBeneficio'.
     * 
     * @return int
     * @return the value of field 'qtModalidadeBeneficio'.
     */
    public int getQtModalidadeBeneficio()
    {
        return this._qtModalidadeBeneficio;
    } //-- int getQtModalidadeBeneficio() 

    /**
     * Returns the value of field 'qtModalidadeFornecedor'.
     * 
     * @return int
     * @return the value of field 'qtModalidadeFornecedor'.
     */
    public int getQtModalidadeFornecedor()
    {
        return this._qtModalidadeFornecedor;
    } //-- int getQtModalidadeFornecedor() 

    /**
     * Returns the value of field 'qtModalidadeRecadastro'.
     * 
     * @return int
     * @return the value of field 'qtModalidadeRecadastro'.
     */
    public int getQtModalidadeRecadastro()
    {
        return this._qtModalidadeRecadastro;
    } //-- int getQtModalidadeRecadastro() 

    /**
     * Returns the value of field 'qtModalidadeSalario'.
     * 
     * @return int
     * @return the value of field 'qtModalidadeSalario'.
     */
    public int getQtModalidadeSalario()
    {
        return this._qtModalidadeSalario;
    } //-- int getQtModalidadeSalario() 

    /**
     * Returns the value of field 'qtModalidadeTributos'.
     * 
     * @return int
     * @return the value of field 'qtModalidadeTributos'.
     */
    public int getQtModalidadeTributos()
    {
        return this._qtModalidadeTributos;
    } //-- int getQtModalidadeTributos() 

    /**
     * Returns the value of field 'qtModularidade'.
     * 
     * @return int
     * @return the value of field 'qtModularidade'.
     */
    public int getQtModularidade()
    {
        return this._qtModularidade;
    } //-- int getQtModularidade() 

    /**
     * Returns the value of field 'qtOperacao'.
     * 
     * @return int
     * @return the value of field 'qtOperacao'.
     */
    public int getQtOperacao()
    {
        return this._qtOperacao;
    } //-- int getQtOperacao() 

    /**
     * Returns the value of field 'qtRecEtapaRecadastramento'.
     * 
     * @return int
     * @return the value of field 'qtRecEtapaRecadastramento'.
     */
    public int getQtRecEtapaRecadastramento()
    {
        return this._qtRecEtapaRecadastramento;
    } //-- int getQtRecEtapaRecadastramento() 

    /**
     * Returns the value of field 'qtRecFaseEtapaRecadastramento'.
     * 
     * @return int
     * @return the value of field 'qtRecFaseEtapaRecadastramento'.
     */
    public int getQtRecFaseEtapaRecadastramento()
    {
        return this._qtRecFaseEtapaRecadastramento;
    } //-- int getQtRecFaseEtapaRecadastramento() 

    /**
     * Returns the value of field 'qtTriTributo'.
     * 
     * @return int
     * @return the value of field 'qtTriTributo'.
     */
    public int getQtTriTributo()
    {
        return this._qtTriTributo;
    } //-- int getQtTriTributo() 

    /**
     * Method hasCdBeneficioQuantidadeDiasExpiracaoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBeneficioQuantidadeDiasExpiracaoCredito()
    {
        return this._has_cdBeneficioQuantidadeDiasExpiracaoCredito;
    } //-- boolean hasCdBeneficioQuantidadeDiasExpiracaoCredito() 

    /**
     * Method hasCdCadastroQuantidadeDiasInativos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCadastroQuantidadeDiasInativos()
    {
        return this._has_cdCadastroQuantidadeDiasInativos;
    } //-- boolean hasCdCadastroQuantidadeDiasInativos() 

    /**
     * Method hasCdSalarioQuantidadeLimiteSolicitacaoCartao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSalarioQuantidadeLimiteSolicitacaoCartao()
    {
        return this._has_cdSalarioQuantidadeLimiteSolicitacaoCartao;
    } //-- boolean hasCdSalarioQuantidadeLimiteSolicitacaoCartao() 

    /**
     * Method hasCdServicoQuantidadeServicoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoQuantidadeServicoPagamento()
    {
        return this._has_cdServicoQuantidadeServicoPagamento;
    } //-- boolean hasCdServicoQuantidadeServicoPagamento() 

    /**
     * Method hasQtAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAviso()
    {
        return this._has_qtAviso;
    } //-- boolean hasQtAviso() 

    /**
     * Method hasQtAvisoFormulario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAvisoFormulario()
    {
        return this._has_qtAvisoFormulario;
    } //-- boolean hasQtAvisoFormulario() 

    /**
     * Method hasQtBprDiaAnteriorAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtBprDiaAnteriorAviso()
    {
        return this._has_qtBprDiaAnteriorAviso;
    } //-- boolean hasQtBprDiaAnteriorAviso() 

    /**
     * Method hasQtBprDiaAnteriorVencimento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtBprDiaAnteriorVencimento()
    {
        return this._has_qtBprDiaAnteriorVencimento;
    } //-- boolean hasQtBprDiaAnteriorVencimento() 

    /**
     * Method hasQtBprMesProvisorio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtBprMesProvisorio()
    {
        return this._has_qtBprMesProvisorio;
    } //-- boolean hasQtBprMesProvisorio() 

    /**
     * Method hasQtComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtComprovante()
    {
        return this._has_qtComprovante;
    } //-- boolean hasQtComprovante() 

    /**
     * Method hasQtComprovanteSalarioDiversos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtComprovanteSalarioDiversos()
    {
        return this._has_qtComprovanteSalarioDiversos;
    } //-- boolean hasQtComprovanteSalarioDiversos() 

    /**
     * Method hasQtCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtCredito()
    {
        return this._has_qtCredito;
    } //-- boolean hasQtCredito() 

    /**
     * Method hasQtForTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtForTitulo()
    {
        return this._has_qtForTitulo;
    } //-- boolean hasQtForTitulo() 

    /**
     * Method hasQtModalidadeBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModalidadeBeneficio()
    {
        return this._has_qtModalidadeBeneficio;
    } //-- boolean hasQtModalidadeBeneficio() 

    /**
     * Method hasQtModalidadeFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModalidadeFornecedor()
    {
        return this._has_qtModalidadeFornecedor;
    } //-- boolean hasQtModalidadeFornecedor() 

    /**
     * Method hasQtModalidadeRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModalidadeRecadastro()
    {
        return this._has_qtModalidadeRecadastro;
    } //-- boolean hasQtModalidadeRecadastro() 

    /**
     * Method hasQtModalidadeSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModalidadeSalario()
    {
        return this._has_qtModalidadeSalario;
    } //-- boolean hasQtModalidadeSalario() 

    /**
     * Method hasQtModalidadeTributos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModalidadeTributos()
    {
        return this._has_qtModalidadeTributos;
    } //-- boolean hasQtModalidadeTributos() 

    /**
     * Method hasQtModularidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModularidade()
    {
        return this._has_qtModularidade;
    } //-- boolean hasQtModularidade() 

    /**
     * Method hasQtOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOperacao()
    {
        return this._has_qtOperacao;
    } //-- boolean hasQtOperacao() 

    /**
     * Method hasQtRecEtapaRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtRecEtapaRecadastramento()
    {
        return this._has_qtRecEtapaRecadastramento;
    } //-- boolean hasQtRecEtapaRecadastramento() 

    /**
     * Method hasQtRecFaseEtapaRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtRecFaseEtapaRecadastramento()
    {
        return this._has_qtRecFaseEtapaRecadastramento;
    } //-- boolean hasQtRecFaseEtapaRecadastramento() 

    /**
     * Method hasQtTriTributo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtTriTributo()
    {
        return this._has_qtTriTributo;
    } //-- boolean hasQtTriTributo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias1
     * 
     */
    public void removeAllOcorrencias1()
    {
        _ocorrencias1List.removeAllElements();
    } //-- void removeAllOcorrencias1() 

    /**
     * Method removeAllOcorrencias10
     * 
     */
    public void removeAllOcorrencias10()
    {
        _ocorrencias10List.removeAllElements();
    } //-- void removeAllOcorrencias10() 

    /**
     * Method removeAllOcorrencias11
     * 
     */
    public void removeAllOcorrencias11()
    {
        _ocorrencias11List.removeAllElements();
    } //-- void removeAllOcorrencias11() 

    /**
     * Method removeAllOcorrencias12
     * 
     */
    public void removeAllOcorrencias12()
    {
        _ocorrencias12List.removeAllElements();
    } //-- void removeAllOcorrencias12() 

    /**
     * Method removeAllOcorrencias13
     * 
     */
    public void removeAllOcorrencias13()
    {
        _ocorrencias13List.removeAllElements();
    } //-- void removeAllOcorrencias13() 

    /**
     * Method removeAllOcorrencias14
     * 
     */
    public void removeAllOcorrencias14()
    {
        _ocorrencias14List.removeAllElements();
    } //-- void removeAllOcorrencias14() 

    /**
     * Method removeAllOcorrencias15
     * 
     */
    public void removeAllOcorrencias15()
    {
        _ocorrencias15List.removeAllElements();
    } //-- void removeAllOcorrencias15() 

    /**
     * Method removeAllOcorrencias2
     * 
     */
    public void removeAllOcorrencias2()
    {
        _ocorrencias2List.removeAllElements();
    } //-- void removeAllOcorrencias2() 

    /**
     * Method removeAllOcorrencias3
     * 
     */
    public void removeAllOcorrencias3()
    {
        _ocorrencias3List.removeAllElements();
    } //-- void removeAllOcorrencias3() 

    /**
     * Method removeAllOcorrencias4
     * 
     */
    public void removeAllOcorrencias4()
    {
        _ocorrencias4List.removeAllElements();
    } //-- void removeAllOcorrencias4() 

    /**
     * Method removeAllOcorrencias5
     * 
     */
    public void removeAllOcorrencias5()
    {
        _ocorrencias5List.removeAllElements();
    } //-- void removeAllOcorrencias5() 

    /**
     * Method removeAllOcorrencias6
     * 
     */
    public void removeAllOcorrencias6()
    {
        _ocorrencias6List.removeAllElements();
    } //-- void removeAllOcorrencias6() 

    /**
     * Method removeAllOcorrencias7
     * 
     */
    public void removeAllOcorrencias7()
    {
        _ocorrencias7List.removeAllElements();
    } //-- void removeAllOcorrencias7() 

    /**
     * Method removeAllOcorrencias8
     * 
     */
    public void removeAllOcorrencias8()
    {
        _ocorrencias8List.removeAllElements();
    } //-- void removeAllOcorrencias8() 

    /**
     * Method removeAllOcorrencias9
     * 
     */
    public void removeAllOcorrencias9()
    {
        _ocorrencias9List.removeAllElements();
    } //-- void removeAllOcorrencias9() 

    /**
     * Method removeOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1 removeOcorrencias1(int index)
    {
        java.lang.Object obj = _ocorrencias1List.elementAt(index);
        _ocorrencias1List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1 removeOcorrencias1(int) 

    /**
     * Method removeOcorrencias10
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias10
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 removeOcorrencias10(int index)
    {
        java.lang.Object obj = _ocorrencias10List.elementAt(index);
        _ocorrencias10List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 removeOcorrencias10(int) 

    /**
     * Method removeOcorrencias11
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias11
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 removeOcorrencias11(int index)
    {
        java.lang.Object obj = _ocorrencias11List.elementAt(index);
        _ocorrencias11List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 removeOcorrencias11(int) 

    /**
     * Method removeOcorrencias12
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias12
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 removeOcorrencias12(int index)
    {
        java.lang.Object obj = _ocorrencias12List.elementAt(index);
        _ocorrencias12List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 removeOcorrencias12(int) 

    /**
     * Method removeOcorrencias13
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias13
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 removeOcorrencias13(int index)
    {
        java.lang.Object obj = _ocorrencias13List.elementAt(index);
        _ocorrencias13List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 removeOcorrencias13(int) 

    /**
     * Method removeOcorrencias14
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias14
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 removeOcorrencias14(int index)
    {
        java.lang.Object obj = _ocorrencias14List.elementAt(index);
        _ocorrencias14List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 removeOcorrencias14(int) 

    /**
     * Method removeOcorrencias15
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias15
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 removeOcorrencias15(int index)
    {
        java.lang.Object obj = _ocorrencias15List.elementAt(index);
        _ocorrencias15List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 removeOcorrencias15(int) 

    /**
     * Method removeOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2 removeOcorrencias2(int index)
    {
        java.lang.Object obj = _ocorrencias2List.elementAt(index);
        _ocorrencias2List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2 removeOcorrencias2(int) 

    /**
     * Method removeOcorrencias3
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3 removeOcorrencias3(int index)
    {
        java.lang.Object obj = _ocorrencias3List.elementAt(index);
        _ocorrencias3List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3 removeOcorrencias3(int) 

    /**
     * Method removeOcorrencias4
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4 removeOcorrencias4(int index)
    {
        java.lang.Object obj = _ocorrencias4List.elementAt(index);
        _ocorrencias4List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4 removeOcorrencias4(int) 

    /**
     * Method removeOcorrencias5
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5 removeOcorrencias5(int index)
    {
        java.lang.Object obj = _ocorrencias5List.elementAt(index);
        _ocorrencias5List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5 removeOcorrencias5(int) 

    /**
     * Method removeOcorrencias6
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 removeOcorrencias6(int index)
    {
        java.lang.Object obj = _ocorrencias6List.elementAt(index);
        _ocorrencias6List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 removeOcorrencias6(int) 

    /**
     * Method removeOcorrencias7
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias7
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 removeOcorrencias7(int index)
    {
        java.lang.Object obj = _ocorrencias7List.elementAt(index);
        _ocorrencias7List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 removeOcorrencias7(int) 

    /**
     * Method removeOcorrencias8
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias8
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 removeOcorrencias8(int index)
    {
        java.lang.Object obj = _ocorrencias8List.elementAt(index);
        _ocorrencias8List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 removeOcorrencias8(int) 

    /**
     * Method removeOcorrencias9
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias9
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 removeOcorrencias9(int index)
    {
        java.lang.Object obj = _ocorrencias9List.elementAt(index);
        _ocorrencias9List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 removeOcorrencias9(int) 

    /**
     * Sets the value of field
     * 'cdBeneficioQuantidadeDiasExpiracaoCredito'.
     * 
     * @param cdBeneficioQuantidadeDiasExpiracaoCredito the value
     * of field 'cdBeneficioQuantidadeDiasExpiracaoCredito'.
     */
    public void setCdBeneficioQuantidadeDiasExpiracaoCredito(int cdBeneficioQuantidadeDiasExpiracaoCredito)
    {
        this._cdBeneficioQuantidadeDiasExpiracaoCredito = cdBeneficioQuantidadeDiasExpiracaoCredito;
        this._has_cdBeneficioQuantidadeDiasExpiracaoCredito = true;
    } //-- void setCdBeneficioQuantidadeDiasExpiracaoCredito(int) 

    /**
     * Sets the value of field 'cdBprIndicadorEmissaoAviso'.
     * 
     * @param cdBprIndicadorEmissaoAviso the value of field
     * 'cdBprIndicadorEmissaoAviso'.
     */
    public void setCdBprIndicadorEmissaoAviso(java.lang.String cdBprIndicadorEmissaoAviso)
    {
        this._cdBprIndicadorEmissaoAviso = cdBprIndicadorEmissaoAviso;
    } //-- void setCdBprIndicadorEmissaoAviso(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdBprUtilizadoMensagemPersonalizada'.
     * 
     * @param cdBprUtilizadoMensagemPersonalizada the value of
     * field 'cdBprUtilizadoMensagemPersonalizada'.
     */
    public void setCdBprUtilizadoMensagemPersonalizada(java.lang.String cdBprUtilizadoMensagemPersonalizada)
    {
        this._cdBprUtilizadoMensagemPersonalizada = cdBprUtilizadoMensagemPersonalizada;
    } //-- void setCdBprUtilizadoMensagemPersonalizada(java.lang.String) 

    /**
     * Sets the value of field 'cdCadastroQuantidadeDiasInativos'.
     * 
     * @param cdCadastroQuantidadeDiasInativos the value of field
     * 'cdCadastroQuantidadeDiasInativos'.
     */
    public void setCdCadastroQuantidadeDiasInativos(int cdCadastroQuantidadeDiasInativos)
    {
        this._cdCadastroQuantidadeDiasInativos = cdCadastroQuantidadeDiasInativos;
        this._has_cdCadastroQuantidadeDiasInativos = true;
    } //-- void setCdCadastroQuantidadeDiasInativos(int) 

    /**
     * Sets the value of field 'cdIndcadorCadastroFavorecido'.
     * 
     * @param cdIndcadorCadastroFavorecido the value of field
     * 'cdIndcadorCadastroFavorecido'.
     */
    public void setCdIndcadorCadastroFavorecido(java.lang.String cdIndcadorCadastroFavorecido)
    {
        this._cdIndcadorCadastroFavorecido = cdIndcadorCadastroFavorecido;
    } //-- void setCdIndcadorCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAviso'.
     * 
     * @param cdIndicadorAviso the value of field 'cdIndicadorAviso'
     */
    public void setCdIndicadorAviso(java.lang.String cdIndicadorAviso)
    {
        this._cdIndicadorAviso = cdIndicadorAviso;
    } //-- void setCdIndicadorAviso(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAvisoFavorecido'.
     * 
     * @param cdIndicadorAvisoFavorecido the value of field
     * 'cdIndicadorAvisoFavorecido'.
     */
    public void setCdIndicadorAvisoFavorecido(java.lang.String cdIndicadorAvisoFavorecido)
    {
        this._cdIndicadorAvisoFavorecido = cdIndicadorAvisoFavorecido;
    } //-- void setCdIndicadorAvisoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAvisoPagador'.
     * 
     * @param cdIndicadorAvisoPagador the value of field
     * 'cdIndicadorAvisoPagador'.
     */
    public void setCdIndicadorAvisoPagador(java.lang.String cdIndicadorAvisoPagador)
    {
        this._cdIndicadorAvisoPagador = cdIndicadorAvisoPagador;
    } //-- void setCdIndicadorAvisoPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorComplemento'.
     * 
     * @param cdIndicadorComplemento the value of field
     * 'cdIndicadorComplemento'.
     */
    public void setCdIndicadorComplemento(java.lang.String cdIndicadorComplemento)
    {
        this._cdIndicadorComplemento = cdIndicadorComplemento;
    } //-- void setCdIndicadorComplemento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorComplementoDivergente'.
     * 
     * @param cdIndicadorComplementoDivergente the value of field
     * 'cdIndicadorComplementoDivergente'.
     */
    public void setCdIndicadorComplementoDivergente(java.lang.String cdIndicadorComplementoDivergente)
    {
        this._cdIndicadorComplementoDivergente = cdIndicadorComplementoDivergente;
    } //-- void setCdIndicadorComplementoDivergente(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorComplementoFavorecido'.
     * 
     * @param cdIndicadorComplementoFavorecido the value of field
     * 'cdIndicadorComplementoFavorecido'.
     */
    public void setCdIndicadorComplementoFavorecido(java.lang.String cdIndicadorComplementoFavorecido)
    {
        this._cdIndicadorComplementoFavorecido = cdIndicadorComplementoFavorecido;
    } //-- void setCdIndicadorComplementoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorComplementoPagador'.
     * 
     * @param cdIndicadorComplementoPagador the value of field
     * 'cdIndicadorComplementoPagador'.
     */
    public void setCdIndicadorComplementoPagador(java.lang.String cdIndicadorComplementoPagador)
    {
        this._cdIndicadorComplementoPagador = cdIndicadorComplementoPagador;
    } //-- void setCdIndicadorComplementoPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorComplementoSalarial'.
     * 
     * @param cdIndicadorComplementoSalarial the value of field
     * 'cdIndicadorComplementoSalarial'.
     */
    public void setCdIndicadorComplementoSalarial(java.lang.String cdIndicadorComplementoSalarial)
    {
        this._cdIndicadorComplementoSalarial = cdIndicadorComplementoSalarial;
    } //-- void setCdIndicadorComplementoSalarial(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdSalarioQuantidadeLimiteSolicitacaoCartao'.
     * 
     * @param cdSalarioQuantidadeLimiteSolicitacaoCartao the value
     * of field 'cdSalarioQuantidadeLimiteSolicitacaoCartao'.
     */
    public void setCdSalarioQuantidadeLimiteSolicitacaoCartao(int cdSalarioQuantidadeLimiteSolicitacaoCartao)
    {
        this._cdSalarioQuantidadeLimiteSolicitacaoCartao = cdSalarioQuantidadeLimiteSolicitacaoCartao;
        this._has_cdSalarioQuantidadeLimiteSolicitacaoCartao = true;
    } //-- void setCdSalarioQuantidadeLimiteSolicitacaoCartao(int) 

    /**
     * Sets the value of field 'cdServicoBeneficio'.
     * 
     * @param cdServicoBeneficio the value of field
     * 'cdServicoBeneficio'.
     */
    public void setCdServicoBeneficio(java.lang.String cdServicoBeneficio)
    {
        this._cdServicoBeneficio = cdServicoBeneficio;
    } //-- void setCdServicoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'cdServicoFornecedor'.
     * 
     * @param cdServicoFornecedor the value of field
     * 'cdServicoFornecedor'.
     */
    public void setCdServicoFornecedor(java.lang.String cdServicoFornecedor)
    {
        this._cdServicoFornecedor = cdServicoFornecedor;
    } //-- void setCdServicoFornecedor(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdServicoQuantidadeServicoPagamento'.
     * 
     * @param cdServicoQuantidadeServicoPagamento the value of
     * field 'cdServicoQuantidadeServicoPagamento'.
     */
    public void setCdServicoQuantidadeServicoPagamento(int cdServicoQuantidadeServicoPagamento)
    {
        this._cdServicoQuantidadeServicoPagamento = cdServicoQuantidadeServicoPagamento;
        this._has_cdServicoQuantidadeServicoPagamento = true;
    } //-- void setCdServicoQuantidadeServicoPagamento(int) 

    /**
     * Sets the value of field 'cdServicoSalarial'.
     * 
     * @param cdServicoSalarial the value of field
     * 'cdServicoSalarial'.
     */
    public void setCdServicoSalarial(java.lang.String cdServicoSalarial)
    {
        this._cdServicoSalarial = cdServicoSalarial;
    } //-- void setCdServicoSalarial(java.lang.String) 

    /**
     * Sets the value of field 'cdServicoTributos'.
     * 
     * @param cdServicoTributos the value of field
     * 'cdServicoTributos'.
     */
    public void setCdServicoTributos(java.lang.String cdServicoTributos)
    {
        this._cdServicoTributos = cdServicoTributos;
    } //-- void setCdServicoTributos(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsBeneficioInformadoAgenciaConta'.
     * 
     * @param dsBeneficioInformadoAgenciaConta the value of field
     * 'dsBeneficioInformadoAgenciaConta'.
     */
    public void setDsBeneficioInformadoAgenciaConta(java.lang.String dsBeneficioInformadoAgenciaConta)
    {
        this._dsBeneficioInformadoAgenciaConta = dsBeneficioInformadoAgenciaConta;
    } //-- void setDsBeneficioInformadoAgenciaConta(java.lang.String) 

    /**
     * Sets the value of field 'dsBeneficioPossuiExpiracaoCredito'.
     * 
     * @param dsBeneficioPossuiExpiracaoCredito the value of field
     * 'dsBeneficioPossuiExpiracaoCredito'.
     */
    public void setDsBeneficioPossuiExpiracaoCredito(java.lang.String dsBeneficioPossuiExpiracaoCredito)
    {
        this._dsBeneficioPossuiExpiracaoCredito = dsBeneficioPossuiExpiracaoCredito;
    } //-- void setDsBeneficioPossuiExpiracaoCredito(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsBeneficioTipoIdentificacaoBeneficio'.
     * 
     * @param dsBeneficioTipoIdentificacaoBeneficio the value of
     * field 'dsBeneficioTipoIdentificacaoBeneficio'.
     */
    public void setDsBeneficioTipoIdentificacaoBeneficio(java.lang.String dsBeneficioTipoIdentificacaoBeneficio)
    {
        this._dsBeneficioTipoIdentificacaoBeneficio = dsBeneficioTipoIdentificacaoBeneficio;
    } //-- void setDsBeneficioTipoIdentificacaoBeneficio(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsBeneficioUtilizacaoCadastroOrganizacao'.
     * 
     * @param dsBeneficioUtilizacaoCadastroOrganizacao the value of
     * field 'dsBeneficioUtilizacaoCadastroOrganizacao'.
     */
    public void setDsBeneficioUtilizacaoCadastroOrganizacao(java.lang.String dsBeneficioUtilizacaoCadastroOrganizacao)
    {
        this._dsBeneficioUtilizacaoCadastroOrganizacao = dsBeneficioUtilizacaoCadastroOrganizacao;
    } //-- void setDsBeneficioUtilizacaoCadastroOrganizacao(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsBeneficioUtilizacaoCadastroProcuradores'.
     * 
     * @param dsBeneficioUtilizacaoCadastroProcuradores the value
     * of field 'dsBeneficioUtilizacaoCadastroProcuradores'.
     */
    public void setDsBeneficioUtilizacaoCadastroProcuradores(java.lang.String dsBeneficioUtilizacaoCadastroProcuradores)
    {
        this._dsBeneficioUtilizacaoCadastroProcuradores = dsBeneficioUtilizacaoCadastroProcuradores;
    } //-- void setDsBeneficioUtilizacaoCadastroProcuradores(java.lang.String) 

    /**
     * Sets the value of field 'dsBprCadastroEnderecoUtilizado'.
     * 
     * @param dsBprCadastroEnderecoUtilizado the value of field
     * 'dsBprCadastroEnderecoUtilizado'.
     */
    public void setDsBprCadastroEnderecoUtilizado(java.lang.String dsBprCadastroEnderecoUtilizado)
    {
        this._dsBprCadastroEnderecoUtilizado = dsBprCadastroEnderecoUtilizado;
    } //-- void setDsBprCadastroEnderecoUtilizado(java.lang.String) 

    /**
     * Sets the value of field 'dsBprDestino'.
     * 
     * @param dsBprDestino the value of field 'dsBprDestino'.
     */
    public void setDsBprDestino(java.lang.String dsBprDestino)
    {
        this._dsBprDestino = dsBprDestino;
    } //-- void setDsBprDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsBprTipoAcao'.
     * 
     * @param dsBprTipoAcao the value of field 'dsBprTipoAcao'.
     */
    public void setDsBprTipoAcao(java.lang.String dsBprTipoAcao)
    {
        this._dsBprTipoAcao = dsBprTipoAcao;
    } //-- void setDsBprTipoAcao(java.lang.String) 

    /**
     * Sets the value of field 'dsCadastroFormaManutencao'.
     * 
     * @param dsCadastroFormaManutencao the value of field
     * 'dsCadastroFormaManutencao'.
     */
    public void setDsCadastroFormaManutencao(java.lang.String dsCadastroFormaManutencao)
    {
        this._dsCadastroFormaManutencao = dsCadastroFormaManutencao;
    } //-- void setDsCadastroFormaManutencao(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsCadastroTipoConsistenciaInscricao'.
     * 
     * @param dsCadastroTipoConsistenciaInscricao the value of
     * field 'dsCadastroTipoConsistenciaInscricao'.
     */
    public void setDsCadastroTipoConsistenciaInscricao(java.lang.String dsCadastroTipoConsistenciaInscricao)
    {
        this._dsCadastroTipoConsistenciaInscricao = dsCadastroTipoConsistenciaInscricao;
    } //-- void setDsCadastroTipoConsistenciaInscricao(java.lang.String) 

    /**
     * Sets the value of field 'dsFraAdesaoSacado'.
     * 
     * @param dsFraAdesaoSacado the value of field
     * 'dsFraAdesaoSacado'.
     */
    public void setDsFraAdesaoSacado(java.lang.String dsFraAdesaoSacado)
    {
        this._dsFraAdesaoSacado = dsFraAdesaoSacado;
    } //-- void setDsFraAdesaoSacado(java.lang.String) 

    /**
     * Sets the value of field 'dsFraAgendaCliente'.
     * 
     * @param dsFraAgendaCliente the value of field
     * 'dsFraAgendaCliente'.
     */
    public void setDsFraAgendaCliente(java.lang.String dsFraAgendaCliente)
    {
        this._dsFraAgendaCliente = dsFraAgendaCliente;
    } //-- void setDsFraAgendaCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsFraAgendaFilial'.
     * 
     * @param dsFraAgendaFilial the value of field
     * 'dsFraAgendaFilial'.
     */
    public void setDsFraAgendaFilial(java.lang.String dsFraAgendaFilial)
    {
        this._dsFraAgendaFilial = dsFraAgendaFilial;
    } //-- void setDsFraAgendaFilial(java.lang.String) 

    /**
     * Sets the value of field 'dsFraBloqueioEmissao'.
     * 
     * @param dsFraBloqueioEmissao the value of field
     * 'dsFraBloqueioEmissao'.
     */
    public void setDsFraBloqueioEmissao(java.lang.String dsFraBloqueioEmissao)
    {
        this._dsFraBloqueioEmissao = dsFraBloqueioEmissao;
    } //-- void setDsFraBloqueioEmissao(java.lang.String) 

    /**
     * Sets the value of field 'dsFraCaptacaoTitulo'.
     * 
     * @param dsFraCaptacaoTitulo the value of field
     * 'dsFraCaptacaoTitulo'.
     */
    public void setDsFraCaptacaoTitulo(java.lang.String dsFraCaptacaoTitulo)
    {
        this._dsFraCaptacaoTitulo = dsFraCaptacaoTitulo;
    } //-- void setDsFraCaptacaoTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dsFraRastreabilidadeNota'.
     * 
     * @param dsFraRastreabilidadeNota the value of field
     * 'dsFraRastreabilidadeNota'.
     */
    public void setDsFraRastreabilidadeNota(java.lang.String dsFraRastreabilidadeNota)
    {
        this._dsFraRastreabilidadeNota = dsFraRastreabilidadeNota;
    } //-- void setDsFraRastreabilidadeNota(java.lang.String) 

    /**
     * Sets the value of field 'dsFraRastreabilidadeTerceiro'.
     * 
     * @param dsFraRastreabilidadeTerceiro the value of field
     * 'dsFraRastreabilidadeTerceiro'.
     */
    public void setDsFraRastreabilidadeTerceiro(java.lang.String dsFraRastreabilidadeTerceiro)
    {
        this._dsFraRastreabilidadeTerceiro = dsFraRastreabilidadeTerceiro;
    } //-- void setDsFraRastreabilidadeTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'dsFraTipoRastreabilidade'.
     * 
     * @param dsFraTipoRastreabilidade the value of field
     * 'dsFraTipoRastreabilidade'.
     */
    public void setDsFraTipoRastreabilidade(java.lang.String dsFraTipoRastreabilidade)
    {
        this._dsFraTipoRastreabilidade = dsFraTipoRastreabilidade;
    } //-- void setDsFraTipoRastreabilidade(java.lang.String) 

    /**
     * Sets the value of field 'dsRecCondicaoEnquadramento'.
     * 
     * @param dsRecCondicaoEnquadramento the value of field
     * 'dsRecCondicaoEnquadramento'.
     */
    public void setDsRecCondicaoEnquadramento(java.lang.String dsRecCondicaoEnquadramento)
    {
        this._dsRecCondicaoEnquadramento = dsRecCondicaoEnquadramento;
    } //-- void setDsRecCondicaoEnquadramento(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsRecCriterioPrincipalEnquadramento'.
     * 
     * @param dsRecCriterioPrincipalEnquadramento the value of
     * field 'dsRecCriterioPrincipalEnquadramento'.
     */
    public void setDsRecCriterioPrincipalEnquadramento(java.lang.String dsRecCriterioPrincipalEnquadramento)
    {
        this._dsRecCriterioPrincipalEnquadramento = dsRecCriterioPrincipalEnquadramento;
    } //-- void setDsRecCriterioPrincipalEnquadramento(java.lang.String) 

    /**
     * Sets the value of field 'dsRecGeradorRetornoInternet'.
     * 
     * @param dsRecGeradorRetornoInternet the value of field
     * 'dsRecGeradorRetornoInternet'.
     */
    public void setDsRecGeradorRetornoInternet(java.lang.String dsRecGeradorRetornoInternet)
    {
        this._dsRecGeradorRetornoInternet = dsRecGeradorRetornoInternet;
    } //-- void setDsRecGeradorRetornoInternet(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsRecTipoConsistenciaIdentificacao'.
     * 
     * @param dsRecTipoConsistenciaIdentificacao the value of field
     * 'dsRecTipoConsistenciaIdentificacao'.
     */
    public void setDsRecTipoConsistenciaIdentificacao(java.lang.String dsRecTipoConsistenciaIdentificacao)
    {
        this._dsRecTipoConsistenciaIdentificacao = dsRecTipoConsistenciaIdentificacao;
    } //-- void setDsRecTipoConsistenciaIdentificacao(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsRecTipoCriterioCompostoEnquadramento'.
     * 
     * @param dsRecTipoCriterioCompostoEnquadramento the value of
     * field 'dsRecTipoCriterioCompostoEnquadramento'.
     */
    public void setDsRecTipoCriterioCompostoEnquadramento(java.lang.String dsRecTipoCriterioCompostoEnquadramento)
    {
        this._dsRecTipoCriterioCompostoEnquadramento = dsRecTipoCriterioCompostoEnquadramento;
    } //-- void setDsRecTipoCriterioCompostoEnquadramento(java.lang.String) 

    /**
     * Sets the value of field 'dsRecTipoIdentificacaoBeneficio'.
     * 
     * @param dsRecTipoIdentificacaoBeneficio the value of field
     * 'dsRecTipoIdentificacaoBeneficio'.
     */
    public void setDsRecTipoIdentificacaoBeneficio(java.lang.String dsRecTipoIdentificacaoBeneficio)
    {
        this._dsRecTipoIdentificacaoBeneficio = dsRecTipoIdentificacaoBeneficio;
    } //-- void setDsRecTipoIdentificacaoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsSalarioAberturaContaExpressa'.
     * 
     * @param dsSalarioAberturaContaExpressa the value of field
     * 'dsSalarioAberturaContaExpressa'.
     */
    public void setDsSalarioAberturaContaExpressa(java.lang.String dsSalarioAberturaContaExpressa)
    {
        this._dsSalarioAberturaContaExpressa = dsSalarioAberturaContaExpressa;
    } //-- void setDsSalarioAberturaContaExpressa(java.lang.String) 

    /**
     * Sets the value of field 'dsSalarioEmissaoAntecipadoCartao'.
     * 
     * @param dsSalarioEmissaoAntecipadoCartao the value of field
     * 'dsSalarioEmissaoAntecipadoCartao'.
     */
    public void setDsSalarioEmissaoAntecipadoCartao(java.lang.String dsSalarioEmissaoAntecipadoCartao)
    {
        this._dsSalarioEmissaoAntecipadoCartao = dsSalarioEmissaoAntecipadoCartao;
    } //-- void setDsSalarioEmissaoAntecipadoCartao(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoRecadastro'.
     * 
     * @param dsServicoRecadastro the value of field
     * 'dsServicoRecadastro'.
     */
    public void setDsServicoRecadastro(java.lang.String dsServicoRecadastro)
    {
        this._dsServicoRecadastro = dsServicoRecadastro;
    } //-- void setDsServicoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dsTdvTipoConsultaProposta'.
     * 
     * @param dsTdvTipoConsultaProposta the value of field
     * 'dsTdvTipoConsultaProposta'.
     */
    public void setDsTdvTipoConsultaProposta(java.lang.String dsTdvTipoConsultaProposta)
    {
        this._dsTdvTipoConsultaProposta = dsTdvTipoConsultaProposta;
    } //-- void setDsTdvTipoConsultaProposta(java.lang.String) 

    /**
     * Sets the value of field 'dsTdvTipoTratamentoValor'.
     * 
     * @param dsTdvTipoTratamentoValor the value of field
     * 'dsTdvTipoTratamentoValor'.
     */
    public void setDsTdvTipoTratamentoValor(java.lang.String dsTdvTipoTratamentoValor)
    {
        this._dsTdvTipoTratamentoValor = dsTdvTipoTratamentoValor;
    } //-- void setDsTdvTipoTratamentoValor(java.lang.String) 

    /**
     * Sets the value of field 'dsTibutoAgendadoDebitoVeicular'.
     * 
     * @param dsTibutoAgendadoDebitoVeicular the value of field
     * 'dsTibutoAgendadoDebitoVeicular'.
     */
    public void setDsTibutoAgendadoDebitoVeicular(java.lang.String dsTibutoAgendadoDebitoVeicular)
    {
        this._dsTibutoAgendadoDebitoVeicular = dsTibutoAgendadoDebitoVeicular;
    } //-- void setDsTibutoAgendadoDebitoVeicular(java.lang.String) 

    /**
     * Sets the value of field 'dtFraInicioBloqueio'.
     * 
     * @param dtFraInicioBloqueio the value of field
     * 'dtFraInicioBloqueio'.
     */
    public void setDtFraInicioBloqueio(java.lang.String dtFraInicioBloqueio)
    {
        this._dtFraInicioBloqueio = dtFraInicioBloqueio;
    } //-- void setDtFraInicioBloqueio(java.lang.String) 

    /**
     * Sets the value of field 'dtFraInicioRastreabilidade'.
     * 
     * @param dtFraInicioRastreabilidade the value of field
     * 'dtFraInicioRastreabilidade'.
     */
    public void setDtFraInicioRastreabilidade(java.lang.String dtFraInicioRastreabilidade)
    {
        this._dtFraInicioRastreabilidade = dtFraInicioRastreabilidade;
    } //-- void setDtFraInicioRastreabilidade(java.lang.String) 

    /**
     * Sets the value of field 'dtRecFinalRecadastramento'.
     * 
     * @param dtRecFinalRecadastramento the value of field
     * 'dtRecFinalRecadastramento'.
     */
    public void setDtRecFinalRecadastramento(java.lang.String dtRecFinalRecadastramento)
    {
        this._dtRecFinalRecadastramento = dtRecFinalRecadastramento;
    } //-- void setDtRecFinalRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dtRecInicioRecadastramento'.
     * 
     * @param dtRecInicioRecadastramento the value of field
     * 'dtRecInicioRecadastramento'.
     */
    public void setDtRecInicioRecadastramento(java.lang.String dtRecInicioRecadastramento)
    {
        this._dtRecInicioRecadastramento = dtRecInicioRecadastramento;
    } //-- void setDtRecInicioRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void setOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias1: Index value '"+index+"' not in range [0.." + (_ocorrencias1List.size() - 1) + "]");
        }
        if (!(index < 10)) {
            throw new IndexOutOfBoundsException("setOcorrencias1 has a maximum of 10");
        }
        _ocorrencias1List.setElementAt(vOcorrencias1, index);
    } //-- void setOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param ocorrencias1Array
     */
    public void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1[] ocorrencias1Array)
    {
        //-- copy array
        _ocorrencias1List.removeAllElements();
        for (int i = 0; i < ocorrencias1Array.length; i++) {
            _ocorrencias1List.addElement(ocorrencias1Array[i]);
        }
    } //-- void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias1) 

    /**
     * Method setOcorrencias10
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias10
     */
    public void setOcorrencias10(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 vOcorrencias10)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias10List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias10: Index value '"+index+"' not in range [0.." + (_ocorrencias10List.size() - 1) + "]");
        }
        if (!(index < 2)) {
            throw new IndexOutOfBoundsException("setOcorrencias10 has a maximum of 2");
        }
        _ocorrencias10List.setElementAt(vOcorrencias10, index);
    } //-- void setOcorrencias10(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10) 

    /**
     * Method setOcorrencias10
     * 
     * 
     * 
     * @param ocorrencias10Array
     */
    public void setOcorrencias10(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10[] ocorrencias10Array)
    {
        //-- copy array
        _ocorrencias10List.removeAllElements();
        for (int i = 0; i < ocorrencias10Array.length; i++) {
            _ocorrencias10List.addElement(ocorrencias10Array[i]);
        }
    } //-- void setOcorrencias10(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10) 

    /**
     * Method setOcorrencias11
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias11
     */
    public void setOcorrencias11(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11 vOcorrencias11)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias11List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias11: Index value '"+index+"' not in range [0.." + (_ocorrencias11List.size() - 1) + "]");
        }
        if (!(index < 3)) {
            throw new IndexOutOfBoundsException("setOcorrencias11 has a maximum of 3");
        }
        _ocorrencias11List.setElementAt(vOcorrencias11, index);
    } //-- void setOcorrencias11(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11) 

    /**
     * Method setOcorrencias11
     * 
     * 
     * 
     * @param ocorrencias11Array
     */
    public void setOcorrencias11(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11[] ocorrencias11Array)
    {
        //-- copy array
        _ocorrencias11List.removeAllElements();
        for (int i = 0; i < ocorrencias11Array.length; i++) {
            _ocorrencias11List.addElement(ocorrencias11Array[i]);
        }
    } //-- void setOcorrencias11(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias11) 

    /**
     * Method setOcorrencias12
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias12
     */
    public void setOcorrencias12(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12 vOcorrencias12)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias12List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias12: Index value '"+index+"' not in range [0.." + (_ocorrencias12List.size() - 1) + "]");
        }
        if (!(index < 3)) {
            throw new IndexOutOfBoundsException("setOcorrencias12 has a maximum of 3");
        }
        _ocorrencias12List.setElementAt(vOcorrencias12, index);
    } //-- void setOcorrencias12(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12) 

    /**
     * Method setOcorrencias12
     * 
     * 
     * 
     * @param ocorrencias12Array
     */
    public void setOcorrencias12(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12[] ocorrencias12Array)
    {
        //-- copy array
        _ocorrencias12List.removeAllElements();
        for (int i = 0; i < ocorrencias12Array.length; i++) {
            _ocorrencias12List.addElement(ocorrencias12Array[i]);
        }
    } //-- void setOcorrencias12(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias12) 

    /**
     * Method setOcorrencias13
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias13
     */
    public void setOcorrencias13(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 vOcorrencias13)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias13List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias13: Index value '"+index+"' not in range [0.." + (_ocorrencias13List.size() - 1) + "]");
        }
        if (!(index < 9)) {
            throw new IndexOutOfBoundsException("setOcorrencias13 has a maximum of 9");
        }
        _ocorrencias13List.setElementAt(vOcorrencias13, index);
    } //-- void setOcorrencias13(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13) 

    /**
     * Method setOcorrencias13
     * 
     * 
     * 
     * @param ocorrencias13Array
     */
    public void setOcorrencias13(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13[] ocorrencias13Array)
    {
        //-- copy array
        _ocorrencias13List.removeAllElements();
        for (int i = 0; i < ocorrencias13Array.length; i++) {
            _ocorrencias13List.addElement(ocorrencias13Array[i]);
        }
    } //-- void setOcorrencias13(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13) 

    /**
     * Method setOcorrencias14
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias14
     */
    public void setOcorrencias14(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 vOcorrencias14)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias14List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias14: Index value '"+index+"' not in range [0.." + (_ocorrencias14List.size() - 1) + "]");
        }
        if (!(index < 5)) {
            throw new IndexOutOfBoundsException("setOcorrencias14 has a maximum of 5");
        }
        _ocorrencias14List.setElementAt(vOcorrencias14, index);
    } //-- void setOcorrencias14(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14) 

    /**
     * Method setOcorrencias14
     * 
     * 
     * 
     * @param ocorrencias14Array
     */
    public void setOcorrencias14(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14[] ocorrencias14Array)
    {
        //-- copy array
        _ocorrencias14List.removeAllElements();
        for (int i = 0; i < ocorrencias14Array.length; i++) {
            _ocorrencias14List.addElement(ocorrencias14Array[i]);
        }
    } //-- void setOcorrencias14(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14) 

    /**
     * Method setOcorrencias15
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias15
     */
    public void setOcorrencias15(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 vOcorrencias15)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias15List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias15: Index value '"+index+"' not in range [0.." + (_ocorrencias15List.size() - 1) + "]");
        }
        if (!(index < 2)) {
            throw new IndexOutOfBoundsException("setOcorrencias15 has a maximum of 2");
        }
        _ocorrencias15List.setElementAt(vOcorrencias15, index);
    } //-- void setOcorrencias15(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15) 

    /**
     * Method setOcorrencias15
     * 
     * 
     * 
     * @param ocorrencias15Array
     */
    public void setOcorrencias15(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15[] ocorrencias15Array)
    {
        //-- copy array
        _ocorrencias15List.removeAllElements();
        for (int i = 0; i < ocorrencias15Array.length; i++) {
            _ocorrencias15List.addElement(ocorrencias15Array[i]);
        }
    } //-- void setOcorrencias15(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void setOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias2: Index value '"+index+"' not in range [0.." + (_ocorrencias2List.size() - 1) + "]");
        }
        if (!(index < 10)) {
            throw new IndexOutOfBoundsException("setOcorrencias2 has a maximum of 10");
        }
        _ocorrencias2List.setElementAt(vOcorrencias2, index);
    } //-- void setOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param ocorrencias2Array
     */
    public void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2[] ocorrencias2Array)
    {
        //-- copy array
        _ocorrencias2List.removeAllElements();
        for (int i = 0; i < ocorrencias2Array.length; i++) {
            _ocorrencias2List.addElement(ocorrencias2Array[i]);
        }
    } //-- void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias2) 

    /**
     * Method setOcorrencias3
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias3
     */
    public void setOcorrencias3(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias3List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias3: Index value '"+index+"' not in range [0.." + (_ocorrencias3List.size() - 1) + "]");
        }
        if (!(index < 4)) {
            throw new IndexOutOfBoundsException("setOcorrencias3 has a maximum of 4");
        }
        _ocorrencias3List.setElementAt(vOcorrencias3, index);
    } //-- void setOcorrencias3(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3) 

    /**
     * Method setOcorrencias3
     * 
     * 
     * 
     * @param ocorrencias3Array
     */
    public void setOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3[] ocorrencias3Array)
    {
        //-- copy array
        _ocorrencias3List.removeAllElements();
        for (int i = 0; i < ocorrencias3Array.length; i++) {
            _ocorrencias3List.addElement(ocorrencias3Array[i]);
        }
    } //-- void setOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias3) 

    /**
     * Method setOcorrencias4
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias4
     */
    public void setOcorrencias4(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias4List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias4: Index value '"+index+"' not in range [0.." + (_ocorrencias4List.size() - 1) + "]");
        }
        if (!(index < 5)) {
            throw new IndexOutOfBoundsException("setOcorrencias4 has a maximum of 5");
        }
        _ocorrencias4List.setElementAt(vOcorrencias4, index);
    } //-- void setOcorrencias4(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4) 

    /**
     * Method setOcorrencias4
     * 
     * 
     * 
     * @param ocorrencias4Array
     */
    public void setOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4[] ocorrencias4Array)
    {
        //-- copy array
        _ocorrencias4List.removeAllElements();
        for (int i = 0; i < ocorrencias4Array.length; i++) {
            _ocorrencias4List.addElement(ocorrencias4Array[i]);
        }
    } //-- void setOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias4) 

    /**
     * Method setOcorrencias5
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias5
     */
    public void setOcorrencias5(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias5List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias5: Index value '"+index+"' not in range [0.." + (_ocorrencias5List.size() - 1) + "]");
        }
        if (!(index < 2)) {
            throw new IndexOutOfBoundsException("setOcorrencias5 has a maximum of 2");
        }
        _ocorrencias5List.setElementAt(vOcorrencias5, index);
    } //-- void setOcorrencias5(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5) 

    /**
     * Method setOcorrencias5
     * 
     * 
     * 
     * @param ocorrencias5Array
     */
    public void setOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5[] ocorrencias5Array)
    {
        //-- copy array
        _ocorrencias5List.removeAllElements();
        for (int i = 0; i < ocorrencias5Array.length; i++) {
            _ocorrencias5List.addElement(ocorrencias5Array[i]);
        }
    } //-- void setOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias5) 

    /**
     * Method setOcorrencias6
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias6
     */
    public void setOcorrencias6(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias6List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias6: Index value '"+index+"' not in range [0.." + (_ocorrencias6List.size() - 1) + "]");
        }
        if (!(index < 4)) {
            throw new IndexOutOfBoundsException("setOcorrencias6 has a maximum of 4");
        }
        _ocorrencias6List.setElementAt(vOcorrencias6, index);
    } //-- void setOcorrencias6(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6) 

    /**
     * Method setOcorrencias6
     * 
     * 
     * 
     * @param ocorrencias6Array
     */
    public void setOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6[] ocorrencias6Array)
    {
        //-- copy array
        _ocorrencias6List.removeAllElements();
        for (int i = 0; i < ocorrencias6Array.length; i++) {
            _ocorrencias6List.addElement(ocorrencias6Array[i]);
        }
    } //-- void setOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6) 

    /**
     * Method setOcorrencias7
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias7
     */
    public void setOcorrencias7(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7 vOcorrencias7)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias7List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias7: Index value '"+index+"' not in range [0.." + (_ocorrencias7List.size() - 1) + "]");
        }
        if (!(index < 2)) {
            throw new IndexOutOfBoundsException("setOcorrencias7 has a maximum of 2");
        }
        _ocorrencias7List.setElementAt(vOcorrencias7, index);
    } //-- void setOcorrencias7(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7) 

    /**
     * Method setOcorrencias7
     * 
     * 
     * 
     * @param ocorrencias7Array
     */
    public void setOcorrencias7(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7[] ocorrencias7Array)
    {
        //-- copy array
        _ocorrencias7List.removeAllElements();
        for (int i = 0; i < ocorrencias7Array.length; i++) {
            _ocorrencias7List.addElement(ocorrencias7Array[i]);
        }
    } //-- void setOcorrencias7(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias7) 

    /**
     * Method setOcorrencias8
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias8
     */
    public void setOcorrencias8(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 vOcorrencias8)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias8List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias8: Index value '"+index+"' not in range [0.." + (_ocorrencias8List.size() - 1) + "]");
        }
        if (!(index < 2)) {
            throw new IndexOutOfBoundsException("setOcorrencias8 has a maximum of 2");
        }
        _ocorrencias8List.setElementAt(vOcorrencias8, index);
    } //-- void setOcorrencias8(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8) 

    /**
     * Method setOcorrencias8
     * 
     * 
     * 
     * @param ocorrencias8Array
     */
    public void setOcorrencias8(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8[] ocorrencias8Array)
    {
        //-- copy array
        _ocorrencias8List.removeAllElements();
        for (int i = 0; i < ocorrencias8Array.length; i++) {
            _ocorrencias8List.addElement(ocorrencias8Array[i]);
        }
    } //-- void setOcorrencias8(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8) 

    /**
     * Method setOcorrencias9
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias9
     */
    public void setOcorrencias9(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9 vOcorrencias9)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias9List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias9: Index value '"+index+"' not in range [0.." + (_ocorrencias9List.size() - 1) + "]");
        }
        if (!(index < 2)) {
            throw new IndexOutOfBoundsException("setOcorrencias9 has a maximum of 2");
        }
        _ocorrencias9List.setElementAt(vOcorrencias9, index);
    } //-- void setOcorrencias9(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9) 

    /**
     * Method setOcorrencias9
     * 
     * 
     * 
     * @param ocorrencias9Array
     */
    public void setOcorrencias9(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9[] ocorrencias9Array)
    {
        //-- copy array
        _ocorrencias9List.removeAllElements();
        for (int i = 0; i < ocorrencias9Array.length; i++) {
            _ocorrencias9List.addElement(ocorrencias9Array[i]);
        }
    } //-- void setOcorrencias9(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias9) 

    /**
     * Sets the value of field 'qtAviso'.
     * 
     * @param qtAviso the value of field 'qtAviso'.
     */
    public void setQtAviso(int qtAviso)
    {
        this._qtAviso = qtAviso;
        this._has_qtAviso = true;
    } //-- void setQtAviso(int) 

    /**
     * Sets the value of field 'qtAvisoFormulario'.
     * 
     * @param qtAvisoFormulario the value of field
     * 'qtAvisoFormulario'.
     */
    public void setQtAvisoFormulario(int qtAvisoFormulario)
    {
        this._qtAvisoFormulario = qtAvisoFormulario;
        this._has_qtAvisoFormulario = true;
    } //-- void setQtAvisoFormulario(int) 

    /**
     * Sets the value of field 'qtBprDiaAnteriorAviso'.
     * 
     * @param qtBprDiaAnteriorAviso the value of field
     * 'qtBprDiaAnteriorAviso'.
     */
    public void setQtBprDiaAnteriorAviso(int qtBprDiaAnteriorAviso)
    {
        this._qtBprDiaAnteriorAviso = qtBprDiaAnteriorAviso;
        this._has_qtBprDiaAnteriorAviso = true;
    } //-- void setQtBprDiaAnteriorAviso(int) 

    /**
     * Sets the value of field 'qtBprDiaAnteriorVencimento'.
     * 
     * @param qtBprDiaAnteriorVencimento the value of field
     * 'qtBprDiaAnteriorVencimento'.
     */
    public void setQtBprDiaAnteriorVencimento(int qtBprDiaAnteriorVencimento)
    {
        this._qtBprDiaAnteriorVencimento = qtBprDiaAnteriorVencimento;
        this._has_qtBprDiaAnteriorVencimento = true;
    } //-- void setQtBprDiaAnteriorVencimento(int) 

    /**
     * Sets the value of field 'qtBprMesProvisorio'.
     * 
     * @param qtBprMesProvisorio the value of field
     * 'qtBprMesProvisorio'.
     */
    public void setQtBprMesProvisorio(int qtBprMesProvisorio)
    {
        this._qtBprMesProvisorio = qtBprMesProvisorio;
        this._has_qtBprMesProvisorio = true;
    } //-- void setQtBprMesProvisorio(int) 

    /**
     * Sets the value of field 'qtComprovante'.
     * 
     * @param qtComprovante the value of field 'qtComprovante'.
     */
    public void setQtComprovante(int qtComprovante)
    {
        this._qtComprovante = qtComprovante;
        this._has_qtComprovante = true;
    } //-- void setQtComprovante(int) 

    /**
     * Sets the value of field 'qtComprovanteSalarioDiversos'.
     * 
     * @param qtComprovanteSalarioDiversos the value of field
     * 'qtComprovanteSalarioDiversos'.
     */
    public void setQtComprovanteSalarioDiversos(int qtComprovanteSalarioDiversos)
    {
        this._qtComprovanteSalarioDiversos = qtComprovanteSalarioDiversos;
        this._has_qtComprovanteSalarioDiversos = true;
    } //-- void setQtComprovanteSalarioDiversos(int) 

    /**
     * Sets the value of field 'qtCredito'.
     * 
     * @param qtCredito the value of field 'qtCredito'.
     */
    public void setQtCredito(int qtCredito)
    {
        this._qtCredito = qtCredito;
        this._has_qtCredito = true;
    } //-- void setQtCredito(int) 

    /**
     * Sets the value of field 'qtForTitulo'.
     * 
     * @param qtForTitulo the value of field 'qtForTitulo'.
     */
    public void setQtForTitulo(int qtForTitulo)
    {
        this._qtForTitulo = qtForTitulo;
        this._has_qtForTitulo = true;
    } //-- void setQtForTitulo(int) 

    /**
     * Sets the value of field 'qtModalidadeBeneficio'.
     * 
     * @param qtModalidadeBeneficio the value of field
     * 'qtModalidadeBeneficio'.
     */
    public void setQtModalidadeBeneficio(int qtModalidadeBeneficio)
    {
        this._qtModalidadeBeneficio = qtModalidadeBeneficio;
        this._has_qtModalidadeBeneficio = true;
    } //-- void setQtModalidadeBeneficio(int) 

    /**
     * Sets the value of field 'qtModalidadeFornecedor'.
     * 
     * @param qtModalidadeFornecedor the value of field
     * 'qtModalidadeFornecedor'.
     */
    public void setQtModalidadeFornecedor(int qtModalidadeFornecedor)
    {
        this._qtModalidadeFornecedor = qtModalidadeFornecedor;
        this._has_qtModalidadeFornecedor = true;
    } //-- void setQtModalidadeFornecedor(int) 

    /**
     * Sets the value of field 'qtModalidadeRecadastro'.
     * 
     * @param qtModalidadeRecadastro the value of field
     * 'qtModalidadeRecadastro'.
     */
    public void setQtModalidadeRecadastro(int qtModalidadeRecadastro)
    {
        this._qtModalidadeRecadastro = qtModalidadeRecadastro;
        this._has_qtModalidadeRecadastro = true;
    } //-- void setQtModalidadeRecadastro(int) 

    /**
     * Sets the value of field 'qtModalidadeSalario'.
     * 
     * @param qtModalidadeSalario the value of field
     * 'qtModalidadeSalario'.
     */
    public void setQtModalidadeSalario(int qtModalidadeSalario)
    {
        this._qtModalidadeSalario = qtModalidadeSalario;
        this._has_qtModalidadeSalario = true;
    } //-- void setQtModalidadeSalario(int) 

    /**
     * Sets the value of field 'qtModalidadeTributos'.
     * 
     * @param qtModalidadeTributos the value of field
     * 'qtModalidadeTributos'.
     */
    public void setQtModalidadeTributos(int qtModalidadeTributos)
    {
        this._qtModalidadeTributos = qtModalidadeTributos;
        this._has_qtModalidadeTributos = true;
    } //-- void setQtModalidadeTributos(int) 

    /**
     * Sets the value of field 'qtModularidade'.
     * 
     * @param qtModularidade the value of field 'qtModularidade'.
     */
    public void setQtModularidade(int qtModularidade)
    {
        this._qtModularidade = qtModularidade;
        this._has_qtModularidade = true;
    } //-- void setQtModularidade(int) 

    /**
     * Sets the value of field 'qtOperacao'.
     * 
     * @param qtOperacao the value of field 'qtOperacao'.
     */
    public void setQtOperacao(int qtOperacao)
    {
        this._qtOperacao = qtOperacao;
        this._has_qtOperacao = true;
    } //-- void setQtOperacao(int) 

    /**
     * Sets the value of field 'qtRecEtapaRecadastramento'.
     * 
     * @param qtRecEtapaRecadastramento the value of field
     * 'qtRecEtapaRecadastramento'.
     */
    public void setQtRecEtapaRecadastramento(int qtRecEtapaRecadastramento)
    {
        this._qtRecEtapaRecadastramento = qtRecEtapaRecadastramento;
        this._has_qtRecEtapaRecadastramento = true;
    } //-- void setQtRecEtapaRecadastramento(int) 

    /**
     * Sets the value of field 'qtRecFaseEtapaRecadastramento'.
     * 
     * @param qtRecFaseEtapaRecadastramento the value of field
     * 'qtRecFaseEtapaRecadastramento'.
     */
    public void setQtRecFaseEtapaRecadastramento(int qtRecFaseEtapaRecadastramento)
    {
        this._qtRecFaseEtapaRecadastramento = qtRecFaseEtapaRecadastramento;
        this._has_qtRecFaseEtapaRecadastramento = true;
    } //-- void setQtRecFaseEtapaRecadastramento(int) 

    /**
     * Sets the value of field 'qtTriTributo'.
     * 
     * @param qtTriTributo the value of field 'qtTriTributo'.
     */
    public void setQtTriTributo(int qtTriTributo)
    {
        this._qtTriTributo = qtTriTributo;
        this._has_qtTriTributo = true;
    } //-- void setQtTriTributo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ImprimirAnexoSegundoContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.ImprimirAnexoSegundoContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.ImprimirAnexoSegundoContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.ImprimirAnexoSegundoContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.ImprimirAnexoSegundoContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
