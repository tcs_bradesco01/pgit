/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean;

/**
 * Nome: ConsultarPagamentosConsolidadosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ConsultarPagamentosConsolidadosEntradaDTO {

	/** Atributo cdTipoPesquisa. */
	private Integer cdTipoPesquisa = null;

	/** Atributo cdAgendadosPagosNaoPagos. */
	private Integer cdAgendadosPagosNaoPagos = null;

	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato = null;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio = null;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio = null;

	/** Atributo dtCreditoPagamentoInicio. */
	private String dtCreditoPagamentoInicio = null;

	/** Atributo dtCreditoPagamentoFim. */
	private String dtCreditoPagamentoFim = null;

	/** Atributo nrArquivoRemessaPagamento. */
	private Long nrArquivoRemessaPagamento = null;

	/** Atributo cdBanco. */
	private Integer cdBanco = null;

	/** Atributo cdAgencia. */
	private Integer cdAgencia = null;

	/** Atributo cdConta. */
	private Long cdConta = null;

	/** Atributo cdDigitoConta. */
	private String cdDigitoConta = null;

	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao = null;

	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado = null;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento = null;

	/** Atributo cdMotivoSituacaoPagamento. */
	private Integer cdMotivoSituacaoPagamento = null;

	/** Atributo cdTituloPgtoRastreado. */
	private Integer cdTituloPgtoRastreado = null;

	/** Atributo Integer */
	private Integer cdIndicadorAutorizacao = null;
	
	/** Atributo Long */
	private Long nrLoteInterno = null;
	
	/** Atributo Integer */
	private Integer cdTipoLayout = null;

	/**
	 * Get: cdAgencia.
	 * 
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 * 
	 * @param cdAgencia
	 *            the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdAgendadosPagosNaoPagos.
	 * 
	 * @return cdAgendadosPagosNaoPagos
	 */
	public Integer getCdAgendadosPagosNaoPagos() {
		return cdAgendadosPagosNaoPagos;
	}

	/**
	 * Set: cdAgendadosPagosNaoPagos.
	 * 
	 * @param cdAgendadosPagosNaoPagos
	 *            the cd agendados pagos nao pagos
	 */
	public void setCdAgendadosPagosNaoPagos(Integer cdAgendadosPagosNaoPagos) {
		this.cdAgendadosPagosNaoPagos = cdAgendadosPagosNaoPagos;
	}

	/**
	 * Get: cdBanco.
	 * 
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 * 
	 * @param cdBanco
	 *            the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdConta.
	 * 
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}

	/**
	 * Set: cdConta.
	 * 
	 * @param cdConta
	 *            the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	/**
	 * Get: cdDigitoConta.
	 * 
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	/**
	 * Set: cdDigitoConta.
	 * 
	 * @param cdDigitoConta
	 *            the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	/**
	 * Get: cdMotivoSituacaoPagamento.
	 * 
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}

	/**
	 * Set: cdMotivoSituacaoPagamento.
	 * 
	 * @param cdMotivoSituacaoPagamento
	 *            the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 * 
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 * 
	 * @param cdPessoaJuridicaContrato
	 *            the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 * 
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 * 
	 * @param cdProdutoServicoOperacao
	 *            the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoServicoRelacionado.
	 * 
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}

	/**
	 * Set: cdProdutoServicoRelacionado.
	 * 
	 * @param cdProdutoServicoRelacionado
	 *            the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(
			Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 * 
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 * 
	 * @param cdSituacaoOperacaoPagamento
	 *            the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(
			Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 * 
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 * 
	 * @param cdTipoContratoNegocio
	 *            the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdTipoPesquisa.
	 * 
	 * @return cdTipoPesquisa
	 */
	public Integer getCdTipoPesquisa() {
		return cdTipoPesquisa;
	}

	/**
	 * Set: cdTipoPesquisa.
	 * 
	 * @param cdTipoPesquisa
	 *            the cd tipo pesquisa
	 */
	public void setCdTipoPesquisa(Integer cdTipoPesquisa) {
		this.cdTipoPesquisa = cdTipoPesquisa;
	}

	/**
	 * Get: dtCreditoPagamentoFim.
	 * 
	 * @return dtCreditoPagamentoFim
	 */
	public String getDtCreditoPagamentoFim() {
		return dtCreditoPagamentoFim;
	}

	/**
	 * Set: dtCreditoPagamentoFim.
	 * 
	 * @param dtCreditoPagamentoFim
	 *            the dt credito pagamento fim
	 */
	public void setDtCreditoPagamentoFim(String dtCreditoPagamentoFim) {
		this.dtCreditoPagamentoFim = dtCreditoPagamentoFim;
	}

	/**
	 * Get: dtCreditoPagamentoInicio.
	 * 
	 * @return dtCreditoPagamentoInicio
	 */
	public String getDtCreditoPagamentoInicio() {
		return dtCreditoPagamentoInicio;
	}

	/**
	 * Set: dtCreditoPagamentoInicio.
	 * 
	 * @param dtCreditoPagamentoInicio
	 *            the dt credito pagamento inicio
	 */
	public void setDtCreditoPagamentoInicio(String dtCreditoPagamentoInicio) {
		this.dtCreditoPagamentoInicio = dtCreditoPagamentoInicio;
	}

	/**
	 * Get: nrArquivoRemessaPagamento.
	 * 
	 * @return nrArquivoRemessaPagamento
	 */
	public Long getNrArquivoRemessaPagamento() {
		return nrArquivoRemessaPagamento;
	}

	/**
	 * Set: nrArquivoRemessaPagamento.
	 * 
	 * @param nrArquivoRemessaPagamento
	 *            the nr arquivo remessa pagamento
	 */
	public void setNrArquivoRemessaPagamento(Long nrArquivoRemessaPagamento) {
		this.nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTituloPgtoRastreado.
	 * 
	 * @return cdTituloPgtoRastreado
	 */
	public Integer getCdTituloPgtoRastreado() {
		return cdTituloPgtoRastreado;
	}

	/**
	 * Set: cdTituloPgtoRastreado.
	 * 
	 * @param cdTituloPgtoRastreado
	 *            the cd titulo pgto rastreado
	 */
	public void setCdTituloPgtoRastreado(Integer cdTituloPgtoRastreado) {
		this.cdTituloPgtoRastreado = cdTituloPgtoRastreado;
	}

	/**
	 * Nome: getCdIndicadorAutorizacao
	 * 
	 * @return
	 */
	public Integer getCdIndicadorAutorizacao() {
		return cdIndicadorAutorizacao;
	}

	/**
	 * Nome: setCdIndicadorAutorizacao
	 * 
	 * @param cdIndicadorAutorizacao
	 */
	public void setCdIndicadorAutorizacao(Integer cdIndicadorAutorizacao) {
		this.cdIndicadorAutorizacao = cdIndicadorAutorizacao;
	}

	/**
	 * Nome: getNrLoteInterno
	 * 
	 * @return
	 */
	public Long getNrLoteInterno() {
		return nrLoteInterno;
	}

	/**
	 * Nome: setNrLoteInterno
	 * 
	 * @param nrLoteInterno
	 */
	public void setNrLoteInterno(Long nrLoteInterno) {
		this.nrLoteInterno = nrLoteInterno;
	}

	/**
	 * Nome: getCdTipoLayout
	 * 
	 * @return
	 */
	public Integer getCdTipoLayout() {
		return cdTipoLayout;
	}

	/**
	 * Nome: setCdTipoLayout
	 * 
	 * @param cdTipoLayout
	 */
	public void setCdTipoLayout(Integer cdTipoLayout) {
		this.cdTipoLayout = cdTipoLayout;
	}

}
