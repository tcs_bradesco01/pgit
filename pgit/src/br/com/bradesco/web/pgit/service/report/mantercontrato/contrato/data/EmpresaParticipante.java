/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

/**
 * Nome: EmpresaParticipante
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmpresaParticipante {

    /** Atributo cnpj. */
    private String cnpj = null;

    /** Atributo empresa. */
    private String empresa = null;

    /** Atributo perfilTransmissao. */
    private String perfilTransmissao = null;

    /** Atributo agencia. */
    private String agencia = null;

    /** Atributo conta. */
    private String conta = null;

    /**
     * Instancia um novo EmpresaParticipante
     *
     * @see
     */
    public EmpresaParticipante() {
        super();
    }

    /**
     * Nome: getCnpj.
     *
     * @return cnpj
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * Nome: setCnpj.
     *
     * @param cnpj the cnpj
     */
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    /**
     * Nome: getEmpresa.
     *
     * @return empresa
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Nome: setEmpresa.
     *
     * @param empresa the empresa
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     * Nome: getPerfilTransmissao.
     *
     * @return perfilTransmissao
     */
    public String getPerfilTransmissao() {
        return perfilTransmissao;
    }

    /**
     * Nome: setPerfilTransmissao.
     *
     * @param perfilTransmissao the perfil transmissao
     */
    public void setPerfilTransmissao(String perfilTransmissao) {
        this.perfilTransmissao = perfilTransmissao;
    }

    /**
     * Nome: getAgencia.
     *
     * @return agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Nome: setAgencia.
     *
     * @param agencia the agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Nome: getConta.
     *
     * @return conta
     */
    public String getConta() {
        return conta;
    }

    /**
     * Nome: setConta.
     *
     * @param conta the conta
     */
    public void setConta(String conta) {
        this.conta = conta;
    }

}
