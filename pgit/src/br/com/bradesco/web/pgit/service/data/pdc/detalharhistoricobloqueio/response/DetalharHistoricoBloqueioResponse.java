/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistoricoBloqueioResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistoricoBloqueioResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _dsCodigoSistema
     */
    private java.lang.String _dsCodigoSistema;

    /**
     * Field _cdProcessoSistema
     */
    private int _cdProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdProcessoSistema
     */
    private boolean _has_cdProcessoSistema;

    /**
     * Field _cdTipoProcessoSistema
     */
    private int _cdTipoProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdTipoProcessoSistema
     */
    private boolean _has_cdTipoProcessoSistema;

    /**
     * Field _dsCodigoTipoProcessoSistema
     */
    private java.lang.String _dsCodigoTipoProcessoSistema;

    /**
     * Field _cdPeriodicidade
     */
    private int _cdPeriodicidade = 0;

    /**
     * keeps track of state for field: _cdPeriodicidade
     */
    private boolean _has_cdPeriodicidade;

    /**
     * Field _dsCodigoPeriodicidade
     */
    private java.lang.String _dsCodigoPeriodicidade;

    /**
     * Field _dsProcessoSistema
     */
    private java.lang.String _dsProcessoSistema;

    /**
     * Field _cdSituacaoProcessoSistema
     */
    private int _cdSituacaoProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdSituacaoProcessoSistema
     */
    private boolean _has_cdSituacaoProcessoSistema;

    /**
     * Field _dsSistemaProcesso
     */
    private java.lang.String _dsSistemaProcesso;

    /**
     * Field _cdNetProcessamentoPagamento
     */
    private java.lang.String _cdNetProcessamentoPagamento;

    /**
     * Field _cdJobProcessamentoPagamento
     */
    private java.lang.String _cdJobProcessamentoPagamento;

    /**
     * Field _dsBloqueioProcessoSistema
     */
    private java.lang.String _dsBloqueioProcessoSistema;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenSegregacaoInclusao
     */
    private java.lang.String _cdAutenSegregacaoInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenSegregacaoManutencao
     */
    private java.lang.String _cdAutenSegregacaoManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrInclusaoManutencao
     */
    private java.lang.String _hrInclusaoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistoricoBloqueioResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.response.DetalharHistoricoBloqueioResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdPeriodicidade
     * 
     */
    public void deleteCdPeriodicidade()
    {
        this._has_cdPeriodicidade= false;
    } //-- void deleteCdPeriodicidade() 

    /**
     * Method deleteCdProcessoSistema
     * 
     */
    public void deleteCdProcessoSistema()
    {
        this._has_cdProcessoSistema= false;
    } //-- void deleteCdProcessoSistema() 

    /**
     * Method deleteCdSituacaoProcessoSistema
     * 
     */
    public void deleteCdSituacaoProcessoSistema()
    {
        this._has_cdSituacaoProcessoSistema= false;
    } //-- void deleteCdSituacaoProcessoSistema() 

    /**
     * Method deleteCdTipoProcessoSistema
     * 
     */
    public void deleteCdTipoProcessoSistema()
    {
        this._has_cdTipoProcessoSistema= false;
    } //-- void deleteCdTipoProcessoSistema() 

    /**
     * Returns the value of field 'cdAutenSegregacaoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegregacaoInclusao'.
     */
    public java.lang.String getCdAutenSegregacaoInclusao()
    {
        return this._cdAutenSegregacaoInclusao;
    } //-- java.lang.String getCdAutenSegregacaoInclusao() 

    /**
     * Returns the value of field 'cdAutenSegregacaoManutencao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegregacaoManutencao'.
     */
    public java.lang.String getCdAutenSegregacaoManutencao()
    {
        return this._cdAutenSegregacaoManutencao;
    } //-- java.lang.String getCdAutenSegregacaoManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdJobProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdJobProcessamentoPagamento'.
     */
    public java.lang.String getCdJobProcessamentoPagamento()
    {
        return this._cdJobProcessamentoPagamento;
    } //-- java.lang.String getCdJobProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdNetProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdNetProcessamentoPagamento'.
     */
    public java.lang.String getCdNetProcessamentoPagamento()
    {
        return this._cdNetProcessamentoPagamento;
    } //-- java.lang.String getCdNetProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdPeriodicidade'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidade'.
     */
    public int getCdPeriodicidade()
    {
        return this._cdPeriodicidade;
    } //-- int getCdPeriodicidade() 

    /**
     * Returns the value of field 'cdProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcessoSistema'.
     */
    public int getCdProcessoSistema()
    {
        return this._cdProcessoSistema;
    } //-- int getCdProcessoSistema() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdSituacaoProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoProcessoSistema'.
     */
    public int getCdSituacaoProcessoSistema()
    {
        return this._cdSituacaoProcessoSistema;
    } //-- int getCdSituacaoProcessoSistema() 

    /**
     * Returns the value of field 'cdTipoProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcessoSistema'.
     */
    public int getCdTipoProcessoSistema()
    {
        return this._cdTipoProcessoSistema;
    } //-- int getCdTipoProcessoSistema() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsBloqueioProcessoSistema'.
     * 
     * @return String
     * @return the value of field 'dsBloqueioProcessoSistema'.
     */
    public java.lang.String getDsBloqueioProcessoSistema()
    {
        return this._dsBloqueioProcessoSistema;
    } //-- java.lang.String getDsBloqueioProcessoSistema() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCodigoPeriodicidade'.
     * 
     * @return String
     * @return the value of field 'dsCodigoPeriodicidade'.
     */
    public java.lang.String getDsCodigoPeriodicidade()
    {
        return this._dsCodigoPeriodicidade;
    } //-- java.lang.String getDsCodigoPeriodicidade() 

    /**
     * Returns the value of field 'dsCodigoSistema'.
     * 
     * @return String
     * @return the value of field 'dsCodigoSistema'.
     */
    public java.lang.String getDsCodigoSistema()
    {
        return this._dsCodigoSistema;
    } //-- java.lang.String getDsCodigoSistema() 

    /**
     * Returns the value of field 'dsCodigoTipoProcessoSistema'.
     * 
     * @return String
     * @return the value of field 'dsCodigoTipoProcessoSistema'.
     */
    public java.lang.String getDsCodigoTipoProcessoSistema()
    {
        return this._dsCodigoTipoProcessoSistema;
    } //-- java.lang.String getDsCodigoTipoProcessoSistema() 

    /**
     * Returns the value of field 'dsProcessoSistema'.
     * 
     * @return String
     * @return the value of field 'dsProcessoSistema'.
     */
    public java.lang.String getDsProcessoSistema()
    {
        return this._dsProcessoSistema;
    } //-- java.lang.String getDsProcessoSistema() 

    /**
     * Returns the value of field 'dsSistemaProcesso'.
     * 
     * @return String
     * @return the value of field 'dsSistemaProcesso'.
     */
    public java.lang.String getDsSistemaProcesso()
    {
        return this._dsSistemaProcesso;
    } //-- java.lang.String getDsSistemaProcesso() 

    /**
     * Returns the value of field 'hrInclusaoManutencao'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoManutencao'.
     */
    public java.lang.String getHrInclusaoManutencao()
    {
        return this._hrInclusaoManutencao;
    } //-- java.lang.String getHrInclusaoManutencao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdPeriodicidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidade()
    {
        return this._has_cdPeriodicidade;
    } //-- boolean hasCdPeriodicidade() 

    /**
     * Method hasCdProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcessoSistema()
    {
        return this._has_cdProcessoSistema;
    } //-- boolean hasCdProcessoSistema() 

    /**
     * Method hasCdSituacaoProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoProcessoSistema()
    {
        return this._has_cdSituacaoProcessoSistema;
    } //-- boolean hasCdSituacaoProcessoSistema() 

    /**
     * Method hasCdTipoProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcessoSistema()
    {
        return this._has_cdTipoProcessoSistema;
    } //-- boolean hasCdTipoProcessoSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenSegregacaoInclusao'.
     * 
     * @param cdAutenSegregacaoInclusao the value of field
     * 'cdAutenSegregacaoInclusao'.
     */
    public void setCdAutenSegregacaoInclusao(java.lang.String cdAutenSegregacaoInclusao)
    {
        this._cdAutenSegregacaoInclusao = cdAutenSegregacaoInclusao;
    } //-- void setCdAutenSegregacaoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenSegregacaoManutencao'.
     * 
     * @param cdAutenSegregacaoManutencao the value of field
     * 'cdAutenSegregacaoManutencao'.
     */
    public void setCdAutenSegregacaoManutencao(java.lang.String cdAutenSegregacaoManutencao)
    {
        this._cdAutenSegregacaoManutencao = cdAutenSegregacaoManutencao;
    } //-- void setCdAutenSegregacaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdJobProcessamentoPagamento'.
     * 
     * @param cdJobProcessamentoPagamento the value of field
     * 'cdJobProcessamentoPagamento'.
     */
    public void setCdJobProcessamentoPagamento(java.lang.String cdJobProcessamentoPagamento)
    {
        this._cdJobProcessamentoPagamento = cdJobProcessamentoPagamento;
    } //-- void setCdJobProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdNetProcessamentoPagamento'.
     * 
     * @param cdNetProcessamentoPagamento the value of field
     * 'cdNetProcessamentoPagamento'.
     */
    public void setCdNetProcessamentoPagamento(java.lang.String cdNetProcessamentoPagamento)
    {
        this._cdNetProcessamentoPagamento = cdNetProcessamentoPagamento;
    } //-- void setCdNetProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdPeriodicidade'.
     * 
     * @param cdPeriodicidade the value of field 'cdPeriodicidade'.
     */
    public void setCdPeriodicidade(int cdPeriodicidade)
    {
        this._cdPeriodicidade = cdPeriodicidade;
        this._has_cdPeriodicidade = true;
    } //-- void setCdPeriodicidade(int) 

    /**
     * Sets the value of field 'cdProcessoSistema'.
     * 
     * @param cdProcessoSistema the value of field
     * 'cdProcessoSistema'.
     */
    public void setCdProcessoSistema(int cdProcessoSistema)
    {
        this._cdProcessoSistema = cdProcessoSistema;
        this._has_cdProcessoSistema = true;
    } //-- void setCdProcessoSistema(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoProcessoSistema'.
     * 
     * @param cdSituacaoProcessoSistema the value of field
     * 'cdSituacaoProcessoSistema'.
     */
    public void setCdSituacaoProcessoSistema(int cdSituacaoProcessoSistema)
    {
        this._cdSituacaoProcessoSistema = cdSituacaoProcessoSistema;
        this._has_cdSituacaoProcessoSistema = true;
    } //-- void setCdSituacaoProcessoSistema(int) 

    /**
     * Sets the value of field 'cdTipoProcessoSistema'.
     * 
     * @param cdTipoProcessoSistema the value of field
     * 'cdTipoProcessoSistema'.
     */
    public void setCdTipoProcessoSistema(int cdTipoProcessoSistema)
    {
        this._cdTipoProcessoSistema = cdTipoProcessoSistema;
        this._has_cdTipoProcessoSistema = true;
    } //-- void setCdTipoProcessoSistema(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsBloqueioProcessoSistema'.
     * 
     * @param dsBloqueioProcessoSistema the value of field
     * 'dsBloqueioProcessoSistema'.
     */
    public void setDsBloqueioProcessoSistema(java.lang.String dsBloqueioProcessoSistema)
    {
        this._dsBloqueioProcessoSistema = dsBloqueioProcessoSistema;
    } //-- void setDsBloqueioProcessoSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoPeriodicidade'.
     * 
     * @param dsCodigoPeriodicidade the value of field
     * 'dsCodigoPeriodicidade'.
     */
    public void setDsCodigoPeriodicidade(java.lang.String dsCodigoPeriodicidade)
    {
        this._dsCodigoPeriodicidade = dsCodigoPeriodicidade;
    } //-- void setDsCodigoPeriodicidade(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoSistema'.
     * 
     * @param dsCodigoSistema the value of field 'dsCodigoSistema'.
     */
    public void setDsCodigoSistema(java.lang.String dsCodigoSistema)
    {
        this._dsCodigoSistema = dsCodigoSistema;
    } //-- void setDsCodigoSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoTipoProcessoSistema'.
     * 
     * @param dsCodigoTipoProcessoSistema the value of field
     * 'dsCodigoTipoProcessoSistema'.
     */
    public void setDsCodigoTipoProcessoSistema(java.lang.String dsCodigoTipoProcessoSistema)
    {
        this._dsCodigoTipoProcessoSistema = dsCodigoTipoProcessoSistema;
    } //-- void setDsCodigoTipoProcessoSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsProcessoSistema'.
     * 
     * @param dsProcessoSistema the value of field
     * 'dsProcessoSistema'.
     */
    public void setDsProcessoSistema(java.lang.String dsProcessoSistema)
    {
        this._dsProcessoSistema = dsProcessoSistema;
    } //-- void setDsProcessoSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsSistemaProcesso'.
     * 
     * @param dsSistemaProcesso the value of field
     * 'dsSistemaProcesso'.
     */
    public void setDsSistemaProcesso(java.lang.String dsSistemaProcesso)
    {
        this._dsSistemaProcesso = dsSistemaProcesso;
    } //-- void setDsSistemaProcesso(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoManutencao'.
     * 
     * @param hrInclusaoManutencao the value of field
     * 'hrInclusaoManutencao'.
     */
    public void setHrInclusaoManutencao(java.lang.String hrInclusaoManutencao)
    {
        this._hrInclusaoManutencao = hrInclusaoManutencao;
    } //-- void setHrInclusaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistoricoBloqueioResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.response.DetalharHistoricoBloqueioResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.response.DetalharHistoricoBloqueioResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.response.DetalharHistoricoBloqueioResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.response.DetalharHistoricoBloqueioResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
