/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class BloqueioDesbloqueioProcessoMassivoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class BloqueioDesbloqueioProcessoMassivoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdProcessoSistema
     */
    private int _cdProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdProcessoSistema
     */
    private boolean _has_cdProcessoSistema;

    /**
     * Field _dsBloqueioProcessoSistema
     */
    private java.lang.String _dsBloqueioProcessoSistema;

    /**
     * Field _cdSituacaoProcessoSistema
     */
    private int _cdSituacaoProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdSituacaoProcessoSistema
     */
    private boolean _has_cdSituacaoProcessoSistema;


      //----------------/
     //- Constructors -/
    //----------------/

    public BloqueioDesbloqueioProcessoMassivoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.request.BloqueioDesbloqueioProcessoMassivoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProcessoSistema
     * 
     */
    public void deleteCdProcessoSistema()
    {
        this._has_cdProcessoSistema= false;
    } //-- void deleteCdProcessoSistema() 

    /**
     * Method deleteCdSituacaoProcessoSistema
     * 
     */
    public void deleteCdSituacaoProcessoSistema()
    {
        this._has_cdSituacaoProcessoSistema= false;
    } //-- void deleteCdSituacaoProcessoSistema() 

    /**
     * Returns the value of field 'cdProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcessoSistema'.
     */
    public int getCdProcessoSistema()
    {
        return this._cdProcessoSistema;
    } //-- int getCdProcessoSistema() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdSituacaoProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoProcessoSistema'.
     */
    public int getCdSituacaoProcessoSistema()
    {
        return this._cdSituacaoProcessoSistema;
    } //-- int getCdSituacaoProcessoSistema() 

    /**
     * Returns the value of field 'dsBloqueioProcessoSistema'.
     * 
     * @return String
     * @return the value of field 'dsBloqueioProcessoSistema'.
     */
    public java.lang.String getDsBloqueioProcessoSistema()
    {
        return this._dsBloqueioProcessoSistema;
    } //-- java.lang.String getDsBloqueioProcessoSistema() 

    /**
     * Method hasCdProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcessoSistema()
    {
        return this._has_cdProcessoSistema;
    } //-- boolean hasCdProcessoSistema() 

    /**
     * Method hasCdSituacaoProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoProcessoSistema()
    {
        return this._has_cdSituacaoProcessoSistema;
    } //-- boolean hasCdSituacaoProcessoSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProcessoSistema'.
     * 
     * @param cdProcessoSistema the value of field
     * 'cdProcessoSistema'.
     */
    public void setCdProcessoSistema(int cdProcessoSistema)
    {
        this._cdProcessoSistema = cdProcessoSistema;
        this._has_cdProcessoSistema = true;
    } //-- void setCdProcessoSistema(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoProcessoSistema'.
     * 
     * @param cdSituacaoProcessoSistema the value of field
     * 'cdSituacaoProcessoSistema'.
     */
    public void setCdSituacaoProcessoSistema(int cdSituacaoProcessoSistema)
    {
        this._cdSituacaoProcessoSistema = cdSituacaoProcessoSistema;
        this._has_cdSituacaoProcessoSistema = true;
    } //-- void setCdSituacaoProcessoSistema(int) 

    /**
     * Sets the value of field 'dsBloqueioProcessoSistema'.
     * 
     * @param dsBloqueioProcessoSistema the value of field
     * 'dsBloqueioProcessoSistema'.
     */
    public void setDsBloqueioProcessoSistema(java.lang.String dsBloqueioProcessoSistema)
    {
        this._dsBloqueioProcessoSistema = dsBloqueioProcessoSistema;
    } //-- void setDsBloqueioProcessoSistema(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return BloqueioDesbloqueioProcessoMassivoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.request.BloqueioDesbloqueioProcessoMassivoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.request.BloqueioDesbloqueioProcessoMassivoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.request.BloqueioDesbloqueioProcessoMassivoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.request.BloqueioDesbloqueioProcessoMassivoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
