package br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean;

import static br.com.bradesco.web.pgit.utils.PgitUtil.formatarCNPJComMascara;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatarCPFComMascara;

import java.math.BigDecimal;

public class OcorrenciasSolicLoteDTO {

	private Long cdCpfCnpjPagador;
	private Integer cdFilialCpfCnpjPagador;
	private Integer cdTipoCtaRecebedor;
	private String dsRazaoSocial;
	private String dsTipoServico;
	private String dsModalidade;
	private String cdControlePagamento;
	private String dsPagto;
	private BigDecimal vlPagto;
	private String dsRazaoSocialFav;
	private Integer cdBancoDebito;
	private Integer cdAgenciaDebito;
	private Long cdContaDebito;
	private Integer cdBcoRecebedor;
	private Integer cdAgenciaRecebedor;
	private Long cdContaRecebedor;
	private String dsMotivoErro;

	public Long getCdCpfCnpjPagador() {
		return cdCpfCnpjPagador;
	}

	public void setCdCpfCnpjPagador(final Long cdCpfCnpjPagador) {
		this.cdCpfCnpjPagador = cdCpfCnpjPagador;
	}

	public Integer getCdFilialCpfCnpjPagador() {
		return cdFilialCpfCnpjPagador;
	}

	public void setCdFilialCpfCnpjPagador(final Integer cdFilialCpfCnpjPagador) {
		this.cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
	}

	public Integer getCdTipoCtaRecebedor() {
		return cdTipoCtaRecebedor;
	}

	public void setCdTipoCtaRecebedor(final Integer cdTipoCtaRecebedor) {
		this.cdTipoCtaRecebedor = cdTipoCtaRecebedor;
	}

	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	public void setDsRazaoSocial(final String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	public String getDsTipoServico() {
		return dsTipoServico;
	}

	public void setDsTipoServico(final String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	public String getDsModalidade() {
		return dsModalidade;
	}

	public void setDsModalidade(final String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	public String getCdControlePagamento() {
		return cdControlePagamento;
	}

	public void setCdControlePagamento(final String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}

	public String getDsPagto() {
		return dsPagto;
	}

	public void setDsPagto(final String dsPagto) {
		this.dsPagto = dsPagto;
	}

	public BigDecimal getVlPagto() {
		return vlPagto;
	}

	public void setVlPagto(final BigDecimal vlPagto) {
		this.vlPagto = vlPagto;
	}

	public String getDsRazaoSocialFav() {
		return dsRazaoSocialFav;
	}

	public void setDsRazaoSocialFav(final String dsRazaoSocialFav) {
		this.dsRazaoSocialFav = dsRazaoSocialFav;
	}

	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	public void setCdBancoDebito(final Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	public void setCdAgenciaDebito(final Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	public void setCdContaDebito(final Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	public Integer getCdBcoRecebedor() {
		return cdBcoRecebedor;
	}

	public void setCdBcoRecebedor(final Integer cdBcoRecebedor) {
		this.cdBcoRecebedor = cdBcoRecebedor;
	}

	public Integer getCdAgenciaRecebedor() {
		return cdAgenciaRecebedor;
	}

	public void setCdAgenciaRecebedor(final Integer cdAgenciaRecebedor) {
		this.cdAgenciaRecebedor = cdAgenciaRecebedor;
	}

	public Long getCdContaRecebedor() {
		return cdContaRecebedor;
	}

	public void setCdContaRecebedor(final Long cdContaRecebedor) {
		this.cdContaRecebedor = cdContaRecebedor;
	}

	public String getDsMotivoErro() {
		return dsMotivoErro;
	}

	public void setDsMotivoErro(final String dsMotivoErro) {
		this.dsMotivoErro = dsMotivoErro;
	}
	
	public String getCpfCnpjFormatado(){
		if (cdFilialCpfCnpjPagador.compareTo(0) == 0) {
			return formatarCPFComMascara(cdCpfCnpjPagador.toString(), cdTipoCtaRecebedor.toString());
		} else {
			return formatarCNPJComMascara(cdCpfCnpjPagador.toString(), cdFilialCpfCnpjPagador.toString(), cdTipoCtaRecebedor.toString());
		}
	}

}
