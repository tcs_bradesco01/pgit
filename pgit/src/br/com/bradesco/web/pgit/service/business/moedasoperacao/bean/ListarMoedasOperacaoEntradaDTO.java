/*
 * Nome: br.com.bradesco.web.pgit.service.business.moedasoperacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.moedasoperacao.bean;

/**
 * Nome: ListarMoedasOperacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMoedasOperacaoEntradaDTO {
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo codigoMoedaIndice. */
	private int codigoMoedaIndice;
	
	/**
	 * Get: codigoMoedaIndice.
	 *
	 * @return codigoMoedaIndice
	 */
	public int getCodigoMoedaIndice() {
		return codigoMoedaIndice;
	}
	
	/**
	 * Set: codigoMoedaIndice.
	 *
	 * @param codigoMoedaIndice the codigo moeda indice
	 */
	public void setCodigoMoedaIndice(int codigoMoedaIndice) {
		this.codigoMoedaIndice = codigoMoedaIndice;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
	
}
