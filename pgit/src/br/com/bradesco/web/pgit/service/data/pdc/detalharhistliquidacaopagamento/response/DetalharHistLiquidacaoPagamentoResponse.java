/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistLiquidacaoPagamentoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistLiquidacaoPagamentoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _dsFormaLiquidacao
     */
    private java.lang.String _dsFormaLiquidacao;

    /**
     * Field _hrLimiteProcedimentoPagamento
     */
    private java.lang.String _hrLimiteProcedimentoPagamento;

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _qtMinutoMargemSeguranca
     */
    private int _qtMinutoMargemSeguranca = 0;

    /**
     * keeps track of state for field: _qtMinutoMargemSeguranca
     */
    private boolean _has_qtMinutoMargemSeguranca;

    /**
     * Field _cdPrioridadeDebitoForma
     */
    private int _cdPrioridadeDebitoForma = 0;

    /**
     * keeps track of state for field: _cdPrioridadeDebitoForma
     */
    private boolean _has_cdPrioridadeDebitoForma;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nrOperacaoFluxoInclusao
     */
    private java.lang.String _nrOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nrOperacaoFluxoManutencao
     */
    private java.lang.String _nrOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistroManutencao
     */
    private java.lang.String _hrManutencaoRegistroManutencao;

    /**
     * Field _dsSistema
     */
    private java.lang.String _dsSistema;

    /**
     * Field _hrConsultasSaldoPagamento
     */
    private java.lang.String _hrConsultasSaldoPagamento;

    /**
     * Field _hrConsultaFolhaPgto
     */
    private java.lang.String _hrConsultaFolhaPgto;

    /**
     * Field _qtMinutosValorSuperior
     */
    private int _qtMinutosValorSuperior = 0;

    /**
     * keeps track of state for field: _qtMinutosValorSuperior
     */
    private boolean _has_qtMinutosValorSuperior;

    /**
     * Field _hrLimiteValorSuperior
     */
    private java.lang.String _hrLimiteValorSuperior;

    /**
     * Field _qtdTempoAgendamentoCobranca
     */
    private int _qtdTempoAgendamentoCobranca = 0;

    /**
     * keeps track of state for field: _qtdTempoAgendamentoCobranca
     */
    private boolean _has_qtdTempoAgendamentoCobranca;

    /**
     * Field _qtdTempoEfetivacaoCiclicaCobranca
     */
    private int _qtdTempoEfetivacaoCiclicaCobranca = 0;

    /**
     * keeps track of state for field:
     * _qtdTempoEfetivacaoCiclicaCobranca
     */
    private boolean _has_qtdTempoEfetivacaoCiclicaCobranca;

    /**
     * Field _qtdTempoEfetivacaoDiariaCobranca
     */
    private int _qtdTempoEfetivacaoDiariaCobranca = 0;

    /**
     * keeps track of state for field:
     * _qtdTempoEfetivacaoDiariaCobranca
     */
    private boolean _has_qtdTempoEfetivacaoDiariaCobranca;

    /**
     * Field _qtdLimiteSemResposta
     */
    private int _qtdLimiteSemResposta = 0;

    /**
     * keeps track of state for field: _qtdLimiteSemResposta
     */
    private boolean _has_qtdLimiteSemResposta;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistLiquidacaoPagamentoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.response.DetalharHistLiquidacaoPagamentoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteCdPrioridadeDebitoForma
     * 
     */
    public void deleteCdPrioridadeDebitoForma()
    {
        this._has_cdPrioridadeDebitoForma= false;
    } //-- void deleteCdPrioridadeDebitoForma() 

    /**
     * Method deleteQtMinutoMargemSeguranca
     * 
     */
    public void deleteQtMinutoMargemSeguranca()
    {
        this._has_qtMinutoMargemSeguranca= false;
    } //-- void deleteQtMinutoMargemSeguranca() 

    /**
     * Method deleteQtMinutosValorSuperior
     * 
     */
    public void deleteQtMinutosValorSuperior()
    {
        this._has_qtMinutosValorSuperior= false;
    } //-- void deleteQtMinutosValorSuperior() 

    /**
     * Method deleteQtdLimiteSemResposta
     * 
     */
    public void deleteQtdLimiteSemResposta()
    {
        this._has_qtdLimiteSemResposta= false;
    } //-- void deleteQtdLimiteSemResposta() 

    /**
     * Method deleteQtdTempoAgendamentoCobranca
     * 
     */
    public void deleteQtdTempoAgendamentoCobranca()
    {
        this._has_qtdTempoAgendamentoCobranca= false;
    } //-- void deleteQtdTempoAgendamentoCobranca() 

    /**
     * Method deleteQtdTempoEfetivacaoCiclicaCobranca
     * 
     */
    public void deleteQtdTempoEfetivacaoCiclicaCobranca()
    {
        this._has_qtdTempoEfetivacaoCiclicaCobranca= false;
    } //-- void deleteQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Method deleteQtdTempoEfetivacaoDiariaCobranca
     * 
     */
    public void deleteQtdTempoEfetivacaoDiariaCobranca()
    {
        this._has_qtdTempoEfetivacaoDiariaCobranca= false;
    } //-- void deleteQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'cdPrioridadeDebitoForma'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeDebitoForma'.
     */
    public int getCdPrioridadeDebitoForma()
    {
        return this._cdPrioridadeDebitoForma;
    } //-- int getCdPrioridadeDebitoForma() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsFormaLiquidacao'.
     * 
     * @return String
     * @return the value of field 'dsFormaLiquidacao'.
     */
    public java.lang.String getDsFormaLiquidacao()
    {
        return this._dsFormaLiquidacao;
    } //-- java.lang.String getDsFormaLiquidacao() 

    /**
     * Returns the value of field 'dsSistema'.
     * 
     * @return String
     * @return the value of field 'dsSistema'.
     */
    public java.lang.String getDsSistema()
    {
        return this._dsSistema;
    } //-- java.lang.String getDsSistema() 

    /**
     * Returns the value of field 'hrConsultaFolhaPgto'.
     * 
     * @return String
     * @return the value of field 'hrConsultaFolhaPgto'.
     */
    public java.lang.String getHrConsultaFolhaPgto()
    {
        return this._hrConsultaFolhaPgto;
    } //-- java.lang.String getHrConsultaFolhaPgto() 

    /**
     * Returns the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrConsultasSaldoPagamento'.
     */
    public java.lang.String getHrConsultasSaldoPagamento()
    {
        return this._hrConsultasSaldoPagamento;
    } //-- java.lang.String getHrConsultasSaldoPagamento() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrLimiteProcedimentoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrLimiteProcedimentoPagamento'.
     */
    public java.lang.String getHrLimiteProcedimentoPagamento()
    {
        return this._hrLimiteProcedimentoPagamento;
    } //-- java.lang.String getHrLimiteProcedimentoPagamento() 

    /**
     * Returns the value of field 'hrLimiteValorSuperior'.
     * 
     * @return String
     * @return the value of field 'hrLimiteValorSuperior'.
     */
    public java.lang.String getHrLimiteValorSuperior()
    {
        return this._hrLimiteValorSuperior;
    } //-- java.lang.String getHrLimiteValorSuperior() 

    /**
     * Returns the value of field 'hrManutencaoRegistroManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroManutencao'.
     */
    public java.lang.String getHrManutencaoRegistroManutencao()
    {
        return this._hrManutencaoRegistroManutencao;
    } //-- java.lang.String getHrManutencaoRegistroManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoInclusao'.
     */
    public java.lang.String getNrOperacaoFluxoInclusao()
    {
        return this._nrOperacaoFluxoInclusao;
    } //-- java.lang.String getNrOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoManutencao'.
     */
    public java.lang.String getNrOperacaoFluxoManutencao()
    {
        return this._nrOperacaoFluxoManutencao;
    } //-- java.lang.String getNrOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'qtMinutoMargemSeguranca'.
     * 
     * @return int
     * @return the value of field 'qtMinutoMargemSeguranca'.
     */
    public int getQtMinutoMargemSeguranca()
    {
        return this._qtMinutoMargemSeguranca;
    } //-- int getQtMinutoMargemSeguranca() 

    /**
     * Returns the value of field 'qtMinutosValorSuperior'.
     * 
     * @return int
     * @return the value of field 'qtMinutosValorSuperior'.
     */
    public int getQtMinutosValorSuperior()
    {
        return this._qtMinutosValorSuperior;
    } //-- int getQtMinutosValorSuperior() 

    /**
     * Returns the value of field 'qtdLimiteSemResposta'.
     * 
     * @return int
     * @return the value of field 'qtdLimiteSemResposta'.
     */
    public int getQtdLimiteSemResposta()
    {
        return this._qtdLimiteSemResposta;
    } //-- int getQtdLimiteSemResposta() 

    /**
     * Returns the value of field 'qtdTempoAgendamentoCobranca'.
     * 
     * @return int
     * @return the value of field 'qtdTempoAgendamentoCobranca'.
     */
    public int getQtdTempoAgendamentoCobranca()
    {
        return this._qtdTempoAgendamentoCobranca;
    } //-- int getQtdTempoAgendamentoCobranca() 

    /**
     * Returns the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     * 
     * @return int
     * @return the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     */
    public int getQtdTempoEfetivacaoCiclicaCobranca()
    {
        return this._qtdTempoEfetivacaoCiclicaCobranca;
    } //-- int getQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Returns the value of field
     * 'qtdTempoEfetivacaoDiariaCobranca'.
     * 
     * @return int
     * @return the value of field 'qtdTempoEfetivacaoDiariaCobranca'
     */
    public int getQtdTempoEfetivacaoDiariaCobranca()
    {
        return this._qtdTempoEfetivacaoDiariaCobranca;
    } //-- int getQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasCdPrioridadeDebitoForma
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeDebitoForma()
    {
        return this._has_cdPrioridadeDebitoForma;
    } //-- boolean hasCdPrioridadeDebitoForma() 

    /**
     * Method hasQtMinutoMargemSeguranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMinutoMargemSeguranca()
    {
        return this._has_qtMinutoMargemSeguranca;
    } //-- boolean hasQtMinutoMargemSeguranca() 

    /**
     * Method hasQtMinutosValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMinutosValorSuperior()
    {
        return this._has_qtMinutosValorSuperior;
    } //-- boolean hasQtMinutosValorSuperior() 

    /**
     * Method hasQtdLimiteSemResposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdLimiteSemResposta()
    {
        return this._has_qtdLimiteSemResposta;
    } //-- boolean hasQtdLimiteSemResposta() 

    /**
     * Method hasQtdTempoAgendamentoCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoAgendamentoCobranca()
    {
        return this._has_qtdTempoAgendamentoCobranca;
    } //-- boolean hasQtdTempoAgendamentoCobranca() 

    /**
     * Method hasQtdTempoEfetivacaoCiclicaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoEfetivacaoCiclicaCobranca()
    {
        return this._has_qtdTempoEfetivacaoCiclicaCobranca;
    } //-- boolean hasQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Method hasQtdTempoEfetivacaoDiariaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoEfetivacaoDiariaCobranca()
    {
        return this._has_qtdTempoEfetivacaoDiariaCobranca;
    } //-- boolean hasQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'cdPrioridadeDebitoForma'.
     * 
     * @param cdPrioridadeDebitoForma the value of field
     * 'cdPrioridadeDebitoForma'.
     */
    public void setCdPrioridadeDebitoForma(int cdPrioridadeDebitoForma)
    {
        this._cdPrioridadeDebitoForma = cdPrioridadeDebitoForma;
        this._has_cdPrioridadeDebitoForma = true;
    } //-- void setCdPrioridadeDebitoForma(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaLiquidacao'.
     * 
     * @param dsFormaLiquidacao the value of field
     * 'dsFormaLiquidacao'.
     */
    public void setDsFormaLiquidacao(java.lang.String dsFormaLiquidacao)
    {
        this._dsFormaLiquidacao = dsFormaLiquidacao;
    } //-- void setDsFormaLiquidacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSistema'.
     * 
     * @param dsSistema the value of field 'dsSistema'.
     */
    public void setDsSistema(java.lang.String dsSistema)
    {
        this._dsSistema = dsSistema;
    } //-- void setDsSistema(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultaFolhaPgto'.
     * 
     * @param hrConsultaFolhaPgto the value of field
     * 'hrConsultaFolhaPgto'.
     */
    public void setHrConsultaFolhaPgto(java.lang.String hrConsultaFolhaPgto)
    {
        this._hrConsultaFolhaPgto = hrConsultaFolhaPgto;
    } //-- void setHrConsultaFolhaPgto(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @param hrConsultasSaldoPagamento the value of field
     * 'hrConsultasSaldoPagamento'.
     */
    public void setHrConsultasSaldoPagamento(java.lang.String hrConsultasSaldoPagamento)
    {
        this._hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
    } //-- void setHrConsultasSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteProcedimentoPagamento'.
     * 
     * @param hrLimiteProcedimentoPagamento the value of field
     * 'hrLimiteProcedimentoPagamento'.
     */
    public void setHrLimiteProcedimentoPagamento(java.lang.String hrLimiteProcedimentoPagamento)
    {
        this._hrLimiteProcedimentoPagamento = hrLimiteProcedimentoPagamento;
    } //-- void setHrLimiteProcedimentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteValorSuperior'.
     * 
     * @param hrLimiteValorSuperior the value of field
     * 'hrLimiteValorSuperior'.
     */
    public void setHrLimiteValorSuperior(java.lang.String hrLimiteValorSuperior)
    {
        this._hrLimiteValorSuperior = hrLimiteValorSuperior;
    } //-- void setHrLimiteValorSuperior(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroManutencao'.
     * 
     * @param hrManutencaoRegistroManutencao the value of field
     * 'hrManutencaoRegistroManutencao'.
     */
    public void setHrManutencaoRegistroManutencao(java.lang.String hrManutencaoRegistroManutencao)
    {
        this._hrManutencaoRegistroManutencao = hrManutencaoRegistroManutencao;
    } //-- void setHrManutencaoRegistroManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @param nrOperacaoFluxoInclusao the value of field
     * 'nrOperacaoFluxoInclusao'.
     */
    public void setNrOperacaoFluxoInclusao(java.lang.String nrOperacaoFluxoInclusao)
    {
        this._nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    } //-- void setNrOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @param nrOperacaoFluxoManutencao the value of field
     * 'nrOperacaoFluxoManutencao'.
     */
    public void setNrOperacaoFluxoManutencao(java.lang.String nrOperacaoFluxoManutencao)
    {
        this._nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
    } //-- void setNrOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'qtMinutoMargemSeguranca'.
     * 
     * @param qtMinutoMargemSeguranca the value of field
     * 'qtMinutoMargemSeguranca'.
     */
    public void setQtMinutoMargemSeguranca(int qtMinutoMargemSeguranca)
    {
        this._qtMinutoMargemSeguranca = qtMinutoMargemSeguranca;
        this._has_qtMinutoMargemSeguranca = true;
    } //-- void setQtMinutoMargemSeguranca(int) 

    /**
     * Sets the value of field 'qtMinutosValorSuperior'.
     * 
     * @param qtMinutosValorSuperior the value of field
     * 'qtMinutosValorSuperior'.
     */
    public void setQtMinutosValorSuperior(int qtMinutosValorSuperior)
    {
        this._qtMinutosValorSuperior = qtMinutosValorSuperior;
        this._has_qtMinutosValorSuperior = true;
    } //-- void setQtMinutosValorSuperior(int) 

    /**
     * Sets the value of field 'qtdLimiteSemResposta'.
     * 
     * @param qtdLimiteSemResposta the value of field
     * 'qtdLimiteSemResposta'.
     */
    public void setQtdLimiteSemResposta(int qtdLimiteSemResposta)
    {
        this._qtdLimiteSemResposta = qtdLimiteSemResposta;
        this._has_qtdLimiteSemResposta = true;
    } //-- void setQtdLimiteSemResposta(int) 

    /**
     * Sets the value of field 'qtdTempoAgendamentoCobranca'.
     * 
     * @param qtdTempoAgendamentoCobranca the value of field
     * 'qtdTempoAgendamentoCobranca'.
     */
    public void setQtdTempoAgendamentoCobranca(int qtdTempoAgendamentoCobranca)
    {
        this._qtdTempoAgendamentoCobranca = qtdTempoAgendamentoCobranca;
        this._has_qtdTempoAgendamentoCobranca = true;
    } //-- void setQtdTempoAgendamentoCobranca(int) 

    /**
     * Sets the value of field 'qtdTempoEfetivacaoCiclicaCobranca'.
     * 
     * @param qtdTempoEfetivacaoCiclicaCobranca the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     */
    public void setQtdTempoEfetivacaoCiclicaCobranca(int qtdTempoEfetivacaoCiclicaCobranca)
    {
        this._qtdTempoEfetivacaoCiclicaCobranca = qtdTempoEfetivacaoCiclicaCobranca;
        this._has_qtdTempoEfetivacaoCiclicaCobranca = true;
    } //-- void setQtdTempoEfetivacaoCiclicaCobranca(int) 

    /**
     * Sets the value of field 'qtdTempoEfetivacaoDiariaCobranca'.
     * 
     * @param qtdTempoEfetivacaoDiariaCobranca the value of field
     * 'qtdTempoEfetivacaoDiariaCobranca'.
     */
    public void setQtdTempoEfetivacaoDiariaCobranca(int qtdTempoEfetivacaoDiariaCobranca)
    {
        this._qtdTempoEfetivacaoDiariaCobranca = qtdTempoEfetivacaoDiariaCobranca;
        this._has_qtdTempoEfetivacaoDiariaCobranca = true;
    } //-- void setQtdTempoEfetivacaoDiariaCobranca(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistLiquidacaoPagamentoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.response.DetalharHistLiquidacaoPagamentoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.response.DetalharHistLiquidacaoPagamentoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.response.DetalharHistLiquidacaoPagamentoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.response.DetalharHistLiquidacaoPagamentoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
