/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroidentificao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroidentificao.bean;

/**
 * Nome: ConsultarListaFuncionarioBradescoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaFuncionarioBradescoEntradaDTO {
	
	/** Atributo numeroOcorrencia. */
	private Integer numeroOcorrencia;
    
    /** Atributo cdFuncionarioBradesco. */
    private Long cdFuncionarioBradesco;
    
    /** Atributo cdJuncaoPertencente. */
    private Integer cdJuncaoPertencente;
    
    /** Atributo cdSecaoSap. */
    private Integer cdSecaoSap;
    
    /** Atributo cdCgcCpf. */
    private Long cdCgcCpf;
    
	/**
	 * Consultar lista funcionario bradesco entrada dto.
	 */
	public ConsultarListaFuncionarioBradescoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Get: cdCgcCpf.
	 *
	 * @return cdCgcCpf
	 */
	public Long getCdCgcCpf() {
		return cdCgcCpf;
	}
	
	/**
	 * Set: cdCgcCpf.
	 *
	 * @param cdCgcCpf the cd cgc cpf
	 */
	public void setCdCgcCpf(Long cdCgcCpf) {
		this.cdCgcCpf = cdCgcCpf;
	}
	
	/**
	 * Get: cdFuncionarioBradesco.
	 *
	 * @return cdFuncionarioBradesco
	 */
	public Long getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}
	
	/**
	 * Set: cdFuncionarioBradesco.
	 *
	 * @param cdFuncionarioBradesco the cd funcionario bradesco
	 */
	public void setCdFuncionarioBradesco(Long cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}
	
	/**
	 * Get: cdJuncaoPertencente.
	 *
	 * @return cdJuncaoPertencente
	 */
	public Integer getCdJuncaoPertencente() {
		return cdJuncaoPertencente;
	}
	
	/**
	 * Set: cdJuncaoPertencente.
	 *
	 * @param cdJuncaoPertencente the cd juncao pertencente
	 */
	public void setCdJuncaoPertencente(Integer cdJuncaoPertencente) {
		this.cdJuncaoPertencente = cdJuncaoPertencente;
	}
	
	/**
	 * Get: cdSecaoSap.
	 *
	 * @return cdSecaoSap
	 */
	public Integer getCdSecaoSap() {
		return cdSecaoSap;
	}
	
	/**
	 * Set: cdSecaoSap.
	 *
	 * @param cdSecaoSap the cd secao sap
	 */
	public void setCdSecaoSap(Integer cdSecaoSap) {
		this.cdSecaoSap = cdSecaoSap;
	}
	
	/**
	 * Get: numeroOcorrencia.
	 *
	 * @return numeroOcorrencia
	 */
	public Integer getNumeroOcorrencia() {
		return numeroOcorrencia;
	}
	
	/**
	 * Set: numeroOcorrencia.
	 *
	 * @param numeroOcorrencia the numero ocorrencia
	 */
	public void setNumeroOcorrencia(Integer numeroOcorrencia) {
		this.numeroOcorrencia = numeroOcorrencia;
	}

}
