/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarRecursoCanalMsgSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarRecursoCanalMsgSaidaDTO {
	
	/** Atributo cdTipoCanal. */
	private int cdTipoCanal;
	
	/** Atributo dsTipoCanal. */
	private String dsTipoCanal;
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public int getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(int cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: dsTipoCanal.
	 *
	 * @return dsTipoCanal
	 */
	public String getDsTipoCanal() {
		return dsTipoCanal;
	}
	
	/**
	 * Set: dsTipoCanal.
	 *
	 * @param dsTipoCanal the ds tipo canal
	 */
	public void setDsTipoCanal(String dsTipoCanal) {
		this.dsTipoCanal = dsTipoCanal;
	}
	
	/**
	 * Listar recurso canal msg saida dto.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 * @param dsTipoCanal the ds tipo canal
	 */
	public ListarRecursoCanalMsgSaidaDTO(int cdTipoCanal, String dsTipoCanal) {
		super();
		this.cdTipoCanal = cdTipoCanal;
		this.dsTipoCanal = dsTipoCanal;
	}
	
	
	
	
}
