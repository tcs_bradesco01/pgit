/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirLiquidacaoPagamentoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirLiquidacaoPagamentoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdPrioridadeDebForma
     */
    private int _cdPrioridadeDebForma = 0;

    /**
     * keeps track of state for field: _cdPrioridadeDebForma
     */
    private boolean _has_cdPrioridadeDebForma;

    /**
     * Field _cdControleHoraLimite
     */
    private int _cdControleHoraLimite = 0;

    /**
     * keeps track of state for field: _cdControleHoraLimite
     */
    private boolean _has_cdControleHoraLimite;

    /**
     * Field _qtMinutoMargemSeguranca
     */
    private int _qtMinutoMargemSeguranca = 0;

    /**
     * keeps track of state for field: _qtMinutoMargemSeguranca
     */
    private boolean _has_qtMinutoMargemSeguranca;

    /**
     * Field _hrLimiteProcessamento
     */
    private java.lang.String _hrLimiteProcessamento;

    /**
     * Field _hrConsultasSaldoPagamento
     */
    private java.lang.String _hrConsultasSaldoPagamento;

    /**
     * Field _hrConsultaFolhaPgto
     */
    private java.lang.String _hrConsultaFolhaPgto;

    /**
     * Field _qtMinutosValorSuperior
     */
    private int _qtMinutosValorSuperior = 0;

    /**
     * keeps track of state for field: _qtMinutosValorSuperior
     */
    private boolean _has_qtMinutosValorSuperior;

    /**
     * Field _hrLimiteValorSuperior
     */
    private java.lang.String _hrLimiteValorSuperior;

    /**
     * Field _qtdTempoAgendamentoCobranca
     */
    private int _qtdTempoAgendamentoCobranca = 0;

    /**
     * keeps track of state for field: _qtdTempoAgendamentoCobranca
     */
    private boolean _has_qtdTempoAgendamentoCobranca;

    /**
     * Field _qtdTempoEfetivacaoCiclicaCobranca
     */
    private int _qtdTempoEfetivacaoCiclicaCobranca = 0;

    /**
     * keeps track of state for field:
     * _qtdTempoEfetivacaoCiclicaCobranca
     */
    private boolean _has_qtdTempoEfetivacaoCiclicaCobranca;

    /**
     * Field _qtdTempoEfetivacaoDiariaCobranca
     */
    private int _qtdTempoEfetivacaoDiariaCobranca = 0;

    /**
     * keeps track of state for field:
     * _qtdTempoEfetivacaoDiariaCobranca
     */
    private boolean _has_qtdTempoEfetivacaoDiariaCobranca;

    /**
     * Field _qtdLimiteSemResposta
     */
    private int _qtdLimiteSemResposta = 0;

    /**
     * keeps track of state for field: _qtdLimiteSemResposta
     */
    private boolean _has_qtdLimiteSemResposta;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirLiquidacaoPagamentoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.request.IncluirLiquidacaoPagamentoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleHoraLimite
     * 
     */
    public void deleteCdControleHoraLimite()
    {
        this._has_cdControleHoraLimite= false;
    } //-- void deleteCdControleHoraLimite() 

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteCdPrioridadeDebForma
     * 
     */
    public void deleteCdPrioridadeDebForma()
    {
        this._has_cdPrioridadeDebForma= false;
    } //-- void deleteCdPrioridadeDebForma() 

    /**
     * Method deleteQtMinutoMargemSeguranca
     * 
     */
    public void deleteQtMinutoMargemSeguranca()
    {
        this._has_qtMinutoMargemSeguranca= false;
    } //-- void deleteQtMinutoMargemSeguranca() 

    /**
     * Method deleteQtMinutosValorSuperior
     * 
     */
    public void deleteQtMinutosValorSuperior()
    {
        this._has_qtMinutosValorSuperior= false;
    } //-- void deleteQtMinutosValorSuperior() 

    /**
     * Method deleteQtdLimiteSemResposta
     * 
     */
    public void deleteQtdLimiteSemResposta()
    {
        this._has_qtdLimiteSemResposta= false;
    } //-- void deleteQtdLimiteSemResposta() 

    /**
     * Method deleteQtdTempoAgendamentoCobranca
     * 
     */
    public void deleteQtdTempoAgendamentoCobranca()
    {
        this._has_qtdTempoAgendamentoCobranca= false;
    } //-- void deleteQtdTempoAgendamentoCobranca() 

    /**
     * Method deleteQtdTempoEfetivacaoCiclicaCobranca
     * 
     */
    public void deleteQtdTempoEfetivacaoCiclicaCobranca()
    {
        this._has_qtdTempoEfetivacaoCiclicaCobranca= false;
    } //-- void deleteQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Method deleteQtdTempoEfetivacaoDiariaCobranca
     * 
     */
    public void deleteQtdTempoEfetivacaoDiariaCobranca()
    {
        this._has_qtdTempoEfetivacaoDiariaCobranca= false;
    } //-- void deleteQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Returns the value of field 'cdControleHoraLimite'.
     * 
     * @return int
     * @return the value of field 'cdControleHoraLimite'.
     */
    public int getCdControleHoraLimite()
    {
        return this._cdControleHoraLimite;
    } //-- int getCdControleHoraLimite() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'cdPrioridadeDebForma'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeDebForma'.
     */
    public int getCdPrioridadeDebForma()
    {
        return this._cdPrioridadeDebForma;
    } //-- int getCdPrioridadeDebForma() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'hrConsultaFolhaPgto'.
     * 
     * @return String
     * @return the value of field 'hrConsultaFolhaPgto'.
     */
    public java.lang.String getHrConsultaFolhaPgto()
    {
        return this._hrConsultaFolhaPgto;
    } //-- java.lang.String getHrConsultaFolhaPgto() 

    /**
     * Returns the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrConsultasSaldoPagamento'.
     */
    public java.lang.String getHrConsultasSaldoPagamento()
    {
        return this._hrConsultasSaldoPagamento;
    } //-- java.lang.String getHrConsultasSaldoPagamento() 

    /**
     * Returns the value of field 'hrLimiteProcessamento'.
     * 
     * @return String
     * @return the value of field 'hrLimiteProcessamento'.
     */
    public java.lang.String getHrLimiteProcessamento()
    {
        return this._hrLimiteProcessamento;
    } //-- java.lang.String getHrLimiteProcessamento() 

    /**
     * Returns the value of field 'hrLimiteValorSuperior'.
     * 
     * @return String
     * @return the value of field 'hrLimiteValorSuperior'.
     */
    public java.lang.String getHrLimiteValorSuperior()
    {
        return this._hrLimiteValorSuperior;
    } //-- java.lang.String getHrLimiteValorSuperior() 

    /**
     * Returns the value of field 'qtMinutoMargemSeguranca'.
     * 
     * @return int
     * @return the value of field 'qtMinutoMargemSeguranca'.
     */
    public int getQtMinutoMargemSeguranca()
    {
        return this._qtMinutoMargemSeguranca;
    } //-- int getQtMinutoMargemSeguranca() 

    /**
     * Returns the value of field 'qtMinutosValorSuperior'.
     * 
     * @return int
     * @return the value of field 'qtMinutosValorSuperior'.
     */
    public int getQtMinutosValorSuperior()
    {
        return this._qtMinutosValorSuperior;
    } //-- int getQtMinutosValorSuperior() 

    /**
     * Returns the value of field 'qtdLimiteSemResposta'.
     * 
     * @return int
     * @return the value of field 'qtdLimiteSemResposta'.
     */
    public int getQtdLimiteSemResposta()
    {
        return this._qtdLimiteSemResposta;
    } //-- int getQtdLimiteSemResposta() 

    /**
     * Returns the value of field 'qtdTempoAgendamentoCobranca'.
     * 
     * @return int
     * @return the value of field 'qtdTempoAgendamentoCobranca'.
     */
    public int getQtdTempoAgendamentoCobranca()
    {
        return this._qtdTempoAgendamentoCobranca;
    } //-- int getQtdTempoAgendamentoCobranca() 

    /**
     * Returns the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     * 
     * @return int
     * @return the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     */
    public int getQtdTempoEfetivacaoCiclicaCobranca()
    {
        return this._qtdTempoEfetivacaoCiclicaCobranca;
    } //-- int getQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Returns the value of field
     * 'qtdTempoEfetivacaoDiariaCobranca'.
     * 
     * @return int
     * @return the value of field 'qtdTempoEfetivacaoDiariaCobranca'
     */
    public int getQtdTempoEfetivacaoDiariaCobranca()
    {
        return this._qtdTempoEfetivacaoDiariaCobranca;
    } //-- int getQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Method hasCdControleHoraLimite
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleHoraLimite()
    {
        return this._has_cdControleHoraLimite;
    } //-- boolean hasCdControleHoraLimite() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasCdPrioridadeDebForma
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeDebForma()
    {
        return this._has_cdPrioridadeDebForma;
    } //-- boolean hasCdPrioridadeDebForma() 

    /**
     * Method hasQtMinutoMargemSeguranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMinutoMargemSeguranca()
    {
        return this._has_qtMinutoMargemSeguranca;
    } //-- boolean hasQtMinutoMargemSeguranca() 

    /**
     * Method hasQtMinutosValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMinutosValorSuperior()
    {
        return this._has_qtMinutosValorSuperior;
    } //-- boolean hasQtMinutosValorSuperior() 

    /**
     * Method hasQtdLimiteSemResposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdLimiteSemResposta()
    {
        return this._has_qtdLimiteSemResposta;
    } //-- boolean hasQtdLimiteSemResposta() 

    /**
     * Method hasQtdTempoAgendamentoCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoAgendamentoCobranca()
    {
        return this._has_qtdTempoAgendamentoCobranca;
    } //-- boolean hasQtdTempoAgendamentoCobranca() 

    /**
     * Method hasQtdTempoEfetivacaoCiclicaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoEfetivacaoCiclicaCobranca()
    {
        return this._has_qtdTempoEfetivacaoCiclicaCobranca;
    } //-- boolean hasQtdTempoEfetivacaoCiclicaCobranca() 

    /**
     * Method hasQtdTempoEfetivacaoDiariaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdTempoEfetivacaoDiariaCobranca()
    {
        return this._has_qtdTempoEfetivacaoDiariaCobranca;
    } //-- boolean hasQtdTempoEfetivacaoDiariaCobranca() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleHoraLimite'.
     * 
     * @param cdControleHoraLimite the value of field
     * 'cdControleHoraLimite'.
     */
    public void setCdControleHoraLimite(int cdControleHoraLimite)
    {
        this._cdControleHoraLimite = cdControleHoraLimite;
        this._has_cdControleHoraLimite = true;
    } //-- void setCdControleHoraLimite(int) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'cdPrioridadeDebForma'.
     * 
     * @param cdPrioridadeDebForma the value of field
     * 'cdPrioridadeDebForma'.
     */
    public void setCdPrioridadeDebForma(int cdPrioridadeDebForma)
    {
        this._cdPrioridadeDebForma = cdPrioridadeDebForma;
        this._has_cdPrioridadeDebForma = true;
    } //-- void setCdPrioridadeDebForma(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultaFolhaPgto'.
     * 
     * @param hrConsultaFolhaPgto the value of field
     * 'hrConsultaFolhaPgto'.
     */
    public void setHrConsultaFolhaPgto(java.lang.String hrConsultaFolhaPgto)
    {
        this._hrConsultaFolhaPgto = hrConsultaFolhaPgto;
    } //-- void setHrConsultaFolhaPgto(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @param hrConsultasSaldoPagamento the value of field
     * 'hrConsultasSaldoPagamento'.
     */
    public void setHrConsultasSaldoPagamento(java.lang.String hrConsultasSaldoPagamento)
    {
        this._hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
    } //-- void setHrConsultasSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteProcessamento'.
     * 
     * @param hrLimiteProcessamento the value of field
     * 'hrLimiteProcessamento'.
     */
    public void setHrLimiteProcessamento(java.lang.String hrLimiteProcessamento)
    {
        this._hrLimiteProcessamento = hrLimiteProcessamento;
    } //-- void setHrLimiteProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteValorSuperior'.
     * 
     * @param hrLimiteValorSuperior the value of field
     * 'hrLimiteValorSuperior'.
     */
    public void setHrLimiteValorSuperior(java.lang.String hrLimiteValorSuperior)
    {
        this._hrLimiteValorSuperior = hrLimiteValorSuperior;
    } //-- void setHrLimiteValorSuperior(java.lang.String) 

    /**
     * Sets the value of field 'qtMinutoMargemSeguranca'.
     * 
     * @param qtMinutoMargemSeguranca the value of field
     * 'qtMinutoMargemSeguranca'.
     */
    public void setQtMinutoMargemSeguranca(int qtMinutoMargemSeguranca)
    {
        this._qtMinutoMargemSeguranca = qtMinutoMargemSeguranca;
        this._has_qtMinutoMargemSeguranca = true;
    } //-- void setQtMinutoMargemSeguranca(int) 

    /**
     * Sets the value of field 'qtMinutosValorSuperior'.
     * 
     * @param qtMinutosValorSuperior the value of field
     * 'qtMinutosValorSuperior'.
     */
    public void setQtMinutosValorSuperior(int qtMinutosValorSuperior)
    {
        this._qtMinutosValorSuperior = qtMinutosValorSuperior;
        this._has_qtMinutosValorSuperior = true;
    } //-- void setQtMinutosValorSuperior(int) 

    /**
     * Sets the value of field 'qtdLimiteSemResposta'.
     * 
     * @param qtdLimiteSemResposta the value of field
     * 'qtdLimiteSemResposta'.
     */
    public void setQtdLimiteSemResposta(int qtdLimiteSemResposta)
    {
        this._qtdLimiteSemResposta = qtdLimiteSemResposta;
        this._has_qtdLimiteSemResposta = true;
    } //-- void setQtdLimiteSemResposta(int) 

    /**
     * Sets the value of field 'qtdTempoAgendamentoCobranca'.
     * 
     * @param qtdTempoAgendamentoCobranca the value of field
     * 'qtdTempoAgendamentoCobranca'.
     */
    public void setQtdTempoAgendamentoCobranca(int qtdTempoAgendamentoCobranca)
    {
        this._qtdTempoAgendamentoCobranca = qtdTempoAgendamentoCobranca;
        this._has_qtdTempoAgendamentoCobranca = true;
    } //-- void setQtdTempoAgendamentoCobranca(int) 

    /**
     * Sets the value of field 'qtdTempoEfetivacaoCiclicaCobranca'.
     * 
     * @param qtdTempoEfetivacaoCiclicaCobranca the value of field
     * 'qtdTempoEfetivacaoCiclicaCobranca'.
     */
    public void setQtdTempoEfetivacaoCiclicaCobranca(int qtdTempoEfetivacaoCiclicaCobranca)
    {
        this._qtdTempoEfetivacaoCiclicaCobranca = qtdTempoEfetivacaoCiclicaCobranca;
        this._has_qtdTempoEfetivacaoCiclicaCobranca = true;
    } //-- void setQtdTempoEfetivacaoCiclicaCobranca(int) 

    /**
     * Sets the value of field 'qtdTempoEfetivacaoDiariaCobranca'.
     * 
     * @param qtdTempoEfetivacaoDiariaCobranca the value of field
     * 'qtdTempoEfetivacaoDiariaCobranca'.
     */
    public void setQtdTempoEfetivacaoDiariaCobranca(int qtdTempoEfetivacaoDiariaCobranca)
    {
        this._qtdTempoEfetivacaoDiariaCobranca = qtdTempoEfetivacaoDiariaCobranca;
        this._has_qtdTempoEfetivacaoDiariaCobranca = true;
    } //-- void setQtdTempoEfetivacaoDiariaCobranca(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirLiquidacaoPagamentoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.request.IncluirLiquidacaoPagamentoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.request.IncluirLiquidacaoPagamentoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.request.IncluirLiquidacaoPagamentoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.request.IncluirLiquidacaoPagamentoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
