/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarServicosCatalogoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarServicosCatalogoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdProduServicoCdps
     */
    private int _cdProduServicoCdps = 0;

    /**
     * keeps track of state for field: _cdProduServicoCdps
     */
    private boolean _has_cdProduServicoCdps;

    /**
     * Field _cdProdtRelacionadoCdps
     */
    private int _cdProdtRelacionadoCdps = 0;

    /**
     * keeps track of state for field: _cdProdtRelacionadoCdps
     */
    private boolean _has_cdProdtRelacionadoCdps;

    /**
     * Field _cdTipoRelacionadoCdps
     */
    private int _cdTipoRelacionadoCdps = 0;

    /**
     * keeps track of state for field: _cdTipoRelacionadoCdps
     */
    private boolean _has_cdTipoRelacionadoCdps;

    /**
     * Field _cdProdtPgit
     */
    private long _cdProdtPgit = 0;

    /**
     * keeps track of state for field: _cdProdtPgit
     */
    private boolean _has_cdProdtPgit;

    /**
     * Field _cdSituacaoServc
     */
    private int _cdSituacaoServc = 0;

    /**
     * keeps track of state for field: _cdSituacaoServc
     */
    private boolean _has_cdSituacaoServc;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarServicosCatalogoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.response.ConsultarServicosCatalogoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProdtPgit
     * 
     */
    public void deleteCdProdtPgit()
    {
        this._has_cdProdtPgit= false;
    } //-- void deleteCdProdtPgit() 

    /**
     * Method deleteCdProdtRelacionadoCdps
     * 
     */
    public void deleteCdProdtRelacionadoCdps()
    {
        this._has_cdProdtRelacionadoCdps= false;
    } //-- void deleteCdProdtRelacionadoCdps() 

    /**
     * Method deleteCdProduServicoCdps
     * 
     */
    public void deleteCdProduServicoCdps()
    {
        this._has_cdProduServicoCdps= false;
    } //-- void deleteCdProduServicoCdps() 

    /**
     * Method deleteCdSituacaoServc
     * 
     */
    public void deleteCdSituacaoServc()
    {
        this._has_cdSituacaoServc= false;
    } //-- void deleteCdSituacaoServc() 

    /**
     * Method deleteCdTipoRelacionadoCdps
     * 
     */
    public void deleteCdTipoRelacionadoCdps()
    {
        this._has_cdTipoRelacionadoCdps= false;
    } //-- void deleteCdTipoRelacionadoCdps() 

    /**
     * Returns the value of field 'cdProdtPgit'.
     * 
     * @return long
     * @return the value of field 'cdProdtPgit'.
     */
    public long getCdProdtPgit()
    {
        return this._cdProdtPgit;
    } //-- long getCdProdtPgit() 

    /**
     * Returns the value of field 'cdProdtRelacionadoCdps'.
     * 
     * @return int
     * @return the value of field 'cdProdtRelacionadoCdps'.
     */
    public int getCdProdtRelacionadoCdps()
    {
        return this._cdProdtRelacionadoCdps;
    } //-- int getCdProdtRelacionadoCdps() 

    /**
     * Returns the value of field 'cdProduServicoCdps'.
     * 
     * @return int
     * @return the value of field 'cdProduServicoCdps'.
     */
    public int getCdProduServicoCdps()
    {
        return this._cdProduServicoCdps;
    } //-- int getCdProduServicoCdps() 

    /**
     * Returns the value of field 'cdSituacaoServc'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoServc'.
     */
    public int getCdSituacaoServc()
    {
        return this._cdSituacaoServc;
    } //-- int getCdSituacaoServc() 

    /**
     * Returns the value of field 'cdTipoRelacionadoCdps'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelacionadoCdps'.
     */
    public int getCdTipoRelacionadoCdps()
    {
        return this._cdTipoRelacionadoCdps;
    } //-- int getCdTipoRelacionadoCdps() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdProdtPgit
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdtPgit()
    {
        return this._has_cdProdtPgit;
    } //-- boolean hasCdProdtPgit() 

    /**
     * Method hasCdProdtRelacionadoCdps
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdtRelacionadoCdps()
    {
        return this._has_cdProdtRelacionadoCdps;
    } //-- boolean hasCdProdtRelacionadoCdps() 

    /**
     * Method hasCdProduServicoCdps
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProduServicoCdps()
    {
        return this._has_cdProduServicoCdps;
    } //-- boolean hasCdProduServicoCdps() 

    /**
     * Method hasCdSituacaoServc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoServc()
    {
        return this._has_cdSituacaoServc;
    } //-- boolean hasCdSituacaoServc() 

    /**
     * Method hasCdTipoRelacionadoCdps
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelacionadoCdps()
    {
        return this._has_cdTipoRelacionadoCdps;
    } //-- boolean hasCdTipoRelacionadoCdps() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProdtPgit'.
     * 
     * @param cdProdtPgit the value of field 'cdProdtPgit'.
     */
    public void setCdProdtPgit(long cdProdtPgit)
    {
        this._cdProdtPgit = cdProdtPgit;
        this._has_cdProdtPgit = true;
    } //-- void setCdProdtPgit(long) 

    /**
     * Sets the value of field 'cdProdtRelacionadoCdps'.
     * 
     * @param cdProdtRelacionadoCdps the value of field
     * 'cdProdtRelacionadoCdps'.
     */
    public void setCdProdtRelacionadoCdps(int cdProdtRelacionadoCdps)
    {
        this._cdProdtRelacionadoCdps = cdProdtRelacionadoCdps;
        this._has_cdProdtRelacionadoCdps = true;
    } //-- void setCdProdtRelacionadoCdps(int) 

    /**
     * Sets the value of field 'cdProduServicoCdps'.
     * 
     * @param cdProduServicoCdps the value of field
     * 'cdProduServicoCdps'.
     */
    public void setCdProduServicoCdps(int cdProduServicoCdps)
    {
        this._cdProduServicoCdps = cdProduServicoCdps;
        this._has_cdProduServicoCdps = true;
    } //-- void setCdProduServicoCdps(int) 

    /**
     * Sets the value of field 'cdSituacaoServc'.
     * 
     * @param cdSituacaoServc the value of field 'cdSituacaoServc'.
     */
    public void setCdSituacaoServc(int cdSituacaoServc)
    {
        this._cdSituacaoServc = cdSituacaoServc;
        this._has_cdSituacaoServc = true;
    } //-- void setCdSituacaoServc(int) 

    /**
     * Sets the value of field 'cdTipoRelacionadoCdps'.
     * 
     * @param cdTipoRelacionadoCdps the value of field
     * 'cdTipoRelacionadoCdps'.
     */
    public void setCdTipoRelacionadoCdps(int cdTipoRelacionadoCdps)
    {
        this._cdTipoRelacionadoCdps = cdTipoRelacionadoCdps;
        this._has_cdTipoRelacionadoCdps = true;
    } //-- void setCdTipoRelacionadoCdps(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarServicosCatalogoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.response.ConsultarServicosCatalogoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.response.ConsultarServicosCatalogoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.response.ConsultarServicosCatalogoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.response.ConsultarServicosCatalogoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
