/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasAvisos
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasAvisos {
	
	/** Atributo dsAvisoTipoAviso. */
	private String dsAvisoTipoAviso;
    
    /** Atributo dsAvisoPeriodicidadeEmissao. */
    private String dsAvisoPeriodicidadeEmissao;
    
    /** Atributo dsAvisoDestino. */
    private String dsAvisoDestino;
    
    /** Atributo dsAvisoPermissaoCorrespondenciaAbertura. */
    private String dsAvisoPermissaoCorrespondenciaAbertura;
    
    /** Atributo dsAvisoDemontracaoInformacaoReservada. */
    private String dsAvisoDemontracaoInformacaoReservada;
    
    /** Atributo cdAvisoQuantidadeViasEmitir. */
    private Integer cdAvisoQuantidadeViasEmitir;
    
    /** Atributo dsAvisoAgrupamentoCorrespondencia. */
    private String dsAvisoAgrupamentoCorrespondencia;
    
	/**
	 * Get: dsAvisoTipoAviso.
	 *
	 * @return dsAvisoTipoAviso
	 */
	public String getDsAvisoTipoAviso() {
		return dsAvisoTipoAviso;
	}
	
	/**
	 * Set: dsAvisoTipoAviso.
	 *
	 * @param dsAvisoTipoAviso the ds aviso tipo aviso
	 */
	public void setDsAvisoTipoAviso(String dsAvisoTipoAviso) {
		this.dsAvisoTipoAviso = dsAvisoTipoAviso;
	}
	
	/**
	 * Get: dsAvisoPeriodicidadeEmissao.
	 *
	 * @return dsAvisoPeriodicidadeEmissao
	 */
	public String getDsAvisoPeriodicidadeEmissao() {
		return dsAvisoPeriodicidadeEmissao;
	}
	
	/**
	 * Set: dsAvisoPeriodicidadeEmissao.
	 *
	 * @param dsAvisoPeriodicidadeEmissao the ds aviso periodicidade emissao
	 */
	public void setDsAvisoPeriodicidadeEmissao(String dsAvisoPeriodicidadeEmissao) {
		this.dsAvisoPeriodicidadeEmissao = dsAvisoPeriodicidadeEmissao;
	}
	
	/**
	 * Get: dsAvisoDestino.
	 *
	 * @return dsAvisoDestino
	 */
	public String getDsAvisoDestino() {
		return dsAvisoDestino;
	}
	
	/**
	 * Set: dsAvisoDestino.
	 *
	 * @param dsAvisoDestino the ds aviso destino
	 */
	public void setDsAvisoDestino(String dsAvisoDestino) {
		this.dsAvisoDestino = dsAvisoDestino;
	}
	
	/**
	 * Get: dsAvisoPermissaoCorrespondenciaAbertura.
	 *
	 * @return dsAvisoPermissaoCorrespondenciaAbertura
	 */
	public String getDsAvisoPermissaoCorrespondenciaAbertura() {
		return dsAvisoPermissaoCorrespondenciaAbertura;
	}
	
	/**
	 * Set: dsAvisoPermissaoCorrespondenciaAbertura.
	 *
	 * @param dsAvisoPermissaoCorrespondenciaAbertura the ds aviso permissao correspondencia abertura
	 */
	public void setDsAvisoPermissaoCorrespondenciaAbertura(
			String dsAvisoPermissaoCorrespondenciaAbertura) {
		this.dsAvisoPermissaoCorrespondenciaAbertura = dsAvisoPermissaoCorrespondenciaAbertura;
	}
	
	/**
	 * Get: dsAvisoDemontracaoInformacaoReservada.
	 *
	 * @return dsAvisoDemontracaoInformacaoReservada
	 */
	public String getDsAvisoDemontracaoInformacaoReservada() {
		return dsAvisoDemontracaoInformacaoReservada;
	}
	
	/**
	 * Set: dsAvisoDemontracaoInformacaoReservada.
	 *
	 * @param dsAvisoDemontracaoInformacaoReservada the ds aviso demontracao informacao reservada
	 */
	public void setDsAvisoDemontracaoInformacaoReservada(
			String dsAvisoDemontracaoInformacaoReservada) {
		this.dsAvisoDemontracaoInformacaoReservada = dsAvisoDemontracaoInformacaoReservada;
	}
	
	/**
	 * Get: cdAvisoQuantidadeViasEmitir.
	 *
	 * @return cdAvisoQuantidadeViasEmitir
	 */
	public Integer getCdAvisoQuantidadeViasEmitir() {
		return cdAvisoQuantidadeViasEmitir;
	}
	
	/**
	 * Set: cdAvisoQuantidadeViasEmitir.
	 *
	 * @param cdAvisoQuantidadeViasEmitir the cd aviso quantidade vias emitir
	 */
	public void setCdAvisoQuantidadeViasEmitir(Integer cdAvisoQuantidadeViasEmitir) {
		this.cdAvisoQuantidadeViasEmitir = cdAvisoQuantidadeViasEmitir;
	}
	
	/**
	 * Get: dsAvisoAgrupamentoCorrespondencia.
	 *
	 * @return dsAvisoAgrupamentoCorrespondencia
	 */
	public String getDsAvisoAgrupamentoCorrespondencia() {
		return dsAvisoAgrupamentoCorrespondencia;
	}
	
	/**
	 * Set: dsAvisoAgrupamentoCorrespondencia.
	 *
	 * @param dsAvisoAgrupamentoCorrespondencia the ds aviso agrupamento correspondencia
	 */
	public void setDsAvisoAgrupamentoCorrespondencia(
			String dsAvisoAgrupamentoCorrespondencia) {
		this.dsAvisoAgrupamentoCorrespondencia = dsAvisoAgrupamentoCorrespondencia;
	}
    
    
}
