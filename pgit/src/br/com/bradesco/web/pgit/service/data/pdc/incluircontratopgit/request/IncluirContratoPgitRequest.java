/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirContratoPgitRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirContratoPgitRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _faseContrato
     */
    private int _faseContrato = 0;

    /**
     * keeps track of state for field: _faseContrato
     */
    private boolean _has_faseContrato;

    /**
     * Field _cdPessoaJuridicaNegocio
     */
    private long _cdPessoaJuridicaNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaNegocio
     */
    private boolean _has_cdPessoaJuridicaNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdAcaoRelacionamento
     */
    private long _cdAcaoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdAcaoRelacionamento
     */
    private boolean _has_cdAcaoRelacionamento;

    /**
     * Field _cdContratoNegocioPagamento
     */
    private java.lang.String _cdContratoNegocioPagamento;

    /**
     * Field _cdIndicadorEconomicoMoeda
     */
    private int _cdIndicadorEconomicoMoeda = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoMoeda
     */
    private boolean _has_cdIndicadorEconomicoMoeda;

    /**
     * Field _cdIdioma
     */
    private int _cdIdioma = 0;

    /**
     * keeps track of state for field: _cdIdioma
     */
    private boolean _has_cdIdioma;

    /**
     * Field _dsContratoNegocio
     */
    private java.lang.String _dsContratoNegocio;

    /**
     * Field _cdTipoResponsavelOrganizacao
     */
    private int _cdTipoResponsavelOrganizacao = 0;

    /**
     * keeps track of state for field: _cdTipoResponsavelOrganizacao
     */
    private boolean _has_cdTipoResponsavelOrganizacao;

    /**
     * Field _cdFuncionalidadeGerContrato
     */
    private long _cdFuncionalidadeGerContrato = 0;

    /**
     * keeps track of state for field: _cdFuncionalidadeGerContrato
     */
    private boolean _has_cdFuncionalidadeGerContrato;

    /**
     * Field _cdAgenciaContrato
     */
    private int _cdAgenciaContrato = 0;

    /**
     * keeps track of state for field: _cdAgenciaContrato
     */
    private boolean _has_cdAgenciaContrato;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdSetor
     */
    private int _cdSetor = 0;

    /**
     * keeps track of state for field: _cdSetor
     */
    private boolean _has_cdSetor;

    /**
     * Field _qtServico
     */
    private int _qtServico = 0;

    /**
     * keeps track of state for field: _qtServico
     */
    private boolean _has_qtServico;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;

    /**
     * Field _qtContas
     */
    private int _qtContas = 0;

    /**
     * keeps track of state for field: _qtContas
     */
    private boolean _has_qtContas;

    /**
     * Field _ocorrencias1List
     */
    private java.util.Vector _ocorrencias1List;

    /**
     * Field _cdCpfCnpjConve
     */
    private long _cdCpfCnpjConve = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjConve
     */
    private boolean _has_cdCpfCnpjConve;

    /**
     * Field _cdFilialCnpjConve
     */
    private int _cdFilialCnpjConve = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjConve
     */
    private boolean _has_cdFilialCnpjConve;

    /**
     * Field _cdControleCnpjConve
     */
    private int _cdControleCnpjConve = 0;

    /**
     * keeps track of state for field: _cdControleCnpjConve
     */
    private boolean _has_cdControleCnpjConve;

    /**
     * Field _dsConveCtaSalrl
     */
    private java.lang.String _dsConveCtaSalrl;

    /**
     * Field _cdBcoEmprConvn
     */
    private int _cdBcoEmprConvn = 0;

    /**
     * keeps track of state for field: _cdBcoEmprConvn
     */
    private boolean _has_cdBcoEmprConvn;

    /**
     * Field _cdAgEmprConvn
     */
    private int _cdAgEmprConvn = 0;

    /**
     * keeps track of state for field: _cdAgEmprConvn
     */
    private boolean _has_cdAgEmprConvn;

    /**
     * Field _cdCtaEmpresaConvn
     */
    private long _cdCtaEmpresaConvn = 0;

    /**
     * keeps track of state for field: _cdCtaEmpresaConvn
     */
    private boolean _has_cdCtaEmpresaConvn;

    /**
     * Field _cdDigEmpresaConvn
     */
    private java.lang.String _cdDigEmpresaConvn;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _cdIndicadorConvnNovo
     */
    private int _cdIndicadorConvnNovo = 0;

    /**
     * keeps track of state for field: _cdIndicadorConvnNovo
     */
    private boolean _has_cdIndicadorConvnNovo;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirContratoPgitRequest() 
     {
        super();
        _ocorrenciasList = new Vector();
        _ocorrencias1List = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.IncluirContratoPgitRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param vOcorrencias1
     */
    public void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias1List.addElement(vOcorrencias1);
    } //-- void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void addOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias1List.insertElementAt(vOcorrencias1, index);
    } //-- void addOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1) 

    /**
     * Method deleteCdAcaoRelacionamento
     * 
     */
    public void deleteCdAcaoRelacionamento()
    {
        this._has_cdAcaoRelacionamento= false;
    } //-- void deleteCdAcaoRelacionamento() 

    /**
     * Method deleteCdAgEmprConvn
     * 
     */
    public void deleteCdAgEmprConvn()
    {
        this._has_cdAgEmprConvn= false;
    } //-- void deleteCdAgEmprConvn() 

    /**
     * Method deleteCdAgenciaContrato
     * 
     */
    public void deleteCdAgenciaContrato()
    {
        this._has_cdAgenciaContrato= false;
    } //-- void deleteCdAgenciaContrato() 

    /**
     * Method deleteCdBcoEmprConvn
     * 
     */
    public void deleteCdBcoEmprConvn()
    {
        this._has_cdBcoEmprConvn= false;
    } //-- void deleteCdBcoEmprConvn() 

    /**
     * Method deleteCdControleCnpjConve
     * 
     */
    public void deleteCdControleCnpjConve()
    {
        this._has_cdControleCnpjConve= false;
    } //-- void deleteCdControleCnpjConve() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdCpfCnpjConve
     * 
     */
    public void deleteCdCpfCnpjConve()
    {
        this._has_cdCpfCnpjConve= false;
    } //-- void deleteCdCpfCnpjConve() 

    /**
     * Method deleteCdCtaEmpresaConvn
     * 
     */
    public void deleteCdCtaEmpresaConvn()
    {
        this._has_cdCtaEmpresaConvn= false;
    } //-- void deleteCdCtaEmpresaConvn() 

    /**
     * Method deleteCdFilialCnpjConve
     * 
     */
    public void deleteCdFilialCnpjConve()
    {
        this._has_cdFilialCnpjConve= false;
    } //-- void deleteCdFilialCnpjConve() 

    /**
     * Method deleteCdFuncionalidadeGerContrato
     * 
     */
    public void deleteCdFuncionalidadeGerContrato()
    {
        this._has_cdFuncionalidadeGerContrato= false;
    } //-- void deleteCdFuncionalidadeGerContrato() 

    /**
     * Method deleteCdIdioma
     * 
     */
    public void deleteCdIdioma()
    {
        this._has_cdIdioma= false;
    } //-- void deleteCdIdioma() 

    /**
     * Method deleteCdIndicadorConvnNovo
     * 
     */
    public void deleteCdIndicadorConvnNovo()
    {
        this._has_cdIndicadorConvnNovo= false;
    } //-- void deleteCdIndicadorConvnNovo() 

    /**
     * Method deleteCdIndicadorEconomicoMoeda
     * 
     */
    public void deleteCdIndicadorEconomicoMoeda()
    {
        this._has_cdIndicadorEconomicoMoeda= false;
    } //-- void deleteCdIndicadorEconomicoMoeda() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaNegocio
     * 
     */
    public void deleteCdPessoaJuridicaNegocio()
    {
        this._has_cdPessoaJuridicaNegocio= false;
    } //-- void deleteCdPessoaJuridicaNegocio() 

    /**
     * Method deleteCdSetor
     * 
     */
    public void deleteCdSetor()
    {
        this._has_cdSetor= false;
    } //-- void deleteCdSetor() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdTipoResponsavelOrganizacao
     * 
     */
    public void deleteCdTipoResponsavelOrganizacao()
    {
        this._has_cdTipoResponsavelOrganizacao= false;
    } //-- void deleteCdTipoResponsavelOrganizacao() 

    /**
     * Method deleteFaseContrato
     * 
     */
    public void deleteFaseContrato()
    {
        this._has_faseContrato= false;
    } //-- void deleteFaseContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtContas
     * 
     */
    public void deleteQtContas()
    {
        this._has_qtContas= false;
    } //-- void deleteQtContas() 

    /**
     * Method deleteQtServico
     * 
     */
    public void deleteQtServico()
    {
        this._has_qtServico= false;
    } //-- void deleteQtServico() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Method enumerateOcorrencias1
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias1()
    {
        return _ocorrencias1List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias1() 

    /**
     * Returns the value of field 'cdAcaoRelacionamento'.
     * 
     * @return long
     * @return the value of field 'cdAcaoRelacionamento'.
     */
    public long getCdAcaoRelacionamento()
    {
        return this._cdAcaoRelacionamento;
    } //-- long getCdAcaoRelacionamento() 

    /**
     * Returns the value of field 'cdAgEmprConvn'.
     * 
     * @return int
     * @return the value of field 'cdAgEmprConvn'.
     */
    public int getCdAgEmprConvn()
    {
        return this._cdAgEmprConvn;
    } //-- int getCdAgEmprConvn() 

    /**
     * Returns the value of field 'cdAgenciaContrato'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaContrato'.
     */
    public int getCdAgenciaContrato()
    {
        return this._cdAgenciaContrato;
    } //-- int getCdAgenciaContrato() 

    /**
     * Returns the value of field 'cdBcoEmprConvn'.
     * 
     * @return int
     * @return the value of field 'cdBcoEmprConvn'.
     */
    public int getCdBcoEmprConvn()
    {
        return this._cdBcoEmprConvn;
    } //-- int getCdBcoEmprConvn() 

    /**
     * Returns the value of field 'cdContratoNegocioPagamento'.
     * 
     * @return String
     * @return the value of field 'cdContratoNegocioPagamento'.
     */
    public java.lang.String getCdContratoNegocioPagamento()
    {
        return this._cdContratoNegocioPagamento;
    } //-- java.lang.String getCdContratoNegocioPagamento() 

    /**
     * Returns the value of field 'cdControleCnpjConve'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjConve'.
     */
    public int getCdControleCnpjConve()
    {
        return this._cdControleCnpjConve;
    } //-- int getCdControleCnpjConve() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdCpfCnpjConve'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjConve'.
     */
    public long getCdCpfCnpjConve()
    {
        return this._cdCpfCnpjConve;
    } //-- long getCdCpfCnpjConve() 

    /**
     * Returns the value of field 'cdCtaEmpresaConvn'.
     * 
     * @return long
     * @return the value of field 'cdCtaEmpresaConvn'.
     */
    public long getCdCtaEmpresaConvn()
    {
        return this._cdCtaEmpresaConvn;
    } //-- long getCdCtaEmpresaConvn() 

    /**
     * Returns the value of field 'cdDigEmpresaConvn'.
     * 
     * @return String
     * @return the value of field 'cdDigEmpresaConvn'.
     */
    public java.lang.String getCdDigEmpresaConvn()
    {
        return this._cdDigEmpresaConvn;
    } //-- java.lang.String getCdDigEmpresaConvn() 

    /**
     * Returns the value of field 'cdFilialCnpjConve'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjConve'.
     */
    public int getCdFilialCnpjConve()
    {
        return this._cdFilialCnpjConve;
    } //-- int getCdFilialCnpjConve() 

    /**
     * Returns the value of field 'cdFuncionalidadeGerContrato'.
     * 
     * @return long
     * @return the value of field 'cdFuncionalidadeGerContrato'.
     */
    public long getCdFuncionalidadeGerContrato()
    {
        return this._cdFuncionalidadeGerContrato;
    } //-- long getCdFuncionalidadeGerContrato() 

    /**
     * Returns the value of field 'cdIdioma'.
     * 
     * @return int
     * @return the value of field 'cdIdioma'.
     */
    public int getCdIdioma()
    {
        return this._cdIdioma;
    } //-- int getCdIdioma() 

    /**
     * Returns the value of field 'cdIndicadorConvnNovo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorConvnNovo'.
     */
    public int getCdIndicadorConvnNovo()
    {
        return this._cdIndicadorConvnNovo;
    } //-- int getCdIndicadorConvnNovo() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoMoeda'.
     */
    public int getCdIndicadorEconomicoMoeda()
    {
        return this._cdIndicadorEconomicoMoeda;
    } //-- int getCdIndicadorEconomicoMoeda() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaNegocio'.
     */
    public long getCdPessoaJuridicaNegocio()
    {
        return this._cdPessoaJuridicaNegocio;
    } //-- long getCdPessoaJuridicaNegocio() 

    /**
     * Returns the value of field 'cdSetor'.
     * 
     * @return int
     * @return the value of field 'cdSetor'.
     */
    public int getCdSetor()
    {
        return this._cdSetor;
    } //-- int getCdSetor() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdTipoResponsavelOrganizacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoResponsavelOrganizacao'.
     */
    public int getCdTipoResponsavelOrganizacao()
    {
        return this._cdTipoResponsavelOrganizacao;
    } //-- int getCdTipoResponsavelOrganizacao() 

    /**
     * Returns the value of field 'dsContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsContratoNegocio'.
     */
    public java.lang.String getDsContratoNegocio()
    {
        return this._dsContratoNegocio;
    } //-- java.lang.String getDsContratoNegocio() 

    /**
     * Returns the value of field 'dsConveCtaSalrl'.
     * 
     * @return String
     * @return the value of field 'dsConveCtaSalrl'.
     */
    public java.lang.String getDsConveCtaSalrl()
    {
        return this._dsConveCtaSalrl;
    } //-- java.lang.String getDsConveCtaSalrl() 

    /**
     * Returns the value of field 'faseContrato'.
     * 
     * @return int
     * @return the value of field 'faseContrato'.
     */
    public int getFaseContrato()
    {
        return this._faseContrato;
    } //-- int getFaseContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1 getOcorrencias1(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias1: Index value '"+index+"' not in range [0.."+(_ocorrencias1List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1) _ocorrencias1List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1 getOcorrencias1(int) 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1[] getOcorrencias1()
    {
        int size = _ocorrencias1List.size();
        br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1) _ocorrencias1List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1[] getOcorrencias1() 

    /**
     * Method getOcorrencias1Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias1Count()
    {
        return _ocorrencias1List.size();
    } //-- int getOcorrencias1Count() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'qtContas'.
     * 
     * @return int
     * @return the value of field 'qtContas'.
     */
    public int getQtContas()
    {
        return this._qtContas;
    } //-- int getQtContas() 

    /**
     * Returns the value of field 'qtServico'.
     * 
     * @return int
     * @return the value of field 'qtServico'.
     */
    public int getQtServico()
    {
        return this._qtServico;
    } //-- int getQtServico() 

    /**
     * Method hasCdAcaoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoRelacionamento()
    {
        return this._has_cdAcaoRelacionamento;
    } //-- boolean hasCdAcaoRelacionamento() 

    /**
     * Method hasCdAgEmprConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgEmprConvn()
    {
        return this._has_cdAgEmprConvn;
    } //-- boolean hasCdAgEmprConvn() 

    /**
     * Method hasCdAgenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaContrato()
    {
        return this._has_cdAgenciaContrato;
    } //-- boolean hasCdAgenciaContrato() 

    /**
     * Method hasCdBcoEmprConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBcoEmprConvn()
    {
        return this._has_cdBcoEmprConvn;
    } //-- boolean hasCdBcoEmprConvn() 

    /**
     * Method hasCdControleCnpjConve
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjConve()
    {
        return this._has_cdControleCnpjConve;
    } //-- boolean hasCdControleCnpjConve() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdCpfCnpjConve
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjConve()
    {
        return this._has_cdCpfCnpjConve;
    } //-- boolean hasCdCpfCnpjConve() 

    /**
     * Method hasCdCtaEmpresaConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtaEmpresaConvn()
    {
        return this._has_cdCtaEmpresaConvn;
    } //-- boolean hasCdCtaEmpresaConvn() 

    /**
     * Method hasCdFilialCnpjConve
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjConve()
    {
        return this._has_cdFilialCnpjConve;
    } //-- boolean hasCdFilialCnpjConve() 

    /**
     * Method hasCdFuncionalidadeGerContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionalidadeGerContrato()
    {
        return this._has_cdFuncionalidadeGerContrato;
    } //-- boolean hasCdFuncionalidadeGerContrato() 

    /**
     * Method hasCdIdioma
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdioma()
    {
        return this._has_cdIdioma;
    } //-- boolean hasCdIdioma() 

    /**
     * Method hasCdIndicadorConvnNovo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorConvnNovo()
    {
        return this._has_cdIndicadorConvnNovo;
    } //-- boolean hasCdIndicadorConvnNovo() 

    /**
     * Method hasCdIndicadorEconomicoMoeda
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoMoeda()
    {
        return this._has_cdIndicadorEconomicoMoeda;
    } //-- boolean hasCdIndicadorEconomicoMoeda() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaNegocio()
    {
        return this._has_cdPessoaJuridicaNegocio;
    } //-- boolean hasCdPessoaJuridicaNegocio() 

    /**
     * Method hasCdSetor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSetor()
    {
        return this._has_cdSetor;
    } //-- boolean hasCdSetor() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdTipoResponsavelOrganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoResponsavelOrganizacao()
    {
        return this._has_cdTipoResponsavelOrganizacao;
    } //-- boolean hasCdTipoResponsavelOrganizacao() 

    /**
     * Method hasFaseContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFaseContrato()
    {
        return this._has_faseContrato;
    } //-- boolean hasFaseContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtContas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtContas()
    {
        return this._has_qtContas;
    } //-- boolean hasQtContas() 

    /**
     * Method hasQtServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtServico()
    {
        return this._has_qtServico;
    } //-- boolean hasQtServico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeAllOcorrencias1
     * 
     */
    public void removeAllOcorrencias1()
    {
        _ocorrencias1List.removeAllElements();
    } //-- void removeAllOcorrencias1() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Method removeOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1 removeOcorrencias1(int index)
    {
        java.lang.Object obj = _ocorrencias1List.elementAt(index);
        _ocorrencias1List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1 removeOcorrencias1(int) 

    /**
     * Sets the value of field 'cdAcaoRelacionamento'.
     * 
     * @param cdAcaoRelacionamento the value of field
     * 'cdAcaoRelacionamento'.
     */
    public void setCdAcaoRelacionamento(long cdAcaoRelacionamento)
    {
        this._cdAcaoRelacionamento = cdAcaoRelacionamento;
        this._has_cdAcaoRelacionamento = true;
    } //-- void setCdAcaoRelacionamento(long) 

    /**
     * Sets the value of field 'cdAgEmprConvn'.
     * 
     * @param cdAgEmprConvn the value of field 'cdAgEmprConvn'.
     */
    public void setCdAgEmprConvn(int cdAgEmprConvn)
    {
        this._cdAgEmprConvn = cdAgEmprConvn;
        this._has_cdAgEmprConvn = true;
    } //-- void setCdAgEmprConvn(int) 

    /**
     * Sets the value of field 'cdAgenciaContrato'.
     * 
     * @param cdAgenciaContrato the value of field
     * 'cdAgenciaContrato'.
     */
    public void setCdAgenciaContrato(int cdAgenciaContrato)
    {
        this._cdAgenciaContrato = cdAgenciaContrato;
        this._has_cdAgenciaContrato = true;
    } //-- void setCdAgenciaContrato(int) 

    /**
     * Sets the value of field 'cdBcoEmprConvn'.
     * 
     * @param cdBcoEmprConvn the value of field 'cdBcoEmprConvn'.
     */
    public void setCdBcoEmprConvn(int cdBcoEmprConvn)
    {
        this._cdBcoEmprConvn = cdBcoEmprConvn;
        this._has_cdBcoEmprConvn = true;
    } //-- void setCdBcoEmprConvn(int) 

    /**
     * Sets the value of field 'cdContratoNegocioPagamento'.
     * 
     * @param cdContratoNegocioPagamento the value of field
     * 'cdContratoNegocioPagamento'.
     */
    public void setCdContratoNegocioPagamento(java.lang.String cdContratoNegocioPagamento)
    {
        this._cdContratoNegocioPagamento = cdContratoNegocioPagamento;
    } //-- void setCdContratoNegocioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdControleCnpjConve'.
     * 
     * @param cdControleCnpjConve the value of field
     * 'cdControleCnpjConve'.
     */
    public void setCdControleCnpjConve(int cdControleCnpjConve)
    {
        this._cdControleCnpjConve = cdControleCnpjConve;
        this._has_cdControleCnpjConve = true;
    } //-- void setCdControleCnpjConve(int) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdCpfCnpjConve'.
     * 
     * @param cdCpfCnpjConve the value of field 'cdCpfCnpjConve'.
     */
    public void setCdCpfCnpjConve(long cdCpfCnpjConve)
    {
        this._cdCpfCnpjConve = cdCpfCnpjConve;
        this._has_cdCpfCnpjConve = true;
    } //-- void setCdCpfCnpjConve(long) 

    /**
     * Sets the value of field 'cdCtaEmpresaConvn'.
     * 
     * @param cdCtaEmpresaConvn the value of field
     * 'cdCtaEmpresaConvn'.
     */
    public void setCdCtaEmpresaConvn(long cdCtaEmpresaConvn)
    {
        this._cdCtaEmpresaConvn = cdCtaEmpresaConvn;
        this._has_cdCtaEmpresaConvn = true;
    } //-- void setCdCtaEmpresaConvn(long) 

    /**
     * Sets the value of field 'cdDigEmpresaConvn'.
     * 
     * @param cdDigEmpresaConvn the value of field
     * 'cdDigEmpresaConvn'.
     */
    public void setCdDigEmpresaConvn(java.lang.String cdDigEmpresaConvn)
    {
        this._cdDigEmpresaConvn = cdDigEmpresaConvn;
    } //-- void setCdDigEmpresaConvn(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpjConve'.
     * 
     * @param cdFilialCnpjConve the value of field
     * 'cdFilialCnpjConve'.
     */
    public void setCdFilialCnpjConve(int cdFilialCnpjConve)
    {
        this._cdFilialCnpjConve = cdFilialCnpjConve;
        this._has_cdFilialCnpjConve = true;
    } //-- void setCdFilialCnpjConve(int) 

    /**
     * Sets the value of field 'cdFuncionalidadeGerContrato'.
     * 
     * @param cdFuncionalidadeGerContrato the value of field
     * 'cdFuncionalidadeGerContrato'.
     */
    public void setCdFuncionalidadeGerContrato(long cdFuncionalidadeGerContrato)
    {
        this._cdFuncionalidadeGerContrato = cdFuncionalidadeGerContrato;
        this._has_cdFuncionalidadeGerContrato = true;
    } //-- void setCdFuncionalidadeGerContrato(long) 

    /**
     * Sets the value of field 'cdIdioma'.
     * 
     * @param cdIdioma the value of field 'cdIdioma'.
     */
    public void setCdIdioma(int cdIdioma)
    {
        this._cdIdioma = cdIdioma;
        this._has_cdIdioma = true;
    } //-- void setCdIdioma(int) 

    /**
     * Sets the value of field 'cdIndicadorConvnNovo'.
     * 
     * @param cdIndicadorConvnNovo the value of field
     * 'cdIndicadorConvnNovo'.
     */
    public void setCdIndicadorConvnNovo(int cdIndicadorConvnNovo)
    {
        this._cdIndicadorConvnNovo = cdIndicadorConvnNovo;
        this._has_cdIndicadorConvnNovo = true;
    } //-- void setCdIndicadorConvnNovo(int) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @param cdIndicadorEconomicoMoeda the value of field
     * 'cdIndicadorEconomicoMoeda'.
     */
    public void setCdIndicadorEconomicoMoeda(int cdIndicadorEconomicoMoeda)
    {
        this._cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
        this._has_cdIndicadorEconomicoMoeda = true;
    } //-- void setCdIndicadorEconomicoMoeda(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @param cdPessoaJuridicaNegocio the value of field
     * 'cdPessoaJuridicaNegocio'.
     */
    public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio)
    {
        this._cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
        this._has_cdPessoaJuridicaNegocio = true;
    } //-- void setCdPessoaJuridicaNegocio(long) 

    /**
     * Sets the value of field 'cdSetor'.
     * 
     * @param cdSetor the value of field 'cdSetor'.
     */
    public void setCdSetor(int cdSetor)
    {
        this._cdSetor = cdSetor;
        this._has_cdSetor = true;
    } //-- void setCdSetor(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdTipoResponsavelOrganizacao'.
     * 
     * @param cdTipoResponsavelOrganizacao the value of field
     * 'cdTipoResponsavelOrganizacao'.
     */
    public void setCdTipoResponsavelOrganizacao(int cdTipoResponsavelOrganizacao)
    {
        this._cdTipoResponsavelOrganizacao = cdTipoResponsavelOrganizacao;
        this._has_cdTipoResponsavelOrganizacao = true;
    } //-- void setCdTipoResponsavelOrganizacao(int) 

    /**
     * Sets the value of field 'dsContratoNegocio'.
     * 
     * @param dsContratoNegocio the value of field
     * 'dsContratoNegocio'.
     */
    public void setDsContratoNegocio(java.lang.String dsContratoNegocio)
    {
        this._dsContratoNegocio = dsContratoNegocio;
    } //-- void setDsContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsConveCtaSalrl'.
     * 
     * @param dsConveCtaSalrl the value of field 'dsConveCtaSalrl'.
     */
    public void setDsConveCtaSalrl(java.lang.String dsConveCtaSalrl)
    {
        this._dsConveCtaSalrl = dsConveCtaSalrl;
    } //-- void setDsConveCtaSalrl(java.lang.String) 

    /**
     * Sets the value of field 'faseContrato'.
     * 
     * @param faseContrato the value of field 'faseContrato'.
     */
    public void setFaseContrato(int faseContrato)
    {
        this._faseContrato = faseContrato;
        this._has_faseContrato = true;
    } //-- void setFaseContrato(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void setOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias1: Index value '"+index+"' not in range [0.." + (_ocorrencias1List.size() - 1) + "]");
        }
        _ocorrencias1List.setElementAt(vOcorrencias1, index);
    } //-- void setOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param ocorrencias1Array
     */
    public void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1[] ocorrencias1Array)
    {
        //-- copy array
        _ocorrencias1List.removeAllElements();
        for (int i = 0; i < ocorrencias1Array.length; i++) {
            _ocorrencias1List.addElement(ocorrencias1Array[i]);
        }
    } //-- void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.Ocorrencias1) 

    /**
     * Sets the value of field 'qtContas'.
     * 
     * @param qtContas the value of field 'qtContas'.
     */
    public void setQtContas(int qtContas)
    {
        this._qtContas = qtContas;
        this._has_qtContas = true;
    } //-- void setQtContas(int) 

    /**
     * Sets the value of field 'qtServico'.
     * 
     * @param qtServico the value of field 'qtServico'.
     */
    public void setQtServico(int qtServico)
    {
        this._qtServico = qtServico;
        this._has_qtServico = true;
    } //-- void setQtServico(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirContratoPgitRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.IncluirContratoPgitRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.IncluirContratoPgitRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.IncluirContratoPgitRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.request.IncluirContratoPgitRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
