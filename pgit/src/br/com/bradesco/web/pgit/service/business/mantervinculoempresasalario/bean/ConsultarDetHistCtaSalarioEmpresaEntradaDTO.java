/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

/**
 * Nome: ConsultarDetHistCtaSalarioEmpresaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetHistCtaSalarioEmpresaEntradaDTO {
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
    
    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato;
    
    /** Atributo nrSeqContrato. */
    private Long nrSeqContrato;
    
    /** Atributo cdPessoaVinc. */
    private Long cdPessoaVinc;
    
    /** Atributo cdTipoContratoVinc. */
    private Integer cdTipoContratoVinc;
    
    /** Atributo nrSeqContratoVinc. */
    private Long nrSeqContratoVinc;
    
    /** Atributo cdTipoVincContrato. */
    private Integer cdTipoVincContrato;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
	/**
	 * Consultar det hist cta salario empresa entrada dto.
	 */
	public ConsultarDetHistCtaSalarioEmpresaEntradaDTO() {
		super();
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaVinc.
	 *
	 * @return cdPessoaVinc
	 */
	public Long getCdPessoaVinc() {
		return cdPessoaVinc;
	}
	
	/**
	 * Set: cdPessoaVinc.
	 *
	 * @param cdPessoaVinc the cd pessoa vinc
	 */
	public void setCdPessoaVinc(Long cdPessoaVinc) {
		this.cdPessoaVinc = cdPessoaVinc;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}
	
	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: nrSeqContrato.
	 *
	 * @return nrSeqContrato
	 */
	public Long getNrSeqContrato() {
		return nrSeqContrato;
	}
	
	/**
	 * Set: nrSeqContrato.
	 *
	 * @param nrSeqContrato the nr seq contrato
	 */
	public void setNrSeqContrato(Long nrSeqContrato) {
		this.nrSeqContrato = nrSeqContrato;
	}
	
	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}
	
	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}

	/**
	 * Get: cdTipoVincContrato.
	 *
	 * @return cdTipoVincContrato
	 */
	public Integer getCdTipoVincContrato() {
		return cdTipoVincContrato;
	}

	/**
	 * Set: cdTipoVincContrato.
	 *
	 * @param cdTipoVincContrato the cd tipo vinc contrato
	 */
	public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
		this.cdTipoVincContrato = cdTipoVincContrato;
	}

}
