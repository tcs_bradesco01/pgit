/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private java.lang.String _cdDigitoAgencia;

    /**
     * Field _cdCta
     */
    private long _cdCta = 0;

    /**
     * keeps track of state for field: _cdCta
     */
    private boolean _has_cdCta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdConvnNovo
     */
    private int _cdConvnNovo = 0;

    /**
     * keeps track of state for field: _cdConvnNovo
     */
    private boolean _has_cdConvnNovo;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _cdCpfCnpjEmp
     */
    private long _cdCpfCnpjEmp = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjEmp
     */
    private boolean _has_cdCpfCnpjEmp;

    /**
     * Field _cdFilialCnpjEmp
     */
    private int _cdFilialCnpjEmp = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjEmp
     */
    private boolean _has_cdFilialCnpjEmp;

    /**
     * Field _cdControleCnpjEmp
     */
    private int _cdControleCnpjEmp = 0;

    /**
     * keeps track of state for field: _cdControleCnpjEmp
     */
    private boolean _has_cdControleCnpjEmp;

    /**
     * Field _dsConvn
     */
    private java.lang.String _dsConvn;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdControleCnpjEmp
     * 
     */
    public void deleteCdControleCnpjEmp()
    {
        this._has_cdControleCnpjEmp= false;
    } //-- void deleteCdControleCnpjEmp() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdConvnNovo
     * 
     */
    public void deleteCdConvnNovo()
    {
        this._has_cdConvnNovo= false;
    } //-- void deleteCdConvnNovo() 

    /**
     * Method deleteCdCpfCnpjEmp
     * 
     */
    public void deleteCdCpfCnpjEmp()
    {
        this._has_cdCpfCnpjEmp= false;
    } //-- void deleteCdCpfCnpjEmp() 

    /**
     * Method deleteCdCta
     * 
     */
    public void deleteCdCta()
    {
        this._has_cdCta= false;
    } //-- void deleteCdCta() 

    /**
     * Method deleteCdFilialCnpjEmp
     * 
     */
    public void deleteCdFilialCnpjEmp()
    {
        this._has_cdFilialCnpjEmp= false;
    } //-- void deleteCdFilialCnpjEmp() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdControleCnpjEmp'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjEmp'.
     */
    public int getCdControleCnpjEmp()
    {
        return this._cdControleCnpjEmp;
    } //-- int getCdControleCnpjEmp() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdConvnNovo'.
     * 
     * @return int
     * @return the value of field 'cdConvnNovo'.
     */
    public int getCdConvnNovo()
    {
        return this._cdConvnNovo;
    } //-- int getCdConvnNovo() 

    /**
     * Returns the value of field 'cdCpfCnpjEmp'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjEmp'.
     */
    public long getCdCpfCnpjEmp()
    {
        return this._cdCpfCnpjEmp;
    } //-- long getCdCpfCnpjEmp() 

    /**
     * Returns the value of field 'cdCta'.
     * 
     * @return long
     * @return the value of field 'cdCta'.
     */
    public long getCdCta()
    {
        return this._cdCta;
    } //-- long getCdCta() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgencia'.
     */
    public java.lang.String getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- java.lang.String getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFilialCnpjEmp'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjEmp'.
     */
    public int getCdFilialCnpjEmp()
    {
        return this._cdFilialCnpjEmp;
    } //-- int getCdFilialCnpjEmp() 

    /**
     * Returns the value of field 'dsConvn'.
     * 
     * @return String
     * @return the value of field 'dsConvn'.
     */
    public java.lang.String getDsConvn()
    {
        return this._dsConvn;
    } //-- java.lang.String getDsConvn() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdControleCnpjEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjEmp()
    {
        return this._has_cdControleCnpjEmp;
    } //-- boolean hasCdControleCnpjEmp() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdConvnNovo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConvnNovo()
    {
        return this._has_cdConvnNovo;
    } //-- boolean hasCdConvnNovo() 

    /**
     * Method hasCdCpfCnpjEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjEmp()
    {
        return this._has_cdCpfCnpjEmp;
    } //-- boolean hasCdCpfCnpjEmp() 

    /**
     * Method hasCdCta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCta()
    {
        return this._has_cdCta;
    } //-- boolean hasCdCta() 

    /**
     * Method hasCdFilialCnpjEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjEmp()
    {
        return this._has_cdFilialCnpjEmp;
    } //-- boolean hasCdFilialCnpjEmp() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdControleCnpjEmp'.
     * 
     * @param cdControleCnpjEmp the value of field
     * 'cdControleCnpjEmp'.
     */
    public void setCdControleCnpjEmp(int cdControleCnpjEmp)
    {
        this._cdControleCnpjEmp = cdControleCnpjEmp;
        this._has_cdControleCnpjEmp = true;
    } //-- void setCdControleCnpjEmp(int) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdConvnNovo'.
     * 
     * @param cdConvnNovo the value of field 'cdConvnNovo'.
     */
    public void setCdConvnNovo(int cdConvnNovo)
    {
        this._cdConvnNovo = cdConvnNovo;
        this._has_cdConvnNovo = true;
    } //-- void setCdConvnNovo(int) 

    /**
     * Sets the value of field 'cdCpfCnpjEmp'.
     * 
     * @param cdCpfCnpjEmp the value of field 'cdCpfCnpjEmp'.
     */
    public void setCdCpfCnpjEmp(long cdCpfCnpjEmp)
    {
        this._cdCpfCnpjEmp = cdCpfCnpjEmp;
        this._has_cdCpfCnpjEmp = true;
    } //-- void setCdCpfCnpjEmp(long) 

    /**
     * Sets the value of field 'cdCta'.
     * 
     * @param cdCta the value of field 'cdCta'.
     */
    public void setCdCta(long cdCta)
    {
        this._cdCta = cdCta;
        this._has_cdCta = true;
    } //-- void setCdCta(long) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(java.lang.String cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
    } //-- void setCdDigitoAgencia(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpjEmp'.
     * 
     * @param cdFilialCnpjEmp the value of field 'cdFilialCnpjEmp'.
     */
    public void setCdFilialCnpjEmp(int cdFilialCnpjEmp)
    {
        this._cdFilialCnpjEmp = cdFilialCnpjEmp;
        this._has_cdFilialCnpjEmp = true;
    } //-- void setCdFilialCnpjEmp(int) 

    /**
     * Sets the value of field 'dsConvn'.
     * 
     * @param dsConvn the value of field 'dsConvn'.
     */
    public void setDsConvn(java.lang.String dsConvn)
    {
        this._dsConvn = dsConvn;
    } //-- void setDsConvn(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
