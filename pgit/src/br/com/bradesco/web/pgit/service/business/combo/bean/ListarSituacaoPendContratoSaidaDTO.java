/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarSituacaoPendContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSituacaoPendContratoSaidaDTO {

	/** Atributo codSituacaoPendenciaContrato. */
	private Integer codSituacaoPendenciaContrato;
    
    /** Atributo dsSituacaoPendenciaContrato. */
    private String dsSituacaoPendenciaContrato;

    /**
     * Listar situacao pend contrato saida dto.
     */
    public ListarSituacaoPendContratoSaidaDTO() {
		super();
		
	}
    
    
    /**
     * Listar situacao pend contrato saida dto.
     *
     * @param codSituacaoPendenciaContrato the cod situacao pendencia contrato
     * @param dsSituacaoPendenciaContrato the ds situacao pendencia contrato
     */
    public ListarSituacaoPendContratoSaidaDTO(Integer codSituacaoPendenciaContrato, String dsSituacaoPendenciaContrato) {
		super();
		this.codSituacaoPendenciaContrato = codSituacaoPendenciaContrato;
		this.dsSituacaoPendenciaContrato = dsSituacaoPendenciaContrato;
	}



	/**
	 * Get: codSituacaoPendenciaContrato.
	 *
	 * @return codSituacaoPendenciaContrato
	 */
	public Integer getCodSituacaoPendenciaContrato() {
		return codSituacaoPendenciaContrato;
	}


	/**
	 * Set: codSituacaoPendenciaContrato.
	 *
	 * @param codSituacaoPendenciaContrato the cod situacao pendencia contrato
	 */
	public void setCodSituacaoPendenciaContrato(Integer codSituacaoPendenciaContrato) {
		this.codSituacaoPendenciaContrato = codSituacaoPendenciaContrato;
	}


	/**
	 * Get: dsSituacaoPendenciaContrato.
	 *
	 * @return dsSituacaoPendenciaContrato
	 */
	public String getDsSituacaoPendenciaContrato() {
		return dsSituacaoPendenciaContrato;
	}

	/**
	 * Set: dsSituacaoPendenciaContrato.
	 *
	 * @param dsSituacaoPendenciaContrato the ds situacao pendencia contrato
	 */
	public void setDsSituacaoPendenciaContrato(String dsSituacaoPendenciaContrato) {
		this.dsSituacaoPendenciaContrato = dsSituacaoPendenciaContrato;
	}

	

}
