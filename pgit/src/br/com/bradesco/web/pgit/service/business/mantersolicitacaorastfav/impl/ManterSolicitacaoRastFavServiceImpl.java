/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.DetalharSolicitacaoRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.DetalharSolicitacaoRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ExcluirSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ExcluirSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.IncluirSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.IncluirSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ManterSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ManterSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.request.ConsultarBancoAgenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response.ConsultarBancoAgenciaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.request.ConsultarListaSolicitacaoRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response.ConsultarListaSolicitacaoRastreamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.request.DetalharSolicitacaoRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.response.DetalharSolicitacaoRastreamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaorastreamento.request.ExcluirSolicitacaoRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaorastreamento.response.ExcluirSolicitacaoRastreamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.request.IncluirSolicitacaoRastreamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.response.IncluirSolicitacaoRastreamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.request.VerificaraAtributoSoltcRequest;
import br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.response.VerificaraAtributoSoltcResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterSolicitacaoRastFav
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterSolicitacaoRastFavServiceImpl implements IManterSolicitacaoRastFavService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;  
	
    /**
     * Manter solicitacao rast fav service impl.
     */
    public ManterSolicitacaoRastFavServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService#pesquisarSolicRastFavorecidos(br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ManterSolicRastFavEntradaDTO)
     */
    public List<ManterSolicRastFavSaidaDTO> pesquisarSolicRastFavorecidos(ManterSolicRastFavEntradaDTO entrada) {
    	
    	ConsultarListaSolicitacaoRastreamentoRequest request = new ConsultarListaSolicitacaoRastreamentoRequest();

		request.setCdPessoa(entrada.getCdPessoaJuridicaContrato() == null ? 0:entrada.getCdPessoaJuridicaContrato());
		request.setCdSituacao(entrada.getCdSituacaoSolicitacaoPagamento() == null ? 0:entrada.getCdSituacaoSolicitacaoPagamento());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0:entrada.getCdTipoContratoNegocio());
		request.setDtFimRastreabilidade(entrada.getDtFimRastreabilidade() == null ?"":entrada.getDtFimRastreabilidadeConv());
		request.setDtInicioRastreabilidade(entrada.getDtInicioRastreabilidade() == null ?"":entrada.getDtInicioRastreabilidadeConv());
		request.setNrOcorrencias(50);
		request.setNrSeqContratoNegocio(entrada.getNrSequenciaContratoNegocio() == null ? 0:entrada.getNrSequenciaContratoNegocio());
		
		ConsultarListaSolicitacaoRastreamentoResponse response = getFactoryAdapter().getConsultarListaSolicitacaoRastreamentoPDCAdapter().invokeProcess(request);
		
		List<ManterSolicRastFavSaidaDTO> list = new ArrayList<ManterSolicRastFavSaidaDTO>();
		
		for (Ocorrencias saida : response.getOcorrencias()) {
			list.add(new ManterSolicRastFavSaidaDTO(saida));
		}
		
		return list;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService#consultarDescricaoBancoAgencia(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO)
     */
    public ConsultarBancoAgenciaSaidaDTO consultarDescricaoBancoAgencia(ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia) {
		
		ConsultarBancoAgenciaRequest request = new ConsultarBancoAgenciaRequest();
		
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaConsultarBancoAgencia.getCdAgencia()));
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaConsultarBancoAgencia.getCdBanco()));
		request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entradaConsultarBancoAgencia.getCdDigitoAgencia()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaConsultarBancoAgencia.getCdPessoaJuridicaContrato()));
		
		
		ConsultarBancoAgenciaResponse response = getFactoryAdapter().getConsultarBancoAgenciaPDCAdapter().invokeProcess(request);
		
		if (response == null){
			return null;
		}

		ConsultarBancoAgenciaSaidaDTO saidaRetorno = new ConsultarBancoAgenciaSaidaDTO();
		
		saidaRetorno.setDsBanco(response.getDsBanco());
		saidaRetorno.setDsAgencia(response.getDsAgencia());
			
		saidaRetorno.setCodMensagem(response.getCodMensagem());
		saidaRetorno.setMensagem(response.getMensagem());

		saidaRetorno.setCdEndereco(response.getCdEndereco());
		saidaRetorno.setLogradouro(response.getLogradouro());
		saidaRetorno.setNumero(response.getNumero());
		saidaRetorno.setBairro(response.getBairro());
		saidaRetorno.setComplemento(response.getComplemento());
		saidaRetorno.setCidade(response.getCidade());
		saidaRetorno.setEstado(response.getEstado());
		saidaRetorno.setPais(response.getPais());
		saidaRetorno.setContato(response.getContato());
		saidaRetorno.setEmail(response.getEmail());
		saidaRetorno.setCep(response.getCep());
		saidaRetorno.setComplementoCep(response.getComplementoCep());

		return saidaRetorno ;		
	}
	
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService#incluirSolicRastFav(br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.IncluirSolicRastFavEntradaDTO)
	 */
	public IncluirSolicRastFavSaidaDTO incluirSolicRastFav(IncluirSolicRastFavEntradaDTO entrada) {
    	
		IncluirSolicitacaoRastreamentoRequest request = new IncluirSolicitacaoRastreamentoRequest(); 
    	
		request.setCdAgencia(entrada.getCdAgencia() == null ? 0:entrada.getCdAgencia());
		request.setCdBanco(entrada.getCdBanco() == null ? 0:entrada.getCdBanco());
		request.setCdConta(entrada.getCdConta() == null ? 0:entrada.getCdConta());
		request.setCdIndicadorDestinoRetorno(entrada.getCdIndicadorDestinoRetorno() == null ? 0:entrada.getCdIndicadorDestinoRetorno());
		request.setCdIndicadorInclusaoFavorecido(entrada.getCdIndicadorInclusaoFavorecido() == null ? 0:entrada.getCdIndicadorInclusaoFavorecido());
		request.setCdModalidade(entrada.getCdModalidade() == null ? 0:entrada.getCdModalidade());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato() == null ? 0:entrada.getCdPessoaJuridicaContrato());
		request.setCdServico(entrada.getCdServico() == null ? 0:entrada.getCdServico());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio() == null ? 0:entrada.getCdTipoContratoNegocio());
		request.setDgConta(entrada.getDgConta() == null ? "":entrada.getDgConta());
		request.setDtFimRastreamento(entrada.getDtFimRastreamento() == null ? "":entrada.getDtFimRastreamento());
		request.setDtInicioRastreamento(entrada.getDtInicioRastreamento() == null ? "":entrada.getDtInicioRastreamento());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio() == null ? 0:entrada.getNrSequenciaContratoNegocio());
		request.setVlTarifaPadrao(PgitUtil.verificaBigDecimalNulo(entrada.getVlTarifaPadrao()));
		request.setNumPercentualDescTarifa(PgitUtil.verificaBigDecimalNulo(entrada.getNumPercentualDescTarifa()));
		request.setVlrTarifaAtual(PgitUtil.verificaBigDecimalNulo(entrada.getVlrTarifaAtual()));
		request.setCdFormaFavorecido(PgitUtil.verificaIntegerNulo(entrada.getCdFormaFavorecido()));
		
		IncluirSolicitacaoRastreamentoResponse response = getFactoryAdapter().getIncluirSolicitacaoRastreamentoPDCAdapter().invokeProcess(request); 
		
		IncluirSolicRastFavSaidaDTO saida = new IncluirSolicRastFavSaidaDTO(response.getCodMensagem(),response.getMensagem());
	
		if(response == null){
			return null;
		}
		
		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService#excluirSolicRastFav(br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ExcluirSolicRastFavEntradaDTO)
	 */
	public ExcluirSolicRastFavSaidaDTO excluirSolicRastFav (ExcluirSolicRastFavEntradaDTO entrada) {
		
		ExcluirSolicitacaoRastreamentoRequest request = new ExcluirSolicitacaoRastreamentoRequest();
		
		request.setCdSolicitacaoPagamento(entrada.getCdSolicitacaoPagamento() ==null ? 0:entrada.getCdSolicitacaoPagamento());
		request.setNrSolicitacaoPagamento(entrada.getNrSolicitacaoPagamento() ==null ? 0:entrada.getNrSolicitacaoPagamento());
		
		ExcluirSolicitacaoRastreamentoResponse response = factoryAdapter.getExcluirSolicitacaoRastreamentoPDCAdapter().invokeProcess(request);
		
		if (response == null){
			return null;
		}

		ExcluirSolicRastFavSaidaDTO saida = new ExcluirSolicRastFavSaidaDTO(response.getCodMensagem(), response.getMensagem());
		
		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService#detalharSolicRastFav(br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.DetalharSolicitacaoRastreamentoEntradaDTO)
	 */
	public DetalharSolicitacaoRastreamentoSaidaDTO detalharSolicRastFav (DetalharSolicitacaoRastreamentoEntradaDTO entrada){
		 	
		DetalharSolicitacaoRastreamentoRequest request = new DetalharSolicitacaoRastreamentoRequest();
		
		request.setCdSolicitacaoPagamento(entrada.getCdSolicitacaoPagamento());
		request.setNrSolicitacaoPagamento(entrada.getNrSolicitacaoPagamento());
		
		DetalharSolicitacaoRastreamentoResponse response = factoryAdapter.getDetalharSolicitacaoRastreamentoPDCAdapter().invokeProcess(request);
		
		if (response == null){
			return null;
		}
		
		DetalharSolicitacaoRastreamentoSaidaDTO saida = new DetalharSolicitacaoRastreamentoSaidaDTO();
		
		saida.setCdAgenciaDeb(response.getCdAgenciaDeb());
		saida.setCdDigitoAgenciaDeb(response.getCdDigitoAgenciaDeb()==0?null:response.getCdDigitoAgenciaDeb());
		saida.setDsAgenciaDeb(response.getDsAgenciaDeb());
		saida.setCdBancoDeb(response.getCdBancoDeb());
		saida.setDsBancoDeb(response.getDsBancoDeb());
		saida.setCdContaDeb(response.getCdContaDeb()==0?null:response.getCdContaDeb());
		saida.setCdDigitoContaDeb(response.getCdDigitoContaDeb());
		saida.setCdIdDestinoRetorno(response.getCdIdDestinoRetorno());
		saida.setCdIndicadorInclusaoFavorecido(response.getCdIndicadorInclusaoFavorecido());
		saida.setCdMotivoSituacaoSolicitacao(response.getCdMotivoSituacaoSolicitacao());
		saida.setCdOperCanalInclusao(response.getCdOperCanalInclusao().equals("0")?"":response.getCdOperCanalInclusao());
		saida.setCdOperCanalManutencao(response.getCdOperCanalManutencao());
		saida.setCdPessoaJuridContrato(response.getCdPessoaJuridContrato());
		saida.setCdProdutoOperacaoRelacionado(response.getCdProdutoOperacaoRelacionado());
		saida.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
		saida.setCdRelacionamentoProduto(response.getCdRelacionamentoProduto());
		saida.setCdSituacaoSolicitacaoPagamento(response.getCdSituacaoSolicitacaoPagamento());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao()==0?null:response.getCdTipoCanalInclusao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao()==0?null:response.getCdTipoCanalManutencao());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExter(response.getCdUsuarioInclusaoExter());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExter(response.getCdUsuarioManutencaoExter());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setDsIdDestinoRetorno(response.getDsIdDestinoRetorno());
		saida.setDsIndicadorInclusaoFavorecido(response.getDsIndicadorInclusaoFavorecido());
		saida.setDsMotivoSituacaoSolicitacao(response.getDsMotivoSituacaoSolicitacao());
		saida.setDsProdutoOperacaoRelacionado(response.getDsProdutoOperacaoRelacionado());
		saida.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
		saida.setDsSituacaoSolicitacaoPagamento(response.getDsSituacaoSolicitacaoPagamento());
		saida.setDtAtendimento(response.getDtAtendimento());
		saida.setDtFimRastreabilidadeFavorecido(response.getDtFimRastreabilidadeFavorecido());
		saida.setDtInclusao(response.getDtInclusao());
		saida.setDtInicioRastreabilidadeFavorecido(response.getDtInicioRastreabilidadeFavorecido());
		saida.setDtManutencao(response.getDtManutencao());
		saida.setDtSolicitacao(response.getDtSolicitacao());
		saida.setHrAtendimento(response.getHrAtendimento());
		saida.setHrInclusao(response.getHrInclusao());
		saida.setHrManutencao(response.getHrManutencao());
		saida.setHrSolicitacao(response.getHrSolicitacao());
		saida.setNrSeqContratoNegocio(response.getNrSeqContratoNegocio());
		
		saida.setNrTarifaBonificacao(response.getVlTarifaPadrao()== BigDecimal.ZERO?null:response.getVlTarifaPadrao());
		
	    saida.setVlTarifaPadrao(response.getVlTarifaPadrao());
	    saida.setNumPercentualDescTarifa(response.getNumPercentualDescTarifa());
	    saida.setVlrTarifaAtual(response.getVlrTarifaAtual());
		
		
		saida.setCdFormaCadastroFavorecidos(response.getCdFormaFavorecido());
		saida.setDsFormaCadastroFavorecidos(response.getDsFormaCadastroFavorecidos());
		
		return saida;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService#verificaraAtributoSoltc(br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO)
	 */
	public VerificaraAtributoSoltcSaidaDTO verificaraAtributoSoltc(VerificaraAtributoSoltcEntradaDTO entrada){
		
		VerificaraAtributoSoltcRequest request =  new VerificaraAtributoSoltcRequest();
		VerificaraAtributoSoltcSaidaDTO saida =  new VerificaraAtributoSoltcSaidaDTO();
		request.setCdFuncionario(PgitUtil.verificaIntegerNulo(entrada.getCdFuncionario()));
		
		VerificaraAtributoSoltcResponse response = factoryAdapter.getVerificaraAtributoSoltcPDCAdapter().invokeProcess(request);
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDsTarifaBloqueio(response.getDsTarifaBloqueio());
		saida.setMensagem(response.getMensagem());
		
		return saida;
	}
	

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}


