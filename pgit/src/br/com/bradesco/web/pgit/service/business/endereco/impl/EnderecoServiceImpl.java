/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.endereco.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DatalharEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DatalharEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DetalharHistoricoEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DetalharHistoricoEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ExcluirEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ExcluirEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.IncluirEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.IncluirEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoEmailOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoParticipanteOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarHistoricoEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.SubstituirVincEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.SubstituirVincEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.request.DetalharEnderecoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.response.DetalharEnderecoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.request.DetalharHistoricoEnderecoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.response.DetalharHistoricoEnderecoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirenderecoparticipante.request.ExcluirEnderecoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirenderecoparticipante.response.ExcluirEnderecoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirenderecoparticipante.request.IncluirEnderecoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirenderecoparticipante.response.IncluirEnderecoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.request.ListarEnderecoEmailRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.response.ListarEnderecoEmailResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarenderecoparticipante.request.ListarEnderecoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarenderecoparticipante.response.ListarEnderecoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarenderecoparticipante.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoenderecoparticipante.request.ListarHistoricoEnderecoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoenderecoparticipante.response.ListarHistoricoEnderecoParticipanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.request.SubstituirVincEnderecoParticipanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.response.SubstituirVincEnderecoParticipanteResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Endereco
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class EnderecoServiceImpl implements IEnderecoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter = null;
	
	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService#listarEnderecoParticipante(br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoParticipanteEntradaDTO)
	 */
	public List<ListarEnderecoParticipanteOcorrenciasSaidaDTO> listarEnderecoParticipante(ListarEnderecoParticipanteEntradaDTO entradaDTO) {
		ListarEnderecoParticipanteRequest request = new ListarEnderecoParticipanteRequest();

		request.setMaxOcorrencias(50);
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdClub(PgitUtil.verificaLongNulo(entradaDTO.getCdClub()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoServico()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(0);

		ListarEnderecoParticipanteResponse response = getFactoryAdapter().getListarEnderecoParticipantePDCAdapter().invokeProcess(request);

		List<ListarEnderecoParticipanteOcorrenciasSaidaDTO> listaEndereco = new ArrayList<ListarEnderecoParticipanteOcorrenciasSaidaDTO>();
		for (Ocorrencias o : response.getOcorrencias()) {
			ListarEnderecoParticipanteOcorrenciasSaidaDTO endereco = new ListarEnderecoParticipanteOcorrenciasSaidaDTO();
			endereco.setCdPessoaJuridicaContrato(o.getCdPessoaJuridicaContrato());
			endereco.setCdTipoContratoNegocio(o.getCdTipoContratoNegocio());
			endereco.setNrSequenciaContratoNegocio(o.getNrSequenciaContratoNegocio());
			endereco.setCdClub(o.getCdClub());
			endereco.setCdCpfCnpj(o.getCdCpfCnpj());
			endereco.setCdCnpjContrato(o.getCdCnpjContrato());
			endereco.setCdCpfCnpjDigito(o.getCdCpfCnpjDigito());
			endereco.setDsRazaoSocial(o.getDsRazaoSocial());
			endereco.setCdTipoParticipacaoPessoa(o.getCdTipoParticipacaoPessoa());
			endereco.setDsTipoParticipante(o.getDsTipoParticipante());
			endereco.setCdDescricaoSituacaoParticapante(o.getCdDescricaoSituacaoParticapante());
			endereco.setDsTipoServico(o.getDsTipoServico());
			endereco.setDsModalidadeServico(o.getDsModalidadeServico());
			endereco.setDsOperacaoCatalogo(o.getDsOperacaoCatalogo());
			endereco.setCdFinalidadeEnderecoContrato(o.getCdFinalidadeEnderecoContrato());
			endereco.setCdEspecieEndereco(o.getCdEspecieEndereco());
			endereco.setNrSequenciaEnderecoOpcional(o.getNrSequenciaEnderecoOpcional());
			endereco.setCdUsoEnderecoPessoa(o.getCdUsoEnderecoPessoa());
			listaEndereco.add(endereco);
		}

		return listaEndereco;
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService#incluirEnderecoParticipante(br.com.bradesco.web.pgit.service.business.endereco.bean.IncluirEnderecoParticipanteEntradaDTO)
	 */
	public IncluirEnderecoParticipanteSaidaDTO incluirEnderecoParticipante(IncluirEnderecoParticipanteEntradaDTO entradaDTO) {
		IncluirEnderecoParticipanteRequest request = new IncluirEnderecoParticipanteRequest();
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoServico()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOperacaoProdutoServico()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
		request.setCdPessoaEndereco(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaEndereco()));
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridica()));
		request.setNrSequenciaEndereco(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSequenciaEndereco()));
		request.setCdUsoPostal(PgitUtil.verificaIntegerNulo(entradaDTO.getCdUsoPostal()));
		request.setCdUsoEnderecoPessoa(PgitUtil.verificaIntegerNulo(Integer.parseInt(entradaDTO.getRdoEnderecoPessoa())));
		request.setCdEspeceEndereco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspeceEndereco()));
		request.setCdOperacaoCanalInclusao(PgitUtil.verificaStringNula(entradaDTO.getCdOperacaoCanalInclusao()));
		request.setCdTipoCanalInclusao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoCanalInclusao()));

		IncluirEnderecoParticipanteResponse response = getFactoryAdapter().getIncluirEnderecoParticipantePDCAdapter().invokeProcess(request);

		return new IncluirEnderecoParticipanteSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService#listarHistoricoEnderecoParticipante(br.com.bradesco.web.pgit.service.business.endereco.bean.ListarHistoricoEnderecoParticipanteEntradaDTO)
	 */
	public List<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO> listarHistoricoEnderecoParticipante(ListarHistoricoEnderecoParticipanteEntradaDTO entradaDTO) {
		ListarHistoricoEnderecoParticipanteRequest request = new ListarHistoricoEnderecoParticipanteRequest();
		request.setMaxOcorrencias(40);
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
//		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoServico()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOperacaoProdutoServico()));
		request.setCdClub(PgitUtil.verificaLongNulo(entradaDTO.getCdClub()));
		request.setDtManutencaoInicio(FormatarData.formataDiaMesAnoToPdc(entradaDTO.getDtManutencaoInicio()));
		request.setDtManutencaoFim(FormatarData.formataDiaMesAnoToPdc(entradaDTO.getDtManutencaoFim()));
		request.setCdusuario("");
		ListarHistoricoEnderecoParticipanteResponse response = getFactoryAdapter().getListarHistoricoEnderecoParticipantePDCAdapter().invokeProcess(request);

		List<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO> listaEndereco = new ArrayList<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoenderecoparticipante.response.Ocorrencias o : response.getOcorrencias()) {
			ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO endereco = new ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO();
			endereco.setCdPessoaJuridicaContrato(o.getCdPessoaJuridicaContrato());
			endereco.setCdTipoContratoNegocio(o.getCdTipoContratoNegocio());
			endereco.setNrSequenciaContratoNegocio(o.getNrSequenciaContratoNegocio());
			endereco.setCdClub(o.getCdClub());
			endereco.setCdCpfCnpj(o.getCdCpfCnpj());
			endereco.setCdCnpjContrato(o.getCdCnpjContrato());
			endereco.setCdCpfCnpjDigito(o.getCdCpfCnpjDigito());
			endereco.setCdRazaoSocial(o.getCdRazaoSocial());
			endereco.setCdTipoParticipacaoPessoa(o.getCdTipoParticipacaoPessoa());
			endereco.setDsTipoParticipante(o.getDsTipoParticipante());
			endereco.setCdDescricaoSituacaoParticapante(o.getCdDescricaoSituacaoParticapante());
			endereco.setDsTipoServico(o.getDsTipoServico());
			endereco.setDsModalidadeServico(o.getDsModalidadeServico());
			endereco.setDsOperacaoCatalogo(o.getDsOperacaoCatalogo());
			endereco.setCdFinalidadeEnderecoContrato(o.getCdFinalidadeEnderecoContrato());
			endereco.setHrInclusaoRegistro(o.getHrInclusaoRegistro());
			endereco.setCdEspeceEndereco(o.getCdEspeceEndereco());
			endereco.setNrSequenciaEndereco(o.getNrSequenciaEndereco());
			endereco.setCdUsoEnderecoPessoa(o.getCdUsoEnderecoPessoa());
			endereco.setCdUsuarioManutencao(o.getCdUsuarioManutencao());
			listaEndereco.add(endereco);
		}

		return listaEndereco;
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService#detalharEnderecoParticipante(br.com.bradesco.web.pgit.service.business.endereco.bean.DatalharEnderecoParticipanteEntradaDTO)
	 */
	public DatalharEnderecoParticipanteSaidaDTO detalharEnderecoParticipante(DatalharEnderecoParticipanteEntradaDTO entradaDTO) {
		DetalharEnderecoParticipanteRequest request = new DetalharEnderecoParticipanteRequest();
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entradaDTO.getCdCorpoCpfCnpj()));
		request.setCdCnpjContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCnpjContrato()));
		request.setCdCpfCnpjDigito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCpfCnpjDigito()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
		request.setCdFinalidadeEnderecoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFinalidadeEnderecoContrato()));
		request.setCdEspeceEndereco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspeceEndereco()));
		request.setNrSequenciaEnderecoOpcional(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSequenciaEnderecoOpcional()));
		request.setCdTipoPessoa(PgitUtil.verificaStringNula(entradaDTO.getCdTipoPessoa()));

		DetalharEnderecoParticipanteResponse response = getFactoryAdapter().getDetalharEnderecoParticipantePDCAdapter().invokeProcess(request);

		DatalharEnderecoParticipanteSaidaDTO detalhe = new DatalharEnderecoParticipanteSaidaDTO();
		detalhe.setCodMensagem(response.getCodMensagem());
		detalhe.setMensagem(response.getMensagem());
		detalhe.setCdEspeceEndereco(response.getCdEspeceEndereco());
		detalhe.setDsEspecieEndereco(response.getDsEspecieEndereco());
		detalhe.setCdNomeRazao(response.getCdNomeRazao());
		detalhe.setDsLogradouroPagador(response.getDsLogradouroPagador());
		detalhe.setDsNumeroLogradouroPagador(response.getDsNumeroLogradouroPagador());
		detalhe.setDsComplementoLogradouroPagador(response.getDsComplementoLogradouroPagador());
		detalhe.setDsBairroClientePagador(response.getDsBairroClientePagador());
		detalhe.setDsMunicipioClientePagador(response.getDsMunicipioClientePagador());
		detalhe.setCdSiglaUfPagador(response.getCdSiglaUfPagador());
		detalhe.setDsPais(response.getDsPais());
		detalhe.setCdCepPagador(response.getCdCepPagador());
		detalhe.setCdCepComplementoPagador(response.getCdCepComplementoPagador());
		detalhe.setDsCpfCnpjRecebedor(response.getDsCpfCnpjRecebedor());
		detalhe.setDsFilialCnpjRecebedor(response.getDsFilialCnpjRecebedor());
		detalhe.setDsControleCpfRecebedor(response.getDsControleCpfRecebedor());
		detalhe.setDsNomeRazao(response.getDsNomeRazao());
		detalhe.setCdEnderecoEletronico(response.getCdEnderecoEletronico());
		detalhe.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		detalhe.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		detalhe.setHrInclusaoRegistro(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
		detalhe.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		detalhe.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		detalhe.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		detalhe.setCdUsuarioManutencaoexterno(response.getCdUsuarioManutencaoexterno());
		detalhe.setHrManutencaoRegistro(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
		detalhe.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		detalhe.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		detalhe.setCdTipo(response.getCdTipo());
		detalhe.setCdUsoPostal(response.getCdUsoPostal());
		detalhe.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		detalhe.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());

		return detalhe;
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService#listarEnderecoEmail(br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoEmailEntradaDTO)
	 */
	public List<ListarEnderecoEmailOcorrenciasSaidaDTO> listarEnderecoEmail(ListarEnderecoEmailEntradaDTO entradaDTO) {
		ListarEnderecoEmailRequest request = new ListarEnderecoEmailRequest();
		request.setNrOcorrencias(50);
		request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entradaDTO.getCdCorpoCpfCnpj()));
		request.setCdCnpjContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCnpjContrato()));
		request.setCdCpfCnpjDigito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCpfCnpjDigito()));
		request.setCdTipoPessoa(PgitUtil.verificaStringNula(entradaDTO.getCdTipoPessoa()));
		request.setCdClub(PgitUtil.verificaLongNulo(entradaDTO.getCdClub()));
		request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
		request.setCdEspecieEndereco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspecieEndereco()));

		ListarEnderecoEmailResponse response = getFactoryAdapter().getListarEnderecoEmailPDCAdapter().invokeProcess(request);

		List<ListarEnderecoEmailOcorrenciasSaidaDTO> lista = new ArrayList<ListarEnderecoEmailOcorrenciasSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.response.Ocorrencias o : response.getOcorrencias()) {
			ListarEnderecoEmailOcorrenciasSaidaDTO endereco = new ListarEnderecoEmailOcorrenciasSaidaDTO();
			endereco.setCdCpfCnpjRecebedor(o.getCdCpfCnpjRecebedor());
			endereco.setCdFilialCnpjRecebedor(o.getCdFilialCnpjRecebedor());
			endereco.setCdControleCpfRecebedor(o.getCdControleCpfRecebedor());
			endereco.setCdNomeRazao(o.getCdNomeRazao());
			endereco.setDsLogradouroPagador(o.getDsLogradouroPagador());
			endereco.setDsNumeroLogradouroPagador(o.getDsNumeroLogradouroPagador());
			endereco.setDsComplementoLogradouroPagador(o.getDsComplementoLogradouroPagador());
			endereco.setDsBairroClientePagador(o.getDsBairroClientePagador());
			endereco.setDsMunicipioClientePagador(o.getDsMunicipioClientePagador());
			endereco.setCdSiglaUfPagador(o.getCdSiglaUfPagador());
			endereco.setDsPais(o.getDsPais());
			endereco.setCdCepPagador(o.getCdCepPagador());
			endereco.setCdCepComplementoPagador(o.getCdCepComplementoPagador());
			endereco.setDsCpfCnpjRecebedor(o.getDsCpfCnpjRecebedor());
			endereco.setDsFilialCnpjRecebedor(o.getDsFilialCnpjRecebedor());
			endereco.setDsControleCpfRecebedor(o.getDsControleCpfRecebedor());
			endereco.setDsNomeRazao(o.getDsNomeRazao());
			endereco.setCdEnderecoEletronico(o.getCdEnderecoEletronico());
			endereco.setNrSequenciaEndereco(o.getNrSequenciaEndereco());
			endereco.setCdClub(o.getCdClub());
			endereco.setCdTipo(o.getCdTipo());

			lista.add(endereco);
		}

		return lista;
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService#alterarEnderecoParticipante(br.com.bradesco.web.pgit.service.business.endereco.bean.SubstituirVincEnderecoParticipanteEntradaDTO)
	 */
	public SubstituirVincEnderecoParticipanteSaidaDTO alterarEnderecoParticipante(SubstituirVincEnderecoParticipanteEntradaDTO entradaDTO) {
		SubstituirVincEnderecoParticipanteRequest request = new SubstituirVincEnderecoParticipanteRequest();
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
		request.setCdFinalidadeEnderecoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFinalidadeEnderecoContrato()));
		request.setCdPessoaEndereco(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaEndereco()));
		request.setCdPessoaJuridicaEndereco(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaEndereco()));
		request.setNrSequenciaEndereco(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSequenciaEndereco()));
		request.setCdUsoEnderecoPessoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdUsoEnderecoPessoa()));
		request.setCdEspeceEndereco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspecieEndereco()));
		request.setCdUsoPostal(PgitUtil.verificaIntegerNulo(entradaDTO.getCdUsoPostal()));

		SubstituirVincEnderecoParticipanteResponse response = getFactoryAdapter().getSubstituirVincEnderecoParticipantePDCAdapter().invokeProcess(request);

		return new SubstituirVincEnderecoParticipanteSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService#detalharHistoricoEnderecoParticipante(br.com.bradesco.web.pgit.service.business.endereco.bean.DetalharHistoricoEnderecoParticipanteEntradaDTO)
	 */
	public DetalharHistoricoEnderecoParticipanteSaidaDTO detalharHistoricoEnderecoParticipante(DetalharHistoricoEnderecoParticipanteEntradaDTO entradaDTO) {
		DetalharHistoricoEnderecoParticipanteRequest request = new DetalharHistoricoEnderecoParticipanteRequest();

		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entradaDTO.getCdCorpoCpfCnpj()));
		request.setCdCnpjContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCnpjContrato()));
		request.setCdCpfCnpjDigito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCpfCnpjDigito()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
		request.setCdFinalidadeEnderecoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFinalidadeEnderecoContrato()));
		request.setHrInclusaoRegistroHist(PgitUtil.verificaStringNula(entradaDTO.getHrInclusaoRegistroHist()));
		request.setCdEspeceEndereco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspeceEndereco()));
		request.setNrSequenciaEnderecoOpcional(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSequenciaEnderecoOpcional()));
		request.setCdTipoPessoa(PgitUtil.verificaStringNula(entradaDTO.getCdTipoPessoa()));

		DetalharHistoricoEnderecoParticipanteResponse response = getFactoryAdapter().getDetalharHistoricoEnderecoParticipantePDCAdapter().invokeProcess(request);

		DetalharHistoricoEnderecoParticipanteSaidaDTO detalhe = new DetalharHistoricoEnderecoParticipanteSaidaDTO();
		detalhe.setCodMensagem(response.getCodMensagem());
		detalhe.setMensagem(response.getMensagem());
		detalhe.setCdEspeceEndereco(response.getCdEspeceEndereco());
		detalhe.setDsEspecieEndereco(response.getDsEspecieEndereco());
		detalhe.setCdNomeRazao(response.getCdNomeRazao());
		detalhe.setDsLogradouroPagador(response.getDsLogradouroPagador());
		detalhe.setDsNumeroLogradouroPagador(response.getDsNumeroLogradouroPagador());
		detalhe.setDsComplementoLogradouroPagador(response.getDsComplementoLogradouroPagador());
		detalhe.setDsBairroClientePagador(response.getDsBairroClientePagador());
		detalhe.setDsMunicipioClientePagador(response.getDsMunicipioClientePagador());
		detalhe.setCdSiglaUfPagador(response.getCdSiglaUfPagador());
		detalhe.setDsPais(response.getDsPais());
		detalhe.setCdCepPagador(response.getCdCepPagador());
		detalhe.setCdCepComplementoPagador(response.getCdCepComplementoPagador());
		detalhe.setDsCpfCnpjRecebedor(response.getDsCpfCnpjRecebedor());
		detalhe.setDsFilialCnpjRecebedor(response.getDsFilialCnpjRecebedor());
		detalhe.setDsControleCpfRecebedor(response.getDsControleCpfRecebedor());
		detalhe.setDsNomeRazao(response.getDsNomeRazao());
		detalhe.setCdEnderecoEletronico(response.getCdEnderecoEletronico());
		detalhe.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		detalhe.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		detalhe.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		detalhe.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
		detalhe.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		detalhe.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		detalhe.setCdUsuarioManutencaoexterno(response.getCdUsuarioManutencaoexterno());
		detalhe.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		detalhe.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		detalhe.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		detalhe.setCdTipo(response.getCdTipo());
		detalhe.setCdUsoPostal(response.getCdUsoPostal());
		detalhe.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		detalhe.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());

		return detalhe;
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService#excluirEnderecoParticipante(br.com.bradesco.web.pgit.service.business.endereco.bean.ExcluirEnderecoParticipanteEntradaDTO)
	 */
	public ExcluirEnderecoParticipanteSaidaDTO excluirEnderecoParticipante(ExcluirEnderecoParticipanteEntradaDTO entradaDTO) {
		ExcluirEnderecoParticipanteRequest request = new ExcluirEnderecoParticipanteRequest();
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoParticipacaoPessoa()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
		request.setCdFinalidadeEnderecoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFinalidadeEnderecoContrato()));

		ExcluirEnderecoParticipanteResponse response = getFactoryAdapter().getExcluirEnderecoParticipantePDCAdapter().invokeProcess(request);

		return new ExcluirEnderecoParticipanteSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

}

