/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasModRecadastro
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasModRecadastro {

	/** Atributo cdModalidadeRecadastro. */
	private String cdModalidadeRecadastro;

	/**
	 * Ocorrencias mod recadastro.
	 */
	public OcorrenciasModRecadastro() {
		super();
	}

	/**
	 * Ocorrencias mod recadastro.
	 *
	 * @param cdModalidadeRecadastro the cd modalidade recadastro
	 */
	public OcorrenciasModRecadastro(String cdModalidadeRecadastro) {
		super();
		this.cdModalidadeRecadastro = cdModalidadeRecadastro;
	}

	/**
	 * Get: cdModalidadeRecadastro.
	 *
	 * @return cdModalidadeRecadastro
	 */
	public String getCdModalidadeRecadastro() {
		return cdModalidadeRecadastro;
	}

	/**
	 * Set: cdModalidadeRecadastro.
	 *
	 * @param cdModalidadeRecadastro the cd modalidade recadastro
	 */
	public void setCdModalidadeRecadastro(String cdModalidadeRecadastro) {
		this.cdModalidadeRecadastro = cdModalidadeRecadastro;
	}
}