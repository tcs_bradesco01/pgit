/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.msgcompsalarial;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.AlterarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.AlterarMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.DetalharMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.DetalharMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ExcluirMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ExcluirMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.IncluirMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.IncluirMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterMensagemComprovanteSalarial
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IMsgCompSalarialService {

    /**
     * M�todo de exemplo.
     */
    void sampleManterMensagemComprovanteSalarial();
    
    /**
     * Listar msg comprovante salrl.
     *
     * @param listarMsgComprovanteSalrlEntradaDTO the listar msg comprovante salrl entrada dto
     * @return the list< listar msg comprovante salrl saida dt o>
     */
    List<ListarMsgComprovanteSalrlSaidaDTO> listarMsgComprovanteSalrl(ListarMsgComprovanteSalrlEntradaDTO listarMsgComprovanteSalrlEntradaDTO);
    
    /**
     * Incluir msg comprovante salrl.
     *
     * @param incluirMsgComprovanteSalrlEntradaDTO the incluir msg comprovante salrl entrada dto
     * @return the incluir msg comprovante salrl saida dto
     */
    IncluirMsgComprovanteSalrlSaidaDTO incluirMsgComprovanteSalrl(IncluirMsgComprovanteSalrlEntradaDTO incluirMsgComprovanteSalrlEntradaDTO);
    
    /**
     * Detalhar msg comprovante salrl.
     *
     * @param detalharMsgComprovanteSalrlEntradaDTO the detalhar msg comprovante salrl entrada dto
     * @return the detalhar msg comprovante salrl saida dto
     */
    DetalharMsgComprovanteSalrlSaidaDTO detalharMsgComprovanteSalrl(DetalharMsgComprovanteSalrlEntradaDTO detalharMsgComprovanteSalrlEntradaDTO);
    
    /**
     * Alterar msg comprovante salrl.
     *
     * @param alterarMsgComprovanteSalrlEntradaDTO the alterar msg comprovante salrl entrada dto
     * @return the alterar msg comprovante salrl saida dto
     */
    AlterarMsgComprovanteSalrlSaidaDTO alterarMsgComprovanteSalrl(AlterarMsgComprovanteSalrlEntradaDTO alterarMsgComprovanteSalrlEntradaDTO);
    
    /**
     * Excluir funcoes alcada.
     *
     * @param excluirMsgComprovanteSalrlEntradaDTO the excluir msg comprovante salrl entrada dto
     * @return the excluir msg comprovante salrl saida dto
     */
    ExcluirMsgComprovanteSalrlSaidaDTO excluirFuncoesAlcada(ExcluirMsgComprovanteSalrlEntradaDTO excluirMsgComprovanteSalrlEntradaDTO);

}

