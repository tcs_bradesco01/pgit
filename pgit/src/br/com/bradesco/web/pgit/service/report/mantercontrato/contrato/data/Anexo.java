/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

/**
 * Nome: Anexo
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class Anexo {
	
	/** Atributo cdIndicadorServicoFornecedor. */
	private String cdIndicadorServicoFornecedor = null;
	
    /** Atributo anexoFornecedores. */
    private AnexoFornecedores anexoFornecedores = null;

    /** Atributo cdIndicadorServicoTributos. */
    private String cdIndicadorServicoTributos = null;
    
    /** Atributo anexoTributosContas. */
    private AnexoTributosContas anexoTributosContas = null;
    
    /** Atributo cdIndicadorServicoSalario. */
    private String cdIndicadorServicoSalario = null;

    /** Atributo anexoSalarios. */
    private AnexoSalarios anexoSalarios = null;

    /** Atributo cdIndicadorServicoComprovanteSalarial. */
    private String cdIndicadorServicoComprovanteSalarial = null;
    
    /** Atributo anexoComprovanteSalarial. */
    private AnexoComprovanteSalarial anexoComprovanteSalarial = null;
    
    /**
     * Instancia um novo Anexo
     *
     * @see
     */
    public Anexo() {
        super();
    }

    /**
     * Nome: getAnexoFornecedores.
     *
     * @return anexoFornecedores
     */
    public AnexoFornecedores getAnexoFornecedores() {
        return anexoFornecedores;
    }

    /**
     * Nome: setAnexoFornecedores.
     *
     * @param anexoFornecedores the anexo fornecedores
     */
    public void setAnexoFornecedores(AnexoFornecedores anexoFornecedores) {
        this.anexoFornecedores = anexoFornecedores;
    }

    /**
     * Nome: getAnexoTributosContas.
     *
     * @return anexoTributosContas
     */
    public AnexoTributosContas getAnexoTributosContas() {
        return anexoTributosContas;
    }

    /**
     * Nome: setAnexoTributosContas.
     *
     * @param anexoTributosContas the anexo tributos contas
     */
    public void setAnexoTributosContas(AnexoTributosContas anexoTributosContas) {
        this.anexoTributosContas = anexoTributosContas;
    }

    /**
     * Nome: getAnexoSalarios.
     *
     * @return anexoSalarios
     */
    public AnexoSalarios getAnexoSalarios() {
        return anexoSalarios;
    }

    /**
     * Nome: setAnexoSalarios.
     *
     * @param anexoSalarios the anexo salarios
     */
    public void setAnexoSalarios(AnexoSalarios anexoSalarios) {
        this.anexoSalarios = anexoSalarios;
    }

    /**
     * Nome: getAnexoComprovanteSalarial.
     *
     * @return anexoComprovanteSalarial
     */
    public AnexoComprovanteSalarial getAnexoComprovanteSalarial() {
        return anexoComprovanteSalarial;
    }

    /**
     * Nome: setAnexoComprovanteSalarial.
     *
     * @param anexoComprovanteSalarial the anexo comprovante salarial
     */
    public void setAnexoComprovanteSalarial(AnexoComprovanteSalarial anexoComprovanteSalarial) {
        this.anexoComprovanteSalarial = anexoComprovanteSalarial;
    }

	/**
	 * @param cdIndicadorServicoFornecedor the cdIndicadorServicoFornecedor to set
	 */
	public void setCdIndicadorServicoFornecedor(
			String cdIndicadorServicoFornecedor) {
		this.cdIndicadorServicoFornecedor = cdIndicadorServicoFornecedor;
	}

	/**
	 * @return the cdIndicadorServicoFornecedor
	 */
	public String getCdIndicadorServicoFornecedor() {
		return cdIndicadorServicoFornecedor;
	}

	/**
	 * @param cdIndicadorServicoTributos the cdIndicadorServicoTributos to set
	 */
	public void setCdIndicadorServicoTributos(String cdIndicadorServicoTributos) {
		this.cdIndicadorServicoTributos = cdIndicadorServicoTributos;
	}

	/**
	 * @return the cdIndicadorServicoTributos
	 */
	public String getCdIndicadorServicoTributos() {
		return cdIndicadorServicoTributos;
	}

	/**
	 * @param cdIndicadorServicoSalario the cdIndicadorServicoSalario to set
	 */
	public void setCdIndicadorServicoSalario(String cdIndicadorServicoSalario) {
		this.cdIndicadorServicoSalario = cdIndicadorServicoSalario;
	}

	/**
	 * @return the cdIndicadorServicoSalario
	 */
	public String getCdIndicadorServicoSalario() {
		return cdIndicadorServicoSalario;
	}

	/**
	 * @param cdIndicadorServicoComprovanteSalarial the cdIndicadorServicoComprovanteSalarial to set
	 */
	public void setCdIndicadorServicoComprovanteSalarial(
			String cdIndicadorServicoComprovanteSalarial) {
		this.cdIndicadorServicoComprovanteSalarial = cdIndicadorServicoComprovanteSalarial;
	}

	/**
	 * @return the cdIndicadorServicoComprovanteSalarial
	 */
	public String getCdIndicadorServicoComprovanteSalarial() {
		return cdIndicadorServicoComprovanteSalarial;
	}


}
