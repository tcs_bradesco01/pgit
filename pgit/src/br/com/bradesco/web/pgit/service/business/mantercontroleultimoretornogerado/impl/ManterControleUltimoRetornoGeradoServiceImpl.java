/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.IManterControleUltimoRetornoGeradoService;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.IManterControleUltimoRetornoGeradoServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.AlterarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.AlterarUltimoNumeroRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ConsultarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ConsultarUltimoNumeroRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ListarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ListarUltimoNumeroRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarultimonumeroretorno.request.AlterarUltimoNumeroRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarultimonumeroretorno.response.AlterarUltimoNumeroRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroretorno.request.ConsultarUltimoNumeroRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroretorno.response.ConsultarUltimoNumeroRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimonumeroretorno.request.ListarUltimoNumeroRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimonumeroretorno.response.ListarUltimoNumeroRetornoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterControleUltimoRetornoGerado
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterControleUltimoRetornoGeradoServiceImpl implements IManterControleUltimoRetornoGeradoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    /**
     * Construtor.
     */
    public ManterControleUltimoRetornoGeradoServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.IManterControleUltimoRetornoGeradoService#alterarUltimoNumeroRetorno(br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.AlterarUltimoNumeroRetornoEntradaDTO)
     */
    public AlterarUltimoNumeroRetornoSaidaDTO alterarUltimoNumeroRetorno (AlterarUltimoNumeroRetornoEntradaDTO entrada){
    	AlterarUltimoNumeroRetornoRequest request = new AlterarUltimoNumeroRetornoRequest();
    	AlterarUltimoNumeroRetornoResponse response = new AlterarUltimoNumeroRetornoResponse();
    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
    	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
    	request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
    	request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLoteLayout()));
    	request.setNrUltimoArquivo(PgitUtil.verificaLongNulo(entrada.getNrUltimoArquivo()));
    	request.setNrMaximoArquivo(PgitUtil.verificaLongNulo(entrada.getNrMaximoArquivo()));
    	request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));
    	
    	response = getFactoryAdapter().getAlterarUltimoNumeroRetornoPDCAdapter().invokeProcess(request);
    	
    	AlterarUltimoNumeroRetornoSaidaDTO saida = new AlterarUltimoNumeroRetornoSaidaDTO();
    	
    	saida.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
    	saida.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
    	
		return saida;
    	
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.IManterControleUltimoRetornoGeradoService#consultarUltimoNumeroRetorno(br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ConsultarUltimoNumeroRetornoEntradaDTO)
     */
    public ConsultarUltimoNumeroRetornoSaidaDTO consultarUltimoNumeroRetorno (ConsultarUltimoNumeroRetornoEntradaDTO entrada){
    	
    	ConsultarUltimoNumeroRetornoRequest request = new ConsultarUltimoNumeroRetornoRequest();
    	ConsultarUltimoNumeroRetornoResponse response = new ConsultarUltimoNumeroRetornoResponse();
    	
    	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
    	request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
    	request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLoteLayout()));
    	request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));
    	
    	response = getFactoryAdapter().getConsultarUltimoNumeroRetornoPDCAdapter().invokeProcess(request);
    	
    	ConsultarUltimoNumeroRetornoSaidaDTO saida = new ConsultarUltimoNumeroRetornoSaidaDTO();
    	
    	saida.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
    	saida.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
    	saida.setDsNivelControle(PgitUtil.verificaStringNula(response.getDsNivelControle()));
    	saida.setDsTipoControle(PgitUtil.verificaStringNula(response.getDsTipoControle()));
    	saida.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(response.getCdTipoLayoutArquivo()));
    	saida.setDsTipoLayoutArquivo(PgitUtil.verificaStringNula(response.getDsTipoLayoutArquivo()));
    	saida.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(response.getCdTipoLoteLayout()));
    	saida.setDsTipoLoteLayout(PgitUtil.verificaStringNula(response.getDsTipoLoteLayout()));
    	saida.setNrUltimoArquivoRemessa(PgitUtil.verificaLongNulo(response.getNrUltimoNumeroArquivoRemessa()));
    	saida.setNrMaximoArquivoRemessa(PgitUtil.verificaLongNulo(response.getNrMaximoArquivoRemessa()));
    	saida.setCdCorpoCpfCnpj(response.getCdCorpoCpfCnpj());

    	return saida;   	
    	
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.IManterControleUltimoRetornoGeradoService#listarUltimoNumeroRetorno(br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ListarUltimoNumeroRetornoEntradaDTO)
     */
    public List<ListarUltimoNumeroRetornoSaidaDTO> listarUltimoNumeroRetorno (ListarUltimoNumeroRetornoEntradaDTO entrada){
    	
    	List<ListarUltimoNumeroRetornoSaidaDTO> listaSaida = new ArrayList<ListarUltimoNumeroRetornoSaidaDTO>();
    	
    	ListarUltimoNumeroRetornoRequest request = new ListarUltimoNumeroRetornoRequest();
    	ListarUltimoNumeroRetornoResponse response = new ListarUltimoNumeroRetornoResponse();
    	
    	request.setNumeroOcorrencias(IManterControleUltimoRetornoGeradoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
    	request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLoteLayout()));
    	request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada.getCdPerfilTrocaArquivo()));
    	
    	response = getFactoryAdapter().getListarUltimoNumeroRetornoPDCAdapter().invokeProcess(request);
    	
    	ListarUltimoNumeroRetornoSaidaDTO saida;
    	
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		saida = new ListarUltimoNumeroRetornoSaidaDTO();
    		saida.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
    		saida.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));
    		
    		saida.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdTipoLayoutArquivo()));    		
    		saida.setDsTipoLayoutArquivo(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsTipoLayoutArquivo()));
    		saida.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(response.getOcorrencias(i).getCdTipoLoteLayout()));
    		saida.setDsTipoLoteLayout(PgitUtil.verificaStringNula(response.getOcorrencias(i).getDsTipoLoteLayout()));
    		saida.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getCdPerfilTrocaArquivo()));
    		saida.setNrUltimoArquivo(PgitUtil.verificaLongNulo(response.getOcorrencias(i).getNrUltimoArquivo()));
    		saida.setCdClub(response.getOcorrencias(i).getCdClub());
    		saida.setCdCorpoCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
    		
    		listaSaida.add(saida);
    	}
    	
    	return listaSaida;
    }
}