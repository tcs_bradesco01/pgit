/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdIndicadorLayoutProprio
     */
    private int _cdIndicadorLayoutProprio = 0;

    /**
     * keeps track of state for field: _cdIndicadorLayoutProprio
     */
    private boolean _has_cdIndicadorLayoutProprio;

    /**
     * Field _cdApliFormat
     */
    private long _cdApliFormat = 0;

    /**
     * keeps track of state for field: _cdApliFormat
     */
    private boolean _has_cdApliFormat;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdApliFormat
     * 
     */
    public void deleteCdApliFormat()
    {
        this._has_cdApliFormat= false;
    } //-- void deleteCdApliFormat() 

    /**
     * Method deleteCdIndicadorLayoutProprio
     * 
     */
    public void deleteCdIndicadorLayoutProprio()
    {
        this._has_cdIndicadorLayoutProprio= false;
    } //-- void deleteCdIndicadorLayoutProprio() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdApliFormat'.
     * 
     * @return long
     * @return the value of field 'cdApliFormat'.
     */
    public long getCdApliFormat()
    {
        return this._cdApliFormat;
    } //-- long getCdApliFormat() 

    /**
     * Returns the value of field 'cdIndicadorLayoutProprio'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorLayoutProprio'.
     */
    public int getCdIndicadorLayoutProprio()
    {
        return this._cdIndicadorLayoutProprio;
    } //-- int getCdIndicadorLayoutProprio() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdApliFormat
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdApliFormat()
    {
        return this._has_cdApliFormat;
    } //-- boolean hasCdApliFormat() 

    /**
     * Method hasCdIndicadorLayoutProprio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorLayoutProprio()
    {
        return this._has_cdIndicadorLayoutProprio;
    } //-- boolean hasCdIndicadorLayoutProprio() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdApliFormat'.
     * 
     * @param cdApliFormat the value of field 'cdApliFormat'.
     */
    public void setCdApliFormat(long cdApliFormat)
    {
        this._cdApliFormat = cdApliFormat;
        this._has_cdApliFormat = true;
    } //-- void setCdApliFormat(long) 

    /**
     * Sets the value of field 'cdIndicadorLayoutProprio'.
     * 
     * @param cdIndicadorLayoutProprio the value of field
     * 'cdIndicadorLayoutProprio'.
     */
    public void setCdIndicadorLayoutProprio(int cdIndicadorLayoutProprio)
    {
        this._cdIndicadorLayoutProprio = cdIndicadorLayoutProprio;
        this._has_cdIndicadorLayoutProprio = true;
    } //-- void setCdIndicadorLayoutProprio(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.request.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
