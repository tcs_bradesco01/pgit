/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharBloqueioRastreamentoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharBloqueioRastreamentoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _nmControleRastreabilidadeTitulo
     */
    private int _nmControleRastreabilidadeTitulo = 0;

    /**
     * keeps track of state for field:
     * _nmControleRastreabilidadeTitulo
     */
    private boolean _has_nmControleRastreabilidadeTitulo;

    /**
     * Field _cdCpfCnpjBloqueio
     */
    private long _cdCpfCnpjBloqueio = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjBloqueio
     */
    private boolean _has_cdCpfCnpjBloqueio;

    /**
     * Field _cdFilialCnpjBloqueio
     */
    private int _cdFilialCnpjBloqueio = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjBloqueio
     */
    private boolean _has_cdFilialCnpjBloqueio;

    /**
     * Field _cdControleCnpjCpf
     */
    private int _cdControleCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdControleCnpjCpf
     */
    private boolean _has_cdControleCnpjCpf;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharBloqueioRastreamentoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.response.DetalharBloqueioRastreamentoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdControleCnpjCpf
     * 
     */
    public void deleteCdControleCnpjCpf()
    {
        this._has_cdControleCnpjCpf= false;
    } //-- void deleteCdControleCnpjCpf() 

    /**
     * Method deleteCdCpfCnpjBloqueio
     * 
     */
    public void deleteCdCpfCnpjBloqueio()
    {
        this._has_cdCpfCnpjBloqueio= false;
    } //-- void deleteCdCpfCnpjBloqueio() 

    /**
     * Method deleteCdFilialCnpjBloqueio
     * 
     */
    public void deleteCdFilialCnpjBloqueio()
    {
        this._has_cdFilialCnpjBloqueio= false;
    } //-- void deleteCdFilialCnpjBloqueio() 

    /**
     * Method deleteNmControleRastreabilidadeTitulo
     * 
     */
    public void deleteNmControleRastreabilidadeTitulo()
    {
        this._has_nmControleRastreabilidadeTitulo= false;
    } //-- void deleteNmControleRastreabilidadeTitulo() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdControleCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjCpf'.
     */
    public int getCdControleCnpjCpf()
    {
        return this._cdControleCnpjCpf;
    } //-- int getCdControleCnpjCpf() 

    /**
     * Returns the value of field 'cdCpfCnpjBloqueio'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjBloqueio'.
     */
    public long getCdCpfCnpjBloqueio()
    {
        return this._cdCpfCnpjBloqueio;
    } //-- long getCdCpfCnpjBloqueio() 

    /**
     * Returns the value of field 'cdFilialCnpjBloqueio'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjBloqueio'.
     */
    public int getCdFilialCnpjBloqueio()
    {
        return this._cdFilialCnpjBloqueio;
    } //-- int getCdFilialCnpjBloqueio() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field
     * 'nmControleRastreabilidadeTitulo'.
     * 
     * @return int
     * @return the value of field 'nmControleRastreabilidadeTitulo'.
     */
    public int getNmControleRastreabilidadeTitulo()
    {
        return this._nmControleRastreabilidadeTitulo;
    } //-- int getNmControleRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdControleCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjCpf()
    {
        return this._has_cdControleCnpjCpf;
    } //-- boolean hasCdControleCnpjCpf() 

    /**
     * Method hasCdCpfCnpjBloqueio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjBloqueio()
    {
        return this._has_cdCpfCnpjBloqueio;
    } //-- boolean hasCdCpfCnpjBloqueio() 

    /**
     * Method hasCdFilialCnpjBloqueio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjBloqueio()
    {
        return this._has_cdFilialCnpjBloqueio;
    } //-- boolean hasCdFilialCnpjBloqueio() 

    /**
     * Method hasNmControleRastreabilidadeTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNmControleRastreabilidadeTitulo()
    {
        return this._has_nmControleRastreabilidadeTitulo;
    } //-- boolean hasNmControleRastreabilidadeTitulo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdControleCnpjCpf'.
     * 
     * @param cdControleCnpjCpf the value of field
     * 'cdControleCnpjCpf'.
     */
    public void setCdControleCnpjCpf(int cdControleCnpjCpf)
    {
        this._cdControleCnpjCpf = cdControleCnpjCpf;
        this._has_cdControleCnpjCpf = true;
    } //-- void setCdControleCnpjCpf(int) 

    /**
     * Sets the value of field 'cdCpfCnpjBloqueio'.
     * 
     * @param cdCpfCnpjBloqueio the value of field
     * 'cdCpfCnpjBloqueio'.
     */
    public void setCdCpfCnpjBloqueio(long cdCpfCnpjBloqueio)
    {
        this._cdCpfCnpjBloqueio = cdCpfCnpjBloqueio;
        this._has_cdCpfCnpjBloqueio = true;
    } //-- void setCdCpfCnpjBloqueio(long) 

    /**
     * Sets the value of field 'cdFilialCnpjBloqueio'.
     * 
     * @param cdFilialCnpjBloqueio the value of field
     * 'cdFilialCnpjBloqueio'.
     */
    public void setCdFilialCnpjBloqueio(int cdFilialCnpjBloqueio)
    {
        this._cdFilialCnpjBloqueio = cdFilialCnpjBloqueio;
        this._has_cdFilialCnpjBloqueio = true;
    } //-- void setCdFilialCnpjBloqueio(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmControleRastreabilidadeTitulo'.
     * 
     * @param nmControleRastreabilidadeTitulo the value of field
     * 'nmControleRastreabilidadeTitulo'.
     */
    public void setNmControleRastreabilidadeTitulo(int nmControleRastreabilidadeTitulo)
    {
        this._nmControleRastreabilidadeTitulo = nmControleRastreabilidadeTitulo;
        this._has_nmControleRastreabilidadeTitulo = true;
    } //-- void setNmControleRastreabilidadeTitulo(int) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharBloqueioRastreamentoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.response.DetalharBloqueioRastreamentoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.response.DetalharBloqueioRastreamentoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.response.DetalharBloqueioRastreamentoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.response.DetalharBloqueioRastreamentoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
