package br.com.bradesco.web.pgit.service.business.contacomplementar.bean;

import java.util.List;

public class ListarContaComplementarSaida {
	
    /** The cod mensagem. */
    private String codMensagem = null;

    /** The mensagem. */
    private String mensagem = null;

    /** The nrLinhas. */
    private Integer nrLinhas = null;
    
    /** The ocorrencias. */
    private List<ListarContaComplementarOcorrenciasSaida> ocorrencias = null;
    
    /**
     * Instancia um novo ListarContaComplementarSaida
     *
     * @see
     */
    public ListarContaComplementarSaida() {
        super();
    }    

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public Integer getNrLinhas() {
		return nrLinhas;
	}

	public void setNrLinhas(Integer nrLinhas) {
		this.nrLinhas = nrLinhas;
	}

	public List<ListarContaComplementarOcorrenciasSaida> getOcorrencias() {
		return ocorrencias;
	}

	public void setOcorrencias(
			List<ListarContaComplementarOcorrenciasSaida> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
}
