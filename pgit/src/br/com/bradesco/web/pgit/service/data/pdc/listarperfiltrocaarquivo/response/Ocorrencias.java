/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCpfCnpjPessoa
     */
    private java.lang.String _cdCpfCnpjPessoa;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _cdPerfilTrocaArquivo
     */
    private long _cdPerfilTrocaArquivo = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArquivo
     */
    private boolean _has_cdPerfilTrocaArquivo;

    /**
     * Field _dsSituacaoPerfil
     */
    private java.lang.String _dsSituacaoPerfil;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPerfilTrocaArquivo
     * 
     */
    public void deleteCdPerfilTrocaArquivo()
    {
        this._has_cdPerfilTrocaArquivo= false;
    } //-- void deleteCdPerfilTrocaArquivo() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Returns the value of field 'cdCpfCnpjPessoa'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjPessoa'.
     */
    public java.lang.String getCdCpfCnpjPessoa()
    {
        return this._cdCpfCnpjPessoa;
    } //-- java.lang.String getCdCpfCnpjPessoa() 

    /**
     * Returns the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArquivo'.
     */
    public long getCdPerfilTrocaArquivo()
    {
        return this._cdPerfilTrocaArquivo;
    } //-- long getCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsSituacaoPerfil'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPerfil'.
     */
    public java.lang.String getDsSituacaoPerfil()
    {
        return this._dsSituacaoPerfil;
    } //-- java.lang.String getDsSituacaoPerfil() 

    /**
     * Method hasCdPerfilTrocaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArquivo()
    {
        return this._has_cdPerfilTrocaArquivo;
    } //-- boolean hasCdPerfilTrocaArquivo() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCpfCnpjPessoa'.
     * 
     * @param cdCpfCnpjPessoa the value of field 'cdCpfCnpjPessoa'.
     */
    public void setCdCpfCnpjPessoa(java.lang.String cdCpfCnpjPessoa)
    {
        this._cdCpfCnpjPessoa = cdCpfCnpjPessoa;
    } //-- void setCdCpfCnpjPessoa(java.lang.String) 

    /**
     * Sets the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @param cdPerfilTrocaArquivo the value of field
     * 'cdPerfilTrocaArquivo'.
     */
    public void setCdPerfilTrocaArquivo(long cdPerfilTrocaArquivo)
    {
        this._cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
        this._has_cdPerfilTrocaArquivo = true;
    } //-- void setCdPerfilTrocaArquivo(long) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPerfil'.
     * 
     * @param dsSituacaoPerfil the value of field 'dsSituacaoPerfil'
     */
    public void setDsSituacaoPerfil(java.lang.String dsSituacaoPerfil)
    {
        this._dsSituacaoPerfil = dsSituacaoPerfil;
    } //-- void setDsSituacaoPerfil(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
