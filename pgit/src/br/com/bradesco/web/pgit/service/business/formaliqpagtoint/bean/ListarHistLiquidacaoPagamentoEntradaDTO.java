/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

/**
 * Nome: ListarHistLiquidacaoPagamentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistLiquidacaoPagamentoEntradaDTO {
    
    /** Atributo qtConsultas. */
    private Integer qtConsultas;
    
    /** Atributo cdFormaLiquidacao. */
    private Integer cdFormaLiquidacao;
    
    /** Atributo dtInicioVigencia. */
    private String dtInicioVigencia;
    
    /** Atributo dtFinalVigencia. */
    private String dtFinalVigencia;
    
    /** Atributo hrInclusaoRegistroHistorico. */
    private String hrInclusaoRegistroHistorico;
 
	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}
	
	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}
	
	/**
	 * Get: dtFinalVigencia.
	 *
	 * @return dtFinalVigencia
	 */
	public String getDtFinalVigencia() {
		return dtFinalVigencia;
	}
	
	/**
	 * Set: dtFinalVigencia.
	 *
	 * @param dtFinalVigencia the dt final vigencia
	 */
	public void setDtFinalVigencia(String dtFinalVigencia) {
		this.dtFinalVigencia = dtFinalVigencia;
	}
	
	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}
	
	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}
	
	/**
	 * Get: hrInclusaoRegistroHistorico.
	 *
	 * @return hrInclusaoRegistroHistorico
	 */
	public String getHrInclusaoRegistroHistorico() {
		return hrInclusaoRegistroHistorico;
	}
	
	/**
	 * Set: hrInclusaoRegistroHistorico.
	 *
	 * @param hrInclusaoRegistroHistorico the hr inclusao registro historico
	 */
	public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
		this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
	}
	
	/**
	 * Get: qtConsultas.
	 *
	 * @return qtConsultas
	 */
	public Integer getQtConsultas() {
		return qtConsultas;
	}
	
	/**
	 * Set: qtConsultas.
	 *
	 * @param qtConsultas the qt consultas
	 */
	public void setQtConsultas(Integer qtConsultas) {
		this.qtConsultas = qtConsultas;
	}

	
    
}
