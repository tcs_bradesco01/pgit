/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class OcorrenciasServico2.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasServico2 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdAcaoServico
     */
    private int _cdAcaoServico = 0;

    /**
     * keeps track of state for field: _cdAcaoServico
     */
    private boolean _has_cdAcaoServico;

    /**
     * Field _cdServicoOperacao
     */
    private int _cdServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdServicoOperacao
     */
    private boolean _has_cdServicoOperacao;

    /**
     * Field _dsServicoOperacao
     */
    private java.lang.String _dsServicoOperacao;

    /**
     * Field _cdServicoRelacionado
     */
    private int _cdServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdServicoRelacionado
     */
    private boolean _has_cdServicoRelacionado;

    /**
     * Field _dsServicoRelacionado
     */
    private java.lang.String _dsServicoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasServico2() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoServico
     * 
     */
    public void deleteCdAcaoServico()
    {
        this._has_cdAcaoServico= false;
    } //-- void deleteCdAcaoServico() 

    /**
     * Method deleteCdServicoOperacao
     * 
     */
    public void deleteCdServicoOperacao()
    {
        this._has_cdServicoOperacao= false;
    } //-- void deleteCdServicoOperacao() 

    /**
     * Method deleteCdServicoRelacionado
     * 
     */
    public void deleteCdServicoRelacionado()
    {
        this._has_cdServicoRelacionado= false;
    } //-- void deleteCdServicoRelacionado() 

    /**
     * Returns the value of field 'cdAcaoServico'.
     * 
     * @return int
     * @return the value of field 'cdAcaoServico'.
     */
    public int getCdAcaoServico()
    {
        return this._cdAcaoServico;
    } //-- int getCdAcaoServico() 

    /**
     * Returns the value of field 'cdServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdServicoOperacao'.
     */
    public int getCdServicoOperacao()
    {
        return this._cdServicoOperacao;
    } //-- int getCdServicoOperacao() 

    /**
     * Returns the value of field 'cdServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdServicoRelacionado'.
     */
    public int getCdServicoRelacionado()
    {
        return this._cdServicoRelacionado;
    } //-- int getCdServicoRelacionado() 

    /**
     * Returns the value of field 'dsServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsServicoOperacao'.
     */
    public java.lang.String getDsServicoOperacao()
    {
        return this._dsServicoOperacao;
    } //-- java.lang.String getDsServicoOperacao() 

    /**
     * Returns the value of field 'dsServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsServicoRelacionado'.
     */
    public java.lang.String getDsServicoRelacionado()
    {
        return this._dsServicoRelacionado;
    } //-- java.lang.String getDsServicoRelacionado() 

    /**
     * Method hasCdAcaoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoServico()
    {
        return this._has_cdAcaoServico;
    } //-- boolean hasCdAcaoServico() 

    /**
     * Method hasCdServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoOperacao()
    {
        return this._has_cdServicoOperacao;
    } //-- boolean hasCdServicoOperacao() 

    /**
     * Method hasCdServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoRelacionado()
    {
        return this._has_cdServicoRelacionado;
    } //-- boolean hasCdServicoRelacionado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoServico'.
     * 
     * @param cdAcaoServico the value of field 'cdAcaoServico'.
     */
    public void setCdAcaoServico(int cdAcaoServico)
    {
        this._cdAcaoServico = cdAcaoServico;
        this._has_cdAcaoServico = true;
    } //-- void setCdAcaoServico(int) 

    /**
     * Sets the value of field 'cdServicoOperacao'.
     * 
     * @param cdServicoOperacao the value of field
     * 'cdServicoOperacao'.
     */
    public void setCdServicoOperacao(int cdServicoOperacao)
    {
        this._cdServicoOperacao = cdServicoOperacao;
        this._has_cdServicoOperacao = true;
    } //-- void setCdServicoOperacao(int) 

    /**
     * Sets the value of field 'cdServicoRelacionado'.
     * 
     * @param cdServicoRelacionado the value of field
     * 'cdServicoRelacionado'.
     */
    public void setCdServicoRelacionado(int cdServicoRelacionado)
    {
        this._cdServicoRelacionado = cdServicoRelacionado;
        this._has_cdServicoRelacionado = true;
    } //-- void setCdServicoRelacionado(int) 

    /**
     * Sets the value of field 'dsServicoOperacao'.
     * 
     * @param dsServicoOperacao the value of field
     * 'dsServicoOperacao'.
     */
    public void setDsServicoOperacao(java.lang.String dsServicoOperacao)
    {
        this._dsServicoOperacao = dsServicoOperacao;
    } //-- void setDsServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoRelacionado'.
     * 
     * @param dsServicoRelacionado the value of field
     * 'dsServicoRelacionado'.
     */
    public void setDsServicoRelacionado(java.lang.String dsServicoRelacionado)
    {
        this._dsServicoRelacionado = dsServicoRelacionado;
    } //-- void setDsServicoRelacionado(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasServico2
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasServico2 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
