/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarMotivoBloqueioDesbloqueioRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarMotivoBloqueioDesbloqueioRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdSituacaoParticipanteContrato
     */
    private int _cdSituacaoParticipanteContrato = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoParticipanteContrato
     */
    private boolean _has_cdSituacaoParticipanteContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarMotivoBloqueioDesbloqueioRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.request.ListarMotivoBloqueioDesbloqueioRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoParticipanteContrato
     * 
     */
    public void deleteCdSituacaoParticipanteContrato()
    {
        this._has_cdSituacaoParticipanteContrato= false;
    } //-- void deleteCdSituacaoParticipanteContrato() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdSituacaoParticipanteContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoParticipanteContrato'.
     */
    public int getCdSituacaoParticipanteContrato()
    {
        return this._cdSituacaoParticipanteContrato;
    } //-- int getCdSituacaoParticipanteContrato() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdSituacaoParticipanteContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoParticipanteContrato()
    {
        return this._has_cdSituacaoParticipanteContrato;
    } //-- boolean hasCdSituacaoParticipanteContrato() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoParticipanteContrato'.
     * 
     * @param cdSituacaoParticipanteContrato the value of field
     * 'cdSituacaoParticipanteContrato'.
     */
    public void setCdSituacaoParticipanteContrato(int cdSituacaoParticipanteContrato)
    {
        this._cdSituacaoParticipanteContrato = cdSituacaoParticipanteContrato;
        this._has_cdSituacaoParticipanteContrato = true;
    } //-- void setCdSituacaoParticipanteContrato(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarMotivoBloqueioDesbloqueioRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.request.ListarMotivoBloqueioDesbloqueioRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.request.ListarMotivoBloqueioDesbloqueioRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.request.ListarMotivoBloqueioDesbloqueioRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.request.ListarMotivoBloqueioDesbloqueioRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
