/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ListarHistVinculoContaSalarioDestinoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistVinculoContaSalarioDestinoSaidaDTO {

    /** Atributo hrInclusaoManutencao. */
    private String hrInclusaoManutencao;

    /** Atributo cdPessoaContratoVinculado. */
    private Long cdPessoaContratoVinculado;

    /** Atributo cdTipoVincContrato. */
    private Integer cdTipoVincContrato;

    /** Atributo nrSequenciaVinculacaoContrato. */
    private Long nrSequenciaVinculacaoContrato;

    /** Atributo cdUsuario. */
    private String cdUsuario;

    /** Atributo dtManutencao. */
    private String dtManutencao;

    /** Atributo hrManutencao. */
    private String hrManutencao;

    /** Atributo cdBancoSalario. */
    private Integer cdBancoSalario;

    /** Atributo dsBancoSalario. */
    private String dsBancoSalario;

    /** Atributo cdAgenciaSalario. */
    private Integer cdAgenciaSalario;

    /** Atributo cdDigitoAgenciaSalario. */
    private Integer cdDigitoAgenciaSalario;

    /** Atributo dsAgenciaSalario. */
    private String dsAgenciaSalario;

    /** Atributo cdContaSalario. */
    private Long cdContaSalario;

    /** Atributo cdDigitoContaSalario. */
    private String cdDigitoContaSalario;

    /** Atributo cdBancoDestino. */
    private Integer cdBancoDestino;

    /** Atributo dsBancoDestino. */
    private String dsBancoDestino;

    /** Atributo cdAgenciaDestino. */
    private Integer cdAgenciaDestino;

    /** Atributo dgAgenciaDestino. */
    private Integer dgAgenciaDestino;

    /** Atributo dsAgenciaDestino. */
    private String dsAgenciaDestino;

    /** Atributo cdContaDestino. */
    private Long cdContaDestino;

    /** Atributo cdDigitoContaDestino. */
    private String cdDigitoContaDestino;

    /** Atributo cdTipoContaDestino. */
    private String cdTipoContaDestino;

    /** Atributo dsSituacaoContaDestino. */
    private String dsSituacaoContaDestino;

    /** Atributo cdContingenciaTed. */
    private String cdContingenciaTed;

    /** Atributo nmParticipante. */
    private String nmParticipante;

    /** Atributo cdCpfParticipante. */
    private String cdCpfParticipante;
    
    /** Atributo tpManutencao. */
    private String tpManutencao;

    /**
     * Listar hist vinculo conta salario destino saida dto.
     */
    public ListarHistVinculoContaSalarioDestinoSaidaDTO() {
	super();
    }

    /**
     * Listar hist vinculo conta salario destino saida dto.
     *
     * @param ocorrencia the ocorrencia
     */
    public ListarHistVinculoContaSalarioDestinoSaidaDTO(Ocorrencias ocorrencia) {
	this.cdPessoaContratoVinculado = ocorrencia.getCdPessoaContratoVinculado();
	this.cdTipoVincContrato = ocorrencia.getCdTipoVincContrato();
	this.nrSequenciaVinculacaoContrato = ocorrencia.getNrSequenciaVinculacaoContrato();
	this.cdUsuario = ocorrencia.getCdUsuario();
	this.dtManutencao = ocorrencia.getDtManutencao();
	this.hrInclusaoManutencao = ocorrencia.getHrInclusaoManutencao();
	this.hrManutencao = ocorrencia.getHrManutencao();
	this.cdBancoSalario = ocorrencia.getCdBancoSalario();
	this.dsBancoSalario = ocorrencia.getDsBancoSalario();
	this.cdAgenciaSalario = ocorrencia.getCdAgenciaSalario();
	this.cdDigitoAgenciaSalario = ocorrencia.getCdDigitoAgenciaSalario();
	this.dsAgenciaSalario = ocorrencia.getDsAgenciaSalario();
	this.cdContaSalario = ocorrencia.getCdContaSalario();
	this.cdDigitoContaSalario = ocorrencia.getCdDigitoContaSalario();
	this.cdBancoDestino = ocorrencia.getCdBancoDestino();
	this.dsBancoDestino = ocorrencia.getDsBancoDestino();
	this.cdAgenciaDestino = ocorrencia.getCdAgenciaDestino();
	this.dgAgenciaDestino = ocorrencia.getCdAgenciaDestino();
	this.dsAgenciaDestino = ocorrencia.getDsAgenciaDestino();
	this.cdContaDestino = ocorrencia.getCdContaDestino();
	this.cdDigitoContaDestino = ocorrencia.getCdDigitoContaDestino();
	this.cdTipoContaDestino = ocorrencia.getCdTipoContaDestino();
	this.dsSituacaoContaDestino = ocorrencia.getDsSituacaoContaDestino();
	this.cdContingenciaTed = ocorrencia.getCdContingenciaTed();
	this.nmParticipante = ocorrencia.getNmParticipante();
	this.cdCpfParticipante = ocorrencia.getCdCpfParticipante();
	this.tpManutencao = ocorrencia.getTpManutencao();
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
	return dtManutencao + " " + hrManutencao;
    }

    /**
     * Get: dsUsuario.
     *
     * @return dsUsuario
     */
    public String getDsUsuario() {
	return cdUsuario;
    }

    /**
     * Set: dsUsuario.
     *
     * @param cdUsuario the ds usuario
     */
    public void setDsUsuario(String cdUsuario) {
	this.cdUsuario = cdUsuario;
    }

    /**
     * Get: dtManutencao.
     *
     * @return dtManutencao
     */
    public String getDtManutencao() {
	return dtManutencao;
    }

    /**
     * Set: dtManutencao.
     *
     * @param dtManutencao the dt manutencao
     */
    public void setDtManutencao(String dtManutencao) {
	this.dtManutencao = dtManutencao;
    }

    /**
     * Get: hrInclusaoManutencao.
     *
     * @return hrInclusaoManutencao
     */
    public String getHrInclusaoManutencao() {
	return hrInclusaoManutencao;
    }

    /**
     * Set: hrInclusaoManutencao.
     *
     * @param hrInclusaoManutencao the hr inclusao manutencao
     */
    public void setHrInclusaoManutencao(String hrInclusaoManutencao) {
	this.hrInclusaoManutencao = hrInclusaoManutencao;
    }

    /**
     * Get: hrManutencao.
     *
     * @return hrManutencao
     */
    public String getHrManutencao() {
	return hrManutencao;
    }

    /**
     * Set: hrManutencao.
     *
     * @param hrManutencao the hr manutencao
     */
    public void setHrManutencao(String hrManutencao) {
	this.hrManutencao = hrManutencao;
    }

    /**
     * Get: cdAgenciaDestino.
     *
     * @return cdAgenciaDestino
     */
    public Integer getCdAgenciaDestino() {
        return cdAgenciaDestino;
    }

    /**
     * Set: cdAgenciaDestino.
     *
     * @param cdAgenciaDestino the cd agencia destino
     */
    public void setCdAgenciaDestino(Integer cdAgenciaDestino) {
        this.cdAgenciaDestino = cdAgenciaDestino;
    }

    /**
     * Get: cdAgenciaSalario.
     *
     * @return cdAgenciaSalario
     */
    public Integer getCdAgenciaSalario() {
        return cdAgenciaSalario;
    }

    /**
     * Set: cdAgenciaSalario.
     *
     * @param cdAgenciaSalario the cd agencia salario
     */
    public void setCdAgenciaSalario(Integer cdAgenciaSalario) {
        this.cdAgenciaSalario = cdAgenciaSalario;
    }

    /**
     * Get: cdBancoDestino.
     *
     * @return cdBancoDestino
     */
    public Integer getCdBancoDestino() {
        return cdBancoDestino;
    }

    /**
     * Set: cdBancoDestino.
     *
     * @param cdBancoDestino the cd banco destino
     */
    public void setCdBancoDestino(Integer cdBancoDestino) {
        this.cdBancoDestino = cdBancoDestino;
    }

    /**
     * Get: cdBancoSalario.
     *
     * @return cdBancoSalario
     */
    public Integer getCdBancoSalario() {
        return cdBancoSalario;
    }

    /**
     * Set: cdBancoSalario.
     *
     * @param cdBancoSalario the cd banco salario
     */
    public void setCdBancoSalario(Integer cdBancoSalario) {
        this.cdBancoSalario = cdBancoSalario;
    }

    /**
     * Get: cdContaDestino.
     *
     * @return cdContaDestino
     */
    public Long getCdContaDestino() {
        return cdContaDestino;
    }

    /**
     * Set: cdContaDestino.
     *
     * @param cdContaDestino the cd conta destino
     */
    public void setCdContaDestino(Long cdContaDestino) {
        this.cdContaDestino = cdContaDestino;
    }

    /**
     * Get: cdContaSalario.
     *
     * @return cdContaSalario
     */
    public Long getCdContaSalario() {
        return cdContaSalario;
    }

    /**
     * Set: cdContaSalario.
     *
     * @param cdContaSalario the cd conta salario
     */
    public void setCdContaSalario(Long cdContaSalario) {
        this.cdContaSalario = cdContaSalario;
    }

    /**
     * Get: cdContingenciaTed.
     *
     * @return cdContingenciaTed
     */
    public String getCdContingenciaTed() {
        return cdContingenciaTed;
    }

    /**
     * Set: cdContingenciaTed.
     *
     * @param cdContingenciaTed the cd contingencia ted
     */
    public void setCdContingenciaTed(String cdContingenciaTed) {
        this.cdContingenciaTed = cdContingenciaTed;
    }

    /**
     * Get: cdCpfParticipante.
     *
     * @return cdCpfParticipante
     */
    public String getCdCpfParticipante() {
        return cdCpfParticipante;
    }

    /**
     * Set: cdCpfParticipante.
     *
     * @param cdCpfParticipante the cd cpf participante
     */
    public void setCdCpfParticipante(String cdCpfParticipante) {
        this.cdCpfParticipante = cdCpfParticipante;
    }

    /**
     * Get: cdDigitoAgenciaSalario.
     *
     * @return cdDigitoAgenciaSalario
     */
    public Integer getCdDigitoAgenciaSalario() {
        return cdDigitoAgenciaSalario;
    }

    /**
     * Set: cdDigitoAgenciaSalario.
     *
     * @param cdDigitoAgenciaSalario the cd digito agencia salario
     */
    public void setCdDigitoAgenciaSalario(Integer cdDigitoAgenciaSalario) {
        this.cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
    }

    /**
     * Get: cdDigitoContaDestino.
     *
     * @return cdDigitoContaDestino
     */
    public String getCdDigitoContaDestino() {
        return cdDigitoContaDestino;
    }

    /**
     * Set: cdDigitoContaDestino.
     *
     * @param cdDigitoContaDestino the cd digito conta destino
     */
    public void setCdDigitoContaDestino(String cdDigitoContaDestino) {
        this.cdDigitoContaDestino = cdDigitoContaDestino;
    }

    /**
     * Get: cdDigitoContaSalario.
     *
     * @return cdDigitoContaSalario
     */
    public String getCdDigitoContaSalario() {
        return cdDigitoContaSalario;
    }

    /**
     * Set: cdDigitoContaSalario.
     *
     * @param cdDigitoContaSalario the cd digito conta salario
     */
    public void setCdDigitoContaSalario(String cdDigitoContaSalario) {
        this.cdDigitoContaSalario = cdDigitoContaSalario;
    }

    /**
     * Get: cdPessoaContratoVinculado.
     *
     * @return cdPessoaContratoVinculado
     */
    public Long getCdPessoaContratoVinculado() {
        return cdPessoaContratoVinculado;
    }

    /**
     * Set: cdPessoaContratoVinculado.
     *
     * @param cdPessoaContratoVinculado the cd pessoa contrato vinculado
     */
    public void setCdPessoaContratoVinculado(Long cdPessoaContratoVinculado) {
        this.cdPessoaContratoVinculado = cdPessoaContratoVinculado;
    }

    /**
     * Get: cdTipoContaDestino.
     *
     * @return cdTipoContaDestino
     */
    public String getCdTipoContaDestino() {
        return cdTipoContaDestino;
    }

    /**
     * Set: cdTipoContaDestino.
     *
     * @param cdTipoContaDestino the cd tipo conta destino
     */
    public void setCdTipoContaDestino(String cdTipoContaDestino) {
        this.cdTipoContaDestino = cdTipoContaDestino;
    }

    /**
     * Get: cdTipoVincContrato.
     *
     * @return cdTipoVincContrato
     */
    public Integer getCdTipoVincContrato() {
        return cdTipoVincContrato;
    }

    /**
     * Set: cdTipoVincContrato.
     *
     * @param cdTipoVincContrato the cd tipo vinc contrato
     */
    public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
        this.cdTipoVincContrato = cdTipoVincContrato;
    }

    /**
     * Get: cdUsuario.
     *
     * @return cdUsuario
     */
    public String getCdUsuario() {
        return cdUsuario;
    }

    /**
     * Set: cdUsuario.
     *
     * @param cdUsuario the cd usuario
     */
    public void setCdUsuario(String cdUsuario) {
        this.cdUsuario = cdUsuario;
    }

    /**
     * Get: dgAgenciaDestino.
     *
     * @return dgAgenciaDestino
     */
    public Integer getDgAgenciaDestino() {
        return dgAgenciaDestino;
    }

    /**
     * Set: dgAgenciaDestino.
     *
     * @param dgAgenciaDestino the dg agencia destino
     */
    public void setDgAgenciaDestino(Integer dgAgenciaDestino) {
        this.dgAgenciaDestino = dgAgenciaDestino;
    }

    /**
     * Get: dsAgenciaDestino.
     *
     * @return dsAgenciaDestino
     */
    public String getDsAgenciaDestino() {
        return dsAgenciaDestino;
    }

    /**
     * Set: dsAgenciaDestino.
     *
     * @param dsAgenciaDestino the ds agencia destino
     */
    public void setDsAgenciaDestino(String dsAgenciaDestino) {
        this.dsAgenciaDestino = dsAgenciaDestino;
    }

    /**
     * Get: dsAgenciaSalario.
     *
     * @return dsAgenciaSalario
     */
    public String getDsAgenciaSalario() {
        return dsAgenciaSalario;
    }

    /**
     * Set: dsAgenciaSalario.
     *
     * @param dsAgenciaSalario the ds agencia salario
     */
    public void setDsAgenciaSalario(String dsAgenciaSalario) {
        this.dsAgenciaSalario = dsAgenciaSalario;
    }

    /**
     * Get: dsBancoDestino.
     *
     * @return dsBancoDestino
     */
    public String getDsBancoDestino() {
        return dsBancoDestino;
    }

    /**
     * Set: dsBancoDestino.
     *
     * @param dsBancoDestino the ds banco destino
     */
    public void setDsBancoDestino(String dsBancoDestino) {
        this.dsBancoDestino = dsBancoDestino;
    }

    /**
     * Get: dsBancoSalario.
     *
     * @return dsBancoSalario
     */
    public String getDsBancoSalario() {
        return dsBancoSalario;
    }

    /**
     * Set: dsBancoSalario.
     *
     * @param dsBancoSalario the ds banco salario
     */
    public void setDsBancoSalario(String dsBancoSalario) {
        this.dsBancoSalario = dsBancoSalario;
    }

    /**
     * Get: dsSituacaoContaDestino.
     *
     * @return dsSituacaoContaDestino
     */
    public String getDsSituacaoContaDestino() {
        return dsSituacaoContaDestino;
    }

    /**
     * Set: dsSituacaoContaDestino.
     *
     * @param dsSituacaoContaDestino the ds situacao conta destino
     */
    public void setDsSituacaoContaDestino(String dsSituacaoContaDestino) {
        this.dsSituacaoContaDestino = dsSituacaoContaDestino;
    }

    /**
     * Get: nmParticipante.
     *
     * @return nmParticipante
     */
    public String getNmParticipante() {
        return nmParticipante;
    }

    /**
     * Set: nmParticipante.
     *
     * @param nmParticipante the nm participante
     */
    public void setNmParticipante(String nmParticipante) {
        this.nmParticipante = nmParticipante;
    }

    /**
     * Get: nrSequenciaVinculacaoContrato.
     *
     * @return nrSequenciaVinculacaoContrato
     */
    public Long getNrSequenciaVinculacaoContrato() {
        return nrSequenciaVinculacaoContrato;
    }

    /**
     * Set: nrSequenciaVinculacaoContrato.
     *
     * @param nrSequenciaVinculacaoContrato the nr sequencia vinculacao contrato
     */
    public void setNrSequenciaVinculacaoContrato(Long nrSequenciaVinculacaoContrato) {
        this.nrSequenciaVinculacaoContrato = nrSequenciaVinculacaoContrato;
    }

    /**
     * Get: bancoSalario.
     *
     * @return bancoSalario
     */
    public String getBancoSalario(){
	return PgitUtil.formatBanco(cdBancoSalario, dsBancoSalario, false);
    }
    
    /**
     * Get: agenciaSalario.
     *
     * @return agenciaSalario
     */
    public String getAgenciaSalario(){
	return PgitUtil.formatAgencia(cdAgenciaSalario, dsAgenciaSalario, false);
    }
    
    /**
     * Get: contaSalario.
     *
     * @return contaSalario
     */
    public String getContaSalario(){
	return PgitUtil.formatConta(cdContaSalario, cdDigitoContaSalario, false);
    }    
    
    /**
     * Get: bancoDestino.
     *
     * @return bancoDestino
     */
    public String getBancoDestino(){
	return PgitUtil.formatBanco(cdBancoDestino, dsBancoDestino, false);
    }
    
    /**
     * Get: agenciaDestino.
     *
     * @return agenciaDestino
     */
    public String getAgenciaDestino(){
	return PgitUtil.formatAgencia(cdAgenciaDestino, dsAgenciaDestino, false);
    }
    
    /**
     * Get: contaDestino.
     *
     * @return contaDestino
     */
    public String getContaDestino(){
	return PgitUtil.formatConta(cdContaDestino, cdDigitoContaDestino, false);
    }
    
    /**
     * Get: cpfParticipante.
     *
     * @return cpfParticipante
     */
    public String getCpfParticipante(){
	String temp = cdCpfParticipante;
	
	if(temp == null){
	    temp = "";
	}
	
	if(temp.length() < 11){
	    temp = PgitUtil.complementaDigito(temp, 11);
	}
	
	String cpf1 = temp.substring(0, 3);
	String cpf2 = temp.substring(3, 6);
	String cpf3 = temp.substring(6, 9);
	String controle = temp.substring(9, 11);
	
	StringBuilder cnpjOuCpf = new StringBuilder("");
	cnpjOuCpf.append(cpf1);
	cnpjOuCpf.append(".");
	cnpjOuCpf.append(cpf2);
	cnpjOuCpf.append(".");
	cnpjOuCpf.append(cpf3);
	cnpjOuCpf.append("-");
	cnpjOuCpf.append(controle);
	
	return cnpjOuCpf.toString();	
    }

    /**
     * Get: tpManutencao.
     *
     * @return tpManutencao
     */
    public String getTpManutencao() {
        return tpManutencao;
    }

    /**
     * Set: tpManutencao.
     *
     * @param tpManutencao the tp manutencao
     */
    public void setTpManutencao(String tpManutencao) {
        this.tpManutencao = tpManutencao;
    }    
}