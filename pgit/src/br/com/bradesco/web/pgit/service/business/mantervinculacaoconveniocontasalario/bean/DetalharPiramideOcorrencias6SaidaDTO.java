/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;


/**
 * The Class DetalharPiramideOcorrencias6SaidaDTO.
 */
public class DetalharPiramideOcorrencias6SaidaDTO {

    /** The cd cren atendimento quest. */
    private Integer cdCrenAtendimentoQuest;

    /** The ds cren atendimento quest. */
    private String dsCrenAtendimentoQuest;

    /** The qtd cren quest. */
    private Long qtdCrenQuest;

    /** The qtd func cren quest. */
    private Long qtdFuncCrenQuest;

    /** The cd unidade med ag quest. */
    private Integer cdUnidadeMedAgQuest;

    /** The cd distc cren quest. */
    private Integer cdDistcCrenQuest;

    /**
     * Set cd cren atendimento quest.
     * 
     * @param cdCrenAtendimentoQuest
     *            the cd cren atendimento quest
     */
    public void setCdCrenAtendimentoQuest(Integer cdCrenAtendimentoQuest) {
        this.cdCrenAtendimentoQuest = cdCrenAtendimentoQuest;
    }

    /**
     * Get cd cren atendimento quest.
     * 
     * @return the cd cren atendimento quest
     */
    public Integer getCdCrenAtendimentoQuest() {
        return this.cdCrenAtendimentoQuest;
    }

    /**
     * Set ds cren atendimento quest.
     * 
     * @param dsCrenAtendimentoQuest
     *            the ds cren atendimento quest
     */
    public void setDsCrenAtendimentoQuest(String dsCrenAtendimentoQuest) {
        this.dsCrenAtendimentoQuest = dsCrenAtendimentoQuest;
    }

    /**
     * Get ds cren atendimento quest.
     * 
     * @return the ds cren atendimento quest
     */
    public String getDsCrenAtendimentoQuest() {
        return this.dsCrenAtendimentoQuest;
    }

    /**
     * Set qtd cren quest.
     * 
     * @param qtdCrenQuest
     *            the qtd cren quest
     */
    public void setQtdCrenQuest(Long qtdCrenQuest) {
        this.qtdCrenQuest = qtdCrenQuest;
    }

    /**
     * Get qtd cren quest.
     * 
     * @return the qtd cren quest
     */
    public Long getQtdCrenQuest() {
        return this.qtdCrenQuest;
    }

    /**
     * Set qtd func cren quest.
     * 
     * @param qtdFuncCrenQuest
     *            the qtd func cren quest
     */
    public void setQtdFuncCrenQuest(Long qtdFuncCrenQuest) {
        this.qtdFuncCrenQuest = qtdFuncCrenQuest;
    }

    /**
     * Get qtd func cren quest.
     * 
     * @return the qtd func cren quest
     */
    public Long getQtdFuncCrenQuest() {
        return this.qtdFuncCrenQuest;
    }

    /**
     * Set cd unidade med ag quest.
     * 
     * @param cdUnidadeMedAgQuest
     *            the cd unidade med ag quest
     */
    public void setCdUnidadeMedAgQuest(Integer cdUnidadeMedAgQuest) {
        this.cdUnidadeMedAgQuest = cdUnidadeMedAgQuest;
    }

    /**
     * Get cd unidade med ag quest.
     * 
     * @return the cd unidade med ag quest
     */
    public Integer getCdUnidadeMedAgQuest() {
        return this.cdUnidadeMedAgQuest;
    }

    /**
     * Set cd distc cren quest.
     * 
     * @param cdDistcCrenQuest
     *            the cd distc cren quest
     */
    public void setCdDistcCrenQuest(Integer cdDistcCrenQuest) {
        this.cdDistcCrenQuest = cdDistcCrenQuest;
    }

    /**
     * Get cd distc cren quest.
     * 
     * @return the cd distc cren quest
     */
    public Integer getCdDistcCrenQuest() {
        return this.cdDistcCrenQuest;
    }
}