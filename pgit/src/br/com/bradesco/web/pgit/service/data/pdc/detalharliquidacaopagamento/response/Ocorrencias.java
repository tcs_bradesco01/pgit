/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _dsFormaLiquidacao
     */
    private java.lang.String _dsFormaLiquidacao;

    /**
     * Field _cdSituacao
     */
    private java.lang.String _cdSituacao;

    /**
     * Field _dsSituacao
     */
    private java.lang.String _dsSituacao;

    /**
     * Field _cdPrioridadeDebito
     */
    private int _cdPrioridadeDebito = 0;

    /**
     * keeps track of state for field: _cdPrioridadeDebito
     */
    private boolean _has_cdPrioridadeDebito;

    /**
     * Field _cdControleHoraLimite
     */
    private int _cdControleHoraLimite = 0;

    /**
     * keeps track of state for field: _cdControleHoraLimite
     */
    private boolean _has_cdControleHoraLimite;

    /**
     * Field _qtMinutosMargemSeguranca
     */
    private int _qtMinutosMargemSeguranca = 0;

    /**
     * keeps track of state for field: _qtMinutosMargemSeguranca
     */
    private boolean _has_qtMinutosMargemSeguranca;

    /**
     * Field _hrLimiteProcessamentoPagamento
     */
    private java.lang.String _hrLimiteProcessamentoPagamento;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegregacaoInclusao
     */
    private java.lang.String _cdAutenticacaoSegregacaoInclusao;

    /**
     * Field _nmOperacaoFluxoInlcusao
     */
    private java.lang.String _nmOperacaoFluxoInlcusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegregacaoManutencao
     */
    private java.lang.String _cdAutenticacaoSegregacaoManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdControleHoraLimite
     * 
     */
    public void deleteCdControleHoraLimite()
    {
        this._has_cdControleHoraLimite= false;
    } //-- void deleteCdControleHoraLimite() 

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteCdPrioridadeDebito
     * 
     */
    public void deleteCdPrioridadeDebito()
    {
        this._has_cdPrioridadeDebito= false;
    } //-- void deleteCdPrioridadeDebito() 

    /**
     * Method deleteQtMinutosMargemSeguranca
     * 
     */
    public void deleteQtMinutosMargemSeguranca()
    {
        this._has_qtMinutosMargemSeguranca= false;
    } //-- void deleteQtMinutosMargemSeguranca() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegregacaoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegregacaoInclusao'
     */
    public java.lang.String getCdAutenticacaoSegregacaoInclusao()
    {
        return this._cdAutenticacaoSegregacaoInclusao;
    } //-- java.lang.String getCdAutenticacaoSegregacaoInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegregacaoManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegregacaoManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegregacaoManutencao()
    {
        return this._cdAutenticacaoSegregacaoManutencao;
    } //-- java.lang.String getCdAutenticacaoSegregacaoManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdControleHoraLimite'.
     * 
     * @return int
     * @return the value of field 'cdControleHoraLimite'.
     */
    public int getCdControleHoraLimite()
    {
        return this._cdControleHoraLimite;
    } //-- int getCdControleHoraLimite() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'cdPrioridadeDebito'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeDebito'.
     */
    public int getCdPrioridadeDebito()
    {
        return this._cdPrioridadeDebito;
    } //-- int getCdPrioridadeDebito() 

    /**
     * Returns the value of field 'cdSituacao'.
     * 
     * @return String
     * @return the value of field 'cdSituacao'.
     */
    public java.lang.String getCdSituacao()
    {
        return this._cdSituacao;
    } //-- java.lang.String getCdSituacao() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsFormaLiquidacao'.
     * 
     * @return String
     * @return the value of field 'dsFormaLiquidacao'.
     */
    public java.lang.String getDsFormaLiquidacao()
    {
        return this._dsFormaLiquidacao;
    } //-- java.lang.String getDsFormaLiquidacao() 

    /**
     * Returns the value of field 'dsSituacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacao'.
     */
    public java.lang.String getDsSituacao()
    {
        return this._dsSituacao;
    } //-- java.lang.String getDsSituacao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrLimiteProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrLimiteProcessamentoPagamento'.
     */
    public java.lang.String getHrLimiteProcessamentoPagamento()
    {
        return this._hrLimiteProcessamentoPagamento;
    } //-- java.lang.String getHrLimiteProcessamentoPagamento() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInlcusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInlcusao'.
     */
    public java.lang.String getNmOperacaoFluxoInlcusao()
    {
        return this._nmOperacaoFluxoInlcusao;
    } //-- java.lang.String getNmOperacaoFluxoInlcusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'qtMinutosMargemSeguranca'.
     * 
     * @return int
     * @return the value of field 'qtMinutosMargemSeguranca'.
     */
    public int getQtMinutosMargemSeguranca()
    {
        return this._qtMinutosMargemSeguranca;
    } //-- int getQtMinutosMargemSeguranca() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdControleHoraLimite
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleHoraLimite()
    {
        return this._has_cdControleHoraLimite;
    } //-- boolean hasCdControleHoraLimite() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasCdPrioridadeDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeDebito()
    {
        return this._has_cdPrioridadeDebito;
    } //-- boolean hasCdPrioridadeDebito() 

    /**
     * Method hasQtMinutosMargemSeguranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMinutosMargemSeguranca()
    {
        return this._has_qtMinutosMargemSeguranca;
    } //-- boolean hasQtMinutosMargemSeguranca() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegregacaoInclusao'.
     * 
     * @param cdAutenticacaoSegregacaoInclusao the value of field
     * 'cdAutenticacaoSegregacaoInclusao'.
     */
    public void setCdAutenticacaoSegregacaoInclusao(java.lang.String cdAutenticacaoSegregacaoInclusao)
    {
        this._cdAutenticacaoSegregacaoInclusao = cdAutenticacaoSegregacaoInclusao;
    } //-- void setCdAutenticacaoSegregacaoInclusao(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdAutenticacaoSegregacaoManutencao'.
     * 
     * @param cdAutenticacaoSegregacaoManutencao the value of field
     * 'cdAutenticacaoSegregacaoManutencao'.
     */
    public void setCdAutenticacaoSegregacaoManutencao(java.lang.String cdAutenticacaoSegregacaoManutencao)
    {
        this._cdAutenticacaoSegregacaoManutencao = cdAutenticacaoSegregacaoManutencao;
    } //-- void setCdAutenticacaoSegregacaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdControleHoraLimite'.
     * 
     * @param cdControleHoraLimite the value of field
     * 'cdControleHoraLimite'.
     */
    public void setCdControleHoraLimite(int cdControleHoraLimite)
    {
        this._cdControleHoraLimite = cdControleHoraLimite;
        this._has_cdControleHoraLimite = true;
    } //-- void setCdControleHoraLimite(int) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'cdPrioridadeDebito'.
     * 
     * @param cdPrioridadeDebito the value of field
     * 'cdPrioridadeDebito'.
     */
    public void setCdPrioridadeDebito(int cdPrioridadeDebito)
    {
        this._cdPrioridadeDebito = cdPrioridadeDebito;
        this._has_cdPrioridadeDebito = true;
    } //-- void setCdPrioridadeDebito(int) 

    /**
     * Sets the value of field 'cdSituacao'.
     * 
     * @param cdSituacao the value of field 'cdSituacao'.
     */
    public void setCdSituacao(java.lang.String cdSituacao)
    {
        this._cdSituacao = cdSituacao;
    } //-- void setCdSituacao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaLiquidacao'.
     * 
     * @param dsFormaLiquidacao the value of field
     * 'dsFormaLiquidacao'.
     */
    public void setDsFormaLiquidacao(java.lang.String dsFormaLiquidacao)
    {
        this._dsFormaLiquidacao = dsFormaLiquidacao;
    } //-- void setDsFormaLiquidacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacao'.
     * 
     * @param dsSituacao the value of field 'dsSituacao'.
     */
    public void setDsSituacao(java.lang.String dsSituacao)
    {
        this._dsSituacao = dsSituacao;
    } //-- void setDsSituacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteProcessamentoPagamento'.
     * 
     * @param hrLimiteProcessamentoPagamento the value of field
     * 'hrLimiteProcessamentoPagamento'.
     */
    public void setHrLimiteProcessamentoPagamento(java.lang.String hrLimiteProcessamentoPagamento)
    {
        this._hrLimiteProcessamentoPagamento = hrLimiteProcessamentoPagamento;
    } //-- void setHrLimiteProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInlcusao'.
     * 
     * @param nmOperacaoFluxoInlcusao the value of field
     * 'nmOperacaoFluxoInlcusao'.
     */
    public void setNmOperacaoFluxoInlcusao(java.lang.String nmOperacaoFluxoInlcusao)
    {
        this._nmOperacaoFluxoInlcusao = nmOperacaoFluxoInlcusao;
    } //-- void setNmOperacaoFluxoInlcusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'qtMinutosMargemSeguranca'.
     * 
     * @param qtMinutosMargemSeguranca the value of field
     * 'qtMinutosMargemSeguranca'.
     */
    public void setQtMinutosMargemSeguranca(int qtMinutosMargemSeguranca)
    {
        this._qtMinutosMargemSeguranca = qtMinutosMargemSeguranca;
        this._has_qtMinutosMargemSeguranca = true;
    } //-- void setQtMinutosMargemSeguranca(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
