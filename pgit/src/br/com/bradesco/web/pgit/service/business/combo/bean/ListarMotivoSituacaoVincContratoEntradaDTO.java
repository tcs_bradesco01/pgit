/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMotivoSituacaoVincContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoSituacaoVincContratoEntradaDTO {
	
	/** Atributo cdSituacaoVincConta. */
	private int cdSituacaoVincConta;

	/**
	 * Get: cdSituacaoVincConta.
	 *
	 * @return cdSituacaoVincConta
	 */
	public int getCdSituacaoVincConta() {
		return cdSituacaoVincConta;
	}

	/**
	 * Set: cdSituacaoVincConta.
	 *
	 * @param cdSituacaoVincConta the cd situacao vinc conta
	 */
	public void setCdSituacaoVincConta(int cdSituacaoVincConta) {
		this.cdSituacaoVincConta = cdSituacaoVincConta;
	}
	
	
}
