package br.com.bradesco.web.pgit.service.business.contacomplementar.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

public class ListarContaComplParticipantesSaidaDTO {

	private Long cdPessoaJuridicaContrato;

	private Long cdCorpoCpfCnpj;
	private Integer cdFilialCpfCnpj;
	private Integer cdControleCpfCnpj;
	
	private String dsNomeRazaoSocial;
	private Long cdGrupoEconomico;
	private String dsGrupoEconomico;
	
	private Integer cdAtividadeEconomica;
	private String dsAtividadeEconomica;
	
	private Integer cdSegmentoEconomico;
	private String dsSegmentoEconomico;
	
	private Integer cdSubSegmento=0;
	private String dsSubSegmentoCliente;
	
	private Integer cdTipoParticipante;
	private String dsTipoParticipante;
	
	private Integer cdSituacaoParticipante;
	private String dsSituacaoParticipante;
	
	private Integer cdMotivoParticipante;
	private String dsMotivoParticipante;
	
	private Integer cdClassificacaoParticipacao;
	private String dsClassificacaoParticipacao;
	
	public ListarContaComplParticipantesSaidaDTO() {
		super();
	}
	
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}
	public String getDsNomeRazaoSocial() {
		return dsNomeRazaoSocial;
	}
	public void setDsNomeRazaoSocial(String dsNomeRazaoSocial) {
		this.dsNomeRazaoSocial = dsNomeRazaoSocial;
	}
	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}
	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}
	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}
	public Integer getCdSegmentoEconomico() {
		return cdSegmentoEconomico;
	}
	public void setCdSegmentoEconomico(Integer cdSegmentoEconomico) {
		this.cdSegmentoEconomico = cdSegmentoEconomico;
	}
	public String getDsSegmentoEconomico() {
		return dsSegmentoEconomico;
	}
	public void setDsSegmentoEconomico(String dsSegmentoEconomico) {
		this.dsSegmentoEconomico = dsSegmentoEconomico;
	}
	public Integer getCdSubSegmento() {
		return cdSubSegmento;
	}
	public void setCdSubSegmento(Integer cdSubSegmento) {
		this.cdSubSegmento = cdSubSegmento;
	}
	public String getDsSubSegmentoCliente() {
		return dsSubSegmentoCliente;
	}
	public void setDsSubSegmentoCliente(String dsSubSegmentoCliente) {
		this.dsSubSegmentoCliente = dsSubSegmentoCliente;
	}
	public Integer getCdTipoParticipante() {
		return cdTipoParticipante;
	}
	public void setCdTipoParticipante(Integer cdTipoParticipante) {
		this.cdTipoParticipante = cdTipoParticipante;
	}
	public String getDsTipoParticipante() {
		return dsTipoParticipante;
	}
	public void setDsTipoParticipante(String dsTipoParticipante) {
		this.dsTipoParticipante = dsTipoParticipante;
	}
	public Integer getCdSituacaoParticipante() {
		return cdSituacaoParticipante;
	}
	public void setCdSituacaoParticipante(Integer cdSituacaoParticipante) {
		this.cdSituacaoParticipante = cdSituacaoParticipante;
	}
	public String getDsSituacaoParticipante() {
		return dsSituacaoParticipante;
	}
	public void setDsSituacaoParticipante(String dsSituacaoParticipante) {
		this.dsSituacaoParticipante = dsSituacaoParticipante;
	}
	public Integer getCdMotivoParticipante() {
		return cdMotivoParticipante;
	}
	public void setCdMotivoParticipante(Integer cdMotivoParticipante) {
		this.cdMotivoParticipante = cdMotivoParticipante;
	}
	public String getDsMotivoParticipante() {
		return dsMotivoParticipante;
	}
	public void setDsMotivoParticipante(String dsMotivoParticipante) {
		this.dsMotivoParticipante = dsMotivoParticipante;
	}
	public Integer getCdClassificacaoParticipacao() {
		return cdClassificacaoParticipacao;
	}
	public void setCdClassificacaoParticipacao(Integer cdClassificacaoParticipacao) {
		this.cdClassificacaoParticipacao = cdClassificacaoParticipacao;
	}
	public String getDsClassificacaoParticipacao() {
		return dsClassificacaoParticipacao;
	}
	public void setDsClassificacaoParticipacao(String dsClassificacaoParticipacao) {
		this.dsClassificacaoParticipacao = dsClassificacaoParticipacao;
	}
	
	public String getCnpjOuCpfFormatado() {
		return CpfCnpjUtils.formatCpfCnpjCompleto(cdCorpoCpfCnpj, cdFilialCpfCnpj, cdControleCpfCnpj);
	}
}