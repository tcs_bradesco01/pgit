/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompServiceConstants;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.BloqDesbloqEmissaoAutAvisosComprvEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.BloqDesbloqEmissaoAutAvisosComprvSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharHistEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharHistEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ExcluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ExcluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.IncluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.IncluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarHistEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarHistEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarIncluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarIncluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarTipoServicoModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarTipoServicoModalidadeOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.request.BloqDesbloqEmissaoAutAvisosComprvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.response.BloqDesbloqEmissaoAutAvisosComprvResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.request.DetalharEmissaoAutAvisosComprvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.response.DetalharEmissaoAutAvisosComprvResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistemissaoautavisoscomprv.request.DetalharHistEmissaoAutAvisosComprvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistemissaoautavisoscomprv.response.DetalharHistEmissaoAutAvisosComprvResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.request.ExcluirEmissaoAutAvisosComprvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.response.ExcluirEmissaoAutAvisosComprvResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.request.IncluirEmissaoAutAvisosComprvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.response.IncluirEmissaoAutAvisosComprvResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaremissaoautavisoscomprv.request.ListarEmissaoAutAvisosComprvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaremissaoautavisoscomprv.response.ListarEmissaoAutAvisosComprvResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistemissaoautavisoscomprv.request.ListarHistEmissaoAutAvisosComprvRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistemissaoautavisoscomprv.response.ListarHistEmissaoAutAvisosComprvResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartiposervicomodalidade.request.ListarTipoServicoModalidadeRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartiposervicomodalidade.response.ListarTipoServicoModalidadeResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;



/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: EmissaoAutomaticaAvisoComprovante
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class EmissaoAutCompServiceImpl implements IEmissaoAutCompService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public EmissaoAutCompServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutomaticaAvisoComprovante#sampleEmissaoAutomaticaAvisoComprovante()
     */
    public void sampleEmissaoAutomaticaAvisoComprovante() {
        // TODO: Implementa�ao
    }
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#listarEmissaoAutComp(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarEmissaoAutCompEntradaDTO)
	 */
	public List<ListarEmissaoAutCompSaidaDTO> listarEmissaoAutComp(ListarEmissaoAutCompEntradaDTO listarEmissaoAutCompEntradaDTO) {
		
		List<ListarEmissaoAutCompSaidaDTO> listaRetorno = new ArrayList<ListarEmissaoAutCompSaidaDTO>();		
		ListarEmissaoAutAvisosComprvRequest request = new ListarEmissaoAutAvisosComprvRequest();
		ListarEmissaoAutAvisosComprvResponse response = new ListarEmissaoAutAvisosComprvResponse();		
		
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listarEmissaoAutCompEntradaDTO.getContratoPssoaJuridContr()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listarEmissaoAutCompEntradaDTO.getContratoTpoContrNegoc()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listarEmissaoAutCompEntradaDTO.getContratoSegContrNegoc()));
		request.setCdProdutoServicoVinculo(PgitUtil.verificaIntegerNulo(listarEmissaoAutCompEntradaDTO.getServicoEmissaoContratado()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listarEmissaoAutCompEntradaDTO.getTipoServico()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(listarEmissaoAutCompEntradaDTO.getModalidadeServico()));
		request.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(listarEmissaoAutCompEntradaDTO.getTipoRelacionamento()));
		
		request.setQtOcorrencias(IEmissaoAutCompServiceConstants.QTDE_CONSULTAS_LISTAR);
	
		response = getFactoryAdapter().getListarEmissaoAutAvisosComprvPDCAdapter().invokeProcess(request);

		ListarEmissaoAutCompSaidaDTO saidaDTO;
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarEmissaoAutCompSaidaDTO(); 
			saidaDTO.setCodigoMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			saidaDTO.setCdModalidadeServico(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			saidaDTO.setTipoRelacionamento(response.getOcorrencias(i).getCdRelacionamentoProduto());
			saidaDTO.setSituacao(response.getOcorrencias(i).getCdSituacaoVinculacaoOperacao());
			saidaDTO.setCdSituacaoVinculacaoOperacao(response.getOcorrencias(i).getCdSituacaoVinculacaoOperacao());
			saidaDTO.setDsSituacaoVinculacaoOperacao(response.getOcorrencias(i).getDsSituacaoVinculacaoOperacao());
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#listarIncluirEmissaoAutComp(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarIncluirEmissaoAutCompEntradaDTO)
	 */
	public List<ListarIncluirEmissaoAutCompSaidaDTO> listarIncluirEmissaoAutComp(ListarIncluirEmissaoAutCompEntradaDTO listarIncluirEmissaoAutCompEntradaDTO) {
		
		List<ListarIncluirEmissaoAutCompSaidaDTO> listaRetorno = new ArrayList<ListarIncluirEmissaoAutCompSaidaDTO>();		
		ListarTipoServicoModalidadeRequest request = new ListarTipoServicoModalidadeRequest();
		ListarTipoServicoModalidadeResponse response = new ListarTipoServicoModalidadeResponse();		
		
		request.setQtConsultas(IEmissaoAutCompServiceConstants.QTDE_CONSULTAS_LISTAR_INCLUIR);
		request.setCdPessoaJuridicaContrato(listarIncluirEmissaoAutCompEntradaDTO.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(listarIncluirEmissaoAutCompEntradaDTO.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(listarIncluirEmissaoAutCompEntradaDTO.getNrSequenciaContratoNegocio());
		request.setCdProdutoServicoContrato(listarIncluirEmissaoAutCompEntradaDTO.getCdProdutoServicoContrato());
		request.setCdProdutoServicoOperacao(listarIncluirEmissaoAutCompEntradaDTO.getTipoServico());
		request.setCdProdutoOperacaoRelacionado(listarIncluirEmissaoAutCompEntradaDTO.getModalidadeServico());
        
				
		response = getFactoryAdapter().getListarTipoServicoModalidadePDCAdapter().invokeProcess(request);

		ListarIncluirEmissaoAutCompSaidaDTO saidaDTO;
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarIncluirEmissaoAutCompSaidaDTO(); 
			saidaDTO.setCodigoMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdModalidadeServico(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i).getDcModalidade());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsServico());
			saidaDTO.setCdRelacionamento(response.getOcorrencias(i).getCdRelacionamentoProdutoProduto());			
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#incluirEmissaoAutComp(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.IncluirEmissaoAutCompEntradaDTO)
	 */
	public IncluirEmissaoAutCompSaidaDTO incluirEmissaoAutComp(IncluirEmissaoAutCompEntradaDTO incluirEmissaoAutCompEntradaDTO) {
			
			IncluirEmissaoAutCompSaidaDTO incluirEmissaoAutCompSaidaDTO = new IncluirEmissaoAutCompSaidaDTO();
			IncluirEmissaoAutAvisosComprvRequest request = new IncluirEmissaoAutAvisosComprvRequest();
			IncluirEmissaoAutAvisosComprvResponse response = new IncluirEmissaoAutAvisosComprvResponse();
		
			request.setCdPessoaJuridicaContrato(incluirEmissaoAutCompEntradaDTO.getContratoPssoaJuridContr());
			request.setCdTipoContratoNegocio(incluirEmissaoAutCompEntradaDTO.getContratoTpoContrNegoc());
			request.setNrSequenciaContratoNegocio(incluirEmissaoAutCompEntradaDTO.getContratoSegContrNegoc());
			//request.setCdProdutoServicoOperacao(incluirEmissaoAutCompEntradaDTO.getServicoEmissaoContratado());
			request.setCdProdutoServicoVinculo(incluirEmissaoAutCompEntradaDTO.getServicoEmissaoContratado());
			request.setQtOcorrencias(incluirEmissaoAutCompEntradaDTO.getQuantidadeOcorrenciasEnviadasInc());
			
			ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.request.Ocorrencias>();
			
			for(int i=0;i<request.getQtOcorrencias();i++){
				
				br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.request.Ocorrencias();
				ocorrencia.setCdProdutoOperacaoRelacionado(incluirEmissaoAutCompEntradaDTO.getListaIncluir().get(i).getCdProdutoOperacaoRelacionado());
				ocorrencia.setCdProdutoServicoOperacao(incluirEmissaoAutCompEntradaDTO.getListaIncluir().get(i).getCdProdutoServicoOperacao());
				ocorrencia.setCdRelacionamentoProduto(incluirEmissaoAutCompEntradaDTO.getListaIncluir().get(i).getCdRelacionamentoProdutoProduto());
				
				ocorrencias.add(ocorrencia);
			}
		
		
			
			request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.request.Ocorrencias[])ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.request.Ocorrencias[0]));
			
			response = getFactoryAdapter().getIncluirEmissaoAutAvisosComprvPDCAdapter().invokeProcess(request);
			
			incluirEmissaoAutCompSaidaDTO.setCodigoMensagem(response.getCodMensagem());
			incluirEmissaoAutCompSaidaDTO.setMensagem(response.getMensagem());
		
			return incluirEmissaoAutCompSaidaDTO;
		}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#excluirEmissaoAutComp(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ExcluirEmissaoAutCompEntradaDTO)
	 */
	public ExcluirEmissaoAutCompSaidaDTO excluirEmissaoAutComp(ExcluirEmissaoAutCompEntradaDTO excluirEmissaoAutCompEntradaDTO){
		
		ExcluirEmissaoAutCompSaidaDTO excluirEmissaoAutCompSaidaDTO = new ExcluirEmissaoAutCompSaidaDTO();
		ExcluirEmissaoAutAvisosComprvRequest request = new ExcluirEmissaoAutAvisosComprvRequest();
		ExcluirEmissaoAutAvisosComprvResponse response = new ExcluirEmissaoAutAvisosComprvResponse();
		
		request.setCdPessoaJuridicaContrato(excluirEmissaoAutCompEntradaDTO.getContratoPssoaJuridContr());
		request.setCdTipoContratoNegocio(excluirEmissaoAutCompEntradaDTO.getContratoTpoContrNegoc());
		request.setNrSequenciaContratoNegocio(excluirEmissaoAutCompEntradaDTO.getContratoSegContrNegoc());
		request.setCdProdutoServicoVinculo(excluirEmissaoAutCompEntradaDTO.getServicoEmissaoContratado());
		
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.request.Ocorrencias>();
		br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.request.Ocorrencias();
		ocorrencia.setCdProdutoOperacaoRelacionado(excluirEmissaoAutCompEntradaDTO.getModalidadeServico());
		ocorrencia.setCdProdutoServicoOperacao(excluirEmissaoAutCompEntradaDTO.getTipoServico());
		ocorrencia.setCdRelacionamentoProduto(excluirEmissaoAutCompEntradaDTO.getTipoRelacionamento());
		ocorrencias.add(ocorrencia);
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.request.Ocorrencias[])ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.request.Ocorrencias[0]));
		
		request.setQtOcorrencias(1);
		
		response = getFactoryAdapter().getExcluirEmissaoAutAvisosComprvPDCAdapter().invokeProcess(request);
		
		excluirEmissaoAutCompSaidaDTO.setCodMensagem(response.getCodMensagem());
		excluirEmissaoAutCompSaidaDTO.setMensagem(response.getMensagem());
		
		return excluirEmissaoAutCompSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#detalharEmissaoAutComp(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharEmissaoAutCompEntradaDTO)
	 */
	public DetalharEmissaoAutCompSaidaDTO detalharEmissaoAutComp (DetalharEmissaoAutCompEntradaDTO detalharEmissaoAutCompEntradaDTO){
		
		DetalharEmissaoAutCompSaidaDTO saidaDTO = new DetalharEmissaoAutCompSaidaDTO();
		DetalharEmissaoAutAvisosComprvRequest request = new DetalharEmissaoAutAvisosComprvRequest();
		DetalharEmissaoAutAvisosComprvResponse response = new DetalharEmissaoAutAvisosComprvResponse();
		
		request.setCdPessoaJuridicaContrato(detalharEmissaoAutCompEntradaDTO.getContratoPssoaJuridContr());
		request.setCdTipoContratoNegocio(detalharEmissaoAutCompEntradaDTO.getContratoTpoContrNegoc());
		request.setNrSequenciaContratoNegocio(detalharEmissaoAutCompEntradaDTO.getContratoSegContrNegoc());
		request.setCdProdutoServioOperacao(detalharEmissaoAutCompEntradaDTO.getTipoServico());
		request.setCdProdutoServicoVinculo(detalharEmissaoAutCompEntradaDTO.getServicoEmissaoContratado());
		request.setCdProdutoOperacaoRelacionado(detalharEmissaoAutCompEntradaDTO.getModalidadeServico());
		request.setCdRelacionamentoProduto(detalharEmissaoAutCompEntradaDTO.getTipoRelacionamento());

		
		
		response = getFactoryAdapter().getDetalharEmissaoAutAvisosComprvPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		saidaDTO.setCdServicoEmissaoContratado(response.getCdProdutoServicoVinculo());
		saidaDTO.setDsServicoEmissaoContratado(response.getDsProdutoServicoVinculo());
		saidaDTO.setCdTipoServico(response.getCdProdutoServioOperacao());
		saidaDTO.setDsTipoServico(response.getDsProdutoServicoOperacao());
		saidaDTO.setCdModalidadeServico(response.getCdProdutoOperacaoRelacionado());
		saidaDTO.setDsModalidadeServico(response.getDsProdutoOperacaoRelacionado());
		saidaDTO.setTipoRelacionamento(response.getCdRelacionamentoProduto());		
		saidaDTO.setSituacaoVinculacao(response.getCdSituacaoVinculacaoOperacao());
		
		
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getCdAutenSegregacaoInclusao());
		saidaDTO.setComplementoInclusao(response.getNmOperacaoFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getHrInclusaoRegistro());
		
		saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getCdAutenSegregacaoManutencao());
		saidaDTO.setComplementoManutencao(response.getNmOperacaoFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getHrManutencaoRegistro());
		
		saidaDTO.setDsSituacaoVinculacaoOperacao(response.getDsSituacaoVinculacaoOperacao());
		
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#listarHistEmissaoAutComp(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarHistEmissaoAutCompEntradaDTO)
	 */
	public List<ListarHistEmissaoAutCompSaidaDTO> listarHistEmissaoAutComp(ListarHistEmissaoAutCompEntradaDTO listarHistEmissaoAutCompEntradaDTO){
		
		List<ListarHistEmissaoAutCompSaidaDTO> listaRetorno = new ArrayList<ListarHistEmissaoAutCompSaidaDTO>();		
		ListarHistEmissaoAutAvisosComprvRequest request = new ListarHistEmissaoAutAvisosComprvRequest();
		ListarHistEmissaoAutAvisosComprvResponse response = new ListarHistEmissaoAutAvisosComprvResponse();
		
		request.setCdPessoaJuridicaContrato(listarHistEmissaoAutCompEntradaDTO.getContratoPssaoJuridContr());
		request.setCdTipoContratoNegocio(listarHistEmissaoAutCompEntradaDTO.getContratoTpoContrNegoc());
		request.setNrSequenciaContratoNegocio(listarHistEmissaoAutCompEntradaDTO.getContratoSeqContrNegoc());
		request.setCdProdutoServicoOperacao(listarHistEmissaoAutCompEntradaDTO.getTipoServico());
		request.setCdProdutoServicoVinculo(listarHistEmissaoAutCompEntradaDTO.getServicoEmissaoContratado());
		request.setCdProdutoOperacaoRelacionado(listarHistEmissaoAutCompEntradaDTO.getModalidadeServico());
		request.setCdRelacionamentoProduto(listarHistEmissaoAutCompEntradaDTO.getTipoRelacionamento());
		request.setDtInicioManutencao(listarHistEmissaoAutCompEntradaDTO.getPeriodoInicialManutencao());
		request.setDtFinalManutencao(listarHistEmissaoAutCompEntradaDTO.getPeriodoFinalManutencao());
		request.setHrInclusaoRegistroManutencao("");
		request.setQtConsultas(IEmissaoAutCompServiceConstants.QTDE_CONSULTAS_LISTAR_HIST);
		
		response = getFactoryAdapter().getListarHistEmissaoAutAvisosComprvPDCAdapter().invokeProcess(request);
		
		ListarHistEmissaoAutCompSaidaDTO saidaDTO;
		
		
		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");		
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarHistEmissaoAutCompSaidaDTO(); 
			
			
			if (response.getOcorrencias(i).getDtManutencaoRegistro() != null && response.getOcorrencias(i).getDtManutencaoRegistro().length() >=19 ){
				
				String stringData = response.getOcorrencias(i).getDtManutencaoRegistro().substring(0, 19);
				
				try {
					saidaDTO.setDataManutencaoDesc(formato2.format(formato1.parse(stringData)));
				}  catch (ParseException e) {
					saidaDTO.setDataManutencaoDesc("");
				}
			}else{
				saidaDTO.setDataManutencaoDesc("");
			}
			
			
			
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setDataManutencao(response.getOcorrencias(i).getDtManutencaoRegistro());
			saidaDTO.setResponsavel(response.getOcorrencias(i).getCdUsuarioManutencao());
			saidaDTO.setTipoManutencao(response.getOcorrencias(i).getCdIndicadorTipoManutencao());
			saidaDTO.setDescTipoManutencao(response.getOcorrencias(i).getCdTipoManutencao());
			
			listaRetorno.add(saidaDTO);			
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#detalharHistEmissaoAutComp(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharHistEmissaoAutCompEntradaDTO)
	 */
	public DetalharHistEmissaoAutCompSaidaDTO detalharHistEmissaoAutComp (DetalharHistEmissaoAutCompEntradaDTO detalharHistEmissaoAutCompEntradaDTO){
		
		DetalharHistEmissaoAutCompSaidaDTO saidaDTO = new DetalharHistEmissaoAutCompSaidaDTO();
		DetalharHistEmissaoAutAvisosComprvRequest request = new DetalharHistEmissaoAutAvisosComprvRequest();
		DetalharHistEmissaoAutAvisosComprvResponse response = new DetalharHistEmissaoAutAvisosComprvResponse();
		
		request.setCdPessoaJuridicaContrato(detalharHistEmissaoAutCompEntradaDTO.getContratoPssaoJuridContr());
		request.setCdTipoContratoNegocio(detalharHistEmissaoAutCompEntradaDTO.getContratoTpoContrNegoc());
		request.setNrSequenciaContratoNegocio(detalharHistEmissaoAutCompEntradaDTO.getContratoSeqContrNegoc());
		
		request.setCdProdutoServicoOperacao(detalharHistEmissaoAutCompEntradaDTO.getTipoServico());
		request.setCdProdutoServicoVinculo(detalharHistEmissaoAutCompEntradaDTO.getServicoEmissaoContratado());
		
		request.setCdProdutoOperacaoRelacionado(detalharHistEmissaoAutCompEntradaDTO.getModalidadeServico());
		request.setCdRelacionamentoProduto(detalharHistEmissaoAutCompEntradaDTO.getTipoRelacionamento());
		request.setHrInclusaoRegistroManutencao(detalharHistEmissaoAutCompEntradaDTO.getDataHoraInclusaoRegistroHistorico());
		request.setDtInicioManutencao(detalharHistEmissaoAutCompEntradaDTO.getPeriodoInicialManutencao());
		request.setDtFinalManutencao(detalharHistEmissaoAutCompEntradaDTO.getPeriodoFinalManutencao());		
		request.setQtConsultas(0);
		
		response = getFactoryAdapter().getDetalharHistEmissaoAutAvisosComprvPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCdServicoEmissaoContratado(response.getCdProdutoServicoOperacao());
		saidaDTO.setDsServicoEmissaoContratado(response.getDsProdutoServicoOperacao());
		saidaDTO.setCdTipoServico(response.getCdProdutoServicoVinculo());
		saidaDTO.setDsTipoServico(response.getDsProdutoServicoVinculo());
		saidaDTO.setCdModalidadeRelacionamento(response.getCdProdutoOperacaoRelacionado());
		saidaDTO.setDsModalidadeRelacionamento(response.getDsProdutoOperacaoRelacionado());
		saidaDTO.setTipoRelacionamento(response.getCdRelacionamentoProduto());
		saidaDTO.setSituacaoVinculacao(response.getCdSituacaoVinculacaoOperacao());
		saidaDTO.setDsSituacaoVinculacao(response.getDsSituacaoVinculacaoOperacao());
		
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setUsuarioInclusao(response.getCdAutenSegregacaoInclusao());
		saidaDTO.setComplementoInclusao(response.getNmOperacaoFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getHrInclusaoRegistro());
		
		saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
		saidaDTO.setUsuarioManutencao(response.getCdAutenSegregacaoManutencao());
		saidaDTO.setComplementoManutencao(response.getNmOperacaoFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getHrManutencaoRegistro());
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#bloqDesbloqEmissaoAutAvisosComprv(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.BloqDesbloqEmissaoAutAvisosComprvEntradaDTO)
	 */
	public BloqDesbloqEmissaoAutAvisosComprvSaidaDTO bloqDesbloqEmissaoAutAvisosComprv(BloqDesbloqEmissaoAutAvisosComprvEntradaDTO entradaDTO){
		
		BloqDesbloqEmissaoAutAvisosComprvSaidaDTO saidaDTO = new BloqDesbloqEmissaoAutAvisosComprvSaidaDTO();
		BloqDesbloqEmissaoAutAvisosComprvRequest request = new BloqDesbloqEmissaoAutAvisosComprvRequest();
		BloqDesbloqEmissaoAutAvisosComprvResponse response = new BloqDesbloqEmissaoAutAvisosComprvResponse();
		
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdProdutoServicoVinculo(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoVinculo()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setQtOcorrencias(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().size()));
		
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.request.Ocorrencias>();
		br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.request.Ocorrencias ocorrencia;		
		
		
		for (int i=0; i< entradaDTO.getListaOcorrencias().size(); i++){
			ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.request.Ocorrencias();		
			ocorrencia.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdProdutoOperacaoRelacionado()));
			ocorrencia.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdProdutoServicoOperacao()));
			ocorrencia.setCdRelacionamentoProdutoProduto(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdRelacionamentoProdutoProduto()));
			ocorrencia.setCdsituacaoVinculacaoAviso(PgitUtil.verificaIntegerNulo(entradaDTO.getListaOcorrencias().get(i).getCdSituacaoVinculacaoAviso()));
			lista.add(ocorrencia);		
		}
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.request.Ocorrencias[0]));		
		response = getFactoryAdapter().getBloqDesbloqEmissaoAutAvisosComprvPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService#listarTipoServicoModalidade(br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarTipoServicoModalidadeEntradaDTO)
	 */
	public List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> listarTipoServicoModalidade(
			ListarTipoServicoModalidadeEntradaDTO entrada) {
		ListarTipoServicoModalidadeRequest request = new ListarTipoServicoModalidadeRequest();
		List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> saida =  new ArrayList<ListarTipoServicoModalidadeOcorrenciaSaidaDTO>();
		
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
		request.setCdProdutoServicoContrato(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoContrato()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setQtConsultas(100);
		
		ListarTipoServicoModalidadeResponse response = getFactoryAdapter().getListarTipoServicoModalidadePDCAdapter().invokeProcess(request);
		
		for(int i = 0; i< response.getOcorrenciasCount(); i++){
			ListarTipoServicoModalidadeOcorrenciaSaidaDTO	occur = new ListarTipoServicoModalidadeOcorrenciaSaidaDTO();
			occur.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			occur.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			occur.setCdRelacionamentoProdutoProduto(response.getOcorrencias(i).getCdRelacionamentoProdutoProduto());
			occur.setDcModalidade(response.getOcorrencias(i).getDcModalidade());
			occur.setDsServico(response.getOcorrencias(i).getDsServico());
			saida.add(occur);
		}
		

		return saida;
	}
}

