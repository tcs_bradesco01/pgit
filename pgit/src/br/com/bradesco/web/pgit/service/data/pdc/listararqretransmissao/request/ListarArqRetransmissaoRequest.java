/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarArqRetransmissaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarArqRetransmissaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtInicioGeracaoRetorno
     */
    private java.lang.String _dtInicioGeracaoRetorno;

    /**
     * Field _dtFimGeracaoRetorno
     */
    private java.lang.String _dtFimGeracaoRetorno;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _nrArquivoRetorno
     */
    private long _nrArquivoRetorno = 0;

    /**
     * keeps track of state for field: _nrArquivoRetorno
     */
    private boolean _has_nrArquivoRetorno;

    /**
     * Field _cdSituacaoProcessamentoRetorno
     */
    private int _cdSituacaoProcessamentoRetorno = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoProcessamentoRetorno
     */
    private boolean _has_cdSituacaoProcessamentoRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarArqRetransmissaoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.request.ListarArqRetransmissaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdSituacaoProcessamentoRetorno
     * 
     */
    public void deleteCdSituacaoProcessamentoRetorno()
    {
        this._has_cdSituacaoProcessamentoRetorno= false;
    } //-- void deleteCdSituacaoProcessamentoRetorno() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrArquivoRetorno
     * 
     */
    public void deleteNrArquivoRetorno()
    {
        this._has_nrArquivoRetorno= false;
    } //-- void deleteNrArquivoRetorno() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdSituacaoProcessamentoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoProcessamentoRetorno'.
     */
    public int getCdSituacaoProcessamentoRetorno()
    {
        return this._cdSituacaoProcessamentoRetorno;
    } //-- int getCdSituacaoProcessamentoRetorno() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dtFimGeracaoRetorno'.
     * 
     * @return String
     * @return the value of field 'dtFimGeracaoRetorno'.
     */
    public java.lang.String getDtFimGeracaoRetorno()
    {
        return this._dtFimGeracaoRetorno;
    } //-- java.lang.String getDtFimGeracaoRetorno() 

    /**
     * Returns the value of field 'dtInicioGeracaoRetorno'.
     * 
     * @return String
     * @return the value of field 'dtInicioGeracaoRetorno'.
     */
    public java.lang.String getDtInicioGeracaoRetorno()
    {
        return this._dtInicioGeracaoRetorno;
    } //-- java.lang.String getDtInicioGeracaoRetorno() 

    /**
     * Returns the value of field 'nrArquivoRetorno'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRetorno'.
     */
    public long getNrArquivoRetorno()
    {
        return this._nrArquivoRetorno;
    } //-- long getNrArquivoRetorno() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdSituacaoProcessamentoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoProcessamentoRetorno()
    {
        return this._has_cdSituacaoProcessamentoRetorno;
    } //-- boolean hasCdSituacaoProcessamentoRetorno() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRetorno()
    {
        return this._has_nrArquivoRetorno;
    } //-- boolean hasNrArquivoRetorno() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdSituacaoProcessamentoRetorno'.
     * 
     * @param cdSituacaoProcessamentoRetorno the value of field
     * 'cdSituacaoProcessamentoRetorno'.
     */
    public void setCdSituacaoProcessamentoRetorno(int cdSituacaoProcessamentoRetorno)
    {
        this._cdSituacaoProcessamentoRetorno = cdSituacaoProcessamentoRetorno;
        this._has_cdSituacaoProcessamentoRetorno = true;
    } //-- void setCdSituacaoProcessamentoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dtFimGeracaoRetorno'.
     * 
     * @param dtFimGeracaoRetorno the value of field
     * 'dtFimGeracaoRetorno'.
     */
    public void setDtFimGeracaoRetorno(java.lang.String dtFimGeracaoRetorno)
    {
        this._dtFimGeracaoRetorno = dtFimGeracaoRetorno;
    } //-- void setDtFimGeracaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioGeracaoRetorno'.
     * 
     * @param dtInicioGeracaoRetorno the value of field
     * 'dtInicioGeracaoRetorno'.
     */
    public void setDtInicioGeracaoRetorno(java.lang.String dtInicioGeracaoRetorno)
    {
        this._dtInicioGeracaoRetorno = dtInicioGeracaoRetorno;
    } //-- void setDtInicioGeracaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRetorno'.
     * 
     * @param nrArquivoRetorno the value of field 'nrArquivoRetorno'
     */
    public void setNrArquivoRetorno(long nrArquivoRetorno)
    {
        this._nrArquivoRetorno = nrArquivoRetorno;
        this._has_nrArquivoRetorno = true;
    } //-- void setNrArquivoRetorno(long) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarArqRetransmissaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.request.ListarArqRetransmissaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.request.ListarArqRetransmissaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.request.ListarArqRetransmissaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.request.ListarArqRetransmissaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
