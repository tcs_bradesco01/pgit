/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaVinculo
     */
    private long _cdPessoaJuridicaVinculo = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaVinculo
     */
    private boolean _has_cdPessoaJuridicaVinculo;

    /**
     * Field _cdTipoContratoVinculo
     */
    private int _cdTipoContratoVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinculo
     */
    private boolean _has_cdTipoContratoVinculo;

    /**
     * Field _nrSequenciaContratoVinculo
     */
    private long _nrSequenciaContratoVinculo = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoVinculo
     */
    private boolean _has_nrSequenciaContratoVinculo;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _cdParticipante
     */
    private long _cdParticipante = 0;

    /**
     * keeps track of state for field: _cdParticipante
     */
    private boolean _has_cdParticipante;

    /**
     * Field _nmParticipante
     */
    private java.lang.String _nmParticipante;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;

    /**
     * Field _cdSituacao
     */
    private int _cdSituacao = 0;

    /**
     * keeps track of state for field: _cdSituacao
     */
    private boolean _has_cdSituacao;

    /**
     * Field _dsSituacao
     */
    private java.lang.String _dsSituacao;

    /**
     * Field _cdSituacaoVinculacaoConta
     */
    private int _cdSituacaoVinculacaoConta = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoConta
     */
    private boolean _has_cdSituacaoVinculacaoConta;

    /**
     * Field _dsSituacaoVinculacaoConta
     */
    private java.lang.String _dsSituacaoVinculacaoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdParticipante
     * 
     */
    public void deleteCdParticipante()
    {
        this._has_cdParticipante= false;
    } //-- void deleteCdParticipante() 

    /**
     * Method deleteCdPessoaJuridicaVinculo
     * 
     */
    public void deleteCdPessoaJuridicaVinculo()
    {
        this._has_cdPessoaJuridicaVinculo= false;
    } //-- void deleteCdPessoaJuridicaVinculo() 

    /**
     * Method deleteCdSituacao
     * 
     */
    public void deleteCdSituacao()
    {
        this._has_cdSituacao= false;
    } //-- void deleteCdSituacao() 

    /**
     * Method deleteCdSituacaoVinculacaoConta
     * 
     */
    public void deleteCdSituacaoVinculacaoConta()
    {
        this._has_cdSituacaoVinculacaoConta= false;
    } //-- void deleteCdSituacaoVinculacaoConta() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoVinculo
     * 
     */
    public void deleteCdTipoContratoVinculo()
    {
        this._has_cdTipoContratoVinculo= false;
    } //-- void deleteCdTipoContratoVinculo() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSequenciaContratoVinculo
     * 
     */
    public void deleteNrSequenciaContratoVinculo()
    {
        this._has_nrSequenciaContratoVinculo= false;
    } //-- void deleteNrSequenciaContratoVinculo() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdParticipante'.
     * 
     * @return long
     * @return the value of field 'cdParticipante'.
     */
    public long getCdParticipante()
    {
        return this._cdParticipante;
    } //-- long getCdParticipante() 

    /**
     * Returns the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaVinculo'.
     */
    public long getCdPessoaJuridicaVinculo()
    {
        return this._cdPessoaJuridicaVinculo;
    } //-- long getCdPessoaJuridicaVinculo() 

    /**
     * Returns the value of field 'cdSituacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacao'.
     */
    public int getCdSituacao()
    {
        return this._cdSituacao;
    } //-- int getCdSituacao() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoConta'.
     */
    public int getCdSituacaoVinculacaoConta()
    {
        return this._cdSituacaoVinculacaoConta;
    } //-- int getCdSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinculo'.
     */
    public int getCdTipoContratoVinculo()
    {
        return this._cdTipoContratoVinculo;
    } //-- int getCdTipoContratoVinculo() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsSituacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacao'.
     */
    public java.lang.String getDsSituacao()
    {
        return this._dsSituacao;
    } //-- java.lang.String getDsSituacao() 

    /**
     * Returns the value of field 'dsSituacaoVinculacaoConta'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoVinculacaoConta'.
     */
    public java.lang.String getDsSituacaoVinculacaoConta()
    {
        return this._dsSituacaoVinculacaoConta;
    } //-- java.lang.String getDsSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'nmParticipante'.
     * 
     * @return String
     * @return the value of field 'nmParticipante'.
     */
    public java.lang.String getNmParticipante()
    {
        return this._nmParticipante;
    } //-- java.lang.String getNmParticipante() 

    /**
     * Returns the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoVinculo'.
     */
    public long getNrSequenciaContratoVinculo()
    {
        return this._nrSequenciaContratoVinculo;
    } //-- long getNrSequenciaContratoVinculo() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParticipante()
    {
        return this._has_cdParticipante;
    } //-- boolean hasCdParticipante() 

    /**
     * Method hasCdPessoaJuridicaVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaVinculo()
    {
        return this._has_cdPessoaJuridicaVinculo;
    } //-- boolean hasCdPessoaJuridicaVinculo() 

    /**
     * Method hasCdSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacao()
    {
        return this._has_cdSituacao;
    } //-- boolean hasCdSituacao() 

    /**
     * Method hasCdSituacaoVinculacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoConta()
    {
        return this._has_cdSituacaoVinculacaoConta;
    } //-- boolean hasCdSituacaoVinculacaoConta() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinculo()
    {
        return this._has_cdTipoContratoVinculo;
    } //-- boolean hasCdTipoContratoVinculo() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSequenciaContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoVinculo()
    {
        return this._has_nrSequenciaContratoVinculo;
    } //-- boolean hasNrSequenciaContratoVinculo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdParticipante'.
     * 
     * @param cdParticipante the value of field 'cdParticipante'.
     */
    public void setCdParticipante(long cdParticipante)
    {
        this._cdParticipante = cdParticipante;
        this._has_cdParticipante = true;
    } //-- void setCdParticipante(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @param cdPessoaJuridicaVinculo the value of field
     * 'cdPessoaJuridicaVinculo'.
     */
    public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo)
    {
        this._cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
        this._has_cdPessoaJuridicaVinculo = true;
    } //-- void setCdPessoaJuridicaVinculo(long) 

    /**
     * Sets the value of field 'cdSituacao'.
     * 
     * @param cdSituacao the value of field 'cdSituacao'.
     */
    public void setCdSituacao(int cdSituacao)
    {
        this._cdSituacao = cdSituacao;
        this._has_cdSituacao = true;
    } //-- void setCdSituacao(int) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @param cdSituacaoVinculacaoConta the value of field
     * 'cdSituacaoVinculacaoConta'.
     */
    public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta)
    {
        this._cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
        this._has_cdSituacaoVinculacaoConta = true;
    } //-- void setCdSituacaoVinculacaoConta(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinculo'.
     * 
     * @param cdTipoContratoVinculo the value of field
     * 'cdTipoContratoVinculo'.
     */
    public void setCdTipoContratoVinculo(int cdTipoContratoVinculo)
    {
        this._cdTipoContratoVinculo = cdTipoContratoVinculo;
        this._has_cdTipoContratoVinculo = true;
    } //-- void setCdTipoContratoVinculo(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacao'.
     * 
     * @param dsSituacao the value of field 'dsSituacao'.
     */
    public void setDsSituacao(java.lang.String dsSituacao)
    {
        this._dsSituacao = dsSituacao;
    } //-- void setDsSituacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoVinculacaoConta'.
     * 
     * @param dsSituacaoVinculacaoConta the value of field
     * 'dsSituacaoVinculacaoConta'.
     */
    public void setDsSituacaoVinculacaoConta(java.lang.String dsSituacaoVinculacaoConta)
    {
        this._dsSituacaoVinculacaoConta = dsSituacaoVinculacaoConta;
    } //-- void setDsSituacaoVinculacaoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'nmParticipante'.
     * 
     * @param nmParticipante the value of field 'nmParticipante'.
     */
    public void setNmParticipante(java.lang.String nmParticipante)
    {
        this._nmParticipante = nmParticipante;
    } //-- void setNmParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @param nrSequenciaContratoVinculo the value of field
     * 'nrSequenciaContratoVinculo'.
     */
    public void setNrSequenciaContratoVinculo(long nrSequenciaContratoVinculo)
    {
        this._nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
        this._has_nrSequenciaContratoVinculo = true;
    } //-- void setNrSequenciaContratoVinculo(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
