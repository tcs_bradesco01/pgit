/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharHistConsultaSaldoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistConsultaSaldoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dsBanco. */
	private String dsBanco;
	
	/** Atributo dsAgencia. */
	private String dsAgencia;
	
	/** Atributo dsTipoConta. */
	private String dsTipoConta;
	
	/** Atributo nrArquivoRemessaPagamento. */
	private String nrArquivoRemessaPagamento;
	
	/** Atributo cdTipoCanal. */
	private Integer cdTipoCanal;
	
	/** Atributo cdPessoaLoteRemessa. */
	private String cdPessoaLoteRemessa;
	
	/** Atributo cdListaDebitoPagamento. */
	private String cdListaDebitoPagamento;
	
	/** Atributo cdCanal. */
	private Integer cdCanal;
	
	/** Atributo dsCanal. */
	private String dsCanal;
	
	/** Atributo dtCreditoPagamento. */
	private String dtCreditoPagamento;
	
	/** Atributo dtVencimento. */
	private String dtVencimento;
	
	/** Atributo sinal. */
	private String sinal;
	
	/** Atributo vlPagamento. */
	private BigDecimal vlPagamento;
	
	/** Atributo vlSaldoAnteriorLancamento. */
	private BigDecimal vlSaldoAnteriorLancamento;
	
	/** Atributo vlSaldoOperacional. */
	private BigDecimal vlSaldoOperacional;
	
	/** Atributo vlSaldoSemReserva. */
	private BigDecimal vlSaldoSemReserva;
	
	/** Atributo vlSaldoComReserva. */
	private BigDecimal vlSaldoComReserva;
	
	/** Atributo vlSaldoVinculadoJudicial. */
	private BigDecimal vlSaldoVinculadoJudicial;
	
	/** Atributo vlSaldoVinculadoAdministrativo. */
	private BigDecimal vlSaldoVinculadoAdministrativo;
	
	/** Atributo vlSaldoVinculadoSeguranca. */
	private BigDecimal vlSaldoVinculadoSeguranca;
	
	/** Atributo vlSaldoVinculadoAdministrativoLp. */
	private BigDecimal vlSaldoVinculadoAdministrativoLp;
	
	/** Atributo vlSaldoAgregadoFundo. */
	private BigDecimal vlSaldoAgregadoFundo;
	
	/** Atributo vlSaldoAgregadoCdb. */
	private BigDecimal vlSaldoAgregadoCdb;
	
	/** Atributo vlSaldoAgregadoPoupanca. */
	private BigDecimal vlSaldoAgregadoPoupanca;
	
	/** Atributo vlSaldoCreditoRotativo. */
	private BigDecimal vlSaldoCreditoRotativo;
	
	/** Atributo dtInclusao. */
	private String dtInclusao;
	
	/** Atributo hrInclusao. */
	private String hrInclusao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo cdFluxoInclusao. */
	private String cdFluxoInclusao;
	
	/** Atributo dtManutencao. */
	private String dtManutencao;
	
	/** Atributo hrManutencao. */
	private String hrManutencao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;
	
	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;
	
	/** Atributo cdFluxoManutencao. */
	private String cdFluxoManutencao;
	
	/** Atributo numControleInternoLote. */
	private Long numControleInternoLote;

	/**
	 * Get: cdCanal.
	 *
	 * @return cdCanal
	 */
	public Integer getCdCanal() {
		return cdCanal;
	}

	/**
	 * Set: cdCanal.
	 *
	 * @param cdCanal the cd canal
	 */
	public void setCdCanal(Integer cdCanal) {
		this.cdCanal = cdCanal;
	}

	/**
	 * Get: cdFluxoInclusao.
	 *
	 * @return cdFluxoInclusao
	 */
	public String getCdFluxoInclusao() {
		return cdFluxoInclusao;
	}

	/**
	 * Set: cdFluxoInclusao.
	 *
	 * @param cdFluxoInclusao the cd fluxo inclusao
	 */
	public void setCdFluxoInclusao(String cdFluxoInclusao) {
		this.cdFluxoInclusao = cdFluxoInclusao;
	}

	/**
	 * Get: cdFluxoManutencao.
	 *
	 * @return cdFluxoManutencao
	 */
	public String getCdFluxoManutencao() {
		return cdFluxoManutencao;
	}

	/**
	 * Set: cdFluxoManutencao.
	 *
	 * @param cdFluxoManutencao the cd fluxo manutencao
	 */
	public void setCdFluxoManutencao(String cdFluxoManutencao) {
		this.cdFluxoManutencao = cdFluxoManutencao;
	}

	/**
	 * Get: cdListaDebitoPagamento.
	 *
	 * @return cdListaDebitoPagamento
	 */
	public String getCdListaDebitoPagamento() {
		return cdListaDebitoPagamento;
	}

	/**
	 * Set: cdListaDebitoPagamento.
	 *
	 * @param cdListaDebitoPagamento the cd lista debito pagamento
	 */
	public void setCdListaDebitoPagamento(String cdListaDebitoPagamento) {
		this.cdListaDebitoPagamento = cdListaDebitoPagamento;
	}

	/**
	 * Get: cdPessoaLoteRemessa.
	 *
	 * @return cdPessoaLoteRemessa
	 */
	public String getCdPessoaLoteRemessa() {
		return cdPessoaLoteRemessa;
	}

	/**
	 * Set: cdPessoaLoteRemessa.
	 *
	 * @param cdPessoaLoteRemessa the cd pessoa lote remessa
	 */
	public void setCdPessoaLoteRemessa(String cdPessoaLoteRemessa) {
		this.cdPessoaLoteRemessa = cdPessoaLoteRemessa;
	}

	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}

	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}

	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}

	/**
	 * Get: dsCanal.
	 *
	 * @return dsCanal
	 */
	public String getDsCanal() {
		return dsCanal;
	}

	/**
	 * Set: dsCanal.
	 *
	 * @param dsCanal the ds canal
	 */
	public void setDsCanal(String dsCanal) {
		this.dsCanal = dsCanal;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

	/**
	 * Get: dtCreditoPagamento.
	 *
	 * @return dtCreditoPagamento
	 */
	public String getDtCreditoPagamento() {
		return dtCreditoPagamento;
	}

	/**
	 * Set: dtCreditoPagamento.
	 *
	 * @param dtCreditoPagamento the dt credito pagamento
	 */
	public void setDtCreditoPagamento(String dtCreditoPagamento) {
		this.dtCreditoPagamento = dtCreditoPagamento;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrArquivoRemessaPagamento.
	 *
	 * @return nrArquivoRemessaPagamento
	 */
	public String getNrArquivoRemessaPagamento() {
		return nrArquivoRemessaPagamento;
	}

	/**
	 * Set: nrArquivoRemessaPagamento.
	 *
	 * @param nrArquivoRemessaPagamento the nr arquivo remessa pagamento
	 */
	public void setNrArquivoRemessaPagamento(String nrArquivoRemessaPagamento) {
		this.nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
	}

	/**
	 * Get: vlPagamento.
	 *
	 * @return vlPagamento
	 */
	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}

	/**
	 * Set: vlPagamento.
	 *
	 * @param vlPagamento the vl pagamento
	 */
	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}

	/**
	 * Get: vlSaldoAgregadoCdb.
	 *
	 * @return vlSaldoAgregadoCdb
	 */
	public BigDecimal getVlSaldoAgregadoCdb() {
		return vlSaldoAgregadoCdb;
	}

	/**
	 * Set: vlSaldoAgregadoCdb.
	 *
	 * @param vlSaldoAgregadoCdb the vl saldo agregado cdb
	 */
	public void setVlSaldoAgregadoCdb(BigDecimal vlSaldoAgregadoCdb) {
		this.vlSaldoAgregadoCdb = vlSaldoAgregadoCdb;
	}

	/**
	 * Get: vlSaldoAgregadoFundo.
	 *
	 * @return vlSaldoAgregadoFundo
	 */
	public BigDecimal getVlSaldoAgregadoFundo() {
		return vlSaldoAgregadoFundo;
	}

	/**
	 * Set: vlSaldoAgregadoFundo.
	 *
	 * @param vlSaldoAgregadoFundo the vl saldo agregado fundo
	 */
	public void setVlSaldoAgregadoFundo(BigDecimal vlSaldoAgregadoFundo) {
		this.vlSaldoAgregadoFundo = vlSaldoAgregadoFundo;
	}

	/**
	 * Get: vlSaldoAgregadoPoupanca.
	 *
	 * @return vlSaldoAgregadoPoupanca
	 */
	public BigDecimal getVlSaldoAgregadoPoupanca() {
		return vlSaldoAgregadoPoupanca;
	}

	/**
	 * Set: vlSaldoAgregadoPoupanca.
	 *
	 * @param vlSaldoAgregadoPoupanca the vl saldo agregado poupanca
	 */
	public void setVlSaldoAgregadoPoupanca(BigDecimal vlSaldoAgregadoPoupanca) {
		this.vlSaldoAgregadoPoupanca = vlSaldoAgregadoPoupanca;
	}

	/**
	 * Get: vlSaldoAnteriorLancamento.
	 *
	 * @return vlSaldoAnteriorLancamento
	 */
	public BigDecimal getVlSaldoAnteriorLancamento() {
		return vlSaldoAnteriorLancamento;
	}

	/**
	 * Set: vlSaldoAnteriorLancamento.
	 *
	 * @param vlSaldoAnteriorLancamento the vl saldo anterior lancamento
	 */
	public void setVlSaldoAnteriorLancamento(
			BigDecimal vlSaldoAnteriorLancamento) {
		this.vlSaldoAnteriorLancamento = vlSaldoAnteriorLancamento;
	}

	/**
	 * Get: vlSaldoComReserva.
	 *
	 * @return vlSaldoComReserva
	 */
	public BigDecimal getVlSaldoComReserva() {
		return vlSaldoComReserva;
	}

	/**
	 * Set: vlSaldoComReserva.
	 *
	 * @param vlSaldoComReserva the vl saldo com reserva
	 */
	public void setVlSaldoComReserva(BigDecimal vlSaldoComReserva) {
		this.vlSaldoComReserva = vlSaldoComReserva;
	}

	/**
	 * Get: vlSaldoCreditoRotativo.
	 *
	 * @return vlSaldoCreditoRotativo
	 */
	public BigDecimal getVlSaldoCreditoRotativo() {
		return vlSaldoCreditoRotativo;
	}

	/**
	 * Set: vlSaldoCreditoRotativo.
	 *
	 * @param vlSaldoCreditoRotativo the vl saldo credito rotativo
	 */
	public void setVlSaldoCreditoRotativo(BigDecimal vlSaldoCreditoRotativo) {
		this.vlSaldoCreditoRotativo = vlSaldoCreditoRotativo;
	}

	/**
	 * Get: vlSaldoOperacional.
	 *
	 * @return vlSaldoOperacional
	 */
	public BigDecimal getVlSaldoOperacional() {
		return vlSaldoOperacional;
	}

	/**
	 * Set: vlSaldoOperacional.
	 *
	 * @param vlSaldoOperacional the vl saldo operacional
	 */
	public void setVlSaldoOperacional(BigDecimal vlSaldoOperacional) {
		this.vlSaldoOperacional = vlSaldoOperacional;
	}

	/**
	 * Get: vlSaldoSemReserva.
	 *
	 * @return vlSaldoSemReserva
	 */
	public BigDecimal getVlSaldoSemReserva() {
		return vlSaldoSemReserva;
	}

	/**
	 * Set: vlSaldoSemReserva.
	 *
	 * @param vlSaldoSemReserva the vl saldo sem reserva
	 */
	public void setVlSaldoSemReserva(BigDecimal vlSaldoSemReserva) {
		this.vlSaldoSemReserva = vlSaldoSemReserva;
	}

	/**
	 * Get: vlSaldoVinculadoAdministrativo.
	 *
	 * @return vlSaldoVinculadoAdministrativo
	 */
	public BigDecimal getVlSaldoVinculadoAdministrativo() {
		return vlSaldoVinculadoAdministrativo;
	}

	/**
	 * Set: vlSaldoVinculadoAdministrativo.
	 *
	 * @param vlSaldoVinculadoAdministrativo the vl saldo vinculado administrativo
	 */
	public void setVlSaldoVinculadoAdministrativo(
			BigDecimal vlSaldoVinculadoAdministrativo) {
		this.vlSaldoVinculadoAdministrativo = vlSaldoVinculadoAdministrativo;
	}

	/**
	 * Get: vlSaldoVinculadoAdministrativoLp.
	 *
	 * @return vlSaldoVinculadoAdministrativoLp
	 */
	public BigDecimal getVlSaldoVinculadoAdministrativoLp() {
		return vlSaldoVinculadoAdministrativoLp;
	}

	/**
	 * Set: vlSaldoVinculadoAdministrativoLp.
	 *
	 * @param vlSaldoVinculadoAdministrativoLp the vl saldo vinculado administrativo lp
	 */
	public void setVlSaldoVinculadoAdministrativoLp(
			BigDecimal vlSaldoVinculadoAdministrativoLp) {
		this.vlSaldoVinculadoAdministrativoLp = vlSaldoVinculadoAdministrativoLp;
	}

	/**
	 * Get: vlSaldoVinculadoJudicial.
	 *
	 * @return vlSaldoVinculadoJudicial
	 */
	public BigDecimal getVlSaldoVinculadoJudicial() {
		return vlSaldoVinculadoJudicial;
	}

	/**
	 * Set: vlSaldoVinculadoJudicial.
	 *
	 * @param vlSaldoVinculadoJudicial the vl saldo vinculado judicial
	 */
	public void setVlSaldoVinculadoJudicial(BigDecimal vlSaldoVinculadoJudicial) {
		this.vlSaldoVinculadoJudicial = vlSaldoVinculadoJudicial;
	}

	/**
	 * Get: vlSaldoVinculadoSeguranca.
	 *
	 * @return vlSaldoVinculadoSeguranca
	 */
	public BigDecimal getVlSaldoVinculadoSeguranca() {
		return vlSaldoVinculadoSeguranca;
	}

	/**
	 * Set: vlSaldoVinculadoSeguranca.
	 *
	 * @param vlSaldoVinculadoSeguranca the vl saldo vinculado seguranca
	 */
	public void setVlSaldoVinculadoSeguranca(
			BigDecimal vlSaldoVinculadoSeguranca) {
		this.vlSaldoVinculadoSeguranca = vlSaldoVinculadoSeguranca;
	}

	/**
	 * Get: sinal.
	 *
	 * @return sinal
	 */
	public String getSinal() {
		return sinal;
	}

	/**
	 * Set: sinal.
	 *
	 * @param sinal the sinal
	 */
	public void setSinal(String sinal) {
		this.sinal = sinal;
	}

	/**
	 * Get: numControleInternoLote.
	 *
	 * @return numControleInternoLote
	 */
	public Long getNumControleInternoLote() {
		return numControleInternoLote;
	}

	/**
	 * Set: numControleInternoLote.
	 *
	 * @param numControleInternoLote the num controle interno lote
	 */
	public void setNumControleInternoLote(Long numControleInternoLote) {
		this.numControleInternoLote = numControleInternoLote;
	}

}
