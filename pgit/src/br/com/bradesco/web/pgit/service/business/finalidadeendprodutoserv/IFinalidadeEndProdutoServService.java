/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.DetalharFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.DetalharFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ExcluirFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ExcluirFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.IncluirFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.IncluirFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ListarFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ListarFinalidadeEndOpeSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: FinalidadeEndProdutoServ
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IFinalidadeEndProdutoServService {

 
	/**
	 * Listar finalidade endereco operacao.
	 *
	 * @param listarFinalidadeEndOpeEntradaDTO the listar finalidade end ope entrada dto
	 * @return the list< listar finalidade end ope saida dt o>
	 */
	List<ListarFinalidadeEndOpeSaidaDTO> listarFinalidadeEnderecoOperacao(ListarFinalidadeEndOpeEntradaDTO listarFinalidadeEndOpeEntradaDTO);
	
	/**
	 * Detalhar finalidade endereco operacao.
	 *
	 * @param detalharFinalidadeEndOpeEntradaDTO the detalhar finalidade end ope entrada dto
	 * @return the detalhar finalidade end ope saida dto
	 */
	DetalharFinalidadeEndOpeSaidaDTO detalharFinalidadeEnderecoOperacao(DetalharFinalidadeEndOpeEntradaDTO detalharFinalidadeEndOpeEntradaDTO);
	
	/**
	 * Incluir finalidade endereco operacao.
	 *
	 * @param incluirFinalidadeEndOpeEntradaDTO the incluir finalidade end ope entrada dto
	 * @return the incluir finalidade end ope saida dto
	 */
	IncluirFinalidadeEndOpeSaidaDTO incluirFinalidadeEnderecoOperacao(IncluirFinalidadeEndOpeEntradaDTO incluirFinalidadeEndOpeEntradaDTO);
	
	/**
	 * Excluir finalidade endereco operacao.
	 *
	 * @param excluirFinalidadeEndOpeEntradaDTO the excluir finalidade end ope entrada dto
	 * @return the excluir finalidade end ope saida dto
	 */
	ExcluirFinalidadeEndOpeSaidaDTO excluirFinalidadeEnderecoOperacao(ExcluirFinalidadeEndOpeEntradaDTO excluirFinalidadeEndOpeEntradaDTO);
	
	

}

