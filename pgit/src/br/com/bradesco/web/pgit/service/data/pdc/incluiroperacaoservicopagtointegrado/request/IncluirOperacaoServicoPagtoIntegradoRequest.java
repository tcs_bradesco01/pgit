/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirOperacaoServicoPagtoIntegradoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirOperacaoServicoPagtoIntegradoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdprodutoServicoOperacao
     */
    private int _cdprodutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdprodutoServicoOperacao
     */
    private boolean _has_cdprodutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _cdOperacaoServicoIntegrado
     */
    private int _cdOperacaoServicoIntegrado = 0;

    /**
     * keeps track of state for field: _cdOperacaoServicoIntegrado
     */
    private boolean _has_cdOperacaoServicoIntegrado;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _cdTipoAlcadaTarifa
     */
    private int _cdTipoAlcadaTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoAlcadaTarifa
     */
    private boolean _has_cdTipoAlcadaTarifa;

    /**
     * Field _vrAlcadaTarifaAgencia
     */
    private java.math.BigDecimal _vrAlcadaTarifaAgencia = new java.math.BigDecimal("0");

    /**
     * Field _pcAlcadaTarifaAgencia
     */
    private java.math.BigDecimal _pcAlcadaTarifaAgencia = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirOperacaoServicoPagtoIntegradoRequest() 
     {
        super();
        setVrAlcadaTarifaAgencia(new java.math.BigDecimal("0"));
        setPcAlcadaTarifaAgencia(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.request.IncluirOperacaoServicoPagtoIntegradoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdOperacaoServicoIntegrado
     * 
     */
    public void deleteCdOperacaoServicoIntegrado()
    {
        this._has_cdOperacaoServicoIntegrado= false;
    } //-- void deleteCdOperacaoServicoIntegrado() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdTipoAlcadaTarifa
     * 
     */
    public void deleteCdTipoAlcadaTarifa()
    {
        this._has_cdTipoAlcadaTarifa= false;
    } //-- void deleteCdTipoAlcadaTarifa() 

    /**
     * Method deleteCdprodutoServicoOperacao
     * 
     */
    public void deleteCdprodutoServicoOperacao()
    {
        this._has_cdprodutoServicoOperacao= false;
    } //-- void deleteCdprodutoServicoOperacao() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field 'cdOperacaoServicoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoServicoIntegrado'.
     */
    public int getCdOperacaoServicoIntegrado()
    {
        return this._cdOperacaoServicoIntegrado;
    } //-- int getCdOperacaoServicoIntegrado() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdTipoAlcadaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoAlcadaTarifa'.
     */
    public int getCdTipoAlcadaTarifa()
    {
        return this._cdTipoAlcadaTarifa;
    } //-- int getCdTipoAlcadaTarifa() 

    /**
     * Returns the value of field 'cdprodutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdprodutoServicoOperacao'.
     */
    public int getCdprodutoServicoOperacao()
    {
        return this._cdprodutoServicoOperacao;
    } //-- int getCdprodutoServicoOperacao() 

    /**
     * Returns the value of field 'pcAlcadaTarifaAgencia'.
     * 
     * @return BigDecimal
     * @return the value of field 'pcAlcadaTarifaAgencia'.
     */
    public java.math.BigDecimal getPcAlcadaTarifaAgencia()
    {
        return this._pcAlcadaTarifaAgencia;
    } //-- java.math.BigDecimal getPcAlcadaTarifaAgencia() 

    /**
     * Returns the value of field 'vrAlcadaTarifaAgencia'.
     * 
     * @return BigDecimal
     * @return the value of field 'vrAlcadaTarifaAgencia'.
     */
    public java.math.BigDecimal getVrAlcadaTarifaAgencia()
    {
        return this._vrAlcadaTarifaAgencia;
    } //-- java.math.BigDecimal getVrAlcadaTarifaAgencia() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdOperacaoServicoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoServicoIntegrado()
    {
        return this._has_cdOperacaoServicoIntegrado;
    } //-- boolean hasCdOperacaoServicoIntegrado() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdTipoAlcadaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAlcadaTarifa()
    {
        return this._has_cdTipoAlcadaTarifa;
    } //-- boolean hasCdTipoAlcadaTarifa() 

    /**
     * Method hasCdprodutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdprodutoServicoOperacao()
    {
        return this._has_cdprodutoServicoOperacao;
    } //-- boolean hasCdprodutoServicoOperacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdOperacaoServicoIntegrado'.
     * 
     * @param cdOperacaoServicoIntegrado the value of field
     * 'cdOperacaoServicoIntegrado'.
     */
    public void setCdOperacaoServicoIntegrado(int cdOperacaoServicoIntegrado)
    {
        this._cdOperacaoServicoIntegrado = cdOperacaoServicoIntegrado;
        this._has_cdOperacaoServicoIntegrado = true;
    } //-- void setCdOperacaoServicoIntegrado(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoAlcadaTarifa'.
     * 
     * @param cdTipoAlcadaTarifa the value of field
     * 'cdTipoAlcadaTarifa'.
     */
    public void setCdTipoAlcadaTarifa(int cdTipoAlcadaTarifa)
    {
        this._cdTipoAlcadaTarifa = cdTipoAlcadaTarifa;
        this._has_cdTipoAlcadaTarifa = true;
    } //-- void setCdTipoAlcadaTarifa(int) 

    /**
     * Sets the value of field 'cdprodutoServicoOperacao'.
     * 
     * @param cdprodutoServicoOperacao the value of field
     * 'cdprodutoServicoOperacao'.
     */
    public void setCdprodutoServicoOperacao(int cdprodutoServicoOperacao)
    {
        this._cdprodutoServicoOperacao = cdprodutoServicoOperacao;
        this._has_cdprodutoServicoOperacao = true;
    } //-- void setCdprodutoServicoOperacao(int) 

    /**
     * Sets the value of field 'pcAlcadaTarifaAgencia'.
     * 
     * @param pcAlcadaTarifaAgencia the value of field
     * 'pcAlcadaTarifaAgencia'.
     */
    public void setPcAlcadaTarifaAgencia(java.math.BigDecimal pcAlcadaTarifaAgencia)
    {
        this._pcAlcadaTarifaAgencia = pcAlcadaTarifaAgencia;
    } //-- void setPcAlcadaTarifaAgencia(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vrAlcadaTarifaAgencia'.
     * 
     * @param vrAlcadaTarifaAgencia the value of field
     * 'vrAlcadaTarifaAgencia'.
     */
    public void setVrAlcadaTarifaAgencia(java.math.BigDecimal vrAlcadaTarifaAgencia)
    {
        this._vrAlcadaTarifaAgencia = vrAlcadaTarifaAgencia;
    } //-- void setVrAlcadaTarifaAgencia(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirOperacaoServicoPagtoIntegradoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.request.IncluirOperacaoServicoPagtoIntegradoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.request.IncluirOperacaoServicoPagtoIntegradoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.request.IncluirOperacaoServicoPagtoIntegradoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.request.IncluirOperacaoServicoPagtoIntegradoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
