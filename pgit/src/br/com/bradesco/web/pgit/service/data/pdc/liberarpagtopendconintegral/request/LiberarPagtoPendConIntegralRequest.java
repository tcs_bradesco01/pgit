/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class LiberarPagtoPendConIntegralRequest.
 * 
 * @version $Revision$ $Date$
 */
public class LiberarPagtoPendConIntegralRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdCnpjCliente
     */
    private long _cdCnpjCliente = 0;

    /**
     * keeps track of state for field: _cdCnpjCliente
     */
    private boolean _has_cdCnpjCliente;

    /**
     * Field _cdCnpjFilial
     */
    private int _cdCnpjFilial = 0;

    /**
     * keeps track of state for field: _cdCnpjFilial
     */
    private boolean _has_cdCnpjFilial;

    /**
     * Field _cdCnpjDigito
     */
    private int _cdCnpjDigito = 0;

    /**
     * keeps track of state for field: _cdCnpjDigito
     */
    private boolean _has_cdCnpjDigito;

    /**
     * Field _nrRemessa
     */
    private long _nrRemessa = 0;

    /**
     * keeps track of state for field: _nrRemessa
     */
    private boolean _has_nrRemessa;

    /**
     * Field _dtCredito
     */
    private java.lang.String _dtCredito;

    /**
     * Field _cdPessoaJuridicaDebito
     */
    private long _cdPessoaJuridicaDebito = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaDebito
     */
    private boolean _has_cdPessoaJuridicaDebito;

    /**
     * Field _cdTipoContratoDebito
     */
    private int _cdTipoContratoDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContratoDebito
     */
    private boolean _has_cdTipoContratoDebito;

    /**
     * Field _nrSequenciaContratoDebito
     */
    private long _nrSequenciaContratoDebito = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoDebito
     */
    private boolean _has_nrSequenciaContratoDebito;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _cdMotivoPendencia
     */
    private int _cdMotivoPendencia = 0;

    /**
     * keeps track of state for field: _cdMotivoPendencia
     */
    private boolean _has_cdMotivoPendencia;


      //----------------/
     //- Constructors -/
    //----------------/

    public LiberarPagtoPendConIntegralRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.request.LiberarPagtoPendConIntegralRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjCliente
     * 
     */
    public void deleteCdCnpjCliente()
    {
        this._has_cdCnpjCliente= false;
    } //-- void deleteCdCnpjCliente() 

    /**
     * Method deleteCdCnpjDigito
     * 
     */
    public void deleteCdCnpjDigito()
    {
        this._has_cdCnpjDigito= false;
    } //-- void deleteCdCnpjDigito() 

    /**
     * Method deleteCdCnpjFilial
     * 
     */
    public void deleteCdCnpjFilial()
    {
        this._has_cdCnpjFilial= false;
    } //-- void deleteCdCnpjFilial() 

    /**
     * Method deleteCdMotivoPendencia
     * 
     */
    public void deleteCdMotivoPendencia()
    {
        this._has_cdMotivoPendencia= false;
    } //-- void deleteCdMotivoPendencia() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaDebito
     * 
     */
    public void deleteCdPessoaJuridicaDebito()
    {
        this._has_cdPessoaJuridicaDebito= false;
    } //-- void deleteCdPessoaJuridicaDebito() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoContratoDebito
     * 
     */
    public void deleteCdTipoContratoDebito()
    {
        this._has_cdTipoContratoDebito= false;
    } //-- void deleteCdTipoContratoDebito() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrRemessa
     * 
     */
    public void deleteNrRemessa()
    {
        this._has_nrRemessa= false;
    } //-- void deleteNrRemessa() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Method deleteNrSequenciaContratoDebito
     * 
     */
    public void deleteNrSequenciaContratoDebito()
    {
        this._has_nrSequenciaContratoDebito= false;
    } //-- void deleteNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'cdCnpjCliente'.
     * 
     * @return long
     * @return the value of field 'cdCnpjCliente'.
     */
    public long getCdCnpjCliente()
    {
        return this._cdCnpjCliente;
    } //-- long getCdCnpjCliente() 

    /**
     * Returns the value of field 'cdCnpjDigito'.
     * 
     * @return int
     * @return the value of field 'cdCnpjDigito'.
     */
    public int getCdCnpjDigito()
    {
        return this._cdCnpjDigito;
    } //-- int getCdCnpjDigito() 

    /**
     * Returns the value of field 'cdCnpjFilial'.
     * 
     * @return int
     * @return the value of field 'cdCnpjFilial'.
     */
    public int getCdCnpjFilial()
    {
        return this._cdCnpjFilial;
    } //-- int getCdCnpjFilial() 

    /**
     * Returns the value of field 'cdMotivoPendencia'.
     * 
     * @return int
     * @return the value of field 'cdMotivoPendencia'.
     */
    public int getCdMotivoPendencia()
    {
        return this._cdMotivoPendencia;
    } //-- int getCdMotivoPendencia() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaDebito'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaDebito'.
     */
    public long getCdPessoaJuridicaDebito()
    {
        return this._cdPessoaJuridicaDebito;
    } //-- long getCdPessoaJuridicaDebito() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoDebito'.
     */
    public int getCdTipoContratoDebito()
    {
        return this._cdTipoContratoDebito;
    } //-- int getCdTipoContratoDebito() 

    /**
     * Returns the value of field 'dtCredito'.
     * 
     * @return String
     * @return the value of field 'dtCredito'.
     */
    public java.lang.String getDtCredito()
    {
        return this._dtCredito;
    } //-- java.lang.String getDtCredito() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrRemessa'.
     * 
     * @return long
     * @return the value of field 'nrRemessa'.
     */
    public long getNrRemessa()
    {
        return this._nrRemessa;
    } //-- long getNrRemessa() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoDebito'.
     */
    public long getNrSequenciaContratoDebito()
    {
        return this._nrSequenciaContratoDebito;
    } //-- long getNrSequenciaContratoDebito() 

    /**
     * Method hasCdCnpjCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCliente()
    {
        return this._has_cdCnpjCliente;
    } //-- boolean hasCdCnpjCliente() 

    /**
     * Method hasCdCnpjDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjDigito()
    {
        return this._has_cdCnpjDigito;
    } //-- boolean hasCdCnpjDigito() 

    /**
     * Method hasCdCnpjFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjFilial()
    {
        return this._has_cdCnpjFilial;
    } //-- boolean hasCdCnpjFilial() 

    /**
     * Method hasCdMotivoPendencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoPendencia()
    {
        return this._has_cdMotivoPendencia;
    } //-- boolean hasCdMotivoPendencia() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaDebito()
    {
        return this._has_cdPessoaJuridicaDebito;
    } //-- boolean hasCdPessoaJuridicaDebito() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoDebito()
    {
        return this._has_cdTipoContratoDebito;
    } //-- boolean hasCdTipoContratoDebito() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrRemessa()
    {
        return this._has_nrRemessa;
    } //-- boolean hasNrRemessa() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method hasNrSequenciaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoDebito()
    {
        return this._has_nrSequenciaContratoDebito;
    } //-- boolean hasNrSequenciaContratoDebito() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjCliente'.
     * 
     * @param cdCnpjCliente the value of field 'cdCnpjCliente'.
     */
    public void setCdCnpjCliente(long cdCnpjCliente)
    {
        this._cdCnpjCliente = cdCnpjCliente;
        this._has_cdCnpjCliente = true;
    } //-- void setCdCnpjCliente(long) 

    /**
     * Sets the value of field 'cdCnpjDigito'.
     * 
     * @param cdCnpjDigito the value of field 'cdCnpjDigito'.
     */
    public void setCdCnpjDigito(int cdCnpjDigito)
    {
        this._cdCnpjDigito = cdCnpjDigito;
        this._has_cdCnpjDigito = true;
    } //-- void setCdCnpjDigito(int) 

    /**
     * Sets the value of field 'cdCnpjFilial'.
     * 
     * @param cdCnpjFilial the value of field 'cdCnpjFilial'.
     */
    public void setCdCnpjFilial(int cdCnpjFilial)
    {
        this._cdCnpjFilial = cdCnpjFilial;
        this._has_cdCnpjFilial = true;
    } //-- void setCdCnpjFilial(int) 

    /**
     * Sets the value of field 'cdMotivoPendencia'.
     * 
     * @param cdMotivoPendencia the value of field
     * 'cdMotivoPendencia'.
     */
    public void setCdMotivoPendencia(int cdMotivoPendencia)
    {
        this._cdMotivoPendencia = cdMotivoPendencia;
        this._has_cdMotivoPendencia = true;
    } //-- void setCdMotivoPendencia(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaDebito'.
     * 
     * @param cdPessoaJuridicaDebito the value of field
     * 'cdPessoaJuridicaDebito'.
     */
    public void setCdPessoaJuridicaDebito(long cdPessoaJuridicaDebito)
    {
        this._cdPessoaJuridicaDebito = cdPessoaJuridicaDebito;
        this._has_cdPessoaJuridicaDebito = true;
    } //-- void setCdPessoaJuridicaDebito(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoDebito'.
     * 
     * @param cdTipoContratoDebito the value of field
     * 'cdTipoContratoDebito'.
     */
    public void setCdTipoContratoDebito(int cdTipoContratoDebito)
    {
        this._cdTipoContratoDebito = cdTipoContratoDebito;
        this._has_cdTipoContratoDebito = true;
    } //-- void setCdTipoContratoDebito(int) 

    /**
     * Sets the value of field 'dtCredito'.
     * 
     * @param dtCredito the value of field 'dtCredito'.
     */
    public void setDtCredito(java.lang.String dtCredito)
    {
        this._dtCredito = dtCredito;
    } //-- void setDtCredito(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrRemessa'.
     * 
     * @param nrRemessa the value of field 'nrRemessa'.
     */
    public void setNrRemessa(long nrRemessa)
    {
        this._nrRemessa = nrRemessa;
        this._has_nrRemessa = true;
    } //-- void setNrRemessa(long) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoDebito'.
     * 
     * @param nrSequenciaContratoDebito the value of field
     * 'nrSequenciaContratoDebito'.
     */
    public void setNrSequenciaContratoDebito(long nrSequenciaContratoDebito)
    {
        this._nrSequenciaContratoDebito = nrSequenciaContratoDebito;
        this._has_nrSequenciaContratoDebito = true;
    } //-- void setNrSequenciaContratoDebito(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return LiberarPagtoPendConIntegralRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.request.LiberarPagtoPendConIntegralRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.request.LiberarPagtoPendConIntegralRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.request.LiberarPagtoPendConIntegralRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.request.LiberarPagtoPendConIntegralRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
