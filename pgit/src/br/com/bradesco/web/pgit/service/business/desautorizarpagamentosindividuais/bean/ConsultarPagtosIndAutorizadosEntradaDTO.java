/*
 * Nome: br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.desautorizarpagamentosindividuais.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagtosIndAutorizadosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtosIndAutorizadosEntradaDTO {
    
    /** Atributo cdTipoPesquisa. */
    private Integer cdTipoPesquisa;

    /** Atributo cdAgendadosPagosNaoPagos. */
    private Integer cdAgendadosPagosNaoPagos;

    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo cdConta. */
    private Long cdConta;

    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;

    /** Atributo dtCreditoPagamentoInicio. */
    private String dtCreditoPagamentoInicio;

    /** Atributo dtCreditoPagamentoFim. */
    private String dtCreditoPagamentoFim;

    /** Atributo nrArquivoRemessaPagamento. */
    private Long nrArquivoRemessaPagamento;

    /** Atributo cdParticipante. */
    private Long cdParticipante;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;

    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;

    /** Atributo cdControlePagamentoDe. */
    private String cdControlePagamentoDe;

    /** Atributo cdContolePagamentoAte. */
    private String cdContolePagamentoAte;

    /** Atributo vlPagamentoDe. */
    private BigDecimal vlPagamentoDe;

    /** Atributo vlPagamentoAte. */
    private BigDecimal vlPagamentoAte;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;

    /** Atributo cdMotivoSituacaoPagamento. */
    private Integer cdMotivoSituacaoPagamento;

    /** Atributo cdBarraDocumento. */
    private String cdBarraDocumento;

    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;

    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;

    /** Atributo cdIdentificacaoInscricaoFavorecido. */
    private Integer cdIdentificacaoInscricaoFavorecido;

    /** Atributo cdBancoBeneficiario. */
    private Integer cdBancoBeneficiario;

    /** Atributo cdAgenciaBeneficiario. */
    private Integer cdAgenciaBeneficiario;

    /** Atributo cdContaBeneficiario. */
    private Long cdContaBeneficiario;

    /** Atributo cdDigitoContaBeneficiario. */
    private String cdDigitoContaBeneficiario;

    /** Atributo cdEmpresaContrato. */
    private Long cdEmpresaContrato;

    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;

    /** Atributo nrContrato. */
    private Long nrContrato;

    /** Atributo nrUnidadeOrganizacional. */
    private Integer nrUnidadeOrganizacional;

    /** Atributo cdBeneficio. */
    private Long cdBeneficio;

    /** Atributo cdEspecieBeneficioInss. */
    private Integer cdEspecieBeneficioInss;

    /** Atributo cdClassificacao. */
    private Integer cdClassificacao;

    /** Atributo cdLayout. */
    private String cdLayout;

    /** Atributo tamanhoLayout. */
    private Integer tamanhoLayout;

    /** Atributo cdCpfCnpjParticipante. */
    private Long cdCpfCnpjParticipante;

    /** Atributo cdFilialCnpjParticipante. */
    private Integer cdFilialCnpjParticipante;

    /** Atributo cdControleCnpjParticipante. */
    private Integer cdControleCnpjParticipante;

    /** Atributo cdPessoaContratoDebito. */
    private Long cdPessoaContratoDebito;

    /** Atributo cdTipoContratoDebito. */
    private Integer cdTipoContratoDebito;

    /** Atributo nrSequenciaContratoDebito. */
    private Long nrSequenciaContratoDebito;

    /** Atributo cdPessoaContratoCredt. */
    private Long cdPessoaContratoCredt;

    /** Atributo cdTipoContratoCredt. */
    private Integer cdTipoContratoCredt;

    /** Atributo nrSequenciaContratoCredt. */
    private Long nrSequenciaContratoCredt;

    /** Atributo cdIndiceSimulaPagamento. */
    private Integer cdIndiceSimulaPagamento;

    /** Atributo cdOrigemRecebidoEfetivacao. */
    private Integer cdOrigemRecebidoEfetivacao;
    
	/** The cd banco salarial. */
	private Integer cdBancoSalarial;
	
	/** The cd agenca salarial. */
	private Integer cdAgencaSalarial;
	
	/** The cd conta salarial. */
	private Long cdContaSalarial;
	
	private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;
    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
	return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
	this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdAgenciaBeneficiario.
     *
     * @return cdAgenciaBeneficiario
     */
    public Integer getCdAgenciaBeneficiario() {
	return cdAgenciaBeneficiario;
    }

    /**
     * Set: cdAgenciaBeneficiario.
     *
     * @param cdAgenciaBeneficiario the cd agencia beneficiario
     */
    public void setCdAgenciaBeneficiario(Integer cdAgenciaBeneficiario) {
	this.cdAgenciaBeneficiario = cdAgenciaBeneficiario;
    }

    /**
     * Get: cdAgendadosPagosNaoPagos.
     *
     * @return cdAgendadosPagosNaoPagos
     */
    public Integer getCdAgendadosPagosNaoPagos() {
	return cdAgendadosPagosNaoPagos;
    }

    /**
     * Set: cdAgendadosPagosNaoPagos.
     *
     * @param cdAgendadosPagosNaoPagos the cd agendados pagos nao pagos
     */
    public void setCdAgendadosPagosNaoPagos(Integer cdAgendadosPagosNaoPagos) {
	this.cdAgendadosPagosNaoPagos = cdAgendadosPagosNaoPagos;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
	return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
	this.cdBanco = cdBanco;
    }

    /**
     * Get: cdBancoBeneficiario.
     *
     * @return cdBancoBeneficiario
     */
    public Integer getCdBancoBeneficiario() {
	return cdBancoBeneficiario;
    }

    /**
     * Set: cdBancoBeneficiario.
     *
     * @param cdBancoBeneficiario the cd banco beneficiario
     */
    public void setCdBancoBeneficiario(Integer cdBancoBeneficiario) {
	this.cdBancoBeneficiario = cdBancoBeneficiario;
    }

    /**
     * Get: cdBarraDocumento.
     *
     * @return cdBarraDocumento
     */
    public String getCdBarraDocumento() {
	return cdBarraDocumento;
    }

    /**
     * Set: cdBarraDocumento.
     *
     * @param cdBarraDocumento the cd barra documento
     */
    public void setCdBarraDocumento(String cdBarraDocumento) {
	this.cdBarraDocumento = cdBarraDocumento;
    }

    /**
     * Get: cdBeneficio.
     *
     * @return cdBeneficio
     */
    public Long getCdBeneficio() {
	return cdBeneficio;
    }

    /**
     * Set: cdBeneficio.
     *
     * @param cdBeneficio the cd beneficio
     */
    public void setCdBeneficio(Long cdBeneficio) {
	this.cdBeneficio = cdBeneficio;
    }

    /**
     * Get: cdClassificacao.
     *
     * @return cdClassificacao
     */
    public Integer getCdClassificacao() {
	return cdClassificacao;
    }

    /**
     * Set: cdClassificacao.
     *
     * @param cdClassificacao the cd classificacao
     */
    public void setCdClassificacao(Integer cdClassificacao) {
	this.cdClassificacao = cdClassificacao;
    }

    /**
     * Get: cdEmpresaContrato.
     *
     * @return cdEmpresaContrato
     */
    public Long getCdEmpresaContrato() {
	return cdEmpresaContrato;
    }

    /**
     * Set: cdEmpresaContrato.
     *
     * @param cdEmpresaContrato the cd empresa contrato
     */
    public void setCdEmpresaContrato(Long cdEmpresaContrato) {
	this.cdEmpresaContrato = cdEmpresaContrato;
    }

    /**
     * Get: cdTipoConta.
     *
     * @return cdTipoConta
     */
    public Integer getCdTipoConta() {
	return cdTipoConta;
    }

    /**
     * Set: cdTipoConta.
     *
     * @param cdTipoConta the cd tipo conta
     */
    public void setCdTipoConta(Integer cdTipoConta) {
	this.cdTipoConta = cdTipoConta;
    }

    /**
     * Get: nrContrato.
     *
     * @return nrContrato
     */
    public Long getNrContrato() {
	return nrContrato;
    }

    /**
     * Set: nrContrato.
     *
     * @param nrContrato the nr contrato
     */
    public void setNrContrato(Long nrContrato) {
	this.nrContrato = nrContrato;
    }

    /**
     * Get: cdConta.
     *
     * @return cdConta
     */
    public Long getCdConta() {
	return cdConta;
    }

    /**
     * Set: cdConta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
	this.cdConta = cdConta;
    }

    /**
     * Get: cdContaBeneficiario.
     *
     * @return cdContaBeneficiario
     */
    public Long getCdContaBeneficiario() {
	return cdContaBeneficiario;
    }

    /**
     * Set: cdContaBeneficiario.
     *
     * @param cdContaBeneficiario the cd conta beneficiario
     */
    public void setCdContaBeneficiario(Long cdContaBeneficiario) {
	this.cdContaBeneficiario = cdContaBeneficiario;
    }

    /**
     * Get: cdContolePagamentoAte.
     *
     * @return cdContolePagamentoAte
     */
    public String getCdContolePagamentoAte() {
	return cdContolePagamentoAte;
    }

    /**
     * Set: cdContolePagamentoAte.
     *
     * @param cdContolePagamentoAte the cd contole pagamento ate
     */
    public void setCdContolePagamentoAte(String cdContolePagamentoAte) {
	this.cdContolePagamentoAte = cdContolePagamentoAte;
    }

    /**
     * Get: cdControlePagamentoDe.
     *
     * @return cdControlePagamentoDe
     */
    public String getCdControlePagamentoDe() {
	return cdControlePagamentoDe;
    }

    /**
     * Set: cdControlePagamentoDe.
     *
     * @param cdControlePagamentoDe the cd controle pagamento de
     */
    public void setCdControlePagamentoDe(String cdControlePagamentoDe) {
	this.cdControlePagamentoDe = cdControlePagamentoDe;
    }

    /**
     * Get: cdDigitoConta.
     *
     * @return cdDigitoConta
     */
    public String getCdDigitoConta() {
	return cdDigitoConta;
    }

    /**
     * Set: cdDigitoConta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
	this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Get: cdDigitoContaBeneficiario.
     *
     * @return cdDigitoContaBeneficiario
     */
    public String getCdDigitoContaBeneficiario() {
	return cdDigitoContaBeneficiario;
    }

    /**
     * Set: cdDigitoContaBeneficiario.
     *
     * @param cdDigitoContaBeneficiario the cd digito conta beneficiario
     */
    public void setCdDigitoContaBeneficiario(String cdDigitoContaBeneficiario) {
	this.cdDigitoContaBeneficiario = cdDigitoContaBeneficiario;
    }

    /**
     * Get: cdEspecieBeneficioInss.
     *
     * @return cdEspecieBeneficioInss
     */
    public Integer getCdEspecieBeneficioInss() {
	return cdEspecieBeneficioInss;
    }

    /**
     * Set: cdEspecieBeneficioInss.
     *
     * @param cdEspecieBeneficioInss the cd especie beneficio inss
     */
    public void setCdEspecieBeneficioInss(Integer cdEspecieBeneficioInss) {
	this.cdEspecieBeneficioInss = cdEspecieBeneficioInss;
    }

    /**
     * Get: cdFavorecidoClientePagador.
     *
     * @return cdFavorecidoClientePagador
     */
    public Long getCdFavorecidoClientePagador() {
	return cdFavorecidoClientePagador;
    }

    /**
     * Set: cdFavorecidoClientePagador.
     *
     * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
     */
    public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
	this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
    }

    /**
     * Get: cdIdentificacaoInscricaoFavorecido.
     *
     * @return cdIdentificacaoInscricaoFavorecido
     */
    public Integer getCdIdentificacaoInscricaoFavorecido() {
	return cdIdentificacaoInscricaoFavorecido;
    }

    /**
     * Set: cdIdentificacaoInscricaoFavorecido.
     *
     * @param cdIdentificacaoInscricaoFavorecido the cd identificacao inscricao favorecido
     */
    public void setCdIdentificacaoInscricaoFavorecido(Integer cdIdentificacaoInscricaoFavorecido) {
	this.cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public Long getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdMotivoSituacaoPagamento.
     *
     * @return cdMotivoSituacaoPagamento
     */
    public Integer getCdMotivoSituacaoPagamento() {
	return cdMotivoSituacaoPagamento;
    }

    /**
     * Set: cdMotivoSituacaoPagamento.
     *
     * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
     */
    public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
	this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    }

    /**
     * Get: cdParticipante.
     *
     * @return cdParticipante
     */
    public Long getCdParticipante() {
	return cdParticipante;
    }

    /**
     * Set: cdParticipante.
     *
     * @param cdParticipante the cd participante
     */
    public void setCdParticipante(Long cdParticipante) {
	this.cdParticipante = cdParticipante;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoRelacionado.
     *
     * @return cdProdutoServicoRelacionado
     */
    public Integer getCdProdutoServicoRelacionado() {
	return cdProdutoServicoRelacionado;
    }

    /**
     * Set: cdProdutoServicoRelacionado.
     *
     * @param cdProdutoServicoRelacionado the cd produto servico relacionado
     */
    public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
	this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public Integer getCdSituacaoOperacaoPagamento() {
	return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
	this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoPesquisa.
     *
     * @return cdTipoPesquisa
     */
    public Integer getCdTipoPesquisa() {
	return cdTipoPesquisa;
    }

    /**
     * Set: cdTipoPesquisa.
     *
     * @param cdTipoPesquisa the cd tipo pesquisa
     */
    public void setCdTipoPesquisa(Integer cdTipoPesquisa) {
	this.cdTipoPesquisa = cdTipoPesquisa;
    }

    /**
     * Get: dtCreditoPagamentoFim.
     *
     * @return dtCreditoPagamentoFim
     */
    public String getDtCreditoPagamentoFim() {
	return dtCreditoPagamentoFim;
    }

    /**
     * Set: dtCreditoPagamentoFim.
     *
     * @param dtCreditoPagamentoFim the dt credito pagamento fim
     */
    public void setDtCreditoPagamentoFim(String dtCreditoPagamentoFim) {
	this.dtCreditoPagamentoFim = dtCreditoPagamentoFim;
    }

    /**
     * Get: dtCreditoPagamentoInicio.
     *
     * @return dtCreditoPagamentoInicio
     */
    public String getDtCreditoPagamentoInicio() {
	return dtCreditoPagamentoInicio;
    }

    /**
     * Set: dtCreditoPagamentoInicio.
     *
     * @param dtCreditoPagamentoInicio the dt credito pagamento inicio
     */
    public void setDtCreditoPagamentoInicio(String dtCreditoPagamentoInicio) {
	this.dtCreditoPagamentoInicio = dtCreditoPagamentoInicio;
    }

    /**
     * Get: nrArquivoRemessaPagamento.
     *
     * @return nrArquivoRemessaPagamento
     */
    public Long getNrArquivoRemessaPagamento() {
	return nrArquivoRemessaPagamento;
    }

    /**
     * Set: nrArquivoRemessaPagamento.
     *
     * @param nrArquivoRemessaPagamento the nr arquivo remessa pagamento
     */
    public void setNrArquivoRemessaPagamento(Long nrArquivoRemessaPagamento) {
	this.nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
    }

    /**
     * Get: nrOcorrencias.
     *
     * @return nrOcorrencias
     */
    public Integer getNrOcorrencias() {
	return nrOcorrencias;
    }

    /**
     * Set: nrOcorrencias.
     *
     * @param nrOcorrencias the nr ocorrencias
     */
    public void setNrOcorrencias(Integer nrOcorrencias) {
	this.nrOcorrencias = nrOcorrencias;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
	return nrSequenciaContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
	this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrUnidadeOrganizacional.
     *
     * @return nrUnidadeOrganizacional
     */
    public Integer getNrUnidadeOrganizacional() {
	return nrUnidadeOrganizacional;
    }

    /**
     * Set: nrUnidadeOrganizacional.
     *
     * @param nrUnidadeOrganizacional the nr unidade organizacional
     */
    public void setNrUnidadeOrganizacional(Integer nrUnidadeOrganizacional) {
	this.nrUnidadeOrganizacional = nrUnidadeOrganizacional;
    }

    /**
     * Get: vlPagamentoAte.
     *
     * @return vlPagamentoAte
     */
    public BigDecimal getVlPagamentoAte() {
	return vlPagamentoAte;
    }

    /**
     * Set: vlPagamentoAte.
     *
     * @param vlPagamentoAte the vl pagamento ate
     */
    public void setVlPagamentoAte(BigDecimal vlPagamentoAte) {
	this.vlPagamentoAte = vlPagamentoAte;
    }

    /**
     * Get: vlPagamentoDe.
     *
     * @return vlPagamentoDe
     */
    public BigDecimal getVlPagamentoDe() {
	return vlPagamentoDe;
    }

    /**
     * Set: vlPagamentoDe.
     *
     * @param vlPagamentoDe the vl pagamento de
     */
    public void setVlPagamentoDe(BigDecimal vlPagamentoDe) {
	this.vlPagamentoDe = vlPagamentoDe;
    }

    /**
     * Get: cdLayout.
     *
     * @return cdLayout
     */
    public String getCdLayout() {
	return cdLayout;
    }

    /**
     * Set: cdLayout.
     *
     * @param cdLayout the cd layout
     */
    public void setCdLayout(String cdLayout) {
	this.cdLayout = cdLayout;
    }

    /**
     * Get: tamanhoLayout.
     *
     * @return tamanhoLayout
     */
    public Integer getTamanhoLayout() {
	return tamanhoLayout;
    }

    /**
     * Set: tamanhoLayout.
     *
     * @param tamanhoLayout the tamanho layout
     */
    public void setTamanhoLayout(Integer tamanhoLayout) {
	this.tamanhoLayout = tamanhoLayout;
    }

    /**
     * Get: cdControleCnpjParticipante.
     *
     * @return cdControleCnpjParticipante
     */
    public Integer getCdControleCnpjParticipante() {
	return cdControleCnpjParticipante;
    }

    /**
     * Set: cdControleCnpjParticipante.
     *
     * @param cdControleCnpjParticipante the cd controle cnpj participante
     */
    public void setCdControleCnpjParticipante(Integer cdControleCnpjParticipante) {
	this.cdControleCnpjParticipante = cdControleCnpjParticipante;
    }

    /**
     * Get: cdCpfCnpjParticipante.
     *
     * @return cdCpfCnpjParticipante
     */
    public Long getCdCpfCnpjParticipante() {
	return cdCpfCnpjParticipante;
    }

    /**
     * Set: cdCpfCnpjParticipante.
     *
     * @param cdCpfCnpjParticipante the cd cpf cnpj participante
     */
    public void setCdCpfCnpjParticipante(Long cdCpfCnpjParticipante) {
	this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
    }

    /**
     * Get: cdFilialCnpjParticipante.
     *
     * @return cdFilialCnpjParticipante
     */
    public Integer getCdFilialCnpjParticipante() {
	return cdFilialCnpjParticipante;
    }

    /**
     * Set: cdFilialCnpjParticipante.
     *
     * @param cdFilialCnpjParticipante the cd filial cnpj participante
     */
    public void setCdFilialCnpjParticipante(Integer cdFilialCnpjParticipante) {
	this.cdFilialCnpjParticipante = cdFilialCnpjParticipante;
    }

    /**
     * Get: cdIndiceSimulaPagamento.
     *
     * @return cdIndiceSimulaPagamento
     */
    public Integer getCdIndiceSimulaPagamento() {
	return cdIndiceSimulaPagamento;
    }

    /**
     * Set: cdIndiceSimulaPagamento.
     *
     * @param cdIndiceSimulaPagamento the cd indice simula pagamento
     */
    public void setCdIndiceSimulaPagamento(Integer cdIndiceSimulaPagamento) {
	this.cdIndiceSimulaPagamento = cdIndiceSimulaPagamento;
    }

    /**
     * Get: cdOrigemRecebidoEfetivacao.
     *
     * @return cdOrigemRecebidoEfetivacao
     */
    public Integer getCdOrigemRecebidoEfetivacao() {
	return cdOrigemRecebidoEfetivacao;
    }

    /**
     * Set: cdOrigemRecebidoEfetivacao.
     *
     * @param cdOrigemRecebidoEfetivacao the cd origem recebido efetivacao
     */
    public void setCdOrigemRecebidoEfetivacao(Integer cdOrigemRecebidoEfetivacao) {
	this.cdOrigemRecebidoEfetivacao = cdOrigemRecebidoEfetivacao;
    }

    /**
     * Get: cdPessoaContratoCredt.
     *
     * @return cdPessoaContratoCredt
     */
    public Long getCdPessoaContratoCredt() {
	return cdPessoaContratoCredt;
    }

    /**
     * Set: cdPessoaContratoCredt.
     *
     * @param cdPessoaContratoCredt the cd pessoa contrato credt
     */
    public void setCdPessoaContratoCredt(Long cdPessoaContratoCredt) {
	this.cdPessoaContratoCredt = cdPessoaContratoCredt;
    }

    /**
     * Get: cdPessoaContratoDebito.
     *
     * @return cdPessoaContratoDebito
     */
    public Long getCdPessoaContratoDebito() {
	return cdPessoaContratoDebito;
    }

    /**
     * Set: cdPessoaContratoDebito.
     *
     * @param cdPessoaContratoDebito the cd pessoa contrato debito
     */
    public void setCdPessoaContratoDebito(Long cdPessoaContratoDebito) {
	this.cdPessoaContratoDebito = cdPessoaContratoDebito;
    }

    /**
     * Get: cdTipoContratoCredt.
     *
     * @return cdTipoContratoCredt
     */
    public Integer getCdTipoContratoCredt() {
	return cdTipoContratoCredt;
    }

    /**
     * Set: cdTipoContratoCredt.
     *
     * @param cdTipoContratoCredt the cd tipo contrato credt
     */
    public void setCdTipoContratoCredt(Integer cdTipoContratoCredt) {
	this.cdTipoContratoCredt = cdTipoContratoCredt;
    }

    /**
     * Get: cdTipoContratoDebito.
     *
     * @return cdTipoContratoDebito
     */
    public Integer getCdTipoContratoDebito() {
	return cdTipoContratoDebito;
    }

    /**
     * Set: cdTipoContratoDebito.
     *
     * @param cdTipoContratoDebito the cd tipo contrato debito
     */
    public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
	this.cdTipoContratoDebito = cdTipoContratoDebito;
    }

    /**
     * Get: nrSequenciaContratoCredt.
     *
     * @return nrSequenciaContratoCredt
     */
    public Long getNrSequenciaContratoCredt() {
	return nrSequenciaContratoCredt;
    }

    /**
     * Set: nrSequenciaContratoCredt.
     *
     * @param nrSequenciaContratoCredt the nr sequencia contrato credt
     */
    public void setNrSequenciaContratoCredt(Long nrSequenciaContratoCredt) {
	this.nrSequenciaContratoCredt = nrSequenciaContratoCredt;
    }

    /**
     * Get: nrSequenciaContratoDebito.
     *
     * @return nrSequenciaContratoDebito
     */
    public Long getNrSequenciaContratoDebito() {
	return nrSequenciaContratoDebito;
    }

    /**
     * Set: nrSequenciaContratoDebito.
     *
     * @param nrSequenciaContratoDebito the nr sequencia contrato debito
     */
    public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
	this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
    }

	public Integer getCdBancoSalarial() {
		return cdBancoSalarial;
	}

	public void setCdBancoSalarial(Integer cdBancoSalarial) {
		this.cdBancoSalarial = cdBancoSalarial;
	}

	public Integer getCdAgencaSalarial() {
		return cdAgencaSalarial;
	}

	public void setCdAgencaSalarial(Integer cdAgencaSalarial) {
		this.cdAgencaSalarial = cdAgencaSalarial;
	}

	public Long getCdContaSalarial() {
		return cdContaSalarial;
	}

	public void setCdContaSalarial(Long cdContaSalarial) {
		this.cdContaSalarial = cdContaSalarial;
	}

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}
}