/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarImprimirPgtoIndividualSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarPagamentosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarPagamentosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoIndCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoIndCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ConsultarPagamentosIndividual
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConsultarPagamentosIndividualService {
    
     /**
      * Consultar hist consulta saldo.
      *
      * @param entradaDTO the entrada dto
      * @return the list< consultar hist consulta saldo saida dt o>
      */
     List<ConsultarHistConsultaSaldoSaidaDTO> consultarHistConsultaSaldo (ConsultarHistConsultaSaldoEntradaDTO entradaDTO);
     
     /**
      * Consultar manutencao pagamento.
      *
      * @param entradaDTO the entrada dto
      * @return the list< consultar manutencao pagamento saida dt o>
      */
     List<ConsultarManutencaoPagamentoSaidaDTO> consultarManutencaoPagamento (ConsultarManutencaoPagamentoEntradaDTO entradaDTO);
     
     /**
      * Consultar pagamentos individuais.
      *
      * @param consultarPagamentosIndividuaisEntradaDTO the consultar pagamentos individuais entrada dto
      * @return the list< consultar pagamentos individuais saida dt o>
      */
     List<ConsultarPagamentosIndividuaisSaidaDTO> consultarPagamentosIndividuais (ConsultarPagamentosIndividuaisEntradaDTO consultarPagamentosIndividuaisEntradaDTO);
   
     /**
      * Detalhar pagto credito conta.
      *
      * @param detalharPagtoCreditoContaEntradaDTO the detalhar pagto credito conta entrada dto
      * @return the detalhar pagto credito conta saida dto
      */
     DetalharPagtoCreditoContaSaidaDTO detalharPagtoCreditoConta (DetalharPagtoCreditoContaEntradaDTO detalharPagtoCreditoContaEntradaDTO);
     
     /**
      * Detalhar pagto debito conta.
      *
      * @param detalharPagtoDebitoContaEntradaDTO the detalhar pagto debito conta entrada dto
      * @return the detalhar pagto debito conta saida dto
      */
     DetalharPagtoDebitoContaSaidaDTO detalharPagtoDebitoConta (DetalharPagtoDebitoContaEntradaDTO detalharPagtoDebitoContaEntradaDTO);
     
     /**
      * Detalhar pagto ted.
      *
      * @param detalharPagtoTEDEntradaDTO the detalhar pagto ted entrada dto
      * @return the detalhar pagto ted saida dto
      */
     DetalharPagtoTEDSaidaDTO detalharPagtoTED (DetalharPagtoTEDEntradaDTO detalharPagtoTEDEntradaDTO);
     
     /**
      * Detalhar pagto doc.
      *
      * @param detalharPagtoDOCEntradaDTO the detalhar pagto doc entrada dto
      * @return the detalhar pagto doc saida dto
      */
     DetalharPagtoDOCSaidaDTO detalharPagtoDOC(DetalharPagtoDOCEntradaDTO detalharPagtoDOCEntradaDTO);
     
     /**
      * Detalhar pagto gare.
      *
      * @param detalharPagtoGAREEntradaDTO the detalhar pagto gare entrada dto
      * @return the detalhar pagto gare saida dto
      */
     DetalharPagtoGARESaidaDTO detalharPagtoGARE (DetalharPagtoGAREEntradaDTO detalharPagtoGAREEntradaDTO);
     
     /**
      * Detalhar pagto ordem pagto.
      *
      * @param detalharPagtoOrdemPagtoEntradaDTO the detalhar pagto ordem pagto entrada dto
      * @return the detalhar pagto ordem pagto saida dto
      */
     DetalharPagtoOrdemPagtoSaidaDTO detalharPagtoOrdemPagto (DetalharPagtoOrdemPagtoEntradaDTO detalharPagtoOrdemPagtoEntradaDTO);
     
     /**
      * Detalhar pagto codigo barras.
      *
      * @param detalharPagtoCodigoBarrasEntradaDTO the detalhar pagto codigo barras entrada dto
      * @return the detalhar pagto codigo barras saida dto
      */
     DetalharPagtoCodigoBarrasSaidaDTO detalharPagtoCodigoBarras (DetalharPagtoCodigoBarrasEntradaDTO detalharPagtoCodigoBarrasEntradaDTO);
     
     /**
      * Detalhar pagto debito veiculos.
      *
      * @param detalharPagtoDebitoVeiculosEntradaDTO the detalhar pagto debito veiculos entrada dto
      * @return the detalhar pagto debito veiculos saida dto
      */
     DetalharPagtoDebitoVeiculosSaidaDTO detalharPagtoDebitoVeiculos(DetalharPagtoDebitoVeiculosEntradaDTO detalharPagtoDebitoVeiculosEntradaDTO);
     
     /**
      * Detalhar pagto dep identificado.
      *
      * @param detalharPagtoDepIdentificadoEntradaDTO the detalhar pagto dep identificado entrada dto
      * @return the detalhar pagto dep identificado saida dto
      */
     DetalharPagtoDepIdentificadoSaidaDTO detalharPagtoDepIdentificado(DetalharPagtoDepIdentificadoEntradaDTO detalharPagtoDepIdentificadoEntradaDTO);
     
     /**
      * Detalhar pagto gps.
      *
      * @param detalharPagtoGPSEntradaDTO the detalhar pagto gps entrada dto
      * @return the detalhar pagto gps saida dto
      */
     DetalharPagtoGPSSaidaDTO detalharPagtoGPS(DetalharPagtoGPSEntradaDTO detalharPagtoGPSEntradaDTO);
     
     /**
      * Detalhar pagto titulo outros bancos.
      *
      * @param detalharPagtoTituloOutrosBancosEntradaDTO the detalhar pagto titulo outros bancos entrada dto
      * @return the detalhar pagto titulo outros bancos saida dto
      */
     DetalharPagtoTituloOutrosBancosSaidaDTO detalharPagtoTituloOutrosBancos(DetalharPagtoTituloOutrosBancosEntradaDTO detalharPagtoTituloOutrosBancosEntradaDTO);
     
     /**
      * Detalhar pagto darf.
      *
      * @param detalharPagtoDARFEntradaDTO the detalhar pagto darf entrada dto
      * @return the detalhar pagto darf saida dto
      */
     DetalharPagtoDARFSaidaDTO detalharPagtoDARF(DetalharPagtoDARFEntradaDTO detalharPagtoDARFEntradaDTO);
     
     /**
      * Detalhar pagto titulo bradesco.
      *
      * @param detalharPagtoTituloBradescoEntradaDTO the detalhar pagto titulo bradesco entrada dto
      * @return the detalhar pagto titulo bradesco saida dto
      */
     DetalharPagtoTituloBradescoSaidaDTO detalharPagtoTituloBradesco(DetalharPagtoTituloBradescoEntradaDTO detalharPagtoTituloBradescoEntradaDTO);
    // IN�CIO M�TODOS UTILIZADOS NO DETALHAR MANUTEN��O DE PAGAMENTOS INDIVIDUAIS
    
     /**
     * Detalhar man credito conta.
     *
     * @param detalharManPagtoIndCreditoContaEntradaDTO the detalhar man pagto ind credito conta entrada dto
     * @return the detalhar man pagto ind credito conta saida dto
     */
    DetalharManPagtoIndCreditoContaSaidaDTO detalharManCreditoConta (DetalharManPagtoIndCreditoContaEntradaDTO detalharManPagtoIndCreditoContaEntradaDTO);
     
     /**
      * Detalhar man pagto debito conta.
      *
      * @param detalharManPagtoDebitoContaEntradaDTO the detalhar man pagto debito conta entrada dto
      * @return the detalhar man pagto debito conta saida dto
      */
     DetalharManPagtoDebitoContaSaidaDTO detalharManPagtoDebitoConta(DetalharManPagtoDebitoContaEntradaDTO detalharManPagtoDebitoContaEntradaDTO);
     
     /**
      * Detalhar man pagto doc.
      *
      * @param detalharManPagtoDOCEntradaDTO the detalhar man pagto doc entrada dto
      * @return the detalhar man pagto doc saida dto
      */
     DetalharManPagtoDOCSaidaDTO detalharManPagtoDOC (DetalharManPagtoDOCEntradaDTO detalharManPagtoDOCEntradaDTO);
  	 
 	  /**
 	   * Detalhar man pagto ted.
 	   *
 	   * @param detalharManPagtoTEDEntradaDTO the detalhar man pagto ted entrada dto
 	   * @return the detalhar man pagto ted saida dto
 	   */
 	  DetalharManPagtoTEDSaidaDTO detalharManPagtoTED(DetalharManPagtoTEDEntradaDTO detalharManPagtoTEDEntradaDTO);
     
     /**
      * Detalhar man pagto titulo bradesco.
      *
      * @param detalharManPagtoTituloBradescoEntradaDTO the detalhar man pagto titulo bradesco entrada dto
      * @return the detalhar man pagto titulo bradesco saida dto
      */
     DetalharManPagtoTituloBradescoSaidaDTO detalharManPagtoTituloBradesco(DetalharManPagtoTituloBradescoEntradaDTO detalharManPagtoTituloBradescoEntradaDTO);
     
     /**
      * Detalhar man pagto titulo outros bancos.
      *
      * @param detalharManPagtoTituloOutrosBancosEntradaDTO the detalhar man pagto titulo outros bancos entrada dto
      * @return the detalhar man pagto titulo outros bancos saida dto
      */
     DetalharManPagtoTituloOutrosBancosSaidaDTO detalharManPagtoTituloOutrosBancos(DetalharManPagtoTituloOutrosBancosEntradaDTO detalharManPagtoTituloOutrosBancosEntradaDTO);
     
     /**
      * Detalhar man pagto dep identificado.
      *
      * @param detalharManPagtoDepIdentificadoEntradaDTO the detalhar man pagto dep identificado entrada dto
      * @return the detalhar man pagto dep identificado saida dto
      */
     DetalharManPagtoDepIdentificadoSaidaDTO detalharManPagtoDepIdentificado(DetalharManPagtoDepIdentificadoEntradaDTO detalharManPagtoDepIdentificadoEntradaDTO);
     
     /**
      * Detalhar man pagto ordem credito.
      *
      * @param detalharManPagtoOrdemCreditoEntradaDTO the detalhar man pagto ordem credito entrada dto
      * @return the detalhar man pagto ordem credito saida dto
      */
     DetalharManPagtoOrdemCreditoSaidaDTO detalharManPagtoOrdemCredito(DetalharManPagtoOrdemCreditoEntradaDTO detalharManPagtoOrdemCreditoEntradaDTO);
     
     /**
      * Detalhar man pagto codigo barras.
      *
      * @param detalharManPagtoCodigoBarrasEntradaDTO the detalhar man pagto codigo barras entrada dto
      * @return the detalhar man pagto codigo barras saida dto
      */
     DetalharManPagtoCodigoBarrasSaidaDTO detalharManPagtoCodigoBarras(DetalharManPagtoCodigoBarrasEntradaDTO detalharManPagtoCodigoBarrasEntradaDTO);
     
     /**
      * Detalhar man pagto debito veiculos.
      *
      * @param detalharManPagtoDebitoVeiculosEntradaDTO the detalhar man pagto debito veiculos entrada dto
      * @return the detalhar man pagto debito veiculos saida dto
      */
     DetalharManPagtoDebitoVeiculosSaidaDTO detalharManPagtoDebitoVeiculos(DetalharManPagtoDebitoVeiculosEntradaDTO detalharManPagtoDebitoVeiculosEntradaDTO);
     
     /**
      * Detalhar man pagto gare.
      *
      * @param detalharManPagtoGAREEntradaDTO the detalhar man pagto gare entrada dto
      * @return the detalhar man pagto gare saida dto
      */
     DetalharManPagtoGARESaidaDTO detalharManPagtoGARE(DetalharManPagtoGAREEntradaDTO detalharManPagtoGAREEntradaDTO);
     
     /**
      * Detalhar man pagto gps.
      *
      * @param detalharManPagtoGPSEntradaDTO the detalhar man pagto gps entrada dto
      * @return the detalhar man pagto gps saida dto
      */
     DetalharManPagtoGPSSaidaDTO detalharManPagtoGPS(DetalharManPagtoGPSEntradaDTO detalharManPagtoGPSEntradaDTO);
     
     /**
      * Detalhar man pagto darf.
      *
      * @param detalharManPagtoDARFEntradaDTO the detalhar man pagto darf entrada dto
      * @return the detalhar man pagto darf saida dto
      */
     DetalharManPagtoDARFSaidaDTO detalharManPagtoDARF(DetalharManPagtoDARFEntradaDTO detalharManPagtoDARFEntradaDTO);
     
     /**
      * Detalhar man pagto ordem pagto.
      *
      * @param detalharManPagtoOrdemPagtoEntradaDTO the detalhar man pagto ordem pagto entrada dto
      * @return the detalhar man pagto ordem pagto saida dto
      */
     DetalharManPagtoOrdemPagtoSaidaDTO detalharManPagtoOrdemPagto (DetalharManPagtoOrdemPagtoEntradaDTO detalharManPagtoOrdemPagtoEntradaDTO);
     
     /**
      * Detalhar pagto ordem credito.
      *
      * @param detalharPagtoOrdemCreditoEntradaDTO the detalhar pagto ordem credito entrada dto
      * @return the detalhar pagto ordem credito saida dto
      */
     DetalharPagtoOrdemCreditoSaidaDTO detalharPagtoOrdemCredito(DetalharPagtoOrdemCreditoEntradaDTO detalharPagtoOrdemCreditoEntradaDTO);
	 
 	/**
 	 * Consultar participante contrato.
 	 *
 	 * @param entrada the entrada
 	 * @return the list< consultar participante contrato saida dt o>
 	 */
 	List<ConsultarParticipanteContratoSaidaDTO> consultarParticipanteContrato(ConsultarParticipanteContratoEntradaDTO entrada);    
	 
 	/**
 	 * Detalhar hist consulta saldo.
 	 *
 	 * @param detalharHistConsultaSaldoEntradaDTO the detalhar hist consulta saldo entrada dto
 	 * @return the detalhar hist consulta saldo saida dto
 	 */
 	DetalharHistConsultaSaldoSaidaDTO detalharHistConsultaSaldo(DetalharHistConsultaSaldoEntradaDTO detalharHistConsultaSaldoEntradaDTO);
    // FIM M�TODOS UTILIZADOS NO DETALHAR MANUTEN��O DE PAGAMENTOS INDIVIDUAIS
 	
 	/**
 	 * Nome: onsultarImprimirPgtoIndividual
 	 * 
 	 * @param entradaDTO
 	 * @return
 	 */
 	ConsultarImprimirPgtoIndividualSaidaDTO consultarImprimirPgtoIndividual(ConsultarPagamentosIndividuaisEntradaDTO entradaDTO);
 	
 	/**
 	 * Nome: imprimirPagamentosIndividuais
 	 * 
 	 * @param entradaDTO
 	 * @return
 	 */
 	List<ConsultarPagamentosIndividuaisSaidaDTO> imprimirPagamentosIndividuais(ConsultarPagamentosIndividuaisEntradaDTO entradaDTO);

}

