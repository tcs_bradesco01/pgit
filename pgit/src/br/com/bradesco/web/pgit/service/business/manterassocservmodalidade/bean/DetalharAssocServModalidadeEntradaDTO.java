/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean;

/**
 * Nome: DetalharAssocServModalidadeEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharAssocServModalidadeEntradaDTO {

	/** Atributo tipoLayoutArquivo. */
	private int tipoLayoutArquivo;
	
	/** Atributo tipoLote. */
	private int tipoLote;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public int getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}
	
	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(int tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}
	
	/**
	 * Get: tipoLote.
	 *
	 * @return tipoLote
	 */
	public int getTipoLote() {
		return tipoLote;
	}
	
	/**
	 * Set: tipoLote.
	 *
	 * @param tipoLote the tipo lote
	 */
	public void setTipoLote(int tipoLote) {
		this.tipoLote = tipoLote;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
	
	
}
