/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: VincMsgHistCompOpeCont
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IVincMsgHistCompOpeContService {
	
	/**
	 * Listar vinculo msg hist comp operacao.
	 *
	 * @param msgVinMsgHistComOpEntradaDTO the msg vin msg hist com op entrada dto
	 * @return the list< listar vin msg hist com op saida dt o>
	 */
	List<ListarVinMsgHistComOpSaidaDTO> listarVinculoMsgHistCompOperacao(ListarVinMsgHistComOpEntradaDTO msgVinMsgHistComOpEntradaDTO);
	
	/**
	 * Listar msg hist complementar.
	 *
	 * @param msgMsgHistCompEntradaDTO the msg msg hist comp entrada dto
	 * @return the list< listar msg hist comp saida dt o>
	 */
	List<ListarMsgHistCompSaidaDTO> listarMsgHistComplementar(ListarMsgHistCompEntradaDTO msgMsgHistCompEntradaDTO);	
	
	/**
	 * Detalhar vin msg historico complementar.
	 *
	 * @param msgVinMsgHistComOpEntradaDTO the msg vin msg hist com op entrada dto
	 * @return the detalhe vin msg hist com op saida dto
	 */
	DetalheVinMsgHistComOpSaidaDTO detalharVinMsgHistoricoComplementar(DetalheVinMsgHistComOpEntradaDTO msgVinMsgHistComOpEntradaDTO);	
	
	/**
	 * Excluir vin msg historico complementar.
	 *
	 * @param msgVinMsgHistCompOpEntradaDTO the msg vin msg hist comp op entrada dto
	 * @return the excluir vin msg hist com op saida dto
	 */
	ExcluirVinMsgHistComOpSaidaDTO excluirVinMsgHistoricoComplementar(ExcluirVinMsgHistComOpEntradaDTO msgVinMsgHistCompOpEntradaDTO);
	
	/**
	 * Incluir vin msg historico complementar.
	 *
	 * @param msgVinMsgHistComOpEntradaDTO the msg vin msg hist com op entrada dto
	 * @return the incluir vin msg hist com op saida dto
	 */
	IncluirVinMsgHistComOpSaidaDTO incluirVinMsgHistoricoComplementar(IncluirVinMsgHistComOpEntradaDTO msgVinMsgHistComOpEntradaDTO);
	
	/**
	 * Detalhar msg historico complementar.
	 *
	 * @param msgMsgHistComOpEntradaDTO the msg msg hist com op entrada dto
	 * @return the detalhe msg hist com op saida dto
	 */
	DetalheMsgHistComOpSaidaDTO detalharMsgHistoricoComplementar(DetalheMsgHistComOpEntradaDTO msgMsgHistComOpEntradaDTO); 	
}

