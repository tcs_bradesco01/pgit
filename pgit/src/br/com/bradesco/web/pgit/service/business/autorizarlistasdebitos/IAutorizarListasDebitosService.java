/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.AutDesListaDebitoParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.ConsultarListaDebitoAutDesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.ConsultarListaDebitoAutDesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.DetalharListaDebitoAutDesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarlistasdebitos.bean.DetalharListaDebitoAutDesSaidaDTO;



/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AutorizarListasDebitos
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAutorizarListasDebitosService {
    
    /**
     * Consultar lista debito aut des.
     *
     * @param entradaDTO the entrada dto
     * @return the list< consultar lista debito aut des saida dt o>
     */
    List<ConsultarListaDebitoAutDesSaidaDTO> consultarListaDebitoAutDes (ConsultarListaDebitoAutDesEntradaDTO entradaDTO);
    
    /**
     * Detalhar lista debito aut des.
     *
     * @param entradaDTO the entrada dto
     * @return the list< detalhar lista debito aut des saida dt o>
     */
    List<DetalharListaDebitoAutDesSaidaDTO> detalharListaDebitoAutDes (DetalharListaDebitoAutDesEntradaDTO entradaDTO);
    
    /**
     * Aut des lista debito parcial.
     *
     * @param listaEntrada the lista entrada
     * @return the aut des lista debito parcial saida dto
     */
    AutDesListaDebitoParcialSaidaDTO autDesListaDebitoParcial (List<AutDesListaDebitoParcialEntradaDTO> listaEntrada);
    
    /**
     * Aut des lista debito integral.
     *
     * @param entradaDTO the entrada dto
     * @return the aut des lista debito integral saida dto
     */
    AutDesListaDebitoIntegralSaidaDTO autDesListaDebitoIntegral (AutDesListaDebitoIntegralEntradaDTO entradaDTO);
}

