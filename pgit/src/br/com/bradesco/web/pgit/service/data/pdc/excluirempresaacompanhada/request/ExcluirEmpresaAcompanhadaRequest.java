/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExcluirEmpresaAcompanhadaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExcluirEmpresaAcompanhadaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCnpjCpf
     */
    private long _cdCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCnpjCpf
     */
    private boolean _has_cdCnpjCpf;

    /**
     * Field _cdFilialCnpjCpf
     */
    private int _cdFilialCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjCpf
     */
    private boolean _has_cdFilialCnpjCpf;

    /**
     * Field _cdCtrlCnpjCPf
     */
    private int _cdCtrlCnpjCPf = 0;

    /**
     * keeps track of state for field: _cdCtrlCnpjCPf
     */
    private boolean _has_cdCtrlCnpjCPf;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExcluirEmpresaAcompanhadaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.request.ExcluirEmpresaAcompanhadaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjCpf
     * 
     */
    public void deleteCdCnpjCpf()
    {
        this._has_cdCnpjCpf= false;
    } //-- void deleteCdCnpjCpf() 

    /**
     * Method deleteCdCtrlCnpjCPf
     * 
     */
    public void deleteCdCtrlCnpjCPf()
    {
        this._has_cdCtrlCnpjCPf= false;
    } //-- void deleteCdCtrlCnpjCPf() 

    /**
     * Method deleteCdFilialCnpjCpf
     * 
     */
    public void deleteCdFilialCnpjCpf()
    {
        this._has_cdFilialCnpjCpf= false;
    } //-- void deleteCdFilialCnpjCpf() 

    /**
     * Returns the value of field 'cdCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCnpjCpf'.
     */
    public long getCdCnpjCpf()
    {
        return this._cdCnpjCpf;
    } //-- long getCdCnpjCpf() 

    /**
     * Returns the value of field 'cdCtrlCnpjCPf'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCnpjCPf'.
     */
    public int getCdCtrlCnpjCPf()
    {
        return this._cdCtrlCnpjCPf;
    } //-- int getCdCtrlCnpjCPf() 

    /**
     * Returns the value of field 'cdFilialCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjCpf'.
     */
    public int getCdFilialCnpjCpf()
    {
        return this._cdFilialCnpjCpf;
    } //-- int getCdFilialCnpjCpf() 

    /**
     * Method hasCdCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpf()
    {
        return this._has_cdCnpjCpf;
    } //-- boolean hasCdCnpjCpf() 

    /**
     * Method hasCdCtrlCnpjCPf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCnpjCPf()
    {
        return this._has_cdCtrlCnpjCPf;
    } //-- boolean hasCdCtrlCnpjCPf() 

    /**
     * Method hasCdFilialCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjCpf()
    {
        return this._has_cdFilialCnpjCpf;
    } //-- boolean hasCdFilialCnpjCpf() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjCpf'.
     * 
     * @param cdCnpjCpf the value of field 'cdCnpjCpf'.
     */
    public void setCdCnpjCpf(long cdCnpjCpf)
    {
        this._cdCnpjCpf = cdCnpjCpf;
        this._has_cdCnpjCpf = true;
    } //-- void setCdCnpjCpf(long) 

    /**
     * Sets the value of field 'cdCtrlCnpjCPf'.
     * 
     * @param cdCtrlCnpjCPf the value of field 'cdCtrlCnpjCPf'.
     */
    public void setCdCtrlCnpjCPf(int cdCtrlCnpjCPf)
    {
        this._cdCtrlCnpjCPf = cdCtrlCnpjCPf;
        this._has_cdCtrlCnpjCPf = true;
    } //-- void setCdCtrlCnpjCPf(int) 

    /**
     * Sets the value of field 'cdFilialCnpjCpf'.
     * 
     * @param cdFilialCnpjCpf the value of field 'cdFilialCnpjCpf'.
     */
    public void setCdFilialCnpjCpf(int cdFilialCnpjCpf)
    {
        this._cdFilialCnpjCpf = cdFilialCnpjCpf;
        this._has_cdFilialCnpjCpf = true;
    } //-- void setCdFilialCnpjCpf(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExcluirEmpresaAcompanhadaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.request.ExcluirEmpresaAcompanhadaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.request.ExcluirEmpresaAcompanhadaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.request.ExcluirEmpresaAcompanhadaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.request.ExcluirEmpresaAcompanhadaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
