/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoFavorecidos
     */
    private int _cdTipoFavorecidos = 0;

    /**
     * keeps track of state for field: _cdTipoFavorecidos
     */
    private boolean _has_cdTipoFavorecidos;

    /**
     * Field _dsTipoFavorecidos
     */
    private java.lang.String _dsTipoFavorecidos;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoFavorecidos
     * 
     */
    public void deleteCdTipoFavorecidos()
    {
        this._has_cdTipoFavorecidos= false;
    } //-- void deleteCdTipoFavorecidos() 

    /**
     * Returns the value of field 'cdTipoFavorecidos'.
     * 
     * @return int
     * @return the value of field 'cdTipoFavorecidos'.
     */
    public int getCdTipoFavorecidos()
    {
        return this._cdTipoFavorecidos;
    } //-- int getCdTipoFavorecidos() 

    /**
     * Returns the value of field 'dsTipoFavorecidos'.
     * 
     * @return String
     * @return the value of field 'dsTipoFavorecidos'.
     */
    public java.lang.String getDsTipoFavorecidos()
    {
        return this._dsTipoFavorecidos;
    } //-- java.lang.String getDsTipoFavorecidos() 

    /**
     * Method hasCdTipoFavorecidos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFavorecidos()
    {
        return this._has_cdTipoFavorecidos;
    } //-- boolean hasCdTipoFavorecidos() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoFavorecidos'.
     * 
     * @param cdTipoFavorecidos the value of field
     * 'cdTipoFavorecidos'.
     */
    public void setCdTipoFavorecidos(int cdTipoFavorecidos)
    {
        this._cdTipoFavorecidos = cdTipoFavorecidos;
        this._has_cdTipoFavorecidos = true;
    } //-- void setCdTipoFavorecidos(int) 

    /**
     * Sets the value of field 'dsTipoFavorecidos'.
     * 
     * @param dsTipoFavorecidos the value of field
     * 'dsTipoFavorecidos'.
     */
    public void setDsTipoFavorecidos(java.lang.String dsTipoFavorecidos)
    {
        this._dsTipoFavorecidos = dsTipoFavorecidos;
    } //-- void setDsTipoFavorecidos(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
