/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionada
     */
    private java.lang.String _dsProdutoOperacaoRelacionada;

    /**
     * Field _cdServicoCompostoPagamento
     */
    private long _cdServicoCompostoPagamento = 0;

    /**
     * keeps track of state for field: _cdServicoCompostoPagamento
     */
    private boolean _has_cdServicoCompostoPagamento;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;

    /**
     * Field _cdOperacaoServicoIntegrado
     */
    private int _cdOperacaoServicoIntegrado = 0;

    /**
     * keeps track of state for field: _cdOperacaoServicoIntegrado
     */
    private boolean _has_cdOperacaoServicoIntegrado;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _dsNatuzOperPgto
     */
    private java.lang.String _dsNatuzOperPgto;

    /**
     * Field _cdPessoaJuridicaVinculo
     */
    private long _cdPessoaJuridicaVinculo = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaVinculo
     */
    private boolean _has_cdPessoaJuridicaVinculo;

    /**
     * Field _cdTipoContratoVinculo
     */
    private int _cdTipoContratoVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinculo
     */
    private boolean _has_cdTipoContratoVinculo;

    /**
     * Field _nrSequenciaContratoVinculo
     */
    private long _nrSequenciaContratoVinculo = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoVinculo
     */
    private boolean _has_nrSequenciaContratoVinculo;

    /**
     * Field _cdTipoVincContrato
     */
    private int _cdTipoVincContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVincContrato
     */
    private boolean _has_cdTipoVincContrato;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private java.lang.String _cdCorpoCpfCnpj;

    /**
     * Field _cdParticipante
     */
    private long _cdParticipante = 0;

    /**
     * keeps track of state for field: _cdParticipante
     */
    private boolean _has_cdParticipante;

    /**
     * Field _nmParticipante
     */
    private java.lang.String _nmParticipante;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdOperacaoServicoIntegrado
     * 
     */
    public void deleteCdOperacaoServicoIntegrado()
    {
        this._has_cdOperacaoServicoIntegrado= false;
    } //-- void deleteCdOperacaoServicoIntegrado() 

    /**
     * Method deleteCdParticipante
     * 
     */
    public void deleteCdParticipante()
    {
        this._has_cdParticipante= false;
    } //-- void deleteCdParticipante() 

    /**
     * Method deleteCdPessoaJuridicaVinculo
     * 
     */
    public void deleteCdPessoaJuridicaVinculo()
    {
        this._has_cdPessoaJuridicaVinculo= false;
    } //-- void deleteCdPessoaJuridicaVinculo() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdServicoCompostoPagamento
     * 
     */
    public void deleteCdServicoCompostoPagamento()
    {
        this._has_cdServicoCompostoPagamento= false;
    } //-- void deleteCdServicoCompostoPagamento() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoVinculo
     * 
     */
    public void deleteCdTipoContratoVinculo()
    {
        this._has_cdTipoContratoVinculo= false;
    } //-- void deleteCdTipoContratoVinculo() 

    /**
     * Method deleteCdTipoVincContrato
     * 
     */
    public void deleteCdTipoVincContrato()
    {
        this._has_cdTipoVincContrato= false;
    } //-- void deleteCdTipoVincContrato() 

    /**
     * Method deleteNrSequenciaContratoVinculo
     * 
     */
    public void deleteNrSequenciaContratoVinculo()
    {
        this._has_nrSequenciaContratoVinculo= false;
    } //-- void deleteNrSequenciaContratoVinculo() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public java.lang.String getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- java.lang.String getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field 'cdOperacaoServicoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoServicoIntegrado'.
     */
    public int getCdOperacaoServicoIntegrado()
    {
        return this._cdOperacaoServicoIntegrado;
    } //-- int getCdOperacaoServicoIntegrado() 

    /**
     * Returns the value of field 'cdParticipante'.
     * 
     * @return long
     * @return the value of field 'cdParticipante'.
     */
    public long getCdParticipante()
    {
        return this._cdParticipante;
    } //-- long getCdParticipante() 

    /**
     * Returns the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaVinculo'.
     */
    public long getCdPessoaJuridicaVinculo()
    {
        return this._cdPessoaJuridicaVinculo;
    } //-- long getCdPessoaJuridicaVinculo() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdServicoCompostoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdServicoCompostoPagamento'.
     */
    public long getCdServicoCompostoPagamento()
    {
        return this._cdServicoCompostoPagamento;
    } //-- long getCdServicoCompostoPagamento() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinculo'.
     */
    public int getCdTipoContratoVinculo()
    {
        return this._cdTipoContratoVinculo;
    } //-- int getCdTipoContratoVinculo() 

    /**
     * Returns the value of field 'cdTipoVincContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVincContrato'.
     */
    public int getCdTipoVincContrato()
    {
        return this._cdTipoVincContrato;
    } //-- int getCdTipoVincContrato() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsNatuzOperPgto'.
     * 
     * @return String
     * @return the value of field 'dsNatuzOperPgto'.
     */
    public java.lang.String getDsNatuzOperPgto()
    {
        return this._dsNatuzOperPgto;
    } //-- java.lang.String getDsNatuzOperPgto() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionada'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionada'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionada()
    {
        return this._dsProdutoOperacaoRelacionada;
    } //-- java.lang.String getDsProdutoOperacaoRelacionada() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'nmParticipante'.
     * 
     * @return String
     * @return the value of field 'nmParticipante'.
     */
    public java.lang.String getNmParticipante()
    {
        return this._nmParticipante;
    } //-- java.lang.String getNmParticipante() 

    /**
     * Returns the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoVinculo'.
     */
    public long getNrSequenciaContratoVinculo()
    {
        return this._nrSequenciaContratoVinculo;
    } //-- long getNrSequenciaContratoVinculo() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdOperacaoServicoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoServicoIntegrado()
    {
        return this._has_cdOperacaoServicoIntegrado;
    } //-- boolean hasCdOperacaoServicoIntegrado() 

    /**
     * Method hasCdParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParticipante()
    {
        return this._has_cdParticipante;
    } //-- boolean hasCdParticipante() 

    /**
     * Method hasCdPessoaJuridicaVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaVinculo()
    {
        return this._has_cdPessoaJuridicaVinculo;
    } //-- boolean hasCdPessoaJuridicaVinculo() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdServicoCompostoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoCompostoPagamento()
    {
        return this._has_cdServicoCompostoPagamento;
    } //-- boolean hasCdServicoCompostoPagamento() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinculo()
    {
        return this._has_cdTipoContratoVinculo;
    } //-- boolean hasCdTipoContratoVinculo() 

    /**
     * Method hasCdTipoVincContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVincContrato()
    {
        return this._has_cdTipoVincContrato;
    } //-- boolean hasCdTipoVincContrato() 

    /**
     * Method hasNrSequenciaContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoVinculo()
    {
        return this._has_nrSequenciaContratoVinculo;
    } //-- boolean hasNrSequenciaContratoVinculo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(java.lang.String cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
    } //-- void setCdCorpoCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdOperacaoServicoIntegrado'.
     * 
     * @param cdOperacaoServicoIntegrado the value of field
     * 'cdOperacaoServicoIntegrado'.
     */
    public void setCdOperacaoServicoIntegrado(int cdOperacaoServicoIntegrado)
    {
        this._cdOperacaoServicoIntegrado = cdOperacaoServicoIntegrado;
        this._has_cdOperacaoServicoIntegrado = true;
    } //-- void setCdOperacaoServicoIntegrado(int) 

    /**
     * Sets the value of field 'cdParticipante'.
     * 
     * @param cdParticipante the value of field 'cdParticipante'.
     */
    public void setCdParticipante(long cdParticipante)
    {
        this._cdParticipante = cdParticipante;
        this._has_cdParticipante = true;
    } //-- void setCdParticipante(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @param cdPessoaJuridicaVinculo the value of field
     * 'cdPessoaJuridicaVinculo'.
     */
    public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo)
    {
        this._cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
        this._has_cdPessoaJuridicaVinculo = true;
    } //-- void setCdPessoaJuridicaVinculo(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdServicoCompostoPagamento'.
     * 
     * @param cdServicoCompostoPagamento the value of field
     * 'cdServicoCompostoPagamento'.
     */
    public void setCdServicoCompostoPagamento(long cdServicoCompostoPagamento)
    {
        this._cdServicoCompostoPagamento = cdServicoCompostoPagamento;
        this._has_cdServicoCompostoPagamento = true;
    } //-- void setCdServicoCompostoPagamento(long) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinculo'.
     * 
     * @param cdTipoContratoVinculo the value of field
     * 'cdTipoContratoVinculo'.
     */
    public void setCdTipoContratoVinculo(int cdTipoContratoVinculo)
    {
        this._cdTipoContratoVinculo = cdTipoContratoVinculo;
        this._has_cdTipoContratoVinculo = true;
    } //-- void setCdTipoContratoVinculo(int) 

    /**
     * Sets the value of field 'cdTipoVincContrato'.
     * 
     * @param cdTipoVincContrato the value of field
     * 'cdTipoVincContrato'.
     */
    public void setCdTipoVincContrato(int cdTipoVincContrato)
    {
        this._cdTipoVincContrato = cdTipoVincContrato;
        this._has_cdTipoVincContrato = true;
    } //-- void setCdTipoVincContrato(int) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsNatuzOperPgto'.
     * 
     * @param dsNatuzOperPgto the value of field 'dsNatuzOperPgto'.
     */
    public void setDsNatuzOperPgto(java.lang.String dsNatuzOperPgto)
    {
        this._dsNatuzOperPgto = dsNatuzOperPgto;
    } //-- void setDsNatuzOperPgto(java.lang.String) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionada'.
     * 
     * @param dsProdutoOperacaoRelacionada the value of field
     * 'dsProdutoOperacaoRelacionada'.
     */
    public void setDsProdutoOperacaoRelacionada(java.lang.String dsProdutoOperacaoRelacionada)
    {
        this._dsProdutoOperacaoRelacionada = dsProdutoOperacaoRelacionada;
    } //-- void setDsProdutoOperacaoRelacionada(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'nmParticipante'.
     * 
     * @param nmParticipante the value of field 'nmParticipante'.
     */
    public void setNmParticipante(java.lang.String nmParticipante)
    {
        this._nmParticipante = nmParticipante;
    } //-- void setNmParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @param nrSequenciaContratoVinculo the value of field
     * 'nrSequenciaContratoVinculo'.
     */
    public void setNrSequenciaContratoVinculo(long nrSequenciaContratoVinculo)
    {
        this._nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
        this._has_nrSequenciaContratoVinculo = true;
    } //-- void setNrSequenciaContratoVinculo(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
