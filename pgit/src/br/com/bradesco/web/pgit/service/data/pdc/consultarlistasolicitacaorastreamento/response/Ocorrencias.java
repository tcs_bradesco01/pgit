/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacaoPagamento
     */
    private int _cdSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSolicitacaoPagamento
     */
    private boolean _has_cdSolicitacaoPagamento;

    /**
     * Field _nrSolicitacaoPagamento
     */
    private int _nrSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _nrSolicitacaoPagamento
     */
    private boolean _has_nrSolicitacaoPagamento;

    /**
     * Field _dtSolicitacao
     */
    private java.lang.String _dtSolicitacao;

    /**
     * Field _hrSolicitacao
     */
    private java.lang.String _hrSolicitacao;

    /**
     * Field _cdSituacaoSolicitacao
     */
    private int _cdSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoSolicitacao
     */
    private boolean _has_cdSituacaoSolicitacao;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoSolicitacao
     * 
     */
    public void deleteCdSituacaoSolicitacao()
    {
        this._has_cdSituacaoSolicitacao= false;
    } //-- void deleteCdSituacaoSolicitacao() 

    /**
     * Method deleteCdSolicitacaoPagamento
     * 
     */
    public void deleteCdSolicitacaoPagamento()
    {
        this._has_cdSolicitacaoPagamento= false;
    } //-- void deleteCdSolicitacaoPagamento() 

    /**
     * Method deleteNrSolicitacaoPagamento
     * 
     */
    public void deleteNrSolicitacaoPagamento()
    {
        this._has_nrSolicitacaoPagamento= false;
    } //-- void deleteNrSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacao'.
     */
    public int getCdSituacaoSolicitacao()
    {
        return this._cdSituacaoSolicitacao;
    } //-- int getCdSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamento'.
     */
    public int getCdSolicitacaoPagamento()
    {
        return this._cdSolicitacaoPagamento;
    } //-- int getCdSolicitacaoPagamento() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dtSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacao'.
     */
    public java.lang.String getDtSolicitacao()
    {
        return this._dtSolicitacao;
    } //-- java.lang.String getDtSolicitacao() 

    /**
     * Returns the value of field 'hrSolicitacao'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacao'.
     */
    public java.lang.String getHrSolicitacao()
    {
        return this._hrSolicitacao;
    } //-- java.lang.String getHrSolicitacao() 

    /**
     * Returns the value of field 'nrSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamento'.
     */
    public int getNrSolicitacaoPagamento()
    {
        return this._nrSolicitacaoPagamento;
    } //-- int getNrSolicitacaoPagamento() 

    /**
     * Method hasCdSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacao()
    {
        return this._has_cdSituacaoSolicitacao;
    } //-- boolean hasCdSituacaoSolicitacao() 

    /**
     * Method hasCdSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamento()
    {
        return this._has_cdSolicitacaoPagamento;
    } //-- boolean hasCdSolicitacaoPagamento() 

    /**
     * Method hasNrSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamento()
    {
        return this._has_nrSolicitacaoPagamento;
    } //-- boolean hasNrSolicitacaoPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacao'.
     * 
     * @param cdSituacaoSolicitacao the value of field
     * 'cdSituacaoSolicitacao'.
     */
    public void setCdSituacaoSolicitacao(int cdSituacaoSolicitacao)
    {
        this._cdSituacaoSolicitacao = cdSituacaoSolicitacao;
        this._has_cdSituacaoSolicitacao = true;
    } //-- void setCdSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamento'.
     * 
     * @param cdSolicitacaoPagamento the value of field
     * 'cdSolicitacaoPagamento'.
     */
    public void setCdSolicitacaoPagamento(int cdSolicitacaoPagamento)
    {
        this._cdSolicitacaoPagamento = cdSolicitacaoPagamento;
        this._has_cdSolicitacaoPagamento = true;
    } //-- void setCdSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtSolicitacao'.
     * 
     * @param dtSolicitacao the value of field 'dtSolicitacao'.
     */
    public void setDtSolicitacao(java.lang.String dtSolicitacao)
    {
        this._dtSolicitacao = dtSolicitacao;
    } //-- void setDtSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacao'.
     * 
     * @param hrSolicitacao the value of field 'hrSolicitacao'.
     */
    public void setHrSolicitacao(java.lang.String hrSolicitacao)
    {
        this._hrSolicitacao = hrSolicitacao;
    } //-- void setHrSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamento'.
     * 
     * @param nrSolicitacaoPagamento the value of field
     * 'nrSolicitacaoPagamento'.
     */
    public void setNrSolicitacaoPagamento(int nrSolicitacaoPagamento)
    {
        this._nrSolicitacaoPagamento = nrSolicitacaoPagamento;
        this._has_nrSolicitacaoPagamento = true;
    } //-- void setNrSolicitacaoPagamento(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
