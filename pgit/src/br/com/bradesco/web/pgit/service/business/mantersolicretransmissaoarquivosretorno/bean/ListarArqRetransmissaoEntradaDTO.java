/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean;

/**
 * Nome: ListarArqRetransmissaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarArqRetransmissaoEntradaDTO {
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dtInicioGeracaoRetorno. */
    private String dtInicioGeracaoRetorno;
    
    /** Atributo dtFimGeracaoRetorno. */
    private String dtFimGeracaoRetorno;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo nrArquivoRetorno. */
    private Long nrArquivoRetorno;
    
    /** Atributo cdSituacaoProcessamentoRetorno. */
    private Integer cdSituacaoProcessamentoRetorno;
    
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: dtInicioGeracaoRetorno.
	 *
	 * @return dtInicioGeracaoRetorno
	 */
	public String getDtInicioGeracaoRetorno() {
		return dtInicioGeracaoRetorno;
	}
	
	/**
	 * Set: dtInicioGeracaoRetorno.
	 *
	 * @param dtInicioGeracaoRetorno the dt inicio geracao retorno
	 */
	public void setDtInicioGeracaoRetorno(String dtInicioGeracaoRetorno) {
		this.dtInicioGeracaoRetorno = dtInicioGeracaoRetorno;
	}
	
	/**
	 * Get: dtFimGeracaoRetorno.
	 *
	 * @return dtFimGeracaoRetorno
	 */
	public String getDtFimGeracaoRetorno() {
		return dtFimGeracaoRetorno;
	}
	
	/**
	 * Set: dtFimGeracaoRetorno.
	 *
	 * @param dtFimGeracaoRetorno the dt fim geracao retorno
	 */
	public void setDtFimGeracaoRetorno(String dtFimGeracaoRetorno) {
		this.dtFimGeracaoRetorno = dtFimGeracaoRetorno;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: nrArquivoRetorno.
	 *
	 * @return nrArquivoRetorno
	 */
	public Long getNrArquivoRetorno() {
		return nrArquivoRetorno;
	}
	
	/**
	 * Set: nrArquivoRetorno.
	 *
	 * @param nrArquivoRetorno the nr arquivo retorno
	 */
	public void setNrArquivoRetorno(Long nrArquivoRetorno) {
		this.nrArquivoRetorno = nrArquivoRetorno;
	}
	
	/**
	 * Get: cdSituacaoProcessamentoRetorno.
	 *
	 * @return cdSituacaoProcessamentoRetorno
	 */
	public Integer getCdSituacaoProcessamentoRetorno() {
		return cdSituacaoProcessamentoRetorno;
	}
	
	/**
	 * Set: cdSituacaoProcessamentoRetorno.
	 *
	 * @param cdSituacaoProcessamentoRetorno the cd situacao processamento retorno
	 */
	public void setCdSituacaoProcessamentoRetorno(
			Integer cdSituacaoProcessamentoRetorno) {
		this.cdSituacaoProcessamentoRetorno = cdSituacaoProcessamentoRetorno;
	}
    
}
