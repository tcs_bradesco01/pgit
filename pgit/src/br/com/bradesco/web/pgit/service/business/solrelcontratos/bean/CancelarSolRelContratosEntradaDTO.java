/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelcontratos.bean;

/**
 * Nome: CancelarSolRelContratosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CancelarSolRelContratosEntradaDTO {
	
		
	/** Atributo cdSolicitacaoPagamentoIntegrado. */
	private int cdSolicitacaoPagamentoIntegrado;
	
	/** Atributo nrSolicitacaoPagamentoIntegrado. */
	private int nrSolicitacaoPagamentoIntegrado;	
	
	/** Atributo cdTipoRelatorio. */
	private int cdTipoRelatorio;
	
	/** Atributo dataInicial. */
	private String dataInicial;
	
	/** Atributo dataFinal. */
	private String dataFinal;
	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public int getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			int cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: cdTipoRelatorio.
	 *
	 * @return cdTipoRelatorio
	 */
	public int getCdTipoRelatorio() {
		return cdTipoRelatorio;
	}
	
	/**
	 * Set: cdTipoRelatorio.
	 *
	 * @param cdTipoServico the cd tipo relatorio
	 */
	public void setCdTipoRelatorio(int cdTipoServico) {
		this.cdTipoRelatorio = cdTipoServico;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public int getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			int nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: dataInicial.
	 *
	 * @return dataInicial
	 */
	public String getDataInicial() {
		return dataInicial;
	}
	
	/**
	 * Set: dataInicial.
	 *
	 * @param dataInicial the data inicial
	 */
	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}
	
	/**
	 * Get: dataFinal.
	 *
	 * @return dataFinal
	 */
	public String getDataFinal() {
		return dataFinal;
	}
	
	/**
	 * Set: dataFinal.
	 *
	 * @param dataFinal the data final
	 */
	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	
	
}
