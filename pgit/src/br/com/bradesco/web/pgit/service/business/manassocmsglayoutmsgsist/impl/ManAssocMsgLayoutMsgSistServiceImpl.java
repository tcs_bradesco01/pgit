/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.IManAssocMsgLayoutMsgSistService;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.IManAssocMsgLayoutMsgSistServiceConstants;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ConsultarVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ConsultarVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.DetalharVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.DetalharVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ExcluirVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ExcluirVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.IncluirVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.IncluirVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarvinculacaomsglayoutsistema.request.ConsultarVinculacaoMsgLayoutSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarvinculacaomsglayoutsistema.response.ConsultarVinculacaoMsgLayoutSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.request.DetalharVinculacaoMsgLayoutSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.response.DetalharVinculacaoMsgLayoutSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculacaomsglayoutsistema.request.ExcluirVinculacaoMsgLayoutSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculacaomsglayoutsistema.response.ExcluirVinculacaoMsgLayoutSistemaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculacaomsglayoutsistema.request.IncluirVinculacaoMsgLayoutSistemaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculacaomsglayoutsistema.response.IncluirVinculacaoMsgLayoutSistemaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManAssocMsgLayoutMsgSist
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManAssocMsgLayoutMsgSistServiceImpl implements IManAssocMsgLayoutMsgSistService {
    
    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /**
         * Construtor.
         */
    public ManAssocMsgLayoutMsgSistServiceImpl() {
    }

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.IManAssocMsgLayoutMsgSistService#detalharVinculacaoMsgLayoutSistema(br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.DetalharVinculacaoMsgLayoutSistemaEntradaDTO)
     */
    public DetalharVinculacaoMsgLayoutSistemaSaidaDTO detalharVinculacaoMsgLayoutSistema(DetalharVinculacaoMsgLayoutSistemaEntradaDTO entrada) {
	DetalharVinculacaoMsgLayoutSistemaSaidaDTO saida = new DetalharVinculacaoMsgLayoutSistemaSaidaDTO();
	DetalharVinculacaoMsgLayoutSistemaRequest request = new DetalharVinculacaoMsgLayoutSistemaRequest();
	DetalharVinculacaoMsgLayoutSistemaResponse response = new DetalharVinculacaoMsgLayoutSistemaResponse();

	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setNrMensagemArquivoRetorno(PgitUtil.verificaIntegerNulo(entrada.getNrMensagemArquivoRetorno()));
	request.setCdMensagemArquivoRetorno(PgitUtil.verificaStringNula(entrada.getCdMensagemArquivoRetorno()));
	request.setCdSistema(PgitUtil.verificaStringNula(entrada.getCdSistema()));
	request.setNrEventoMensagemNegocio(PgitUtil.verificaIntegerNulo(entrada.getNrEventoMensagemNegocio()));
	request.setCdRecursoGeradorMensagem(PgitUtil.verificaIntegerNulo(entrada.getCdRecursoGeradorMensagem()));
	request.setCdIdiomaTextoMensagem(PgitUtil.verificaIntegerNulo(entrada.getCdIdiomaTextoMensagem()));

	response = getFactoryAdapter().getDetalharVinculacaoMsgLayoutSistemaPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());
	saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
	saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
	saida.setNrMensagemArquivoRetorno(response.getNrMensagemArquivoRetorno());
	saida.setCdSistema(response.getCdSistema());
	saida.setDsCodigoSistema(response.getDsCodigoSistema());
	saida.setNrEventoMensagemNegocio(response.getNrEventoMensagemNegocio());
	saida.setDsEventoMensagemNegocio(response.getDsEventoMensagemNegocio());
	saida.setCdRecursoGeradorMensagem(response.getCdRecursoGeradorMensagem());
	saida.setDsRecGedorMensagem(response.getDsRecGedorMensagem());
	saida.setCdIdiomaTextoMensagem(response.getCdIdiomaTextoMensagem());
	saida.setDsIdiomaTextoMensagem(response.getDsIdiomaTextoMensagem());
	saida.setCdMensagemArquivoRetorno(response.getCdMensagemArquivoRetorno());
	saida.setDsMensagemArquivoRetorno(response.getDsMensagemArquivoRetorno());
	saida.setCdCanalInclusao(response.getCdCanalInclusao());
	saida.setDsCanalInclusao(response.getDsCanalInclusao());
	saida.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
	saida.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
	saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
	saida.setCdCanalManutencao(response.getCdCanalManutencao());
	saida.setDsCanalManutencao(response.getDsCanalManutencao());
	saida.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
	saida.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
	saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());

	saida.setCanalInclusaoFormatado(PgitUtil.concatenarCampos(saida.getCdCanalInclusao(), saida.getDsCanalInclusao()));
	saida.setCanalManutencaoFormatado(PgitUtil.concatenarCampos(saida.getCdCanalManutencao(), saida.getDsCanalManutencao()));

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.IManAssocMsgLayoutMsgSistService#excluirVinculacaoMsgLayoutSistema(br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ExcluirVinculacaoMsgLayoutSistemaEntradaDTO)
     */
    public ExcluirVinculacaoMsgLayoutSistemaSaidaDTO excluirVinculacaoMsgLayoutSistema(ExcluirVinculacaoMsgLayoutSistemaEntradaDTO entrada) {
	ExcluirVinculacaoMsgLayoutSistemaSaidaDTO saida = new ExcluirVinculacaoMsgLayoutSistemaSaidaDTO();
	ExcluirVinculacaoMsgLayoutSistemaRequest request = new ExcluirVinculacaoMsgLayoutSistemaRequest();
	ExcluirVinculacaoMsgLayoutSistemaResponse response = new ExcluirVinculacaoMsgLayoutSistemaResponse();

	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setNrMensagemArquivoRetorno(PgitUtil.verificaIntegerNulo(entrada.getNrMensagemArquivoRetorno()));
	request.setCdSistema(PgitUtil.verificaStringNula(entrada.getCdSistema()));
	request.setNrEventoMensagemNegocio(PgitUtil.verificaIntegerNulo(entrada.getNrEventoMensagemNegocio()));
	request.setCdRecursoGeradorMensagem(PgitUtil.verificaIntegerNulo(entrada.getCdRecursoGeradorMensagem()));
	request.setCdIdiomaTextoMensagem(PgitUtil.verificaIntegerNulo(entrada.getCdIdiomaTextoMensagem()));

	response = getFactoryAdapter().getExcluirVinculacaoMsgLayoutSistemaPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.IManAssocMsgLayoutMsgSistService#incluirVinculacaoMsgLayoutSistema(br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.IncluirVinculacaoMsgLayoutSistemaEntradaDTO)
     */
    public IncluirVinculacaoMsgLayoutSistemaSaidaDTO incluirVinculacaoMsgLayoutSistema(IncluirVinculacaoMsgLayoutSistemaEntradaDTO entrada) {
	IncluirVinculacaoMsgLayoutSistemaSaidaDTO saida = new IncluirVinculacaoMsgLayoutSistemaSaidaDTO();
	IncluirVinculacaoMsgLayoutSistemaRequest request = new IncluirVinculacaoMsgLayoutSistemaRequest();
	IncluirVinculacaoMsgLayoutSistemaResponse response = new IncluirVinculacaoMsgLayoutSistemaResponse();

	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setCdSistema(PgitUtil.verificaStringNula(entrada.getCdSistema()));
	request.setCdMensagemArquivoRetorno(PgitUtil.verificaStringNula(entrada.getCdMensagemArquivoRetorno()));
	request.setCdRecursoGeradorMensagem(PgitUtil.verificaIntegerNulo(entrada.getCdRecursoGeradorMensagem()));
	request.setNrEventoMensagemNegocio(PgitUtil.verificaIntegerNulo(entrada.getNrEventoMensagemNegocio()));
	request.setCdIdiomaTextoMensagem(PgitUtil.verificaIntegerNulo(entrada.getCdIdiomaTextoMensagem()));

	response = getFactoryAdapter().getIncluirVinculacaoMsgLayoutSistemaPDCAdapter().invokeProcess(request);

	saida.setCodMensagem(response.getCodMensagem());
	saida.setMensagem(response.getMensagem());

	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.IManAssocMsgLayoutMsgSistService#consultarVinculacaoMsgLayoutSistema(br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ConsultarVinculacaoMsgLayoutSistemaEntradaDTO)
     */
    public List<ConsultarVinculacaoMsgLayoutSistemaSaidaDTO> consultarVinculacaoMsgLayoutSistema(ConsultarVinculacaoMsgLayoutSistemaEntradaDTO entrada) {
	List<ConsultarVinculacaoMsgLayoutSistemaSaidaDTO> listaSaida = new ArrayList<ConsultarVinculacaoMsgLayoutSistemaSaidaDTO>();
	ConsultarVinculacaoMsgLayoutSistemaRequest request = new ConsultarVinculacaoMsgLayoutSistemaRequest();
	ConsultarVinculacaoMsgLayoutSistemaResponse response = new ConsultarVinculacaoMsgLayoutSistemaResponse();

	request.setQtdeConsultas(PgitUtil.verificaIntegerNulo(IManAssocMsgLayoutMsgSistServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR));
	request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
	request.setCdMensagemArquivoRetorno(PgitUtil.verificaStringNula(entrada.getCdMensagemArquivoRetorno()));
	request.setCdSistema(PgitUtil.verificaStringNula(entrada.getCdSistema()));
	request.setNrEventoMensagemNegocio(PgitUtil.verificaIntegerNulo(entrada.getNrEventoMensagemNegocio()));
	request.setCdRecursoGeradorMensagem(PgitUtil.verificaIntegerNulo(entrada.getCdRecursoGeradorMensagem()));
	request.setCdIdiomaTextoMensagem(PgitUtil.verificaIntegerNulo(entrada.getCdIdiomaTextoMensagem()));

	response = getFactoryAdapter().getConsultarVinculacaoMsgLayoutSistemaPDCAdapter().invokeProcess(request);

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    ConsultarVinculacaoMsgLayoutSistemaSaidaDTO saida = new ConsultarVinculacaoMsgLayoutSistemaSaidaDTO();
	    saida.setCodMensagem(response.getCodMensagem());
	    saida.setMensagem(response.getMensagem());
	    saida.setNumeroLinhas(response.getNumeroLinhas());
	    saida.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
	    saida.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
	    saida.setNrMensagemArquivoRetorno(response.getOcorrencias(i).getNrMensagemArquivoRetorno());
	    saida.setCdMensagemArquivoRetorno(response.getOcorrencias(i).getCdMensagemArquivoRetorno());
	    saida.setDsMensagemLayoutRetorno(response.getOcorrencias(i).getDsMensagemLayoutRetorno());
	    saida.setCdSistema(response.getOcorrencias(i).getCdSistema());
	    saida.setDsCodigoSistema(response.getOcorrencias(i).getDsCodigoSistema());
	    saida.setNrEventoMensagemNegocio(response.getOcorrencias(i).getNrEventoMensagemNegocio());
	    saida.setDsEventoMensagemNegocio(response.getOcorrencias(i).getDsEventoMensagemNegocio());
	    saida.setCdRecursoGeradorMensagem(response.getOcorrencias(i).getCdRecursoGeradorMensagem());
	    saida.setDsRecGedorMensagem(response.getOcorrencias(i).getDsRecGedorMensagem());
	    saida.setCdIdiomaTextoMensagem(response.getOcorrencias(i).getCdIdiomaTextoMensagem());
	    saida.setDsIdiomaTextoMensagem(response.getOcorrencias(i).getDsIdiomaTextoMensagem());

	    saida.setTipoLayoutArquivoFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdTipoLayoutArquivo(), response
		    .getOcorrencias(i).getDsTipoLayoutArquivo()));
	    saida.setMensagemFormatada(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdMensagemArquivoRetorno(), response.getOcorrencias(i)
		    .getDsMensagemLayoutRetorno()));
	    saida.setCentroCustoFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdSistema(), response.getOcorrencias(i)
		    .getDsCodigoSistema()));
	    saida.setEventoFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(i).getNrEventoMensagemNegocio(), response.getOcorrencias(i)
		    .getDsEventoMensagemNegocio()));
	    saida.setRecursoFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdRecursoGeradorMensagem(), response.getOcorrencias(i)
		    .getDsRecGedorMensagem()));

	    listaSaida.add(saida);
	}

	return listaSaida;
    }
}
