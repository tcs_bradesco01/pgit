/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

/**
 * Nome: DetalharDadosContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharDadosContratoSaidaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
    
    /** Atributo dsPessoaJuridicaContrato. */
    private String dsPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo dsTipoContratoNegocio. */
    private String dsTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdParticipanteTitular. */
    private Long cdParticipanteTitular;
    
    /** Atributo cdCnpjCpfTitular. */
    private String cdCnpjCpfTitular;
    
    /** Atributo dsParticipanteTitular. */
    private String dsParticipanteTitular;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdSituacaoContratoNegocio. */
    private Integer cdSituacaoContratoNegocio;
    
    /** Atributo dsSituacaoContratoNegocio. */
    private String dsSituacaoContratoNegocio;
    
	/**
	 * Get: cdCnpjCpfTitular.
	 *
	 * @return cdCnpjCpfTitular
	 */
	public String getCdCnpjCpfTitular() {
		return cdCnpjCpfTitular;
	}
	
	/**
	 * Set: cdCnpjCpfTitular.
	 *
	 * @param cdCnpjCpfTitular the cd cnpj cpf titular
	 */
	public void setCdCnpjCpfTitular(String cdCnpjCpfTitular) {
		this.cdCnpjCpfTitular = cdCnpjCpfTitular;
	}
	
	/**
	 * Get: cdParticipanteTitular.
	 *
	 * @return cdParticipanteTitular
	 */
	public Long getCdParticipanteTitular() {
		return cdParticipanteTitular;
	}
	
	/**
	 * Set: cdParticipanteTitular.
	 *
	 * @param cdParticipanteTitular the cd participante titular
	 */
	public void setCdParticipanteTitular(Long cdParticipanteTitular) {
		this.cdParticipanteTitular = cdParticipanteTitular;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoContratoNegocio.
	 *
	 * @return cdSituacaoContratoNegocio
	 */
	public Integer getCdSituacaoContratoNegocio() {
		return cdSituacaoContratoNegocio;
	}
	
	/**
	 * Set: cdSituacaoContratoNegocio.
	 *
	 * @param cdSituacaoContratoNegocio the cd situacao contrato negocio
	 */
	public void setCdSituacaoContratoNegocio(Integer cdSituacaoContratoNegocio) {
		this.cdSituacaoContratoNegocio = cdSituacaoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsParticipanteTitular.
	 *
	 * @return dsParticipanteTitular
	 */
	public String getDsParticipanteTitular() {
		return dsParticipanteTitular;
	}
	
	/**
	 * Set: dsParticipanteTitular.
	 *
	 * @param dsParticipanteTitular the ds participante titular
	 */
	public void setDsParticipanteTitular(String dsParticipanteTitular) {
		this.dsParticipanteTitular = dsParticipanteTitular;
	}
	
	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}
	
	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}
	
	/**
	 * Get: dsSituacaoContratoNegocio.
	 *
	 * @return dsSituacaoContratoNegocio
	 */
	public String getDsSituacaoContratoNegocio() {
		return dsSituacaoContratoNegocio;
	}
	
	/**
	 * Set: dsSituacaoContratoNegocio.
	 *
	 * @param dsSituacaoContratoNegocio the ds situacao contrato negocio
	 */
	public void setDsSituacaoContratoNegocio(String dsSituacaoContratoNegocio) {
		this.dsSituacaoContratoNegocio = dsSituacaoContratoNegocio;
	}
	
	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}
	
	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
    
    
}
