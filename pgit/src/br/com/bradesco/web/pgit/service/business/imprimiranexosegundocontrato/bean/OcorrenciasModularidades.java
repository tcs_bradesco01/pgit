/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * Nome: OcorrenciasModularidades
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasModularidades {
	
	/** Atributo dsModTipoServico. */
	private String dsModTipoServico;
    
    /** Atributo dsModTipoModalidade. */
    private String dsModTipoModalidade;
    
    /** Atributo dsModTipoConsultaSaldo. */
    private String dsModTipoConsultaSaldo;
    
    /** Atributo dsModTipoProcessamento. */
    private String dsModTipoProcessamento;
    
    /** Atributo dsModTipoTratamentoFeriado. */
    private String dsModTipoTratamentoFeriado;
    
    /** Atributo dsModTipoRejeicaoEfetivacao. */
    private String dsModTipoRejeicaoEfetivacao;
    
    /** Atributo dsModTipoRejeicaoAgendado. */
    private String dsModTipoRejeicaoAgendado;
    
    /** Atributo cdModIndicadorPercentualMaximo. */
    private String cdModIndicadorPercentualMaximo;
    
    /** Atributo cdModIndicadorPercentualMaximoInconsistente. */
    private Integer cdModIndicadorPercentualMaximoInconsistente;
    
    /** Atributo cdModIndicadorQuantidadeMaxima. */
    private String cdModIndicadorQuantidadeMaxima;
    
    /** Atributo qtModMaximoInconsistencia. */
    private Integer qtModMaximoInconsistencia;
    
    /** Atributo cdModIndicadorValorMaximo. */
    private String cdModIndicadorValorMaximo;
    
    /** Atributo vlModMaximoFavorecidoNao. */
    private BigDecimal vlModMaximoFavorecidoNao;
    
    /** Atributo dsModPrioridadeDebito. */
    private String dsModPrioridadeDebito;
    
    /** Atributo cdModIndicadorValorDia. */
    private String cdModIndicadorValorDia;
    
    /** Atributo vlModLimiteDiario. */
    private BigDecimal vlModLimiteDiario;
    
    /** Atributo cdModIndicadorValorIndividual. */
    private String cdModIndicadorValorIndividual;
    
    /** Atributo vlModLimiteIndividual. */
    private BigDecimal vlModLimiteIndividual;
    
    /** Atributo cdModIndicadorQuantidadeFloating. */
    private String cdModIndicadorQuantidadeFloating;
    
    /** Atributo qtModDiaFloating. */
    private Integer qtModDiaFloating;
    
    /** Atributo cdModIndicadorCadastroFavorecido. */
    private String cdModIndicadorCadastroFavorecido;
    
    /** Atributo cdModUtilizacaoCadastroFavorecido. */
    private String cdModUtilizacaoCadastroFavorecido;
    
    /** Atributo cdModIndicadorQuantidadeRepique. */
    private String cdModIndicadorQuantidadeRepique;
    
    /** Atributo qtModDiaRepique. */
    private Integer qtModDiaRepique;
    
    /** The ds modalidade cferi loc. */
    private String dsModalidadeCferiLoc;  
    
    /** The ds mod tipo cons favorecido. */
    private String dsModTipoConsFavorecido;
    
	/**
	 * Get: dsModTipoServico.
	 *
	 * @return dsModTipoServico
	 */
	public String getDsModTipoServico() {
		return dsModTipoServico;
	}
	
	/**
	 * Set: dsModTipoServico.
	 *
	 * @param dsModTipoServico the ds mod tipo servico
	 */
	public void setDsModTipoServico(String dsModTipoServico) {
		this.dsModTipoServico = dsModTipoServico;
	}
	
	/**
	 * Get: dsModTipoModalidade.
	 *
	 * @return dsModTipoModalidade
	 */
	public String getDsModTipoModalidade() {
		return dsModTipoModalidade;
	}
	
	/**
	 * Set: dsModTipoModalidade.
	 *
	 * @param dsModTipoModalidade the ds mod tipo modalidade
	 */
	public void setDsModTipoModalidade(String dsModTipoModalidade) {
		this.dsModTipoModalidade = dsModTipoModalidade;
	}
	
	/**
	 * Get: dsModTipoConsultaSaldo.
	 *
	 * @return dsModTipoConsultaSaldo
	 */
	public String getDsModTipoConsultaSaldo() {
		return dsModTipoConsultaSaldo;
	}
	
	/**
	 * Set: dsModTipoConsultaSaldo.
	 *
	 * @param dsModTipoConsultaSaldo the ds mod tipo consulta saldo
	 */
	public void setDsModTipoConsultaSaldo(String dsModTipoConsultaSaldo) {
		this.dsModTipoConsultaSaldo = dsModTipoConsultaSaldo;
	}
	
	/**
	 * Get: dsModTipoProcessamento.
	 *
	 * @return dsModTipoProcessamento
	 */
	public String getDsModTipoProcessamento() {
		return dsModTipoProcessamento;
	}
	
	/**
	 * Set: dsModTipoProcessamento.
	 *
	 * @param dsModTipoProcessamento the ds mod tipo processamento
	 */
	public void setDsModTipoProcessamento(String dsModTipoProcessamento) {
		this.dsModTipoProcessamento = dsModTipoProcessamento;
	}
	
	/**
	 * Get: dsModTipoTratamentoFeriado.
	 *
	 * @return dsModTipoTratamentoFeriado
	 */
	public String getDsModTipoTratamentoFeriado() {
		return dsModTipoTratamentoFeriado;
	}
	
	/**
	 * Set: dsModTipoTratamentoFeriado.
	 *
	 * @param dsModTipoTratamentoFeriado the ds mod tipo tratamento feriado
	 */
	public void setDsModTipoTratamentoFeriado(String dsModTipoTratamentoFeriado) {
		this.dsModTipoTratamentoFeriado = dsModTipoTratamentoFeriado;
	}
	
	/**
	 * Get: dsModTipoRejeicaoEfetivacao.
	 *
	 * @return dsModTipoRejeicaoEfetivacao
	 */
	public String getDsModTipoRejeicaoEfetivacao() {
		return dsModTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Set: dsModTipoRejeicaoEfetivacao.
	 *
	 * @param dsModTipoRejeicaoEfetivacao the ds mod tipo rejeicao efetivacao
	 */
	public void setDsModTipoRejeicaoEfetivacao(String dsModTipoRejeicaoEfetivacao) {
		this.dsModTipoRejeicaoEfetivacao = dsModTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Get: dsModTipoRejeicaoAgendado.
	 *
	 * @return dsModTipoRejeicaoAgendado
	 */
	public String getDsModTipoRejeicaoAgendado() {
		return dsModTipoRejeicaoAgendado;
	}
	
	/**
	 * Set: dsModTipoRejeicaoAgendado.
	 *
	 * @param dsModTipoRejeicaoAgendado the ds mod tipo rejeicao agendado
	 */
	public void setDsModTipoRejeicaoAgendado(String dsModTipoRejeicaoAgendado) {
		this.dsModTipoRejeicaoAgendado = dsModTipoRejeicaoAgendado;
	}
	
	/**
	 * Get: cdModIndicadorPercentualMaximo.
	 *
	 * @return cdModIndicadorPercentualMaximo
	 */
	public String getCdModIndicadorPercentualMaximo() {
		return cdModIndicadorPercentualMaximo;
	}
	
	/**
	 * Set: cdModIndicadorPercentualMaximo.
	 *
	 * @param cdModIndicadorPercentualMaximo the cd mod indicador percentual maximo
	 */
	public void setCdModIndicadorPercentualMaximo(
			String cdModIndicadorPercentualMaximo) {
		this.cdModIndicadorPercentualMaximo = cdModIndicadorPercentualMaximo;
	}
	
	/**
	 * Get: cdModIndicadorPercentualMaximoInconsistente.
	 *
	 * @return cdModIndicadorPercentualMaximoInconsistente
	 */
	public Integer getCdModIndicadorPercentualMaximoInconsistente() {
		return cdModIndicadorPercentualMaximoInconsistente;
	}
	
	/**
	 * Set: cdModIndicadorPercentualMaximoInconsistente.
	 *
	 * @param cdModIndicadorPercentualMaximoInconsistente the cd mod indicador percentual maximo inconsistente
	 */
	public void setCdModIndicadorPercentualMaximoInconsistente(
			Integer cdModIndicadorPercentualMaximoInconsistente) {
		this.cdModIndicadorPercentualMaximoInconsistente = cdModIndicadorPercentualMaximoInconsistente;
	}
	
	/**
	 * Get: cdModIndicadorQuantidadeMaxima.
	 *
	 * @return cdModIndicadorQuantidadeMaxima
	 */
	public String getCdModIndicadorQuantidadeMaxima() {
		return cdModIndicadorQuantidadeMaxima;
	}
	
	/**
	 * Set: cdModIndicadorQuantidadeMaxima.
	 *
	 * @param cdModIndicadorQuantidadeMaxima the cd mod indicador quantidade maxima
	 */
	public void setCdModIndicadorQuantidadeMaxima(
			String cdModIndicadorQuantidadeMaxima) {
		this.cdModIndicadorQuantidadeMaxima = cdModIndicadorQuantidadeMaxima;
	}
	
	/**
	 * Get: qtModMaximoInconsistencia.
	 *
	 * @return qtModMaximoInconsistencia
	 */
	public Integer getQtModMaximoInconsistencia() {
		return qtModMaximoInconsistencia;
	}
	
	/**
	 * Set: qtModMaximoInconsistencia.
	 *
	 * @param qtModMaximoInconsistencia the qt mod maximo inconsistencia
	 */
	public void setQtModMaximoInconsistencia(Integer qtModMaximoInconsistencia) {
		this.qtModMaximoInconsistencia = qtModMaximoInconsistencia;
	}
	
	/**
	 * Get: cdModIndicadorValorMaximo.
	 *
	 * @return cdModIndicadorValorMaximo
	 */
	public String getCdModIndicadorValorMaximo() {
		return cdModIndicadorValorMaximo;
	}
	
	/**
	 * Set: cdModIndicadorValorMaximo.
	 *
	 * @param cdModIndicadorValorMaximo the cd mod indicador valor maximo
	 */
	public void setCdModIndicadorValorMaximo(String cdModIndicadorValorMaximo) {
		this.cdModIndicadorValorMaximo = cdModIndicadorValorMaximo;
	}
	
	/**
	 * Get: vlModMaximoFavorecidoNao.
	 *
	 * @return vlModMaximoFavorecidoNao
	 */
	public BigDecimal getVlModMaximoFavorecidoNao() {
		return vlModMaximoFavorecidoNao;
	}
	
	/**
	 * Set: vlModMaximoFavorecidoNao.
	 *
	 * @param vlModMaximoFavorecidoNao the vl mod maximo favorecido nao
	 */
	public void setVlModMaximoFavorecidoNao(BigDecimal vlModMaximoFavorecidoNao) {
		this.vlModMaximoFavorecidoNao = vlModMaximoFavorecidoNao;
	}
	
	/**
	 * Get: dsModPrioridadeDebito.
	 *
	 * @return dsModPrioridadeDebito
	 */
	public String getDsModPrioridadeDebito() {
		return dsModPrioridadeDebito;
	}
	
	/**
	 * Set: dsModPrioridadeDebito.
	 *
	 * @param dsModPrioridadeDebito the ds mod prioridade debito
	 */
	public void setDsModPrioridadeDebito(String dsModPrioridadeDebito) {
		this.dsModPrioridadeDebito = dsModPrioridadeDebito;
	}
	
	/**
	 * Get: cdModIndicadorValorDia.
	 *
	 * @return cdModIndicadorValorDia
	 */
	public String getCdModIndicadorValorDia() {
		return cdModIndicadorValorDia;
	}
	
	/**
	 * Set: cdModIndicadorValorDia.
	 *
	 * @param cdModIndicadorValorDia the cd mod indicador valor dia
	 */
	public void setCdModIndicadorValorDia(String cdModIndicadorValorDia) {
		this.cdModIndicadorValorDia = cdModIndicadorValorDia;
	}
	
	/**
	 * Get: vlModLimiteDiario.
	 *
	 * @return vlModLimiteDiario
	 */
	public BigDecimal getVlModLimiteDiario() {
		return vlModLimiteDiario;
	}
	
	/**
	 * Set: vlModLimiteDiario.
	 *
	 * @param vlModLimiteDiario the vl mod limite diario
	 */
	public void setVlModLimiteDiario(BigDecimal vlModLimiteDiario) {
		this.vlModLimiteDiario = vlModLimiteDiario;
	}
	
	/**
	 * Get: cdModIndicadorValorIndividual.
	 *
	 * @return cdModIndicadorValorIndividual
	 */
	public String getCdModIndicadorValorIndividual() {
		return cdModIndicadorValorIndividual;
	}
	
	/**
	 * Set: cdModIndicadorValorIndividual.
	 *
	 * @param cdModIndicadorValorIndividual the cd mod indicador valor individual
	 */
	public void setCdModIndicadorValorIndividual(
			String cdModIndicadorValorIndividual) {
		this.cdModIndicadorValorIndividual = cdModIndicadorValorIndividual;
	}
	
	/**
	 * Get: vlModLimiteIndividual.
	 *
	 * @return vlModLimiteIndividual
	 */
	public BigDecimal getVlModLimiteIndividual() {
		return vlModLimiteIndividual;
	}
	
	/**
	 * Set: vlModLimiteIndividual.
	 *
	 * @param vlModLimiteIndividual the vl mod limite individual
	 */
	public void setVlModLimiteIndividual(BigDecimal vlModLimiteIndividual) {
		this.vlModLimiteIndividual = vlModLimiteIndividual;
	}
	
	/**
	 * Get: cdModIndicadorQuantidadeFloating.
	 *
	 * @return cdModIndicadorQuantidadeFloating
	 */
	public String getCdModIndicadorQuantidadeFloating() {
		return cdModIndicadorQuantidadeFloating;
	}
	
	/**
	 * Set: cdModIndicadorQuantidadeFloating.
	 *
	 * @param cdModIndicadorQuantidadeFloating the cd mod indicador quantidade floating
	 */
	public void setCdModIndicadorQuantidadeFloating(
			String cdModIndicadorQuantidadeFloating) {
		this.cdModIndicadorQuantidadeFloating = cdModIndicadorQuantidadeFloating;
	}
	
	/**
	 * Get: qtModDiaFloating.
	 *
	 * @return qtModDiaFloating
	 */
	public Integer getQtModDiaFloating() {
		return qtModDiaFloating;
	}
	
	/**
	 * Set: qtModDiaFloating.
	 *
	 * @param qtModDiaFloating the qt mod dia floating
	 */
	public void setQtModDiaFloating(Integer qtModDiaFloating) {
		this.qtModDiaFloating = qtModDiaFloating;
	}
	
	/**
	 * Get: cdModIndicadorCadastroFavorecido.
	 *
	 * @return cdModIndicadorCadastroFavorecido
	 */
	public String getCdModIndicadorCadastroFavorecido() {
		return cdModIndicadorCadastroFavorecido;
	}
	
	/**
	 * Set: cdModIndicadorCadastroFavorecido.
	 *
	 * @param cdModIndicadorCadastroFavorecido the cd mod indicador cadastro favorecido
	 */
	public void setCdModIndicadorCadastroFavorecido(
			String cdModIndicadorCadastroFavorecido) {
		this.cdModIndicadorCadastroFavorecido = cdModIndicadorCadastroFavorecido;
	}
	
	/**
	 * Get: cdModUtilizacaoCadastroFavorecido.
	 *
	 * @return cdModUtilizacaoCadastroFavorecido
	 */
	public String getCdModUtilizacaoCadastroFavorecido() {
		return cdModUtilizacaoCadastroFavorecido;
	}
	
	/**
	 * Set: cdModUtilizacaoCadastroFavorecido.
	 *
	 * @param cdModUtilizacaoCadastroFavorecido the cd mod utilizacao cadastro favorecido
	 */
	public void setCdModUtilizacaoCadastroFavorecido(
			String cdModUtilizacaoCadastroFavorecido) {
		this.cdModUtilizacaoCadastroFavorecido = cdModUtilizacaoCadastroFavorecido;
	}
	
	/**
	 * Get: cdModIndicadorQuantidadeRepique.
	 *
	 * @return cdModIndicadorQuantidadeRepique
	 */
	public String getCdModIndicadorQuantidadeRepique() {
		return cdModIndicadorQuantidadeRepique;
	}
	
	/**
	 * Set: cdModIndicadorQuantidadeRepique.
	 *
	 * @param cdModIndicadorQuantidadeRepique the cd mod indicador quantidade repique
	 */
	public void setCdModIndicadorQuantidadeRepique(
			String cdModIndicadorQuantidadeRepique) {
		this.cdModIndicadorQuantidadeRepique = cdModIndicadorQuantidadeRepique;
	}
	
	/**
	 * Get: qtModDiaRepique.
	 *
	 * @return qtModDiaRepique
	 */
	public Integer getQtModDiaRepique() {
		return qtModDiaRepique;
	}
	
	/**
	 * Set: qtModDiaRepique.
	 *
	 * @param qtModDiaRepique the qt mod dia repique
	 */
	public void setQtModDiaRepique(Integer qtModDiaRepique) {
		this.qtModDiaRepique = qtModDiaRepique;
	}

	/**
	 * Gets the ds modalidade cferi loc.
	 *
	 * @return the dsModalidadeCferiLoc
	 */
	public String getDsModalidadeCferiLoc() {
		return dsModalidadeCferiLoc;
	}

	/**
	 * Sets the ds modalidade cferi loc.
	 *
	 * @param dsModalidadeCferiLoc the dsModalidadeCferiLoc to set
	 */
	public void setDsModalidadeCferiLoc(String dsModalidadeCferiLoc) {
		this.dsModalidadeCferiLoc = dsModalidadeCferiLoc;
	}

	/**
	 * Gets the ds mod tipo cons favorecido.
	 *
	 * @return the dsModTipoConsFavorecido
	 */
	public String getDsModTipoConsFavorecido() {
		return dsModTipoConsFavorecido;
	}

	/**
	 * Sets the ds mod tipo cons favorecido.
	 *
	 * @param dsModTipoConsFavorecido the dsModTipoConsFavorecido to set
	 */
	public void setDsModTipoConsFavorecido(String dsModTipoConsFavorecido) {
		this.dsModTipoConsFavorecido = dsModTipoConsFavorecido;
	}
    
}
