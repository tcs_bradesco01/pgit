/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarSolicRecuperacaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarSolicRecuperacaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtInicioSolicitacao
     */
    private java.lang.String _dtInicioSolicitacao;

    /**
     * Field _dtFinalSolicitacao
     */
    private java.lang.String _dtFinalSolicitacao;

    /**
     * Field _cdUsuarioSolicitacao
     */
    private java.lang.String _cdUsuarioSolicitacao;

    /**
     * Field _cdSituacaoSolicitacaoRecuperacao
     */
    private int _cdSituacaoSolicitacaoRecuperacao = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoRecuperacao
     */
    private boolean _has_cdSituacaoSolicitacaoRecuperacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarSolicRecuperacaoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.request.ConsultarSolicRecuperacaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoSolicitacaoRecuperacao
     * 
     */
    public void deleteCdSituacaoSolicitacaoRecuperacao()
    {
        this._has_cdSituacaoSolicitacaoRecuperacao= false;
    } //-- void deleteCdSituacaoSolicitacaoRecuperacao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field
     * 'cdSituacaoSolicitacaoRecuperacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoRecuperacao'
     */
    public int getCdSituacaoSolicitacaoRecuperacao()
    {
        return this._cdSituacaoSolicitacaoRecuperacao;
    } //-- int getCdSituacaoSolicitacaoRecuperacao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUsuarioSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioSolicitacao'.
     */
    public java.lang.String getCdUsuarioSolicitacao()
    {
        return this._cdUsuarioSolicitacao;
    } //-- java.lang.String getCdUsuarioSolicitacao() 

    /**
     * Returns the value of field 'dtFinalSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtFinalSolicitacao'.
     */
    public java.lang.String getDtFinalSolicitacao()
    {
        return this._dtFinalSolicitacao;
    } //-- java.lang.String getDtFinalSolicitacao() 

    /**
     * Returns the value of field 'dtInicioSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtInicioSolicitacao'.
     */
    public java.lang.String getDtInicioSolicitacao()
    {
        return this._dtInicioSolicitacao;
    } //-- java.lang.String getDtInicioSolicitacao() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoSolicitacaoRecuperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoRecuperacao()
    {
        return this._has_cdSituacaoSolicitacaoRecuperacao;
    } //-- boolean hasCdSituacaoSolicitacaoRecuperacao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoRecuperacao'.
     * 
     * @param cdSituacaoSolicitacaoRecuperacao the value of field
     * 'cdSituacaoSolicitacaoRecuperacao'.
     */
    public void setCdSituacaoSolicitacaoRecuperacao(int cdSituacaoSolicitacaoRecuperacao)
    {
        this._cdSituacaoSolicitacaoRecuperacao = cdSituacaoSolicitacaoRecuperacao;
        this._has_cdSituacaoSolicitacaoRecuperacao = true;
    } //-- void setCdSituacaoSolicitacaoRecuperacao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUsuarioSolicitacao'.
     * 
     * @param cdUsuarioSolicitacao the value of field
     * 'cdUsuarioSolicitacao'.
     */
    public void setCdUsuarioSolicitacao(java.lang.String cdUsuarioSolicitacao)
    {
        this._cdUsuarioSolicitacao = cdUsuarioSolicitacao;
    } //-- void setCdUsuarioSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtFinalSolicitacao'.
     * 
     * @param dtFinalSolicitacao the value of field
     * 'dtFinalSolicitacao'.
     */
    public void setDtFinalSolicitacao(java.lang.String dtFinalSolicitacao)
    {
        this._dtFinalSolicitacao = dtFinalSolicitacao;
    } //-- void setDtFinalSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioSolicitacao'.
     * 
     * @param dtInicioSolicitacao the value of field
     * 'dtInicioSolicitacao'.
     */
    public void setDtInicioSolicitacao(java.lang.String dtInicioSolicitacao)
    {
        this._dtInicioSolicitacao = dtInicioSolicitacao;
    } //-- void setDtInicioSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarSolicRecuperacaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.request.ConsultarSolicRecuperacaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.request.ConsultarSolicRecuperacaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.request.ConsultarSolicRecuperacaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.request.ConsultarSolicRecuperacaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
