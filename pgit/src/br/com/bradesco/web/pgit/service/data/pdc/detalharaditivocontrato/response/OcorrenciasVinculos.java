/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class OcorrenciasVinculos.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasVinculos implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrCpfCnpjVinc
     */
    private java.lang.String _nrCpfCnpjVinc;

    /**
     * Field _nmRazaoSocialVinc
     */
    private java.lang.String _nmRazaoSocialVinc;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgenciaBancaria
     */
    private int _cdAgenciaBancaria = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancaria
     */
    private boolean _has_cdAgenciaBancaria;

    /**
     * Field _dsAgenciaBancaria
     */
    private java.lang.String _dsAgenciaBancaria;

    /**
     * Field _cdContaBancaria
     */
    private long _cdContaBancaria = 0;

    /**
     * keeps track of state for field: _cdContaBancaria
     */
    private boolean _has_cdContaBancaria;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private java.lang.String _cdTipoConta;

    /**
     * Field _dsAcaoManutencaoVinc
     */
    private java.lang.String _dsAcaoManutencaoVinc;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasVinculos() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculos()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaBancaria
     * 
     */
    public void deleteCdAgenciaBancaria()
    {
        this._has_cdAgenciaBancaria= false;
    } //-- void deleteCdAgenciaBancaria() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdContaBancaria
     * 
     */
    public void deleteCdContaBancaria()
    {
        this._has_cdContaBancaria= false;
    } //-- void deleteCdContaBancaria() 

    /**
     * Returns the value of field 'cdAgenciaBancaria'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancaria'.
     */
    public int getCdAgenciaBancaria()
    {
        return this._cdAgenciaBancaria;
    } //-- int getCdAgenciaBancaria() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdContaBancaria'.
     * 
     * @return long
     * @return the value of field 'cdContaBancaria'.
     */
    public long getCdContaBancaria()
    {
        return this._cdContaBancaria;
    } //-- long getCdContaBancaria() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return String
     * @return the value of field 'cdTipoConta'.
     */
    public java.lang.String getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- java.lang.String getCdTipoConta() 

    /**
     * Returns the value of field 'dsAcaoManutencaoVinc'.
     * 
     * @return String
     * @return the value of field 'dsAcaoManutencaoVinc'.
     */
    public java.lang.String getDsAcaoManutencaoVinc()
    {
        return this._dsAcaoManutencaoVinc;
    } //-- java.lang.String getDsAcaoManutencaoVinc() 

    /**
     * Returns the value of field 'dsAgenciaBancaria'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaBancaria'.
     */
    public java.lang.String getDsAgenciaBancaria()
    {
        return this._dsAgenciaBancaria;
    } //-- java.lang.String getDsAgenciaBancaria() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'nmRazaoSocialVinc'.
     * 
     * @return String
     * @return the value of field 'nmRazaoSocialVinc'.
     */
    public java.lang.String getNmRazaoSocialVinc()
    {
        return this._nmRazaoSocialVinc;
    } //-- java.lang.String getNmRazaoSocialVinc() 

    /**
     * Returns the value of field 'nrCpfCnpjVinc'.
     * 
     * @return String
     * @return the value of field 'nrCpfCnpjVinc'.
     */
    public java.lang.String getNrCpfCnpjVinc()
    {
        return this._nrCpfCnpjVinc;
    } //-- java.lang.String getNrCpfCnpjVinc() 

    /**
     * Method hasCdAgenciaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancaria()
    {
        return this._has_cdAgenciaBancaria;
    } //-- boolean hasCdAgenciaBancaria() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdContaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancaria()
    {
        return this._has_cdContaBancaria;
    } //-- boolean hasCdContaBancaria() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaBancaria'.
     * 
     * @param cdAgenciaBancaria the value of field
     * 'cdAgenciaBancaria'.
     */
    public void setCdAgenciaBancaria(int cdAgenciaBancaria)
    {
        this._cdAgenciaBancaria = cdAgenciaBancaria;
        this._has_cdAgenciaBancaria = true;
    } //-- void setCdAgenciaBancaria(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdContaBancaria'.
     * 
     * @param cdContaBancaria the value of field 'cdContaBancaria'.
     */
    public void setCdContaBancaria(long cdContaBancaria)
    {
        this._cdContaBancaria = cdContaBancaria;
        this._has_cdContaBancaria = true;
    } //-- void setCdContaBancaria(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(java.lang.String cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
    } //-- void setCdTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsAcaoManutencaoVinc'.
     * 
     * @param dsAcaoManutencaoVinc the value of field
     * 'dsAcaoManutencaoVinc'.
     */
    public void setDsAcaoManutencaoVinc(java.lang.String dsAcaoManutencaoVinc)
    {
        this._dsAcaoManutencaoVinc = dsAcaoManutencaoVinc;
    } //-- void setDsAcaoManutencaoVinc(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaBancaria'.
     * 
     * @param dsAgenciaBancaria the value of field
     * 'dsAgenciaBancaria'.
     */
    public void setDsAgenciaBancaria(java.lang.String dsAgenciaBancaria)
    {
        this._dsAgenciaBancaria = dsAgenciaBancaria;
    } //-- void setDsAgenciaBancaria(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'nmRazaoSocialVinc'.
     * 
     * @param nmRazaoSocialVinc the value of field
     * 'nmRazaoSocialVinc'.
     */
    public void setNmRazaoSocialVinc(java.lang.String nmRazaoSocialVinc)
    {
        this._nmRazaoSocialVinc = nmRazaoSocialVinc;
    } //-- void setNmRazaoSocialVinc(java.lang.String) 

    /**
     * Sets the value of field 'nrCpfCnpjVinc'.
     * 
     * @param nrCpfCnpjVinc the value of field 'nrCpfCnpjVinc'.
     */
    public void setNrCpfCnpjVinc(java.lang.String nrCpfCnpjVinc)
    {
        this._nrCpfCnpjVinc = nrCpfCnpjVinc;
    } //-- void setNrCpfCnpjVinc(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasVinculos
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculos unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculos) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculos.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculos unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
