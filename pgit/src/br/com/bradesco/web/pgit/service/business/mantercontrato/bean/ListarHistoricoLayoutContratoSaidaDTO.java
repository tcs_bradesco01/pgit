/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarHistoricoLayoutContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistoricoLayoutContratoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdArquivoRetornoPagamento. */
	private Integer cdArquivoRetornoPagamento;

	/** Atributo dsArquivoRetornoPagamento. */
	private String dsArquivoRetornoPagamento;

	/** Atributo hrInclusaoManutencao. */
	private String hrInclusaoManutencao;

	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;

	/** Atributo dtManutencao. */
	private String dtManutencao;

	/** Atributo hrManutencao. */
	private String hrManutencao;

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;

	/** Atributo hrInclusaoManutencaoFormatada. */
	private String hrInclusaoManutencaoFormatada;

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: hrInclusaoManutencaoFormatada.
	 *
	 * @return hrInclusaoManutencaoFormatada
	 */
	public String getHrInclusaoManutencaoFormatada() {
		return hrInclusaoManutencaoFormatada;
	}

	/**
	 * Set: hrInclusaoManutencaoFormatada.
	 *
	 * @param hrInclusaoManutencaoFormatada the hr inclusao manutencao formatada
	 */
	public void setHrInclusaoManutencaoFormatada(String hrInclusaoManutencaoFormatada) {
		this.hrInclusaoManutencaoFormatada = hrInclusaoManutencaoFormatada;
	}

	/**
	 * Get: cdArquivoRetornoPagamento.
	 *
	 * @return cdArquivoRetornoPagamento
	 */
	public Integer getCdArquivoRetornoPagamento() {
		return cdArquivoRetornoPagamento;
	}

	/**
	 * Set: cdArquivoRetornoPagamento.
	 *
	 * @param cdArquivoRetornoPagamento the cd arquivo retorno pagamento
	 */
	public void setCdArquivoRetornoPagamento(Integer cdArquivoRetornoPagamento) {
		this.cdArquivoRetornoPagamento = cdArquivoRetornoPagamento;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsArquivoRetornoPagamento.
	 *
	 * @return dsArquivoRetornoPagamento
	 */
	public String getDsArquivoRetornoPagamento() {
		return dsArquivoRetornoPagamento;
	}

	/**
	 * Set: dsArquivoRetornoPagamento.
	 *
	 * @param dsArquivoRetornoPagamento the ds arquivo retorno pagamento
	 */
	public void setDsArquivoRetornoPagamento(String dsArquivoRetornoPagamento) {
		this.dsArquivoRetornoPagamento = dsArquivoRetornoPagamento;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrInclusaoManutencao.
	 *
	 * @return hrInclusaoManutencao
	 */
	public String getHrInclusaoManutencao() {
		return hrInclusaoManutencao;
	}

	/**
	 * Set: hrInclusaoManutencao.
	 *
	 * @param hrInclusaoManutencao the hr inclusao manutencao
	 */
	public void setHrInclusaoManutencao(String hrInclusaoManutencao) {
		this.hrInclusaoManutencao = hrInclusaoManutencao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
