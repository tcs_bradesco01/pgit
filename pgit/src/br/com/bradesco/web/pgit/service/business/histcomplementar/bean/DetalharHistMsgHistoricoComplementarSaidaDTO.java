/*
 * Nome: br.com.bradesco.web.pgit.service.business.histcomplementar.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.histcomplementar.bean;

/**
 * Nome: DetalharHistMsgHistoricoComplementarSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistMsgHistoricoComplementarSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo codigoHistoricoComplementar. */
	private int codigoHistoricoComplementar;
	
	/** Atributo prefixoDebito. */
	private String prefixoDebito;
	
	/** Atributo cdMensagemDebito. */
	private int cdMensagemDebito;
	
	/** Atributo dsMensagemDebito. */
	private String dsMensagemDebito;
	
	/** Atributo cdRecursoDebito. */
	private int cdRecursoDebito;
	
	/** Atributo dsRecursoDebito. */
	private String dsRecursoDebito;
	
	/** Atributo cdIdiomaDebito. */
	private int cdIdiomaDebito;
	
	/** Atributo dsIdiomaDebito. */
	private String dsIdiomaDebito;
	
	/** Atributo prefixoCredito. */
	private String prefixoCredito;
	
	/** Atributo cdMensagemCredito. */
	private int cdMensagemCredito;
	
	/** Atributo dsMensagemCredito. */
	private String dsMensagemCredito;
	
	/** Atributo cdRecursoCredito. */
	private int cdRecursoCredito;
	
	/** Atributo dsRecursoCredito. */
	private String dsRecursoCredito;
	
	/** Atributo cdIdiomaCredito. */
	private int cdIdiomaCredito;
	
	/** Atributo dsIdiomaCredito. */
	private String dsIdiomaCredito;
	
	/** Atributo restricao. */
	private int restricao;
	
	/** Atributo tipoLancamento. */
	private int tipoLancamento;
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdModalidadeServico. */
	private int cdModalidadeServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo relacionamento. */
	private int relacionamento;
	
	/** Atributo cdTipoCanalInclusao. */
	private int cdTipoCanalInclusao;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo cdTipoCanalManutencao. */
	private int cdTipoCanalManutencao;
	
	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	
	/** Atributo dsSegundaLinhaExtratoCredito. */ 
	private String dsSegundaLinhaExtratoCredito;
	
	/** Atributo dsSegundaLinhaExtratoDebito. */
	private String dsSegundaLinhaExtratoDebito;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdIdiomaCredito.
	 *
	 * @return cdIdiomaCredito
	 */
	public int getCdIdiomaCredito() {
		return cdIdiomaCredito;
	}
	
	/**
	 * Set: cdIdiomaCredito.
	 *
	 * @param cdIdiomaCredito the cd idioma credito
	 */
	public void setCdIdiomaCredito(int cdIdiomaCredito) {
		this.cdIdiomaCredito = cdIdiomaCredito;
	}
	
	/**
	 * Get: cdIdiomaDebito.
	 *
	 * @return cdIdiomaDebito
	 */
	public int getCdIdiomaDebito() {
		return cdIdiomaDebito;
	}
	
	/**
	 * Set: cdIdiomaDebito.
	 *
	 * @param cdIdiomaDebito the cd idioma debito
	 */
	public void setCdIdiomaDebito(int cdIdiomaDebito) {
		this.cdIdiomaDebito = cdIdiomaDebito;
	}
	
	/**
	 * Get: cdMensagemCredito.
	 *
	 * @return cdMensagemCredito
	 */
	public int getCdMensagemCredito() {
		return cdMensagemCredito;
	}
	
	/**
	 * Set: cdMensagemCredito.
	 *
	 * @param cdMensagemCredito the cd mensagem credito
	 */
	public void setCdMensagemCredito(int cdMensagemCredito) {
		this.cdMensagemCredito = cdMensagemCredito;
	}
	
	/**
	 * Get: cdMensagemDebito.
	 *
	 * @return cdMensagemDebito
	 */
	public int getCdMensagemDebito() {
		return cdMensagemDebito;
	}
	
	/**
	 * Set: cdMensagemDebito.
	 *
	 * @param cdMensagemDebito the cd mensagem debito
	 */
	public void setCdMensagemDebito(int cdMensagemDebito) {
		this.cdMensagemDebito = cdMensagemDebito;
	}
	
	/**
	 * Get: cdModalidadeServico.
	 *
	 * @return cdModalidadeServico
	 */
	public int getCdModalidadeServico() {
		return cdModalidadeServico;
	}
	
	/**
	 * Set: cdModalidadeServico.
	 *
	 * @param cdModalidadeServico the cd modalidade servico
	 */
	public void setCdModalidadeServico(int cdModalidadeServico) {
		this.cdModalidadeServico = cdModalidadeServico;
	}
	
	/**
	 * Get: cdRecursoCredito.
	 *
	 * @return cdRecursoCredito
	 */
	public int getCdRecursoCredito() {
		return cdRecursoCredito;
	}
	
	/**
	 * Set: cdRecursoCredito.
	 *
	 * @param cdRecursoCredito the cd recurso credito
	 */
	public void setCdRecursoCredito(int cdRecursoCredito) {
		this.cdRecursoCredito = cdRecursoCredito;
	}
	
	/**
	 * Get: cdRecursoDebito.
	 *
	 * @return cdRecursoDebito
	 */
	public int getCdRecursoDebito() {
		return cdRecursoDebito;
	}
	
	/**
	 * Set: cdRecursoDebito.
	 *
	 * @param cdRecursoDebito the cd recurso debito
	 */
	public void setCdRecursoDebito(int cdRecursoDebito) {
		this.cdRecursoDebito = cdRecursoDebito;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public int getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(int cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public int getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(int cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codigoHistoricoComplementar.
	 *
	 * @return codigoHistoricoComplementar
	 */
	public int getCodigoHistoricoComplementar() {
		return codigoHistoricoComplementar;
	}
	
	/**
	 * Set: codigoHistoricoComplementar.
	 *
	 * @param codigoHistoricoComplementar the codigo historico complementar
	 */
	public void setCodigoHistoricoComplementar(int codigoHistoricoComplementar) {
		this.codigoHistoricoComplementar = codigoHistoricoComplementar;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsIdiomaCredito.
	 *
	 * @return dsIdiomaCredito
	 */
	public String getDsIdiomaCredito() {
		return dsIdiomaCredito;
	}
	
	/**
	 * Set: dsIdiomaCredito.
	 *
	 * @param dsIdiomaCredito the ds idioma credito
	 */
	public void setDsIdiomaCredito(String dsIdiomaCredito) {
		this.dsIdiomaCredito = dsIdiomaCredito;
	}
	
	/**
	 * Get: dsIdiomaDebito.
	 *
	 * @return dsIdiomaDebito
	 */
	public String getDsIdiomaDebito() {
		return dsIdiomaDebito;
	}
	
	/**
	 * Set: dsIdiomaDebito.
	 *
	 * @param dsIdiomaDebito the ds idioma debito
	 */
	public void setDsIdiomaDebito(String dsIdiomaDebito) {
		this.dsIdiomaDebito = dsIdiomaDebito;
	}
	
	/**
	 * Get: dsMensagemCredito.
	 *
	 * @return dsMensagemCredito
	 */
	public String getDsMensagemCredito() {
		return dsMensagemCredito;
	}
	
	/**
	 * Set: dsMensagemCredito.
	 *
	 * @param dsMensagemCredito the ds mensagem credito
	 */
	public void setDsMensagemCredito(String dsMensagemCredito) {
		this.dsMensagemCredito = dsMensagemCredito;
	}
	
	/**
	 * Get: dsMensagemDebito.
	 *
	 * @return dsMensagemDebito
	 */
	public String getDsMensagemDebito() {
		return dsMensagemDebito;
	}
	
	/**
	 * Set: dsMensagemDebito.
	 *
	 * @param dsMensagemDebito the ds mensagem debito
	 */
	public void setDsMensagemDebito(String dsMensagemDebito) {
		this.dsMensagemDebito = dsMensagemDebito;
	}
	
	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}
	
	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}
	
	/**
	 * Get: dsRecursoCredito.
	 *
	 * @return dsRecursoCredito
	 */
	public String getDsRecursoCredito() {
		return dsRecursoCredito;
	}
	
	/**
	 * Set: dsRecursoCredito.
	 *
	 * @param dsRecursoCredito the ds recurso credito
	 */
	public void setDsRecursoCredito(String dsRecursoCredito) {
		this.dsRecursoCredito = dsRecursoCredito;
	}
	
	/**
	 * Get: dsRecursoDebito.
	 *
	 * @return dsRecursoDebito
	 */
	public String getDsRecursoDebito() {
		return dsRecursoDebito;
	}
	
	/**
	 * Set: dsRecursoDebito.
	 *
	 * @param dsRecursoDebito the ds recurso debito
	 */
	public void setDsRecursoDebito(String dsRecursoDebito) {
		this.dsRecursoDebito = dsRecursoDebito;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: prefixoCredito.
	 *
	 * @return prefixoCredito
	 */
	public String getPrefixoCredito() {
		return prefixoCredito;
	}
	
	/**
	 * Set: prefixoCredito.
	 *
	 * @param prefixoCredito the prefixo credito
	 */
	public void setPrefixoCredito(String prefixoCredito) {
		this.prefixoCredito = prefixoCredito;
	}
	
	/**
	 * Get: prefixoDebito.
	 *
	 * @return prefixoDebito
	 */
	public String getPrefixoDebito() {
		return prefixoDebito;
	}
	
	/**
	 * Set: prefixoDebito.
	 *
	 * @param prefixoDebito the prefixo debito
	 */
	public void setPrefixoDebito(String prefixoDebito) {
		this.prefixoDebito = prefixoDebito;
	}
	
	/**
	 * Get: relacionamento.
	 *
	 * @return relacionamento
	 */
	public int getRelacionamento() {
		return relacionamento;
	}
	
	/**
	 * Set: relacionamento.
	 *
	 * @param relacionamento the relacionamento
	 */
	public void setRelacionamento(int relacionamento) {
		this.relacionamento = relacionamento;
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public int getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(int restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: tipoLancamento.
	 *
	 * @return tipoLancamento
	 */
	public int getTipoLancamento() {
		return tipoLancamento;
	}
	
	/**
	 * Set: tipoLancamento.
	 *
	 * @param tipoLancamento the tipo lancamento
	 */
	public void setTipoLancamento(int tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * @return the dsSegundaLinhaExtratoCredito
	 */
	public String getDsSegundaLinhaExtratoCredito() {
		return dsSegundaLinhaExtratoCredito;
	}

	/**
	 * @param dsSegundaLinhaExtratoCredito the dsSegundaLinhaExtratoCredito to set
	 */
	public void setDsSegundaLinhaExtratoCredito(String dsSegundaLinhaExtratoCredito) {
		this.dsSegundaLinhaExtratoCredito = dsSegundaLinhaExtratoCredito;
	}

	/**
	 * @return the dsSegundaLinhaExtratoDebito
	 */
	public String getDsSegundaLinhaExtratoDebito() {
		return dsSegundaLinhaExtratoDebito;
	}

	/**
	 * @param dsSegundaLinhaExtratoDebito the dsSegundaLinhaExtratoDebito to set
	 */
	public void setDsSegundaLinhaExtratoDebito(String dsSegundaLinhaExtratoDebito) {
		this.dsSegundaLinhaExtratoDebito = dsSegundaLinhaExtratoDebito;
	}
	
	
	
	

}
