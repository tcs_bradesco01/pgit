/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarPagtosConsParaCancelamentoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarPagtosConsParaCancelamentoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoPesquisa
     */
    private int _cdTipoPesquisa = 0;

    /**
     * keeps track of state for field: _cdTipoPesquisa
     */
    private boolean _has_cdTipoPesquisa;

    /**
     * Field _cdAgendamentoPagosNaoPagos
     */
    private int _cdAgendamentoPagosNaoPagos = 0;

    /**
     * keeps track of state for field: _cdAgendamentoPagosNaoPagos
     */
    private boolean _has_cdAgendamentoPagosNaoPagos;

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequencialContrato
     */
    private long _nrSequencialContrato = 0;

    /**
     * keeps track of state for field: _nrSequencialContrato
     */
    private boolean _has_nrSequencialContrato;

    /**
     * Field _dtInicialPagamento
     */
    private java.lang.String _dtInicialPagamento;

    /**
     * Field _dtFinalPagamento
     */
    private java.lang.String _dtFinalPagamento;

    /**
     * Field _nrRemessa
     */
    private long _nrRemessa = 0;

    /**
     * keeps track of state for field: _nrRemessa
     */
    private boolean _has_nrRemessa;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private int _cdMotivoSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoPagamento
     */
    private boolean _has_cdMotivoSituacaoPagamento;

    /**
     * Field _cdTituloPgtoRastreado
     */
    private int _cdTituloPgtoRastreado = 0;

    /**
     * keeps track of state for field: _cdTituloPgtoRastreado
     */
    private boolean _has_cdTituloPgtoRastreado;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarPagtosConsParaCancelamentoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.request.ConsultarPagtosConsParaCancelamentoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdAgendamentoPagosNaoPagos
     * 
     */
    public void deleteCdAgendamentoPagosNaoPagos()
    {
        this._has_cdAgendamentoPagosNaoPagos= false;
    } //-- void deleteCdAgendamentoPagosNaoPagos() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdMotivoSituacaoPagamento
     * 
     */
    public void deleteCdMotivoSituacaoPagamento()
    {
        this._has_cdMotivoSituacaoPagamento= false;
    } //-- void deleteCdMotivoSituacaoPagamento() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoPesquisa
     * 
     */
    public void deleteCdTipoPesquisa()
    {
        this._has_cdTipoPesquisa= false;
    } //-- void deleteCdTipoPesquisa() 

    /**
     * Method deleteCdTituloPgtoRastreado
     * 
     */
    public void deleteCdTituloPgtoRastreado()
    {
        this._has_cdTituloPgtoRastreado= false;
    } //-- void deleteCdTituloPgtoRastreado() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrRemessa
     * 
     */
    public void deleteNrRemessa()
    {
        this._has_nrRemessa= false;
    } //-- void deleteNrRemessa() 

    /**
     * Method deleteNrSequencialContrato
     * 
     */
    public void deleteNrSequencialContrato()
    {
        this._has_nrSequencialContrato= false;
    } //-- void deleteNrSequencialContrato() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdAgendamentoPagosNaoPagos'.
     * 
     * @return int
     * @return the value of field 'cdAgendamentoPagosNaoPagos'.
     */
    public int getCdAgendamentoPagosNaoPagos()
    {
        return this._cdAgendamentoPagosNaoPagos;
    } //-- int getCdAgendamentoPagosNaoPagos() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public int getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- int getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoPesquisa'.
     * 
     * @return int
     * @return the value of field 'cdTipoPesquisa'.
     */
    public int getCdTipoPesquisa()
    {
        return this._cdTipoPesquisa;
    } //-- int getCdTipoPesquisa() 

    /**
     * Returns the value of field 'cdTituloPgtoRastreado'.
     * 
     * @return int
     * @return the value of field 'cdTituloPgtoRastreado'.
     */
    public int getCdTituloPgtoRastreado()
    {
        return this._cdTituloPgtoRastreado;
    } //-- int getCdTituloPgtoRastreado() 

    /**
     * Returns the value of field 'dtFinalPagamento'.
     * 
     * @return String
     * @return the value of field 'dtFinalPagamento'.
     */
    public java.lang.String getDtFinalPagamento()
    {
        return this._dtFinalPagamento;
    } //-- java.lang.String getDtFinalPagamento() 

    /**
     * Returns the value of field 'dtInicialPagamento'.
     * 
     * @return String
     * @return the value of field 'dtInicialPagamento'.
     */
    public java.lang.String getDtInicialPagamento()
    {
        return this._dtInicialPagamento;
    } //-- java.lang.String getDtInicialPagamento() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrRemessa'.
     * 
     * @return long
     * @return the value of field 'nrRemessa'.
     */
    public long getNrRemessa()
    {
        return this._nrRemessa;
    } //-- long getNrRemessa() 

    /**
     * Returns the value of field 'nrSequencialContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequencialContrato'.
     */
    public long getNrSequencialContrato()
    {
        return this._nrSequencialContrato;
    } //-- long getNrSequencialContrato() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdAgendamentoPagosNaoPagos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendamentoPagosNaoPagos()
    {
        return this._has_cdAgendamentoPagosNaoPagos;
    } //-- boolean hasCdAgendamentoPagosNaoPagos() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdMotivoSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoPagamento()
    {
        return this._has_cdMotivoSituacaoPagamento;
    } //-- boolean hasCdMotivoSituacaoPagamento() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoPesquisa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoPesquisa()
    {
        return this._has_cdTipoPesquisa;
    } //-- boolean hasCdTipoPesquisa() 

    /**
     * Method hasCdTituloPgtoRastreado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTituloPgtoRastreado()
    {
        return this._has_cdTituloPgtoRastreado;
    } //-- boolean hasCdTituloPgtoRastreado() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrRemessa()
    {
        return this._has_nrRemessa;
    } //-- boolean hasNrRemessa() 

    /**
     * Method hasNrSequencialContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequencialContrato()
    {
        return this._has_nrSequencialContrato;
    } //-- boolean hasNrSequencialContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdAgendamentoPagosNaoPagos'.
     * 
     * @param cdAgendamentoPagosNaoPagos the value of field
     * 'cdAgendamentoPagosNaoPagos'.
     */
    public void setCdAgendamentoPagosNaoPagos(int cdAgendamentoPagosNaoPagos)
    {
        this._cdAgendamentoPagosNaoPagos = cdAgendamentoPagosNaoPagos;
        this._has_cdAgendamentoPagosNaoPagos = true;
    } //-- void setCdAgendamentoPagosNaoPagos(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(int cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
        this._has_cdMotivoSituacaoPagamento = true;
    } //-- void setCdMotivoSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoPesquisa'.
     * 
     * @param cdTipoPesquisa the value of field 'cdTipoPesquisa'.
     */
    public void setCdTipoPesquisa(int cdTipoPesquisa)
    {
        this._cdTipoPesquisa = cdTipoPesquisa;
        this._has_cdTipoPesquisa = true;
    } //-- void setCdTipoPesquisa(int) 

    /**
     * Sets the value of field 'cdTituloPgtoRastreado'.
     * 
     * @param cdTituloPgtoRastreado the value of field
     * 'cdTituloPgtoRastreado'.
     */
    public void setCdTituloPgtoRastreado(int cdTituloPgtoRastreado)
    {
        this._cdTituloPgtoRastreado = cdTituloPgtoRastreado;
        this._has_cdTituloPgtoRastreado = true;
    } //-- void setCdTituloPgtoRastreado(int) 

    /**
     * Sets the value of field 'dtFinalPagamento'.
     * 
     * @param dtFinalPagamento the value of field 'dtFinalPagamento'
     */
    public void setDtFinalPagamento(java.lang.String dtFinalPagamento)
    {
        this._dtFinalPagamento = dtFinalPagamento;
    } //-- void setDtFinalPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtInicialPagamento'.
     * 
     * @param dtInicialPagamento the value of field
     * 'dtInicialPagamento'.
     */
    public void setDtInicialPagamento(java.lang.String dtInicialPagamento)
    {
        this._dtInicialPagamento = dtInicialPagamento;
    } //-- void setDtInicialPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrRemessa'.
     * 
     * @param nrRemessa the value of field 'nrRemessa'.
     */
    public void setNrRemessa(long nrRemessa)
    {
        this._nrRemessa = nrRemessa;
        this._has_nrRemessa = true;
    } //-- void setNrRemessa(long) 

    /**
     * Sets the value of field 'nrSequencialContrato'.
     * 
     * @param nrSequencialContrato the value of field
     * 'nrSequencialContrato'.
     */
    public void setNrSequencialContrato(long nrSequencialContrato)
    {
        this._nrSequencialContrato = nrSequencialContrato;
        this._has_nrSequencialContrato = true;
    } //-- void setNrSequencialContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarPagtosConsParaCancelamentoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.request.ConsultarPagtosConsParaCancelamentoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.request.ConsultarPagtosConsParaCancelamentoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.request.ConsultarPagtosConsParaCancelamentoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.request.ConsultarPagtosConsParaCancelamentoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
