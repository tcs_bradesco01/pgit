/*
 * Nome: br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean;

/**
 * Nome: ConsultarServRelacServperacionalEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarServRelacServperacionalEntradaDTO {
	
	
    /** Atributo cdprodutoServicoOperacao. */
    private Integer cdprodutoServicoOperacao;
    
    /** Atributo cdRelacionamentoProdutoProduto. */
    private Integer cdRelacionamentoProdutoProduto;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdprodutoServicoOperacao.
	 *
	 * @return cdprodutoServicoOperacao
	 */
	public Integer getCdprodutoServicoOperacao() {
		return cdprodutoServicoOperacao;
	}
	
	/**
	 * Set: cdprodutoServicoOperacao.
	 *
	 * @param cdprodutoServicoOperacao the cdproduto servico operacao
	 */
	public void setCdprodutoServicoOperacao(Integer cdprodutoServicoOperacao) {
		this.cdprodutoServicoOperacao = cdprodutoServicoOperacao;
	}
	
	/**
	 * Get: cdRelacionamentoProdutoProduto.
	 *
	 * @return cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProdutoProduto.
	 *
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
    
    
    

}
