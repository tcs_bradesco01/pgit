/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.impl;

import br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.ISolRecPagamentosConsultaService;
import br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.bean.SolicitarRecuparacaoPagtosConsultaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.bean.SolicitarRecuparacaoPagtosConsultaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosbackupconsulta.request.SolicitarRecPagtosBackupConsultaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosbackupconsulta.response.SolicitarRecPagtosBackupConsultaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: SolRecPagamentosConsulta
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SolRecPagamentosConsultaServiceImpl implements ISolRecPagamentosConsultaService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.ISolRecPagamentosConsultaService#solicitarRecuparacaoPagtosConsulta(br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.bean.SolicitarRecuparacaoPagtosConsultaEntradaDTO)
	 */
	public SolicitarRecuparacaoPagtosConsultaSaidaDTO solicitarRecuparacaoPagtosConsulta(SolicitarRecuparacaoPagtosConsultaEntradaDTO entradaDTO) {
		SolicitarRecPagtosBackupConsultaRequest request = new SolicitarRecPagtosBackupConsultaRequest();
		SolicitarRecPagtosBackupConsultaResponse response = new SolicitarRecPagtosBackupConsultaResponse();
		SolicitarRecuparacaoPagtosConsultaSaidaDTO saidaDTO = new SolicitarRecuparacaoPagtosConsultaSaidaDTO();
		
		request.setCdFinalidadeRecuperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFinalidadeRecuperacao()));	
	    request.setCdSelecaoRecuperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSelecaoRecuperacao()));	    
	    request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));	    
	    request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));	    
	    request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));	    
	    request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));	    
	    request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));	    
	    request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));	    
	    request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));	    
	    request.setCdDigitoContaDebito(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoContaDebito()));	    
	    request.setCdTipoContaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContaDebito()));	    
	    request.setDtInicioPeriodoPesquisa(PgitUtil.verificaStringNula(entradaDTO.getDtInicioPeriodoPesquisa()));	    
	    request.setDtFimPeriodoPesquisa(PgitUtil.verificaStringNula(entradaDTO.getDtFimPeriodoPesquisa()));	    
	    request.setCdProduto(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProduto()));	    
	    request.setCdProdutoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoRelacionado()));	    
	    request.setNrSequenciaArquivoRemessa(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaArquivoRemessa()));	    
	    request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));	    
	    request.setCdControlePagamentoInicial(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoInicial()));	    
	    request.setCdControlePagamentoFinal(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoFinal()));	    
	    request.setVlrInicio(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlrInicio()));	    
	    request.setVlFim(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlFim()));	    
	    request.setCdSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoPagamento()));	    
	    request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));	    
	    request.setLinhaDigitavel(PgitUtil.verificaStringNula(entradaDTO.getLinhaDigitavel()));	    
	    request.setCdFavorecidoCliente(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecidoCliente()));	    
	    request.setCdTipoInscricaoRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoInscricaoRecebedor()));	    
	    request.setCdInscricaoRecebedor(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoRecebedor()));	    
	    request.setCdCpfCnpjRecebedor(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjRecebedor()));	    
	    request.setCdFilialCnpjRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjRecebedor()));	    
	    request.setCdControleCpfRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCpfRecebedor()));	    
	    request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));	    
	    request.setCdBancoCredito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoCredito()));	    
	    request.setCdAgenciaCredito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaCredito()));	    
	    request.setCdContaCredito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaCredito()));	    
	    request.setDigitoContaCredito(PgitUtil.verificaStringNula(entradaDTO.getDigitoContaCredito()));	    
	    request.setCdTipoContaCredito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContaCredito()));	    
	    request.setDtRecuperacaoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtRecuperacaoPagamento()));	    
	    request.setDtNovoVencimentoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtNovoVencimentoPagamento()));	    
	    request.setVlTarifaPadrao(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlTarifaPadrao()));	    
	    request.setVlTarifaNegociacao(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlTarifaNegociacao()));	    
	    request.setPercentualBonificacaoTarifa(PgitUtil.verificaBigDecimalNulo(entradaDTO.getPercentualBonificacaoTarifa()));
	    
	    request.setECbcoSalario(PgitUtil.verificaIntegerNulo(entradaDTO.getBancoSalario()));	    
	    request.setEContaSalario(PgitUtil.verificaLongNulo(entradaDTO.getContaSalario()));
	    request.setEAgenciaSalario(PgitUtil.verificaIntegerNulo(entradaDTO.getAgenciaSalario()));
	    request.setEDigContaSalario(PgitUtil.verificaStringNula(entradaDTO.getDigitoSalario()));
	    request.setELoteInterno(PgitUtil.verificaLongNulo(entradaDTO.getLoteInterno()));
	    request.setECbcoPgto(PgitUtil.verificaIntegerNulo(entradaDTO.getBancoPgtoDestino()));
	    request.setEIspbPgtoDest(PgitUtil.verificaStringNula(entradaDTO.getIspbPgtoDestino()));
	    request.setEContaPgtoDest(PgitUtil.verificaLongNulo(entradaDTO.getContaPgtoDestino()));
	    request.setEDestPgtoRecup(PgitUtil.verificaIntegerNulo(entradaDTO.getDestinoPagamento()));

	    response = getFactoryAdapter().getSolicitarRecPagtosBackupConsultaPDCAdapter().invokeProcess(request);
	    
	    saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
    
}

