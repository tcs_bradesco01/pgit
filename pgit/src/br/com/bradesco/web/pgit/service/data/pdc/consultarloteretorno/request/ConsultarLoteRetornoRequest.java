/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarLoteRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarLoteRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _qtdeOcorrencia
     */
    private int _qtdeOcorrencia = 0;

    /**
     * keeps track of state for field: _qtdeOcorrencia
     */
    private boolean _has_qtdeOcorrencia;

    /**
     * Field _codigoSistemaLegado
     */
    private java.lang.String _codigoSistemaLegado;

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filial
     */
    private int _filial = 0;

    /**
     * keeps track of state for field: _filial
     */
    private boolean _has_filial;

    /**
     * Field _codigoPerfilComunicacao
     */
    private long _codigoPerfilComunicacao = 0;

    /**
     * keeps track of state for field: _codigoPerfilComunicacao
     */
    private boolean _has_codigoPerfilComunicacao;

    /**
     * Field _numeroArquivoRetorno
     */
    private long _numeroArquivoRetorno = 0;

    /**
     * keeps track of state for field: _numeroArquivoRetorno
     */
    private boolean _has_numeroArquivoRetorno;

    /**
     * Field _horaInclusao
     */
    private java.lang.String _horaInclusao;

    /**
     * Field _numeroLote
     */
    private long _numeroLote = 0;

    /**
     * keeps track of state for field: _numeroLote
     */
    private boolean _has_numeroLote;

    /**
     * Field _numeroSequenciaRegistroRetorno
     */
    private long _numeroSequenciaRegistroRetorno = 0;

    /**
     * keeps track of state for field:
     * _numeroSequenciaRegistroRetorno
     */
    private boolean _has_numeroSequenciaRegistroRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarLoteRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.request.ConsultarLoteRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodigoPerfilComunicacao
     * 
     */
    public void deleteCodigoPerfilComunicacao()
    {
        this._has_codigoPerfilComunicacao= false;
    } //-- void deleteCodigoPerfilComunicacao() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilial
     * 
     */
    public void deleteFilial()
    {
        this._has_filial= false;
    } //-- void deleteFilial() 

    /**
     * Method deleteNumeroArquivoRetorno
     * 
     */
    public void deleteNumeroArquivoRetorno()
    {
        this._has_numeroArquivoRetorno= false;
    } //-- void deleteNumeroArquivoRetorno() 

    /**
     * Method deleteNumeroLote
     * 
     */
    public void deleteNumeroLote()
    {
        this._has_numeroLote= false;
    } //-- void deleteNumeroLote() 

    /**
     * Method deleteNumeroSequenciaRegistroRetorno
     * 
     */
    public void deleteNumeroSequenciaRegistroRetorno()
    {
        this._has_numeroSequenciaRegistroRetorno= false;
    } //-- void deleteNumeroSequenciaRegistroRetorno() 

    /**
     * Method deleteQtdeOcorrencia
     * 
     */
    public void deleteQtdeOcorrencia()
    {
        this._has_qtdeOcorrencia= false;
    } //-- void deleteQtdeOcorrencia() 

    /**
     * Returns the value of field 'codigoPerfilComunicacao'.
     * 
     * @return long
     * @return the value of field 'codigoPerfilComunicacao'.
     */
    public long getCodigoPerfilComunicacao()
    {
        return this._codigoPerfilComunicacao;
    } //-- long getCodigoPerfilComunicacao() 

    /**
     * Returns the value of field 'codigoSistemaLegado'.
     * 
     * @return String
     * @return the value of field 'codigoSistemaLegado'.
     */
    public java.lang.String getCodigoSistemaLegado()
    {
        return this._codigoSistemaLegado;
    } //-- java.lang.String getCodigoSistemaLegado() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'filial'.
     * 
     * @return int
     * @return the value of field 'filial'.
     */
    public int getFilial()
    {
        return this._filial;
    } //-- int getFilial() 

    /**
     * Returns the value of field 'horaInclusao'.
     * 
     * @return String
     * @return the value of field 'horaInclusao'.
     */
    public java.lang.String getHoraInclusao()
    {
        return this._horaInclusao;
    } //-- java.lang.String getHoraInclusao() 

    /**
     * Returns the value of field 'numeroArquivoRetorno'.
     * 
     * @return long
     * @return the value of field 'numeroArquivoRetorno'.
     */
    public long getNumeroArquivoRetorno()
    {
        return this._numeroArquivoRetorno;
    } //-- long getNumeroArquivoRetorno() 

    /**
     * Returns the value of field 'numeroLote'.
     * 
     * @return long
     * @return the value of field 'numeroLote'.
     */
    public long getNumeroLote()
    {
        return this._numeroLote;
    } //-- long getNumeroLote() 

    /**
     * Returns the value of field 'numeroSequenciaRegistroRetorno'.
     * 
     * @return long
     * @return the value of field 'numeroSequenciaRegistroRetorno'.
     */
    public long getNumeroSequenciaRegistroRetorno()
    {
        return this._numeroSequenciaRegistroRetorno;
    } //-- long getNumeroSequenciaRegistroRetorno() 

    /**
     * Returns the value of field 'qtdeOcorrencia'.
     * 
     * @return int
     * @return the value of field 'qtdeOcorrencia'.
     */
    public int getQtdeOcorrencia()
    {
        return this._qtdeOcorrencia;
    } //-- int getQtdeOcorrencia() 

    /**
     * Method hasCodigoPerfilComunicacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoPerfilComunicacao()
    {
        return this._has_codigoPerfilComunicacao;
    } //-- boolean hasCodigoPerfilComunicacao() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilial()
    {
        return this._has_filial;
    } //-- boolean hasFilial() 

    /**
     * Method hasNumeroArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroArquivoRetorno()
    {
        return this._has_numeroArquivoRetorno;
    } //-- boolean hasNumeroArquivoRetorno() 

    /**
     * Method hasNumeroLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLote()
    {
        return this._has_numeroLote;
    } //-- boolean hasNumeroLote() 

    /**
     * Method hasNumeroSequenciaRegistroRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroSequenciaRegistroRetorno()
    {
        return this._has_numeroSequenciaRegistroRetorno;
    } //-- boolean hasNumeroSequenciaRegistroRetorno() 

    /**
     * Method hasQtdeOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeOcorrencia()
    {
        return this._has_qtdeOcorrencia;
    } //-- boolean hasQtdeOcorrencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codigoPerfilComunicacao'.
     * 
     * @param codigoPerfilComunicacao the value of field
     * 'codigoPerfilComunicacao'.
     */
    public void setCodigoPerfilComunicacao(long codigoPerfilComunicacao)
    {
        this._codigoPerfilComunicacao = codigoPerfilComunicacao;
        this._has_codigoPerfilComunicacao = true;
    } //-- void setCodigoPerfilComunicacao(long) 

    /**
     * Sets the value of field 'codigoSistemaLegado'.
     * 
     * @param codigoSistemaLegado the value of field
     * 'codigoSistemaLegado'.
     */
    public void setCodigoSistemaLegado(java.lang.String codigoSistemaLegado)
    {
        this._codigoSistemaLegado = codigoSistemaLegado;
    } //-- void setCodigoSistemaLegado(java.lang.String) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'filial'.
     * 
     * @param filial the value of field 'filial'.
     */
    public void setFilial(int filial)
    {
        this._filial = filial;
        this._has_filial = true;
    } //-- void setFilial(int) 

    /**
     * Sets the value of field 'horaInclusao'.
     * 
     * @param horaInclusao the value of field 'horaInclusao'.
     */
    public void setHoraInclusao(java.lang.String horaInclusao)
    {
        this._horaInclusao = horaInclusao;
    } //-- void setHoraInclusao(java.lang.String) 

    /**
     * Sets the value of field 'numeroArquivoRetorno'.
     * 
     * @param numeroArquivoRetorno the value of field
     * 'numeroArquivoRetorno'.
     */
    public void setNumeroArquivoRetorno(long numeroArquivoRetorno)
    {
        this._numeroArquivoRetorno = numeroArquivoRetorno;
        this._has_numeroArquivoRetorno = true;
    } //-- void setNumeroArquivoRetorno(long) 

    /**
     * Sets the value of field 'numeroLote'.
     * 
     * @param numeroLote the value of field 'numeroLote'.
     */
    public void setNumeroLote(long numeroLote)
    {
        this._numeroLote = numeroLote;
        this._has_numeroLote = true;
    } //-- void setNumeroLote(long) 

    /**
     * Sets the value of field 'numeroSequenciaRegistroRetorno'.
     * 
     * @param numeroSequenciaRegistroRetorno the value of field
     * 'numeroSequenciaRegistroRetorno'.
     */
    public void setNumeroSequenciaRegistroRetorno(long numeroSequenciaRegistroRetorno)
    {
        this._numeroSequenciaRegistroRetorno = numeroSequenciaRegistroRetorno;
        this._has_numeroSequenciaRegistroRetorno = true;
    } //-- void setNumeroSequenciaRegistroRetorno(long) 

    /**
     * Sets the value of field 'qtdeOcorrencia'.
     * 
     * @param qtdeOcorrencia the value of field 'qtdeOcorrencia'.
     */
    public void setQtdeOcorrencia(int qtdeOcorrencia)
    {
        this._qtdeOcorrencia = qtdeOcorrencia;
        this._has_qtdeOcorrencia = true;
    } //-- void setQtdeOcorrencia(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarLoteRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.request.ConsultarLoteRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.request.ConsultarLoteRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.request.ConsultarLoteRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.request.ConsultarLoteRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
