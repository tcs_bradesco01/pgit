/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdServicoRelacionado
     */
    private int _cdServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdServicoRelacionado
     */
    private boolean _has_cdServicoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdServicoRelacionado
     * 
     */
    public void deleteCdServicoRelacionado()
    {
        this._has_cdServicoRelacionado= false;
    } //-- void deleteCdServicoRelacionado() 

    /**
     * Returns the value of field 'cdServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdServicoRelacionado'.
     */
    public int getCdServicoRelacionado()
    {
        return this._cdServicoRelacionado;
    } //-- int getCdServicoRelacionado() 

    /**
     * Method hasCdServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoRelacionado()
    {
        return this._has_cdServicoRelacionado;
    } //-- boolean hasCdServicoRelacionado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdServicoRelacionado'.
     * 
     * @param cdServicoRelacionado the value of field
     * 'cdServicoRelacionado'.
     */
    public void setCdServicoRelacionado(int cdServicoRelacionado)
    {
        this._cdServicoRelacionado = cdServicoRelacionado;
        this._has_cdServicoRelacionado = true;
    } //-- void setCdServicoRelacionado(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
