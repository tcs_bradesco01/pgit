package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

import java.math.BigDecimal;

/**
 * Arquivo criado em 06/10/15.
 */
public class IncluirPagamentoSalarioEntradaDTO {

    private Long cdpessoaJuridicaContrato; //

    private Integer cdTipoContratoNegocio;//

    private Long nrSequenciaContratoNegocio;//

    private Integer cdProdutoServicoOperacao;

    private Integer cdProdutoOperacaoRelacionado;

    private Long cdClubRepresentante;//

    private Long cdCpfCnpjRepresentante;//

    private Integer cdFilialCnpjRepresentante;//

    private Integer cdControleCpfRepresentante;//

    private String nmRazaoRepresentante;

    private Long cdPessoaJuridicaConta;//

    private Integer cdTipoContratoConta;//

    private Long nrSequenciaContratoConta;//

    private Integer cdInstituicaoBancaria;//

    private Integer cdUnidadeOrganizacional;//

    private Integer nrConta;

    private String nrContaDigito;//

    private Integer cdIndicadorNumConve;//

    private Long cdConveCtaSalarial; //

    private BigDecimal vlPagamentoMes;//

    private Integer qtdFuncionarioFlPgto;//

    private BigDecimal vlMediaSalarialMes;//

    private Integer cdPagamentoSalarialMes;//

    private Integer cdPagamentoSalarialQzena;//

    private Integer cdLocalContaSalarial;//

    private Integer dtReftFlPgto;//

    private Integer cdIndicadorAlcadaAgencia;
    
    private Integer cdFidelize;
    
    private String dsEmailEmpresa;
    
    private String dsEmailAgencia;
    
    private Integer cdAltoTurnover;
    
    private Integer cdCartaoPre;
    
    private Integer cdPeloApp;
    
    public int getCdAltoTurnover() {
		return cdAltoTurnover;
	}

	public void setCdAltoTurnover(int cdAltoTurnover) {
		this.cdAltoTurnover = cdAltoTurnover;
	}

	public int getCdCartaoPre() {
		return cdCartaoPre;
	}

	public void setCdCartaoPre(int cdCartaoPre) {
		this.cdCartaoPre = cdCartaoPre;
	}

	public int getCdPeloApp() {
		return cdPeloApp;
	}

	public void setCdPeloApp(int cdPeloApp) {
		this.cdPeloApp = cdPeloApp;
	}
    
    public Integer getCdFidelize() {
		return cdFidelize;
	}

	public void setCdFidelize(Integer cdFidelize) {
		this.cdFidelize = cdFidelize;
	}

	public String getDsEmailEmpresa() {
		return dsEmailEmpresa;
	}

	public void setDsEmailEmpresa(String dsEmailEmpresa) {
		this.dsEmailEmpresa = dsEmailEmpresa;
	}

	public String getDsEmailAgencia() {
		return dsEmailAgencia;
	}

	public void setDsEmailAgencia(String dsEmailAgencia) {
		this.dsEmailAgencia = dsEmailAgencia;
	}

	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }

    public void setCdClubRepresentante(Long cdClubRepresentante) {
        this.cdClubRepresentante = cdClubRepresentante;
    }

    public Long getCdClubRepresentante() {
        return this.cdClubRepresentante;
    }

    public void setCdCpfCnpjRepresentante(Long cdCpfCnpjRepresentante) {
        this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
    }

    public Long getCdCpfCnpjRepresentante() {
        return this.cdCpfCnpjRepresentante;
    }

    public void setCdFilialCnpjRepresentante(Integer cdFilialCnpjRepresentante) {
        this.cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
    }

    public Integer getCdFilialCnpjRepresentante() {
        return this.cdFilialCnpjRepresentante;
    }

    public void setCdControleCpfRepresentante(Integer cdControleCpfRepresentante) {
        this.cdControleCpfRepresentante = cdControleCpfRepresentante;
    }

    public Integer getCdControleCpfRepresentante() {
        return this.cdControleCpfRepresentante;
    }

    public void setNmRazaoRepresentante(String nmRazaoRepresentante) {
        this.nmRazaoRepresentante = nmRazaoRepresentante;
    }

    public String getNmRazaoRepresentante() {
        return this.nmRazaoRepresentante;
    }

    public void setCdPessoaJuridicaConta(Long cdPessoaJuridicaConta) {
        this.cdPessoaJuridicaConta = cdPessoaJuridicaConta;
    }

    public Long getCdPessoaJuridicaConta() {
        return this.cdPessoaJuridicaConta;
    }

    public void setCdTipoContratoConta(Integer cdTipoContratoConta) {
        this.cdTipoContratoConta = cdTipoContratoConta;
    }

    public Integer getCdTipoContratoConta() {
        return this.cdTipoContratoConta;
    }

    public void setNrSequenciaContratoConta(Long nrSequenciaContratoConta) {
        this.nrSequenciaContratoConta = nrSequenciaContratoConta;
    }

    public Long getNrSequenciaContratoConta() {
        return this.nrSequenciaContratoConta;
    }

    public void setCdInstituicaoBancaria(Integer cdInstituicaoBancaria) {
        this.cdInstituicaoBancaria = cdInstituicaoBancaria;
    }

    public Integer getCdInstituicaoBancaria() {
        return this.cdInstituicaoBancaria;
    }

    public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
        this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
    }

    public Integer getCdUnidadeOrganizacional() {
        return this.cdUnidadeOrganizacional;
    }

    public void setNrConta(Integer nrConta) {
        this.nrConta = nrConta;
    }

    public Integer getNrConta() {
        return this.nrConta;
    }

    public void setNrContaDigito(String nrContaDigito) {
        this.nrContaDigito = nrContaDigito;
    }

    public String getNrContaDigito() {
        return this.nrContaDigito;
    }

    public void setCdIndicadorNumConve(Integer cdIndicadorNumConve) {
        this.cdIndicadorNumConve = cdIndicadorNumConve;
    }

    public Integer getCdIndicadorNumConve() {
        return this.cdIndicadorNumConve;
    }

    public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
        this.cdConveCtaSalarial = cdConveCtaSalarial;
    }

    public Long getCdConveCtaSalarial() {
        return this.cdConveCtaSalarial;
    }

    public void setVlPagamentoMes(BigDecimal vlPagamentoMes) {
        this.vlPagamentoMes = vlPagamentoMes;
    }

    public BigDecimal getVlPagamentoMes() {
        return this.vlPagamentoMes;
    }

    public void setQtdFuncionarioFlPgto(Integer qtdFuncionarioFlPgto) {
        this.qtdFuncionarioFlPgto = qtdFuncionarioFlPgto;
    }

    public Integer getQtdFuncionarioFlPgto() {
        return this.qtdFuncionarioFlPgto;
    }

    public void setVlMediaSalarialMes(BigDecimal vlMediaSalarialMes) {
        this.vlMediaSalarialMes = vlMediaSalarialMes;
    }

    public BigDecimal getVlMediaSalarialMes() {
        return this.vlMediaSalarialMes;
    }

    public void setCdPagamentoSalarialMes(Integer cdPagamentoSalarialMes) {
        this.cdPagamentoSalarialMes = cdPagamentoSalarialMes;
    }

    public Integer getCdPagamentoSalarialMes() {
        return this.cdPagamentoSalarialMes;
    }

    public void setCdPagamentoSalarialQzena(Integer cdPagamentoSalarialQzena) {
        this.cdPagamentoSalarialQzena = cdPagamentoSalarialQzena;
    }

    public Integer getCdPagamentoSalarialQzena() {
        return this.cdPagamentoSalarialQzena;
    }

    public void setCdLocalContaSalarial(Integer cdLocalContaSalarial) {
        this.cdLocalContaSalarial = cdLocalContaSalarial;
    }

    public Integer getCdLocalContaSalarial() {
        return this.cdLocalContaSalarial;
    }

    public void setDtReftFlPgto(Integer dtReftFlPgto) {
        this.dtReftFlPgto = dtReftFlPgto;
    }

    public Integer getDtReftFlPgto() {
        return this.dtReftFlPgto;
    }

    public void setCdIndicadorAlcadaAgencia(Integer cdIndicadorAlcadaAgencia) {
        this.cdIndicadorAlcadaAgencia = cdIndicadorAlcadaAgencia;
    }

    public Integer getCdIndicadorAlcadaAgencia() {
        return this.cdIndicadorAlcadaAgencia;
    }
}