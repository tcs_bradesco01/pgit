/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;

import java.awt.Color;
import java.util.Date;

import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;
import br.com.bradesco.web.pgit.utils.DateUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: PagamentoFornecedorReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PagamentoFornecedorReport extends BasicPdfReport {
	
	/** Atributo fTexto. */
	private Font fTexto = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabela. */
	private Font fTextoTabela = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
	
	/** Atributo fTextoTabelaCinza. */
	private Font fTextoTabelaCinza = new Font(Font.TIMES_ROMAN, 8, Font.BOLD,Color.GRAY);
	
	/** Atributo creditoConta. */
	private String creditoConta;
	
	/** Atributo chequeOP. */
	private String chequeOP;
	
	/** Atributo docCompe. */
	private String docCompe;
	
	/** Atributo creditoContaRealTime. */
	private String creditoContaRealTime;
	
	/** Atributo ted. */
	private String ted;
	
	/** Atributo tituloRegistradoBradesco. */
	private String tituloRegistradoBradesco;
	
	/** Atributo tituloRegistradoOutrosBancos. */
	private String tituloRegistradoOutrosBancos;
	
	/** Atributo imagemRelatorio. */
	private Image imagemRelatorio = null;
	
	/** Atributo saidaImprimirContrato. */
	private ImprimirContratoSaidaDTO saidaImprimirContrato;
	
	/**
	 * Pagamento fornecedor report.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public PagamentoFornecedorReport(ImprimirContratoSaidaDTO saidaImprimirContrato) {
		super();
		this.imagemRelatorio = getImagemRelatorio(); 
		this.saidaImprimirContrato = saidaImprimirContrato;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	protected void popularPdf(Document document) throws DocumentException {

		preencherCabec(document);
		document.add(Chunk.NEWLINE);
		preencherAnexo(document);
		document.add(Chunk.NEWLINE);
		preencherTitulo(document);
		document.add(Chunk.NEWLINE);
		
		preencherParagrafo1(document);
		document.add(Chunk.NEWLINE);
		
		preencherQuadro1(document);
		document.add(Chunk.NEWLINE);
		
		preencherParagrafo2(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo3(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo4(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo5(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo6(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo7(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo8(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo9(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo10(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo11(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo12(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo13(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo14(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo15(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo16(document);
		preencherParagrafo17(document);
		preencherParagrafo18(document);
		preencherParagrafo19(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo20(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo21(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo22(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo23(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo24(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo25(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo26(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo27(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo28(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo29(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo30(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafo31(document);
		document.add(Chunk.NEWLINE);
		
		document.add(Chunk.NEXTPAGE);
		preencherData(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaUm(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherAssinaturas3(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaDois(document);
		

	}
	
	/**
	 * Preencher cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		tabela.setWidths(new float[] {40.0f, 60.0f});
		
		Paragraph pLinha1Esq = new Paragraph(new Chunk("ANEXO AO CONTRATO DE PRESTA��O DE SERVI�OS DE PAGAMENTOS.", fTextoTabelaCinza));
		pLinha1Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfPCell.NO_BORDER);
		celula1.addElement(imagemRelatorio);
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher anexo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAnexo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("ANEXO 2", fTextoTabelaCinza));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher titulo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherTitulo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("Pag-For Bradesco Pagamento a Fornecedores.", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher paragrafo1.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo1(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("As partes nomeadas e qualificadas no pre�mbulo do ", fTexto));
		pLinha.add(new Chunk("Contrato, ", fTextoTabela));
		pLinha.add(new Chunk("ao qual este Anexo faz parte, denominadas ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("e ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", resolvem estabelecer as condi��es operacionais ", fTexto));
		pLinha.add(new Chunk("e comerciais espec�ficas � presta��o dos servi�os contratados.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);

		document.add(pLinha);
	}
	
	/**
	 * Preencher quadro1.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherQuadro1(Document document) throws DocumentException {
		
		assinalarValor();		
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("( " + getCreditoConta() + " ) 01 - Cr�dito em Conta Corrente: ", fTextoTabela));
		pLinha.add(new Chunk("permite liquidar seus compromissos, por meio de transfer�ncia de recursos ", fTexto));
		pLinha.add(new Chunk("financeiros diretamente para a conta corrente ou poupan�a de seus fornecedores; ", fTexto));
		pLinha.add(new Chunk("o cr�dito � disponibilizado na ", fTexto));
		pLinha.add(new Chunk("conta do fornecedor no per�odo noturno do dia indicado para pagamento.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		Paragraph pLinha2 = new Paragraph();
		pLinha2.add(new Chunk("( " + getChequeOP() + " ) 02 - Cheque OP (Ordem de Pagamento): ", fTextoTabela));
		pLinha2.add(new Chunk("esta modalidade � espec�fica para o caso do fornecedor n�o possuir conta corrente ", fTexto));
		pLinha2.add(new Chunk("em banco ou n�o desejar receber seus pagamentos diretamente na sua conta banc�ria.", fTexto));
		pLinha2.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		Paragraph pLinha3 = new Paragraph();
		pLinha3.add(new Chunk("( " + getDocCompe() + " ) 03 - DOC COMPE (Documento de Ordem de Cr�dito): ", fTextoTabela));
		pLinha3.add(new Chunk("propicia a transfer�ncia de recursos financeiros entre contas ", fTexto));
		pLinha3.add(new Chunk("correntes mantidas em diferentes bancos.", fTexto));
		pLinha3.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		Paragraph pLinha4 = new Paragraph();
		pLinha4.add(new Chunk("( " + getCreditoContaRealTime() + " ) 05 - Cr�dito em Conta Corrente Real Time: ", fTextoTabela));
		pLinha4.add(new Chunk("cr�dito � disponibilizado na conta corrente ou poupan�a do fornecedor, no momento ", fTexto));
		pLinha4.add(new Chunk("do processamento do arquivo remessa, desde que a transmiss�o ocorra na data do pagamento. ", fTexto));
		pLinha4.add(new Chunk("Caso o arquivo seja transmitido com anteced�ncia, o cr�dito � disponibilizado ", fTexto));
		pLinha4.add(new Chunk("ao fornecedor, no primeiro processamento do dia do pagamento.", fTexto));
		pLinha4.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		Paragraph pLinha5 = new Paragraph();
		pLinha5.add(new Chunk("( " + getTed() + " ) 08 - TED (Transfer�ncia Eletr�nica Dispon�vel): ", fTextoTabela));
		pLinha5.add(new Chunk("propicia a transfer�ncia de recursos financeiros entre contas correntes ", fTexto));
		pLinha5.add(new Chunk("mantidas em diferentes bancos, on-line, ap�s o processamento do arquivo remessa.", fTexto));
		pLinha5.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		Paragraph pLinha6 = new Paragraph();
		pLinha6.add(new Chunk("( " + getTituloRegistradoBradesco() + " ) 30 - T�tulos registrados na Cobran�a Bradesco: ", fTextoTabela));
		pLinha6.add(new Chunk("por meio do CNPJ (cadastro Nacional de Pessoas Jur�dicas) da ", fTexto));
		pLinha6.add(new Chunk("Cliente", fTextoTabela));
		pLinha6.add(new Chunk(", o sistema ", fTexto));
		pLinha6.add(new Chunk("PAG-FOR BRADESCO realiza um rastreamento no cadastro da cobran�a, ", fTexto));
		pLinha6.add(new Chunk("comparando os t�tulos registrados com os CNPJs das empresas cadastradas no ", fTexto));
		pLinha6.add(new Chunk("sistema. Monta-se, ent�o, um arquivo com o resultado da pesquisa, que fica ", fTexto));
		pLinha6.add(new Chunk("dispon�vel � ", fTexto));
		pLinha6.add(new Chunk("Cliente", fTextoTabela));
		pLinha6.add(new Chunk(", para autoriza��o do pagamento.", fTexto));
		pLinha6.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		Paragraph pLinha7 = new Paragraph();
		pLinha7.add(new Chunk("( " + getTituloRegistradoOutrosBancos() + " ) 31 - T�tulos registrados na Cobran�a de outros Bancos: ", fTextoTabela));
		pLinha7.add(new Chunk("para o pagamento de t�tulos � necess�rio apenas a captura dos dados, por interm�dio ", fTexto));
		pLinha7.add(new Chunk("da leitura do c�digo de barras ou da men��o dos n�meros constantes ", fTexto));
		pLinha7.add(new Chunk("da linha digit�vel, localizada na parte superior das papeletas.", fTexto));
		pLinha7.setAlignment(Element.ALIGN_JUSTIFIED);
		
		
		PdfPTable tabelaInterna = new PdfPTable(1);
		tabelaInterna.setWidthPercentage(100f);
		
		PdfPCell celulaBordaPrincipal = new PdfPCell();
		celulaBordaPrincipal.setBorder(PdfCell.NO_BORDER);
		celulaBordaPrincipal.setBorderWidth(1);
		celulaBordaPrincipal.addElement(tabelaInterna);
		
		tabelaInterna.addCell(pLinha);
		tabelaInterna.addCell(pLinha2);
		tabelaInterna.addCell(pLinha3);
		tabelaInterna.addCell(pLinha4);
		tabelaInterna.addCell(pLinha5);
		tabelaInterna.addCell(pLinha6);
		tabelaInterna.addCell(pLinha7);

		tabela.addCell(celulaBordaPrincipal);

		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo2.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo2(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Primeira: ", fTextoTabela));
		pLinha.add(new Chunk("Anexo tem por objeto a presta��o pelo ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("ao ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("dos servi�os de PAG-FOR BRADESCO - Pagamento a Fornecedores (�PAG-FOR BRADESCO�).", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo3(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		pLinha.add(new Chunk("O servi�o PAG-FOR BRADESCO tem como finalidade ", fTexto));
		pLinha.add(new Chunk("a simplifica��o e automa��o dos processos de contas a pagar, por meio de transmiss�o de dados, ", fTexto));
		pLinha.add(new Chunk("via computador, que conecta o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("ao ", fTexto));
		pLinha.add(new Chunk("Banco.", fTextoTabela));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo4.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo4(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Segunda: ", fTextoTabela));
		pLinha.add(new Chunk("As modalidades de pagamento que comp�em o PAG-FOR BRADESCO ", fTexto));
		pLinha.add(new Chunk("encontram-se relacionadas no quadro acima - Modalidades de Pagamentos a Fornecedores, ", fTexto));
		pLinha.add(new Chunk("no pre�mbulo deste Anexo.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo5.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo5(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		pLinha.add(new Chunk("Para a realiza��o do servi�o objeto deste Anexo, ", fTexto));
		pLinha.add(new Chunk("o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("fornecer� ao ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("todas as informa��es ", fTexto));
		pLinha.add(new Chunk("necess�rias � efetiva��o dos pagamentos.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo6.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo6(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Terceira: ", fTextoTabela));
		pLinha.add(new Chunk("Os servi�os que o ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("prestar�, de acordo com a cl�usula primeira, ", fTexto));
		pLinha.add(new Chunk("dever�o ser realizados por meio de transmiss�o de arquivos eletr�nicos, contendo ", fTexto));
		pLinha.add(new Chunk("todos os dados necess�rios, conforme layout ou software fornecido ao Cliente.", fTexto));
		pLinha.add(new Chunk("Cliente.", fTextoTabela));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo7.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo7(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Quarta: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("se obriga a tomar as cautelas necess�rias para a correta transcri��o ", fTexto));
		pLinha.add(new Chunk("de todos os dados dos pagamentos a serem realizados com base neste Anexo, isentando o , ", fTexto));
		pLinha.add(new Chunk("Banco, ", fTextoTabela));
		pLinha.add(new Chunk("neste ato, de toda e qualquer responsabilidade relativa a eventuais reclama��es, preju�zos, perdas e danos, ", fTexto));
		pLinha.add(new Chunk("lucros cessantes e/ou danos emergentes, inclusive perante terceiros, decorrentes de erros, ", fTexto));
		pLinha.add(new Chunk("falhas, irregularidades e omiss�es dos dados constantes de cada pagamento.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo8.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo8(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Quinta: ", fTextoTabela));
		pLinha.add(new Chunk("Os t�tulos registrados na Cobran�a Bradesco somente ser�o rastreados ", fTexto));
		pLinha.add(new Chunk("e informados ao", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("se o n�mero completo do CNPJ (base/filial/d�gito) ou CPF do ", fTexto));
		pLinha.add(new Chunk("sacado for o mesmo do seu registro como cliente do ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("e previamente estar cadastrado ", fTexto));
		pLinha.add(new Chunk("no PAG-FOR BRADESCO, na Modalidade 30.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo9.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo9(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Sexta: ", fTextoTabela));
		pLinha.add(new Chunk("Os arquivos contendo os agendamentos poder�o ser encaminhados ", fTexto));
		pLinha.add(new Chunk("ao ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", diariamente, 24 (vinte e quatro) horas por dia.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo10.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo10(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		pLinha.add(new Chunk("Para a efetiva��o de pagamentos no mesmo dia do agendamento, ", fTexto));
		pLinha.add(new Chunk("os arquivos, obrigatoriamente, dever�o estar em poder do ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("(Centro de Processamento de Dados) ", fTexto));
		pLinha.add(new Chunk("at� �s 19h30min (hor�rio de Bras�lia), desse mesmo dia, com exce��o da modalidade TED, ", fTexto));
		pLinha.add(new Chunk("que  possui hor�rio espec�fico e vari�vel ", fTexto));
		pLinha.add(new Chunk("para transmiss�o, de acordo com o estipulado pelos respectivos �rg�os gestores.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo11.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo11(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", ap�s conclu�do o processamento (o qual ocorre de hora em hora) ", fTexto));
		pLinha.add(new Chunk("tornar� dispon�vel o arquivo retorno dos agendamentos, ficando sob a ", fTexto));
		pLinha.add(new Chunk("responsabilidade do Cliente a confirma��o das informa��es ", fTexto));
		pLinha.add(new Chunk("contidas nesses arquivos, no mesmo dia do agendamento.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo12.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo12(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		pLinha.add(new Chunk("Na hip�tese de ocorr�ncia de casos fortuitos ou de for�a maior que ", fTexto));
		pLinha.add(new Chunk("impe�am o agendamento e/ou pagamento por meio do PAG-FOR BRADESCO, ", fTexto));
		pLinha.add(new Chunk("o ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk("e o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", em comum acordo, tomar�o as medidas necess�rias ", fTexto));
		pLinha.add(new Chunk("para que os pagamentos sejam efetuados por interm�dio da quita��o ", fTexto));
		pLinha.add(new Chunk("de documentos f�sicos nos guich�s das ag�ncias do Banco, visando a atender o interesse das partes envolvidas.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo13.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo13(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula S�tima: ", fTextoTabela));
		pLinha.add(new Chunk("Para a realiza��o dos pagamentos agendados e autorizados para o dia, ", fTexto));
		pLinha.add(new Chunk("o ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("consultar� a(s) conta(s) corrente(s) n.� ", fTexto));
		pLinha.add(new Chunk(saidaImprimirContrato.getCdContaAgenciaGestor() + "-" + saidaImprimirContrato.getCdDigitoContaGestor(), fTexto));
		pLinha.add(new Chunk(", cadastrada(s) na(s) ag�ncia(s) ", fTexto));
		pLinha.add(new Chunk(saidaImprimirContrato.getCdAgenciaGestorContrato() + "-" + saidaImprimirContrato.getCdDigitoAgenciaGestor(), fTexto));
		pLinha.add(new Chunk(", do Banco Bradesco S.A. ", fTexto));
		pLinha.add(new Chunk("de titularidade do ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("e cadastrada(s) no PAG-FOR BRADESCO. ", fTexto));
		pLinha.add(new Chunk("A consulta se dar�, diariamente, a partir das 20h30min (hor�rio de Bras�lia), exceto para as Modalidades ", fTexto));
		pLinha.add(new Chunk("05 - Cr�dito em Conta Real Time e 08 -TED, cuja consulta ocorre assim que o arquivo remessa � processado pelo ", fTexto));
		pLinha.add(new Chunk("Banco.", fTextoTabela));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo14.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo14(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		pLinha.add(new Chunk("Caso o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("venha a utilizar-se da modalidade Lista de D�bito, ", fTexto));
		pLinha.add(new Chunk("para a efetiva��o dos pagamentos, o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("autoriza o", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("a efetivar os d�bitos respectivos na(s) ", fTexto));
		pLinha.add(new Chunk("conta(s) n.� ", fTexto));
		pLinha.add(new Chunk(saidaImprimirContrato.getCdContaAgenciaGestor() + "-" + saidaImprimirContrato.getCdDigitoContaGestor(), fTexto));
		pLinha.add(new Chunk(", cadastrada(s) na(s) ag�ncia(s) ", fTexto));
		pLinha.add(new Chunk(saidaImprimirContrato.getCdAgenciaGestorContrato() + "-" + saidaImprimirContrato.getCdDigitoAgenciaGestor(), fTexto));
		pLinha.add(new Chunk(" de titularidade do Cliente, mediante a apresenta��o ", fTexto));
		pLinha.add(new Chunk("de uma Lista de D�bito, na qual dever�o constar a raz�o social, ", fTexto));
		pLinha.add(new Chunk("CNPJ e ag�ncia do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", nome dos fornecedores favorecidos, valor de cada pagamento, ", fTexto));
		pLinha.add(new Chunk("n�mero da Lista de D�bito, quantidade e valor total dos pagamentos a serem ", fTexto));
		pLinha.add(new Chunk("efetuados no dia devidamente assinada pelo(s) representante(s) e/ou procurador(es) do ", fTexto));
		pLinha.add(new Chunk("Cliente.", fTextoTabela));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo15.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo15(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco :", fTextoTabela));
		pLinha.add(new Chunk("n�o se responsabilizar� pela n�o realiza��o dos pagamentos, nos seguintes casos:", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo16.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo16(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("a) ", fTextoTabela));
		pLinha.add(new Chunk("falhas ou omiss�es nas informa��es prestadas pelo ", fTexto));
		pLinha.add(new Chunk("Cliente;", fTextoTabela));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo17.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo17(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("b) ", fTextoTabela));
		pLinha.add(new Chunk("na entrega de informa��es pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", obedecendo aos seguintes hor�rios limites: ", fTexto));
		pLinha.add(new Chunk("(I) TED (Transfer�ncia Eletr�nica Dispon�vel) - at� �s 15h30min (hor�rio de Bras�lia), ", fTexto));
		pLinha.add(new Chunk("(II) Outras Modalidades - at� �s 19h30min (hor�rio de Bras�lia);", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo18.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo18(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("c) ", fTextoTabela));
		pLinha.add(new Chunk("n�o autoriza��o pelo ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("dos pagamentos agendados para o respectivo dia; e", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo19.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo19(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("d) ", fTextoTabela));
		pLinha.add(new Chunk("inexist�ncia de saldo dispon�vel na(s) conta(s) corrente(s) do ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("para os pagamentos agendados.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo20.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo20(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("declara estar ciente que a quita��o efetiva dos compromissos ", fTexto));
		pLinha.add(new Chunk("nos termos deste Anexo, que envolvam outras institui��es financeiras, depender� ", fTexto));
		pLinha.add(new Chunk("do regular funcionamento do banco cedente: (I) junto �s C�maras de Processamento, ", fTexto));
		pLinha.add(new Chunk("notadamente perante a C�mara Interbanc�ria de Pagamentos - CIP; ", fTexto));
		pLinha.add(new Chunk("e (II) em conformidade com as regras do Banco Central do Brasil e Conselho Monet�rio Nacional.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo21.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo21(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Oitava: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("se obriga a provisionar sua(s) conta(s) corrente(s) com valor ", fTexto));
		pLinha.add(new Chunk("suficiente para a efetiva��o dos pagamentos, a fim de que o ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("possa efetuar os d�bitos respectivos. ", fTexto));
		pLinha.add(new Chunk("Na hip�tese do saldo dispon�vel na(s) conta(s) corrente(s) do ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("ser ", fTexto));
		pLinha.add(new Chunk("insuficiente para a quita��o total das obriga��es agendadas, o pagamento ser� efetuado at� ", fTexto));
		pLinha.add(new Chunk("o montante do saldo dispon�vel, observada a ordem crescente dos valores ", fTexto));
		pLinha.add(new Chunk("dos pagamentos a serem efetuados, em cada modalidade.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo22.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo22(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco ", fTexto));
		pLinha.add(new Chunk("poder�, a seu crit�rio, efetuar pagamentos em montante superior ao saldo ", fTexto));
		pLinha.add(new Chunk("dispon�vel na(s) conta(s) corrente(s) indicada(s) pelo ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("e cadastrada(s) no PAG-FOR BRADESCO. ", fTexto));
		pLinha.add(new Chunk("Se isso vier a ocorrer, o(s) valor(es) disponibilizado(s) pelo ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("ao ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("ser� havido como ", fTexto));
		pLinha.add(new Chunk("�adiantamento a depositante�, sujeitando-se o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("a (I) restitu�-lo(s) acrescido(s) de encargos calculados com base na taxa ", fTexto));
		pLinha.add(new Chunk("m�dia dos dep�sitos interbanc�rios de 1 (um) dia (CDI/CETIP/EXTRA GRUPO), mais multa de 2% (dois por ", fTexto));
		pLinha.add(new Chunk("cento) sobre o valor apurado, entre a data de liquida��o e a data da efetiva cobran�a, e a (II) arcar com ", fTexto));
		pLinha.add(new Chunk("o Imposto sobre Opera��es de Cr�dito, C�mbio e Seguro, ou Relativas a T�tulos ou Valores Mobili�rios ", fTexto));
		pLinha.add(new Chunk("- IOF, quando devido, e outras despesas pr�prias de opera��es de cr�dito, nelas se ", fTexto));
		pLinha.add(new Chunk("incluindo os encargos de adiantamento a depositante.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo23.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo23(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Nona: ", fTextoTabela));
		pLinha.add(new Chunk("Para pagamentos agendados cuja data de d�bito seja um dia n�o �til ", fTexto));
		pLinha.add(new Chunk("(s�bado, domingo ou feriado, inclusive local), ", fTexto));
		pLinha.add(new Chunk("a quita��o ser� efetivada, sempre, no pr�ximo dia �til subseq�ente.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo24.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo24(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Dez: ", fTextoTabela));
		pLinha.add(new Chunk("Pela presta��o dos servi�os objeto deste Anexo, o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("pagar� ao ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", dependendo ", fTexto));
		pLinha.add(new Chunk("dos servi�os utilizados, as tarifas �Cr�dito em Conta�, �Cr�dito em Conta F�cil Real Time�, ", fTexto));
		pLinha.add(new Chunk("�TED�, �DOC� e/ou �Cheque OP�, conforme negociado entre as partes, nos valores, ", fTexto));
		pLinha.add(new Chunk("os quais ser�o atualizados anualmente e monetariamente pela varia��o do IGPM ou outro ", fTexto));
		pLinha.add(new Chunk("�ndice que vier a substitu�-lo, e periodicidade previstos na Tabela de, �Tarifas de Servi�os Banc�rios� ", fTexto));
		pLinha.add(new Chunk("afixada nas Ag�ncias do Banco e no site �www.bradesco.com.br�, nos ", fTexto));
		pLinha.add(new Chunk("termos das normas editadas pelo Conselho Monet�rio Nacional e Banco Central do Brasil.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo25.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo25(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("desde j� autoriza o ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("a debitar de sua(s) conta(s) corrente(s) ", fTexto));
		pLinha.add(new Chunk("o(s) valor(es) da(s) tarifa(s) referida(s) no caput desta cl�usula dez, no prazo estabelecido entre as partes.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo26.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo26(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Onze: ", fTextoTabela));
		pLinha.add(new Chunk("Para todos os fins e efeitos de direito, o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("reconhece e reconhecer� como ", fTexto));
		pLinha.add(new Chunk("l�quido e certo, o valor de todos os lan�amentos de d�bitos, efetuados em sua(s) conta(s) corrente(s), ", fTexto));
		pLinha.add(new Chunk("desde que autorizados nos termos deste Anexo.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo27.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo27(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("se compromete a disponibilizar, diariamente, a partir das 6h30min ", fTexto));
		pLinha.add(new Chunk("(hor�rio de Bras�lia), o arquivo retorno de confirma��o dos pagamentos efetuados no dia anterior.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo28.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo28(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Doze: ", fTextoTabela));
		pLinha.add(new Chunk("Nos casos de t�tulos (papeletas) registrados na Cobran�a Bradesco (Modalidade 30) ", fTexto));
		pLinha.add(new Chunk("e n�o rastreados, e os registrados na Cobran�a de outras institui��es financeiras (Modalidade 31), ", fTexto));
		pLinha.add(new Chunk("o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("dever� manter os respectivos t�tulos (papeletas) em seu poder e � disposi��o do ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("por um prazo de 10 (dez) dias �teis ap�s a data do pagamento do t�tulo (papeleta).", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo29.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo29(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("se obriga a fornecer, quando solicitado pelo ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", c�pia desses ", fTexto));
		pLinha.add(new Chunk("t�tulos (papeletas), que ser�o utilizadas para dirimir d�vidas ou irregularidades e definir com clareza, ", fTexto));
		pLinha.add(new Chunk("o favorecido do respectivo cr�dito.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo30.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo30(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cada uma das partes agir� com toda a boa-f� e dilig�ncia necess�rias para ", fTexto));
		pLinha.add(new Chunk("a perfeita execu��o do todos os termos e condi��es estabelecidos neste contrato. ", fTexto));
		pLinha.add(new Chunk("Na hip�tese de qualquer uma das partes vier a ser demandada judicialmente ou extrajudicialmente ", fTexto));
		pLinha.add(new Chunk("por ato ou fato causado pela outra, esta tomar� as medidas necess�rias para isentar a parte inocente, ", fTexto));
		pLinha.add(new Chunk("assumindo toda e qualquer responsabilidade relativa a eventuais preju�zos, perdas e danos, ", fTexto));
		pLinha.add(new Chunk("lucros cessantes e danos emergentes, inclusive perante terceiros, decorrentes de quaisquer erros, ", fTexto));
		pLinha.add(new Chunk("falhas, irregularidades, omiss�es ou atos il�citos comprovadamente praticados pelas ", fTexto));
		pLinha.add(new Chunk("partes, por seu prepostos, empregados ou terceiros contratados.", fTexto));
		pLinha.add(new Chunk("Cl�usula Treze: ", fTextoTabela));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo31.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo31(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Por se acharem de pleno acordo, com tudo aqui pactuado, as partes firmam este Anexo em", fTexto));
		pLinha.add(new Chunk("03 (tr�s) vias de igual teor e forma, juntamente com as testemunhas abaixo:", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher data.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherData(Document document) throws DocumentException {
		Paragraph data = new Paragraph(new Chunk("Osasco, " + DateUtils.formatarDataPorExtenso(new Date()) + ".", fTexto));
		data.setAlignment(Paragraph.ALIGN_RIGHT);
		document.add(data);
	}
	
	/**
	 * Assinatura um.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaUm(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  CLIENTE:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  BANCO BRADESCO S/A:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  Ag�ncia:", fTexto));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Esq);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher assinaturas3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAssinaturas3(Document document) throws DocumentException {
		Paragraph testemunha = new Paragraph(new Chunk("  TESTEMUNHAS:"));
		testemunha.setAlignment(Element.ALIGN_LEFT);
		document.add(testemunha);
	}
	
	/**
	 * Assinatura dois.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaDois(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
		celula1.addElement(pLinha3Esq);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Dir);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Assinalar valor.
	 */
	public void assinalarValor(){
		if (saidaImprimirContrato.getDsCreditoConta().equalsIgnoreCase("S")){
			setCreditoConta("X");
			setCreditoContaRealTime("X");
		}else{
			setCreditoConta("");
			setCreditoContaRealTime("");
		}
		
		if (saidaImprimirContrato.getDsChequeOp().equalsIgnoreCase("S")){
			setChequeOP("X");
		}else{
			setChequeOP("");
		}
		
		if (saidaImprimirContrato.getDsDoc().equalsIgnoreCase("S")){
			setDocCompe("X");
		}else{
			setDocCompe("");
		}
		
		if (saidaImprimirContrato.getDsTed().equalsIgnoreCase("S")){
			setTed("X");
		}else{
			setTed("");
		}
		
		if (saidaImprimirContrato.getDsTituloBradesco().equalsIgnoreCase("S")){
			setTituloRegistradoBradesco("X");
		}else{
			setTituloRegistradoBradesco("");
		}
		
		if (saidaImprimirContrato.getDsTituloOutros().equalsIgnoreCase("S")){
			setTituloRegistradoOutrosBancos("X");
		}else{
			setTituloRegistradoOutrosBancos("");
		}
	}

	/**
	 * Get: saidaImprimirContrato.
	 *
	 * @return saidaImprimirContrato
	 */
	public ImprimirContratoSaidaDTO getSaidaImprimirContrato() {
		return saidaImprimirContrato;
	}

	/**
	 * Set: saidaImprimirContrato.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public void setSaidaImprimirContrato(
			ImprimirContratoSaidaDTO saidaImprimirContrato) {
		this.saidaImprimirContrato = saidaImprimirContrato;
	}

	/**
	 * Get: chequeOP.
	 *
	 * @return chequeOP
	 */
	public String getChequeOP() {
		return chequeOP;
	}

	/**
	 * Set: chequeOP.
	 *
	 * @param chequeOP the cheque op
	 */
	public void setChequeOP(String chequeOP) {
		this.chequeOP = chequeOP;
	}

	/**
	 * Get: creditoConta.
	 *
	 * @return creditoConta
	 */
	public String getCreditoConta() {
		return creditoConta;
	}

	/**
	 * Set: creditoConta.
	 *
	 * @param creditoConta the credito conta
	 */
	public void setCreditoConta(String creditoConta) {
		this.creditoConta = creditoConta;
	}

	/**
	 * Get: creditoContaRealTime.
	 *
	 * @return creditoContaRealTime
	 */
	public String getCreditoContaRealTime() {
		return creditoContaRealTime;
	}

	/**
	 * Set: creditoContaRealTime.
	 *
	 * @param creditoContaRealTime the credito conta real time
	 */
	public void setCreditoContaRealTime(String creditoContaRealTime) {
		this.creditoContaRealTime = creditoContaRealTime;
	}

	/**
	 * Get: docCompe.
	 *
	 * @return docCompe
	 */
	public String getDocCompe() {
		return docCompe;
	}

	/**
	 * Set: docCompe.
	 *
	 * @param docCompe the doc compe
	 */
	public void setDocCompe(String docCompe) {
		this.docCompe = docCompe;
	}

	/**
	 * Get: ted.
	 *
	 * @return ted
	 */
	public String getTed() {
		return ted;
	}

	/**
	 * Set: ted.
	 *
	 * @param ted the ted
	 */
	public void setTed(String ted) {
		this.ted = ted;
	}

	/**
	 * Get: tituloRegistradoBradesco.
	 *
	 * @return tituloRegistradoBradesco
	 */
	public String getTituloRegistradoBradesco() {
		return tituloRegistradoBradesco;
	}

	/**
	 * Set: tituloRegistradoBradesco.
	 *
	 * @param tituloRegistradoBradesco the titulo registrado bradesco
	 */
	public void setTituloRegistradoBradesco(String tituloRegistradoBradesco) {
		this.tituloRegistradoBradesco = tituloRegistradoBradesco;
	}

	/**
	 * Get: tituloRegistradoOutrosBancos.
	 *
	 * @return tituloRegistradoOutrosBancos
	 */
	public String getTituloRegistradoOutrosBancos() {
		return tituloRegistradoOutrosBancos;
	}

	/**
	 * Set: tituloRegistradoOutrosBancos.
	 *
	 * @param tituloRegistradoOutrosBancos the titulo registrado outros bancos
	 */
	public void setTituloRegistradoOutrosBancos(String tituloRegistradoOutrosBancos) {
		this.tituloRegistradoOutrosBancos = tituloRegistradoOutrosBancos;
	}

}
