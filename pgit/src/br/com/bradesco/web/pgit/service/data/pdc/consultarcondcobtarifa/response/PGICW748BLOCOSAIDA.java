/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class PGICW748BLOCOSAIDA.
 * 
 * @version $Revision$ $Date$
 */
public class PGICW748BLOCOSAIDA implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPeriodicidadeCobrancaTarifa
     */
    private int _cdPeriodicidadeCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeCobrancaTarif
     */
    private boolean _has_cdPeriodicidadeCobrancaTarifa;

    /**
     * Field _dsPeriodicidadeCobrancaTarifa
     */
    private java.lang.String _dsPeriodicidadeCobrancaTarifa;

    /**
     * Field _qtDiaCobrancaTarifa
     */
    private int _qtDiaCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _qtDiaCobrancaTarifa
     */
    private boolean _has_qtDiaCobrancaTarifa;

    /**
     * Field _nrFechamentoApuracaoTarifa
     */
    private int _nrFechamentoApuracaoTarifa = 0;

    /**
     * keeps track of state for field: _nrFechamentoApuracaoTarifa
     */
    private boolean _has_nrFechamentoApuracaoTarifa;


      //----------------/
     //- Constructors -/
    //----------------/

    public PGICW748BLOCOSAIDA() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.response.PGICW748BLOCOSAIDA()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPeriodicidadeCobrancaTarifa
     * 
     */
    public void deleteCdPeriodicidadeCobrancaTarifa()
    {
        this._has_cdPeriodicidadeCobrancaTarifa= false;
    } //-- void deleteCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method deleteNrFechamentoApuracaoTarifa
     * 
     */
    public void deleteNrFechamentoApuracaoTarifa()
    {
        this._has_nrFechamentoApuracaoTarifa= false;
    } //-- void deleteNrFechamentoApuracaoTarifa() 

    /**
     * Method deleteQtDiaCobrancaTarifa
     * 
     */
    public void deleteQtDiaCobrancaTarifa()
    {
        this._has_qtDiaCobrancaTarifa= false;
    } //-- void deleteQtDiaCobrancaTarifa() 

    /**
     * Returns the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeCobrancaTarifa'.
     */
    public int getCdPeriodicidadeCobrancaTarifa()
    {
        return this._cdPeriodicidadeCobrancaTarifa;
    } //-- int getCdPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'dsPeriodicidadeCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeCobrancaTarifa'.
     */
    public java.lang.String getDsPeriodicidadeCobrancaTarifa()
    {
        return this._dsPeriodicidadeCobrancaTarifa;
    } //-- java.lang.String getDsPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @return int
     * @return the value of field 'nrFechamentoApuracaoTarifa'.
     */
    public int getNrFechamentoApuracaoTarifa()
    {
        return this._nrFechamentoApuracaoTarifa;
    } //-- int getNrFechamentoApuracaoTarifa() 

    /**
     * Returns the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'qtDiaCobrancaTarifa'.
     */
    public int getQtDiaCobrancaTarifa()
    {
        return this._qtDiaCobrancaTarifa;
    } //-- int getQtDiaCobrancaTarifa() 

    /**
     * Method hasCdPeriodicidadeCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeCobrancaTarifa()
    {
        return this._has_cdPeriodicidadeCobrancaTarifa;
    } //-- boolean hasCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method hasNrFechamentoApuracaoTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFechamentoApuracaoTarifa()
    {
        return this._has_nrFechamentoApuracaoTarifa;
    } //-- boolean hasNrFechamentoApuracaoTarifa() 

    /**
     * Method hasQtDiaCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaCobrancaTarifa()
    {
        return this._has_qtDiaCobrancaTarifa;
    } //-- boolean hasQtDiaCobrancaTarifa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @param cdPeriodicidadeCobrancaTarifa the value of field
     * 'cdPeriodicidadeCobrancaTarifa'.
     */
    public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa)
    {
        this._cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
        this._has_cdPeriodicidadeCobrancaTarifa = true;
    } //-- void setCdPeriodicidadeCobrancaTarifa(int) 

    /**
     * Sets the value of field 'dsPeriodicidadeCobrancaTarifa'.
     * 
     * @param dsPeriodicidadeCobrancaTarifa the value of field
     * 'dsPeriodicidadeCobrancaTarifa'.
     */
    public void setDsPeriodicidadeCobrancaTarifa(java.lang.String dsPeriodicidadeCobrancaTarifa)
    {
        this._dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
    } //-- void setDsPeriodicidadeCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @param nrFechamentoApuracaoTarifa the value of field
     * 'nrFechamentoApuracaoTarifa'.
     */
    public void setNrFechamentoApuracaoTarifa(int nrFechamentoApuracaoTarifa)
    {
        this._nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
        this._has_nrFechamentoApuracaoTarifa = true;
    } //-- void setNrFechamentoApuracaoTarifa(int) 

    /**
     * Sets the value of field 'qtDiaCobrancaTarifa'.
     * 
     * @param qtDiaCobrancaTarifa the value of field
     * 'qtDiaCobrancaTarifa'.
     */
    public void setQtDiaCobrancaTarifa(int qtDiaCobrancaTarifa)
    {
        this._qtDiaCobrancaTarifa = qtDiaCobrancaTarifa;
        this._has_qtDiaCobrancaTarifa = true;
    } //-- void setQtDiaCobrancaTarifa(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return PGICW748BLOCOSAIDA
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.response.PGICW748BLOCOSAIDA unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.response.PGICW748BLOCOSAIDA) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.response.PGICW748BLOCOSAIDA.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.response.PGICW748BLOCOSAIDA unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
