package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 11/12/15.
 */
public class ListarServicoRelacionadoPgitCdpsSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem;

    /** The mensagem. */
    private String mensagem;

    /** The nr linhas. */
    private Integer nrLinhas;

    /** The ocorrencias. */
    private List<ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO> ocorrencias;

    /**
     * Sets the cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Gets the cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Sets the mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Gets the mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Sets the nr linhas.
     *
     * @param nrLinhas the nr linhas
     */
    public void setNrLinhas(Integer nrLinhas) {
        this.nrLinhas = nrLinhas;
    }

    /**
     * Gets the nr linhas.
     *
     * @return the nr linhas
     */
    public Integer getNrLinhas() {
        return this.nrLinhas;
    }

    /**
     * Sets the ocorrencias.
     *
     * @param ocorrencias the ocorrencias
     */
    public void setOcorrencias(List<ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    /**
     * Gets the ocorrencias.
     *
     * @return the ocorrencias
     */
    public List<ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}