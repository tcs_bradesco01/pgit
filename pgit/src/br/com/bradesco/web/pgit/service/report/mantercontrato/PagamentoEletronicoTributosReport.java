/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;

import java.awt.Color;
import java.util.Date;

import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;
import br.com.bradesco.web.pgit.utils.DateUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: PagamentoEletronicoTributosReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PagamentoEletronicoTributosReport extends BasicPdfReport {
	
	/** Atributo fTextoTabelaNormal. */
	private Font fTextoTabelaNormal = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabelaNegrito. */
	private Font fTextoTabelaNegrito = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
	
	/** Atributo fTextoTabelaCinza. */
	private Font fTextoTabelaCinza = new Font(Font.TIMES_ROMAN, 8, Font.BOLD,Color.GRAY);
	
	/** Atributo imagemRelatorio. */
	private Image imagemRelatorio = null;
	
	/** Atributo saidaImprimirContrato. */
	private ImprimirContratoSaidaDTO saidaImprimirContrato;
	
	/**
	 * Pagamento eletronico tributos report.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public PagamentoEletronicoTributosReport(ImprimirContratoSaidaDTO saidaImprimirContrato) {
		super();
		this.saidaImprimirContrato = saidaImprimirContrato;
		this.imagemRelatorio = getImagemRelatorio(); 
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	protected void popularPdf(Document document) throws DocumentException {

		preencherCabec(document);
		document.add(Chunk.NEWLINE);
		preencherAnexo(document);
		document.add(Chunk.NEWLINE);
		preencherTitulo(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaCondicoes(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaPrimeira(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaSegunda(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafoPrimeiro(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafoSegundo(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiro(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafoQuarto(document);
		document.add(Chunk.NEWLINE);
		preencherParagrafoQuinto(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceira(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaQuarta(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaPrimeiro(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaSegundo(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaTerceiro(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaQuarto(document);
		document.add(Chunk.NEWLINE);
		preencherClausulaQuinta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaPrimeiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaSegundo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaTerceiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaSexta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaCondicao(document);
		
		document.add(Chunk.NEXTPAGE);
		preencherData(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaUm(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherAssinaturas3(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaDois(document);
				
	}
	
	/**
	 * Preencher cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		tabela.setWidths(new float[] {30.0f, 70.0f});
		
		Paragraph pLinha1Esq = new Paragraph(new Chunk("ANEXO AO CONTRATO DE PRESTA��O DE SERVI�OS DE PAGAMENTOS.", fTextoTabelaCinza));
		pLinha1Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfPCell.NO_BORDER);
		celula1.addElement(imagemRelatorio);
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher anexo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAnexo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("ANEXO 5", fTextoTabelaCinza));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher titulo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherTitulo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("Pagamento Eletr�nico de Tributos (Taxas, Impostos e Contribui��es)", fTextoTabelaNegrito));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher clausula condicoes.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaCondicoes(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Titulo 1 */
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("As partes nomeadas e qualificadas no pre�mbulo do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Contrato", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", ao qual este Anexo faz parte, denominadas ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco e Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", resolvem estabelecer as condi��es operacionais e comerciais espec�ficas � presta��o dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco e Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" servi�os contratados.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);

		document.add(tabela);
	}

	/**
	 * Preencher clausula primeira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaPrimeira(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Primeira: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Este Anexo tem por objeto a presta��o pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente ", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", dos servi�os de ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Pagamento Eletr�nico de Tributos, por interm�dio de Software exclusivo, fornecido pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco.", fTextoTabelaNegrito));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula segunda.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherClausulaSegunda(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Segunda: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" dever� fornecer ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" todas as informa��es necess�rias � realiza��o dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("pagamentos de seus Tributos, Taxas e Contribui��es, de compet�ncia dos respectivos �rg�os.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherParagrafoPrimeiro(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" tomar� as provid�ncias necess�rias para a correta transcri��o de todos os ", fTextoTabelaNormal));
		pLinha.add(new Chunk("dados dos pagamentos a serem realizados com base neste Anexo, isentando o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", neste ato, de toda e ", fTextoTabelaNormal));
		pLinha.add(new Chunk("qualquer responsabilidade relativa a eventuais reclama��es, preju�zos, perdas e danos, lucros cessantes e/ou ", fTextoTabelaNormal));
		pLinha.add(new Chunk("emergentes, inclusive perante terceiros, decorrentes de erros, falhas, irregularidades e omiss�es dos dados ", fTextoTabelaNormal));
		pLinha.add(new Chunk("constantes de cada pagamento.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherParagrafoSegundo(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" dever� transmitir os arquivos ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" (Centro de Processamento de Dados - ", fTextoTabelaNormal));
		pLinha.add(new Chunk("na Cidade de Deus - Osasco - S�o Paulo), obrigatoriamente, at� �s 18h00 (Hor�rio de Bras�lia), do dia do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("pagamento, para que se viabilize o cumprimento dos pagamentos.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	private void preencherParagrafoTerceiro(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Terceiro: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Caber� ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" provisionar a(s) sua(s) conta(s) " + saidaImprimirContrato.getCdContaAgenciaGestor()+ "-" + saidaImprimirContrato.getCdDigitoContaGestor() , fTextoTabelaNormal));
		pLinha.add(new Chunk(", ag�ncia " + saidaImprimirContrato.getCdAgenciaGestorContrato()+ "-" + saidaImprimirContrato.getCdDigitoAgenciaGestor()+ " , cadastrada ", fTextoTabelaNormal));
		pLinha.add(new Chunk("no Banco Bradesco S.A., com valor suficiente para efetiva��o dos pagamentos, ficando o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" autorizado a ", fTextoTabelaNormal));
		pLinha.add(new Chunk("nela(s) efetivar os d�bitos respectivos, sendo que ele n�o se responsabilizar� pela n�o realiza��o dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("pagamentos nos seguintes casos:", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 2 */
		Paragraph pLinha2 = new Paragraph();
		
		pLinha2.add(new Chunk("a) falhas nas informa��es prestadas;", fTextoTabelaNormal));
		pLinha2.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 3 */
		Paragraph pLinha3 = new Paragraph();
		
		pLinha3.add(new Chunk("b) atraso na entrega de informa��es;", fTextoTabelaNormal));
		pLinha3.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 4 */
		Paragraph pLinha4 = new Paragraph();
		
		pLinha4.add(new Chunk("c) aus�ncia da autoriza��o de d�bito do documento por parte do ", fTextoTabelaNormal));
		pLinha4.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha4.add(new Chunk("; e,", fTextoTabelaNormal));
		pLinha4.setAlignment(Element.ALIGN_JUSTIFIED);
		
		/* Linha 5 */
		Paragraph pLinha5 = new Paragraph();
		
		pLinha5.add(new Chunk("d) inexist�ncia de saldo suficiente e dispon�vel para os pagamentos agendados, em conformidade com o ", fTextoTabelaNormal));
		pLinha5.add(new Chunk("previsto no par�grafo primeiro da cl�usula quarta.", fTextoTabelaNormal));
		pLinha5.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		celula.addElement(new Chunk(Chunk.NEWLINE));
		celula.addElement(pLinha2);
		celula.addElement(new Chunk(Chunk.NEWLINE));
		celula.addElement(pLinha3);
		celula.addElement(new Chunk(Chunk.NEWLINE));
		celula.addElement(pLinha4);
		celula.addElement(new Chunk(Chunk.NEWLINE));
		celula.addElement(pLinha5);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo quarto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoQuarto(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Quarto: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" dever� responsabilizar-se por eventuais perdas e danos, preju�zos, que venham ", fTextoTabelaNormal));
		pLinha.add(new Chunk("a causar a si pr�pria, ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" e a terceiros, e que decorra do acesso e utiliza��o inadequada ou impr�pria ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("sistema por pessoas n�o autorizadas ou credenciadas.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher paragrafo quinto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoQuinto(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Quinto: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Caber� ainda ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", responsabilizar-se por pagamentos agendados ap�s a data de ", fTextoTabelaNormal));
		pLinha.add(new Chunk("vencimento sem os devidos acr�scimos, bem como, quanto � respectiva regulariza��o junto ao �rg�o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("envolvido.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula terceira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTerceira(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Terceira: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" dever� processar os arquivos recebidos do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", efetuando a quita��o dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("pagamentos agendados para o dia, a d�bito da(s) conta(s) corrente(s), desde que exista saldo suficiente e ", fTextoTabelaNormal));
		pLinha.add(new Chunk("dispon�vel nesta(s) conta(s).", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quarta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuarta(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Quarta: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", ap�s conclu�da a transmiss�o/processamento tornar� dispon�vel o arquivo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("retorno, contendo o resultado do processamento, ficando sob a exclusiva responsabilidade do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" realizar ", fTextoTabelaNormal));
		pLinha.add(new Chunk("a confer�ncia e verificar/corrigir os dados dos registros inconsistentes , at� a data programada para o d�bito.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quarta primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaPrimeiro(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("A consulta de saldo ser� feita, diariamente, sempre �s 21h00 (hor�rio de Bras�lia), e ", fTextoTabelaNormal));
		pLinha.add(new Chunk("ser� considerado o saldo dispon�vel naquele momento.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quarta segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaSegundo(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O saldo dispon�vel ser� representado por: a)disponibilidade real na conta corrente; b) ", fTextoTabelaNormal));
		pLinha.add(new Chunk("aplica��es financeiras com baixa autom�tica, registrados naquela conta; c) limite dispon�vel decorrente de ", fTextoTabelaNormal));
		pLinha.add(new Chunk("contrato de: (I) Saque - F�cil ou (II) Conta Corrente Garantida ou (III) Cr�dito Rotativo com Garantia de ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cheques (CRGC).", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quarta terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaTerceiro(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Terceiro: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Na hip�tese do saldo dispon�vel ser insuficiente para a quita��o total das obriga��es ", fTextoTabelaNormal));
		pLinha.add(new Chunk("agendadas, o pagamento ser� efetuado at� o montante do saldo dispon�vel, observada a ordem decrescente dos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("valores dos pagamentos a serem efetuados, com a finalidade de evitar nesses casos, a cobran�a dos acr�scimos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("devidos pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", dos Tributos, Taxas e Contribui��es pagos com atraso.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quarta quarto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaQuarto(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Quarto: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Caso o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" envie agendamentos para datas de d�bito que recaiam em feriados locais ", fTextoTabelaNormal));
		pLinha.add(new Chunk("onde s�o mantidas as contas correntes dos clientes, o sistema rejeitar� o arquivo. Nesse caso o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" dever� ", fTextoTabelaNormal));
		pLinha.add(new Chunk("enviar novo arquivo para 1� (primeiro) dia �til subsequente ao feriado.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quinta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuinta(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Quinta: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" comprovar� os pagamentos efetuados, mediante remessa ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" de arquivo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("retorno, contendo dados dos documentos pagos e n�o pagos.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quinta primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaPrimeiro(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Para os pagamentos realizados, o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", por interm�dio de Software fornecido pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(", poder� emitir comprovante de pagamento, com autentica��o criptografada, contendo todos os dados ", fTextoTabelaNormal));
		pLinha.add(new Chunk("informados no documento quitado, conforme as exig�ncias dos �rg�os competentes.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quinta segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaSegundo(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" disponibilizar�, diariamente, a partir das 8h00 (hor�rio de Bras�lia), o arquivo-", fTextoTabelaNormal));
		pLinha.add(new Chunk("retorno de confirma��o dos pagamentos efetuados do dia anterior, que ficar� � disposi��o do ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" pelo ", fTextoTabelaNormal));
		pLinha.add(new Chunk("prazo m�ximo de 10 (dez) dias �teis, ap�s a data dos pagamentos efetuados, sendo que ap�s este per�odo, os ", fTextoTabelaNormal));
		pLinha.add(new Chunk("arquivos n�o retirados ser�o automaticamente exclu�dos.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula quinta terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaTerceiro(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Terceiro: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("Para todos os fins e efeitos de direito, o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente ", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" reconhecer� como l�quido e certo, o ", fTextoTabelaNormal));
		pLinha.add(new Chunk("valor de todos os lan�amentos de d�bito, efetuados em sua(s) conta(s) corrente(s), desde que autorizados nos ", fTextoTabelaNormal));
		pLinha.add(new Chunk("termos deste Anexo, identificado como Pagamento Eletr�nico de Tributos.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula sexta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaSexta(Document document) throws DocumentException {
		PdfPTable tabela = new PdfPTable(1);
		tabela.setWidthPercentage(100f);
		
		/* Linha 1 */
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Sexta: ", fTextoTabelaNegrito));
		pLinha.add(new Chunk("O ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Cliente", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" n�o pagar� ao ", fTextoTabelaNormal));
		pLinha.add(new Chunk("Banco", fTextoTabelaNegrito));
		pLinha.add(new Chunk(" quaisquer valores pelos servi�os de Pagamento Eletr�nico ", fTextoTabelaNormal));
		pLinha.add(new Chunk("de Tributos ora contratados, porem, n�o estar� isento do pagamento das tarifas de movimenta��o que venham ", fTextoTabelaNormal));
		pLinha.add(new Chunk("a incidir em sua(s) conta(s) corrente(s) decorrentes dos pagamentos efetuados.", fTextoTabelaNormal));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
	
		PdfPCell celula = new PdfPCell();
		celula.setBorder(PdfCell.NO_BORDER);
		
		celula.addElement(pLinha);
		
		tabela.addCell(celula);
		
		document.add(tabela);
	}
	
	/**
	 * Preencher clausula condicao.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaCondicao(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Por se acharem de pleno acordo, com tudo aqui pactuado, ");
		textoClausula.append("as partes firmam este Anexo em 03 (tr�s) vias de igual ");
		textoClausula.append("teor e forma, juntamente com as testemunhas abaixo:");
		Paragraph condicoes4 = new Paragraph(new Chunk(textoClausula.toString(), fTextoTabelaNormal));
		condicoes4.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(condicoes4);
	}
	
	
	/**
	 * Preencher data.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherData(Document document) throws DocumentException {
		Paragraph data = new Paragraph(new Chunk("Osasco, " + DateUtils.formatarDataPorExtenso(new Date()) + ".", fTextoTabelaNormal));
		data.setAlignment(Paragraph.ALIGN_RIGHT);
		document.add(data);
	}
	
	/**
	 * Assinatura um.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaUm(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabelaNegrito));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabelaNegrito));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  CLIENTE:", fTextoTabelaNegrito));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  BANCO BRADESCO S/A:", fTextoTabelaNegrito));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  Ag�ncia:", fTextoTabelaNormal));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Esq);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher assinaturas3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAssinaturas3(Document document) throws DocumentException {
		Paragraph testemunha = new Paragraph(new Chunk("  TESTEMUNHAS:", fTextoTabelaNegrito));
		testemunha.setAlignment(Element.ALIGN_LEFT);
		document.add(testemunha);
	}
	
	/**
	 * Assinatura dois.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaDois(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabelaNegrito));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabelaNegrito));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  NOME:", fTextoTabelaNormal));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("  NOME:", fTextoTabelaNormal));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  CPF:", fTextoTabelaNormal));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  CPF:", fTextoTabelaNormal));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
		celula1.addElement(pLinha3Esq);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Dir);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}

	/**
	 * Get: saidaImprimirContrato.
	 *
	 * @return saidaImprimirContrato
	 */
	public ImprimirContratoSaidaDTO getSaidaImprimirContrato() {
		return saidaImprimirContrato;
	}

	/**
	 * Set: saidaImprimirContrato.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public void setSaidaImprimirContrato(
			ImprimirContratoSaidaDTO saidaImprimirContrato) {
		this.saidaImprimirContrato = saidaImprimirContrato;
	}

}
