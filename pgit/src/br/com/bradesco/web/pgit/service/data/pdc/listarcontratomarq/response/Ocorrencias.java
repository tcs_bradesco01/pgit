/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoJuridicaContrato
     */
    private long _cdPessoJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoJuridicaContrato
     */
    private boolean _has_cdPessoJuridicaContrato;

    /**
     * Field _dsPessoaJuridicaContrato
     */
    private java.lang.String _dsPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _dsTipoContratoNegocio
     */
    private java.lang.String _dsTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdSituacaoContratoNegocio
     */
    private int _cdSituacaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdSituacaoContratoNegocio
     */
    private boolean _has_cdSituacaoContratoNegocio;

    /**
     * Field _dsSituacaoContratoNegocio
     */
    private java.lang.String _dsSituacaoContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoJuridicaContrato
     * 
     */
    public void deleteCdPessoJuridicaContrato()
    {
        this._has_cdPessoJuridicaContrato= false;
    } //-- void deleteCdPessoJuridicaContrato() 

    /**
     * Method deleteCdSituacaoContratoNegocio
     * 
     */
    public void deleteCdSituacaoContratoNegocio()
    {
        this._has_cdSituacaoContratoNegocio= false;
    } //-- void deleteCdSituacaoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdPessoJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoJuridicaContrato'.
     */
    public long getCdPessoJuridicaContrato()
    {
        return this._cdPessoJuridicaContrato;
    } //-- long getCdPessoJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContratoNegocio'.
     */
    public int getCdSituacaoContratoNegocio()
    {
        return this._cdSituacaoContratoNegocio;
    } //-- int getCdSituacaoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaContrato'.
     */
    public java.lang.String getDsPessoaJuridicaContrato()
    {
        return this._dsPessoaJuridicaContrato;
    } //-- java.lang.String getDsPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsSituacaoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContratoNegocio'.
     */
    public java.lang.String getDsSituacaoContratoNegocio()
    {
        return this._dsSituacaoContratoNegocio;
    } //-- java.lang.String getDsSituacaoContratoNegocio() 

    /**
     * Returns the value of field 'dsTipoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoNegocio'.
     */
    public java.lang.String getDsTipoContratoNegocio()
    {
        return this._dsTipoContratoNegocio;
    } //-- java.lang.String getDsTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoJuridicaContrato()
    {
        return this._has_cdPessoJuridicaContrato;
    } //-- boolean hasCdPessoJuridicaContrato() 

    /**
     * Method hasCdSituacaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContratoNegocio()
    {
        return this._has_cdSituacaoContratoNegocio;
    } //-- boolean hasCdSituacaoContratoNegocio() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoJuridicaContrato'.
     * 
     * @param cdPessoJuridicaContrato the value of field
     * 'cdPessoJuridicaContrato'.
     */
    public void setCdPessoJuridicaContrato(long cdPessoJuridicaContrato)
    {
        this._cdPessoJuridicaContrato = cdPessoJuridicaContrato;
        this._has_cdPessoJuridicaContrato = true;
    } //-- void setCdPessoJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoContratoNegocio'.
     * 
     * @param cdSituacaoContratoNegocio the value of field
     * 'cdSituacaoContratoNegocio'.
     */
    public void setCdSituacaoContratoNegocio(int cdSituacaoContratoNegocio)
    {
        this._cdSituacaoContratoNegocio = cdSituacaoContratoNegocio;
        this._has_cdSituacaoContratoNegocio = true;
    } //-- void setCdSituacaoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @param dsPessoaJuridicaContrato the value of field
     * 'dsPessoaJuridicaContrato'.
     */
    public void setDsPessoaJuridicaContrato(java.lang.String dsPessoaJuridicaContrato)
    {
        this._dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
    } //-- void setDsPessoaJuridicaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContratoNegocio'.
     * 
     * @param dsSituacaoContratoNegocio the value of field
     * 'dsSituacaoContratoNegocio'.
     */
    public void setDsSituacaoContratoNegocio(java.lang.String dsSituacaoContratoNegocio)
    {
        this._dsSituacaoContratoNegocio = dsSituacaoContratoNegocio;
    } //-- void setDsSituacaoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoNegocio'.
     * 
     * @param dsTipoContratoNegocio the value of field
     * 'dsTipoContratoNegocio'.
     */
    public void setDsTipoContratoNegocio(java.lang.String dsTipoContratoNegocio)
    {
        this._dsTipoContratoNegocio = dsTipoContratoNegocio;
    } //-- void setDsTipoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
