/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarListaOperacaoTarifaTipoServicoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarListaOperacaoTarifaTipoServicoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdFluxoNegocio
     */
    private int _cdFluxoNegocio = 0;

    /**
     * keeps track of state for field: _cdFluxoNegocio
     */
    private boolean _has_cdFluxoNegocio;

    /**
     * Field _cdGrupoInfoFluxo
     */
    private int _cdGrupoInfoFluxo = 0;

    /**
     * keeps track of state for field: _cdGrupoInfoFluxo
     */
    private boolean _has_cdGrupoInfoFluxo;

    /**
     * Field _cdProdutoOperacaoDeflt
     */
    private int _cdProdutoOperacaoDeflt = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoDeflt
     */
    private boolean _has_cdProdutoOperacaoDeflt;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _cdPessoaJuridicaProposta
     */
    private long _cdPessoaJuridicaProposta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaProposta
     */
    private boolean _has_cdPessoaJuridicaProposta;

    /**
     * Field _cdTipoContratoProposta
     */
    private int _cdTipoContratoProposta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoProposta
     */
    private boolean _has_cdTipoContratoProposta;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _nrOcorGrupoProposta
     */
    private int _nrOcorGrupoProposta = 0;

    /**
     * keeps track of state for field: _nrOcorGrupoProposta
     */
    private boolean _has_nrOcorGrupoProposta;

    /**
     * Field _cdPessoaContrato
     */
    private long _cdPessoaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaContrato
     */
    private boolean _has_cdPessoaContrato;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdNegocioRenegocio
     */
    private int _cdNegocioRenegocio = 0;

    /**
     * keeps track of state for field: _cdNegocioRenegocio
     */
    private boolean _has_cdNegocioRenegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarListaOperacaoTarifaTipoServicoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.request.ConsultarListaOperacaoTarifaTipoServicoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFluxoNegocio
     * 
     */
    public void deleteCdFluxoNegocio()
    {
        this._has_cdFluxoNegocio= false;
    } //-- void deleteCdFluxoNegocio() 

    /**
     * Method deleteCdGrupoInfoFluxo
     * 
     */
    public void deleteCdGrupoInfoFluxo()
    {
        this._has_cdGrupoInfoFluxo= false;
    } //-- void deleteCdGrupoInfoFluxo() 

    /**
     * Method deleteCdNegocioRenegocio
     * 
     */
    public void deleteCdNegocioRenegocio()
    {
        this._has_cdNegocioRenegocio= false;
    } //-- void deleteCdNegocioRenegocio() 

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdPessoaContrato
     * 
     */
    public void deleteCdPessoaContrato()
    {
        this._has_cdPessoaContrato= false;
    } //-- void deleteCdPessoaContrato() 

    /**
     * Method deleteCdPessoaJuridicaProposta
     * 
     */
    public void deleteCdPessoaJuridicaProposta()
    {
        this._has_cdPessoaJuridicaProposta= false;
    } //-- void deleteCdPessoaJuridicaProposta() 

    /**
     * Method deleteCdProdutoOperacaoDeflt
     * 
     */
    public void deleteCdProdutoOperacaoDeflt()
    {
        this._has_cdProdutoOperacaoDeflt= false;
    } //-- void deleteCdProdutoOperacaoDeflt() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoContratoProposta
     * 
     */
    public void deleteCdTipoContratoProposta()
    {
        this._has_cdTipoContratoProposta= false;
    } //-- void deleteCdTipoContratoProposta() 

    /**
     * Method deleteNrOcorGrupoProposta
     * 
     */
    public void deleteNrOcorGrupoProposta()
    {
        this._has_nrOcorGrupoProposta= false;
    } //-- void deleteNrOcorGrupoProposta() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdFluxoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdFluxoNegocio'.
     */
    public int getCdFluxoNegocio()
    {
        return this._cdFluxoNegocio;
    } //-- int getCdFluxoNegocio() 

    /**
     * Returns the value of field 'cdGrupoInfoFluxo'.
     * 
     * @return int
     * @return the value of field 'cdGrupoInfoFluxo'.
     */
    public int getCdGrupoInfoFluxo()
    {
        return this._cdGrupoInfoFluxo;
    } //-- int getCdGrupoInfoFluxo() 

    /**
     * Returns the value of field 'cdNegocioRenegocio'.
     * 
     * @return int
     * @return the value of field 'cdNegocioRenegocio'.
     */
    public int getCdNegocioRenegocio()
    {
        return this._cdNegocioRenegocio;
    } //-- int getCdNegocioRenegocio() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field 'cdPessoaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContrato'.
     */
    public long getCdPessoaContrato()
    {
        return this._cdPessoaContrato;
    } //-- long getCdPessoaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaProposta'.
     */
    public long getCdPessoaJuridicaProposta()
    {
        return this._cdPessoaJuridicaProposta;
    } //-- long getCdPessoaJuridicaProposta() 

    /**
     * Returns the value of field 'cdProdutoOperacaoDeflt'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoDeflt'.
     */
    public int getCdProdutoOperacaoDeflt()
    {
        return this._cdProdutoOperacaoDeflt;
    } //-- int getCdProdutoOperacaoDeflt() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoProposta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoProposta'.
     */
    public int getCdTipoContratoProposta()
    {
        return this._cdTipoContratoProposta;
    } //-- int getCdTipoContratoProposta() 

    /**
     * Returns the value of field 'nrOcorGrupoProposta'.
     * 
     * @return int
     * @return the value of field 'nrOcorGrupoProposta'.
     */
    public int getNrOcorGrupoProposta()
    {
        return this._nrOcorGrupoProposta;
    } //-- int getNrOcorGrupoProposta() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdFluxoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFluxoNegocio()
    {
        return this._has_cdFluxoNegocio;
    } //-- boolean hasCdFluxoNegocio() 

    /**
     * Method hasCdGrupoInfoFluxo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoInfoFluxo()
    {
        return this._has_cdGrupoInfoFluxo;
    } //-- boolean hasCdGrupoInfoFluxo() 

    /**
     * Method hasCdNegocioRenegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNegocioRenegocio()
    {
        return this._has_cdNegocioRenegocio;
    } //-- boolean hasCdNegocioRenegocio() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdPessoaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContrato()
    {
        return this._has_cdPessoaContrato;
    } //-- boolean hasCdPessoaContrato() 

    /**
     * Method hasCdPessoaJuridicaProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaProposta()
    {
        return this._has_cdPessoaJuridicaProposta;
    } //-- boolean hasCdPessoaJuridicaProposta() 

    /**
     * Method hasCdProdutoOperacaoDeflt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoDeflt()
    {
        return this._has_cdProdutoOperacaoDeflt;
    } //-- boolean hasCdProdutoOperacaoDeflt() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoContratoProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoProposta()
    {
        return this._has_cdTipoContratoProposta;
    } //-- boolean hasCdTipoContratoProposta() 

    /**
     * Method hasNrOcorGrupoProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorGrupoProposta()
    {
        return this._has_nrOcorGrupoProposta;
    } //-- boolean hasNrOcorGrupoProposta() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFluxoNegocio'.
     * 
     * @param cdFluxoNegocio the value of field 'cdFluxoNegocio'.
     */
    public void setCdFluxoNegocio(int cdFluxoNegocio)
    {
        this._cdFluxoNegocio = cdFluxoNegocio;
        this._has_cdFluxoNegocio = true;
    } //-- void setCdFluxoNegocio(int) 

    /**
     * Sets the value of field 'cdGrupoInfoFluxo'.
     * 
     * @param cdGrupoInfoFluxo the value of field 'cdGrupoInfoFluxo'
     */
    public void setCdGrupoInfoFluxo(int cdGrupoInfoFluxo)
    {
        this._cdGrupoInfoFluxo = cdGrupoInfoFluxo;
        this._has_cdGrupoInfoFluxo = true;
    } //-- void setCdGrupoInfoFluxo(int) 

    /**
     * Sets the value of field 'cdNegocioRenegocio'.
     * 
     * @param cdNegocioRenegocio the value of field
     * 'cdNegocioRenegocio'.
     */
    public void setCdNegocioRenegocio(int cdNegocioRenegocio)
    {
        this._cdNegocioRenegocio = cdNegocioRenegocio;
        this._has_cdNegocioRenegocio = true;
    } //-- void setCdNegocioRenegocio(int) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdPessoaContrato'.
     * 
     * @param cdPessoaContrato the value of field 'cdPessoaContrato'
     */
    public void setCdPessoaContrato(long cdPessoaContrato)
    {
        this._cdPessoaContrato = cdPessoaContrato;
        this._has_cdPessoaContrato = true;
    } //-- void setCdPessoaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @param cdPessoaJuridicaProposta the value of field
     * 'cdPessoaJuridicaProposta'.
     */
    public void setCdPessoaJuridicaProposta(long cdPessoaJuridicaProposta)
    {
        this._cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
        this._has_cdPessoaJuridicaProposta = true;
    } //-- void setCdPessoaJuridicaProposta(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoDeflt'.
     * 
     * @param cdProdutoOperacaoDeflt the value of field
     * 'cdProdutoOperacaoDeflt'.
     */
    public void setCdProdutoOperacaoDeflt(int cdProdutoOperacaoDeflt)
    {
        this._cdProdutoOperacaoDeflt = cdProdutoOperacaoDeflt;
        this._has_cdProdutoOperacaoDeflt = true;
    } //-- void setCdProdutoOperacaoDeflt(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoProposta'.
     * 
     * @param cdTipoContratoProposta the value of field
     * 'cdTipoContratoProposta'.
     */
    public void setCdTipoContratoProposta(int cdTipoContratoProposta)
    {
        this._cdTipoContratoProposta = cdTipoContratoProposta;
        this._has_cdTipoContratoProposta = true;
    } //-- void setCdTipoContratoProposta(int) 

    /**
     * Sets the value of field 'nrOcorGrupoProposta'.
     * 
     * @param nrOcorGrupoProposta the value of field
     * 'nrOcorGrupoProposta'.
     */
    public void setNrOcorGrupoProposta(int nrOcorGrupoProposta)
    {
        this._nrOcorGrupoProposta = nrOcorGrupoProposta;
        this._has_nrOcorGrupoProposta = true;
    } //-- void setNrOcorGrupoProposta(int) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarListaOperacaoTarifaTipoServicoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.request.ConsultarListaOperacaoTarifaTipoServicoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.request.ConsultarListaOperacaoTarifaTipoServicoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.request.ConsultarListaOperacaoTarifaTipoServicoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.request.ConsultarListaOperacaoTarifaTipoServicoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
