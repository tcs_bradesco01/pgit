/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarSolRecPagtosVencNaoPagoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarSolRecPagtosVencNaoPagoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdFinalidadeRecuperacao
     */
    private int _cdFinalidadeRecuperacao = 0;

    /**
     * keeps track of state for field: _cdFinalidadeRecuperacao
     */
    private boolean _has_cdFinalidadeRecuperacao;

    /**
     * Field _cdSelecaoRecuperacao
     */
    private int _cdSelecaoRecuperacao = 0;

    /**
     * keeps track of state for field: _cdSelecaoRecuperacao
     */
    private boolean _has_cdSelecaoRecuperacao;

    /**
     * Field _cdSolicitacaoPagamento
     */
    private int _cdSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSolicitacaoPagamento
     */
    private boolean _has_cdSolicitacaoPagamento;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdDigitoAgenciaDebito
     */
    private int _cdDigitoAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaDebito
     */
    private boolean _has_cdDigitoAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdDigitoContaDebito
     */
    private java.lang.String _cdDigitoContaDebito;

    /**
     * Field _cdTipoContaDebito
     */
    private int _cdTipoContaDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContaDebito
     */
    private boolean _has_cdTipoContaDebito;

    /**
     * Field _dtInicioPesquisa
     */
    private java.lang.String _dtInicioPesquisa;

    /**
     * Field _dtFimPesquisa
     */
    private java.lang.String _dtFimPesquisa;

    /**
     * Field _cdProduto
     */
    private int _cdProduto = 0;

    /**
     * keeps track of state for field: _cdProduto
     */
    private boolean _has_cdProduto;

    /**
     * Field _cdProdutoRelacionado
     */
    private int _cdProdutoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoRelacionado
     */
    private boolean _has_cdProdutoRelacionado;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _cdMotivoSolicitacao
     */
    private int _cdMotivoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSolicitacao
     */
    private boolean _has_cdMotivoSolicitacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarSolRecPagtosVencNaoPagoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.request.ConsultarSolRecPagtosVencNaoPagoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdDigitoAgenciaDebito
     * 
     */
    public void deleteCdDigitoAgenciaDebito()
    {
        this._has_cdDigitoAgenciaDebito= false;
    } //-- void deleteCdDigitoAgenciaDebito() 

    /**
     * Method deleteCdFinalidadeRecuperacao
     * 
     */
    public void deleteCdFinalidadeRecuperacao()
    {
        this._has_cdFinalidadeRecuperacao= false;
    } //-- void deleteCdFinalidadeRecuperacao() 

    /**
     * Method deleteCdMotivoSolicitacao
     * 
     */
    public void deleteCdMotivoSolicitacao()
    {
        this._has_cdMotivoSolicitacao= false;
    } //-- void deleteCdMotivoSolicitacao() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProduto
     * 
     */
    public void deleteCdProduto()
    {
        this._has_cdProduto= false;
    } //-- void deleteCdProduto() 

    /**
     * Method deleteCdProdutoRelacionado
     * 
     */
    public void deleteCdProdutoRelacionado()
    {
        this._has_cdProdutoRelacionado= false;
    } //-- void deleteCdProdutoRelacionado() 

    /**
     * Method deleteCdSelecaoRecuperacao
     * 
     */
    public void deleteCdSelecaoRecuperacao()
    {
        this._has_cdSelecaoRecuperacao= false;
    } //-- void deleteCdSelecaoRecuperacao() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdSolicitacaoPagamento
     * 
     */
    public void deleteCdSolicitacaoPagamento()
    {
        this._has_cdSolicitacaoPagamento= false;
    } //-- void deleteCdSolicitacaoPagamento() 

    /**
     * Method deleteCdTipoContaDebito
     * 
     */
    public void deleteCdTipoContaDebito()
    {
        this._has_cdTipoContaDebito= false;
    } //-- void deleteCdTipoContaDebito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaDebito'.
     */
    public int getCdDigitoAgenciaDebito()
    {
        return this._cdDigitoAgenciaDebito;
    } //-- int getCdDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'cdDigitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDebito'.
     */
    public java.lang.String getCdDigitoContaDebito()
    {
        return this._cdDigitoContaDebito;
    } //-- java.lang.String getCdDigitoContaDebito() 

    /**
     * Returns the value of field 'cdFinalidadeRecuperacao'.
     * 
     * @return int
     * @return the value of field 'cdFinalidadeRecuperacao'.
     */
    public int getCdFinalidadeRecuperacao()
    {
        return this._cdFinalidadeRecuperacao;
    } //-- int getCdFinalidadeRecuperacao() 

    /**
     * Returns the value of field 'cdMotivoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSolicitacao'.
     */
    public int getCdMotivoSolicitacao()
    {
        return this._cdMotivoSolicitacao;
    } //-- int getCdMotivoSolicitacao() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProduto'.
     * 
     * @return int
     * @return the value of field 'cdProduto'.
     */
    public int getCdProduto()
    {
        return this._cdProduto;
    } //-- int getCdProduto() 

    /**
     * Returns the value of field 'cdProdutoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoRelacionado'.
     */
    public int getCdProdutoRelacionado()
    {
        return this._cdProdutoRelacionado;
    } //-- int getCdProdutoRelacionado() 

    /**
     * Returns the value of field 'cdSelecaoRecuperacao'.
     * 
     * @return int
     * @return the value of field 'cdSelecaoRecuperacao'.
     */
    public int getCdSelecaoRecuperacao()
    {
        return this._cdSelecaoRecuperacao;
    } //-- int getCdSelecaoRecuperacao() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamento'.
     */
    public int getCdSolicitacaoPagamento()
    {
        return this._cdSolicitacaoPagamento;
    } //-- int getCdSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContaDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaDebito'.
     */
    public int getCdTipoContaDebito()
    {
        return this._cdTipoContaDebito;
    } //-- int getCdTipoContaDebito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtFimPesquisa'.
     * 
     * @return String
     * @return the value of field 'dtFimPesquisa'.
     */
    public java.lang.String getDtFimPesquisa()
    {
        return this._dtFimPesquisa;
    } //-- java.lang.String getDtFimPesquisa() 

    /**
     * Returns the value of field 'dtInicioPesquisa'.
     * 
     * @return String
     * @return the value of field 'dtInicioPesquisa'.
     */
    public java.lang.String getDtInicioPesquisa()
    {
        return this._dtInicioPesquisa;
    } //-- java.lang.String getDtInicioPesquisa() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdDigitoAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaDebito()
    {
        return this._has_cdDigitoAgenciaDebito;
    } //-- boolean hasCdDigitoAgenciaDebito() 

    /**
     * Method hasCdFinalidadeRecuperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidadeRecuperacao()
    {
        return this._has_cdFinalidadeRecuperacao;
    } //-- boolean hasCdFinalidadeRecuperacao() 

    /**
     * Method hasCdMotivoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSolicitacao()
    {
        return this._has_cdMotivoSolicitacao;
    } //-- boolean hasCdMotivoSolicitacao() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProduto()
    {
        return this._has_cdProduto;
    } //-- boolean hasCdProduto() 

    /**
     * Method hasCdProdutoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoRelacionado()
    {
        return this._has_cdProdutoRelacionado;
    } //-- boolean hasCdProdutoRelacionado() 

    /**
     * Method hasCdSelecaoRecuperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSelecaoRecuperacao()
    {
        return this._has_cdSelecaoRecuperacao;
    } //-- boolean hasCdSelecaoRecuperacao() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamento()
    {
        return this._has_cdSolicitacaoPagamento;
    } //-- boolean hasCdSolicitacaoPagamento() 

    /**
     * Method hasCdTipoContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaDebito()
    {
        return this._has_cdTipoContaDebito;
    } //-- boolean hasCdTipoContaDebito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @param cdDigitoAgenciaDebito the value of field
     * 'cdDigitoAgenciaDebito'.
     */
    public void setCdDigitoAgenciaDebito(int cdDigitoAgenciaDebito)
    {
        this._cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
        this._has_cdDigitoAgenciaDebito = true;
    } //-- void setCdDigitoAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdDigitoContaDebito'.
     * 
     * @param cdDigitoContaDebito the value of field
     * 'cdDigitoContaDebito'.
     */
    public void setCdDigitoContaDebito(java.lang.String cdDigitoContaDebito)
    {
        this._cdDigitoContaDebito = cdDigitoContaDebito;
    } //-- void setCdDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdFinalidadeRecuperacao'.
     * 
     * @param cdFinalidadeRecuperacao the value of field
     * 'cdFinalidadeRecuperacao'.
     */
    public void setCdFinalidadeRecuperacao(int cdFinalidadeRecuperacao)
    {
        this._cdFinalidadeRecuperacao = cdFinalidadeRecuperacao;
        this._has_cdFinalidadeRecuperacao = true;
    } //-- void setCdFinalidadeRecuperacao(int) 

    /**
     * Sets the value of field 'cdMotivoSolicitacao'.
     * 
     * @param cdMotivoSolicitacao the value of field
     * 'cdMotivoSolicitacao'.
     */
    public void setCdMotivoSolicitacao(int cdMotivoSolicitacao)
    {
        this._cdMotivoSolicitacao = cdMotivoSolicitacao;
        this._has_cdMotivoSolicitacao = true;
    } //-- void setCdMotivoSolicitacao(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProduto'.
     * 
     * @param cdProduto the value of field 'cdProduto'.
     */
    public void setCdProduto(int cdProduto)
    {
        this._cdProduto = cdProduto;
        this._has_cdProduto = true;
    } //-- void setCdProduto(int) 

    /**
     * Sets the value of field 'cdProdutoRelacionado'.
     * 
     * @param cdProdutoRelacionado the value of field
     * 'cdProdutoRelacionado'.
     */
    public void setCdProdutoRelacionado(int cdProdutoRelacionado)
    {
        this._cdProdutoRelacionado = cdProdutoRelacionado;
        this._has_cdProdutoRelacionado = true;
    } //-- void setCdProdutoRelacionado(int) 

    /**
     * Sets the value of field 'cdSelecaoRecuperacao'.
     * 
     * @param cdSelecaoRecuperacao the value of field
     * 'cdSelecaoRecuperacao'.
     */
    public void setCdSelecaoRecuperacao(int cdSelecaoRecuperacao)
    {
        this._cdSelecaoRecuperacao = cdSelecaoRecuperacao;
        this._has_cdSelecaoRecuperacao = true;
    } //-- void setCdSelecaoRecuperacao(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamento'.
     * 
     * @param cdSolicitacaoPagamento the value of field
     * 'cdSolicitacaoPagamento'.
     */
    public void setCdSolicitacaoPagamento(int cdSolicitacaoPagamento)
    {
        this._cdSolicitacaoPagamento = cdSolicitacaoPagamento;
        this._has_cdSolicitacaoPagamento = true;
    } //-- void setCdSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContaDebito'.
     * 
     * @param cdTipoContaDebito the value of field
     * 'cdTipoContaDebito'.
     */
    public void setCdTipoContaDebito(int cdTipoContaDebito)
    {
        this._cdTipoContaDebito = cdTipoContaDebito;
        this._has_cdTipoContaDebito = true;
    } //-- void setCdTipoContaDebito(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dtFimPesquisa'.
     * 
     * @param dtFimPesquisa the value of field 'dtFimPesquisa'.
     */
    public void setDtFimPesquisa(java.lang.String dtFimPesquisa)
    {
        this._dtFimPesquisa = dtFimPesquisa;
    } //-- void setDtFimPesquisa(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioPesquisa'.
     * 
     * @param dtInicioPesquisa the value of field 'dtInicioPesquisa'
     */
    public void setDtInicioPesquisa(java.lang.String dtInicioPesquisa)
    {
        this._dtInicioPesquisa = dtInicioPesquisa;
    } //-- void setDtInicioPesquisa(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarSolRecPagtosVencNaoPagoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.request.ConsultarSolRecPagtosVencNaoPagoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.request.ConsultarSolRecPagtosVencNaoPagoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.request.ConsultarSolRecPagtosVencNaoPagoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.request.ConsultarSolRecPagtosVencNaoPagoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
