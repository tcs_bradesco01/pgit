/**
 * Nome: br.com.bradesco.web.pgit.service.business.imprimir
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.imprimir;

import java.io.OutputStream;

import br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteSaidaDTO;

// TODO: Auto-generated Javadoc
/**
 * Nome: IImprimirService
 * <p>
 * Prop�sito: Interface do adaptador Imprimir
 * </p>.
 *
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IImprimirService {

	/**
	 * Imprimir comprovante cred conta.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteCredConta(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante codigo barras.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteCodigoBarras(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante doc.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteDOC(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante ted.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteTED(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante op.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteOP(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante titulo bradesco.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteTituloBradesco(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante titulo outros.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteTituloOutros(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante darf.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteDARF(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante gare.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteGARE(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir comprovante gps.
	 *
	 * @param imprimirComprovanteCredContaEntradaDTO the imprimir comprovante cred conta entrada dto
	 * @return the imprimir comprovante saida dto
	 */
	ImprimirComprovanteSaidaDTO imprimirComprovanteGPS(ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO);

	/**
	 * Imprimir arquivo.
	 *
	 * @param outputStream the output stream
	 * @param protocolo the protocolo
	 */
	void imprimirArquivo(OutputStream outputStream, Long protocolo);

    /**
     * Imprimir arquivo duas vias.
     * 
     * @param outputStream
     *            the output stream
     * @param protocoloPrimeiraVia
     *            the protocolo primeira via
     * @param protocoloSegundaVia
     *            the protocolo segunda via
     */
    void imprimirArquivoDuasVias(OutputStream outputStream, Long protocoloPrimeiraVia, Long protocoloSegundaVia);
}