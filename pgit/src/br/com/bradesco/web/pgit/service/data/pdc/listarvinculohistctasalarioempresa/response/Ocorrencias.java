/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridVinc
     */
    private long _cdPessoaJuridVinc = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridVinc
     */
    private boolean _has_cdPessoaJuridVinc;

    /**
     * Field _cdTipoContratoVinc
     */
    private int _cdTipoContratoVinc = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinc
     */
    private boolean _has_cdTipoContratoVinc;

    /**
     * Field _nrSeqContratoVinc
     */
    private long _nrSeqContratoVinc = 0;

    /**
     * keeps track of state for field: _nrSeqContratoVinc
     */
    private boolean _has_nrSeqContratoVinc;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuario
     */
    private java.lang.String _cdUsuario;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgenciaBancaria
     */
    private int _cdAgenciaBancaria = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancaria
     */
    private boolean _has_cdAgenciaBancaria;

    /**
     * Field _cdDigitoAgenciaBancaria
     */
    private int _cdDigitoAgenciaBancaria = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaBancaria
     */
    private boolean _has_cdDigitoAgenciaBancaria;

    /**
     * Field _dsAgenciaBancaria
     */
    private java.lang.String _dsAgenciaBancaria;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _tpManutencao
     */
    private java.lang.String _tpManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaBancaria
     * 
     */
    public void deleteCdAgenciaBancaria()
    {
        this._has_cdAgenciaBancaria= false;
    } //-- void deleteCdAgenciaBancaria() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdDigitoAgenciaBancaria
     * 
     */
    public void deleteCdDigitoAgenciaBancaria()
    {
        this._has_cdDigitoAgenciaBancaria= false;
    } //-- void deleteCdDigitoAgenciaBancaria() 

    /**
     * Method deleteCdPessoaJuridVinc
     * 
     */
    public void deleteCdPessoaJuridVinc()
    {
        this._has_cdPessoaJuridVinc= false;
    } //-- void deleteCdPessoaJuridVinc() 

    /**
     * Method deleteCdTipoContratoVinc
     * 
     */
    public void deleteCdTipoContratoVinc()
    {
        this._has_cdTipoContratoVinc= false;
    } //-- void deleteCdTipoContratoVinc() 

    /**
     * Method deleteNrSeqContratoVinc
     * 
     */
    public void deleteNrSeqContratoVinc()
    {
        this._has_nrSeqContratoVinc= false;
    } //-- void deleteNrSeqContratoVinc() 

    /**
     * Returns the value of field 'cdAgenciaBancaria'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancaria'.
     */
    public int getCdAgenciaBancaria()
    {
        return this._cdAgenciaBancaria;
    } //-- int getCdAgenciaBancaria() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoAgenciaBancaria'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaBancaria'.
     */
    public int getCdDigitoAgenciaBancaria()
    {
        return this._cdDigitoAgenciaBancaria;
    } //-- int getCdDigitoAgenciaBancaria() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdPessoaJuridVinc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridVinc'.
     */
    public long getCdPessoaJuridVinc()
    {
        return this._cdPessoaJuridVinc;
    } //-- long getCdPessoaJuridVinc() 

    /**
     * Returns the value of field 'cdTipoContratoVinc'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinc'.
     */
    public int getCdTipoContratoVinc()
    {
        return this._cdTipoContratoVinc;
    } //-- int getCdTipoContratoVinc() 

    /**
     * Returns the value of field 'cdUsuario'.
     * 
     * @return String
     * @return the value of field 'cdUsuario'.
     */
    public java.lang.String getCdUsuario()
    {
        return this._cdUsuario;
    } //-- java.lang.String getCdUsuario() 

    /**
     * Returns the value of field 'dsAgenciaBancaria'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaBancaria'.
     */
    public java.lang.String getDsAgenciaBancaria()
    {
        return this._dsAgenciaBancaria;
    } //-- java.lang.String getDsAgenciaBancaria() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'nrSeqContratoVinc'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoVinc'.
     */
    public long getNrSeqContratoVinc()
    {
        return this._nrSeqContratoVinc;
    } //-- long getNrSeqContratoVinc() 

    /**
     * Returns the value of field 'tpManutencao'.
     * 
     * @return String
     * @return the value of field 'tpManutencao'.
     */
    public java.lang.String getTpManutencao()
    {
        return this._tpManutencao;
    } //-- java.lang.String getTpManutencao() 

    /**
     * Method hasCdAgenciaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancaria()
    {
        return this._has_cdAgenciaBancaria;
    } //-- boolean hasCdAgenciaBancaria() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdDigitoAgenciaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaBancaria()
    {
        return this._has_cdDigitoAgenciaBancaria;
    } //-- boolean hasCdDigitoAgenciaBancaria() 

    /**
     * Method hasCdPessoaJuridVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridVinc()
    {
        return this._has_cdPessoaJuridVinc;
    } //-- boolean hasCdPessoaJuridVinc() 

    /**
     * Method hasCdTipoContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinc()
    {
        return this._has_cdTipoContratoVinc;
    } //-- boolean hasCdTipoContratoVinc() 

    /**
     * Method hasNrSeqContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoVinc()
    {
        return this._has_nrSeqContratoVinc;
    } //-- boolean hasNrSeqContratoVinc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaBancaria'.
     * 
     * @param cdAgenciaBancaria the value of field
     * 'cdAgenciaBancaria'.
     */
    public void setCdAgenciaBancaria(int cdAgenciaBancaria)
    {
        this._cdAgenciaBancaria = cdAgenciaBancaria;
        this._has_cdAgenciaBancaria = true;
    } //-- void setCdAgenciaBancaria(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaBancaria'.
     * 
     * @param cdDigitoAgenciaBancaria the value of field
     * 'cdDigitoAgenciaBancaria'.
     */
    public void setCdDigitoAgenciaBancaria(int cdDigitoAgenciaBancaria)
    {
        this._cdDigitoAgenciaBancaria = cdDigitoAgenciaBancaria;
        this._has_cdDigitoAgenciaBancaria = true;
    } //-- void setCdDigitoAgenciaBancaria(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridVinc'.
     * 
     * @param cdPessoaJuridVinc the value of field
     * 'cdPessoaJuridVinc'.
     */
    public void setCdPessoaJuridVinc(long cdPessoaJuridVinc)
    {
        this._cdPessoaJuridVinc = cdPessoaJuridVinc;
        this._has_cdPessoaJuridVinc = true;
    } //-- void setCdPessoaJuridVinc(long) 

    /**
     * Sets the value of field 'cdTipoContratoVinc'.
     * 
     * @param cdTipoContratoVinc the value of field
     * 'cdTipoContratoVinc'.
     */
    public void setCdTipoContratoVinc(int cdTipoContratoVinc)
    {
        this._cdTipoContratoVinc = cdTipoContratoVinc;
        this._has_cdTipoContratoVinc = true;
    } //-- void setCdTipoContratoVinc(int) 

    /**
     * Sets the value of field 'cdUsuario'.
     * 
     * @param cdUsuario the value of field 'cdUsuario'.
     */
    public void setCdUsuario(java.lang.String cdUsuario)
    {
        this._cdUsuario = cdUsuario;
    } //-- void setCdUsuario(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaBancaria'.
     * 
     * @param dsAgenciaBancaria the value of field
     * 'dsAgenciaBancaria'.
     */
    public void setDsAgenciaBancaria(java.lang.String dsAgenciaBancaria)
    {
        this._dsAgenciaBancaria = dsAgenciaBancaria;
    } //-- void setDsAgenciaBancaria(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqContratoVinc'.
     * 
     * @param nrSeqContratoVinc the value of field
     * 'nrSeqContratoVinc'.
     */
    public void setNrSeqContratoVinc(long nrSeqContratoVinc)
    {
        this._nrSeqContratoVinc = nrSeqContratoVinc;
        this._has_nrSeqContratoVinc = true;
    } //-- void setNrSeqContratoVinc(long) 

    /**
     * Sets the value of field 'tpManutencao'.
     * 
     * @param tpManutencao the value of field 'tpManutencao'.
     */
    public void setTpManutencao(java.lang.String tpManutencao)
    {
        this._tpManutencao = tpManutencao;
    } //-- void setTpManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
