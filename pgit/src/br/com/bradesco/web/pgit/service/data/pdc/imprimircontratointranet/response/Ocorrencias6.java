/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias6.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias6 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsProdutoSalarioFoneFacil
     */
    private java.lang.String _dsProdutoSalarioFoneFacil;

    /**
     * Field _dsServicoSalarioFoneFacil
     */
    private java.lang.String _dsServicoSalarioFoneFacil;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias6() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'dsProdutoSalarioFoneFacil'.
     * 
     * @return String
     * @return the value of field 'dsProdutoSalarioFoneFacil'.
     */
    public java.lang.String getDsProdutoSalarioFoneFacil()
    {
        return this._dsProdutoSalarioFoneFacil;
    } //-- java.lang.String getDsProdutoSalarioFoneFacil() 

    /**
     * Returns the value of field 'dsServicoSalarioFoneFacil'.
     * 
     * @return String
     * @return the value of field 'dsServicoSalarioFoneFacil'.
     */
    public java.lang.String getDsServicoSalarioFoneFacil()
    {
        return this._dsServicoSalarioFoneFacil;
    } //-- java.lang.String getDsServicoSalarioFoneFacil() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsProdutoSalarioFoneFacil'.
     * 
     * @param dsProdutoSalarioFoneFacil the value of field
     * 'dsProdutoSalarioFoneFacil'.
     */
    public void setDsProdutoSalarioFoneFacil(java.lang.String dsProdutoSalarioFoneFacil)
    {
        this._dsProdutoSalarioFoneFacil = dsProdutoSalarioFoneFacil;
    } //-- void setDsProdutoSalarioFoneFacil(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoSalarioFoneFacil'.
     * 
     * @param dsServicoSalarioFoneFacil the value of field
     * 'dsServicoSalarioFoneFacil'.
     */
    public void setDsServicoSalarioFoneFacil(java.lang.String dsServicoSalarioFoneFacil)
    {
        this._dsServicoSalarioFoneFacil = dsServicoSalarioFoneFacil;
    } //-- void setDsServicoSalarioFoneFacil(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias6
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
