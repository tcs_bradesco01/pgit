/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ConsultarManterFormAltContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManterFormAltContratoSaidaDTO {
	
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nrAditivoContratoNegocio. */
    private Long nrAditivoContratoNegocio;
    
    /** Atributo dtInclusaoAditivoContrato. */
    private String dtInclusaoAditivoContrato;
    
    /** Atributo hrInclusaoAditivoContrato. */
    private String hrInclusaoAditivoContrato;
    
    /** Atributo cdSituacaoAditivoContrato. */
    private int cdSituacaoAditivoContrato;
    
    /** Atributo dsSitucaoAditivoContrato. */
    private String dsSitucaoAditivoContrato;
    
    public String getDataHoraGeracao(){
        return FormatarData.formataData(new Date(), "dd/MM/yyyy HH:mm:ss");
    }
    
    public String getDataHoraInclusaoAditivoContrato(){
        return getDtInclusaoAditivoContrato() + " " + getHrInclusaoAditivoContrato();
    }
    
	/**
	 * Get: cdSituacaoAditivoContrato.
	 *
	 * @return cdSituacaoAditivoContrato
	 */
	public int getCdSituacaoAditivoContrato() {
		return cdSituacaoAditivoContrato;
	}
	
	/**
	 * Set: cdSituacaoAditivoContrato.
	 *
	 * @param cdSituacaoAditivoContrato the cd situacao aditivo contrato
	 */
	public void setCdSituacaoAditivoContrato(int cdSituacaoAditivoContrato) {
		this.cdSituacaoAditivoContrato = cdSituacaoAditivoContrato;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsSitucaoAditivoContrato.
	 *
	 * @return dsSitucaoAditivoContrato
	 */
	public String getDsSitucaoAditivoContrato() {
		return dsSitucaoAditivoContrato;
	}
	
	/**
	 * Set: dsSitucaoAditivoContrato.
	 *
	 * @param dsSitucaoAditivoContrato the ds situcao aditivo contrato
	 */
	public void setDsSitucaoAditivoContrato(String dsSitucaoAditivoContrato) {
		this.dsSitucaoAditivoContrato = dsSitucaoAditivoContrato;
	}
	
	/**
	 * Get: dtInclusaoAditivoContrato.
	 *
	 * @return dtInclusaoAditivoContrato
	 */
	public String getDtInclusaoAditivoContrato() {
		return dtInclusaoAditivoContrato;
	}
	
	/**
	 * Set: dtInclusaoAditivoContrato.
	 *
	 * @param dtInclusaoAditivoContrato the dt inclusao aditivo contrato
	 */
	public void setDtInclusaoAditivoContrato(String dtInclusaoAditivoContrato) {
		this.dtInclusaoAditivoContrato = dtInclusaoAditivoContrato;
	}
	
	/**
	 * Get: hrInclusaoAditivoContrato.
	 *
	 * @return hrInclusaoAditivoContrato
	 */
	public String getHrInclusaoAditivoContrato() {
		return hrInclusaoAditivoContrato;
	}
	
	/**
	 * Set: hrInclusaoAditivoContrato.
	 *
	 * @param hrInclusaoAditivoContrato the hr inclusao aditivo contrato
	 */
	public void setHrInclusaoAditivoContrato(String hrInclusaoAditivoContrato) {
		this.hrInclusaoAditivoContrato = hrInclusaoAditivoContrato;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public Long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}
	
	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}


}
