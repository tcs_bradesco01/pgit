/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExcluirMsgLayoutArqRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExcluirMsgLayoutArqRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _nrMensagemArquivoRetorno
     */
    private int _nrMensagemArquivoRetorno = 0;

    /**
     * keeps track of state for field: _nrMensagemArquivoRetorno
     */
    private boolean _has_nrMensagemArquivoRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExcluirMsgLayoutArqRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.request.ExcluirMsgLayoutArqRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrMensagemArquivoRetorno
     * 
     */
    public void deleteNrMensagemArquivoRetorno()
    {
        this._has_nrMensagemArquivoRetorno= false;
    } //-- void deleteNrMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'nrMensagemArquivoRetorno'.
     * 
     * @return int
     * @return the value of field 'nrMensagemArquivoRetorno'.
     */
    public int getNrMensagemArquivoRetorno()
    {
        return this._nrMensagemArquivoRetorno;
    } //-- int getNrMensagemArquivoRetorno() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrMensagemArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMensagemArquivoRetorno()
    {
        return this._has_nrMensagemArquivoRetorno;
    } //-- boolean hasNrMensagemArquivoRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'nrMensagemArquivoRetorno'.
     * 
     * @param nrMensagemArquivoRetorno the value of field
     * 'nrMensagemArquivoRetorno'.
     */
    public void setNrMensagemArquivoRetorno(int nrMensagemArquivoRetorno)
    {
        this._nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
        this._has_nrMensagemArquivoRetorno = true;
    } //-- void setNrMensagemArquivoRetorno(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExcluirMsgLayoutArqRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.request.ExcluirMsgLayoutArqRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.request.ExcluirMsgLayoutArqRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.request.ExcluirMsgLayoutArqRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.request.ExcluirMsgLayoutArqRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
