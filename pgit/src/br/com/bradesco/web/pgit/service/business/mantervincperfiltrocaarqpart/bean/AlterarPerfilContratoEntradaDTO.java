/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

import java.math.BigDecimal;

/**
 * Nome: AlterarPerfilContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarPerfilContratoEntradaDTO {
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo = null;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo = null;
    
    /** Atributo cdMeioPrincipalRemessa. */
    private Integer cdMeioPrincipalRemessa = null;
    
    /** Atributo cdmeioAlternativoRemessa. */
    private Integer codigoMeioAlternativoRemessa = null;
    
    /** Atributo cdMeioPrincipalRetorno. */
    private Integer cdMeioPrincipalRetorno = null;
    
    /** Atributo cdMeioAlternativoRetorno. */
    private Integer cdMeioAlternativoRetorno = null;
    
    /** Atributo cdPessoaJuridicaParceiro. */
    private Long cdPessoaJuridicaParceiro = null;
    
    /** Atributo cdEmpresaResponsavel. */
    private Integer cdEmpresaResponsavel = null;
    
    /** Atributo cdCustoTransmicao. */
    private BigDecimal cdCustoTransmicao = null;
    
    /** Atributo cdApliFormat. */
    private Long cdApliFormat = null;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica = null;
    
    /** Atributo cdUnidadeOrganizacional. */
    private Integer cdUnidadeOrganizacional = null;
    
    /** Atributo dsNomeContatoCliente. */
    private String dsNomeContatoCliente = null;
    
    /** Atributo cdAreaFone. */
    private Integer cdAreaFone = null;
    
    /** Atributo cdFoneContatoCliente. */
    private Long cdFoneContatoCliente = null;
    
    /** Atributo cdRamalContatoCliente. */
    private String cdRamalContatoCliente = null;
    
    /** Atributo dsEmailContatoCliente. */
    private String dsEmailContatoCliente = null;
    
    /** Atributo dsSolicitacaoPendente. */
    private String dsSolicitacaoPendente = null;
    
    /** Atributo cdAreaFonePend. */
    private Integer cdAreaFonePend = null;
    
    /** Atributo cdFoneSolicitacaoPend. */
    private Long cdFoneSolicitacaoPend = null;
    
    /** Atributo cdRamalSolctPend. */
    private String cdRamalSolctPend = null;
    
    /** Atributo dsEmailSolctPend. */
    private String dsEmailSolctPend = null;
    
    /** Atributo qtMesRegistroTrafg. */
    private Long qtMesRegistroTrafg = null;
    
    /** Atributo dsObsGeralPerfil. */
    private String dsObsGeralPerfil = null;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao = null;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado = null;
    
    /** Atributo cdRelacionamentoProduto. */
    private Integer cdRelacionamentoProduto = null;

    
	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo() {
		return cdPerfilTrocaArquivo;
	}
	
	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdMeioPrincipalRemessa.
	 *
	 * @return cdMeioPrincipalRemessa
	 */
	public Integer getCdMeioPrincipalRemessa() {
		return cdMeioPrincipalRemessa;
	}
	
	/**
	 * Set: cdMeioPrincipalRemessa.
	 *
	 * @param cdMeioPrincipalRemessa the cd meio principal remessa
	 */
	public void setCdMeioPrincipalRemessa(Integer cdMeioPrincipalRemessa) {
		this.cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
	}
	
	/**
	 * Get: codigoMeioAlternativoRemessa.
	 *
	 * @return codigoMeioAlternativoRemessa
	 */
	public Integer getCodigoMeioAlternativoRemessa() {
		return codigoMeioAlternativoRemessa;
	}
	
	/**
	 * Set: codigoMeioAlternativoRemessa.
	 *
	 * @param codigoMeioAlternativoRemessa the cdmeio alternativo remessa
	 */
	public void setCodigoMeioAlternativoRemessa(Integer codigoMeioAlternativoRemessa) {
		this.codigoMeioAlternativoRemessa = codigoMeioAlternativoRemessa;
	}
	
	/**
	 * Get: cdMeioPrincipalRetorno.
	 *
	 * @return cdMeioPrincipalRetorno
	 */
	public Integer getCdMeioPrincipalRetorno() {
		return cdMeioPrincipalRetorno;
	}
	
	/**
	 * Set: cdMeioPrincipalRetorno.
	 *
	 * @param cdMeioPrincipalRetorno the cd meio principal retorno
	 */
	public void setCdMeioPrincipalRetorno(Integer cdMeioPrincipalRetorno) {
		this.cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
	}
	
	/**
	 * Get: cdMeioAlternativoRetorno.
	 *
	 * @return cdMeioAlternativoRetorno
	 */
	public Integer getCdMeioAlternativoRetorno() {
		return cdMeioAlternativoRetorno;
	}
	
	/**
	 * Set: cdMeioAlternativoRetorno.
	 *
	 * @param cdMeioAlternativoRetorno the cd meio alternativo retorno
	 */
	public void setCdMeioAlternativoRetorno(Integer cdMeioAlternativoRetorno) {
		this.cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
	}
	
	/**
	 * Get: cdPessoaJuridicaParceiro.
	 *
	 * @return cdPessoaJuridicaParceiro
	 */
	public Long getCdPessoaJuridicaParceiro() {
		return cdPessoaJuridicaParceiro;
	}
	
	/**
	 * Set: cdPessoaJuridicaParceiro.
	 *
	 * @param cdPessoaJuridicaParceiro the cd pessoa juridica parceiro
	 */
	public void setCdPessoaJuridicaParceiro(Long cdPessoaJuridicaParceiro) {
		this.cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
	}
	
	/**
	 * Get: cdEmpresaResponsavel.
	 *
	 * @return cdEmpresaResponsavel
	 */
	public Integer getCdEmpresaResponsavel() {
		return cdEmpresaResponsavel;
	}
	
	/**
	 * Set: cdEmpresaResponsavel.
	 *
	 * @param cdEmpresaResponsavel the cd empresa responsavel
	 */
	public void setCdEmpresaResponsavel(Integer cdEmpresaResponsavel) {
		this.cdEmpresaResponsavel = cdEmpresaResponsavel;
	}
	
	/**
	 * Get: cdCustoTransmicao.
	 *
	 * @return cdCustoTransmicao
	 */
	public BigDecimal getCdCustoTransmicao() {
		return cdCustoTransmicao;
	}
	
	/**
	 * Set: cdCustoTransmicao.
	 *
	 * @param cdCustoTransmicao the cd custo transmicao
	 */
	public void setCdCustoTransmicao(BigDecimal cdCustoTransmicao) {
		this.cdCustoTransmicao = cdCustoTransmicao;
	}
	
	/**
	 * Get: cdApliFormat.
	 *
	 * @return cdApliFormat
	 */
	public Long getCdApliFormat() {
		return cdApliFormat;
	}
	
	/**
	 * Set: cdApliFormat.
	 *
	 * @param cdApliFormat the cd apli format
	 */
	public void setCdApliFormat(Long cdApliFormat) {
		this.cdApliFormat = cdApliFormat;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdUnidadeOrganizacional.
	 *
	 * @return cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}
	
	/**
	 * Set: cdUnidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}
	
	/**
	 * Get: dsNomeContatoCliente.
	 *
	 * @return dsNomeContatoCliente
	 */
	public String getDsNomeContatoCliente() {
		return dsNomeContatoCliente;
	}
	
	/**
	 * Set: dsNomeContatoCliente.
	 *
	 * @param dsNomeContatoCliente the ds nome contato cliente
	 */
	public void setDsNomeContatoCliente(String dsNomeContatoCliente) {
		this.dsNomeContatoCliente = dsNomeContatoCliente;
	}
	
	/**
	 * Get: cdAreaFone.
	 *
	 * @return cdAreaFone
	 */
	public Integer getCdAreaFone() {
		return cdAreaFone;
	}
	
	/**
	 * Set: cdAreaFone.
	 *
	 * @param cdAreaFone the cd area fone
	 */
	public void setCdAreaFone(Integer cdAreaFone) {
		this.cdAreaFone = cdAreaFone;
	}
	
	/**
	 * Get: cdFoneContatoCliente.
	 *
	 * @return cdFoneContatoCliente
	 */
	public Long getCdFoneContatoCliente() {
		return cdFoneContatoCliente;
	}
	
	/**
	 * Set: cdFoneContatoCliente.
	 *
	 * @param cdFoneContatoCliente the cd fone contato cliente
	 */
	public void setCdFoneContatoCliente(Long cdFoneContatoCliente) {
		this.cdFoneContatoCliente = cdFoneContatoCliente;
	}
	
	/**
	 * Get: cdRamalContatoCliente.
	 *
	 * @return cdRamalContatoCliente
	 */
	public String getCdRamalContatoCliente() {
		return cdRamalContatoCliente;
	}
	
	/**
	 * Set: cdRamalContatoCliente.
	 *
	 * @param cdRamalContatoCliente the cd ramal contato cliente
	 */
	public void setCdRamalContatoCliente(String cdRamalContatoCliente) {
		this.cdRamalContatoCliente = cdRamalContatoCliente;
	}
	
	/**
	 * Get: dsEmailContatoCliente.
	 *
	 * @return dsEmailContatoCliente
	 */
	public String getDsEmailContatoCliente() {
		return dsEmailContatoCliente;
	}
	
	/**
	 * Set: dsEmailContatoCliente.
	 *
	 * @param dsEmailContatoCliente the ds email contato cliente
	 */
	public void setDsEmailContatoCliente(String dsEmailContatoCliente) {
		this.dsEmailContatoCliente = dsEmailContatoCliente;
	}
	
	/**
	 * Get: dsSolicitacaoPendente.
	 *
	 * @return dsSolicitacaoPendente
	 */
	public String getDsSolicitacaoPendente() {
		return dsSolicitacaoPendente;
	}
	
	/**
	 * Set: dsSolicitacaoPendente.
	 *
	 * @param dsSolicitacaoPendente the ds solicitacao pendente
	 */
	public void setDsSolicitacaoPendente(String dsSolicitacaoPendente) {
		this.dsSolicitacaoPendente = dsSolicitacaoPendente;
	}
	
	/**
	 * Get: cdAreaFonePend.
	 *
	 * @return cdAreaFonePend
	 */
	public Integer getCdAreaFonePend() {
		return cdAreaFonePend;
	}
	
	/**
	 * Set: cdAreaFonePend.
	 *
	 * @param cdAreaFonePend the cd area fone pend
	 */
	public void setCdAreaFonePend(Integer cdAreaFonePend) {
		this.cdAreaFonePend = cdAreaFonePend;
	}
	
	/**
	 * Get: cdFoneSolicitacaoPend.
	 *
	 * @return cdFoneSolicitacaoPend
	 */
	public Long getCdFoneSolicitacaoPend() {
		return cdFoneSolicitacaoPend;
	}
	
	/**
	 * Set: cdFoneSolicitacaoPend.
	 *
	 * @param cdFoneSolicitacaoPend the cd fone solicitacao pend
	 */
	public void setCdFoneSolicitacaoPend(Long cdFoneSolicitacaoPend) {
		this.cdFoneSolicitacaoPend = cdFoneSolicitacaoPend;
	}
	
	/**
	 * Get: cdRamalSolctPend.
	 *
	 * @return cdRamalSolctPend
	 */
	public String getCdRamalSolctPend() {
		return cdRamalSolctPend;
	}
	
	/**
	 * Set: cdRamalSolctPend.
	 *
	 * @param cdRamalSolctPend the cd ramal solct pend
	 */
	public void setCdRamalSolctPend(String cdRamalSolctPend) {
		this.cdRamalSolctPend = cdRamalSolctPend;
	}
	
	/**
	 * Get: dsEmailSolctPend.
	 *
	 * @return dsEmailSolctPend
	 */
	public String getDsEmailSolctPend() {
		return dsEmailSolctPend;
	}
	
	/**
	 * Set: dsEmailSolctPend.
	 *
	 * @param dsEmailSolctPend the ds email solct pend
	 */
	public void setDsEmailSolctPend(String dsEmailSolctPend) {
		this.dsEmailSolctPend = dsEmailSolctPend;
	}
	
	/**
	 * Get: qtMesRegistroTrafg.
	 *
	 * @return qtMesRegistroTrafg
	 */
	public Long getQtMesRegistroTrafg() {
		return qtMesRegistroTrafg;
	}
	
	/**
	 * Set: qtMesRegistroTrafg.
	 *
	 * @param qtMesRegistroTrafg the qt mes registro trafg
	 */
	public void setQtMesRegistroTrafg(Long qtMesRegistroTrafg) {
		this.qtMesRegistroTrafg = qtMesRegistroTrafg;
	}
	
	/**
	 * Get: dsObsGeralPerfil.
	 *
	 * @return dsObsGeralPerfil
	 */
	public String getDsObsGeralPerfil() {
		return dsObsGeralPerfil;
	}
	
	/**
	 * Set: dsObsGeralPerfil.
	 *
	 * @param dsObsGeralPerfil the ds obs geral perfil
	 */
	public void setDsObsGeralPerfil(String dsObsGeralPerfil) {
		this.dsObsGeralPerfil = dsObsGeralPerfil;
	}

	/**
	 * @return the cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * @param cdProdutoServicoOperacao the cdProdutoServicoOperacao to set
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * @return the cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * @param cdProdutoOperacaoRelacionado the cdProdutoOperacaoRelacionado to set
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * @return the dRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	/**
	 * @param dRelacionamentoProduto the dRelacionamentoProduto to set
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
    
}
