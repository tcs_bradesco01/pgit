/*
 * Nome: br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean;

/**
 * Nome: ConsultarAditivoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarAditivoContratoSaidaDTO {
	
	
	/** Atributo cdMensagem. */
	private String cdMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo nrAditivoContratoNegocio. */
	private long nrAditivoContratoNegocio;
	
	/** Atributo dtInclusaoAditivoContrato. */
	private String dtInclusaoAditivoContrato;
	
	/** Atributo hrInclusaoAditivoContrato. */
	private String hrInclusaoAditivoContrato;
	
	/** Atributo cdSituacaoAditivoContrato. */
	private int cdSituacaoAditivoContrato;
	
	/** Atributo dsSituacaoAditivoContrato. */
	private String dsSituacaoAditivoContrato;
	
	/** Atributo dataHoraInclusaoFormatada. */
	private String dataHoraInclusaoFormatada;
	
	/**
	 * Get: dataHoraInclusaoFormatada.
	 *
	 * @return dataHoraInclusaoFormatada
	 */
	public String getDataHoraInclusaoFormatada() {
		return dataHoraInclusaoFormatada;
	}
	
	/**
	 * Set: dataHoraInclusaoFormatada.
	 *
	 * @param dataHoraInclusaoFormatada the data hora inclusao formatada
	 */
	public void setDataHoraInclusaoFormatada(String dataHoraInclusaoFormatada) {
		this.dataHoraInclusaoFormatada = dataHoraInclusaoFormatada;
	}
	
	/**
	 * Get: cdMensagem.
	 *
	 * @return cdMensagem
	 */
	public String getCdMensagem() {
		return cdMensagem;
	}
	
	/**
	 * Set: cdMensagem.
	 *
	 * @param cdMensagem the cd mensagem
	 */
	public void setCdMensagem(String cdMensagem) {
		this.cdMensagem = cdMensagem;
	}
	
	/**
	 * Get: cdSituacaoAditivoContrato.
	 *
	 * @return cdSituacaoAditivoContrato
	 */
	public int getCdSituacaoAditivoContrato() {
		return cdSituacaoAditivoContrato;
	}
	
	/**
	 * Set: cdSituacaoAditivoContrato.
	 *
	 * @param cdSituacaoAditivoContrato the cd situacao aditivo contrato
	 */
	public void setCdSituacaoAditivoContrato(int cdSituacaoAditivoContrato) {
		this.cdSituacaoAditivoContrato = cdSituacaoAditivoContrato;
	}
	
	/**
	 * Get: dsSituacaoAditivoContrato.
	 *
	 * @return dsSituacaoAditivoContrato
	 */
	public String getDsSituacaoAditivoContrato() {
		return dsSituacaoAditivoContrato;
	}
	
	/**
	 * Set: dsSituacaoAditivoContrato.
	 *
	 * @param dsSituacaoAditivoContrato the ds situacao aditivo contrato
	 */
	public void setDsSituacaoAditivoContrato(String dsSituacaoAditivoContrato) {
		this.dsSituacaoAditivoContrato = dsSituacaoAditivoContrato;
	}
	
	/**
	 * Get: dtInclusaoAditivoContrato.
	 *
	 * @return dtInclusaoAditivoContrato
	 */
	public String getDtInclusaoAditivoContrato() {
		return dtInclusaoAditivoContrato;
	}
	
	/**
	 * Set: dtInclusaoAditivoContrato.
	 *
	 * @param dtInclusaoAditivoContrato the dt inclusao aditivo contrato
	 */
	public void setDtInclusaoAditivoContrato(String dtInclusaoAditivoContrato) {
		this.dtInclusaoAditivoContrato = dtInclusaoAditivoContrato;
	}
	
	/**
	 * Get: hrInclusaoAditivoContrato.
	 *
	 * @return hrInclusaoAditivoContrato
	 */
	public String getHrInclusaoAditivoContrato() {
		return hrInclusaoAditivoContrato;
	}
	
	/**
	 * Set: hrInclusaoAditivoContrato.
	 *
	 * @param hrInclusaoAditivoContrato the hr inclusao aditivo contrato
	 */
	public void setHrInclusaoAditivoContrato(String hrInclusaoAditivoContrato) {
		this.hrInclusaoAditivoContrato = hrInclusaoAditivoContrato;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}
	
	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}
	
	
	
}
