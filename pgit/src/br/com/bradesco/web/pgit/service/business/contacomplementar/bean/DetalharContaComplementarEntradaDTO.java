package br.com.bradesco.web.pgit.service.business.contacomplementar.bean;

public class DetalharContaComplementarEntradaDTO {
	
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Integer cdContaComplementar;
	private Long cdCorpoCnpjCpf;
	private Integer cdFilialCnpjCpf;
	private Integer cdControleCnpjCpf;
	
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	public Integer getCdContaComplementar() {
		return cdContaComplementar;
	}
	public void setCdContaComplementar(Integer cdContaComplementar) {
		this.cdContaComplementar = cdContaComplementar;
	}
	public Long getCdCorpoCnpjCpf() {
		return cdCorpoCnpjCpf;
	}
	public void setCdCorpoCnpjCpf(Long cdCorpoCnpjCpf) {
		this.cdCorpoCnpjCpf = cdCorpoCnpjCpf;
	}
	public Integer getCdFilialCnpjCpf() {
		return cdFilialCnpjCpf;
	}
	public void setCdFilialCnpjCpf(Integer cdFilialCnpjCpf) {
		this.cdFilialCnpjCpf = cdFilialCnpjCpf;
	}
	public Integer getCdControleCnpjCpf() {
		return cdControleCnpjCpf;
	}
	public void setCdControleCnpjCpf(Integer cdControleCnpjCpf) {
		this.cdControleCnpjCpf = cdControleCnpjCpf;
	}
}