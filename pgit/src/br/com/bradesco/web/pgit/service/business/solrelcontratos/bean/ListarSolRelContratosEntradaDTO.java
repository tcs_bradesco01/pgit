/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelcontratos.bean;

/**
 * Nome: ListarSolRelContratosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSolRelContratosEntradaDTO {

	/** Atributo dataInicial. */
	private String dataInicial;
	
	/** Atributo dataFinal. */
	private String dataFinal;
	
	/** Atributo tipoRelatorio. */
	private int tipoRelatorio;
	
	/**
	 * Get: dataFinal.
	 *
	 * @return dataFinal
	 */
	public String getDataFinal() {
		return dataFinal;
	}
	
	/**
	 * Set: dataFinal.
	 *
	 * @param dataFinal the data final
	 */
	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	/**
	 * Get: dataInicial.
	 *
	 * @return dataInicial
	 */
	public String getDataInicial() {
		return dataInicial;
	}
	
	/**
	 * Set: dataInicial.
	 *
	 * @param dataInicial the data inicial
	 */
	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}
	
	/**
	 * Get: tipoRelatorio.
	 *
	 * @return tipoRelatorio
	 */
	public int getTipoRelatorio() {
		return tipoRelatorio;
	}
	
	/**
	 * Set: tipoRelatorio.
	 *
	 * @param tipoRelatorio the tipo relatorio
	 */
	public void setTipoRelatorio(int tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}
	
	
}
