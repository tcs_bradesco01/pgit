/*
 * Nome: br.com.bradesco.web.pgit.service.business.moedassistema.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.moedassistema.bean;

/**
 * Nome: MoedaSistemaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MoedaSistemaSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdIndicadorEconomicoMoeda. */
	private int cdIndicadorEconomicoMoeda;
	
	/** Atributo cdTipoLayoutArquivo. */
	private int cdTipoLayoutArquivo;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dsIndicadorEconomicoMoeda. */
	private String dsIndicadorEconomicoMoeda;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdIndicadorEconomicoLayout. */
	private String cdIndicadorEconomicoLayout;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo nrOperacaoFluxoInclusao. */
	private String nrOperacaoFluxoInclusao;
	
	/** Atributo nrOperacaoFluxoManutencao. */
	private String nrOperacaoFluxoManutencao;
	
	/** Atributo dsIndicadorEconomicoLayout. */
	private String dsIndicadorEconomicoLayout;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public int getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}
	
	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(int cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public int getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsIndicadorEconomicoMoeda.
	 *
	 * @return dsIndicadorEconomicoMoeda
	 */
	public String getDsIndicadorEconomicoMoeda() {
		return dsIndicadorEconomicoMoeda;
	}
	
	/**
	 * Set: dsIndicadorEconomicoMoeda.
	 *
	 * @param dsIndicadorEconomicoMoeda the ds indicador economico moeda
	 */
	public void setDsIndicadorEconomicoMoeda(String dsIndicadorEconomicoMoeda) {
		this.dsIndicadorEconomicoMoeda = dsIndicadorEconomicoMoeda;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdIndicadorEconomicoLayout.
	 *
	 * @return cdIndicadorEconomicoLayout
	 */
	public String getCdIndicadorEconomicoLayout() {
		return cdIndicadorEconomicoLayout;
	}
	
	/**
	 * Set: cdIndicadorEconomicoLayout.
	 *
	 * @param cdIndicadorEconomicoLayout the cd indicador economico layout
	 */
	public void setCdIndicadorEconomicoLayout(String cdIndicadorEconomicoLayout) {
		this.cdIndicadorEconomicoLayout = cdIndicadorEconomicoLayout;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: nrOperacaoFluxoInclusao.
	 *
	 * @return nrOperacaoFluxoInclusao
	 */
	public String getNrOperacaoFluxoInclusao() {
		return nrOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nrOperacaoFluxoInclusao.
	 *
	 * @param nrOperacaoFluxoInclusao the nr operacao fluxo inclusao
	 */
	public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
		this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: nrOperacaoFluxoManutencao.
	 *
	 * @return nrOperacaoFluxoManutencao
	 */
	public String getNrOperacaoFluxoManutencao() {
		return nrOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: nrOperacaoFluxoManutencao.
	 *
	 * @param nrOperacaoFluxoManutencao the nr operacao fluxo manutencao
	 */
	public void setNrOperacaoFluxoManutencao(String nrOperacaoFluxoManutencao) {
		this.nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: dsIndicadorEconomicoLayout.
	 *
	 * @return dsIndicadorEconomicoLayout
	 */
	public String getDsIndicadorEconomicoLayout() {
		return dsIndicadorEconomicoLayout;
	}
	
	/**
	 * Set: dsIndicadorEconomicoLayout.
	 *
	 * @param dsIndicadorEconomicoLayout the ds indicador economico layout
	 */
	public void setDsIndicadorEconomicoLayout(String dsIndicadorEconomicoLayout) {
		this.dsIndicadorEconomicoLayout = dsIndicadorEconomicoLayout;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	
	
}
