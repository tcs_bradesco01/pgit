/*
 * Nome: br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarSaldoAtualSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSaldoAtualSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;
    
    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;
    
    /** Atributo dsTipoContaDebito. */
    private String dsTipoContaDebito;
    
    /** Atributo dsTipoSaldo. */
    private String dsTipoSaldo;
    
    /** Atributo qtSemSaldo. */
    private Long qtSemSaldo;
    
    /** Atributo vlSemSaldo. */
    private BigDecimal vlSemSaldo;
    
    /** Atributo qtPendenteSemSaldo. */
    private Long qtPendenteSemSaldo;
    
    /** Atributo vlPendenteSemSaldo. */
    private BigDecimal vlPendenteSemSaldo;
    
    /** Atributo qtAgendadoSemSaldo. */
    private Long qtAgendadoSemSaldo;
    
    /** Atributo vlAgendadoSemSaldo. */
    private BigDecimal vlAgendadoSemSaldo;
    
    /** Atributo qtEfetivadoSemSaldo. */
    private Long qtEfetivadoSemSaldo;
    
    /** Atributo vlEfetivadoSemSaldo. */
    private BigDecimal vlEfetivadoSemSaldo;
    
    /** Atributo vlSaldoLivre. */
    private BigDecimal vlSaldoLivre;
    
    /** Atributo vlReservaDisponivel. */
    private BigDecimal vlReservaDisponivel;
    
    /** Atributo vlReservaNaoDisponivel. */
    private BigDecimal vlReservaNaoDisponivel;
    
    /** Atributo vlTotal. */
    private BigDecimal vlTotal;
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}
	
	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}
	
	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}
	
	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}
	
	/**
	 * Get: dsTipoContaDebito.
	 *
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}
	
	/**
	 * Set: dsTipoContaDebito.
	 *
	 * @param dsTipoContaDebito the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}
	
	/**
	 * Get: dsTipoSaldo.
	 *
	 * @return dsTipoSaldo
	 */
	public String getDsTipoSaldo() {
		return dsTipoSaldo;
	}
	
	/**
	 * Set: dsTipoSaldo.
	 *
	 * @param dsTipoSaldo the ds tipo saldo
	 */
	public void setDsTipoSaldo(String dsTipoSaldo) {
		this.dsTipoSaldo = dsTipoSaldo;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: qtAgendadoSemSaldo.
	 *
	 * @return qtAgendadoSemSaldo
	 */
	public Long getQtAgendadoSemSaldo() {
		return qtAgendadoSemSaldo;
	}
	
	/**
	 * Set: qtAgendadoSemSaldo.
	 *
	 * @param qtAgendadoSemSaldo the qt agendado sem saldo
	 */
	public void setQtAgendadoSemSaldo(Long qtAgendadoSemSaldo) {
		this.qtAgendadoSemSaldo = qtAgendadoSemSaldo;
	}
	
	/**
	 * Get: qtEfetivadoSemSaldo.
	 *
	 * @return qtEfetivadoSemSaldo
	 */
	public Long getQtEfetivadoSemSaldo() {
		return qtEfetivadoSemSaldo;
	}
	
	/**
	 * Set: qtEfetivadoSemSaldo.
	 *
	 * @param qtEfetivadoSemSaldo the qt efetivado sem saldo
	 */
	public void setQtEfetivadoSemSaldo(Long qtEfetivadoSemSaldo) {
		this.qtEfetivadoSemSaldo = qtEfetivadoSemSaldo;
	}
	
	/**
	 * Get: qtPendenteSemSaldo.
	 *
	 * @return qtPendenteSemSaldo
	 */
	public Long getQtPendenteSemSaldo() {
		return qtPendenteSemSaldo;
	}
	
	/**
	 * Set: qtPendenteSemSaldo.
	 *
	 * @param qtPendenteSemSaldo the qt pendente sem saldo
	 */
	public void setQtPendenteSemSaldo(Long qtPendenteSemSaldo) {
		this.qtPendenteSemSaldo = qtPendenteSemSaldo;
	}
	
	/**
	 * Get: qtSemSaldo.
	 *
	 * @return qtSemSaldo
	 */
	public Long getQtSemSaldo() {
		return qtSemSaldo;
	}
	
	/**
	 * Set: qtSemSaldo.
	 *
	 * @param qtSemSaldo the qt sem saldo
	 */
	public void setQtSemSaldo(Long qtSemSaldo) {
		this.qtSemSaldo = qtSemSaldo;
	}
	
	/**
	 * Get: vlAgendadoSemSaldo.
	 *
	 * @return vlAgendadoSemSaldo
	 */
	public BigDecimal getVlAgendadoSemSaldo() {
		return vlAgendadoSemSaldo;
	}
	
	/**
	 * Set: vlAgendadoSemSaldo.
	 *
	 * @param vlAgendadoSemSaldo the vl agendado sem saldo
	 */
	public void setVlAgendadoSemSaldo(BigDecimal vlAgendadoSemSaldo) {
		this.vlAgendadoSemSaldo = vlAgendadoSemSaldo;
	}
	
	/**
	 * Get: vlEfetivadoSemSaldo.
	 *
	 * @return vlEfetivadoSemSaldo
	 */
	public BigDecimal getVlEfetivadoSemSaldo() {
		return vlEfetivadoSemSaldo;
	}
	
	/**
	 * Set: vlEfetivadoSemSaldo.
	 *
	 * @param vlEfetivadoSemSaldo the vl efetivado sem saldo
	 */
	public void setVlEfetivadoSemSaldo(BigDecimal vlEfetivadoSemSaldo) {
		this.vlEfetivadoSemSaldo = vlEfetivadoSemSaldo;
	}
	
	/**
	 * Get: vlPendenteSemSaldo.
	 *
	 * @return vlPendenteSemSaldo
	 */
	public BigDecimal getVlPendenteSemSaldo() {
		return vlPendenteSemSaldo;
	}
	
	/**
	 * Set: vlPendenteSemSaldo.
	 *
	 * @param vlPendenteSemSaldo the vl pendente sem saldo
	 */
	public void setVlPendenteSemSaldo(BigDecimal vlPendenteSemSaldo) {
		this.vlPendenteSemSaldo = vlPendenteSemSaldo;
	}
	
	/**
	 * Get: vlReservaDisponivel.
	 *
	 * @return vlReservaDisponivel
	 */
	public BigDecimal getVlReservaDisponivel() {
		return vlReservaDisponivel;
	}
	
	/**
	 * Set: vlReservaDisponivel.
	 *
	 * @param vlReservaDisponivel the vl reserva disponivel
	 */
	public void setVlReservaDisponivel(BigDecimal vlReservaDisponivel) {
		this.vlReservaDisponivel = vlReservaDisponivel;
	}
	
	/**
	 * Get: vlReservaNaoDisponivel.
	 *
	 * @return vlReservaNaoDisponivel
	 */
	public BigDecimal getVlReservaNaoDisponivel() {
		return vlReservaNaoDisponivel;
	}
	
	/**
	 * Set: vlReservaNaoDisponivel.
	 *
	 * @param vlReservaNaoDisponivel the vl reserva nao disponivel
	 */
	public void setVlReservaNaoDisponivel(BigDecimal vlReservaNaoDisponivel) {
		this.vlReservaNaoDisponivel = vlReservaNaoDisponivel;
	}
	
	/**
	 * Get: vlSaldoLivre.
	 *
	 * @return vlSaldoLivre
	 */
	public BigDecimal getVlSaldoLivre() {
		return vlSaldoLivre;
	}
	
	/**
	 * Set: vlSaldoLivre.
	 *
	 * @param vlSaldoLivre the vl saldo livre
	 */
	public void setVlSaldoLivre(BigDecimal vlSaldoLivre) {
		this.vlSaldoLivre = vlSaldoLivre;
	}
	
	/**
	 * Get: vlSemSaldo.
	 *
	 * @return vlSemSaldo
	 */
	public BigDecimal getVlSemSaldo() {
		return vlSemSaldo;
	}
	
	/**
	 * Set: vlSemSaldo.
	 *
	 * @param vlSemSaldo the vl sem saldo
	 */
	public void setVlSemSaldo(BigDecimal vlSemSaldo) {
		this.vlSemSaldo = vlSemSaldo;
	}
	
	/**
	 * Get: vlTotal.
	 *
	 * @return vlTotal
	 */
	public BigDecimal getVlTotal() {
		return vlTotal;
	}
	
	/**
	 * Set: vlTotal.
	 *
	 * @param vlTotal the vl total
	 */
	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}
    
    
}
