package br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.bean;

public class ExcluirSolicLiberacaoLotePgtoEntradaDTO {

	private Integer cdSolicitacaoPagamentoIntegrado;
	private Integer nrSolicitacaoPagamentoIntegrado;

	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}

	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}

	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}

	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}

}
