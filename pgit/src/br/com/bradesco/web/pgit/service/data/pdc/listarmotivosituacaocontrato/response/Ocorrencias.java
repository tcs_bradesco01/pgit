/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMotivoSituacaoOper
     */
    private int _cdMotivoSituacaoOper = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoOper
     */
    private boolean _has_cdMotivoSituacaoOper;

    /**
     * Field _dsMotivoSituacaoOper
     */
    private java.lang.String _dsMotivoSituacaoOper;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoSituacaoOper
     * 
     */
    public void deleteCdMotivoSituacaoOper()
    {
        this._has_cdMotivoSituacaoOper= false;
    } //-- void deleteCdMotivoSituacaoOper() 

    /**
     * Returns the value of field 'cdMotivoSituacaoOper'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoOper'.
     */
    public int getCdMotivoSituacaoOper()
    {
        return this._cdMotivoSituacaoOper;
    } //-- int getCdMotivoSituacaoOper() 

    /**
     * Returns the value of field 'dsMotivoSituacaoOper'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoOper'.
     */
    public java.lang.String getDsMotivoSituacaoOper()
    {
        return this._dsMotivoSituacaoOper;
    } //-- java.lang.String getDsMotivoSituacaoOper() 

    /**
     * Method hasCdMotivoSituacaoOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoOper()
    {
        return this._has_cdMotivoSituacaoOper;
    } //-- boolean hasCdMotivoSituacaoOper() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoSituacaoOper'.
     * 
     * @param cdMotivoSituacaoOper the value of field
     * 'cdMotivoSituacaoOper'.
     */
    public void setCdMotivoSituacaoOper(int cdMotivoSituacaoOper)
    {
        this._cdMotivoSituacaoOper = cdMotivoSituacaoOper;
        this._has_cdMotivoSituacaoOper = true;
    } //-- void setCdMotivoSituacaoOper(int) 

    /**
     * Sets the value of field 'dsMotivoSituacaoOper'.
     * 
     * @param dsMotivoSituacaoOper the value of field
     * 'dsMotivoSituacaoOper'.
     */
    public void setDsMotivoSituacaoOper(java.lang.String dsMotivoSituacaoOper)
    {
        this._dsMotivoSituacaoOper = dsMotivoSituacaoOper;
    } //-- void setDsMotivoSituacaoOper(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
