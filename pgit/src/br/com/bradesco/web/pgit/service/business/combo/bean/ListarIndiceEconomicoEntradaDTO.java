/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarIndiceEconomicoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarIndiceEconomicoEntradaDTO {

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao = null;

    /**
     * Listar indice economico entrada dto.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public ListarIndiceEconomicoEntradaDTO(Integer cdProdutoServicoOperacao) {
	super();
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }
}
