/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.moedasoperacao;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.DetalharMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.DetalharMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ExcluirMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ExcluirMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.IncluirMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.IncluirMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ListarMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ListarMoedasOperacaoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterMoedasOperacao
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IMoedasOperacaoService {

    
	/**
	 * Listar moedas operacao.
	 *
	 * @param listarMoedasOperacaoEntradaDTO the listar moedas operacao entrada dto
	 * @return the list< listar moedas operacao saida dt o>
	 */
	List<ListarMoedasOperacaoSaidaDTO> listarMoedasOperacao(ListarMoedasOperacaoEntradaDTO listarMoedasOperacaoEntradaDTO);
	
	/**
	 * Incluir moedas operacao.
	 *
	 * @param incluirMoedasOperacaoEntradaDTO the incluir moedas operacao entrada dto
	 * @return the incluir moedas operacao saida dto
	 */
	IncluirMoedasOperacaoSaidaDTO incluirMoedasOperacao(IncluirMoedasOperacaoEntradaDTO incluirMoedasOperacaoEntradaDTO);
	
	/**
	 * Excluir moedas operacao.
	 *
	 * @param excluirMoedasOperacaoEntradaDTO the excluir moedas operacao entrada dto
	 * @return the excluir moedas operacao saida dto
	 */
	ExcluirMoedasOperacaoSaidaDTO excluirMoedasOperacao(ExcluirMoedasOperacaoEntradaDTO excluirMoedasOperacaoEntradaDTO);
	
	/**
	 * Detalhar moedas operacao.
	 *
	 * @param detalharMoedasOperacaoEntradaDTO the detalhar moedas operacao entrada dto
	 * @return the detalhar moedas operacao saida dto
	 */
	DetalharMoedasOperacaoSaidaDTO detalharMoedasOperacao(DetalharMoedasOperacaoEntradaDTO detalharMoedasOperacaoEntradaDTO);
	
	
}

