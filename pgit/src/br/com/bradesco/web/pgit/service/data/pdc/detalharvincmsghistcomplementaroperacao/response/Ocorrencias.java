/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdMensagemLinhaExtrato
     */
    private int _cdMensagemLinhaExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhaExtrato
     */
    private boolean _has_cdMensagemLinhaExtrato;

    /**
     * Field _cdSistemaLancamentoDebito
     */
    private java.lang.String _cdSistemaLancamentoDebito;

    /**
     * Field _dsSistemaLancamentoDebito
     */
    private java.lang.String _dsSistemaLancamentoDebito;

    /**
     * Field _nrEventoLancamentoDebito
     */
    private int _nrEventoLancamentoDebito = 0;

    /**
     * keeps track of state for field: _nrEventoLancamentoDebito
     */
    private boolean _has_nrEventoLancamentoDebito;

    /**
     * Field _dsEventoLancamentoDebito
     */
    private java.lang.String _dsEventoLancamentoDebito;

    /**
     * Field _cdRecursoLancamentoDebito
     */
    private int _cdRecursoLancamentoDebito = 0;

    /**
     * keeps track of state for field: _cdRecursoLancamentoDebito
     */
    private boolean _has_cdRecursoLancamentoDebito;

    /**
     * Field _dsRecursoLancamentoDebito
     */
    private java.lang.String _dsRecursoLancamentoDebito;

    /**
     * Field _cdIdiomaLancamentoDebito
     */
    private int _cdIdiomaLancamentoDebito = 0;

    /**
     * keeps track of state for field: _cdIdiomaLancamentoDebito
     */
    private boolean _has_cdIdiomaLancamentoDebito;

    /**
     * Field _dsIdiomaLancamentoDebito
     */
    private java.lang.String _dsIdiomaLancamentoDebito;

    /**
     * Field _cdSistemaLancamentoCredito
     */
    private java.lang.String _cdSistemaLancamentoCredito;

    /**
     * Field _dsSistemaLancamentoCredito
     */
    private java.lang.String _dsSistemaLancamentoCredito;

    /**
     * Field _nrEventoLancamentoCredito
     */
    private int _nrEventoLancamentoCredito = 0;

    /**
     * keeps track of state for field: _nrEventoLancamentoCredito
     */
    private boolean _has_nrEventoLancamentoCredito;

    /**
     * Field _dsEventoLancamentoCredito
     */
    private java.lang.String _dsEventoLancamentoCredito;

    /**
     * Field _cdRecursoLancamentoCredito
     */
    private int _cdRecursoLancamentoCredito = 0;

    /**
     * keeps track of state for field: _cdRecursoLancamentoCredito
     */
    private boolean _has_cdRecursoLancamentoCredito;

    /**
     * Field _dsRecursoLancamentoCredito
     */
    private java.lang.String _dsRecursoLancamentoCredito;

    /**
     * Field _cdIdiomaLancamentoCredito
     */
    private int _cdIdiomaLancamentoCredito = 0;

    /**
     * keeps track of state for field: _cdIdiomaLancamentoCredito
     */
    private boolean _has_cdIdiomaLancamentoCredito;

    /**
     * Field _dsIdiomaLancamentoCredito
     */
    private java.lang.String _dsIdiomaLancamentoCredito;

    /**
     * Field _cdIndicadorRestricaoContrato
     */
    private int _cdIndicadorRestricaoContrato = 0;

    /**
     * keeps track of state for field: _cdIndicadorRestricaoContrato
     */
    private boolean _has_cdIndicadorRestricaoContrato;

    /**
     * Field _cdTipoMensagemExtrato
     */
    private int _cdTipoMensagemExtrato = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemExtrato
     */
    private boolean _has_cdTipoMensagemExtrato;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionadoProduto
     */
    private int _cdRelacionadoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionadoProduto
     */
    private boolean _has_cdRelacionadoProduto;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _dsMensagemLinhaCredito
     */
    private java.lang.String _dsMensagemLinhaCredito;

    /**
     * Field _dsMensagemLinhaDebito
     */
    private java.lang.String _dsMensagemLinhaDebito;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdIdiomaLancamentoCredito
     * 
     */
    public void deleteCdIdiomaLancamentoCredito()
    {
        this._has_cdIdiomaLancamentoCredito= false;
    } //-- void deleteCdIdiomaLancamentoCredito() 

    /**
     * Method deleteCdIdiomaLancamentoDebito
     * 
     */
    public void deleteCdIdiomaLancamentoDebito()
    {
        this._has_cdIdiomaLancamentoDebito= false;
    } //-- void deleteCdIdiomaLancamentoDebito() 

    /**
     * Method deleteCdIndicadorRestricaoContrato
     * 
     */
    public void deleteCdIndicadorRestricaoContrato()
    {
        this._has_cdIndicadorRestricaoContrato= false;
    } //-- void deleteCdIndicadorRestricaoContrato() 

    /**
     * Method deleteCdMensagemLinhaExtrato
     * 
     */
    public void deleteCdMensagemLinhaExtrato()
    {
        this._has_cdMensagemLinhaExtrato= false;
    } //-- void deleteCdMensagemLinhaExtrato() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRecursoLancamentoCredito
     * 
     */
    public void deleteCdRecursoLancamentoCredito()
    {
        this._has_cdRecursoLancamentoCredito= false;
    } //-- void deleteCdRecursoLancamentoCredito() 

    /**
     * Method deleteCdRecursoLancamentoDebito
     * 
     */
    public void deleteCdRecursoLancamentoDebito()
    {
        this._has_cdRecursoLancamentoDebito= false;
    } //-- void deleteCdRecursoLancamentoDebito() 

    /**
     * Method deleteCdRelacionadoProduto
     * 
     */
    public void deleteCdRelacionadoProduto()
    {
        this._has_cdRelacionadoProduto= false;
    } //-- void deleteCdRelacionadoProduto() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoMensagemExtrato
     * 
     */
    public void deleteCdTipoMensagemExtrato()
    {
        this._has_cdTipoMensagemExtrato= false;
    } //-- void deleteCdTipoMensagemExtrato() 

    /**
     * Method deleteNrEventoLancamentoCredito
     * 
     */
    public void deleteNrEventoLancamentoCredito()
    {
        this._has_nrEventoLancamentoCredito= false;
    } //-- void deleteNrEventoLancamentoCredito() 

    /**
     * Method deleteNrEventoLancamentoDebito
     * 
     */
    public void deleteNrEventoLancamentoDebito()
    {
        this._has_nrEventoLancamentoDebito= false;
    } //-- void deleteNrEventoLancamentoDebito() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdIdiomaLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaLancamentoCredito'.
     */
    public int getCdIdiomaLancamentoCredito()
    {
        return this._cdIdiomaLancamentoCredito;
    } //-- int getCdIdiomaLancamentoCredito() 

    /**
     * Returns the value of field 'cdIdiomaLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaLancamentoDebito'.
     */
    public int getCdIdiomaLancamentoDebito()
    {
        return this._cdIdiomaLancamentoDebito;
    } //-- int getCdIdiomaLancamentoDebito() 

    /**
     * Returns the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRestricaoContrato'.
     */
    public int getCdIndicadorRestricaoContrato()
    {
        return this._cdIndicadorRestricaoContrato;
    } //-- int getCdIndicadorRestricaoContrato() 

    /**
     * Returns the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhaExtrato'.
     */
    public int getCdMensagemLinhaExtrato()
    {
        return this._cdMensagemLinhaExtrato;
    } //-- int getCdMensagemLinhaExtrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRecursoLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdRecursoLancamentoCredito'.
     */
    public int getCdRecursoLancamentoCredito()
    {
        return this._cdRecursoLancamentoCredito;
    } //-- int getCdRecursoLancamentoCredito() 

    /**
     * Returns the value of field 'cdRecursoLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdRecursoLancamentoDebito'.
     */
    public int getCdRecursoLancamentoDebito()
    {
        return this._cdRecursoLancamentoDebito;
    } //-- int getCdRecursoLancamentoDebito() 

    /**
     * Returns the value of field 'cdRelacionadoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionadoProduto'.
     */
    public int getCdRelacionadoProduto()
    {
        return this._cdRelacionadoProduto;
    } //-- int getCdRelacionadoProduto() 

    /**
     * Returns the value of field 'cdSistemaLancamentoCredito'.
     * 
     * @return String
     * @return the value of field 'cdSistemaLancamentoCredito'.
     */
    public java.lang.String getCdSistemaLancamentoCredito()
    {
        return this._cdSistemaLancamentoCredito;
    } //-- java.lang.String getCdSistemaLancamentoCredito() 

    /**
     * Returns the value of field 'cdSistemaLancamentoDebito'.
     * 
     * @return String
     * @return the value of field 'cdSistemaLancamentoDebito'.
     */
    public java.lang.String getCdSistemaLancamentoDebito()
    {
        return this._cdSistemaLancamentoDebito;
    } //-- java.lang.String getCdSistemaLancamentoDebito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoMensagemExtrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemExtrato'.
     */
    public int getCdTipoMensagemExtrato()
    {
        return this._cdTipoMensagemExtrato;
    } //-- int getCdTipoMensagemExtrato() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsEventoLancamentoCredito'.
     * 
     * @return String
     * @return the value of field 'dsEventoLancamentoCredito'.
     */
    public java.lang.String getDsEventoLancamentoCredito()
    {
        return this._dsEventoLancamentoCredito;
    } //-- java.lang.String getDsEventoLancamentoCredito() 

    /**
     * Returns the value of field 'dsEventoLancamentoDebito'.
     * 
     * @return String
     * @return the value of field 'dsEventoLancamentoDebito'.
     */
    public java.lang.String getDsEventoLancamentoDebito()
    {
        return this._dsEventoLancamentoDebito;
    } //-- java.lang.String getDsEventoLancamentoDebito() 

    /**
     * Returns the value of field 'dsIdiomaLancamentoCredito'.
     * 
     * @return String
     * @return the value of field 'dsIdiomaLancamentoCredito'.
     */
    public java.lang.String getDsIdiomaLancamentoCredito()
    {
        return this._dsIdiomaLancamentoCredito;
    } //-- java.lang.String getDsIdiomaLancamentoCredito() 

    /**
     * Returns the value of field 'dsIdiomaLancamentoDebito'.
     * 
     * @return String
     * @return the value of field 'dsIdiomaLancamentoDebito'.
     */
    public java.lang.String getDsIdiomaLancamentoDebito()
    {
        return this._dsIdiomaLancamentoDebito;
    } //-- java.lang.String getDsIdiomaLancamentoDebito() 

    /**
     * Returns the value of field 'dsMensagemLinhaCredito'.
     * 
     * @return String
     * @return the value of field 'dsMensagemLinhaCredito'.
     */
    public java.lang.String getDsMensagemLinhaCredito()
    {
        return this._dsMensagemLinhaCredito;
    } //-- java.lang.String getDsMensagemLinhaCredito() 

    /**
     * Returns the value of field 'dsMensagemLinhaDebito'.
     * 
     * @return String
     * @return the value of field 'dsMensagemLinhaDebito'.
     */
    public java.lang.String getDsMensagemLinhaDebito()
    {
        return this._dsMensagemLinhaDebito;
    } //-- java.lang.String getDsMensagemLinhaDebito() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsRecursoLancamentoCredito'.
     * 
     * @return String
     * @return the value of field 'dsRecursoLancamentoCredito'.
     */
    public java.lang.String getDsRecursoLancamentoCredito()
    {
        return this._dsRecursoLancamentoCredito;
    } //-- java.lang.String getDsRecursoLancamentoCredito() 

    /**
     * Returns the value of field 'dsRecursoLancamentoDebito'.
     * 
     * @return String
     * @return the value of field 'dsRecursoLancamentoDebito'.
     */
    public java.lang.String getDsRecursoLancamentoDebito()
    {
        return this._dsRecursoLancamentoDebito;
    } //-- java.lang.String getDsRecursoLancamentoDebito() 

    /**
     * Returns the value of field 'dsSistemaLancamentoCredito'.
     * 
     * @return String
     * @return the value of field 'dsSistemaLancamentoCredito'.
     */
    public java.lang.String getDsSistemaLancamentoCredito()
    {
        return this._dsSistemaLancamentoCredito;
    } //-- java.lang.String getDsSistemaLancamentoCredito() 

    /**
     * Returns the value of field 'dsSistemaLancamentoDebito'.
     * 
     * @return String
     * @return the value of field 'dsSistemaLancamentoDebito'.
     */
    public java.lang.String getDsSistemaLancamentoDebito()
    {
        return this._dsSistemaLancamentoDebito;
    } //-- java.lang.String getDsSistemaLancamentoDebito() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrEventoLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'nrEventoLancamentoCredito'.
     */
    public int getNrEventoLancamentoCredito()
    {
        return this._nrEventoLancamentoCredito;
    } //-- int getNrEventoLancamentoCredito() 

    /**
     * Returns the value of field 'nrEventoLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'nrEventoLancamentoDebito'.
     */
    public int getNrEventoLancamentoDebito()
    {
        return this._nrEventoLancamentoDebito;
    } //-- int getNrEventoLancamentoDebito() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdIdiomaLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaLancamentoCredito()
    {
        return this._has_cdIdiomaLancamentoCredito;
    } //-- boolean hasCdIdiomaLancamentoCredito() 

    /**
     * Method hasCdIdiomaLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaLancamentoDebito()
    {
        return this._has_cdIdiomaLancamentoDebito;
    } //-- boolean hasCdIdiomaLancamentoDebito() 

    /**
     * Method hasCdIndicadorRestricaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRestricaoContrato()
    {
        return this._has_cdIndicadorRestricaoContrato;
    } //-- boolean hasCdIndicadorRestricaoContrato() 

    /**
     * Method hasCdMensagemLinhaExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhaExtrato()
    {
        return this._has_cdMensagemLinhaExtrato;
    } //-- boolean hasCdMensagemLinhaExtrato() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRecursoLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecursoLancamentoCredito()
    {
        return this._has_cdRecursoLancamentoCredito;
    } //-- boolean hasCdRecursoLancamentoCredito() 

    /**
     * Method hasCdRecursoLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecursoLancamentoDebito()
    {
        return this._has_cdRecursoLancamentoDebito;
    } //-- boolean hasCdRecursoLancamentoDebito() 

    /**
     * Method hasCdRelacionadoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionadoProduto()
    {
        return this._has_cdRelacionadoProduto;
    } //-- boolean hasCdRelacionadoProduto() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoMensagemExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemExtrato()
    {
        return this._has_cdTipoMensagemExtrato;
    } //-- boolean hasCdTipoMensagemExtrato() 

    /**
     * Method hasNrEventoLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoLancamentoCredito()
    {
        return this._has_nrEventoLancamentoCredito;
    } //-- boolean hasNrEventoLancamentoCredito() 

    /**
     * Method hasNrEventoLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoLancamentoDebito()
    {
        return this._has_nrEventoLancamentoDebito;
    } //-- boolean hasNrEventoLancamentoDebito() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdIdiomaLancamentoCredito'.
     * 
     * @param cdIdiomaLancamentoCredito the value of field
     * 'cdIdiomaLancamentoCredito'.
     */
    public void setCdIdiomaLancamentoCredito(int cdIdiomaLancamentoCredito)
    {
        this._cdIdiomaLancamentoCredito = cdIdiomaLancamentoCredito;
        this._has_cdIdiomaLancamentoCredito = true;
    } //-- void setCdIdiomaLancamentoCredito(int) 

    /**
     * Sets the value of field 'cdIdiomaLancamentoDebito'.
     * 
     * @param cdIdiomaLancamentoDebito the value of field
     * 'cdIdiomaLancamentoDebito'.
     */
    public void setCdIdiomaLancamentoDebito(int cdIdiomaLancamentoDebito)
    {
        this._cdIdiomaLancamentoDebito = cdIdiomaLancamentoDebito;
        this._has_cdIdiomaLancamentoDebito = true;
    } //-- void setCdIdiomaLancamentoDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @param cdIndicadorRestricaoContrato the value of field
     * 'cdIndicadorRestricaoContrato'.
     */
    public void setCdIndicadorRestricaoContrato(int cdIndicadorRestricaoContrato)
    {
        this._cdIndicadorRestricaoContrato = cdIndicadorRestricaoContrato;
        this._has_cdIndicadorRestricaoContrato = true;
    } //-- void setCdIndicadorRestricaoContrato(int) 

    /**
     * Sets the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @param cdMensagemLinhaExtrato the value of field
     * 'cdMensagemLinhaExtrato'.
     */
    public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato)
    {
        this._cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
        this._has_cdMensagemLinhaExtrato = true;
    } //-- void setCdMensagemLinhaExtrato(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRecursoLancamentoCredito'.
     * 
     * @param cdRecursoLancamentoCredito the value of field
     * 'cdRecursoLancamentoCredito'.
     */
    public void setCdRecursoLancamentoCredito(int cdRecursoLancamentoCredito)
    {
        this._cdRecursoLancamentoCredito = cdRecursoLancamentoCredito;
        this._has_cdRecursoLancamentoCredito = true;
    } //-- void setCdRecursoLancamentoCredito(int) 

    /**
     * Sets the value of field 'cdRecursoLancamentoDebito'.
     * 
     * @param cdRecursoLancamentoDebito the value of field
     * 'cdRecursoLancamentoDebito'.
     */
    public void setCdRecursoLancamentoDebito(int cdRecursoLancamentoDebito)
    {
        this._cdRecursoLancamentoDebito = cdRecursoLancamentoDebito;
        this._has_cdRecursoLancamentoDebito = true;
    } //-- void setCdRecursoLancamentoDebito(int) 

    /**
     * Sets the value of field 'cdRelacionadoProduto'.
     * 
     * @param cdRelacionadoProduto the value of field
     * 'cdRelacionadoProduto'.
     */
    public void setCdRelacionadoProduto(int cdRelacionadoProduto)
    {
        this._cdRelacionadoProduto = cdRelacionadoProduto;
        this._has_cdRelacionadoProduto = true;
    } //-- void setCdRelacionadoProduto(int) 

    /**
     * Sets the value of field 'cdSistemaLancamentoCredito'.
     * 
     * @param cdSistemaLancamentoCredito the value of field
     * 'cdSistemaLancamentoCredito'.
     */
    public void setCdSistemaLancamentoCredito(java.lang.String cdSistemaLancamentoCredito)
    {
        this._cdSistemaLancamentoCredito = cdSistemaLancamentoCredito;
    } //-- void setCdSistemaLancamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdSistemaLancamentoDebito'.
     * 
     * @param cdSistemaLancamentoDebito the value of field
     * 'cdSistemaLancamentoDebito'.
     */
    public void setCdSistemaLancamentoDebito(java.lang.String cdSistemaLancamentoDebito)
    {
        this._cdSistemaLancamentoDebito = cdSistemaLancamentoDebito;
    } //-- void setCdSistemaLancamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoMensagemExtrato'.
     * 
     * @param cdTipoMensagemExtrato the value of field
     * 'cdTipoMensagemExtrato'.
     */
    public void setCdTipoMensagemExtrato(int cdTipoMensagemExtrato)
    {
        this._cdTipoMensagemExtrato = cdTipoMensagemExtrato;
        this._has_cdTipoMensagemExtrato = true;
    } //-- void setCdTipoMensagemExtrato(int) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsEventoLancamentoCredito'.
     * 
     * @param dsEventoLancamentoCredito the value of field
     * 'dsEventoLancamentoCredito'.
     */
    public void setDsEventoLancamentoCredito(java.lang.String dsEventoLancamentoCredito)
    {
        this._dsEventoLancamentoCredito = dsEventoLancamentoCredito;
    } //-- void setDsEventoLancamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsEventoLancamentoDebito'.
     * 
     * @param dsEventoLancamentoDebito the value of field
     * 'dsEventoLancamentoDebito'.
     */
    public void setDsEventoLancamentoDebito(java.lang.String dsEventoLancamentoDebito)
    {
        this._dsEventoLancamentoDebito = dsEventoLancamentoDebito;
    } //-- void setDsEventoLancamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsIdiomaLancamentoCredito'.
     * 
     * @param dsIdiomaLancamentoCredito the value of field
     * 'dsIdiomaLancamentoCredito'.
     */
    public void setDsIdiomaLancamentoCredito(java.lang.String dsIdiomaLancamentoCredito)
    {
        this._dsIdiomaLancamentoCredito = dsIdiomaLancamentoCredito;
    } //-- void setDsIdiomaLancamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsIdiomaLancamentoDebito'.
     * 
     * @param dsIdiomaLancamentoDebito the value of field
     * 'dsIdiomaLancamentoDebito'.
     */
    public void setDsIdiomaLancamentoDebito(java.lang.String dsIdiomaLancamentoDebito)
    {
        this._dsIdiomaLancamentoDebito = dsIdiomaLancamentoDebito;
    } //-- void setDsIdiomaLancamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemLinhaCredito'.
     * 
     * @param dsMensagemLinhaCredito the value of field
     * 'dsMensagemLinhaCredito'.
     */
    public void setDsMensagemLinhaCredito(java.lang.String dsMensagemLinhaCredito)
    {
        this._dsMensagemLinhaCredito = dsMensagemLinhaCredito;
    } //-- void setDsMensagemLinhaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemLinhaDebito'.
     * 
     * @param dsMensagemLinhaDebito the value of field
     * 'dsMensagemLinhaDebito'.
     */
    public void setDsMensagemLinhaDebito(java.lang.String dsMensagemLinhaDebito)
    {
        this._dsMensagemLinhaDebito = dsMensagemLinhaDebito;
    } //-- void setDsMensagemLinhaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsRecursoLancamentoCredito'.
     * 
     * @param dsRecursoLancamentoCredito the value of field
     * 'dsRecursoLancamentoCredito'.
     */
    public void setDsRecursoLancamentoCredito(java.lang.String dsRecursoLancamentoCredito)
    {
        this._dsRecursoLancamentoCredito = dsRecursoLancamentoCredito;
    } //-- void setDsRecursoLancamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsRecursoLancamentoDebito'.
     * 
     * @param dsRecursoLancamentoDebito the value of field
     * 'dsRecursoLancamentoDebito'.
     */
    public void setDsRecursoLancamentoDebito(java.lang.String dsRecursoLancamentoDebito)
    {
        this._dsRecursoLancamentoDebito = dsRecursoLancamentoDebito;
    } //-- void setDsRecursoLancamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsSistemaLancamentoCredito'.
     * 
     * @param dsSistemaLancamentoCredito the value of field
     * 'dsSistemaLancamentoCredito'.
     */
    public void setDsSistemaLancamentoCredito(java.lang.String dsSistemaLancamentoCredito)
    {
        this._dsSistemaLancamentoCredito = dsSistemaLancamentoCredito;
    } //-- void setDsSistemaLancamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsSistemaLancamentoDebito'.
     * 
     * @param dsSistemaLancamentoDebito the value of field
     * 'dsSistemaLancamentoDebito'.
     */
    public void setDsSistemaLancamentoDebito(java.lang.String dsSistemaLancamentoDebito)
    {
        this._dsSistemaLancamentoDebito = dsSistemaLancamentoDebito;
    } //-- void setDsSistemaLancamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrEventoLancamentoCredito'.
     * 
     * @param nrEventoLancamentoCredito the value of field
     * 'nrEventoLancamentoCredito'.
     */
    public void setNrEventoLancamentoCredito(int nrEventoLancamentoCredito)
    {
        this._nrEventoLancamentoCredito = nrEventoLancamentoCredito;
        this._has_nrEventoLancamentoCredito = true;
    } //-- void setNrEventoLancamentoCredito(int) 

    /**
     * Sets the value of field 'nrEventoLancamentoDebito'.
     * 
     * @param nrEventoLancamentoDebito the value of field
     * 'nrEventoLancamentoDebito'.
     */
    public void setNrEventoLancamentoDebito(int nrEventoLancamentoDebito)
    {
        this._nrEventoLancamentoDebito = nrEventoLancamentoDebito;
        this._has_nrEventoLancamentoDebito = true;
    } //-- void setNrEventoLancamentoDebito(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
