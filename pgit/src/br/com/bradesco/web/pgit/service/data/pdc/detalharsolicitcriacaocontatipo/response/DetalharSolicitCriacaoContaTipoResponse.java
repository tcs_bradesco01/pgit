/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolicitCriacaoContaTipoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolicitCriacaoContaTipoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdCpfCnpjPssoa
     */
    private long _cdCpfCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPssoa
     */
    private boolean _has_cdCpfCnpjPssoa;

    /**
     * Field _cdFilialCnpjPssoa
     */
    private int _cdFilialCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjPssoa
     */
    private boolean _has_cdFilialCnpjPssoa;

    /**
     * Field _cdControleCnpjPssoa
     */
    private int _cdControleCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdControleCnpjPssoa
     */
    private boolean _has_cdControleCnpjPssoa;

    /**
     * Field _dsNomePssoa
     */
    private java.lang.String _dsNomePssoa;

    /**
     * Field _cdSituacaoSolicitacao
     */
    private int _cdSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoSolicitacao
     */
    private boolean _has_cdSituacaoSolicitacao;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;

    /**
     * Field _cdMotvoSolicit
     */
    private int _cdMotvoSolicit = 0;

    /**
     * keeps track of state for field: _cdMotvoSolicit
     */
    private boolean _has_cdMotvoSolicit;

    /**
     * Field _dsMotvoSolicit
     */
    private java.lang.String _dsMotvoSolicit;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdGrupoContabil
     */
    private int _cdGrupoContabil = 0;

    /**
     * keeps track of state for field: _cdGrupoContabil
     */
    private boolean _has_cdGrupoContabil;

    /**
     * Field _cdSubGrupoContabil
     */
    private int _cdSubGrupoContabil = 0;

    /**
     * keeps track of state for field: _cdSubGrupoContabil
     */
    private boolean _has_cdSubGrupoContabil;

    /**
     * Field _dsGrupoContabil
     */
    private java.lang.String _dsGrupoContabil;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _dsUsuarioInclusao
     */
    private java.lang.String _dsUsuarioInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _dtTipoCanal
     */
    private java.lang.String _dtTipoCanal;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolicitCriacaoContaTipoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.response.DetalharSolicitCriacaoContaTipoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdControleCnpjPssoa
     * 
     */
    public void deleteCdControleCnpjPssoa()
    {
        this._has_cdControleCnpjPssoa= false;
    } //-- void deleteCdControleCnpjPssoa() 

    /**
     * Method deleteCdCpfCnpjPssoa
     * 
     */
    public void deleteCdCpfCnpjPssoa()
    {
        this._has_cdCpfCnpjPssoa= false;
    } //-- void deleteCdCpfCnpjPssoa() 

    /**
     * Method deleteCdFilialCnpjPssoa
     * 
     */
    public void deleteCdFilialCnpjPssoa()
    {
        this._has_cdFilialCnpjPssoa= false;
    } //-- void deleteCdFilialCnpjPssoa() 

    /**
     * Method deleteCdGrupoContabil
     * 
     */
    public void deleteCdGrupoContabil()
    {
        this._has_cdGrupoContabil= false;
    } //-- void deleteCdGrupoContabil() 

    /**
     * Method deleteCdMotvoSolicit
     * 
     */
    public void deleteCdMotvoSolicit()
    {
        this._has_cdMotvoSolicit= false;
    } //-- void deleteCdMotvoSolicit() 

    /**
     * Method deleteCdSituacaoSolicitacao
     * 
     */
    public void deleteCdSituacaoSolicitacao()
    {
        this._has_cdSituacaoSolicitacao= false;
    } //-- void deleteCdSituacaoSolicitacao() 

    /**
     * Method deleteCdSubGrupoContabil
     * 
     */
    public void deleteCdSubGrupoContabil()
    {
        this._has_cdSubGrupoContabil= false;
    } //-- void deleteCdSubGrupoContabil() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdControleCnpjPssoa'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjPssoa'.
     */
    public int getCdControleCnpjPssoa()
    {
        return this._cdControleCnpjPssoa;
    } //-- int getCdControleCnpjPssoa() 

    /**
     * Returns the value of field 'cdCpfCnpjPssoa'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPssoa'.
     */
    public long getCdCpfCnpjPssoa()
    {
        return this._cdCpfCnpjPssoa;
    } //-- long getCdCpfCnpjPssoa() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFilialCnpjPssoa'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjPssoa'.
     */
    public int getCdFilialCnpjPssoa()
    {
        return this._cdFilialCnpjPssoa;
    } //-- int getCdFilialCnpjPssoa() 

    /**
     * Returns the value of field 'cdGrupoContabil'.
     * 
     * @return int
     * @return the value of field 'cdGrupoContabil'.
     */
    public int getCdGrupoContabil()
    {
        return this._cdGrupoContabil;
    } //-- int getCdGrupoContabil() 

    /**
     * Returns the value of field 'cdMotvoSolicit'.
     * 
     * @return int
     * @return the value of field 'cdMotvoSolicit'.
     */
    public int getCdMotvoSolicit()
    {
        return this._cdMotvoSolicit;
    } //-- int getCdMotvoSolicit() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacao'.
     */
    public int getCdSituacaoSolicitacao()
    {
        return this._cdSituacaoSolicitacao;
    } //-- int getCdSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdSubGrupoContabil'.
     * 
     * @return int
     * @return the value of field 'cdSubGrupoContabil'.
     */
    public int getCdSubGrupoContabil()
    {
        return this._cdSubGrupoContabil;
    } //-- int getCdSubGrupoContabil() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsGrupoContabil'.
     * 
     * @return String
     * @return the value of field 'dsGrupoContabil'.
     */
    public java.lang.String getDsGrupoContabil()
    {
        return this._dsGrupoContabil;
    } //-- java.lang.String getDsGrupoContabil() 

    /**
     * Returns the value of field 'dsMotvoSolicit'.
     * 
     * @return String
     * @return the value of field 'dsMotvoSolicit'.
     */
    public java.lang.String getDsMotvoSolicit()
    {
        return this._dsMotvoSolicit;
    } //-- java.lang.String getDsMotvoSolicit() 

    /**
     * Returns the value of field 'dsNomePssoa'.
     * 
     * @return String
     * @return the value of field 'dsNomePssoa'.
     */
    public java.lang.String getDsNomePssoa()
    {
        return this._dsNomePssoa;
    } //-- java.lang.String getDsNomePssoa() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dsUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'dsUsuarioInclusao'.
     */
    public java.lang.String getDsUsuarioInclusao()
    {
        return this._dsUsuarioInclusao;
    } //-- java.lang.String getDsUsuarioInclusao() 

    /**
     * Returns the value of field 'dtTipoCanal'.
     * 
     * @return String
     * @return the value of field 'dtTipoCanal'.
     */
    public java.lang.String getDtTipoCanal()
    {
        return this._dtTipoCanal;
    } //-- java.lang.String getDtTipoCanal() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdControleCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjPssoa()
    {
        return this._has_cdControleCnpjPssoa;
    } //-- boolean hasCdControleCnpjPssoa() 

    /**
     * Method hasCdCpfCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPssoa()
    {
        return this._has_cdCpfCnpjPssoa;
    } //-- boolean hasCdCpfCnpjPssoa() 

    /**
     * Method hasCdFilialCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjPssoa()
    {
        return this._has_cdFilialCnpjPssoa;
    } //-- boolean hasCdFilialCnpjPssoa() 

    /**
     * Method hasCdGrupoContabil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoContabil()
    {
        return this._has_cdGrupoContabil;
    } //-- boolean hasCdGrupoContabil() 

    /**
     * Method hasCdMotvoSolicit
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotvoSolicit()
    {
        return this._has_cdMotvoSolicit;
    } //-- boolean hasCdMotvoSolicit() 

    /**
     * Method hasCdSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacao()
    {
        return this._has_cdSituacaoSolicitacao;
    } //-- boolean hasCdSituacaoSolicitacao() 

    /**
     * Method hasCdSubGrupoContabil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubGrupoContabil()
    {
        return this._has_cdSubGrupoContabil;
    } //-- boolean hasCdSubGrupoContabil() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdControleCnpjPssoa'.
     * 
     * @param cdControleCnpjPssoa the value of field
     * 'cdControleCnpjPssoa'.
     */
    public void setCdControleCnpjPssoa(int cdControleCnpjPssoa)
    {
        this._cdControleCnpjPssoa = cdControleCnpjPssoa;
        this._has_cdControleCnpjPssoa = true;
    } //-- void setCdControleCnpjPssoa(int) 

    /**
     * Sets the value of field 'cdCpfCnpjPssoa'.
     * 
     * @param cdCpfCnpjPssoa the value of field 'cdCpfCnpjPssoa'.
     */
    public void setCdCpfCnpjPssoa(long cdCpfCnpjPssoa)
    {
        this._cdCpfCnpjPssoa = cdCpfCnpjPssoa;
        this._has_cdCpfCnpjPssoa = true;
    } //-- void setCdCpfCnpjPssoa(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpjPssoa'.
     * 
     * @param cdFilialCnpjPssoa the value of field
     * 'cdFilialCnpjPssoa'.
     */
    public void setCdFilialCnpjPssoa(int cdFilialCnpjPssoa)
    {
        this._cdFilialCnpjPssoa = cdFilialCnpjPssoa;
        this._has_cdFilialCnpjPssoa = true;
    } //-- void setCdFilialCnpjPssoa(int) 

    /**
     * Sets the value of field 'cdGrupoContabil'.
     * 
     * @param cdGrupoContabil the value of field 'cdGrupoContabil'.
     */
    public void setCdGrupoContabil(int cdGrupoContabil)
    {
        this._cdGrupoContabil = cdGrupoContabil;
        this._has_cdGrupoContabil = true;
    } //-- void setCdGrupoContabil(int) 

    /**
     * Sets the value of field 'cdMotvoSolicit'.
     * 
     * @param cdMotvoSolicit the value of field 'cdMotvoSolicit'.
     */
    public void setCdMotvoSolicit(int cdMotvoSolicit)
    {
        this._cdMotvoSolicit = cdMotvoSolicit;
        this._has_cdMotvoSolicit = true;
    } //-- void setCdMotvoSolicit(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacao'.
     * 
     * @param cdSituacaoSolicitacao the value of field
     * 'cdSituacaoSolicitacao'.
     */
    public void setCdSituacaoSolicitacao(int cdSituacaoSolicitacao)
    {
        this._cdSituacaoSolicitacao = cdSituacaoSolicitacao;
        this._has_cdSituacaoSolicitacao = true;
    } //-- void setCdSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdSubGrupoContabil'.
     * 
     * @param cdSubGrupoContabil the value of field
     * 'cdSubGrupoContabil'.
     */
    public void setCdSubGrupoContabil(int cdSubGrupoContabil)
    {
        this._cdSubGrupoContabil = cdSubGrupoContabil;
        this._has_cdSubGrupoContabil = true;
    } //-- void setCdSubGrupoContabil(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoContabil'.
     * 
     * @param dsGrupoContabil the value of field 'dsGrupoContabil'.
     */
    public void setDsGrupoContabil(java.lang.String dsGrupoContabil)
    {
        this._dsGrupoContabil = dsGrupoContabil;
    } //-- void setDsGrupoContabil(java.lang.String) 

    /**
     * Sets the value of field 'dsMotvoSolicit'.
     * 
     * @param dsMotvoSolicit the value of field 'dsMotvoSolicit'.
     */
    public void setDsMotvoSolicit(java.lang.String dsMotvoSolicit)
    {
        this._dsMotvoSolicit = dsMotvoSolicit;
    } //-- void setDsMotvoSolicit(java.lang.String) 

    /**
     * Sets the value of field 'dsNomePssoa'.
     * 
     * @param dsNomePssoa the value of field 'dsNomePssoa'.
     */
    public void setDsNomePssoa(java.lang.String dsNomePssoa)
    {
        this._dsNomePssoa = dsNomePssoa;
    } //-- void setDsNomePssoa(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsUsuarioInclusao'.
     * 
     * @param dsUsuarioInclusao the value of field
     * 'dsUsuarioInclusao'.
     */
    public void setDsUsuarioInclusao(java.lang.String dsUsuarioInclusao)
    {
        this._dsUsuarioInclusao = dsUsuarioInclusao;
    } //-- void setDsUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtTipoCanal'.
     * 
     * @param dtTipoCanal the value of field 'dtTipoCanal'.
     */
    public void setDtTipoCanal(java.lang.String dtTipoCanal)
    {
        this._dtTipoCanal = dtTipoCanal;
    } //-- void setDtTipoCanal(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolicitCriacaoContaTipoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.response.DetalharSolicitCriacaoContaTipoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.response.DetalharSolicitCriacaoContaTipoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.response.DetalharSolicitCriacaoContaTipoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.response.DetalharSolicitCriacaoContaTipoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
