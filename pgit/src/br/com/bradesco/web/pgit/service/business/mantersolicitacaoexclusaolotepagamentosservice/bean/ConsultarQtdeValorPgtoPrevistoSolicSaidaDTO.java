package br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean;

import java.math.BigDecimal;

public class ConsultarQtdeValorPgtoPrevistoSolicSaidaDTO {

	private String codMensagem;
	private String mensagem;
	private Long qtdeTotalPagtoPrevistoSoltc;
	private BigDecimal vlrTotPagtoPrevistoSolicitacao;

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Long getQtdeTotalPagtoPrevistoSoltc() {
		return qtdeTotalPagtoPrevistoSoltc;
	}

	public void setQtdeTotalPagtoPrevistoSoltc(Long qtdeTotalPagtoPrevistoSoltc) {
		this.qtdeTotalPagtoPrevistoSoltc = qtdeTotalPagtoPrevistoSoltc;
	}

	public BigDecimal getVlrTotPagtoPrevistoSolicitacao() {
		return vlrTotPagtoPrevistoSolicitacao;
	}

	public void setVlrTotPagtoPrevistoSolicitacao(
			BigDecimal vlrTotPagtoPrevistoSolicitacao) {
		this.vlrTotPagtoPrevistoSolicitacao = vlrTotPagtoPrevistoSolicitacao;
	}

}
