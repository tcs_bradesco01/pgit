/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.domain
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 04/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

/**
 * Nome: ContratoPrestacaoServico
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ContratoPrestacaoServico {

    /** Atributo identificacao. */
    private IdentificacaoContrato identificacao = null;

    /** Atributo anexo. */
    private Anexo anexo = null;

    /** Atributo v. */
    private String versao = null;
    
    /** Atributo data. */
    private String data = null;
    
    /**
     * Instancia um novo ContratoPrestacaoServico
     *
     * @see
     */
    public ContratoPrestacaoServico() {
        super();
    }

    /**
     * Nome: getIdentificacao.
     *
     * @return identificacao
     */
    public IdentificacaoContrato getIdentificacao() {
        return identificacao;
    }

    /**
     * Nome: setIdentificacao.
     *
     * @param identificacao the identificacao
     */
    public void setIdentificacao(IdentificacaoContrato identificacao) {
        this.identificacao = identificacao;
    }

    /**
     * Nome: getAnexo.
     *
     * @return anexo
     */
    public Anexo getAnexo() {
        return anexo;
    }

    /**
     * Nome: setAnexo.
     *
     * @param anexo the anexo
     */
    public void setAnexo(Anexo anexo) {
        this.anexo = anexo;
    }

    /**
     * Nome: getV.
     *
     * @return v
     */
    public String getVersao() {
        return versao;
    }

    /**
     * Nome: setV.
     *
     * @param v the v
     */
    public void setVersao(String versao) {
        this.versao = versao;
    }

    /**
     * Nome: getData.
     *
     * @return data
     */
    public String getData() {
        return data;
    }

    /**
     * Nome: setData.
     *
     * @param data the data
     */
    public void setData(String data) {
        this.data = data;
    }

}
