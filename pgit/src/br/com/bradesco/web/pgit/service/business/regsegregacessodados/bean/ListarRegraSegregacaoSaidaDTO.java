/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: ListarRegraSegregacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarRegraSegregacaoSaidaDTO {
	
	/** Atributo codigoRegra. */
	private int codigoRegra;
	
	/** Atributo cdTipoUnidadeOrganizacionalUsuario. */
	private int cdTipoUnidadeOrganizacionalUsuario;
	
	/** Atributo dsTipoUnidadeOrganizacionalUsuario. */
	private String dsTipoUnidadeOrganizacionalUsuario;
	
	/** Atributo cdTipoUnidadeOrganizacionalProprietario. */
	private int cdTipoUnidadeOrganizacionalProprietario;
	
	/** Atributo dsTipoUnidadeOrganizacionalProprietario. */
	private String dsTipoUnidadeOrganizacionalProprietario;
	
	/** Atributo tipoAcao. */
	private int tipoAcao;
	
	/** Atributo nivelPermissao. */
	private int nivelPermissao;
	
	/** Atributo cdMensagem. */
	private String cdMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/**
	 * Get: cdMensagem.
	 *
	 * @return cdMensagem
	 */
	public String getCdMensagem() {
		return cdMensagem;
	}
	
	/**
	 * Set: cdMensagem.
	 *
	 * @param cdMensagem the cd mensagem
	 */
	public void setCdMensagem(String cdMensagem) {
		this.cdMensagem = cdMensagem;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacionalProprietario.
	 *
	 * @return cdTipoUnidadeOrganizacionalProprietario
	 */
	public int getCdTipoUnidadeOrganizacionalProprietario() {
		return cdTipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacionalProprietario.
	 *
	 * @param cdTipoUnidadeOrganizacionalProprietario the cd tipo unidade organizacional proprietario
	 */
	public void setCdTipoUnidadeOrganizacionalProprietario(
			int cdTipoUnidadeOrganizacionalProprietario) {
		this.cdTipoUnidadeOrganizacionalProprietario = cdTipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacionalUsuario.
	 *
	 * @return cdTipoUnidadeOrganizacionalUsuario
	 */
	public int getCdTipoUnidadeOrganizacionalUsuario() {
		return cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacionalUsuario.
	 *
	 * @param cdTipoUnidadeOrganizacionalUsuario the cd tipo unidade organizacional usuario
	 */
	public void setCdTipoUnidadeOrganizacionalUsuario(
			int cdTipoUnidadeOrganizacionalUsuario) {
		this.cdTipoUnidadeOrganizacionalUsuario = cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: codigoRegra.
	 *
	 * @return codigoRegra
	 */
	public int getCodigoRegra() {
		return codigoRegra;
	}
	
	/**
	 * Set: codigoRegra.
	 *
	 * @param codigoRegra the codigo regra
	 */
	public void setCodigoRegra(int codigoRegra) {
		this.codigoRegra = codigoRegra;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacionalUsuario.
	 *
	 * @return dsTipoUnidadeOrganizacionalUsuario
	 */
	public String getDsTipoUnidadeOrganizacionalUsuario() {
		return dsTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacionalUsuario.
	 *
	 * @param dscdTipoUnidadeOrganizacionalUsuario the ds tipo unidade organizacional usuario
	 */
	public void setDsTipoUnidadeOrganizacionalUsuario(
			String dscdTipoUnidadeOrganizacionalUsuario) {
		this.dsTipoUnidadeOrganizacionalUsuario = dscdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacionalProprietario.
	 *
	 * @return dsTipoUnidadeOrganizacionalProprietario
	 */
	public String getDsTipoUnidadeOrganizacionalProprietario() {
		return dsTipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacionalProprietario.
	 *
	 * @param dsTipoUnidadeOrganizacionalProprietario the ds tipo unidade organizacional proprietario
	 */
	public void setDsTipoUnidadeOrganizacionalProprietario(
			String dsTipoUnidadeOrganizacionalProprietario) {
		this.dsTipoUnidadeOrganizacionalProprietario = dsTipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nivelPermissao.
	 *
	 * @return nivelPermissao
	 */
	public int getNivelPermissao() {
		return nivelPermissao;
	}
	
	/**
	 * Set: nivelPermissao.
	 *
	 * @param nivelPermissao the nivel permissao
	 */
	public void setNivelPermissao(int nivelPermissao) {
		this.nivelPermissao = nivelPermissao;
	}
	
	/**
	 * Get: tipoAcao.
	 *
	 * @return tipoAcao
	 */
	public int getTipoAcao() {
		return tipoAcao;
	}
	
	/**
	 * Set: tipoAcao.
	 *
	 * @param tipoAcao the tipo acao
	 */
	public void setTipoAcao(int tipoAcao) {
		this.tipoAcao = tipoAcao;
	}
	
		

}
