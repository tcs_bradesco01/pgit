/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirMsgHistoricoComplementarRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirMsgHistoricoComplementarRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMensagemLinhaExtrato
     */
    private int _cdMensagemLinhaExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhaExtrato
     */
    private boolean _has_cdMensagemLinhaExtrato;

    /**
     * Field _cdSistemaLancamentoDebito
     */
    private java.lang.String _cdSistemaLancamentoDebito;

    /**
     * Field _nrEventoLancamentoDebito
     */
    private int _nrEventoLancamentoDebito = 0;

    /**
     * keeps track of state for field: _nrEventoLancamentoDebito
     */
    private boolean _has_nrEventoLancamentoDebito;

    /**
     * Field _cdRecursoLancamentoDebito
     */
    private int _cdRecursoLancamentoDebito = 0;

    /**
     * keeps track of state for field: _cdRecursoLancamentoDebito
     */
    private boolean _has_cdRecursoLancamentoDebito;

    /**
     * Field _cdIdiomaLancamentoDebito
     */
    private int _cdIdiomaLancamentoDebito = 0;

    /**
     * keeps track of state for field: _cdIdiomaLancamentoDebito
     */
    private boolean _has_cdIdiomaLancamentoDebito;

    /**
     * Field _cdSistemaLancamentoCredito
     */
    private java.lang.String _cdSistemaLancamentoCredito;

    /**
     * Field _nrEventoLancamentoCredito
     */
    private int _nrEventoLancamentoCredito = 0;

    /**
     * keeps track of state for field: _nrEventoLancamentoCredito
     */
    private boolean _has_nrEventoLancamentoCredito;

    /**
     * Field _cdRecursoLancamentoCredito
     */
    private int _cdRecursoLancamentoCredito = 0;

    /**
     * keeps track of state for field: _cdRecursoLancamentoCredito
     */
    private boolean _has_cdRecursoLancamentoCredito;

    /**
     * Field _cdIdiomaLancamentoCredito
     */
    private int _cdIdiomaLancamentoCredito = 0;

    /**
     * keeps track of state for field: _cdIdiomaLancamentoCredito
     */
    private boolean _has_cdIdiomaLancamentoCredito;

    /**
     * Field _cdIndicadorRestricaoContrato
     */
    private int _cdIndicadorRestricaoContrato = 0;

    /**
     * keeps track of state for field: _cdIndicadorRestricaoContrato
     */
    private boolean _has_cdIndicadorRestricaoContrato;

    /**
     * Field _cdTipoMensagemExtrato
     */
    private int _cdTipoMensagemExtrato = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemExtrato
     */
    private boolean _has_cdTipoMensagemExtrato;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _dsSegundaLinhaExtratoCredito
     */
    private java.lang.String _dsSegundaLinhaExtratoCredito;

    /**
     * Field _dsSegundaLinhaExtratoDebito
     */
    private java.lang.String _dsSegundaLinhaExtratoDebito;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirMsgHistoricoComplementarRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.request.IncluirMsgHistoricoComplementarRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdiomaLancamentoCredito
     * 
     */
    public void deleteCdIdiomaLancamentoCredito()
    {
        this._has_cdIdiomaLancamentoCredito= false;
    } //-- void deleteCdIdiomaLancamentoCredito() 

    /**
     * Method deleteCdIdiomaLancamentoDebito
     * 
     */
    public void deleteCdIdiomaLancamentoDebito()
    {
        this._has_cdIdiomaLancamentoDebito= false;
    } //-- void deleteCdIdiomaLancamentoDebito() 

    /**
     * Method deleteCdIndicadorRestricaoContrato
     * 
     */
    public void deleteCdIndicadorRestricaoContrato()
    {
        this._has_cdIndicadorRestricaoContrato= false;
    } //-- void deleteCdIndicadorRestricaoContrato() 

    /**
     * Method deleteCdMensagemLinhaExtrato
     * 
     */
    public void deleteCdMensagemLinhaExtrato()
    {
        this._has_cdMensagemLinhaExtrato= false;
    } //-- void deleteCdMensagemLinhaExtrato() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRecursoLancamentoCredito
     * 
     */
    public void deleteCdRecursoLancamentoCredito()
    {
        this._has_cdRecursoLancamentoCredito= false;
    } //-- void deleteCdRecursoLancamentoCredito() 

    /**
     * Method deleteCdRecursoLancamentoDebito
     * 
     */
    public void deleteCdRecursoLancamentoDebito()
    {
        this._has_cdRecursoLancamentoDebito= false;
    } //-- void deleteCdRecursoLancamentoDebito() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdTipoMensagemExtrato
     * 
     */
    public void deleteCdTipoMensagemExtrato()
    {
        this._has_cdTipoMensagemExtrato= false;
    } //-- void deleteCdTipoMensagemExtrato() 

    /**
     * Method deleteNrEventoLancamentoCredito
     * 
     */
    public void deleteNrEventoLancamentoCredito()
    {
        this._has_nrEventoLancamentoCredito= false;
    } //-- void deleteNrEventoLancamentoCredito() 

    /**
     * Method deleteNrEventoLancamentoDebito
     * 
     */
    public void deleteNrEventoLancamentoDebito()
    {
        this._has_nrEventoLancamentoDebito= false;
    } //-- void deleteNrEventoLancamentoDebito() 

    /**
     * Returns the value of field 'cdIdiomaLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaLancamentoCredito'.
     */
    public int getCdIdiomaLancamentoCredito()
    {
        return this._cdIdiomaLancamentoCredito;
    } //-- int getCdIdiomaLancamentoCredito() 

    /**
     * Returns the value of field 'cdIdiomaLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaLancamentoDebito'.
     */
    public int getCdIdiomaLancamentoDebito()
    {
        return this._cdIdiomaLancamentoDebito;
    } //-- int getCdIdiomaLancamentoDebito() 

    /**
     * Returns the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRestricaoContrato'.
     */
    public int getCdIndicadorRestricaoContrato()
    {
        return this._cdIndicadorRestricaoContrato;
    } //-- int getCdIndicadorRestricaoContrato() 

    /**
     * Returns the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhaExtrato'.
     */
    public int getCdMensagemLinhaExtrato()
    {
        return this._cdMensagemLinhaExtrato;
    } //-- int getCdMensagemLinhaExtrato() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRecursoLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdRecursoLancamentoCredito'.
     */
    public int getCdRecursoLancamentoCredito()
    {
        return this._cdRecursoLancamentoCredito;
    } //-- int getCdRecursoLancamentoCredito() 

    /**
     * Returns the value of field 'cdRecursoLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdRecursoLancamentoDebito'.
     */
    public int getCdRecursoLancamentoDebito()
    {
        return this._cdRecursoLancamentoDebito;
    } //-- int getCdRecursoLancamentoDebito() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdSistemaLancamentoCredito'.
     * 
     * @return String
     * @return the value of field 'cdSistemaLancamentoCredito'.
     */
    public java.lang.String getCdSistemaLancamentoCredito()
    {
        return this._cdSistemaLancamentoCredito;
    } //-- java.lang.String getCdSistemaLancamentoCredito() 

    /**
     * Returns the value of field 'cdSistemaLancamentoDebito'.
     * 
     * @return String
     * @return the value of field 'cdSistemaLancamentoDebito'.
     */
    public java.lang.String getCdSistemaLancamentoDebito()
    {
        return this._cdSistemaLancamentoDebito;
    } //-- java.lang.String getCdSistemaLancamentoDebito() 

    /**
     * Returns the value of field 'cdTipoMensagemExtrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemExtrato'.
     */
    public int getCdTipoMensagemExtrato()
    {
        return this._cdTipoMensagemExtrato;
    } //-- int getCdTipoMensagemExtrato() 

    /**
     * Returns the value of field 'dsSegundaLinhaExtratoCredito'.
     * 
     * @return String
     * @return the value of field 'dsSegundaLinhaExtratoCredito'.
     */
    public java.lang.String getDsSegundaLinhaExtratoCredito()
    {
        return this._dsSegundaLinhaExtratoCredito;
    } //-- java.lang.String getDsSegundaLinhaExtratoCredito() 

    /**
     * Returns the value of field 'dsSegundaLinhaExtratoDebito'.
     * 
     * @return String
     * @return the value of field 'dsSegundaLinhaExtratoDebito'.
     */
    public java.lang.String getDsSegundaLinhaExtratoDebito()
    {
        return this._dsSegundaLinhaExtratoDebito;
    } //-- java.lang.String getDsSegundaLinhaExtratoDebito() 

    /**
     * Returns the value of field 'nrEventoLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'nrEventoLancamentoCredito'.
     */
    public int getNrEventoLancamentoCredito()
    {
        return this._nrEventoLancamentoCredito;
    } //-- int getNrEventoLancamentoCredito() 

    /**
     * Returns the value of field 'nrEventoLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'nrEventoLancamentoDebito'.
     */
    public int getNrEventoLancamentoDebito()
    {
        return this._nrEventoLancamentoDebito;
    } //-- int getNrEventoLancamentoDebito() 

    /**
     * Method hasCdIdiomaLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaLancamentoCredito()
    {
        return this._has_cdIdiomaLancamentoCredito;
    } //-- boolean hasCdIdiomaLancamentoCredito() 

    /**
     * Method hasCdIdiomaLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaLancamentoDebito()
    {
        return this._has_cdIdiomaLancamentoDebito;
    } //-- boolean hasCdIdiomaLancamentoDebito() 

    /**
     * Method hasCdIndicadorRestricaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRestricaoContrato()
    {
        return this._has_cdIndicadorRestricaoContrato;
    } //-- boolean hasCdIndicadorRestricaoContrato() 

    /**
     * Method hasCdMensagemLinhaExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhaExtrato()
    {
        return this._has_cdMensagemLinhaExtrato;
    } //-- boolean hasCdMensagemLinhaExtrato() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRecursoLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecursoLancamentoCredito()
    {
        return this._has_cdRecursoLancamentoCredito;
    } //-- boolean hasCdRecursoLancamentoCredito() 

    /**
     * Method hasCdRecursoLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecursoLancamentoDebito()
    {
        return this._has_cdRecursoLancamentoDebito;
    } //-- boolean hasCdRecursoLancamentoDebito() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdTipoMensagemExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemExtrato()
    {
        return this._has_cdTipoMensagemExtrato;
    } //-- boolean hasCdTipoMensagemExtrato() 

    /**
     * Method hasNrEventoLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoLancamentoCredito()
    {
        return this._has_nrEventoLancamentoCredito;
    } //-- boolean hasNrEventoLancamentoCredito() 

    /**
     * Method hasNrEventoLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoLancamentoDebito()
    {
        return this._has_nrEventoLancamentoDebito;
    } //-- boolean hasNrEventoLancamentoDebito() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIdiomaLancamentoCredito'.
     * 
     * @param cdIdiomaLancamentoCredito the value of field
     * 'cdIdiomaLancamentoCredito'.
     */
    public void setCdIdiomaLancamentoCredito(int cdIdiomaLancamentoCredito)
    {
        this._cdIdiomaLancamentoCredito = cdIdiomaLancamentoCredito;
        this._has_cdIdiomaLancamentoCredito = true;
    } //-- void setCdIdiomaLancamentoCredito(int) 

    /**
     * Sets the value of field 'cdIdiomaLancamentoDebito'.
     * 
     * @param cdIdiomaLancamentoDebito the value of field
     * 'cdIdiomaLancamentoDebito'.
     */
    public void setCdIdiomaLancamentoDebito(int cdIdiomaLancamentoDebito)
    {
        this._cdIdiomaLancamentoDebito = cdIdiomaLancamentoDebito;
        this._has_cdIdiomaLancamentoDebito = true;
    } //-- void setCdIdiomaLancamentoDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @param cdIndicadorRestricaoContrato the value of field
     * 'cdIndicadorRestricaoContrato'.
     */
    public void setCdIndicadorRestricaoContrato(int cdIndicadorRestricaoContrato)
    {
        this._cdIndicadorRestricaoContrato = cdIndicadorRestricaoContrato;
        this._has_cdIndicadorRestricaoContrato = true;
    } //-- void setCdIndicadorRestricaoContrato(int) 

    /**
     * Sets the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @param cdMensagemLinhaExtrato the value of field
     * 'cdMensagemLinhaExtrato'.
     */
    public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato)
    {
        this._cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
        this._has_cdMensagemLinhaExtrato = true;
    } //-- void setCdMensagemLinhaExtrato(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRecursoLancamentoCredito'.
     * 
     * @param cdRecursoLancamentoCredito the value of field
     * 'cdRecursoLancamentoCredito'.
     */
    public void setCdRecursoLancamentoCredito(int cdRecursoLancamentoCredito)
    {
        this._cdRecursoLancamentoCredito = cdRecursoLancamentoCredito;
        this._has_cdRecursoLancamentoCredito = true;
    } //-- void setCdRecursoLancamentoCredito(int) 

    /**
     * Sets the value of field 'cdRecursoLancamentoDebito'.
     * 
     * @param cdRecursoLancamentoDebito the value of field
     * 'cdRecursoLancamentoDebito'.
     */
    public void setCdRecursoLancamentoDebito(int cdRecursoLancamentoDebito)
    {
        this._cdRecursoLancamentoDebito = cdRecursoLancamentoDebito;
        this._has_cdRecursoLancamentoDebito = true;
    } //-- void setCdRecursoLancamentoDebito(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdSistemaLancamentoCredito'.
     * 
     * @param cdSistemaLancamentoCredito the value of field
     * 'cdSistemaLancamentoCredito'.
     */
    public void setCdSistemaLancamentoCredito(java.lang.String cdSistemaLancamentoCredito)
    {
        this._cdSistemaLancamentoCredito = cdSistemaLancamentoCredito;
    } //-- void setCdSistemaLancamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdSistemaLancamentoDebito'.
     * 
     * @param cdSistemaLancamentoDebito the value of field
     * 'cdSistemaLancamentoDebito'.
     */
    public void setCdSistemaLancamentoDebito(java.lang.String cdSistemaLancamentoDebito)
    {
        this._cdSistemaLancamentoDebito = cdSistemaLancamentoDebito;
    } //-- void setCdSistemaLancamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoMensagemExtrato'.
     * 
     * @param cdTipoMensagemExtrato the value of field
     * 'cdTipoMensagemExtrato'.
     */
    public void setCdTipoMensagemExtrato(int cdTipoMensagemExtrato)
    {
        this._cdTipoMensagemExtrato = cdTipoMensagemExtrato;
        this._has_cdTipoMensagemExtrato = true;
    } //-- void setCdTipoMensagemExtrato(int) 

    /**
     * Sets the value of field 'dsSegundaLinhaExtratoCredito'.
     * 
     * @param dsSegundaLinhaExtratoCredito the value of field
     * 'dsSegundaLinhaExtratoCredito'.
     */
    public void setDsSegundaLinhaExtratoCredito(java.lang.String dsSegundaLinhaExtratoCredito)
    {
        this._dsSegundaLinhaExtratoCredito = dsSegundaLinhaExtratoCredito;
    } //-- void setDsSegundaLinhaExtratoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsSegundaLinhaExtratoDebito'.
     * 
     * @param dsSegundaLinhaExtratoDebito the value of field
     * 'dsSegundaLinhaExtratoDebito'.
     */
    public void setDsSegundaLinhaExtratoDebito(java.lang.String dsSegundaLinhaExtratoDebito)
    {
        this._dsSegundaLinhaExtratoDebito = dsSegundaLinhaExtratoDebito;
    } //-- void setDsSegundaLinhaExtratoDebito(java.lang.String) 

    /**
     * Sets the value of field 'nrEventoLancamentoCredito'.
     * 
     * @param nrEventoLancamentoCredito the value of field
     * 'nrEventoLancamentoCredito'.
     */
    public void setNrEventoLancamentoCredito(int nrEventoLancamentoCredito)
    {
        this._nrEventoLancamentoCredito = nrEventoLancamentoCredito;
        this._has_nrEventoLancamentoCredito = true;
    } //-- void setNrEventoLancamentoCredito(int) 

    /**
     * Sets the value of field 'nrEventoLancamentoDebito'.
     * 
     * @param nrEventoLancamentoDebito the value of field
     * 'nrEventoLancamentoDebito'.
     */
    public void setNrEventoLancamentoDebito(int nrEventoLancamentoDebito)
    {
        this._nrEventoLancamentoDebito = nrEventoLancamentoDebito;
        this._has_nrEventoLancamentoDebito = true;
    } //-- void setNrEventoLancamentoDebito(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirMsgHistoricoComplementarRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.request.IncluirMsgHistoricoComplementarRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.request.IncluirMsgHistoricoComplementarRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.request.IncluirMsgHistoricoComplementarRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.request.IncluirMsgHistoricoComplementarRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
