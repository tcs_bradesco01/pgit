/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ConsultarListaContaFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaContaFavorecidoSaidaDTO {

    /** Atributo nrOcorrenciaContaFavorecido. */
    private Integer nrOcorrenciaContaFavorecido;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo dsBanco. */
    private String dsBanco;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;

    /** Atributo nmAgenciaFavorecido. */
    private String nmAgenciaFavorecido;

    /** Atributo nrContaFavorecido. */
    private Long nrContaFavorecido;

    /** Atributo nrDigitoConta. */
    private String nrDigitoConta;

    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;

    /** Atributo dsCdTipoConta. */
    private String dsCdTipoConta;

    /** Atributo dsSituacaoConta. */
    private String dsSituacaoConta;

    /** Atributo bancoFormatado. */
    private String bancoFormatado;

    /** Atributo agenciaFormatada. */
    private String agenciaFormatada;

    /** Atributo contaFormatada. */
    private String contaFormatada;

    /** Atributo tipoContaFormatado. */
    private String tipoContaFormatado;

    /**
     * Consultar lista conta favorecido saida dto.
     */
    public ConsultarListaContaFavorecidoSaidaDTO() {
	super();
    }

    /**
     * Consultar lista conta favorecido saida dto.
     *
     * @param saida the saida
     */
    public ConsultarListaContaFavorecidoSaidaDTO(Ocorrencias saida) {
	super();
	this.nrOcorrenciaContaFavorecido = saida.getNrOcorrenciaContaFavorecido();
	this.cdBanco = saida.getCdBanco();
	this.dsBanco = saida.getDsBanco();
	this.cdAgencia = saida.getCdAgencia();
	this.cdDigitoAgencia = saida.getCdDigitoAgencia();
	this.nmAgenciaFavorecido = saida.getDsAgencia();
	this.nrContaFavorecido = saida.getNrContaFavorecido();
	this.nrDigitoConta = saida.getNrDigitoConta();
	this.cdTipoConta = saida.getCdTipoConta();
	this.dsCdTipoConta = saida.getDsCdTipoConta();
	this.dsSituacaoConta = saida.getDsSituacaoConta();

	this.bancoFormatado = PgitUtil.formatBanco(saida.getCdBanco(), saida.getDsBanco(), false);
	this.agenciaFormatada = PgitUtil.formatAgencia(saida.getCdAgencia(), saida.getCdDigitoAgencia(), saida.getDsAgencia(), false);
	this.contaFormatada = PgitUtil.formatConta(saida.getNrContaFavorecido(), saida.getNrDigitoConta(), false);
	this.tipoContaFormatado = PgitUtil.concatenarCampos(saida.getCdTipoConta(), saida.getDsCdTipoConta());
    }

    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
	return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
	this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
	return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
	this.cdBanco = cdBanco;
    }

    /**
     * Get: cdDigitoAgencia.
     *
     * @return cdDigitoAgencia
     */
    public Integer getCdDigitoAgencia() {
	return cdDigitoAgencia;
    }

    /**
     * Set: cdDigitoAgencia.
     *
     * @param cdDigitoAgencia the cd digito agencia
     */
    public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
	this.cdDigitoAgencia = cdDigitoAgencia;
    }

    /**
     * Get: cdTipoConta.
     *
     * @return cdTipoConta
     */
    public Integer getCdTipoConta() {
	return cdTipoConta;
    }

    /**
     * Set: cdTipoConta.
     *
     * @param cdTipoConta the cd tipo conta
     */
    public void setCdTipoConta(Integer cdTipoConta) {
	this.cdTipoConta = cdTipoConta;
    }

    /**
     * Get: dsBanco.
     *
     * @return dsBanco
     */
    public String getDsBanco() {
	return dsBanco;
    }

    /**
     * Set: dsBanco.
     *
     * @param dsBanco the ds banco
     */
    public void setDsBanco(String dsBanco) {
	this.dsBanco = dsBanco;
    }

    /**
     * Get: dsCdTipoConta.
     *
     * @return dsCdTipoConta
     */
    public String getDsCdTipoConta() {
	return dsCdTipoConta;
    }

    /**
     * Set: dsCdTipoConta.
     *
     * @param dsCdTipoConta the ds cd tipo conta
     */
    public void setDsCdTipoConta(String dsCdTipoConta) {
	this.dsCdTipoConta = dsCdTipoConta;
    }

    /**
     * Get: dsSituacaoConta.
     *
     * @return dsSituacaoConta
     */
    public String getDsSituacaoConta() {
	return dsSituacaoConta;
    }

    /**
     * Set: dsSituacaoConta.
     *
     * @param dsSituacaoConta the ds situacao conta
     */
    public void setDsSituacaoConta(String dsSituacaoConta) {
	this.dsSituacaoConta = dsSituacaoConta;
    }

    /**
     * Get: nmAgenciaFavorecido.
     *
     * @return nmAgenciaFavorecido
     */
    public String getNmAgenciaFavorecido() {
	return nmAgenciaFavorecido;
    }

    /**
     * Set: nmAgenciaFavorecido.
     *
     * @param nmAgenciaFavorecido the nm agencia favorecido
     */
    public void setNmAgenciaFavorecido(String nmAgenciaFavorecido) {
	this.nmAgenciaFavorecido = nmAgenciaFavorecido;
    }

    /**
     * Get: nrContaFavorecido.
     *
     * @return nrContaFavorecido
     */
    public Long getNrContaFavorecido() {
	return nrContaFavorecido;
    }

    /**
     * Set: nrContaFavorecido.
     *
     * @param nrContaFavorecido the nr conta favorecido
     */
    public void setNrContaFavorecido(Long nrContaFavorecido) {
	this.nrContaFavorecido = nrContaFavorecido;
    }

    /**
     * Get: nrDigitoConta.
     *
     * @return nrDigitoConta
     */
    public String getNrDigitoConta() {
	return nrDigitoConta;
    }

    /**
     * Set: nrDigitoConta.
     *
     * @param nrDigitoConta the nr digito conta
     */
    public void setNrDigitoConta(String nrDigitoConta) {
	this.nrDigitoConta = nrDigitoConta;
    }

    /**
     * Get: nrOcorrenciaContaFavorecido.
     *
     * @return nrOcorrenciaContaFavorecido
     */
    public Integer getNrOcorrenciaContaFavorecido() {
	return nrOcorrenciaContaFavorecido;
    }

    /**
     * Set: nrOcorrenciaContaFavorecido.
     *
     * @param nrOcorrenciaContaFavorecido the nr ocorrencia conta favorecido
     */
    public void setNrOcorrenciaContaFavorecido(Integer nrOcorrenciaContaFavorecido) {
	this.nrOcorrenciaContaFavorecido = nrOcorrenciaContaFavorecido;
    }

    /**
     * Get: agenciaFormatada.
     *
     * @return agenciaFormatada
     */
    public String getAgenciaFormatada() {
	return agenciaFormatada;
    }

    /**
     * Set: agenciaFormatada.
     *
     * @param agenciaFormatada the agencia formatada
     */
    public void setAgenciaFormatada(String agenciaFormatada) {
	this.agenciaFormatada = agenciaFormatada;
    }

    /**
     * Get: bancoFormatado.
     *
     * @return bancoFormatado
     */
    public String getBancoFormatado() {
	return bancoFormatado;
    }

    /**
     * Set: bancoFormatado.
     *
     * @param bancoFormatado the banco formatado
     */
    public void setBancoFormatado(String bancoFormatado) {
	this.bancoFormatado = bancoFormatado;
    }

    /**
     * Get: contaFormatada.
     *
     * @return contaFormatada
     */
    public String getContaFormatada() {
	return contaFormatada;
    }

    /**
     * Set: contaFormatada.
     *
     * @param contaFormatada the conta formatada
     */
    public void setContaFormatada(String contaFormatada) {
	this.contaFormatada = contaFormatada;
    }

    /**
     * Get: tipoContaFormatado.
     *
     * @return tipoContaFormatado
     */
    public String getTipoContaFormatado() {
	return tipoContaFormatado;
    }

    /**
     * Set: tipoContaFormatado.
     *
     * @param tipoContaFormatado the tipo conta formatado
     */
    public void setTipoContaFormatado(String tipoContaFormatado) {
	this.tipoContaFormatado = tipoContaFormatado;
    }

}