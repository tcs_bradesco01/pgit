/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;

/**
 * Nome: IManterSolicLiberacaoLotePgtoService
 * <p>
 * Prop�sito: Interface do adaptador ManterSolicLiberacaoLotePgto
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IManterSolicLiberacaoLotePgtoService {

	public ConsultarLotePagamentosSaidaDTO consultarSolicLiberacaoLotePgto(
			ConsultarLotePagamentosEntradaDTO entrada);

	public IncluirLotePagamentosSaidaDTO incluirSolicLiberacaoLotePgto(
			IncluirLotePagamentosEntradaDTO entrada);

	public DetalharLotePagamentosSaidaDTO detalharSolicLiberacaoLotePgto(
			DetalharLotePagamentosEntradaDTO entrada);

	public ExcluirLotePagamentosSaidaDTO excluirSolicLiberacaoLotePgto(
			ExcluirLotePagamentosEntradaDTO entrada);

}