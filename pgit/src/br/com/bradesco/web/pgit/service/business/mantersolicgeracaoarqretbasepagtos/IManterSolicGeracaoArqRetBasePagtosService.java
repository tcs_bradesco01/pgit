/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.DetalharSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.DetalharSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ExcluirSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ExcluirSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.IncluirSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.IncluirSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ListarSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ListarSolBasePagtoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterSolicGeracaoArqRetBasePagtos
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterSolicGeracaoArqRetBasePagtosService {

	/**
	 * Listar sol base pagto.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar sol base pagto saida dt o>
	 */
	List<ListarSolBasePagtoSaidaDTO> listarSolBasePagto(ListarSolBasePagtoEntradaDTO entradaDTO);
	
	/**
	 * Detalhar sol base pagto.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar sol base pagto saida dto
	 */
	DetalharSolBasePagtoSaidaDTO detalharSolBasePagto(DetalharSolBasePagtoEntradaDTO entradaDTO);
	
	/**
	 * Incluir sol base pagto.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the incluir sol base pagto saida dto
	 */
	IncluirSolBasePagtoSaidaDTO incluirSolBasePagto(IncluirSolBasePagtoEntradaDTO entradaDTO);
	
	/**
	 * Excluir sol base pagto.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir sol base pagto saida dto
	 */
	ExcluirSolBasePagtoSaidaDTO excluirSolBasePagto(ExcluirSolBasePagtoEntradaDTO entradaDTO);

}

