/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipocomprovante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipocomprovante.bean;

/**
 * Nome: ConsultarDescricaoMensagemSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDescricaoMensagemSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nrEventoMensagemNegocio. */
    private Integer nrEventoMensagemNegocio;
    
    /** Atributo dsEventoMensagemNegocio. */
    private String dsEventoMensagemNegocio;
    
    /** Atributo ordemLinhaMensagem. */
    private Long ordemLinhaMensagem;
	
    /**
     * Get: numDescMensagemNegocio.
     *
     * @return numDescMensagemNegocio
     */
    public String getNumDescMensagemNegocio(){
    	 if (dsEventoMensagemNegocio != null && !(dsEventoMensagemNegocio.equals(""))) {
    		 return nrEventoMensagemNegocio + "-" + dsEventoMensagemNegocio;
    	 }
    	 return String.valueOf(nrEventoMensagemNegocio);
    } 
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsEventoMensagemNegocio.
	 *
	 * @return dsEventoMensagemNegocio
	 */
	public String getDsEventoMensagemNegocio() {
		return dsEventoMensagemNegocio;
	}
	
	/**
	 * Set: dsEventoMensagemNegocio.
	 *
	 * @param dsEventoMensagemNegocio the ds evento mensagem negocio
	 */
	public void setDsEventoMensagemNegocio(String dsEventoMensagemNegocio) {
		this.dsEventoMensagemNegocio = dsEventoMensagemNegocio;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrEventoMensagemNegocio.
	 *
	 * @return nrEventoMensagemNegocio
	 */
	public Integer getNrEventoMensagemNegocio() {
		return nrEventoMensagemNegocio;
	}
	
	/**
	 * Set: nrEventoMensagemNegocio.
	 *
	 * @param nrEventoMensagemNegocio the nr evento mensagem negocio
	 */
	public void setNrEventoMensagemNegocio(Integer nrEventoMensagemNegocio) {
		this.nrEventoMensagemNegocio = nrEventoMensagemNegocio;
	}

	/**
	 * Get: ordemLinhaMensagem.
	 *
	 * @return ordemLinhaMensagem
	 */
	public Long getOrdemLinhaMensagem() {
		return ordemLinhaMensagem;
	}

	/**
	 * Set: ordemLinhaMensagem.
	 *
	 * @param ordemLinhaMensagem the ordem linha mensagem
	 */
	public void setOrdemLinhaMensagem(Long ordemLinhaMensagem) {
		this.ordemLinhaMensagem = ordemLinhaMensagem;
	}
    
    

}
