/*
 * Nome: br.com.bradesco.web.pgit.service.business.listaregra.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.listaregra.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listaregra.response.Ocorrencia;

/**
 * Nome: ListaRegraVO.java
 * <p>Prop�sito: TODO: Criar descri��o </p>
 * 
 * @author M&aacute;rcio Alves Barroso <br/>
 * Equipe: CPM Braxis / TI Melhorias - Arquitetura 
 * @version 1.0
 * @see 
 */
public class ListaRegraVO {

    // Vari�vel indGestorMaster
    /** Atributo indGestorMaster. */
    private String indGestorMaster;
    
    // Vari�vel tipoConsulta
    /** Atributo tipoConsulta. */
    private Integer tipoConsulta;
    
    // Vari�vel codRegraAlcada
    /** Atributo codRegraAlcada. */
    private Integer codRegraAlcada;
    
    // Vari�vel codSituacao
    /** Atributo codSituacao. */
    private Integer codSituacao;
    
    // Vari�vel dataPesquisaInic
    /** Atributo dataPesquisaInic. */
    private String dataPesquisaInic;
    
    // Vari�vel dataPesquisaFim
    /** Atributo dataPesquisaFim. */
    private String dataPesquisaFim;
    
    // Vari�vel codPessoaJuridica
    /** Atributo codPessoaJuridica. */
    private Long codPessoaJuridica;
    
    // Vari�vel codDepartamento
    /** Atributo codDepartamento. */
    private Integer codDepartamento;
    
    // Vari�vel codTipoUorg
    /** Atributo codTipoUorg. */
    private Integer codTipoUorg;
    
    
	// Vari�vel descRegraAlcada
    /** Atributo descRegraAlcada. */
	private String descRegraAlcada;
    
    // Vari�vel descSituacao
    /** Atributo descSituacao. */
    private String descSituacao;
    
    // Vari�vel dataAtivacaoRegra
    /** Atributo dataAtivacaoRegra. */
    private String dataAtivacaoRegra;
    
    // Vari�vel codEmpresa
    /** Atributo codEmpresa. */
    private Long codEmpresa;
    
    // Vari�vel descEmpresa
    /** Atributo descEmpresa. */
    private String descEmpresa;
    
    // Vari�vel codDeptoGesto
    /** Atributo codDeptoGesto. */
    private Integer codDeptoGesto;
    
    // Vari�vel descDeptoGesto
    /** Atributo descDeptoGesto. */
    private String descDeptoGesto;
    
    // Vari�vel codWorkFlow
    /** Atributo codWorkFlow. */
    private String codWorkFlow;
    
    // Vari�vel codProcessoNegocio
    /** Atributo codProcessoNegocio. */
    private String codProcessoNegocio;
     

    /**
     * Nome: ListaRegraVO
     * <p>Prop�sito: M�todo construtor que cria uma inst�ncia padr�o desta classe. </p>
     */
    public ListaRegraVO() { }

    /**
     * Lista regra vo.
     *
     * @param ocur the ocur
     */
    public ListaRegraVO(Ocorrencia ocur) { 
		this.codRegraAlcada = ocur.getCodRegraAlcada();
		this.descRegraAlcada = ocur.getDescRegraAlcada();
		this.descSituacao = ocur.getDescSituacao();
		this.dataAtivacaoRegra = ocur.getDataAtivacaoRegra();
		this.codEmpresa = ocur.getCodEmpresa();
		this.descEmpresa = ocur.getDescEmpresa();
		this.codDeptoGesto = ocur.getCodDeptoGesto();
		this.descDeptoGesto = ocur.getDescDeptoGesto();
		this.codWorkFlow = ocur.getCodWorkFlow();
		this.codSituacao = ocur.getCodSituacao();
		this.codProcessoNegocio = ocur.getCodProcessoNegocio();
    }

	/**
	 * Nome: getCodDepartamento
	 * <p>Prop�sito: M�todo get para o atributo codDepartamento. </p>
	 * @return Integer codDepartamento
	 */
	public Integer getCodDepartamento() {
		return codDepartamento;
	}

	/**
	 * Nome: setCodDepartamento
	 * <p>Prop�sito: M�todo set para o atributo codDepartamento. </p>
	 * @param Integer codDepartamento
	 */
	public void setCodDepartamento(Integer codDepartamento) {
		this.codDepartamento = codDepartamento;
	}

	/**
	 * Nome: getCodDeptoGesto
	 * <p>Prop�sito: M�todo get para o atributo codDeptoGesto. </p>
	 * @return Integer codDeptoGesto
	 */
	public Integer getCodDeptoGesto() {
		return codDeptoGesto;
	}

	/**
	 * Nome: setCodDeptoGesto
	 * <p>Prop�sito: M�todo set para o atributo codDeptoGesto. </p>
	 * @param Integer codDeptoGesto
	 */
	public void setCodDeptoGesto(Integer codDeptoGesto) {
		this.codDeptoGesto = codDeptoGesto;
	}

	/**
	 * Nome: getCodEmpresa
	 * <p>Prop�sito: M�todo get para o atributo codEmpresa. </p>
	 * @return Long codEmpresa
	 */
	public Long getCodEmpresa() {
		return codEmpresa;
	}

	/**
	 * Nome: setCodEmpresa
	 * <p>Prop�sito: M�todo set para o atributo codEmpresa. </p>
	 * @param Long codEmpresa
	 */
	public void setCodEmpresa(Long codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	/**
	 * Nome: getCodPessoaJuridica
	 * <p>Prop�sito: M�todo get para o atributo codPessoaJuridica. </p>
	 * @return Long codPessoaJuridica
	 */
	public Long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}

	/**
	 * Nome: setCodPessoaJuridica
	 * <p>Prop�sito: M�todo set para o atributo codPessoaJuridica. </p>
	 * @param Long codPessoaJuridica
	 */
	public void setCodPessoaJuridica(Long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}

	/**
	 * Nome: getCodProcessoNegocio
	 * <p>Prop�sito: M�todo get para o atributo codProcessoNegocio. </p>
	 * @return String codProcessoNegocio
	 */
	public String getCodProcessoNegocio() {
		return codProcessoNegocio;
	}

	/**
	 * Nome: setCodProcessoNegocio
	 * <p>Prop�sito: M�todo set para o atributo codProcessoNegocio. </p>
	 * @param String codProcessoNegocio
	 */
	public void setCodProcessoNegocio(String codProcessoNegocio) {
		this.codProcessoNegocio = codProcessoNegocio;
	}

	/**
	 * Nome: getCodRegraAlcada
	 * <p>Prop�sito: M�todo get para o atributo codRegraAlcada. </p>
	 * @return Integer codRegraAlcada
	 */
	public Integer getCodRegraAlcada() {
		return codRegraAlcada;
	}

	/**
	 * Nome: setCodRegraAlcada
	 * <p>Prop�sito: M�todo set para o atributo codRegraAlcada. </p>
	 * @param Integer codRegraAlcada
	 */
	public void setCodRegraAlcada(Integer codRegraAlcada) {
		this.codRegraAlcada = codRegraAlcada;
	}

	/**
	 * Nome: getCodSituacao
	 * <p>Prop�sito: M�todo get para o atributo codSituacao. </p>
	 * @return Integer codSituacao
	 */
	public Integer getCodSituacao() {
		return codSituacao;
	}

	/**
	 * Nome: setCodSituacao
	 * <p>Prop�sito: M�todo set para o atributo codSituacao. </p>
	 * @param Integer codSituacao
	 */
	public void setCodSituacao(Integer codSituacao) {
		this.codSituacao = codSituacao;
	}

	

	/**
	 * Nome: getCodTipoUorg
	 * <p>Prop�sito: M�todo get para o atributo codTipoUorg. </p>
	 * @return Integer codTipoUorg
	 */
	public Integer getCodTipoUorg() {
		return codTipoUorg;
	}

	/**
	 * Nome: setCodTipoUorg
	 * <p>Prop�sito: M�todo set para o atributo codTipoUorg. </p>
	 * @param Integer codTipoUorg
	 */
	public void setCodTipoUorg(Integer codTipoUorg) {
		this.codTipoUorg = codTipoUorg;
	}

	/**
	 * Nome: getCodWorkFlow
	 * <p>Prop�sito: M�todo get para o atributo codWorkFlow. </p>
	 * @return String codWorkFlow
	 */
	public String getCodWorkFlow() {
		return codWorkFlow;
	}

	/**
	 * Nome: setCodWorkFlow
	 * <p>Prop�sito: M�todo set para o atributo codWorkFlow. </p>
	 * @param String codWorkFlow
	 */
	public void setCodWorkFlow(String codWorkFlow) {
		this.codWorkFlow = codWorkFlow;
	}

	/**
	 * Nome: getDataAtivacaoRegra
	 * <p>Prop�sito: M�todo get para o atributo dataAtivacaoRegra. </p>
	 * @return String dataAtivacaoRegra
	 */
	public String getDataAtivacaoRegra() {
		return dataAtivacaoRegra;
	}

	/**
	 * Nome: setDataAtivacaoRegra
	 * <p>Prop�sito: M�todo set para o atributo dataAtivacaoRegra. </p>
	 * @param String dataAtivacaoRegra
	 */
	public void setDataAtivacaoRegra(String dataAtivacaoRegra) {
		this.dataAtivacaoRegra = dataAtivacaoRegra;
	}

	/**
	 * Nome: getDataPesquisaFim
	 * <p>Prop�sito: M�todo get para o atributo dataPesquisaFim. </p>
	 * @return String dataPesquisaFim
	 */
	public String getDataPesquisaFim() {
		return dataPesquisaFim;
	}

	/**
	 * Nome: setDataPesquisaFim
	 * <p>Prop�sito: M�todo set para o atributo dataPesquisaFim. </p>
	 * @param String dataPesquisaFim
	 */
	public void setDataPesquisaFim(String dataPesquisaFim) {
		this.dataPesquisaFim = dataPesquisaFim;
	}

	/**
	 * Nome: getDataPesquisaInic
	 * <p>Prop�sito: M�todo get para o atributo dataPesquisaInic. </p>
	 * @return String dataPesquisaInic
	 */
	public String getDataPesquisaInic() {
		return dataPesquisaInic;
	}

	/**
	 * Nome: setDataPesquisaInic
	 * <p>Prop�sito: M�todo set para o atributo dataPesquisaInic. </p>
	 * @param String dataPesquisaInic
	 */
	public void setDataPesquisaInic(String dataPesquisaInic) {
		this.dataPesquisaInic = dataPesquisaInic;
	}

	/**
	 * Nome: getDescDeptoGesto
	 * <p>Prop�sito: M�todo get para o atributo descDeptoGesto. </p>
	 * @return String descDeptoGesto
	 */
	public String getDescDeptoGesto() {
		return descDeptoGesto;
	}

	/**
	 * Nome: setDescDeptoGesto
	 * <p>Prop�sito: M�todo set para o atributo descDeptoGesto. </p>
	 * @param String descDeptoGesto
	 */
	public void setDescDeptoGesto(String descDeptoGesto) {
		this.descDeptoGesto = descDeptoGesto;
	}

	/**
	 * Nome: getDescEmpresa
	 * <p>Prop�sito: M�todo get para o atributo descEmpresa. </p>
	 * @return String descEmpresa
	 */
	public String getDescEmpresa() {
		return descEmpresa;
	}

	/**
	 * Nome: setDescEmpresa
	 * <p>Prop�sito: M�todo set para o atributo descEmpresa. </p>
	 * @param String descEmpresa
	 */
	public void setDescEmpresa(String descEmpresa) {
		this.descEmpresa = descEmpresa;
	}

	/**
	 * Nome: getDescRegraAlcada
	 * <p>Prop�sito: M�todo get para o atributo descRegraAlcada. </p>
	 * @return String descRegraAlcada
	 */
	public String getDescRegraAlcada() {
		return descRegraAlcada;
	}

	/**
	 * Nome: setDescRegraAlcada
	 * <p>Prop�sito: M�todo set para o atributo descRegraAlcada. </p>
	 * @param String descRegraAlcada
	 */
	public void setDescRegraAlcada(String descRegraAlcada) {
		this.descRegraAlcada = descRegraAlcada;
	}

	/**
	 * Nome: getDescSituacao
	 * <p>Prop�sito: M�todo get para o atributo descSituacao. </p>
	 * @return String descSituacao
	 */
	public String getDescSituacao() {
		return descSituacao;
	}

	/**
	 * Nome: setDescSituacao
	 * <p>Prop�sito: M�todo set para o atributo descSituacao. </p>
	 * @param String descSituacao
	 */
	public void setDescSituacao(String descSituacao) {
		this.descSituacao = descSituacao;
	}



	/**
	 * Nome: getIndGestorMaster
	 * <p>Prop�sito: M�todo get para o atributo indGestorMaster. </p>
	 * @return String indGestorMaster
	 */
	public String getIndGestorMaster() {
		return indGestorMaster;
	}

	/**
	 * Nome: setIndGestorMaster
	 * <p>Prop�sito: M�todo set para o atributo indGestorMaster. </p>
	 * @param String indGestorMaster
	 */
	public void setIndGestorMaster(String indGestorMaster) {
		this.indGestorMaster = indGestorMaster;
	}

	/**
	 * Nome: getTipoConsulta
	 * <p>Prop�sito: M�todo get para o atributo tipoConsulta. </p>
	 * @return Integer tipoConsulta
	 */
	public Integer getTipoConsulta() {
		return tipoConsulta;
	}

	/**
	 * Nome: setTipoConsulta
	 * <p>Prop�sito: M�todo set para o atributo tipoConsulta. </p>
	 * @param Integer tipoConsulta
	 */
	public void setTipoConsulta(Integer tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}      
}
