/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean;

/**
 * Nome: AssociacaoLinhasTipoMensagemDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssociacaoLinhasTipoMensagemDTO {
	//Request
	/** Atributo tipoMensagem. */
	private String tipoMensagem;
	
	/** Atributo idioma. */
	private String idioma;
	
	/** Atributo recurso. */
	private String recurso;
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo ordemLinha. */
	private String ordemLinha;
	
	/** Atributo prefixo. */
	private String prefixo;
	
	/**
	 * Get: prefixo.
	 *
	 * @return prefixo
	 */
	public String getPrefixo() {
		return prefixo;
	}

	/**
	 * Set: prefixo.
	 *
	 * @param prefixo the prefixo
	 */
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}

	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: ordemLinha.
	 *
	 * @return ordemLinha
	 */
	public String getOrdemLinha() {
		return ordemLinha;
	}

	/**
	 * Set: ordemLinha.
	 *
	 * @param ordemLinha the ordem linha
	 */
	public void setOrdemLinha(String ordemLinha) {
		this.ordemLinha = ordemLinha;
	}

	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public String getRecurso() {
		return recurso;
	}

	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public String getTipoMensagem() {
		return tipoMensagem;
	}

	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(String tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	
	
	
	
}
