/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarListaPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarListaPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarPagtoPendConsolidadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarPagtoPendConsolidadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConParcialSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: LiberarPagtosConsolidadosPend
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ILiberarPagtosConsolidadosPendService {

    /**
     * Consultar pagto pend consolidado.
     *
     * @param entradaDTO the entrada dto
     * @return the list< consultar pagto pend consolidado saida dt o>
     */
    List<ConsultarPagtoPendConsolidadoSaidaDTO> consultarPagtoPendConsolidado(ConsultarPagtoPendConsolidadoEntradaDTO entradaDTO);
    
    /**
     * Consultar lista pagamento.
     *
     * @param entrada the entrada
     * @return the list< consultar lista pagamento saida dt o>
     */
    List<ConsultarListaPagamentoSaidaDTO> consultarListaPagamento (ConsultarListaPagamentoEntradaDTO entrada); 
    
    /**
     * Liberar pagto pend con integral.
     *
     * @param entrada the entrada
     * @return the liberar pagto pend con integral saida dto
     */
    LiberarPagtoPendConIntegralSaidaDTO liberarPagtoPendConIntegral (LiberarPagtoPendConIntegralEntradaDTO entrada);
    
    /**
     * Liberar pagto pend con parcial.
     *
     * @param listaEntrada the lista entrada
     * @return the liberar pagto pend con parcial saida dto
     */
    LiberarPagtoPendConParcialSaidaDTO liberarPagtoPendConParcial (List<LiberarPagtoPendConParcialEntradaDTO> listaEntrada);
    
    /**
     * Liberar pendencia pagto con parcial.
     *
     * @param listaEntrada the lista entrada
     * @return the liberar pendencia pagto con parcial saida dto
     */
    LiberarPendenciaPagtoConParcialSaidaDTO liberarPendenciaPagtoConParcial(List<LiberarPendenciaPagtoConParcialEntradaDTO> listaEntrada);
    
    /**
     * Liberar pendencia pagto con integral.
     *
     * @param entrada the entrada
     * @return the liberar pendencia pagto con integral saida dto
     */
    LiberarPendenciaPagtoConIntegralSaidaDTO liberarPendenciaPagtoConIntegral (LiberarPendenciaPagtoConIntegralEntradaDTO entrada);
}

