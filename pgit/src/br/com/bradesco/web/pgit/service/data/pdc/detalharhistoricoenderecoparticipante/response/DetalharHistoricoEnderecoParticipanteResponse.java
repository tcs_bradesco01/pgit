/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistoricoEnderecoParticipanteResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistoricoEnderecoParticipanteResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdEspeceEndereco
     */
    private int _cdEspeceEndereco = 0;

    /**
     * keeps track of state for field: _cdEspeceEndereco
     */
    private boolean _has_cdEspeceEndereco;

    /**
     * Field _dsEspecieEndereco
     */
    private java.lang.String _dsEspecieEndereco;

    /**
     * Field _cdNomeRazao
     */
    private java.lang.String _cdNomeRazao;

    /**
     * Field _dsLogradouroPagador
     */
    private java.lang.String _dsLogradouroPagador;

    /**
     * Field _dsNumeroLogradouroPagador
     */
    private java.lang.String _dsNumeroLogradouroPagador;

    /**
     * Field _dsComplementoLogradouroPagador
     */
    private java.lang.String _dsComplementoLogradouroPagador;

    /**
     * Field _dsBairroClientePagador
     */
    private java.lang.String _dsBairroClientePagador;

    /**
     * Field _dsMunicipioClientePagador
     */
    private java.lang.String _dsMunicipioClientePagador;

    /**
     * Field _cdSiglaUfPagador
     */
    private java.lang.String _cdSiglaUfPagador;

    /**
     * Field _dsPais
     */
    private java.lang.String _dsPais;

    /**
     * Field _cdCepPagador
     */
    private int _cdCepPagador = 0;

    /**
     * keeps track of state for field: _cdCepPagador
     */
    private boolean _has_cdCepPagador;

    /**
     * Field _cdCepComplementoPagador
     */
    private int _cdCepComplementoPagador = 0;

    /**
     * keeps track of state for field: _cdCepComplementoPagador
     */
    private boolean _has_cdCepComplementoPagador;

    /**
     * Field _dsCpfCnpjRecebedor
     */
    private long _dsCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _dsCpfCnpjRecebedor
     */
    private boolean _has_dsCpfCnpjRecebedor;

    /**
     * Field _dsFilialCnpjRecebedor
     */
    private int _dsFilialCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _dsFilialCnpjRecebedor
     */
    private boolean _has_dsFilialCnpjRecebedor;

    /**
     * Field _dsControleCpfRecebedor
     */
    private int _dsControleCpfRecebedor = 0;

    /**
     * keeps track of state for field: _dsControleCpfRecebedor
     */
    private boolean _has_dsControleCpfRecebedor;

    /**
     * Field _dsNomeRazao
     */
    private java.lang.String _dsNomeRazao;

    /**
     * Field _cdEnderecoEletronico
     */
    private java.lang.String _cdEnderecoEletronico;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoexterno
     */
    private java.lang.String _cdUsuarioManutencaoexterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _cdTipo
     */
    private java.lang.String _cdTipo;

    /**
     * Field _cdUsoPostal
     */
    private int _cdUsoPostal = 0;

    /**
     * keeps track of state for field: _cdUsoPostal
     */
    private boolean _has_cdUsoPostal;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistoricoEnderecoParticipanteResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.response.DetalharHistoricoEnderecoParticipanteResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCepComplementoPagador
     * 
     */
    public void deleteCdCepComplementoPagador()
    {
        this._has_cdCepComplementoPagador= false;
    } //-- void deleteCdCepComplementoPagador() 

    /**
     * Method deleteCdCepPagador
     * 
     */
    public void deleteCdCepPagador()
    {
        this._has_cdCepPagador= false;
    } //-- void deleteCdCepPagador() 

    /**
     * Method deleteCdEspeceEndereco
     * 
     */
    public void deleteCdEspeceEndereco()
    {
        this._has_cdEspeceEndereco= false;
    } //-- void deleteCdEspeceEndereco() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdUsoPostal
     * 
     */
    public void deleteCdUsoPostal()
    {
        this._has_cdUsoPostal= false;
    } //-- void deleteCdUsoPostal() 

    /**
     * Method deleteDsControleCpfRecebedor
     * 
     */
    public void deleteDsControleCpfRecebedor()
    {
        this._has_dsControleCpfRecebedor= false;
    } //-- void deleteDsControleCpfRecebedor() 

    /**
     * Method deleteDsCpfCnpjRecebedor
     * 
     */
    public void deleteDsCpfCnpjRecebedor()
    {
        this._has_dsCpfCnpjRecebedor= false;
    } //-- void deleteDsCpfCnpjRecebedor() 

    /**
     * Method deleteDsFilialCnpjRecebedor
     * 
     */
    public void deleteDsFilialCnpjRecebedor()
    {
        this._has_dsFilialCnpjRecebedor= false;
    } //-- void deleteDsFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'cdCepComplementoPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepComplementoPagador'.
     */
    public int getCdCepComplementoPagador()
    {
        return this._cdCepComplementoPagador;
    } //-- int getCdCepComplementoPagador() 

    /**
     * Returns the value of field 'cdCepPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepPagador'.
     */
    public int getCdCepPagador()
    {
        return this._cdCepPagador;
    } //-- int getCdCepPagador() 

    /**
     * Returns the value of field 'cdEnderecoEletronico'.
     * 
     * @return String
     * @return the value of field 'cdEnderecoEletronico'.
     */
    public java.lang.String getCdEnderecoEletronico()
    {
        return this._cdEnderecoEletronico;
    } //-- java.lang.String getCdEnderecoEletronico() 

    /**
     * Returns the value of field 'cdEspeceEndereco'.
     * 
     * @return int
     * @return the value of field 'cdEspeceEndereco'.
     */
    public int getCdEspeceEndereco()
    {
        return this._cdEspeceEndereco;
    } //-- int getCdEspeceEndereco() 

    /**
     * Returns the value of field 'cdNomeRazao'.
     * 
     * @return String
     * @return the value of field 'cdNomeRazao'.
     */
    public java.lang.String getCdNomeRazao()
    {
        return this._cdNomeRazao;
    } //-- java.lang.String getCdNomeRazao() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdSiglaUfPagador'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUfPagador'.
     */
    public java.lang.String getCdSiglaUfPagador()
    {
        return this._cdSiglaUfPagador;
    } //-- java.lang.String getCdSiglaUfPagador() 

    /**
     * Returns the value of field 'cdTipo'.
     * 
     * @return String
     * @return the value of field 'cdTipo'.
     */
    public java.lang.String getCdTipo()
    {
        return this._cdTipo;
    } //-- java.lang.String getCdTipo() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdUsoPostal'.
     * 
     * @return int
     * @return the value of field 'cdUsoPostal'.
     */
    public int getCdUsoPostal()
    {
        return this._cdUsoPostal;
    } //-- int getCdUsoPostal() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoexterno'.
     */
    public java.lang.String getCdUsuarioManutencaoexterno()
    {
        return this._cdUsuarioManutencaoexterno;
    } //-- java.lang.String getCdUsuarioManutencaoexterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsBairroClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsBairroClientePagador'.
     */
    public java.lang.String getDsBairroClientePagador()
    {
        return this._dsBairroClientePagador;
    } //-- java.lang.String getDsBairroClientePagador() 

    /**
     * Returns the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsComplementoLogradouroPagador'.
     */
    public java.lang.String getDsComplementoLogradouroPagador()
    {
        return this._dsComplementoLogradouroPagador;
    } //-- java.lang.String getDsComplementoLogradouroPagador() 

    /**
     * Returns the value of field 'dsControleCpfRecebedor'.
     * 
     * @return int
     * @return the value of field 'dsControleCpfRecebedor'.
     */
    public int getDsControleCpfRecebedor()
    {
        return this._dsControleCpfRecebedor;
    } //-- int getDsControleCpfRecebedor() 

    /**
     * Returns the value of field 'dsCpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'dsCpfCnpjRecebedor'.
     */
    public long getDsCpfCnpjRecebedor()
    {
        return this._dsCpfCnpjRecebedor;
    } //-- long getDsCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'dsEspecieEndereco'.
     * 
     * @return String
     * @return the value of field 'dsEspecieEndereco'.
     */
    public java.lang.String getDsEspecieEndereco()
    {
        return this._dsEspecieEndereco;
    } //-- java.lang.String getDsEspecieEndereco() 

    /**
     * Returns the value of field 'dsFilialCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'dsFilialCnpjRecebedor'.
     */
    public int getDsFilialCnpjRecebedor()
    {
        return this._dsFilialCnpjRecebedor;
    } //-- int getDsFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'dsLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroPagador'.
     */
    public java.lang.String getDsLogradouroPagador()
    {
        return this._dsLogradouroPagador;
    } //-- java.lang.String getDsLogradouroPagador() 

    /**
     * Returns the value of field 'dsMunicipioClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioClientePagador'.
     */
    public java.lang.String getDsMunicipioClientePagador()
    {
        return this._dsMunicipioClientePagador;
    } //-- java.lang.String getDsMunicipioClientePagador() 

    /**
     * Returns the value of field 'dsNomeRazao'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazao'.
     */
    public java.lang.String getDsNomeRazao()
    {
        return this._dsNomeRazao;
    } //-- java.lang.String getDsNomeRazao() 

    /**
     * Returns the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsNumeroLogradouroPagador'.
     */
    public java.lang.String getDsNumeroLogradouroPagador()
    {
        return this._dsNumeroLogradouroPagador;
    } //-- java.lang.String getDsNumeroLogradouroPagador() 

    /**
     * Returns the value of field 'dsPais'.
     * 
     * @return String
     * @return the value of field 'dsPais'.
     */
    public java.lang.String getDsPais()
    {
        return this._dsPais;
    } //-- java.lang.String getDsPais() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdCepComplementoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplementoPagador()
    {
        return this._has_cdCepComplementoPagador;
    } //-- boolean hasCdCepComplementoPagador() 

    /**
     * Method hasCdCepPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepPagador()
    {
        return this._has_cdCepPagador;
    } //-- boolean hasCdCepPagador() 

    /**
     * Method hasCdEspeceEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEspeceEndereco()
    {
        return this._has_cdEspeceEndereco;
    } //-- boolean hasCdEspeceEndereco() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdUsoPostal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUsoPostal()
    {
        return this._has_cdUsoPostal;
    } //-- boolean hasCdUsoPostal() 

    /**
     * Method hasDsControleCpfRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsControleCpfRecebedor()
    {
        return this._has_dsControleCpfRecebedor;
    } //-- boolean hasDsControleCpfRecebedor() 

    /**
     * Method hasDsCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsCpfCnpjRecebedor()
    {
        return this._has_dsCpfCnpjRecebedor;
    } //-- boolean hasDsCpfCnpjRecebedor() 

    /**
     * Method hasDsFilialCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsFilialCnpjRecebedor()
    {
        return this._has_dsFilialCnpjRecebedor;
    } //-- boolean hasDsFilialCnpjRecebedor() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCepComplementoPagador'.
     * 
     * @param cdCepComplementoPagador the value of field
     * 'cdCepComplementoPagador'.
     */
    public void setCdCepComplementoPagador(int cdCepComplementoPagador)
    {
        this._cdCepComplementoPagador = cdCepComplementoPagador;
        this._has_cdCepComplementoPagador = true;
    } //-- void setCdCepComplementoPagador(int) 

    /**
     * Sets the value of field 'cdCepPagador'.
     * 
     * @param cdCepPagador the value of field 'cdCepPagador'.
     */
    public void setCdCepPagador(int cdCepPagador)
    {
        this._cdCepPagador = cdCepPagador;
        this._has_cdCepPagador = true;
    } //-- void setCdCepPagador(int) 

    /**
     * Sets the value of field 'cdEnderecoEletronico'.
     * 
     * @param cdEnderecoEletronico the value of field
     * 'cdEnderecoEletronico'.
     */
    public void setCdEnderecoEletronico(java.lang.String cdEnderecoEletronico)
    {
        this._cdEnderecoEletronico = cdEnderecoEletronico;
    } //-- void setCdEnderecoEletronico(java.lang.String) 

    /**
     * Sets the value of field 'cdEspeceEndereco'.
     * 
     * @param cdEspeceEndereco the value of field 'cdEspeceEndereco'
     */
    public void setCdEspeceEndereco(int cdEspeceEndereco)
    {
        this._cdEspeceEndereco = cdEspeceEndereco;
        this._has_cdEspeceEndereco = true;
    } //-- void setCdEspeceEndereco(int) 

    /**
     * Sets the value of field 'cdNomeRazao'.
     * 
     * @param cdNomeRazao the value of field 'cdNomeRazao'.
     */
    public void setCdNomeRazao(java.lang.String cdNomeRazao)
    {
        this._cdNomeRazao = cdNomeRazao;
    } //-- void setCdNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdSiglaUfPagador'.
     * 
     * @param cdSiglaUfPagador the value of field 'cdSiglaUfPagador'
     */
    public void setCdSiglaUfPagador(java.lang.String cdSiglaUfPagador)
    {
        this._cdSiglaUfPagador = cdSiglaUfPagador;
    } //-- void setCdSiglaUfPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdTipo'.
     * 
     * @param cdTipo the value of field 'cdTipo'.
     */
    public void setCdTipo(java.lang.String cdTipo)
    {
        this._cdTipo = cdTipo;
    } //-- void setCdTipo(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdUsoPostal'.
     * 
     * @param cdUsoPostal the value of field 'cdUsoPostal'.
     */
    public void setCdUsoPostal(int cdUsoPostal)
    {
        this._cdUsoPostal = cdUsoPostal;
        this._has_cdUsoPostal = true;
    } //-- void setCdUsoPostal(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @param cdUsuarioManutencaoexterno the value of field
     * 'cdUsuarioManutencaoexterno'.
     */
    public void setCdUsuarioManutencaoexterno(java.lang.String cdUsuarioManutencaoexterno)
    {
        this._cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
    } //-- void setCdUsuarioManutencaoexterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroClientePagador'.
     * 
     * @param dsBairroClientePagador the value of field
     * 'dsBairroClientePagador'.
     */
    public void setDsBairroClientePagador(java.lang.String dsBairroClientePagador)
    {
        this._dsBairroClientePagador = dsBairroClientePagador;
    } //-- void setDsBairroClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @param dsComplementoLogradouroPagador the value of field
     * 'dsComplementoLogradouroPagador'.
     */
    public void setDsComplementoLogradouroPagador(java.lang.String dsComplementoLogradouroPagador)
    {
        this._dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
    } //-- void setDsComplementoLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsControleCpfRecebedor'.
     * 
     * @param dsControleCpfRecebedor the value of field
     * 'dsControleCpfRecebedor'.
     */
    public void setDsControleCpfRecebedor(int dsControleCpfRecebedor)
    {
        this._dsControleCpfRecebedor = dsControleCpfRecebedor;
        this._has_dsControleCpfRecebedor = true;
    } //-- void setDsControleCpfRecebedor(int) 

    /**
     * Sets the value of field 'dsCpfCnpjRecebedor'.
     * 
     * @param dsCpfCnpjRecebedor the value of field
     * 'dsCpfCnpjRecebedor'.
     */
    public void setDsCpfCnpjRecebedor(long dsCpfCnpjRecebedor)
    {
        this._dsCpfCnpjRecebedor = dsCpfCnpjRecebedor;
        this._has_dsCpfCnpjRecebedor = true;
    } //-- void setDsCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'dsEspecieEndereco'.
     * 
     * @param dsEspecieEndereco the value of field
     * 'dsEspecieEndereco'.
     */
    public void setDsEspecieEndereco(java.lang.String dsEspecieEndereco)
    {
        this._dsEspecieEndereco = dsEspecieEndereco;
    } //-- void setDsEspecieEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsFilialCnpjRecebedor'.
     * 
     * @param dsFilialCnpjRecebedor the value of field
     * 'dsFilialCnpjRecebedor'.
     */
    public void setDsFilialCnpjRecebedor(int dsFilialCnpjRecebedor)
    {
        this._dsFilialCnpjRecebedor = dsFilialCnpjRecebedor;
        this._has_dsFilialCnpjRecebedor = true;
    } //-- void setDsFilialCnpjRecebedor(int) 

    /**
     * Sets the value of field 'dsLogradouroPagador'.
     * 
     * @param dsLogradouroPagador the value of field
     * 'dsLogradouroPagador'.
     */
    public void setDsLogradouroPagador(java.lang.String dsLogradouroPagador)
    {
        this._dsLogradouroPagador = dsLogradouroPagador;
    } //-- void setDsLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioClientePagador'.
     * 
     * @param dsMunicipioClientePagador the value of field
     * 'dsMunicipioClientePagador'.
     */
    public void setDsMunicipioClientePagador(java.lang.String dsMunicipioClientePagador)
    {
        this._dsMunicipioClientePagador = dsMunicipioClientePagador;
    } //-- void setDsMunicipioClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeRazao'.
     * 
     * @param dsNomeRazao the value of field 'dsNomeRazao'.
     */
    public void setDsNomeRazao(java.lang.String dsNomeRazao)
    {
        this._dsNomeRazao = dsNomeRazao;
    } //-- void setDsNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @param dsNumeroLogradouroPagador the value of field
     * 'dsNumeroLogradouroPagador'.
     */
    public void setDsNumeroLogradouroPagador(java.lang.String dsNumeroLogradouroPagador)
    {
        this._dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
    } //-- void setDsNumeroLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsPais'.
     * 
     * @param dsPais the value of field 'dsPais'.
     */
    public void setDsPais(java.lang.String dsPais)
    {
        this._dsPais = dsPais;
    } //-- void setDsPais(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistoricoEnderecoParticipanteResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.response.DetalharHistoricoEnderecoParticipanteResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.response.DetalharHistoricoEnderecoParticipanteResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.response.DetalharHistoricoEnderecoParticipanteResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.response.DetalharHistoricoEnderecoParticipanteResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
