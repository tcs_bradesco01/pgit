/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ConsultarConManContaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConManContaEntradaDTO {    
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdPessoaJuridicaConta. */
    private Long cdPessoaJuridicaConta;
    
    /** Atributo cdTipoContratoConta. */
    private Integer cdTipoContratoConta;
    
    /** Atributo nrSequenciaContratoConta. */
    private Long nrSequenciaContratoConta;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdTipoVinculoContrato. */
    private Integer cdTipoVinculoContrato;    

	/**
	 * Get: cdPessoaJuridicaConta.
	 *
	 * @return cdPessoaJuridicaConta
	 */
	public Long getCdPessoaJuridicaConta() {
		return cdPessoaJuridicaConta;
	}
	
	/**
	 * Set: cdPessoaJuridicaConta.
	 *
	 * @param cdPessoaJuridicaConta the cd pessoa juridica conta
	 */
	public void setCdPessoaJuridicaConta(Long cdPessoaJuridicaConta) {
		this.cdPessoaJuridicaConta = cdPessoaJuridicaConta;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoConta.
	 *
	 * @return cdTipoContratoConta
	 */
	public Integer getCdTipoContratoConta() {
		return cdTipoContratoConta;
	}
	
	/**
	 * Set: cdTipoContratoConta.
	 *
	 * @param cdTipoContratoConta the cd tipo contrato conta
	 */
	public void setCdTipoContratoConta(Integer cdTipoContratoConta) {
		this.cdTipoContratoConta = cdTipoContratoConta;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	
	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: nrSequenciaContratoConta.
	 *
	 * @return nrSequenciaContratoConta
	 */
	public Long getNrSequenciaContratoConta() {
		return nrSequenciaContratoConta;
	}
	
	/**
	 * Set: nrSequenciaContratoConta.
	 *
	 * @param nrSequenciaContratoConta the nr sequencia contrato conta
	 */
	public void setNrSequenciaContratoConta(Long nrSequenciaContratoConta) {
		this.nrSequenciaContratoConta = nrSequenciaContratoConta;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}    	
}
