/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dtAgendamentoPagamento
     */
    private java.lang.String _dtAgendamentoPagamento;

    /**
     * Field _vlLimiteDiaUtilizado
     */
    private java.math.BigDecimal _vlLimiteDiaUtilizado = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlLimiteDiaUtilizado(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'dtAgendamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtAgendamentoPagamento'.
     */
    public java.lang.String getDtAgendamentoPagamento()
    {
        return this._dtAgendamentoPagamento;
    } //-- java.lang.String getDtAgendamentoPagamento() 

    /**
     * Returns the value of field 'vlLimiteDiaUtilizado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteDiaUtilizado'.
     */
    public java.math.BigDecimal getVlLimiteDiaUtilizado()
    {
        return this._vlLimiteDiaUtilizado;
    } //-- java.math.BigDecimal getVlLimiteDiaUtilizado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dtAgendamentoPagamento'.
     * 
     * @param dtAgendamentoPagamento the value of field
     * 'dtAgendamentoPagamento'.
     */
    public void setDtAgendamentoPagamento(java.lang.String dtAgendamentoPagamento)
    {
        this._dtAgendamentoPagamento = dtAgendamentoPagamento;
    } //-- void setDtAgendamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'vlLimiteDiaUtilizado'.
     * 
     * @param vlLimiteDiaUtilizado the value of field
     * 'vlLimiteDiaUtilizado'.
     */
    public void setVlLimiteDiaUtilizado(java.math.BigDecimal vlLimiteDiaUtilizado)
    {
        this._vlLimiteDiaUtilizado = vlLimiteDiaUtilizado;
    } //-- void setVlLimiteDiaUtilizado(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
