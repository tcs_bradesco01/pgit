/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarempresaconglomerado.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrCnpj
     */
    private long _nrCnpj = 0;

    /**
     * keeps track of state for field: _nrCnpj
     */
    private boolean _has_nrCnpj;

    /**
     * Field _nrFilialCnpj
     */
    private int _nrFilialCnpj = 0;

    /**
     * keeps track of state for field: _nrFilialCnpj
     */
    private boolean _has_nrFilialCnpj;

    /**
     * Field _nrDigitoCnpj
     */
    private int _nrDigitoCnpj = 0;

    /**
     * keeps track of state for field: _nrDigitoCnpj
     */
    private boolean _has_nrDigitoCnpj;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarempresaconglomerado.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteNrCnpj
     * 
     */
    public void deleteNrCnpj()
    {
        this._has_nrCnpj= false;
    } //-- void deleteNrCnpj() 

    /**
     * Method deleteNrDigitoCnpj
     * 
     */
    public void deleteNrDigitoCnpj()
    {
        this._has_nrDigitoCnpj= false;
    } //-- void deleteNrDigitoCnpj() 

    /**
     * Method deleteNrFilialCnpj
     * 
     */
    public void deleteNrFilialCnpj()
    {
        this._has_nrFilialCnpj= false;
    } //-- void deleteNrFilialCnpj() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'nrCnpj'.
     * 
     * @return long
     * @return the value of field 'nrCnpj'.
     */
    public long getNrCnpj()
    {
        return this._nrCnpj;
    } //-- long getNrCnpj() 

    /**
     * Returns the value of field 'nrDigitoCnpj'.
     * 
     * @return int
     * @return the value of field 'nrDigitoCnpj'.
     */
    public int getNrDigitoCnpj()
    {
        return this._nrDigitoCnpj;
    } //-- int getNrDigitoCnpj() 

    /**
     * Returns the value of field 'nrFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'nrFilialCnpj'.
     */
    public int getNrFilialCnpj()
    {
        return this._nrFilialCnpj;
    } //-- int getNrFilialCnpj() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasNrCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrCnpj()
    {
        return this._has_nrCnpj;
    } //-- boolean hasNrCnpj() 

    /**
     * Method hasNrDigitoCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrDigitoCnpj()
    {
        return this._has_nrDigitoCnpj;
    } //-- boolean hasNrDigitoCnpj() 

    /**
     * Method hasNrFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFilialCnpj()
    {
        return this._has_nrFilialCnpj;
    } //-- boolean hasNrFilialCnpj() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'nrCnpj'.
     * 
     * @param nrCnpj the value of field 'nrCnpj'.
     */
    public void setNrCnpj(long nrCnpj)
    {
        this._nrCnpj = nrCnpj;
        this._has_nrCnpj = true;
    } //-- void setNrCnpj(long) 

    /**
     * Sets the value of field 'nrDigitoCnpj'.
     * 
     * @param nrDigitoCnpj the value of field 'nrDigitoCnpj'.
     */
    public void setNrDigitoCnpj(int nrDigitoCnpj)
    {
        this._nrDigitoCnpj = nrDigitoCnpj;
        this._has_nrDigitoCnpj = true;
    } //-- void setNrDigitoCnpj(int) 

    /**
     * Sets the value of field 'nrFilialCnpj'.
     * 
     * @param nrFilialCnpj the value of field 'nrFilialCnpj'.
     */
    public void setNrFilialCnpj(int nrFilialCnpj)
    {
        this._nrFilialCnpj = nrFilialCnpj;
        this._has_nrFilialCnpj = true;
    } //-- void setNrFilialCnpj(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarempresaconglomerado.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarempresaconglomerado.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarempresaconglomerado.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarempresaconglomerado.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
