/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: IncluirEmissaoAutCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirEmissaoAutCompEntradaDTO {
	
	/** Atributo contratoPssoaJuridContr. */
	private long contratoPssoaJuridContr;
	
	/** Atributo contratoTpoContrNegoc. */
	private int contratoTpoContrNegoc;
	
	/** Atributo contratoSegContrNegoc. */
	private long contratoSegContrNegoc;
	
	/** Atributo servicoEmissaoContratado. */
	private int servicoEmissaoContratado;
	
	/** Atributo quantidadeOcorrenciasEnviadasInc. */
	private int quantidadeOcorrenciasEnviadasInc;
	
	/** Atributo listaIncluir. */
	private List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> listaIncluir = new ArrayList<ListarTipoServicoModalidadeOcorrenciaSaidaDTO>();
	
	/**
	 * Get: contratoPssoaJuridContr.
	 *
	 * @return contratoPssoaJuridContr
	 */
	public long getContratoPssoaJuridContr() {
		return contratoPssoaJuridContr;
	}
	
	/**
	 * Set: contratoPssoaJuridContr.
	 *
	 * @param contratoPssoaJuridContr the contrato pssoa jurid contr
	 */
	public void setContratoPssoaJuridContr(long contratoPssoaJuridContr) {
		this.contratoPssoaJuridContr = contratoPssoaJuridContr;
	}
	
	/**
	 * Get: contratoSegContrNegoc.
	 *
	 * @return contratoSegContrNegoc
	 */
	public long getContratoSegContrNegoc() {
		return contratoSegContrNegoc;
	}
	
	/**
	 * Set: contratoSegContrNegoc.
	 *
	 * @param contratoSegContrNegoc the contrato seg contr negoc
	 */
	public void setContratoSegContrNegoc(long contratoSegContrNegoc) {
		this.contratoSegContrNegoc = contratoSegContrNegoc;
	}
	
	/**
	 * Get: contratoTpoContrNegoc.
	 *
	 * @return contratoTpoContrNegoc
	 */
	public int getContratoTpoContrNegoc() {
		return contratoTpoContrNegoc;
	}
	
	/**
	 * Set: contratoTpoContrNegoc.
	 *
	 * @param contratoTpoContrNegoc the contrato tpo contr negoc
	 */
	public void setContratoTpoContrNegoc(int contratoTpoContrNegoc) {
		this.contratoTpoContrNegoc = contratoTpoContrNegoc;
	}

	/**
	 * Get: quantidadeOcorrenciasEnviadasInc.
	 *
	 * @return quantidadeOcorrenciasEnviadasInc
	 */
	public int getQuantidadeOcorrenciasEnviadasInc() {
		return quantidadeOcorrenciasEnviadasInc;
	}
	
	/**
	 * Set: quantidadeOcorrenciasEnviadasInc.
	 *
	 * @param quantidadeOcorrenciasEnviadasInc the quantidade ocorrencias enviadas inc
	 */
	public void setQuantidadeOcorrenciasEnviadasInc(
			int quantidadeOcorrenciasEnviadasInc) {
		this.quantidadeOcorrenciasEnviadasInc = quantidadeOcorrenciasEnviadasInc;
	}
	
	/**
	 * Get: servicoEmissaoContratado.
	 *
	 * @return servicoEmissaoContratado
	 */
	public int getServicoEmissaoContratado() {
		return servicoEmissaoContratado;
	}
	
	/**
	 * Set: servicoEmissaoContratado.
	 *
	 * @param servicoEmissaoContratado the servico emissao contratado
	 */
	public void setServicoEmissaoContratado(int servicoEmissaoContratado) {
		this.servicoEmissaoContratado = servicoEmissaoContratado;
	}
	
	/**
	 * Get: listaIncluir.
	 *
	 * @return listaIncluir
	 */
	public List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> getListaIncluir() {
		return listaIncluir;
	}
	
	/**
	 * Set: listaIncluir.
	 *
	 * @param listaIncluir the lista incluir
	 */
	public void setListaIncluir(List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> listaIncluir) {
		this.listaIncluir = listaIncluir;
	}

	
}
