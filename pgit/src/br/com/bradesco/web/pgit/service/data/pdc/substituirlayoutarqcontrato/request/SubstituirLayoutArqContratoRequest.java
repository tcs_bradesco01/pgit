/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.substituirlayoutarqcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class SubstituirLayoutArqContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class SubstituirLayoutArqContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdTipoLayoutArquivoNovo
     */
    private int _cdTipoLayoutArquivoNovo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivoNovo
     */
    private boolean _has_cdTipoLayoutArquivoNovo;

    /**
     * Field _cdNivelControleRemessa
     */
    private int _cdNivelControleRemessa = 0;

    /**
     * keeps track of state for field: _cdNivelControleRemessa
     */
    private boolean _has_cdNivelControleRemessa;

    /**
     * Field _cdControleNumeroRemessa
     */
    private int _cdControleNumeroRemessa = 0;

    /**
     * keeps track of state for field: _cdControleNumeroRemessa
     */
    private boolean _has_cdControleNumeroRemessa;

    /**
     * Field _cdPerdcContagemRemessa
     */
    private int _cdPerdcContagemRemessa = 0;

    /**
     * keeps track of state for field: _cdPerdcContagemRemessa
     */
    private boolean _has_cdPerdcContagemRemessa;

    /**
     * Field _nrMaximoContagemRemessa
     */
    private long _nrMaximoContagemRemessa = 0;

    /**
     * keeps track of state for field: _nrMaximoContagemRemessa
     */
    private boolean _has_nrMaximoContagemRemessa;

    /**
     * Field _cdRejeicaoAcolhimentoRemessa
     */
    private int _cdRejeicaoAcolhimentoRemessa = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAcolhimentoRemessa
     */
    private boolean _has_cdRejeicaoAcolhimentoRemessa;

    /**
     * Field _cdPercentualRejeicaoRemessa
     */
    private java.math.BigDecimal _cdPercentualRejeicaoRemessa = new java.math.BigDecimal("0");

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _dsEmpresaTratamentoArquivo
     */
    private java.lang.String _dsEmpresaTratamentoArquivo;

    /**
     * Field _cdAplicacaoTransmissaoArquivo
     */
    private int _cdAplicacaoTransmissaoArquivo = 0;

    /**
     * keeps track of state for field: _cdAplicacaoTransmissaoArquiv
     */
    private boolean _has_cdAplicacaoTransmissaoArquivo;

    /**
     * Field _cdSerieAplicacaoTransmissao
     */
    private java.lang.String _cdSerieAplicacaoTransmissao;

    /**
     * Field _cdMeioPrincipalRetorno
     */
    private int _cdMeioPrincipalRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRetorno
     */
    private boolean _has_cdMeioPrincipalRetorno;

    /**
     * Field _cdMeioAlternativoRetorno
     */
    private int _cdMeioAlternativoRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioAlternativoRetorno
     */
    private boolean _has_cdMeioAlternativoRetorno;

    /**
     * Field _cdmeioAlternativoRemessa
     */
    private int _cdmeioAlternativoRemessa = 0;

    /**
     * keeps track of state for field: _cdmeioAlternativoRemessa
     */
    private boolean _has_cdmeioAlternativoRemessa;

    /**
     * Field _cdMeioPrincipalRemessa
     */
    private int _cdMeioPrincipalRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRemessa
     */
    private boolean _has_cdMeioPrincipalRemessa;

    /**
     * Field _dsNomeCliente
     */
    private java.lang.String _dsNomeCliente;

    /**
     * Field _dsNomeArquivoRemessa
     */
    private java.lang.String _dsNomeArquivoRemessa;

    /**
     * Field _dsNomeArquivoRetorno
     */
    private java.lang.String _dsNomeArquivoRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public SubstituirLayoutArqContratoRequest() 
     {
        super();
        setCdPercentualRejeicaoRemessa(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirlayoutarqcontrato.request.SubstituirLayoutArqContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplicacaoTransmissaoArquivo
     * 
     */
    public void deleteCdAplicacaoTransmissaoArquivo()
    {
        this._has_cdAplicacaoTransmissaoArquivo= false;
    } //-- void deleteCdAplicacaoTransmissaoArquivo() 

    /**
     * Method deleteCdControleNumeroRemessa
     * 
     */
    public void deleteCdControleNumeroRemessa()
    {
        this._has_cdControleNumeroRemessa= false;
    } //-- void deleteCdControleNumeroRemessa() 

    /**
     * Method deleteCdMeioAlternativoRetorno
     * 
     */
    public void deleteCdMeioAlternativoRetorno()
    {
        this._has_cdMeioAlternativoRetorno= false;
    } //-- void deleteCdMeioAlternativoRetorno() 

    /**
     * Method deleteCdMeioPrincipalRemessa
     * 
     */
    public void deleteCdMeioPrincipalRemessa()
    {
        this._has_cdMeioPrincipalRemessa= false;
    } //-- void deleteCdMeioPrincipalRemessa() 

    /**
     * Method deleteCdMeioPrincipalRetorno
     * 
     */
    public void deleteCdMeioPrincipalRetorno()
    {
        this._has_cdMeioPrincipalRetorno= false;
    } //-- void deleteCdMeioPrincipalRetorno() 

    /**
     * Method deleteCdNivelControleRemessa
     * 
     */
    public void deleteCdNivelControleRemessa()
    {
        this._has_cdNivelControleRemessa= false;
    } //-- void deleteCdNivelControleRemessa() 

    /**
     * Method deleteCdPerdcContagemRemessa
     * 
     */
    public void deleteCdPerdcContagemRemessa()
    {
        this._has_cdPerdcContagemRemessa= false;
    } //-- void deleteCdPerdcContagemRemessa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdRejeicaoAcolhimentoRemessa
     * 
     */
    public void deleteCdRejeicaoAcolhimentoRemessa()
    {
        this._has_cdRejeicaoAcolhimentoRemessa= false;
    } //-- void deleteCdRejeicaoAcolhimentoRemessa() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoLayoutArquivoNovo
     * 
     */
    public void deleteCdTipoLayoutArquivoNovo()
    {
        this._has_cdTipoLayoutArquivoNovo= false;
    } //-- void deleteCdTipoLayoutArquivoNovo() 

    /**
     * Method deleteCdmeioAlternativoRemessa
     * 
     */
    public void deleteCdmeioAlternativoRemessa()
    {
        this._has_cdmeioAlternativoRemessa= false;
    } //-- void deleteCdmeioAlternativoRemessa() 

    /**
     * Method deleteNrMaximoContagemRemessa
     * 
     */
    public void deleteNrMaximoContagemRemessa()
    {
        this._has_nrMaximoContagemRemessa= false;
    } //-- void deleteNrMaximoContagemRemessa() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAplicacaoTransmissaoArquivo'.
     * 
     * @return int
     * @return the value of field 'cdAplicacaoTransmissaoArquivo'.
     */
    public int getCdAplicacaoTransmissaoArquivo()
    {
        return this._cdAplicacaoTransmissaoArquivo;
    } //-- int getCdAplicacaoTransmissaoArquivo() 

    /**
     * Returns the value of field 'cdControleNumeroRemessa'.
     * 
     * @return int
     * @return the value of field 'cdControleNumeroRemessa'.
     */
    public int getCdControleNumeroRemessa()
    {
        return this._cdControleNumeroRemessa;
    } //-- int getCdControleNumeroRemessa() 

    /**
     * Returns the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternativoRetorno'.
     */
    public int getCdMeioAlternativoRetorno()
    {
        return this._cdMeioAlternativoRetorno;
    } //-- int getCdMeioAlternativoRetorno() 

    /**
     * Returns the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRemessa'.
     */
    public int getCdMeioPrincipalRemessa()
    {
        return this._cdMeioPrincipalRemessa;
    } //-- int getCdMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRetorno'.
     */
    public int getCdMeioPrincipalRetorno()
    {
        return this._cdMeioPrincipalRetorno;
    } //-- int getCdMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdNivelControleRemessa'.
     * 
     * @return int
     * @return the value of field 'cdNivelControleRemessa'.
     */
    public int getCdNivelControleRemessa()
    {
        return this._cdNivelControleRemessa;
    } //-- int getCdNivelControleRemessa() 

    /**
     * Returns the value of field 'cdPercentualRejeicaoRemessa'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualRejeicaoRemessa'.
     */
    public java.math.BigDecimal getCdPercentualRejeicaoRemessa()
    {
        return this._cdPercentualRejeicaoRemessa;
    } //-- java.math.BigDecimal getCdPercentualRejeicaoRemessa() 

    /**
     * Returns the value of field 'cdPerdcContagemRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPerdcContagemRemessa'.
     */
    public int getCdPerdcContagemRemessa()
    {
        return this._cdPerdcContagemRemessa;
    } //-- int getCdPerdcContagemRemessa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdRejeicaoAcolhimentoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAcolhimentoRemessa'.
     */
    public int getCdRejeicaoAcolhimentoRemessa()
    {
        return this._cdRejeicaoAcolhimentoRemessa;
    } //-- int getCdRejeicaoAcolhimentoRemessa() 

    /**
     * Returns the value of field 'cdSerieAplicacaoTransmissao'.
     * 
     * @return String
     * @return the value of field 'cdSerieAplicacaoTransmissao'.
     */
    public java.lang.String getCdSerieAplicacaoTransmissao()
    {
        return this._cdSerieAplicacaoTransmissao;
    } //-- java.lang.String getCdSerieAplicacaoTransmissao() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivoNovo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivoNovo'.
     */
    public int getCdTipoLayoutArquivoNovo()
    {
        return this._cdTipoLayoutArquivoNovo;
    } //-- int getCdTipoLayoutArquivoNovo() 

    /**
     * Returns the value of field 'cdmeioAlternativoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdmeioAlternativoRemessa'.
     */
    public int getCdmeioAlternativoRemessa()
    {
        return this._cdmeioAlternativoRemessa;
    } //-- int getCdmeioAlternativoRemessa() 

    /**
     * Returns the value of field 'dsEmpresaTratamentoArquivo'.
     * 
     * @return String
     * @return the value of field 'dsEmpresaTratamentoArquivo'.
     */
    public java.lang.String getDsEmpresaTratamentoArquivo()
    {
        return this._dsEmpresaTratamentoArquivo;
    } //-- java.lang.String getDsEmpresaTratamentoArquivo() 

    /**
     * Returns the value of field 'dsNomeArquivoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRemessa'.
     */
    public java.lang.String getDsNomeArquivoRemessa()
    {
        return this._dsNomeArquivoRemessa;
    } //-- java.lang.String getDsNomeArquivoRemessa() 

    /**
     * Returns the value of field 'dsNomeArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRetorno'.
     */
    public java.lang.String getDsNomeArquivoRetorno()
    {
        return this._dsNomeArquivoRetorno;
    } //-- java.lang.String getDsNomeArquivoRetorno() 

    /**
     * Returns the value of field 'dsNomeCliente'.
     * 
     * @return String
     * @return the value of field 'dsNomeCliente'.
     */
    public java.lang.String getDsNomeCliente()
    {
        return this._dsNomeCliente;
    } //-- java.lang.String getDsNomeCliente() 

    /**
     * Returns the value of field 'nrMaximoContagemRemessa'.
     * 
     * @return long
     * @return the value of field 'nrMaximoContagemRemessa'.
     */
    public long getNrMaximoContagemRemessa()
    {
        return this._nrMaximoContagemRemessa;
    } //-- long getNrMaximoContagemRemessa() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAplicacaoTransmissaoArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplicacaoTransmissaoArquivo()
    {
        return this._has_cdAplicacaoTransmissaoArquivo;
    } //-- boolean hasCdAplicacaoTransmissaoArquivo() 

    /**
     * Method hasCdControleNumeroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleNumeroRemessa()
    {
        return this._has_cdControleNumeroRemessa;
    } //-- boolean hasCdControleNumeroRemessa() 

    /**
     * Method hasCdMeioAlternativoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternativoRetorno()
    {
        return this._has_cdMeioAlternativoRetorno;
    } //-- boolean hasCdMeioAlternativoRetorno() 

    /**
     * Method hasCdMeioPrincipalRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRemessa()
    {
        return this._has_cdMeioPrincipalRemessa;
    } //-- boolean hasCdMeioPrincipalRemessa() 

    /**
     * Method hasCdMeioPrincipalRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRetorno()
    {
        return this._has_cdMeioPrincipalRetorno;
    } //-- boolean hasCdMeioPrincipalRetorno() 

    /**
     * Method hasCdNivelControleRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelControleRemessa()
    {
        return this._has_cdNivelControleRemessa;
    } //-- boolean hasCdNivelControleRemessa() 

    /**
     * Method hasCdPerdcContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcContagemRemessa()
    {
        return this._has_cdPerdcContagemRemessa;
    } //-- boolean hasCdPerdcContagemRemessa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdRejeicaoAcolhimentoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAcolhimentoRemessa()
    {
        return this._has_cdRejeicaoAcolhimentoRemessa;
    } //-- boolean hasCdRejeicaoAcolhimentoRemessa() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoLayoutArquivoNovo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivoNovo()
    {
        return this._has_cdTipoLayoutArquivoNovo;
    } //-- boolean hasCdTipoLayoutArquivoNovo() 

    /**
     * Method hasCdmeioAlternativoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdmeioAlternativoRemessa()
    {
        return this._has_cdmeioAlternativoRemessa;
    } //-- boolean hasCdmeioAlternativoRemessa() 

    /**
     * Method hasNrMaximoContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMaximoContagemRemessa()
    {
        return this._has_nrMaximoContagemRemessa;
    } //-- boolean hasNrMaximoContagemRemessa() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplicacaoTransmissaoArquivo'.
     * 
     * @param cdAplicacaoTransmissaoArquivo the value of field
     * 'cdAplicacaoTransmissaoArquivo'.
     */
    public void setCdAplicacaoTransmissaoArquivo(int cdAplicacaoTransmissaoArquivo)
    {
        this._cdAplicacaoTransmissaoArquivo = cdAplicacaoTransmissaoArquivo;
        this._has_cdAplicacaoTransmissaoArquivo = true;
    } //-- void setCdAplicacaoTransmissaoArquivo(int) 

    /**
     * Sets the value of field 'cdControleNumeroRemessa'.
     * 
     * @param cdControleNumeroRemessa the value of field
     * 'cdControleNumeroRemessa'.
     */
    public void setCdControleNumeroRemessa(int cdControleNumeroRemessa)
    {
        this._cdControleNumeroRemessa = cdControleNumeroRemessa;
        this._has_cdControleNumeroRemessa = true;
    } //-- void setCdControleNumeroRemessa(int) 

    /**
     * Sets the value of field 'cdMeioAlternativoRetorno'.
     * 
     * @param cdMeioAlternativoRetorno the value of field
     * 'cdMeioAlternativoRetorno'.
     */
    public void setCdMeioAlternativoRetorno(int cdMeioAlternativoRetorno)
    {
        this._cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
        this._has_cdMeioAlternativoRetorno = true;
    } //-- void setCdMeioAlternativoRetorno(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @param cdMeioPrincipalRemessa the value of field
     * 'cdMeioPrincipalRemessa'.
     */
    public void setCdMeioPrincipalRemessa(int cdMeioPrincipalRemessa)
    {
        this._cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
        this._has_cdMeioPrincipalRemessa = true;
    } //-- void setCdMeioPrincipalRemessa(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @param cdMeioPrincipalRetorno the value of field
     * 'cdMeioPrincipalRetorno'.
     */
    public void setCdMeioPrincipalRetorno(int cdMeioPrincipalRetorno)
    {
        this._cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
        this._has_cdMeioPrincipalRetorno = true;
    } //-- void setCdMeioPrincipalRetorno(int) 

    /**
     * Sets the value of field 'cdNivelControleRemessa'.
     * 
     * @param cdNivelControleRemessa the value of field
     * 'cdNivelControleRemessa'.
     */
    public void setCdNivelControleRemessa(int cdNivelControleRemessa)
    {
        this._cdNivelControleRemessa = cdNivelControleRemessa;
        this._has_cdNivelControleRemessa = true;
    } //-- void setCdNivelControleRemessa(int) 

    /**
     * Sets the value of field 'cdPercentualRejeicaoRemessa'.
     * 
     * @param cdPercentualRejeicaoRemessa the value of field
     * 'cdPercentualRejeicaoRemessa'.
     */
    public void setCdPercentualRejeicaoRemessa(java.math.BigDecimal cdPercentualRejeicaoRemessa)
    {
        this._cdPercentualRejeicaoRemessa = cdPercentualRejeicaoRemessa;
    } //-- void setCdPercentualRejeicaoRemessa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPerdcContagemRemessa'.
     * 
     * @param cdPerdcContagemRemessa the value of field
     * 'cdPerdcContagemRemessa'.
     */
    public void setCdPerdcContagemRemessa(int cdPerdcContagemRemessa)
    {
        this._cdPerdcContagemRemessa = cdPerdcContagemRemessa;
        this._has_cdPerdcContagemRemessa = true;
    } //-- void setCdPerdcContagemRemessa(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdRejeicaoAcolhimentoRemessa'.
     * 
     * @param cdRejeicaoAcolhimentoRemessa the value of field
     * 'cdRejeicaoAcolhimentoRemessa'.
     */
    public void setCdRejeicaoAcolhimentoRemessa(int cdRejeicaoAcolhimentoRemessa)
    {
        this._cdRejeicaoAcolhimentoRemessa = cdRejeicaoAcolhimentoRemessa;
        this._has_cdRejeicaoAcolhimentoRemessa = true;
    } //-- void setCdRejeicaoAcolhimentoRemessa(int) 

    /**
     * Sets the value of field 'cdSerieAplicacaoTransmissao'.
     * 
     * @param cdSerieAplicacaoTransmissao the value of field
     * 'cdSerieAplicacaoTransmissao'.
     */
    public void setCdSerieAplicacaoTransmissao(java.lang.String cdSerieAplicacaoTransmissao)
    {
        this._cdSerieAplicacaoTransmissao = cdSerieAplicacaoTransmissao;
    } //-- void setCdSerieAplicacaoTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivoNovo'.
     * 
     * @param cdTipoLayoutArquivoNovo the value of field
     * 'cdTipoLayoutArquivoNovo'.
     */
    public void setCdTipoLayoutArquivoNovo(int cdTipoLayoutArquivoNovo)
    {
        this._cdTipoLayoutArquivoNovo = cdTipoLayoutArquivoNovo;
        this._has_cdTipoLayoutArquivoNovo = true;
    } //-- void setCdTipoLayoutArquivoNovo(int) 

    /**
     * Sets the value of field 'cdmeioAlternativoRemessa'.
     * 
     * @param cdmeioAlternativoRemessa the value of field
     * 'cdmeioAlternativoRemessa'.
     */
    public void setCdmeioAlternativoRemessa(int cdmeioAlternativoRemessa)
    {
        this._cdmeioAlternativoRemessa = cdmeioAlternativoRemessa;
        this._has_cdmeioAlternativoRemessa = true;
    } //-- void setCdmeioAlternativoRemessa(int) 

    /**
     * Sets the value of field 'dsEmpresaTratamentoArquivo'.
     * 
     * @param dsEmpresaTratamentoArquivo the value of field
     * 'dsEmpresaTratamentoArquivo'.
     */
    public void setDsEmpresaTratamentoArquivo(java.lang.String dsEmpresaTratamentoArquivo)
    {
        this._dsEmpresaTratamentoArquivo = dsEmpresaTratamentoArquivo;
    } //-- void setDsEmpresaTratamentoArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRemessa'.
     * 
     * @param dsNomeArquivoRemessa the value of field
     * 'dsNomeArquivoRemessa'.
     */
    public void setDsNomeArquivoRemessa(java.lang.String dsNomeArquivoRemessa)
    {
        this._dsNomeArquivoRemessa = dsNomeArquivoRemessa;
    } //-- void setDsNomeArquivoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRetorno'.
     * 
     * @param dsNomeArquivoRetorno the value of field
     * 'dsNomeArquivoRetorno'.
     */
    public void setDsNomeArquivoRetorno(java.lang.String dsNomeArquivoRetorno)
    {
        this._dsNomeArquivoRetorno = dsNomeArquivoRetorno;
    } //-- void setDsNomeArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeCliente'.
     * 
     * @param dsNomeCliente the value of field 'dsNomeCliente'.
     */
    public void setDsNomeCliente(java.lang.String dsNomeCliente)
    {
        this._dsNomeCliente = dsNomeCliente;
    } //-- void setDsNomeCliente(java.lang.String) 

    /**
     * Sets the value of field 'nrMaximoContagemRemessa'.
     * 
     * @param nrMaximoContagemRemessa the value of field
     * 'nrMaximoContagemRemessa'.
     */
    public void setNrMaximoContagemRemessa(long nrMaximoContagemRemessa)
    {
        this._nrMaximoContagemRemessa = nrMaximoContagemRemessa;
        this._has_nrMaximoContagemRemessa = true;
    } //-- void setNrMaximoContagemRemessa(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return SubstituirLayoutArqContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.substituirlayoutarqcontrato.request.SubstituirLayoutArqContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.substituirlayoutarqcontrato.request.SubstituirLayoutArqContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.substituirlayoutarqcontrato.request.SubstituirLayoutArqContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirlayoutarqcontrato.request.SubstituirLayoutArqContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
