/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias13.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias13 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsModTipoServico
     */
    private java.lang.String _dsModTipoServico;

    /**
     * Field _dsModTipoModalidade
     */
    private java.lang.String _dsModTipoModalidade;

    /**
     * Field _dsModTipoConsultaSaldo
     */
    private java.lang.String _dsModTipoConsultaSaldo;

    /**
     * Field _dsModTipoProcessamento
     */
    private java.lang.String _dsModTipoProcessamento;

    /**
     * Field _dsModTipoTratamentoFeriado
     */
    private java.lang.String _dsModTipoTratamentoFeriado;

    /**
     * Field _dsModTipoRejeicaoEfetivacao
     */
    private java.lang.String _dsModTipoRejeicaoEfetivacao;

    /**
     * Field _dsModTipoRejeicaoAgendado
     */
    private java.lang.String _dsModTipoRejeicaoAgendado;

    /**
     * Field _cdModIndicadorPercentualMaximo
     */
    private java.lang.String _cdModIndicadorPercentualMaximo;

    /**
     * Field _cdModIndicadorPercentualMaximoInconsistente
     */
    private int _cdModIndicadorPercentualMaximoInconsistente = 0;

    /**
     * keeps track of state for field:
     * _cdModIndicadorPercentualMaximoInconsistente
     */
    private boolean _has_cdModIndicadorPercentualMaximoInconsistente;

    /**
     * Field _cdModIndicadorQuantidadeMaxima
     */
    private java.lang.String _cdModIndicadorQuantidadeMaxima;

    /**
     * Field _qtModMaximoInconsistencia
     */
    private int _qtModMaximoInconsistencia = 0;

    /**
     * keeps track of state for field: _qtModMaximoInconsistencia
     */
    private boolean _has_qtModMaximoInconsistencia;

    /**
     * Field _cdModIndicadorValorMaximo
     */
    private java.lang.String _cdModIndicadorValorMaximo;

    /**
     * Field _vlModMaximoFavorecidoNao
     */
    private java.math.BigDecimal _vlModMaximoFavorecidoNao = new java.math.BigDecimal("0");

    /**
     * Field _dsModPrioridadeDebito
     */
    private java.lang.String _dsModPrioridadeDebito;

    /**
     * Field _cdModIndicadorValorDia
     */
    private java.lang.String _cdModIndicadorValorDia;

    /**
     * Field _vlModLimiteDiario
     */
    private java.math.BigDecimal _vlModLimiteDiario = new java.math.BigDecimal("0");

    /**
     * Field _cdModIndicadorValorIndividual
     */
    private java.lang.String _cdModIndicadorValorIndividual;

    /**
     * Field _vlModLimiteIndividual
     */
    private java.math.BigDecimal _vlModLimiteIndividual = new java.math.BigDecimal("0");

    /**
     * Field _cdModIndicadorQuantidadeFloating
     */
    private java.lang.String _cdModIndicadorQuantidadeFloating;

    /**
     * Field _qtModDiaFloating
     */
    private int _qtModDiaFloating = 0;

    /**
     * keeps track of state for field: _qtModDiaFloating
     */
    private boolean _has_qtModDiaFloating;

    /**
     * Field _cdModIndicadorCadastroFavorecido
     */
    private java.lang.String _cdModIndicadorCadastroFavorecido;

    /**
     * Field _cdModUtilizacaoCadastroFavorecido
     */
    private java.lang.String _cdModUtilizacaoCadastroFavorecido;

    /**
     * Field _cdModIndicadorQuantidadeRepique
     */
    private java.lang.String _cdModIndicadorQuantidadeRepique;

    /**
     * Field _qtModDiaRepique
     */
    private int _qtModDiaRepique = 0;

    /**
     * keeps track of state for field: _qtModDiaRepique
     */
    private boolean _has_qtModDiaRepique;

    /**
     * Field _dsModalidadeCferiLoc
     */
    private java.lang.String _dsModalidadeCferiLoc;

    /**
     * Field _dsModTipoConsFavorecido
     */
    private java.lang.String _dsModTipoConsFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias13() 
     {
        super();
        setVlModMaximoFavorecidoNao(new java.math.BigDecimal("0"));
        setVlModLimiteDiario(new java.math.BigDecimal("0"));
        setVlModLimiteIndividual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModIndicadorPercentualMaximoInconsistente
     * 
     */
    public void deleteCdModIndicadorPercentualMaximoInconsistente()
    {
        this._has_cdModIndicadorPercentualMaximoInconsistente= false;
    } //-- void deleteCdModIndicadorPercentualMaximoInconsistente() 

    /**
     * Method deleteQtModDiaFloating
     * 
     */
    public void deleteQtModDiaFloating()
    {
        this._has_qtModDiaFloating= false;
    } //-- void deleteQtModDiaFloating() 

    /**
     * Method deleteQtModDiaRepique
     * 
     */
    public void deleteQtModDiaRepique()
    {
        this._has_qtModDiaRepique= false;
    } //-- void deleteQtModDiaRepique() 

    /**
     * Method deleteQtModMaximoInconsistencia
     * 
     */
    public void deleteQtModMaximoInconsistencia()
    {
        this._has_qtModMaximoInconsistencia= false;
    } //-- void deleteQtModMaximoInconsistencia() 

    /**
     * Returns the value of field
     * 'cdModIndicadorCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdModIndicadorCadastroFavorecido'
     */
    public java.lang.String getCdModIndicadorCadastroFavorecido()
    {
        return this._cdModIndicadorCadastroFavorecido;
    } //-- java.lang.String getCdModIndicadorCadastroFavorecido() 

    /**
     * Returns the value of field 'cdModIndicadorPercentualMaximo'.
     * 
     * @return String
     * @return the value of field 'cdModIndicadorPercentualMaximo'.
     */
    public java.lang.String getCdModIndicadorPercentualMaximo()
    {
        return this._cdModIndicadorPercentualMaximo;
    } //-- java.lang.String getCdModIndicadorPercentualMaximo() 

    /**
     * Returns the value of field
     * 'cdModIndicadorPercentualMaximoInconsistente'.
     * 
     * @return int
     * @return the value of field
     * 'cdModIndicadorPercentualMaximoInconsistente'.
     */
    public int getCdModIndicadorPercentualMaximoInconsistente()
    {
        return this._cdModIndicadorPercentualMaximoInconsistente;
    } //-- int getCdModIndicadorPercentualMaximoInconsistente() 

    /**
     * Returns the value of field
     * 'cdModIndicadorQuantidadeFloating'.
     * 
     * @return String
     * @return the value of field 'cdModIndicadorQuantidadeFloating'
     */
    public java.lang.String getCdModIndicadorQuantidadeFloating()
    {
        return this._cdModIndicadorQuantidadeFloating;
    } //-- java.lang.String getCdModIndicadorQuantidadeFloating() 

    /**
     * Returns the value of field 'cdModIndicadorQuantidadeMaxima'.
     * 
     * @return String
     * @return the value of field 'cdModIndicadorQuantidadeMaxima'.
     */
    public java.lang.String getCdModIndicadorQuantidadeMaxima()
    {
        return this._cdModIndicadorQuantidadeMaxima;
    } //-- java.lang.String getCdModIndicadorQuantidadeMaxima() 

    /**
     * Returns the value of field
     * 'cdModIndicadorQuantidadeRepique'.
     * 
     * @return String
     * @return the value of field 'cdModIndicadorQuantidadeRepique'.
     */
    public java.lang.String getCdModIndicadorQuantidadeRepique()
    {
        return this._cdModIndicadorQuantidadeRepique;
    } //-- java.lang.String getCdModIndicadorQuantidadeRepique() 

    /**
     * Returns the value of field 'cdModIndicadorValorDia'.
     * 
     * @return String
     * @return the value of field 'cdModIndicadorValorDia'.
     */
    public java.lang.String getCdModIndicadorValorDia()
    {
        return this._cdModIndicadorValorDia;
    } //-- java.lang.String getCdModIndicadorValorDia() 

    /**
     * Returns the value of field 'cdModIndicadorValorIndividual'.
     * 
     * @return String
     * @return the value of field 'cdModIndicadorValorIndividual'.
     */
    public java.lang.String getCdModIndicadorValorIndividual()
    {
        return this._cdModIndicadorValorIndividual;
    } //-- java.lang.String getCdModIndicadorValorIndividual() 

    /**
     * Returns the value of field 'cdModIndicadorValorMaximo'.
     * 
     * @return String
     * @return the value of field 'cdModIndicadorValorMaximo'.
     */
    public java.lang.String getCdModIndicadorValorMaximo()
    {
        return this._cdModIndicadorValorMaximo;
    } //-- java.lang.String getCdModIndicadorValorMaximo() 

    /**
     * Returns the value of field
     * 'cdModUtilizacaoCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'cdModUtilizacaoCadastroFavorecido'.
     */
    public java.lang.String getCdModUtilizacaoCadastroFavorecido()
    {
        return this._cdModUtilizacaoCadastroFavorecido;
    } //-- java.lang.String getCdModUtilizacaoCadastroFavorecido() 

    /**
     * Returns the value of field 'dsModPrioridadeDebito'.
     * 
     * @return String
     * @return the value of field 'dsModPrioridadeDebito'.
     */
    public java.lang.String getDsModPrioridadeDebito()
    {
        return this._dsModPrioridadeDebito;
    } //-- java.lang.String getDsModPrioridadeDebito() 

    /**
     * Returns the value of field 'dsModTipoConsFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsModTipoConsFavorecido'.
     */
    public java.lang.String getDsModTipoConsFavorecido()
    {
        return this._dsModTipoConsFavorecido;
    } //-- java.lang.String getDsModTipoConsFavorecido() 

    /**
     * Returns the value of field 'dsModTipoConsultaSaldo'.
     * 
     * @return String
     * @return the value of field 'dsModTipoConsultaSaldo'.
     */
    public java.lang.String getDsModTipoConsultaSaldo()
    {
        return this._dsModTipoConsultaSaldo;
    } //-- java.lang.String getDsModTipoConsultaSaldo() 

    /**
     * Returns the value of field 'dsModTipoModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModTipoModalidade'.
     */
    public java.lang.String getDsModTipoModalidade()
    {
        return this._dsModTipoModalidade;
    } //-- java.lang.String getDsModTipoModalidade() 

    /**
     * Returns the value of field 'dsModTipoProcessamento'.
     * 
     * @return String
     * @return the value of field 'dsModTipoProcessamento'.
     */
    public java.lang.String getDsModTipoProcessamento()
    {
        return this._dsModTipoProcessamento;
    } //-- java.lang.String getDsModTipoProcessamento() 

    /**
     * Returns the value of field 'dsModTipoRejeicaoAgendado'.
     * 
     * @return String
     * @return the value of field 'dsModTipoRejeicaoAgendado'.
     */
    public java.lang.String getDsModTipoRejeicaoAgendado()
    {
        return this._dsModTipoRejeicaoAgendado;
    } //-- java.lang.String getDsModTipoRejeicaoAgendado() 

    /**
     * Returns the value of field 'dsModTipoRejeicaoEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dsModTipoRejeicaoEfetivacao'.
     */
    public java.lang.String getDsModTipoRejeicaoEfetivacao()
    {
        return this._dsModTipoRejeicaoEfetivacao;
    } //-- java.lang.String getDsModTipoRejeicaoEfetivacao() 

    /**
     * Returns the value of field 'dsModTipoServico'.
     * 
     * @return String
     * @return the value of field 'dsModTipoServico'.
     */
    public java.lang.String getDsModTipoServico()
    {
        return this._dsModTipoServico;
    } //-- java.lang.String getDsModTipoServico() 

    /**
     * Returns the value of field 'dsModTipoTratamentoFeriado'.
     * 
     * @return String
     * @return the value of field 'dsModTipoTratamentoFeriado'.
     */
    public java.lang.String getDsModTipoTratamentoFeriado()
    {
        return this._dsModTipoTratamentoFeriado;
    } //-- java.lang.String getDsModTipoTratamentoFeriado() 

    /**
     * Returns the value of field 'dsModalidadeCferiLoc'.
     * 
     * @return String
     * @return the value of field 'dsModalidadeCferiLoc'.
     */
    public java.lang.String getDsModalidadeCferiLoc()
    {
        return this._dsModalidadeCferiLoc;
    } //-- java.lang.String getDsModalidadeCferiLoc() 

    /**
     * Returns the value of field 'qtModDiaFloating'.
     * 
     * @return int
     * @return the value of field 'qtModDiaFloating'.
     */
    public int getQtModDiaFloating()
    {
        return this._qtModDiaFloating;
    } //-- int getQtModDiaFloating() 

    /**
     * Returns the value of field 'qtModDiaRepique'.
     * 
     * @return int
     * @return the value of field 'qtModDiaRepique'.
     */
    public int getQtModDiaRepique()
    {
        return this._qtModDiaRepique;
    } //-- int getQtModDiaRepique() 

    /**
     * Returns the value of field 'qtModMaximoInconsistencia'.
     * 
     * @return int
     * @return the value of field 'qtModMaximoInconsistencia'.
     */
    public int getQtModMaximoInconsistencia()
    {
        return this._qtModMaximoInconsistencia;
    } //-- int getQtModMaximoInconsistencia() 

    /**
     * Returns the value of field 'vlModLimiteDiario'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlModLimiteDiario'.
     */
    public java.math.BigDecimal getVlModLimiteDiario()
    {
        return this._vlModLimiteDiario;
    } //-- java.math.BigDecimal getVlModLimiteDiario() 

    /**
     * Returns the value of field 'vlModLimiteIndividual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlModLimiteIndividual'.
     */
    public java.math.BigDecimal getVlModLimiteIndividual()
    {
        return this._vlModLimiteIndividual;
    } //-- java.math.BigDecimal getVlModLimiteIndividual() 

    /**
     * Returns the value of field 'vlModMaximoFavorecidoNao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlModMaximoFavorecidoNao'.
     */
    public java.math.BigDecimal getVlModMaximoFavorecidoNao()
    {
        return this._vlModMaximoFavorecidoNao;
    } //-- java.math.BigDecimal getVlModMaximoFavorecidoNao() 

    /**
     * Method hasCdModIndicadorPercentualMaximoInconsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModIndicadorPercentualMaximoInconsistente()
    {
        return this._has_cdModIndicadorPercentualMaximoInconsistente;
    } //-- boolean hasCdModIndicadorPercentualMaximoInconsistente() 

    /**
     * Method hasQtModDiaFloating
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModDiaFloating()
    {
        return this._has_qtModDiaFloating;
    } //-- boolean hasQtModDiaFloating() 

    /**
     * Method hasQtModDiaRepique
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModDiaRepique()
    {
        return this._has_qtModDiaRepique;
    } //-- boolean hasQtModDiaRepique() 

    /**
     * Method hasQtModMaximoInconsistencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtModMaximoInconsistencia()
    {
        return this._has_qtModMaximoInconsistencia;
    } //-- boolean hasQtModMaximoInconsistencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModIndicadorCadastroFavorecido'.
     * 
     * @param cdModIndicadorCadastroFavorecido the value of field
     * 'cdModIndicadorCadastroFavorecido'.
     */
    public void setCdModIndicadorCadastroFavorecido(java.lang.String cdModIndicadorCadastroFavorecido)
    {
        this._cdModIndicadorCadastroFavorecido = cdModIndicadorCadastroFavorecido;
    } //-- void setCdModIndicadorCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdModIndicadorPercentualMaximo'.
     * 
     * @param cdModIndicadorPercentualMaximo the value of field
     * 'cdModIndicadorPercentualMaximo'.
     */
    public void setCdModIndicadorPercentualMaximo(java.lang.String cdModIndicadorPercentualMaximo)
    {
        this._cdModIndicadorPercentualMaximo = cdModIndicadorPercentualMaximo;
    } //-- void setCdModIndicadorPercentualMaximo(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdModIndicadorPercentualMaximoInconsistente'.
     * 
     * @param cdModIndicadorPercentualMaximoInconsistente the value
     * of field 'cdModIndicadorPercentualMaximoInconsistente'.
     */
    public void setCdModIndicadorPercentualMaximoInconsistente(int cdModIndicadorPercentualMaximoInconsistente)
    {
        this._cdModIndicadorPercentualMaximoInconsistente = cdModIndicadorPercentualMaximoInconsistente;
        this._has_cdModIndicadorPercentualMaximoInconsistente = true;
    } //-- void setCdModIndicadorPercentualMaximoInconsistente(int) 

    /**
     * Sets the value of field 'cdModIndicadorQuantidadeFloating'.
     * 
     * @param cdModIndicadorQuantidadeFloating the value of field
     * 'cdModIndicadorQuantidadeFloating'.
     */
    public void setCdModIndicadorQuantidadeFloating(java.lang.String cdModIndicadorQuantidadeFloating)
    {
        this._cdModIndicadorQuantidadeFloating = cdModIndicadorQuantidadeFloating;
    } //-- void setCdModIndicadorQuantidadeFloating(java.lang.String) 

    /**
     * Sets the value of field 'cdModIndicadorQuantidadeMaxima'.
     * 
     * @param cdModIndicadorQuantidadeMaxima the value of field
     * 'cdModIndicadorQuantidadeMaxima'.
     */
    public void setCdModIndicadorQuantidadeMaxima(java.lang.String cdModIndicadorQuantidadeMaxima)
    {
        this._cdModIndicadorQuantidadeMaxima = cdModIndicadorQuantidadeMaxima;
    } //-- void setCdModIndicadorQuantidadeMaxima(java.lang.String) 

    /**
     * Sets the value of field 'cdModIndicadorQuantidadeRepique'.
     * 
     * @param cdModIndicadorQuantidadeRepique the value of field
     * 'cdModIndicadorQuantidadeRepique'.
     */
    public void setCdModIndicadorQuantidadeRepique(java.lang.String cdModIndicadorQuantidadeRepique)
    {
        this._cdModIndicadorQuantidadeRepique = cdModIndicadorQuantidadeRepique;
    } //-- void setCdModIndicadorQuantidadeRepique(java.lang.String) 

    /**
     * Sets the value of field 'cdModIndicadorValorDia'.
     * 
     * @param cdModIndicadorValorDia the value of field
     * 'cdModIndicadorValorDia'.
     */
    public void setCdModIndicadorValorDia(java.lang.String cdModIndicadorValorDia)
    {
        this._cdModIndicadorValorDia = cdModIndicadorValorDia;
    } //-- void setCdModIndicadorValorDia(java.lang.String) 

    /**
     * Sets the value of field 'cdModIndicadorValorIndividual'.
     * 
     * @param cdModIndicadorValorIndividual the value of field
     * 'cdModIndicadorValorIndividual'.
     */
    public void setCdModIndicadorValorIndividual(java.lang.String cdModIndicadorValorIndividual)
    {
        this._cdModIndicadorValorIndividual = cdModIndicadorValorIndividual;
    } //-- void setCdModIndicadorValorIndividual(java.lang.String) 

    /**
     * Sets the value of field 'cdModIndicadorValorMaximo'.
     * 
     * @param cdModIndicadorValorMaximo the value of field
     * 'cdModIndicadorValorMaximo'.
     */
    public void setCdModIndicadorValorMaximo(java.lang.String cdModIndicadorValorMaximo)
    {
        this._cdModIndicadorValorMaximo = cdModIndicadorValorMaximo;
    } //-- void setCdModIndicadorValorMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdModUtilizacaoCadastroFavorecido'.
     * 
     * @param cdModUtilizacaoCadastroFavorecido the value of field
     * 'cdModUtilizacaoCadastroFavorecido'.
     */
    public void setCdModUtilizacaoCadastroFavorecido(java.lang.String cdModUtilizacaoCadastroFavorecido)
    {
        this._cdModUtilizacaoCadastroFavorecido = cdModUtilizacaoCadastroFavorecido;
    } //-- void setCdModUtilizacaoCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsModPrioridadeDebito'.
     * 
     * @param dsModPrioridadeDebito the value of field
     * 'dsModPrioridadeDebito'.
     */
    public void setDsModPrioridadeDebito(java.lang.String dsModPrioridadeDebito)
    {
        this._dsModPrioridadeDebito = dsModPrioridadeDebito;
    } //-- void setDsModPrioridadeDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsModTipoConsFavorecido'.
     * 
     * @param dsModTipoConsFavorecido the value of field
     * 'dsModTipoConsFavorecido'.
     */
    public void setDsModTipoConsFavorecido(java.lang.String dsModTipoConsFavorecido)
    {
        this._dsModTipoConsFavorecido = dsModTipoConsFavorecido;
    } //-- void setDsModTipoConsFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsModTipoConsultaSaldo'.
     * 
     * @param dsModTipoConsultaSaldo the value of field
     * 'dsModTipoConsultaSaldo'.
     */
    public void setDsModTipoConsultaSaldo(java.lang.String dsModTipoConsultaSaldo)
    {
        this._dsModTipoConsultaSaldo = dsModTipoConsultaSaldo;
    } //-- void setDsModTipoConsultaSaldo(java.lang.String) 

    /**
     * Sets the value of field 'dsModTipoModalidade'.
     * 
     * @param dsModTipoModalidade the value of field
     * 'dsModTipoModalidade'.
     */
    public void setDsModTipoModalidade(java.lang.String dsModTipoModalidade)
    {
        this._dsModTipoModalidade = dsModTipoModalidade;
    } //-- void setDsModTipoModalidade(java.lang.String) 

    /**
     * Sets the value of field 'dsModTipoProcessamento'.
     * 
     * @param dsModTipoProcessamento the value of field
     * 'dsModTipoProcessamento'.
     */
    public void setDsModTipoProcessamento(java.lang.String dsModTipoProcessamento)
    {
        this._dsModTipoProcessamento = dsModTipoProcessamento;
    } //-- void setDsModTipoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'dsModTipoRejeicaoAgendado'.
     * 
     * @param dsModTipoRejeicaoAgendado the value of field
     * 'dsModTipoRejeicaoAgendado'.
     */
    public void setDsModTipoRejeicaoAgendado(java.lang.String dsModTipoRejeicaoAgendado)
    {
        this._dsModTipoRejeicaoAgendado = dsModTipoRejeicaoAgendado;
    } //-- void setDsModTipoRejeicaoAgendado(java.lang.String) 

    /**
     * Sets the value of field 'dsModTipoRejeicaoEfetivacao'.
     * 
     * @param dsModTipoRejeicaoEfetivacao the value of field
     * 'dsModTipoRejeicaoEfetivacao'.
     */
    public void setDsModTipoRejeicaoEfetivacao(java.lang.String dsModTipoRejeicaoEfetivacao)
    {
        this._dsModTipoRejeicaoEfetivacao = dsModTipoRejeicaoEfetivacao;
    } //-- void setDsModTipoRejeicaoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'dsModTipoServico'.
     * 
     * @param dsModTipoServico the value of field 'dsModTipoServico'
     */
    public void setDsModTipoServico(java.lang.String dsModTipoServico)
    {
        this._dsModTipoServico = dsModTipoServico;
    } //-- void setDsModTipoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsModTipoTratamentoFeriado'.
     * 
     * @param dsModTipoTratamentoFeriado the value of field
     * 'dsModTipoTratamentoFeriado'.
     */
    public void setDsModTipoTratamentoFeriado(java.lang.String dsModTipoTratamentoFeriado)
    {
        this._dsModTipoTratamentoFeriado = dsModTipoTratamentoFeriado;
    } //-- void setDsModTipoTratamentoFeriado(java.lang.String) 

    /**
     * Sets the value of field 'dsModalidadeCferiLoc'.
     * 
     * @param dsModalidadeCferiLoc the value of field
     * 'dsModalidadeCferiLoc'.
     */
    public void setDsModalidadeCferiLoc(java.lang.String dsModalidadeCferiLoc)
    {
        this._dsModalidadeCferiLoc = dsModalidadeCferiLoc;
    } //-- void setDsModalidadeCferiLoc(java.lang.String) 

    /**
     * Sets the value of field 'qtModDiaFloating'.
     * 
     * @param qtModDiaFloating the value of field 'qtModDiaFloating'
     */
    public void setQtModDiaFloating(int qtModDiaFloating)
    {
        this._qtModDiaFloating = qtModDiaFloating;
        this._has_qtModDiaFloating = true;
    } //-- void setQtModDiaFloating(int) 

    /**
     * Sets the value of field 'qtModDiaRepique'.
     * 
     * @param qtModDiaRepique the value of field 'qtModDiaRepique'.
     */
    public void setQtModDiaRepique(int qtModDiaRepique)
    {
        this._qtModDiaRepique = qtModDiaRepique;
        this._has_qtModDiaRepique = true;
    } //-- void setQtModDiaRepique(int) 

    /**
     * Sets the value of field 'qtModMaximoInconsistencia'.
     * 
     * @param qtModMaximoInconsistencia the value of field
     * 'qtModMaximoInconsistencia'.
     */
    public void setQtModMaximoInconsistencia(int qtModMaximoInconsistencia)
    {
        this._qtModMaximoInconsistencia = qtModMaximoInconsistencia;
        this._has_qtModMaximoInconsistencia = true;
    } //-- void setQtModMaximoInconsistencia(int) 

    /**
     * Sets the value of field 'vlModLimiteDiario'.
     * 
     * @param vlModLimiteDiario the value of field
     * 'vlModLimiteDiario'.
     */
    public void setVlModLimiteDiario(java.math.BigDecimal vlModLimiteDiario)
    {
        this._vlModLimiteDiario = vlModLimiteDiario;
    } //-- void setVlModLimiteDiario(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlModLimiteIndividual'.
     * 
     * @param vlModLimiteIndividual the value of field
     * 'vlModLimiteIndividual'.
     */
    public void setVlModLimiteIndividual(java.math.BigDecimal vlModLimiteIndividual)
    {
        this._vlModLimiteIndividual = vlModLimiteIndividual;
    } //-- void setVlModLimiteIndividual(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlModMaximoFavorecidoNao'.
     * 
     * @param vlModMaximoFavorecidoNao the value of field
     * 'vlModMaximoFavorecidoNao'.
     */
    public void setVlModMaximoFavorecidoNao(java.math.BigDecimal vlModMaximoFavorecidoNao)
    {
        this._vlModMaximoFavorecidoNao = vlModMaximoFavorecidoNao;
    } //-- void setVlModMaximoFavorecidoNao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias13
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias13 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
