/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ExcluirMoedasArquivoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ExcluirMoedasArquivoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoservicoOperacao
     */
    private int _cdProdutoservicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoservicoOperacao
     */
    private boolean _has_cdProdutoservicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionadoProduto
     */
    private int _cdRelacionadoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionadoProduto
     */
    private boolean _has_cdRelacionadoProduto;

    /**
     * Field _cdIndicadorEconomicoMoeda
     */
    private int _cdIndicadorEconomicoMoeda = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoMoeda
     */
    private boolean _has_cdIndicadorEconomicoMoeda;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ExcluirMoedasArquivoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.request.ExcluirMoedasArquivoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorEconomicoMoeda
     * 
     */
    public void deleteCdIndicadorEconomicoMoeda()
    {
        this._has_cdIndicadorEconomicoMoeda= false;
    } //-- void deleteCdIndicadorEconomicoMoeda() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoservicoOperacao
     * 
     */
    public void deleteCdProdutoservicoOperacao()
    {
        this._has_cdProdutoservicoOperacao= false;
    } //-- void deleteCdProdutoservicoOperacao() 

    /**
     * Method deleteCdRelacionadoProduto
     * 
     */
    public void deleteCdRelacionadoProduto()
    {
        this._has_cdRelacionadoProduto= false;
    } //-- void deleteCdRelacionadoProduto() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoMoeda'.
     */
    public int getCdIndicadorEconomicoMoeda()
    {
        return this._cdIndicadorEconomicoMoeda;
    } //-- int getCdIndicadorEconomicoMoeda() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoservicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoservicoOperacao'.
     */
    public int getCdProdutoservicoOperacao()
    {
        return this._cdProdutoservicoOperacao;
    } //-- int getCdProdutoservicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionadoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionadoProduto'.
     */
    public int getCdRelacionadoProduto()
    {
        return this._cdRelacionadoProduto;
    } //-- int getCdRelacionadoProduto() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdIndicadorEconomicoMoeda
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoMoeda()
    {
        return this._has_cdIndicadorEconomicoMoeda;
    } //-- boolean hasCdIndicadorEconomicoMoeda() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoservicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoservicoOperacao()
    {
        return this._has_cdProdutoservicoOperacao;
    } //-- boolean hasCdProdutoservicoOperacao() 

    /**
     * Method hasCdRelacionadoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionadoProduto()
    {
        return this._has_cdRelacionadoProduto;
    } //-- boolean hasCdRelacionadoProduto() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @param cdIndicadorEconomicoMoeda the value of field
     * 'cdIndicadorEconomicoMoeda'.
     */
    public void setCdIndicadorEconomicoMoeda(int cdIndicadorEconomicoMoeda)
    {
        this._cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
        this._has_cdIndicadorEconomicoMoeda = true;
    } //-- void setCdIndicadorEconomicoMoeda(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoservicoOperacao'.
     * 
     * @param cdProdutoservicoOperacao the value of field
     * 'cdProdutoservicoOperacao'.
     */
    public void setCdProdutoservicoOperacao(int cdProdutoservicoOperacao)
    {
        this._cdProdutoservicoOperacao = cdProdutoservicoOperacao;
        this._has_cdProdutoservicoOperacao = true;
    } //-- void setCdProdutoservicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionadoProduto'.
     * 
     * @param cdRelacionadoProduto the value of field
     * 'cdRelacionadoProduto'.
     */
    public void setCdRelacionadoProduto(int cdRelacionadoProduto)
    {
        this._cdRelacionadoProduto = cdRelacionadoProduto;
        this._has_cdRelacionadoProduto = true;
    } //-- void setCdRelacionadoProduto(int) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ExcluirMoedasArquivoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.request.ExcluirMoedasArquivoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.request.ExcluirMoedasArquivoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.request.ExcluirMoedasArquivoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.request.ExcluirMoedasArquivoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
