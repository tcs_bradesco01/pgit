/*
 * Nome: br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean;

import java.math.BigDecimal;
import java.util.List;

/**
 * Nome: IncluirEstornoOrdensPagamentosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirEstornoOrdensPagamentosEntradaDTO {

	/** Atributo cdPrimeiraSolicitacao. */
	private String cdPrimeiraSolicitacao;
	
	/** Atributo cdSolicitacaoPagamento. */
	private Integer cdSolicitacaoPagamento;
	
	/** Atributo nrSolicitacaoPagamento. */
	private Integer nrSolicitacaoPagamento;
	
	/** Atributo vlrPrevistoEstornoPagamento. */
	private BigDecimal vlrPrevistoEstornoPagamento;
	
	/** Atributo dsObservacaoAutorizacaoEstorno. */
	private String dsObservacaoAutorizacaoEstorno;
	
	/** Atributo cdUsuarioAutorizacaoEstorno. */
	private String cdUsuarioAutorizacaoEstorno;
	
	/** Atributo nrPagamento. */
	private Integer nrPagamento;
	
	/** Atributo ocorrencias. */
	private List<IncluirEstornoOrdensPagamentosListaDTO> ocorrencias;

	/**
	 * Get: cdPrimeiraSolicitacao.
	 *
	 * @return cdPrimeiraSolicitacao
	 */
	public String getCdPrimeiraSolicitacao() {
		return cdPrimeiraSolicitacao;
	}

	/**
	 * Set: cdPrimeiraSolicitacao.
	 *
	 * @param cdPrimeiraSolicitacao the cd primeira solicitacao
	 */
	public void setCdPrimeiraSolicitacao(String cdPrimeiraSolicitacao) {
		this.cdPrimeiraSolicitacao = cdPrimeiraSolicitacao;
	}

	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}

	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}

	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}

	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}

	/**
	 * Get: vlrPrevistoEstornoPagamento.
	 *
	 * @return vlrPrevistoEstornoPagamento
	 */
	public BigDecimal getVlrPrevistoEstornoPagamento() {
		return vlrPrevistoEstornoPagamento;
	}

	/**
	 * Set: vlrPrevistoEstornoPagamento.
	 *
	 * @param vlrPrevistoEstornoPagamento the vlr previsto estorno pagamento
	 */
	public void setVlrPrevistoEstornoPagamento(BigDecimal vlrPrevistoEstornoPagamento) {
		this.vlrPrevistoEstornoPagamento = vlrPrevistoEstornoPagamento;
	}

	/**
	 * Get: dsObservacaoAutorizacaoEstorno.
	 *
	 * @return dsObservacaoAutorizacaoEstorno
	 */
	public String getDsObservacaoAutorizacaoEstorno() {
		return dsObservacaoAutorizacaoEstorno;
	}

	/**
	 * Set: dsObservacaoAutorizacaoEstorno.
	 *
	 * @param dsObservacaoAutorizacaoEstorno the ds observacao autorizacao estorno
	 */
	public void setDsObservacaoAutorizacaoEstorno(String dsObservacaoAutorizacaoEstorno) {
		this.dsObservacaoAutorizacaoEstorno = dsObservacaoAutorizacaoEstorno;
	}

	/**
	 * Get: cdUsuarioAutorizacaoEstorno.
	 *
	 * @return cdUsuarioAutorizacaoEstorno
	 */
	public String getCdUsuarioAutorizacaoEstorno() {
		return cdUsuarioAutorizacaoEstorno;
	}

	/**
	 * Set: cdUsuarioAutorizacaoEstorno.
	 *
	 * @param cdUsuarioAutorizacaoEstorno the cd usuario autorizacao estorno
	 */
	public void setCdUsuarioAutorizacaoEstorno(String cdUsuarioAutorizacaoEstorno) {
		this.cdUsuarioAutorizacaoEstorno = cdUsuarioAutorizacaoEstorno;
	}

	/**
	 * Get: nrPagamento.
	 *
	 * @return nrPagamento
	 */
	public Integer getNrPagamento() {
		return nrPagamento;
	}

	/**
	 * Set: nrPagamento.
	 *
	 * @param nrPagamento the nr pagamento
	 */
	public void setNrPagamento(Integer nrPagamento) {
		this.nrPagamento = nrPagamento;
	}

	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<IncluirEstornoOrdensPagamentosListaDTO> getOcorrencias() {
		return ocorrencias;
	}

	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(List<IncluirEstornoOrdensPagamentosListaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
}