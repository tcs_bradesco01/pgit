/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AlterarOrgaoPagadorRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarOrgaoPagadorRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdOrgaoPagador
     */
    private int _cdOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdOrgaoPagador
     */
    private boolean _has_cdOrgaoPagador;

    /**
     * Field _cdTipoUnidadeOrganizacional
     */
    private int _cdTipoUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacional
     */
    private boolean _has_cdTipoUnidadeOrganizacional;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _nrSeqUnidadeOrganizacional
     */
    private int _nrSeqUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _nrSeqUnidadeOrganizacional
     */
    private boolean _has_nrSeqUnidadeOrganizacional;

    /**
     * Field _cdTarifOrgaoPagador
     */
    private int _cdTarifOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdTarifOrgaoPagador
     */
    private boolean _has_cdTarifOrgaoPagador;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarOrgaoPagadorRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.request.AlterarOrgaoPagadorRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOrgaoPagador
     * 
     */
    public void deleteCdOrgaoPagador()
    {
        this._has_cdOrgaoPagador= false;
    } //-- void deleteCdOrgaoPagador() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdTarifOrgaoPagador
     * 
     */
    public void deleteCdTarifOrgaoPagador()
    {
        this._has_cdTarifOrgaoPagador= false;
    } //-- void deleteCdTarifOrgaoPagador() 

    /**
     * Method deleteCdTipoUnidadeOrganizacional
     * 
     */
    public void deleteCdTipoUnidadeOrganizacional()
    {
        this._has_cdTipoUnidadeOrganizacional= false;
    } //-- void deleteCdTipoUnidadeOrganizacional() 

    /**
     * Method deleteNrSeqUnidadeOrganizacional
     * 
     */
    public void deleteNrSeqUnidadeOrganizacional()
    {
        this._has_nrSeqUnidadeOrganizacional= false;
    } //-- void deleteNrSeqUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdOrgaoPagador'.
     */
    public int getCdOrgaoPagador()
    {
        return this._cdOrgaoPagador;
    } //-- int getCdOrgaoPagador() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdTarifOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdTarifOrgaoPagador'.
     */
    public int getCdTarifOrgaoPagador()
    {
        return this._cdTarifOrgaoPagador;
    } //-- int getCdTarifOrgaoPagador() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacional'.
     */
    public int getCdTipoUnidadeOrganizacional()
    {
        return this._cdTipoUnidadeOrganizacional;
    } //-- int getCdTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'nrSeqUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSeqUnidadeOrganizacional'.
     */
    public int getNrSeqUnidadeOrganizacional()
    {
        return this._nrSeqUnidadeOrganizacional;
    } //-- int getNrSeqUnidadeOrganizacional() 

    /**
     * Method hasCdOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrgaoPagador()
    {
        return this._has_cdOrgaoPagador;
    } //-- boolean hasCdOrgaoPagador() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdTarifOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTarifOrgaoPagador()
    {
        return this._has_cdTarifOrgaoPagador;
    } //-- boolean hasCdTarifOrgaoPagador() 

    /**
     * Method hasCdTipoUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacional()
    {
        return this._has_cdTipoUnidadeOrganizacional;
    } //-- boolean hasCdTipoUnidadeOrganizacional() 

    /**
     * Method hasNrSeqUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqUnidadeOrganizacional()
    {
        return this._has_nrSeqUnidadeOrganizacional;
    } //-- boolean hasNrSeqUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOrgaoPagador'.
     * 
     * @param cdOrgaoPagador the value of field 'cdOrgaoPagador'.
     */
    public void setCdOrgaoPagador(int cdOrgaoPagador)
    {
        this._cdOrgaoPagador = cdOrgaoPagador;
        this._has_cdOrgaoPagador = true;
    } //-- void setCdOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTarifOrgaoPagador'.
     * 
     * @param cdTarifOrgaoPagador the value of field
     * 'cdTarifOrgaoPagador'.
     */
    public void setCdTarifOrgaoPagador(int cdTarifOrgaoPagador)
    {
        this._cdTarifOrgaoPagador = cdTarifOrgaoPagador;
        this._has_cdTarifOrgaoPagador = true;
    } //-- void setCdTarifOrgaoPagador(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @param cdTipoUnidadeOrganizacional the value of field
     * 'cdTipoUnidadeOrganizacional'.
     */
    public void setCdTipoUnidadeOrganizacional(int cdTipoUnidadeOrganizacional)
    {
        this._cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
        this._has_cdTipoUnidadeOrganizacional = true;
    } //-- void setCdTipoUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'nrSeqUnidadeOrganizacional'.
     * 
     * @param nrSeqUnidadeOrganizacional the value of field
     * 'nrSeqUnidadeOrganizacional'.
     */
    public void setNrSeqUnidadeOrganizacional(int nrSeqUnidadeOrganizacional)
    {
        this._nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
        this._has_nrSeqUnidadeOrganizacional = true;
    } //-- void setNrSeqUnidadeOrganizacional(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarOrgaoPagadorRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.request.AlterarOrgaoPagadorRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.request.AlterarOrgaoPagadorRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.request.AlterarOrgaoPagadorRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.request.AlterarOrgaoPagadorRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
