/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacaoPagamentoIntegrado
     */
    private int _cdSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdSolicitacaoPagamentoIntegrado
     */
    private boolean _has_cdSolicitacaoPagamentoIntegrado;

    /**
     * Field _nrSolicitacaoPagamentoIntegrado
     */
    private int _nrSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _nrSolicitacaoPagamentoIntegrado
     */
    private boolean _has_nrSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdProdutoServicoOper
     */
    private int _cdProdutoServicoOper = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOper
     */
    private boolean _has_cdProdutoServicoOper;

    /**
     * Field _dsProdutoServicoOper
     */
    private java.lang.String _dsProdutoServicoOper;

    /**
     * Field _cdProdutoOperRelacionado
     */
    private int _cdProdutoOperRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperRelacionado
     */
    private boolean _has_cdProdutoOperRelacionado;

    /**
     * Field _dsProdutoOperRelacionado
     */
    private java.lang.String _dsProdutoOperRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _hrSolicitacaoPagamentoIntegrado
     */
    private java.lang.String _hrSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _cdPessoaJuridDir
     */
    private long _cdPessoaJuridDir = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridDir
     */
    private boolean _has_cdPessoaJuridDir;

    /**
     * Field _dsPessoaJuridDir
     */
    private java.lang.String _dsPessoaJuridDir;

    /**
     * Field _nrSeqUnidadeOrgnzDir
     */
    private int _nrSeqUnidadeOrgnzDir = 0;

    /**
     * keeps track of state for field: _nrSeqUnidadeOrgnzDir
     */
    private boolean _has_nrSeqUnidadeOrgnzDir;

    /**
     * Field _dsSeqUnidadeOrgnzDir
     */
    private java.lang.String _dsSeqUnidadeOrgnzDir;

    /**
     * Field _cdPessoaJuridGerc
     */
    private long _cdPessoaJuridGerc = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridGerc
     */
    private boolean _has_cdPessoaJuridGerc;

    /**
     * Field _dsPessoaJuridGerc
     */
    private java.lang.String _dsPessoaJuridGerc;

    /**
     * Field _nrUnidadeOrgnzGerc
     */
    private int _nrUnidadeOrgnzGerc = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrgnzGerc
     */
    private boolean _has_nrUnidadeOrgnzGerc;

    /**
     * Field _dsUnidadeOrgnzGerc
     */
    private java.lang.String _dsUnidadeOrgnzGerc;

    /**
     * Field _cdPessoaJuridNegocio
     */
    private long _cdPessoaJuridNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridNegocio
     */
    private boolean _has_cdPessoaJuridNegocio;

    /**
     * Field _dsPessoaJuridNegocio
     */
    private java.lang.String _dsPessoaJuridNegocio;

    /**
     * Field _nrUnidadeOrgnzOper
     */
    private int _nrUnidadeOrgnzOper = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrgnzOper
     */
    private boolean _has_nrUnidadeOrgnzOper;

    /**
     * Field _dsUnidadeOrgnzOper
     */
    private java.lang.String _dsUnidadeOrgnzOper;

    /**
     * Field _cdTipoRelatPagamento
     */
    private int _cdTipoRelatPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoRelatPagamento
     */
    private boolean _has_cdTipoRelatPagamento;

    /**
     * Field _cdSegmentoCliente
     */
    private int _cdSegmentoCliente = 0;

    /**
     * keeps track of state for field: _cdSegmentoCliente
     */
    private boolean _has_cdSegmentoCliente;

    /**
     * Field _dsSegmentoCliente
     */
    private java.lang.String _dsSegmentoCliente;

    /**
     * Field _cdGrpEconomicoCli
     */
    private long _cdGrpEconomicoCli = 0;

    /**
     * keeps track of state for field: _cdGrpEconomicoCli
     */
    private boolean _has_cdGrpEconomicoCli;

    /**
     * Field _dsGrpEconomicoCli
     */
    private java.lang.String _dsGrpEconomicoCli;

    /**
     * Field _cdClassAtividadeEconomica
     */
    private java.lang.String _cdClassAtividadeEconomica;

    /**
     * Field _dsRamoAtividadeEconomica
     */
    private int _dsRamoAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _dsRamoAtividadeEconomica
     */
    private boolean _has_dsRamoAtividadeEconomica;

    /**
     * Field _cdSubRamoAtividadeEconomica
     */
    private int _cdSubRamoAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdSubRamoAtividadeEconomica
     */
    private boolean _has_cdSubRamoAtividadeEconomica;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenSegrcInclusao
     */
    private java.lang.String _cdAutenSegrcInclusao;

    /**
     * Field _nmOperFluxoInclusao
     */
    private java.lang.String _nmOperFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenSegrcManutencao
     */
    private java.lang.String _cdAutenSegrcManutencao;

    /**
     * Field _nmOperFluxoManutencao
     */
    private java.lang.String _nmOperFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdGrpEconomicoCli
     * 
     */
    public void deleteCdGrpEconomicoCli()
    {
        this._has_cdGrpEconomicoCli= false;
    } //-- void deleteCdGrpEconomicoCli() 

    /**
     * Method deleteCdPessoaJuridDir
     * 
     */
    public void deleteCdPessoaJuridDir()
    {
        this._has_cdPessoaJuridDir= false;
    } //-- void deleteCdPessoaJuridDir() 

    /**
     * Method deleteCdPessoaJuridGerc
     * 
     */
    public void deleteCdPessoaJuridGerc()
    {
        this._has_cdPessoaJuridGerc= false;
    } //-- void deleteCdPessoaJuridGerc() 

    /**
     * Method deleteCdPessoaJuridNegocio
     * 
     */
    public void deleteCdPessoaJuridNegocio()
    {
        this._has_cdPessoaJuridNegocio= false;
    } //-- void deleteCdPessoaJuridNegocio() 

    /**
     * Method deleteCdProdutoOperRelacionado
     * 
     */
    public void deleteCdProdutoOperRelacionado()
    {
        this._has_cdProdutoOperRelacionado= false;
    } //-- void deleteCdProdutoOperRelacionado() 

    /**
     * Method deleteCdProdutoServicoOper
     * 
     */
    public void deleteCdProdutoServicoOper()
    {
        this._has_cdProdutoServicoOper= false;
    } //-- void deleteCdProdutoServicoOper() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdSegmentoCliente
     * 
     */
    public void deleteCdSegmentoCliente()
    {
        this._has_cdSegmentoCliente= false;
    } //-- void deleteCdSegmentoCliente() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteCdSolicitacaoPagamentoIntegrado()
    {
        this._has_cdSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteCdSubRamoAtividadeEconomica
     * 
     */
    public void deleteCdSubRamoAtividadeEconomica()
    {
        this._has_cdSubRamoAtividadeEconomica= false;
    } //-- void deleteCdSubRamoAtividadeEconomica() 

    /**
     * Method deleteCdTipoRelatPagamento
     * 
     */
    public void deleteCdTipoRelatPagamento()
    {
        this._has_cdTipoRelatPagamento= false;
    } //-- void deleteCdTipoRelatPagamento() 

    /**
     * Method deleteDsRamoAtividadeEconomica
     * 
     */
    public void deleteDsRamoAtividadeEconomica()
    {
        this._has_dsRamoAtividadeEconomica= false;
    } //-- void deleteDsRamoAtividadeEconomica() 

    /**
     * Method deleteNrSeqUnidadeOrgnzDir
     * 
     */
    public void deleteNrSeqUnidadeOrgnzDir()
    {
        this._has_nrSeqUnidadeOrgnzDir= false;
    } //-- void deleteNrSeqUnidadeOrgnzDir() 

    /**
     * Method deleteNrSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteNrSolicitacaoPagamentoIntegrado()
    {
        this._has_nrSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteNrUnidadeOrgnzGerc
     * 
     */
    public void deleteNrUnidadeOrgnzGerc()
    {
        this._has_nrUnidadeOrgnzGerc= false;
    } //-- void deleteNrUnidadeOrgnzGerc() 

    /**
     * Method deleteNrUnidadeOrgnzOper
     * 
     */
    public void deleteNrUnidadeOrgnzOper()
    {
        this._has_nrUnidadeOrgnzOper= false;
    } //-- void deleteNrUnidadeOrgnzOper() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdAutenSegrcInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcInclusao'.
     */
    public java.lang.String getCdAutenSegrcInclusao()
    {
        return this._cdAutenSegrcInclusao;
    } //-- java.lang.String getCdAutenSegrcInclusao() 

    /**
     * Returns the value of field 'cdAutenSegrcManutencao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcManutencao'.
     */
    public java.lang.String getCdAutenSegrcManutencao()
    {
        return this._cdAutenSegrcManutencao;
    } //-- java.lang.String getCdAutenSegrcManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdClassAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'cdClassAtividadeEconomica'.
     */
    public java.lang.String getCdClassAtividadeEconomica()
    {
        return this._cdClassAtividadeEconomica;
    } //-- java.lang.String getCdClassAtividadeEconomica() 

    /**
     * Returns the value of field 'cdGrpEconomicoCli'.
     * 
     * @return long
     * @return the value of field 'cdGrpEconomicoCli'.
     */
    public long getCdGrpEconomicoCli()
    {
        return this._cdGrpEconomicoCli;
    } //-- long getCdGrpEconomicoCli() 

    /**
     * Returns the value of field 'cdPessoaJuridDir'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridDir'.
     */
    public long getCdPessoaJuridDir()
    {
        return this._cdPessoaJuridDir;
    } //-- long getCdPessoaJuridDir() 

    /**
     * Returns the value of field 'cdPessoaJuridGerc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridGerc'.
     */
    public long getCdPessoaJuridGerc()
    {
        return this._cdPessoaJuridGerc;
    } //-- long getCdPessoaJuridGerc() 

    /**
     * Returns the value of field 'cdPessoaJuridNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridNegocio'.
     */
    public long getCdPessoaJuridNegocio()
    {
        return this._cdPessoaJuridNegocio;
    } //-- long getCdPessoaJuridNegocio() 

    /**
     * Returns the value of field 'cdProdutoOperRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperRelacionado'.
     */
    public int getCdProdutoOperRelacionado()
    {
        return this._cdProdutoOperRelacionado;
    } //-- int getCdProdutoOperRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOper'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOper'.
     */
    public int getCdProdutoServicoOper()
    {
        return this._cdProdutoServicoOper;
    } //-- int getCdProdutoServicoOper() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdSegmentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoCliente'.
     */
    public int getCdSegmentoCliente()
    {
        return this._cdSegmentoCliente;
    } //-- int getCdSegmentoCliente() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamentoIntegrado'.
     */
    public int getCdSolicitacaoPagamentoIntegrado()
    {
        return this._cdSolicitacaoPagamentoIntegrado;
    } //-- int getCdSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdSubRamoAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdSubRamoAtividadeEconomica'.
     */
    public int getCdSubRamoAtividadeEconomica()
    {
        return this._cdSubRamoAtividadeEconomica;
    } //-- int getCdSubRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'cdTipoRelatPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelatPagamento'.
     */
    public int getCdTipoRelatPagamento()
    {
        return this._cdTipoRelatPagamento;
    } //-- int getCdTipoRelatPagamento() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsGrpEconomicoCli'.
     * 
     * @return String
     * @return the value of field 'dsGrpEconomicoCli'.
     */
    public java.lang.String getDsGrpEconomicoCli()
    {
        return this._dsGrpEconomicoCli;
    } //-- java.lang.String getDsGrpEconomicoCli() 

    /**
     * Returns the value of field 'dsPessoaJuridDir'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridDir'.
     */
    public java.lang.String getDsPessoaJuridDir()
    {
        return this._dsPessoaJuridDir;
    } //-- java.lang.String getDsPessoaJuridDir() 

    /**
     * Returns the value of field 'dsPessoaJuridGerc'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridGerc'.
     */
    public java.lang.String getDsPessoaJuridGerc()
    {
        return this._dsPessoaJuridGerc;
    } //-- java.lang.String getDsPessoaJuridGerc() 

    /**
     * Returns the value of field 'dsPessoaJuridNegocio'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridNegocio'.
     */
    public java.lang.String getDsPessoaJuridNegocio()
    {
        return this._dsPessoaJuridNegocio;
    } //-- java.lang.String getDsPessoaJuridNegocio() 

    /**
     * Returns the value of field 'dsProdutoOperRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperRelacionado'.
     */
    public java.lang.String getDsProdutoOperRelacionado()
    {
        return this._dsProdutoOperRelacionado;
    } //-- java.lang.String getDsProdutoOperRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOper'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOper'.
     */
    public java.lang.String getDsProdutoServicoOper()
    {
        return this._dsProdutoServicoOper;
    } //-- java.lang.String getDsProdutoServicoOper() 

    /**
     * Returns the value of field 'dsRamoAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'dsRamoAtividadeEconomica'.
     */
    public int getDsRamoAtividadeEconomica()
    {
        return this._dsRamoAtividadeEconomica;
    } //-- int getDsRamoAtividadeEconomica() 

    /**
     * Returns the value of field 'dsSegmentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoCliente'.
     */
    public java.lang.String getDsSegmentoCliente()
    {
        return this._dsSegmentoCliente;
    } //-- java.lang.String getDsSegmentoCliente() 

    /**
     * Returns the value of field 'dsSeqUnidadeOrgnzDir'.
     * 
     * @return String
     * @return the value of field 'dsSeqUnidadeOrgnzDir'.
     */
    public java.lang.String getDsSeqUnidadeOrgnzDir()
    {
        return this._dsSeqUnidadeOrgnzDir;
    } //-- java.lang.String getDsSeqUnidadeOrgnzDir() 

    /**
     * Returns the value of field 'dsUnidadeOrgnzGerc'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrgnzGerc'.
     */
    public java.lang.String getDsUnidadeOrgnzGerc()
    {
        return this._dsUnidadeOrgnzGerc;
    } //-- java.lang.String getDsUnidadeOrgnzGerc() 

    /**
     * Returns the value of field 'dsUnidadeOrgnzOper'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrgnzOper'.
     */
    public java.lang.String getDsUnidadeOrgnzOper()
    {
        return this._dsUnidadeOrgnzOper;
    } //-- java.lang.String getDsUnidadeOrgnzOper() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field
     * 'hrSolicitacaoPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacaoPagamentoIntegrado'.
     */
    public java.lang.String getHrSolicitacaoPagamentoIntegrado()
    {
        return this._hrSolicitacaoPagamentoIntegrado;
    } //-- java.lang.String getHrSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'nmOperFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoInclusao'.
     */
    public java.lang.String getNmOperFluxoInclusao()
    {
        return this._nmOperFluxoInclusao;
    } //-- java.lang.String getNmOperFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoManutencao'.
     */
    public java.lang.String getNmOperFluxoManutencao()
    {
        return this._nmOperFluxoManutencao;
    } //-- java.lang.String getNmOperFluxoManutencao() 

    /**
     * Returns the value of field 'nrSeqUnidadeOrgnzDir'.
     * 
     * @return int
     * @return the value of field 'nrSeqUnidadeOrgnzDir'.
     */
    public int getNrSeqUnidadeOrgnzDir()
    {
        return this._nrSeqUnidadeOrgnzDir;
    } //-- int getNrSeqUnidadeOrgnzDir() 

    /**
     * Returns the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamentoIntegrado'.
     */
    public int getNrSolicitacaoPagamentoIntegrado()
    {
        return this._nrSolicitacaoPagamentoIntegrado;
    } //-- int getNrSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'nrUnidadeOrgnzGerc'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrgnzGerc'.
     */
    public int getNrUnidadeOrgnzGerc()
    {
        return this._nrUnidadeOrgnzGerc;
    } //-- int getNrUnidadeOrgnzGerc() 

    /**
     * Returns the value of field 'nrUnidadeOrgnzOper'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrgnzOper'.
     */
    public int getNrUnidadeOrgnzOper()
    {
        return this._nrUnidadeOrgnzOper;
    } //-- int getNrUnidadeOrgnzOper() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdGrpEconomicoCli
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrpEconomicoCli()
    {
        return this._has_cdGrpEconomicoCli;
    } //-- boolean hasCdGrpEconomicoCli() 

    /**
     * Method hasCdPessoaJuridDir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridDir()
    {
        return this._has_cdPessoaJuridDir;
    } //-- boolean hasCdPessoaJuridDir() 

    /**
     * Method hasCdPessoaJuridGerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridGerc()
    {
        return this._has_cdPessoaJuridGerc;
    } //-- boolean hasCdPessoaJuridGerc() 

    /**
     * Method hasCdPessoaJuridNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridNegocio()
    {
        return this._has_cdPessoaJuridNegocio;
    } //-- boolean hasCdPessoaJuridNegocio() 

    /**
     * Method hasCdProdutoOperRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperRelacionado()
    {
        return this._has_cdProdutoOperRelacionado;
    } //-- boolean hasCdProdutoOperRelacionado() 

    /**
     * Method hasCdProdutoServicoOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOper()
    {
        return this._has_cdProdutoServicoOper;
    } //-- boolean hasCdProdutoServicoOper() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdSegmentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoCliente()
    {
        return this._has_cdSegmentoCliente;
    } //-- boolean hasCdSegmentoCliente() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamentoIntegrado()
    {
        return this._has_cdSolicitacaoPagamentoIntegrado;
    } //-- boolean hasCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdSubRamoAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubRamoAtividadeEconomica()
    {
        return this._has_cdSubRamoAtividadeEconomica;
    } //-- boolean hasCdSubRamoAtividadeEconomica() 

    /**
     * Method hasCdTipoRelatPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelatPagamento()
    {
        return this._has_cdTipoRelatPagamento;
    } //-- boolean hasCdTipoRelatPagamento() 

    /**
     * Method hasDsRamoAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsRamoAtividadeEconomica()
    {
        return this._has_dsRamoAtividadeEconomica;
    } //-- boolean hasDsRamoAtividadeEconomica() 

    /**
     * Method hasNrSeqUnidadeOrgnzDir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqUnidadeOrgnzDir()
    {
        return this._has_nrSeqUnidadeOrgnzDir;
    } //-- boolean hasNrSeqUnidadeOrgnzDir() 

    /**
     * Method hasNrSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamentoIntegrado()
    {
        return this._has_nrSolicitacaoPagamentoIntegrado;
    } //-- boolean hasNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasNrUnidadeOrgnzGerc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrgnzGerc()
    {
        return this._has_nrUnidadeOrgnzGerc;
    } //-- boolean hasNrUnidadeOrgnzGerc() 

    /**
     * Method hasNrUnidadeOrgnzOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrgnzOper()
    {
        return this._has_nrUnidadeOrgnzOper;
    } //-- boolean hasNrUnidadeOrgnzOper() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdAutenSegrcInclusao'.
     * 
     * @param cdAutenSegrcInclusao the value of field
     * 'cdAutenSegrcInclusao'.
     */
    public void setCdAutenSegrcInclusao(java.lang.String cdAutenSegrcInclusao)
    {
        this._cdAutenSegrcInclusao = cdAutenSegrcInclusao;
    } //-- void setCdAutenSegrcInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenSegrcManutencao'.
     * 
     * @param cdAutenSegrcManutencao the value of field
     * 'cdAutenSegrcManutencao'.
     */
    public void setCdAutenSegrcManutencao(java.lang.String cdAutenSegrcManutencao)
    {
        this._cdAutenSegrcManutencao = cdAutenSegrcManutencao;
    } //-- void setCdAutenSegrcManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdClassAtividadeEconomica'.
     * 
     * @param cdClassAtividadeEconomica the value of field
     * 'cdClassAtividadeEconomica'.
     */
    public void setCdClassAtividadeEconomica(java.lang.String cdClassAtividadeEconomica)
    {
        this._cdClassAtividadeEconomica = cdClassAtividadeEconomica;
    } //-- void setCdClassAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'cdGrpEconomicoCli'.
     * 
     * @param cdGrpEconomicoCli the value of field
     * 'cdGrpEconomicoCli'.
     */
    public void setCdGrpEconomicoCli(long cdGrpEconomicoCli)
    {
        this._cdGrpEconomicoCli = cdGrpEconomicoCli;
        this._has_cdGrpEconomicoCli = true;
    } //-- void setCdGrpEconomicoCli(long) 

    /**
     * Sets the value of field 'cdPessoaJuridDir'.
     * 
     * @param cdPessoaJuridDir the value of field 'cdPessoaJuridDir'
     */
    public void setCdPessoaJuridDir(long cdPessoaJuridDir)
    {
        this._cdPessoaJuridDir = cdPessoaJuridDir;
        this._has_cdPessoaJuridDir = true;
    } //-- void setCdPessoaJuridDir(long) 

    /**
     * Sets the value of field 'cdPessoaJuridGerc'.
     * 
     * @param cdPessoaJuridGerc the value of field
     * 'cdPessoaJuridGerc'.
     */
    public void setCdPessoaJuridGerc(long cdPessoaJuridGerc)
    {
        this._cdPessoaJuridGerc = cdPessoaJuridGerc;
        this._has_cdPessoaJuridGerc = true;
    } //-- void setCdPessoaJuridGerc(long) 

    /**
     * Sets the value of field 'cdPessoaJuridNegocio'.
     * 
     * @param cdPessoaJuridNegocio the value of field
     * 'cdPessoaJuridNegocio'.
     */
    public void setCdPessoaJuridNegocio(long cdPessoaJuridNegocio)
    {
        this._cdPessoaJuridNegocio = cdPessoaJuridNegocio;
        this._has_cdPessoaJuridNegocio = true;
    } //-- void setCdPessoaJuridNegocio(long) 

    /**
     * Sets the value of field 'cdProdutoOperRelacionado'.
     * 
     * @param cdProdutoOperRelacionado the value of field
     * 'cdProdutoOperRelacionado'.
     */
    public void setCdProdutoOperRelacionado(int cdProdutoOperRelacionado)
    {
        this._cdProdutoOperRelacionado = cdProdutoOperRelacionado;
        this._has_cdProdutoOperRelacionado = true;
    } //-- void setCdProdutoOperRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOper'.
     * 
     * @param cdProdutoServicoOper the value of field
     * 'cdProdutoServicoOper'.
     */
    public void setCdProdutoServicoOper(int cdProdutoServicoOper)
    {
        this._cdProdutoServicoOper = cdProdutoServicoOper;
        this._has_cdProdutoServicoOper = true;
    } //-- void setCdProdutoServicoOper(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdSegmentoCliente'.
     * 
     * @param cdSegmentoCliente the value of field
     * 'cdSegmentoCliente'.
     */
    public void setCdSegmentoCliente(int cdSegmentoCliente)
    {
        this._cdSegmentoCliente = cdSegmentoCliente;
        this._has_cdSegmentoCliente = true;
    } //-- void setCdSegmentoCliente(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @param cdSolicitacaoPagamentoIntegrado the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     */
    public void setCdSolicitacaoPagamentoIntegrado(int cdSolicitacaoPagamentoIntegrado)
    {
        this._cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
        this._has_cdSolicitacaoPagamentoIntegrado = true;
    } //-- void setCdSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdSubRamoAtividadeEconomica'.
     * 
     * @param cdSubRamoAtividadeEconomica the value of field
     * 'cdSubRamoAtividadeEconomica'.
     */
    public void setCdSubRamoAtividadeEconomica(int cdSubRamoAtividadeEconomica)
    {
        this._cdSubRamoAtividadeEconomica = cdSubRamoAtividadeEconomica;
        this._has_cdSubRamoAtividadeEconomica = true;
    } //-- void setCdSubRamoAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdTipoRelatPagamento'.
     * 
     * @param cdTipoRelatPagamento the value of field
     * 'cdTipoRelatPagamento'.
     */
    public void setCdTipoRelatPagamento(int cdTipoRelatPagamento)
    {
        this._cdTipoRelatPagamento = cdTipoRelatPagamento;
        this._has_cdTipoRelatPagamento = true;
    } //-- void setCdTipoRelatPagamento(int) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsGrpEconomicoCli'.
     * 
     * @param dsGrpEconomicoCli the value of field
     * 'dsGrpEconomicoCli'.
     */
    public void setDsGrpEconomicoCli(java.lang.String dsGrpEconomicoCli)
    {
        this._dsGrpEconomicoCli = dsGrpEconomicoCli;
    } //-- void setDsGrpEconomicoCli(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridDir'.
     * 
     * @param dsPessoaJuridDir the value of field 'dsPessoaJuridDir'
     */
    public void setDsPessoaJuridDir(java.lang.String dsPessoaJuridDir)
    {
        this._dsPessoaJuridDir = dsPessoaJuridDir;
    } //-- void setDsPessoaJuridDir(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridGerc'.
     * 
     * @param dsPessoaJuridGerc the value of field
     * 'dsPessoaJuridGerc'.
     */
    public void setDsPessoaJuridGerc(java.lang.String dsPessoaJuridGerc)
    {
        this._dsPessoaJuridGerc = dsPessoaJuridGerc;
    } //-- void setDsPessoaJuridGerc(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridNegocio'.
     * 
     * @param dsPessoaJuridNegocio the value of field
     * 'dsPessoaJuridNegocio'.
     */
    public void setDsPessoaJuridNegocio(java.lang.String dsPessoaJuridNegocio)
    {
        this._dsPessoaJuridNegocio = dsPessoaJuridNegocio;
    } //-- void setDsPessoaJuridNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperRelacionado'.
     * 
     * @param dsProdutoOperRelacionado the value of field
     * 'dsProdutoOperRelacionado'.
     */
    public void setDsProdutoOperRelacionado(java.lang.String dsProdutoOperRelacionado)
    {
        this._dsProdutoOperRelacionado = dsProdutoOperRelacionado;
    } //-- void setDsProdutoOperRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOper'.
     * 
     * @param dsProdutoServicoOper the value of field
     * 'dsProdutoServicoOper'.
     */
    public void setDsProdutoServicoOper(java.lang.String dsProdutoServicoOper)
    {
        this._dsProdutoServicoOper = dsProdutoServicoOper;
    } //-- void setDsProdutoServicoOper(java.lang.String) 

    /**
     * Sets the value of field 'dsRamoAtividadeEconomica'.
     * 
     * @param dsRamoAtividadeEconomica the value of field
     * 'dsRamoAtividadeEconomica'.
     */
    public void setDsRamoAtividadeEconomica(int dsRamoAtividadeEconomica)
    {
        this._dsRamoAtividadeEconomica = dsRamoAtividadeEconomica;
        this._has_dsRamoAtividadeEconomica = true;
    } //-- void setDsRamoAtividadeEconomica(int) 

    /**
     * Sets the value of field 'dsSegmentoCliente'.
     * 
     * @param dsSegmentoCliente the value of field
     * 'dsSegmentoCliente'.
     */
    public void setDsSegmentoCliente(java.lang.String dsSegmentoCliente)
    {
        this._dsSegmentoCliente = dsSegmentoCliente;
    } //-- void setDsSegmentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSeqUnidadeOrgnzDir'.
     * 
     * @param dsSeqUnidadeOrgnzDir the value of field
     * 'dsSeqUnidadeOrgnzDir'.
     */
    public void setDsSeqUnidadeOrgnzDir(java.lang.String dsSeqUnidadeOrgnzDir)
    {
        this._dsSeqUnidadeOrgnzDir = dsSeqUnidadeOrgnzDir;
    } //-- void setDsSeqUnidadeOrgnzDir(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrgnzGerc'.
     * 
     * @param dsUnidadeOrgnzGerc the value of field
     * 'dsUnidadeOrgnzGerc'.
     */
    public void setDsUnidadeOrgnzGerc(java.lang.String dsUnidadeOrgnzGerc)
    {
        this._dsUnidadeOrgnzGerc = dsUnidadeOrgnzGerc;
    } //-- void setDsUnidadeOrgnzGerc(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrgnzOper'.
     * 
     * @param dsUnidadeOrgnzOper the value of field
     * 'dsUnidadeOrgnzOper'.
     */
    public void setDsUnidadeOrgnzOper(java.lang.String dsUnidadeOrgnzOper)
    {
        this._dsUnidadeOrgnzOper = dsUnidadeOrgnzOper;
    } //-- void setDsUnidadeOrgnzOper(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacaoPagamentoIntegrado'.
     * 
     * @param hrSolicitacaoPagamentoIntegrado the value of field
     * 'hrSolicitacaoPagamentoIntegrado'.
     */
    public void setHrSolicitacaoPagamentoIntegrado(java.lang.String hrSolicitacaoPagamentoIntegrado)
    {
        this._hrSolicitacaoPagamentoIntegrado = hrSolicitacaoPagamentoIntegrado;
    } //-- void setHrSolicitacaoPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoInclusao'.
     * 
     * @param nmOperFluxoInclusao the value of field
     * 'nmOperFluxoInclusao'.
     */
    public void setNmOperFluxoInclusao(java.lang.String nmOperFluxoInclusao)
    {
        this._nmOperFluxoInclusao = nmOperFluxoInclusao;
    } //-- void setNmOperFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoManutencao'.
     * 
     * @param nmOperFluxoManutencao the value of field
     * 'nmOperFluxoManutencao'.
     */
    public void setNmOperFluxoManutencao(java.lang.String nmOperFluxoManutencao)
    {
        this._nmOperFluxoManutencao = nmOperFluxoManutencao;
    } //-- void setNmOperFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqUnidadeOrgnzDir'.
     * 
     * @param nrSeqUnidadeOrgnzDir the value of field
     * 'nrSeqUnidadeOrgnzDir'.
     */
    public void setNrSeqUnidadeOrgnzDir(int nrSeqUnidadeOrgnzDir)
    {
        this._nrSeqUnidadeOrgnzDir = nrSeqUnidadeOrgnzDir;
        this._has_nrSeqUnidadeOrgnzDir = true;
    } //-- void setNrSeqUnidadeOrgnzDir(int) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @param nrSolicitacaoPagamentoIntegrado the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     */
    public void setNrSolicitacaoPagamentoIntegrado(int nrSolicitacaoPagamentoIntegrado)
    {
        this._nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
        this._has_nrSolicitacaoPagamentoIntegrado = true;
    } //-- void setNrSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'nrUnidadeOrgnzGerc'.
     * 
     * @param nrUnidadeOrgnzGerc the value of field
     * 'nrUnidadeOrgnzGerc'.
     */
    public void setNrUnidadeOrgnzGerc(int nrUnidadeOrgnzGerc)
    {
        this._nrUnidadeOrgnzGerc = nrUnidadeOrgnzGerc;
        this._has_nrUnidadeOrgnzGerc = true;
    } //-- void setNrUnidadeOrgnzGerc(int) 

    /**
     * Sets the value of field 'nrUnidadeOrgnzOper'.
     * 
     * @param nrUnidadeOrgnzOper the value of field
     * 'nrUnidadeOrgnzOper'.
     */
    public void setNrUnidadeOrgnzOper(int nrUnidadeOrgnzOper)
    {
        this._nrUnidadeOrgnzOper = nrUnidadeOrgnzOper;
        this._has_nrUnidadeOrgnzOper = true;
    } //-- void setNrUnidadeOrgnzOper(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
