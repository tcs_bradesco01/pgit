/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean;

// TODO: Auto-generated Javadoc
/**
 * Nome: AlterarFuncoesAlcadaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarHorarioLimiteSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;

	/**
	 * Gets the cod mensagem.
	 *
	 * @return the codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Sets the cod mensagem.
	 *
	 * @param codMensagem the codMensagem to set
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Gets the mensagem.
	 *
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Sets the mensagem.
	 *
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
