/*
 * Nome: br.com.bradesco.web.pgit.service.report.jasper.data.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.jasper.data.impl;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;
import br.com.bradesco.web.pgit.service.report.jasper.data.JasperGenericParameter;

/**
 * Nome: JasperGenericParameterImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 * 
 * @see br.com.bradesco.web.pgit.service.report.jasper.data.JasperGenericParameter
 */
public class JasperGenericParameterImpl implements JasperGenericParameter {

    /** Atributo parameter. */
    private Object parameter = null;
    
    /**
     * Instancia um novo JasperGenericParameterImpl.
     *
     * @param genericParameter the generic parameter
     * 
     * @see
     */
    public JasperGenericParameterImpl(Object genericParameter) {
        super();
        this.parameter = genericParameter;
    }

    /**
     * (non-Javadoc).
     *
     * @param property the property
     * @return the object
     * @see br.com.bradesco.web.pgit.service.report.jasper.data.JasperGenericParameter#value(java.lang.String)
     */
    public Object value(String property) {
        try {
            return PropertyUtils.getProperty(parameter, property);
        } catch (IllegalAccessException e) {
            throw new BradescoApplicationException(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            throw new BradescoApplicationException(e.getMessage(), e);
        } catch (NoSuchMethodException e) {
            throw new BradescoApplicationException(e.getMessage(), e);
        }
    }

}
