/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;


/**
 * Nome: DetalharContaRelacionadaHistSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharContaRelacionadaHistSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdCorpoCpfCnpjParticipante. */
    private Long cdCorpoCpfCnpjParticipante;
    
    /** Atributo cdFilialCpfCnpjParticipante. */
    private Integer cdFilialCpfCnpjParticipante;
    
    /** Atributo cdDigitoCpfCnpjParticipante. */
    private Integer cdDigitoCpfCnpjParticipante;
    
    /** Atributo cdBancoRelacionamento. */
    private Integer cdBancoRelacionamento;
    
    /** Atributo cdAgenciaRelacionamento. */
    private Integer cdAgenciaRelacionamento;
    
    /** Atributo cdContaRelacionamento. */
    private Long cdContaRelacionamento;
    
    /** Atributo cdDigitoContaRelacionamento. */
    private String cdDigitoContaRelacionamento;
    
    /** Atributo cdTipoContaRelacionamento. */
    private Integer cdTipoContaRelacionamento;
    
    /** Atributo dsTipoContaRelacionamento. */
    private String dsTipoContaRelacionamento;
    
    /** Atributo cdIndicadorTipoManutencao. */
    private Integer cdIndicadorTipoManutencao;
    
    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExterno. */
    private String cdUsuarioManutencaoExterno;
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo cpfCnpjParticipanteFormatado. */
    private String cpfCnpjParticipanteFormatado;
    
    /** Atributo usuarioInclusaoFormatado. */
    private String usuarioInclusaoFormatado;
    
    /** Atributo tipoCanalInclusaoFormatado. */
    private String tipoCanalInclusaoFormatado;
    
    /** Atributo usuarioManutencaoFormatado. */
    private String usuarioManutencaoFormatado;
    
    /** Atributo tipoCanalManutencaoFormatado. */
    private String tipoCanalManutencaoFormatado;
    
	/**
	 * Get: cdAgenciaRelacionamento.
	 *
	 * @return cdAgenciaRelacionamento
	 */
	public Integer getCdAgenciaRelacionamento() {
		return cdAgenciaRelacionamento;
	}
	
	/**
	 * Set: cdAgenciaRelacionamento.
	 *
	 * @param cdAgenciaRelacionamento the cd agencia relacionamento
	 */
	public void setCdAgenciaRelacionamento(Integer cdAgenciaRelacionamento) {
		this.cdAgenciaRelacionamento = cdAgenciaRelacionamento;
	}
	
	/**
	 * Get: cdBancoRelacionamento.
	 *
	 * @return cdBancoRelacionamento
	 */
	public Integer getCdBancoRelacionamento() {
		return cdBancoRelacionamento;
	}
	
	/**
	 * Set: cdBancoRelacionamento.
	 *
	 * @param cdBancoRelacionamento the cd banco relacionamento
	 */
	public void setCdBancoRelacionamento(Integer cdBancoRelacionamento) {
		this.cdBancoRelacionamento = cdBancoRelacionamento;
	}
	
	/**
	 * Get: cdContaRelacionamento.
	 *
	 * @return cdContaRelacionamento
	 */
	public Long getCdContaRelacionamento() {
		return cdContaRelacionamento;
	}
	
	/**
	 * Set: cdContaRelacionamento.
	 *
	 * @param cdContaRelacionamento the cd conta relacionamento
	 */
	public void setCdContaRelacionamento(Long cdContaRelacionamento) {
		this.cdContaRelacionamento = cdContaRelacionamento;
	}
	
	/**
	 * Get: cdCorpoCpfCnpjParticipante.
	 *
	 * @return cdCorpoCpfCnpjParticipante
	 */
	public Long getCdCorpoCpfCnpjParticipante() {
		return cdCorpoCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCorpoCpfCnpjParticipante.
	 *
	 * @param cdCorpoCpfCnpjParticipante the cd corpo cpf cnpj participante
	 */
	public void setCdCorpoCpfCnpjParticipante(Long cdCorpoCpfCnpjParticipante) {
		this.cdCorpoCpfCnpjParticipante = cdCorpoCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdDigitoContaRelacionamento.
	 *
	 * @return cdDigitoContaRelacionamento
	 */
	public String getCdDigitoContaRelacionamento() {
		return cdDigitoContaRelacionamento;
	}
	
	/**
	 * Set: cdDigitoContaRelacionamento.
	 *
	 * @param cdDigitoContaRelacionamento the cd digito conta relacionamento
	 */
	public void setCdDigitoContaRelacionamento(String cdDigitoContaRelacionamento) {
		this.cdDigitoContaRelacionamento = cdDigitoContaRelacionamento;
	}
	
	/**
	 * Get: cdDigitoCpfCnpjParticipante.
	 *
	 * @return cdDigitoCpfCnpjParticipante
	 */
	public Integer getCdDigitoCpfCnpjParticipante() {
		return cdDigitoCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdDigitoCpfCnpjParticipante.
	 *
	 * @param cdDigitoCpfCnpjParticipante the cd digito cpf cnpj participante
	 */
	public void setCdDigitoCpfCnpjParticipante(Integer cdDigitoCpfCnpjParticipante) {
		this.cdDigitoCpfCnpjParticipante = cdDigitoCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdFilialCpfCnpjParticipante.
	 *
	 * @return cdFilialCpfCnpjParticipante
	 */
	public Integer getCdFilialCpfCnpjParticipante() {
		return cdFilialCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdFilialCpfCnpjParticipante.
	 *
	 * @param cdFilialCpfCnpjParticipante the cd filial cpf cnpj participante
	 */
	public void setCdFilialCpfCnpjParticipante(Integer cdFilialCpfCnpjParticipante) {
		this.cdFilialCpfCnpjParticipante = cdFilialCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}
	
	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoContaRelacionamento.
	 *
	 * @return cdTipoContaRelacionamento
	 */
	public Integer getCdTipoContaRelacionamento() {
		return cdTipoContaRelacionamento;
	}
	
	/**
	 * Set: cdTipoContaRelacionamento.
	 *
	 * @param cdTipoContaRelacionamento the cd tipo conta relacionamento
	 */
	public void setCdTipoContaRelacionamento(Integer cdTipoContaRelacionamento) {
		this.cdTipoContaRelacionamento = cdTipoContaRelacionamento;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsTipoContaRelacionamento.
	 *
	 * @return dsTipoContaRelacionamento
	 */
	public String getDsTipoContaRelacionamento() {
		return dsTipoContaRelacionamento;
	}
	
	/**
	 * Set: dsTipoContaRelacionamento.
	 *
	 * @param dsTipoContaRelacionamento the ds tipo conta relacionamento
	 */
	public void setDsTipoContaRelacionamento(String dsTipoContaRelacionamento) {
		this.dsTipoContaRelacionamento = dsTipoContaRelacionamento;
	}
	
	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}
	
	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cpfCnpjParticipanteFormatado.
	 *
	 * @return cpfCnpjParticipanteFormatado
	 */
	public String getCpfCnpjParticipanteFormatado() {
		return cpfCnpjParticipanteFormatado;
	}
	
	/**
	 * Set: cpfCnpjParticipanteFormatado.
	 *
	 * @param cpfCnpjParticipanteFormatado the cpf cnpj participante formatado
	 */
	public void setCpfCnpjParticipanteFormatado(String cpfCnpjParticipanteFormatado) {
		this.cpfCnpjParticipanteFormatado = cpfCnpjParticipanteFormatado;
	}
	
	/**
	 * Get: tipoCanalInclusaoFormatado.
	 *
	 * @return tipoCanalInclusaoFormatado
	 */
	public String getTipoCanalInclusaoFormatado() {
		return tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Set: tipoCanalInclusaoFormatado.
	 *
	 * @param tipoCanalInclusaoFormatado the tipo canal inclusao formatado
	 */
	public void setTipoCanalInclusaoFormatado(String tipoCanalInclusaoFormatado) {
		this.tipoCanalInclusaoFormatado = tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Get: tipoCanalManutencaoFormatado.
	 *
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado() {
		return tipoCanalManutencaoFormatado;
	}
	
	/**
	 * Set: tipoCanalManutencaoFormatado.
	 *
	 * @param tipoCanalManutencaoFormatado the tipo canal manutencao formatado
	 */
	public void setTipoCanalManutencaoFormatado(String tipoCanalManutencaoFormatado) {
		this.tipoCanalManutencaoFormatado = tipoCanalManutencaoFormatado;
	}
	
	/**
	 * Get: usuarioInclusaoFormatado.
	 *
	 * @return usuarioInclusaoFormatado
	 */
	public String getUsuarioInclusaoFormatado() {
		return usuarioInclusaoFormatado;
	}
	
	/**
	 * Set: usuarioInclusaoFormatado.
	 *
	 * @param usuarioInclusaoFormatado the usuario inclusao formatado
	 */
	public void setUsuarioInclusaoFormatado(String usuarioInclusaoFormatado) {
		this.usuarioInclusaoFormatado = usuarioInclusaoFormatado;
	}
	
	/**
	 * Get: usuarioManutencaoFormatado.
	 *
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}
	
	/**
	 * Set: usuarioManutencaoFormatado.
	 *
	 * @param usuarioManutencaoFormatado the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}
}