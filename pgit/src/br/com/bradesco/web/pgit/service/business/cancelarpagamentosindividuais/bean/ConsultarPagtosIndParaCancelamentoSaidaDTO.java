/*
 * Nome: br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cancelarpagamentosindividuais.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagtosIndParaCancelamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtosIndParaCancelamentoSaidaDTO {
    
    /** Atributo check. */
    private boolean check = false;

    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo nrCnpjCpf. */
    private Long nrCnpjCpf;

    /** Atributo nrFilialCnpjCpf. */
    private Integer nrFilialCnpjCpf;

    /** Atributo nrDigitoCnpjCpf. */
    private Integer nrDigitoCnpjCpf;

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;

    /** Atributo cdEmpresa. */
    private Long cdEmpresa;

    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrContrato. */
    private Long nrContrato;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;

    /** Atributo dsResumoProdutoServico. */
    private String dsResumoProdutoServico;

    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;

    /** Atributo dsOperacaoProdutoServico. */
    private String dsOperacaoProdutoServico;

    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;

    /** Atributo dtCreditoPagamento. */
    private String dtCreditoPagamento;

    /** Atributo vlEfetivoPagamento. */
    private BigDecimal vlEfetivoPagamento;

    /** Atributo cdInscricaoFavorecido. */
    private String cdInscricaoFavorecido;

    /** Atributo dsFavorecido. */
    private String dsFavorecido;

    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;

    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;

    /** Atributo cdDigitoAgenciaDebito. */
    private Integer cdDigitoAgenciaDebito;

    /** Atributo cdContaDebito. */
    private Long cdContaDebito;

    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;

    /** Atributo cdBancoCredito. */
    private Integer cdBancoCredito;

    /** Atributo cdAgenciaCredito. */
    private Integer cdAgenciaCredito;

    /** Atributo cdDigitoAgenciaCredito. */
    private Integer cdDigitoAgenciaCredito;

    /** Atributo cdContaCredito. */
    private Long cdContaCredito;

    /** Atributo cdDigitoContaCredito. */
    private String cdDigitoContaCredito;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;

    /** Atributo dsSituacaoOperacaoPagamento. */
    private String dsSituacaoOperacaoPagamento;

    /** Atributo cdMotivoSituacaoPagamento. */
    private Integer cdMotivoSituacaoPagamento;

    /** Atributo dsMotivoSituacaoPagamento. */
    private String dsMotivoSituacaoPagamento;

    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;

    /** Atributo cdTipoTela. */
    private Integer cdTipoTela;

    /** Atributo cdBancoCreditoFormatado. */
    private String cdBancoCreditoFormatado;

    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;

    /** Atributo favorecidoBeneficiarioFormatado. */
    private String favorecidoBeneficiarioFormatado;

    /** Atributo contaCreditoFormatada. */
    private String contaCreditoFormatada;

    /** Atributo bancoAgenciaContaDebitoFormatado. */
    private String bancoAgenciaContaDebitoFormatado;

    /** Atributo bancoAgenciaContaCreditoFormatado. */
    private String bancoAgenciaContaCreditoFormatado;

    /** Atributo vlEfetivoPagamentoFormatado. */
    private String vlEfetivoPagamentoFormatado;

    /** Atributo cdBancoDebitoFormatado. */
    private String cdBancoDebitoFormatado;

    /** Atributo cdAgenciaDebitoFormatado. */
    private String cdAgenciaDebitoFormatado;

    /** Atributo cdDigitoAgenciaDebitoFormatado. */
    private String cdDigitoAgenciaDebitoFormatado;

    /** Atributo cdContaDebitoFormatado. */
    private String cdContaDebitoFormatado;

    /** Atributo cdDigitoContaDebitoFormatado. */
    private String cdDigitoContaDebitoFormatado;

    /** Atributo agenciaDebitoFormatada. */
    private String agenciaDebitoFormatada;

    /** Atributo contaDebitoFormatada. */
    private String contaDebitoFormatada;

    /** Atributo agenciaCreditoFormatada. */
    private String agenciaCreditoFormatada;

    /** Atributo cdCpfCnpjFormatado. */
    private String cdCpfCnpjFormatado;

    /** Atributo dsEfetivacaoPagamento. */
    private String dsEfetivacaoPagamento;

    /** Atributo dsIndicadorPagamento. */
    private String dsIndicadorPagamento;

	private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;
	
    /**
     * Get: contaCreditoFormatada.
     *
     * @return contaCreditoFormatada
     */
    public String getContaCreditoFormatada() {
	return contaCreditoFormatada;
    }

    /**
     * Get: agenciaCreditoFormatada.
     *
     * @return agenciaCreditoFormatada
     */
    public String getAgenciaCreditoFormatada() {
	return agenciaCreditoFormatada;
    }

    /**
     * Set: agenciaCreditoFormatada.
     *
     * @param agenciaCreditoFormatada the agencia credito formatada
     */
    public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
	this.agenciaCreditoFormatada = agenciaCreditoFormatada;
    }

    /**
     * Get: agenciaDebitoFormatada.
     *
     * @return agenciaDebitoFormatada
     */
    public String getAgenciaDebitoFormatada() {
	return agenciaDebitoFormatada;
    }

    /**
     * Set: agenciaDebitoFormatada.
     *
     * @param agenciaDebitoFormatada the agencia debito formatada
     */
    public void setAgenciaDebitoFormatada(String agenciaDebitoFormatada) {
	this.agenciaDebitoFormatada = agenciaDebitoFormatada;
    }

    /**
     * Get: cdAgenciaDebitoFormatado.
     *
     * @return cdAgenciaDebitoFormatado
     */
    public String getCdAgenciaDebitoFormatado() {
	return cdAgenciaDebitoFormatado;
    }

    /**
     * Set: cdAgenciaDebitoFormatado.
     *
     * @param cdAgenciaDebitoFormatado the cd agencia debito formatado
     */
    public void setCdAgenciaDebitoFormatado(String cdAgenciaDebitoFormatado) {
	this.cdAgenciaDebitoFormatado = cdAgenciaDebitoFormatado;
    }

    /**
     * Get: cdBancoDebitoFormatado.
     *
     * @return cdBancoDebitoFormatado
     */
    public String getCdBancoDebitoFormatado() {
	return cdBancoDebitoFormatado;
    }

    /**
     * Set: cdBancoDebitoFormatado.
     *
     * @param cdBancoDebitoFormatado the cd banco debito formatado
     */
    public void setCdBancoDebitoFormatado(String cdBancoDebitoFormatado) {
	this.cdBancoDebitoFormatado = cdBancoDebitoFormatado;
    }

    /**
     * Get: cdContaDebitoFormatado.
     *
     * @return cdContaDebitoFormatado
     */
    public String getCdContaDebitoFormatado() {
	return cdContaDebitoFormatado;
    }

    /**
     * Set: cdContaDebitoFormatado.
     *
     * @param cdContaDebitoFormatado the cd conta debito formatado
     */
    public void setCdContaDebitoFormatado(String cdContaDebitoFormatado) {
	this.cdContaDebitoFormatado = cdContaDebitoFormatado;
    }

    /**
     * Get: cdCpfCnpjFormatado.
     *
     * @return cdCpfCnpjFormatado
     */
    public String getCdCpfCnpjFormatado() {
	return cdCpfCnpjFormatado;
    }

    /**
     * Set: cdCpfCnpjFormatado.
     *
     * @param cdCpfCnpjFormatado the cd cpf cnpj formatado
     */
    public void setCdCpfCnpjFormatado(String cdCpfCnpjFormatado) {
	this.cdCpfCnpjFormatado = cdCpfCnpjFormatado;
    }

    /**
     * Get: cdDigitoAgenciaDebitoFormatado.
     *
     * @return cdDigitoAgenciaDebitoFormatado
     */
    public String getCdDigitoAgenciaDebitoFormatado() {
	return cdDigitoAgenciaDebitoFormatado;
    }

    /**
     * Set: cdDigitoAgenciaDebitoFormatado.
     *
     * @param cdDigitoAgenciaDebitoFormatado the cd digito agencia debito formatado
     */
    public void setCdDigitoAgenciaDebitoFormatado(String cdDigitoAgenciaDebitoFormatado) {
	this.cdDigitoAgenciaDebitoFormatado = cdDigitoAgenciaDebitoFormatado;
    }

    /**
     * Get: cdDigitoContaDebitoFormatado.
     *
     * @return cdDigitoContaDebitoFormatado
     */
    public String getCdDigitoContaDebitoFormatado() {
	return cdDigitoContaDebitoFormatado;
    }

    /**
     * Set: cdDigitoContaDebitoFormatado.
     *
     * @param cdDigitoContaDebitoFormatado the cd digito conta debito formatado
     */
    public void setCdDigitoContaDebitoFormatado(String cdDigitoContaDebitoFormatado) {
	this.cdDigitoContaDebitoFormatado = cdDigitoContaDebitoFormatado;
    }

    /**
     * Get: contaDebitoFormatada.
     *
     * @return contaDebitoFormatada
     */
    public String getContaDebitoFormatada() {
	return contaDebitoFormatada;
    }

    /**
     * Set: contaDebitoFormatada.
     *
     * @param contaDebitoFormatada the conta debito formatada
     */
    public void setContaDebitoFormatada(String contaDebitoFormatada) {
	this.contaDebitoFormatada = contaDebitoFormatada;
    }

    /**
     * Get: bancoAgenciaContaCreditoFormatado.
     *
     * @return bancoAgenciaContaCreditoFormatado
     */
    public String getBancoAgenciaContaCreditoFormatado() {
	return bancoAgenciaContaCreditoFormatado;
    }

    /**
     * Get: vlEfetivoPagamentoFormatado.
     *
     * @return vlEfetivoPagamentoFormatado
     */
    public String getVlEfetivoPagamentoFormatado() {
	return vlEfetivoPagamentoFormatado;
    }

    /**
     * Set: vlEfetivoPagamentoFormatado.
     *
     * @param vlEfetivoPagamentoFormatado the vl efetivo pagamento formatado
     */
    public void setVlEfetivoPagamentoFormatado(String vlEfetivoPagamentoFormatado) {
	this.vlEfetivoPagamentoFormatado = vlEfetivoPagamentoFormatado;
    }

    /**
     * Set: bancoAgenciaContaCreditoFormatado.
     *
     * @param bancoAgenciaContaCreditoFormatado the banco agencia conta credito formatado
     */
    public void setBancoAgenciaContaCreditoFormatado(String bancoAgenciaContaCreditoFormatado) {
	this.bancoAgenciaContaCreditoFormatado = bancoAgenciaContaCreditoFormatado;
    }

    /**
     * Get: bancoAgenciaContaDebitoFormatado.
     *
     * @return bancoAgenciaContaDebitoFormatado
     */
    public String getBancoAgenciaContaDebitoFormatado() {
	return bancoAgenciaContaDebitoFormatado;
    }

    /**
     * Set: bancoAgenciaContaDebitoFormatado.
     *
     * @param bancoAgenciaContaDebitoFormatado the banco agencia conta debito formatado
     */
    public void setBancoAgenciaContaDebitoFormatado(String bancoAgenciaContaDebitoFormatado) {
	this.bancoAgenciaContaDebitoFormatado = bancoAgenciaContaDebitoFormatado;
    }

    /**
     * Set: contaCreditoFormatada.
     *
     * @param contaCreditoFormatada the conta credito formatada
     */
    public void setContaCreditoFormatada(String contaCreditoFormatada) {
	this.contaCreditoFormatada = contaCreditoFormatada;
    }

    /**
     * Get: favorecidoBeneficiarioFormatado.
     *
     * @return favorecidoBeneficiarioFormatado
     */
    public String getFavorecidoBeneficiarioFormatado() {
	return favorecidoBeneficiarioFormatado;
    }

    /**
     * Get: cdBancoCreditoFormatado.
     *
     * @return cdBancoCreditoFormatado
     */
    public String getCdBancoCreditoFormatado() {
	return cdBancoCreditoFormatado;
    }

    /**
     * Set: cdBancoCreditoFormatado.
     *
     * @param cdBancoCreditoFormatado the cd banco credito formatado
     */
    public void setCdBancoCreditoFormatado(String cdBancoCreditoFormatado) {
	this.cdBancoCreditoFormatado = cdBancoCreditoFormatado;
    }

    /**
     * Set: favorecidoBeneficiarioFormatado.
     *
     * @param favorecidoBeneficiarioFormatado the favorecido beneficiario formatado
     */
    public void setFavorecidoBeneficiarioFormatado(String favorecidoBeneficiarioFormatado) {
	this.favorecidoBeneficiarioFormatado = favorecidoBeneficiarioFormatado;
    }

    /**
     * Get: cpfCnpjFormatado.
     *
     * @return cpfCnpjFormatado
     */
    public String getCpfCnpjFormatado() {
	return cpfCnpjFormatado;
    }

    /**
     * Set: cpfCnpjFormatado.
     *
     * @param cpfCnpjFormatado the cpf cnpj formatado
     */
    public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
	this.cpfCnpjFormatado = cpfCnpjFormatado;
    }

    /**
     * Get: cdAgenciaCredito.
     *
     * @return cdAgenciaCredito
     */
    public Integer getCdAgenciaCredito() {
	return cdAgenciaCredito;
    }

    /**
     * Set: cdAgenciaCredito.
     *
     * @param cdAgenciaCredito the cd agencia credito
     */
    public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
	this.cdAgenciaCredito = cdAgenciaCredito;
    }

    /**
     * Get: cdAgenciaDebito.
     *
     * @return cdAgenciaDebito
     */
    public Integer getCdAgenciaDebito() {
	return cdAgenciaDebito;
    }

    /**
     * Set: cdAgenciaDebito.
     *
     * @param cdAgenciaDebito the cd agencia debito
     */
    public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
	this.cdAgenciaDebito = cdAgenciaDebito;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public Integer getCdBancoCredito() {
	return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(Integer cdBancoCredito) {
	this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: cdBancoDebito.
     *
     * @return cdBancoDebito
     */
    public Integer getCdBancoDebito() {
	return cdBancoDebito;
    }

    /**
     * Set: cdBancoDebito.
     *
     * @param cdBancoDebito the cd banco debito
     */
    public void setCdBancoDebito(Integer cdBancoDebito) {
	this.cdBancoDebito = cdBancoDebito;
    }

    /**
     * Get: cdContaCredito.
     *
     * @return cdContaCredito
     */
    public Long getCdContaCredito() {
	return cdContaCredito;
    }

    /**
     * Set: cdContaCredito.
     *
     * @param cdContaCredito the cd conta credito
     */
    public void setCdContaCredito(Long cdContaCredito) {
	this.cdContaCredito = cdContaCredito;
    }

    /**
     * Get: cdContaDebito.
     *
     * @return cdContaDebito
     */
    public Long getCdContaDebito() {
	return cdContaDebito;
    }

    /**
     * Set: cdContaDebito.
     *
     * @param cdContaDebito the cd conta debito
     */
    public void setCdContaDebito(Long cdContaDebito) {
	this.cdContaDebito = cdContaDebito;
    }

    /**
     * Get: cdControlePagamento.
     *
     * @return cdControlePagamento
     */
    public String getCdControlePagamento() {
	return cdControlePagamento;
    }

    /**
     * Set: cdControlePagamento.
     *
     * @param cdControlePagamento the cd controle pagamento
     */
    public void setCdControlePagamento(String cdControlePagamento) {
	this.cdControlePagamento = cdControlePagamento;
    }

    /**
     * Get: cdDigitoAgenciaCredito.
     *
     * @return cdDigitoAgenciaCredito
     */
    public Integer getCdDigitoAgenciaCredito() {
	return cdDigitoAgenciaCredito;
    }

    /**
     * Set: cdDigitoAgenciaCredito.
     *
     * @param cdDigitoAgenciaCredito the cd digito agencia credito
     */
    public void setCdDigitoAgenciaCredito(Integer cdDigitoAgenciaCredito) {
	this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
    }

    /**
     * Get: cdDigitoAgenciaDebito.
     *
     * @return cdDigitoAgenciaDebito
     */
    public Integer getCdDigitoAgenciaDebito() {
	return cdDigitoAgenciaDebito;
    }

    /**
     * Set: cdDigitoAgenciaDebito.
     *
     * @param cdDigitoAgenciaDebito the cd digito agencia debito
     */
    public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
	this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
    }

    /**
     * Get: cdDigitoContaCredito.
     *
     * @return cdDigitoContaCredito
     */
    public String getCdDigitoContaCredito() {
	return cdDigitoContaCredito;
    }

    /**
     * Set: cdDigitoContaCredito.
     *
     * @param cdDigitoContaCredito the cd digito conta credito
     */
    public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
	this.cdDigitoContaCredito = cdDigitoContaCredito;
    }

    /**
     * Get: cdDigitoContaDebito.
     *
     * @return cdDigitoContaDebito
     */
    public String getCdDigitoContaDebito() {
	return cdDigitoContaDebito;
    }

    /**
     * Set: cdDigitoContaDebito.
     *
     * @param cdDigitoContaDebito the cd digito conta debito
     */
    public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
	this.cdDigitoContaDebito = cdDigitoContaDebito;
    }

    /**
     * Get: cdEmpresa.
     *
     * @return cdEmpresa
     */
    public Long getCdEmpresa() {
	return cdEmpresa;
    }

    /**
     * Set: cdEmpresa.
     *
     * @param cdEmpresa the cd empresa
     */
    public void setCdEmpresa(Long cdEmpresa) {
	this.cdEmpresa = cdEmpresa;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public String getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(String cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdMotivoSituacaoPagamento.
     *
     * @return cdMotivoSituacaoPagamento
     */
    public Integer getCdMotivoSituacaoPagamento() {
	return cdMotivoSituacaoPagamento;
    }

    /**
     * Set: cdMotivoSituacaoPagamento.
     *
     * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
     */
    public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
	this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoRelacionado.
     *
     * @return cdProdutoServicoRelacionado
     */
    public Integer getCdProdutoServicoRelacionado() {
	return cdProdutoServicoRelacionado;
    }

    /**
     * Set: cdProdutoServicoRelacionado.
     *
     * @param cdProdutoServicoRelacionado the cd produto servico relacionado
     */
    public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
	this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public Integer getCdSituacaoOperacaoPagamento() {
	return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
	this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: cdTipoCanal.
     *
     * @return cdTipoCanal
     */
    public Integer getCdTipoCanal() {
	return cdTipoCanal;
    }

    /**
     * Set: cdTipoCanal.
     *
     * @param cdTipoCanal the cd tipo canal
     */
    public void setCdTipoCanal(Integer cdTipoCanal) {
	this.cdTipoCanal = cdTipoCanal;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoTela.
     *
     * @return cdTipoTela
     */
    public Integer getCdTipoTela() {
	return cdTipoTela;
    }

    /**
     * Set: cdTipoTela.
     *
     * @param cdTipoTela the cd tipo tela
     */
    public void setCdTipoTela(Integer cdTipoTela) {
	this.cdTipoTela = cdTipoTela;
    }

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
	return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
	this.codMensagem = codMensagem;
    }

    /**
     * Get: dsEmpresa.
     *
     * @return dsEmpresa
     */
    public String getDsEmpresa() {
	return dsEmpresa;
    }

    /**
     * Set: dsEmpresa.
     *
     * @param dsEmpresa the ds empresa
     */
    public void setDsEmpresa(String dsEmpresa) {
	this.dsEmpresa = dsEmpresa;
    }

    /**
     * Get: dsFavorecido.
     *
     * @return dsFavorecido
     */
    public String getDsFavorecido() {
	return dsFavorecido;
    }

    /**
     * Set: dsFavorecido.
     *
     * @param dsFavorecido the ds favorecido
     */
    public void setDsFavorecido(String dsFavorecido) {
	this.dsFavorecido = dsFavorecido;
    }

    /**
     * Get: dsMotivoSituacaoPagamento.
     *
     * @return dsMotivoSituacaoPagamento
     */
    public String getDsMotivoSituacaoPagamento() {
	return dsMotivoSituacaoPagamento;
    }

    /**
     * Set: dsMotivoSituacaoPagamento.
     *
     * @param dsMotivoSituacaoPagamento the ds motivo situacao pagamento
     */
    public void setDsMotivoSituacaoPagamento(String dsMotivoSituacaoPagamento) {
	this.dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
    }

    /**
     * Get: dsOperacaoProdutoServico.
     *
     * @return dsOperacaoProdutoServico
     */
    public String getDsOperacaoProdutoServico() {
	return dsOperacaoProdutoServico;
    }

    /**
     * Set: dsOperacaoProdutoServico.
     *
     * @param dsOperacaoProdutoServico the ds operacao produto servico
     */
    public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
	this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    }

    /**
     * Get: dsRazaoSocial.
     *
     * @return dsRazaoSocial
     */
    public String getDsRazaoSocial() {
	return dsRazaoSocial;
    }

    /**
     * Set: dsRazaoSocial.
     *
     * @param dsRazaoSocial the ds razao social
     */
    public void setDsRazaoSocial(String dsRazaoSocial) {
	this.dsRazaoSocial = dsRazaoSocial;
    }

    /**
     * Get: dsResumoProdutoServico.
     *
     * @return dsResumoProdutoServico
     */
    public String getDsResumoProdutoServico() {
	return dsResumoProdutoServico;
    }

    /**
     * Set: dsResumoProdutoServico.
     *
     * @param dsResumoProdutoServico the ds resumo produto servico
     */
    public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
	this.dsResumoProdutoServico = dsResumoProdutoServico;
    }

    /**
     * Get: dsSituacaoOperacaoPagamento.
     *
     * @return dsSituacaoOperacaoPagamento
     */
    public String getDsSituacaoOperacaoPagamento() {
	return dsSituacaoOperacaoPagamento;
    }

    /**
     * Set: dsSituacaoOperacaoPagamento.
     *
     * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
     */
    public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
	this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
    }

    /**
     * Get: dtCreditoPagamento.
     *
     * @return dtCreditoPagamento
     */
    public String getDtCreditoPagamento() {
	return dtCreditoPagamento;
    }

    /**
     * Set: dtCreditoPagamento.
     *
     * @param dtCreditoPagamento the dt credito pagamento
     */
    public void setDtCreditoPagamento(String dtCreditoPagamento) {
	this.dtCreditoPagamento = dtCreditoPagamento;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
	return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
	this.mensagem = mensagem;
    }

    /**
     * Get: nrCnpjCpf.
     *
     * @return nrCnpjCpf
     */
    public Long getNrCnpjCpf() {
	return nrCnpjCpf;
    }

    /**
     * Set: nrCnpjCpf.
     *
     * @param nrCnpjCpf the nr cnpj cpf
     */
    public void setNrCnpjCpf(Long nrCnpjCpf) {
	this.nrCnpjCpf = nrCnpjCpf;
    }

    /**
     * Get: nrContrato.
     *
     * @return nrContrato
     */
    public Long getNrContrato() {
	return nrContrato;
    }

    /**
     * Set: nrContrato.
     *
     * @param nrContrato the nr contrato
     */
    public void setNrContrato(Long nrContrato) {
	this.nrContrato = nrContrato;
    }

    /**
     * Get: nrDigitoCnpjCpf.
     *
     * @return nrDigitoCnpjCpf
     */
    public Integer getNrDigitoCnpjCpf() {
	return nrDigitoCnpjCpf;
    }

    /**
     * Set: nrDigitoCnpjCpf.
     *
     * @param nrDigitoCnpjCpf the nr digito cnpj cpf
     */
    public void setNrDigitoCnpjCpf(Integer nrDigitoCnpjCpf) {
	this.nrDigitoCnpjCpf = nrDigitoCnpjCpf;
    }

    /**
     * Get: nrFilialCnpjCpf.
     *
     * @return nrFilialCnpjCpf
     */
    public Integer getNrFilialCnpjCpf() {
	return nrFilialCnpjCpf;
    }

    /**
     * Set: nrFilialCnpjCpf.
     *
     * @param nrFilialCnpjCpf the nr filial cnpj cpf
     */
    public void setNrFilialCnpjCpf(Integer nrFilialCnpjCpf) {
	this.nrFilialCnpjCpf = nrFilialCnpjCpf;
    }

    /**
     * Get: vlEfetivoPagamento.
     *
     * @return vlEfetivoPagamento
     */
    public BigDecimal getVlEfetivoPagamento() {
	return vlEfetivoPagamento;
    }

    /**
     * Set: vlEfetivoPagamento.
     *
     * @param vlEfetivoPagamento the vl efetivo pagamento
     */
    public void setVlEfetivoPagamento(BigDecimal vlEfetivoPagamento) {
	this.vlEfetivoPagamento = vlEfetivoPagamento;
    }

    /**
     * Is check.
     *
     * @return true, if is check
     */
    public boolean isCheck() {
	return check;
    }

    /**
     * Set: check.
     *
     * @param check the check
     */
    public void setCheck(boolean check) {
	this.check = check;
    }

    /**
     * Get: dsEfetivacaoPagamento.
     *
     * @return dsEfetivacaoPagamento
     */
    public String getDsEfetivacaoPagamento() {
	return dsEfetivacaoPagamento;
    }

    /**
     * Set: dsEfetivacaoPagamento.
     *
     * @param dsEfetivacaoPagamento the ds efetivacao pagamento
     */
    public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
	this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
    }

    /**
     * Get: dsIndicadorPagamento.
     *
     * @return dsIndicadorPagamento
     */
    public String getDsIndicadorPagamento() {
	return dsIndicadorPagamento;
    }

    /**
     * Set: dsIndicadorPagamento.
     *
     * @param dsIndicadorPagamento the ds indicador pagamento
     */
    public void setDsIndicadorPagamento(String dsIndicadorPagamento) {
	this.dsIndicadorPagamento = dsIndicadorPagamento;
    }

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}
}