/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Nome: ConsultarManutencaoPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManutencaoPagamentoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo nrArquivoRemessaPagamento. */
	private String nrArquivoRemessaPagamento;

	/** Atributo cdLote. */
	private String cdLote;

	/** Atributo cdListaDebitoPagamento. */
	private String cdListaDebitoPagamento;

	/** Atributo cdTipoCanal. */
	private Integer cdTipoCanal;

	/** Atributo dsTipoCanal. */
	private String dsTipoCanal;

	/** Atributo vlPagamento. */
	private BigDecimal vlPagamento;

	/** Atributo cdModalidade. */
	private Integer cdModalidade;

	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;

	/** Atributo dsTipoContaDebito. */
	private String dsTipoContaDebito;

	/** Atributo cdSituacaoContrato. */
	private Integer cdSituacaoContrato;

	/** Atributo dsSituacaoContrato. */
	private String dsSituacaoContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo dtVencimento. */
	private String dtVencimento;

	/** Atributo numeroConsultas. */
	private Integer numeroConsultas;

	/** Atributo dtHoraManutencao. */
	private String dtHoraManutencao;

	/** Atributo cdTipoManutencao. */
	private Integer cdTipoManutencao;

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;

	/** Atributo cdTipoCanalLista. */
	private Integer cdTipoCanalLista;

	/** Atributo dsTipoCanalLista. */
	private String dsTipoCanalLista;

	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;

	/** Atributo dsUsuarioManutencao. */
	private String dsUsuarioManutencao;

	/** Atributo cdTipoInscricaoFavorecido. */
	private String cdTipoInscricaoFavorecido;

	/** Atributo inscricaoFavorecido. */
	private Long inscricaoFavorecido;

	/** Atributo dsSituacaoPagamentoCliente. */
	private String dsSituacaoPagamentoCliente;

	/** Atributo dsMotivoPagamentoCliente. */
	private String dsMotivoPagamentoCliente;

	/** Atributo cdBancoCreditoFavorecido. */
	private Integer cdBancoCreditoFavorecido;

	/** Atributo dsBancoCreditoFavorecido. */
	private String dsBancoCreditoFavorecido;

	/** Atributo cdAgenciaBancariaCreditoFavorecido. */
	private Integer cdAgenciaBancariaCreditoFavorecido;

	/** Atributo cdDigitoAgenciaCreditoFavorecido. */
	private Integer cdDigitoAgenciaCreditoFavorecido;

	/** Atributo dsAgenciaCreditoFavorecido. */
	private String dsAgenciaCreditoFavorecido;

	/** Atributo cdContaBancariaCreditoFavorecido. */
	private Long cdContaBancariaCreditoFavorecido;

	/** Atributo cdDigitoContaCreditoFavorecido. */
	private String cdDigitoContaCreditoFavorecido;

	/** Atributo cdTipoContaCreditoFavorecido. */
	private String cdTipoContaCreditoFavorecido;

	/** Atributo bancoFavorecidoFormatado. */
	private String bancoFavorecidoFormatado;

	/** Atributo agenciaFavorecidoFormatada. */
	private String agenciaFavorecidoFormatada;

	/** Atributo contaFavorecidoFormatada. */
	private String contaFavorecidoFormatada;

	/** Atributo dataHoraFormatada. */
	private String dataHoraFormatada;

	/** Atributo vlPagamentoFormatado. */
	private String vlPagamentoFormatado;

	/** Atributo usuarioManutencaoFormatado. */
	private String usuarioManutencaoFormatado;

	/** Atributo dtVencimentoFormatada. */
	private String dtVencimentoFormatada;

	/** Atributo dtPagamento. */
	private Date dtPagamento;

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public Date getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: dtVencimentoFormatada.
	 *
	 * @return dtVencimentoFormatada
	 */
	public String getDtVencimentoFormatada() {
		return dtVencimentoFormatada;
	}

	/**
	 * Set: dtVencimentoFormatada.
	 *
	 * @param dtVencimentoFormatada the dt vencimento formatada
	 */
	public void setDtVencimentoFormatada(String dtVencimentoFormatada) {
		this.dtVencimentoFormatada = dtVencimentoFormatada;
	}

	/**
	 * Get: usuarioManutencaoFormatado.
	 *
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}

	/**
	 * Set: usuarioManutencaoFormatado.
	 *
	 * @param usuarioManutencaoFormatado the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsTipoContaDebito.
	 *
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}

	/**
	 * Set: dsTipoContaDebito.
	 *
	 * @param dsTipoContaDebito the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}

	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}

	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}

	/**
	 * Get: cdListaDebitoPagamento.
	 *
	 * @return cdListaDebitoPagamento
	 */
	public String getCdListaDebitoPagamento() {
		return cdListaDebitoPagamento;
	}

	/**
	 * Set: cdListaDebitoPagamento.
	 *
	 * @param cdListaDebitoPagamento the cd lista debito pagamento
	 */
	public void setCdListaDebitoPagamento(String cdListaDebitoPagamento) {
		this.cdListaDebitoPagamento = cdListaDebitoPagamento;
	}

	/**
	 * Get: cdLote.
	 *
	 * @return cdLote
	 */
	public String getCdLote() {
		return cdLote;
	}

	/**
	 * Set: cdLote.
	 *
	 * @param cdLote the cd lote
	 */
	public void setCdLote(String cdLote) {
		this.cdLote = cdLote;
	}

	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}

	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}

	/**
	 * Get: cdTipoCanalLista.
	 *
	 * @return cdTipoCanalLista
	 */
	public Integer getCdTipoCanalLista() {
		return cdTipoCanalLista;
	}

	/**
	 * Set: cdTipoCanalLista.
	 *
	 * @param cdTipoCanalLista the cd tipo canal lista
	 */
	public void setCdTipoCanalLista(Integer cdTipoCanalLista) {
		this.cdTipoCanalLista = cdTipoCanalLista;
	}

	/**
	 * Get: cdTipoManutencao.
	 *
	 * @return cdTipoManutencao
	 */
	public Integer getCdTipoManutencao() {
		return cdTipoManutencao;
	}

	/**
	 * Set: cdTipoManutencao.
	 *
	 * @param cdTipoManutencao the cd tipo manutencao
	 */
	public void setCdTipoManutencao(Integer cdTipoManutencao) {
		this.cdTipoManutencao = cdTipoManutencao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dataHoraFormatada.
	 *
	 * @return dataHoraFormatada
	 */
	public String getDataHoraFormatada() {
		return dataHoraFormatada;
	}

	/**
	 * Set: dataHoraFormatada.
	 *
	 * @param dataHoraFormatada the data hora formatada
	 */
	public void setDataHoraFormatada(String dataHoraFormatada) {
		this.dataHoraFormatada = dataHoraFormatada;
	}

	/**
	 * Get: dsTipoCanal.
	 *
	 * @return dsTipoCanal
	 */
	public String getDsTipoCanal() {
		return dsTipoCanal;
	}

	/**
	 * Set: dsTipoCanal.
	 *
	 * @param dsTipoCanal the ds tipo canal
	 */
	public void setDsTipoCanal(String dsTipoCanal) {
		this.dsTipoCanal = dsTipoCanal;
	}

	/**
	 * Get: dsTipoCanalLista.
	 *
	 * @return dsTipoCanalLista
	 */
	public String getDsTipoCanalLista() {
		return dsTipoCanalLista;
	}

	/**
	 * Set: dsTipoCanalLista.
	 *
	 * @param dsTipoCanalLista the ds tipo canal lista
	 */
	public void setDsTipoCanalLista(String dsTipoCanalLista) {
		this.dsTipoCanalLista = dsTipoCanalLista;
	}

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: dsUsuarioManutencao.
	 *
	 * @return dsUsuarioManutencao
	 */
	public String getDsUsuarioManutencao() {
		return dsUsuarioManutencao;
	}

	/**
	 * Set: dsUsuarioManutencao.
	 *
	 * @param dsUsuarioManutencao the ds usuario manutencao
	 */
	public void setDsUsuarioManutencao(String dsUsuarioManutencao) {
		this.dsUsuarioManutencao = dsUsuarioManutencao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrArquivoRemessaPagamento.
	 *
	 * @return nrArquivoRemessaPagamento
	 */
	public String getNrArquivoRemessaPagamento() {
		return nrArquivoRemessaPagamento;
	}

	/**
	 * Set: nrArquivoRemessaPagamento.
	 *
	 * @param nrArquivoRemessaPagamento the nr arquivo remessa pagamento
	 */
	public void setNrArquivoRemessaPagamento(String nrArquivoRemessaPagamento) {
		this.nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
	}

	/**
	 * Get: vlPagamento.
	 *
	 * @return vlPagamento
	 */
	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}

	/**
	 * Set: vlPagamento.
	 *
	 * @param vlPagamento the vl pagamento
	 */
	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}

	/**
	 * Get: dtHoraManutencao.
	 *
	 * @return dtHoraManutencao
	 */
	public String getDtHoraManutencao() {
		return dtHoraManutencao;
	}

	/**
	 * Set: dtHoraManutencao.
	 *
	 * @param dtHoraManutencao the dt hora manutencao
	 */
	public void setDtHoraManutencao(String dtHoraManutencao) {
		this.dtHoraManutencao = dtHoraManutencao;
	}

	/**
	 * Get: vlPagamentoFormatado.
	 *
	 * @return vlPagamentoFormatado
	 */
	public String getVlPagamentoFormatado() {
		return vlPagamentoFormatado;
	}

	/**
	 * Set: vlPagamentoFormatado.
	 *
	 * @param vlPagamentoFormatado the vl pagamento formatado
	 */
	public void setVlPagamentoFormatado(String vlPagamentoFormatado) {
		this.vlPagamentoFormatado = vlPagamentoFormatado;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}

	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}

	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public String getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(String cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}

	/**
	 * Get: inscricaoFavorecido.
	 *
	 * @return inscricaoFavorecido
	 */
	public Long getInscricaoFavorecido() {
		return inscricaoFavorecido;
	}

	/**
	 * Set: inscricaoFavorecido.
	 *
	 * @param inscricaoFavorecido the inscricao favorecido
	 */
	public void setInscricaoFavorecido(Long inscricaoFavorecido) {
		this.inscricaoFavorecido = inscricaoFavorecido;
	}

	/**
	 * Get: cdContaBancariaCreditoFavorecido.
	 *
	 * @return cdContaBancariaCreditoFavorecido
	 */
	public Long getCdContaBancariaCreditoFavorecido() {
		return cdContaBancariaCreditoFavorecido;
	}

	/**
	 * Set: cdContaBancariaCreditoFavorecido.
	 *
	 * @param cdContaBancariaCreditoFavorecido the cd conta bancaria credito favorecido
	 */
	public void setCdContaBancariaCreditoFavorecido(Long cdContaBancariaCreditoFavorecido) {
		this.cdContaBancariaCreditoFavorecido = cdContaBancariaCreditoFavorecido;
	}

	/**
	 * Get: cdDigitoContaCreditoFavorecido.
	 *
	 * @return cdDigitoContaCreditoFavorecido
	 */
	public String getCdDigitoContaCreditoFavorecido() {
		return cdDigitoContaCreditoFavorecido;
	}

	/**
	 * Set: cdDigitoContaCreditoFavorecido.
	 *
	 * @param cdDigitoContaCreditoFavorecido the cd digito conta credito favorecido
	 */
	public void setCdDigitoContaCreditoFavorecido(String cdDigitoContaCreditoFavorecido) {
		this.cdDigitoContaCreditoFavorecido = cdDigitoContaCreditoFavorecido;
	}

	/**
	 * Get: cdTipoContaCreditoFavorecido.
	 *
	 * @return cdTipoContaCreditoFavorecido
	 */
	public String getCdTipoContaCreditoFavorecido() {
		return cdTipoContaCreditoFavorecido;
	}

	/**
	 * Set: cdTipoContaCreditoFavorecido.
	 *
	 * @param cdTipoContaCreditoFavorecido the cd tipo conta credito favorecido
	 */
	public void setCdTipoContaCreditoFavorecido(String cdTipoContaCreditoFavorecido) {
		this.cdTipoContaCreditoFavorecido = cdTipoContaCreditoFavorecido;
	}

	/**
	 * Get: dsAgenciaCreditoFavorecido.
	 *
	 * @return dsAgenciaCreditoFavorecido
	 */
	public String getDsAgenciaCreditoFavorecido() {
		return dsAgenciaCreditoFavorecido;
	}

	/**
	 * Set: dsAgenciaCreditoFavorecido.
	 *
	 * @param dsAgenciaCreditoFavorecido the ds agencia credito favorecido
	 */
	public void setDsAgenciaCreditoFavorecido(String dsAgenciaCreditoFavorecido) {
		this.dsAgenciaCreditoFavorecido = dsAgenciaCreditoFavorecido;
	}

	/**
	 * Get: dsBancoCreditoFavorecido.
	 *
	 * @return dsBancoCreditoFavorecido
	 */
	public String getDsBancoCreditoFavorecido() {
		return dsBancoCreditoFavorecido;
	}

	/**
	 * Set: dsBancoCreditoFavorecido.
	 *
	 * @param dsBancoCreditoFavorecido the ds banco credito favorecido
	 */
	public void setDsBancoCreditoFavorecido(String dsBancoCreditoFavorecido) {
		this.dsBancoCreditoFavorecido = dsBancoCreditoFavorecido;
	}

	/**
	 * Get: dsMotivoPagamentoCliente.
	 *
	 * @return dsMotivoPagamentoCliente
	 */
	public String getDsMotivoPagamentoCliente() {
		return dsMotivoPagamentoCliente;
	}

	/**
	 * Set: dsMotivoPagamentoCliente.
	 *
	 * @param dsMotivoPagamentoCliente the ds motivo pagamento cliente
	 */
	public void setDsMotivoPagamentoCliente(String dsMotivoPagamentoCliente) {
		this.dsMotivoPagamentoCliente = dsMotivoPagamentoCliente;
	}

	/**
	 * Get: dsSituacaoPagamentoCliente.
	 *
	 * @return dsSituacaoPagamentoCliente
	 */
	public String getDsSituacaoPagamentoCliente() {
		return dsSituacaoPagamentoCliente;
	}

	/**
	 * Set: dsSituacaoPagamentoCliente.
	 *
	 * @param dsSituacaoPagamentoCliente the ds situacao pagamento cliente
	 */
	public void setDsSituacaoPagamentoCliente(String dsSituacaoPagamentoCliente) {
		this.dsSituacaoPagamentoCliente = dsSituacaoPagamentoCliente;
	}

	/**
	 * Get: cdBancoCreditoFavorecido.
	 *
	 * @return cdBancoCreditoFavorecido
	 */
	public Integer getCdBancoCreditoFavorecido() {
		return cdBancoCreditoFavorecido;
	}

	/**
	 * Set: cdBancoCreditoFavorecido.
	 *
	 * @param cdBancoCreditoFavorecido the cd banco credito favorecido
	 */
	public void setCdBancoCreditoFavorecido(Integer cdBancoCreditoFavorecido) {
		this.cdBancoCreditoFavorecido = cdBancoCreditoFavorecido;
	}

	/**
	 * Get: cdAgenciaBancariaCreditoFavorecido.
	 *
	 * @return cdAgenciaBancariaCreditoFavorecido
	 */
	public Integer getCdAgenciaBancariaCreditoFavorecido() {
		return cdAgenciaBancariaCreditoFavorecido;
	}

	/**
	 * Set: cdAgenciaBancariaCreditoFavorecido.
	 *
	 * @param cdAgenciaBancariaCreditoFavorecido the cd agencia bancaria credito favorecido
	 */
	public void setCdAgenciaBancariaCreditoFavorecido(Integer cdAgenciaBancariaCreditoFavorecido) {
		this.cdAgenciaBancariaCreditoFavorecido = cdAgenciaBancariaCreditoFavorecido;
	}

	/**
	 * Get: agenciaFavorecidoFormatada.
	 *
	 * @return agenciaFavorecidoFormatada
	 */
	public String getAgenciaFavorecidoFormatada() {
		return agenciaFavorecidoFormatada;
	}

	/**
	 * Set: agenciaFavorecidoFormatada.
	 *
	 * @param agenciaFavorecidoFormatada the agencia favorecido formatada
	 */
	public void setAgenciaFavorecidoFormatada(String agenciaFavorecidoFormatada) {
		this.agenciaFavorecidoFormatada = agenciaFavorecidoFormatada;
	}

	/**
	 * Get: bancoFavorecidoFormatado.
	 *
	 * @return bancoFavorecidoFormatado
	 */
	public String getBancoFavorecidoFormatado() {
		return bancoFavorecidoFormatado;
	}

	/**
	 * Set: bancoFavorecidoFormatado.
	 *
	 * @param bancoFavorecidoFormatado the banco favorecido formatado
	 */
	public void setBancoFavorecidoFormatado(String bancoFavorecidoFormatado) {
		this.bancoFavorecidoFormatado = bancoFavorecidoFormatado;
	}

	/**
	 * Get: contaFavorecidoFormatada.
	 *
	 * @return contaFavorecidoFormatada
	 */
	public String getContaFavorecidoFormatada() {
		return contaFavorecidoFormatada;
	}

	/**
	 * Set: contaFavorecidoFormatada.
	 *
	 * @param contaFavorecidoFormatada the conta favorecido formatada
	 */
	public void setContaFavorecidoFormatada(String contaFavorecidoFormatada) {
		this.contaFavorecidoFormatada = contaFavorecidoFormatada;
	}

	/**
	 * Get: cdDigitoAgenciaCreditoFavorecido.
	 *
	 * @return cdDigitoAgenciaCreditoFavorecido
	 */
	public Integer getCdDigitoAgenciaCreditoFavorecido() {
		return cdDigitoAgenciaCreditoFavorecido;
	}

	/**
	 * Set: cdDigitoAgenciaCreditoFavorecido.
	 *
	 * @param cdDigitoAgenciaCreditoFavorecido the cd digito agencia credito favorecido
	 */
	public void setCdDigitoAgenciaCreditoFavorecido(Integer cdDigitoAgenciaCreditoFavorecido) {
		this.cdDigitoAgenciaCreditoFavorecido = cdDigitoAgenciaCreditoFavorecido;
	}
}