/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarMsgLayoutArqRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarMsgLayoutArqRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _nrMensagemArquivoRetorno
     */
    private int _nrMensagemArquivoRetorno = 0;

    /**
     * keeps track of state for field: _nrMensagemArquivoRetorno
     */
    private boolean _has_nrMensagemArquivoRetorno;

    /**
     * Field _cdMensagemArquivoRetorno
     */
    private java.lang.String _cdMensagemArquivoRetorno;

    /**
     * Field _cdTipoMensagemRetorno
     */
    private int _cdTipoMensagemRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemRetorno
     */
    private boolean _has_cdTipoMensagemRetorno;

    /**
     * Field _nrPosicaoInicialMensagem
     */
    private int _nrPosicaoInicialMensagem = 0;

    /**
     * keeps track of state for field: _nrPosicaoInicialMensagem
     */
    private boolean _has_nrPosicaoInicialMensagem;

    /**
     * Field _nrPosicaoFinalMensagem
     */
    private int _nrPosicaoFinalMensagem = 0;

    /**
     * keeps track of state for field: _nrPosicaoFinalMensagem
     */
    private boolean _has_nrPosicaoFinalMensagem;

    /**
     * Field _cdNivelMensagemLayout
     */
    private int _cdNivelMensagemLayout = 0;

    /**
     * keeps track of state for field: _cdNivelMensagemLayout
     */
    private boolean _has_cdNivelMensagemLayout;

    /**
     * Field _dsMensagemLayoutRetorno
     */
    private java.lang.String _dsMensagemLayoutRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarMsgLayoutArqRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.request.AlterarMsgLayoutArqRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdNivelMensagemLayout
     * 
     */
    public void deleteCdNivelMensagemLayout()
    {
        this._has_cdNivelMensagemLayout= false;
    } //-- void deleteCdNivelMensagemLayout() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoMensagemRetorno
     * 
     */
    public void deleteCdTipoMensagemRetorno()
    {
        this._has_cdTipoMensagemRetorno= false;
    } //-- void deleteCdTipoMensagemRetorno() 

    /**
     * Method deleteNrMensagemArquivoRetorno
     * 
     */
    public void deleteNrMensagemArquivoRetorno()
    {
        this._has_nrMensagemArquivoRetorno= false;
    } //-- void deleteNrMensagemArquivoRetorno() 

    /**
     * Method deleteNrPosicaoFinalMensagem
     * 
     */
    public void deleteNrPosicaoFinalMensagem()
    {
        this._has_nrPosicaoFinalMensagem= false;
    } //-- void deleteNrPosicaoFinalMensagem() 

    /**
     * Method deleteNrPosicaoInicialMensagem
     * 
     */
    public void deleteNrPosicaoInicialMensagem()
    {
        this._has_nrPosicaoInicialMensagem= false;
    } //-- void deleteNrPosicaoInicialMensagem() 

    /**
     * Returns the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'cdMensagemArquivoRetorno'.
     */
    public java.lang.String getCdMensagemArquivoRetorno()
    {
        return this._cdMensagemArquivoRetorno;
    } //-- java.lang.String getCdMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'cdNivelMensagemLayout'.
     * 
     * @return int
     * @return the value of field 'cdNivelMensagemLayout'.
     */
    public int getCdNivelMensagemLayout()
    {
        return this._cdNivelMensagemLayout;
    } //-- int getCdNivelMensagemLayout() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoMensagemRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemRetorno'.
     */
    public int getCdTipoMensagemRetorno()
    {
        return this._cdTipoMensagemRetorno;
    } //-- int getCdTipoMensagemRetorno() 

    /**
     * Returns the value of field 'dsMensagemLayoutRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMensagemLayoutRetorno'.
     */
    public java.lang.String getDsMensagemLayoutRetorno()
    {
        return this._dsMensagemLayoutRetorno;
    } //-- java.lang.String getDsMensagemLayoutRetorno() 

    /**
     * Returns the value of field 'nrMensagemArquivoRetorno'.
     * 
     * @return int
     * @return the value of field 'nrMensagemArquivoRetorno'.
     */
    public int getNrMensagemArquivoRetorno()
    {
        return this._nrMensagemArquivoRetorno;
    } //-- int getNrMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'nrPosicaoFinalMensagem'.
     * 
     * @return int
     * @return the value of field 'nrPosicaoFinalMensagem'.
     */
    public int getNrPosicaoFinalMensagem()
    {
        return this._nrPosicaoFinalMensagem;
    } //-- int getNrPosicaoFinalMensagem() 

    /**
     * Returns the value of field 'nrPosicaoInicialMensagem'.
     * 
     * @return int
     * @return the value of field 'nrPosicaoInicialMensagem'.
     */
    public int getNrPosicaoInicialMensagem()
    {
        return this._nrPosicaoInicialMensagem;
    } //-- int getNrPosicaoInicialMensagem() 

    /**
     * Method hasCdNivelMensagemLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelMensagemLayout()
    {
        return this._has_cdNivelMensagemLayout;
    } //-- boolean hasCdNivelMensagemLayout() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoMensagemRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemRetorno()
    {
        return this._has_cdTipoMensagemRetorno;
    } //-- boolean hasCdTipoMensagemRetorno() 

    /**
     * Method hasNrMensagemArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMensagemArquivoRetorno()
    {
        return this._has_nrMensagemArquivoRetorno;
    } //-- boolean hasNrMensagemArquivoRetorno() 

    /**
     * Method hasNrPosicaoFinalMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPosicaoFinalMensagem()
    {
        return this._has_nrPosicaoFinalMensagem;
    } //-- boolean hasNrPosicaoFinalMensagem() 

    /**
     * Method hasNrPosicaoInicialMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPosicaoInicialMensagem()
    {
        return this._has_nrPosicaoInicialMensagem;
    } //-- boolean hasNrPosicaoInicialMensagem() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @param cdMensagemArquivoRetorno the value of field
     * 'cdMensagemArquivoRetorno'.
     */
    public void setCdMensagemArquivoRetorno(java.lang.String cdMensagemArquivoRetorno)
    {
        this._cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
    } //-- void setCdMensagemArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdNivelMensagemLayout'.
     * 
     * @param cdNivelMensagemLayout the value of field
     * 'cdNivelMensagemLayout'.
     */
    public void setCdNivelMensagemLayout(int cdNivelMensagemLayout)
    {
        this._cdNivelMensagemLayout = cdNivelMensagemLayout;
        this._has_cdNivelMensagemLayout = true;
    } //-- void setCdNivelMensagemLayout(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoMensagemRetorno'.
     * 
     * @param cdTipoMensagemRetorno the value of field
     * 'cdTipoMensagemRetorno'.
     */
    public void setCdTipoMensagemRetorno(int cdTipoMensagemRetorno)
    {
        this._cdTipoMensagemRetorno = cdTipoMensagemRetorno;
        this._has_cdTipoMensagemRetorno = true;
    } //-- void setCdTipoMensagemRetorno(int) 

    /**
     * Sets the value of field 'dsMensagemLayoutRetorno'.
     * 
     * @param dsMensagemLayoutRetorno the value of field
     * 'dsMensagemLayoutRetorno'.
     */
    public void setDsMensagemLayoutRetorno(java.lang.String dsMensagemLayoutRetorno)
    {
        this._dsMensagemLayoutRetorno = dsMensagemLayoutRetorno;
    } //-- void setDsMensagemLayoutRetorno(java.lang.String) 

    /**
     * Sets the value of field 'nrMensagemArquivoRetorno'.
     * 
     * @param nrMensagemArquivoRetorno the value of field
     * 'nrMensagemArquivoRetorno'.
     */
    public void setNrMensagemArquivoRetorno(int nrMensagemArquivoRetorno)
    {
        this._nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
        this._has_nrMensagemArquivoRetorno = true;
    } //-- void setNrMensagemArquivoRetorno(int) 

    /**
     * Sets the value of field 'nrPosicaoFinalMensagem'.
     * 
     * @param nrPosicaoFinalMensagem the value of field
     * 'nrPosicaoFinalMensagem'.
     */
    public void setNrPosicaoFinalMensagem(int nrPosicaoFinalMensagem)
    {
        this._nrPosicaoFinalMensagem = nrPosicaoFinalMensagem;
        this._has_nrPosicaoFinalMensagem = true;
    } //-- void setNrPosicaoFinalMensagem(int) 

    /**
     * Sets the value of field 'nrPosicaoInicialMensagem'.
     * 
     * @param nrPosicaoInicialMensagem the value of field
     * 'nrPosicaoInicialMensagem'.
     */
    public void setNrPosicaoInicialMensagem(int nrPosicaoInicialMensagem)
    {
        this._nrPosicaoInicialMensagem = nrPosicaoInicialMensagem;
        this._has_nrPosicaoInicialMensagem = true;
    } //-- void setNrPosicaoInicialMensagem(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarMsgLayoutArqRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.request.AlterarMsgLayoutArqRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.request.AlterarMsgLayoutArqRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.request.AlterarMsgLayoutArqRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.request.AlterarMsgLayoutArqRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
