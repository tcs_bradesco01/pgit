/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class EfetuarValidacaoTarifaOperTipoServicoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class EfetuarValidacaoTarifaOperTipoServicoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdAtividade
     */
    private java.lang.String _cdAtividade;

    /**
     * Field _cdCanal
     */
    private int _cdCanal = 0;

    /**
     * keeps track of state for field: _cdCanal
     */
    private boolean _has_cdCanal;

    /**
     * Field _cdFluxo
     */
    private java.lang.String _cdFluxo;

    /**
     * Field _cdRegra
     */
    private int _cdRegra = 0;

    /**
     * keeps track of state for field: _cdRegra
     */
    private boolean _has_cdRegra;

    /**
     * Field _cdCondicao
     */
    private java.lang.String _cdCondicao;


      //----------------/
     //- Constructors -/
    //----------------/

    public EfetuarValidacaoTarifaOperTipoServicoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.response.EfetuarValidacaoTarifaOperTipoServicoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanal
     * 
     */
    public void deleteCdCanal()
    {
        this._has_cdCanal= false;
    } //-- void deleteCdCanal() 

    /**
     * Method deleteCdRegra
     * 
     */
    public void deleteCdRegra()
    {
        this._has_cdRegra= false;
    } //-- void deleteCdRegra() 

    /**
     * Returns the value of field 'cdAtividade'.
     * 
     * @return String
     * @return the value of field 'cdAtividade'.
     */
    public java.lang.String getCdAtividade()
    {
        return this._cdAtividade;
    } //-- java.lang.String getCdAtividade() 

    /**
     * Returns the value of field 'cdCanal'.
     * 
     * @return int
     * @return the value of field 'cdCanal'.
     */
    public int getCdCanal()
    {
        return this._cdCanal;
    } //-- int getCdCanal() 

    /**
     * Returns the value of field 'cdCondicao'.
     * 
     * @return String
     * @return the value of field 'cdCondicao'.
     */
    public java.lang.String getCdCondicao()
    {
        return this._cdCondicao;
    } //-- java.lang.String getCdCondicao() 

    /**
     * Returns the value of field 'cdFluxo'.
     * 
     * @return String
     * @return the value of field 'cdFluxo'.
     */
    public java.lang.String getCdFluxo()
    {
        return this._cdFluxo;
    } //-- java.lang.String getCdFluxo() 

    /**
     * Returns the value of field 'cdRegra'.
     * 
     * @return int
     * @return the value of field 'cdRegra'.
     */
    public int getCdRegra()
    {
        return this._cdRegra;
    } //-- int getCdRegra() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanal()
    {
        return this._has_cdCanal;
    } //-- boolean hasCdCanal() 

    /**
     * Method hasCdRegra
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegra()
    {
        return this._has_cdRegra;
    } //-- boolean hasCdRegra() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividade'.
     * 
     * @param cdAtividade the value of field 'cdAtividade'.
     */
    public void setCdAtividade(java.lang.String cdAtividade)
    {
        this._cdAtividade = cdAtividade;
    } //-- void setCdAtividade(java.lang.String) 

    /**
     * Sets the value of field 'cdCanal'.
     * 
     * @param cdCanal the value of field 'cdCanal'.
     */
    public void setCdCanal(int cdCanal)
    {
        this._cdCanal = cdCanal;
        this._has_cdCanal = true;
    } //-- void setCdCanal(int) 

    /**
     * Sets the value of field 'cdCondicao'.
     * 
     * @param cdCondicao the value of field 'cdCondicao'.
     */
    public void setCdCondicao(java.lang.String cdCondicao)
    {
        this._cdCondicao = cdCondicao;
    } //-- void setCdCondicao(java.lang.String) 

    /**
     * Sets the value of field 'cdFluxo'.
     * 
     * @param cdFluxo the value of field 'cdFluxo'.
     */
    public void setCdFluxo(java.lang.String cdFluxo)
    {
        this._cdFluxo = cdFluxo;
    } //-- void setCdFluxo(java.lang.String) 

    /**
     * Sets the value of field 'cdRegra'.
     * 
     * @param cdRegra the value of field 'cdRegra'.
     */
    public void setCdRegra(int cdRegra)
    {
        this._cdRegra = cdRegra;
        this._has_cdRegra = true;
    } //-- void setCdRegra(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return EfetuarValidacaoTarifaOperTipoServicoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.response.EfetuarValidacaoTarifaOperTipoServicoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.response.EfetuarValidacaoTarifaOperTipoServicoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.response.EfetuarValidacaoTarifaOperTipoServicoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.response.EfetuarValidacaoTarifaOperTipoServicoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
