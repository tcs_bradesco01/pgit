/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarPagtoSolEmiCompFavorecidoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarPagtoSolEmiCompFavorecidoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtCreditoPagamentoInicio
     */
    private java.lang.String _dtCreditoPagamentoInicio;

    /**
     * Field _dtCreditoPagamentoFim
     */
    private java.lang.String _dtCreditoPagamentoFim;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _cdControlePagamentoDe
     */
    private java.lang.String _cdControlePagamentoDe;

    /**
     * Field _cdControlePagamentoAte
     */
    private java.lang.String _cdControlePagamentoAte;

    /**
     * Field _vlPagamentoDe
     */
    private java.math.BigDecimal _vlPagamentoDe = new java.math.BigDecimal("0");

    /**
     * Field _vlPagamentoAte
     */
    private java.math.BigDecimal _vlPagamentoAte = new java.math.BigDecimal("0");

    /**
     * Field _cdFavorecidoClientePagador
     */
    private long _cdFavorecidoClientePagador = 0;

    /**
     * keeps track of state for field: _cdFavorecidoClientePagador
     */
    private boolean _has_cdFavorecidoClientePagador;

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _cdIdentificacaoInscricaoFavorecido
     */
    private int _cdIdentificacaoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificacaoInscricaoFavorecido
     */
    private boolean _has_cdIdentificacaoInscricaoFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarPagtoSolEmiCompFavorecidoRequest() 
     {
        super();
        setVlPagamentoDe(new java.math.BigDecimal("0"));
        setVlPagamentoAte(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.request.ConsultarPagtoSolEmiCompFavorecidoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFavorecidoClientePagador
     * 
     */
    public void deleteCdFavorecidoClientePagador()
    {
        this._has_cdFavorecidoClientePagador= false;
    } //-- void deleteCdFavorecidoClientePagador() 

    /**
     * Method deleteCdIdentificacaoInscricaoFavorecido
     * 
     */
    public void deleteCdIdentificacaoInscricaoFavorecido()
    {
        this._has_cdIdentificacaoInscricaoFavorecido= false;
    } //-- void deleteCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdControlePagamentoAte'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamentoAte'.
     */
    public java.lang.String getCdControlePagamentoAte()
    {
        return this._cdControlePagamentoAte;
    } //-- java.lang.String getCdControlePagamentoAte() 

    /**
     * Returns the value of field 'cdControlePagamentoDe'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamentoDe'.
     */
    public java.lang.String getCdControlePagamentoDe()
    {
        return this._cdControlePagamentoDe;
    } //-- java.lang.String getCdControlePagamentoDe() 

    /**
     * Returns the value of field 'cdFavorecidoClientePagador'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoClientePagador'.
     */
    public long getCdFavorecidoClientePagador()
    {
        return this._cdFavorecidoClientePagador;
    } //-- long getCdFavorecidoClientePagador() 

    /**
     * Returns the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public int getCdIdentificacaoInscricaoFavorecido()
    {
        return this._cdIdentificacaoInscricaoFavorecido;
    } //-- int getCdIdentificacaoInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtCreditoPagamentoFim'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamentoFim'.
     */
    public java.lang.String getDtCreditoPagamentoFim()
    {
        return this._dtCreditoPagamentoFim;
    } //-- java.lang.String getDtCreditoPagamentoFim() 

    /**
     * Returns the value of field 'dtCreditoPagamentoInicio'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamentoInicio'.
     */
    public java.lang.String getDtCreditoPagamentoInicio()
    {
        return this._dtCreditoPagamentoInicio;
    } //-- java.lang.String getDtCreditoPagamentoInicio() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'vlPagamentoAte'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoAte'.
     */
    public java.math.BigDecimal getVlPagamentoAte()
    {
        return this._vlPagamentoAte;
    } //-- java.math.BigDecimal getVlPagamentoAte() 

    /**
     * Returns the value of field 'vlPagamentoDe'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoDe'.
     */
    public java.math.BigDecimal getVlPagamentoDe()
    {
        return this._vlPagamentoDe;
    } //-- java.math.BigDecimal getVlPagamentoDe() 

    /**
     * Method hasCdFavorecidoClientePagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoClientePagador()
    {
        return this._has_cdFavorecidoClientePagador;
    } //-- boolean hasCdFavorecidoClientePagador() 

    /**
     * Method hasCdIdentificacaoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificacaoInscricaoFavorecido()
    {
        return this._has_cdIdentificacaoInscricaoFavorecido;
    } //-- boolean hasCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControlePagamentoAte'.
     * 
     * @param cdControlePagamentoAte the value of field
     * 'cdControlePagamentoAte'.
     */
    public void setCdControlePagamentoAte(java.lang.String cdControlePagamentoAte)
    {
        this._cdControlePagamentoAte = cdControlePagamentoAte;
    } //-- void setCdControlePagamentoAte(java.lang.String) 

    /**
     * Sets the value of field 'cdControlePagamentoDe'.
     * 
     * @param cdControlePagamentoDe the value of field
     * 'cdControlePagamentoDe'.
     */
    public void setCdControlePagamentoDe(java.lang.String cdControlePagamentoDe)
    {
        this._cdControlePagamentoDe = cdControlePagamentoDe;
    } //-- void setCdControlePagamentoDe(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecidoClientePagador'.
     * 
     * @param cdFavorecidoClientePagador the value of field
     * 'cdFavorecidoClientePagador'.
     */
    public void setCdFavorecidoClientePagador(long cdFavorecidoClientePagador)
    {
        this._cdFavorecidoClientePagador = cdFavorecidoClientePagador;
        this._has_cdFavorecidoClientePagador = true;
    } //-- void setCdFavorecidoClientePagador(long) 

    /**
     * Sets the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @param cdIdentificacaoInscricaoFavorecido the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public void setCdIdentificacaoInscricaoFavorecido(int cdIdentificacaoInscricaoFavorecido)
    {
        this._cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
        this._has_cdIdentificacaoInscricaoFavorecido = true;
    } //-- void setCdIdentificacaoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dtCreditoPagamentoFim'.
     * 
     * @param dtCreditoPagamentoFim the value of field
     * 'dtCreditoPagamentoFim'.
     */
    public void setDtCreditoPagamentoFim(java.lang.String dtCreditoPagamentoFim)
    {
        this._dtCreditoPagamentoFim = dtCreditoPagamentoFim;
    } //-- void setDtCreditoPagamentoFim(java.lang.String) 

    /**
     * Sets the value of field 'dtCreditoPagamentoInicio'.
     * 
     * @param dtCreditoPagamentoInicio the value of field
     * 'dtCreditoPagamentoInicio'.
     */
    public void setDtCreditoPagamentoInicio(java.lang.String dtCreditoPagamentoInicio)
    {
        this._dtCreditoPagamentoInicio = dtCreditoPagamentoInicio;
    } //-- void setDtCreditoPagamentoInicio(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'vlPagamentoAte'.
     * 
     * @param vlPagamentoAte the value of field 'vlPagamentoAte'.
     */
    public void setVlPagamentoAte(java.math.BigDecimal vlPagamentoAte)
    {
        this._vlPagamentoAte = vlPagamentoAte;
    } //-- void setVlPagamentoAte(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoDe'.
     * 
     * @param vlPagamentoDe the value of field 'vlPagamentoDe'.
     */
    public void setVlPagamentoDe(java.math.BigDecimal vlPagamentoDe)
    {
        this._vlPagamentoDe = vlPagamentoDe;
    } //-- void setVlPagamentoDe(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarPagtoSolEmiCompFavorecidoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.request.ConsultarPagtoSolEmiCompFavorecidoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.request.ConsultarPagtoSolEmiCompFavorecidoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.request.ConsultarPagtoSolEmiCompFavorecidoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.request.ConsultarPagtoSolEmiCompFavorecidoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
