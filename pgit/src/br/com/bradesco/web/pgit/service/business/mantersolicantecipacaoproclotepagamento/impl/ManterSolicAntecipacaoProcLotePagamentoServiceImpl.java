/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.impl;

import static br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.IManterSolicAntecipacaoProcLotePagamentoServiceConstants.QUANTIDADE_OCORRENCIAS;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaBigDecimalNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataData;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataTimestampFromPdc;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.OcorrenciasLotePagamentosDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.IManterSolicAntecipacaoProcLotePagamentoService;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean.ConsultarLotesIncSolicEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean.ConsultarLotesIncSolicSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.request.ConsultarLotesIncSolicRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.response.ConsultarLotesIncSolicResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicantproclotepgtoagda.request.ConsultarSolicAntProcLotePgtoAgdaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicantproclotepgtoagda.response.ConsultarSolicAntProcLotePgtoAgdaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantproclotepgtoagda.request.DetalharSolicAntProcLotePgtoAgdaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantproclotepgtoagda.response.DetalharSolicAntProcLotePgtoAgdaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicantproclotepgtoagda.request.ExcluirSolicAntProcLotePgtoAgdaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicantproclotepgtoagda.response.ExcluirSolicAntProcLotePgtoAgdaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicantprocagda.request.IncluirSolicAntProcAgdaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicantprocagda.response.IncluirSolicAntProcAgdaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManterSolicAntecipacaoProcLotePagamentoServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador ManterSolicAntecipacaoProcLotePagamento
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IManterSolicAntecipacaoProcLotePagamentoService
 */
public class ManterSolicAntecipacaoProcLotePagamentoServiceImpl implements
		IManterSolicAntecipacaoProcLotePagamentoService {

	private FactoryAdapter factoryAdapter;

	public ConsultarLotePagamentosSaidaDTO consultarSolicAntProcLotePgtoAgda(
			ConsultarLotePagamentosEntradaDTO entrada) {

		ConsultarLotePagamentosSaidaDTO saida = new ConsultarLotePagamentosSaidaDTO();
		ConsultarSolicAntProcLotePgtoAgdaRequest request = new ConsultarSolicAntProcLotePgtoAgdaRequest();
		ConsultarSolicAntProcLotePgtoAgdaResponse response = null;

		request.setNrOcorrencias(QUANTIDADE_OCORRENCIAS);
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request
				.setDtPgtoInicial(verificaStringNula(entrada.getDtPgtoInicial()));
		request.setDtPgtoFinal(verificaStringNula(entrada.getDtPgtoFinal()));
		request.setCdSituacaoSolicitacaoPagamento(verificaIntegerNulo(entrada
				.getCdSituacaoSolicitacaoPagamento()));
		request.setCdMotivoSolicitacao(verificaIntegerNulo(entrada
				.getCdMotivoSolicitacao()));

		response = getFactoryAdapter()
				.getConsultarSolicAntProcLotePgtoAgdaPDCAdapter()
				.invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());

		List<OcorrenciasLotePagamentosDTO> ocorrencias = new ArrayList<OcorrenciasLotePagamentosDTO>();

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			OcorrenciasLotePagamentosDTO ocorrencia = new OcorrenciasLotePagamentosDTO();

			ocorrencia.setCdSolicitacaoPagamentoIntegrado(response
					.getOcorrencias(i).getCdSolicitacaoPagamentoIntegrado());
			ocorrencia.setNrSolicitacaoPagamentoIntegrado(response
					.getOcorrencias(i).getNrSolicitacaoPagamentoIntegrado());
			ocorrencia.setCdpessoaJuridicaContrato(response.getOcorrencias(i)
					.getCdpessoaJuridicaContrato());
			ocorrencia.setCdTipoContratoNegocio(response.getOcorrencias(i)
					.getCdTipoContratoNegocio());
			ocorrencia.setNrSequenciaContratoNegocio(response.getOcorrencias(i)
					.getNrSequenciaContratoNegocio());
			ocorrencia.setNrLoteInterno(response.getOcorrencias(i)
					.getNrLoteInterno());
			ocorrencia.setCdTipoLayoutArquivo(response.getOcorrencias(i)
					.getCdTipoLayoutArquivo());
			ocorrencia.setDsTipoLayoutArquivo(response.getOcorrencias(i)
					.getDsTipoLayoutArquivo());
			ocorrencia.setCdSituacaoSolicitacaoPagamento(response
					.getOcorrencias(i).getCdSituacaoSolicitacaoPagamento());
			ocorrencia.setDsSituacaoSolicitaoPgto(response.getOcorrencias(i)
					.getDsSituacaoSolicitaoPgto());
			ocorrencia.setCdMotivoSolicitacao(response.getOcorrencias(i)
					.getCdMotivoSolicitacao());
			ocorrencia.setDsMotivoSolicitacao(response.getOcorrencias(i)
					.getDsMotivoSolicitacao());
			ocorrencia.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
			ocorrencia.setDsSolicitacao(response.getOcorrencias(i)
					.getDsSolicitacao());
			ocorrencia.setHrSolicitacao(response.getOcorrencias(i)
					.getHrSolicitacao());
			ocorrencia.setCdProdutoServicoOperacao(response.getOcorrencias(i)
					.getCdProdutoServicoOperacao());
			ocorrencia.setDsProdutoServicoOperacao(response.getOcorrencias(i)
					.getDsProdutoServicoOperacao());
			ocorrencia.setDataHoraFormatada(PgitUtil.concatenarCampos(response.getOcorrencias(i).getDsSolicitacao(), response.getOcorrencias(i).getHrSolicitacao(), "-"));

			ocorrencias.add(ocorrencia);
		}

		saida.setOcorrencias(ocorrencias);

		return saida;
	}

	public DetalharLotePagamentosSaidaDTO detalharSolicAntProcLotePgtoAgda(
			DetalharLotePagamentosEntradaDTO entrada) {

		DetalharLotePagamentosSaidaDTO saida = new DetalharLotePagamentosSaidaDTO();
		DetalharSolicAntProcLotePgtoAgdaRequest request = new DetalharSolicAntProcLotePgtoAgdaRequest();
		DetalharSolicAntProcLotePgtoAgdaResponse response = null;

		request.setCdSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entrada
						.getCdSolicitacaoPagamentoIntegrado()));
		request.setNrSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entrada
						.getNrSolicitacaoPagamentoIntegrado()));

		response = getFactoryAdapter()
				.getDetalharSolicAntProcLotePgtoAgdaPDCAdapter().invokeProcess(
						request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdpessoaJuridicaContrato(response
				.getCdpessoaJuridicaContrato());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio());
		saida.setCdSolicitacaoPagamentoIntegrado(response
				.getCdSolicitacaoPagamentoIntegrado());
		saida.setNrSolicitacaoPagamentoIntegrado(response
				.getNrSolicitacaoPagamentoIntegrado());
		saida.setDsSituacaoSolicitaoPgto(response.getDsSituacaoSolicitaoPgto());
		saida.setDsMotivoSolicitacao(response.getDsMotivoSolicitacao());
		saida.setDsSolicitacao(response.getDsSolicitacao());
		saida.setHrSolicitacao(response.getHrSolicitacao());
		saida.setNrLoteInterno(response.getNrLoteInterno());
		saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saida.setFormatDataHora(response.getDsSolicitacao() + response.getHrSolicitacao());
        if(saida.getDsSituacaoSolicitaoPgto().toUpperCase().equals("PENDENTE")){
        	saida.setFormatDataHora(null);        	
        }    
        if(StringUtils.isNotEmpty(saida.getFormatDataHora())){
        	saida.setFormatDataHora(String.format("%s - %s", response.getDsSolicitacao(), response.getHrSolicitacao()));	
        }
		saida.setDtPgtoInicial(response.getDtPgtoInicial());
		saida.setDtPgtoFinal(response.getDtPgtoFinal());
		saida.setCdBancoDebito(response.getCdBancoDebito());
		saida.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saida.setCdContaDebito(response.getCdContaDebito());
		saida.setDsProdutoServicoOperacao(response
				.getDsProdutoServicoOperacao());
		saida.setDsModalidadePgtoCliente(response.getDsModalidadePgtoCliente());

		saida.setQtdeTotalPagtoPrevistoSoltc(response
				.getQtdeTotalPagtoPrevistoSoltc());
		saida.setVlrTotPagtoPrevistoSolicitacao(response
				.getVlrTotPagtoPrevistoSolicitacao());

		saida.setQtTotalPgtoEfetivadoSolicitacao(response
				.getQtTotalPgtoEfetivadoSolicitacao());
		saida.setVlTotalPgtoEfetuadoSolicitacao(response
				.getVlTotalPgtoEfetuadoSolicitacao());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setCdAutenticacaoSegurancaInclusao(response
				.getCdAutenticacaoSegurancaInclusao());
		saida.setHrInclusaoRegistro(formataTimestampFromPdc(formataData(
				response.getHrInclusaoRegistro(), "yyyy-MM-dd-HH.mm.ss")));
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setCdAutenticacaoSegurancaManutencao(response
				.getCdAutenticacaoSegurancaManutencao());
		saida.setNmOperacaoFluxoManutencao(response
				.getNmOperacaoFluxoManutencao());
		saida.setHrManutencaoRegistro(formataTimestampFromPdc(formataData(
				response.getHrManutencaoRegistro(), "yyyy-MM-dd-HH.mm.ss")));

		return saida;
	}

	public ExcluirLotePagamentosSaidaDTO excluirSolicAntProcLotePgtoAgda(
			ExcluirLotePagamentosEntradaDTO entrada) {

		ExcluirLotePagamentosSaidaDTO saida = new ExcluirLotePagamentosSaidaDTO();
		ExcluirSolicAntProcLotePgtoAgdaRequest request = new ExcluirSolicAntProcLotePgtoAgdaRequest();
		ExcluirSolicAntProcLotePgtoAgdaResponse response = null;

		request.setCdSolicitacaoPagamentoIntegrado(entrada
				.getCdSolicitacaoPagamentoIntegrado());
		request.setNrSolicitacaoPagamentoIntegrado(entrada
				.getNrSolicitacaoPagamentoIntegrado());

		response = getFactoryAdapter()
				.getExcluirSolicAntProcLotePgtoAgdaPDCAdapter().invokeProcess(
						request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	public IncluirLotePagamentosSaidaDTO incluirSolicAntProcAgda(
			IncluirLotePagamentosEntradaDTO entrada) {

		IncluirLotePagamentosSaidaDTO saida = new IncluirLotePagamentosSaidaDTO();
		IncluirSolicAntProcAgdaRequest request = new IncluirSolicAntProcAgdaRequest();
		IncluirSolicAntProcAgdaResponse response = null;

		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
		request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request
				.setDtPgtoInicial(verificaStringNula(entrada.getDtPgtoInicial()));
		request.setDtPgtoFinal(verificaStringNula(entrada.getDtPgtoFinal()));
		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada
				.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entrada
				.getCdProdutoServicoRelacionado()));
		request
				.setCdBancoDebito(verificaIntegerNulo(entrada
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(verificaIntegerNulo(entrada
				.getCdAgenciaDebito()));
		request.setCdContaDebito(verificaLongNulo(entrada.getCdContaDebito()));
		request.setQtdeTotalPagtoPrevistoSoltc(verificaLongNulo(entrada
				.getQtdeTotalPagtoPrevistoSoltc()));
		request
				.setVlrTotPagtoPrevistoSolicitacao(verificaBigDecimalNulo(entrada
						.getVlrTotPagtoPrevistoSolicitacao()));
		request.setCdTipoInscricaoPagador(verificaIntegerNulo(entrada
				.getCdTipoInscricaoPagador()));
		request.setCdCpfCnpjPagador(verificaLongNulo(entrada
				.getCdCpfCnpjPagador()));
		request.setCdFilialCpfCnpjPagador(verificaIntegerNulo(entrada
				.getCdFilialCpfCnpjPagador()));
		request.setCdControleCpfCnpjPagador(verificaIntegerNulo(entrada
				.getCdControleCpfCnpjPagador()));
		request.setCdTipoCtaRecebedor(verificaIntegerNulo(entrada
				.getCdTipoCtaRecebedor()));
		request.setCdBcoRecebedor(verificaIntegerNulo(entrada
				.getCdBcoRecebedor()));
		request.setCdAgenciaRecebedor(verificaIntegerNulo(entrada
				.getCdAgenciaRecebedor()));
		request.setCdContaRecebedor(verificaLongNulo(entrada
				.getCdContaRecebedor()));
		request.setCdTipoInscricaoRecebedor(verificaIntegerNulo(entrada
				.getCdTipoInscricaoRecebedor()));
		request.setCdCpfCnpjRecebedor(PgitUtil.verificaLongNulo(entrada
				.getCdCpfCnpjRecebedor()));
		request.setCdFilialCnpjRecebedor(verificaIntegerNulo(entrada
				.getCdFilialCnpjRecebedor()));
		request.setCdControleCpfRecebedor(verificaIntegerNulo(entrada
				.getCdControleCpfRecebedor()));

		response = getFactoryAdapter().getIncluirSolicAntProcAgdaPDCAdapter()
				.invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
	
	public List<ConsultarLotesIncSolicSaidaDTO> consultarLotes(
			ConsultarLotesIncSolicEntradaDTO entrada) {
		List<ConsultarLotesIncSolicSaidaDTO> listaOcorrencias = new ArrayList<ConsultarLotesIncSolicSaidaDTO>();
		ConsultarLotesIncSolicRequest request = new ConsultarLotesIncSolicRequest();
		ConsultarLotesIncSolicResponse response = new ConsultarLotesIncSolicResponse();

		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entrada
						.getCdSolicitacaoPagamentoIntegrado()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrLoteInterno(PgitUtil.verificaLongNulo(entrada
				.getNrLoteInterno()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		
		response = getFactoryAdapter().getConsultarLotesIncSolicPDCAdapter()
		.invokeProcess(request);
		
		
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ConsultarLotesIncSolicSaidaDTO ocorrencia = new ConsultarLotesIncSolicSaidaDTO();
			
			ocorrencia.setCdIndicadorLoteApto(response.getOcorrencias(i).getCdIndicadorLoteApto());
			ocorrencia.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			ocorrencia.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
			ocorrencia.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			ocorrencia.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
			ocorrencia.setNrLoteInterno(response.getOcorrencias(i).getNrLoteInterno());
			ocorrencia.setQtTotalPagamentoLote(response.getOcorrencias(i).getQtTotalPagamentoLote());
			ocorrencia.setVlTotalPagamentoLote(response.getOcorrencias(i).getVlTotalPagamentoLote());
			
			listaOcorrencias.add(ocorrencia);
		}
		
		return listaOcorrencias;
	}

	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

}