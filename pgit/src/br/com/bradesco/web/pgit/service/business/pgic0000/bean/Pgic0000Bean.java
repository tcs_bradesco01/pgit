/*
 * Nome: br.com.bradesco.web.pgit.service.business.pgic0000.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.pgic0000.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO;

/**
 * Nome: Pgic0000Bean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class Pgic0000Bean {
	
	/** Atributo mostraBotoes0000. */
	private boolean mostraBotoes0000;
	
	/** Atributo hiddenRadioContrato. */
	private int hiddenRadioContrato;		
	
	/** Atributo hiddenRadioFiltroArgumento. */
	private int hiddenRadioFiltroArgumento;	
	
	/** Atributo listaGridContrato. */
	private List<ListarClientesDTO> listaGridContrato;
	
	/** Atributo empresaFiltro. */
	private String empresaFiltro;
	
	/** Atributo tipoFiltro. */
	private String tipoFiltro;
	
	/** Atributo numeroFiltro. */
	private String numeroFiltro;
	
	/** Atributo listaEmpresa. */
	private List<SelectItem> listaEmpresa = new ArrayList<SelectItem>();
	
	/** Atributo listaTipo. */
	private List<SelectItem> listaTipo = new ArrayList<SelectItem>();
	
	/** Atributo agenciaOperadoraFiltro. */
	private String agenciaOperadoraFiltro;
	
	/** Atributo gerenteResponsavelFiltro. */
	private String gerenteResponsavelFiltro;
	
	/** Atributo listaGerenteResponsavel. */
	private List<SelectItem> listaGerenteResponsavel = new ArrayList<SelectItem>();
	
	/** Atributo dataCadastramentoFiltro. */
	private String dataCadastramentoFiltro;
	
	/** Atributo situacaoContratoFiltro. */
	private String situacaoContratoFiltro;
	
	/** Atributo listaSituacaoContrato. */
	private List<SelectItem> listaSituacaoContrato = new ArrayList<SelectItem>();
	
	/** Atributo tela. */
	private String tela;
	
	/** Atributo empresa. */
	private String empresa;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo numero. */
	private String numero;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacao. */
	private String situacao;
	
	/** Atributo operacao. */
	private String operacao;
	
	/** Atributo layout. */
	private String layout;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	
	/**
	 * Is mostra botoes0000.
	 *
	 * @return true, if is mostra botoes0000
	 */
	public boolean isMostraBotoes0000() {
		return mostraBotoes0000;
	}

	/**
	 * Set: mostraBotoes0000.
	 *
	 * @param mostraBotoes0000 the mostra botoes0000
	 */
	public void setMostraBotoes0000(boolean mostraBotoes0000) {
		this.mostraBotoes0000 = mostraBotoes0000;
	}

	/**
	 * Limpar lista contratos.
	 */
	public void limparListaContratos(){
		this.listaGridContrato = null;
	}

	/**
	 * Get: listaGridContrato.
	 *
	 * @return listaGridContrato
	 */
	public List<ListarClientesDTO> getListaGridContrato() {
		return listaGridContrato;
	}

	/**
	 * Set: listaGridContrato.
	 *
	 * @param listaGridContrato the lista grid contrato
	 */
	public void setListaGridContrato(List<ListarClientesDTO> listaGridContrato) {
		this.listaGridContrato = listaGridContrato;
	}

	/**
	 * Get: hiddenRadioContrato.
	 *
	 * @return hiddenRadioContrato
	 */
	public int getHiddenRadioContrato() {
		return hiddenRadioContrato;
	}

	/**
	 * Set: hiddenRadioContrato.
	 *
	 * @param hiddenRadioContrato the hidden radio contrato
	 */
	public void setHiddenRadioContrato(int hiddenRadioContrato) {
		this.hiddenRadioContrato = hiddenRadioContrato;
	}

	/**
	 * Get: hiddenRadioFiltroArgumento.
	 *
	 * @return hiddenRadioFiltroArgumento
	 */
	public int getHiddenRadioFiltroArgumento() {
		return hiddenRadioFiltroArgumento;
	}

	/**
	 * Set: hiddenRadioFiltroArgumento.
	 *
	 * @param hiddenRadioFiltroArgumento the hidden radio filtro argumento
	 */
	public void setHiddenRadioFiltroArgumento(int hiddenRadioFiltroArgumento) {
		this.hiddenRadioFiltroArgumento = hiddenRadioFiltroArgumento;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: listaEmpresa.
	 *
	 * @return listaEmpresa
	 */
	public List<SelectItem> getListaEmpresa() {
		return listaEmpresa;
	}

	/**
	 * Set: listaEmpresa.
	 *
	 * @param listaEmpresa the lista empresa
	 */
	public void setListaEmpresa(List<SelectItem> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Get: listaTipo.
	 *
	 * @return listaTipo
	 */
	public List<SelectItem> getListaTipo() {
		return listaTipo;
	}

	/**
	 * Set: listaTipo.
	 *
	 * @param listaTipo the lista tipo
	 */
	public void setListaTipo(List<SelectItem> listaTipo) {
		this.listaTipo = listaTipo;
	}

	/**
	 * Get: listaGerenteResponsavel.
	 *
	 * @return listaGerenteResponsavel
	 */
	public List<SelectItem> getListaGerenteResponsavel() {
		return listaGerenteResponsavel;
	}

	/**
	 * Set: listaGerenteResponsavel.
	 *
	 * @param listaGerenteResponsavel the lista gerente responsavel
	 */
	public void setListaGerenteResponsavel(List<SelectItem> listaGerenteResponsavel) {
		this.listaGerenteResponsavel = listaGerenteResponsavel;
	}

	/**
	 * Get: listaSituacaoContrato.
	 *
	 * @return listaSituacaoContrato
	 */
	public List<SelectItem> getListaSituacaoContrato() {
		return listaSituacaoContrato;
	}

	/**
	 * Set: listaSituacaoContrato.
	 *
	 * @param listaSituacaoContrato the lista situacao contrato
	 */
	public void setListaSituacaoContrato(List<SelectItem> listaSituacaoContrato) {
		this.listaSituacaoContrato = listaSituacaoContrato;
	}
	
	/**
	 * Limpar dados contrato.
	 *
	 * @return the string
	 */
	public String limparDadosContrato() {
	    this.setAgenciaOperadoraFiltro("");
	    this.setGerenteResponsavelFiltro("");
	    this.setDataCadastramentoFiltro("");
	    this.setSituacaoContratoFiltro("");
		this.setEmpresaFiltro("");
		this.setTipoFiltro("");
	    this.setNumeroFiltro("");
		setEmpresa("");
		setTipo("");
	    setNumero("");
	    setDescricaoContrato("");
	    setSituacao("");
	    
		return "ok";
		
	}

	/**
	 * Get: agenciaOperadoraFiltro.
	 *
	 * @return agenciaOperadoraFiltro
	 */
	public String getAgenciaOperadoraFiltro() {
		return agenciaOperadoraFiltro;
	}

	/**
	 * Set: agenciaOperadoraFiltro.
	 *
	 * @param agenciaOperadoraFiltro the agencia operadora filtro
	 */
	public void setAgenciaOperadoraFiltro(String agenciaOperadoraFiltro) {
		this.agenciaOperadoraFiltro = agenciaOperadoraFiltro;
	}

	/**
	 * Get: dataCadastramentoFiltro.
	 *
	 * @return dataCadastramentoFiltro
	 */
	public String getDataCadastramentoFiltro() {
		return dataCadastramentoFiltro;
	}

	/**
	 * Set: dataCadastramentoFiltro.
	 *
	 * @param dataCadastramentoFiltro the data cadastramento filtro
	 */
	public void setDataCadastramentoFiltro(String dataCadastramentoFiltro) {
		this.dataCadastramentoFiltro = dataCadastramentoFiltro;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: empresaFiltro.
	 *
	 * @return empresaFiltro
	 */
	public String getEmpresaFiltro() {
		return empresaFiltro;
	}

	/**
	 * Set: empresaFiltro.
	 *
	 * @param empresaFiltro the empresa filtro
	 */
	public void setEmpresaFiltro(String empresaFiltro) {
		this.empresaFiltro = empresaFiltro;
	}

	/**
	 * Get: gerenteResponsavelFiltro.
	 *
	 * @return gerenteResponsavelFiltro
	 */
	public String getGerenteResponsavelFiltro() {
		return gerenteResponsavelFiltro;
	}

	/**
	 * Set: gerenteResponsavelFiltro.
	 *
	 * @param gerenteResponsavelFiltro the gerente responsavel filtro
	 */
	public void setGerenteResponsavelFiltro(String gerenteResponsavelFiltro) {
		this.gerenteResponsavelFiltro = gerenteResponsavelFiltro;
	}

	/**
	 * Get: numeroFiltro.
	 *
	 * @return numeroFiltro
	 */
	public String getNumeroFiltro() {
		return numeroFiltro;
	}

	/**
	 * Set: numeroFiltro.
	 *
	 * @param numeroFiltro the numero filtro
	 */
	public void setNumeroFiltro(String numeroFiltro) {
		this.numeroFiltro = numeroFiltro;
	}

	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * Get: situacaoContratoFiltro.
	 *
	 * @return situacaoContratoFiltro
	 */
	public String getSituacaoContratoFiltro() {
		return situacaoContratoFiltro;
	}

	/**
	 * Set: situacaoContratoFiltro.
	 *
	 * @param situacaoContratoFiltro the situacao contrato filtro
	 */
	public void setSituacaoContratoFiltro(String situacaoContratoFiltro) {
		this.situacaoContratoFiltro = situacaoContratoFiltro;
	}

	/**
	 * Get: tela.
	 *
	 * @return tela
	 */
	public String getTela() {
		return tela;
	}

	/**
	 * Set: tela.
	 *
	 * @param tela the tela
	 */
	public void setTela(String tela) {
		this.tela = tela;
	}

	/**
	 * Get: tipoFiltro.
	 *
	 * @return tipoFiltro
	 */
	public String getTipoFiltro() {
		return tipoFiltro;
	}

	/**
	 * Set: tipoFiltro.
	 *
	 * @param tipoFiltro the tipo filtro
	 */
	public void setTipoFiltro(String tipoFiltro) {
		this.tipoFiltro = tipoFiltro;
	}
	
	/**
	 * Carrega lista clientes.
	 *
	 * @return the string
	 */
	public String carregaListaClientes() {
		return "CONSULTAR_CONTRATO";
	}
	
	/**
	 * Voltar contrato.
	 *
	 * @return the string
	 */
	public String voltarContrato(){
		this.listaGridContrato = null;
		limparDadosContrato();		
		return tela;
	}
	
	/**
	 * Confirmar contrato.
	 *
	 * @return the string
	 */
	public String confirmarContrato(){
        
        limparListaContratos();
		return tela;
	}
    
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {			
		return "ok";		
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: operacao.
	 *
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * Set: operacao.
	 *
	 * @param operacao the operacao
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	/**
	 * Get: layout.
	 *
	 * @return layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Set: layout.
	 *
	 * @param layout the layout
	 */
	public void setLayout(String layout) {
		this.layout = layout;
	}
		
}
