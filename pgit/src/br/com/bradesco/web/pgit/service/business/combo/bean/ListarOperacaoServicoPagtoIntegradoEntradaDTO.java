/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarOperacaoServicoPagtoIntegradoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarOperacaoServicoPagtoIntegradoEntradaDTO{
	
	/** Atributo maxOcorrencias. */
	private Integer maxOcorrencias;
	
	/** Atributo cdprodutoServicoOperacao. */
	private Integer cdprodutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdOperacaoProdutoServico. */
	private Integer cdOperacaoProdutoServico;

	/**
	 * Get: maxOcorrencias.
	 *
	 * @return maxOcorrencias
	 */
	public Integer getMaxOcorrencias(){
		return maxOcorrencias;
	}

	/**
	 * Set: maxOcorrencias.
	 *
	 * @param maxOcorrencias the max ocorrencias
	 */
	public void setMaxOcorrencias(Integer maxOcorrencias){
		this.maxOcorrencias = maxOcorrencias;
	}

	/**
	 * Get: cdprodutoServicoOperacao.
	 *
	 * @return cdprodutoServicoOperacao
	 */
	public Integer getCdprodutoServicoOperacao(){
		return cdprodutoServicoOperacao;
	}

	/**
	 * Set: cdprodutoServicoOperacao.
	 *
	 * @param cdprodutoServicoOperacao the cdproduto servico operacao
	 */
	public void setCdprodutoServicoOperacao(Integer cdprodutoServicoOperacao){
		this.cdprodutoServicoOperacao = cdprodutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado(){
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado){
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public Integer getCdOperacaoProdutoServico(){
		return cdOperacaoProdutoServico;
	}

	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico){
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}
}