/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsConveCtaSalrl
     */
    private java.lang.String _dsConveCtaSalrl;

    /**
     * Field _cdBancoEmpresaConvn
     */
    private int _cdBancoEmpresaConvn = 0;

    /**
     * keeps track of state for field: _cdBancoEmpresaConvn
     */
    private boolean _has_cdBancoEmpresaConvn;

    /**
     * Field _cdAgeEmpresaConvn
     */
    private int _cdAgeEmpresaConvn = 0;

    /**
     * keeps track of state for field: _cdAgeEmpresaConvn
     */
    private boolean _has_cdAgeEmpresaConvn;

    /**
     * Field _cdDigitoAgencia
     */
    private java.lang.String _cdDigitoAgencia;

    /**
     * Field _cdCtaEmpresaConvn
     */
    private long _cdCtaEmpresaConvn = 0;

    /**
     * keeps track of state for field: _cdCtaEmpresaConvn
     */
    private boolean _has_cdCtaEmpresaConvn;

    /**
     * Field _cdDigEmpresaConvn
     */
    private java.lang.String _cdDigEmpresaConvn;

    /**
     * Field _cdConveNovo
     */
    private int _cdConveNovo = 0;

    /**
     * keeps track of state for field: _cdConveNovo
     */
    private boolean _has_cdConveNovo;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _cdCpfCnpjConve
     */
    private long _cdCpfCnpjConve = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjConve
     */
    private boolean _has_cdCpfCnpjConve;

    /**
     * Field _cdFilialCnpjConve
     */
    private int _cdFilialCnpjConve = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjConve
     */
    private boolean _has_cdFilialCnpjConve;

    /**
     * Field _cdCtrlCnpjEmp
     */
    private int _cdCtrlCnpjEmp = 0;

    /**
     * keeps track of state for field: _cdCtrlCnpjEmp
     */
    private boolean _has_cdCtrlCnpjEmp;

    /**
     * Field _dsSitConve
     */
    private java.lang.String _dsSitConve;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgeEmpresaConvn
     * 
     */
    public void deleteCdAgeEmpresaConvn()
    {
        this._has_cdAgeEmpresaConvn= false;
    } //-- void deleteCdAgeEmpresaConvn() 

    /**
     * Method deleteCdBancoEmpresaConvn
     * 
     */
    public void deleteCdBancoEmpresaConvn()
    {
        this._has_cdBancoEmpresaConvn= false;
    } //-- void deleteCdBancoEmpresaConvn() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdConveNovo
     * 
     */
    public void deleteCdConveNovo()
    {
        this._has_cdConveNovo= false;
    } //-- void deleteCdConveNovo() 

    /**
     * Method deleteCdCpfCnpjConve
     * 
     */
    public void deleteCdCpfCnpjConve()
    {
        this._has_cdCpfCnpjConve= false;
    } //-- void deleteCdCpfCnpjConve() 

    /**
     * Method deleteCdCtaEmpresaConvn
     * 
     */
    public void deleteCdCtaEmpresaConvn()
    {
        this._has_cdCtaEmpresaConvn= false;
    } //-- void deleteCdCtaEmpresaConvn() 

    /**
     * Method deleteCdCtrlCnpjEmp
     * 
     */
    public void deleteCdCtrlCnpjEmp()
    {
        this._has_cdCtrlCnpjEmp= false;
    } //-- void deleteCdCtrlCnpjEmp() 

    /**
     * Method deleteCdFilialCnpjConve
     * 
     */
    public void deleteCdFilialCnpjConve()
    {
        this._has_cdFilialCnpjConve= false;
    } //-- void deleteCdFilialCnpjConve() 

    /**
     * Returns the value of field 'cdAgeEmpresaConvn'.
     * 
     * @return int
     * @return the value of field 'cdAgeEmpresaConvn'.
     */
    public int getCdAgeEmpresaConvn()
    {
        return this._cdAgeEmpresaConvn;
    } //-- int getCdAgeEmpresaConvn() 

    /**
     * Returns the value of field 'cdBancoEmpresaConvn'.
     * 
     * @return int
     * @return the value of field 'cdBancoEmpresaConvn'.
     */
    public int getCdBancoEmpresaConvn()
    {
        return this._cdBancoEmpresaConvn;
    } //-- int getCdBancoEmpresaConvn() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdConveNovo'.
     * 
     * @return int
     * @return the value of field 'cdConveNovo'.
     */
    public int getCdConveNovo()
    {
        return this._cdConveNovo;
    } //-- int getCdConveNovo() 

    /**
     * Returns the value of field 'cdCpfCnpjConve'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjConve'.
     */
    public long getCdCpfCnpjConve()
    {
        return this._cdCpfCnpjConve;
    } //-- long getCdCpfCnpjConve() 

    /**
     * Returns the value of field 'cdCtaEmpresaConvn'.
     * 
     * @return long
     * @return the value of field 'cdCtaEmpresaConvn'.
     */
    public long getCdCtaEmpresaConvn()
    {
        return this._cdCtaEmpresaConvn;
    } //-- long getCdCtaEmpresaConvn() 

    /**
     * Returns the value of field 'cdCtrlCnpjEmp'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCnpjEmp'.
     */
    public int getCdCtrlCnpjEmp()
    {
        return this._cdCtrlCnpjEmp;
    } //-- int getCdCtrlCnpjEmp() 

    /**
     * Returns the value of field 'cdDigEmpresaConvn'.
     * 
     * @return String
     * @return the value of field 'cdDigEmpresaConvn'.
     */
    public java.lang.String getCdDigEmpresaConvn()
    {
        return this._cdDigEmpresaConvn;
    } //-- java.lang.String getCdDigEmpresaConvn() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgencia'.
     */
    public java.lang.String getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- java.lang.String getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdFilialCnpjConve'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjConve'.
     */
    public int getCdFilialCnpjConve()
    {
        return this._cdFilialCnpjConve;
    } //-- int getCdFilialCnpjConve() 

    /**
     * Returns the value of field 'dsConveCtaSalrl'.
     * 
     * @return String
     * @return the value of field 'dsConveCtaSalrl'.
     */
    public java.lang.String getDsConveCtaSalrl()
    {
        return this._dsConveCtaSalrl;
    } //-- java.lang.String getDsConveCtaSalrl() 

    /**
     * Returns the value of field 'dsSitConve'.
     * 
     * @return String
     * @return the value of field 'dsSitConve'.
     */
    public java.lang.String getDsSitConve()
    {
        return this._dsSitConve;
    } //-- java.lang.String getDsSitConve() 

    /**
     * Method hasCdAgeEmpresaConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgeEmpresaConvn()
    {
        return this._has_cdAgeEmpresaConvn;
    } //-- boolean hasCdAgeEmpresaConvn() 

    /**
     * Method hasCdBancoEmpresaConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoEmpresaConvn()
    {
        return this._has_cdBancoEmpresaConvn;
    } //-- boolean hasCdBancoEmpresaConvn() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdConveNovo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveNovo()
    {
        return this._has_cdConveNovo;
    } //-- boolean hasCdConveNovo() 

    /**
     * Method hasCdCpfCnpjConve
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjConve()
    {
        return this._has_cdCpfCnpjConve;
    } //-- boolean hasCdCpfCnpjConve() 

    /**
     * Method hasCdCtaEmpresaConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtaEmpresaConvn()
    {
        return this._has_cdCtaEmpresaConvn;
    } //-- boolean hasCdCtaEmpresaConvn() 

    /**
     * Method hasCdCtrlCnpjEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCnpjEmp()
    {
        return this._has_cdCtrlCnpjEmp;
    } //-- boolean hasCdCtrlCnpjEmp() 

    /**
     * Method hasCdFilialCnpjConve
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjConve()
    {
        return this._has_cdFilialCnpjConve;
    } //-- boolean hasCdFilialCnpjConve() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgeEmpresaConvn'.
     * 
     * @param cdAgeEmpresaConvn the value of field
     * 'cdAgeEmpresaConvn'.
     */
    public void setCdAgeEmpresaConvn(int cdAgeEmpresaConvn)
    {
        this._cdAgeEmpresaConvn = cdAgeEmpresaConvn;
        this._has_cdAgeEmpresaConvn = true;
    } //-- void setCdAgeEmpresaConvn(int) 

    /**
     * Sets the value of field 'cdBancoEmpresaConvn'.
     * 
     * @param cdBancoEmpresaConvn the value of field
     * 'cdBancoEmpresaConvn'.
     */
    public void setCdBancoEmpresaConvn(int cdBancoEmpresaConvn)
    {
        this._cdBancoEmpresaConvn = cdBancoEmpresaConvn;
        this._has_cdBancoEmpresaConvn = true;
    } //-- void setCdBancoEmpresaConvn(int) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdConveNovo'.
     * 
     * @param cdConveNovo the value of field 'cdConveNovo'.
     */
    public void setCdConveNovo(int cdConveNovo)
    {
        this._cdConveNovo = cdConveNovo;
        this._has_cdConveNovo = true;
    } //-- void setCdConveNovo(int) 

    /**
     * Sets the value of field 'cdCpfCnpjConve'.
     * 
     * @param cdCpfCnpjConve the value of field 'cdCpfCnpjConve'.
     */
    public void setCdCpfCnpjConve(long cdCpfCnpjConve)
    {
        this._cdCpfCnpjConve = cdCpfCnpjConve;
        this._has_cdCpfCnpjConve = true;
    } //-- void setCdCpfCnpjConve(long) 

    /**
     * Sets the value of field 'cdCtaEmpresaConvn'.
     * 
     * @param cdCtaEmpresaConvn the value of field
     * 'cdCtaEmpresaConvn'.
     */
    public void setCdCtaEmpresaConvn(long cdCtaEmpresaConvn)
    {
        this._cdCtaEmpresaConvn = cdCtaEmpresaConvn;
        this._has_cdCtaEmpresaConvn = true;
    } //-- void setCdCtaEmpresaConvn(long) 

    /**
     * Sets the value of field 'cdCtrlCnpjEmp'.
     * 
     * @param cdCtrlCnpjEmp the value of field 'cdCtrlCnpjEmp'.
     */
    public void setCdCtrlCnpjEmp(int cdCtrlCnpjEmp)
    {
        this._cdCtrlCnpjEmp = cdCtrlCnpjEmp;
        this._has_cdCtrlCnpjEmp = true;
    } //-- void setCdCtrlCnpjEmp(int) 

    /**
     * Sets the value of field 'cdDigEmpresaConvn'.
     * 
     * @param cdDigEmpresaConvn the value of field
     * 'cdDigEmpresaConvn'.
     */
    public void setCdDigEmpresaConvn(java.lang.String cdDigEmpresaConvn)
    {
        this._cdDigEmpresaConvn = cdDigEmpresaConvn;
    } //-- void setCdDigEmpresaConvn(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(java.lang.String cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
    } //-- void setCdDigitoAgencia(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpjConve'.
     * 
     * @param cdFilialCnpjConve the value of field
     * 'cdFilialCnpjConve'.
     */
    public void setCdFilialCnpjConve(int cdFilialCnpjConve)
    {
        this._cdFilialCnpjConve = cdFilialCnpjConve;
        this._has_cdFilialCnpjConve = true;
    } //-- void setCdFilialCnpjConve(int) 

    /**
     * Sets the value of field 'dsConveCtaSalrl'.
     * 
     * @param dsConveCtaSalrl the value of field 'dsConveCtaSalrl'.
     */
    public void setDsConveCtaSalrl(java.lang.String dsConveCtaSalrl)
    {
        this._dsConveCtaSalrl = dsConveCtaSalrl;
    } //-- void setDsConveCtaSalrl(java.lang.String) 

    /**
     * Sets the value of field 'dsSitConve'.
     * 
     * @param dsSitConve the value of field 'dsSitConve'.
     */
    public void setDsSitConve(java.lang.String dsSitConve)
    {
        this._dsSitConve = dsSitConve;
    } //-- void setDsSitConve(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
