/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrContrato
     */
    private long _nrContrato = 0;

    /**
     * keeps track of state for field: _nrContrato
     */
    private boolean _has_nrContrato;

    /**
     * Field _nrCnpjCpf
     */
    private long _nrCnpjCpf = 0;

    /**
     * keeps track of state for field: _nrCnpjCpf
     */
    private boolean _has_nrCnpjCpf;

    /**
     * Field _nrFilialCnpjCpf
     */
    private int _nrFilialCnpjCpf = 0;

    /**
     * keeps track of state for field: _nrFilialCnpjCpf
     */
    private boolean _has_nrFilialCnpjCpf;

    /**
     * Field _cdDigitoCnpjCpf
     */
    private int _cdDigitoCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdDigitoCnpjCpf
     */
    private boolean _has_cdDigitoCnpjCpf;

    /**
     * Field _nrRemessa
     */
    private long _nrRemessa = 0;

    /**
     * keeps track of state for field: _nrRemessa
     */
    private boolean _has_nrRemessa;

    /**
     * Field _dtPagamento
     */
    private java.lang.String _dtPagamento;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsResumoProdutoServico
     */
    private java.lang.String _dsResumoProdutoServico;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _dsSituacaoOperacaoPagamento
     */
    private java.lang.String _dsSituacaoOperacaoPagamento;

    /**
     * Field _qtPagamento
     */
    private long _qtPagamento = 0;

    /**
     * keeps track of state for field: _qtPagamento
     */
    private boolean _has_qtPagamento;

    /**
     * Field _vlEfetivacaoPagamentoCliente
     */
    private java.math.BigDecimal _vlEfetivacaoPagamentoCliente = new java.math.BigDecimal("0");

    /**
     * Field _cdPessoaContratoDebito
     */
    private long _cdPessoaContratoDebito = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoDebito
     */
    private boolean _has_cdPessoaContratoDebito;

    /**
     * Field _cdTipoContratoDebito
     */
    private int _cdTipoContratoDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContratoDebito
     */
    private boolean _has_cdTipoContratoDebito;

    /**
     * Field _nrSequenciaContratoDebito
     */
    private long _nrSequenciaContratoDebito = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoDebito
     */
    private boolean _has_nrSequenciaContratoDebito;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlEfetivacaoPagamentoCliente(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdDigitoCnpjCpf
     * 
     */
    public void deleteCdDigitoCnpjCpf()
    {
        this._has_cdDigitoCnpjCpf= false;
    } //-- void deleteCdDigitoCnpjCpf() 

    /**
     * Method deleteCdPessoaContratoDebito
     * 
     */
    public void deleteCdPessoaContratoDebito()
    {
        this._has_cdPessoaContratoDebito= false;
    } //-- void deleteCdPessoaContratoDebito() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoContratoDebito
     * 
     */
    public void deleteCdTipoContratoDebito()
    {
        this._has_cdTipoContratoDebito= false;
    } //-- void deleteCdTipoContratoDebito() 

    /**
     * Method deleteNrCnpjCpf
     * 
     */
    public void deleteNrCnpjCpf()
    {
        this._has_nrCnpjCpf= false;
    } //-- void deleteNrCnpjCpf() 

    /**
     * Method deleteNrContrato
     * 
     */
    public void deleteNrContrato()
    {
        this._has_nrContrato= false;
    } //-- void deleteNrContrato() 

    /**
     * Method deleteNrFilialCnpjCpf
     * 
     */
    public void deleteNrFilialCnpjCpf()
    {
        this._has_nrFilialCnpjCpf= false;
    } //-- void deleteNrFilialCnpjCpf() 

    /**
     * Method deleteNrRemessa
     * 
     */
    public void deleteNrRemessa()
    {
        this._has_nrRemessa= false;
    } //-- void deleteNrRemessa() 

    /**
     * Method deleteNrSequenciaContratoDebito
     * 
     */
    public void deleteNrSequenciaContratoDebito()
    {
        this._has_nrSequenciaContratoDebito= false;
    } //-- void deleteNrSequenciaContratoDebito() 

    /**
     * Method deleteQtPagamento
     * 
     */
    public void deleteQtPagamento()
    {
        this._has_qtPagamento= false;
    } //-- void deleteQtPagamento() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCnpjCpf'.
     */
    public int getCdDigitoCnpjCpf()
    {
        return this._cdDigitoCnpjCpf;
    } //-- int getCdDigitoCnpjCpf() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdPessoaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoDebito'.
     */
    public long getCdPessoaContratoDebito()
    {
        return this._cdPessoaContratoDebito;
    } //-- long getCdPessoaContratoDebito() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoDebito'.
     */
    public int getCdTipoContratoDebito()
    {
        return this._cdTipoContratoDebito;
    } //-- int getCdTipoContratoDebito() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsResumoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsResumoProdutoServico'.
     */
    public java.lang.String getDsResumoProdutoServico()
    {
        return this._dsResumoProdutoServico;
    } //-- java.lang.String getDsResumoProdutoServico() 

    /**
     * Returns the value of field 'dsSituacaoOperacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoOperacaoPagamento'.
     */
    public java.lang.String getDsSituacaoOperacaoPagamento()
    {
        return this._dsSituacaoOperacaoPagamento;
    } //-- java.lang.String getDsSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'dtPagamento'.
     * 
     * @return String
     * @return the value of field 'dtPagamento'.
     */
    public java.lang.String getDtPagamento()
    {
        return this._dtPagamento;
    } //-- java.lang.String getDtPagamento() 

    /**
     * Returns the value of field 'nrCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'nrCnpjCpf'.
     */
    public long getNrCnpjCpf()
    {
        return this._nrCnpjCpf;
    } //-- long getNrCnpjCpf() 

    /**
     * Returns the value of field 'nrContrato'.
     * 
     * @return long
     * @return the value of field 'nrContrato'.
     */
    public long getNrContrato()
    {
        return this._nrContrato;
    } //-- long getNrContrato() 

    /**
     * Returns the value of field 'nrFilialCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'nrFilialCnpjCpf'.
     */
    public int getNrFilialCnpjCpf()
    {
        return this._nrFilialCnpjCpf;
    } //-- int getNrFilialCnpjCpf() 

    /**
     * Returns the value of field 'nrRemessa'.
     * 
     * @return long
     * @return the value of field 'nrRemessa'.
     */
    public long getNrRemessa()
    {
        return this._nrRemessa;
    } //-- long getNrRemessa() 

    /**
     * Returns the value of field 'nrSequenciaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoDebito'.
     */
    public long getNrSequenciaContratoDebito()
    {
        return this._nrSequenciaContratoDebito;
    } //-- long getNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'qtPagamento'.
     * 
     * @return long
     * @return the value of field 'qtPagamento'.
     */
    public long getQtPagamento()
    {
        return this._qtPagamento;
    } //-- long getQtPagamento() 

    /**
     * Returns the value of field 'vlEfetivacaoPagamentoCliente'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlEfetivacaoPagamentoCliente'.
     */
    public java.math.BigDecimal getVlEfetivacaoPagamentoCliente()
    {
        return this._vlEfetivacaoPagamentoCliente;
    } //-- java.math.BigDecimal getVlEfetivacaoPagamentoCliente() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdDigitoCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCnpjCpf()
    {
        return this._has_cdDigitoCnpjCpf;
    } //-- boolean hasCdDigitoCnpjCpf() 

    /**
     * Method hasCdPessoaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoDebito()
    {
        return this._has_cdPessoaContratoDebito;
    } //-- boolean hasCdPessoaContratoDebito() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoDebito()
    {
        return this._has_cdTipoContratoDebito;
    } //-- boolean hasCdTipoContratoDebito() 

    /**
     * Method hasNrCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrCnpjCpf()
    {
        return this._has_nrCnpjCpf;
    } //-- boolean hasNrCnpjCpf() 

    /**
     * Method hasNrContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContrato()
    {
        return this._has_nrContrato;
    } //-- boolean hasNrContrato() 

    /**
     * Method hasNrFilialCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFilialCnpjCpf()
    {
        return this._has_nrFilialCnpjCpf;
    } //-- boolean hasNrFilialCnpjCpf() 

    /**
     * Method hasNrRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrRemessa()
    {
        return this._has_nrRemessa;
    } //-- boolean hasNrRemessa() 

    /**
     * Method hasNrSequenciaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoDebito()
    {
        return this._has_nrSequenciaContratoDebito;
    } //-- boolean hasNrSequenciaContratoDebito() 

    /**
     * Method hasQtPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagamento()
    {
        return this._has_qtPagamento;
    } //-- boolean hasQtPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoCnpjCpf'.
     * 
     * @param cdDigitoCnpjCpf the value of field 'cdDigitoCnpjCpf'.
     */
    public void setCdDigitoCnpjCpf(int cdDigitoCnpjCpf)
    {
        this._cdDigitoCnpjCpf = cdDigitoCnpjCpf;
        this._has_cdDigitoCnpjCpf = true;
    } //-- void setCdDigitoCnpjCpf(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaContratoDebito'.
     * 
     * @param cdPessoaContratoDebito the value of field
     * 'cdPessoaContratoDebito'.
     */
    public void setCdPessoaContratoDebito(long cdPessoaContratoDebito)
    {
        this._cdPessoaContratoDebito = cdPessoaContratoDebito;
        this._has_cdPessoaContratoDebito = true;
    } //-- void setCdPessoaContratoDebito(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoDebito'.
     * 
     * @param cdTipoContratoDebito the value of field
     * 'cdTipoContratoDebito'.
     */
    public void setCdTipoContratoDebito(int cdTipoContratoDebito)
    {
        this._cdTipoContratoDebito = cdTipoContratoDebito;
        this._has_cdTipoContratoDebito = true;
    } //-- void setCdTipoContratoDebito(int) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsResumoProdutoServico'.
     * 
     * @param dsResumoProdutoServico the value of field
     * 'dsResumoProdutoServico'.
     */
    public void setDsResumoProdutoServico(java.lang.String dsResumoProdutoServico)
    {
        this._dsResumoProdutoServico = dsResumoProdutoServico;
    } //-- void setDsResumoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoOperacaoPagamento'.
     * 
     * @param dsSituacaoOperacaoPagamento the value of field
     * 'dsSituacaoOperacaoPagamento'.
     */
    public void setDsSituacaoOperacaoPagamento(java.lang.String dsSituacaoOperacaoPagamento)
    {
        this._dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
    } //-- void setDsSituacaoOperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtPagamento'.
     * 
     * @param dtPagamento the value of field 'dtPagamento'.
     */
    public void setDtPagamento(java.lang.String dtPagamento)
    {
        this._dtPagamento = dtPagamento;
    } //-- void setDtPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrCnpjCpf'.
     * 
     * @param nrCnpjCpf the value of field 'nrCnpjCpf'.
     */
    public void setNrCnpjCpf(long nrCnpjCpf)
    {
        this._nrCnpjCpf = nrCnpjCpf;
        this._has_nrCnpjCpf = true;
    } //-- void setNrCnpjCpf(long) 

    /**
     * Sets the value of field 'nrContrato'.
     * 
     * @param nrContrato the value of field 'nrContrato'.
     */
    public void setNrContrato(long nrContrato)
    {
        this._nrContrato = nrContrato;
        this._has_nrContrato = true;
    } //-- void setNrContrato(long) 

    /**
     * Sets the value of field 'nrFilialCnpjCpf'.
     * 
     * @param nrFilialCnpjCpf the value of field 'nrFilialCnpjCpf'.
     */
    public void setNrFilialCnpjCpf(int nrFilialCnpjCpf)
    {
        this._nrFilialCnpjCpf = nrFilialCnpjCpf;
        this._has_nrFilialCnpjCpf = true;
    } //-- void setNrFilialCnpjCpf(int) 

    /**
     * Sets the value of field 'nrRemessa'.
     * 
     * @param nrRemessa the value of field 'nrRemessa'.
     */
    public void setNrRemessa(long nrRemessa)
    {
        this._nrRemessa = nrRemessa;
        this._has_nrRemessa = true;
    } //-- void setNrRemessa(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoDebito'.
     * 
     * @param nrSequenciaContratoDebito the value of field
     * 'nrSequenciaContratoDebito'.
     */
    public void setNrSequenciaContratoDebito(long nrSequenciaContratoDebito)
    {
        this._nrSequenciaContratoDebito = nrSequenciaContratoDebito;
        this._has_nrSequenciaContratoDebito = true;
    } //-- void setNrSequenciaContratoDebito(long) 

    /**
     * Sets the value of field 'qtPagamento'.
     * 
     * @param qtPagamento the value of field 'qtPagamento'.
     */
    public void setQtPagamento(long qtPagamento)
    {
        this._qtPagamento = qtPagamento;
        this._has_qtPagamento = true;
    } //-- void setQtPagamento(long) 

    /**
     * Sets the value of field 'vlEfetivacaoPagamentoCliente'.
     * 
     * @param vlEfetivacaoPagamentoCliente the value of field
     * 'vlEfetivacaoPagamentoCliente'.
     */
    public void setVlEfetivacaoPagamentoCliente(java.math.BigDecimal vlEfetivacaoPagamentoCliente)
    {
        this._vlEfetivacaoPagamentoCliente = vlEfetivacaoPagamentoCliente;
    } //-- void setVlEfetivacaoPagamentoCliente(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
