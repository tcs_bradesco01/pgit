/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.bean;

/**
 * Nome: ComboProdutoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ComboProdutoDTO {

	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo codigoProduto. */
	private String codigoProduto;
	
	/** Atributo descricaoCentroCusto. */
	private String descricaoCentroCusto;
		
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: descricaoCentroCusto.
	 *
	 * @return descricaoCentroCusto
	 */
	public String getDescricaoCentroCusto() {
		return descricaoCentroCusto;
	}
	
	/**
	 * Set: descricaoCentroCusto.
	 *
	 * @param descricaoCentroCusto the descricao centro custo
	 */
	public void setDescricaoCentroCusto(String descricaoCentroCusto) {
		this.descricaoCentroCusto = descricaoCentroCusto;
	}
	
	/**
	 * Get: codigoProduto.
	 *
	 * @return codigoProduto
	 */
	public String getCodigoProduto() {
		return codigoProduto;
	}
	
	/**
	 * Set: codigoProduto.
	 *
	 * @param codigoProduto the codigo produto
	 */
	public void setCodigoProduto(String codigoProduto) {
		this.codigoProduto = codigoProduto;
	}
	
}
