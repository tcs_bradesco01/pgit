/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarListaHistoricoFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaHistoricoFavorecidoSaidaDTO {

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo dtSistema. */
    private String dtSistema;

    /** Atributo cdHora. */
    private String cdHora;

    /** Atributo cdUsuario. */
    private String cdUsuario;

    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;

    /** Atributo dsComplementoFavorecido. */
    private String dsComplementoFavorecido;

    /** Atributo dsTipoFavorecido. */
    private String dsTipoFavorecido;

    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;

    /** Atributo cdTipoIsncricaoFavorecido. */
    private Integer cdTipoIsncricaoFavorecido;

    /** Atributo cdSituacaoFavorecido. */
    private String cdSituacaoFavorecido;

    /** Atributo favorecidoFormatado. */
    private String favorecidoFormatado;
    
    /** Atributo cpfCnpjFavorecidoFormatado. */
    private String cpfCnpjFavorecidoFormatado;
    
    /** Atributo tpManutencao. */
    private String tpManutencao;
    
    /**
     * Get: cdUsuario.
     *
     * @return cdUsuario
     */
    public String getCdUsuario() {
	return cdUsuario;
    }

    /**
     * Set: cdUsuario.
     *
     * @param dsUsuario the cd usuario
     */
    public void setCdUsuario(String dsUsuario) {
	this.cdUsuario = dsUsuario;
    }

    /**
     * Get: dtSistema.
     *
     * @return dtSistema
     */
    public String getDtSistema() {
	return dtSistema;
    }

    /**
     * Set: dtSistema.
     *
     * @param dtData the dt sistema
     */
    public void setDtSistema(String dtData) {
	this.dtSistema = dtData;
    }

    /**
     * Get: cdHora.
     *
     * @return cdHora
     */
    public String getCdHora() {
	return cdHora;
    }

    /**
     * Set: cdHora.
     *
     * @param hrHora the cd hora
     */
    public void setCdHora(String hrHora) {
	this.cdHora = hrHora;
    }

    /**
     * Get: hrInclusaoRegistro.
     *
     * @return hrInclusaoRegistro
     */
    public String getHrInclusaoRegistro() {
	return hrInclusaoRegistro;
    }

    /**
     * Set: hrInclusaoRegistro.
     *
     * @param hrInclusaoRegistro the hr inclusao registro
     */
    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
	this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    /**
     * Get: cdFavorecidoClientePagador.
     *
     * @return cdFavorecidoClientePagador
     */
    public Long getCdFavorecidoClientePagador() {
	return cdFavorecidoClientePagador;
    }

    /**
     * Set: cdFavorecidoClientePagador.
     *
     * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
     */
    public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
	this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public Long getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdSituacaoFavorecido.
     *
     * @return cdSituacaoFavorecido
     */
    public String getCdSituacaoFavorecido() {
	return cdSituacaoFavorecido;
    }

    /**
     * Set: cdSituacaoFavorecido.
     *
     * @param cdSituacaoFavorecido the cd situacao favorecido
     */
    public void setCdSituacaoFavorecido(String cdSituacaoFavorecido) {
	this.cdSituacaoFavorecido = cdSituacaoFavorecido;
    }

    /**
     * Get: cdTipoIsncricaoFavorecido.
     *
     * @return cdTipoIsncricaoFavorecido
     */
    public Integer getCdTipoIsncricaoFavorecido() {
	return cdTipoIsncricaoFavorecido;
    }

    /**
     * Set: cdTipoIsncricaoFavorecido.
     *
     * @param cdTipoIsncricaoFavorecido the cd tipo isncricao favorecido
     */
    public void setCdTipoIsncricaoFavorecido(Integer cdTipoIsncricaoFavorecido) {
	this.cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
    }

    /**
     * Get: dsComplementoFavorecido.
     *
     * @return dsComplementoFavorecido
     */
    public String getDsComplementoFavorecido() {
	return dsComplementoFavorecido;
    }

    /**
     * Set: dsComplementoFavorecido.
     *
     * @param dsComplementoFavorecido the ds complemento favorecido
     */
    public void setDsComplementoFavorecido(String dsComplementoFavorecido) {
	this.dsComplementoFavorecido = dsComplementoFavorecido;
    }

    /**
     * Get: dsTipoFavorecido.
     *
     * @return dsTipoFavorecido
     */
    public String getDsTipoFavorecido() {
	return dsTipoFavorecido;
    }

    /**
     * Set: dsTipoFavorecido.
     *
     * @param dsTipoFavorecido the ds tipo favorecido
     */
    public void setDsTipoFavorecido(String dsTipoFavorecido) {
	this.dsTipoFavorecido = dsTipoFavorecido;
    }

    /**
     * Get: favorecidoFormatado.
     *
     * @return favorecidoFormatado
     */
    public String getFavorecidoFormatado() {
	return favorecidoFormatado;
    }

    /**
     * Set: favorecidoFormatado.
     *
     * @param favorecidoFormatado the favorecido formatado
     */
    public void setFavorecidoFormatado(String favorecidoFormatado) {
	this.favorecidoFormatado = favorecidoFormatado;
    }

    /**
     * Get: cpfCnpjFavorecidoFormatado.
     *
     * @return cpfCnpjFavorecidoFormatado
     */
    public String getCpfCnpjFavorecidoFormatado() {
        return cpfCnpjFavorecidoFormatado;
    }

    /**
     * Set: cpfCnpjFavorecidoFormatado.
     *
     * @param cpfCnpjFavorecidoFormatado the cpf cnpj favorecido formatado
     */
    public void setCpfCnpjFavorecidoFormatado(String cpfCnpjFavorecidoFormatado) {
        this.cpfCnpjFavorecidoFormatado = cpfCnpjFavorecidoFormatado;
    }

    /**
     * Get: tpManutencao.
     *
     * @return tpManutencao
     */
    public String getTpManutencao() {
        return tpManutencao;
    }

    /**
     * Set: tpManutencao.
     *
     * @param tpManutencao the tp manutencao
     */
    public void setTpManutencao(String tpManutencao) {
        this.tpManutencao = tpManutencao;
    }
}