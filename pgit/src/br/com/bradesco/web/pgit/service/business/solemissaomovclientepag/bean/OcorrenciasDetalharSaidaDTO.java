/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean;

import java.math.BigDecimal;

/**
 * Nome: OcorrenciasDetalharSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasDetalharSaidaDTO {
	   
   	/** Atributo cdTipoCanal. */
   	private Integer cdTipoCanal;
	   
   	/** Atributo cdControlePagamento. */
   	private String cdControlePagamento;
	   
   	/** Atributo dtPagamento. */
   	private String dtPagamento;
	   
   	/** Atributo vlPagamento. */
   	private BigDecimal vlPagamento;
	   
   	/** Atributo cdFavorecido. */
   	private Long cdFavorecido;
	   
   	/** Atributo dsFavorecido. */
   	private String dsFavorecido;
	   
   	/** Atributo cdBancoDebito. */
   	private Integer cdBancoDebito;
	   
   	/** Atributo cdAgenciaDebito. */
   	private Integer cdAgenciaDebito;
	   
   	/** Atributo cdContaDebito. */
   	private Long cdContaDebito;
	   
   	/** Atributo cdDigitoContaDebito. */
   	private Integer cdDigitoContaDebito;
	   
   	/** Atributo cdProdutoServico. */
   	private Integer cdProdutoServico;
	   
   	/** Atributo cdSituacaoPagamento. */
   	private Integer cdSituacaoPagamento;
	   
   	/** Atributo dsSituacaoPagamento. */
   	private String dsSituacaoPagamento;
	   
   	/** Atributo dsCodigoProdutoServicoOperacao. */
   	private String dsCodigoProdutoServicoOperacao;
	   
   	/** Atributo cdProdutoRelacionado. */
   	private Integer cdProdutoRelacionado;
	   
   	/** Atributo dsCodigoProdutoServicoRelacionado. */
   	private String dsCodigoProdutoServicoRelacionado;
	   
	   /** Atributo favorecidoFormatado. */
   	private String favorecidoFormatado;
	   
   	/** Atributo dataFormatada. */
   	private String dataFormatada;
	   
   	/** Atributo contaDebitoFormatada. */
   	private String contaDebitoFormatada;
	   
	   
	/**
	 * Get: contaDebitoFormatada.
	 *
	 * @return contaDebitoFormatada
	 */
	public String getContaDebitoFormatada() {
		return contaDebitoFormatada;
	}
	
	/**
	 * Set: contaDebitoFormatada.
	 *
	 * @param contaDebitoFormatada the conta debito formatada
	 */
	public void setContaDebitoFormatada(String contaDebitoFormatada) {
		this.contaDebitoFormatada = contaDebitoFormatada;
	}
	
	/**
	 * Get: dataFormatada.
	 *
	 * @return dataFormatada
	 */
	public String getDataFormatada() {
		return dataFormatada;
	}
	
	/**
	 * Set: dataFormatada.
	 *
	 * @param dataFormatada the data formatada
	 */
	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}
	
	/**
	 * Get: favorecidoFormatado.
	 *
	 * @return favorecidoFormatado
	 */
	public String getFavorecidoFormatado() {
		return favorecidoFormatado;
	}
	
	/**
	 * Set: favorecidoFormatado.
	 *
	 * @param favorecidoFormatado the favorecido formatado
	 */
	public void setFavorecidoFormatado(String favorecidoFormatado) {
		this.favorecidoFormatado = favorecidoFormatado;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public Integer getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}
	
	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(Integer cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}
	
	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}
	
	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}
	
	/**
	 * Get: cdProdutoRelacionado.
	 *
	 * @return cdProdutoRelacionado
	 */
	public Integer getCdProdutoRelacionado() {
		return cdProdutoRelacionado;
	}
	
	/**
	 * Set: cdProdutoRelacionado.
	 *
	 * @param cdProdutoRelacionado the cd produto relacionado
	 */
	public void setCdProdutoRelacionado(Integer cdProdutoRelacionado) {
		this.cdProdutoRelacionado = cdProdutoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServico.
	 *
	 * @return cdProdutoServico
	 */
	public Integer getCdProdutoServico() {
		return cdProdutoServico;
	}
	
	/**
	 * Set: cdProdutoServico.
	 *
	 * @param cdProdutoServico the cd produto servico
	 */
	public void setCdProdutoServico(Integer cdProdutoServico) {
		this.cdProdutoServico = cdProdutoServico;
	}
	
	/**
	 * Get: cdSituacaoPagamento.
	 *
	 * @return cdSituacaoPagamento
	 */
	public Integer getCdSituacaoPagamento() {
		return cdSituacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoPagamento.
	 *
	 * @param cdSituacaoPagamento the cd situacao pagamento
	 */
	public void setCdSituacaoPagamento(Integer cdSituacaoPagamento) {
		this.cdSituacaoPagamento = cdSituacaoPagamento;
	}
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: dsCodigoProdutoServicoOperacao.
	 *
	 * @return dsCodigoProdutoServicoOperacao
	 */
	public String getDsCodigoProdutoServicoOperacao() {
		return dsCodigoProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsCodigoProdutoServicoOperacao.
	 *
	 * @param dsCodigoProdutoServicoOperacao the ds codigo produto servico operacao
	 */
	public void setDsCodigoProdutoServicoOperacao(
			String dsCodigoProdutoServicoOperacao) {
		this.dsCodigoProdutoServicoOperacao = dsCodigoProdutoServicoOperacao;
	}
	
	/**
	 * Get: dsCodigoProdutoServicoRelacionado.
	 *
	 * @return dsCodigoProdutoServicoRelacionado
	 */
	public String getDsCodigoProdutoServicoRelacionado() {
		return dsCodigoProdutoServicoRelacionado;
	}
	
	/**
	 * Set: dsCodigoProdutoServicoRelacionado.
	 *
	 * @param dsCodigoProdutoServicoRelacionado the ds codigo produto servico relacionado
	 */
	public void setDsCodigoProdutoServicoRelacionado(
			String dsCodigoProdutoServicoRelacionado) {
		this.dsCodigoProdutoServicoRelacionado = dsCodigoProdutoServicoRelacionado;
	}
	
	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}
	
	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}
	
	/**
	 * Get: dsSituacaoPagamento.
	 *
	 * @return dsSituacaoPagamento
	 */
	public String getDsSituacaoPagamento() {
		return dsSituacaoPagamento;
	}
	
	/**
	 * Set: dsSituacaoPagamento.
	 *
	 * @param dsSituacaoPagamento the ds situacao pagamento
	 */
	public void setDsSituacaoPagamento(String dsSituacaoPagamento) {
		this.dsSituacaoPagamento = dsSituacaoPagamento;
	}
	
	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}
	
	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	
	/**
	 * Get: vlPagamento.
	 *
	 * @return vlPagamento
	 */
	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}
	
	/**
	 * Set: vlPagamento.
	 *
	 * @param vlPagamento the vl pagamento
	 */
	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}
	   
	   
}
