/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean;

import java.math.BigDecimal;

/**
 * Nome: IncluirSolBasePagtoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSolBasePagtoEntradaDTO{
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dtInicioPagamento. */
    private String dtInicioPagamento;
    
    /** Atributo dtFimPagamento. */
    private String dtFimPagamento;
    
    /** Atributo cdServico. */
    private Integer cdServico;
    
    /** Atributo cdServicoRelacionado. */
    private Integer cdServicoRelacionado;
    
    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo digitoContaDebito. */
    private String digitoContaDebito;
    
    /** Atributo cdIndicadorPagos. */
    private String cdIndicadorPagos;
    
    /** Atributo cdIndicadorNaoPagos. */
    private String cdIndicadorNaoPagos;
    
    /** Atributo cdIndicadorAgendados. */
    private String cdIndicadorAgendados;
    
    /** Atributo cdFavorecido. */
    private Long cdFavorecido;
    
    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;
    
    /** Atributo cdTipoInscricaoFavorecido. */
    private Integer cdTipoInscricaoFavorecido;
    
    /** Atributo nmMinemonicoFavorecido. */
    private String nmMinemonicoFavorecido;
    
    /** Atributo cdOrigem. */
    private Integer cdOrigem;
    
    /** Atributo cdDestino. */
    private Integer cdDestino;
    
    /** Atributo vlTarifaPadrao. */
    private BigDecimal vlTarifaPadrao;
    
    /** Atributo numPercentualDescTarifa. */
    private BigDecimal numPercentualDescTarifa;
    
    /** Atributo vlrTarifaAtual. */
    private BigDecimal vlrTarifaAtual;
    
    /** Atributo cdPessoaJuridicaConta. */
    private Long cdPessoaJuridicaConta;
    
    /** Atributo cdTipoContratoConta. */
    private Integer cdTipoContratoConta;
    
    /** Atributo nrSequenciaContratoConta. */
    private Long nrSequenciaContratoConta;   

    
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdDestino.
	 *
	 * @return cdDestino
	 */
	public Integer getCdDestino() {
		return cdDestino;
	}
	
	/**
	 * Set: cdDestino.
	 *
	 * @param cdDestino the cd destino
	 */
	public void setCdDestino(Integer cdDestino) {
		this.cdDestino = cdDestino;
	}
	
	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}
	
	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}
	
	/**
	 * Get: cdIndicadorAgendados.
	 *
	 * @return cdIndicadorAgendados
	 */
	public String getCdIndicadorAgendados() {
		return cdIndicadorAgendados;
	}
	
	/**
	 * Set: cdIndicadorAgendados.
	 *
	 * @param cdIndicadorAgendados the cd indicador agendados
	 */
	public void setCdIndicadorAgendados(String cdIndicadorAgendados) {
		this.cdIndicadorAgendados = cdIndicadorAgendados;
	}
	
	/**
	 * Get: cdIndicadorNaoPagos.
	 *
	 * @return cdIndicadorNaoPagos
	 */
	public String getCdIndicadorNaoPagos() {
		return cdIndicadorNaoPagos;
	}
	
	/**
	 * Set: cdIndicadorNaoPagos.
	 *
	 * @param cdIndicadorNaoPagos the cd indicador nao pagos
	 */
	public void setCdIndicadorNaoPagos(String cdIndicadorNaoPagos) {
		this.cdIndicadorNaoPagos = cdIndicadorNaoPagos;
	}
	
	/**
	 * Get: cdIndicadorPagos.
	 *
	 * @return cdIndicadorPagos
	 */
	public String getCdIndicadorPagos() {
		return cdIndicadorPagos;
	}
	
	/**
	 * Set: cdIndicadorPagos.
	 *
	 * @param cdIndicadorPagos the cd indicador pagos
	 */
	public void setCdIndicadorPagos(String cdIndicadorPagos) {
		this.cdIndicadorPagos = cdIndicadorPagos;
	}
	
	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public Long getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}
	
	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}
	
	/**
	 * Get: cdOrigem.
	 *
	 * @return cdOrigem
	 */
	public Integer getCdOrigem() {
		return cdOrigem;
	}
	
	/**
	 * Set: cdOrigem.
	 *
	 * @param cdOrigem the cd origem
	 */
	public void setCdOrigem(Integer cdOrigem) {
		this.cdOrigem = cdOrigem;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: cdServicoRelacionado.
	 *
	 * @return cdServicoRelacionado
	 */
	public Integer getCdServicoRelacionado() {
		return cdServicoRelacionado;
	}
	
	/**
	 * Set: cdServicoRelacionado.
	 *
	 * @param cdServicoRelacionado the cd servico relacionado
	 */
	public void setCdServicoRelacionado(Integer cdServicoRelacionado) {
		this.cdServicoRelacionado = cdServicoRelacionado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public Integer getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}
	
	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(Integer cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}
	
	/**
	 * Get: digitoContaDebito.
	 *
	 * @return digitoContaDebito
	 */
	public String getDigitoContaDebito() {
		return digitoContaDebito;
	}
	
	/**
	 * Set: digitoContaDebito.
	 *
	 * @param digitoContaDebito the digito conta debito
	 */
	public void setDigitoContaDebito(String digitoContaDebito) {
		this.digitoContaDebito = digitoContaDebito;
	}
	
	/**
	 * Get: dtFimPagamento.
	 *
	 * @return dtFimPagamento
	 */
	public String getDtFimPagamento() {
		return dtFimPagamento;
	}
	
	/**
	 * Set: dtFimPagamento.
	 *
	 * @param dtFimPagamento the dt fim pagamento
	 */
	public void setDtFimPagamento(String dtFimPagamento) {
		this.dtFimPagamento = dtFimPagamento;
	}
	
	/**
	 * Get: dtInicioPagamento.
	 *
	 * @return dtInicioPagamento
	 */
	public String getDtInicioPagamento() {
		return dtInicioPagamento;
	}
	
	/**
	 * Set: dtInicioPagamento.
	 *
	 * @param dtInicioPagamento the dt inicio pagamento
	 */
	public void setDtInicioPagamento(String dtInicioPagamento) {
		this.dtInicioPagamento = dtInicioPagamento;
	}
	
	/**
	 * Get: nmMinemonicoFavorecido.
	 *
	 * @return nmMinemonicoFavorecido
	 */
	public String getNmMinemonicoFavorecido() {
		return nmMinemonicoFavorecido;
	}
	
	/**
	 * Set: nmMinemonicoFavorecido.
	 *
	 * @param nmMinemonicoFavorecido the nm minemonico favorecido
	 */
	public void setNmMinemonicoFavorecido(String nmMinemonicoFavorecido) {
		this.nmMinemonicoFavorecido = nmMinemonicoFavorecido;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdPessoaJuridicaConta.
	 *
	 * @return cdPessoaJuridicaConta
	 */
	public Long getCdPessoaJuridicaConta() {
		return cdPessoaJuridicaConta;
	}
	
	/**
	 * Set: cdPessoaJuridicaConta.
	 *
	 * @param cdPessoaJuridicaConta the cd pessoa juridica conta
	 */
	public void setCdPessoaJuridicaConta(Long cdPessoaJuridicaConta) {
		this.cdPessoaJuridicaConta = cdPessoaJuridicaConta;
	}
	
	/**
	 * Get: cdTipoContratoConta.
	 *
	 * @return cdTipoContratoConta
	 */
	public Integer getCdTipoContratoConta() {
		return cdTipoContratoConta;
	}
	
	/**
	 * Set: cdTipoContratoConta.
	 *
	 * @param cdTipoContratoConta the cd tipo contrato conta
	 */
	public void setCdTipoContratoConta(Integer cdTipoContratoConta) {
		this.cdTipoContratoConta = cdTipoContratoConta;
	}
	
	/**
	 * Get: nrSequenciaContratoConta.
	 *
	 * @return nrSequenciaContratoConta
	 */
	public Long getNrSequenciaContratoConta() {
		return nrSequenciaContratoConta;
	}
	
	/**
	 * Set: nrSequenciaContratoConta.
	 *
	 * @param nrSequenciaContratoConta the nr sequencia contrato conta
	 */
	public void setNrSequenciaContratoConta(Long nrSequenciaContratoConta) {
		this.nrSequenciaContratoConta = nrSequenciaContratoConta;
	}
	
	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}
	
	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}
	
	/**
	 * Get: numPercentualDescTarifa.
	 *
	 * @return numPercentualDescTarifa
	 */
	public BigDecimal getNumPercentualDescTarifa() {
		return numPercentualDescTarifa;
	}
	
	/**
	 * Set: numPercentualDescTarifa.
	 *
	 * @param numPercentualDescTarifa the num percentual desc tarifa
	 */
	public void setNumPercentualDescTarifa(BigDecimal numPercentualDescTarifa) {
		this.numPercentualDescTarifa = numPercentualDescTarifa;
	}
	
	/**
	 * Get: vlrTarifaAtual.
	 *
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}
	
	/**
	 * Set: vlrTarifaAtual.
	 *
	 * @param vlrTarifaAtual the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}
}