/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class SubstituirVincEnderecoParticipanteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class SubstituirVincEnderecoParticipanteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdFinalidadeEnderecoContrato
     */
    private int _cdFinalidadeEnderecoContrato = 0;

    /**
     * keeps track of state for field: _cdFinalidadeEnderecoContrato
     */
    private boolean _has_cdFinalidadeEnderecoContrato;

    /**
     * Field _cdPessoaEndereco
     */
    private long _cdPessoaEndereco = 0;

    /**
     * keeps track of state for field: _cdPessoaEndereco
     */
    private boolean _has_cdPessoaEndereco;

    /**
     * Field _cdPessoaJuridicaEndereco
     */
    private long _cdPessoaJuridicaEndereco = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaEndereco
     */
    private boolean _has_cdPessoaJuridicaEndereco;

    /**
     * Field _nrSequenciaEndereco
     */
    private int _nrSequenciaEndereco = 0;

    /**
     * keeps track of state for field: _nrSequenciaEndereco
     */
    private boolean _has_nrSequenciaEndereco;

    /**
     * Field _cdUsoEnderecoPessoa
     */
    private int _cdUsoEnderecoPessoa = 0;

    /**
     * keeps track of state for field: _cdUsoEnderecoPessoa
     */
    private boolean _has_cdUsoEnderecoPessoa;

    /**
     * Field _cdEspeceEndereco
     */
    private int _cdEspeceEndereco = 0;

    /**
     * keeps track of state for field: _cdEspeceEndereco
     */
    private boolean _has_cdEspeceEndereco;

    /**
     * Field _cdUsoPostal
     */
    private int _cdUsoPostal = 0;

    /**
     * keeps track of state for field: _cdUsoPostal
     */
    private boolean _has_cdUsoPostal;


      //----------------/
     //- Constructors -/
    //----------------/

    public SubstituirVincEnderecoParticipanteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.request.SubstituirVincEnderecoParticipanteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdEspeceEndereco
     * 
     */
    public void deleteCdEspeceEndereco()
    {
        this._has_cdEspeceEndereco= false;
    } //-- void deleteCdEspeceEndereco() 

    /**
     * Method deleteCdFinalidadeEnderecoContrato
     * 
     */
    public void deleteCdFinalidadeEnderecoContrato()
    {
        this._has_cdFinalidadeEnderecoContrato= false;
    } //-- void deleteCdFinalidadeEnderecoContrato() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaEndereco
     * 
     */
    public void deleteCdPessoaEndereco()
    {
        this._has_cdPessoaEndereco= false;
    } //-- void deleteCdPessoaEndereco() 

    /**
     * Method deleteCdPessoaJuridicaEndereco
     * 
     */
    public void deleteCdPessoaJuridicaEndereco()
    {
        this._has_cdPessoaJuridicaEndereco= false;
    } //-- void deleteCdPessoaJuridicaEndereco() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdUsoEnderecoPessoa
     * 
     */
    public void deleteCdUsoEnderecoPessoa()
    {
        this._has_cdUsoEnderecoPessoa= false;
    } //-- void deleteCdUsoEnderecoPessoa() 

    /**
     * Method deleteCdUsoPostal
     * 
     */
    public void deleteCdUsoPostal()
    {
        this._has_cdUsoPostal= false;
    } //-- void deleteCdUsoPostal() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaEndereco
     * 
     */
    public void deleteNrSequenciaEndereco()
    {
        this._has_nrSequenciaEndereco= false;
    } //-- void deleteNrSequenciaEndereco() 

    /**
     * Returns the value of field 'cdEspeceEndereco'.
     * 
     * @return int
     * @return the value of field 'cdEspeceEndereco'.
     */
    public int getCdEspeceEndereco()
    {
        return this._cdEspeceEndereco;
    } //-- int getCdEspeceEndereco() 

    /**
     * Returns the value of field 'cdFinalidadeEnderecoContrato'.
     * 
     * @return int
     * @return the value of field 'cdFinalidadeEnderecoContrato'.
     */
    public int getCdFinalidadeEnderecoContrato()
    {
        return this._cdFinalidadeEnderecoContrato;
    } //-- int getCdFinalidadeEnderecoContrato() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaEndereco'.
     * 
     * @return long
     * @return the value of field 'cdPessoaEndereco'.
     */
    public long getCdPessoaEndereco()
    {
        return this._cdPessoaEndereco;
    } //-- long getCdPessoaEndereco() 

    /**
     * Returns the value of field 'cdPessoaJuridicaEndereco'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaEndereco'.
     */
    public long getCdPessoaJuridicaEndereco()
    {
        return this._cdPessoaJuridicaEndereco;
    } //-- long getCdPessoaJuridicaEndereco() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdUsoEnderecoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdUsoEnderecoPessoa'.
     */
    public int getCdUsoEnderecoPessoa()
    {
        return this._cdUsoEnderecoPessoa;
    } //-- int getCdUsoEnderecoPessoa() 

    /**
     * Returns the value of field 'cdUsoPostal'.
     * 
     * @return int
     * @return the value of field 'cdUsoPostal'.
     */
    public int getCdUsoPostal()
    {
        return this._cdUsoPostal;
    } //-- int getCdUsoPostal() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaEndereco'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaEndereco'.
     */
    public int getNrSequenciaEndereco()
    {
        return this._nrSequenciaEndereco;
    } //-- int getNrSequenciaEndereco() 

    /**
     * Method hasCdEspeceEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEspeceEndereco()
    {
        return this._has_cdEspeceEndereco;
    } //-- boolean hasCdEspeceEndereco() 

    /**
     * Method hasCdFinalidadeEnderecoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidadeEnderecoContrato()
    {
        return this._has_cdFinalidadeEnderecoContrato;
    } //-- boolean hasCdFinalidadeEnderecoContrato() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaEndereco()
    {
        return this._has_cdPessoaEndereco;
    } //-- boolean hasCdPessoaEndereco() 

    /**
     * Method hasCdPessoaJuridicaEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaEndereco()
    {
        return this._has_cdPessoaJuridicaEndereco;
    } //-- boolean hasCdPessoaJuridicaEndereco() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdUsoEnderecoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUsoEnderecoPessoa()
    {
        return this._has_cdUsoEnderecoPessoa;
    } //-- boolean hasCdUsoEnderecoPessoa() 

    /**
     * Method hasCdUsoPostal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUsoPostal()
    {
        return this._has_cdUsoPostal;
    } //-- boolean hasCdUsoPostal() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaEndereco()
    {
        return this._has_nrSequenciaEndereco;
    } //-- boolean hasNrSequenciaEndereco() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdEspeceEndereco'.
     * 
     * @param cdEspeceEndereco the value of field 'cdEspeceEndereco'
     */
    public void setCdEspeceEndereco(int cdEspeceEndereco)
    {
        this._cdEspeceEndereco = cdEspeceEndereco;
        this._has_cdEspeceEndereco = true;
    } //-- void setCdEspeceEndereco(int) 

    /**
     * Sets the value of field 'cdFinalidadeEnderecoContrato'.
     * 
     * @param cdFinalidadeEnderecoContrato the value of field
     * 'cdFinalidadeEnderecoContrato'.
     */
    public void setCdFinalidadeEnderecoContrato(int cdFinalidadeEnderecoContrato)
    {
        this._cdFinalidadeEnderecoContrato = cdFinalidadeEnderecoContrato;
        this._has_cdFinalidadeEnderecoContrato = true;
    } //-- void setCdFinalidadeEnderecoContrato(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaEndereco'.
     * 
     * @param cdPessoaEndereco the value of field 'cdPessoaEndereco'
     */
    public void setCdPessoaEndereco(long cdPessoaEndereco)
    {
        this._cdPessoaEndereco = cdPessoaEndereco;
        this._has_cdPessoaEndereco = true;
    } //-- void setCdPessoaEndereco(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaEndereco'.
     * 
     * @param cdPessoaJuridicaEndereco the value of field
     * 'cdPessoaJuridicaEndereco'.
     */
    public void setCdPessoaJuridicaEndereco(long cdPessoaJuridicaEndereco)
    {
        this._cdPessoaJuridicaEndereco = cdPessoaJuridicaEndereco;
        this._has_cdPessoaJuridicaEndereco = true;
    } //-- void setCdPessoaJuridicaEndereco(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdUsoEnderecoPessoa'.
     * 
     * @param cdUsoEnderecoPessoa the value of field
     * 'cdUsoEnderecoPessoa'.
     */
    public void setCdUsoEnderecoPessoa(int cdUsoEnderecoPessoa)
    {
        this._cdUsoEnderecoPessoa = cdUsoEnderecoPessoa;
        this._has_cdUsoEnderecoPessoa = true;
    } //-- void setCdUsoEnderecoPessoa(int) 

    /**
     * Sets the value of field 'cdUsoPostal'.
     * 
     * @param cdUsoPostal the value of field 'cdUsoPostal'.
     */
    public void setCdUsoPostal(int cdUsoPostal)
    {
        this._cdUsoPostal = cdUsoPostal;
        this._has_cdUsoPostal = true;
    } //-- void setCdUsoPostal(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaEndereco'.
     * 
     * @param nrSequenciaEndereco the value of field
     * 'nrSequenciaEndereco'.
     */
    public void setNrSequenciaEndereco(int nrSequenciaEndereco)
    {
        this._nrSequenciaEndereco = nrSequenciaEndereco;
        this._has_nrSequenciaEndereco = true;
    } //-- void setNrSequenciaEndereco(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return SubstituirVincEnderecoParticipanteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.request.SubstituirVincEnderecoParticipanteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.request.SubstituirVincEnderecoParticipanteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.request.SubstituirVincEnderecoParticipanteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.request.SubstituirVincEnderecoParticipanteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
