package br.com.bradesco.web.pgit.service.data.pdc;

import br.com.bradesco.web.pgit.service.data.pdc.alteraragenciagestora.IAlterarAgenciaGestoraPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alteraralcada.IAlterarAlcadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterararquivoservico.IAlterarArquivoServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcondcobtarifa.IAlterarCondCobTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcondreajustetarifa.IAlterarCondReajusteTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.IAlterarConfContaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaolayoutarqcontrato.IAlterarConfiguracaoLayoutArqContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.IAlterarConfiguracaoTipoServModContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcontacomplementar.IAlterarContaComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.IAlterarDadosBasicoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarduplicacaoarqretorno.IAlterarDuplicacaoArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarempresaacompanhada.IAlterarEmpresaAcompanhadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarexcecaoregrasegregacaoacesso.IAlterarExcecaoRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarhorariolimite.IAlterarHorarioLimitePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlayoutservico.IAlterarLayoutServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.IAlterarLiberacaoPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.IAlterarLimDIaOpePgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlimiteintrapgit.IAlterarLimiteIntraPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarlinhamsgcomprovante.IAlterarLinhaMsgComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarliquidacaopagamento.IAlterarLiquidacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsgalertagestor.IAlterarMsgAlertaGestorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsgcomprovantesalrl.IAlterarMsgComprovanteSalrlPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsghistoricocomplementar.IAlterarMsgHistoricoComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.IAlterarMsgLancamentoPersonalizadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.IAlterarMsgLayoutArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.IAlterarOperacaoServicoPagtoIntegradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarorgaopagador.IAlterarOrgaoPagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantecontratopgit.IAlterarParticipanteContratoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantesorgaopublico.IAlterarParticipantesOrgaoPublicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarperfilcontrato.IAlterarPerfilContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarperfiltrocaarquivo.IAlterarPerfilTrocaArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarprioridadetipocompromisso.IAlterarPrioridadeTipoCompromissoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarprocessomassivo.IAlterarProcessoMassivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarregrasegregacaoacesso.IAlterarRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarrepresentantecontratopgit.IAlterarRepresentanteContratoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarservicocomposto.IAlterarServicoCompostoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarservicorelacionado.IAlterarServicoRelacionadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterartarifaacimapadrao.IAlterarTarifaAcimaPadraoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.IAlterarTercParticiContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.IAlterarTipoComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterartipopendenciaresponsaveis.IAlterarTipoPendenciaResponsaveisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterartiporetornolayout.IAlterarTipoRetornoLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarultimonumeroremessa.IAlterarUltimoNumeroRemessaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarultimonumeroretorno.IAlterarUltimoNumeroRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarvincconvnctasalario.IAlterarVincConvnCtaSalarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarvinculocontasalariodestino.IAlterarVinculoContaSalarioDestinoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.anteciparpostergarpagtosindividuais.IAnteciparPostergarPagtosIndividuaisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosintegral.IAntPostergarPagtosIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.antpostergarpagtosparcial.IAntPostergarPagtosParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.aprovarsolicitacaoconsistprevia.IAprovarSolicitacaoConsistPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.ativaraditivocontratopendass.IAtivarAditivoContratoPendAssPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.ativarcontratopendassinatura.IAtivarContratoPendAssinaturaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitointegral.IAutDesListaDebitoIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autdeslistadebitoparcial.IAutDesListaDebitoParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.authenticationservice.IAuthenticationServicePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarintegral.IAutorizarIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarpagtosindividuais.IAutorizarPagtosIndividuaisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarparcial.IAutorizarParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.autorizarsolicitacaoestornopatos.IAutorizarSolicitacaoEstornoPatosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqdesbloqemissaoautavisoscomprv.IBloqDesbloqEmissaoAutAvisosComprvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearcontafavorecido.IBloquearContaFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.IBloquearDesbloquearContaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontrato.IBloquearDesbloquearContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontratopgit.IBloquearDesbloquearContratoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.IBloquearDesbloquearProdutoServContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearfavorecido.IBloquearFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloquearorgaopagador.IBloquearOrgaoPagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.IBloqueioDesbloqueioProcessoMassivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaointegral.ICancelarLiberacaoIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaoparcial.ICancelarLiberacaoParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarlibpagtosindsemconsultasaldo.ICancelarLibPagtosIndSemConsultaSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarmanutencaoparticipante.ICancelarManutencaoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosindividuais.ICancelarPagtosIndividuaisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosintegral.ICancelarPagtosIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarpagtosparcial.ICancelarPagtosParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.ICancelarRelatoriosContratosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.IConContratoMigradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.IConfCestaTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.conpagtossolrecvennaopago.IConPagtosSolRecVenNaoPagoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.IConRelacaoConManutencoesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.IConsistirDadosLogicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.IConsSolicitacaoEstornoPagtosOPPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultapostergarconsolidadosolicitacao.IConsultaPostergarConsolidadoSolicitacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.IConsultarAditivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.IConsultarAgenciaOperadoraPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.IConsultarAgenciaOpeTarifaPadraoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.IConsultarArquivoRecuperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.IConsultarAssVersaoLayoutLoteServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.IConsultarAutenticacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarautorizantesnetempresa.IConsultarAutorizantesNetEmpresaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.IConsultarBancoAgenciaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.IConsultarBloqueioRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcanlibpagtosconssemconssaldo.IConsultarCanLibPagtosConsSemConsSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcanlibpagtosindsemconssaldo.IConsultarCanLibPagtosIndSemConsSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcondcobtarifa.IConsultarCondCobTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcondreajustetarifa.IConsultarCondReajusteTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.IConsultarConfigPadraoAtribTipoServModPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.IConsultarConfigTipoServicoModalidadePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanambpart.IConsultarConManAmbPartPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanconta.IConsultarConManContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.IConsultarConManDadosBasicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.IConsultarConManLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanparticipantes.IConsultarConManParticipantesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.IConsultarConManServicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconmantarifas.IConsultarConManTarifasPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontacontrato.IConsultarContaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontadebtarifa.IConsultarContaDebTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontrato.IConsultarContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.IConsultarContratoClienteFiltroPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.IConsultarDadosBasicoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardadoscliente.IConsultarDadosClientePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescbancoagenciaconta.IConsultarDescBancoAgenciaContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaogrupoeconomico.IConsultarDescricaoGrupoEconomicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaomensagem.IConsultarDescricaoMensagemPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardescricaounidadeorganizacional.IConsultarDescricaoUnidadeOrganizacionalPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontafavorecido.IConsultarDetalheContaFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.IConsultarDetalheContratoMsgPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.IConsultarDetalheFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.IConsultarDetalheHistoricoContaFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.IConsultarDetalheLinhaMensagemPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhemsgcomprovantesalrl.IConsultarDetalheMsgComprovanteSalrlPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.IConsultarDetalhesSolicitacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.IConsultarDetalhesSolicitacaoOPPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.IConsultarDetalheVinculoContaSalarioDestinoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.IConsultarDetHistContaSalarioDestinoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardethistctasalarioempresa.IConsultarDetHistCtaSalarioEmpresaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetliberacaoprevia.IConsultarDetLiberacaoPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetmanutencaoparticipante.IConsultarDetManutencaoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.IConsultarDetVinculoCtaSalarioEmpresaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultaremail.IConsultarEmailPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarempresaacompanhada.IConsultarEmpresaAcompanhadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarendereco.IConsultarEnderecoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.IConsultarEstatisticasLotePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.IConsultarHistConsultaSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistmanutmsglancpersonalizado.IConsultarHistManutMsgLancPersonalizadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistoricoperfiltrocaarquivo.IConsultarHistoricoPerfilTrocaArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarimprimirpgtoconsolidado.IConsultarImprimirPgtoConsolidadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarimprimirpgtoindividual.IConsultarImprimirPgtoIndividualPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.IConsultarInconsistenciaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistenciaretorno.IConsultarInconsistenciaRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.IConsultarInformacoesFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.IConsultarLayoutArquivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.IConsultarLiberacaoPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlibpagtosconssemconsultasaldo.IConsultarLibPagtosConsSemConsultaSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlibpagtosindsemconssaldo.IConsultarLibPagtosIndSemConsSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimdiarioutilizado.IConsultarLimDiarioUtilizadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimiteconta.IConsultarLimiteContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimitehistorico.IConsultarLimiteHistoricoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlimiteintrapgit.IConsultarLimiteIntraPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaclientepessoas.IConsultarListaClientePessoasPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.IConsultarListaContaFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontasinclusao.IConsultarListaContasInclusaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.IConsultarListaContratosManutencaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratospessoas.IConsultarListaContratosPessoasPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.IConsultarListaDebitoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebitoautdes.IConsultarListaDebitoAutDesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.IConsultarListaFuncionarioBradescoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistahistoricocontafavorecido.IConsultarListaHistoricoContaFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistahistoricofavorecido.IConsultarListaHistoricoFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistalayoutarquivocontrato.IConsultarListaLayoutArquivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistamodalidades.IConsultarListaModalidadesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.IConsultarListaOperacaoTarifaTipoServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistapagamento.IConsultarListaPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaparticipantescontrato.IConsultarListaParticipantesContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.IConsultarListaPendenciaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivo.IConsultarListaPerfilTrocaArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperfiltrocaarquivoloop.IConsultarListaPerfilTrocaArquivoLoopPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaperiodicidadesistemapgic.IConsultarListaPeriodicidadeSistemaPGICPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservicospgto.IConsultarListaServicosPgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.IConsultarListaServLimDiaIndvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistasolicitacaorastreamento.IConsultarListaSolicitacaoRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistatiporetornolayout.IConsultarListaTipoRetornoLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistavaloresdiscretos.IConsultarListaValoresDiscretosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlote.IConsultarLotePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarloteretorno.IConsultarLoteRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlotesincsolic.IConsultarLotesIncSolicPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoconfigcontrato.IConsultarManutencaoConfigContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontacontrato.IConsultarManutencaoContaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaocontrato.IConsultarManutencaoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.IConsultarManutencaoMeioTransmissaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.IConsultarManutencaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipante.IConsultarManutencaoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.IConsultarManutencaoServicoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidade.IConsultarModalidadePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadepagto.IConsultarModalidadePagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadepgit.IConsultarModalidadePGITPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmodalidadeservico.IConsultarModalidadeServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.IConsultarMotivoBloqueioContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaoestorno.IConsultarMotivoSituacaoEstornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagamento.IConsultarMotivoSituacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmotivosituacaopagpendente.IConsultarMotivoSituacaoPagPendentePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.IConsultarMsgLancamentoPersonalizadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.IConsultarMsgLayoutArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciaretorno.IConsultarOcorrenciaRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrencias.IConsultarOcorrenciasPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.IConsultarOcorrenciasSolicLotePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarordempagtoporagepagadora.IConsultarOrdemPagtoPorAgePagadoraPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.IConsultarPagamentoFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentosconsolidados.IConsultarPagamentosConsolidadosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentosindividuais.IConsultarPagamentosIndividuaisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagotsvencnaopagos.IConsultarPagotsVencNaoPagosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtoindpendautorizacao.IConsultarPagtoIndPendAutorizacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtopendconsolidado.IConsultarPagtoPendConsolidadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtopendindividual.IConsultarPagtoPendIndividualPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsantpostergacao.IConsultarPagtosConsAntPostergacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsautorizados.IConsultarPagtosConsAutorizadosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconsparacancelamento.IConsultarPagtosConsParaCancelamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosconspendautorizacao.IConsultarPagtosConsPendAutorizacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.IConsultarPagtosEstornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestornoop.IConsultarPagtosEstornoOPPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindantpostergacao.IConsultarPagtosIndAntPostergacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindautorizados.IConsultarPagtosIndAutorizadosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosindparacancelamento.IConsultarPagtosIndParaCancelamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.IConsultarPagtoSolEmiCompFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaoestorno.IConsultarPagtosSolicitacaoEstornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.IConsultarPagtosSolicitacaoRecuperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.IConsultarParticipanteContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.IConsultarParticipantesContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.IConsultarPendenciaContratoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.IConsultarPosDiaPagtoAgenciaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.IConsultarQtdeValorPgtoPrevistoSolicPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.IConsultarRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.IConsultarSaldoAtualPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservico.IConsultarServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicolancamentocnab.IConsultarServicoLancamentoCnabPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicopagto.IConsultarServicoPagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.IConsultarServicosCatalogoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarservrelacservperacional.IConsultarServRelacServperacionalPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaopagamento.IConsultarSituacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolconsistenciaprevia.IConsultarSituacaoSolConsistenciaPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsituacaosolicitacaoestorno.IConsultarSituacaoSolicitacaoEstornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtocliepagador.IConsultarSolEmiAviMovtoCliePagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemiavimovtofavorecido.IConsultarSolEmiAviMovtoFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.IConsultarSolEmiComprovanteCliePagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.IConsultarSolEmiComprovanteFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicantposdtpgto.IConsultarSolicAntPosDtPgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicantproclotepgtoagda.IConsultarSolicAntProcLotePgtoAgdaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsoliccancelamentolotepgto.IConsultarSolicCancelamentoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicexclusaolotepgto.IConsultarSolicExclusaoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambiente.IConsultarSolicitacaoAmbientePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoambientepartc.IConsultarSolicitacaoAmbientePartcPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.IConsultarSolicitacaoConsistPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.IConsultarSolicitacaoEstornoPagtosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambiente.IConsultarSolicitacaoMudancaAmbientePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.IConsultarSolicitacaoMudancaAmbPartcPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicliberacaolotepgto.IConsultarSolicLiberacaoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.IConsultarSolicRecuperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecpagtosvencnaopago.IConsultarSolRecPagtosVencNaoPagoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecuperacaopagtosbackup.IConsultarSolRecuperacaoPagtosBackupPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.IConsultarTarifaCatalogoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultartarifacontrato.IConsultarTarifaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultartarifacontratoloop.IConsultarTarifaContratoLoopPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultartercpartcontrato.IConsultarTercPartContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.IConsultarTipoContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.IConsultarUltimoNumeroRemessaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroretorno.IConsultarUltimoNumeroRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.IConsultarUsuarioSoliRelatorioOrgaosPublicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarvinculacaomsglayoutsistema.IConsultarVinculacaoMsgLayoutSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consutarcontrenvioformaliquidacao.IConsutarContrEnvioFormaLiquidacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarintegral.IDesautorizarIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarpagtosindividuais.IDesautorizarPagtosIndividuaisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.desautorizarparcial.IDesautorizarParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.IDetalharAditivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.IDetalharAlcadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharambiente.IDetalharAmbientePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharantpostergarpagtos.IDetalharAntPostergarPagtosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharasslotelayoutarqservico.IDetalharAssLoteLayoutArqServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharasslotelayoutarquivo.IDetalharAssLoteLayoutArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharassloteoperacaoservico.IDetalharAssLoteOperacaoServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharassretornolayoutservico.IDetalharAssRetornoLayoutServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharbloqueiorastreamento.IDetalharBloqueioRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.IDetalharCanLibPagtosConsSemConsSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcnpjficticio.IDetalharCnpjFicticioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.IDetalharContaComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontarelacionadahist.IDetalharContaRelacionadaHistPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.IDetalharContratoMigradoHsbcPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontrenvioformaliquidacao.IDetalharContrEnvioFormaLiquidacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalhardadoscontrato.IDetalharDadosContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.IDetalharDuplicacaoArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharemissaoautavisoscomprv.IDetalharEmissaoAutAvisosComprvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharenderecoparticipante.IDetalharEnderecoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.IDetalharExcecaoRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharfinalidadeenderecooperacao.IDetalharFinalidadeEnderecoOperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharfolha.IDetalharFolhaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.IDetalharHistAssocTrocaArqParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldo.IDetalharHistConsultaSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistemissaoautavisoscomprv.IDetalharHistEmissaoAutAvisosComprvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.IDetalharHistLiquidacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistmanutmsghistcomplementar.IDetalharHistManutMsgHistComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistmanutmsglancpersonalizado.IDetalharHistManutMsgLancPersonalizadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.IDetalharHistoricoBloqueioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricodebtarifa.IDetalharHistoricoDebTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoenderecoparticipante.IDetalharHistoricoEnderecoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricofavorecido.IDetalharHistoricoFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricolayoutcontrato.IDetalharHistoricoLayoutContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.IDetalharHistoricoOperacaoServicoPagtoIntegradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoorgaopagador.IDetalharHistoricoOrgaoPagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.IDetalharHistoricoPerfilTrocaArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhorariolimite.IDetalharHorarioLimitePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlayoutarqservico.IDetalharLayoutArqServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlibpagtosconssemconsultasaldo.IDetalharLibPagtosConsSemConsultaSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.IDetalharLiquidacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.IDetalharListaDebitoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.IDetalharListaDebitoAutDesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocodigobarras.IDetalharManPagtoCodigoBarrasPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.IDetalharManPagtoCreditoContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodarf.IDetalharManPagtoDARFPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodebitoconta.IDetalharManPagtoDebitoContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodebitoveiculos.IDetalharManPagtoDebitoVeiculosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodepidentificado.IDetalharManPagtoDepIdentificadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodoc.IDetalharManPagtoDOCPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogare.IDetalharManPagtoGAREPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.IDetalharManPagtoGPSPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoordemcredito.IDetalharManPagtoOrdemCreditoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoordempagto.IDetalharManPagtoOrdemPagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoted.IDetalharManPagtoTEDPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtotitulobradesco.IDetalharManPagtoTituloBradescoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtotitulooutrosbancos.IDetalharManPagtoTituloOutrosBancosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmoedasarquivo.IDetalharMoedasArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmoedassistema.IDetalharMoedasSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.IDetalharMsgAlertaGestorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsghistoricocomplementar.IDetalharMsgHistoricoComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsglancamentopersonalizado.IDetalharMsgLancamentoPersonalizadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.IDetalharMsgLayoutArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharoperacaoservicopagtointegrado.IDetalharOperacaoServicoPagtoIntegradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharorgaopagador.IDetalharOrgaoPagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.IDetalharPagamentosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentosconsolidados.IDetalharPagamentosConsolidadosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtocodigobarras.IDetalharPagtoCodigoBarrasPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtocreditoconta.IDetalharPagtoCreditoContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodarf.IDetalharPagtoDARFPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodebitoconta.IDetalharPagtoDebitoContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodebitoveiculos.IDetalharPagtoDebitoVeiculosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodepidentificado.IDetalharPagtoDepIdentificadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodoc.IDetalharPagtoDOCPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtogare.IDetalharPagtoGAREPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtogps.IDetalharPagtoGPSPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoordemcredito.IDetalharPagtoOrdemCreditoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoordempagto.IDetalharPagtoOrdemPagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsautorizados.IDetalharPagtosConsAutorizadosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconsparacancelamento.IDetalharPagtosConsParaCancelamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtosconspendautorizacao.IDetalharPagtosConsPendAutorizacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoted.IDetalharPagtoTEDPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtotitulobradesco.IDetalharPagtoTituloBradescoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtotitulooutrosbancos.IDetalharPagtoTituloOutrosBancosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharparticipantesorgaopublico.IDetalharParticipantesOrgaoPublicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharperfiltrocaarquivo.IDetalharPerfilTrocaArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpiramide.IDetalharPiramidePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.IDetalharPorContaDebitoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.IDetalharPrioridadeTipoCompromissoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharprocessomassivo.IDetalharProcessoMassivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharregrasegregacaoacesso.IDetalharRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.IDetalharRelatorioContratosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicocomposto.IDetalharServicoCompostoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicolancamentocnab.IDetalharServicoLancamentoCnabPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharservicorelacionado.IDetalharServicoRelacionadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasefavorecido.IDetalharSolBaseFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.IDetalharSolBasePagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.IDetalharSolBaseRetransmissaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasesistema.IDetalharSolBaseSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtocliepagador.IDetalharSolEmiAviMovtoCliePagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.IDetalharSolEmiAviMovtoFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemicomprovantecliepagador.IDetalharSolEmiComprovanteCliePagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemicomprovantefavorecido.IDetalharSolEmiComprovanteFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantposdtpgto.IDetalharSolicAntPosDtPgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicantproclotepgtoagda.IDetalharSolicAntProcLotePgtoAgdaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsoliccancelamentolotepgto.IDetalharSolicCancelamentoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicexclusaolotepgto.IDetalharSolicExclusaoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorastreamento.IDetalharSolicitacaoRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitacaorelatoriofavorecidos.IDetalharSolicitacaoRelatorioFavorecidosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.IDetalharSolicitCriacaoContaTipoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicliberacaolotepgto.IDetalharSolicLiberacaoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.IDetalharSolicRecuperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolirelatoriopagamento.IDetalharSoliRelatorioPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartipocomprovante.IDetalharTipoComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartipopendenciaresponsaveis.IDetalharTipoPendenciaResponsaveisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartiporetornolayout.IDetalharTipoRetornoLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.IDetalharTipoServModContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.IDetalharVincConvnCtaSalarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsghistcomplementaroperacao.IDetalharVincMsgHistComplementarOperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvincmsglancoper.IDetalharVincMsgLancOperPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvinctrocaarqparticipante.IDetalharVincTrocaArqParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.IDetalharVinculacaoMsgLayoutSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.IDetalhePostergarConsolidadoSolicitacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.IEfetuarCalcOperReceitaTarifasPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.IEfetuarValidacaoConfigAtribModalidadePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.IEfetuarValidacaoTarifaOperTipoServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.encerrarcontrato.IEncerrarContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentos.IEstornarPagamentosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.IEstornarPagamentosOPPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluiraditivocontrato.IExcluirAditivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralcada.IExcluirAlcadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralteracaoambiente.IExcluirAlteracaoAmbientePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluiralteracaoambientepartc.IExcluirAlteracaoAmbientePartcPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirasslotelayoutarqservico.IExcluirAssLoteLayoutArqServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirasslotelayoutarquivo.IExcluirAssLoteLayoutArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassloteoperacaoservico.IExcluirAssLoteOperacaoServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassociacaocontratomsg.IExcluirAssociacaoContratoMsgPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassretornolayoutservico.IExcluirAssRetornoLayoutServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassversaolayoutloteservico.IExcluirAssVersaoLayoutLoteServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirbloqueiorastreamento.IExcluirBloqueioRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluircnpjficticio.IExcluirCnpjFicticioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontacomplementar.IExcluirContaComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontacontrato.IExcluirContaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontrenvioarqformaliquidacao.IExcluirContrEnvioArqFormaLiquidacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirduplicacaoarqretorno.IExcluirDuplicacaoArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluiremissaoautavisoscomprv.IExcluirEmissaoAutAvisosComprvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirempresaacompanhada.IExcluirEmpresaAcompanhadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirenderecoparticipante.IExcluirEnderecoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirexcecaoregrasegregacaoacesso.IExcluirExcecaoRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirfinalidadeenderecooperacao.IExcluirFinalidadeEnderecoOperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirhorariolimite.IExcluirHorarioLimitePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlayout.IExcluirLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlayoutarqservico.IExcluirLayoutArqServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirliberacaoprevia.IExcluirLiberacaoPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirlinhamsgcomprovante.IExcluirLinhaMsgComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.IExcluirLiquidacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmoedasarquivo.IExcluirMoedasArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmoedassistema.IExcluirMoedasSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.IExcluirMsgAlertaGestorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsgcomprovantesalrl.IExcluirMsgComprovanteSalrlPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsghistoricocomplementar.IExcluirMsgHistoricoComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsglancamentopersonalizado.IExcluirMsgLancamentoPersonalizadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.IExcluirMsgLayoutArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluiroperacaoservicopagtointegrado.IExcluirOperacaoServicoPagtoIntegradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirorgaopagador.IExcluirOrgaoPagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparametrocontrato.IExcluirParametroContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparticipantecontratopgit.IExcluirParticipanteContratoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparticipantesorgaopublico.IExcluirParticipantesOrgaoPublicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirperfiltrocaarquivo.IExcluirPerfilTrocaArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirpostergarconsolidadosolicitacao.IExcluirPostergarConsolidadoSolicitacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.IExcluirPrioridadeTipoCompromissoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirprocessomassivo.IExcluirProcessoMassivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirregrasegregacaoacesso.IExcluirRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirrelaciassoc.IExcluirRelaciAssocPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicocomposto.IExcluirServicoCompostoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicolancamentocnab.IExcluirServicoLancamentoCnabPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirservicorelacionado.IExcluirServicoRelacionadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasefavorecido.IExcluirSolBaseFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasepagto.IExcluirSolBasePagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbaseretransmissao.IExcluirSolBaseRetransmissaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasesistema.IExcluirSolBaseSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtocliepagador.IExcluirSolEmiAviMovtoCliePagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemiavimovtofavorecido.IExcluirSolEmiAviMovtoFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantecliepagador.IExcluirSolEmiComprovanteCliePagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.IExcluirSolEmiComprovanteFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicantposdtpgto.IExcluirSolicAntPosDtPgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicantproclotepgtoagda.IExcluirSolicAntProcLotePgtoAgdaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsoliccancelamentolotepgto.IExcluirSolicCancelamentoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicexclusaolotepgto.IExcluirSolicExclusaoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaoestornopagtos.IExcluirSolicitacaoEstornoPagtosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaorastreamento.IExcluirSolicitacaoRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitacaorelatoriofavorecidos.IExcluirSolicitacaoRelatorioFavorecidosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitcriacaocontatipo.IExcluirSolicitCriacaoContaTipoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicliberacaolotepgto.IExcluirSolicLiberacaoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicrecuperacao.IExcluirSolicRecuperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatorioempresaacompanhada.IExcluirSoliRelatorioEmpresaAcompanhadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatorioorgaospublicos.IExcluirSoliRelatorioOrgaosPublicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatoriopagamento.IExcluirSoliRelatorioPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolrecpagtosvencidonaopago.IExcluirSolRecPagtosVencidoNaoPagoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolrecuperacaopagtos.IExcluirSolRecuperacaoPagtosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirterceiropartcontrato.IExcluirTerceiroPartContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtipocomprovante.IExcluirTipoComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtipopendenciaresponsaveis.IExcluirTipoPendenciaResponsaveisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtiporetornolayout.IExcluirTipoRetornoLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtiposervmodcontrato.IExcluirTipoServModContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincconvnctasalario.IExcluirVincConvnCtaSalarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincmsghistcomplementaroperacao.IExcluirVincMsgHistComplementarOperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvincmsglancoper.IExcluirVincMsgLancOperPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinctrocaarqparticipante.IExcluirVincTrocaArqParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculacaomsglayoutsistema.IExcluirVinculacaoMsgLayoutSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculocontasalariodestino.IExcluirVinculoContaSalarioDestinoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculoctasalarioempresa.IExcluirVinculoCtaSalarioEmpresaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.impdiaad.IImpdiaadPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.IImprimirAditivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontrato.IImprimirAnexoPrimeiroContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.IImprimirAnexoPrimeiroContratoLoopPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.IImprimirAnexoSegundoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecodigobarra.IImprimirComprovanteCodigoBarraPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecredconta.IImprimirComprovanteCredContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantedarf.IImprimirComprovanteDARFPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantedoc.IImprimirComprovanteDOCPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantegare.IImprimirComprovanteGAREPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantegps.IImprimirComprovanteGPSPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovanteop.IImprimirComprovanteOPPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovanteted.IImprimirComprovanteTEDPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.IImprimirComprovanteTituloBradescoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulooutros.IImprimirComprovanteTituloOutrosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.IImprimirContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.IImprimirContratoIntranetPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontratomulticanal.IImprimirContratoMulticanalPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirpagamentosconsolidados.IImprimirPagamentosConsolidadosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirpagamentosindividuais.IImprimirPagamentosIndividuaisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirrecisaocontratopgitmulticanal.IImprimirRecisaoContratoPgitMulticanalPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluiraditivocontrato.IIncluirAditivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.IIncluirAlcadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambiente.IIncluirAlteracaoAmbientePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluiralteracaoambientepartc.IIncluirAlteracaoAmbientePartcPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirasslotelayoutarqservico.IIncluirAssLoteLayoutArqServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirasslotelayoutarquivo.IIncluirAssLoteLayoutArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassloteoperacaoservico.IIncluirAssLoteOperacaoServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassociacaocontratomsg.IIncluirAssociacaoContratoMsgPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassretornolayoutservico.IIncluirAssRetornoLayoutServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassversaolayoutloteservico.IIncluirAssVersaoLayoutLoteServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirbloqueiorastreamento.IIncluirBloqueioRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluircnpjficticio.IIncluirCnpjFicticioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.IIncluirContaComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontacontrato.IIncluirContaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontadebtarifa.IIncluirContaDebTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontratopgit.IIncluirContratoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontrenvioarqformaliquidacao.IIncluirContrEnvioArqFormaLiquidacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirduplicacaoarqretorno.IIncluirDuplicacaoArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluiremissaoautavisoscomprv.IIncluirEmissaoAutAvisosComprvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirempresaacompanhada.IIncluirEmpresaAcompanhadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirenderecoparticipante.IIncluirEnderecoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.IIncluirEquiparacaoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirexcecaoregrasegregacaoacesso.IIncluirExcecaoRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirfinalidadeenderecooperacao.IIncluirFinalidadeEnderecoOperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirhorariolimite.IIncluirHorarioLimitePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlayout.IIncluirLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.IIncluirLayoutArqServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirliberacaoprevia.IIncluirLiberacaoPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirlinhamsgcomprovante.IIncluirLinhaMsgComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.IIncluirLiquidacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmoedasarquivo.IIncluirMoedasArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmoedassistema.IIncluirMoedasSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsgalertagestor.IIncluirMsgAlertaGestorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsgcomprovantesalrl.IIncluirMsgComprovanteSalrlPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsghistoricocomplementar.IIncluirMsgHistoricoComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsglancamentopersonalizado.IIncluirMsgLancamentoPersonalizadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.IIncluirMsgLayoutArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.IIncluirOperacaoServicoPagtoIntegradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirorgaopagador.IIncluirOrgaoPagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirpagamentosalario.IIncluirPagamentoSalarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparametrocontrato.IIncluirParametroContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantecontratopgit.IIncluirParticipanteContratoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantesorgaopublico.IIncluirParticipantesOrgaoPublicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirperfiltrocaarquivo.IIncluirPerfilTrocaArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.IIncluirPrioridadeTipoCompromissoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.IIncluirProcessoMassivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirregrasegregacaoacesso.IIncluirRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirrelaccontacontrato.IIncluirRelacContaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.IIncluirRepresentantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicocomposto.IIncluirServicoCompostoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicolancamentocnab.IIncluirServicoLancamentoCnabPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirservicorelacionado.IIncluirServicoRelacionadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasefavorecido.IIncluirSolBaseFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.IIncluirSolBasePagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.IIncluirSolBaseRetransmissaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasesistema.IIncluirSolBaseSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtocliepagador.IIncluirSolEmiAviMovtoCliePagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemiavimovtofavorecido.IIncluirSolEmiAviMovtoFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantecliepagador.IIncluirSolEmiComprovanteCliePagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.IIncluirSolEmiComprovanteFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicantposdtpgto.IIncluirSolicAntPosDtPgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicantprocagda.IIncluirSolicAntProcAgdaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsoliccancelamentolotepgto.IIncluirSolicCancelamentoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicexclusaolotepgto.IIncluirSolicExclusaoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.IIncluirSolicitacaoRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.IIncluirSolicitacaoRelatorioFavorecidosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitcriacaocontatipo.IIncluirSolicitCriacaoContaTipoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicliberacaolotepgto.IIncluirSolicLiberacaoLotePgtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicrecuperacao.IIncluirSolicRecuperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioempresaacompanhada.IIncluirSoliRelatorioEmpresaAcompanhadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioorgaospublicos.IIncluirSoliRelatorioOrgaosPublicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.IIncluirSoliRelatorioPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamentovalidacao.IIncluirSoliRelatorioPagamentoValidacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirterceiropartcontrato.IIncluirTerceiroPartContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtipocomprovante.IIncluirTipoComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.IIncluirTipoPendenciaResponsaveisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.IIncluirTipoServModContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinccontratomaecontratofilho.IIncluirVincContratoMaeContratoFilhoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.IIncluirVincConvnCtaSalarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsghistcomplementaroperacao.IIncluirVincMsgHistComplementarOperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvincmsglancoper.IIncluirVincMsgLancOperPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinctrocaarqparticipante.IIncluirVincTrocaArqParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculacaomsglayoutsistema.IIncluirVinculacaoMsgLayoutSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.IIncluirVinculoContaSalarioDestinoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculoemprpagcontsal.IIncluirVinculoEmprPagContSalPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.inlcuirtiporetornolayout.IInlcuirTipoRetornoLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarintegral.ILiberarIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtoindividualpendencia.ILiberarPagtoIndividualPendenciaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconintegral.ILiberarPagtoPendConIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendconparcial.ILiberarPagtoPendConParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtopendindividual.ILiberarPagtoPendIndividualPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtosindsemconsultasaldo.ILiberarPagtosIndSemConsultaSaldoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarparcial.ILiberarParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconintegral.ILiberarPendenciaPagtoConIntegralPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpendenciapagtoconparcial.ILiberarPendenciaPagtoConParcialPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listacontratosvinculadosperfil.IListaContratosVinculadosPerfilPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaragregado.IListarAgregadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaralcada.IListarAlcadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarambiente.IListarAmbientePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaraplicativoformatacaoarquivo.IListarAplicativoFormatacaoArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaraplictvformttipolayoutarqv.IListarAplictvFormtTipoLayoutArqvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listararqretransmissao.IListarArqRetransmissaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listararquivoretorno.IListarArquivoRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarqservico.IListarAssLoteLayoutArqServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarasslotelayoutarquivo.IListarAssLoteLayoutArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.IListarAssLoteOperacaoServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.IListarAssociacaoContratoMsgPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.IListarAssocTrocaArqParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarassretornolayoutservico.IListarAssRetornoLayoutServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarassversaolayoutloteservico.IListarAssVersaoLayoutLoteServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.IListarCentroCustoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarclasseramoatvdd.IListarClasseRamoAtvddPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.IListarClientesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.IListarClientesMarqPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarclubesclientes.IListarClubesClientesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcnpjficticio.IListarCnpjFicticioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcomboformaliquidacao.IListarComboFormaLiquidacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.IListarComprovanteSalarialIntranetPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanambpart.IListarConManAmbPartPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmancontavinculada.IListarConManContaVinculadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmandadosbasicos.IListarConManDadosBasicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanlayout.IListarConManLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanparticipante.IListarConManParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmanservico.IListarConManServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconmantarifa.IListarConManTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.IListarContaComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontadebtarifa.IListarContaDebTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasexclusao.IListarContasExclusaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontaspossiveisinclusao.IListarContasPossiveisInclusaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.IListarContasRelacionadasContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.IListarContasRelacionadasHistPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasparacontrato.IListarContasRelacionadasParaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontrato.IListarContasVinculadasContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontasvinculadascontratoloop.IListarContasVinculadasContratoLoopPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratomae.IListarContratoMaePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratomarq.IListarContratoMarqPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratosclientemarq.IListarContratosClienteMarqPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratospgit.IListarContratosPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontratosrenegociacao.IListarContratosRenegociacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.IListarConvenioContaSalarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.IListarConvenioContaSalarioNovoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarctasvincpartorgaopublico.IListarCtasVincPartOrgaoPublicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvn.IListarDadosCtaConvnPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listardadosctaconvncontingencia.IListarDadosCtaConvnContingenciaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listardependdiretoria.IListarDependDiretoriaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.IListarDependEmpresaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarduplicacaoarqretorno.IListarDuplicacaoArqRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaregra.IListaRegraPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaremissaoautavisoscomprv.IListarEmissaoAutAvisosComprvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarempresaconglomerado.IListarEmpresaConglomeradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarempresagestora.IListarEmpresaGestoraPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarempresatransmissaoarquivovan.IListarEmpresaTransmissaoArquivoVanPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarenderecoemail.IListarEnderecoEmailPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarenderecoparticipante.IListarEnderecoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.IListarEstatisticaProcessoMassivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.IListarExcecaoRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.IListarFaixaSalarialPropostaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.IListarFavorecidosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeconta.IListarFinalidadeContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeendereco.IListarFinalidadeEnderecoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.IListarFinalidadeEnderecoOperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarformalanccnab.IListarFormaLancCnabPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarformaliquidacao.IListarFormaLiquidacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistassoctrocaarqparticipante.IListarHistAssocTrocaArqParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistemissaoautavisoscomprv.IListarHistEmissaoAutAvisosComprvPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistliquidacaopagamento.IListarHistLiquidacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistmsghistoricocomplementar.IListarHistMsgHistoricoComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricobloqueio.IListarHistoricoBloqueioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricodebtarifa.IListarHistoricoDebTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoenderecoparticipante.IListarHistoricoEnderecoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricolayoutcontrato.IListarHistoricoLayoutContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.IListarHistoricoOperacaoServicoPagtoIntegradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.IListarHistoricoOrgaoPagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.IListarHistVinculoContaSalarioDestinoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarhorariolimite.IListarHorarioLimitePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaridioma.IListarIdiomaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarincluirsolemicomprovante.IListarIncluirSolEmiComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarindiceeconomico.IListarIndiceEconomicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.IListarInscricaoFavorecidosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarlayoutarqservico.IListarLayoutArqServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarlayoutscontrato.IListarLayoutsContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.IListarLinhaMsgComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.IListarLiquidacaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissao.IListarMeioTransmissaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmeiotransmissaopagamento.IListarMeioTransmissaoPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadecontrato.IListarModalidadeContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadeequiparacaocontrato.IListarModalidadeEquiparacaoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadelistadebito.IListarModalidadeListaDebitoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidades.IListarModalidadesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadesestorno.IListarModalidadesEstornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadetiposervico.IListarModalidadeTipoServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.IListarMoedaEconomicaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedasarquivo.IListarMoedasArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmoedassistema.IListarMoedasSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiodesbloqueio.IListarMotivoBloqueioDesbloqueioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.IListarMotivoBloqueioFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.IListarMotivoSituacaoAditivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaocontrato.IListarMotivoSituacaoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.IListarMotivoSituacaoPendContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoservcontrato.IListarMotivoSituacaoServContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaovinccontrato.IListarMotivoSituacaoVincContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.IListarMsgAlertaGestorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.IListarMsgComprovanteSalrlPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.IListarMsgHistoricoComplementarPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarmunicipio.IListarMunicipioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.IListarNumeroVersaoLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.IListarOperacaoContaDebTarifaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.IListarOperacaoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.IListarOperacaoServicoPagtoIntegradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacoesservico.IListarOperacoesServicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarorgaopagador.IListarOrgaoPagadorPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.IListarParametroContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparametros.IListarParametrosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipante.IListarParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipanteagregado.IListarParticipanteAgregadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.IListarParticipantesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.IListarParticipantesContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.IListarParticipantesIntranetPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.IListarParticipantesLoopPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesorgaopublico.IListarParticipantesOrgaoPublicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarperfiltrocaarquivo.IListarPerfilTrocaArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarperiodicidade.IListarPeriodicidadePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarpessoajuridicacontratomarq.IListarPessoaJuridicaContratoMarqPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarpostergarconsolidadosolicitacao.IListarPostergarConsolidadoSolicitacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.IListarPrioridadeTipoCompromissoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.IListarProcessoMassivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarrecursocanalmsg.IListarRecursoCanalMsgPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.IListarRegraSegregacaoAcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.IListarRelatorioContratosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarremessa.IListarRemessaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarrepresentante.IListarRepresentantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarresponscontrato.IListarResponsContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarresultadoprocessamentoremessa.IListarResultadoProcessamentoRemessaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.IListarSegmentoClientesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicocnab.IListarServicoCnabPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.IListarServicoCompostoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicoequiparacaocontrato.IListarServicoEquiparacaoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicomodalidadeemissaoaviso.IListarServicoModalidadeEmissaoAvisoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionado.IListarServicoRelacionadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdps.IListarServicoRelacionadoCdpsPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.IListarServicoRelacionadoCdpsIncluirPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadopgit.IListarServicoRelacionadoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadopgitcdps.IListarServicoRelacionadoPgitCdpsPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicos.IListarServicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicoscontrato.IListarServicosContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.IListarServicosEmissaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoaditivocontrato.IListarSituacaoAditivoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.IListarSituacaoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaopendcontrato.IListarSituacaoPendContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoremessa.IListarSituacaoRemessaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoretorno.IListarSituacaoRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaoservicocontrato.IListarSituacaoServicoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaosolicitacao.IListarSituacaoSolicitacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsituacaovinccontrato.IListarSituacaoVincContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasefavorecido.IListarSolBaseFavorecidoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasepagto.IListarSolBasePagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbaseretransmissao.IListarSolBaseRetransmissaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasesistema.IListarSolBaseSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolicitacaorelatoriofavorecidos.IListarSolicitacaoRelatorioFavorecidosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolicitcriacaocontatipo.IListarSolicitCriacaoContaTipoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.IListarSoliRelatorioEmpresaAcompanhadaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.IListarSoliRelatorioOrgaosPublicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.IListarSoliRelatorioPagamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarsramoatvddeconc.IListarSramoAtvddEconcPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.IListarTarifasContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.IListarTercParticipanteContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoacao.IListarTipoAcaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.IListarTipoArquivoRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipocomprovante.IListarTipoComprovantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoconta.IListarTipoContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipocontrato.IListarTipoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipocontratomarq.IListarTipoContratoMarqPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipofavorecidos.IListarTipoFavorecidosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayout.IListarTipoLayoutPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutarquivo.IListarTipoLayoutArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolayoutcontrato.IListarTipoLayoutContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipolote.IListarTipoLotePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipopendencia.IListarTipoPendenciaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipopendenciaresponsaveis.IListarTipoPendenciaResponsaveisPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoprocessamentolayoutarquivo.IListarTipoProcessamentoLayoutArquivoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipoprocesso.IListarTipoProcessoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.IListarTipoRelacContaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartiporespons.IListarTipoResponsPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.IListarTipoRetiradaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartiposervicomodalidade.IListarTipoServicoModalidadePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartiposistema.IListarTipoSistemaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartiposolicitacao.IListarTipoSolicitacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipospagto.IListarTiposPagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.IListarTipoUnidadeOrganizacionalPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.IListarTipoVinculoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimadatapagto.IListarUltimaDataPagtoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimonumeroremessa.IListarUltimoNumeroRemessaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimonumeroretorno.IListarUltimoNumeroRetornoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.IListarUnidadeFederativaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.IListarVincConvnCtaSalarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincmsghistcomplementaroperacao.IListarVincMsgHistComplementarOperacaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarvincmsglancoper.IListarVincMsgLancOperPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.IListarVinculoContaSalarioDestinoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.IListarVinculoCtaSalarioEmpresaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculohistctasalarioempresa.IListarVinculoHistCtaSalarioEmpresaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listatiposervicocontrato.IListaTipoServicoContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.IMontarComboProdutoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.recuperarcontrato.IRecuperarContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.reenviaremail.IReenviarEmailPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.registrarassinaturacontrato.IRegistrarAssinaturaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.IRegistrarFormalizacaoManContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.IRegistrarPendenciaContratoPgitPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.rejeitarsolicitacaoconsistprevia.IRejeitarSolicitacaoConsistPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.reprocessarsolicitacaoconsistprevia.IReprocessarSolicitacaoConsistPreviaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.revertcontratomigrado.IRevertContratoMigradoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosbackupconsulta.ISolicitarRecPagtosBackupConsultaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosvencidosnaopagos.ISolicitarRecPagtosVencidosNaoPagosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.ISolicitarRelatorioContratosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.substituirrelaccontacontrato.ISubstituirRelacContaContratoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.substituirvincenderecoparticipante.ISubstituirVincEnderecoParticipantePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.substituirvinculoctasalarioempresa.ISubstituirVinculoCtaSalarioEmpresaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validaragencia.IValidarAgenciaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.IValidarCnpjFicticioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarcontratopagsalpartorgaopublico.IValidarContratoPagSalPartOrgaoPublicoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.IValidarInclusaoBloqueioRastreamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarparametrotela.IValidarParametroTelaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarpropostaandamento.IValidarPropostaAndamentoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarqtddiascobrancaapsapuracao.IValidarQtdDiasCobrancaApsApuracaoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarservicocontratado.IValidarServicoContratadoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarusuario.IValidarUsuarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.validarvinculacaoconveniocontasalario.IValidarVinculacaoConvenioContaSalarioPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.IVerificaraAtributoSoltcPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.verificaratributosdadosbasicos.IVerificarAtributosDadosBasicosPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.IVerificarAtributosServicoModalidadePDCAdapter;

/**
 * 
 * <p>
 * <b>T�tulo:</b> Arquitetura Bradesco Canal Internet.
 * </p>
 * <p>
 * <b>Descri��o:</b>
 * </p>
 * <p>
 * F�brica para criar os adaptadores definidos no sistema.
 * </p>
 * 
 * @author GFT Iberia Solutions / Emagine <BR/> copyright Copyright (c) 2006
 *         <BR/> created <BR/>
 * @version 1.0
 *
 * Esta classe foi automaticamente gerada com 
 * <a href="http://www.bradesco.com.br">Generador de Adaptadores</a>
 */
 
public class FactoryAdapter {

    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AuthenticationService.
     */
    private IAuthenticationServicePDCAdapter pdcAuthenticationService;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloqueioDesbloqueioProcessoMassivo.
     */
    private IBloqueioDesbloqueioProcessoMassivoPDCAdapter pdcBloqueioDesbloqueioProcessoMassivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarMotivoBloqueioConta.
     */
    private IConsultarMotivoBloqueioContaPDCAdapter pdcConsultarMotivoBloqueioConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMotivoBloqueioFavorecido.
     */
    private IListarMotivoBloqueioFavorecidoPDCAdapter pdcListarMotivoBloqueioFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicitacaoRelatorioFavorecidos.
     */
    private IExcluirSolicitacaoRelatorioFavorecidosPDCAdapter pdcExcluirSolicitacaoRelatorioFavorecidos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSolicitacaoRelatorioFavorecidos.
     */
    private IListarSolicitacaoRelatorioFavorecidosPDCAdapter pdcListarSolicitacaoRelatorioFavorecidos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaSolicitacaoRastreamento.
     */
    private IConsultarListaSolicitacaoRastreamentoPDCAdapter pdcConsultarListaSolicitacaoRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicitacaoRastreamento.
     */
    private IExcluirSolicitacaoRastreamentoPDCAdapter pdcExcluirSolicitacaoRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloquearFavorecido.
     */
    private IBloquearFavorecidoPDCAdapter pdcBloquearFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarDependEmpresa.
     */
    private IListarDependEmpresaPDCAdapter pdcListarDependEmpresa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarEmpresaGestora.
     */
    private IListarEmpresaGestoraPDCAdapter pdcListarEmpresaGestora;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarFinalidadeConta.
     */
    private IListarFinalidadeContaPDCAdapter pdcListarFinalidadeConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarFormaLancCnab.
     */
    private IListarFormaLancCnabPDCAdapter pdcListarFormaLancCnab;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarFormaLiquidacao.
     */
    private IListarFormaLiquidacaoPDCAdapter pdcListarFormaLiquidacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarIdioma.
     */
    private IListarIdiomaPDCAdapter pdcListarIdioma;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarOperacaoContrato.
     */
    private IListarOperacaoContratoPDCAdapter pdcListarOperacaoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarPeriodicidade.
     */
    private IListarPeriodicidadePDCAdapter pdcListarPeriodicidade;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarResponsContrato.
     */
    private IListarResponsContratoPDCAdapter pdcListarResponsContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoCnab.
     */
    private IListarServicoCnabPDCAdapter pdcListarServicoCnab;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicosContrato.
     */
    private IListarServicosContratoPDCAdapter pdcListarServicosContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSituacaoContrato.
     */
    private IListarSituacaoContratoPDCAdapter pdcListarSituacaoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSituacaoPendContrato.
     */
    private IListarSituacaoPendContratoPDCAdapter pdcListarSituacaoPendContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSituacaoServicoContrato.
     */
    private IListarSituacaoServicoContratoPDCAdapter pdcListarSituacaoServicoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSituacaoSolicitacao.
     */
    private IListarSituacaoSolicitacaoPDCAdapter pdcListarSituacaoSolicitacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSituacaoVincContrato.
     */
    private IListarSituacaoVincContratoPDCAdapter pdcListarSituacaoVincContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoContrato.
     */
    private IListarTipoContratoPDCAdapter pdcListarTipoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoLote.
     */
    private IListarTipoLotePDCAdapter pdcListarTipoLote;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoPendencia.
     */
    private IListarTipoPendenciaPDCAdapter pdcListarTipoPendencia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoRelacConta.
     */
    private IListarTipoRelacContaPDCAdapter pdcListarTipoRelacConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoRespons.
     */
    private IListarTipoResponsPDCAdapter pdcListarTipoRespons;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoSolicitacao.
     */
    private IListarTipoSolicitacaoPDCAdapter pdcListarTipoSolicitacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoVinculo.
     */
    private IListarTipoVinculoPDCAdapter pdcListarTipoVinculo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListaRegra.
     */
    private IListaRegraPDCAdapter pdcListaRegra;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador MontarComboProduto.
     */
    private IMontarComboProdutoPDCAdapter pdcMontarComboProduto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarRemessa.
     */
    private IListarRemessaPDCAdapter pdcListarRemessa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarInconsistencia.
     */
    private IConsultarInconsistenciaPDCAdapter pdcConsultarInconsistencia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLote.
     */
    private IConsultarLotePDCAdapter pdcConsultarLote;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarOcorrencias.
     */
    private IConsultarOcorrenciasPDCAdapter pdcConsultarOcorrencias;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarClientes.
     */
    private IListarClientesPDCAdapter pdcListarClientes;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarOcorrenciaRetorno.
     */
    private IConsultarOcorrenciaRetornoPDCAdapter pdcConsultarOcorrenciaRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLoteRetorno.
     */
    private IConsultarLoteRetornoPDCAdapter pdcConsultarLoteRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarInconsistenciaRetorno.
     */
    private IConsultarInconsistenciaRetornoPDCAdapter pdcConsultarInconsistenciaRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarRetorno.
     */
    private IConsultarRetornoPDCAdapter pdcConsultarRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarEstatisticasLote.
     */
    private IConsultarEstatisticasLotePDCAdapter pdcConsultarEstatisticasLote;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAssLoteLayoutArquivo.
     */
    private IListarAssLoteLayoutArquivoPDCAdapter pdcListarAssLoteLayoutArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharAssLoteLayoutArquivo.
     */
    private IDetalharAssLoteLayoutArquivoPDCAdapter pdcDetalharAssLoteLayoutArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAssLoteLayoutArquivo.
     */
    private IExcluirAssLoteLayoutArquivoPDCAdapter pdcExcluirAssLoteLayoutArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAssLoteLayoutArquivo.
     */
    private IIncluirAssLoteLayoutArquivoPDCAdapter pdcIncluirAssLoteLayoutArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharMoedasSistema.
     */
    private IDetalharMoedasSistemaPDCAdapter pdcDetalharMoedasSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirMoedasSistema.
     */
    private IExcluirMoedasSistemaPDCAdapter pdcExcluirMoedasSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirMoedasSistema.
     */
    private IIncluirMoedasSistemaPDCAdapter pdcIncluirMoedasSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMoedasSistema.
     */
    private IListarMoedasSistemaPDCAdapter pdcListarMoedasSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoUnidadeOrganizacional.
     */
    private IListarTipoUnidadeOrganizacionalPDCAdapter pdcListarTipoUnidadeOrganizacional;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirOrgaoPagador.
     */
    private IIncluirOrgaoPagadorPDCAdapter pdcIncluirOrgaoPagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarOrgaoPagador.
     */
    private IAlterarOrgaoPagadorPDCAdapter pdcAlterarOrgaoPagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharOrgaoPagador.
     */
    private IDetalharOrgaoPagadorPDCAdapter pdcDetalharOrgaoPagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirOrgaoPagador.
     */
    private IExcluirOrgaoPagadorPDCAdapter pdcExcluirOrgaoPagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloquearOrgaoPagador.
     */
    private IBloquearOrgaoPagadorPDCAdapter pdcBloquearOrgaoPagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistoricoOrgaoPagador.
     */
    private IDetalharHistoricoOrgaoPagadorPDCAdapter pdcDetalharHistoricoOrgaoPagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistoricoOrgaoPagador.
     */
    private IListarHistoricoOrgaoPagadorPDCAdapter pdcListarHistoricoOrgaoPagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMoedaEconomica.
     */
    private IListarMoedaEconomicaPDCAdapter pdcListarMoedaEconomica;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarFavorecidos.
     */
    private IListarFavorecidosPDCAdapter pdcListarFavorecidos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarInscricaoFavorecidos.
     */
    private IListarInscricaoFavorecidosPDCAdapter pdcListarInscricaoFavorecidos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoFavorecidos.
     */
    private IListarTipoFavorecidosPDCAdapter pdcListarTipoFavorecidos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirMoedasArquivo.
     */
    private IExcluirMoedasArquivoPDCAdapter pdcExcluirMoedasArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirMoedasArquivo.
     */
    private IIncluirMoedasArquivoPDCAdapter pdcIncluirMoedasArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMoedasArquivo.
     */
    private IListarMoedasArquivoPDCAdapter pdcListarMoedasArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAssLoteLayoutArqServico.
     */
    private IExcluirAssLoteLayoutArqServicoPDCAdapter pdcExcluirAssLoteLayoutArqServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAssLoteLayoutArqServico.
     */
    private IIncluirAssLoteLayoutArqServicoPDCAdapter pdcIncluirAssLoteLayoutArqServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAssLoteLayoutArqServico.
     */
    private IListarAssLoteLayoutArqServicoPDCAdapter pdcListarAssLoteLayoutArqServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarOrgaoPagador.
     */
    private IListarOrgaoPagadorPDCAdapter pdcListarOrgaoPagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarCentroCusto.
     */
    private IListarCentroCustoPDCAdapter pdcListarCentroCusto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirProcessoMassivo.
     */
    private IIncluirProcessoMassivoPDCAdapter pdcIncluirProcessoMassivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharProcessoMassivo.
     */
    private IDetalharProcessoMassivoPDCAdapter pdcDetalharProcessoMassivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirVinculoContaSalarioDestino.
     */
    private IExcluirVinculoContaSalarioDestinoPDCAdapter pdcExcluirVinculoContaSalarioDestino;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarProcessoMassivo.
     */
    private IAlterarProcessoMassivoPDCAdapter pdcAlterarProcessoMassivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirProcessoMassivo.
     */
    private IExcluirProcessoMassivoPDCAdapter pdcExcluirProcessoMassivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistoricoFavorecido.
     */
    private IDetalharHistoricoFavorecidoPDCAdapter pdcDetalharHistoricoFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloquearContaFavorecido.
     */
    private IBloquearContaFavorecidoPDCAdapter pdcBloquearContaFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalheContaFavorecido.
     */
    private IConsultarDetalheContaFavorecidoPDCAdapter pdcConsultarDetalheContaFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalheFavorecido.
     */
    private IConsultarDetalheFavorecidoPDCAdapter pdcConsultarDetalheFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalheHistoricoContaFavorecido.
     */
    private IConsultarDetalheHistoricoContaFavorecidoPDCAdapter pdcConsultarDetalheHistoricoContaFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharMoedasArquivo.
     */
    private IDetalharMoedasArquivoPDCAdapter pdcDetalharMoedasArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarAlcada.
     */
    private IAlterarAlcadaPDCAdapter pdcAlterarAlcada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAlcada.
     */
    private IExcluirAlcadaPDCAdapter pdcExcluirAlcada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAlcada.
     */
    private IIncluirAlcadaPDCAdapter pdcIncluirAlcada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoAcao.
     */
    private IListarTipoAcaoPDCAdapter pdcListarTipoAcao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSegmentoClientes.
     */
    private IListarSegmentoClientesPDCAdapter pdcListarSegmentoClientes;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador CancelarRelatoriosContratos.
     */
    private ICancelarRelatoriosContratosPDCAdapter pdcCancelarRelatoriosContratos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarEmpresaConglomerado.
     */
    private IListarEmpresaConglomeradoPDCAdapter pdcListarEmpresaConglomerado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarDependDiretoria.
     */
    private IListarDependDiretoriaPDCAdapter pdcListarDependDiretoria;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarOperacoesServico.
     */
    private IListarOperacoesServicoPDCAdapter pdcListarOperacoesServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharAssLoteLayoutArqServico.
     */
    private IDetalharAssLoteLayoutArqServicoPDCAdapter pdcDetalharAssLoteLayoutArqServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarProcessoMassivo.
     */
    private IListarProcessoMassivoPDCAdapter pdcListarProcessoMassivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharAssRetornoLayoutServico.
     */
    private IDetalharAssRetornoLayoutServicoPDCAdapter pdcDetalharAssRetornoLayoutServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAssRetornoLayoutServico.
     */
    private IExcluirAssRetornoLayoutServicoPDCAdapter pdcExcluirAssRetornoLayoutServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAssRetornoLayoutServico.
     */
    private IIncluirAssRetornoLayoutServicoPDCAdapter pdcIncluirAssRetornoLayoutServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAssRetornoLayoutServico.
     */
    private IListarAssRetornoLayoutServicoPDCAdapter pdcListarAssRetornoLayoutServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaContaFavorecido.
     */
    private IConsultarListaContaFavorecidoPDCAdapter pdcConsultarListaContaFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarFinalidadeEndereco.
     */
    private IListarFinalidadeEnderecoPDCAdapter pdcListarFinalidadeEndereco;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharAssLoteOperacaoServico.
     */
    private IDetalharAssLoteOperacaoServicoPDCAdapter pdcDetalharAssLoteOperacaoServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAssLoteOperacaoServico.
     */
    private IExcluirAssLoteOperacaoServicoPDCAdapter pdcExcluirAssLoteOperacaoServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAssLoteOperacaoServico.
     */
    private IListarAssLoteOperacaoServicoPDCAdapter pdcListarAssLoteOperacaoServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarEstatisticaProcessoMassivo.
     */
    private IListarEstatisticaProcessoMassivoPDCAdapter pdcListarEstatisticaProcessoMassivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoConta.
     */
    private IListarTipoContaPDCAdapter pdcListarTipoConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharAlcada.
     */
    private IDetalharAlcadaPDCAdapter pdcDetalharAlcada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarMsgComprovanteSalrl.
     */
    private IAlterarMsgComprovanteSalrlPDCAdapter pdcAlterarMsgComprovanteSalrl;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirMsgComprovanteSalrl.
     */
    private IExcluirMsgComprovanteSalrlPDCAdapter pdcExcluirMsgComprovanteSalrl;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirMsgComprovanteSalrl.
     */
    private IIncluirMsgComprovanteSalrlPDCAdapter pdcIncluirMsgComprovanteSalrl;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetHistContaSalarioDestino.
     */
    private IConsultarDetHistContaSalarioDestinoPDCAdapter pdcConsultarDetHistContaSalarioDestino;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirLinhaMsgComprovante.
     */
    private IIncluirLinhaMsgComprovantePDCAdapter pdcIncluirLinhaMsgComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarLinhaMsgComprovante.
     */
    private IAlterarLinhaMsgComprovantePDCAdapter pdcAlterarLinhaMsgComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirLinhaMsgComprovante.
     */
    private IExcluirLinhaMsgComprovantePDCAdapter pdcExcluirLinhaMsgComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoComprovante.
     */
    private IListarTipoComprovantePDCAdapter pdcListarTipoComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirTipoComprovante.
     */
    private IIncluirTipoComprovantePDCAdapter pdcIncluirTipoComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirTipoComprovante.
     */
    private IExcluirTipoComprovantePDCAdapter pdcExcluirTipoComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarTipoComprovante.
     */
    private IAlterarTipoComprovantePDCAdapter pdcAlterarTipoComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharTipoComprovante.
     */
    private IDetalharTipoComprovantePDCAdapter pdcDetalharTipoComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarRegraSegregacaoAcesso.
     */
    private IAlterarRegraSegregacaoAcessoPDCAdapter pdcAlterarRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharRegraSegregacaoAcesso.
     */
    private IDetalharRegraSegregacaoAcessoPDCAdapter pdcDetalharRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirRegraSegregacaoAcesso.
     */
    private IExcluirRegraSegregacaoAcessoPDCAdapter pdcExcluirRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirRegraSegregacaoAcesso.
     */
    private IIncluirRegraSegregacaoAcessoPDCAdapter pdcIncluirRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarRegraSegregacaoAcesso.
     */
    private IListarRegraSegregacaoAcessoPDCAdapter pdcListarRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMsgAlertaGestor.
     */
    private IListarMsgAlertaGestorPDCAdapter pdcListarMsgAlertaGestor;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaFuncionarioBradesco.
     */
    private IConsultarListaFuncionarioBradescoPDCAdapter pdcConsultarListaFuncionarioBradesco;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarRecursoCanalMsg.
     */
    private IListarRecursoCanalMsgPDCAdapter pdcListarRecursoCanalMsg;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAssLoteOperacaoServico.
     */
    private IIncluirAssLoteOperacaoServicoPDCAdapter pdcIncluirAssLoteOperacaoServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalheMsgComprovanteSalrl.
     */
    private IConsultarDetalheMsgComprovanteSalrlPDCAdapter pdcConsultarDetalheMsgComprovanteSalrl;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAssociacaoContratoMsg.
     */
    private IListarAssociacaoContratoMsgPDCAdapter pdcListarAssociacaoContratoMsg;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAssociacaoContratoMsg.
     */
    private IIncluirAssociacaoContratoMsgPDCAdapter pdcIncluirAssociacaoContratoMsg;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAssociacaoContratoMsg.
     */
    private IExcluirAssociacaoContratoMsgPDCAdapter pdcExcluirAssociacaoContratoMsg;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalheContratoMsg.
     */
    private IConsultarDetalheContratoMsgPDCAdapter pdcConsultarDetalheContratoMsg;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDescricaoUnidadeOrganizacional.
     */
    private IConsultarDescricaoUnidadeOrganizacionalPDCAdapter pdcConsultarDescricaoUnidadeOrganizacional;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarMsgLancamentoPersonalizado.
     */
    private IConsultarMsgLancamentoPersonalizadoPDCAdapter pdcConsultarMsgLancamentoPersonalizado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarMsgLancamentoPersonalizado.
     */
    private IAlterarMsgLancamentoPersonalizadoPDCAdapter pdcAlterarMsgLancamentoPersonalizado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharMsgLancamentoPersonalizado.
     */
    private IDetalharMsgLancamentoPersonalizadoPDCAdapter pdcDetalharMsgLancamentoPersonalizado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirMsgLancamentoPersonalizado.
     */
    private IExcluirMsgLancamentoPersonalizadoPDCAdapter pdcExcluirMsgLancamentoPersonalizado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirMsgLancamentoPersonalizado.
     */
    private IIncluirMsgLancamentoPersonalizadoPDCAdapter pdcIncluirMsgLancamentoPersonalizado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistManutMsgLancPersonalizado.
     */
    private IDetalharHistManutMsgLancPersonalizadoPDCAdapter pdcDetalharHistManutMsgLancPersonalizado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarHistManutMsgLancPersonalizado.
     */
    private IConsultarHistManutMsgLancPersonalizadoPDCAdapter pdcConsultarHistManutMsgLancPersonalizado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarVinculoCtaSalarioEmpresa.
     */
    private IListarVinculoCtaSalarioEmpresaPDCAdapter pdcListarVinculoCtaSalarioEmpresa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirVinculoContaSalarioDestino.
     */
    private IIncluirVinculoContaSalarioDestinoPDCAdapter pdcIncluirVinculoContaSalarioDestino;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarExcecaoRegraSegregacaoAcesso.
     */
    private IListarExcecaoRegraSegregacaoAcessoPDCAdapter pdcListarExcecaoRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharExcecaoRegraSegregacaoAcesso.
     */
    private IDetalharExcecaoRegraSegregacaoAcessoPDCAdapter pdcDetalharExcecaoRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarLinhaMsgComprovante.
     */
    private IListarLinhaMsgComprovantePDCAdapter pdcListarLinhaMsgComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalheLinhaMensagem.
     */
    private IConsultarDetalheLinhaMensagemPDCAdapter pdcConsultarDetalheLinhaMensagem;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirExcecaoRegraSegregacaoAcesso.
     */
    private IIncluirExcecaoRegraSegregacaoAcessoPDCAdapter pdcIncluirExcecaoRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarExcecaoRegraSegregacaoAcesso.
     */
    private IAlterarExcecaoRegraSegregacaoAcessoPDCAdapter pdcAlterarExcecaoRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirExcecaoRegraSegregacaoAcesso.
     */
    private IExcluirExcecaoRegraSegregacaoAcessoPDCAdapter pdcExcluirExcecaoRegraSegregacaoAcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirMsgAlertaGestor.
     */
    private IIncluirMsgAlertaGestorPDCAdapter pdcIncluirMsgAlertaGestor;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetVinculoCtaSalarioEmpresa.
     */
    private IConsultarDetVinculoCtaSalarioEmpresaPDCAdapter pdcConsultarDetVinculoCtaSalarioEmpresa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalheVinculoContaSalarioDestino.
     */
    private IConsultarDetalheVinculoContaSalarioDestinoPDCAdapter pdcConsultarDetalheVinculoContaSalarioDestino;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador SubstituirVinculoCtaSalarioEmpresa.
     */
    private ISubstituirVinculoCtaSalarioEmpresaPDCAdapter pdcSubstituirVinculoCtaSalarioEmpresa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirPrioridadeTipoCompromisso.
     */
    private IExcluirPrioridadeTipoCompromissoPDCAdapter pdcExcluirPrioridadeTipoCompromisso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarMsgAlertaGestor.
     */
    private IAlterarMsgAlertaGestorPDCAdapter pdcAlterarMsgAlertaGestor;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirMsgAlertaGestor.
     */
    private IExcluirMsgAlertaGestorPDCAdapter pdcExcluirMsgAlertaGestor;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarVincMsgHistComplementarOperacao.
     */
    private IListarVincMsgHistComplementarOperacaoPDCAdapter pdcListarVincMsgHistComplementarOperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirVincMsgHistComplementarOperacao.
     */
    private IIncluirVincMsgHistComplementarOperacaoPDCAdapter pdcIncluirVincMsgHistComplementarOperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirVincMsgHistComplementarOperacao.
     */
    private IExcluirVincMsgHistComplementarOperacaoPDCAdapter pdcExcluirVincMsgHistComplementarOperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharVincMsgHistComplementarOperacao.
     */
    private IDetalharVincMsgHistComplementarOperacaoPDCAdapter pdcDetalharVincMsgHistComplementarOperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMsgHistoricoComplementar.
     */
    private IListarMsgHistoricoComplementarPDCAdapter pdcListarMsgHistoricoComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirVincMsgLancOper.
     */
    private IExcluirVincMsgLancOperPDCAdapter pdcExcluirVincMsgLancOper;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharMsgAlertaGestor.
     */
    private IDetalharMsgAlertaGestorPDCAdapter pdcDetalharMsgAlertaGestor;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarMsgHistoricoComplementar.
     */
    private IAlterarMsgHistoricoComplementarPDCAdapter pdcAlterarMsgHistoricoComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharMsgHistoricoComplementar.
     */
    private IDetalharMsgHistoricoComplementarPDCAdapter pdcDetalharMsgHistoricoComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirMsgHistoricoComplementar.
     */
    private IExcluirMsgHistoricoComplementarPDCAdapter pdcExcluirMsgHistoricoComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirMsgHistoricoComplementar.
     */
    private IIncluirMsgHistoricoComplementarPDCAdapter pdcIncluirMsgHistoricoComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAlcada.
     */
    private IListarAlcadaPDCAdapter pdcListarAlcada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarVinculoContaSalarioDestino.
     */
    private IAlterarVinculoContaSalarioDestinoPDCAdapter pdcAlterarVinculoContaSalarioDestino;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaContratosManutencao.
     */
    private IConsultarListaContratosManutencaoPDCAdapter pdcConsultarListaContratosManutencao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistMsgHistoricoComplementar.
     */
    private IListarHistMsgHistoricoComplementarPDCAdapter pdcListarHistMsgHistoricoComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistManutMsgHistComplementar.
     */
    private IDetalharHistManutMsgHistComplementarPDCAdapter pdcDetalharHistManutMsgHistComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirParticipanteContratoPgit.
     */
    private IExcluirParticipanteContratoPgitPDCAdapter pdcExcluirParticipanteContratoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicos.
     */
    private IListarServicosPDCAdapter pdcListarServicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarUnidadeFederativa.
     */
    private IListarUnidadeFederativaPDCAdapter pdcListarUnidadeFederativa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador RegistrarPendenciaContratoPgit.
     */
    private IRegistrarPendenciaContratoPgitPDCAdapter pdcRegistrarPendenciaContratoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarModalidades.
     */
    private IListarModalidadesPDCAdapter pdcListarModalidades;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMotivoSituacaoPendContrato.
     */
    private IListarMotivoSituacaoPendContratoPDCAdapter pdcListarMotivoSituacaoPendContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPendenciaContratoPgit.
     */
    private IConsultarPendenciaContratoPgitPDCAdapter pdcConsultarPendenciaContratoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirContaContrato.
     */
    private IIncluirContaContratoPDCAdapter pdcIncluirContaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador CancelarManutencaoParticipante.
     */
    private ICancelarManutencaoParticipantePDCAdapter pdcCancelarManutencaoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloquearDesbloquearContrato.
     */
    private IBloquearDesbloquearContratoPDCAdapter pdcBloquearDesbloquearContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloquearDesbloquearContaContrato.
     */
    private IBloquearDesbloquearContaContratoPDCAdapter pdcBloquearDesbloquearContaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarCondCobTarifa.
     */
    private IAlterarCondCobTarifaPDCAdapter pdcAlterarCondCobTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirVinculoCtaSalarioEmpresa.
     */
    private IExcluirVinculoCtaSalarioEmpresaPDCAdapter pdcExcluirVinculoCtaSalarioEmpresa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConManParticipante.
     */
    private IListarConManParticipantePDCAdapter pdcListarConManParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDescricaoMensagem.
     */
    private IConsultarDescricaoMensagemPDCAdapter pdcConsultarDescricaoMensagem;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMotivoSituacaoServContrato.
     */
    private IListarMotivoSituacaoServContratoPDCAdapter pdcListarMotivoSituacaoServContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarCondReajusteTarifa.
     */
    private IAlterarCondReajusteTarifaPDCAdapter pdcAlterarCondReajusteTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMotivoSituacaoContrato.
     */
    private IListarMotivoSituacaoContratoPDCAdapter pdcListarMotivoSituacaoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirTipoRetornoLayout.
     */
    private IExcluirTipoRetornoLayoutPDCAdapter pdcExcluirTipoRetornoLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMotivoSituacaoVincContrato.
     */
    private IListarMotivoSituacaoVincContratoPDCAdapter pdcListarMotivoSituacaoVincContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMotivoSituacaoAditivoContrato.
     */
    private IListarMotivoSituacaoAditivoContratoPDCAdapter pdcListarMotivoSituacaoAditivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarVincMsgLancOper.
     */
    private IListarVincMsgLancOperPDCAdapter pdcListarVincMsgLancOper;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarComboFormaLiquidacao.
     */
    private IListarComboFormaLiquidacaoPDCAdapter pdcListarComboFormaLiquidacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarAditivoContrato.
     */
    private IConsultarAditivoContratoPDCAdapter pdcConsultarAditivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AtivarAditivoContratoPendAss.
     */
    private IAtivarAditivoContratoPendAssPDCAdapter pdcAtivarAditivoContratoPendAss;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAditivoContrato.
     */
    private IExcluirAditivoContratoPDCAdapter pdcExcluirAditivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AtivarContratoPendAssinatura.
     */
    private IAtivarContratoPendAssinaturaPDCAdapter pdcAtivarContratoPendAssinatura;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador RegistrarFormalizacaoManContrato.
     */
    private IRegistrarFormalizacaoManContratoPDCAdapter pdcRegistrarFormalizacaoManContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMeioTransmissao.
     */
    private IListarMeioTransmissaoPDCAdapter pdcListarMeioTransmissao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador RegistrarAssinaturaContrato.
     */
    private IRegistrarAssinaturaContratoPDCAdapter pdcRegistrarAssinaturaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicitacaoAmbiente.
     */
    private IConsultarSolicitacaoAmbientePDCAdapter pdcConsultarSolicitacaoAmbiente;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoServicoModalidade.
     */
    private IListarTipoServicoModalidadePDCAdapter pdcListarTipoServicoModalidade;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSituacaoAditivoContrato.
     */
    private IListarSituacaoAditivoContratoPDCAdapter pdcListarSituacaoAditivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTarifasContrato.
     */
    private IListarTarifasContratoPDCAdapter pdcListarTarifasContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConManTarifas.
     */
    private IConsultarConManTarifasPDCAdapter pdcConsultarConManTarifas;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarLayoutServico.
     */
    private IAlterarLayoutServicoPDCAdapter pdcAlterarLayoutServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AprovarSolicitacaoConsistPrevia.
     */
    private IAprovarSolicitacaoConsistPreviaPDCAdapter pdcAprovarSolicitacaoConsistPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador RejeitarSolicitacaoConsistPrevia.
     */
    private IRejeitarSolicitacaoConsistPreviaPDCAdapter pdcRejeitarSolicitacaoConsistPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ReprocessarSolicitacaoConsistPrevia.
     */
    private IReprocessarSolicitacaoConsistPreviaPDCAdapter pdcReprocessarSolicitacaoConsistPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicRecuperacao.
     */
    private IConsultarSolicRecuperacaoPDCAdapter pdcConsultarSolicRecuperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicRecuperacao.
     */
    private IDetalharSolicRecuperacaoPDCAdapter pdcDetalharSolicRecuperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicRecuperacao.
     */
    private IIncluirSolicRecuperacaoPDCAdapter pdcIncluirSolicRecuperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarArquivoRecuperacao.
     */
    private IConsultarArquivoRecuperacaoPDCAdapter pdcConsultarArquivoRecuperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicitacaoConsistPrevia.
     */
    private IConsultarSolicitacaoConsistPreviaPDCAdapter pdcConsultarSolicitacaoConsistPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolEmiComprovanteFavorecido.
     */
    private IConsultarSolEmiComprovanteFavorecidoPDCAdapter pdcConsultarSolEmiComprovanteFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarClientesMarq.
     */
    private IListarClientesMarqPDCAdapter pdcListarClientesMarq;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContratosClienteMarq.
     */
    private IListarContratosClienteMarqPDCAdapter pdcListarContratosClienteMarq;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolEmiComprovanteCliePagador.
     */
    private IConsultarSolEmiComprovanteCliePagadorPDCAdapter pdcConsultarSolEmiComprovanteCliePagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarPessoaJuridicaContratoMarq.
     */
    private IListarPessoaJuridicaContratoMarqPDCAdapter pdcListarPessoaJuridicaContratoMarq;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoContratoMarq.
     */
    private IListarTipoContratoMarqPDCAdapter pdcListarTipoContratoMarq;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharListaDebitoAutDes.
     */
    private IDetalharListaDebitoAutDesPDCAdapter pdcDetalharListaDebitoAutDes;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarPerfilTrocaArquivo.
     */
    private IListarPerfilTrocaArquivoPDCAdapter pdcListarPerfilTrocaArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AutDesListaDebitoParcial.
     */
    private IAutDesListaDebitoParcialPDCAdapter pdcAutDesListaDebitoParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AutDesListaDebitoIntegral.
     */
    private IAutDesListaDebitoIntegralPDCAdapter pdcAutDesListaDebitoIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicRecuperacao.
     */
    private IExcluirSolicRecuperacaoPDCAdapter pdcExcluirSolicRecuperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContratoMarq.
     */
    private IListarContratoMarqPDCAdapter pdcListarContratoMarq;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarTipoConta.
     */
    private IConsultarTipoContaPDCAdapter pdcConsultarTipoConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DesautorizarPagtosIndividuais.
     */
    private IDesautorizarPagtosIndividuaisPDCAdapter pdcDesautorizarPagtosIndividuais;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarPagtosIndSemConsultaSaldo.
     */
    private ILiberarPagtosIndSemConsultaSaldoPDCAdapter pdcLiberarPagtosIndSemConsultaSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AutorizarPagtosIndividuais.
     */
    private IAutorizarPagtosIndividuaisPDCAdapter pdcAutorizarPagtosIndividuais;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador CancelarLiberacaoParcial.
     */
    private ICancelarLiberacaoParcialPDCAdapter pdcCancelarLiberacaoParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador CancelarLiberacaoIntegral.
     */
    private ICancelarLiberacaoIntegralPDCAdapter pdcCancelarLiberacaoIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador CancelarPagtosParcial.
     */
    private ICancelarPagtosParcialPDCAdapter pdcCancelarPagtosParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarParcial.
     */
    private ILiberarParcialPDCAdapter pdcLiberarParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarIntegral.
     */
    private ILiberarIntegralPDCAdapter pdcLiberarIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador CancelarLibPagtosIndSemConsultaSaldo.
     */
    private ICancelarLibPagtosIndSemConsultaSaldoPDCAdapter pdcCancelarLibPagtosIndSemConsultaSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DesautorizarParcial.
     */
    private IDesautorizarParcialPDCAdapter pdcDesautorizarParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSolBasePagto.
     */
    private IListarSolBasePagtoPDCAdapter pdcListarSolBasePagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarLiberacaoPrevia.
     */
    private IAlterarLiberacaoPreviaPDCAdapter pdcAlterarLiberacaoPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirLiberacaoPrevia.
     */
    private IExcluirLiberacaoPreviaPDCAdapter pdcExcluirLiberacaoPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirLiberacaoPrevia.
     */
    private IIncluirLiberacaoPreviaPDCAdapter pdcIncluirLiberacaoPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DesautorizarIntegral.
     */
    private IDesautorizarIntegralPDCAdapter pdcDesautorizarIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarPagtoIndividualPendencia.
     */
    private ILiberarPagtoIndividualPendenciaPDCAdapter pdcLiberarPagtoIndividualPendencia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarPagtoPendIndividual.
     */
    private ILiberarPagtoPendIndividualPDCAdapter pdcLiberarPagtoPendIndividual;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSolBaseFavorecido.
     */
    private IListarSolBaseFavorecidoPDCAdapter pdcListarSolBaseFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolBaseFavorecido.
     */
    private IExcluirSolBaseFavorecidoPDCAdapter pdcExcluirSolBaseFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolBasePagto.
     */
    private IExcluirSolBasePagtoPDCAdapter pdcExcluirSolBasePagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AutorizarParcial.
     */
    private IAutorizarParcialPDCAdapter pdcAutorizarParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSolBaseSistema.
     */
    private IListarSolBaseSistemaPDCAdapter pdcListarSolBaseSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolEmiComprovanteFavorecido.
     */
    private IExcluirSolEmiComprovanteFavorecidoPDCAdapter pdcExcluirSolEmiComprovanteFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolBaseSistema.
     */
    private IExcluirSolBaseSistemaPDCAdapter pdcExcluirSolBaseSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AutorizarIntegral.
     */
    private IAutorizarIntegralPDCAdapter pdcAutorizarIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoSistema.
     */
    private IListarTipoSistemaPDCAdapter pdcListarTipoSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSolBaseRetransmissao.
     */
    private IListarSolBaseRetransmissaoPDCAdapter pdcListarSolBaseRetransmissao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolBaseRetransmissao.
     */
    private IExcluirSolBaseRetransmissaoPDCAdapter pdcExcluirSolBaseRetransmissao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarModalidadePGIT.
     */
    private IConsultarModalidadePGITPDCAdapter pdcConsultarModalidadePGIT;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolEmiComprovanteCliePagador.
     */
    private IExcluirSolEmiComprovanteCliePagadorPDCAdapter pdcExcluirSolEmiComprovanteCliePagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSituacaoRetorno.
     */
    private IListarSituacaoRetornoPDCAdapter pdcListarSituacaoRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoLayout.
     */
    private IListarTipoLayoutPDCAdapter pdcListarTipoLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarArquivoRetorno.
     */
    private IListarArquivoRetornoPDCAdapter pdcListarArquivoRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolEmiAviMovtoFavorecido.
     */
    private IConsultarSolEmiAviMovtoFavorecidoPDCAdapter pdcConsultarSolEmiAviMovtoFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolEmiAviMovtoFavorecido.
     */
    private IExcluirSolEmiAviMovtoFavorecidoPDCAdapter pdcExcluirSolEmiAviMovtoFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador CancelarPagtosIndividuais.
     */
    private ICancelarPagtosIndividuaisPDCAdapter pdcCancelarPagtosIndividuais;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolEmiAviMovtoCliePagador.
     */
    private IConsultarSolEmiAviMovtoCliePagadorPDCAdapter pdcConsultarSolEmiAviMovtoCliePagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador EncerrarContrato.
     */
    private IEncerrarContratoPDCAdapter pdcEncerrarContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador RecuperarContrato.
     */
    private IRecuperarContratoPDCAdapter pdcRecuperarContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarAgenciaOperadora.
     */
    private IConsultarAgenciaOperadoraPDCAdapter pdcConsultarAgenciaOperadora;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSituacaoRemessa.
     */
    private IListarSituacaoRemessaPDCAdapter pdcListarSituacaoRemessa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarResultadoProcessamentoRemessa.
     */
    private IListarResultadoProcessamentoRemessaPDCAdapter pdcListarResultadoProcessamentoRemessa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolEmiAviMovtoCliePagador.
     */
    private IExcluirSolEmiAviMovtoCliePagadorPDCAdapter pdcExcluirSolEmiAviMovtoCliePagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador CancelarPagtosIntegral.
     */
    private ICancelarPagtosIntegralPDCAdapter pdcCancelarPagtosIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtoPendConsolidado.
     */
    private IConsultarPagtoPendConsolidadoPDCAdapter pdcConsultarPagtoPendConsolidado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaPagamento.
     */
    private IConsultarListaPagamentoPDCAdapter pdcConsultarListaPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarPagtoPendConIntegral.
     */
    private ILiberarPagtoPendConIntegralPDCAdapter pdcLiberarPagtoPendConIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarPagtoPendConParcial.
     */
    private ILiberarPagtoPendConParcialPDCAdapter pdcLiberarPagtoPendConParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarPendenciaPagtoConIntegral.
     */
    private ILiberarPendenciaPagtoConIntegralPDCAdapter pdcLiberarPendenciaPagtoConIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador LiberarPendenciaPagtoConParcial.
     */
    private ILiberarPendenciaPagtoConParcialPDCAdapter pdcLiberarPendenciaPagtoConParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarModalidade.
     */
    private IConsultarModalidadePDCAdapter pdcConsultarModalidade;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarServicoPagto.
     */
    private IConsultarServicoPagtoPDCAdapter pdcConsultarServicoPagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarModalidadePagto.
     */
    private IConsultarModalidadePagtoPDCAdapter pdcConsultarModalidadePagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador EfetuarCalcOperReceitaTarifas.
     */
    private IEfetuarCalcOperReceitaTarifasPDCAdapter pdcEfetuarCalcOperReceitaTarifas;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolEmiComprovanteCliePagador.
     */
    private IIncluirSolEmiComprovanteCliePagadorPDCAdapter pdcIncluirSolEmiComprovanteCliePagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolEmiComprovanteFavorecido.
     */
    private IIncluirSolEmiComprovanteFavorecidoPDCAdapter pdcIncluirSolEmiComprovanteFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoLayoutContrato.
     */
    private IListarTipoLayoutContratoPDCAdapter pdcListarTipoLayoutContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarInformacoesFavorecido.
     */
    private IConsultarInformacoesFavorecidoPDCAdapter pdcConsultarInformacoesFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarServRelacServperacional.
     */
    private IConsultarServRelacServperacionalPDCAdapter pdcConsultarServRelacServperacional;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirContrato.
     */
    private IImprimirContratoPDCAdapter pdcImprimirContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarMotivoSituacaoEstorno.
     */
    private IConsultarMotivoSituacaoEstornoPDCAdapter pdcConsultarMotivoSituacaoEstorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarMotivoSituacaoPagamento.
     */
    private IConsultarMotivoSituacaoPagamentoPDCAdapter pdcConsultarMotivoSituacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSituacaoPagamento.
     */
    private IConsultarSituacaoPagamentoPDCAdapter pdcConsultarSituacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSituacaoSolConsistenciaPrevia.
     */
    private IConsultarSituacaoSolConsistenciaPreviaPDCAdapter pdcConsultarSituacaoSolConsistenciaPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirAditivoContrato.
     */
    private IImprimirAditivoContratoPDCAdapter pdcImprimirAditivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarTipoRetornoLayout.
     */
    private IAlterarTipoRetornoLayoutPDCAdapter pdcAlterarTipoRetornoLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador InlcuirTipoRetornoLayout.
     */
    private IInlcuirTipoRetornoLayoutPDCAdapter pdcInlcuirTipoRetornoLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicitacaoRelatorioFavorecidos.
     */
    private IIncluirSolicitacaoRelatorioFavorecidosPDCAdapter pdcIncluirSolicitacaoRelatorioFavorecidos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolRecPagtosVencidoNaoPago.
     */
    private IExcluirSolRecPagtosVencidoNaoPagoPDCAdapter pdcExcluirSolRecPagtosVencidoNaoPago;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolRecuperacaoPagtos.
     */
    private IExcluirSolRecuperacaoPagtosPDCAdapter pdcExcluirSolRecuperacaoPagtos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistLiquidacaoPagamento.
     */
    private IListarHistLiquidacaoPagamentoPDCAdapter pdcListarHistLiquidacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPrioridadeTipoCompromisso.
     */
    private IDetalharPrioridadeTipoCompromissoPDCAdapter pdcDetalharPrioridadeTipoCompromisso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirPrioridadeTipoCompromisso.
     */
    private IIncluirPrioridadeTipoCompromissoPDCAdapter pdcIncluirPrioridadeTipoCompromisso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharDadosContrato.
     */
    private IDetalharDadosContratoPDCAdapter pdcDetalharDadosContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetLiberacaoPrevia.
     */
    private IConsultarDetLiberacaoPreviaPDCAdapter pdcConsultarDetLiberacaoPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLiberacaoPrevia.
     */
    private IConsultarLiberacaoPreviaPDCAdapter pdcConsultarLiberacaoPrevia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaValoresDiscretos.
     */
    private IConsultarListaValoresDiscretosPDCAdapter pdcConsultarListaValoresDiscretos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador SolicitarRecPagtosBackupConsulta.
     */
    private ISolicitarRecPagtosBackupConsultaPDCAdapter pdcSolicitarRecPagtosBackupConsulta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarUltimaDataPagto.
     */
    private IListarUltimaDataPagtoPDCAdapter pdcListarUltimaDataPagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetManutencaoParticipante.
     */
    private IConsultarDetManutencaoParticipantePDCAdapter pdcConsultarDetManutencaoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarManutencaoContaContrato.
     */
    private IConsultarManutencaoContaContratoPDCAdapter pdcConsultarManutencaoContaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaParticipantesContrato.
     */
    private IConsultarListaParticipantesContratoPDCAdapter pdcConsultarListaParticipantesContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloquearDesbloquearProdutoServContrato.
     */
    private IBloquearDesbloquearProdutoServContratoPDCAdapter pdcBloquearDesbloquearProdutoServContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirDuplicacaoArqRetorno.
     */
    private IIncluirDuplicacaoArqRetornoPDCAdapter pdcIncluirDuplicacaoArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarDuplicacaoArqRetorno.
     */
    private IListarDuplicacaoArqRetornoPDCAdapter pdcListarDuplicacaoArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConPagtosSolRecVenNaoPago.
     */
    private IConPagtosSolRecVenNaoPagoPDCAdapter pdcConPagtosSolRecVenNaoPago;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador SolicitarRecPagtosVencidosNaoPagos.
     */
    private ISolicitarRecPagtosVencidosNaoPagosPDCAdapter pdcSolicitarRecPagtosVencidosNaoPagos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoPendenciaResponsaveis.
     */
    private IListarTipoPendenciaResponsaveisPDCAdapter pdcListarTipoPendenciaResponsaveis;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoCreditoConta.
     */
    private IDetalharManPagtoCreditoContaPDCAdapter pdcDetalharManPagtoCreditoConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarModalidadeServico.
     */
    private IConsultarModalidadeServicoPDCAdapter pdcConsultarModalidadeServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaClientePessoas.
     */
    private IConsultarListaClientePessoasPDCAdapter pdcConsultarListaClientePessoas;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarBancoAgencia.
     */
    private IConsultarBancoAgenciaPDCAdapter pdcConsultarBancoAgencia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirOperacaoServicoPagtoIntegrado.
     */
    private IExcluirOperacaoServicoPagtoIntegradoPDCAdapter pdcExcluirOperacaoServicoPagtoIntegrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAlteracaoAmbiente.
     */
    private IIncluirAlteracaoAmbientePDCAdapter pdcIncluirAlteracaoAmbiente;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicitacaoMudancaAmbiente.
     */
    private IConsultarSolicitacaoMudancaAmbientePDCAdapter pdcConsultarSolicitacaoMudancaAmbiente;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirDuplicacaoArqRetorno.
     */
    private IExcluirDuplicacaoArqRetornoPDCAdapter pdcExcluirDuplicacaoArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirLayoutArqServico.
     */
    private IExcluirLayoutArqServicoPDCAdapter pdcExcluirLayoutArqServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarHistoricoPerfilTrocaArquivo.
     */
    private IConsultarHistoricoPerfilTrocaArquivoPDCAdapter pdcConsultarHistoricoPerfilTrocaArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarEmail.
     */
    private IConsultarEmailPDCAdapter pdcConsultarEmail;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarEndereco.
     */
    private IConsultarEnderecoPDCAdapter pdcConsultarEndereco;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AutorizarSolicitacaoEstornoPatos.
     */
    private IAutorizarSolicitacaoEstornoPatosPDCAdapter pdcAutorizarSolicitacaoEstornoPatos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicitacaoEstornoPagtos.
     */
    private IExcluirSolicitacaoEstornoPagtosPDCAdapter pdcExcluirSolicitacaoEstornoPagtos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMotivoBloqueioDesbloqueio.
     */
    private IListarMotivoBloqueioDesbloqueioPDCAdapter pdcListarMotivoBloqueioDesbloqueio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloquearDesbloquearContratoPgit.
     */
    private IBloquearDesbloquearContratoPgitPDCAdapter pdcBloquearDesbloquearContratoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarMsgLayoutArqRetorno.
     */
    private IAlterarMsgLayoutArqRetornoPDCAdapter pdcAlterarMsgLayoutArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador RevertContratoMigrado.
     */
    private IRevertContratoMigradoPDCAdapter pdcRevertContratoMigrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarRepresentanteContratoPgit.
     */
    private IAlterarRepresentanteContratoPgitPDCAdapter pdcAlterarRepresentanteContratoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaContasInclusao.
     */
    private IConsultarListaContasInclusaoPDCAdapter pdcConsultarListaContasInclusao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarFinalidadeEnderecoOperacao.
     */
    private IListarFinalidadeEnderecoOperacaoPDCAdapter pdcListarFinalidadeEnderecoOperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirFinalidadeEnderecoOperacao.
     */
    private IIncluirFinalidadeEnderecoOperacaoPDCAdapter pdcIncluirFinalidadeEnderecoOperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirFinalidadeEnderecoOperacao.
     */
    private IExcluirFinalidadeEnderecoOperacaoPDCAdapter pdcExcluirFinalidadeEnderecoOperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParticipantesContrato.
     */
    private IListarParticipantesContratoPDCAdapter pdcListarParticipantesContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirContrEnvioArqFormaLiquidacao.
     */
    private IExcluirContrEnvioArqFormaLiquidacaoPDCAdapter pdcExcluirContrEnvioArqFormaLiquidacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirContrEnvioArqFormaLiquidacao.
     */
    private IIncluirContrEnvioArqFormaLiquidacaoPDCAdapter pdcIncluirContrEnvioArqFormaLiquidacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharFinalidadeEnderecoOperacao.
     */
    private IDetalharFinalidadeEnderecoOperacaoPDCAdapter pdcDetalharFinalidadeEnderecoOperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirServicoRelacionado.
     */
    private IExcluirServicoRelacionadoPDCAdapter pdcExcluirServicoRelacionado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoRelacionado.
     */
    private IListarServicoRelacionadoPDCAdapter pdcListarServicoRelacionado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirMsgLayoutArqRetorno.
     */
    private IExcluirMsgLayoutArqRetornoPDCAdapter pdcExcluirMsgLayoutArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirMsgLayoutArqRetorno.
     */
    private IIncluirMsgLayoutArqRetornoPDCAdapter pdcIncluirMsgLayoutArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPosDiaPagtoAgencia.
     */
    private IConsultarPosDiaPagtoAgenciaPDCAdapter pdcConsultarPosDiaPagtoAgencia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPorContaDebito.
     */
    private IDetalharPorContaDebitoPDCAdapter pdcDetalharPorContaDebito;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarTarifaContrato.
     */
    private IConsultarTarifaContratoPDCAdapter pdcConsultarTarifaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AnteciparPostergarPagtosIndividuais.
     */
    private IAnteciparPostergarPagtosIndividuaisPDCAdapter pdcAnteciparPostergarPagtosIndividuais;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AntPostergarPagtosParcial.
     */
    private IAntPostergarPagtosParcialPDCAdapter pdcAntPostergarPagtosParcial;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AntPostergarPagtosIntegral.
     */
    private IAntPostergarPagtosIntegralPDCAdapter pdcAntPostergarPagtosIntegral;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSaldoAtual.
     */
    private IConsultarSaldoAtualPDCAdapter pdcConsultarSaldoAtual;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsutarContrEnvioFormaLiquidacao.
     */
    private IConsutarContrEnvioFormaLiquidacaoPDCAdapter pdcConsutarContrEnvioFormaLiquidacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarPrioridadeTipoCompromisso.
     */
    private IAlterarPrioridadeTipoCompromissoPDCAdapter pdcAlterarPrioridadeTipoCompromisso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharContrEnvioFormaLiquidacao.
     */
    private IDetalharContrEnvioFormaLiquidacaoPDCAdapter pdcDetalharContrEnvioFormaLiquidacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirServicoLancamentoCnab.
     */
    private IIncluirServicoLancamentoCnabPDCAdapter pdcIncluirServicoLancamentoCnab;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirServicoLancamentoCnab.
     */
    private IExcluirServicoLancamentoCnabPDCAdapter pdcExcluirServicoLancamentoCnab;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarServicoLancamentoCnab.
     */
    private IConsultarServicoLancamentoCnabPDCAdapter pdcConsultarServicoLancamentoCnab;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharDuplicacaoArqRetorno.
     */
    private IDetalharDuplicacaoArqRetornoPDCAdapter pdcDetalharDuplicacaoArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarVinculacaoMsgLayoutSistema.
     */
    private IConsultarVinculacaoMsgLayoutSistemaPDCAdapter pdcConsultarVinculacaoMsgLayoutSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirVinculacaoMsgLayoutSistema.
     */
    private IExcluirVinculacaoMsgLayoutSistemaPDCAdapter pdcExcluirVinculacaoMsgLayoutSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirVinculacaoMsgLayoutSistema.
     */
    private IIncluirVinculacaoMsgLayoutSistemaPDCAdapter pdcIncluirVinculacaoMsgLayoutSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharServicoLancamentoCnab.
     */
    private IDetalharServicoLancamentoCnabPDCAdapter pdcDetalharServicoLancamentoCnab;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharVinculacaoMsgLayoutSistema.
     */
    private IDetalharVinculacaoMsgLayoutSistemaPDCAdapter pdcDetalharVinculacaoMsgLayoutSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDescBancoAgenciaConta.
     */
    private IConsultarDescBancoAgenciaContaPDCAdapter pdcConsultarDescBancoAgenciaConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosSolicitacaoRecuperacao.
     */
    private IConsultarPagtosSolicitacaoRecuperacaoPDCAdapter pdcConsultarPagtosSolicitacaoRecuperacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirTipoServModContrato.
     */
    private IExcluirTipoServModContratoPDCAdapter pdcExcluirTipoServModContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarOperacaoServicoPagtoIntegrado.
     */
    private IListarOperacaoServicoPagtoIntegradoPDCAdapter pdcListarOperacaoServicoPagtoIntegrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarVinculoContaSalarioDestino.
     */
    private IListarVinculoContaSalarioDestinoPDCAdapter pdcListarVinculoContaSalarioDestino;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarIncluirSolEmiComprovante.
     */
    private IListarIncluirSolEmiComprovantePDCAdapter pdcListarIncluirSolEmiComprovante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtoSolEmiCompFavorecido.
     */
    private IConsultarPagtoSolEmiCompFavorecidoPDCAdapter pdcConsultarPagtoSolEmiCompFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMsgComprovanteSalrl.
     */
    private IListarMsgComprovanteSalrlPDCAdapter pdcListarMsgComprovanteSalrl;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador BloqDesbloqEmissaoAutAvisosComprv.
     */
    private IBloqDesbloqEmissaoAutAvisosComprvPDCAdapter pdcBloqDesbloqEmissaoAutAvisosComprv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarParticipanteContrato.
     */
    private IConsultarParticipanteContratoPDCAdapter pdcConsultarParticipanteContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarEmissaoAutAvisosComprv.
     */
    private IListarEmissaoAutAvisosComprvPDCAdapter pdcListarEmissaoAutAvisosComprv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarMotivoSituacaoPagPendente.
     */
    private IConsultarMotivoSituacaoPagPendentePDCAdapter pdcConsultarMotivoSituacaoPagPendente;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarClasseRamoAtvdd.
     */
    private IListarClasseRamoAtvddPDCAdapter pdcListarClasseRamoAtvdd;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSramoAtvddEconc.
     */
    private IListarSramoAtvddEconcPDCAdapter pdcListarSramoAtvddEconc;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirVinculoEmprPagContSal.
     */
    private IIncluirVinculoEmprPagContSalPDCAdapter pdcIncluirVinculoEmprPagContSal;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetHistCtaSalarioEmpresa.
     */
    private IConsultarDetHistCtaSalarioEmpresaPDCAdapter pdcConsultarDetHistCtaSalarioEmpresa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirTerceiroPartContrato.
     */
    private IExcluirTerceiroPartContratoPDCAdapter pdcExcluirTerceiroPartContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirTerceiroPartContrato.
     */
    private IIncluirTerceiroPartContratoPDCAdapter pdcIncluirTerceiroPartContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoProcesso.
     */
    private IListarTipoProcessoPDCAdapter pdcListarTipoProcesso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirEmissaoAutAvisosComprv.
     */
    private IIncluirEmissaoAutAvisosComprvPDCAdapter pdcIncluirEmissaoAutAvisosComprv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTercParticipanteContrato.
     */
    private IListarTercParticipanteContratoPDCAdapter pdcListarTercParticipanteContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharEmissaoAutAvisosComprv.
     */
    private IDetalharEmissaoAutAvisosComprvPDCAdapter pdcDetalharEmissaoAutAvisosComprv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarRelatorioContratos.
     */
    private IListarRelatorioContratosPDCAdapter pdcListarRelatorioContratos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarLimDIaOpePgit.
     */
    private IAlterarLimDIaOpePgitPDCAdapter pdcAlterarLimDIaOpePgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLimDiarioUtilizado.
     */
    private IConsultarLimDiarioUtilizadoPDCAdapter pdcConsultarLimDiarioUtilizado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicitacaoRelatorioFavorecidos.
     */
    private IDetalharSolicitacaoRelatorioFavorecidosPDCAdapter pdcDetalharSolicitacaoRelatorioFavorecidos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador SolicitarRelatorioContratos.
     */
    private ISolicitarRelatorioContratosPDCAdapter pdcSolicitarRelatorioContratos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirEmissaoAutAvisosComprv.
     */
    private IExcluirEmissaoAutAvisosComprvPDCAdapter pdcExcluirEmissaoAutAvisosComprv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistEmissaoAutAvisosComprv.
     */
    private IListarHistEmissaoAutAvisosComprvPDCAdapter pdcListarHistEmissaoAutAvisosComprv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistEmissaoAutAvisosComprv.
     */
    private IDetalharHistEmissaoAutAvisosComprvPDCAdapter pdcDetalharHistEmissaoAutAvisosComprv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharRelatorioContratos.
     */
    private IDetalharRelatorioContratosPDCAdapter pdcDetalharRelatorioContratos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteCredConta.
     */
    private IImprimirComprovanteCredContaPDCAdapter pdcImprimirComprovanteCredConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador Impdiaad.
     */
    private IImpdiaadPDCAdapter pdcImpdiaad;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirTipoPendenciaResponsaveis.
     */
    private IExcluirTipoPendenciaResponsaveisPDCAdapter pdcExcluirTipoPendenciaResponsaveis;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarIndiceEconomico.
     */
    private IListarIndiceEconomicoPDCAdapter pdcListarIndiceEconomico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarCondCobTarifa.
     */
    private IConsultarCondCobTarifaPDCAdapter pdcConsultarCondCobTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaHistoricoFavorecido.
     */
    private IConsultarListaHistoricoFavorecidoPDCAdapter pdcConsultarListaHistoricoFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaHistoricoContaFavorecido.
     */
    private IConsultarListaHistoricoContaFavorecidoPDCAdapter pdcConsultarListaHistoricoContaFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistVinculoContaSalarioDestino.
     */
    private IListarHistVinculoContaSalarioDestinoPDCAdapter pdcListarHistVinculoContaSalarioDestino;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarVinculoHistCtaSalarioEmpresa.
     */
    private IListarVinculoHistCtaSalarioEmpresaPDCAdapter pdcListarVinculoHistCtaSalarioEmpresa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDescricaoGrupoEconomico.
     */
    private IConsultarDescricaoGrupoEconomicoPDCAdapter pdcConsultarDescricaoGrupoEconomico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirEnderecoParticipante.
     */
    private IIncluirEnderecoParticipantePDCAdapter pdcIncluirEnderecoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirEnderecoParticipante.
     */
    private IExcluirEnderecoParticipantePDCAdapter pdcExcluirEnderecoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarParticipanteContratoPgit.
     */
    private IAlterarParticipanteContratoPgitPDCAdapter pdcAlterarParticipanteContratoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolRecuperacaoPagtosBackup.
     */
    private IConsultarSolRecuperacaoPagtosBackupPDCAdapter pdcConsultarSolRecuperacaoPagtosBackup;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolRecPagtosVencNaoPago.
     */
    private IConsultarSolRecPagtosVencNaoPagoPDCAdapter pdcConsultarSolRecPagtosVencNaoPago;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarEnderecoEmail.
     */
    private IListarEnderecoEmailPDCAdapter pdcListarEnderecoEmail;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarEnderecoParticipante.
     */
    private IListarEnderecoParticipantePDCAdapter pdcListarEnderecoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarBloqueioRastreamento.
     */
    private IConsultarBloqueioRastreamentoPDCAdapter pdcConsultarBloqueioRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirBloqueioRastreamento.
     */
    private IIncluirBloqueioRastreamentoPDCAdapter pdcIncluirBloqueioRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarInclusaoBloqueioRastreamento.
     */
    private IValidarInclusaoBloqueioRastreamentoPDCAdapter pdcValidarInclusaoBloqueioRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirBloqueioRastreamento.
     */
    private IExcluirBloqueioRastreamentoPDCAdapter pdcExcluirBloqueioRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharBloqueioRastreamento.
     */
    private IDetalharBloqueioRastreamentoPDCAdapter pdcDetalharBloqueioRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarTercPartContrato.
     */
    private IConsultarTercPartContratoPDCAdapter pdcConsultarTercPartContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarTercParticiContrato.
     */
    private IAlterarTercParticiContratoPDCAdapter pdcAlterarTercParticiContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistoricoBloqueio.
     */
    private IDetalharHistoricoBloqueioPDCAdapter pdcDetalharHistoricoBloqueio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistoricoBloqueio.
     */
    private IListarHistoricoBloqueioPDCAdapter pdcListarHistoricoBloqueio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConManDadosBasicos.
     */
    private IListarConManDadosBasicosPDCAdapter pdcListarConManDadosBasicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarManutencaoParticipante.
     */
    private IConsultarManutencaoParticipantePDCAdapter pdcConsultarManutencaoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConManContaVinculada.
     */
    private IListarConManContaVinculadaPDCAdapter pdcListarConManContaVinculada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConManTarifa.
     */
    private IListarConManTarifaPDCAdapter pdcListarConManTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteDOC.
     */
    private IImprimirComprovanteDOCPDCAdapter pdcImprimirComprovanteDOC;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteTED.
     */
    private IImprimirComprovanteTEDPDCAdapter pdcImprimirComprovanteTED;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteOP.
     */
    private IImprimirComprovanteOPPDCAdapter pdcImprimirComprovanteOP;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteTituloBradesco.
     */
    private IImprimirComprovanteTituloBradescoPDCAdapter pdcImprimirComprovanteTituloBradesco;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteTituloOutros.
     */
    private IImprimirComprovanteTituloOutrosPDCAdapter pdcImprimirComprovanteTituloOutros;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteDARF.
     */
    private IImprimirComprovanteDARFPDCAdapter pdcImprimirComprovanteDARF;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteGARE.
     */
    private IImprimirComprovanteGAREPDCAdapter pdcImprimirComprovanteGARE;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteGPS.
     */
    private IImprimirComprovanteGPSPDCAdapter pdcImprimirComprovanteGPS;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDadosCliente.
     */
    private IConsultarDadosClientePDCAdapter pdcConsultarDadosCliente;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConManServico.
     */
    private IListarConManServicoPDCAdapter pdcListarConManServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistoricoLayoutContrato.
     */
    private IListarHistoricoLayoutContratoPDCAdapter pdcListarHistoricoLayoutContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAlteracaoAmbiente.
     */
    private IExcluirAlteracaoAmbientePDCAdapter pdcExcluirAlteracaoAmbiente;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaServLimDiaIndv.
     */
    private IConsultarListaServLimDiaIndvPDCAdapter pdcConsultarListaServLimDiaIndv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarEmpresaTransmissaoArquivoVan.
     */
    private IListarEmpresaTransmissaoArquivoVanPDCAdapter pdcListarEmpresaTransmissaoArquivoVan;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirEquiparacaoContrato.
     */
    private IIncluirEquiparacaoContratoPDCAdapter pdcIncluirEquiparacaoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarMsgLayoutArqRetorno.
     */
    private IConsultarMsgLayoutArqRetornoPDCAdapter pdcConsultarMsgLayoutArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarPrioridadeTipoCompromisso.
     */
    private IListarPrioridadeTipoCompromissoPDCAdapter pdcListarPrioridadeTipoCompromisso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarLayoutArqServico.
     */
    private IListarLayoutArqServicoPDCAdapter pdcListarLayoutArqServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarServicoContratado.
     */
    private IValidarServicoContratadoPDCAdapter pdcValidarServicoContratado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConManLayout.
     */
    private IListarConManLayoutPDCAdapter pdcListarConManLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarManutencaoContrato.
     */
    private IConsultarManutencaoContratoPDCAdapter pdcConsultarManutencaoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicosEmissao.
     */
    private IListarServicosEmissaoPDCAdapter pdcListarServicosEmissao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSolicitCriacaoContaTipo.
     */
    private IListarSolicitCriacaoContaTipoPDCAdapter pdcListarSolicitCriacaoContaTipo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicitCriacaoContaTipo.
     */
    private IIncluirSolicitCriacaoContaTipoPDCAdapter pdcIncluirSolicitCriacaoContaTipo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicitCriacaoContaTipo.
     */
    private IExcluirSolicitCriacaoContaTipoPDCAdapter pdcExcluirSolicitCriacaoContaTipo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarModalidadeListaDebito.
     */
    private IListarModalidadeListaDebitoPDCAdapter pdcListarModalidadeListaDebito;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador EfetuarValidacaoTarifaOperTipoServico.
     */
    private IEfetuarValidacaoTarifaOperTipoServicoPDCAdapter pdcEfetuarValidacaoTarifaOperTipoServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaDebitoAutDes.
     */
    private IConsultarListaDebitoAutDesPDCAdapter pdcConsultarListaDebitoAutDes;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaDebito.
     */
    private IConsultarListaDebitoPDCAdapter pdcConsultarListaDebito;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarLimiteIntraPgit.
     */
    private IAlterarLimiteIntraPgitPDCAdapter pdcAlterarLimiteIntraPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirRecisaoContratoPgitMulticanal.
     */
    private IImprimirRecisaoContratoPgitMulticanalPDCAdapter pdcImprimirRecisaoContratoPgitMulticanal;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador SubstituirVincEnderecoParticipante.
     */
    private ISubstituirVincEnderecoParticipantePDCAdapter pdcSubstituirVincEnderecoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistoricoEnderecoParticipante.
     */
    private IListarHistoricoEnderecoParticipantePDCAdapter pdcListarHistoricoEnderecoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParticipanteAgregado.
     */
    private IListarParticipanteAgregadoPDCAdapter pdcListarParticipanteAgregado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAgregado.
     */
    private IListarAgregadoPDCAdapter pdcListarAgregado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirVincConvnCtaSalario.
     */
    private IIncluirVincConvnCtaSalarioPDCAdapter pdcIncluirVincConvnCtaSalario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarVincConvnCtaSalario.
     */
    private IAlterarVincConvnCtaSalarioPDCAdapter pdcAlterarVincConvnCtaSalario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirVincConvnCtaSalario.
     */
    private IExcluirVincConvnCtaSalarioPDCAdapter pdcExcluirVincConvnCtaSalario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosConsPendAutorizacao.
     */
    private IConsultarPagtosConsPendAutorizacaoPDCAdapter pdcConsultarPagtosConsPendAutorizacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosConsAutorizados.
     */
    private IConsultarPagtosConsAutorizadosPDCAdapter pdcConsultarPagtosConsAutorizados;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLibPagtosConsSemConsultaSaldo.
     */
    private IConsultarLibPagtosConsSemConsultaSaldoPDCAdapter pdcConsultarLibPagtosConsSemConsultaSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarCanLibPagtosConsSemConsSaldo.
     */
    private IConsultarCanLibPagtosConsSemConsSaldoPDCAdapter pdcConsultarCanLibPagtosConsSemConsSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosConsParaCancelamento.
     */
    private IConsultarPagtosConsParaCancelamentoPDCAdapter pdcConsultarPagtosConsParaCancelamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharLibPagtosConsSemConsultaSaldo.
     */
    private IDetalharLibPagtosConsSemConsultaSaldoPDCAdapter pdcDetalharLibPagtosConsSemConsultaSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharCanLibPagtosConsSemConsSaldo.
     */
    private IDetalharCanLibPagtosConsSemConsSaldoPDCAdapter pdcDetalharCanLibPagtosConsSemConsSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtosConsPendAutorizacao.
     */
    private IDetalharPagtosConsPendAutorizacaoPDCAdapter pdcDetalharPagtosConsPendAutorizacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharAntPostergarPagtos.
     */
    private IDetalharAntPostergarPagtosPDCAdapter pdcDetalharAntPostergarPagtos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtosConsParaCancelamento.
     */
    private IDetalharPagtosConsParaCancelamentoPDCAdapter pdcDetalharPagtosConsParaCancelamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtosConsAutorizados.
     */
    private IDetalharPagtosConsAutorizadosPDCAdapter pdcDetalharPagtosConsAutorizados;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoDebitoVeiculos.
     */
    private IDetalharPagtoDebitoVeiculosPDCAdapter pdcDetalharPagtoDebitoVeiculos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoDebitoVeiculos.
     */
    private IDetalharManPagtoDebitoVeiculosPDCAdapter pdcDetalharManPagtoDebitoVeiculos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicitCriacaoContaTipo.
     */
    private IDetalharSolicitCriacaoContaTipoPDCAdapter pdcDetalharSolicitCriacaoContaTipo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarClubesClientes.
     */
    private IListarClubesClientesPDCAdapter pdcListarClubesClientes;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoDebitoConta.
     */
    private IDetalharManPagtoDebitoContaPDCAdapter pdcDetalharManPagtoDebitoConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoDepIdentificado.
     */
    private IDetalharManPagtoDepIdentificadoPDCAdapter pdcDetalharManPagtoDepIdentificado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoOrdemCredito.
     */
    private IDetalharManPagtoOrdemCreditoPDCAdapter pdcDetalharManPagtoOrdemCredito;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoDebitoConta.
     */
    private IDetalharPagtoDebitoContaPDCAdapter pdcDetalharPagtoDebitoConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoDepIdentificado.
     */
    private IDetalharPagtoDepIdentificadoPDCAdapter pdcDetalharPagtoDepIdentificado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoOrdemCredito.
     */
    private IDetalharPagtoOrdemCreditoPDCAdapter pdcDetalharPagtoOrdemCredito;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarDadosCtaConvnContingencia.
     */
    private IListarDadosCtaConvnContingenciaPDCAdapter pdcListarDadosCtaConvnContingencia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarParametroTela.
     */
    private IValidarParametroTelaPDCAdapter pdcValidarParametroTela;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAplictvFormtTipoLayoutArqv.
     */
    private IListarAplictvFormtTipoLayoutArqvPDCAdapter pdcListarAplictvFormtTipoLayoutArqv;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharEnderecoParticipante.
     */
    private IDetalharEnderecoParticipantePDCAdapter pdcDetalharEnderecoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistoricoEnderecoParticipante.
     */
    private IDetalharHistoricoEnderecoParticipantePDCAdapter pdcDetalharHistoricoEnderecoParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaOperacaoTarifaTipoServico.
     */
    private IConsultarListaOperacaoTarifaTipoServicoPDCAdapter pdcConsultarListaOperacaoTarifaTipoServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharMsgLayoutArqRetorno.
     */
    private IDetalharMsgLayoutArqRetornoPDCAdapter pdcDetalharMsgLayoutArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaPeriodicidadeSistemaPGIC.
     */
    private IConsultarListaPeriodicidadeSistemaPGICPDCAdapter pdcConsultarListaPeriodicidadeSistemaPGIC;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListaTipoServicoContrato.
     */
    private IListaTipoServicoContratoPDCAdapter pdcListaTipoServicoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarModalidadeContrato.
     */
    private IListarModalidadeContratoPDCAdapter pdcListarModalidadeContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosConsAntPostergacao.
     */
    private IConsultarPagtosConsAntPostergacaoPDCAdapter pdcConsultarPagtosConsAntPostergacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolEmiComprovanteCliePagador.
     */
    private IDetalharSolEmiComprovanteCliePagadorPDCAdapter pdcDetalharSolEmiComprovanteCliePagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolEmiComprovanteFavorecido.
     */
    private IDetalharSolEmiComprovanteFavorecidoPDCAdapter pdcDetalharSolEmiComprovanteFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarDuplicacaoArqRetorno.
     */
    private IAlterarDuplicacaoArqRetornoPDCAdapter pdcAlterarDuplicacaoArqRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarTipoPendenciaResponsaveis.
     */
    private IAlterarTipoPendenciaResponsaveisPDCAdapter pdcAlterarTipoPendenciaResponsaveis;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharTipoPendenciaResponsaveis.
     */
    private IDetalharTipoPendenciaResponsaveisPDCAdapter pdcDetalharTipoPendenciaResponsaveis;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirTipoPendenciaResponsaveis.
     */
    private IIncluirTipoPendenciaResponsaveisPDCAdapter pdcIncluirTipoPendenciaResponsaveis;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMeioTransmissaoPagamento.
     */
    private IListarMeioTransmissaoPagamentoPDCAdapter pdcListarMeioTransmissaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaPerfilTrocaArquivo.
     */
    private IConsultarListaPerfilTrocaArquivoPDCAdapter pdcConsultarListaPerfilTrocaArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarManutencaoMeioTransmissao.
     */
    private IConsultarManutencaoMeioTransmissaoPDCAdapter pdcConsultarManutencaoMeioTransmissao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListaContratosVinculadosPerfil.
     */
    private IListaContratosVinculadosPerfilPDCAdapter pdcListaContratosVinculadosPerfil;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirLiquidacaoPagamento.
     */
    private IExcluirLiquidacaoPagamentoPDCAdapter pdcExcluirLiquidacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAplicativoFormatacaoArquivo.
     */
    private IListarAplicativoFormatacaoArquivoPDCAdapter pdcListarAplicativoFormatacaoArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarCondReajusteTarifa.
     */
    private IConsultarCondReajusteTarifaPDCAdapter pdcConsultarCondReajusteTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSituacaoSolicitacaoEstorno.
     */
    private IConsultarSituacaoSolicitacaoEstornoPDCAdapter pdcConsultarSituacaoSolicitacaoEstorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ReenviarEmail.
     */
    private IReenviarEmailPDCAdapter pdcReenviarEmail;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirTipoServModContrato.
     */
    private IIncluirTipoServModContratoPDCAdapter pdcIncluirTipoServModContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoEquiparacaoContrato.
     */
    private IListarServicoEquiparacaoContratoPDCAdapter pdcListarServicoEquiparacaoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarModalidadeEquiparacaoContrato.
     */
    private IListarModalidadeEquiparacaoContratoPDCAdapter pdcListarModalidadeEquiparacaoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoModalidadeEmissaoAviso.
     */
    private IListarServicoModalidadeEmissaoAvisoPDCAdapter pdcListarServicoModalidadeEmissaoAviso;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarServico.
     */
    private IConsultarServicoPDCAdapter pdcConsultarServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarContratoClienteFiltro.
     */
    private IConsultarContratoClienteFiltroPDCAdapter pdcConsultarContratoClienteFiltro;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContratosPgit.
     */
    private IListarContratosPgitPDCAdapter pdcListarContratosPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaContratosPessoas.
     */
    private IConsultarListaContratosPessoasPDCAdapter pdcConsultarListaContratosPessoas;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharVincConvnCtaSalario.
     */
    private IDetalharVincConvnCtaSalarioPDCAdapter pdcDetalharVincConvnCtaSalario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAlteracaoAmbientePartc.
     */
    private IIncluirAlteracaoAmbientePartcPDCAdapter pdcIncluirAlteracaoAmbientePartc;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicitacaoAmbientePartc.
     */
    private IConsultarSolicitacaoAmbientePartcPDCAdapter pdcConsultarSolicitacaoAmbientePartc;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAlteracaoAmbientePartc.
     */
    private IExcluirAlteracaoAmbientePartcPDCAdapter pdcExcluirAlteracaoAmbientePartc;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharAmbiente.
     */
    private IDetalharAmbientePDCAdapter pdcDetalharAmbiente;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConManAmbPart.
     */
    private IConsultarConManAmbPartPDCAdapter pdcConsultarConManAmbPart;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConManAmbPart.
     */
    private IListarConManAmbPartPDCAdapter pdcListarConManAmbPart;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador VerificaraAtributoSoltc.
     */
    private IVerificaraAtributoSoltcPDCAdapter pdcVerificaraAtributoSoltc;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarContaContrato.
     */
    private IConsultarContaContratoPDCAdapter pdcConsultarContaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAssocTrocaArqParticipante.
     */
    private IListarAssocTrocaArqParticipantePDCAdapter pdcListarAssocTrocaArqParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParticipante.
     */
    private IListarParticipantePDCAdapter pdcListarParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistAssocTrocaArqParticipante.
     */
    private IListarHistAssocTrocaArqParticipantePDCAdapter pdcListarHistAssocTrocaArqParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaPendenciaContrato.
     */
    private IConsultarListaPendenciaContratoPDCAdapter pdcConsultarListaPendenciaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParticipantes.
     */
    private IListarParticipantesPDCAdapter pdcListarParticipantes;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConManParticipantes.
     */
    private IConsultarConManParticipantesPDCAdapter pdcConsultarConManParticipantes;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirContratoMulticanal.
     */
    private IImprimirContratoMulticanalPDCAdapter pdcImprimirContratoMulticanal;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarTarifaAcimaPadrao.
     */
    private IAlterarTarifaAcimaPadraoPDCAdapter pdcAlterarTarifaAcimaPadrao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSoliRelatorioPagamento.
     */
    private IListarSoliRelatorioPagamentoPDCAdapter pdcListarSoliRelatorioPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSoliRelatorioPagamento.
     */
    private IDetalharSoliRelatorioPagamentoPDCAdapter pdcDetalharSoliRelatorioPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSoliRelatorioPagamento.
     */
    private IExcluirSoliRelatorioPagamentoPDCAdapter pdcExcluirSoliRelatorioPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSoliRelatorioPagamentoValidacao.
     */
    private IIncluirSoliRelatorioPagamentoValidacaoPDCAdapter pdcIncluirSoliRelatorioPagamentoValidacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSoliRelatorioPagamento.
     */
    private IIncluirSoliRelatorioPagamentoPDCAdapter pdcIncluirSoliRelatorioPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSoliRelatorioOrgaosPublicos.
     */
    private IListarSoliRelatorioOrgaosPublicosPDCAdapter pdcListarSoliRelatorioOrgaosPublicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSoliRelatorioOrgaosPublicos.
     */
    private IExcluirSoliRelatorioOrgaosPublicosPDCAdapter pdcExcluirSoliRelatorioOrgaosPublicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharParticipantesOrgaoPublico.
     */
    private IDetalharParticipantesOrgaoPublicoPDCAdapter pdcDetalharParticipantesOrgaoPublico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirParticipantesOrgaoPublico.
     */
    private IExcluirParticipantesOrgaoPublicoPDCAdapter pdcExcluirParticipantesOrgaoPublico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarParticipantesOrgaoPublico.
     */
    private IAlterarParticipantesOrgaoPublicoPDCAdapter pdcAlterarParticipantesOrgaoPublico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarContratoPagSalPartOrgaoPublico.
     */
    private IValidarContratoPagSalPartOrgaoPublicoPDCAdapter pdcValidarContratoPagSalPartOrgaoPublico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarCtasVincPartOrgaoPublico.
     */
    private IListarCtasVincPartOrgaoPublicoPDCAdapter pdcListarCtasVincPartOrgaoPublico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirParticipantesOrgaoPublico.
     */
    private IIncluirParticipantesOrgaoPublicoPDCAdapter pdcIncluirParticipantesOrgaoPublico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarUsuarioSoliRelatorioOrgaosPublicos.
     */
    private IConsultarUsuarioSoliRelatorioOrgaosPublicosPDCAdapter pdcConsultarUsuarioSoliRelatorioOrgaosPublicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSoliRelatorioOrgaosPublicos.
     */
    private IIncluirSoliRelatorioOrgaosPublicosPDCAdapter pdcIncluirSoliRelatorioOrgaosPublicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador EstornarPagamentosOP.
     */
    private IEstornarPagamentosOPPDCAdapter pdcEstornarPagamentosOP;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalhesSolicitacaoOP.
     */
    private IConsultarDetalhesSolicitacaoOPPDCAdapter pdcConsultarDetalhesSolicitacaoOP;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsSolicitacaoEstornoPagtosOP.
     */
    private IConsSolicitacaoEstornoPagtosOPPDCAdapter pdcConsSolicitacaoEstornoPagtosOP;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaServicosPgto.
     */
    private IConsultarListaServicosPgtoPDCAdapter pdcConsultarListaServicosPgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaModalidades.
     */
    private IConsultarListaModalidadesPDCAdapter pdcConsultarListaModalidades;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarAgenciaGestora.
     */
    private IAlterarAgenciaGestoraPDCAdapter pdcAlterarAgenciaGestora;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParticipantesOrgaoPublico.
     */
    private IListarParticipantesOrgaoPublicoPDCAdapter pdcListarParticipantesOrgaoPublico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarSoliRelatorioEmpresaAcompanhada.
     */
    private IListarSoliRelatorioEmpresaAcompanhadaPDCAdapter pdcListarSoliRelatorioEmpresaAcompanhada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSoliRelatorioEmpresaAcompanhada.
     */
    private IIncluirSoliRelatorioEmpresaAcompanhadaPDCAdapter pdcIncluirSoliRelatorioEmpresaAcompanhada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSoliRelatorioEmpresaAcompanhada.
     */
    private IExcluirSoliRelatorioEmpresaAcompanhadaPDCAdapter pdcExcluirSoliRelatorioEmpresaAcompanhada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarEmpresaAcompanhada.
     */
    private IAlterarEmpresaAcompanhadaPDCAdapter pdcAlterarEmpresaAcompanhada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsistirDadosLogicos.
     */
    private IConsistirDadosLogicosPDCAdapter pdcConsistirDadosLogicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirEmpresaAcompanhada.
     */
    private IExcluirEmpresaAcompanhadaPDCAdapter pdcExcluirEmpresaAcompanhada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirEmpresaAcompanhada.
     */
    private IIncluirEmpresaAcompanhadaPDCAdapter pdcIncluirEmpresaAcompanhada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharCnpjFicticio.
     */
    private IDetalharCnpjFicticioPDCAdapter pdcDetalharCnpjFicticio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirCnpjFicticio.
     */
    private IExcluirCnpjFicticioPDCAdapter pdcExcluirCnpjFicticio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirCnpjFicticio.
     */
    private IIncluirCnpjFicticioPDCAdapter pdcIncluirCnpjFicticio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarCnpjFicticio.
     */
    private IListarCnpjFicticioPDCAdapter pdcListarCnpjFicticio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarCnpjFicticio.
     */
    private IValidarCnpjFicticioPDCAdapter pdcValidarCnpjFicticio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarModalidadesEstorno.
     */
    private IListarModalidadesEstornoPDCAdapter pdcListarModalidadesEstorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContratosRenegociacao.
     */
    private IListarContratosRenegociacaoPDCAdapter pdcListarContratosRenegociacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarEmpresaAcompanhada.
     */
    private IConsultarEmpresaAcompanhadaPDCAdapter pdcConsultarEmpresaAcompanhada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoArquivoRetorno.
     */
    private IListarTipoArquivoRetornoPDCAdapter pdcListarTipoArquivoRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaTipoRetornoLayout.
     */
    private IConsultarListaTipoRetornoLayoutPDCAdapter pdcConsultarListaTipoRetornoLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolEmiAviMovtoFavorecido.
     */
    private IDetalharSolEmiAviMovtoFavorecidoPDCAdapter pdcDetalharSolEmiAviMovtoFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolEmiAviMovtoCliePagador.
     */
    private IDetalharSolEmiAviMovtoCliePagadorPDCAdapter pdcDetalharSolEmiAviMovtoCliePagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicitacaoRastreamento.
     */
    private IDetalharSolicitacaoRastreamentoPDCAdapter pdcDetalharSolicitacaoRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolBaseFavorecido.
     */
    private IDetalharSolBaseFavorecidoPDCAdapter pdcDetalharSolBaseFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolBasePagto.
     */
    private IDetalharSolBasePagtoPDCAdapter pdcDetalharSolBasePagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolBaseRetransmissao.
     */
    private IDetalharSolBaseRetransmissaoPDCAdapter pdcDetalharSolBaseRetransmissao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolBaseSistema.
     */
    private IDetalharSolBaseSistemaPDCAdapter pdcDetalharSolBaseSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosSolicitacaoEstorno.
     */
    private IConsultarPagtosSolicitacaoEstornoPDCAdapter pdcConsultarPagtosSolicitacaoEstorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarServicoComposto.
     */
    private IAlterarServicoCompostoPDCAdapter pdcAlterarServicoComposto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharServicoComposto.
     */
    private IDetalharServicoCompostoPDCAdapter pdcDetalharServicoComposto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirServicoComposto.
     */
    private IExcluirServicoCompostoPDCAdapter pdcExcluirServicoComposto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirServicoComposto.
     */
    private IIncluirServicoCompostoPDCAdapter pdcIncluirServicoComposto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoComposto.
     */
    private IListarServicoCompostoPDCAdapter pdcListarServicoComposto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolEmiAviMovtoFavorecido.
     */
    private IIncluirSolEmiAviMovtoFavorecidoPDCAdapter pdcIncluirSolEmiAviMovtoFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicitacaoRastreamento.
     */
    private IIncluirSolicitacaoRastreamentoPDCAdapter pdcIncluirSolicitacaoRastreamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolBaseSistema.
     */
    private IIncluirSolBaseSistemaPDCAdapter pdcIncluirSolBaseSistema;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolBaseFavorecido.
     */
    private IIncluirSolBaseFavorecidoPDCAdapter pdcIncluirSolBaseFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarAgenciaOpeTarifaPadrao.
     */
    private IConsultarAgenciaOpeTarifaPadraoPDCAdapter pdcConsultarAgenciaOpeTarifaPadrao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolBasePagto.
     */
    private IIncluirSolBasePagtoPDCAdapter pdcIncluirSolBasePagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolBaseRetransmissao.
     */
    private IIncluirSolBaseRetransmissaoPDCAdapter pdcIncluirSolBaseRetransmissao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarArqRetransmissao.
     */
    private IListarArqRetransmissaoPDCAdapter pdcListarArqRetransmissao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolEmiAviMovtoCliePagador.
     */
    private IIncluirSolEmiAviMovtoCliePagadorPDCAdapter pdcIncluirSolEmiAviMovtoCliePagador;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarOperacaoContaDebTarifa.
     */
    private IListarOperacaoContaDebTarifaPDCAdapter pdcListarOperacaoContaDebTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistoricoDebTarifa.
     */
    private IListarHistoricoDebTarifaPDCAdapter pdcListarHistoricoDebTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContaDebTarifa.
     */
    private IListarContaDebTarifaPDCAdapter pdcListarContaDebTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistoricoDebTarifa.
     */
    private IDetalharHistoricoDebTarifaPDCAdapter pdcDetalharHistoricoDebTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarContaDebTarifa.
     */
    private IConsultarContaDebTarifaPDCAdapter pdcConsultarContaDebTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicExclusaoLotePgto.
     */
    private IDetalharSolicExclusaoLotePgtoPDCAdapter pdcDetalharSolicExclusaoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicExclusaoLotePgto.
     */
    private IExcluirSolicExclusaoLotePgtoPDCAdapter pdcExcluirSolicExclusaoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicExclusaoLotePgto.
     */
    private IIncluirSolicExclusaoLotePgtoPDCAdapter pdcIncluirSolicExclusaoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarOcorrenciasSolicLote.
     */
    private IConsultarOcorrenciasSolicLotePDCAdapter pdcConsultarOcorrenciasSolicLote;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarQtdeValorPgtoPrevistoSolic.
     */
    private IConsultarQtdeValorPgtoPrevistoSolicPDCAdapter pdcConsultarQtdeValorPgtoPrevistoSolic;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirContaDebTarifa.
     */
    private IIncluirContaDebTarifaPDCAdapter pdcIncluirContaDebTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicLiberacaoLotePgto.
     */
    private IIncluirSolicLiberacaoLotePgtoPDCAdapter pdcIncluirSolicLiberacaoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicLiberacaoLotePgto.
     */
    private IDetalharSolicLiberacaoLotePgtoPDCAdapter pdcDetalharSolicLiberacaoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicLiberacaoLotePgto.
     */
    private IExcluirSolicLiberacaoLotePgtoPDCAdapter pdcExcluirSolicLiberacaoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicCancelamentoLotePgto.
     */
    private IIncluirSolicCancelamentoLotePgtoPDCAdapter pdcIncluirSolicCancelamentoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicCancelamentoLotePgto.
     */
    private IDetalharSolicCancelamentoLotePgtoPDCAdapter pdcDetalharSolicCancelamentoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicCancelamentoLotePgto.
     */
    private IExcluirSolicCancelamentoLotePgtoPDCAdapter pdcExcluirSolicCancelamentoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContasPossiveisInclusao.
     */
    private IListarContasPossiveisInclusaoPDCAdapter pdcListarContasPossiveisInclusao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirContratoPgit.
     */
    private IIncluirContratoPgitPDCAdapter pdcIncluirContratoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagamentosConsolidados.
     */
    private IConsultarPagamentosConsolidadosPDCAdapter pdcConsultarPagamentosConsolidados;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarConfContaContrato.
     */
    private IAlterarConfContaContratoPDCAdapter pdcAlterarConfContaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLimiteIntraPgit.
     */
    private IConsultarLimiteIntraPgitPDCAdapter pdcConsultarLimiteIntraPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarContrato.
     */
    private IConsultarContratoPDCAdapter pdcConsultarContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicitacaoMudancaAmbPartc.
     */
    private IConsultarSolicitacaoMudancaAmbPartcPDCAdapter pdcConsultarSolicitacaoMudancaAmbPartc;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAmbiente.
     */
    private IListarAmbientePDCAdapter pdcListarAmbiente;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoTituloBradesco.
     */
    private IDetalharPagtoTituloBradescoPDCAdapter pdcDetalharPagtoTituloBradesco;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoTituloOutrosBancos.
     */
    private IDetalharPagtoTituloOutrosBancosPDCAdapter pdcDetalharPagtoTituloOutrosBancos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoDARF.
     */
    private IDetalharPagtoDARFPDCAdapter pdcDetalharPagtoDARF;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoGARE.
     */
    private IDetalharPagtoGAREPDCAdapter pdcDetalharPagtoGARE;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoGPS.
     */
    private IDetalharPagtoGPSPDCAdapter pdcDetalharPagtoGPS;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoCodigoBarras.
     */
    private IDetalharPagtoCodigoBarrasPDCAdapter pdcDetalharPagtoCodigoBarras;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoOrdemPagto.
     */
    private IDetalharManPagtoOrdemPagtoPDCAdapter pdcDetalharManPagtoOrdemPagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoTituloBradesco.
     */
    private IDetalharManPagtoTituloBradescoPDCAdapter pdcDetalharManPagtoTituloBradesco;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoTituloOutrosBancos.
     */
    private IDetalharManPagtoTituloOutrosBancosPDCAdapter pdcDetalharManPagtoTituloOutrosBancos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoDARF.
     */
    private IDetalharManPagtoDARFPDCAdapter pdcDetalharManPagtoDARF;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoGARE.
     */
    private IDetalharManPagtoGAREPDCAdapter pdcDetalharManPagtoGARE;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoGPS.
     */
    private IDetalharManPagtoGPSPDCAdapter pdcDetalharManPagtoGPS;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoCodigoBarras.
     */
    private IDetalharManPagtoCodigoBarrasPDCAdapter pdcDetalharManPagtoCodigoBarras;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicAntProcLotePgtoAgda.
     */
    private IDetalharSolicAntProcLotePgtoAgdaPDCAdapter pdcDetalharSolicAntProcLotePgtoAgda;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicAntProcLotePgtoAgda.
     */
    private IExcluirSolicAntProcLotePgtoAgdaPDCAdapter pdcExcluirSolicAntProcLotePgtoAgda;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicAntProcAgda.
     */
    private IIncluirSolicAntProcAgdaPDCAdapter pdcIncluirSolicAntProcAgda;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirSolicAntPosDtPgto.
     */
    private IExcluirSolicAntPosDtPgtoPDCAdapter pdcExcluirSolicAntPosDtPgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharServicoRelacionado.
     */
    private IDetalharServicoRelacionadoPDCAdapter pdcDetalharServicoRelacionado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarServicoRelacionado.
     */
    private IAlterarServicoRelacionadoPDCAdapter pdcAlterarServicoRelacionado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirServicoRelacionado.
     */
    private IIncluirServicoRelacionadoPDCAdapter pdcIncluirServicoRelacionado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarUltimoNumeroRemessa.
     */
    private IAlterarUltimoNumeroRemessaPDCAdapter pdcAlterarUltimoNumeroRemessa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarUltimoNumeroRemessa.
     */
    private IConsultarUltimoNumeroRemessaPDCAdapter pdcConsultarUltimoNumeroRemessa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarUltimoNumeroRemessa.
     */
    private IListarUltimoNumeroRemessaPDCAdapter pdcListarUltimoNumeroRemessa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarMunicipio.
     */
    private IListarMunicipioPDCAdapter pdcListarMunicipio;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarUltimoNumeroRetorno.
     */
    private IAlterarUltimoNumeroRetornoPDCAdapter pdcAlterarUltimoNumeroRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarUltimoNumeroRetorno.
     */
    private IConsultarUltimoNumeroRetornoPDCAdapter pdcConsultarUltimoNumeroRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarUltimoNumeroRetorno.
     */
    private IListarUltimoNumeroRetornoPDCAdapter pdcListarUltimoNumeroRetorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicAntProcLotePgtoAgda.
     */
    private IConsultarSolicAntProcLotePgtoAgdaPDCAdapter pdcConsultarSolicAntProcLotePgtoAgda;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicExclusaoLotePgto.
     */
    private IConsultarSolicExclusaoLotePgtoPDCAdapter pdcConsultarSolicExclusaoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicAntPosDtPgto.
     */
    private IConsultarSolicAntPosDtPgtoPDCAdapter pdcConsultarSolicAntPosDtPgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicCancelamentoLotePgto.
     */
    private IConsultarSolicCancelamentoLotePgtoPDCAdapter pdcConsultarSolicCancelamentoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLotesIncSolic.
     */
    private IConsultarLotesIncSolicPDCAdapter pdcConsultarLotesIncSolic;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirSolicAntPosDtPgto.
     */
    private IIncluirSolicAntPosDtPgtoPDCAdapter pdcIncluirSolicAntPosDtPgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicLiberacaoLotePgto.
     */
    private IConsultarSolicLiberacaoLotePgtoPDCAdapter pdcConsultarSolicLiberacaoLotePgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharSolicAntPosDtPgto.
     */
    private IDetalharSolicAntPosDtPgtoPDCAdapter pdcDetalharSolicAntPosDtPgto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAssVersaoLayoutLoteServico.
     */
    private IIncluirAssVersaoLayoutLoteServicoPDCAdapter pdcIncluirAssVersaoLayoutLoteServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirAssVersaoLayoutLoteServico.
     */
    private IExcluirAssVersaoLayoutLoteServicoPDCAdapter pdcExcluirAssVersaoLayoutLoteServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarAssVersaoLayoutLoteServico.
     */
    private IConsultarAssVersaoLayoutLoteServicoPDCAdapter pdcConsultarAssVersaoLayoutLoteServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarSolicitacaoEstornoPagtos.
     */
    private IConsultarSolicitacaoEstornoPagtosPDCAdapter pdcConsultarSolicitacaoEstornoPagtos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarNumeroVersaoLayout.
     */
    private IListarNumeroVersaoLayoutPDCAdapter pdcListarNumeroVersaoLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirLayout.
     */
    private IExcluirLayoutPDCAdapter pdcExcluirLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConfigPadraoAtribTipoServMod.
     */
    private IConsultarConfigPadraoAtribTipoServModPDCAdapter pdcConsultarConfigPadraoAtribTipoServMod;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador EfetuarValidacaoConfigAtribModalidade.
     */
    private IEfetuarValidacaoConfigAtribModalidadePDCAdapter pdcEfetuarValidacaoConfigAtribModalidade;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosEstornoOP.
     */
    private IConsultarPagtosEstornoOPPDCAdapter pdcConsultarPagtosEstornoOP;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarPropostaAndamento.
     */
    private IValidarPropostaAndamentoPDCAdapter pdcValidarPropostaAndamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConfigTipoServicoModalidade.
     */
    private IConsultarConfigTipoServicoModalidadePDCAdapter pdcConsultarConfigTipoServicoModalidade;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarAssVersaoLayoutLoteServico.
     */
    private IListarAssVersaoLayoutLoteServicoPDCAdapter pdcListarAssVersaoLayoutLoteServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirLayout.
     */
    private IIncluirLayoutPDCAdapter pdcIncluirLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaLayoutArquivoContrato.
     */
    private IConsultarListaLayoutArquivoContratoPDCAdapter pdcConsultarListaLayoutArquivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosEstorno.
     */
    private IConsultarPagtosEstornoPDCAdapter pdcConsultarPagtosEstorno;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarModalidadeTipoServico.
     */
    private IListarModalidadeTipoServicoPDCAdapter pdcListarModalidadeTipoServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoRelacionadoCdps.
     */
    private IListarServicoRelacionadoCdpsPDCAdapter pdcListarServicoRelacionadoCdps;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador EstornarPagamentos.
     */
    private IEstornarPagamentosPDCAdapter pdcEstornarPagamentos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarPerfilContrato.
     */
    private IAlterarPerfilContratoPDCAdapter pdcAlterarPerfilContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirHorarioLimite.
     */
    private IIncluirHorarioLimitePDCAdapter pdcIncluirHorarioLimite;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharListaDebito.
     */
    private IDetalharListaDebitoPDCAdapter pdcDetalharListaDebito;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharFolha.
     */
    private IDetalharFolhaPDCAdapter pdcDetalharFolha;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoRelacionadoCdpsIncluir.
     */
    private IListarServicoRelacionadoCdpsIncluirPDCAdapter pdcListarServicoRelacionadoCdpsIncluir;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirRepresentante.
     */
    private IIncluirRepresentantePDCAdapter pdcIncluirRepresentante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarUsuario.
     */
    private IValidarUsuarioPDCAdapter pdcValidarUsuario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagamentosConsolidados.
     */
    private IDetalharPagamentosConsolidadosPDCAdapter pdcDetalharPagamentosConsolidados;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagamentoFavorecido.
     */
    private IConsultarPagamentoFavorecidoPDCAdapter pdcConsultarPagamentoFavorecido;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagamentosIndividuais.
     */
    private IConsultarPagamentosIndividuaisPDCAdapter pdcConsultarPagamentosIndividuais;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarVinculacaoConvenioContaSalario.
     */
    private IValidarVinculacaoConvenioContaSalarioPDCAdapter pdcValidarVinculacaoConvenioContaSalario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoLayoutArquivo.
     */
    private IListarTipoLayoutArquivoPDCAdapter pdcListarTipoLayoutArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConManLayout.
     */
    private IConsultarConManLayoutPDCAdapter pdcConsultarConManLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPiramide.
     */
    private IDetalharPiramidePDCAdapter pdcDetalharPiramide;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirPerfilTrocaArquivo.
     */
    private IExcluirPerfilTrocaArquivoPDCAdapter pdcExcluirPerfilTrocaArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirLayoutArqServico.
     */
    private IIncluirLayoutArqServicoPDCAdapter pdcIncluirLayoutArqServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharLayoutArqServico.
     */
    private IDetalharLayoutArqServicoPDCAdapter pdcDetalharLayoutArqServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarAgencia.
     */
    private IValidarAgenciaPDCAdapter pdcValidarAgencia;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagotsVencNaoPagos.
     */
    private IConsultarPagotsVencNaoPagosPDCAdapter pdcConsultarPagotsVencNaoPagos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLibPagtosIndSemConsSaldo.
     */
    private IConsultarLibPagtosIndSemConsSaldoPDCAdapter pdcConsultarLibPagtosIndSemConsSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtoPendIndividual.
     */
    private IConsultarPagtoPendIndividualPDCAdapter pdcConsultarPagtoPendIndividual;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosIndAutorizados.
     */
    private IConsultarPagtosIndAutorizadosPDCAdapter pdcConsultarPagtosIndAutorizados;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagamentos.
     */
    private IDetalharPagamentosPDCAdapter pdcDetalharPagamentos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosIndParaCancelamento.
     */
    private IConsultarPagtosIndParaCancelamentoPDCAdapter pdcConsultarPagtosIndParaCancelamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarCanLibPagtosIndSemConsSaldo.
     */
    private IConsultarCanLibPagtosIndSemConsSaldoPDCAdapter pdcConsultarCanLibPagtosIndSemConsSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtoIndPendAutorizacao.
     */
    private IConsultarPagtoIndPendAutorizacaoPDCAdapter pdcConsultarPagtoIndPendAutorizacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarPagtosIndAntPostergacao.
     */
    private IConsultarPagtosIndAntPostergacaoPDCAdapter pdcConsultarPagtosIndAntPostergacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarRepresentante.
     */
    private IListarRepresentantePDCAdapter pdcListarRepresentante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarArquivoServico.
     */
    private IAlterarArquivoServicoPDCAdapter pdcAlterarArquivoServico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoCreditoConta.
     */
    private IDetalharPagtoCreditoContaPDCAdapter pdcDetalharPagtoCreditoConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador VerificarAtributosServicoModalidade.
     */
    private IVerificarAtributosServicoModalidadePDCAdapter pdcVerificarAtributosServicoModalidade;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarListaPerfilTrocaArquivoLoop.
     */
    private IConsultarListaPerfilTrocaArquivoLoopPDCAdapter pdcConsultarListaPerfilTrocaArquivoLoop;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoProcessamentoLayoutArquivo.
     */
    private IListarTipoProcessamentoLayoutArquivoPDCAdapter pdcListarTipoProcessamentoLayoutArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarTarifaContratoLoop.
     */
    private IConsultarTarifaContratoLoopPDCAdapter pdcConsultarTarifaContratoLoop;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarConfiguracaoLayoutArqContrato.
     */
    private IAlterarConfiguracaoLayoutArqContratoPDCAdapter pdcAlterarConfiguracaoLayoutArqContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLayoutArquivoContrato.
     */
    private IConsultarLayoutArquivoContratoPDCAdapter pdcConsultarLayoutArquivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ValidarQtdDiasCobrancaApsApuracao.
     */
    private IValidarQtdDiasCobrancaApsApuracaoPDCAdapter pdcValidarQtdDiasCobrancaApsApuracao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarFaixaSalarialProposta.
     */
    private IListarFaixaSalarialPropostaPDCAdapter pdcListarFaixaSalarialProposta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoRelacionadoPgitCdps.
     */
    private IListarServicoRelacionadoPgitCdpsPDCAdapter pdcListarServicoRelacionadoPgitCdps;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarServicoRelacionadoPgit.
     */
    private IListarServicoRelacionadoPgitPDCAdapter pdcListarServicoRelacionadoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirAnexoSegundoContrato.
     */
    private IImprimirAnexoSegundoContratoPDCAdapter pdcImprimirAnexoSegundoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharVincMsgLancOper.
     */
    private IDetalharVincMsgLancOperPDCAdapter pdcDetalharVincMsgLancOper;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTiposPagto.
     */
    private IListarTiposPagtoPDCAdapter pdcListarTiposPagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirVincMsgLancOper.
     */
    private IIncluirVincMsgLancOperPDCAdapter pdcIncluirVincMsgLancOper;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContratoMae.
     */
    private IListarContratoMaePDCAdapter pdcListarContratoMae;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirVincTrocaArqParticipante.
     */
    private IIncluirVincTrocaArqParticipantePDCAdapter pdcIncluirVincTrocaArqParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirVincTrocaArqParticipante.
     */
    private IExcluirVincTrocaArqParticipantePDCAdapter pdcExcluirVincTrocaArqParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharVincTrocaArqParticipante.
     */
    private IDetalharVincTrocaArqParticipantePDCAdapter pdcDetalharVincTrocaArqParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistAssocTrocaArqParticipante.
     */
    private IDetalharHistAssocTrocaArqParticipantePDCAdapter pdcDetalharHistAssocTrocaArqParticipante;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarPerfilTrocaArquivo.
     */
    private IAlterarPerfilTrocaArquivoPDCAdapter pdcAlterarPerfilTrocaArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirPerfilTrocaArquivo.
     */
    private IIncluirPerfilTrocaArquivoPDCAdapter pdcIncluirPerfilTrocaArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistoricoPerfilTrocaArquivo.
     */
    private IDetalharHistoricoPerfilTrocaArquivoPDCAdapter pdcDetalharHistoricoPerfilTrocaArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharContratoMigradoHsbc.
     */
    private IDetalharContratoMigradoHsbcPDCAdapter pdcDetalharContratoMigradoHsbc;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParametroContrato.
     */
    private IListarParametroContratoPDCAdapter pdcListarParametroContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParametros.
     */
    private IListarParametrosPDCAdapter pdcListarParametros;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirParametroContrato.
     */
    private IExcluirParametroContratoPDCAdapter pdcExcluirParametroContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirParametroContrato.
     */
    private IIncluirParametroContratoPDCAdapter pdcIncluirParametroContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirVincContratoMaeContratoFilho.
     */
    private IIncluirVincContratoMaeContratoFilhoPDCAdapter pdcIncluirVincContratoMaeContratoFilho;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharTipoRetornoLayout.
     */
    private IDetalharTipoRetornoLayoutPDCAdapter pdcDetalharTipoRetornoLayout;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistoricoLayoutContrato.
     */
    private IDetalharHistoricoLayoutContratoPDCAdapter pdcDetalharHistoricoLayoutContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLimiteConta.
     */
    private IConsultarLimiteContaPDCAdapter pdcConsultarLimiteConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarLimiteHistorico.
     */
    private IConsultarLimiteHistoricoPDCAdapter pdcConsultarLimiteHistorico;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPerfilTrocaArquivo.
     */
    private IDetalharPerfilTrocaArquivoPDCAdapter pdcDetalharPerfilTrocaArquivo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarLayoutsContrato.
     */
    private IListarLayoutsContratoPDCAdapter pdcListarLayoutsContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarManutencaoConfigContrato.
     */
    private IConsultarManutencaoConfigContratoPDCAdapter pdcConsultarManutencaoConfigContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarManutencaoServicoContrato.
     */
    private IConsultarManutencaoServicoContratoPDCAdapter pdcConsultarManutencaoServicoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirContratoIntranet.
     */
    private IImprimirContratoIntranetPDCAdapter pdcImprimirContratoIntranet;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarComprovanteSalarialIntranet.
     */
    private IListarComprovanteSalarialIntranetPDCAdapter pdcListarComprovanteSalarialIntranet;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirAditivoContrato.
     */
    private IIncluirAditivoContratoPDCAdapter pdcIncluirAditivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharAditivoContrato.
     */
    private IDetalharAditivoContratoPDCAdapter pdcDetalharAditivoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarAutorizantesNetEmpresa.
     */
    private IConsultarAutorizantesNetEmpresaPDCAdapter pdcConsultarAutorizantesNetEmpresa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarServicosCatalogo.
     */
    private IConsultarServicosCatalogoPDCAdapter pdcConsultarServicosCatalogo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarAutenticacao.
     */
    private IConsultarAutenticacaoPDCAdapter pdcConsultarAutenticacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDadosBasicoContrato.
     */
    private IConsultarDadosBasicoContratoPDCAdapter pdcConsultarDadosBasicoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarDadosBasicoContrato.
     */
    private IAlterarDadosBasicoContratoPDCAdapter pdcAlterarDadosBasicoContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConManDadosBasicos.
     */
    private IConsultarConManDadosBasicosPDCAdapter pdcConsultarConManDadosBasicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador VerificarAtributosDadosBasicos.
     */
    private IVerificarAtributosDadosBasicosPDCAdapter pdcVerificarAtributosDadosBasicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConContratoMigrado.
     */
    private IConContratoMigradoPDCAdapter pdcConContratoMigrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConRelacaoConManutencoes.
     */
    private IConRelacaoConManutencoesPDCAdapter pdcConRelacaoConManutencoes;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirComprovanteCodigoBarra.
     */
    private IImprimirComprovanteCodigoBarraPDCAdapter pdcImprimirComprovanteCodigoBarra;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarDetalhesSolicitacao.
     */
    private IConsultarDetalhesSolicitacaoPDCAdapter pdcConsultarDetalhesSolicitacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParticipantesIntranet.
     */
    private IListarParticipantesIntranetPDCAdapter pdcListarParticipantesIntranet;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarLiquidacaoPagamento.
     */
    private IListarLiquidacaoPagamentoPDCAdapter pdcListarLiquidacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharLiquidacaoPagamento.
     */
    private IDetalharLiquidacaoPagamentoPDCAdapter pdcDetalharLiquidacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarLiquidacaoPagamento.
     */
    private IAlterarLiquidacaoPagamentoPDCAdapter pdcAlterarLiquidacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirLiquidacaoPagamento.
     */
    private IIncluirLiquidacaoPagamentoPDCAdapter pdcIncluirLiquidacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistLiquidacaoPagamento.
     */
    private IDetalharHistLiquidacaoPagamentoPDCAdapter pdcDetalharHistLiquidacaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoTED.
     */
    private IDetalharPagtoTEDPDCAdapter pdcDetalharPagtoTED;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoDOC.
     */
    private IDetalharManPagtoDOCPDCAdapter pdcDetalharManPagtoDOC;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoDOC.
     */
    private IDetalharPagtoDOCPDCAdapter pdcDetalharPagtoDOC;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharManPagtoTED.
     */
    private IDetalharManPagtoTEDPDCAdapter pdcDetalharManPagtoTED;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarHistConsultaSaldo.
     */
    private IConsultarHistConsultaSaldoPDCAdapter pdcConsultarHistConsultaSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistConsultaSaldo.
     */
    private IDetalharHistConsultaSaldoPDCAdapter pdcDetalharHistConsultaSaldo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarImprimirPgtoIndividual.
     */
    private IConsultarImprimirPgtoIndividualPDCAdapter pdcConsultarImprimirPgtoIndividual;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirPagamentosIndividuais.
     */
    private IImprimirPagamentosIndividuaisPDCAdapter pdcImprimirPagamentosIndividuais;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarImprimirPgtoConsolidado.
     */
    private IConsultarImprimirPgtoConsolidadoPDCAdapter pdcConsultarImprimirPgtoConsolidado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirPagamentosConsolidados.
     */
    private IImprimirPagamentosConsolidadosPDCAdapter pdcImprimirPagamentosConsolidados;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarManutencaoPagamento.
     */
    private IConsultarManutencaoPagamentoPDCAdapter pdcConsultarManutencaoPagamento;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultaPostergarConsolidadoSolicitacao.
     */
    private IConsultaPostergarConsolidadoSolicitacaoPDCAdapter pdcConsultaPostergarConsolidadoSolicitacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalhePostergarConsolidadoSolicitacao.
     */
    private IDetalhePostergarConsolidadoSolicitacaoPDCAdapter pdcDetalhePostergarConsolidadoSolicitacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarPostergarConsolidadoSolicitacao.
     */
    private IListarPostergarConsolidadoSolicitacaoPDCAdapter pdcListarPostergarConsolidadoSolicitacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirPostergarConsolidadoSolicitacao.
     */
    private IExcluirPostergarConsolidadoSolicitacaoPDCAdapter pdcExcluirPostergarConsolidadoSolicitacao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarParticipantesLoop.
     */
    private IListarParticipantesLoopPDCAdapter pdcListarParticipantesLoop;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarTipoRetirada.
     */
    private IListarTipoRetiradaPDCAdapter pdcListarTipoRetirada;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarOrdemPagtoPorAgePagadora.
     */
    private IConsultarOrdemPagtoPorAgePagadoraPDCAdapter pdcConsultarOrdemPagtoPorAgePagadora;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirOperacaoServicoPagtoIntegrado.
     */
    private IIncluirOperacaoServicoPagtoIntegradoPDCAdapter pdcIncluirOperacaoServicoPagtoIntegrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharOperacaoServicoPagtoIntegrado.
     */
    private IDetalharOperacaoServicoPagtoIntegradoPDCAdapter pdcDetalharOperacaoServicoPagtoIntegrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarOperacaoServicoPagtoIntegrado.
     */
    private IAlterarOperacaoServicoPagtoIntegradoPDCAdapter pdcAlterarOperacaoServicoPagtoIntegrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarTarifaCatalogo.
     */
    private IConsultarTarifaCatalogoPDCAdapter pdcConsultarTarifaCatalogo;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHistoricoOperacaoServicoPagtoIntegrado.
     */
    private IDetalharHistoricoOperacaoServicoPagtoIntegradoPDCAdapter pdcDetalharHistoricoOperacaoServicoPagtoIntegrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHistoricoOperacaoServicoPagtoIntegrado.
     */
    private IListarHistoricoOperacaoServicoPagtoIntegradoPDCAdapter pdcListarHistoricoOperacaoServicoPagtoIntegrado;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirParticipanteContratoPgit.
     */
    private IIncluirParticipanteContratoPgitPDCAdapter pdcIncluirParticipanteContratoPgit;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContasExclusao.
     */
    private IListarContasExclusaoPDCAdapter pdcListarContasExclusao;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirContaContrato.
     */
    private IExcluirContaContratoPDCAdapter pdcExcluirContaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContaComplementar.
     */
    private IListarContaComplementarPDCAdapter pdcListarContaComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarParticipantesContrato.
     */
    private IConsultarParticipantesContratoPDCAdapter pdcConsultarParticipantesContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirContaComplementar.
     */
    private IIncluirContaComplementarPDCAdapter pdcIncluirContaComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirContaComplementar.
     */
    private IExcluirContaComplementarPDCAdapter pdcExcluirContaComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharContaComplementar.
     */
    private IDetalharContaComplementarPDCAdapter pdcDetalharContaComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarContaComplementar.
     */
    private IAlterarContaComplementarPDCAdapter pdcAlterarContaComplementar;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContasRelacionadasConta.
     */
    private IListarContasRelacionadasContaPDCAdapter pdcListarContasRelacionadasConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContasRelacionadasParaContrato.
     */
    private IListarContasRelacionadasParaContratoPDCAdapter pdcListarContasRelacionadasParaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirRelaciAssoc.
     */
    private IExcluirRelaciAssocPDCAdapter pdcExcluirRelaciAssoc;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharPagtoOrdemPagto.
     */
    private IDetalharPagtoOrdemPagtoPDCAdapter pdcDetalharPagtoOrdemPagto;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirRelacContaContrato.
     */
    private IIncluirRelacContaContratoPDCAdapter pdcIncluirRelacContaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador SubstituirRelacContaContrato.
     */
    private ISubstituirRelacContaContratoPDCAdapter pdcSubstituirRelacContaContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContasVinculadasContrato.
     */
    private IListarContasVinculadasContratoPDCAdapter pdcListarContasVinculadasContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContasVinculadasContratoLoop.
     */
    private IListarContasVinculadasContratoLoopPDCAdapter pdcListarContasVinculadasContratoLoop;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarContasRelacionadasHist.
     */
    private IListarContasRelacionadasHistPDCAdapter pdcListarContasRelacionadasHist;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharContaRelacionadaHist.
     */
    private IDetalharContaRelacionadaHistPDCAdapter pdcDetalharContaRelacionadaHist;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador IncluirPagamentoSalario.
     */
    private IIncluirPagamentoSalarioPDCAdapter pdcIncluirPagamentoSalario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharTipoServModContrato.
     */
    private IDetalharTipoServModContratoPDCAdapter pdcDetalharTipoServModContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConManServicos.
     */
    private IConsultarConManServicosPDCAdapter pdcConsultarConManServicos;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarConfiguracaoTipoServModContrato.
     */
    private IAlterarConfiguracaoTipoServModContratoPDCAdapter pdcAlterarConfiguracaoTipoServModContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador AlterarHorarioLimite.
     */
    private IAlterarHorarioLimitePDCAdapter pdcAlterarHorarioLimite;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarHorarioLimite.
     */
    private IListarHorarioLimitePDCAdapter pdcListarHorarioLimite;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador DetalharHorarioLimite.
     */
    private IDetalharHorarioLimitePDCAdapter pdcDetalharHorarioLimite;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ExcluirHorarioLimite.
     */
    private IExcluirHorarioLimitePDCAdapter pdcExcluirHorarioLimite;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarVincConvnCtaSalario.
     */
    private IListarVincConvnCtaSalarioPDCAdapter pdcListarVincConvnCtaSalario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConvenioContaSalario.
     */
    private IListarConvenioContaSalarioPDCAdapter pdcListarConvenioContaSalario;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarDadosCtaConvn.
     */
    private IListarDadosCtaConvnPDCAdapter pdcListarDadosCtaConvn;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirAnexoPrimeiroContrato.
     */
    private IImprimirAnexoPrimeiroContratoPDCAdapter pdcImprimirAnexoPrimeiroContrato;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ImprimirAnexoPrimeiroContratoLoop.
     */
    private IImprimirAnexoPrimeiroContratoLoopPDCAdapter pdcImprimirAnexoPrimeiroContratoLoop;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConfCestaTarifa.
     */
    private IConfCestaTarifaPDCAdapter pdcConfCestaTarifa;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ConsultarConManConta.
     */
    private IConsultarConManContaPDCAdapter pdcConsultarConManConta;
    /**
     * Vari�vel que cont�m uma inst�ncia do adaptador ListarConvenioContaSalarioNovo.
     */
    private IListarConvenioContaSalarioNovoPDCAdapter pdcListarConvenioContaSalarioNovo;

    /**
     * M�todo invocado para obter um adaptador AuthenticationService.
     * 
     * @return Adaptador AuthenticationService
     */
    public IAuthenticationServicePDCAdapter getAuthenticationServicePDCAdapter() {
        return pdcAuthenticationService;
    }

    /**
     * M�todo invocado para establecer um adaptador AuthenticationService.
     * 
     * @param pdcAuthenticationService
     *            Adaptador AuthenticationService
     */
    public void setAuthenticationServicePDCAdapter(IAuthenticationServicePDCAdapter pdcAuthenticationService) {
        this.pdcAuthenticationService = pdcAuthenticationService;
    }
    /**
     * M�todo invocado para obter um adaptador BloqueioDesbloqueioProcessoMassivo.
     * 
     * @return Adaptador BloqueioDesbloqueioProcessoMassivo
     */
    public IBloqueioDesbloqueioProcessoMassivoPDCAdapter getBloqueioDesbloqueioProcessoMassivoPDCAdapter() {
        return pdcBloqueioDesbloqueioProcessoMassivo;
    }

    /**
     * M�todo invocado para establecer um adaptador BloqueioDesbloqueioProcessoMassivo.
     * 
     * @param pdcBloqueioDesbloqueioProcessoMassivo
     *            Adaptador BloqueioDesbloqueioProcessoMassivo
     */
    public void setBloqueioDesbloqueioProcessoMassivoPDCAdapter(IBloqueioDesbloqueioProcessoMassivoPDCAdapter pdcBloqueioDesbloqueioProcessoMassivo) {
        this.pdcBloqueioDesbloqueioProcessoMassivo = pdcBloqueioDesbloqueioProcessoMassivo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarMotivoBloqueioConta.
     * 
     * @return Adaptador ConsultarMotivoBloqueioConta
     */
    public IConsultarMotivoBloqueioContaPDCAdapter getConsultarMotivoBloqueioContaPDCAdapter() {
        return pdcConsultarMotivoBloqueioConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarMotivoBloqueioConta.
     * 
     * @param pdcConsultarMotivoBloqueioConta
     *            Adaptador ConsultarMotivoBloqueioConta
     */
    public void setConsultarMotivoBloqueioContaPDCAdapter(IConsultarMotivoBloqueioContaPDCAdapter pdcConsultarMotivoBloqueioConta) {
        this.pdcConsultarMotivoBloqueioConta = pdcConsultarMotivoBloqueioConta;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMotivoBloqueioFavorecido.
     * 
     * @return Adaptador ListarMotivoBloqueioFavorecido
     */
    public IListarMotivoBloqueioFavorecidoPDCAdapter getListarMotivoBloqueioFavorecidoPDCAdapter() {
        return pdcListarMotivoBloqueioFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMotivoBloqueioFavorecido.
     * 
     * @param pdcListarMotivoBloqueioFavorecido
     *            Adaptador ListarMotivoBloqueioFavorecido
     */
    public void setListarMotivoBloqueioFavorecidoPDCAdapter(IListarMotivoBloqueioFavorecidoPDCAdapter pdcListarMotivoBloqueioFavorecido) {
        this.pdcListarMotivoBloqueioFavorecido = pdcListarMotivoBloqueioFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicitacaoRelatorioFavorecidos.
     * 
     * @return Adaptador ExcluirSolicitacaoRelatorioFavorecidos
     */
    public IExcluirSolicitacaoRelatorioFavorecidosPDCAdapter getExcluirSolicitacaoRelatorioFavorecidosPDCAdapter() {
        return pdcExcluirSolicitacaoRelatorioFavorecidos;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicitacaoRelatorioFavorecidos.
     * 
     * @param pdcExcluirSolicitacaoRelatorioFavorecidos
     *            Adaptador ExcluirSolicitacaoRelatorioFavorecidos
     */
    public void setExcluirSolicitacaoRelatorioFavorecidosPDCAdapter(IExcluirSolicitacaoRelatorioFavorecidosPDCAdapter pdcExcluirSolicitacaoRelatorioFavorecidos) {
        this.pdcExcluirSolicitacaoRelatorioFavorecidos = pdcExcluirSolicitacaoRelatorioFavorecidos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSolicitacaoRelatorioFavorecidos.
     * 
     * @return Adaptador ListarSolicitacaoRelatorioFavorecidos
     */
    public IListarSolicitacaoRelatorioFavorecidosPDCAdapter getListarSolicitacaoRelatorioFavorecidosPDCAdapter() {
        return pdcListarSolicitacaoRelatorioFavorecidos;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSolicitacaoRelatorioFavorecidos.
     * 
     * @param pdcListarSolicitacaoRelatorioFavorecidos
     *            Adaptador ListarSolicitacaoRelatorioFavorecidos
     */
    public void setListarSolicitacaoRelatorioFavorecidosPDCAdapter(IListarSolicitacaoRelatorioFavorecidosPDCAdapter pdcListarSolicitacaoRelatorioFavorecidos) {
        this.pdcListarSolicitacaoRelatorioFavorecidos = pdcListarSolicitacaoRelatorioFavorecidos;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaSolicitacaoRastreamento.
     * 
     * @return Adaptador ConsultarListaSolicitacaoRastreamento
     */
    public IConsultarListaSolicitacaoRastreamentoPDCAdapter getConsultarListaSolicitacaoRastreamentoPDCAdapter() {
        return pdcConsultarListaSolicitacaoRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaSolicitacaoRastreamento.
     * 
     * @param pdcConsultarListaSolicitacaoRastreamento
     *            Adaptador ConsultarListaSolicitacaoRastreamento
     */
    public void setConsultarListaSolicitacaoRastreamentoPDCAdapter(IConsultarListaSolicitacaoRastreamentoPDCAdapter pdcConsultarListaSolicitacaoRastreamento) {
        this.pdcConsultarListaSolicitacaoRastreamento = pdcConsultarListaSolicitacaoRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicitacaoRastreamento.
     * 
     * @return Adaptador ExcluirSolicitacaoRastreamento
     */
    public IExcluirSolicitacaoRastreamentoPDCAdapter getExcluirSolicitacaoRastreamentoPDCAdapter() {
        return pdcExcluirSolicitacaoRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicitacaoRastreamento.
     * 
     * @param pdcExcluirSolicitacaoRastreamento
     *            Adaptador ExcluirSolicitacaoRastreamento
     */
    public void setExcluirSolicitacaoRastreamentoPDCAdapter(IExcluirSolicitacaoRastreamentoPDCAdapter pdcExcluirSolicitacaoRastreamento) {
        this.pdcExcluirSolicitacaoRastreamento = pdcExcluirSolicitacaoRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador BloquearFavorecido.
     * 
     * @return Adaptador BloquearFavorecido
     */
    public IBloquearFavorecidoPDCAdapter getBloquearFavorecidoPDCAdapter() {
        return pdcBloquearFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador BloquearFavorecido.
     * 
     * @param pdcBloquearFavorecido
     *            Adaptador BloquearFavorecido
     */
    public void setBloquearFavorecidoPDCAdapter(IBloquearFavorecidoPDCAdapter pdcBloquearFavorecido) {
        this.pdcBloquearFavorecido = pdcBloquearFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ListarDependEmpresa.
     * 
     * @return Adaptador ListarDependEmpresa
     */
    public IListarDependEmpresaPDCAdapter getListarDependEmpresaPDCAdapter() {
        return pdcListarDependEmpresa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarDependEmpresa.
     * 
     * @param pdcListarDependEmpresa
     *            Adaptador ListarDependEmpresa
     */
    public void setListarDependEmpresaPDCAdapter(IListarDependEmpresaPDCAdapter pdcListarDependEmpresa) {
        this.pdcListarDependEmpresa = pdcListarDependEmpresa;
    }
    /**
     * M�todo invocado para obter um adaptador ListarEmpresaGestora.
     * 
     * @return Adaptador ListarEmpresaGestora
     */
    public IListarEmpresaGestoraPDCAdapter getListarEmpresaGestoraPDCAdapter() {
        return pdcListarEmpresaGestora;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarEmpresaGestora.
     * 
     * @param pdcListarEmpresaGestora
     *            Adaptador ListarEmpresaGestora
     */
    public void setListarEmpresaGestoraPDCAdapter(IListarEmpresaGestoraPDCAdapter pdcListarEmpresaGestora) {
        this.pdcListarEmpresaGestora = pdcListarEmpresaGestora;
    }
    /**
     * M�todo invocado para obter um adaptador ListarFinalidadeConta.
     * 
     * @return Adaptador ListarFinalidadeConta
     */
    public IListarFinalidadeContaPDCAdapter getListarFinalidadeContaPDCAdapter() {
        return pdcListarFinalidadeConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarFinalidadeConta.
     * 
     * @param pdcListarFinalidadeConta
     *            Adaptador ListarFinalidadeConta
     */
    public void setListarFinalidadeContaPDCAdapter(IListarFinalidadeContaPDCAdapter pdcListarFinalidadeConta) {
        this.pdcListarFinalidadeConta = pdcListarFinalidadeConta;
    }
    /**
     * M�todo invocado para obter um adaptador ListarFormaLancCnab.
     * 
     * @return Adaptador ListarFormaLancCnab
     */
    public IListarFormaLancCnabPDCAdapter getListarFormaLancCnabPDCAdapter() {
        return pdcListarFormaLancCnab;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarFormaLancCnab.
     * 
     * @param pdcListarFormaLancCnab
     *            Adaptador ListarFormaLancCnab
     */
    public void setListarFormaLancCnabPDCAdapter(IListarFormaLancCnabPDCAdapter pdcListarFormaLancCnab) {
        this.pdcListarFormaLancCnab = pdcListarFormaLancCnab;
    }
    /**
     * M�todo invocado para obter um adaptador ListarFormaLiquidacao.
     * 
     * @return Adaptador ListarFormaLiquidacao
     */
    public IListarFormaLiquidacaoPDCAdapter getListarFormaLiquidacaoPDCAdapter() {
        return pdcListarFormaLiquidacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarFormaLiquidacao.
     * 
     * @param pdcListarFormaLiquidacao
     *            Adaptador ListarFormaLiquidacao
     */
    public void setListarFormaLiquidacaoPDCAdapter(IListarFormaLiquidacaoPDCAdapter pdcListarFormaLiquidacao) {
        this.pdcListarFormaLiquidacao = pdcListarFormaLiquidacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarIdioma.
     * 
     * @return Adaptador ListarIdioma
     */
    public IListarIdiomaPDCAdapter getListarIdiomaPDCAdapter() {
        return pdcListarIdioma;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarIdioma.
     * 
     * @param pdcListarIdioma
     *            Adaptador ListarIdioma
     */
    public void setListarIdiomaPDCAdapter(IListarIdiomaPDCAdapter pdcListarIdioma) {
        this.pdcListarIdioma = pdcListarIdioma;
    }
    /**
     * M�todo invocado para obter um adaptador ListarOperacaoContrato.
     * 
     * @return Adaptador ListarOperacaoContrato
     */
    public IListarOperacaoContratoPDCAdapter getListarOperacaoContratoPDCAdapter() {
        return pdcListarOperacaoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarOperacaoContrato.
     * 
     * @param pdcListarOperacaoContrato
     *            Adaptador ListarOperacaoContrato
     */
    public void setListarOperacaoContratoPDCAdapter(IListarOperacaoContratoPDCAdapter pdcListarOperacaoContrato) {
        this.pdcListarOperacaoContrato = pdcListarOperacaoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarPeriodicidade.
     * 
     * @return Adaptador ListarPeriodicidade
     */
    public IListarPeriodicidadePDCAdapter getListarPeriodicidadePDCAdapter() {
        return pdcListarPeriodicidade;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarPeriodicidade.
     * 
     * @param pdcListarPeriodicidade
     *            Adaptador ListarPeriodicidade
     */
    public void setListarPeriodicidadePDCAdapter(IListarPeriodicidadePDCAdapter pdcListarPeriodicidade) {
        this.pdcListarPeriodicidade = pdcListarPeriodicidade;
    }
    /**
     * M�todo invocado para obter um adaptador ListarResponsContrato.
     * 
     * @return Adaptador ListarResponsContrato
     */
    public IListarResponsContratoPDCAdapter getListarResponsContratoPDCAdapter() {
        return pdcListarResponsContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarResponsContrato.
     * 
     * @param pdcListarResponsContrato
     *            Adaptador ListarResponsContrato
     */
    public void setListarResponsContratoPDCAdapter(IListarResponsContratoPDCAdapter pdcListarResponsContrato) {
        this.pdcListarResponsContrato = pdcListarResponsContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoCnab.
     * 
     * @return Adaptador ListarServicoCnab
     */
    public IListarServicoCnabPDCAdapter getListarServicoCnabPDCAdapter() {
        return pdcListarServicoCnab;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoCnab.
     * 
     * @param pdcListarServicoCnab
     *            Adaptador ListarServicoCnab
     */
    public void setListarServicoCnabPDCAdapter(IListarServicoCnabPDCAdapter pdcListarServicoCnab) {
        this.pdcListarServicoCnab = pdcListarServicoCnab;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicosContrato.
     * 
     * @return Adaptador ListarServicosContrato
     */
    public IListarServicosContratoPDCAdapter getListarServicosContratoPDCAdapter() {
        return pdcListarServicosContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicosContrato.
     * 
     * @param pdcListarServicosContrato
     *            Adaptador ListarServicosContrato
     */
    public void setListarServicosContratoPDCAdapter(IListarServicosContratoPDCAdapter pdcListarServicosContrato) {
        this.pdcListarServicosContrato = pdcListarServicosContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSituacaoContrato.
     * 
     * @return Adaptador ListarSituacaoContrato
     */
    public IListarSituacaoContratoPDCAdapter getListarSituacaoContratoPDCAdapter() {
        return pdcListarSituacaoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSituacaoContrato.
     * 
     * @param pdcListarSituacaoContrato
     *            Adaptador ListarSituacaoContrato
     */
    public void setListarSituacaoContratoPDCAdapter(IListarSituacaoContratoPDCAdapter pdcListarSituacaoContrato) {
        this.pdcListarSituacaoContrato = pdcListarSituacaoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSituacaoPendContrato.
     * 
     * @return Adaptador ListarSituacaoPendContrato
     */
    public IListarSituacaoPendContratoPDCAdapter getListarSituacaoPendContratoPDCAdapter() {
        return pdcListarSituacaoPendContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSituacaoPendContrato.
     * 
     * @param pdcListarSituacaoPendContrato
     *            Adaptador ListarSituacaoPendContrato
     */
    public void setListarSituacaoPendContratoPDCAdapter(IListarSituacaoPendContratoPDCAdapter pdcListarSituacaoPendContrato) {
        this.pdcListarSituacaoPendContrato = pdcListarSituacaoPendContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSituacaoServicoContrato.
     * 
     * @return Adaptador ListarSituacaoServicoContrato
     */
    public IListarSituacaoServicoContratoPDCAdapter getListarSituacaoServicoContratoPDCAdapter() {
        return pdcListarSituacaoServicoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSituacaoServicoContrato.
     * 
     * @param pdcListarSituacaoServicoContrato
     *            Adaptador ListarSituacaoServicoContrato
     */
    public void setListarSituacaoServicoContratoPDCAdapter(IListarSituacaoServicoContratoPDCAdapter pdcListarSituacaoServicoContrato) {
        this.pdcListarSituacaoServicoContrato = pdcListarSituacaoServicoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSituacaoSolicitacao.
     * 
     * @return Adaptador ListarSituacaoSolicitacao
     */
    public IListarSituacaoSolicitacaoPDCAdapter getListarSituacaoSolicitacaoPDCAdapter() {
        return pdcListarSituacaoSolicitacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSituacaoSolicitacao.
     * 
     * @param pdcListarSituacaoSolicitacao
     *            Adaptador ListarSituacaoSolicitacao
     */
    public void setListarSituacaoSolicitacaoPDCAdapter(IListarSituacaoSolicitacaoPDCAdapter pdcListarSituacaoSolicitacao) {
        this.pdcListarSituacaoSolicitacao = pdcListarSituacaoSolicitacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSituacaoVincContrato.
     * 
     * @return Adaptador ListarSituacaoVincContrato
     */
    public IListarSituacaoVincContratoPDCAdapter getListarSituacaoVincContratoPDCAdapter() {
        return pdcListarSituacaoVincContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSituacaoVincContrato.
     * 
     * @param pdcListarSituacaoVincContrato
     *            Adaptador ListarSituacaoVincContrato
     */
    public void setListarSituacaoVincContratoPDCAdapter(IListarSituacaoVincContratoPDCAdapter pdcListarSituacaoVincContrato) {
        this.pdcListarSituacaoVincContrato = pdcListarSituacaoVincContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoContrato.
     * 
     * @return Adaptador ListarTipoContrato
     */
    public IListarTipoContratoPDCAdapter getListarTipoContratoPDCAdapter() {
        return pdcListarTipoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoContrato.
     * 
     * @param pdcListarTipoContrato
     *            Adaptador ListarTipoContrato
     */
    public void setListarTipoContratoPDCAdapter(IListarTipoContratoPDCAdapter pdcListarTipoContrato) {
        this.pdcListarTipoContrato = pdcListarTipoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoLote.
     * 
     * @return Adaptador ListarTipoLote
     */
    public IListarTipoLotePDCAdapter getListarTipoLotePDCAdapter() {
        return pdcListarTipoLote;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoLote.
     * 
     * @param pdcListarTipoLote
     *            Adaptador ListarTipoLote
     */
    public void setListarTipoLotePDCAdapter(IListarTipoLotePDCAdapter pdcListarTipoLote) {
        this.pdcListarTipoLote = pdcListarTipoLote;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoPendencia.
     * 
     * @return Adaptador ListarTipoPendencia
     */
    public IListarTipoPendenciaPDCAdapter getListarTipoPendenciaPDCAdapter() {
        return pdcListarTipoPendencia;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoPendencia.
     * 
     * @param pdcListarTipoPendencia
     *            Adaptador ListarTipoPendencia
     */
    public void setListarTipoPendenciaPDCAdapter(IListarTipoPendenciaPDCAdapter pdcListarTipoPendencia) {
        this.pdcListarTipoPendencia = pdcListarTipoPendencia;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoRelacConta.
     * 
     * @return Adaptador ListarTipoRelacConta
     */
    public IListarTipoRelacContaPDCAdapter getListarTipoRelacContaPDCAdapter() {
        return pdcListarTipoRelacConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoRelacConta.
     * 
     * @param pdcListarTipoRelacConta
     *            Adaptador ListarTipoRelacConta
     */
    public void setListarTipoRelacContaPDCAdapter(IListarTipoRelacContaPDCAdapter pdcListarTipoRelacConta) {
        this.pdcListarTipoRelacConta = pdcListarTipoRelacConta;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoRespons.
     * 
     * @return Adaptador ListarTipoRespons
     */
    public IListarTipoResponsPDCAdapter getListarTipoResponsPDCAdapter() {
        return pdcListarTipoRespons;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoRespons.
     * 
     * @param pdcListarTipoRespons
     *            Adaptador ListarTipoRespons
     */
    public void setListarTipoResponsPDCAdapter(IListarTipoResponsPDCAdapter pdcListarTipoRespons) {
        this.pdcListarTipoRespons = pdcListarTipoRespons;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoSolicitacao.
     * 
     * @return Adaptador ListarTipoSolicitacao
     */
    public IListarTipoSolicitacaoPDCAdapter getListarTipoSolicitacaoPDCAdapter() {
        return pdcListarTipoSolicitacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoSolicitacao.
     * 
     * @param pdcListarTipoSolicitacao
     *            Adaptador ListarTipoSolicitacao
     */
    public void setListarTipoSolicitacaoPDCAdapter(IListarTipoSolicitacaoPDCAdapter pdcListarTipoSolicitacao) {
        this.pdcListarTipoSolicitacao = pdcListarTipoSolicitacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoVinculo.
     * 
     * @return Adaptador ListarTipoVinculo
     */
    public IListarTipoVinculoPDCAdapter getListarTipoVinculoPDCAdapter() {
        return pdcListarTipoVinculo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoVinculo.
     * 
     * @param pdcListarTipoVinculo
     *            Adaptador ListarTipoVinculo
     */
    public void setListarTipoVinculoPDCAdapter(IListarTipoVinculoPDCAdapter pdcListarTipoVinculo) {
        this.pdcListarTipoVinculo = pdcListarTipoVinculo;
    }
    /**
     * M�todo invocado para obter um adaptador ListaRegra.
     * 
     * @return Adaptador ListaRegra
     */
    public IListaRegraPDCAdapter getListaRegraPDCAdapter() {
        return pdcListaRegra;
    }

    /**
     * M�todo invocado para establecer um adaptador ListaRegra.
     * 
     * @param pdcListaRegra
     *            Adaptador ListaRegra
     */
    public void setListaRegraPDCAdapter(IListaRegraPDCAdapter pdcListaRegra) {
        this.pdcListaRegra = pdcListaRegra;
    }
    /**
     * M�todo invocado para obter um adaptador MontarComboProduto.
     * 
     * @return Adaptador MontarComboProduto
     */
    public IMontarComboProdutoPDCAdapter getMontarComboProdutoPDCAdapter() {
        return pdcMontarComboProduto;
    }

    /**
     * M�todo invocado para establecer um adaptador MontarComboProduto.
     * 
     * @param pdcMontarComboProduto
     *            Adaptador MontarComboProduto
     */
    public void setMontarComboProdutoPDCAdapter(IMontarComboProdutoPDCAdapter pdcMontarComboProduto) {
        this.pdcMontarComboProduto = pdcMontarComboProduto;
    }
    /**
     * M�todo invocado para obter um adaptador ListarRemessa.
     * 
     * @return Adaptador ListarRemessa
     */
    public IListarRemessaPDCAdapter getListarRemessaPDCAdapter() {
        return pdcListarRemessa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarRemessa.
     * 
     * @param pdcListarRemessa
     *            Adaptador ListarRemessa
     */
    public void setListarRemessaPDCAdapter(IListarRemessaPDCAdapter pdcListarRemessa) {
        this.pdcListarRemessa = pdcListarRemessa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarInconsistencia.
     * 
     * @return Adaptador ConsultarInconsistencia
     */
    public IConsultarInconsistenciaPDCAdapter getConsultarInconsistenciaPDCAdapter() {
        return pdcConsultarInconsistencia;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarInconsistencia.
     * 
     * @param pdcConsultarInconsistencia
     *            Adaptador ConsultarInconsistencia
     */
    public void setConsultarInconsistenciaPDCAdapter(IConsultarInconsistenciaPDCAdapter pdcConsultarInconsistencia) {
        this.pdcConsultarInconsistencia = pdcConsultarInconsistencia;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLote.
     * 
     * @return Adaptador ConsultarLote
     */
    public IConsultarLotePDCAdapter getConsultarLotePDCAdapter() {
        return pdcConsultarLote;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLote.
     * 
     * @param pdcConsultarLote
     *            Adaptador ConsultarLote
     */
    public void setConsultarLotePDCAdapter(IConsultarLotePDCAdapter pdcConsultarLote) {
        this.pdcConsultarLote = pdcConsultarLote;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarOcorrencias.
     * 
     * @return Adaptador ConsultarOcorrencias
     */
    public IConsultarOcorrenciasPDCAdapter getConsultarOcorrenciasPDCAdapter() {
        return pdcConsultarOcorrencias;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarOcorrencias.
     * 
     * @param pdcConsultarOcorrencias
     *            Adaptador ConsultarOcorrencias
     */
    public void setConsultarOcorrenciasPDCAdapter(IConsultarOcorrenciasPDCAdapter pdcConsultarOcorrencias) {
        this.pdcConsultarOcorrencias = pdcConsultarOcorrencias;
    }
    /**
     * M�todo invocado para obter um adaptador ListarClientes.
     * 
     * @return Adaptador ListarClientes
     */
    public IListarClientesPDCAdapter getListarClientesPDCAdapter() {
        return pdcListarClientes;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarClientes.
     * 
     * @param pdcListarClientes
     *            Adaptador ListarClientes
     */
    public void setListarClientesPDCAdapter(IListarClientesPDCAdapter pdcListarClientes) {
        this.pdcListarClientes = pdcListarClientes;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarOcorrenciaRetorno.
     * 
     * @return Adaptador ConsultarOcorrenciaRetorno
     */
    public IConsultarOcorrenciaRetornoPDCAdapter getConsultarOcorrenciaRetornoPDCAdapter() {
        return pdcConsultarOcorrenciaRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarOcorrenciaRetorno.
     * 
     * @param pdcConsultarOcorrenciaRetorno
     *            Adaptador ConsultarOcorrenciaRetorno
     */
    public void setConsultarOcorrenciaRetornoPDCAdapter(IConsultarOcorrenciaRetornoPDCAdapter pdcConsultarOcorrenciaRetorno) {
        this.pdcConsultarOcorrenciaRetorno = pdcConsultarOcorrenciaRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLoteRetorno.
     * 
     * @return Adaptador ConsultarLoteRetorno
     */
    public IConsultarLoteRetornoPDCAdapter getConsultarLoteRetornoPDCAdapter() {
        return pdcConsultarLoteRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLoteRetorno.
     * 
     * @param pdcConsultarLoteRetorno
     *            Adaptador ConsultarLoteRetorno
     */
    public void setConsultarLoteRetornoPDCAdapter(IConsultarLoteRetornoPDCAdapter pdcConsultarLoteRetorno) {
        this.pdcConsultarLoteRetorno = pdcConsultarLoteRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarInconsistenciaRetorno.
     * 
     * @return Adaptador ConsultarInconsistenciaRetorno
     */
    public IConsultarInconsistenciaRetornoPDCAdapter getConsultarInconsistenciaRetornoPDCAdapter() {
        return pdcConsultarInconsistenciaRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarInconsistenciaRetorno.
     * 
     * @param pdcConsultarInconsistenciaRetorno
     *            Adaptador ConsultarInconsistenciaRetorno
     */
    public void setConsultarInconsistenciaRetornoPDCAdapter(IConsultarInconsistenciaRetornoPDCAdapter pdcConsultarInconsistenciaRetorno) {
        this.pdcConsultarInconsistenciaRetorno = pdcConsultarInconsistenciaRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarRetorno.
     * 
     * @return Adaptador ConsultarRetorno
     */
    public IConsultarRetornoPDCAdapter getConsultarRetornoPDCAdapter() {
        return pdcConsultarRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarRetorno.
     * 
     * @param pdcConsultarRetorno
     *            Adaptador ConsultarRetorno
     */
    public void setConsultarRetornoPDCAdapter(IConsultarRetornoPDCAdapter pdcConsultarRetorno) {
        this.pdcConsultarRetorno = pdcConsultarRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarEstatisticasLote.
     * 
     * @return Adaptador ConsultarEstatisticasLote
     */
    public IConsultarEstatisticasLotePDCAdapter getConsultarEstatisticasLotePDCAdapter() {
        return pdcConsultarEstatisticasLote;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarEstatisticasLote.
     * 
     * @param pdcConsultarEstatisticasLote
     *            Adaptador ConsultarEstatisticasLote
     */
    public void setConsultarEstatisticasLotePDCAdapter(IConsultarEstatisticasLotePDCAdapter pdcConsultarEstatisticasLote) {
        this.pdcConsultarEstatisticasLote = pdcConsultarEstatisticasLote;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAssLoteLayoutArquivo.
     * 
     * @return Adaptador ListarAssLoteLayoutArquivo
     */
    public IListarAssLoteLayoutArquivoPDCAdapter getListarAssLoteLayoutArquivoPDCAdapter() {
        return pdcListarAssLoteLayoutArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAssLoteLayoutArquivo.
     * 
     * @param pdcListarAssLoteLayoutArquivo
     *            Adaptador ListarAssLoteLayoutArquivo
     */
    public void setListarAssLoteLayoutArquivoPDCAdapter(IListarAssLoteLayoutArquivoPDCAdapter pdcListarAssLoteLayoutArquivo) {
        this.pdcListarAssLoteLayoutArquivo = pdcListarAssLoteLayoutArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharAssLoteLayoutArquivo.
     * 
     * @return Adaptador DetalharAssLoteLayoutArquivo
     */
    public IDetalharAssLoteLayoutArquivoPDCAdapter getDetalharAssLoteLayoutArquivoPDCAdapter() {
        return pdcDetalharAssLoteLayoutArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharAssLoteLayoutArquivo.
     * 
     * @param pdcDetalharAssLoteLayoutArquivo
     *            Adaptador DetalharAssLoteLayoutArquivo
     */
    public void setDetalharAssLoteLayoutArquivoPDCAdapter(IDetalharAssLoteLayoutArquivoPDCAdapter pdcDetalharAssLoteLayoutArquivo) {
        this.pdcDetalharAssLoteLayoutArquivo = pdcDetalharAssLoteLayoutArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAssLoteLayoutArquivo.
     * 
     * @return Adaptador ExcluirAssLoteLayoutArquivo
     */
    public IExcluirAssLoteLayoutArquivoPDCAdapter getExcluirAssLoteLayoutArquivoPDCAdapter() {
        return pdcExcluirAssLoteLayoutArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAssLoteLayoutArquivo.
     * 
     * @param pdcExcluirAssLoteLayoutArquivo
     *            Adaptador ExcluirAssLoteLayoutArquivo
     */
    public void setExcluirAssLoteLayoutArquivoPDCAdapter(IExcluirAssLoteLayoutArquivoPDCAdapter pdcExcluirAssLoteLayoutArquivo) {
        this.pdcExcluirAssLoteLayoutArquivo = pdcExcluirAssLoteLayoutArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAssLoteLayoutArquivo.
     * 
     * @return Adaptador IncluirAssLoteLayoutArquivo
     */
    public IIncluirAssLoteLayoutArquivoPDCAdapter getIncluirAssLoteLayoutArquivoPDCAdapter() {
        return pdcIncluirAssLoteLayoutArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAssLoteLayoutArquivo.
     * 
     * @param pdcIncluirAssLoteLayoutArquivo
     *            Adaptador IncluirAssLoteLayoutArquivo
     */
    public void setIncluirAssLoteLayoutArquivoPDCAdapter(IIncluirAssLoteLayoutArquivoPDCAdapter pdcIncluirAssLoteLayoutArquivo) {
        this.pdcIncluirAssLoteLayoutArquivo = pdcIncluirAssLoteLayoutArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharMoedasSistema.
     * 
     * @return Adaptador DetalharMoedasSistema
     */
    public IDetalharMoedasSistemaPDCAdapter getDetalharMoedasSistemaPDCAdapter() {
        return pdcDetalharMoedasSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharMoedasSistema.
     * 
     * @param pdcDetalharMoedasSistema
     *            Adaptador DetalharMoedasSistema
     */
    public void setDetalharMoedasSistemaPDCAdapter(IDetalharMoedasSistemaPDCAdapter pdcDetalharMoedasSistema) {
        this.pdcDetalharMoedasSistema = pdcDetalharMoedasSistema;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirMoedasSistema.
     * 
     * @return Adaptador ExcluirMoedasSistema
     */
    public IExcluirMoedasSistemaPDCAdapter getExcluirMoedasSistemaPDCAdapter() {
        return pdcExcluirMoedasSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirMoedasSistema.
     * 
     * @param pdcExcluirMoedasSistema
     *            Adaptador ExcluirMoedasSistema
     */
    public void setExcluirMoedasSistemaPDCAdapter(IExcluirMoedasSistemaPDCAdapter pdcExcluirMoedasSistema) {
        this.pdcExcluirMoedasSistema = pdcExcluirMoedasSistema;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirMoedasSistema.
     * 
     * @return Adaptador IncluirMoedasSistema
     */
    public IIncluirMoedasSistemaPDCAdapter getIncluirMoedasSistemaPDCAdapter() {
        return pdcIncluirMoedasSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirMoedasSistema.
     * 
     * @param pdcIncluirMoedasSistema
     *            Adaptador IncluirMoedasSistema
     */
    public void setIncluirMoedasSistemaPDCAdapter(IIncluirMoedasSistemaPDCAdapter pdcIncluirMoedasSistema) {
        this.pdcIncluirMoedasSistema = pdcIncluirMoedasSistema;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMoedasSistema.
     * 
     * @return Adaptador ListarMoedasSistema
     */
    public IListarMoedasSistemaPDCAdapter getListarMoedasSistemaPDCAdapter() {
        return pdcListarMoedasSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMoedasSistema.
     * 
     * @param pdcListarMoedasSistema
     *            Adaptador ListarMoedasSistema
     */
    public void setListarMoedasSistemaPDCAdapter(IListarMoedasSistemaPDCAdapter pdcListarMoedasSistema) {
        this.pdcListarMoedasSistema = pdcListarMoedasSistema;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoUnidadeOrganizacional.
     * 
     * @return Adaptador ListarTipoUnidadeOrganizacional
     */
    public IListarTipoUnidadeOrganizacionalPDCAdapter getListarTipoUnidadeOrganizacionalPDCAdapter() {
        return pdcListarTipoUnidadeOrganizacional;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoUnidadeOrganizacional.
     * 
     * @param pdcListarTipoUnidadeOrganizacional
     *            Adaptador ListarTipoUnidadeOrganizacional
     */
    public void setListarTipoUnidadeOrganizacionalPDCAdapter(IListarTipoUnidadeOrganizacionalPDCAdapter pdcListarTipoUnidadeOrganizacional) {
        this.pdcListarTipoUnidadeOrganizacional = pdcListarTipoUnidadeOrganizacional;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirOrgaoPagador.
     * 
     * @return Adaptador IncluirOrgaoPagador
     */
    public IIncluirOrgaoPagadorPDCAdapter getIncluirOrgaoPagadorPDCAdapter() {
        return pdcIncluirOrgaoPagador;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirOrgaoPagador.
     * 
     * @param pdcIncluirOrgaoPagador
     *            Adaptador IncluirOrgaoPagador
     */
    public void setIncluirOrgaoPagadorPDCAdapter(IIncluirOrgaoPagadorPDCAdapter pdcIncluirOrgaoPagador) {
        this.pdcIncluirOrgaoPagador = pdcIncluirOrgaoPagador;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarOrgaoPagador.
     * 
     * @return Adaptador AlterarOrgaoPagador
     */
    public IAlterarOrgaoPagadorPDCAdapter getAlterarOrgaoPagadorPDCAdapter() {
        return pdcAlterarOrgaoPagador;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarOrgaoPagador.
     * 
     * @param pdcAlterarOrgaoPagador
     *            Adaptador AlterarOrgaoPagador
     */
    public void setAlterarOrgaoPagadorPDCAdapter(IAlterarOrgaoPagadorPDCAdapter pdcAlterarOrgaoPagador) {
        this.pdcAlterarOrgaoPagador = pdcAlterarOrgaoPagador;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharOrgaoPagador.
     * 
     * @return Adaptador DetalharOrgaoPagador
     */
    public IDetalharOrgaoPagadorPDCAdapter getDetalharOrgaoPagadorPDCAdapter() {
        return pdcDetalharOrgaoPagador;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharOrgaoPagador.
     * 
     * @param pdcDetalharOrgaoPagador
     *            Adaptador DetalharOrgaoPagador
     */
    public void setDetalharOrgaoPagadorPDCAdapter(IDetalharOrgaoPagadorPDCAdapter pdcDetalharOrgaoPagador) {
        this.pdcDetalharOrgaoPagador = pdcDetalharOrgaoPagador;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirOrgaoPagador.
     * 
     * @return Adaptador ExcluirOrgaoPagador
     */
    public IExcluirOrgaoPagadorPDCAdapter getExcluirOrgaoPagadorPDCAdapter() {
        return pdcExcluirOrgaoPagador;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirOrgaoPagador.
     * 
     * @param pdcExcluirOrgaoPagador
     *            Adaptador ExcluirOrgaoPagador
     */
    public void setExcluirOrgaoPagadorPDCAdapter(IExcluirOrgaoPagadorPDCAdapter pdcExcluirOrgaoPagador) {
        this.pdcExcluirOrgaoPagador = pdcExcluirOrgaoPagador;
    }
    /**
     * M�todo invocado para obter um adaptador BloquearOrgaoPagador.
     * 
     * @return Adaptador BloquearOrgaoPagador
     */
    public IBloquearOrgaoPagadorPDCAdapter getBloquearOrgaoPagadorPDCAdapter() {
        return pdcBloquearOrgaoPagador;
    }

    /**
     * M�todo invocado para establecer um adaptador BloquearOrgaoPagador.
     * 
     * @param pdcBloquearOrgaoPagador
     *            Adaptador BloquearOrgaoPagador
     */
    public void setBloquearOrgaoPagadorPDCAdapter(IBloquearOrgaoPagadorPDCAdapter pdcBloquearOrgaoPagador) {
        this.pdcBloquearOrgaoPagador = pdcBloquearOrgaoPagador;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistoricoOrgaoPagador.
     * 
     * @return Adaptador DetalharHistoricoOrgaoPagador
     */
    public IDetalharHistoricoOrgaoPagadorPDCAdapter getDetalharHistoricoOrgaoPagadorPDCAdapter() {
        return pdcDetalharHistoricoOrgaoPagador;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistoricoOrgaoPagador.
     * 
     * @param pdcDetalharHistoricoOrgaoPagador
     *            Adaptador DetalharHistoricoOrgaoPagador
     */
    public void setDetalharHistoricoOrgaoPagadorPDCAdapter(IDetalharHistoricoOrgaoPagadorPDCAdapter pdcDetalharHistoricoOrgaoPagador) {
        this.pdcDetalharHistoricoOrgaoPagador = pdcDetalharHistoricoOrgaoPagador;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistoricoOrgaoPagador.
     * 
     * @return Adaptador ListarHistoricoOrgaoPagador
     */
    public IListarHistoricoOrgaoPagadorPDCAdapter getListarHistoricoOrgaoPagadorPDCAdapter() {
        return pdcListarHistoricoOrgaoPagador;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistoricoOrgaoPagador.
     * 
     * @param pdcListarHistoricoOrgaoPagador
     *            Adaptador ListarHistoricoOrgaoPagador
     */
    public void setListarHistoricoOrgaoPagadorPDCAdapter(IListarHistoricoOrgaoPagadorPDCAdapter pdcListarHistoricoOrgaoPagador) {
        this.pdcListarHistoricoOrgaoPagador = pdcListarHistoricoOrgaoPagador;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMoedaEconomica.
     * 
     * @return Adaptador ListarMoedaEconomica
     */
    public IListarMoedaEconomicaPDCAdapter getListarMoedaEconomicaPDCAdapter() {
        return pdcListarMoedaEconomica;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMoedaEconomica.
     * 
     * @param pdcListarMoedaEconomica
     *            Adaptador ListarMoedaEconomica
     */
    public void setListarMoedaEconomicaPDCAdapter(IListarMoedaEconomicaPDCAdapter pdcListarMoedaEconomica) {
        this.pdcListarMoedaEconomica = pdcListarMoedaEconomica;
    }
    /**
     * M�todo invocado para obter um adaptador ListarFavorecidos.
     * 
     * @return Adaptador ListarFavorecidos
     */
    public IListarFavorecidosPDCAdapter getListarFavorecidosPDCAdapter() {
        return pdcListarFavorecidos;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarFavorecidos.
     * 
     * @param pdcListarFavorecidos
     *            Adaptador ListarFavorecidos
     */
    public void setListarFavorecidosPDCAdapter(IListarFavorecidosPDCAdapter pdcListarFavorecidos) {
        this.pdcListarFavorecidos = pdcListarFavorecidos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarInscricaoFavorecidos.
     * 
     * @return Adaptador ListarInscricaoFavorecidos
     */
    public IListarInscricaoFavorecidosPDCAdapter getListarInscricaoFavorecidosPDCAdapter() {
        return pdcListarInscricaoFavorecidos;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarInscricaoFavorecidos.
     * 
     * @param pdcListarInscricaoFavorecidos
     *            Adaptador ListarInscricaoFavorecidos
     */
    public void setListarInscricaoFavorecidosPDCAdapter(IListarInscricaoFavorecidosPDCAdapter pdcListarInscricaoFavorecidos) {
        this.pdcListarInscricaoFavorecidos = pdcListarInscricaoFavorecidos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoFavorecidos.
     * 
     * @return Adaptador ListarTipoFavorecidos
     */
    public IListarTipoFavorecidosPDCAdapter getListarTipoFavorecidosPDCAdapter() {
        return pdcListarTipoFavorecidos;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoFavorecidos.
     * 
     * @param pdcListarTipoFavorecidos
     *            Adaptador ListarTipoFavorecidos
     */
    public void setListarTipoFavorecidosPDCAdapter(IListarTipoFavorecidosPDCAdapter pdcListarTipoFavorecidos) {
        this.pdcListarTipoFavorecidos = pdcListarTipoFavorecidos;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirMoedasArquivo.
     * 
     * @return Adaptador ExcluirMoedasArquivo
     */
    public IExcluirMoedasArquivoPDCAdapter getExcluirMoedasArquivoPDCAdapter() {
        return pdcExcluirMoedasArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirMoedasArquivo.
     * 
     * @param pdcExcluirMoedasArquivo
     *            Adaptador ExcluirMoedasArquivo
     */
    public void setExcluirMoedasArquivoPDCAdapter(IExcluirMoedasArquivoPDCAdapter pdcExcluirMoedasArquivo) {
        this.pdcExcluirMoedasArquivo = pdcExcluirMoedasArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirMoedasArquivo.
     * 
     * @return Adaptador IncluirMoedasArquivo
     */
    public IIncluirMoedasArquivoPDCAdapter getIncluirMoedasArquivoPDCAdapter() {
        return pdcIncluirMoedasArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirMoedasArquivo.
     * 
     * @param pdcIncluirMoedasArquivo
     *            Adaptador IncluirMoedasArquivo
     */
    public void setIncluirMoedasArquivoPDCAdapter(IIncluirMoedasArquivoPDCAdapter pdcIncluirMoedasArquivo) {
        this.pdcIncluirMoedasArquivo = pdcIncluirMoedasArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMoedasArquivo.
     * 
     * @return Adaptador ListarMoedasArquivo
     */
    public IListarMoedasArquivoPDCAdapter getListarMoedasArquivoPDCAdapter() {
        return pdcListarMoedasArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMoedasArquivo.
     * 
     * @param pdcListarMoedasArquivo
     *            Adaptador ListarMoedasArquivo
     */
    public void setListarMoedasArquivoPDCAdapter(IListarMoedasArquivoPDCAdapter pdcListarMoedasArquivo) {
        this.pdcListarMoedasArquivo = pdcListarMoedasArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAssLoteLayoutArqServico.
     * 
     * @return Adaptador ExcluirAssLoteLayoutArqServico
     */
    public IExcluirAssLoteLayoutArqServicoPDCAdapter getExcluirAssLoteLayoutArqServicoPDCAdapter() {
        return pdcExcluirAssLoteLayoutArqServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAssLoteLayoutArqServico.
     * 
     * @param pdcExcluirAssLoteLayoutArqServico
     *            Adaptador ExcluirAssLoteLayoutArqServico
     */
    public void setExcluirAssLoteLayoutArqServicoPDCAdapter(IExcluirAssLoteLayoutArqServicoPDCAdapter pdcExcluirAssLoteLayoutArqServico) {
        this.pdcExcluirAssLoteLayoutArqServico = pdcExcluirAssLoteLayoutArqServico;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAssLoteLayoutArqServico.
     * 
     * @return Adaptador IncluirAssLoteLayoutArqServico
     */
    public IIncluirAssLoteLayoutArqServicoPDCAdapter getIncluirAssLoteLayoutArqServicoPDCAdapter() {
        return pdcIncluirAssLoteLayoutArqServico;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAssLoteLayoutArqServico.
     * 
     * @param pdcIncluirAssLoteLayoutArqServico
     *            Adaptador IncluirAssLoteLayoutArqServico
     */
    public void setIncluirAssLoteLayoutArqServicoPDCAdapter(IIncluirAssLoteLayoutArqServicoPDCAdapter pdcIncluirAssLoteLayoutArqServico) {
        this.pdcIncluirAssLoteLayoutArqServico = pdcIncluirAssLoteLayoutArqServico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAssLoteLayoutArqServico.
     * 
     * @return Adaptador ListarAssLoteLayoutArqServico
     */
    public IListarAssLoteLayoutArqServicoPDCAdapter getListarAssLoteLayoutArqServicoPDCAdapter() {
        return pdcListarAssLoteLayoutArqServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAssLoteLayoutArqServico.
     * 
     * @param pdcListarAssLoteLayoutArqServico
     *            Adaptador ListarAssLoteLayoutArqServico
     */
    public void setListarAssLoteLayoutArqServicoPDCAdapter(IListarAssLoteLayoutArqServicoPDCAdapter pdcListarAssLoteLayoutArqServico) {
        this.pdcListarAssLoteLayoutArqServico = pdcListarAssLoteLayoutArqServico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarOrgaoPagador.
     * 
     * @return Adaptador ListarOrgaoPagador
     */
    public IListarOrgaoPagadorPDCAdapter getListarOrgaoPagadorPDCAdapter() {
        return pdcListarOrgaoPagador;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarOrgaoPagador.
     * 
     * @param pdcListarOrgaoPagador
     *            Adaptador ListarOrgaoPagador
     */
    public void setListarOrgaoPagadorPDCAdapter(IListarOrgaoPagadorPDCAdapter pdcListarOrgaoPagador) {
        this.pdcListarOrgaoPagador = pdcListarOrgaoPagador;
    }
    /**
     * M�todo invocado para obter um adaptador ListarCentroCusto.
     * 
     * @return Adaptador ListarCentroCusto
     */
    public IListarCentroCustoPDCAdapter getListarCentroCustoPDCAdapter() {
        return pdcListarCentroCusto;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarCentroCusto.
     * 
     * @param pdcListarCentroCusto
     *            Adaptador ListarCentroCusto
     */
    public void setListarCentroCustoPDCAdapter(IListarCentroCustoPDCAdapter pdcListarCentroCusto) {
        this.pdcListarCentroCusto = pdcListarCentroCusto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirProcessoMassivo.
     * 
     * @return Adaptador IncluirProcessoMassivo
     */
    public IIncluirProcessoMassivoPDCAdapter getIncluirProcessoMassivoPDCAdapter() {
        return pdcIncluirProcessoMassivo;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirProcessoMassivo.
     * 
     * @param pdcIncluirProcessoMassivo
     *            Adaptador IncluirProcessoMassivo
     */
    public void setIncluirProcessoMassivoPDCAdapter(IIncluirProcessoMassivoPDCAdapter pdcIncluirProcessoMassivo) {
        this.pdcIncluirProcessoMassivo = pdcIncluirProcessoMassivo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharProcessoMassivo.
     * 
     * @return Adaptador DetalharProcessoMassivo
     */
    public IDetalharProcessoMassivoPDCAdapter getDetalharProcessoMassivoPDCAdapter() {
        return pdcDetalharProcessoMassivo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharProcessoMassivo.
     * 
     * @param pdcDetalharProcessoMassivo
     *            Adaptador DetalharProcessoMassivo
     */
    public void setDetalharProcessoMassivoPDCAdapter(IDetalharProcessoMassivoPDCAdapter pdcDetalharProcessoMassivo) {
        this.pdcDetalharProcessoMassivo = pdcDetalharProcessoMassivo;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirVinculoContaSalarioDestino.
     * 
     * @return Adaptador ExcluirVinculoContaSalarioDestino
     */
    public IExcluirVinculoContaSalarioDestinoPDCAdapter getExcluirVinculoContaSalarioDestinoPDCAdapter() {
        return pdcExcluirVinculoContaSalarioDestino;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirVinculoContaSalarioDestino.
     * 
     * @param pdcExcluirVinculoContaSalarioDestino
     *            Adaptador ExcluirVinculoContaSalarioDestino
     */
    public void setExcluirVinculoContaSalarioDestinoPDCAdapter(IExcluirVinculoContaSalarioDestinoPDCAdapter pdcExcluirVinculoContaSalarioDestino) {
        this.pdcExcluirVinculoContaSalarioDestino = pdcExcluirVinculoContaSalarioDestino;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarProcessoMassivo.
     * 
     * @return Adaptador AlterarProcessoMassivo
     */
    public IAlterarProcessoMassivoPDCAdapter getAlterarProcessoMassivoPDCAdapter() {
        return pdcAlterarProcessoMassivo;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarProcessoMassivo.
     * 
     * @param pdcAlterarProcessoMassivo
     *            Adaptador AlterarProcessoMassivo
     */
    public void setAlterarProcessoMassivoPDCAdapter(IAlterarProcessoMassivoPDCAdapter pdcAlterarProcessoMassivo) {
        this.pdcAlterarProcessoMassivo = pdcAlterarProcessoMassivo;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirProcessoMassivo.
     * 
     * @return Adaptador ExcluirProcessoMassivo
     */
    public IExcluirProcessoMassivoPDCAdapter getExcluirProcessoMassivoPDCAdapter() {
        return pdcExcluirProcessoMassivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirProcessoMassivo.
     * 
     * @param pdcExcluirProcessoMassivo
     *            Adaptador ExcluirProcessoMassivo
     */
    public void setExcluirProcessoMassivoPDCAdapter(IExcluirProcessoMassivoPDCAdapter pdcExcluirProcessoMassivo) {
        this.pdcExcluirProcessoMassivo = pdcExcluirProcessoMassivo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistoricoFavorecido.
     * 
     * @return Adaptador DetalharHistoricoFavorecido
     */
    public IDetalharHistoricoFavorecidoPDCAdapter getDetalharHistoricoFavorecidoPDCAdapter() {
        return pdcDetalharHistoricoFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistoricoFavorecido.
     * 
     * @param pdcDetalharHistoricoFavorecido
     *            Adaptador DetalharHistoricoFavorecido
     */
    public void setDetalharHistoricoFavorecidoPDCAdapter(IDetalharHistoricoFavorecidoPDCAdapter pdcDetalharHistoricoFavorecido) {
        this.pdcDetalharHistoricoFavorecido = pdcDetalharHistoricoFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador BloquearContaFavorecido.
     * 
     * @return Adaptador BloquearContaFavorecido
     */
    public IBloquearContaFavorecidoPDCAdapter getBloquearContaFavorecidoPDCAdapter() {
        return pdcBloquearContaFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador BloquearContaFavorecido.
     * 
     * @param pdcBloquearContaFavorecido
     *            Adaptador BloquearContaFavorecido
     */
    public void setBloquearContaFavorecidoPDCAdapter(IBloquearContaFavorecidoPDCAdapter pdcBloquearContaFavorecido) {
        this.pdcBloquearContaFavorecido = pdcBloquearContaFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalheContaFavorecido.
     * 
     * @return Adaptador ConsultarDetalheContaFavorecido
     */
    public IConsultarDetalheContaFavorecidoPDCAdapter getConsultarDetalheContaFavorecidoPDCAdapter() {
        return pdcConsultarDetalheContaFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalheContaFavorecido.
     * 
     * @param pdcConsultarDetalheContaFavorecido
     *            Adaptador ConsultarDetalheContaFavorecido
     */
    public void setConsultarDetalheContaFavorecidoPDCAdapter(IConsultarDetalheContaFavorecidoPDCAdapter pdcConsultarDetalheContaFavorecido) {
        this.pdcConsultarDetalheContaFavorecido = pdcConsultarDetalheContaFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalheFavorecido.
     * 
     * @return Adaptador ConsultarDetalheFavorecido
     */
    public IConsultarDetalheFavorecidoPDCAdapter getConsultarDetalheFavorecidoPDCAdapter() {
        return pdcConsultarDetalheFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalheFavorecido.
     * 
     * @param pdcConsultarDetalheFavorecido
     *            Adaptador ConsultarDetalheFavorecido
     */
    public void setConsultarDetalheFavorecidoPDCAdapter(IConsultarDetalheFavorecidoPDCAdapter pdcConsultarDetalheFavorecido) {
        this.pdcConsultarDetalheFavorecido = pdcConsultarDetalheFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalheHistoricoContaFavorecido.
     * 
     * @return Adaptador ConsultarDetalheHistoricoContaFavorecido
     */
    public IConsultarDetalheHistoricoContaFavorecidoPDCAdapter getConsultarDetalheHistoricoContaFavorecidoPDCAdapter() {
        return pdcConsultarDetalheHistoricoContaFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalheHistoricoContaFavorecido.
     * 
     * @param pdcConsultarDetalheHistoricoContaFavorecido
     *            Adaptador ConsultarDetalheHistoricoContaFavorecido
     */
    public void setConsultarDetalheHistoricoContaFavorecidoPDCAdapter(IConsultarDetalheHistoricoContaFavorecidoPDCAdapter pdcConsultarDetalheHistoricoContaFavorecido) {
        this.pdcConsultarDetalheHistoricoContaFavorecido = pdcConsultarDetalheHistoricoContaFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharMoedasArquivo.
     * 
     * @return Adaptador DetalharMoedasArquivo
     */
    public IDetalharMoedasArquivoPDCAdapter getDetalharMoedasArquivoPDCAdapter() {
        return pdcDetalharMoedasArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharMoedasArquivo.
     * 
     * @param pdcDetalharMoedasArquivo
     *            Adaptador DetalharMoedasArquivo
     */
    public void setDetalharMoedasArquivoPDCAdapter(IDetalharMoedasArquivoPDCAdapter pdcDetalharMoedasArquivo) {
        this.pdcDetalharMoedasArquivo = pdcDetalharMoedasArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarAlcada.
     * 
     * @return Adaptador AlterarAlcada
     */
    public IAlterarAlcadaPDCAdapter getAlterarAlcadaPDCAdapter() {
        return pdcAlterarAlcada;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarAlcada.
     * 
     * @param pdcAlterarAlcada
     *            Adaptador AlterarAlcada
     */
    public void setAlterarAlcadaPDCAdapter(IAlterarAlcadaPDCAdapter pdcAlterarAlcada) {
        this.pdcAlterarAlcada = pdcAlterarAlcada;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAlcada.
     * 
     * @return Adaptador ExcluirAlcada
     */
    public IExcluirAlcadaPDCAdapter getExcluirAlcadaPDCAdapter() {
        return pdcExcluirAlcada;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAlcada.
     * 
     * @param pdcExcluirAlcada
     *            Adaptador ExcluirAlcada
     */
    public void setExcluirAlcadaPDCAdapter(IExcluirAlcadaPDCAdapter pdcExcluirAlcada) {
        this.pdcExcluirAlcada = pdcExcluirAlcada;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAlcada.
     * 
     * @return Adaptador IncluirAlcada
     */
    public IIncluirAlcadaPDCAdapter getIncluirAlcadaPDCAdapter() {
        return pdcIncluirAlcada;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAlcada.
     * 
     * @param pdcIncluirAlcada
     *            Adaptador IncluirAlcada
     */
    public void setIncluirAlcadaPDCAdapter(IIncluirAlcadaPDCAdapter pdcIncluirAlcada) {
        this.pdcIncluirAlcada = pdcIncluirAlcada;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoAcao.
     * 
     * @return Adaptador ListarTipoAcao
     */
    public IListarTipoAcaoPDCAdapter getListarTipoAcaoPDCAdapter() {
        return pdcListarTipoAcao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoAcao.
     * 
     * @param pdcListarTipoAcao
     *            Adaptador ListarTipoAcao
     */
    public void setListarTipoAcaoPDCAdapter(IListarTipoAcaoPDCAdapter pdcListarTipoAcao) {
        this.pdcListarTipoAcao = pdcListarTipoAcao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSegmentoClientes.
     * 
     * @return Adaptador ListarSegmentoClientes
     */
    public IListarSegmentoClientesPDCAdapter getListarSegmentoClientesPDCAdapter() {
        return pdcListarSegmentoClientes;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSegmentoClientes.
     * 
     * @param pdcListarSegmentoClientes
     *            Adaptador ListarSegmentoClientes
     */
    public void setListarSegmentoClientesPDCAdapter(IListarSegmentoClientesPDCAdapter pdcListarSegmentoClientes) {
        this.pdcListarSegmentoClientes = pdcListarSegmentoClientes;
    }
    /**
     * M�todo invocado para obter um adaptador CancelarRelatoriosContratos.
     * 
     * @return Adaptador CancelarRelatoriosContratos
     */
    public ICancelarRelatoriosContratosPDCAdapter getCancelarRelatoriosContratosPDCAdapter() {
        return pdcCancelarRelatoriosContratos;
    }

    /**
     * M�todo invocado para establecer um adaptador CancelarRelatoriosContratos.
     * 
     * @param pdcCancelarRelatoriosContratos
     *            Adaptador CancelarRelatoriosContratos
     */
    public void setCancelarRelatoriosContratosPDCAdapter(ICancelarRelatoriosContratosPDCAdapter pdcCancelarRelatoriosContratos) {
        this.pdcCancelarRelatoriosContratos = pdcCancelarRelatoriosContratos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarEmpresaConglomerado.
     * 
     * @return Adaptador ListarEmpresaConglomerado
     */
    public IListarEmpresaConglomeradoPDCAdapter getListarEmpresaConglomeradoPDCAdapter() {
        return pdcListarEmpresaConglomerado;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarEmpresaConglomerado.
     * 
     * @param pdcListarEmpresaConglomerado
     *            Adaptador ListarEmpresaConglomerado
     */
    public void setListarEmpresaConglomeradoPDCAdapter(IListarEmpresaConglomeradoPDCAdapter pdcListarEmpresaConglomerado) {
        this.pdcListarEmpresaConglomerado = pdcListarEmpresaConglomerado;
    }
    /**
     * M�todo invocado para obter um adaptador ListarDependDiretoria.
     * 
     * @return Adaptador ListarDependDiretoria
     */
    public IListarDependDiretoriaPDCAdapter getListarDependDiretoriaPDCAdapter() {
        return pdcListarDependDiretoria;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarDependDiretoria.
     * 
     * @param pdcListarDependDiretoria
     *            Adaptador ListarDependDiretoria
     */
    public void setListarDependDiretoriaPDCAdapter(IListarDependDiretoriaPDCAdapter pdcListarDependDiretoria) {
        this.pdcListarDependDiretoria = pdcListarDependDiretoria;
    }
    /**
     * M�todo invocado para obter um adaptador ListarOperacoesServico.
     * 
     * @return Adaptador ListarOperacoesServico
     */
    public IListarOperacoesServicoPDCAdapter getListarOperacoesServicoPDCAdapter() {
        return pdcListarOperacoesServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarOperacoesServico.
     * 
     * @param pdcListarOperacoesServico
     *            Adaptador ListarOperacoesServico
     */
    public void setListarOperacoesServicoPDCAdapter(IListarOperacoesServicoPDCAdapter pdcListarOperacoesServico) {
        this.pdcListarOperacoesServico = pdcListarOperacoesServico;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharAssLoteLayoutArqServico.
     * 
     * @return Adaptador DetalharAssLoteLayoutArqServico
     */
    public IDetalharAssLoteLayoutArqServicoPDCAdapter getDetalharAssLoteLayoutArqServicoPDCAdapter() {
        return pdcDetalharAssLoteLayoutArqServico;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharAssLoteLayoutArqServico.
     * 
     * @param pdcDetalharAssLoteLayoutArqServico
     *            Adaptador DetalharAssLoteLayoutArqServico
     */
    public void setDetalharAssLoteLayoutArqServicoPDCAdapter(IDetalharAssLoteLayoutArqServicoPDCAdapter pdcDetalharAssLoteLayoutArqServico) {
        this.pdcDetalharAssLoteLayoutArqServico = pdcDetalharAssLoteLayoutArqServico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarProcessoMassivo.
     * 
     * @return Adaptador ListarProcessoMassivo
     */
    public IListarProcessoMassivoPDCAdapter getListarProcessoMassivoPDCAdapter() {
        return pdcListarProcessoMassivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarProcessoMassivo.
     * 
     * @param pdcListarProcessoMassivo
     *            Adaptador ListarProcessoMassivo
     */
    public void setListarProcessoMassivoPDCAdapter(IListarProcessoMassivoPDCAdapter pdcListarProcessoMassivo) {
        this.pdcListarProcessoMassivo = pdcListarProcessoMassivo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharAssRetornoLayoutServico.
     * 
     * @return Adaptador DetalharAssRetornoLayoutServico
     */
    public IDetalharAssRetornoLayoutServicoPDCAdapter getDetalharAssRetornoLayoutServicoPDCAdapter() {
        return pdcDetalharAssRetornoLayoutServico;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharAssRetornoLayoutServico.
     * 
     * @param pdcDetalharAssRetornoLayoutServico
     *            Adaptador DetalharAssRetornoLayoutServico
     */
    public void setDetalharAssRetornoLayoutServicoPDCAdapter(IDetalharAssRetornoLayoutServicoPDCAdapter pdcDetalharAssRetornoLayoutServico) {
        this.pdcDetalharAssRetornoLayoutServico = pdcDetalharAssRetornoLayoutServico;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAssRetornoLayoutServico.
     * 
     * @return Adaptador ExcluirAssRetornoLayoutServico
     */
    public IExcluirAssRetornoLayoutServicoPDCAdapter getExcluirAssRetornoLayoutServicoPDCAdapter() {
        return pdcExcluirAssRetornoLayoutServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAssRetornoLayoutServico.
     * 
     * @param pdcExcluirAssRetornoLayoutServico
     *            Adaptador ExcluirAssRetornoLayoutServico
     */
    public void setExcluirAssRetornoLayoutServicoPDCAdapter(IExcluirAssRetornoLayoutServicoPDCAdapter pdcExcluirAssRetornoLayoutServico) {
        this.pdcExcluirAssRetornoLayoutServico = pdcExcluirAssRetornoLayoutServico;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAssRetornoLayoutServico.
     * 
     * @return Adaptador IncluirAssRetornoLayoutServico
     */
    public IIncluirAssRetornoLayoutServicoPDCAdapter getIncluirAssRetornoLayoutServicoPDCAdapter() {
        return pdcIncluirAssRetornoLayoutServico;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAssRetornoLayoutServico.
     * 
     * @param pdcIncluirAssRetornoLayoutServico
     *            Adaptador IncluirAssRetornoLayoutServico
     */
    public void setIncluirAssRetornoLayoutServicoPDCAdapter(IIncluirAssRetornoLayoutServicoPDCAdapter pdcIncluirAssRetornoLayoutServico) {
        this.pdcIncluirAssRetornoLayoutServico = pdcIncluirAssRetornoLayoutServico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAssRetornoLayoutServico.
     * 
     * @return Adaptador ListarAssRetornoLayoutServico
     */
    public IListarAssRetornoLayoutServicoPDCAdapter getListarAssRetornoLayoutServicoPDCAdapter() {
        return pdcListarAssRetornoLayoutServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAssRetornoLayoutServico.
     * 
     * @param pdcListarAssRetornoLayoutServico
     *            Adaptador ListarAssRetornoLayoutServico
     */
    public void setListarAssRetornoLayoutServicoPDCAdapter(IListarAssRetornoLayoutServicoPDCAdapter pdcListarAssRetornoLayoutServico) {
        this.pdcListarAssRetornoLayoutServico = pdcListarAssRetornoLayoutServico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaContaFavorecido.
     * 
     * @return Adaptador ConsultarListaContaFavorecido
     */
    public IConsultarListaContaFavorecidoPDCAdapter getConsultarListaContaFavorecidoPDCAdapter() {
        return pdcConsultarListaContaFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaContaFavorecido.
     * 
     * @param pdcConsultarListaContaFavorecido
     *            Adaptador ConsultarListaContaFavorecido
     */
    public void setConsultarListaContaFavorecidoPDCAdapter(IConsultarListaContaFavorecidoPDCAdapter pdcConsultarListaContaFavorecido) {
        this.pdcConsultarListaContaFavorecido = pdcConsultarListaContaFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ListarFinalidadeEndereco.
     * 
     * @return Adaptador ListarFinalidadeEndereco
     */
    public IListarFinalidadeEnderecoPDCAdapter getListarFinalidadeEnderecoPDCAdapter() {
        return pdcListarFinalidadeEndereco;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarFinalidadeEndereco.
     * 
     * @param pdcListarFinalidadeEndereco
     *            Adaptador ListarFinalidadeEndereco
     */
    public void setListarFinalidadeEnderecoPDCAdapter(IListarFinalidadeEnderecoPDCAdapter pdcListarFinalidadeEndereco) {
        this.pdcListarFinalidadeEndereco = pdcListarFinalidadeEndereco;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharAssLoteOperacaoServico.
     * 
     * @return Adaptador DetalharAssLoteOperacaoServico
     */
    public IDetalharAssLoteOperacaoServicoPDCAdapter getDetalharAssLoteOperacaoServicoPDCAdapter() {
        return pdcDetalharAssLoteOperacaoServico;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharAssLoteOperacaoServico.
     * 
     * @param pdcDetalharAssLoteOperacaoServico
     *            Adaptador DetalharAssLoteOperacaoServico
     */
    public void setDetalharAssLoteOperacaoServicoPDCAdapter(IDetalharAssLoteOperacaoServicoPDCAdapter pdcDetalharAssLoteOperacaoServico) {
        this.pdcDetalharAssLoteOperacaoServico = pdcDetalharAssLoteOperacaoServico;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAssLoteOperacaoServico.
     * 
     * @return Adaptador ExcluirAssLoteOperacaoServico
     */
    public IExcluirAssLoteOperacaoServicoPDCAdapter getExcluirAssLoteOperacaoServicoPDCAdapter() {
        return pdcExcluirAssLoteOperacaoServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAssLoteOperacaoServico.
     * 
     * @param pdcExcluirAssLoteOperacaoServico
     *            Adaptador ExcluirAssLoteOperacaoServico
     */
    public void setExcluirAssLoteOperacaoServicoPDCAdapter(IExcluirAssLoteOperacaoServicoPDCAdapter pdcExcluirAssLoteOperacaoServico) {
        this.pdcExcluirAssLoteOperacaoServico = pdcExcluirAssLoteOperacaoServico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAssLoteOperacaoServico.
     * 
     * @return Adaptador ListarAssLoteOperacaoServico
     */
    public IListarAssLoteOperacaoServicoPDCAdapter getListarAssLoteOperacaoServicoPDCAdapter() {
        return pdcListarAssLoteOperacaoServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAssLoteOperacaoServico.
     * 
     * @param pdcListarAssLoteOperacaoServico
     *            Adaptador ListarAssLoteOperacaoServico
     */
    public void setListarAssLoteOperacaoServicoPDCAdapter(IListarAssLoteOperacaoServicoPDCAdapter pdcListarAssLoteOperacaoServico) {
        this.pdcListarAssLoteOperacaoServico = pdcListarAssLoteOperacaoServico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarEstatisticaProcessoMassivo.
     * 
     * @return Adaptador ListarEstatisticaProcessoMassivo
     */
    public IListarEstatisticaProcessoMassivoPDCAdapter getListarEstatisticaProcessoMassivoPDCAdapter() {
        return pdcListarEstatisticaProcessoMassivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarEstatisticaProcessoMassivo.
     * 
     * @param pdcListarEstatisticaProcessoMassivo
     *            Adaptador ListarEstatisticaProcessoMassivo
     */
    public void setListarEstatisticaProcessoMassivoPDCAdapter(IListarEstatisticaProcessoMassivoPDCAdapter pdcListarEstatisticaProcessoMassivo) {
        this.pdcListarEstatisticaProcessoMassivo = pdcListarEstatisticaProcessoMassivo;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoConta.
     * 
     * @return Adaptador ListarTipoConta
     */
    public IListarTipoContaPDCAdapter getListarTipoContaPDCAdapter() {
        return pdcListarTipoConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoConta.
     * 
     * @param pdcListarTipoConta
     *            Adaptador ListarTipoConta
     */
    public void setListarTipoContaPDCAdapter(IListarTipoContaPDCAdapter pdcListarTipoConta) {
        this.pdcListarTipoConta = pdcListarTipoConta;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharAlcada.
     * 
     * @return Adaptador DetalharAlcada
     */
    public IDetalharAlcadaPDCAdapter getDetalharAlcadaPDCAdapter() {
        return pdcDetalharAlcada;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharAlcada.
     * 
     * @param pdcDetalharAlcada
     *            Adaptador DetalharAlcada
     */
    public void setDetalharAlcadaPDCAdapter(IDetalharAlcadaPDCAdapter pdcDetalharAlcada) {
        this.pdcDetalharAlcada = pdcDetalharAlcada;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarMsgComprovanteSalrl.
     * 
     * @return Adaptador AlterarMsgComprovanteSalrl
     */
    public IAlterarMsgComprovanteSalrlPDCAdapter getAlterarMsgComprovanteSalrlPDCAdapter() {
        return pdcAlterarMsgComprovanteSalrl;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarMsgComprovanteSalrl.
     * 
     * @param pdcAlterarMsgComprovanteSalrl
     *            Adaptador AlterarMsgComprovanteSalrl
     */
    public void setAlterarMsgComprovanteSalrlPDCAdapter(IAlterarMsgComprovanteSalrlPDCAdapter pdcAlterarMsgComprovanteSalrl) {
        this.pdcAlterarMsgComprovanteSalrl = pdcAlterarMsgComprovanteSalrl;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirMsgComprovanteSalrl.
     * 
     * @return Adaptador ExcluirMsgComprovanteSalrl
     */
    public IExcluirMsgComprovanteSalrlPDCAdapter getExcluirMsgComprovanteSalrlPDCAdapter() {
        return pdcExcluirMsgComprovanteSalrl;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirMsgComprovanteSalrl.
     * 
     * @param pdcExcluirMsgComprovanteSalrl
     *            Adaptador ExcluirMsgComprovanteSalrl
     */
    public void setExcluirMsgComprovanteSalrlPDCAdapter(IExcluirMsgComprovanteSalrlPDCAdapter pdcExcluirMsgComprovanteSalrl) {
        this.pdcExcluirMsgComprovanteSalrl = pdcExcluirMsgComprovanteSalrl;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirMsgComprovanteSalrl.
     * 
     * @return Adaptador IncluirMsgComprovanteSalrl
     */
    public IIncluirMsgComprovanteSalrlPDCAdapter getIncluirMsgComprovanteSalrlPDCAdapter() {
        return pdcIncluirMsgComprovanteSalrl;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirMsgComprovanteSalrl.
     * 
     * @param pdcIncluirMsgComprovanteSalrl
     *            Adaptador IncluirMsgComprovanteSalrl
     */
    public void setIncluirMsgComprovanteSalrlPDCAdapter(IIncluirMsgComprovanteSalrlPDCAdapter pdcIncluirMsgComprovanteSalrl) {
        this.pdcIncluirMsgComprovanteSalrl = pdcIncluirMsgComprovanteSalrl;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetHistContaSalarioDestino.
     * 
     * @return Adaptador ConsultarDetHistContaSalarioDestino
     */
    public IConsultarDetHistContaSalarioDestinoPDCAdapter getConsultarDetHistContaSalarioDestinoPDCAdapter() {
        return pdcConsultarDetHistContaSalarioDestino;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetHistContaSalarioDestino.
     * 
     * @param pdcConsultarDetHistContaSalarioDestino
     *            Adaptador ConsultarDetHistContaSalarioDestino
     */
    public void setConsultarDetHistContaSalarioDestinoPDCAdapter(IConsultarDetHistContaSalarioDestinoPDCAdapter pdcConsultarDetHistContaSalarioDestino) {
        this.pdcConsultarDetHistContaSalarioDestino = pdcConsultarDetHistContaSalarioDestino;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirLinhaMsgComprovante.
     * 
     * @return Adaptador IncluirLinhaMsgComprovante
     */
    public IIncluirLinhaMsgComprovantePDCAdapter getIncluirLinhaMsgComprovantePDCAdapter() {
        return pdcIncluirLinhaMsgComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirLinhaMsgComprovante.
     * 
     * @param pdcIncluirLinhaMsgComprovante
     *            Adaptador IncluirLinhaMsgComprovante
     */
    public void setIncluirLinhaMsgComprovantePDCAdapter(IIncluirLinhaMsgComprovantePDCAdapter pdcIncluirLinhaMsgComprovante) {
        this.pdcIncluirLinhaMsgComprovante = pdcIncluirLinhaMsgComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarLinhaMsgComprovante.
     * 
     * @return Adaptador AlterarLinhaMsgComprovante
     */
    public IAlterarLinhaMsgComprovantePDCAdapter getAlterarLinhaMsgComprovantePDCAdapter() {
        return pdcAlterarLinhaMsgComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarLinhaMsgComprovante.
     * 
     * @param pdcAlterarLinhaMsgComprovante
     *            Adaptador AlterarLinhaMsgComprovante
     */
    public void setAlterarLinhaMsgComprovantePDCAdapter(IAlterarLinhaMsgComprovantePDCAdapter pdcAlterarLinhaMsgComprovante) {
        this.pdcAlterarLinhaMsgComprovante = pdcAlterarLinhaMsgComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirLinhaMsgComprovante.
     * 
     * @return Adaptador ExcluirLinhaMsgComprovante
     */
    public IExcluirLinhaMsgComprovantePDCAdapter getExcluirLinhaMsgComprovantePDCAdapter() {
        return pdcExcluirLinhaMsgComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirLinhaMsgComprovante.
     * 
     * @param pdcExcluirLinhaMsgComprovante
     *            Adaptador ExcluirLinhaMsgComprovante
     */
    public void setExcluirLinhaMsgComprovantePDCAdapter(IExcluirLinhaMsgComprovantePDCAdapter pdcExcluirLinhaMsgComprovante) {
        this.pdcExcluirLinhaMsgComprovante = pdcExcluirLinhaMsgComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoComprovante.
     * 
     * @return Adaptador ListarTipoComprovante
     */
    public IListarTipoComprovantePDCAdapter getListarTipoComprovantePDCAdapter() {
        return pdcListarTipoComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoComprovante.
     * 
     * @param pdcListarTipoComprovante
     *            Adaptador ListarTipoComprovante
     */
    public void setListarTipoComprovantePDCAdapter(IListarTipoComprovantePDCAdapter pdcListarTipoComprovante) {
        this.pdcListarTipoComprovante = pdcListarTipoComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirTipoComprovante.
     * 
     * @return Adaptador IncluirTipoComprovante
     */
    public IIncluirTipoComprovantePDCAdapter getIncluirTipoComprovantePDCAdapter() {
        return pdcIncluirTipoComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirTipoComprovante.
     * 
     * @param pdcIncluirTipoComprovante
     *            Adaptador IncluirTipoComprovante
     */
    public void setIncluirTipoComprovantePDCAdapter(IIncluirTipoComprovantePDCAdapter pdcIncluirTipoComprovante) {
        this.pdcIncluirTipoComprovante = pdcIncluirTipoComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirTipoComprovante.
     * 
     * @return Adaptador ExcluirTipoComprovante
     */
    public IExcluirTipoComprovantePDCAdapter getExcluirTipoComprovantePDCAdapter() {
        return pdcExcluirTipoComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirTipoComprovante.
     * 
     * @param pdcExcluirTipoComprovante
     *            Adaptador ExcluirTipoComprovante
     */
    public void setExcluirTipoComprovantePDCAdapter(IExcluirTipoComprovantePDCAdapter pdcExcluirTipoComprovante) {
        this.pdcExcluirTipoComprovante = pdcExcluirTipoComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarTipoComprovante.
     * 
     * @return Adaptador AlterarTipoComprovante
     */
    public IAlterarTipoComprovantePDCAdapter getAlterarTipoComprovantePDCAdapter() {
        return pdcAlterarTipoComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarTipoComprovante.
     * 
     * @param pdcAlterarTipoComprovante
     *            Adaptador AlterarTipoComprovante
     */
    public void setAlterarTipoComprovantePDCAdapter(IAlterarTipoComprovantePDCAdapter pdcAlterarTipoComprovante) {
        this.pdcAlterarTipoComprovante = pdcAlterarTipoComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharTipoComprovante.
     * 
     * @return Adaptador DetalharTipoComprovante
     */
    public IDetalharTipoComprovantePDCAdapter getDetalharTipoComprovantePDCAdapter() {
        return pdcDetalharTipoComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharTipoComprovante.
     * 
     * @param pdcDetalharTipoComprovante
     *            Adaptador DetalharTipoComprovante
     */
    public void setDetalharTipoComprovantePDCAdapter(IDetalharTipoComprovantePDCAdapter pdcDetalharTipoComprovante) {
        this.pdcDetalharTipoComprovante = pdcDetalharTipoComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarRegraSegregacaoAcesso.
     * 
     * @return Adaptador AlterarRegraSegregacaoAcesso
     */
    public IAlterarRegraSegregacaoAcessoPDCAdapter getAlterarRegraSegregacaoAcessoPDCAdapter() {
        return pdcAlterarRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarRegraSegregacaoAcesso.
     * 
     * @param pdcAlterarRegraSegregacaoAcesso
     *            Adaptador AlterarRegraSegregacaoAcesso
     */
    public void setAlterarRegraSegregacaoAcessoPDCAdapter(IAlterarRegraSegregacaoAcessoPDCAdapter pdcAlterarRegraSegregacaoAcesso) {
        this.pdcAlterarRegraSegregacaoAcesso = pdcAlterarRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharRegraSegregacaoAcesso.
     * 
     * @return Adaptador DetalharRegraSegregacaoAcesso
     */
    public IDetalharRegraSegregacaoAcessoPDCAdapter getDetalharRegraSegregacaoAcessoPDCAdapter() {
        return pdcDetalharRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharRegraSegregacaoAcesso.
     * 
     * @param pdcDetalharRegraSegregacaoAcesso
     *            Adaptador DetalharRegraSegregacaoAcesso
     */
    public void setDetalharRegraSegregacaoAcessoPDCAdapter(IDetalharRegraSegregacaoAcessoPDCAdapter pdcDetalharRegraSegregacaoAcesso) {
        this.pdcDetalharRegraSegregacaoAcesso = pdcDetalharRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirRegraSegregacaoAcesso.
     * 
     * @return Adaptador ExcluirRegraSegregacaoAcesso
     */
    public IExcluirRegraSegregacaoAcessoPDCAdapter getExcluirRegraSegregacaoAcessoPDCAdapter() {
        return pdcExcluirRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirRegraSegregacaoAcesso.
     * 
     * @param pdcExcluirRegraSegregacaoAcesso
     *            Adaptador ExcluirRegraSegregacaoAcesso
     */
    public void setExcluirRegraSegregacaoAcessoPDCAdapter(IExcluirRegraSegregacaoAcessoPDCAdapter pdcExcluirRegraSegregacaoAcesso) {
        this.pdcExcluirRegraSegregacaoAcesso = pdcExcluirRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirRegraSegregacaoAcesso.
     * 
     * @return Adaptador IncluirRegraSegregacaoAcesso
     */
    public IIncluirRegraSegregacaoAcessoPDCAdapter getIncluirRegraSegregacaoAcessoPDCAdapter() {
        return pdcIncluirRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirRegraSegregacaoAcesso.
     * 
     * @param pdcIncluirRegraSegregacaoAcesso
     *            Adaptador IncluirRegraSegregacaoAcesso
     */
    public void setIncluirRegraSegregacaoAcessoPDCAdapter(IIncluirRegraSegregacaoAcessoPDCAdapter pdcIncluirRegraSegregacaoAcesso) {
        this.pdcIncluirRegraSegregacaoAcesso = pdcIncluirRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador ListarRegraSegregacaoAcesso.
     * 
     * @return Adaptador ListarRegraSegregacaoAcesso
     */
    public IListarRegraSegregacaoAcessoPDCAdapter getListarRegraSegregacaoAcessoPDCAdapter() {
        return pdcListarRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarRegraSegregacaoAcesso.
     * 
     * @param pdcListarRegraSegregacaoAcesso
     *            Adaptador ListarRegraSegregacaoAcesso
     */
    public void setListarRegraSegregacaoAcessoPDCAdapter(IListarRegraSegregacaoAcessoPDCAdapter pdcListarRegraSegregacaoAcesso) {
        this.pdcListarRegraSegregacaoAcesso = pdcListarRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMsgAlertaGestor.
     * 
     * @return Adaptador ListarMsgAlertaGestor
     */
    public IListarMsgAlertaGestorPDCAdapter getListarMsgAlertaGestorPDCAdapter() {
        return pdcListarMsgAlertaGestor;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMsgAlertaGestor.
     * 
     * @param pdcListarMsgAlertaGestor
     *            Adaptador ListarMsgAlertaGestor
     */
    public void setListarMsgAlertaGestorPDCAdapter(IListarMsgAlertaGestorPDCAdapter pdcListarMsgAlertaGestor) {
        this.pdcListarMsgAlertaGestor = pdcListarMsgAlertaGestor;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaFuncionarioBradesco.
     * 
     * @return Adaptador ConsultarListaFuncionarioBradesco
     */
    public IConsultarListaFuncionarioBradescoPDCAdapter getConsultarListaFuncionarioBradescoPDCAdapter() {
        return pdcConsultarListaFuncionarioBradesco;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaFuncionarioBradesco.
     * 
     * @param pdcConsultarListaFuncionarioBradesco
     *            Adaptador ConsultarListaFuncionarioBradesco
     */
    public void setConsultarListaFuncionarioBradescoPDCAdapter(IConsultarListaFuncionarioBradescoPDCAdapter pdcConsultarListaFuncionarioBradesco) {
        this.pdcConsultarListaFuncionarioBradesco = pdcConsultarListaFuncionarioBradesco;
    }
    /**
     * M�todo invocado para obter um adaptador ListarRecursoCanalMsg.
     * 
     * @return Adaptador ListarRecursoCanalMsg
     */
    public IListarRecursoCanalMsgPDCAdapter getListarRecursoCanalMsgPDCAdapter() {
        return pdcListarRecursoCanalMsg;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarRecursoCanalMsg.
     * 
     * @param pdcListarRecursoCanalMsg
     *            Adaptador ListarRecursoCanalMsg
     */
    public void setListarRecursoCanalMsgPDCAdapter(IListarRecursoCanalMsgPDCAdapter pdcListarRecursoCanalMsg) {
        this.pdcListarRecursoCanalMsg = pdcListarRecursoCanalMsg;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAssLoteOperacaoServico.
     * 
     * @return Adaptador IncluirAssLoteOperacaoServico
     */
    public IIncluirAssLoteOperacaoServicoPDCAdapter getIncluirAssLoteOperacaoServicoPDCAdapter() {
        return pdcIncluirAssLoteOperacaoServico;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAssLoteOperacaoServico.
     * 
     * @param pdcIncluirAssLoteOperacaoServico
     *            Adaptador IncluirAssLoteOperacaoServico
     */
    public void setIncluirAssLoteOperacaoServicoPDCAdapter(IIncluirAssLoteOperacaoServicoPDCAdapter pdcIncluirAssLoteOperacaoServico) {
        this.pdcIncluirAssLoteOperacaoServico = pdcIncluirAssLoteOperacaoServico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalheMsgComprovanteSalrl.
     * 
     * @return Adaptador ConsultarDetalheMsgComprovanteSalrl
     */
    public IConsultarDetalheMsgComprovanteSalrlPDCAdapter getConsultarDetalheMsgComprovanteSalrlPDCAdapter() {
        return pdcConsultarDetalheMsgComprovanteSalrl;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalheMsgComprovanteSalrl.
     * 
     * @param pdcConsultarDetalheMsgComprovanteSalrl
     *            Adaptador ConsultarDetalheMsgComprovanteSalrl
     */
    public void setConsultarDetalheMsgComprovanteSalrlPDCAdapter(IConsultarDetalheMsgComprovanteSalrlPDCAdapter pdcConsultarDetalheMsgComprovanteSalrl) {
        this.pdcConsultarDetalheMsgComprovanteSalrl = pdcConsultarDetalheMsgComprovanteSalrl;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAssociacaoContratoMsg.
     * 
     * @return Adaptador ListarAssociacaoContratoMsg
     */
    public IListarAssociacaoContratoMsgPDCAdapter getListarAssociacaoContratoMsgPDCAdapter() {
        return pdcListarAssociacaoContratoMsg;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAssociacaoContratoMsg.
     * 
     * @param pdcListarAssociacaoContratoMsg
     *            Adaptador ListarAssociacaoContratoMsg
     */
    public void setListarAssociacaoContratoMsgPDCAdapter(IListarAssociacaoContratoMsgPDCAdapter pdcListarAssociacaoContratoMsg) {
        this.pdcListarAssociacaoContratoMsg = pdcListarAssociacaoContratoMsg;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAssociacaoContratoMsg.
     * 
     * @return Adaptador IncluirAssociacaoContratoMsg
     */
    public IIncluirAssociacaoContratoMsgPDCAdapter getIncluirAssociacaoContratoMsgPDCAdapter() {
        return pdcIncluirAssociacaoContratoMsg;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAssociacaoContratoMsg.
     * 
     * @param pdcIncluirAssociacaoContratoMsg
     *            Adaptador IncluirAssociacaoContratoMsg
     */
    public void setIncluirAssociacaoContratoMsgPDCAdapter(IIncluirAssociacaoContratoMsgPDCAdapter pdcIncluirAssociacaoContratoMsg) {
        this.pdcIncluirAssociacaoContratoMsg = pdcIncluirAssociacaoContratoMsg;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAssociacaoContratoMsg.
     * 
     * @return Adaptador ExcluirAssociacaoContratoMsg
     */
    public IExcluirAssociacaoContratoMsgPDCAdapter getExcluirAssociacaoContratoMsgPDCAdapter() {
        return pdcExcluirAssociacaoContratoMsg;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAssociacaoContratoMsg.
     * 
     * @param pdcExcluirAssociacaoContratoMsg
     *            Adaptador ExcluirAssociacaoContratoMsg
     */
    public void setExcluirAssociacaoContratoMsgPDCAdapter(IExcluirAssociacaoContratoMsgPDCAdapter pdcExcluirAssociacaoContratoMsg) {
        this.pdcExcluirAssociacaoContratoMsg = pdcExcluirAssociacaoContratoMsg;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalheContratoMsg.
     * 
     * @return Adaptador ConsultarDetalheContratoMsg
     */
    public IConsultarDetalheContratoMsgPDCAdapter getConsultarDetalheContratoMsgPDCAdapter() {
        return pdcConsultarDetalheContratoMsg;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalheContratoMsg.
     * 
     * @param pdcConsultarDetalheContratoMsg
     *            Adaptador ConsultarDetalheContratoMsg
     */
    public void setConsultarDetalheContratoMsgPDCAdapter(IConsultarDetalheContratoMsgPDCAdapter pdcConsultarDetalheContratoMsg) {
        this.pdcConsultarDetalheContratoMsg = pdcConsultarDetalheContratoMsg;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDescricaoUnidadeOrganizacional.
     * 
     * @return Adaptador ConsultarDescricaoUnidadeOrganizacional
     */
    public IConsultarDescricaoUnidadeOrganizacionalPDCAdapter getConsultarDescricaoUnidadeOrganizacionalPDCAdapter() {
        return pdcConsultarDescricaoUnidadeOrganizacional;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDescricaoUnidadeOrganizacional.
     * 
     * @param pdcConsultarDescricaoUnidadeOrganizacional
     *            Adaptador ConsultarDescricaoUnidadeOrganizacional
     */
    public void setConsultarDescricaoUnidadeOrganizacionalPDCAdapter(IConsultarDescricaoUnidadeOrganizacionalPDCAdapter pdcConsultarDescricaoUnidadeOrganizacional) {
        this.pdcConsultarDescricaoUnidadeOrganizacional = pdcConsultarDescricaoUnidadeOrganizacional;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarMsgLancamentoPersonalizado.
     * 
     * @return Adaptador ConsultarMsgLancamentoPersonalizado
     */
    public IConsultarMsgLancamentoPersonalizadoPDCAdapter getConsultarMsgLancamentoPersonalizadoPDCAdapter() {
        return pdcConsultarMsgLancamentoPersonalizado;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarMsgLancamentoPersonalizado.
     * 
     * @param pdcConsultarMsgLancamentoPersonalizado
     *            Adaptador ConsultarMsgLancamentoPersonalizado
     */
    public void setConsultarMsgLancamentoPersonalizadoPDCAdapter(IConsultarMsgLancamentoPersonalizadoPDCAdapter pdcConsultarMsgLancamentoPersonalizado) {
        this.pdcConsultarMsgLancamentoPersonalizado = pdcConsultarMsgLancamentoPersonalizado;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarMsgLancamentoPersonalizado.
     * 
     * @return Adaptador AlterarMsgLancamentoPersonalizado
     */
    public IAlterarMsgLancamentoPersonalizadoPDCAdapter getAlterarMsgLancamentoPersonalizadoPDCAdapter() {
        return pdcAlterarMsgLancamentoPersonalizado;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarMsgLancamentoPersonalizado.
     * 
     * @param pdcAlterarMsgLancamentoPersonalizado
     *            Adaptador AlterarMsgLancamentoPersonalizado
     */
    public void setAlterarMsgLancamentoPersonalizadoPDCAdapter(IAlterarMsgLancamentoPersonalizadoPDCAdapter pdcAlterarMsgLancamentoPersonalizado) {
        this.pdcAlterarMsgLancamentoPersonalizado = pdcAlterarMsgLancamentoPersonalizado;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharMsgLancamentoPersonalizado.
     * 
     * @return Adaptador DetalharMsgLancamentoPersonalizado
     */
    public IDetalharMsgLancamentoPersonalizadoPDCAdapter getDetalharMsgLancamentoPersonalizadoPDCAdapter() {
        return pdcDetalharMsgLancamentoPersonalizado;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharMsgLancamentoPersonalizado.
     * 
     * @param pdcDetalharMsgLancamentoPersonalizado
     *            Adaptador DetalharMsgLancamentoPersonalizado
     */
    public void setDetalharMsgLancamentoPersonalizadoPDCAdapter(IDetalharMsgLancamentoPersonalizadoPDCAdapter pdcDetalharMsgLancamentoPersonalizado) {
        this.pdcDetalharMsgLancamentoPersonalizado = pdcDetalharMsgLancamentoPersonalizado;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirMsgLancamentoPersonalizado.
     * 
     * @return Adaptador ExcluirMsgLancamentoPersonalizado
     */
    public IExcluirMsgLancamentoPersonalizadoPDCAdapter getExcluirMsgLancamentoPersonalizadoPDCAdapter() {
        return pdcExcluirMsgLancamentoPersonalizado;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirMsgLancamentoPersonalizado.
     * 
     * @param pdcExcluirMsgLancamentoPersonalizado
     *            Adaptador ExcluirMsgLancamentoPersonalizado
     */
    public void setExcluirMsgLancamentoPersonalizadoPDCAdapter(IExcluirMsgLancamentoPersonalizadoPDCAdapter pdcExcluirMsgLancamentoPersonalizado) {
        this.pdcExcluirMsgLancamentoPersonalizado = pdcExcluirMsgLancamentoPersonalizado;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirMsgLancamentoPersonalizado.
     * 
     * @return Adaptador IncluirMsgLancamentoPersonalizado
     */
    public IIncluirMsgLancamentoPersonalizadoPDCAdapter getIncluirMsgLancamentoPersonalizadoPDCAdapter() {
        return pdcIncluirMsgLancamentoPersonalizado;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirMsgLancamentoPersonalizado.
     * 
     * @param pdcIncluirMsgLancamentoPersonalizado
     *            Adaptador IncluirMsgLancamentoPersonalizado
     */
    public void setIncluirMsgLancamentoPersonalizadoPDCAdapter(IIncluirMsgLancamentoPersonalizadoPDCAdapter pdcIncluirMsgLancamentoPersonalizado) {
        this.pdcIncluirMsgLancamentoPersonalizado = pdcIncluirMsgLancamentoPersonalizado;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistManutMsgLancPersonalizado.
     * 
     * @return Adaptador DetalharHistManutMsgLancPersonalizado
     */
    public IDetalharHistManutMsgLancPersonalizadoPDCAdapter getDetalharHistManutMsgLancPersonalizadoPDCAdapter() {
        return pdcDetalharHistManutMsgLancPersonalizado;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistManutMsgLancPersonalizado.
     * 
     * @param pdcDetalharHistManutMsgLancPersonalizado
     *            Adaptador DetalharHistManutMsgLancPersonalizado
     */
    public void setDetalharHistManutMsgLancPersonalizadoPDCAdapter(IDetalharHistManutMsgLancPersonalizadoPDCAdapter pdcDetalharHistManutMsgLancPersonalizado) {
        this.pdcDetalharHistManutMsgLancPersonalizado = pdcDetalharHistManutMsgLancPersonalizado;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarHistManutMsgLancPersonalizado.
     * 
     * @return Adaptador ConsultarHistManutMsgLancPersonalizado
     */
    public IConsultarHistManutMsgLancPersonalizadoPDCAdapter getConsultarHistManutMsgLancPersonalizadoPDCAdapter() {
        return pdcConsultarHistManutMsgLancPersonalizado;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarHistManutMsgLancPersonalizado.
     * 
     * @param pdcConsultarHistManutMsgLancPersonalizado
     *            Adaptador ConsultarHistManutMsgLancPersonalizado
     */
    public void setConsultarHistManutMsgLancPersonalizadoPDCAdapter(IConsultarHistManutMsgLancPersonalizadoPDCAdapter pdcConsultarHistManutMsgLancPersonalizado) {
        this.pdcConsultarHistManutMsgLancPersonalizado = pdcConsultarHistManutMsgLancPersonalizado;
    }
    /**
     * M�todo invocado para obter um adaptador ListarVinculoCtaSalarioEmpresa.
     * 
     * @return Adaptador ListarVinculoCtaSalarioEmpresa
     */
    public IListarVinculoCtaSalarioEmpresaPDCAdapter getListarVinculoCtaSalarioEmpresaPDCAdapter() {
        return pdcListarVinculoCtaSalarioEmpresa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarVinculoCtaSalarioEmpresa.
     * 
     * @param pdcListarVinculoCtaSalarioEmpresa
     *            Adaptador ListarVinculoCtaSalarioEmpresa
     */
    public void setListarVinculoCtaSalarioEmpresaPDCAdapter(IListarVinculoCtaSalarioEmpresaPDCAdapter pdcListarVinculoCtaSalarioEmpresa) {
        this.pdcListarVinculoCtaSalarioEmpresa = pdcListarVinculoCtaSalarioEmpresa;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirVinculoContaSalarioDestino.
     * 
     * @return Adaptador IncluirVinculoContaSalarioDestino
     */
    public IIncluirVinculoContaSalarioDestinoPDCAdapter getIncluirVinculoContaSalarioDestinoPDCAdapter() {
        return pdcIncluirVinculoContaSalarioDestino;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirVinculoContaSalarioDestino.
     * 
     * @param pdcIncluirVinculoContaSalarioDestino
     *            Adaptador IncluirVinculoContaSalarioDestino
     */
    public void setIncluirVinculoContaSalarioDestinoPDCAdapter(IIncluirVinculoContaSalarioDestinoPDCAdapter pdcIncluirVinculoContaSalarioDestino) {
        this.pdcIncluirVinculoContaSalarioDestino = pdcIncluirVinculoContaSalarioDestino;
    }
    /**
     * M�todo invocado para obter um adaptador ListarExcecaoRegraSegregacaoAcesso.
     * 
     * @return Adaptador ListarExcecaoRegraSegregacaoAcesso
     */
    public IListarExcecaoRegraSegregacaoAcessoPDCAdapter getListarExcecaoRegraSegregacaoAcessoPDCAdapter() {
        return pdcListarExcecaoRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarExcecaoRegraSegregacaoAcesso.
     * 
     * @param pdcListarExcecaoRegraSegregacaoAcesso
     *            Adaptador ListarExcecaoRegraSegregacaoAcesso
     */
    public void setListarExcecaoRegraSegregacaoAcessoPDCAdapter(IListarExcecaoRegraSegregacaoAcessoPDCAdapter pdcListarExcecaoRegraSegregacaoAcesso) {
        this.pdcListarExcecaoRegraSegregacaoAcesso = pdcListarExcecaoRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharExcecaoRegraSegregacaoAcesso.
     * 
     * @return Adaptador DetalharExcecaoRegraSegregacaoAcesso
     */
    public IDetalharExcecaoRegraSegregacaoAcessoPDCAdapter getDetalharExcecaoRegraSegregacaoAcessoPDCAdapter() {
        return pdcDetalharExcecaoRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharExcecaoRegraSegregacaoAcesso.
     * 
     * @param pdcDetalharExcecaoRegraSegregacaoAcesso
     *            Adaptador DetalharExcecaoRegraSegregacaoAcesso
     */
    public void setDetalharExcecaoRegraSegregacaoAcessoPDCAdapter(IDetalharExcecaoRegraSegregacaoAcessoPDCAdapter pdcDetalharExcecaoRegraSegregacaoAcesso) {
        this.pdcDetalharExcecaoRegraSegregacaoAcesso = pdcDetalharExcecaoRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador ListarLinhaMsgComprovante.
     * 
     * @return Adaptador ListarLinhaMsgComprovante
     */
    public IListarLinhaMsgComprovantePDCAdapter getListarLinhaMsgComprovantePDCAdapter() {
        return pdcListarLinhaMsgComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarLinhaMsgComprovante.
     * 
     * @param pdcListarLinhaMsgComprovante
     *            Adaptador ListarLinhaMsgComprovante
     */
    public void setListarLinhaMsgComprovantePDCAdapter(IListarLinhaMsgComprovantePDCAdapter pdcListarLinhaMsgComprovante) {
        this.pdcListarLinhaMsgComprovante = pdcListarLinhaMsgComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalheLinhaMensagem.
     * 
     * @return Adaptador ConsultarDetalheLinhaMensagem
     */
    public IConsultarDetalheLinhaMensagemPDCAdapter getConsultarDetalheLinhaMensagemPDCAdapter() {
        return pdcConsultarDetalheLinhaMensagem;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalheLinhaMensagem.
     * 
     * @param pdcConsultarDetalheLinhaMensagem
     *            Adaptador ConsultarDetalheLinhaMensagem
     */
    public void setConsultarDetalheLinhaMensagemPDCAdapter(IConsultarDetalheLinhaMensagemPDCAdapter pdcConsultarDetalheLinhaMensagem) {
        this.pdcConsultarDetalheLinhaMensagem = pdcConsultarDetalheLinhaMensagem;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirExcecaoRegraSegregacaoAcesso.
     * 
     * @return Adaptador IncluirExcecaoRegraSegregacaoAcesso
     */
    public IIncluirExcecaoRegraSegregacaoAcessoPDCAdapter getIncluirExcecaoRegraSegregacaoAcessoPDCAdapter() {
        return pdcIncluirExcecaoRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirExcecaoRegraSegregacaoAcesso.
     * 
     * @param pdcIncluirExcecaoRegraSegregacaoAcesso
     *            Adaptador IncluirExcecaoRegraSegregacaoAcesso
     */
    public void setIncluirExcecaoRegraSegregacaoAcessoPDCAdapter(IIncluirExcecaoRegraSegregacaoAcessoPDCAdapter pdcIncluirExcecaoRegraSegregacaoAcesso) {
        this.pdcIncluirExcecaoRegraSegregacaoAcesso = pdcIncluirExcecaoRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarExcecaoRegraSegregacaoAcesso.
     * 
     * @return Adaptador AlterarExcecaoRegraSegregacaoAcesso
     */
    public IAlterarExcecaoRegraSegregacaoAcessoPDCAdapter getAlterarExcecaoRegraSegregacaoAcessoPDCAdapter() {
        return pdcAlterarExcecaoRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarExcecaoRegraSegregacaoAcesso.
     * 
     * @param pdcAlterarExcecaoRegraSegregacaoAcesso
     *            Adaptador AlterarExcecaoRegraSegregacaoAcesso
     */
    public void setAlterarExcecaoRegraSegregacaoAcessoPDCAdapter(IAlterarExcecaoRegraSegregacaoAcessoPDCAdapter pdcAlterarExcecaoRegraSegregacaoAcesso) {
        this.pdcAlterarExcecaoRegraSegregacaoAcesso = pdcAlterarExcecaoRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirExcecaoRegraSegregacaoAcesso.
     * 
     * @return Adaptador ExcluirExcecaoRegraSegregacaoAcesso
     */
    public IExcluirExcecaoRegraSegregacaoAcessoPDCAdapter getExcluirExcecaoRegraSegregacaoAcessoPDCAdapter() {
        return pdcExcluirExcecaoRegraSegregacaoAcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirExcecaoRegraSegregacaoAcesso.
     * 
     * @param pdcExcluirExcecaoRegraSegregacaoAcesso
     *            Adaptador ExcluirExcecaoRegraSegregacaoAcesso
     */
    public void setExcluirExcecaoRegraSegregacaoAcessoPDCAdapter(IExcluirExcecaoRegraSegregacaoAcessoPDCAdapter pdcExcluirExcecaoRegraSegregacaoAcesso) {
        this.pdcExcluirExcecaoRegraSegregacaoAcesso = pdcExcluirExcecaoRegraSegregacaoAcesso;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirMsgAlertaGestor.
     * 
     * @return Adaptador IncluirMsgAlertaGestor
     */
    public IIncluirMsgAlertaGestorPDCAdapter getIncluirMsgAlertaGestorPDCAdapter() {
        return pdcIncluirMsgAlertaGestor;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirMsgAlertaGestor.
     * 
     * @param pdcIncluirMsgAlertaGestor
     *            Adaptador IncluirMsgAlertaGestor
     */
    public void setIncluirMsgAlertaGestorPDCAdapter(IIncluirMsgAlertaGestorPDCAdapter pdcIncluirMsgAlertaGestor) {
        this.pdcIncluirMsgAlertaGestor = pdcIncluirMsgAlertaGestor;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetVinculoCtaSalarioEmpresa.
     * 
     * @return Adaptador ConsultarDetVinculoCtaSalarioEmpresa
     */
    public IConsultarDetVinculoCtaSalarioEmpresaPDCAdapter getConsultarDetVinculoCtaSalarioEmpresaPDCAdapter() {
        return pdcConsultarDetVinculoCtaSalarioEmpresa;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetVinculoCtaSalarioEmpresa.
     * 
     * @param pdcConsultarDetVinculoCtaSalarioEmpresa
     *            Adaptador ConsultarDetVinculoCtaSalarioEmpresa
     */
    public void setConsultarDetVinculoCtaSalarioEmpresaPDCAdapter(IConsultarDetVinculoCtaSalarioEmpresaPDCAdapter pdcConsultarDetVinculoCtaSalarioEmpresa) {
        this.pdcConsultarDetVinculoCtaSalarioEmpresa = pdcConsultarDetVinculoCtaSalarioEmpresa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalheVinculoContaSalarioDestino.
     * 
     * @return Adaptador ConsultarDetalheVinculoContaSalarioDestino
     */
    public IConsultarDetalheVinculoContaSalarioDestinoPDCAdapter getConsultarDetalheVinculoContaSalarioDestinoPDCAdapter() {
        return pdcConsultarDetalheVinculoContaSalarioDestino;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalheVinculoContaSalarioDestino.
     * 
     * @param pdcConsultarDetalheVinculoContaSalarioDestino
     *            Adaptador ConsultarDetalheVinculoContaSalarioDestino
     */
    public void setConsultarDetalheVinculoContaSalarioDestinoPDCAdapter(IConsultarDetalheVinculoContaSalarioDestinoPDCAdapter pdcConsultarDetalheVinculoContaSalarioDestino) {
        this.pdcConsultarDetalheVinculoContaSalarioDestino = pdcConsultarDetalheVinculoContaSalarioDestino;
    }
    /**
     * M�todo invocado para obter um adaptador SubstituirVinculoCtaSalarioEmpresa.
     * 
     * @return Adaptador SubstituirVinculoCtaSalarioEmpresa
     */
    public ISubstituirVinculoCtaSalarioEmpresaPDCAdapter getSubstituirVinculoCtaSalarioEmpresaPDCAdapter() {
        return pdcSubstituirVinculoCtaSalarioEmpresa;
    }

    /**
     * M�todo invocado para establecer um adaptador SubstituirVinculoCtaSalarioEmpresa.
     * 
     * @param pdcSubstituirVinculoCtaSalarioEmpresa
     *            Adaptador SubstituirVinculoCtaSalarioEmpresa
     */
    public void setSubstituirVinculoCtaSalarioEmpresaPDCAdapter(ISubstituirVinculoCtaSalarioEmpresaPDCAdapter pdcSubstituirVinculoCtaSalarioEmpresa) {
        this.pdcSubstituirVinculoCtaSalarioEmpresa = pdcSubstituirVinculoCtaSalarioEmpresa;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirPrioridadeTipoCompromisso.
     * 
     * @return Adaptador ExcluirPrioridadeTipoCompromisso
     */
    public IExcluirPrioridadeTipoCompromissoPDCAdapter getExcluirPrioridadeTipoCompromissoPDCAdapter() {
        return pdcExcluirPrioridadeTipoCompromisso;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirPrioridadeTipoCompromisso.
     * 
     * @param pdcExcluirPrioridadeTipoCompromisso
     *            Adaptador ExcluirPrioridadeTipoCompromisso
     */
    public void setExcluirPrioridadeTipoCompromissoPDCAdapter(IExcluirPrioridadeTipoCompromissoPDCAdapter pdcExcluirPrioridadeTipoCompromisso) {
        this.pdcExcluirPrioridadeTipoCompromisso = pdcExcluirPrioridadeTipoCompromisso;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarMsgAlertaGestor.
     * 
     * @return Adaptador AlterarMsgAlertaGestor
     */
    public IAlterarMsgAlertaGestorPDCAdapter getAlterarMsgAlertaGestorPDCAdapter() {
        return pdcAlterarMsgAlertaGestor;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarMsgAlertaGestor.
     * 
     * @param pdcAlterarMsgAlertaGestor
     *            Adaptador AlterarMsgAlertaGestor
     */
    public void setAlterarMsgAlertaGestorPDCAdapter(IAlterarMsgAlertaGestorPDCAdapter pdcAlterarMsgAlertaGestor) {
        this.pdcAlterarMsgAlertaGestor = pdcAlterarMsgAlertaGestor;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirMsgAlertaGestor.
     * 
     * @return Adaptador ExcluirMsgAlertaGestor
     */
    public IExcluirMsgAlertaGestorPDCAdapter getExcluirMsgAlertaGestorPDCAdapter() {
        return pdcExcluirMsgAlertaGestor;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirMsgAlertaGestor.
     * 
     * @param pdcExcluirMsgAlertaGestor
     *            Adaptador ExcluirMsgAlertaGestor
     */
    public void setExcluirMsgAlertaGestorPDCAdapter(IExcluirMsgAlertaGestorPDCAdapter pdcExcluirMsgAlertaGestor) {
        this.pdcExcluirMsgAlertaGestor = pdcExcluirMsgAlertaGestor;
    }
    /**
     * M�todo invocado para obter um adaptador ListarVincMsgHistComplementarOperacao.
     * 
     * @return Adaptador ListarVincMsgHistComplementarOperacao
     */
    public IListarVincMsgHistComplementarOperacaoPDCAdapter getListarVincMsgHistComplementarOperacaoPDCAdapter() {
        return pdcListarVincMsgHistComplementarOperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarVincMsgHistComplementarOperacao.
     * 
     * @param pdcListarVincMsgHistComplementarOperacao
     *            Adaptador ListarVincMsgHistComplementarOperacao
     */
    public void setListarVincMsgHistComplementarOperacaoPDCAdapter(IListarVincMsgHistComplementarOperacaoPDCAdapter pdcListarVincMsgHistComplementarOperacao) {
        this.pdcListarVincMsgHistComplementarOperacao = pdcListarVincMsgHistComplementarOperacao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirVincMsgHistComplementarOperacao.
     * 
     * @return Adaptador IncluirVincMsgHistComplementarOperacao
     */
    public IIncluirVincMsgHistComplementarOperacaoPDCAdapter getIncluirVincMsgHistComplementarOperacaoPDCAdapter() {
        return pdcIncluirVincMsgHistComplementarOperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirVincMsgHistComplementarOperacao.
     * 
     * @param pdcIncluirVincMsgHistComplementarOperacao
     *            Adaptador IncluirVincMsgHistComplementarOperacao
     */
    public void setIncluirVincMsgHistComplementarOperacaoPDCAdapter(IIncluirVincMsgHistComplementarOperacaoPDCAdapter pdcIncluirVincMsgHistComplementarOperacao) {
        this.pdcIncluirVincMsgHistComplementarOperacao = pdcIncluirVincMsgHistComplementarOperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirVincMsgHistComplementarOperacao.
     * 
     * @return Adaptador ExcluirVincMsgHistComplementarOperacao
     */
    public IExcluirVincMsgHistComplementarOperacaoPDCAdapter getExcluirVincMsgHistComplementarOperacaoPDCAdapter() {
        return pdcExcluirVincMsgHistComplementarOperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirVincMsgHistComplementarOperacao.
     * 
     * @param pdcExcluirVincMsgHistComplementarOperacao
     *            Adaptador ExcluirVincMsgHistComplementarOperacao
     */
    public void setExcluirVincMsgHistComplementarOperacaoPDCAdapter(IExcluirVincMsgHistComplementarOperacaoPDCAdapter pdcExcluirVincMsgHistComplementarOperacao) {
        this.pdcExcluirVincMsgHistComplementarOperacao = pdcExcluirVincMsgHistComplementarOperacao;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharVincMsgHistComplementarOperacao.
     * 
     * @return Adaptador DetalharVincMsgHistComplementarOperacao
     */
    public IDetalharVincMsgHistComplementarOperacaoPDCAdapter getDetalharVincMsgHistComplementarOperacaoPDCAdapter() {
        return pdcDetalharVincMsgHistComplementarOperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharVincMsgHistComplementarOperacao.
     * 
     * @param pdcDetalharVincMsgHistComplementarOperacao
     *            Adaptador DetalharVincMsgHistComplementarOperacao
     */
    public void setDetalharVincMsgHistComplementarOperacaoPDCAdapter(IDetalharVincMsgHistComplementarOperacaoPDCAdapter pdcDetalharVincMsgHistComplementarOperacao) {
        this.pdcDetalharVincMsgHistComplementarOperacao = pdcDetalharVincMsgHistComplementarOperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMsgHistoricoComplementar.
     * 
     * @return Adaptador ListarMsgHistoricoComplementar
     */
    public IListarMsgHistoricoComplementarPDCAdapter getListarMsgHistoricoComplementarPDCAdapter() {
        return pdcListarMsgHistoricoComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMsgHistoricoComplementar.
     * 
     * @param pdcListarMsgHistoricoComplementar
     *            Adaptador ListarMsgHistoricoComplementar
     */
    public void setListarMsgHistoricoComplementarPDCAdapter(IListarMsgHistoricoComplementarPDCAdapter pdcListarMsgHistoricoComplementar) {
        this.pdcListarMsgHistoricoComplementar = pdcListarMsgHistoricoComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirVincMsgLancOper.
     * 
     * @return Adaptador ExcluirVincMsgLancOper
     */
    public IExcluirVincMsgLancOperPDCAdapter getExcluirVincMsgLancOperPDCAdapter() {
        return pdcExcluirVincMsgLancOper;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirVincMsgLancOper.
     * 
     * @param pdcExcluirVincMsgLancOper
     *            Adaptador ExcluirVincMsgLancOper
     */
    public void setExcluirVincMsgLancOperPDCAdapter(IExcluirVincMsgLancOperPDCAdapter pdcExcluirVincMsgLancOper) {
        this.pdcExcluirVincMsgLancOper = pdcExcluirVincMsgLancOper;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharMsgAlertaGestor.
     * 
     * @return Adaptador DetalharMsgAlertaGestor
     */
    public IDetalharMsgAlertaGestorPDCAdapter getDetalharMsgAlertaGestorPDCAdapter() {
        return pdcDetalharMsgAlertaGestor;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharMsgAlertaGestor.
     * 
     * @param pdcDetalharMsgAlertaGestor
     *            Adaptador DetalharMsgAlertaGestor
     */
    public void setDetalharMsgAlertaGestorPDCAdapter(IDetalharMsgAlertaGestorPDCAdapter pdcDetalharMsgAlertaGestor) {
        this.pdcDetalharMsgAlertaGestor = pdcDetalharMsgAlertaGestor;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarMsgHistoricoComplementar.
     * 
     * @return Adaptador AlterarMsgHistoricoComplementar
     */
    public IAlterarMsgHistoricoComplementarPDCAdapter getAlterarMsgHistoricoComplementarPDCAdapter() {
        return pdcAlterarMsgHistoricoComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarMsgHistoricoComplementar.
     * 
     * @param pdcAlterarMsgHistoricoComplementar
     *            Adaptador AlterarMsgHistoricoComplementar
     */
    public void setAlterarMsgHistoricoComplementarPDCAdapter(IAlterarMsgHistoricoComplementarPDCAdapter pdcAlterarMsgHistoricoComplementar) {
        this.pdcAlterarMsgHistoricoComplementar = pdcAlterarMsgHistoricoComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharMsgHistoricoComplementar.
     * 
     * @return Adaptador DetalharMsgHistoricoComplementar
     */
    public IDetalharMsgHistoricoComplementarPDCAdapter getDetalharMsgHistoricoComplementarPDCAdapter() {
        return pdcDetalharMsgHistoricoComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharMsgHistoricoComplementar.
     * 
     * @param pdcDetalharMsgHistoricoComplementar
     *            Adaptador DetalharMsgHistoricoComplementar
     */
    public void setDetalharMsgHistoricoComplementarPDCAdapter(IDetalharMsgHistoricoComplementarPDCAdapter pdcDetalharMsgHistoricoComplementar) {
        this.pdcDetalharMsgHistoricoComplementar = pdcDetalharMsgHistoricoComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirMsgHistoricoComplementar.
     * 
     * @return Adaptador ExcluirMsgHistoricoComplementar
     */
    public IExcluirMsgHistoricoComplementarPDCAdapter getExcluirMsgHistoricoComplementarPDCAdapter() {
        return pdcExcluirMsgHistoricoComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirMsgHistoricoComplementar.
     * 
     * @param pdcExcluirMsgHistoricoComplementar
     *            Adaptador ExcluirMsgHistoricoComplementar
     */
    public void setExcluirMsgHistoricoComplementarPDCAdapter(IExcluirMsgHistoricoComplementarPDCAdapter pdcExcluirMsgHistoricoComplementar) {
        this.pdcExcluirMsgHistoricoComplementar = pdcExcluirMsgHistoricoComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirMsgHistoricoComplementar.
     * 
     * @return Adaptador IncluirMsgHistoricoComplementar
     */
    public IIncluirMsgHistoricoComplementarPDCAdapter getIncluirMsgHistoricoComplementarPDCAdapter() {
        return pdcIncluirMsgHistoricoComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirMsgHistoricoComplementar.
     * 
     * @param pdcIncluirMsgHistoricoComplementar
     *            Adaptador IncluirMsgHistoricoComplementar
     */
    public void setIncluirMsgHistoricoComplementarPDCAdapter(IIncluirMsgHistoricoComplementarPDCAdapter pdcIncluirMsgHistoricoComplementar) {
        this.pdcIncluirMsgHistoricoComplementar = pdcIncluirMsgHistoricoComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAlcada.
     * 
     * @return Adaptador ListarAlcada
     */
    public IListarAlcadaPDCAdapter getListarAlcadaPDCAdapter() {
        return pdcListarAlcada;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAlcada.
     * 
     * @param pdcListarAlcada
     *            Adaptador ListarAlcada
     */
    public void setListarAlcadaPDCAdapter(IListarAlcadaPDCAdapter pdcListarAlcada) {
        this.pdcListarAlcada = pdcListarAlcada;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarVinculoContaSalarioDestino.
     * 
     * @return Adaptador AlterarVinculoContaSalarioDestino
     */
    public IAlterarVinculoContaSalarioDestinoPDCAdapter getAlterarVinculoContaSalarioDestinoPDCAdapter() {
        return pdcAlterarVinculoContaSalarioDestino;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarVinculoContaSalarioDestino.
     * 
     * @param pdcAlterarVinculoContaSalarioDestino
     *            Adaptador AlterarVinculoContaSalarioDestino
     */
    public void setAlterarVinculoContaSalarioDestinoPDCAdapter(IAlterarVinculoContaSalarioDestinoPDCAdapter pdcAlterarVinculoContaSalarioDestino) {
        this.pdcAlterarVinculoContaSalarioDestino = pdcAlterarVinculoContaSalarioDestino;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaContratosManutencao.
     * 
     * @return Adaptador ConsultarListaContratosManutencao
     */
    public IConsultarListaContratosManutencaoPDCAdapter getConsultarListaContratosManutencaoPDCAdapter() {
        return pdcConsultarListaContratosManutencao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaContratosManutencao.
     * 
     * @param pdcConsultarListaContratosManutencao
     *            Adaptador ConsultarListaContratosManutencao
     */
    public void setConsultarListaContratosManutencaoPDCAdapter(IConsultarListaContratosManutencaoPDCAdapter pdcConsultarListaContratosManutencao) {
        this.pdcConsultarListaContratosManutencao = pdcConsultarListaContratosManutencao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistMsgHistoricoComplementar.
     * 
     * @return Adaptador ListarHistMsgHistoricoComplementar
     */
    public IListarHistMsgHistoricoComplementarPDCAdapter getListarHistMsgHistoricoComplementarPDCAdapter() {
        return pdcListarHistMsgHistoricoComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistMsgHistoricoComplementar.
     * 
     * @param pdcListarHistMsgHistoricoComplementar
     *            Adaptador ListarHistMsgHistoricoComplementar
     */
    public void setListarHistMsgHistoricoComplementarPDCAdapter(IListarHistMsgHistoricoComplementarPDCAdapter pdcListarHistMsgHistoricoComplementar) {
        this.pdcListarHistMsgHistoricoComplementar = pdcListarHistMsgHistoricoComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistManutMsgHistComplementar.
     * 
     * @return Adaptador DetalharHistManutMsgHistComplementar
     */
    public IDetalharHistManutMsgHistComplementarPDCAdapter getDetalharHistManutMsgHistComplementarPDCAdapter() {
        return pdcDetalharHistManutMsgHistComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistManutMsgHistComplementar.
     * 
     * @param pdcDetalharHistManutMsgHistComplementar
     *            Adaptador DetalharHistManutMsgHistComplementar
     */
    public void setDetalharHistManutMsgHistComplementarPDCAdapter(IDetalharHistManutMsgHistComplementarPDCAdapter pdcDetalharHistManutMsgHistComplementar) {
        this.pdcDetalharHistManutMsgHistComplementar = pdcDetalharHistManutMsgHistComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirParticipanteContratoPgit.
     * 
     * @return Adaptador ExcluirParticipanteContratoPgit
     */
    public IExcluirParticipanteContratoPgitPDCAdapter getExcluirParticipanteContratoPgitPDCAdapter() {
        return pdcExcluirParticipanteContratoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirParticipanteContratoPgit.
     * 
     * @param pdcExcluirParticipanteContratoPgit
     *            Adaptador ExcluirParticipanteContratoPgit
     */
    public void setExcluirParticipanteContratoPgitPDCAdapter(IExcluirParticipanteContratoPgitPDCAdapter pdcExcluirParticipanteContratoPgit) {
        this.pdcExcluirParticipanteContratoPgit = pdcExcluirParticipanteContratoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicos.
     * 
     * @return Adaptador ListarServicos
     */
    public IListarServicosPDCAdapter getListarServicosPDCAdapter() {
        return pdcListarServicos;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicos.
     * 
     * @param pdcListarServicos
     *            Adaptador ListarServicos
     */
    public void setListarServicosPDCAdapter(IListarServicosPDCAdapter pdcListarServicos) {
        this.pdcListarServicos = pdcListarServicos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarUnidadeFederativa.
     * 
     * @return Adaptador ListarUnidadeFederativa
     */
    public IListarUnidadeFederativaPDCAdapter getListarUnidadeFederativaPDCAdapter() {
        return pdcListarUnidadeFederativa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarUnidadeFederativa.
     * 
     * @param pdcListarUnidadeFederativa
     *            Adaptador ListarUnidadeFederativa
     */
    public void setListarUnidadeFederativaPDCAdapter(IListarUnidadeFederativaPDCAdapter pdcListarUnidadeFederativa) {
        this.pdcListarUnidadeFederativa = pdcListarUnidadeFederativa;
    }
    /**
     * M�todo invocado para obter um adaptador RegistrarPendenciaContratoPgit.
     * 
     * @return Adaptador RegistrarPendenciaContratoPgit
     */
    public IRegistrarPendenciaContratoPgitPDCAdapter getRegistrarPendenciaContratoPgitPDCAdapter() {
        return pdcRegistrarPendenciaContratoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador RegistrarPendenciaContratoPgit.
     * 
     * @param pdcRegistrarPendenciaContratoPgit
     *            Adaptador RegistrarPendenciaContratoPgit
     */
    public void setRegistrarPendenciaContratoPgitPDCAdapter(IRegistrarPendenciaContratoPgitPDCAdapter pdcRegistrarPendenciaContratoPgit) {
        this.pdcRegistrarPendenciaContratoPgit = pdcRegistrarPendenciaContratoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ListarModalidades.
     * 
     * @return Adaptador ListarModalidades
     */
    public IListarModalidadesPDCAdapter getListarModalidadesPDCAdapter() {
        return pdcListarModalidades;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarModalidades.
     * 
     * @param pdcListarModalidades
     *            Adaptador ListarModalidades
     */
    public void setListarModalidadesPDCAdapter(IListarModalidadesPDCAdapter pdcListarModalidades) {
        this.pdcListarModalidades = pdcListarModalidades;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMotivoSituacaoPendContrato.
     * 
     * @return Adaptador ListarMotivoSituacaoPendContrato
     */
    public IListarMotivoSituacaoPendContratoPDCAdapter getListarMotivoSituacaoPendContratoPDCAdapter() {
        return pdcListarMotivoSituacaoPendContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMotivoSituacaoPendContrato.
     * 
     * @param pdcListarMotivoSituacaoPendContrato
     *            Adaptador ListarMotivoSituacaoPendContrato
     */
    public void setListarMotivoSituacaoPendContratoPDCAdapter(IListarMotivoSituacaoPendContratoPDCAdapter pdcListarMotivoSituacaoPendContrato) {
        this.pdcListarMotivoSituacaoPendContrato = pdcListarMotivoSituacaoPendContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPendenciaContratoPgit.
     * 
     * @return Adaptador ConsultarPendenciaContratoPgit
     */
    public IConsultarPendenciaContratoPgitPDCAdapter getConsultarPendenciaContratoPgitPDCAdapter() {
        return pdcConsultarPendenciaContratoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPendenciaContratoPgit.
     * 
     * @param pdcConsultarPendenciaContratoPgit
     *            Adaptador ConsultarPendenciaContratoPgit
     */
    public void setConsultarPendenciaContratoPgitPDCAdapter(IConsultarPendenciaContratoPgitPDCAdapter pdcConsultarPendenciaContratoPgit) {
        this.pdcConsultarPendenciaContratoPgit = pdcConsultarPendenciaContratoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirContaContrato.
     * 
     * @return Adaptador IncluirContaContrato
     */
    public IIncluirContaContratoPDCAdapter getIncluirContaContratoPDCAdapter() {
        return pdcIncluirContaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirContaContrato.
     * 
     * @param pdcIncluirContaContrato
     *            Adaptador IncluirContaContrato
     */
    public void setIncluirContaContratoPDCAdapter(IIncluirContaContratoPDCAdapter pdcIncluirContaContrato) {
        this.pdcIncluirContaContrato = pdcIncluirContaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador CancelarManutencaoParticipante.
     * 
     * @return Adaptador CancelarManutencaoParticipante
     */
    public ICancelarManutencaoParticipantePDCAdapter getCancelarManutencaoParticipantePDCAdapter() {
        return pdcCancelarManutencaoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador CancelarManutencaoParticipante.
     * 
     * @param pdcCancelarManutencaoParticipante
     *            Adaptador CancelarManutencaoParticipante
     */
    public void setCancelarManutencaoParticipantePDCAdapter(ICancelarManutencaoParticipantePDCAdapter pdcCancelarManutencaoParticipante) {
        this.pdcCancelarManutencaoParticipante = pdcCancelarManutencaoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador BloquearDesbloquearContrato.
     * 
     * @return Adaptador BloquearDesbloquearContrato
     */
    public IBloquearDesbloquearContratoPDCAdapter getBloquearDesbloquearContratoPDCAdapter() {
        return pdcBloquearDesbloquearContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador BloquearDesbloquearContrato.
     * 
     * @param pdcBloquearDesbloquearContrato
     *            Adaptador BloquearDesbloquearContrato
     */
    public void setBloquearDesbloquearContratoPDCAdapter(IBloquearDesbloquearContratoPDCAdapter pdcBloquearDesbloquearContrato) {
        this.pdcBloquearDesbloquearContrato = pdcBloquearDesbloquearContrato;
    }
    /**
     * M�todo invocado para obter um adaptador BloquearDesbloquearContaContrato.
     * 
     * @return Adaptador BloquearDesbloquearContaContrato
     */
    public IBloquearDesbloquearContaContratoPDCAdapter getBloquearDesbloquearContaContratoPDCAdapter() {
        return pdcBloquearDesbloquearContaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador BloquearDesbloquearContaContrato.
     * 
     * @param pdcBloquearDesbloquearContaContrato
     *            Adaptador BloquearDesbloquearContaContrato
     */
    public void setBloquearDesbloquearContaContratoPDCAdapter(IBloquearDesbloquearContaContratoPDCAdapter pdcBloquearDesbloquearContaContrato) {
        this.pdcBloquearDesbloquearContaContrato = pdcBloquearDesbloquearContaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarCondCobTarifa.
     * 
     * @return Adaptador AlterarCondCobTarifa
     */
    public IAlterarCondCobTarifaPDCAdapter getAlterarCondCobTarifaPDCAdapter() {
        return pdcAlterarCondCobTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarCondCobTarifa.
     * 
     * @param pdcAlterarCondCobTarifa
     *            Adaptador AlterarCondCobTarifa
     */
    public void setAlterarCondCobTarifaPDCAdapter(IAlterarCondCobTarifaPDCAdapter pdcAlterarCondCobTarifa) {
        this.pdcAlterarCondCobTarifa = pdcAlterarCondCobTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirVinculoCtaSalarioEmpresa.
     * 
     * @return Adaptador ExcluirVinculoCtaSalarioEmpresa
     */
    public IExcluirVinculoCtaSalarioEmpresaPDCAdapter getExcluirVinculoCtaSalarioEmpresaPDCAdapter() {
        return pdcExcluirVinculoCtaSalarioEmpresa;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirVinculoCtaSalarioEmpresa.
     * 
     * @param pdcExcluirVinculoCtaSalarioEmpresa
     *            Adaptador ExcluirVinculoCtaSalarioEmpresa
     */
    public void setExcluirVinculoCtaSalarioEmpresaPDCAdapter(IExcluirVinculoCtaSalarioEmpresaPDCAdapter pdcExcluirVinculoCtaSalarioEmpresa) {
        this.pdcExcluirVinculoCtaSalarioEmpresa = pdcExcluirVinculoCtaSalarioEmpresa;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConManParticipante.
     * 
     * @return Adaptador ListarConManParticipante
     */
    public IListarConManParticipantePDCAdapter getListarConManParticipantePDCAdapter() {
        return pdcListarConManParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConManParticipante.
     * 
     * @param pdcListarConManParticipante
     *            Adaptador ListarConManParticipante
     */
    public void setListarConManParticipantePDCAdapter(IListarConManParticipantePDCAdapter pdcListarConManParticipante) {
        this.pdcListarConManParticipante = pdcListarConManParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDescricaoMensagem.
     * 
     * @return Adaptador ConsultarDescricaoMensagem
     */
    public IConsultarDescricaoMensagemPDCAdapter getConsultarDescricaoMensagemPDCAdapter() {
        return pdcConsultarDescricaoMensagem;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDescricaoMensagem.
     * 
     * @param pdcConsultarDescricaoMensagem
     *            Adaptador ConsultarDescricaoMensagem
     */
    public void setConsultarDescricaoMensagemPDCAdapter(IConsultarDescricaoMensagemPDCAdapter pdcConsultarDescricaoMensagem) {
        this.pdcConsultarDescricaoMensagem = pdcConsultarDescricaoMensagem;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMotivoSituacaoServContrato.
     * 
     * @return Adaptador ListarMotivoSituacaoServContrato
     */
    public IListarMotivoSituacaoServContratoPDCAdapter getListarMotivoSituacaoServContratoPDCAdapter() {
        return pdcListarMotivoSituacaoServContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMotivoSituacaoServContrato.
     * 
     * @param pdcListarMotivoSituacaoServContrato
     *            Adaptador ListarMotivoSituacaoServContrato
     */
    public void setListarMotivoSituacaoServContratoPDCAdapter(IListarMotivoSituacaoServContratoPDCAdapter pdcListarMotivoSituacaoServContrato) {
        this.pdcListarMotivoSituacaoServContrato = pdcListarMotivoSituacaoServContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarCondReajusteTarifa.
     * 
     * @return Adaptador AlterarCondReajusteTarifa
     */
    public IAlterarCondReajusteTarifaPDCAdapter getAlterarCondReajusteTarifaPDCAdapter() {
        return pdcAlterarCondReajusteTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarCondReajusteTarifa.
     * 
     * @param pdcAlterarCondReajusteTarifa
     *            Adaptador AlterarCondReajusteTarifa
     */
    public void setAlterarCondReajusteTarifaPDCAdapter(IAlterarCondReajusteTarifaPDCAdapter pdcAlterarCondReajusteTarifa) {
        this.pdcAlterarCondReajusteTarifa = pdcAlterarCondReajusteTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMotivoSituacaoContrato.
     * 
     * @return Adaptador ListarMotivoSituacaoContrato
     */
    public IListarMotivoSituacaoContratoPDCAdapter getListarMotivoSituacaoContratoPDCAdapter() {
        return pdcListarMotivoSituacaoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMotivoSituacaoContrato.
     * 
     * @param pdcListarMotivoSituacaoContrato
     *            Adaptador ListarMotivoSituacaoContrato
     */
    public void setListarMotivoSituacaoContratoPDCAdapter(IListarMotivoSituacaoContratoPDCAdapter pdcListarMotivoSituacaoContrato) {
        this.pdcListarMotivoSituacaoContrato = pdcListarMotivoSituacaoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirTipoRetornoLayout.
     * 
     * @return Adaptador ExcluirTipoRetornoLayout
     */
    public IExcluirTipoRetornoLayoutPDCAdapter getExcluirTipoRetornoLayoutPDCAdapter() {
        return pdcExcluirTipoRetornoLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirTipoRetornoLayout.
     * 
     * @param pdcExcluirTipoRetornoLayout
     *            Adaptador ExcluirTipoRetornoLayout
     */
    public void setExcluirTipoRetornoLayoutPDCAdapter(IExcluirTipoRetornoLayoutPDCAdapter pdcExcluirTipoRetornoLayout) {
        this.pdcExcluirTipoRetornoLayout = pdcExcluirTipoRetornoLayout;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMotivoSituacaoVincContrato.
     * 
     * @return Adaptador ListarMotivoSituacaoVincContrato
     */
    public IListarMotivoSituacaoVincContratoPDCAdapter getListarMotivoSituacaoVincContratoPDCAdapter() {
        return pdcListarMotivoSituacaoVincContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMotivoSituacaoVincContrato.
     * 
     * @param pdcListarMotivoSituacaoVincContrato
     *            Adaptador ListarMotivoSituacaoVincContrato
     */
    public void setListarMotivoSituacaoVincContratoPDCAdapter(IListarMotivoSituacaoVincContratoPDCAdapter pdcListarMotivoSituacaoVincContrato) {
        this.pdcListarMotivoSituacaoVincContrato = pdcListarMotivoSituacaoVincContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMotivoSituacaoAditivoContrato.
     * 
     * @return Adaptador ListarMotivoSituacaoAditivoContrato
     */
    public IListarMotivoSituacaoAditivoContratoPDCAdapter getListarMotivoSituacaoAditivoContratoPDCAdapter() {
        return pdcListarMotivoSituacaoAditivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMotivoSituacaoAditivoContrato.
     * 
     * @param pdcListarMotivoSituacaoAditivoContrato
     *            Adaptador ListarMotivoSituacaoAditivoContrato
     */
    public void setListarMotivoSituacaoAditivoContratoPDCAdapter(IListarMotivoSituacaoAditivoContratoPDCAdapter pdcListarMotivoSituacaoAditivoContrato) {
        this.pdcListarMotivoSituacaoAditivoContrato = pdcListarMotivoSituacaoAditivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarVincMsgLancOper.
     * 
     * @return Adaptador ListarVincMsgLancOper
     */
    public IListarVincMsgLancOperPDCAdapter getListarVincMsgLancOperPDCAdapter() {
        return pdcListarVincMsgLancOper;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarVincMsgLancOper.
     * 
     * @param pdcListarVincMsgLancOper
     *            Adaptador ListarVincMsgLancOper
     */
    public void setListarVincMsgLancOperPDCAdapter(IListarVincMsgLancOperPDCAdapter pdcListarVincMsgLancOper) {
        this.pdcListarVincMsgLancOper = pdcListarVincMsgLancOper;
    }
    /**
     * M�todo invocado para obter um adaptador ListarComboFormaLiquidacao.
     * 
     * @return Adaptador ListarComboFormaLiquidacao
     */
    public IListarComboFormaLiquidacaoPDCAdapter getListarComboFormaLiquidacaoPDCAdapter() {
        return pdcListarComboFormaLiquidacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarComboFormaLiquidacao.
     * 
     * @param pdcListarComboFormaLiquidacao
     *            Adaptador ListarComboFormaLiquidacao
     */
    public void setListarComboFormaLiquidacaoPDCAdapter(IListarComboFormaLiquidacaoPDCAdapter pdcListarComboFormaLiquidacao) {
        this.pdcListarComboFormaLiquidacao = pdcListarComboFormaLiquidacao;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarAditivoContrato.
     * 
     * @return Adaptador ConsultarAditivoContrato
     */
    public IConsultarAditivoContratoPDCAdapter getConsultarAditivoContratoPDCAdapter() {
        return pdcConsultarAditivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarAditivoContrato.
     * 
     * @param pdcConsultarAditivoContrato
     *            Adaptador ConsultarAditivoContrato
     */
    public void setConsultarAditivoContratoPDCAdapter(IConsultarAditivoContratoPDCAdapter pdcConsultarAditivoContrato) {
        this.pdcConsultarAditivoContrato = pdcConsultarAditivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AtivarAditivoContratoPendAss.
     * 
     * @return Adaptador AtivarAditivoContratoPendAss
     */
    public IAtivarAditivoContratoPendAssPDCAdapter getAtivarAditivoContratoPendAssPDCAdapter() {
        return pdcAtivarAditivoContratoPendAss;
    }

    /**
     * M�todo invocado para establecer um adaptador AtivarAditivoContratoPendAss.
     * 
     * @param pdcAtivarAditivoContratoPendAss
     *            Adaptador AtivarAditivoContratoPendAss
     */
    public void setAtivarAditivoContratoPendAssPDCAdapter(IAtivarAditivoContratoPendAssPDCAdapter pdcAtivarAditivoContratoPendAss) {
        this.pdcAtivarAditivoContratoPendAss = pdcAtivarAditivoContratoPendAss;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAditivoContrato.
     * 
     * @return Adaptador ExcluirAditivoContrato
     */
    public IExcluirAditivoContratoPDCAdapter getExcluirAditivoContratoPDCAdapter() {
        return pdcExcluirAditivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAditivoContrato.
     * 
     * @param pdcExcluirAditivoContrato
     *            Adaptador ExcluirAditivoContrato
     */
    public void setExcluirAditivoContratoPDCAdapter(IExcluirAditivoContratoPDCAdapter pdcExcluirAditivoContrato) {
        this.pdcExcluirAditivoContrato = pdcExcluirAditivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AtivarContratoPendAssinatura.
     * 
     * @return Adaptador AtivarContratoPendAssinatura
     */
    public IAtivarContratoPendAssinaturaPDCAdapter getAtivarContratoPendAssinaturaPDCAdapter() {
        return pdcAtivarContratoPendAssinatura;
    }

    /**
     * M�todo invocado para establecer um adaptador AtivarContratoPendAssinatura.
     * 
     * @param pdcAtivarContratoPendAssinatura
     *            Adaptador AtivarContratoPendAssinatura
     */
    public void setAtivarContratoPendAssinaturaPDCAdapter(IAtivarContratoPendAssinaturaPDCAdapter pdcAtivarContratoPendAssinatura) {
        this.pdcAtivarContratoPendAssinatura = pdcAtivarContratoPendAssinatura;
    }
    /**
     * M�todo invocado para obter um adaptador RegistrarFormalizacaoManContrato.
     * 
     * @return Adaptador RegistrarFormalizacaoManContrato
     */
    public IRegistrarFormalizacaoManContratoPDCAdapter getRegistrarFormalizacaoManContratoPDCAdapter() {
        return pdcRegistrarFormalizacaoManContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador RegistrarFormalizacaoManContrato.
     * 
     * @param pdcRegistrarFormalizacaoManContrato
     *            Adaptador RegistrarFormalizacaoManContrato
     */
    public void setRegistrarFormalizacaoManContratoPDCAdapter(IRegistrarFormalizacaoManContratoPDCAdapter pdcRegistrarFormalizacaoManContrato) {
        this.pdcRegistrarFormalizacaoManContrato = pdcRegistrarFormalizacaoManContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMeioTransmissao.
     * 
     * @return Adaptador ListarMeioTransmissao
     */
    public IListarMeioTransmissaoPDCAdapter getListarMeioTransmissaoPDCAdapter() {
        return pdcListarMeioTransmissao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMeioTransmissao.
     * 
     * @param pdcListarMeioTransmissao
     *            Adaptador ListarMeioTransmissao
     */
    public void setListarMeioTransmissaoPDCAdapter(IListarMeioTransmissaoPDCAdapter pdcListarMeioTransmissao) {
        this.pdcListarMeioTransmissao = pdcListarMeioTransmissao;
    }
    /**
     * M�todo invocado para obter um adaptador RegistrarAssinaturaContrato.
     * 
     * @return Adaptador RegistrarAssinaturaContrato
     */
    public IRegistrarAssinaturaContratoPDCAdapter getRegistrarAssinaturaContratoPDCAdapter() {
        return pdcRegistrarAssinaturaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador RegistrarAssinaturaContrato.
     * 
     * @param pdcRegistrarAssinaturaContrato
     *            Adaptador RegistrarAssinaturaContrato
     */
    public void setRegistrarAssinaturaContratoPDCAdapter(IRegistrarAssinaturaContratoPDCAdapter pdcRegistrarAssinaturaContrato) {
        this.pdcRegistrarAssinaturaContrato = pdcRegistrarAssinaturaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicitacaoAmbiente.
     * 
     * @return Adaptador ConsultarSolicitacaoAmbiente
     */
    public IConsultarSolicitacaoAmbientePDCAdapter getConsultarSolicitacaoAmbientePDCAdapter() {
        return pdcConsultarSolicitacaoAmbiente;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicitacaoAmbiente.
     * 
     * @param pdcConsultarSolicitacaoAmbiente
     *            Adaptador ConsultarSolicitacaoAmbiente
     */
    public void setConsultarSolicitacaoAmbientePDCAdapter(IConsultarSolicitacaoAmbientePDCAdapter pdcConsultarSolicitacaoAmbiente) {
        this.pdcConsultarSolicitacaoAmbiente = pdcConsultarSolicitacaoAmbiente;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoServicoModalidade.
     * 
     * @return Adaptador ListarTipoServicoModalidade
     */
    public IListarTipoServicoModalidadePDCAdapter getListarTipoServicoModalidadePDCAdapter() {
        return pdcListarTipoServicoModalidade;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoServicoModalidade.
     * 
     * @param pdcListarTipoServicoModalidade
     *            Adaptador ListarTipoServicoModalidade
     */
    public void setListarTipoServicoModalidadePDCAdapter(IListarTipoServicoModalidadePDCAdapter pdcListarTipoServicoModalidade) {
        this.pdcListarTipoServicoModalidade = pdcListarTipoServicoModalidade;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSituacaoAditivoContrato.
     * 
     * @return Adaptador ListarSituacaoAditivoContrato
     */
    public IListarSituacaoAditivoContratoPDCAdapter getListarSituacaoAditivoContratoPDCAdapter() {
        return pdcListarSituacaoAditivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSituacaoAditivoContrato.
     * 
     * @param pdcListarSituacaoAditivoContrato
     *            Adaptador ListarSituacaoAditivoContrato
     */
    public void setListarSituacaoAditivoContratoPDCAdapter(IListarSituacaoAditivoContratoPDCAdapter pdcListarSituacaoAditivoContrato) {
        this.pdcListarSituacaoAditivoContrato = pdcListarSituacaoAditivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTarifasContrato.
     * 
     * @return Adaptador ListarTarifasContrato
     */
    public IListarTarifasContratoPDCAdapter getListarTarifasContratoPDCAdapter() {
        return pdcListarTarifasContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTarifasContrato.
     * 
     * @param pdcListarTarifasContrato
     *            Adaptador ListarTarifasContrato
     */
    public void setListarTarifasContratoPDCAdapter(IListarTarifasContratoPDCAdapter pdcListarTarifasContrato) {
        this.pdcListarTarifasContrato = pdcListarTarifasContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConManTarifas.
     * 
     * @return Adaptador ConsultarConManTarifas
     */
    public IConsultarConManTarifasPDCAdapter getConsultarConManTarifasPDCAdapter() {
        return pdcConsultarConManTarifas;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConManTarifas.
     * 
     * @param pdcConsultarConManTarifas
     *            Adaptador ConsultarConManTarifas
     */
    public void setConsultarConManTarifasPDCAdapter(IConsultarConManTarifasPDCAdapter pdcConsultarConManTarifas) {
        this.pdcConsultarConManTarifas = pdcConsultarConManTarifas;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarLayoutServico.
     * 
     * @return Adaptador AlterarLayoutServico
     */
    public IAlterarLayoutServicoPDCAdapter getAlterarLayoutServicoPDCAdapter() {
        return pdcAlterarLayoutServico;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarLayoutServico.
     * 
     * @param pdcAlterarLayoutServico
     *            Adaptador AlterarLayoutServico
     */
    public void setAlterarLayoutServicoPDCAdapter(IAlterarLayoutServicoPDCAdapter pdcAlterarLayoutServico) {
        this.pdcAlterarLayoutServico = pdcAlterarLayoutServico;
    }
    /**
     * M�todo invocado para obter um adaptador AprovarSolicitacaoConsistPrevia.
     * 
     * @return Adaptador AprovarSolicitacaoConsistPrevia
     */
    public IAprovarSolicitacaoConsistPreviaPDCAdapter getAprovarSolicitacaoConsistPreviaPDCAdapter() {
        return pdcAprovarSolicitacaoConsistPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador AprovarSolicitacaoConsistPrevia.
     * 
     * @param pdcAprovarSolicitacaoConsistPrevia
     *            Adaptador AprovarSolicitacaoConsistPrevia
     */
    public void setAprovarSolicitacaoConsistPreviaPDCAdapter(IAprovarSolicitacaoConsistPreviaPDCAdapter pdcAprovarSolicitacaoConsistPrevia) {
        this.pdcAprovarSolicitacaoConsistPrevia = pdcAprovarSolicitacaoConsistPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador RejeitarSolicitacaoConsistPrevia.
     * 
     * @return Adaptador RejeitarSolicitacaoConsistPrevia
     */
    public IRejeitarSolicitacaoConsistPreviaPDCAdapter getRejeitarSolicitacaoConsistPreviaPDCAdapter() {
        return pdcRejeitarSolicitacaoConsistPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador RejeitarSolicitacaoConsistPrevia.
     * 
     * @param pdcRejeitarSolicitacaoConsistPrevia
     *            Adaptador RejeitarSolicitacaoConsistPrevia
     */
    public void setRejeitarSolicitacaoConsistPreviaPDCAdapter(IRejeitarSolicitacaoConsistPreviaPDCAdapter pdcRejeitarSolicitacaoConsistPrevia) {
        this.pdcRejeitarSolicitacaoConsistPrevia = pdcRejeitarSolicitacaoConsistPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador ReprocessarSolicitacaoConsistPrevia.
     * 
     * @return Adaptador ReprocessarSolicitacaoConsistPrevia
     */
    public IReprocessarSolicitacaoConsistPreviaPDCAdapter getReprocessarSolicitacaoConsistPreviaPDCAdapter() {
        return pdcReprocessarSolicitacaoConsistPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador ReprocessarSolicitacaoConsistPrevia.
     * 
     * @param pdcReprocessarSolicitacaoConsistPrevia
     *            Adaptador ReprocessarSolicitacaoConsistPrevia
     */
    public void setReprocessarSolicitacaoConsistPreviaPDCAdapter(IReprocessarSolicitacaoConsistPreviaPDCAdapter pdcReprocessarSolicitacaoConsistPrevia) {
        this.pdcReprocessarSolicitacaoConsistPrevia = pdcReprocessarSolicitacaoConsistPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicRecuperacao.
     * 
     * @return Adaptador ConsultarSolicRecuperacao
     */
    public IConsultarSolicRecuperacaoPDCAdapter getConsultarSolicRecuperacaoPDCAdapter() {
        return pdcConsultarSolicRecuperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicRecuperacao.
     * 
     * @param pdcConsultarSolicRecuperacao
     *            Adaptador ConsultarSolicRecuperacao
     */
    public void setConsultarSolicRecuperacaoPDCAdapter(IConsultarSolicRecuperacaoPDCAdapter pdcConsultarSolicRecuperacao) {
        this.pdcConsultarSolicRecuperacao = pdcConsultarSolicRecuperacao;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicRecuperacao.
     * 
     * @return Adaptador DetalharSolicRecuperacao
     */
    public IDetalharSolicRecuperacaoPDCAdapter getDetalharSolicRecuperacaoPDCAdapter() {
        return pdcDetalharSolicRecuperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicRecuperacao.
     * 
     * @param pdcDetalharSolicRecuperacao
     *            Adaptador DetalharSolicRecuperacao
     */
    public void setDetalharSolicRecuperacaoPDCAdapter(IDetalharSolicRecuperacaoPDCAdapter pdcDetalharSolicRecuperacao) {
        this.pdcDetalharSolicRecuperacao = pdcDetalharSolicRecuperacao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicRecuperacao.
     * 
     * @return Adaptador IncluirSolicRecuperacao
     */
    public IIncluirSolicRecuperacaoPDCAdapter getIncluirSolicRecuperacaoPDCAdapter() {
        return pdcIncluirSolicRecuperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicRecuperacao.
     * 
     * @param pdcIncluirSolicRecuperacao
     *            Adaptador IncluirSolicRecuperacao
     */
    public void setIncluirSolicRecuperacaoPDCAdapter(IIncluirSolicRecuperacaoPDCAdapter pdcIncluirSolicRecuperacao) {
        this.pdcIncluirSolicRecuperacao = pdcIncluirSolicRecuperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarArquivoRecuperacao.
     * 
     * @return Adaptador ConsultarArquivoRecuperacao
     */
    public IConsultarArquivoRecuperacaoPDCAdapter getConsultarArquivoRecuperacaoPDCAdapter() {
        return pdcConsultarArquivoRecuperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarArquivoRecuperacao.
     * 
     * @param pdcConsultarArquivoRecuperacao
     *            Adaptador ConsultarArquivoRecuperacao
     */
    public void setConsultarArquivoRecuperacaoPDCAdapter(IConsultarArquivoRecuperacaoPDCAdapter pdcConsultarArquivoRecuperacao) {
        this.pdcConsultarArquivoRecuperacao = pdcConsultarArquivoRecuperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicitacaoConsistPrevia.
     * 
     * @return Adaptador ConsultarSolicitacaoConsistPrevia
     */
    public IConsultarSolicitacaoConsistPreviaPDCAdapter getConsultarSolicitacaoConsistPreviaPDCAdapter() {
        return pdcConsultarSolicitacaoConsistPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicitacaoConsistPrevia.
     * 
     * @param pdcConsultarSolicitacaoConsistPrevia
     *            Adaptador ConsultarSolicitacaoConsistPrevia
     */
    public void setConsultarSolicitacaoConsistPreviaPDCAdapter(IConsultarSolicitacaoConsistPreviaPDCAdapter pdcConsultarSolicitacaoConsistPrevia) {
        this.pdcConsultarSolicitacaoConsistPrevia = pdcConsultarSolicitacaoConsistPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolEmiComprovanteFavorecido.
     * 
     * @return Adaptador ConsultarSolEmiComprovanteFavorecido
     */
    public IConsultarSolEmiComprovanteFavorecidoPDCAdapter getConsultarSolEmiComprovanteFavorecidoPDCAdapter() {
        return pdcConsultarSolEmiComprovanteFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolEmiComprovanteFavorecido.
     * 
     * @param pdcConsultarSolEmiComprovanteFavorecido
     *            Adaptador ConsultarSolEmiComprovanteFavorecido
     */
    public void setConsultarSolEmiComprovanteFavorecidoPDCAdapter(IConsultarSolEmiComprovanteFavorecidoPDCAdapter pdcConsultarSolEmiComprovanteFavorecido) {
        this.pdcConsultarSolEmiComprovanteFavorecido = pdcConsultarSolEmiComprovanteFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ListarClientesMarq.
     * 
     * @return Adaptador ListarClientesMarq
     */
    public IListarClientesMarqPDCAdapter getListarClientesMarqPDCAdapter() {
        return pdcListarClientesMarq;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarClientesMarq.
     * 
     * @param pdcListarClientesMarq
     *            Adaptador ListarClientesMarq
     */
    public void setListarClientesMarqPDCAdapter(IListarClientesMarqPDCAdapter pdcListarClientesMarq) {
        this.pdcListarClientesMarq = pdcListarClientesMarq;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContratosClienteMarq.
     * 
     * @return Adaptador ListarContratosClienteMarq
     */
    public IListarContratosClienteMarqPDCAdapter getListarContratosClienteMarqPDCAdapter() {
        return pdcListarContratosClienteMarq;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContratosClienteMarq.
     * 
     * @param pdcListarContratosClienteMarq
     *            Adaptador ListarContratosClienteMarq
     */
    public void setListarContratosClienteMarqPDCAdapter(IListarContratosClienteMarqPDCAdapter pdcListarContratosClienteMarq) {
        this.pdcListarContratosClienteMarq = pdcListarContratosClienteMarq;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolEmiComprovanteCliePagador.
     * 
     * @return Adaptador ConsultarSolEmiComprovanteCliePagador
     */
    public IConsultarSolEmiComprovanteCliePagadorPDCAdapter getConsultarSolEmiComprovanteCliePagadorPDCAdapter() {
        return pdcConsultarSolEmiComprovanteCliePagador;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolEmiComprovanteCliePagador.
     * 
     * @param pdcConsultarSolEmiComprovanteCliePagador
     *            Adaptador ConsultarSolEmiComprovanteCliePagador
     */
    public void setConsultarSolEmiComprovanteCliePagadorPDCAdapter(IConsultarSolEmiComprovanteCliePagadorPDCAdapter pdcConsultarSolEmiComprovanteCliePagador) {
        this.pdcConsultarSolEmiComprovanteCliePagador = pdcConsultarSolEmiComprovanteCliePagador;
    }
    /**
     * M�todo invocado para obter um adaptador ListarPessoaJuridicaContratoMarq.
     * 
     * @return Adaptador ListarPessoaJuridicaContratoMarq
     */
    public IListarPessoaJuridicaContratoMarqPDCAdapter getListarPessoaJuridicaContratoMarqPDCAdapter() {
        return pdcListarPessoaJuridicaContratoMarq;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarPessoaJuridicaContratoMarq.
     * 
     * @param pdcListarPessoaJuridicaContratoMarq
     *            Adaptador ListarPessoaJuridicaContratoMarq
     */
    public void setListarPessoaJuridicaContratoMarqPDCAdapter(IListarPessoaJuridicaContratoMarqPDCAdapter pdcListarPessoaJuridicaContratoMarq) {
        this.pdcListarPessoaJuridicaContratoMarq = pdcListarPessoaJuridicaContratoMarq;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoContratoMarq.
     * 
     * @return Adaptador ListarTipoContratoMarq
     */
    public IListarTipoContratoMarqPDCAdapter getListarTipoContratoMarqPDCAdapter() {
        return pdcListarTipoContratoMarq;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoContratoMarq.
     * 
     * @param pdcListarTipoContratoMarq
     *            Adaptador ListarTipoContratoMarq
     */
    public void setListarTipoContratoMarqPDCAdapter(IListarTipoContratoMarqPDCAdapter pdcListarTipoContratoMarq) {
        this.pdcListarTipoContratoMarq = pdcListarTipoContratoMarq;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharListaDebitoAutDes.
     * 
     * @return Adaptador DetalharListaDebitoAutDes
     */
    public IDetalharListaDebitoAutDesPDCAdapter getDetalharListaDebitoAutDesPDCAdapter() {
        return pdcDetalharListaDebitoAutDes;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharListaDebitoAutDes.
     * 
     * @param pdcDetalharListaDebitoAutDes
     *            Adaptador DetalharListaDebitoAutDes
     */
    public void setDetalharListaDebitoAutDesPDCAdapter(IDetalharListaDebitoAutDesPDCAdapter pdcDetalharListaDebitoAutDes) {
        this.pdcDetalharListaDebitoAutDes = pdcDetalharListaDebitoAutDes;
    }
    /**
     * M�todo invocado para obter um adaptador ListarPerfilTrocaArquivo.
     * 
     * @return Adaptador ListarPerfilTrocaArquivo
     */
    public IListarPerfilTrocaArquivoPDCAdapter getListarPerfilTrocaArquivoPDCAdapter() {
        return pdcListarPerfilTrocaArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarPerfilTrocaArquivo.
     * 
     * @param pdcListarPerfilTrocaArquivo
     *            Adaptador ListarPerfilTrocaArquivo
     */
    public void setListarPerfilTrocaArquivoPDCAdapter(IListarPerfilTrocaArquivoPDCAdapter pdcListarPerfilTrocaArquivo) {
        this.pdcListarPerfilTrocaArquivo = pdcListarPerfilTrocaArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador AutDesListaDebitoParcial.
     * 
     * @return Adaptador AutDesListaDebitoParcial
     */
    public IAutDesListaDebitoParcialPDCAdapter getAutDesListaDebitoParcialPDCAdapter() {
        return pdcAutDesListaDebitoParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador AutDesListaDebitoParcial.
     * 
     * @param pdcAutDesListaDebitoParcial
     *            Adaptador AutDesListaDebitoParcial
     */
    public void setAutDesListaDebitoParcialPDCAdapter(IAutDesListaDebitoParcialPDCAdapter pdcAutDesListaDebitoParcial) {
        this.pdcAutDesListaDebitoParcial = pdcAutDesListaDebitoParcial;
    }
    /**
     * M�todo invocado para obter um adaptador AutDesListaDebitoIntegral.
     * 
     * @return Adaptador AutDesListaDebitoIntegral
     */
    public IAutDesListaDebitoIntegralPDCAdapter getAutDesListaDebitoIntegralPDCAdapter() {
        return pdcAutDesListaDebitoIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador AutDesListaDebitoIntegral.
     * 
     * @param pdcAutDesListaDebitoIntegral
     *            Adaptador AutDesListaDebitoIntegral
     */
    public void setAutDesListaDebitoIntegralPDCAdapter(IAutDesListaDebitoIntegralPDCAdapter pdcAutDesListaDebitoIntegral) {
        this.pdcAutDesListaDebitoIntegral = pdcAutDesListaDebitoIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicRecuperacao.
     * 
     * @return Adaptador ExcluirSolicRecuperacao
     */
    public IExcluirSolicRecuperacaoPDCAdapter getExcluirSolicRecuperacaoPDCAdapter() {
        return pdcExcluirSolicRecuperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicRecuperacao.
     * 
     * @param pdcExcluirSolicRecuperacao
     *            Adaptador ExcluirSolicRecuperacao
     */
    public void setExcluirSolicRecuperacaoPDCAdapter(IExcluirSolicRecuperacaoPDCAdapter pdcExcluirSolicRecuperacao) {
        this.pdcExcluirSolicRecuperacao = pdcExcluirSolicRecuperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContratoMarq.
     * 
     * @return Adaptador ListarContratoMarq
     */
    public IListarContratoMarqPDCAdapter getListarContratoMarqPDCAdapter() {
        return pdcListarContratoMarq;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContratoMarq.
     * 
     * @param pdcListarContratoMarq
     *            Adaptador ListarContratoMarq
     */
    public void setListarContratoMarqPDCAdapter(IListarContratoMarqPDCAdapter pdcListarContratoMarq) {
        this.pdcListarContratoMarq = pdcListarContratoMarq;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarTipoConta.
     * 
     * @return Adaptador ConsultarTipoConta
     */
    public IConsultarTipoContaPDCAdapter getConsultarTipoContaPDCAdapter() {
        return pdcConsultarTipoConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarTipoConta.
     * 
     * @param pdcConsultarTipoConta
     *            Adaptador ConsultarTipoConta
     */
    public void setConsultarTipoContaPDCAdapter(IConsultarTipoContaPDCAdapter pdcConsultarTipoConta) {
        this.pdcConsultarTipoConta = pdcConsultarTipoConta;
    }
    /**
     * M�todo invocado para obter um adaptador DesautorizarPagtosIndividuais.
     * 
     * @return Adaptador DesautorizarPagtosIndividuais
     */
    public IDesautorizarPagtosIndividuaisPDCAdapter getDesautorizarPagtosIndividuaisPDCAdapter() {
        return pdcDesautorizarPagtosIndividuais;
    }

    /**
     * M�todo invocado para establecer um adaptador DesautorizarPagtosIndividuais.
     * 
     * @param pdcDesautorizarPagtosIndividuais
     *            Adaptador DesautorizarPagtosIndividuais
     */
    public void setDesautorizarPagtosIndividuaisPDCAdapter(IDesautorizarPagtosIndividuaisPDCAdapter pdcDesautorizarPagtosIndividuais) {
        this.pdcDesautorizarPagtosIndividuais = pdcDesautorizarPagtosIndividuais;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarPagtosIndSemConsultaSaldo.
     * 
     * @return Adaptador LiberarPagtosIndSemConsultaSaldo
     */
    public ILiberarPagtosIndSemConsultaSaldoPDCAdapter getLiberarPagtosIndSemConsultaSaldoPDCAdapter() {
        return pdcLiberarPagtosIndSemConsultaSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarPagtosIndSemConsultaSaldo.
     * 
     * @param pdcLiberarPagtosIndSemConsultaSaldo
     *            Adaptador LiberarPagtosIndSemConsultaSaldo
     */
    public void setLiberarPagtosIndSemConsultaSaldoPDCAdapter(ILiberarPagtosIndSemConsultaSaldoPDCAdapter pdcLiberarPagtosIndSemConsultaSaldo) {
        this.pdcLiberarPagtosIndSemConsultaSaldo = pdcLiberarPagtosIndSemConsultaSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador AutorizarPagtosIndividuais.
     * 
     * @return Adaptador AutorizarPagtosIndividuais
     */
    public IAutorizarPagtosIndividuaisPDCAdapter getAutorizarPagtosIndividuaisPDCAdapter() {
        return pdcAutorizarPagtosIndividuais;
    }

    /**
     * M�todo invocado para establecer um adaptador AutorizarPagtosIndividuais.
     * 
     * @param pdcAutorizarPagtosIndividuais
     *            Adaptador AutorizarPagtosIndividuais
     */
    public void setAutorizarPagtosIndividuaisPDCAdapter(IAutorizarPagtosIndividuaisPDCAdapter pdcAutorizarPagtosIndividuais) {
        this.pdcAutorizarPagtosIndividuais = pdcAutorizarPagtosIndividuais;
    }
    /**
     * M�todo invocado para obter um adaptador CancelarLiberacaoParcial.
     * 
     * @return Adaptador CancelarLiberacaoParcial
     */
    public ICancelarLiberacaoParcialPDCAdapter getCancelarLiberacaoParcialPDCAdapter() {
        return pdcCancelarLiberacaoParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador CancelarLiberacaoParcial.
     * 
     * @param pdcCancelarLiberacaoParcial
     *            Adaptador CancelarLiberacaoParcial
     */
    public void setCancelarLiberacaoParcialPDCAdapter(ICancelarLiberacaoParcialPDCAdapter pdcCancelarLiberacaoParcial) {
        this.pdcCancelarLiberacaoParcial = pdcCancelarLiberacaoParcial;
    }
    /**
     * M�todo invocado para obter um adaptador CancelarLiberacaoIntegral.
     * 
     * @return Adaptador CancelarLiberacaoIntegral
     */
    public ICancelarLiberacaoIntegralPDCAdapter getCancelarLiberacaoIntegralPDCAdapter() {
        return pdcCancelarLiberacaoIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador CancelarLiberacaoIntegral.
     * 
     * @param pdcCancelarLiberacaoIntegral
     *            Adaptador CancelarLiberacaoIntegral
     */
    public void setCancelarLiberacaoIntegralPDCAdapter(ICancelarLiberacaoIntegralPDCAdapter pdcCancelarLiberacaoIntegral) {
        this.pdcCancelarLiberacaoIntegral = pdcCancelarLiberacaoIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador CancelarPagtosParcial.
     * 
     * @return Adaptador CancelarPagtosParcial
     */
    public ICancelarPagtosParcialPDCAdapter getCancelarPagtosParcialPDCAdapter() {
        return pdcCancelarPagtosParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador CancelarPagtosParcial.
     * 
     * @param pdcCancelarPagtosParcial
     *            Adaptador CancelarPagtosParcial
     */
    public void setCancelarPagtosParcialPDCAdapter(ICancelarPagtosParcialPDCAdapter pdcCancelarPagtosParcial) {
        this.pdcCancelarPagtosParcial = pdcCancelarPagtosParcial;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarParcial.
     * 
     * @return Adaptador LiberarParcial
     */
    public ILiberarParcialPDCAdapter getLiberarParcialPDCAdapter() {
        return pdcLiberarParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarParcial.
     * 
     * @param pdcLiberarParcial
     *            Adaptador LiberarParcial
     */
    public void setLiberarParcialPDCAdapter(ILiberarParcialPDCAdapter pdcLiberarParcial) {
        this.pdcLiberarParcial = pdcLiberarParcial;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarIntegral.
     * 
     * @return Adaptador LiberarIntegral
     */
    public ILiberarIntegralPDCAdapter getLiberarIntegralPDCAdapter() {
        return pdcLiberarIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarIntegral.
     * 
     * @param pdcLiberarIntegral
     *            Adaptador LiberarIntegral
     */
    public void setLiberarIntegralPDCAdapter(ILiberarIntegralPDCAdapter pdcLiberarIntegral) {
        this.pdcLiberarIntegral = pdcLiberarIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador CancelarLibPagtosIndSemConsultaSaldo.
     * 
     * @return Adaptador CancelarLibPagtosIndSemConsultaSaldo
     */
    public ICancelarLibPagtosIndSemConsultaSaldoPDCAdapter getCancelarLibPagtosIndSemConsultaSaldoPDCAdapter() {
        return pdcCancelarLibPagtosIndSemConsultaSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador CancelarLibPagtosIndSemConsultaSaldo.
     * 
     * @param pdcCancelarLibPagtosIndSemConsultaSaldo
     *            Adaptador CancelarLibPagtosIndSemConsultaSaldo
     */
    public void setCancelarLibPagtosIndSemConsultaSaldoPDCAdapter(ICancelarLibPagtosIndSemConsultaSaldoPDCAdapter pdcCancelarLibPagtosIndSemConsultaSaldo) {
        this.pdcCancelarLibPagtosIndSemConsultaSaldo = pdcCancelarLibPagtosIndSemConsultaSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador DesautorizarParcial.
     * 
     * @return Adaptador DesautorizarParcial
     */
    public IDesautorizarParcialPDCAdapter getDesautorizarParcialPDCAdapter() {
        return pdcDesautorizarParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador DesautorizarParcial.
     * 
     * @param pdcDesautorizarParcial
     *            Adaptador DesautorizarParcial
     */
    public void setDesautorizarParcialPDCAdapter(IDesautorizarParcialPDCAdapter pdcDesautorizarParcial) {
        this.pdcDesautorizarParcial = pdcDesautorizarParcial;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSolBasePagto.
     * 
     * @return Adaptador ListarSolBasePagto
     */
    public IListarSolBasePagtoPDCAdapter getListarSolBasePagtoPDCAdapter() {
        return pdcListarSolBasePagto;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSolBasePagto.
     * 
     * @param pdcListarSolBasePagto
     *            Adaptador ListarSolBasePagto
     */
    public void setListarSolBasePagtoPDCAdapter(IListarSolBasePagtoPDCAdapter pdcListarSolBasePagto) {
        this.pdcListarSolBasePagto = pdcListarSolBasePagto;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarLiberacaoPrevia.
     * 
     * @return Adaptador AlterarLiberacaoPrevia
     */
    public IAlterarLiberacaoPreviaPDCAdapter getAlterarLiberacaoPreviaPDCAdapter() {
        return pdcAlterarLiberacaoPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarLiberacaoPrevia.
     * 
     * @param pdcAlterarLiberacaoPrevia
     *            Adaptador AlterarLiberacaoPrevia
     */
    public void setAlterarLiberacaoPreviaPDCAdapter(IAlterarLiberacaoPreviaPDCAdapter pdcAlterarLiberacaoPrevia) {
        this.pdcAlterarLiberacaoPrevia = pdcAlterarLiberacaoPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirLiberacaoPrevia.
     * 
     * @return Adaptador ExcluirLiberacaoPrevia
     */
    public IExcluirLiberacaoPreviaPDCAdapter getExcluirLiberacaoPreviaPDCAdapter() {
        return pdcExcluirLiberacaoPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirLiberacaoPrevia.
     * 
     * @param pdcExcluirLiberacaoPrevia
     *            Adaptador ExcluirLiberacaoPrevia
     */
    public void setExcluirLiberacaoPreviaPDCAdapter(IExcluirLiberacaoPreviaPDCAdapter pdcExcluirLiberacaoPrevia) {
        this.pdcExcluirLiberacaoPrevia = pdcExcluirLiberacaoPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirLiberacaoPrevia.
     * 
     * @return Adaptador IncluirLiberacaoPrevia
     */
    public IIncluirLiberacaoPreviaPDCAdapter getIncluirLiberacaoPreviaPDCAdapter() {
        return pdcIncluirLiberacaoPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirLiberacaoPrevia.
     * 
     * @param pdcIncluirLiberacaoPrevia
     *            Adaptador IncluirLiberacaoPrevia
     */
    public void setIncluirLiberacaoPreviaPDCAdapter(IIncluirLiberacaoPreviaPDCAdapter pdcIncluirLiberacaoPrevia) {
        this.pdcIncluirLiberacaoPrevia = pdcIncluirLiberacaoPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador DesautorizarIntegral.
     * 
     * @return Adaptador DesautorizarIntegral
     */
    public IDesautorizarIntegralPDCAdapter getDesautorizarIntegralPDCAdapter() {
        return pdcDesautorizarIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador DesautorizarIntegral.
     * 
     * @param pdcDesautorizarIntegral
     *            Adaptador DesautorizarIntegral
     */
    public void setDesautorizarIntegralPDCAdapter(IDesautorizarIntegralPDCAdapter pdcDesautorizarIntegral) {
        this.pdcDesautorizarIntegral = pdcDesautorizarIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarPagtoIndividualPendencia.
     * 
     * @return Adaptador LiberarPagtoIndividualPendencia
     */
    public ILiberarPagtoIndividualPendenciaPDCAdapter getLiberarPagtoIndividualPendenciaPDCAdapter() {
        return pdcLiberarPagtoIndividualPendencia;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarPagtoIndividualPendencia.
     * 
     * @param pdcLiberarPagtoIndividualPendencia
     *            Adaptador LiberarPagtoIndividualPendencia
     */
    public void setLiberarPagtoIndividualPendenciaPDCAdapter(ILiberarPagtoIndividualPendenciaPDCAdapter pdcLiberarPagtoIndividualPendencia) {
        this.pdcLiberarPagtoIndividualPendencia = pdcLiberarPagtoIndividualPendencia;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarPagtoPendIndividual.
     * 
     * @return Adaptador LiberarPagtoPendIndividual
     */
    public ILiberarPagtoPendIndividualPDCAdapter getLiberarPagtoPendIndividualPDCAdapter() {
        return pdcLiberarPagtoPendIndividual;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarPagtoPendIndividual.
     * 
     * @param pdcLiberarPagtoPendIndividual
     *            Adaptador LiberarPagtoPendIndividual
     */
    public void setLiberarPagtoPendIndividualPDCAdapter(ILiberarPagtoPendIndividualPDCAdapter pdcLiberarPagtoPendIndividual) {
        this.pdcLiberarPagtoPendIndividual = pdcLiberarPagtoPendIndividual;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSolBaseFavorecido.
     * 
     * @return Adaptador ListarSolBaseFavorecido
     */
    public IListarSolBaseFavorecidoPDCAdapter getListarSolBaseFavorecidoPDCAdapter() {
        return pdcListarSolBaseFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSolBaseFavorecido.
     * 
     * @param pdcListarSolBaseFavorecido
     *            Adaptador ListarSolBaseFavorecido
     */
    public void setListarSolBaseFavorecidoPDCAdapter(IListarSolBaseFavorecidoPDCAdapter pdcListarSolBaseFavorecido) {
        this.pdcListarSolBaseFavorecido = pdcListarSolBaseFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolBaseFavorecido.
     * 
     * @return Adaptador ExcluirSolBaseFavorecido
     */
    public IExcluirSolBaseFavorecidoPDCAdapter getExcluirSolBaseFavorecidoPDCAdapter() {
        return pdcExcluirSolBaseFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolBaseFavorecido.
     * 
     * @param pdcExcluirSolBaseFavorecido
     *            Adaptador ExcluirSolBaseFavorecido
     */
    public void setExcluirSolBaseFavorecidoPDCAdapter(IExcluirSolBaseFavorecidoPDCAdapter pdcExcluirSolBaseFavorecido) {
        this.pdcExcluirSolBaseFavorecido = pdcExcluirSolBaseFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolBasePagto.
     * 
     * @return Adaptador ExcluirSolBasePagto
     */
    public IExcluirSolBasePagtoPDCAdapter getExcluirSolBasePagtoPDCAdapter() {
        return pdcExcluirSolBasePagto;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolBasePagto.
     * 
     * @param pdcExcluirSolBasePagto
     *            Adaptador ExcluirSolBasePagto
     */
    public void setExcluirSolBasePagtoPDCAdapter(IExcluirSolBasePagtoPDCAdapter pdcExcluirSolBasePagto) {
        this.pdcExcluirSolBasePagto = pdcExcluirSolBasePagto;
    }
    /**
     * M�todo invocado para obter um adaptador AutorizarParcial.
     * 
     * @return Adaptador AutorizarParcial
     */
    public IAutorizarParcialPDCAdapter getAutorizarParcialPDCAdapter() {
        return pdcAutorizarParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador AutorizarParcial.
     * 
     * @param pdcAutorizarParcial
     *            Adaptador AutorizarParcial
     */
    public void setAutorizarParcialPDCAdapter(IAutorizarParcialPDCAdapter pdcAutorizarParcial) {
        this.pdcAutorizarParcial = pdcAutorizarParcial;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSolBaseSistema.
     * 
     * @return Adaptador ListarSolBaseSistema
     */
    public IListarSolBaseSistemaPDCAdapter getListarSolBaseSistemaPDCAdapter() {
        return pdcListarSolBaseSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSolBaseSistema.
     * 
     * @param pdcListarSolBaseSistema
     *            Adaptador ListarSolBaseSistema
     */
    public void setListarSolBaseSistemaPDCAdapter(IListarSolBaseSistemaPDCAdapter pdcListarSolBaseSistema) {
        this.pdcListarSolBaseSistema = pdcListarSolBaseSistema;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolEmiComprovanteFavorecido.
     * 
     * @return Adaptador ExcluirSolEmiComprovanteFavorecido
     */
    public IExcluirSolEmiComprovanteFavorecidoPDCAdapter getExcluirSolEmiComprovanteFavorecidoPDCAdapter() {
        return pdcExcluirSolEmiComprovanteFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolEmiComprovanteFavorecido.
     * 
     * @param pdcExcluirSolEmiComprovanteFavorecido
     *            Adaptador ExcluirSolEmiComprovanteFavorecido
     */
    public void setExcluirSolEmiComprovanteFavorecidoPDCAdapter(IExcluirSolEmiComprovanteFavorecidoPDCAdapter pdcExcluirSolEmiComprovanteFavorecido) {
        this.pdcExcluirSolEmiComprovanteFavorecido = pdcExcluirSolEmiComprovanteFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolBaseSistema.
     * 
     * @return Adaptador ExcluirSolBaseSistema
     */
    public IExcluirSolBaseSistemaPDCAdapter getExcluirSolBaseSistemaPDCAdapter() {
        return pdcExcluirSolBaseSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolBaseSistema.
     * 
     * @param pdcExcluirSolBaseSistema
     *            Adaptador ExcluirSolBaseSistema
     */
    public void setExcluirSolBaseSistemaPDCAdapter(IExcluirSolBaseSistemaPDCAdapter pdcExcluirSolBaseSistema) {
        this.pdcExcluirSolBaseSistema = pdcExcluirSolBaseSistema;
    }
    /**
     * M�todo invocado para obter um adaptador AutorizarIntegral.
     * 
     * @return Adaptador AutorizarIntegral
     */
    public IAutorizarIntegralPDCAdapter getAutorizarIntegralPDCAdapter() {
        return pdcAutorizarIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador AutorizarIntegral.
     * 
     * @param pdcAutorizarIntegral
     *            Adaptador AutorizarIntegral
     */
    public void setAutorizarIntegralPDCAdapter(IAutorizarIntegralPDCAdapter pdcAutorizarIntegral) {
        this.pdcAutorizarIntegral = pdcAutorizarIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoSistema.
     * 
     * @return Adaptador ListarTipoSistema
     */
    public IListarTipoSistemaPDCAdapter getListarTipoSistemaPDCAdapter() {
        return pdcListarTipoSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoSistema.
     * 
     * @param pdcListarTipoSistema
     *            Adaptador ListarTipoSistema
     */
    public void setListarTipoSistemaPDCAdapter(IListarTipoSistemaPDCAdapter pdcListarTipoSistema) {
        this.pdcListarTipoSistema = pdcListarTipoSistema;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSolBaseRetransmissao.
     * 
     * @return Adaptador ListarSolBaseRetransmissao
     */
    public IListarSolBaseRetransmissaoPDCAdapter getListarSolBaseRetransmissaoPDCAdapter() {
        return pdcListarSolBaseRetransmissao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSolBaseRetransmissao.
     * 
     * @param pdcListarSolBaseRetransmissao
     *            Adaptador ListarSolBaseRetransmissao
     */
    public void setListarSolBaseRetransmissaoPDCAdapter(IListarSolBaseRetransmissaoPDCAdapter pdcListarSolBaseRetransmissao) {
        this.pdcListarSolBaseRetransmissao = pdcListarSolBaseRetransmissao;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolBaseRetransmissao.
     * 
     * @return Adaptador ExcluirSolBaseRetransmissao
     */
    public IExcluirSolBaseRetransmissaoPDCAdapter getExcluirSolBaseRetransmissaoPDCAdapter() {
        return pdcExcluirSolBaseRetransmissao;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolBaseRetransmissao.
     * 
     * @param pdcExcluirSolBaseRetransmissao
     *            Adaptador ExcluirSolBaseRetransmissao
     */
    public void setExcluirSolBaseRetransmissaoPDCAdapter(IExcluirSolBaseRetransmissaoPDCAdapter pdcExcluirSolBaseRetransmissao) {
        this.pdcExcluirSolBaseRetransmissao = pdcExcluirSolBaseRetransmissao;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarModalidadePGIT.
     * 
     * @return Adaptador ConsultarModalidadePGIT
     */
    public IConsultarModalidadePGITPDCAdapter getConsultarModalidadePGITPDCAdapter() {
        return pdcConsultarModalidadePGIT;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarModalidadePGIT.
     * 
     * @param pdcConsultarModalidadePGIT
     *            Adaptador ConsultarModalidadePGIT
     */
    public void setConsultarModalidadePGITPDCAdapter(IConsultarModalidadePGITPDCAdapter pdcConsultarModalidadePGIT) {
        this.pdcConsultarModalidadePGIT = pdcConsultarModalidadePGIT;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolEmiComprovanteCliePagador.
     * 
     * @return Adaptador ExcluirSolEmiComprovanteCliePagador
     */
    public IExcluirSolEmiComprovanteCliePagadorPDCAdapter getExcluirSolEmiComprovanteCliePagadorPDCAdapter() {
        return pdcExcluirSolEmiComprovanteCliePagador;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolEmiComprovanteCliePagador.
     * 
     * @param pdcExcluirSolEmiComprovanteCliePagador
     *            Adaptador ExcluirSolEmiComprovanteCliePagador
     */
    public void setExcluirSolEmiComprovanteCliePagadorPDCAdapter(IExcluirSolEmiComprovanteCliePagadorPDCAdapter pdcExcluirSolEmiComprovanteCliePagador) {
        this.pdcExcluirSolEmiComprovanteCliePagador = pdcExcluirSolEmiComprovanteCliePagador;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSituacaoRetorno.
     * 
     * @return Adaptador ListarSituacaoRetorno
     */
    public IListarSituacaoRetornoPDCAdapter getListarSituacaoRetornoPDCAdapter() {
        return pdcListarSituacaoRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSituacaoRetorno.
     * 
     * @param pdcListarSituacaoRetorno
     *            Adaptador ListarSituacaoRetorno
     */
    public void setListarSituacaoRetornoPDCAdapter(IListarSituacaoRetornoPDCAdapter pdcListarSituacaoRetorno) {
        this.pdcListarSituacaoRetorno = pdcListarSituacaoRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoLayout.
     * 
     * @return Adaptador ListarTipoLayout
     */
    public IListarTipoLayoutPDCAdapter getListarTipoLayoutPDCAdapter() {
        return pdcListarTipoLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoLayout.
     * 
     * @param pdcListarTipoLayout
     *            Adaptador ListarTipoLayout
     */
    public void setListarTipoLayoutPDCAdapter(IListarTipoLayoutPDCAdapter pdcListarTipoLayout) {
        this.pdcListarTipoLayout = pdcListarTipoLayout;
    }
    /**
     * M�todo invocado para obter um adaptador ListarArquivoRetorno.
     * 
     * @return Adaptador ListarArquivoRetorno
     */
    public IListarArquivoRetornoPDCAdapter getListarArquivoRetornoPDCAdapter() {
        return pdcListarArquivoRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarArquivoRetorno.
     * 
     * @param pdcListarArquivoRetorno
     *            Adaptador ListarArquivoRetorno
     */
    public void setListarArquivoRetornoPDCAdapter(IListarArquivoRetornoPDCAdapter pdcListarArquivoRetorno) {
        this.pdcListarArquivoRetorno = pdcListarArquivoRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolEmiAviMovtoFavorecido.
     * 
     * @return Adaptador ConsultarSolEmiAviMovtoFavorecido
     */
    public IConsultarSolEmiAviMovtoFavorecidoPDCAdapter getConsultarSolEmiAviMovtoFavorecidoPDCAdapter() {
        return pdcConsultarSolEmiAviMovtoFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolEmiAviMovtoFavorecido.
     * 
     * @param pdcConsultarSolEmiAviMovtoFavorecido
     *            Adaptador ConsultarSolEmiAviMovtoFavorecido
     */
    public void setConsultarSolEmiAviMovtoFavorecidoPDCAdapter(IConsultarSolEmiAviMovtoFavorecidoPDCAdapter pdcConsultarSolEmiAviMovtoFavorecido) {
        this.pdcConsultarSolEmiAviMovtoFavorecido = pdcConsultarSolEmiAviMovtoFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolEmiAviMovtoFavorecido.
     * 
     * @return Adaptador ExcluirSolEmiAviMovtoFavorecido
     */
    public IExcluirSolEmiAviMovtoFavorecidoPDCAdapter getExcluirSolEmiAviMovtoFavorecidoPDCAdapter() {
        return pdcExcluirSolEmiAviMovtoFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolEmiAviMovtoFavorecido.
     * 
     * @param pdcExcluirSolEmiAviMovtoFavorecido
     *            Adaptador ExcluirSolEmiAviMovtoFavorecido
     */
    public void setExcluirSolEmiAviMovtoFavorecidoPDCAdapter(IExcluirSolEmiAviMovtoFavorecidoPDCAdapter pdcExcluirSolEmiAviMovtoFavorecido) {
        this.pdcExcluirSolEmiAviMovtoFavorecido = pdcExcluirSolEmiAviMovtoFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador CancelarPagtosIndividuais.
     * 
     * @return Adaptador CancelarPagtosIndividuais
     */
    public ICancelarPagtosIndividuaisPDCAdapter getCancelarPagtosIndividuaisPDCAdapter() {
        return pdcCancelarPagtosIndividuais;
    }

    /**
     * M�todo invocado para establecer um adaptador CancelarPagtosIndividuais.
     * 
     * @param pdcCancelarPagtosIndividuais
     *            Adaptador CancelarPagtosIndividuais
     */
    public void setCancelarPagtosIndividuaisPDCAdapter(ICancelarPagtosIndividuaisPDCAdapter pdcCancelarPagtosIndividuais) {
        this.pdcCancelarPagtosIndividuais = pdcCancelarPagtosIndividuais;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolEmiAviMovtoCliePagador.
     * 
     * @return Adaptador ConsultarSolEmiAviMovtoCliePagador
     */
    public IConsultarSolEmiAviMovtoCliePagadorPDCAdapter getConsultarSolEmiAviMovtoCliePagadorPDCAdapter() {
        return pdcConsultarSolEmiAviMovtoCliePagador;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolEmiAviMovtoCliePagador.
     * 
     * @param pdcConsultarSolEmiAviMovtoCliePagador
     *            Adaptador ConsultarSolEmiAviMovtoCliePagador
     */
    public void setConsultarSolEmiAviMovtoCliePagadorPDCAdapter(IConsultarSolEmiAviMovtoCliePagadorPDCAdapter pdcConsultarSolEmiAviMovtoCliePagador) {
        this.pdcConsultarSolEmiAviMovtoCliePagador = pdcConsultarSolEmiAviMovtoCliePagador;
    }
    /**
     * M�todo invocado para obter um adaptador EncerrarContrato.
     * 
     * @return Adaptador EncerrarContrato
     */
    public IEncerrarContratoPDCAdapter getEncerrarContratoPDCAdapter() {
        return pdcEncerrarContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador EncerrarContrato.
     * 
     * @param pdcEncerrarContrato
     *            Adaptador EncerrarContrato
     */
    public void setEncerrarContratoPDCAdapter(IEncerrarContratoPDCAdapter pdcEncerrarContrato) {
        this.pdcEncerrarContrato = pdcEncerrarContrato;
    }
    /**
     * M�todo invocado para obter um adaptador RecuperarContrato.
     * 
     * @return Adaptador RecuperarContrato
     */
    public IRecuperarContratoPDCAdapter getRecuperarContratoPDCAdapter() {
        return pdcRecuperarContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador RecuperarContrato.
     * 
     * @param pdcRecuperarContrato
     *            Adaptador RecuperarContrato
     */
    public void setRecuperarContratoPDCAdapter(IRecuperarContratoPDCAdapter pdcRecuperarContrato) {
        this.pdcRecuperarContrato = pdcRecuperarContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarAgenciaOperadora.
     * 
     * @return Adaptador ConsultarAgenciaOperadora
     */
    public IConsultarAgenciaOperadoraPDCAdapter getConsultarAgenciaOperadoraPDCAdapter() {
        return pdcConsultarAgenciaOperadora;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarAgenciaOperadora.
     * 
     * @param pdcConsultarAgenciaOperadora
     *            Adaptador ConsultarAgenciaOperadora
     */
    public void setConsultarAgenciaOperadoraPDCAdapter(IConsultarAgenciaOperadoraPDCAdapter pdcConsultarAgenciaOperadora) {
        this.pdcConsultarAgenciaOperadora = pdcConsultarAgenciaOperadora;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSituacaoRemessa.
     * 
     * @return Adaptador ListarSituacaoRemessa
     */
    public IListarSituacaoRemessaPDCAdapter getListarSituacaoRemessaPDCAdapter() {
        return pdcListarSituacaoRemessa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSituacaoRemessa.
     * 
     * @param pdcListarSituacaoRemessa
     *            Adaptador ListarSituacaoRemessa
     */
    public void setListarSituacaoRemessaPDCAdapter(IListarSituacaoRemessaPDCAdapter pdcListarSituacaoRemessa) {
        this.pdcListarSituacaoRemessa = pdcListarSituacaoRemessa;
    }
    /**
     * M�todo invocado para obter um adaptador ListarResultadoProcessamentoRemessa.
     * 
     * @return Adaptador ListarResultadoProcessamentoRemessa
     */
    public IListarResultadoProcessamentoRemessaPDCAdapter getListarResultadoProcessamentoRemessaPDCAdapter() {
        return pdcListarResultadoProcessamentoRemessa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarResultadoProcessamentoRemessa.
     * 
     * @param pdcListarResultadoProcessamentoRemessa
     *            Adaptador ListarResultadoProcessamentoRemessa
     */
    public void setListarResultadoProcessamentoRemessaPDCAdapter(IListarResultadoProcessamentoRemessaPDCAdapter pdcListarResultadoProcessamentoRemessa) {
        this.pdcListarResultadoProcessamentoRemessa = pdcListarResultadoProcessamentoRemessa;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolEmiAviMovtoCliePagador.
     * 
     * @return Adaptador ExcluirSolEmiAviMovtoCliePagador
     */
    public IExcluirSolEmiAviMovtoCliePagadorPDCAdapter getExcluirSolEmiAviMovtoCliePagadorPDCAdapter() {
        return pdcExcluirSolEmiAviMovtoCliePagador;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolEmiAviMovtoCliePagador.
     * 
     * @param pdcExcluirSolEmiAviMovtoCliePagador
     *            Adaptador ExcluirSolEmiAviMovtoCliePagador
     */
    public void setExcluirSolEmiAviMovtoCliePagadorPDCAdapter(IExcluirSolEmiAviMovtoCliePagadorPDCAdapter pdcExcluirSolEmiAviMovtoCliePagador) {
        this.pdcExcluirSolEmiAviMovtoCliePagador = pdcExcluirSolEmiAviMovtoCliePagador;
    }
    /**
     * M�todo invocado para obter um adaptador CancelarPagtosIntegral.
     * 
     * @return Adaptador CancelarPagtosIntegral
     */
    public ICancelarPagtosIntegralPDCAdapter getCancelarPagtosIntegralPDCAdapter() {
        return pdcCancelarPagtosIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador CancelarPagtosIntegral.
     * 
     * @param pdcCancelarPagtosIntegral
     *            Adaptador CancelarPagtosIntegral
     */
    public void setCancelarPagtosIntegralPDCAdapter(ICancelarPagtosIntegralPDCAdapter pdcCancelarPagtosIntegral) {
        this.pdcCancelarPagtosIntegral = pdcCancelarPagtosIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtoPendConsolidado.
     * 
     * @return Adaptador ConsultarPagtoPendConsolidado
     */
    public IConsultarPagtoPendConsolidadoPDCAdapter getConsultarPagtoPendConsolidadoPDCAdapter() {
        return pdcConsultarPagtoPendConsolidado;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtoPendConsolidado.
     * 
     * @param pdcConsultarPagtoPendConsolidado
     *            Adaptador ConsultarPagtoPendConsolidado
     */
    public void setConsultarPagtoPendConsolidadoPDCAdapter(IConsultarPagtoPendConsolidadoPDCAdapter pdcConsultarPagtoPendConsolidado) {
        this.pdcConsultarPagtoPendConsolidado = pdcConsultarPagtoPendConsolidado;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaPagamento.
     * 
     * @return Adaptador ConsultarListaPagamento
     */
    public IConsultarListaPagamentoPDCAdapter getConsultarListaPagamentoPDCAdapter() {
        return pdcConsultarListaPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaPagamento.
     * 
     * @param pdcConsultarListaPagamento
     *            Adaptador ConsultarListaPagamento
     */
    public void setConsultarListaPagamentoPDCAdapter(IConsultarListaPagamentoPDCAdapter pdcConsultarListaPagamento) {
        this.pdcConsultarListaPagamento = pdcConsultarListaPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarPagtoPendConIntegral.
     * 
     * @return Adaptador LiberarPagtoPendConIntegral
     */
    public ILiberarPagtoPendConIntegralPDCAdapter getLiberarPagtoPendConIntegralPDCAdapter() {
        return pdcLiberarPagtoPendConIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarPagtoPendConIntegral.
     * 
     * @param pdcLiberarPagtoPendConIntegral
     *            Adaptador LiberarPagtoPendConIntegral
     */
    public void setLiberarPagtoPendConIntegralPDCAdapter(ILiberarPagtoPendConIntegralPDCAdapter pdcLiberarPagtoPendConIntegral) {
        this.pdcLiberarPagtoPendConIntegral = pdcLiberarPagtoPendConIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarPagtoPendConParcial.
     * 
     * @return Adaptador LiberarPagtoPendConParcial
     */
    public ILiberarPagtoPendConParcialPDCAdapter getLiberarPagtoPendConParcialPDCAdapter() {
        return pdcLiberarPagtoPendConParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarPagtoPendConParcial.
     * 
     * @param pdcLiberarPagtoPendConParcial
     *            Adaptador LiberarPagtoPendConParcial
     */
    public void setLiberarPagtoPendConParcialPDCAdapter(ILiberarPagtoPendConParcialPDCAdapter pdcLiberarPagtoPendConParcial) {
        this.pdcLiberarPagtoPendConParcial = pdcLiberarPagtoPendConParcial;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarPendenciaPagtoConIntegral.
     * 
     * @return Adaptador LiberarPendenciaPagtoConIntegral
     */
    public ILiberarPendenciaPagtoConIntegralPDCAdapter getLiberarPendenciaPagtoConIntegralPDCAdapter() {
        return pdcLiberarPendenciaPagtoConIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarPendenciaPagtoConIntegral.
     * 
     * @param pdcLiberarPendenciaPagtoConIntegral
     *            Adaptador LiberarPendenciaPagtoConIntegral
     */
    public void setLiberarPendenciaPagtoConIntegralPDCAdapter(ILiberarPendenciaPagtoConIntegralPDCAdapter pdcLiberarPendenciaPagtoConIntegral) {
        this.pdcLiberarPendenciaPagtoConIntegral = pdcLiberarPendenciaPagtoConIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador LiberarPendenciaPagtoConParcial.
     * 
     * @return Adaptador LiberarPendenciaPagtoConParcial
     */
    public ILiberarPendenciaPagtoConParcialPDCAdapter getLiberarPendenciaPagtoConParcialPDCAdapter() {
        return pdcLiberarPendenciaPagtoConParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador LiberarPendenciaPagtoConParcial.
     * 
     * @param pdcLiberarPendenciaPagtoConParcial
     *            Adaptador LiberarPendenciaPagtoConParcial
     */
    public void setLiberarPendenciaPagtoConParcialPDCAdapter(ILiberarPendenciaPagtoConParcialPDCAdapter pdcLiberarPendenciaPagtoConParcial) {
        this.pdcLiberarPendenciaPagtoConParcial = pdcLiberarPendenciaPagtoConParcial;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarModalidade.
     * 
     * @return Adaptador ConsultarModalidade
     */
    public IConsultarModalidadePDCAdapter getConsultarModalidadePDCAdapter() {
        return pdcConsultarModalidade;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarModalidade.
     * 
     * @param pdcConsultarModalidade
     *            Adaptador ConsultarModalidade
     */
    public void setConsultarModalidadePDCAdapter(IConsultarModalidadePDCAdapter pdcConsultarModalidade) {
        this.pdcConsultarModalidade = pdcConsultarModalidade;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarServicoPagto.
     * 
     * @return Adaptador ConsultarServicoPagto
     */
    public IConsultarServicoPagtoPDCAdapter getConsultarServicoPagtoPDCAdapter() {
        return pdcConsultarServicoPagto;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarServicoPagto.
     * 
     * @param pdcConsultarServicoPagto
     *            Adaptador ConsultarServicoPagto
     */
    public void setConsultarServicoPagtoPDCAdapter(IConsultarServicoPagtoPDCAdapter pdcConsultarServicoPagto) {
        this.pdcConsultarServicoPagto = pdcConsultarServicoPagto;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarModalidadePagto.
     * 
     * @return Adaptador ConsultarModalidadePagto
     */
    public IConsultarModalidadePagtoPDCAdapter getConsultarModalidadePagtoPDCAdapter() {
        return pdcConsultarModalidadePagto;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarModalidadePagto.
     * 
     * @param pdcConsultarModalidadePagto
     *            Adaptador ConsultarModalidadePagto
     */
    public void setConsultarModalidadePagtoPDCAdapter(IConsultarModalidadePagtoPDCAdapter pdcConsultarModalidadePagto) {
        this.pdcConsultarModalidadePagto = pdcConsultarModalidadePagto;
    }
    /**
     * M�todo invocado para obter um adaptador EfetuarCalcOperReceitaTarifas.
     * 
     * @return Adaptador EfetuarCalcOperReceitaTarifas
     */
    public IEfetuarCalcOperReceitaTarifasPDCAdapter getEfetuarCalcOperReceitaTarifasPDCAdapter() {
        return pdcEfetuarCalcOperReceitaTarifas;
    }

    /**
     * M�todo invocado para establecer um adaptador EfetuarCalcOperReceitaTarifas.
     * 
     * @param pdcEfetuarCalcOperReceitaTarifas
     *            Adaptador EfetuarCalcOperReceitaTarifas
     */
    public void setEfetuarCalcOperReceitaTarifasPDCAdapter(IEfetuarCalcOperReceitaTarifasPDCAdapter pdcEfetuarCalcOperReceitaTarifas) {
        this.pdcEfetuarCalcOperReceitaTarifas = pdcEfetuarCalcOperReceitaTarifas;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolEmiComprovanteCliePagador.
     * 
     * @return Adaptador IncluirSolEmiComprovanteCliePagador
     */
    public IIncluirSolEmiComprovanteCliePagadorPDCAdapter getIncluirSolEmiComprovanteCliePagadorPDCAdapter() {
        return pdcIncluirSolEmiComprovanteCliePagador;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolEmiComprovanteCliePagador.
     * 
     * @param pdcIncluirSolEmiComprovanteCliePagador
     *            Adaptador IncluirSolEmiComprovanteCliePagador
     */
    public void setIncluirSolEmiComprovanteCliePagadorPDCAdapter(IIncluirSolEmiComprovanteCliePagadorPDCAdapter pdcIncluirSolEmiComprovanteCliePagador) {
        this.pdcIncluirSolEmiComprovanteCliePagador = pdcIncluirSolEmiComprovanteCliePagador;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolEmiComprovanteFavorecido.
     * 
     * @return Adaptador IncluirSolEmiComprovanteFavorecido
     */
    public IIncluirSolEmiComprovanteFavorecidoPDCAdapter getIncluirSolEmiComprovanteFavorecidoPDCAdapter() {
        return pdcIncluirSolEmiComprovanteFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolEmiComprovanteFavorecido.
     * 
     * @param pdcIncluirSolEmiComprovanteFavorecido
     *            Adaptador IncluirSolEmiComprovanteFavorecido
     */
    public void setIncluirSolEmiComprovanteFavorecidoPDCAdapter(IIncluirSolEmiComprovanteFavorecidoPDCAdapter pdcIncluirSolEmiComprovanteFavorecido) {
        this.pdcIncluirSolEmiComprovanteFavorecido = pdcIncluirSolEmiComprovanteFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoLayoutContrato.
     * 
     * @return Adaptador ListarTipoLayoutContrato
     */
    public IListarTipoLayoutContratoPDCAdapter getListarTipoLayoutContratoPDCAdapter() {
        return pdcListarTipoLayoutContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoLayoutContrato.
     * 
     * @param pdcListarTipoLayoutContrato
     *            Adaptador ListarTipoLayoutContrato
     */
    public void setListarTipoLayoutContratoPDCAdapter(IListarTipoLayoutContratoPDCAdapter pdcListarTipoLayoutContrato) {
        this.pdcListarTipoLayoutContrato = pdcListarTipoLayoutContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarInformacoesFavorecido.
     * 
     * @return Adaptador ConsultarInformacoesFavorecido
     */
    public IConsultarInformacoesFavorecidoPDCAdapter getConsultarInformacoesFavorecidoPDCAdapter() {
        return pdcConsultarInformacoesFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarInformacoesFavorecido.
     * 
     * @param pdcConsultarInformacoesFavorecido
     *            Adaptador ConsultarInformacoesFavorecido
     */
    public void setConsultarInformacoesFavorecidoPDCAdapter(IConsultarInformacoesFavorecidoPDCAdapter pdcConsultarInformacoesFavorecido) {
        this.pdcConsultarInformacoesFavorecido = pdcConsultarInformacoesFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarServRelacServperacional.
     * 
     * @return Adaptador ConsultarServRelacServperacional
     */
    public IConsultarServRelacServperacionalPDCAdapter getConsultarServRelacServperacionalPDCAdapter() {
        return pdcConsultarServRelacServperacional;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarServRelacServperacional.
     * 
     * @param pdcConsultarServRelacServperacional
     *            Adaptador ConsultarServRelacServperacional
     */
    public void setConsultarServRelacServperacionalPDCAdapter(IConsultarServRelacServperacionalPDCAdapter pdcConsultarServRelacServperacional) {
        this.pdcConsultarServRelacServperacional = pdcConsultarServRelacServperacional;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirContrato.
     * 
     * @return Adaptador ImprimirContrato
     */
    public IImprimirContratoPDCAdapter getImprimirContratoPDCAdapter() {
        return pdcImprimirContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirContrato.
     * 
     * @param pdcImprimirContrato
     *            Adaptador ImprimirContrato
     */
    public void setImprimirContratoPDCAdapter(IImprimirContratoPDCAdapter pdcImprimirContrato) {
        this.pdcImprimirContrato = pdcImprimirContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarMotivoSituacaoEstorno.
     * 
     * @return Adaptador ConsultarMotivoSituacaoEstorno
     */
    public IConsultarMotivoSituacaoEstornoPDCAdapter getConsultarMotivoSituacaoEstornoPDCAdapter() {
        return pdcConsultarMotivoSituacaoEstorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarMotivoSituacaoEstorno.
     * 
     * @param pdcConsultarMotivoSituacaoEstorno
     *            Adaptador ConsultarMotivoSituacaoEstorno
     */
    public void setConsultarMotivoSituacaoEstornoPDCAdapter(IConsultarMotivoSituacaoEstornoPDCAdapter pdcConsultarMotivoSituacaoEstorno) {
        this.pdcConsultarMotivoSituacaoEstorno = pdcConsultarMotivoSituacaoEstorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarMotivoSituacaoPagamento.
     * 
     * @return Adaptador ConsultarMotivoSituacaoPagamento
     */
    public IConsultarMotivoSituacaoPagamentoPDCAdapter getConsultarMotivoSituacaoPagamentoPDCAdapter() {
        return pdcConsultarMotivoSituacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarMotivoSituacaoPagamento.
     * 
     * @param pdcConsultarMotivoSituacaoPagamento
     *            Adaptador ConsultarMotivoSituacaoPagamento
     */
    public void setConsultarMotivoSituacaoPagamentoPDCAdapter(IConsultarMotivoSituacaoPagamentoPDCAdapter pdcConsultarMotivoSituacaoPagamento) {
        this.pdcConsultarMotivoSituacaoPagamento = pdcConsultarMotivoSituacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSituacaoPagamento.
     * 
     * @return Adaptador ConsultarSituacaoPagamento
     */
    public IConsultarSituacaoPagamentoPDCAdapter getConsultarSituacaoPagamentoPDCAdapter() {
        return pdcConsultarSituacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSituacaoPagamento.
     * 
     * @param pdcConsultarSituacaoPagamento
     *            Adaptador ConsultarSituacaoPagamento
     */
    public void setConsultarSituacaoPagamentoPDCAdapter(IConsultarSituacaoPagamentoPDCAdapter pdcConsultarSituacaoPagamento) {
        this.pdcConsultarSituacaoPagamento = pdcConsultarSituacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSituacaoSolConsistenciaPrevia.
     * 
     * @return Adaptador ConsultarSituacaoSolConsistenciaPrevia
     */
    public IConsultarSituacaoSolConsistenciaPreviaPDCAdapter getConsultarSituacaoSolConsistenciaPreviaPDCAdapter() {
        return pdcConsultarSituacaoSolConsistenciaPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSituacaoSolConsistenciaPrevia.
     * 
     * @param pdcConsultarSituacaoSolConsistenciaPrevia
     *            Adaptador ConsultarSituacaoSolConsistenciaPrevia
     */
    public void setConsultarSituacaoSolConsistenciaPreviaPDCAdapter(IConsultarSituacaoSolConsistenciaPreviaPDCAdapter pdcConsultarSituacaoSolConsistenciaPrevia) {
        this.pdcConsultarSituacaoSolConsistenciaPrevia = pdcConsultarSituacaoSolConsistenciaPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirAditivoContrato.
     * 
     * @return Adaptador ImprimirAditivoContrato
     */
    public IImprimirAditivoContratoPDCAdapter getImprimirAditivoContratoPDCAdapter() {
        return pdcImprimirAditivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirAditivoContrato.
     * 
     * @param pdcImprimirAditivoContrato
     *            Adaptador ImprimirAditivoContrato
     */
    public void setImprimirAditivoContratoPDCAdapter(IImprimirAditivoContratoPDCAdapter pdcImprimirAditivoContrato) {
        this.pdcImprimirAditivoContrato = pdcImprimirAditivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarTipoRetornoLayout.
     * 
     * @return Adaptador AlterarTipoRetornoLayout
     */
    public IAlterarTipoRetornoLayoutPDCAdapter getAlterarTipoRetornoLayoutPDCAdapter() {
        return pdcAlterarTipoRetornoLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarTipoRetornoLayout.
     * 
     * @param pdcAlterarTipoRetornoLayout
     *            Adaptador AlterarTipoRetornoLayout
     */
    public void setAlterarTipoRetornoLayoutPDCAdapter(IAlterarTipoRetornoLayoutPDCAdapter pdcAlterarTipoRetornoLayout) {
        this.pdcAlterarTipoRetornoLayout = pdcAlterarTipoRetornoLayout;
    }
    /**
     * M�todo invocado para obter um adaptador InlcuirTipoRetornoLayout.
     * 
     * @return Adaptador InlcuirTipoRetornoLayout
     */
    public IInlcuirTipoRetornoLayoutPDCAdapter getInlcuirTipoRetornoLayoutPDCAdapter() {
        return pdcInlcuirTipoRetornoLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador InlcuirTipoRetornoLayout.
     * 
     * @param pdcInlcuirTipoRetornoLayout
     *            Adaptador InlcuirTipoRetornoLayout
     */
    public void setInlcuirTipoRetornoLayoutPDCAdapter(IInlcuirTipoRetornoLayoutPDCAdapter pdcInlcuirTipoRetornoLayout) {
        this.pdcInlcuirTipoRetornoLayout = pdcInlcuirTipoRetornoLayout;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicitacaoRelatorioFavorecidos.
     * 
     * @return Adaptador IncluirSolicitacaoRelatorioFavorecidos
     */
    public IIncluirSolicitacaoRelatorioFavorecidosPDCAdapter getIncluirSolicitacaoRelatorioFavorecidosPDCAdapter() {
        return pdcIncluirSolicitacaoRelatorioFavorecidos;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicitacaoRelatorioFavorecidos.
     * 
     * @param pdcIncluirSolicitacaoRelatorioFavorecidos
     *            Adaptador IncluirSolicitacaoRelatorioFavorecidos
     */
    public void setIncluirSolicitacaoRelatorioFavorecidosPDCAdapter(IIncluirSolicitacaoRelatorioFavorecidosPDCAdapter pdcIncluirSolicitacaoRelatorioFavorecidos) {
        this.pdcIncluirSolicitacaoRelatorioFavorecidos = pdcIncluirSolicitacaoRelatorioFavorecidos;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolRecPagtosVencidoNaoPago.
     * 
     * @return Adaptador ExcluirSolRecPagtosVencidoNaoPago
     */
    public IExcluirSolRecPagtosVencidoNaoPagoPDCAdapter getExcluirSolRecPagtosVencidoNaoPagoPDCAdapter() {
        return pdcExcluirSolRecPagtosVencidoNaoPago;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolRecPagtosVencidoNaoPago.
     * 
     * @param pdcExcluirSolRecPagtosVencidoNaoPago
     *            Adaptador ExcluirSolRecPagtosVencidoNaoPago
     */
    public void setExcluirSolRecPagtosVencidoNaoPagoPDCAdapter(IExcluirSolRecPagtosVencidoNaoPagoPDCAdapter pdcExcluirSolRecPagtosVencidoNaoPago) {
        this.pdcExcluirSolRecPagtosVencidoNaoPago = pdcExcluirSolRecPagtosVencidoNaoPago;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolRecuperacaoPagtos.
     * 
     * @return Adaptador ExcluirSolRecuperacaoPagtos
     */
    public IExcluirSolRecuperacaoPagtosPDCAdapter getExcluirSolRecuperacaoPagtosPDCAdapter() {
        return pdcExcluirSolRecuperacaoPagtos;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolRecuperacaoPagtos.
     * 
     * @param pdcExcluirSolRecuperacaoPagtos
     *            Adaptador ExcluirSolRecuperacaoPagtos
     */
    public void setExcluirSolRecuperacaoPagtosPDCAdapter(IExcluirSolRecuperacaoPagtosPDCAdapter pdcExcluirSolRecuperacaoPagtos) {
        this.pdcExcluirSolRecuperacaoPagtos = pdcExcluirSolRecuperacaoPagtos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistLiquidacaoPagamento.
     * 
     * @return Adaptador ListarHistLiquidacaoPagamento
     */
    public IListarHistLiquidacaoPagamentoPDCAdapter getListarHistLiquidacaoPagamentoPDCAdapter() {
        return pdcListarHistLiquidacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistLiquidacaoPagamento.
     * 
     * @param pdcListarHistLiquidacaoPagamento
     *            Adaptador ListarHistLiquidacaoPagamento
     */
    public void setListarHistLiquidacaoPagamentoPDCAdapter(IListarHistLiquidacaoPagamentoPDCAdapter pdcListarHistLiquidacaoPagamento) {
        this.pdcListarHistLiquidacaoPagamento = pdcListarHistLiquidacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPrioridadeTipoCompromisso.
     * 
     * @return Adaptador DetalharPrioridadeTipoCompromisso
     */
    public IDetalharPrioridadeTipoCompromissoPDCAdapter getDetalharPrioridadeTipoCompromissoPDCAdapter() {
        return pdcDetalharPrioridadeTipoCompromisso;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPrioridadeTipoCompromisso.
     * 
     * @param pdcDetalharPrioridadeTipoCompromisso
     *            Adaptador DetalharPrioridadeTipoCompromisso
     */
    public void setDetalharPrioridadeTipoCompromissoPDCAdapter(IDetalharPrioridadeTipoCompromissoPDCAdapter pdcDetalharPrioridadeTipoCompromisso) {
        this.pdcDetalharPrioridadeTipoCompromisso = pdcDetalharPrioridadeTipoCompromisso;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirPrioridadeTipoCompromisso.
     * 
     * @return Adaptador IncluirPrioridadeTipoCompromisso
     */
    public IIncluirPrioridadeTipoCompromissoPDCAdapter getIncluirPrioridadeTipoCompromissoPDCAdapter() {
        return pdcIncluirPrioridadeTipoCompromisso;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirPrioridadeTipoCompromisso.
     * 
     * @param pdcIncluirPrioridadeTipoCompromisso
     *            Adaptador IncluirPrioridadeTipoCompromisso
     */
    public void setIncluirPrioridadeTipoCompromissoPDCAdapter(IIncluirPrioridadeTipoCompromissoPDCAdapter pdcIncluirPrioridadeTipoCompromisso) {
        this.pdcIncluirPrioridadeTipoCompromisso = pdcIncluirPrioridadeTipoCompromisso;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharDadosContrato.
     * 
     * @return Adaptador DetalharDadosContrato
     */
    public IDetalharDadosContratoPDCAdapter getDetalharDadosContratoPDCAdapter() {
        return pdcDetalharDadosContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharDadosContrato.
     * 
     * @param pdcDetalharDadosContrato
     *            Adaptador DetalharDadosContrato
     */
    public void setDetalharDadosContratoPDCAdapter(IDetalharDadosContratoPDCAdapter pdcDetalharDadosContrato) {
        this.pdcDetalharDadosContrato = pdcDetalharDadosContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetLiberacaoPrevia.
     * 
     * @return Adaptador ConsultarDetLiberacaoPrevia
     */
    public IConsultarDetLiberacaoPreviaPDCAdapter getConsultarDetLiberacaoPreviaPDCAdapter() {
        return pdcConsultarDetLiberacaoPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetLiberacaoPrevia.
     * 
     * @param pdcConsultarDetLiberacaoPrevia
     *            Adaptador ConsultarDetLiberacaoPrevia
     */
    public void setConsultarDetLiberacaoPreviaPDCAdapter(IConsultarDetLiberacaoPreviaPDCAdapter pdcConsultarDetLiberacaoPrevia) {
        this.pdcConsultarDetLiberacaoPrevia = pdcConsultarDetLiberacaoPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLiberacaoPrevia.
     * 
     * @return Adaptador ConsultarLiberacaoPrevia
     */
    public IConsultarLiberacaoPreviaPDCAdapter getConsultarLiberacaoPreviaPDCAdapter() {
        return pdcConsultarLiberacaoPrevia;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLiberacaoPrevia.
     * 
     * @param pdcConsultarLiberacaoPrevia
     *            Adaptador ConsultarLiberacaoPrevia
     */
    public void setConsultarLiberacaoPreviaPDCAdapter(IConsultarLiberacaoPreviaPDCAdapter pdcConsultarLiberacaoPrevia) {
        this.pdcConsultarLiberacaoPrevia = pdcConsultarLiberacaoPrevia;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaValoresDiscretos.
     * 
     * @return Adaptador ConsultarListaValoresDiscretos
     */
    public IConsultarListaValoresDiscretosPDCAdapter getConsultarListaValoresDiscretosPDCAdapter() {
        return pdcConsultarListaValoresDiscretos;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaValoresDiscretos.
     * 
     * @param pdcConsultarListaValoresDiscretos
     *            Adaptador ConsultarListaValoresDiscretos
     */
    public void setConsultarListaValoresDiscretosPDCAdapter(IConsultarListaValoresDiscretosPDCAdapter pdcConsultarListaValoresDiscretos) {
        this.pdcConsultarListaValoresDiscretos = pdcConsultarListaValoresDiscretos;
    }
    /**
     * M�todo invocado para obter um adaptador SolicitarRecPagtosBackupConsulta.
     * 
     * @return Adaptador SolicitarRecPagtosBackupConsulta
     */
    public ISolicitarRecPagtosBackupConsultaPDCAdapter getSolicitarRecPagtosBackupConsultaPDCAdapter() {
        return pdcSolicitarRecPagtosBackupConsulta;
    }

    /**
     * M�todo invocado para establecer um adaptador SolicitarRecPagtosBackupConsulta.
     * 
     * @param pdcSolicitarRecPagtosBackupConsulta
     *            Adaptador SolicitarRecPagtosBackupConsulta
     */
    public void setSolicitarRecPagtosBackupConsultaPDCAdapter(ISolicitarRecPagtosBackupConsultaPDCAdapter pdcSolicitarRecPagtosBackupConsulta) {
        this.pdcSolicitarRecPagtosBackupConsulta = pdcSolicitarRecPagtosBackupConsulta;
    }
    /**
     * M�todo invocado para obter um adaptador ListarUltimaDataPagto.
     * 
     * @return Adaptador ListarUltimaDataPagto
     */
    public IListarUltimaDataPagtoPDCAdapter getListarUltimaDataPagtoPDCAdapter() {
        return pdcListarUltimaDataPagto;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarUltimaDataPagto.
     * 
     * @param pdcListarUltimaDataPagto
     *            Adaptador ListarUltimaDataPagto
     */
    public void setListarUltimaDataPagtoPDCAdapter(IListarUltimaDataPagtoPDCAdapter pdcListarUltimaDataPagto) {
        this.pdcListarUltimaDataPagto = pdcListarUltimaDataPagto;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetManutencaoParticipante.
     * 
     * @return Adaptador ConsultarDetManutencaoParticipante
     */
    public IConsultarDetManutencaoParticipantePDCAdapter getConsultarDetManutencaoParticipantePDCAdapter() {
        return pdcConsultarDetManutencaoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetManutencaoParticipante.
     * 
     * @param pdcConsultarDetManutencaoParticipante
     *            Adaptador ConsultarDetManutencaoParticipante
     */
    public void setConsultarDetManutencaoParticipantePDCAdapter(IConsultarDetManutencaoParticipantePDCAdapter pdcConsultarDetManutencaoParticipante) {
        this.pdcConsultarDetManutencaoParticipante = pdcConsultarDetManutencaoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarManutencaoContaContrato.
     * 
     * @return Adaptador ConsultarManutencaoContaContrato
     */
    public IConsultarManutencaoContaContratoPDCAdapter getConsultarManutencaoContaContratoPDCAdapter() {
        return pdcConsultarManutencaoContaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarManutencaoContaContrato.
     * 
     * @param pdcConsultarManutencaoContaContrato
     *            Adaptador ConsultarManutencaoContaContrato
     */
    public void setConsultarManutencaoContaContratoPDCAdapter(IConsultarManutencaoContaContratoPDCAdapter pdcConsultarManutencaoContaContrato) {
        this.pdcConsultarManutencaoContaContrato = pdcConsultarManutencaoContaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaParticipantesContrato.
     * 
     * @return Adaptador ConsultarListaParticipantesContrato
     */
    public IConsultarListaParticipantesContratoPDCAdapter getConsultarListaParticipantesContratoPDCAdapter() {
        return pdcConsultarListaParticipantesContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaParticipantesContrato.
     * 
     * @param pdcConsultarListaParticipantesContrato
     *            Adaptador ConsultarListaParticipantesContrato
     */
    public void setConsultarListaParticipantesContratoPDCAdapter(IConsultarListaParticipantesContratoPDCAdapter pdcConsultarListaParticipantesContrato) {
        this.pdcConsultarListaParticipantesContrato = pdcConsultarListaParticipantesContrato;
    }
    /**
     * M�todo invocado para obter um adaptador BloquearDesbloquearProdutoServContrato.
     * 
     * @return Adaptador BloquearDesbloquearProdutoServContrato
     */
    public IBloquearDesbloquearProdutoServContratoPDCAdapter getBloquearDesbloquearProdutoServContratoPDCAdapter() {
        return pdcBloquearDesbloquearProdutoServContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador BloquearDesbloquearProdutoServContrato.
     * 
     * @param pdcBloquearDesbloquearProdutoServContrato
     *            Adaptador BloquearDesbloquearProdutoServContrato
     */
    public void setBloquearDesbloquearProdutoServContratoPDCAdapter(IBloquearDesbloquearProdutoServContratoPDCAdapter pdcBloquearDesbloquearProdutoServContrato) {
        this.pdcBloquearDesbloquearProdutoServContrato = pdcBloquearDesbloquearProdutoServContrato;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirDuplicacaoArqRetorno.
     * 
     * @return Adaptador IncluirDuplicacaoArqRetorno
     */
    public IIncluirDuplicacaoArqRetornoPDCAdapter getIncluirDuplicacaoArqRetornoPDCAdapter() {
        return pdcIncluirDuplicacaoArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirDuplicacaoArqRetorno.
     * 
     * @param pdcIncluirDuplicacaoArqRetorno
     *            Adaptador IncluirDuplicacaoArqRetorno
     */
    public void setIncluirDuplicacaoArqRetornoPDCAdapter(IIncluirDuplicacaoArqRetornoPDCAdapter pdcIncluirDuplicacaoArqRetorno) {
        this.pdcIncluirDuplicacaoArqRetorno = pdcIncluirDuplicacaoArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ListarDuplicacaoArqRetorno.
     * 
     * @return Adaptador ListarDuplicacaoArqRetorno
     */
    public IListarDuplicacaoArqRetornoPDCAdapter getListarDuplicacaoArqRetornoPDCAdapter() {
        return pdcListarDuplicacaoArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarDuplicacaoArqRetorno.
     * 
     * @param pdcListarDuplicacaoArqRetorno
     *            Adaptador ListarDuplicacaoArqRetorno
     */
    public void setListarDuplicacaoArqRetornoPDCAdapter(IListarDuplicacaoArqRetornoPDCAdapter pdcListarDuplicacaoArqRetorno) {
        this.pdcListarDuplicacaoArqRetorno = pdcListarDuplicacaoArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConPagtosSolRecVenNaoPago.
     * 
     * @return Adaptador ConPagtosSolRecVenNaoPago
     */
    public IConPagtosSolRecVenNaoPagoPDCAdapter getConPagtosSolRecVenNaoPagoPDCAdapter() {
        return pdcConPagtosSolRecVenNaoPago;
    }

    /**
     * M�todo invocado para establecer um adaptador ConPagtosSolRecVenNaoPago.
     * 
     * @param pdcConPagtosSolRecVenNaoPago
     *            Adaptador ConPagtosSolRecVenNaoPago
     */
    public void setConPagtosSolRecVenNaoPagoPDCAdapter(IConPagtosSolRecVenNaoPagoPDCAdapter pdcConPagtosSolRecVenNaoPago) {
        this.pdcConPagtosSolRecVenNaoPago = pdcConPagtosSolRecVenNaoPago;
    }
    /**
     * M�todo invocado para obter um adaptador SolicitarRecPagtosVencidosNaoPagos.
     * 
     * @return Adaptador SolicitarRecPagtosVencidosNaoPagos
     */
    public ISolicitarRecPagtosVencidosNaoPagosPDCAdapter getSolicitarRecPagtosVencidosNaoPagosPDCAdapter() {
        return pdcSolicitarRecPagtosVencidosNaoPagos;
    }

    /**
     * M�todo invocado para establecer um adaptador SolicitarRecPagtosVencidosNaoPagos.
     * 
     * @param pdcSolicitarRecPagtosVencidosNaoPagos
     *            Adaptador SolicitarRecPagtosVencidosNaoPagos
     */
    public void setSolicitarRecPagtosVencidosNaoPagosPDCAdapter(ISolicitarRecPagtosVencidosNaoPagosPDCAdapter pdcSolicitarRecPagtosVencidosNaoPagos) {
        this.pdcSolicitarRecPagtosVencidosNaoPagos = pdcSolicitarRecPagtosVencidosNaoPagos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoPendenciaResponsaveis.
     * 
     * @return Adaptador ListarTipoPendenciaResponsaveis
     */
    public IListarTipoPendenciaResponsaveisPDCAdapter getListarTipoPendenciaResponsaveisPDCAdapter() {
        return pdcListarTipoPendenciaResponsaveis;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoPendenciaResponsaveis.
     * 
     * @param pdcListarTipoPendenciaResponsaveis
     *            Adaptador ListarTipoPendenciaResponsaveis
     */
    public void setListarTipoPendenciaResponsaveisPDCAdapter(IListarTipoPendenciaResponsaveisPDCAdapter pdcListarTipoPendenciaResponsaveis) {
        this.pdcListarTipoPendenciaResponsaveis = pdcListarTipoPendenciaResponsaveis;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoCreditoConta.
     * 
     * @return Adaptador DetalharManPagtoCreditoConta
     */
    public IDetalharManPagtoCreditoContaPDCAdapter getDetalharManPagtoCreditoContaPDCAdapter() {
        return pdcDetalharManPagtoCreditoConta;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoCreditoConta.
     * 
     * @param pdcDetalharManPagtoCreditoConta
     *            Adaptador DetalharManPagtoCreditoConta
     */
    public void setDetalharManPagtoCreditoContaPDCAdapter(IDetalharManPagtoCreditoContaPDCAdapter pdcDetalharManPagtoCreditoConta) {
        this.pdcDetalharManPagtoCreditoConta = pdcDetalharManPagtoCreditoConta;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarModalidadeServico.
     * 
     * @return Adaptador ConsultarModalidadeServico
     */
    public IConsultarModalidadeServicoPDCAdapter getConsultarModalidadeServicoPDCAdapter() {
        return pdcConsultarModalidadeServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarModalidadeServico.
     * 
     * @param pdcConsultarModalidadeServico
     *            Adaptador ConsultarModalidadeServico
     */
    public void setConsultarModalidadeServicoPDCAdapter(IConsultarModalidadeServicoPDCAdapter pdcConsultarModalidadeServico) {
        this.pdcConsultarModalidadeServico = pdcConsultarModalidadeServico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaClientePessoas.
     * 
     * @return Adaptador ConsultarListaClientePessoas
     */
    public IConsultarListaClientePessoasPDCAdapter getConsultarListaClientePessoasPDCAdapter() {
        return pdcConsultarListaClientePessoas;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaClientePessoas.
     * 
     * @param pdcConsultarListaClientePessoas
     *            Adaptador ConsultarListaClientePessoas
     */
    public void setConsultarListaClientePessoasPDCAdapter(IConsultarListaClientePessoasPDCAdapter pdcConsultarListaClientePessoas) {
        this.pdcConsultarListaClientePessoas = pdcConsultarListaClientePessoas;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarBancoAgencia.
     * 
     * @return Adaptador ConsultarBancoAgencia
     */
    public IConsultarBancoAgenciaPDCAdapter getConsultarBancoAgenciaPDCAdapter() {
        return pdcConsultarBancoAgencia;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarBancoAgencia.
     * 
     * @param pdcConsultarBancoAgencia
     *            Adaptador ConsultarBancoAgencia
     */
    public void setConsultarBancoAgenciaPDCAdapter(IConsultarBancoAgenciaPDCAdapter pdcConsultarBancoAgencia) {
        this.pdcConsultarBancoAgencia = pdcConsultarBancoAgencia;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirOperacaoServicoPagtoIntegrado.
     * 
     * @return Adaptador ExcluirOperacaoServicoPagtoIntegrado
     */
    public IExcluirOperacaoServicoPagtoIntegradoPDCAdapter getExcluirOperacaoServicoPagtoIntegradoPDCAdapter() {
        return pdcExcluirOperacaoServicoPagtoIntegrado;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirOperacaoServicoPagtoIntegrado.
     * 
     * @param pdcExcluirOperacaoServicoPagtoIntegrado
     *            Adaptador ExcluirOperacaoServicoPagtoIntegrado
     */
    public void setExcluirOperacaoServicoPagtoIntegradoPDCAdapter(IExcluirOperacaoServicoPagtoIntegradoPDCAdapter pdcExcluirOperacaoServicoPagtoIntegrado) {
        this.pdcExcluirOperacaoServicoPagtoIntegrado = pdcExcluirOperacaoServicoPagtoIntegrado;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAlteracaoAmbiente.
     * 
     * @return Adaptador IncluirAlteracaoAmbiente
     */
    public IIncluirAlteracaoAmbientePDCAdapter getIncluirAlteracaoAmbientePDCAdapter() {
        return pdcIncluirAlteracaoAmbiente;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAlteracaoAmbiente.
     * 
     * @param pdcIncluirAlteracaoAmbiente
     *            Adaptador IncluirAlteracaoAmbiente
     */
    public void setIncluirAlteracaoAmbientePDCAdapter(IIncluirAlteracaoAmbientePDCAdapter pdcIncluirAlteracaoAmbiente) {
        this.pdcIncluirAlteracaoAmbiente = pdcIncluirAlteracaoAmbiente;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicitacaoMudancaAmbiente.
     * 
     * @return Adaptador ConsultarSolicitacaoMudancaAmbiente
     */
    public IConsultarSolicitacaoMudancaAmbientePDCAdapter getConsultarSolicitacaoMudancaAmbientePDCAdapter() {
        return pdcConsultarSolicitacaoMudancaAmbiente;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicitacaoMudancaAmbiente.
     * 
     * @param pdcConsultarSolicitacaoMudancaAmbiente
     *            Adaptador ConsultarSolicitacaoMudancaAmbiente
     */
    public void setConsultarSolicitacaoMudancaAmbientePDCAdapter(IConsultarSolicitacaoMudancaAmbientePDCAdapter pdcConsultarSolicitacaoMudancaAmbiente) {
        this.pdcConsultarSolicitacaoMudancaAmbiente = pdcConsultarSolicitacaoMudancaAmbiente;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirDuplicacaoArqRetorno.
     * 
     * @return Adaptador ExcluirDuplicacaoArqRetorno
     */
    public IExcluirDuplicacaoArqRetornoPDCAdapter getExcluirDuplicacaoArqRetornoPDCAdapter() {
        return pdcExcluirDuplicacaoArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirDuplicacaoArqRetorno.
     * 
     * @param pdcExcluirDuplicacaoArqRetorno
     *            Adaptador ExcluirDuplicacaoArqRetorno
     */
    public void setExcluirDuplicacaoArqRetornoPDCAdapter(IExcluirDuplicacaoArqRetornoPDCAdapter pdcExcluirDuplicacaoArqRetorno) {
        this.pdcExcluirDuplicacaoArqRetorno = pdcExcluirDuplicacaoArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirLayoutArqServico.
     * 
     * @return Adaptador ExcluirLayoutArqServico
     */
    public IExcluirLayoutArqServicoPDCAdapter getExcluirLayoutArqServicoPDCAdapter() {
        return pdcExcluirLayoutArqServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirLayoutArqServico.
     * 
     * @param pdcExcluirLayoutArqServico
     *            Adaptador ExcluirLayoutArqServico
     */
    public void setExcluirLayoutArqServicoPDCAdapter(IExcluirLayoutArqServicoPDCAdapter pdcExcluirLayoutArqServico) {
        this.pdcExcluirLayoutArqServico = pdcExcluirLayoutArqServico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarHistoricoPerfilTrocaArquivo.
     * 
     * @return Adaptador ConsultarHistoricoPerfilTrocaArquivo
     */
    public IConsultarHistoricoPerfilTrocaArquivoPDCAdapter getConsultarHistoricoPerfilTrocaArquivoPDCAdapter() {
        return pdcConsultarHistoricoPerfilTrocaArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarHistoricoPerfilTrocaArquivo.
     * 
     * @param pdcConsultarHistoricoPerfilTrocaArquivo
     *            Adaptador ConsultarHistoricoPerfilTrocaArquivo
     */
    public void setConsultarHistoricoPerfilTrocaArquivoPDCAdapter(IConsultarHistoricoPerfilTrocaArquivoPDCAdapter pdcConsultarHistoricoPerfilTrocaArquivo) {
        this.pdcConsultarHistoricoPerfilTrocaArquivo = pdcConsultarHistoricoPerfilTrocaArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarEmail.
     * 
     * @return Adaptador ConsultarEmail
     */
    public IConsultarEmailPDCAdapter getConsultarEmailPDCAdapter() {
        return pdcConsultarEmail;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarEmail.
     * 
     * @param pdcConsultarEmail
     *            Adaptador ConsultarEmail
     */
    public void setConsultarEmailPDCAdapter(IConsultarEmailPDCAdapter pdcConsultarEmail) {
        this.pdcConsultarEmail = pdcConsultarEmail;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarEndereco.
     * 
     * @return Adaptador ConsultarEndereco
     */
    public IConsultarEnderecoPDCAdapter getConsultarEnderecoPDCAdapter() {
        return pdcConsultarEndereco;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarEndereco.
     * 
     * @param pdcConsultarEndereco
     *            Adaptador ConsultarEndereco
     */
    public void setConsultarEnderecoPDCAdapter(IConsultarEnderecoPDCAdapter pdcConsultarEndereco) {
        this.pdcConsultarEndereco = pdcConsultarEndereco;
    }
    /**
     * M�todo invocado para obter um adaptador AutorizarSolicitacaoEstornoPatos.
     * 
     * @return Adaptador AutorizarSolicitacaoEstornoPatos
     */
    public IAutorizarSolicitacaoEstornoPatosPDCAdapter getAutorizarSolicitacaoEstornoPatosPDCAdapter() {
        return pdcAutorizarSolicitacaoEstornoPatos;
    }

    /**
     * M�todo invocado para establecer um adaptador AutorizarSolicitacaoEstornoPatos.
     * 
     * @param pdcAutorizarSolicitacaoEstornoPatos
     *            Adaptador AutorizarSolicitacaoEstornoPatos
     */
    public void setAutorizarSolicitacaoEstornoPatosPDCAdapter(IAutorizarSolicitacaoEstornoPatosPDCAdapter pdcAutorizarSolicitacaoEstornoPatos) {
        this.pdcAutorizarSolicitacaoEstornoPatos = pdcAutorizarSolicitacaoEstornoPatos;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicitacaoEstornoPagtos.
     * 
     * @return Adaptador ExcluirSolicitacaoEstornoPagtos
     */
    public IExcluirSolicitacaoEstornoPagtosPDCAdapter getExcluirSolicitacaoEstornoPagtosPDCAdapter() {
        return pdcExcluirSolicitacaoEstornoPagtos;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicitacaoEstornoPagtos.
     * 
     * @param pdcExcluirSolicitacaoEstornoPagtos
     *            Adaptador ExcluirSolicitacaoEstornoPagtos
     */
    public void setExcluirSolicitacaoEstornoPagtosPDCAdapter(IExcluirSolicitacaoEstornoPagtosPDCAdapter pdcExcluirSolicitacaoEstornoPagtos) {
        this.pdcExcluirSolicitacaoEstornoPagtos = pdcExcluirSolicitacaoEstornoPagtos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMotivoBloqueioDesbloqueio.
     * 
     * @return Adaptador ListarMotivoBloqueioDesbloqueio
     */
    public IListarMotivoBloqueioDesbloqueioPDCAdapter getListarMotivoBloqueioDesbloqueioPDCAdapter() {
        return pdcListarMotivoBloqueioDesbloqueio;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMotivoBloqueioDesbloqueio.
     * 
     * @param pdcListarMotivoBloqueioDesbloqueio
     *            Adaptador ListarMotivoBloqueioDesbloqueio
     */
    public void setListarMotivoBloqueioDesbloqueioPDCAdapter(IListarMotivoBloqueioDesbloqueioPDCAdapter pdcListarMotivoBloqueioDesbloqueio) {
        this.pdcListarMotivoBloqueioDesbloqueio = pdcListarMotivoBloqueioDesbloqueio;
    }
    /**
     * M�todo invocado para obter um adaptador BloquearDesbloquearContratoPgit.
     * 
     * @return Adaptador BloquearDesbloquearContratoPgit
     */
    public IBloquearDesbloquearContratoPgitPDCAdapter getBloquearDesbloquearContratoPgitPDCAdapter() {
        return pdcBloquearDesbloquearContratoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador BloquearDesbloquearContratoPgit.
     * 
     * @param pdcBloquearDesbloquearContratoPgit
     *            Adaptador BloquearDesbloquearContratoPgit
     */
    public void setBloquearDesbloquearContratoPgitPDCAdapter(IBloquearDesbloquearContratoPgitPDCAdapter pdcBloquearDesbloquearContratoPgit) {
        this.pdcBloquearDesbloquearContratoPgit = pdcBloquearDesbloquearContratoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarMsgLayoutArqRetorno.
     * 
     * @return Adaptador AlterarMsgLayoutArqRetorno
     */
    public IAlterarMsgLayoutArqRetornoPDCAdapter getAlterarMsgLayoutArqRetornoPDCAdapter() {
        return pdcAlterarMsgLayoutArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarMsgLayoutArqRetorno.
     * 
     * @param pdcAlterarMsgLayoutArqRetorno
     *            Adaptador AlterarMsgLayoutArqRetorno
     */
    public void setAlterarMsgLayoutArqRetornoPDCAdapter(IAlterarMsgLayoutArqRetornoPDCAdapter pdcAlterarMsgLayoutArqRetorno) {
        this.pdcAlterarMsgLayoutArqRetorno = pdcAlterarMsgLayoutArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador RevertContratoMigrado.
     * 
     * @return Adaptador RevertContratoMigrado
     */
    public IRevertContratoMigradoPDCAdapter getRevertContratoMigradoPDCAdapter() {
        return pdcRevertContratoMigrado;
    }

    /**
     * M�todo invocado para establecer um adaptador RevertContratoMigrado.
     * 
     * @param pdcRevertContratoMigrado
     *            Adaptador RevertContratoMigrado
     */
    public void setRevertContratoMigradoPDCAdapter(IRevertContratoMigradoPDCAdapter pdcRevertContratoMigrado) {
        this.pdcRevertContratoMigrado = pdcRevertContratoMigrado;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarRepresentanteContratoPgit.
     * 
     * @return Adaptador AlterarRepresentanteContratoPgit
     */
    public IAlterarRepresentanteContratoPgitPDCAdapter getAlterarRepresentanteContratoPgitPDCAdapter() {
        return pdcAlterarRepresentanteContratoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarRepresentanteContratoPgit.
     * 
     * @param pdcAlterarRepresentanteContratoPgit
     *            Adaptador AlterarRepresentanteContratoPgit
     */
    public void setAlterarRepresentanteContratoPgitPDCAdapter(IAlterarRepresentanteContratoPgitPDCAdapter pdcAlterarRepresentanteContratoPgit) {
        this.pdcAlterarRepresentanteContratoPgit = pdcAlterarRepresentanteContratoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaContasInclusao.
     * 
     * @return Adaptador ConsultarListaContasInclusao
     */
    public IConsultarListaContasInclusaoPDCAdapter getConsultarListaContasInclusaoPDCAdapter() {
        return pdcConsultarListaContasInclusao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaContasInclusao.
     * 
     * @param pdcConsultarListaContasInclusao
     *            Adaptador ConsultarListaContasInclusao
     */
    public void setConsultarListaContasInclusaoPDCAdapter(IConsultarListaContasInclusaoPDCAdapter pdcConsultarListaContasInclusao) {
        this.pdcConsultarListaContasInclusao = pdcConsultarListaContasInclusao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarFinalidadeEnderecoOperacao.
     * 
     * @return Adaptador ListarFinalidadeEnderecoOperacao
     */
    public IListarFinalidadeEnderecoOperacaoPDCAdapter getListarFinalidadeEnderecoOperacaoPDCAdapter() {
        return pdcListarFinalidadeEnderecoOperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarFinalidadeEnderecoOperacao.
     * 
     * @param pdcListarFinalidadeEnderecoOperacao
     *            Adaptador ListarFinalidadeEnderecoOperacao
     */
    public void setListarFinalidadeEnderecoOperacaoPDCAdapter(IListarFinalidadeEnderecoOperacaoPDCAdapter pdcListarFinalidadeEnderecoOperacao) {
        this.pdcListarFinalidadeEnderecoOperacao = pdcListarFinalidadeEnderecoOperacao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirFinalidadeEnderecoOperacao.
     * 
     * @return Adaptador IncluirFinalidadeEnderecoOperacao
     */
    public IIncluirFinalidadeEnderecoOperacaoPDCAdapter getIncluirFinalidadeEnderecoOperacaoPDCAdapter() {
        return pdcIncluirFinalidadeEnderecoOperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirFinalidadeEnderecoOperacao.
     * 
     * @param pdcIncluirFinalidadeEnderecoOperacao
     *            Adaptador IncluirFinalidadeEnderecoOperacao
     */
    public void setIncluirFinalidadeEnderecoOperacaoPDCAdapter(IIncluirFinalidadeEnderecoOperacaoPDCAdapter pdcIncluirFinalidadeEnderecoOperacao) {
        this.pdcIncluirFinalidadeEnderecoOperacao = pdcIncluirFinalidadeEnderecoOperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirFinalidadeEnderecoOperacao.
     * 
     * @return Adaptador ExcluirFinalidadeEnderecoOperacao
     */
    public IExcluirFinalidadeEnderecoOperacaoPDCAdapter getExcluirFinalidadeEnderecoOperacaoPDCAdapter() {
        return pdcExcluirFinalidadeEnderecoOperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirFinalidadeEnderecoOperacao.
     * 
     * @param pdcExcluirFinalidadeEnderecoOperacao
     *            Adaptador ExcluirFinalidadeEnderecoOperacao
     */
    public void setExcluirFinalidadeEnderecoOperacaoPDCAdapter(IExcluirFinalidadeEnderecoOperacaoPDCAdapter pdcExcluirFinalidadeEnderecoOperacao) {
        this.pdcExcluirFinalidadeEnderecoOperacao = pdcExcluirFinalidadeEnderecoOperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParticipantesContrato.
     * 
     * @return Adaptador ListarParticipantesContrato
     */
    public IListarParticipantesContratoPDCAdapter getListarParticipantesContratoPDCAdapter() {
        return pdcListarParticipantesContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParticipantesContrato.
     * 
     * @param pdcListarParticipantesContrato
     *            Adaptador ListarParticipantesContrato
     */
    public void setListarParticipantesContratoPDCAdapter(IListarParticipantesContratoPDCAdapter pdcListarParticipantesContrato) {
        this.pdcListarParticipantesContrato = pdcListarParticipantesContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirContrEnvioArqFormaLiquidacao.
     * 
     * @return Adaptador ExcluirContrEnvioArqFormaLiquidacao
     */
    public IExcluirContrEnvioArqFormaLiquidacaoPDCAdapter getExcluirContrEnvioArqFormaLiquidacaoPDCAdapter() {
        return pdcExcluirContrEnvioArqFormaLiquidacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirContrEnvioArqFormaLiquidacao.
     * 
     * @param pdcExcluirContrEnvioArqFormaLiquidacao
     *            Adaptador ExcluirContrEnvioArqFormaLiquidacao
     */
    public void setExcluirContrEnvioArqFormaLiquidacaoPDCAdapter(IExcluirContrEnvioArqFormaLiquidacaoPDCAdapter pdcExcluirContrEnvioArqFormaLiquidacao) {
        this.pdcExcluirContrEnvioArqFormaLiquidacao = pdcExcluirContrEnvioArqFormaLiquidacao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirContrEnvioArqFormaLiquidacao.
     * 
     * @return Adaptador IncluirContrEnvioArqFormaLiquidacao
     */
    public IIncluirContrEnvioArqFormaLiquidacaoPDCAdapter getIncluirContrEnvioArqFormaLiquidacaoPDCAdapter() {
        return pdcIncluirContrEnvioArqFormaLiquidacao;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirContrEnvioArqFormaLiquidacao.
     * 
     * @param pdcIncluirContrEnvioArqFormaLiquidacao
     *            Adaptador IncluirContrEnvioArqFormaLiquidacao
     */
    public void setIncluirContrEnvioArqFormaLiquidacaoPDCAdapter(IIncluirContrEnvioArqFormaLiquidacaoPDCAdapter pdcIncluirContrEnvioArqFormaLiquidacao) {
        this.pdcIncluirContrEnvioArqFormaLiquidacao = pdcIncluirContrEnvioArqFormaLiquidacao;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharFinalidadeEnderecoOperacao.
     * 
     * @return Adaptador DetalharFinalidadeEnderecoOperacao
     */
    public IDetalharFinalidadeEnderecoOperacaoPDCAdapter getDetalharFinalidadeEnderecoOperacaoPDCAdapter() {
        return pdcDetalharFinalidadeEnderecoOperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharFinalidadeEnderecoOperacao.
     * 
     * @param pdcDetalharFinalidadeEnderecoOperacao
     *            Adaptador DetalharFinalidadeEnderecoOperacao
     */
    public void setDetalharFinalidadeEnderecoOperacaoPDCAdapter(IDetalharFinalidadeEnderecoOperacaoPDCAdapter pdcDetalharFinalidadeEnderecoOperacao) {
        this.pdcDetalharFinalidadeEnderecoOperacao = pdcDetalharFinalidadeEnderecoOperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirServicoRelacionado.
     * 
     * @return Adaptador ExcluirServicoRelacionado
     */
    public IExcluirServicoRelacionadoPDCAdapter getExcluirServicoRelacionadoPDCAdapter() {
        return pdcExcluirServicoRelacionado;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirServicoRelacionado.
     * 
     * @param pdcExcluirServicoRelacionado
     *            Adaptador ExcluirServicoRelacionado
     */
    public void setExcluirServicoRelacionadoPDCAdapter(IExcluirServicoRelacionadoPDCAdapter pdcExcluirServicoRelacionado) {
        this.pdcExcluirServicoRelacionado = pdcExcluirServicoRelacionado;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoRelacionado.
     * 
     * @return Adaptador ListarServicoRelacionado
     */
    public IListarServicoRelacionadoPDCAdapter getListarServicoRelacionadoPDCAdapter() {
        return pdcListarServicoRelacionado;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoRelacionado.
     * 
     * @param pdcListarServicoRelacionado
     *            Adaptador ListarServicoRelacionado
     */
    public void setListarServicoRelacionadoPDCAdapter(IListarServicoRelacionadoPDCAdapter pdcListarServicoRelacionado) {
        this.pdcListarServicoRelacionado = pdcListarServicoRelacionado;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirMsgLayoutArqRetorno.
     * 
     * @return Adaptador ExcluirMsgLayoutArqRetorno
     */
    public IExcluirMsgLayoutArqRetornoPDCAdapter getExcluirMsgLayoutArqRetornoPDCAdapter() {
        return pdcExcluirMsgLayoutArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirMsgLayoutArqRetorno.
     * 
     * @param pdcExcluirMsgLayoutArqRetorno
     *            Adaptador ExcluirMsgLayoutArqRetorno
     */
    public void setExcluirMsgLayoutArqRetornoPDCAdapter(IExcluirMsgLayoutArqRetornoPDCAdapter pdcExcluirMsgLayoutArqRetorno) {
        this.pdcExcluirMsgLayoutArqRetorno = pdcExcluirMsgLayoutArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirMsgLayoutArqRetorno.
     * 
     * @return Adaptador IncluirMsgLayoutArqRetorno
     */
    public IIncluirMsgLayoutArqRetornoPDCAdapter getIncluirMsgLayoutArqRetornoPDCAdapter() {
        return pdcIncluirMsgLayoutArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirMsgLayoutArqRetorno.
     * 
     * @param pdcIncluirMsgLayoutArqRetorno
     *            Adaptador IncluirMsgLayoutArqRetorno
     */
    public void setIncluirMsgLayoutArqRetornoPDCAdapter(IIncluirMsgLayoutArqRetornoPDCAdapter pdcIncluirMsgLayoutArqRetorno) {
        this.pdcIncluirMsgLayoutArqRetorno = pdcIncluirMsgLayoutArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPosDiaPagtoAgencia.
     * 
     * @return Adaptador ConsultarPosDiaPagtoAgencia
     */
    public IConsultarPosDiaPagtoAgenciaPDCAdapter getConsultarPosDiaPagtoAgenciaPDCAdapter() {
        return pdcConsultarPosDiaPagtoAgencia;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPosDiaPagtoAgencia.
     * 
     * @param pdcConsultarPosDiaPagtoAgencia
     *            Adaptador ConsultarPosDiaPagtoAgencia
     */
    public void setConsultarPosDiaPagtoAgenciaPDCAdapter(IConsultarPosDiaPagtoAgenciaPDCAdapter pdcConsultarPosDiaPagtoAgencia) {
        this.pdcConsultarPosDiaPagtoAgencia = pdcConsultarPosDiaPagtoAgencia;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPorContaDebito.
     * 
     * @return Adaptador DetalharPorContaDebito
     */
    public IDetalharPorContaDebitoPDCAdapter getDetalharPorContaDebitoPDCAdapter() {
        return pdcDetalharPorContaDebito;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPorContaDebito.
     * 
     * @param pdcDetalharPorContaDebito
     *            Adaptador DetalharPorContaDebito
     */
    public void setDetalharPorContaDebitoPDCAdapter(IDetalharPorContaDebitoPDCAdapter pdcDetalharPorContaDebito) {
        this.pdcDetalharPorContaDebito = pdcDetalharPorContaDebito;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarTarifaContrato.
     * 
     * @return Adaptador ConsultarTarifaContrato
     */
    public IConsultarTarifaContratoPDCAdapter getConsultarTarifaContratoPDCAdapter() {
        return pdcConsultarTarifaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarTarifaContrato.
     * 
     * @param pdcConsultarTarifaContrato
     *            Adaptador ConsultarTarifaContrato
     */
    public void setConsultarTarifaContratoPDCAdapter(IConsultarTarifaContratoPDCAdapter pdcConsultarTarifaContrato) {
        this.pdcConsultarTarifaContrato = pdcConsultarTarifaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AnteciparPostergarPagtosIndividuais.
     * 
     * @return Adaptador AnteciparPostergarPagtosIndividuais
     */
    public IAnteciparPostergarPagtosIndividuaisPDCAdapter getAnteciparPostergarPagtosIndividuaisPDCAdapter() {
        return pdcAnteciparPostergarPagtosIndividuais;
    }

    /**
     * M�todo invocado para establecer um adaptador AnteciparPostergarPagtosIndividuais.
     * 
     * @param pdcAnteciparPostergarPagtosIndividuais
     *            Adaptador AnteciparPostergarPagtosIndividuais
     */
    public void setAnteciparPostergarPagtosIndividuaisPDCAdapter(IAnteciparPostergarPagtosIndividuaisPDCAdapter pdcAnteciparPostergarPagtosIndividuais) {
        this.pdcAnteciparPostergarPagtosIndividuais = pdcAnteciparPostergarPagtosIndividuais;
    }
    /**
     * M�todo invocado para obter um adaptador AntPostergarPagtosParcial.
     * 
     * @return Adaptador AntPostergarPagtosParcial
     */
    public IAntPostergarPagtosParcialPDCAdapter getAntPostergarPagtosParcialPDCAdapter() {
        return pdcAntPostergarPagtosParcial;
    }

    /**
     * M�todo invocado para establecer um adaptador AntPostergarPagtosParcial.
     * 
     * @param pdcAntPostergarPagtosParcial
     *            Adaptador AntPostergarPagtosParcial
     */
    public void setAntPostergarPagtosParcialPDCAdapter(IAntPostergarPagtosParcialPDCAdapter pdcAntPostergarPagtosParcial) {
        this.pdcAntPostergarPagtosParcial = pdcAntPostergarPagtosParcial;
    }
    /**
     * M�todo invocado para obter um adaptador AntPostergarPagtosIntegral.
     * 
     * @return Adaptador AntPostergarPagtosIntegral
     */
    public IAntPostergarPagtosIntegralPDCAdapter getAntPostergarPagtosIntegralPDCAdapter() {
        return pdcAntPostergarPagtosIntegral;
    }

    /**
     * M�todo invocado para establecer um adaptador AntPostergarPagtosIntegral.
     * 
     * @param pdcAntPostergarPagtosIntegral
     *            Adaptador AntPostergarPagtosIntegral
     */
    public void setAntPostergarPagtosIntegralPDCAdapter(IAntPostergarPagtosIntegralPDCAdapter pdcAntPostergarPagtosIntegral) {
        this.pdcAntPostergarPagtosIntegral = pdcAntPostergarPagtosIntegral;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSaldoAtual.
     * 
     * @return Adaptador ConsultarSaldoAtual
     */
    public IConsultarSaldoAtualPDCAdapter getConsultarSaldoAtualPDCAdapter() {
        return pdcConsultarSaldoAtual;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSaldoAtual.
     * 
     * @param pdcConsultarSaldoAtual
     *            Adaptador ConsultarSaldoAtual
     */
    public void setConsultarSaldoAtualPDCAdapter(IConsultarSaldoAtualPDCAdapter pdcConsultarSaldoAtual) {
        this.pdcConsultarSaldoAtual = pdcConsultarSaldoAtual;
    }
    /**
     * M�todo invocado para obter um adaptador ConsutarContrEnvioFormaLiquidacao.
     * 
     * @return Adaptador ConsutarContrEnvioFormaLiquidacao
     */
    public IConsutarContrEnvioFormaLiquidacaoPDCAdapter getConsutarContrEnvioFormaLiquidacaoPDCAdapter() {
        return pdcConsutarContrEnvioFormaLiquidacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsutarContrEnvioFormaLiquidacao.
     * 
     * @param pdcConsutarContrEnvioFormaLiquidacao
     *            Adaptador ConsutarContrEnvioFormaLiquidacao
     */
    public void setConsutarContrEnvioFormaLiquidacaoPDCAdapter(IConsutarContrEnvioFormaLiquidacaoPDCAdapter pdcConsutarContrEnvioFormaLiquidacao) {
        this.pdcConsutarContrEnvioFormaLiquidacao = pdcConsutarContrEnvioFormaLiquidacao;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarPrioridadeTipoCompromisso.
     * 
     * @return Adaptador AlterarPrioridadeTipoCompromisso
     */
    public IAlterarPrioridadeTipoCompromissoPDCAdapter getAlterarPrioridadeTipoCompromissoPDCAdapter() {
        return pdcAlterarPrioridadeTipoCompromisso;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarPrioridadeTipoCompromisso.
     * 
     * @param pdcAlterarPrioridadeTipoCompromisso
     *            Adaptador AlterarPrioridadeTipoCompromisso
     */
    public void setAlterarPrioridadeTipoCompromissoPDCAdapter(IAlterarPrioridadeTipoCompromissoPDCAdapter pdcAlterarPrioridadeTipoCompromisso) {
        this.pdcAlterarPrioridadeTipoCompromisso = pdcAlterarPrioridadeTipoCompromisso;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharContrEnvioFormaLiquidacao.
     * 
     * @return Adaptador DetalharContrEnvioFormaLiquidacao
     */
    public IDetalharContrEnvioFormaLiquidacaoPDCAdapter getDetalharContrEnvioFormaLiquidacaoPDCAdapter() {
        return pdcDetalharContrEnvioFormaLiquidacao;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharContrEnvioFormaLiquidacao.
     * 
     * @param pdcDetalharContrEnvioFormaLiquidacao
     *            Adaptador DetalharContrEnvioFormaLiquidacao
     */
    public void setDetalharContrEnvioFormaLiquidacaoPDCAdapter(IDetalharContrEnvioFormaLiquidacaoPDCAdapter pdcDetalharContrEnvioFormaLiquidacao) {
        this.pdcDetalharContrEnvioFormaLiquidacao = pdcDetalharContrEnvioFormaLiquidacao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirServicoLancamentoCnab.
     * 
     * @return Adaptador IncluirServicoLancamentoCnab
     */
    public IIncluirServicoLancamentoCnabPDCAdapter getIncluirServicoLancamentoCnabPDCAdapter() {
        return pdcIncluirServicoLancamentoCnab;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirServicoLancamentoCnab.
     * 
     * @param pdcIncluirServicoLancamentoCnab
     *            Adaptador IncluirServicoLancamentoCnab
     */
    public void setIncluirServicoLancamentoCnabPDCAdapter(IIncluirServicoLancamentoCnabPDCAdapter pdcIncluirServicoLancamentoCnab) {
        this.pdcIncluirServicoLancamentoCnab = pdcIncluirServicoLancamentoCnab;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirServicoLancamentoCnab.
     * 
     * @return Adaptador ExcluirServicoLancamentoCnab
     */
    public IExcluirServicoLancamentoCnabPDCAdapter getExcluirServicoLancamentoCnabPDCAdapter() {
        return pdcExcluirServicoLancamentoCnab;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirServicoLancamentoCnab.
     * 
     * @param pdcExcluirServicoLancamentoCnab
     *            Adaptador ExcluirServicoLancamentoCnab
     */
    public void setExcluirServicoLancamentoCnabPDCAdapter(IExcluirServicoLancamentoCnabPDCAdapter pdcExcluirServicoLancamentoCnab) {
        this.pdcExcluirServicoLancamentoCnab = pdcExcluirServicoLancamentoCnab;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarServicoLancamentoCnab.
     * 
     * @return Adaptador ConsultarServicoLancamentoCnab
     */
    public IConsultarServicoLancamentoCnabPDCAdapter getConsultarServicoLancamentoCnabPDCAdapter() {
        return pdcConsultarServicoLancamentoCnab;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarServicoLancamentoCnab.
     * 
     * @param pdcConsultarServicoLancamentoCnab
     *            Adaptador ConsultarServicoLancamentoCnab
     */
    public void setConsultarServicoLancamentoCnabPDCAdapter(IConsultarServicoLancamentoCnabPDCAdapter pdcConsultarServicoLancamentoCnab) {
        this.pdcConsultarServicoLancamentoCnab = pdcConsultarServicoLancamentoCnab;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharDuplicacaoArqRetorno.
     * 
     * @return Adaptador DetalharDuplicacaoArqRetorno
     */
    public IDetalharDuplicacaoArqRetornoPDCAdapter getDetalharDuplicacaoArqRetornoPDCAdapter() {
        return pdcDetalharDuplicacaoArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharDuplicacaoArqRetorno.
     * 
     * @param pdcDetalharDuplicacaoArqRetorno
     *            Adaptador DetalharDuplicacaoArqRetorno
     */
    public void setDetalharDuplicacaoArqRetornoPDCAdapter(IDetalharDuplicacaoArqRetornoPDCAdapter pdcDetalharDuplicacaoArqRetorno) {
        this.pdcDetalharDuplicacaoArqRetorno = pdcDetalharDuplicacaoArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarVinculacaoMsgLayoutSistema.
     * 
     * @return Adaptador ConsultarVinculacaoMsgLayoutSistema
     */
    public IConsultarVinculacaoMsgLayoutSistemaPDCAdapter getConsultarVinculacaoMsgLayoutSistemaPDCAdapter() {
        return pdcConsultarVinculacaoMsgLayoutSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarVinculacaoMsgLayoutSistema.
     * 
     * @param pdcConsultarVinculacaoMsgLayoutSistema
     *            Adaptador ConsultarVinculacaoMsgLayoutSistema
     */
    public void setConsultarVinculacaoMsgLayoutSistemaPDCAdapter(IConsultarVinculacaoMsgLayoutSistemaPDCAdapter pdcConsultarVinculacaoMsgLayoutSistema) {
        this.pdcConsultarVinculacaoMsgLayoutSistema = pdcConsultarVinculacaoMsgLayoutSistema;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirVinculacaoMsgLayoutSistema.
     * 
     * @return Adaptador ExcluirVinculacaoMsgLayoutSistema
     */
    public IExcluirVinculacaoMsgLayoutSistemaPDCAdapter getExcluirVinculacaoMsgLayoutSistemaPDCAdapter() {
        return pdcExcluirVinculacaoMsgLayoutSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirVinculacaoMsgLayoutSistema.
     * 
     * @param pdcExcluirVinculacaoMsgLayoutSistema
     *            Adaptador ExcluirVinculacaoMsgLayoutSistema
     */
    public void setExcluirVinculacaoMsgLayoutSistemaPDCAdapter(IExcluirVinculacaoMsgLayoutSistemaPDCAdapter pdcExcluirVinculacaoMsgLayoutSistema) {
        this.pdcExcluirVinculacaoMsgLayoutSistema = pdcExcluirVinculacaoMsgLayoutSistema;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirVinculacaoMsgLayoutSistema.
     * 
     * @return Adaptador IncluirVinculacaoMsgLayoutSistema
     */
    public IIncluirVinculacaoMsgLayoutSistemaPDCAdapter getIncluirVinculacaoMsgLayoutSistemaPDCAdapter() {
        return pdcIncluirVinculacaoMsgLayoutSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirVinculacaoMsgLayoutSistema.
     * 
     * @param pdcIncluirVinculacaoMsgLayoutSistema
     *            Adaptador IncluirVinculacaoMsgLayoutSistema
     */
    public void setIncluirVinculacaoMsgLayoutSistemaPDCAdapter(IIncluirVinculacaoMsgLayoutSistemaPDCAdapter pdcIncluirVinculacaoMsgLayoutSistema) {
        this.pdcIncluirVinculacaoMsgLayoutSistema = pdcIncluirVinculacaoMsgLayoutSistema;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharServicoLancamentoCnab.
     * 
     * @return Adaptador DetalharServicoLancamentoCnab
     */
    public IDetalharServicoLancamentoCnabPDCAdapter getDetalharServicoLancamentoCnabPDCAdapter() {
        return pdcDetalharServicoLancamentoCnab;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharServicoLancamentoCnab.
     * 
     * @param pdcDetalharServicoLancamentoCnab
     *            Adaptador DetalharServicoLancamentoCnab
     */
    public void setDetalharServicoLancamentoCnabPDCAdapter(IDetalharServicoLancamentoCnabPDCAdapter pdcDetalharServicoLancamentoCnab) {
        this.pdcDetalharServicoLancamentoCnab = pdcDetalharServicoLancamentoCnab;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharVinculacaoMsgLayoutSistema.
     * 
     * @return Adaptador DetalharVinculacaoMsgLayoutSistema
     */
    public IDetalharVinculacaoMsgLayoutSistemaPDCAdapter getDetalharVinculacaoMsgLayoutSistemaPDCAdapter() {
        return pdcDetalharVinculacaoMsgLayoutSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharVinculacaoMsgLayoutSistema.
     * 
     * @param pdcDetalharVinculacaoMsgLayoutSistema
     *            Adaptador DetalharVinculacaoMsgLayoutSistema
     */
    public void setDetalharVinculacaoMsgLayoutSistemaPDCAdapter(IDetalharVinculacaoMsgLayoutSistemaPDCAdapter pdcDetalharVinculacaoMsgLayoutSistema) {
        this.pdcDetalharVinculacaoMsgLayoutSistema = pdcDetalharVinculacaoMsgLayoutSistema;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDescBancoAgenciaConta.
     * 
     * @return Adaptador ConsultarDescBancoAgenciaConta
     */
    public IConsultarDescBancoAgenciaContaPDCAdapter getConsultarDescBancoAgenciaContaPDCAdapter() {
        return pdcConsultarDescBancoAgenciaConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDescBancoAgenciaConta.
     * 
     * @param pdcConsultarDescBancoAgenciaConta
     *            Adaptador ConsultarDescBancoAgenciaConta
     */
    public void setConsultarDescBancoAgenciaContaPDCAdapter(IConsultarDescBancoAgenciaContaPDCAdapter pdcConsultarDescBancoAgenciaConta) {
        this.pdcConsultarDescBancoAgenciaConta = pdcConsultarDescBancoAgenciaConta;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosSolicitacaoRecuperacao.
     * 
     * @return Adaptador ConsultarPagtosSolicitacaoRecuperacao
     */
    public IConsultarPagtosSolicitacaoRecuperacaoPDCAdapter getConsultarPagtosSolicitacaoRecuperacaoPDCAdapter() {
        return pdcConsultarPagtosSolicitacaoRecuperacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosSolicitacaoRecuperacao.
     * 
     * @param pdcConsultarPagtosSolicitacaoRecuperacao
     *            Adaptador ConsultarPagtosSolicitacaoRecuperacao
     */
    public void setConsultarPagtosSolicitacaoRecuperacaoPDCAdapter(IConsultarPagtosSolicitacaoRecuperacaoPDCAdapter pdcConsultarPagtosSolicitacaoRecuperacao) {
        this.pdcConsultarPagtosSolicitacaoRecuperacao = pdcConsultarPagtosSolicitacaoRecuperacao;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirTipoServModContrato.
     * 
     * @return Adaptador ExcluirTipoServModContrato
     */
    public IExcluirTipoServModContratoPDCAdapter getExcluirTipoServModContratoPDCAdapter() {
        return pdcExcluirTipoServModContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirTipoServModContrato.
     * 
     * @param pdcExcluirTipoServModContrato
     *            Adaptador ExcluirTipoServModContrato
     */
    public void setExcluirTipoServModContratoPDCAdapter(IExcluirTipoServModContratoPDCAdapter pdcExcluirTipoServModContrato) {
        this.pdcExcluirTipoServModContrato = pdcExcluirTipoServModContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarOperacaoServicoPagtoIntegrado.
     * 
     * @return Adaptador ListarOperacaoServicoPagtoIntegrado
     */
    public IListarOperacaoServicoPagtoIntegradoPDCAdapter getListarOperacaoServicoPagtoIntegradoPDCAdapter() {
        return pdcListarOperacaoServicoPagtoIntegrado;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarOperacaoServicoPagtoIntegrado.
     * 
     * @param pdcListarOperacaoServicoPagtoIntegrado
     *            Adaptador ListarOperacaoServicoPagtoIntegrado
     */
    public void setListarOperacaoServicoPagtoIntegradoPDCAdapter(IListarOperacaoServicoPagtoIntegradoPDCAdapter pdcListarOperacaoServicoPagtoIntegrado) {
        this.pdcListarOperacaoServicoPagtoIntegrado = pdcListarOperacaoServicoPagtoIntegrado;
    }
    /**
     * M�todo invocado para obter um adaptador ListarVinculoContaSalarioDestino.
     * 
     * @return Adaptador ListarVinculoContaSalarioDestino
     */
    public IListarVinculoContaSalarioDestinoPDCAdapter getListarVinculoContaSalarioDestinoPDCAdapter() {
        return pdcListarVinculoContaSalarioDestino;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarVinculoContaSalarioDestino.
     * 
     * @param pdcListarVinculoContaSalarioDestino
     *            Adaptador ListarVinculoContaSalarioDestino
     */
    public void setListarVinculoContaSalarioDestinoPDCAdapter(IListarVinculoContaSalarioDestinoPDCAdapter pdcListarVinculoContaSalarioDestino) {
        this.pdcListarVinculoContaSalarioDestino = pdcListarVinculoContaSalarioDestino;
    }
    /**
     * M�todo invocado para obter um adaptador ListarIncluirSolEmiComprovante.
     * 
     * @return Adaptador ListarIncluirSolEmiComprovante
     */
    public IListarIncluirSolEmiComprovantePDCAdapter getListarIncluirSolEmiComprovantePDCAdapter() {
        return pdcListarIncluirSolEmiComprovante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarIncluirSolEmiComprovante.
     * 
     * @param pdcListarIncluirSolEmiComprovante
     *            Adaptador ListarIncluirSolEmiComprovante
     */
    public void setListarIncluirSolEmiComprovantePDCAdapter(IListarIncluirSolEmiComprovantePDCAdapter pdcListarIncluirSolEmiComprovante) {
        this.pdcListarIncluirSolEmiComprovante = pdcListarIncluirSolEmiComprovante;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtoSolEmiCompFavorecido.
     * 
     * @return Adaptador ConsultarPagtoSolEmiCompFavorecido
     */
    public IConsultarPagtoSolEmiCompFavorecidoPDCAdapter getConsultarPagtoSolEmiCompFavorecidoPDCAdapter() {
        return pdcConsultarPagtoSolEmiCompFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtoSolEmiCompFavorecido.
     * 
     * @param pdcConsultarPagtoSolEmiCompFavorecido
     *            Adaptador ConsultarPagtoSolEmiCompFavorecido
     */
    public void setConsultarPagtoSolEmiCompFavorecidoPDCAdapter(IConsultarPagtoSolEmiCompFavorecidoPDCAdapter pdcConsultarPagtoSolEmiCompFavorecido) {
        this.pdcConsultarPagtoSolEmiCompFavorecido = pdcConsultarPagtoSolEmiCompFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMsgComprovanteSalrl.
     * 
     * @return Adaptador ListarMsgComprovanteSalrl
     */
    public IListarMsgComprovanteSalrlPDCAdapter getListarMsgComprovanteSalrlPDCAdapter() {
        return pdcListarMsgComprovanteSalrl;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMsgComprovanteSalrl.
     * 
     * @param pdcListarMsgComprovanteSalrl
     *            Adaptador ListarMsgComprovanteSalrl
     */
    public void setListarMsgComprovanteSalrlPDCAdapter(IListarMsgComprovanteSalrlPDCAdapter pdcListarMsgComprovanteSalrl) {
        this.pdcListarMsgComprovanteSalrl = pdcListarMsgComprovanteSalrl;
    }
    /**
     * M�todo invocado para obter um adaptador BloqDesbloqEmissaoAutAvisosComprv.
     * 
     * @return Adaptador BloqDesbloqEmissaoAutAvisosComprv
     */
    public IBloqDesbloqEmissaoAutAvisosComprvPDCAdapter getBloqDesbloqEmissaoAutAvisosComprvPDCAdapter() {
        return pdcBloqDesbloqEmissaoAutAvisosComprv;
    }

    /**
     * M�todo invocado para establecer um adaptador BloqDesbloqEmissaoAutAvisosComprv.
     * 
     * @param pdcBloqDesbloqEmissaoAutAvisosComprv
     *            Adaptador BloqDesbloqEmissaoAutAvisosComprv
     */
    public void setBloqDesbloqEmissaoAutAvisosComprvPDCAdapter(IBloqDesbloqEmissaoAutAvisosComprvPDCAdapter pdcBloqDesbloqEmissaoAutAvisosComprv) {
        this.pdcBloqDesbloqEmissaoAutAvisosComprv = pdcBloqDesbloqEmissaoAutAvisosComprv;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarParticipanteContrato.
     * 
     * @return Adaptador ConsultarParticipanteContrato
     */
    public IConsultarParticipanteContratoPDCAdapter getConsultarParticipanteContratoPDCAdapter() {
        return pdcConsultarParticipanteContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarParticipanteContrato.
     * 
     * @param pdcConsultarParticipanteContrato
     *            Adaptador ConsultarParticipanteContrato
     */
    public void setConsultarParticipanteContratoPDCAdapter(IConsultarParticipanteContratoPDCAdapter pdcConsultarParticipanteContrato) {
        this.pdcConsultarParticipanteContrato = pdcConsultarParticipanteContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarEmissaoAutAvisosComprv.
     * 
     * @return Adaptador ListarEmissaoAutAvisosComprv
     */
    public IListarEmissaoAutAvisosComprvPDCAdapter getListarEmissaoAutAvisosComprvPDCAdapter() {
        return pdcListarEmissaoAutAvisosComprv;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarEmissaoAutAvisosComprv.
     * 
     * @param pdcListarEmissaoAutAvisosComprv
     *            Adaptador ListarEmissaoAutAvisosComprv
     */
    public void setListarEmissaoAutAvisosComprvPDCAdapter(IListarEmissaoAutAvisosComprvPDCAdapter pdcListarEmissaoAutAvisosComprv) {
        this.pdcListarEmissaoAutAvisosComprv = pdcListarEmissaoAutAvisosComprv;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarMotivoSituacaoPagPendente.
     * 
     * @return Adaptador ConsultarMotivoSituacaoPagPendente
     */
    public IConsultarMotivoSituacaoPagPendentePDCAdapter getConsultarMotivoSituacaoPagPendentePDCAdapter() {
        return pdcConsultarMotivoSituacaoPagPendente;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarMotivoSituacaoPagPendente.
     * 
     * @param pdcConsultarMotivoSituacaoPagPendente
     *            Adaptador ConsultarMotivoSituacaoPagPendente
     */
    public void setConsultarMotivoSituacaoPagPendentePDCAdapter(IConsultarMotivoSituacaoPagPendentePDCAdapter pdcConsultarMotivoSituacaoPagPendente) {
        this.pdcConsultarMotivoSituacaoPagPendente = pdcConsultarMotivoSituacaoPagPendente;
    }
    /**
     * M�todo invocado para obter um adaptador ListarClasseRamoAtvdd.
     * 
     * @return Adaptador ListarClasseRamoAtvdd
     */
    public IListarClasseRamoAtvddPDCAdapter getListarClasseRamoAtvddPDCAdapter() {
        return pdcListarClasseRamoAtvdd;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarClasseRamoAtvdd.
     * 
     * @param pdcListarClasseRamoAtvdd
     *            Adaptador ListarClasseRamoAtvdd
     */
    public void setListarClasseRamoAtvddPDCAdapter(IListarClasseRamoAtvddPDCAdapter pdcListarClasseRamoAtvdd) {
        this.pdcListarClasseRamoAtvdd = pdcListarClasseRamoAtvdd;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSramoAtvddEconc.
     * 
     * @return Adaptador ListarSramoAtvddEconc
     */
    public IListarSramoAtvddEconcPDCAdapter getListarSramoAtvddEconcPDCAdapter() {
        return pdcListarSramoAtvddEconc;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSramoAtvddEconc.
     * 
     * @param pdcListarSramoAtvddEconc
     *            Adaptador ListarSramoAtvddEconc
     */
    public void setListarSramoAtvddEconcPDCAdapter(IListarSramoAtvddEconcPDCAdapter pdcListarSramoAtvddEconc) {
        this.pdcListarSramoAtvddEconc = pdcListarSramoAtvddEconc;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirVinculoEmprPagContSal.
     * 
     * @return Adaptador IncluirVinculoEmprPagContSal
     */
    public IIncluirVinculoEmprPagContSalPDCAdapter getIncluirVinculoEmprPagContSalPDCAdapter() {
        return pdcIncluirVinculoEmprPagContSal;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirVinculoEmprPagContSal.
     * 
     * @param pdcIncluirVinculoEmprPagContSal
     *            Adaptador IncluirVinculoEmprPagContSal
     */
    public void setIncluirVinculoEmprPagContSalPDCAdapter(IIncluirVinculoEmprPagContSalPDCAdapter pdcIncluirVinculoEmprPagContSal) {
        this.pdcIncluirVinculoEmprPagContSal = pdcIncluirVinculoEmprPagContSal;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetHistCtaSalarioEmpresa.
     * 
     * @return Adaptador ConsultarDetHistCtaSalarioEmpresa
     */
    public IConsultarDetHistCtaSalarioEmpresaPDCAdapter getConsultarDetHistCtaSalarioEmpresaPDCAdapter() {
        return pdcConsultarDetHistCtaSalarioEmpresa;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetHistCtaSalarioEmpresa.
     * 
     * @param pdcConsultarDetHistCtaSalarioEmpresa
     *            Adaptador ConsultarDetHistCtaSalarioEmpresa
     */
    public void setConsultarDetHistCtaSalarioEmpresaPDCAdapter(IConsultarDetHistCtaSalarioEmpresaPDCAdapter pdcConsultarDetHistCtaSalarioEmpresa) {
        this.pdcConsultarDetHistCtaSalarioEmpresa = pdcConsultarDetHistCtaSalarioEmpresa;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirTerceiroPartContrato.
     * 
     * @return Adaptador ExcluirTerceiroPartContrato
     */
    public IExcluirTerceiroPartContratoPDCAdapter getExcluirTerceiroPartContratoPDCAdapter() {
        return pdcExcluirTerceiroPartContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirTerceiroPartContrato.
     * 
     * @param pdcExcluirTerceiroPartContrato
     *            Adaptador ExcluirTerceiroPartContrato
     */
    public void setExcluirTerceiroPartContratoPDCAdapter(IExcluirTerceiroPartContratoPDCAdapter pdcExcluirTerceiroPartContrato) {
        this.pdcExcluirTerceiroPartContrato = pdcExcluirTerceiroPartContrato;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirTerceiroPartContrato.
     * 
     * @return Adaptador IncluirTerceiroPartContrato
     */
    public IIncluirTerceiroPartContratoPDCAdapter getIncluirTerceiroPartContratoPDCAdapter() {
        return pdcIncluirTerceiroPartContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirTerceiroPartContrato.
     * 
     * @param pdcIncluirTerceiroPartContrato
     *            Adaptador IncluirTerceiroPartContrato
     */
    public void setIncluirTerceiroPartContratoPDCAdapter(IIncluirTerceiroPartContratoPDCAdapter pdcIncluirTerceiroPartContrato) {
        this.pdcIncluirTerceiroPartContrato = pdcIncluirTerceiroPartContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoProcesso.
     * 
     * @return Adaptador ListarTipoProcesso
     */
    public IListarTipoProcessoPDCAdapter getListarTipoProcessoPDCAdapter() {
        return pdcListarTipoProcesso;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoProcesso.
     * 
     * @param pdcListarTipoProcesso
     *            Adaptador ListarTipoProcesso
     */
    public void setListarTipoProcessoPDCAdapter(IListarTipoProcessoPDCAdapter pdcListarTipoProcesso) {
        this.pdcListarTipoProcesso = pdcListarTipoProcesso;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirEmissaoAutAvisosComprv.
     * 
     * @return Adaptador IncluirEmissaoAutAvisosComprv
     */
    public IIncluirEmissaoAutAvisosComprvPDCAdapter getIncluirEmissaoAutAvisosComprvPDCAdapter() {
        return pdcIncluirEmissaoAutAvisosComprv;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirEmissaoAutAvisosComprv.
     * 
     * @param pdcIncluirEmissaoAutAvisosComprv
     *            Adaptador IncluirEmissaoAutAvisosComprv
     */
    public void setIncluirEmissaoAutAvisosComprvPDCAdapter(IIncluirEmissaoAutAvisosComprvPDCAdapter pdcIncluirEmissaoAutAvisosComprv) {
        this.pdcIncluirEmissaoAutAvisosComprv = pdcIncluirEmissaoAutAvisosComprv;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTercParticipanteContrato.
     * 
     * @return Adaptador ListarTercParticipanteContrato
     */
    public IListarTercParticipanteContratoPDCAdapter getListarTercParticipanteContratoPDCAdapter() {
        return pdcListarTercParticipanteContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTercParticipanteContrato.
     * 
     * @param pdcListarTercParticipanteContrato
     *            Adaptador ListarTercParticipanteContrato
     */
    public void setListarTercParticipanteContratoPDCAdapter(IListarTercParticipanteContratoPDCAdapter pdcListarTercParticipanteContrato) {
        this.pdcListarTercParticipanteContrato = pdcListarTercParticipanteContrato;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharEmissaoAutAvisosComprv.
     * 
     * @return Adaptador DetalharEmissaoAutAvisosComprv
     */
    public IDetalharEmissaoAutAvisosComprvPDCAdapter getDetalharEmissaoAutAvisosComprvPDCAdapter() {
        return pdcDetalharEmissaoAutAvisosComprv;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharEmissaoAutAvisosComprv.
     * 
     * @param pdcDetalharEmissaoAutAvisosComprv
     *            Adaptador DetalharEmissaoAutAvisosComprv
     */
    public void setDetalharEmissaoAutAvisosComprvPDCAdapter(IDetalharEmissaoAutAvisosComprvPDCAdapter pdcDetalharEmissaoAutAvisosComprv) {
        this.pdcDetalharEmissaoAutAvisosComprv = pdcDetalharEmissaoAutAvisosComprv;
    }
    /**
     * M�todo invocado para obter um adaptador ListarRelatorioContratos.
     * 
     * @return Adaptador ListarRelatorioContratos
     */
    public IListarRelatorioContratosPDCAdapter getListarRelatorioContratosPDCAdapter() {
        return pdcListarRelatorioContratos;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarRelatorioContratos.
     * 
     * @param pdcListarRelatorioContratos
     *            Adaptador ListarRelatorioContratos
     */
    public void setListarRelatorioContratosPDCAdapter(IListarRelatorioContratosPDCAdapter pdcListarRelatorioContratos) {
        this.pdcListarRelatorioContratos = pdcListarRelatorioContratos;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarLimDIaOpePgit.
     * 
     * @return Adaptador AlterarLimDIaOpePgit
     */
    public IAlterarLimDIaOpePgitPDCAdapter getAlterarLimDIaOpePgitPDCAdapter() {
        return pdcAlterarLimDIaOpePgit;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarLimDIaOpePgit.
     * 
     * @param pdcAlterarLimDIaOpePgit
     *            Adaptador AlterarLimDIaOpePgit
     */
    public void setAlterarLimDIaOpePgitPDCAdapter(IAlterarLimDIaOpePgitPDCAdapter pdcAlterarLimDIaOpePgit) {
        this.pdcAlterarLimDIaOpePgit = pdcAlterarLimDIaOpePgit;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLimDiarioUtilizado.
     * 
     * @return Adaptador ConsultarLimDiarioUtilizado
     */
    public IConsultarLimDiarioUtilizadoPDCAdapter getConsultarLimDiarioUtilizadoPDCAdapter() {
        return pdcConsultarLimDiarioUtilizado;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLimDiarioUtilizado.
     * 
     * @param pdcConsultarLimDiarioUtilizado
     *            Adaptador ConsultarLimDiarioUtilizado
     */
    public void setConsultarLimDiarioUtilizadoPDCAdapter(IConsultarLimDiarioUtilizadoPDCAdapter pdcConsultarLimDiarioUtilizado) {
        this.pdcConsultarLimDiarioUtilizado = pdcConsultarLimDiarioUtilizado;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicitacaoRelatorioFavorecidos.
     * 
     * @return Adaptador DetalharSolicitacaoRelatorioFavorecidos
     */
    public IDetalharSolicitacaoRelatorioFavorecidosPDCAdapter getDetalharSolicitacaoRelatorioFavorecidosPDCAdapter() {
        return pdcDetalharSolicitacaoRelatorioFavorecidos;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicitacaoRelatorioFavorecidos.
     * 
     * @param pdcDetalharSolicitacaoRelatorioFavorecidos
     *            Adaptador DetalharSolicitacaoRelatorioFavorecidos
     */
    public void setDetalharSolicitacaoRelatorioFavorecidosPDCAdapter(IDetalharSolicitacaoRelatorioFavorecidosPDCAdapter pdcDetalharSolicitacaoRelatorioFavorecidos) {
        this.pdcDetalharSolicitacaoRelatorioFavorecidos = pdcDetalharSolicitacaoRelatorioFavorecidos;
    }
    /**
     * M�todo invocado para obter um adaptador SolicitarRelatorioContratos.
     * 
     * @return Adaptador SolicitarRelatorioContratos
     */
    public ISolicitarRelatorioContratosPDCAdapter getSolicitarRelatorioContratosPDCAdapter() {
        return pdcSolicitarRelatorioContratos;
    }

    /**
     * M�todo invocado para establecer um adaptador SolicitarRelatorioContratos.
     * 
     * @param pdcSolicitarRelatorioContratos
     *            Adaptador SolicitarRelatorioContratos
     */
    public void setSolicitarRelatorioContratosPDCAdapter(ISolicitarRelatorioContratosPDCAdapter pdcSolicitarRelatorioContratos) {
        this.pdcSolicitarRelatorioContratos = pdcSolicitarRelatorioContratos;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirEmissaoAutAvisosComprv.
     * 
     * @return Adaptador ExcluirEmissaoAutAvisosComprv
     */
    public IExcluirEmissaoAutAvisosComprvPDCAdapter getExcluirEmissaoAutAvisosComprvPDCAdapter() {
        return pdcExcluirEmissaoAutAvisosComprv;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirEmissaoAutAvisosComprv.
     * 
     * @param pdcExcluirEmissaoAutAvisosComprv
     *            Adaptador ExcluirEmissaoAutAvisosComprv
     */
    public void setExcluirEmissaoAutAvisosComprvPDCAdapter(IExcluirEmissaoAutAvisosComprvPDCAdapter pdcExcluirEmissaoAutAvisosComprv) {
        this.pdcExcluirEmissaoAutAvisosComprv = pdcExcluirEmissaoAutAvisosComprv;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistEmissaoAutAvisosComprv.
     * 
     * @return Adaptador ListarHistEmissaoAutAvisosComprv
     */
    public IListarHistEmissaoAutAvisosComprvPDCAdapter getListarHistEmissaoAutAvisosComprvPDCAdapter() {
        return pdcListarHistEmissaoAutAvisosComprv;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistEmissaoAutAvisosComprv.
     * 
     * @param pdcListarHistEmissaoAutAvisosComprv
     *            Adaptador ListarHistEmissaoAutAvisosComprv
     */
    public void setListarHistEmissaoAutAvisosComprvPDCAdapter(IListarHistEmissaoAutAvisosComprvPDCAdapter pdcListarHistEmissaoAutAvisosComprv) {
        this.pdcListarHistEmissaoAutAvisosComprv = pdcListarHistEmissaoAutAvisosComprv;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistEmissaoAutAvisosComprv.
     * 
     * @return Adaptador DetalharHistEmissaoAutAvisosComprv
     */
    public IDetalharHistEmissaoAutAvisosComprvPDCAdapter getDetalharHistEmissaoAutAvisosComprvPDCAdapter() {
        return pdcDetalharHistEmissaoAutAvisosComprv;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistEmissaoAutAvisosComprv.
     * 
     * @param pdcDetalharHistEmissaoAutAvisosComprv
     *            Adaptador DetalharHistEmissaoAutAvisosComprv
     */
    public void setDetalharHistEmissaoAutAvisosComprvPDCAdapter(IDetalharHistEmissaoAutAvisosComprvPDCAdapter pdcDetalharHistEmissaoAutAvisosComprv) {
        this.pdcDetalharHistEmissaoAutAvisosComprv = pdcDetalharHistEmissaoAutAvisosComprv;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharRelatorioContratos.
     * 
     * @return Adaptador DetalharRelatorioContratos
     */
    public IDetalharRelatorioContratosPDCAdapter getDetalharRelatorioContratosPDCAdapter() {
        return pdcDetalharRelatorioContratos;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharRelatorioContratos.
     * 
     * @param pdcDetalharRelatorioContratos
     *            Adaptador DetalharRelatorioContratos
     */
    public void setDetalharRelatorioContratosPDCAdapter(IDetalharRelatorioContratosPDCAdapter pdcDetalharRelatorioContratos) {
        this.pdcDetalharRelatorioContratos = pdcDetalharRelatorioContratos;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteCredConta.
     * 
     * @return Adaptador ImprimirComprovanteCredConta
     */
    public IImprimirComprovanteCredContaPDCAdapter getImprimirComprovanteCredContaPDCAdapter() {
        return pdcImprimirComprovanteCredConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteCredConta.
     * 
     * @param pdcImprimirComprovanteCredConta
     *            Adaptador ImprimirComprovanteCredConta
     */
    public void setImprimirComprovanteCredContaPDCAdapter(IImprimirComprovanteCredContaPDCAdapter pdcImprimirComprovanteCredConta) {
        this.pdcImprimirComprovanteCredConta = pdcImprimirComprovanteCredConta;
    }
    /**
     * M�todo invocado para obter um adaptador Impdiaad.
     * 
     * @return Adaptador Impdiaad
     */
    public IImpdiaadPDCAdapter getImpdiaadPDCAdapter() {
        return pdcImpdiaad;
    }

    /**
     * M�todo invocado para establecer um adaptador Impdiaad.
     * 
     * @param pdcImpdiaad
     *            Adaptador Impdiaad
     */
    public void setImpdiaadPDCAdapter(IImpdiaadPDCAdapter pdcImpdiaad) {
        this.pdcImpdiaad = pdcImpdiaad;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirTipoPendenciaResponsaveis.
     * 
     * @return Adaptador ExcluirTipoPendenciaResponsaveis
     */
    public IExcluirTipoPendenciaResponsaveisPDCAdapter getExcluirTipoPendenciaResponsaveisPDCAdapter() {
        return pdcExcluirTipoPendenciaResponsaveis;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirTipoPendenciaResponsaveis.
     * 
     * @param pdcExcluirTipoPendenciaResponsaveis
     *            Adaptador ExcluirTipoPendenciaResponsaveis
     */
    public void setExcluirTipoPendenciaResponsaveisPDCAdapter(IExcluirTipoPendenciaResponsaveisPDCAdapter pdcExcluirTipoPendenciaResponsaveis) {
        this.pdcExcluirTipoPendenciaResponsaveis = pdcExcluirTipoPendenciaResponsaveis;
    }
    /**
     * M�todo invocado para obter um adaptador ListarIndiceEconomico.
     * 
     * @return Adaptador ListarIndiceEconomico
     */
    public IListarIndiceEconomicoPDCAdapter getListarIndiceEconomicoPDCAdapter() {
        return pdcListarIndiceEconomico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarIndiceEconomico.
     * 
     * @param pdcListarIndiceEconomico
     *            Adaptador ListarIndiceEconomico
     */
    public void setListarIndiceEconomicoPDCAdapter(IListarIndiceEconomicoPDCAdapter pdcListarIndiceEconomico) {
        this.pdcListarIndiceEconomico = pdcListarIndiceEconomico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarCondCobTarifa.
     * 
     * @return Adaptador ConsultarCondCobTarifa
     */
    public IConsultarCondCobTarifaPDCAdapter getConsultarCondCobTarifaPDCAdapter() {
        return pdcConsultarCondCobTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarCondCobTarifa.
     * 
     * @param pdcConsultarCondCobTarifa
     *            Adaptador ConsultarCondCobTarifa
     */
    public void setConsultarCondCobTarifaPDCAdapter(IConsultarCondCobTarifaPDCAdapter pdcConsultarCondCobTarifa) {
        this.pdcConsultarCondCobTarifa = pdcConsultarCondCobTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaHistoricoFavorecido.
     * 
     * @return Adaptador ConsultarListaHistoricoFavorecido
     */
    public IConsultarListaHistoricoFavorecidoPDCAdapter getConsultarListaHistoricoFavorecidoPDCAdapter() {
        return pdcConsultarListaHistoricoFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaHistoricoFavorecido.
     * 
     * @param pdcConsultarListaHistoricoFavorecido
     *            Adaptador ConsultarListaHistoricoFavorecido
     */
    public void setConsultarListaHistoricoFavorecidoPDCAdapter(IConsultarListaHistoricoFavorecidoPDCAdapter pdcConsultarListaHistoricoFavorecido) {
        this.pdcConsultarListaHistoricoFavorecido = pdcConsultarListaHistoricoFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaHistoricoContaFavorecido.
     * 
     * @return Adaptador ConsultarListaHistoricoContaFavorecido
     */
    public IConsultarListaHistoricoContaFavorecidoPDCAdapter getConsultarListaHistoricoContaFavorecidoPDCAdapter() {
        return pdcConsultarListaHistoricoContaFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaHistoricoContaFavorecido.
     * 
     * @param pdcConsultarListaHistoricoContaFavorecido
     *            Adaptador ConsultarListaHistoricoContaFavorecido
     */
    public void setConsultarListaHistoricoContaFavorecidoPDCAdapter(IConsultarListaHistoricoContaFavorecidoPDCAdapter pdcConsultarListaHistoricoContaFavorecido) {
        this.pdcConsultarListaHistoricoContaFavorecido = pdcConsultarListaHistoricoContaFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistVinculoContaSalarioDestino.
     * 
     * @return Adaptador ListarHistVinculoContaSalarioDestino
     */
    public IListarHistVinculoContaSalarioDestinoPDCAdapter getListarHistVinculoContaSalarioDestinoPDCAdapter() {
        return pdcListarHistVinculoContaSalarioDestino;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistVinculoContaSalarioDestino.
     * 
     * @param pdcListarHistVinculoContaSalarioDestino
     *            Adaptador ListarHistVinculoContaSalarioDestino
     */
    public void setListarHistVinculoContaSalarioDestinoPDCAdapter(IListarHistVinculoContaSalarioDestinoPDCAdapter pdcListarHistVinculoContaSalarioDestino) {
        this.pdcListarHistVinculoContaSalarioDestino = pdcListarHistVinculoContaSalarioDestino;
    }
    /**
     * M�todo invocado para obter um adaptador ListarVinculoHistCtaSalarioEmpresa.
     * 
     * @return Adaptador ListarVinculoHistCtaSalarioEmpresa
     */
    public IListarVinculoHistCtaSalarioEmpresaPDCAdapter getListarVinculoHistCtaSalarioEmpresaPDCAdapter() {
        return pdcListarVinculoHistCtaSalarioEmpresa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarVinculoHistCtaSalarioEmpresa.
     * 
     * @param pdcListarVinculoHistCtaSalarioEmpresa
     *            Adaptador ListarVinculoHistCtaSalarioEmpresa
     */
    public void setListarVinculoHistCtaSalarioEmpresaPDCAdapter(IListarVinculoHistCtaSalarioEmpresaPDCAdapter pdcListarVinculoHistCtaSalarioEmpresa) {
        this.pdcListarVinculoHistCtaSalarioEmpresa = pdcListarVinculoHistCtaSalarioEmpresa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDescricaoGrupoEconomico.
     * 
     * @return Adaptador ConsultarDescricaoGrupoEconomico
     */
    public IConsultarDescricaoGrupoEconomicoPDCAdapter getConsultarDescricaoGrupoEconomicoPDCAdapter() {
        return pdcConsultarDescricaoGrupoEconomico;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDescricaoGrupoEconomico.
     * 
     * @param pdcConsultarDescricaoGrupoEconomico
     *            Adaptador ConsultarDescricaoGrupoEconomico
     */
    public void setConsultarDescricaoGrupoEconomicoPDCAdapter(IConsultarDescricaoGrupoEconomicoPDCAdapter pdcConsultarDescricaoGrupoEconomico) {
        this.pdcConsultarDescricaoGrupoEconomico = pdcConsultarDescricaoGrupoEconomico;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirEnderecoParticipante.
     * 
     * @return Adaptador IncluirEnderecoParticipante
     */
    public IIncluirEnderecoParticipantePDCAdapter getIncluirEnderecoParticipantePDCAdapter() {
        return pdcIncluirEnderecoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirEnderecoParticipante.
     * 
     * @param pdcIncluirEnderecoParticipante
     *            Adaptador IncluirEnderecoParticipante
     */
    public void setIncluirEnderecoParticipantePDCAdapter(IIncluirEnderecoParticipantePDCAdapter pdcIncluirEnderecoParticipante) {
        this.pdcIncluirEnderecoParticipante = pdcIncluirEnderecoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirEnderecoParticipante.
     * 
     * @return Adaptador ExcluirEnderecoParticipante
     */
    public IExcluirEnderecoParticipantePDCAdapter getExcluirEnderecoParticipantePDCAdapter() {
        return pdcExcluirEnderecoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirEnderecoParticipante.
     * 
     * @param pdcExcluirEnderecoParticipante
     *            Adaptador ExcluirEnderecoParticipante
     */
    public void setExcluirEnderecoParticipantePDCAdapter(IExcluirEnderecoParticipantePDCAdapter pdcExcluirEnderecoParticipante) {
        this.pdcExcluirEnderecoParticipante = pdcExcluirEnderecoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarParticipanteContratoPgit.
     * 
     * @return Adaptador AlterarParticipanteContratoPgit
     */
    public IAlterarParticipanteContratoPgitPDCAdapter getAlterarParticipanteContratoPgitPDCAdapter() {
        return pdcAlterarParticipanteContratoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarParticipanteContratoPgit.
     * 
     * @param pdcAlterarParticipanteContratoPgit
     *            Adaptador AlterarParticipanteContratoPgit
     */
    public void setAlterarParticipanteContratoPgitPDCAdapter(IAlterarParticipanteContratoPgitPDCAdapter pdcAlterarParticipanteContratoPgit) {
        this.pdcAlterarParticipanteContratoPgit = pdcAlterarParticipanteContratoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolRecuperacaoPagtosBackup.
     * 
     * @return Adaptador ConsultarSolRecuperacaoPagtosBackup
     */
    public IConsultarSolRecuperacaoPagtosBackupPDCAdapter getConsultarSolRecuperacaoPagtosBackupPDCAdapter() {
        return pdcConsultarSolRecuperacaoPagtosBackup;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolRecuperacaoPagtosBackup.
     * 
     * @param pdcConsultarSolRecuperacaoPagtosBackup
     *            Adaptador ConsultarSolRecuperacaoPagtosBackup
     */
    public void setConsultarSolRecuperacaoPagtosBackupPDCAdapter(IConsultarSolRecuperacaoPagtosBackupPDCAdapter pdcConsultarSolRecuperacaoPagtosBackup) {
        this.pdcConsultarSolRecuperacaoPagtosBackup = pdcConsultarSolRecuperacaoPagtosBackup;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolRecPagtosVencNaoPago.
     * 
     * @return Adaptador ConsultarSolRecPagtosVencNaoPago
     */
    public IConsultarSolRecPagtosVencNaoPagoPDCAdapter getConsultarSolRecPagtosVencNaoPagoPDCAdapter() {
        return pdcConsultarSolRecPagtosVencNaoPago;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolRecPagtosVencNaoPago.
     * 
     * @param pdcConsultarSolRecPagtosVencNaoPago
     *            Adaptador ConsultarSolRecPagtosVencNaoPago
     */
    public void setConsultarSolRecPagtosVencNaoPagoPDCAdapter(IConsultarSolRecPagtosVencNaoPagoPDCAdapter pdcConsultarSolRecPagtosVencNaoPago) {
        this.pdcConsultarSolRecPagtosVencNaoPago = pdcConsultarSolRecPagtosVencNaoPago;
    }
    /**
     * M�todo invocado para obter um adaptador ListarEnderecoEmail.
     * 
     * @return Adaptador ListarEnderecoEmail
     */
    public IListarEnderecoEmailPDCAdapter getListarEnderecoEmailPDCAdapter() {
        return pdcListarEnderecoEmail;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarEnderecoEmail.
     * 
     * @param pdcListarEnderecoEmail
     *            Adaptador ListarEnderecoEmail
     */
    public void setListarEnderecoEmailPDCAdapter(IListarEnderecoEmailPDCAdapter pdcListarEnderecoEmail) {
        this.pdcListarEnderecoEmail = pdcListarEnderecoEmail;
    }
    /**
     * M�todo invocado para obter um adaptador ListarEnderecoParticipante.
     * 
     * @return Adaptador ListarEnderecoParticipante
     */
    public IListarEnderecoParticipantePDCAdapter getListarEnderecoParticipantePDCAdapter() {
        return pdcListarEnderecoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarEnderecoParticipante.
     * 
     * @param pdcListarEnderecoParticipante
     *            Adaptador ListarEnderecoParticipante
     */
    public void setListarEnderecoParticipantePDCAdapter(IListarEnderecoParticipantePDCAdapter pdcListarEnderecoParticipante) {
        this.pdcListarEnderecoParticipante = pdcListarEnderecoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarBloqueioRastreamento.
     * 
     * @return Adaptador ConsultarBloqueioRastreamento
     */
    public IConsultarBloqueioRastreamentoPDCAdapter getConsultarBloqueioRastreamentoPDCAdapter() {
        return pdcConsultarBloqueioRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarBloqueioRastreamento.
     * 
     * @param pdcConsultarBloqueioRastreamento
     *            Adaptador ConsultarBloqueioRastreamento
     */
    public void setConsultarBloqueioRastreamentoPDCAdapter(IConsultarBloqueioRastreamentoPDCAdapter pdcConsultarBloqueioRastreamento) {
        this.pdcConsultarBloqueioRastreamento = pdcConsultarBloqueioRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirBloqueioRastreamento.
     * 
     * @return Adaptador IncluirBloqueioRastreamento
     */
    public IIncluirBloqueioRastreamentoPDCAdapter getIncluirBloqueioRastreamentoPDCAdapter() {
        return pdcIncluirBloqueioRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirBloqueioRastreamento.
     * 
     * @param pdcIncluirBloqueioRastreamento
     *            Adaptador IncluirBloqueioRastreamento
     */
    public void setIncluirBloqueioRastreamentoPDCAdapter(IIncluirBloqueioRastreamentoPDCAdapter pdcIncluirBloqueioRastreamento) {
        this.pdcIncluirBloqueioRastreamento = pdcIncluirBloqueioRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarInclusaoBloqueioRastreamento.
     * 
     * @return Adaptador ValidarInclusaoBloqueioRastreamento
     */
    public IValidarInclusaoBloqueioRastreamentoPDCAdapter getValidarInclusaoBloqueioRastreamentoPDCAdapter() {
        return pdcValidarInclusaoBloqueioRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarInclusaoBloqueioRastreamento.
     * 
     * @param pdcValidarInclusaoBloqueioRastreamento
     *            Adaptador ValidarInclusaoBloqueioRastreamento
     */
    public void setValidarInclusaoBloqueioRastreamentoPDCAdapter(IValidarInclusaoBloqueioRastreamentoPDCAdapter pdcValidarInclusaoBloqueioRastreamento) {
        this.pdcValidarInclusaoBloqueioRastreamento = pdcValidarInclusaoBloqueioRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirBloqueioRastreamento.
     * 
     * @return Adaptador ExcluirBloqueioRastreamento
     */
    public IExcluirBloqueioRastreamentoPDCAdapter getExcluirBloqueioRastreamentoPDCAdapter() {
        return pdcExcluirBloqueioRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirBloqueioRastreamento.
     * 
     * @param pdcExcluirBloqueioRastreamento
     *            Adaptador ExcluirBloqueioRastreamento
     */
    public void setExcluirBloqueioRastreamentoPDCAdapter(IExcluirBloqueioRastreamentoPDCAdapter pdcExcluirBloqueioRastreamento) {
        this.pdcExcluirBloqueioRastreamento = pdcExcluirBloqueioRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharBloqueioRastreamento.
     * 
     * @return Adaptador DetalharBloqueioRastreamento
     */
    public IDetalharBloqueioRastreamentoPDCAdapter getDetalharBloqueioRastreamentoPDCAdapter() {
        return pdcDetalharBloqueioRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharBloqueioRastreamento.
     * 
     * @param pdcDetalharBloqueioRastreamento
     *            Adaptador DetalharBloqueioRastreamento
     */
    public void setDetalharBloqueioRastreamentoPDCAdapter(IDetalharBloqueioRastreamentoPDCAdapter pdcDetalharBloqueioRastreamento) {
        this.pdcDetalharBloqueioRastreamento = pdcDetalharBloqueioRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarTercPartContrato.
     * 
     * @return Adaptador ConsultarTercPartContrato
     */
    public IConsultarTercPartContratoPDCAdapter getConsultarTercPartContratoPDCAdapter() {
        return pdcConsultarTercPartContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarTercPartContrato.
     * 
     * @param pdcConsultarTercPartContrato
     *            Adaptador ConsultarTercPartContrato
     */
    public void setConsultarTercPartContratoPDCAdapter(IConsultarTercPartContratoPDCAdapter pdcConsultarTercPartContrato) {
        this.pdcConsultarTercPartContrato = pdcConsultarTercPartContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarTercParticiContrato.
     * 
     * @return Adaptador AlterarTercParticiContrato
     */
    public IAlterarTercParticiContratoPDCAdapter getAlterarTercParticiContratoPDCAdapter() {
        return pdcAlterarTercParticiContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarTercParticiContrato.
     * 
     * @param pdcAlterarTercParticiContrato
     *            Adaptador AlterarTercParticiContrato
     */
    public void setAlterarTercParticiContratoPDCAdapter(IAlterarTercParticiContratoPDCAdapter pdcAlterarTercParticiContrato) {
        this.pdcAlterarTercParticiContrato = pdcAlterarTercParticiContrato;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistoricoBloqueio.
     * 
     * @return Adaptador DetalharHistoricoBloqueio
     */
    public IDetalharHistoricoBloqueioPDCAdapter getDetalharHistoricoBloqueioPDCAdapter() {
        return pdcDetalharHistoricoBloqueio;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistoricoBloqueio.
     * 
     * @param pdcDetalharHistoricoBloqueio
     *            Adaptador DetalharHistoricoBloqueio
     */
    public void setDetalharHistoricoBloqueioPDCAdapter(IDetalharHistoricoBloqueioPDCAdapter pdcDetalharHistoricoBloqueio) {
        this.pdcDetalharHistoricoBloqueio = pdcDetalharHistoricoBloqueio;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistoricoBloqueio.
     * 
     * @return Adaptador ListarHistoricoBloqueio
     */
    public IListarHistoricoBloqueioPDCAdapter getListarHistoricoBloqueioPDCAdapter() {
        return pdcListarHistoricoBloqueio;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistoricoBloqueio.
     * 
     * @param pdcListarHistoricoBloqueio
     *            Adaptador ListarHistoricoBloqueio
     */
    public void setListarHistoricoBloqueioPDCAdapter(IListarHistoricoBloqueioPDCAdapter pdcListarHistoricoBloqueio) {
        this.pdcListarHistoricoBloqueio = pdcListarHistoricoBloqueio;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConManDadosBasicos.
     * 
     * @return Adaptador ListarConManDadosBasicos
     */
    public IListarConManDadosBasicosPDCAdapter getListarConManDadosBasicosPDCAdapter() {
        return pdcListarConManDadosBasicos;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConManDadosBasicos.
     * 
     * @param pdcListarConManDadosBasicos
     *            Adaptador ListarConManDadosBasicos
     */
    public void setListarConManDadosBasicosPDCAdapter(IListarConManDadosBasicosPDCAdapter pdcListarConManDadosBasicos) {
        this.pdcListarConManDadosBasicos = pdcListarConManDadosBasicos;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarManutencaoParticipante.
     * 
     * @return Adaptador ConsultarManutencaoParticipante
     */
    public IConsultarManutencaoParticipantePDCAdapter getConsultarManutencaoParticipantePDCAdapter() {
        return pdcConsultarManutencaoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarManutencaoParticipante.
     * 
     * @param pdcConsultarManutencaoParticipante
     *            Adaptador ConsultarManutencaoParticipante
     */
    public void setConsultarManutencaoParticipantePDCAdapter(IConsultarManutencaoParticipantePDCAdapter pdcConsultarManutencaoParticipante) {
        this.pdcConsultarManutencaoParticipante = pdcConsultarManutencaoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConManContaVinculada.
     * 
     * @return Adaptador ListarConManContaVinculada
     */
    public IListarConManContaVinculadaPDCAdapter getListarConManContaVinculadaPDCAdapter() {
        return pdcListarConManContaVinculada;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConManContaVinculada.
     * 
     * @param pdcListarConManContaVinculada
     *            Adaptador ListarConManContaVinculada
     */
    public void setListarConManContaVinculadaPDCAdapter(IListarConManContaVinculadaPDCAdapter pdcListarConManContaVinculada) {
        this.pdcListarConManContaVinculada = pdcListarConManContaVinculada;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConManTarifa.
     * 
     * @return Adaptador ListarConManTarifa
     */
    public IListarConManTarifaPDCAdapter getListarConManTarifaPDCAdapter() {
        return pdcListarConManTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConManTarifa.
     * 
     * @param pdcListarConManTarifa
     *            Adaptador ListarConManTarifa
     */
    public void setListarConManTarifaPDCAdapter(IListarConManTarifaPDCAdapter pdcListarConManTarifa) {
        this.pdcListarConManTarifa = pdcListarConManTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteDOC.
     * 
     * @return Adaptador ImprimirComprovanteDOC
     */
    public IImprimirComprovanteDOCPDCAdapter getImprimirComprovanteDOCPDCAdapter() {
        return pdcImprimirComprovanteDOC;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteDOC.
     * 
     * @param pdcImprimirComprovanteDOC
     *            Adaptador ImprimirComprovanteDOC
     */
    public void setImprimirComprovanteDOCPDCAdapter(IImprimirComprovanteDOCPDCAdapter pdcImprimirComprovanteDOC) {
        this.pdcImprimirComprovanteDOC = pdcImprimirComprovanteDOC;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteTED.
     * 
     * @return Adaptador ImprimirComprovanteTED
     */
    public IImprimirComprovanteTEDPDCAdapter getImprimirComprovanteTEDPDCAdapter() {
        return pdcImprimirComprovanteTED;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteTED.
     * 
     * @param pdcImprimirComprovanteTED
     *            Adaptador ImprimirComprovanteTED
     */
    public void setImprimirComprovanteTEDPDCAdapter(IImprimirComprovanteTEDPDCAdapter pdcImprimirComprovanteTED) {
        this.pdcImprimirComprovanteTED = pdcImprimirComprovanteTED;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteOP.
     * 
     * @return Adaptador ImprimirComprovanteOP
     */
    public IImprimirComprovanteOPPDCAdapter getImprimirComprovanteOPPDCAdapter() {
        return pdcImprimirComprovanteOP;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteOP.
     * 
     * @param pdcImprimirComprovanteOP
     *            Adaptador ImprimirComprovanteOP
     */
    public void setImprimirComprovanteOPPDCAdapter(IImprimirComprovanteOPPDCAdapter pdcImprimirComprovanteOP) {
        this.pdcImprimirComprovanteOP = pdcImprimirComprovanteOP;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteTituloBradesco.
     * 
     * @return Adaptador ImprimirComprovanteTituloBradesco
     */
    public IImprimirComprovanteTituloBradescoPDCAdapter getImprimirComprovanteTituloBradescoPDCAdapter() {
        return pdcImprimirComprovanteTituloBradesco;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteTituloBradesco.
     * 
     * @param pdcImprimirComprovanteTituloBradesco
     *            Adaptador ImprimirComprovanteTituloBradesco
     */
    public void setImprimirComprovanteTituloBradescoPDCAdapter(IImprimirComprovanteTituloBradescoPDCAdapter pdcImprimirComprovanteTituloBradesco) {
        this.pdcImprimirComprovanteTituloBradesco = pdcImprimirComprovanteTituloBradesco;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteTituloOutros.
     * 
     * @return Adaptador ImprimirComprovanteTituloOutros
     */
    public IImprimirComprovanteTituloOutrosPDCAdapter getImprimirComprovanteTituloOutrosPDCAdapter() {
        return pdcImprimirComprovanteTituloOutros;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteTituloOutros.
     * 
     * @param pdcImprimirComprovanteTituloOutros
     *            Adaptador ImprimirComprovanteTituloOutros
     */
    public void setImprimirComprovanteTituloOutrosPDCAdapter(IImprimirComprovanteTituloOutrosPDCAdapter pdcImprimirComprovanteTituloOutros) {
        this.pdcImprimirComprovanteTituloOutros = pdcImprimirComprovanteTituloOutros;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteDARF.
     * 
     * @return Adaptador ImprimirComprovanteDARF
     */
    public IImprimirComprovanteDARFPDCAdapter getImprimirComprovanteDARFPDCAdapter() {
        return pdcImprimirComprovanteDARF;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteDARF.
     * 
     * @param pdcImprimirComprovanteDARF
     *            Adaptador ImprimirComprovanteDARF
     */
    public void setImprimirComprovanteDARFPDCAdapter(IImprimirComprovanteDARFPDCAdapter pdcImprimirComprovanteDARF) {
        this.pdcImprimirComprovanteDARF = pdcImprimirComprovanteDARF;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteGARE.
     * 
     * @return Adaptador ImprimirComprovanteGARE
     */
    public IImprimirComprovanteGAREPDCAdapter getImprimirComprovanteGAREPDCAdapter() {
        return pdcImprimirComprovanteGARE;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteGARE.
     * 
     * @param pdcImprimirComprovanteGARE
     *            Adaptador ImprimirComprovanteGARE
     */
    public void setImprimirComprovanteGAREPDCAdapter(IImprimirComprovanteGAREPDCAdapter pdcImprimirComprovanteGARE) {
        this.pdcImprimirComprovanteGARE = pdcImprimirComprovanteGARE;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteGPS.
     * 
     * @return Adaptador ImprimirComprovanteGPS
     */
    public IImprimirComprovanteGPSPDCAdapter getImprimirComprovanteGPSPDCAdapter() {
        return pdcImprimirComprovanteGPS;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteGPS.
     * 
     * @param pdcImprimirComprovanteGPS
     *            Adaptador ImprimirComprovanteGPS
     */
    public void setImprimirComprovanteGPSPDCAdapter(IImprimirComprovanteGPSPDCAdapter pdcImprimirComprovanteGPS) {
        this.pdcImprimirComprovanteGPS = pdcImprimirComprovanteGPS;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDadosCliente.
     * 
     * @return Adaptador ConsultarDadosCliente
     */
    public IConsultarDadosClientePDCAdapter getConsultarDadosClientePDCAdapter() {
        return pdcConsultarDadosCliente;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDadosCliente.
     * 
     * @param pdcConsultarDadosCliente
     *            Adaptador ConsultarDadosCliente
     */
    public void setConsultarDadosClientePDCAdapter(IConsultarDadosClientePDCAdapter pdcConsultarDadosCliente) {
        this.pdcConsultarDadosCliente = pdcConsultarDadosCliente;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConManServico.
     * 
     * @return Adaptador ListarConManServico
     */
    public IListarConManServicoPDCAdapter getListarConManServicoPDCAdapter() {
        return pdcListarConManServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConManServico.
     * 
     * @param pdcListarConManServico
     *            Adaptador ListarConManServico
     */
    public void setListarConManServicoPDCAdapter(IListarConManServicoPDCAdapter pdcListarConManServico) {
        this.pdcListarConManServico = pdcListarConManServico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistoricoLayoutContrato.
     * 
     * @return Adaptador ListarHistoricoLayoutContrato
     */
    public IListarHistoricoLayoutContratoPDCAdapter getListarHistoricoLayoutContratoPDCAdapter() {
        return pdcListarHistoricoLayoutContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistoricoLayoutContrato.
     * 
     * @param pdcListarHistoricoLayoutContrato
     *            Adaptador ListarHistoricoLayoutContrato
     */
    public void setListarHistoricoLayoutContratoPDCAdapter(IListarHistoricoLayoutContratoPDCAdapter pdcListarHistoricoLayoutContrato) {
        this.pdcListarHistoricoLayoutContrato = pdcListarHistoricoLayoutContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAlteracaoAmbiente.
     * 
     * @return Adaptador ExcluirAlteracaoAmbiente
     */
    public IExcluirAlteracaoAmbientePDCAdapter getExcluirAlteracaoAmbientePDCAdapter() {
        return pdcExcluirAlteracaoAmbiente;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAlteracaoAmbiente.
     * 
     * @param pdcExcluirAlteracaoAmbiente
     *            Adaptador ExcluirAlteracaoAmbiente
     */
    public void setExcluirAlteracaoAmbientePDCAdapter(IExcluirAlteracaoAmbientePDCAdapter pdcExcluirAlteracaoAmbiente) {
        this.pdcExcluirAlteracaoAmbiente = pdcExcluirAlteracaoAmbiente;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaServLimDiaIndv.
     * 
     * @return Adaptador ConsultarListaServLimDiaIndv
     */
    public IConsultarListaServLimDiaIndvPDCAdapter getConsultarListaServLimDiaIndvPDCAdapter() {
        return pdcConsultarListaServLimDiaIndv;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaServLimDiaIndv.
     * 
     * @param pdcConsultarListaServLimDiaIndv
     *            Adaptador ConsultarListaServLimDiaIndv
     */
    public void setConsultarListaServLimDiaIndvPDCAdapter(IConsultarListaServLimDiaIndvPDCAdapter pdcConsultarListaServLimDiaIndv) {
        this.pdcConsultarListaServLimDiaIndv = pdcConsultarListaServLimDiaIndv;
    }
    /**
     * M�todo invocado para obter um adaptador ListarEmpresaTransmissaoArquivoVan.
     * 
     * @return Adaptador ListarEmpresaTransmissaoArquivoVan
     */
    public IListarEmpresaTransmissaoArquivoVanPDCAdapter getListarEmpresaTransmissaoArquivoVanPDCAdapter() {
        return pdcListarEmpresaTransmissaoArquivoVan;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarEmpresaTransmissaoArquivoVan.
     * 
     * @param pdcListarEmpresaTransmissaoArquivoVan
     *            Adaptador ListarEmpresaTransmissaoArquivoVan
     */
    public void setListarEmpresaTransmissaoArquivoVanPDCAdapter(IListarEmpresaTransmissaoArquivoVanPDCAdapter pdcListarEmpresaTransmissaoArquivoVan) {
        this.pdcListarEmpresaTransmissaoArquivoVan = pdcListarEmpresaTransmissaoArquivoVan;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirEquiparacaoContrato.
     * 
     * @return Adaptador IncluirEquiparacaoContrato
     */
    public IIncluirEquiparacaoContratoPDCAdapter getIncluirEquiparacaoContratoPDCAdapter() {
        return pdcIncluirEquiparacaoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirEquiparacaoContrato.
     * 
     * @param pdcIncluirEquiparacaoContrato
     *            Adaptador IncluirEquiparacaoContrato
     */
    public void setIncluirEquiparacaoContratoPDCAdapter(IIncluirEquiparacaoContratoPDCAdapter pdcIncluirEquiparacaoContrato) {
        this.pdcIncluirEquiparacaoContrato = pdcIncluirEquiparacaoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarMsgLayoutArqRetorno.
     * 
     * @return Adaptador ConsultarMsgLayoutArqRetorno
     */
    public IConsultarMsgLayoutArqRetornoPDCAdapter getConsultarMsgLayoutArqRetornoPDCAdapter() {
        return pdcConsultarMsgLayoutArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarMsgLayoutArqRetorno.
     * 
     * @param pdcConsultarMsgLayoutArqRetorno
     *            Adaptador ConsultarMsgLayoutArqRetorno
     */
    public void setConsultarMsgLayoutArqRetornoPDCAdapter(IConsultarMsgLayoutArqRetornoPDCAdapter pdcConsultarMsgLayoutArqRetorno) {
        this.pdcConsultarMsgLayoutArqRetorno = pdcConsultarMsgLayoutArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ListarPrioridadeTipoCompromisso.
     * 
     * @return Adaptador ListarPrioridadeTipoCompromisso
     */
    public IListarPrioridadeTipoCompromissoPDCAdapter getListarPrioridadeTipoCompromissoPDCAdapter() {
        return pdcListarPrioridadeTipoCompromisso;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarPrioridadeTipoCompromisso.
     * 
     * @param pdcListarPrioridadeTipoCompromisso
     *            Adaptador ListarPrioridadeTipoCompromisso
     */
    public void setListarPrioridadeTipoCompromissoPDCAdapter(IListarPrioridadeTipoCompromissoPDCAdapter pdcListarPrioridadeTipoCompromisso) {
        this.pdcListarPrioridadeTipoCompromisso = pdcListarPrioridadeTipoCompromisso;
    }
    /**
     * M�todo invocado para obter um adaptador ListarLayoutArqServico.
     * 
     * @return Adaptador ListarLayoutArqServico
     */
    public IListarLayoutArqServicoPDCAdapter getListarLayoutArqServicoPDCAdapter() {
        return pdcListarLayoutArqServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarLayoutArqServico.
     * 
     * @param pdcListarLayoutArqServico
     *            Adaptador ListarLayoutArqServico
     */
    public void setListarLayoutArqServicoPDCAdapter(IListarLayoutArqServicoPDCAdapter pdcListarLayoutArqServico) {
        this.pdcListarLayoutArqServico = pdcListarLayoutArqServico;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarServicoContratado.
     * 
     * @return Adaptador ValidarServicoContratado
     */
    public IValidarServicoContratadoPDCAdapter getValidarServicoContratadoPDCAdapter() {
        return pdcValidarServicoContratado;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarServicoContratado.
     * 
     * @param pdcValidarServicoContratado
     *            Adaptador ValidarServicoContratado
     */
    public void setValidarServicoContratadoPDCAdapter(IValidarServicoContratadoPDCAdapter pdcValidarServicoContratado) {
        this.pdcValidarServicoContratado = pdcValidarServicoContratado;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConManLayout.
     * 
     * @return Adaptador ListarConManLayout
     */
    public IListarConManLayoutPDCAdapter getListarConManLayoutPDCAdapter() {
        return pdcListarConManLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConManLayout.
     * 
     * @param pdcListarConManLayout
     *            Adaptador ListarConManLayout
     */
    public void setListarConManLayoutPDCAdapter(IListarConManLayoutPDCAdapter pdcListarConManLayout) {
        this.pdcListarConManLayout = pdcListarConManLayout;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarManutencaoContrato.
     * 
     * @return Adaptador ConsultarManutencaoContrato
     */
    public IConsultarManutencaoContratoPDCAdapter getConsultarManutencaoContratoPDCAdapter() {
        return pdcConsultarManutencaoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarManutencaoContrato.
     * 
     * @param pdcConsultarManutencaoContrato
     *            Adaptador ConsultarManutencaoContrato
     */
    public void setConsultarManutencaoContratoPDCAdapter(IConsultarManutencaoContratoPDCAdapter pdcConsultarManutencaoContrato) {
        this.pdcConsultarManutencaoContrato = pdcConsultarManutencaoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicosEmissao.
     * 
     * @return Adaptador ListarServicosEmissao
     */
    public IListarServicosEmissaoPDCAdapter getListarServicosEmissaoPDCAdapter() {
        return pdcListarServicosEmissao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicosEmissao.
     * 
     * @param pdcListarServicosEmissao
     *            Adaptador ListarServicosEmissao
     */
    public void setListarServicosEmissaoPDCAdapter(IListarServicosEmissaoPDCAdapter pdcListarServicosEmissao) {
        this.pdcListarServicosEmissao = pdcListarServicosEmissao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSolicitCriacaoContaTipo.
     * 
     * @return Adaptador ListarSolicitCriacaoContaTipo
     */
    public IListarSolicitCriacaoContaTipoPDCAdapter getListarSolicitCriacaoContaTipoPDCAdapter() {
        return pdcListarSolicitCriacaoContaTipo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSolicitCriacaoContaTipo.
     * 
     * @param pdcListarSolicitCriacaoContaTipo
     *            Adaptador ListarSolicitCriacaoContaTipo
     */
    public void setListarSolicitCriacaoContaTipoPDCAdapter(IListarSolicitCriacaoContaTipoPDCAdapter pdcListarSolicitCriacaoContaTipo) {
        this.pdcListarSolicitCriacaoContaTipo = pdcListarSolicitCriacaoContaTipo;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicitCriacaoContaTipo.
     * 
     * @return Adaptador IncluirSolicitCriacaoContaTipo
     */
    public IIncluirSolicitCriacaoContaTipoPDCAdapter getIncluirSolicitCriacaoContaTipoPDCAdapter() {
        return pdcIncluirSolicitCriacaoContaTipo;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicitCriacaoContaTipo.
     * 
     * @param pdcIncluirSolicitCriacaoContaTipo
     *            Adaptador IncluirSolicitCriacaoContaTipo
     */
    public void setIncluirSolicitCriacaoContaTipoPDCAdapter(IIncluirSolicitCriacaoContaTipoPDCAdapter pdcIncluirSolicitCriacaoContaTipo) {
        this.pdcIncluirSolicitCriacaoContaTipo = pdcIncluirSolicitCriacaoContaTipo;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicitCriacaoContaTipo.
     * 
     * @return Adaptador ExcluirSolicitCriacaoContaTipo
     */
    public IExcluirSolicitCriacaoContaTipoPDCAdapter getExcluirSolicitCriacaoContaTipoPDCAdapter() {
        return pdcExcluirSolicitCriacaoContaTipo;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicitCriacaoContaTipo.
     * 
     * @param pdcExcluirSolicitCriacaoContaTipo
     *            Adaptador ExcluirSolicitCriacaoContaTipo
     */
    public void setExcluirSolicitCriacaoContaTipoPDCAdapter(IExcluirSolicitCriacaoContaTipoPDCAdapter pdcExcluirSolicitCriacaoContaTipo) {
        this.pdcExcluirSolicitCriacaoContaTipo = pdcExcluirSolicitCriacaoContaTipo;
    }
    /**
     * M�todo invocado para obter um adaptador ListarModalidadeListaDebito.
     * 
     * @return Adaptador ListarModalidadeListaDebito
     */
    public IListarModalidadeListaDebitoPDCAdapter getListarModalidadeListaDebitoPDCAdapter() {
        return pdcListarModalidadeListaDebito;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarModalidadeListaDebito.
     * 
     * @param pdcListarModalidadeListaDebito
     *            Adaptador ListarModalidadeListaDebito
     */
    public void setListarModalidadeListaDebitoPDCAdapter(IListarModalidadeListaDebitoPDCAdapter pdcListarModalidadeListaDebito) {
        this.pdcListarModalidadeListaDebito = pdcListarModalidadeListaDebito;
    }
    /**
     * M�todo invocado para obter um adaptador EfetuarValidacaoTarifaOperTipoServico.
     * 
     * @return Adaptador EfetuarValidacaoTarifaOperTipoServico
     */
    public IEfetuarValidacaoTarifaOperTipoServicoPDCAdapter getEfetuarValidacaoTarifaOperTipoServicoPDCAdapter() {
        return pdcEfetuarValidacaoTarifaOperTipoServico;
    }

    /**
     * M�todo invocado para establecer um adaptador EfetuarValidacaoTarifaOperTipoServico.
     * 
     * @param pdcEfetuarValidacaoTarifaOperTipoServico
     *            Adaptador EfetuarValidacaoTarifaOperTipoServico
     */
    public void setEfetuarValidacaoTarifaOperTipoServicoPDCAdapter(IEfetuarValidacaoTarifaOperTipoServicoPDCAdapter pdcEfetuarValidacaoTarifaOperTipoServico) {
        this.pdcEfetuarValidacaoTarifaOperTipoServico = pdcEfetuarValidacaoTarifaOperTipoServico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaDebitoAutDes.
     * 
     * @return Adaptador ConsultarListaDebitoAutDes
     */
    public IConsultarListaDebitoAutDesPDCAdapter getConsultarListaDebitoAutDesPDCAdapter() {
        return pdcConsultarListaDebitoAutDes;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaDebitoAutDes.
     * 
     * @param pdcConsultarListaDebitoAutDes
     *            Adaptador ConsultarListaDebitoAutDes
     */
    public void setConsultarListaDebitoAutDesPDCAdapter(IConsultarListaDebitoAutDesPDCAdapter pdcConsultarListaDebitoAutDes) {
        this.pdcConsultarListaDebitoAutDes = pdcConsultarListaDebitoAutDes;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaDebito.
     * 
     * @return Adaptador ConsultarListaDebito
     */
    public IConsultarListaDebitoPDCAdapter getConsultarListaDebitoPDCAdapter() {
        return pdcConsultarListaDebito;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaDebito.
     * 
     * @param pdcConsultarListaDebito
     *            Adaptador ConsultarListaDebito
     */
    public void setConsultarListaDebitoPDCAdapter(IConsultarListaDebitoPDCAdapter pdcConsultarListaDebito) {
        this.pdcConsultarListaDebito = pdcConsultarListaDebito;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarLimiteIntraPgit.
     * 
     * @return Adaptador AlterarLimiteIntraPgit
     */
    public IAlterarLimiteIntraPgitPDCAdapter getAlterarLimiteIntraPgitPDCAdapter() {
        return pdcAlterarLimiteIntraPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarLimiteIntraPgit.
     * 
     * @param pdcAlterarLimiteIntraPgit
     *            Adaptador AlterarLimiteIntraPgit
     */
    public void setAlterarLimiteIntraPgitPDCAdapter(IAlterarLimiteIntraPgitPDCAdapter pdcAlterarLimiteIntraPgit) {
        this.pdcAlterarLimiteIntraPgit = pdcAlterarLimiteIntraPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirRecisaoContratoPgitMulticanal.
     * 
     * @return Adaptador ImprimirRecisaoContratoPgitMulticanal
     */
    public IImprimirRecisaoContratoPgitMulticanalPDCAdapter getImprimirRecisaoContratoPgitMulticanalPDCAdapter() {
        return pdcImprimirRecisaoContratoPgitMulticanal;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirRecisaoContratoPgitMulticanal.
     * 
     * @param pdcImprimirRecisaoContratoPgitMulticanal
     *            Adaptador ImprimirRecisaoContratoPgitMulticanal
     */
    public void setImprimirRecisaoContratoPgitMulticanalPDCAdapter(IImprimirRecisaoContratoPgitMulticanalPDCAdapter pdcImprimirRecisaoContratoPgitMulticanal) {
        this.pdcImprimirRecisaoContratoPgitMulticanal = pdcImprimirRecisaoContratoPgitMulticanal;
    }
    /**
     * M�todo invocado para obter um adaptador SubstituirVincEnderecoParticipante.
     * 
     * @return Adaptador SubstituirVincEnderecoParticipante
     */
    public ISubstituirVincEnderecoParticipantePDCAdapter getSubstituirVincEnderecoParticipantePDCAdapter() {
        return pdcSubstituirVincEnderecoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador SubstituirVincEnderecoParticipante.
     * 
     * @param pdcSubstituirVincEnderecoParticipante
     *            Adaptador SubstituirVincEnderecoParticipante
     */
    public void setSubstituirVincEnderecoParticipantePDCAdapter(ISubstituirVincEnderecoParticipantePDCAdapter pdcSubstituirVincEnderecoParticipante) {
        this.pdcSubstituirVincEnderecoParticipante = pdcSubstituirVincEnderecoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistoricoEnderecoParticipante.
     * 
     * @return Adaptador ListarHistoricoEnderecoParticipante
     */
    public IListarHistoricoEnderecoParticipantePDCAdapter getListarHistoricoEnderecoParticipantePDCAdapter() {
        return pdcListarHistoricoEnderecoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistoricoEnderecoParticipante.
     * 
     * @param pdcListarHistoricoEnderecoParticipante
     *            Adaptador ListarHistoricoEnderecoParticipante
     */
    public void setListarHistoricoEnderecoParticipantePDCAdapter(IListarHistoricoEnderecoParticipantePDCAdapter pdcListarHistoricoEnderecoParticipante) {
        this.pdcListarHistoricoEnderecoParticipante = pdcListarHistoricoEnderecoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParticipanteAgregado.
     * 
     * @return Adaptador ListarParticipanteAgregado
     */
    public IListarParticipanteAgregadoPDCAdapter getListarParticipanteAgregadoPDCAdapter() {
        return pdcListarParticipanteAgregado;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParticipanteAgregado.
     * 
     * @param pdcListarParticipanteAgregado
     *            Adaptador ListarParticipanteAgregado
     */
    public void setListarParticipanteAgregadoPDCAdapter(IListarParticipanteAgregadoPDCAdapter pdcListarParticipanteAgregado) {
        this.pdcListarParticipanteAgregado = pdcListarParticipanteAgregado;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAgregado.
     * 
     * @return Adaptador ListarAgregado
     */
    public IListarAgregadoPDCAdapter getListarAgregadoPDCAdapter() {
        return pdcListarAgregado;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAgregado.
     * 
     * @param pdcListarAgregado
     *            Adaptador ListarAgregado
     */
    public void setListarAgregadoPDCAdapter(IListarAgregadoPDCAdapter pdcListarAgregado) {
        this.pdcListarAgregado = pdcListarAgregado;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirVincConvnCtaSalario.
     * 
     * @return Adaptador IncluirVincConvnCtaSalario
     */
    public IIncluirVincConvnCtaSalarioPDCAdapter getIncluirVincConvnCtaSalarioPDCAdapter() {
        return pdcIncluirVincConvnCtaSalario;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirVincConvnCtaSalario.
     * 
     * @param pdcIncluirVincConvnCtaSalario
     *            Adaptador IncluirVincConvnCtaSalario
     */
    public void setIncluirVincConvnCtaSalarioPDCAdapter(IIncluirVincConvnCtaSalarioPDCAdapter pdcIncluirVincConvnCtaSalario) {
        this.pdcIncluirVincConvnCtaSalario = pdcIncluirVincConvnCtaSalario;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarVincConvnCtaSalario.
     * 
     * @return Adaptador AlterarVincConvnCtaSalario
     */
    public IAlterarVincConvnCtaSalarioPDCAdapter getAlterarVincConvnCtaSalarioPDCAdapter() {
        return pdcAlterarVincConvnCtaSalario;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarVincConvnCtaSalario.
     * 
     * @param pdcAlterarVincConvnCtaSalario
     *            Adaptador AlterarVincConvnCtaSalario
     */
    public void setAlterarVincConvnCtaSalarioPDCAdapter(IAlterarVincConvnCtaSalarioPDCAdapter pdcAlterarVincConvnCtaSalario) {
        this.pdcAlterarVincConvnCtaSalario = pdcAlterarVincConvnCtaSalario;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirVincConvnCtaSalario.
     * 
     * @return Adaptador ExcluirVincConvnCtaSalario
     */
    public IExcluirVincConvnCtaSalarioPDCAdapter getExcluirVincConvnCtaSalarioPDCAdapter() {
        return pdcExcluirVincConvnCtaSalario;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirVincConvnCtaSalario.
     * 
     * @param pdcExcluirVincConvnCtaSalario
     *            Adaptador ExcluirVincConvnCtaSalario
     */
    public void setExcluirVincConvnCtaSalarioPDCAdapter(IExcluirVincConvnCtaSalarioPDCAdapter pdcExcluirVincConvnCtaSalario) {
        this.pdcExcluirVincConvnCtaSalario = pdcExcluirVincConvnCtaSalario;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosConsPendAutorizacao.
     * 
     * @return Adaptador ConsultarPagtosConsPendAutorizacao
     */
    public IConsultarPagtosConsPendAutorizacaoPDCAdapter getConsultarPagtosConsPendAutorizacaoPDCAdapter() {
        return pdcConsultarPagtosConsPendAutorizacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosConsPendAutorizacao.
     * 
     * @param pdcConsultarPagtosConsPendAutorizacao
     *            Adaptador ConsultarPagtosConsPendAutorizacao
     */
    public void setConsultarPagtosConsPendAutorizacaoPDCAdapter(IConsultarPagtosConsPendAutorizacaoPDCAdapter pdcConsultarPagtosConsPendAutorizacao) {
        this.pdcConsultarPagtosConsPendAutorizacao = pdcConsultarPagtosConsPendAutorizacao;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosConsAutorizados.
     * 
     * @return Adaptador ConsultarPagtosConsAutorizados
     */
    public IConsultarPagtosConsAutorizadosPDCAdapter getConsultarPagtosConsAutorizadosPDCAdapter() {
        return pdcConsultarPagtosConsAutorizados;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosConsAutorizados.
     * 
     * @param pdcConsultarPagtosConsAutorizados
     *            Adaptador ConsultarPagtosConsAutorizados
     */
    public void setConsultarPagtosConsAutorizadosPDCAdapter(IConsultarPagtosConsAutorizadosPDCAdapter pdcConsultarPagtosConsAutorizados) {
        this.pdcConsultarPagtosConsAutorizados = pdcConsultarPagtosConsAutorizados;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLibPagtosConsSemConsultaSaldo.
     * 
     * @return Adaptador ConsultarLibPagtosConsSemConsultaSaldo
     */
    public IConsultarLibPagtosConsSemConsultaSaldoPDCAdapter getConsultarLibPagtosConsSemConsultaSaldoPDCAdapter() {
        return pdcConsultarLibPagtosConsSemConsultaSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLibPagtosConsSemConsultaSaldo.
     * 
     * @param pdcConsultarLibPagtosConsSemConsultaSaldo
     *            Adaptador ConsultarLibPagtosConsSemConsultaSaldo
     */
    public void setConsultarLibPagtosConsSemConsultaSaldoPDCAdapter(IConsultarLibPagtosConsSemConsultaSaldoPDCAdapter pdcConsultarLibPagtosConsSemConsultaSaldo) {
        this.pdcConsultarLibPagtosConsSemConsultaSaldo = pdcConsultarLibPagtosConsSemConsultaSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarCanLibPagtosConsSemConsSaldo.
     * 
     * @return Adaptador ConsultarCanLibPagtosConsSemConsSaldo
     */
    public IConsultarCanLibPagtosConsSemConsSaldoPDCAdapter getConsultarCanLibPagtosConsSemConsSaldoPDCAdapter() {
        return pdcConsultarCanLibPagtosConsSemConsSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarCanLibPagtosConsSemConsSaldo.
     * 
     * @param pdcConsultarCanLibPagtosConsSemConsSaldo
     *            Adaptador ConsultarCanLibPagtosConsSemConsSaldo
     */
    public void setConsultarCanLibPagtosConsSemConsSaldoPDCAdapter(IConsultarCanLibPagtosConsSemConsSaldoPDCAdapter pdcConsultarCanLibPagtosConsSemConsSaldo) {
        this.pdcConsultarCanLibPagtosConsSemConsSaldo = pdcConsultarCanLibPagtosConsSemConsSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosConsParaCancelamento.
     * 
     * @return Adaptador ConsultarPagtosConsParaCancelamento
     */
    public IConsultarPagtosConsParaCancelamentoPDCAdapter getConsultarPagtosConsParaCancelamentoPDCAdapter() {
        return pdcConsultarPagtosConsParaCancelamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosConsParaCancelamento.
     * 
     * @param pdcConsultarPagtosConsParaCancelamento
     *            Adaptador ConsultarPagtosConsParaCancelamento
     */
    public void setConsultarPagtosConsParaCancelamentoPDCAdapter(IConsultarPagtosConsParaCancelamentoPDCAdapter pdcConsultarPagtosConsParaCancelamento) {
        this.pdcConsultarPagtosConsParaCancelamento = pdcConsultarPagtosConsParaCancelamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharLibPagtosConsSemConsultaSaldo.
     * 
     * @return Adaptador DetalharLibPagtosConsSemConsultaSaldo
     */
    public IDetalharLibPagtosConsSemConsultaSaldoPDCAdapter getDetalharLibPagtosConsSemConsultaSaldoPDCAdapter() {
        return pdcDetalharLibPagtosConsSemConsultaSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharLibPagtosConsSemConsultaSaldo.
     * 
     * @param pdcDetalharLibPagtosConsSemConsultaSaldo
     *            Adaptador DetalharLibPagtosConsSemConsultaSaldo
     */
    public void setDetalharLibPagtosConsSemConsultaSaldoPDCAdapter(IDetalharLibPagtosConsSemConsultaSaldoPDCAdapter pdcDetalharLibPagtosConsSemConsultaSaldo) {
        this.pdcDetalharLibPagtosConsSemConsultaSaldo = pdcDetalharLibPagtosConsSemConsultaSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharCanLibPagtosConsSemConsSaldo.
     * 
     * @return Adaptador DetalharCanLibPagtosConsSemConsSaldo
     */
    public IDetalharCanLibPagtosConsSemConsSaldoPDCAdapter getDetalharCanLibPagtosConsSemConsSaldoPDCAdapter() {
        return pdcDetalharCanLibPagtosConsSemConsSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharCanLibPagtosConsSemConsSaldo.
     * 
     * @param pdcDetalharCanLibPagtosConsSemConsSaldo
     *            Adaptador DetalharCanLibPagtosConsSemConsSaldo
     */
    public void setDetalharCanLibPagtosConsSemConsSaldoPDCAdapter(IDetalharCanLibPagtosConsSemConsSaldoPDCAdapter pdcDetalharCanLibPagtosConsSemConsSaldo) {
        this.pdcDetalharCanLibPagtosConsSemConsSaldo = pdcDetalharCanLibPagtosConsSemConsSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtosConsPendAutorizacao.
     * 
     * @return Adaptador DetalharPagtosConsPendAutorizacao
     */
    public IDetalharPagtosConsPendAutorizacaoPDCAdapter getDetalharPagtosConsPendAutorizacaoPDCAdapter() {
        return pdcDetalharPagtosConsPendAutorizacao;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtosConsPendAutorizacao.
     * 
     * @param pdcDetalharPagtosConsPendAutorizacao
     *            Adaptador DetalharPagtosConsPendAutorizacao
     */
    public void setDetalharPagtosConsPendAutorizacaoPDCAdapter(IDetalharPagtosConsPendAutorizacaoPDCAdapter pdcDetalharPagtosConsPendAutorizacao) {
        this.pdcDetalharPagtosConsPendAutorizacao = pdcDetalharPagtosConsPendAutorizacao;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharAntPostergarPagtos.
     * 
     * @return Adaptador DetalharAntPostergarPagtos
     */
    public IDetalharAntPostergarPagtosPDCAdapter getDetalharAntPostergarPagtosPDCAdapter() {
        return pdcDetalharAntPostergarPagtos;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharAntPostergarPagtos.
     * 
     * @param pdcDetalharAntPostergarPagtos
     *            Adaptador DetalharAntPostergarPagtos
     */
    public void setDetalharAntPostergarPagtosPDCAdapter(IDetalharAntPostergarPagtosPDCAdapter pdcDetalharAntPostergarPagtos) {
        this.pdcDetalharAntPostergarPagtos = pdcDetalharAntPostergarPagtos;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtosConsParaCancelamento.
     * 
     * @return Adaptador DetalharPagtosConsParaCancelamento
     */
    public IDetalharPagtosConsParaCancelamentoPDCAdapter getDetalharPagtosConsParaCancelamentoPDCAdapter() {
        return pdcDetalharPagtosConsParaCancelamento;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtosConsParaCancelamento.
     * 
     * @param pdcDetalharPagtosConsParaCancelamento
     *            Adaptador DetalharPagtosConsParaCancelamento
     */
    public void setDetalharPagtosConsParaCancelamentoPDCAdapter(IDetalharPagtosConsParaCancelamentoPDCAdapter pdcDetalharPagtosConsParaCancelamento) {
        this.pdcDetalharPagtosConsParaCancelamento = pdcDetalharPagtosConsParaCancelamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtosConsAutorizados.
     * 
     * @return Adaptador DetalharPagtosConsAutorizados
     */
    public IDetalharPagtosConsAutorizadosPDCAdapter getDetalharPagtosConsAutorizadosPDCAdapter() {
        return pdcDetalharPagtosConsAutorizados;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtosConsAutorizados.
     * 
     * @param pdcDetalharPagtosConsAutorizados
     *            Adaptador DetalharPagtosConsAutorizados
     */
    public void setDetalharPagtosConsAutorizadosPDCAdapter(IDetalharPagtosConsAutorizadosPDCAdapter pdcDetalharPagtosConsAutorizados) {
        this.pdcDetalharPagtosConsAutorizados = pdcDetalharPagtosConsAutorizados;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoDebitoVeiculos.
     * 
     * @return Adaptador DetalharPagtoDebitoVeiculos
     */
    public IDetalharPagtoDebitoVeiculosPDCAdapter getDetalharPagtoDebitoVeiculosPDCAdapter() {
        return pdcDetalharPagtoDebitoVeiculos;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoDebitoVeiculos.
     * 
     * @param pdcDetalharPagtoDebitoVeiculos
     *            Adaptador DetalharPagtoDebitoVeiculos
     */
    public void setDetalharPagtoDebitoVeiculosPDCAdapter(IDetalharPagtoDebitoVeiculosPDCAdapter pdcDetalharPagtoDebitoVeiculos) {
        this.pdcDetalharPagtoDebitoVeiculos = pdcDetalharPagtoDebitoVeiculos;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoDebitoVeiculos.
     * 
     * @return Adaptador DetalharManPagtoDebitoVeiculos
     */
    public IDetalharManPagtoDebitoVeiculosPDCAdapter getDetalharManPagtoDebitoVeiculosPDCAdapter() {
        return pdcDetalharManPagtoDebitoVeiculos;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoDebitoVeiculos.
     * 
     * @param pdcDetalharManPagtoDebitoVeiculos
     *            Adaptador DetalharManPagtoDebitoVeiculos
     */
    public void setDetalharManPagtoDebitoVeiculosPDCAdapter(IDetalharManPagtoDebitoVeiculosPDCAdapter pdcDetalharManPagtoDebitoVeiculos) {
        this.pdcDetalharManPagtoDebitoVeiculos = pdcDetalharManPagtoDebitoVeiculos;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicitCriacaoContaTipo.
     * 
     * @return Adaptador DetalharSolicitCriacaoContaTipo
     */
    public IDetalharSolicitCriacaoContaTipoPDCAdapter getDetalharSolicitCriacaoContaTipoPDCAdapter() {
        return pdcDetalharSolicitCriacaoContaTipo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicitCriacaoContaTipo.
     * 
     * @param pdcDetalharSolicitCriacaoContaTipo
     *            Adaptador DetalharSolicitCriacaoContaTipo
     */
    public void setDetalharSolicitCriacaoContaTipoPDCAdapter(IDetalharSolicitCriacaoContaTipoPDCAdapter pdcDetalharSolicitCriacaoContaTipo) {
        this.pdcDetalharSolicitCriacaoContaTipo = pdcDetalharSolicitCriacaoContaTipo;
    }
    /**
     * M�todo invocado para obter um adaptador ListarClubesClientes.
     * 
     * @return Adaptador ListarClubesClientes
     */
    public IListarClubesClientesPDCAdapter getListarClubesClientesPDCAdapter() {
        return pdcListarClubesClientes;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarClubesClientes.
     * 
     * @param pdcListarClubesClientes
     *            Adaptador ListarClubesClientes
     */
    public void setListarClubesClientesPDCAdapter(IListarClubesClientesPDCAdapter pdcListarClubesClientes) {
        this.pdcListarClubesClientes = pdcListarClubesClientes;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoDebitoConta.
     * 
     * @return Adaptador DetalharManPagtoDebitoConta
     */
    public IDetalharManPagtoDebitoContaPDCAdapter getDetalharManPagtoDebitoContaPDCAdapter() {
        return pdcDetalharManPagtoDebitoConta;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoDebitoConta.
     * 
     * @param pdcDetalharManPagtoDebitoConta
     *            Adaptador DetalharManPagtoDebitoConta
     */
    public void setDetalharManPagtoDebitoContaPDCAdapter(IDetalharManPagtoDebitoContaPDCAdapter pdcDetalharManPagtoDebitoConta) {
        this.pdcDetalharManPagtoDebitoConta = pdcDetalharManPagtoDebitoConta;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoDepIdentificado.
     * 
     * @return Adaptador DetalharManPagtoDepIdentificado
     */
    public IDetalharManPagtoDepIdentificadoPDCAdapter getDetalharManPagtoDepIdentificadoPDCAdapter() {
        return pdcDetalharManPagtoDepIdentificado;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoDepIdentificado.
     * 
     * @param pdcDetalharManPagtoDepIdentificado
     *            Adaptador DetalharManPagtoDepIdentificado
     */
    public void setDetalharManPagtoDepIdentificadoPDCAdapter(IDetalharManPagtoDepIdentificadoPDCAdapter pdcDetalharManPagtoDepIdentificado) {
        this.pdcDetalharManPagtoDepIdentificado = pdcDetalharManPagtoDepIdentificado;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoOrdemCredito.
     * 
     * @return Adaptador DetalharManPagtoOrdemCredito
     */
    public IDetalharManPagtoOrdemCreditoPDCAdapter getDetalharManPagtoOrdemCreditoPDCAdapter() {
        return pdcDetalharManPagtoOrdemCredito;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoOrdemCredito.
     * 
     * @param pdcDetalharManPagtoOrdemCredito
     *            Adaptador DetalharManPagtoOrdemCredito
     */
    public void setDetalharManPagtoOrdemCreditoPDCAdapter(IDetalharManPagtoOrdemCreditoPDCAdapter pdcDetalharManPagtoOrdemCredito) {
        this.pdcDetalharManPagtoOrdemCredito = pdcDetalharManPagtoOrdemCredito;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoDebitoConta.
     * 
     * @return Adaptador DetalharPagtoDebitoConta
     */
    public IDetalharPagtoDebitoContaPDCAdapter getDetalharPagtoDebitoContaPDCAdapter() {
        return pdcDetalharPagtoDebitoConta;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoDebitoConta.
     * 
     * @param pdcDetalharPagtoDebitoConta
     *            Adaptador DetalharPagtoDebitoConta
     */
    public void setDetalharPagtoDebitoContaPDCAdapter(IDetalharPagtoDebitoContaPDCAdapter pdcDetalharPagtoDebitoConta) {
        this.pdcDetalharPagtoDebitoConta = pdcDetalharPagtoDebitoConta;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoDepIdentificado.
     * 
     * @return Adaptador DetalharPagtoDepIdentificado
     */
    public IDetalharPagtoDepIdentificadoPDCAdapter getDetalharPagtoDepIdentificadoPDCAdapter() {
        return pdcDetalharPagtoDepIdentificado;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoDepIdentificado.
     * 
     * @param pdcDetalharPagtoDepIdentificado
     *            Adaptador DetalharPagtoDepIdentificado
     */
    public void setDetalharPagtoDepIdentificadoPDCAdapter(IDetalharPagtoDepIdentificadoPDCAdapter pdcDetalharPagtoDepIdentificado) {
        this.pdcDetalharPagtoDepIdentificado = pdcDetalharPagtoDepIdentificado;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoOrdemCredito.
     * 
     * @return Adaptador DetalharPagtoOrdemCredito
     */
    public IDetalharPagtoOrdemCreditoPDCAdapter getDetalharPagtoOrdemCreditoPDCAdapter() {
        return pdcDetalharPagtoOrdemCredito;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoOrdemCredito.
     * 
     * @param pdcDetalharPagtoOrdemCredito
     *            Adaptador DetalharPagtoOrdemCredito
     */
    public void setDetalharPagtoOrdemCreditoPDCAdapter(IDetalharPagtoOrdemCreditoPDCAdapter pdcDetalharPagtoOrdemCredito) {
        this.pdcDetalharPagtoOrdemCredito = pdcDetalharPagtoOrdemCredito;
    }
    /**
     * M�todo invocado para obter um adaptador ListarDadosCtaConvnContingencia.
     * 
     * @return Adaptador ListarDadosCtaConvnContingencia
     */
    public IListarDadosCtaConvnContingenciaPDCAdapter getListarDadosCtaConvnContingenciaPDCAdapter() {
        return pdcListarDadosCtaConvnContingencia;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarDadosCtaConvnContingencia.
     * 
     * @param pdcListarDadosCtaConvnContingencia
     *            Adaptador ListarDadosCtaConvnContingencia
     */
    public void setListarDadosCtaConvnContingenciaPDCAdapter(IListarDadosCtaConvnContingenciaPDCAdapter pdcListarDadosCtaConvnContingencia) {
        this.pdcListarDadosCtaConvnContingencia = pdcListarDadosCtaConvnContingencia;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarParametroTela.
     * 
     * @return Adaptador ValidarParametroTela
     */
    public IValidarParametroTelaPDCAdapter getValidarParametroTelaPDCAdapter() {
        return pdcValidarParametroTela;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarParametroTela.
     * 
     * @param pdcValidarParametroTela
     *            Adaptador ValidarParametroTela
     */
    public void setValidarParametroTelaPDCAdapter(IValidarParametroTelaPDCAdapter pdcValidarParametroTela) {
        this.pdcValidarParametroTela = pdcValidarParametroTela;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAplictvFormtTipoLayoutArqv.
     * 
     * @return Adaptador ListarAplictvFormtTipoLayoutArqv
     */
    public IListarAplictvFormtTipoLayoutArqvPDCAdapter getListarAplictvFormtTipoLayoutArqvPDCAdapter() {
        return pdcListarAplictvFormtTipoLayoutArqv;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAplictvFormtTipoLayoutArqv.
     * 
     * @param pdcListarAplictvFormtTipoLayoutArqv
     *            Adaptador ListarAplictvFormtTipoLayoutArqv
     */
    public void setListarAplictvFormtTipoLayoutArqvPDCAdapter(IListarAplictvFormtTipoLayoutArqvPDCAdapter pdcListarAplictvFormtTipoLayoutArqv) {
        this.pdcListarAplictvFormtTipoLayoutArqv = pdcListarAplictvFormtTipoLayoutArqv;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharEnderecoParticipante.
     * 
     * @return Adaptador DetalharEnderecoParticipante
     */
    public IDetalharEnderecoParticipantePDCAdapter getDetalharEnderecoParticipantePDCAdapter() {
        return pdcDetalharEnderecoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharEnderecoParticipante.
     * 
     * @param pdcDetalharEnderecoParticipante
     *            Adaptador DetalharEnderecoParticipante
     */
    public void setDetalharEnderecoParticipantePDCAdapter(IDetalharEnderecoParticipantePDCAdapter pdcDetalharEnderecoParticipante) {
        this.pdcDetalharEnderecoParticipante = pdcDetalharEnderecoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistoricoEnderecoParticipante.
     * 
     * @return Adaptador DetalharHistoricoEnderecoParticipante
     */
    public IDetalharHistoricoEnderecoParticipantePDCAdapter getDetalharHistoricoEnderecoParticipantePDCAdapter() {
        return pdcDetalharHistoricoEnderecoParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistoricoEnderecoParticipante.
     * 
     * @param pdcDetalharHistoricoEnderecoParticipante
     *            Adaptador DetalharHistoricoEnderecoParticipante
     */
    public void setDetalharHistoricoEnderecoParticipantePDCAdapter(IDetalharHistoricoEnderecoParticipantePDCAdapter pdcDetalharHistoricoEnderecoParticipante) {
        this.pdcDetalharHistoricoEnderecoParticipante = pdcDetalharHistoricoEnderecoParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaOperacaoTarifaTipoServico.
     * 
     * @return Adaptador ConsultarListaOperacaoTarifaTipoServico
     */
    public IConsultarListaOperacaoTarifaTipoServicoPDCAdapter getConsultarListaOperacaoTarifaTipoServicoPDCAdapter() {
        return pdcConsultarListaOperacaoTarifaTipoServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaOperacaoTarifaTipoServico.
     * 
     * @param pdcConsultarListaOperacaoTarifaTipoServico
     *            Adaptador ConsultarListaOperacaoTarifaTipoServico
     */
    public void setConsultarListaOperacaoTarifaTipoServicoPDCAdapter(IConsultarListaOperacaoTarifaTipoServicoPDCAdapter pdcConsultarListaOperacaoTarifaTipoServico) {
        this.pdcConsultarListaOperacaoTarifaTipoServico = pdcConsultarListaOperacaoTarifaTipoServico;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharMsgLayoutArqRetorno.
     * 
     * @return Adaptador DetalharMsgLayoutArqRetorno
     */
    public IDetalharMsgLayoutArqRetornoPDCAdapter getDetalharMsgLayoutArqRetornoPDCAdapter() {
        return pdcDetalharMsgLayoutArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharMsgLayoutArqRetorno.
     * 
     * @param pdcDetalharMsgLayoutArqRetorno
     *            Adaptador DetalharMsgLayoutArqRetorno
     */
    public void setDetalharMsgLayoutArqRetornoPDCAdapter(IDetalharMsgLayoutArqRetornoPDCAdapter pdcDetalharMsgLayoutArqRetorno) {
        this.pdcDetalharMsgLayoutArqRetorno = pdcDetalharMsgLayoutArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaPeriodicidadeSistemaPGIC.
     * 
     * @return Adaptador ConsultarListaPeriodicidadeSistemaPGIC
     */
    public IConsultarListaPeriodicidadeSistemaPGICPDCAdapter getConsultarListaPeriodicidadeSistemaPGICPDCAdapter() {
        return pdcConsultarListaPeriodicidadeSistemaPGIC;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaPeriodicidadeSistemaPGIC.
     * 
     * @param pdcConsultarListaPeriodicidadeSistemaPGIC
     *            Adaptador ConsultarListaPeriodicidadeSistemaPGIC
     */
    public void setConsultarListaPeriodicidadeSistemaPGICPDCAdapter(IConsultarListaPeriodicidadeSistemaPGICPDCAdapter pdcConsultarListaPeriodicidadeSistemaPGIC) {
        this.pdcConsultarListaPeriodicidadeSistemaPGIC = pdcConsultarListaPeriodicidadeSistemaPGIC;
    }
    /**
     * M�todo invocado para obter um adaptador ListaTipoServicoContrato.
     * 
     * @return Adaptador ListaTipoServicoContrato
     */
    public IListaTipoServicoContratoPDCAdapter getListaTipoServicoContratoPDCAdapter() {
        return pdcListaTipoServicoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListaTipoServicoContrato.
     * 
     * @param pdcListaTipoServicoContrato
     *            Adaptador ListaTipoServicoContrato
     */
    public void setListaTipoServicoContratoPDCAdapter(IListaTipoServicoContratoPDCAdapter pdcListaTipoServicoContrato) {
        this.pdcListaTipoServicoContrato = pdcListaTipoServicoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarModalidadeContrato.
     * 
     * @return Adaptador ListarModalidadeContrato
     */
    public IListarModalidadeContratoPDCAdapter getListarModalidadeContratoPDCAdapter() {
        return pdcListarModalidadeContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarModalidadeContrato.
     * 
     * @param pdcListarModalidadeContrato
     *            Adaptador ListarModalidadeContrato
     */
    public void setListarModalidadeContratoPDCAdapter(IListarModalidadeContratoPDCAdapter pdcListarModalidadeContrato) {
        this.pdcListarModalidadeContrato = pdcListarModalidadeContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosConsAntPostergacao.
     * 
     * @return Adaptador ConsultarPagtosConsAntPostergacao
     */
    public IConsultarPagtosConsAntPostergacaoPDCAdapter getConsultarPagtosConsAntPostergacaoPDCAdapter() {
        return pdcConsultarPagtosConsAntPostergacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosConsAntPostergacao.
     * 
     * @param pdcConsultarPagtosConsAntPostergacao
     *            Adaptador ConsultarPagtosConsAntPostergacao
     */
    public void setConsultarPagtosConsAntPostergacaoPDCAdapter(IConsultarPagtosConsAntPostergacaoPDCAdapter pdcConsultarPagtosConsAntPostergacao) {
        this.pdcConsultarPagtosConsAntPostergacao = pdcConsultarPagtosConsAntPostergacao;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolEmiComprovanteCliePagador.
     * 
     * @return Adaptador DetalharSolEmiComprovanteCliePagador
     */
    public IDetalharSolEmiComprovanteCliePagadorPDCAdapter getDetalharSolEmiComprovanteCliePagadorPDCAdapter() {
        return pdcDetalharSolEmiComprovanteCliePagador;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolEmiComprovanteCliePagador.
     * 
     * @param pdcDetalharSolEmiComprovanteCliePagador
     *            Adaptador DetalharSolEmiComprovanteCliePagador
     */
    public void setDetalharSolEmiComprovanteCliePagadorPDCAdapter(IDetalharSolEmiComprovanteCliePagadorPDCAdapter pdcDetalharSolEmiComprovanteCliePagador) {
        this.pdcDetalharSolEmiComprovanteCliePagador = pdcDetalharSolEmiComprovanteCliePagador;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolEmiComprovanteFavorecido.
     * 
     * @return Adaptador DetalharSolEmiComprovanteFavorecido
     */
    public IDetalharSolEmiComprovanteFavorecidoPDCAdapter getDetalharSolEmiComprovanteFavorecidoPDCAdapter() {
        return pdcDetalharSolEmiComprovanteFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolEmiComprovanteFavorecido.
     * 
     * @param pdcDetalharSolEmiComprovanteFavorecido
     *            Adaptador DetalharSolEmiComprovanteFavorecido
     */
    public void setDetalharSolEmiComprovanteFavorecidoPDCAdapter(IDetalharSolEmiComprovanteFavorecidoPDCAdapter pdcDetalharSolEmiComprovanteFavorecido) {
        this.pdcDetalharSolEmiComprovanteFavorecido = pdcDetalharSolEmiComprovanteFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarDuplicacaoArqRetorno.
     * 
     * @return Adaptador AlterarDuplicacaoArqRetorno
     */
    public IAlterarDuplicacaoArqRetornoPDCAdapter getAlterarDuplicacaoArqRetornoPDCAdapter() {
        return pdcAlterarDuplicacaoArqRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarDuplicacaoArqRetorno.
     * 
     * @param pdcAlterarDuplicacaoArqRetorno
     *            Adaptador AlterarDuplicacaoArqRetorno
     */
    public void setAlterarDuplicacaoArqRetornoPDCAdapter(IAlterarDuplicacaoArqRetornoPDCAdapter pdcAlterarDuplicacaoArqRetorno) {
        this.pdcAlterarDuplicacaoArqRetorno = pdcAlterarDuplicacaoArqRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarTipoPendenciaResponsaveis.
     * 
     * @return Adaptador AlterarTipoPendenciaResponsaveis
     */
    public IAlterarTipoPendenciaResponsaveisPDCAdapter getAlterarTipoPendenciaResponsaveisPDCAdapter() {
        return pdcAlterarTipoPendenciaResponsaveis;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarTipoPendenciaResponsaveis.
     * 
     * @param pdcAlterarTipoPendenciaResponsaveis
     *            Adaptador AlterarTipoPendenciaResponsaveis
     */
    public void setAlterarTipoPendenciaResponsaveisPDCAdapter(IAlterarTipoPendenciaResponsaveisPDCAdapter pdcAlterarTipoPendenciaResponsaveis) {
        this.pdcAlterarTipoPendenciaResponsaveis = pdcAlterarTipoPendenciaResponsaveis;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharTipoPendenciaResponsaveis.
     * 
     * @return Adaptador DetalharTipoPendenciaResponsaveis
     */
    public IDetalharTipoPendenciaResponsaveisPDCAdapter getDetalharTipoPendenciaResponsaveisPDCAdapter() {
        return pdcDetalharTipoPendenciaResponsaveis;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharTipoPendenciaResponsaveis.
     * 
     * @param pdcDetalharTipoPendenciaResponsaveis
     *            Adaptador DetalharTipoPendenciaResponsaveis
     */
    public void setDetalharTipoPendenciaResponsaveisPDCAdapter(IDetalharTipoPendenciaResponsaveisPDCAdapter pdcDetalharTipoPendenciaResponsaveis) {
        this.pdcDetalharTipoPendenciaResponsaveis = pdcDetalharTipoPendenciaResponsaveis;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirTipoPendenciaResponsaveis.
     * 
     * @return Adaptador IncluirTipoPendenciaResponsaveis
     */
    public IIncluirTipoPendenciaResponsaveisPDCAdapter getIncluirTipoPendenciaResponsaveisPDCAdapter() {
        return pdcIncluirTipoPendenciaResponsaveis;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirTipoPendenciaResponsaveis.
     * 
     * @param pdcIncluirTipoPendenciaResponsaveis
     *            Adaptador IncluirTipoPendenciaResponsaveis
     */
    public void setIncluirTipoPendenciaResponsaveisPDCAdapter(IIncluirTipoPendenciaResponsaveisPDCAdapter pdcIncluirTipoPendenciaResponsaveis) {
        this.pdcIncluirTipoPendenciaResponsaveis = pdcIncluirTipoPendenciaResponsaveis;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMeioTransmissaoPagamento.
     * 
     * @return Adaptador ListarMeioTransmissaoPagamento
     */
    public IListarMeioTransmissaoPagamentoPDCAdapter getListarMeioTransmissaoPagamentoPDCAdapter() {
        return pdcListarMeioTransmissaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMeioTransmissaoPagamento.
     * 
     * @param pdcListarMeioTransmissaoPagamento
     *            Adaptador ListarMeioTransmissaoPagamento
     */
    public void setListarMeioTransmissaoPagamentoPDCAdapter(IListarMeioTransmissaoPagamentoPDCAdapter pdcListarMeioTransmissaoPagamento) {
        this.pdcListarMeioTransmissaoPagamento = pdcListarMeioTransmissaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaPerfilTrocaArquivo.
     * 
     * @return Adaptador ConsultarListaPerfilTrocaArquivo
     */
    public IConsultarListaPerfilTrocaArquivoPDCAdapter getConsultarListaPerfilTrocaArquivoPDCAdapter() {
        return pdcConsultarListaPerfilTrocaArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaPerfilTrocaArquivo.
     * 
     * @param pdcConsultarListaPerfilTrocaArquivo
     *            Adaptador ConsultarListaPerfilTrocaArquivo
     */
    public void setConsultarListaPerfilTrocaArquivoPDCAdapter(IConsultarListaPerfilTrocaArquivoPDCAdapter pdcConsultarListaPerfilTrocaArquivo) {
        this.pdcConsultarListaPerfilTrocaArquivo = pdcConsultarListaPerfilTrocaArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarManutencaoMeioTransmissao.
     * 
     * @return Adaptador ConsultarManutencaoMeioTransmissao
     */
    public IConsultarManutencaoMeioTransmissaoPDCAdapter getConsultarManutencaoMeioTransmissaoPDCAdapter() {
        return pdcConsultarManutencaoMeioTransmissao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarManutencaoMeioTransmissao.
     * 
     * @param pdcConsultarManutencaoMeioTransmissao
     *            Adaptador ConsultarManutencaoMeioTransmissao
     */
    public void setConsultarManutencaoMeioTransmissaoPDCAdapter(IConsultarManutencaoMeioTransmissaoPDCAdapter pdcConsultarManutencaoMeioTransmissao) {
        this.pdcConsultarManutencaoMeioTransmissao = pdcConsultarManutencaoMeioTransmissao;
    }
    /**
     * M�todo invocado para obter um adaptador ListaContratosVinculadosPerfil.
     * 
     * @return Adaptador ListaContratosVinculadosPerfil
     */
    public IListaContratosVinculadosPerfilPDCAdapter getListaContratosVinculadosPerfilPDCAdapter() {
        return pdcListaContratosVinculadosPerfil;
    }

    /**
     * M�todo invocado para establecer um adaptador ListaContratosVinculadosPerfil.
     * 
     * @param pdcListaContratosVinculadosPerfil
     *            Adaptador ListaContratosVinculadosPerfil
     */
    public void setListaContratosVinculadosPerfilPDCAdapter(IListaContratosVinculadosPerfilPDCAdapter pdcListaContratosVinculadosPerfil) {
        this.pdcListaContratosVinculadosPerfil = pdcListaContratosVinculadosPerfil;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirLiquidacaoPagamento.
     * 
     * @return Adaptador ExcluirLiquidacaoPagamento
     */
    public IExcluirLiquidacaoPagamentoPDCAdapter getExcluirLiquidacaoPagamentoPDCAdapter() {
        return pdcExcluirLiquidacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirLiquidacaoPagamento.
     * 
     * @param pdcExcluirLiquidacaoPagamento
     *            Adaptador ExcluirLiquidacaoPagamento
     */
    public void setExcluirLiquidacaoPagamentoPDCAdapter(IExcluirLiquidacaoPagamentoPDCAdapter pdcExcluirLiquidacaoPagamento) {
        this.pdcExcluirLiquidacaoPagamento = pdcExcluirLiquidacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAplicativoFormatacaoArquivo.
     * 
     * @return Adaptador ListarAplicativoFormatacaoArquivo
     */
    public IListarAplicativoFormatacaoArquivoPDCAdapter getListarAplicativoFormatacaoArquivoPDCAdapter() {
        return pdcListarAplicativoFormatacaoArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAplicativoFormatacaoArquivo.
     * 
     * @param pdcListarAplicativoFormatacaoArquivo
     *            Adaptador ListarAplicativoFormatacaoArquivo
     */
    public void setListarAplicativoFormatacaoArquivoPDCAdapter(IListarAplicativoFormatacaoArquivoPDCAdapter pdcListarAplicativoFormatacaoArquivo) {
        this.pdcListarAplicativoFormatacaoArquivo = pdcListarAplicativoFormatacaoArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarCondReajusteTarifa.
     * 
     * @return Adaptador ConsultarCondReajusteTarifa
     */
    public IConsultarCondReajusteTarifaPDCAdapter getConsultarCondReajusteTarifaPDCAdapter() {
        return pdcConsultarCondReajusteTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarCondReajusteTarifa.
     * 
     * @param pdcConsultarCondReajusteTarifa
     *            Adaptador ConsultarCondReajusteTarifa
     */
    public void setConsultarCondReajusteTarifaPDCAdapter(IConsultarCondReajusteTarifaPDCAdapter pdcConsultarCondReajusteTarifa) {
        this.pdcConsultarCondReajusteTarifa = pdcConsultarCondReajusteTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSituacaoSolicitacaoEstorno.
     * 
     * @return Adaptador ConsultarSituacaoSolicitacaoEstorno
     */
    public IConsultarSituacaoSolicitacaoEstornoPDCAdapter getConsultarSituacaoSolicitacaoEstornoPDCAdapter() {
        return pdcConsultarSituacaoSolicitacaoEstorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSituacaoSolicitacaoEstorno.
     * 
     * @param pdcConsultarSituacaoSolicitacaoEstorno
     *            Adaptador ConsultarSituacaoSolicitacaoEstorno
     */
    public void setConsultarSituacaoSolicitacaoEstornoPDCAdapter(IConsultarSituacaoSolicitacaoEstornoPDCAdapter pdcConsultarSituacaoSolicitacaoEstorno) {
        this.pdcConsultarSituacaoSolicitacaoEstorno = pdcConsultarSituacaoSolicitacaoEstorno;
    }
    /**
     * M�todo invocado para obter um adaptador ReenviarEmail.
     * 
     * @return Adaptador ReenviarEmail
     */
    public IReenviarEmailPDCAdapter getReenviarEmailPDCAdapter() {
        return pdcReenviarEmail;
    }

    /**
     * M�todo invocado para establecer um adaptador ReenviarEmail.
     * 
     * @param pdcReenviarEmail
     *            Adaptador ReenviarEmail
     */
    public void setReenviarEmailPDCAdapter(IReenviarEmailPDCAdapter pdcReenviarEmail) {
        this.pdcReenviarEmail = pdcReenviarEmail;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirTipoServModContrato.
     * 
     * @return Adaptador IncluirTipoServModContrato
     */
    public IIncluirTipoServModContratoPDCAdapter getIncluirTipoServModContratoPDCAdapter() {
        return pdcIncluirTipoServModContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirTipoServModContrato.
     * 
     * @param pdcIncluirTipoServModContrato
     *            Adaptador IncluirTipoServModContrato
     */
    public void setIncluirTipoServModContratoPDCAdapter(IIncluirTipoServModContratoPDCAdapter pdcIncluirTipoServModContrato) {
        this.pdcIncluirTipoServModContrato = pdcIncluirTipoServModContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoEquiparacaoContrato.
     * 
     * @return Adaptador ListarServicoEquiparacaoContrato
     */
    public IListarServicoEquiparacaoContratoPDCAdapter getListarServicoEquiparacaoContratoPDCAdapter() {
        return pdcListarServicoEquiparacaoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoEquiparacaoContrato.
     * 
     * @param pdcListarServicoEquiparacaoContrato
     *            Adaptador ListarServicoEquiparacaoContrato
     */
    public void setListarServicoEquiparacaoContratoPDCAdapter(IListarServicoEquiparacaoContratoPDCAdapter pdcListarServicoEquiparacaoContrato) {
        this.pdcListarServicoEquiparacaoContrato = pdcListarServicoEquiparacaoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarModalidadeEquiparacaoContrato.
     * 
     * @return Adaptador ListarModalidadeEquiparacaoContrato
     */
    public IListarModalidadeEquiparacaoContratoPDCAdapter getListarModalidadeEquiparacaoContratoPDCAdapter() {
        return pdcListarModalidadeEquiparacaoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarModalidadeEquiparacaoContrato.
     * 
     * @param pdcListarModalidadeEquiparacaoContrato
     *            Adaptador ListarModalidadeEquiparacaoContrato
     */
    public void setListarModalidadeEquiparacaoContratoPDCAdapter(IListarModalidadeEquiparacaoContratoPDCAdapter pdcListarModalidadeEquiparacaoContrato) {
        this.pdcListarModalidadeEquiparacaoContrato = pdcListarModalidadeEquiparacaoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoModalidadeEmissaoAviso.
     * 
     * @return Adaptador ListarServicoModalidadeEmissaoAviso
     */
    public IListarServicoModalidadeEmissaoAvisoPDCAdapter getListarServicoModalidadeEmissaoAvisoPDCAdapter() {
        return pdcListarServicoModalidadeEmissaoAviso;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoModalidadeEmissaoAviso.
     * 
     * @param pdcListarServicoModalidadeEmissaoAviso
     *            Adaptador ListarServicoModalidadeEmissaoAviso
     */
    public void setListarServicoModalidadeEmissaoAvisoPDCAdapter(IListarServicoModalidadeEmissaoAvisoPDCAdapter pdcListarServicoModalidadeEmissaoAviso) {
        this.pdcListarServicoModalidadeEmissaoAviso = pdcListarServicoModalidadeEmissaoAviso;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarServico.
     * 
     * @return Adaptador ConsultarServico
     */
    public IConsultarServicoPDCAdapter getConsultarServicoPDCAdapter() {
        return pdcConsultarServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarServico.
     * 
     * @param pdcConsultarServico
     *            Adaptador ConsultarServico
     */
    public void setConsultarServicoPDCAdapter(IConsultarServicoPDCAdapter pdcConsultarServico) {
        this.pdcConsultarServico = pdcConsultarServico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarContratoClienteFiltro.
     * 
     * @return Adaptador ConsultarContratoClienteFiltro
     */
    public IConsultarContratoClienteFiltroPDCAdapter getConsultarContratoClienteFiltroPDCAdapter() {
        return pdcConsultarContratoClienteFiltro;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarContratoClienteFiltro.
     * 
     * @param pdcConsultarContratoClienteFiltro
     *            Adaptador ConsultarContratoClienteFiltro
     */
    public void setConsultarContratoClienteFiltroPDCAdapter(IConsultarContratoClienteFiltroPDCAdapter pdcConsultarContratoClienteFiltro) {
        this.pdcConsultarContratoClienteFiltro = pdcConsultarContratoClienteFiltro;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContratosPgit.
     * 
     * @return Adaptador ListarContratosPgit
     */
    public IListarContratosPgitPDCAdapter getListarContratosPgitPDCAdapter() {
        return pdcListarContratosPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContratosPgit.
     * 
     * @param pdcListarContratosPgit
     *            Adaptador ListarContratosPgit
     */
    public void setListarContratosPgitPDCAdapter(IListarContratosPgitPDCAdapter pdcListarContratosPgit) {
        this.pdcListarContratosPgit = pdcListarContratosPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaContratosPessoas.
     * 
     * @return Adaptador ConsultarListaContratosPessoas
     */
    public IConsultarListaContratosPessoasPDCAdapter getConsultarListaContratosPessoasPDCAdapter() {
        return pdcConsultarListaContratosPessoas;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaContratosPessoas.
     * 
     * @param pdcConsultarListaContratosPessoas
     *            Adaptador ConsultarListaContratosPessoas
     */
    public void setConsultarListaContratosPessoasPDCAdapter(IConsultarListaContratosPessoasPDCAdapter pdcConsultarListaContratosPessoas) {
        this.pdcConsultarListaContratosPessoas = pdcConsultarListaContratosPessoas;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharVincConvnCtaSalario.
     * 
     * @return Adaptador DetalharVincConvnCtaSalario
     */
    public IDetalharVincConvnCtaSalarioPDCAdapter getDetalharVincConvnCtaSalarioPDCAdapter() {
        return pdcDetalharVincConvnCtaSalario;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharVincConvnCtaSalario.
     * 
     * @param pdcDetalharVincConvnCtaSalario
     *            Adaptador DetalharVincConvnCtaSalario
     */
    public void setDetalharVincConvnCtaSalarioPDCAdapter(IDetalharVincConvnCtaSalarioPDCAdapter pdcDetalharVincConvnCtaSalario) {
        this.pdcDetalharVincConvnCtaSalario = pdcDetalharVincConvnCtaSalario;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAlteracaoAmbientePartc.
     * 
     * @return Adaptador IncluirAlteracaoAmbientePartc
     */
    public IIncluirAlteracaoAmbientePartcPDCAdapter getIncluirAlteracaoAmbientePartcPDCAdapter() {
        return pdcIncluirAlteracaoAmbientePartc;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAlteracaoAmbientePartc.
     * 
     * @param pdcIncluirAlteracaoAmbientePartc
     *            Adaptador IncluirAlteracaoAmbientePartc
     */
    public void setIncluirAlteracaoAmbientePartcPDCAdapter(IIncluirAlteracaoAmbientePartcPDCAdapter pdcIncluirAlteracaoAmbientePartc) {
        this.pdcIncluirAlteracaoAmbientePartc = pdcIncluirAlteracaoAmbientePartc;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicitacaoAmbientePartc.
     * 
     * @return Adaptador ConsultarSolicitacaoAmbientePartc
     */
    public IConsultarSolicitacaoAmbientePartcPDCAdapter getConsultarSolicitacaoAmbientePartcPDCAdapter() {
        return pdcConsultarSolicitacaoAmbientePartc;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicitacaoAmbientePartc.
     * 
     * @param pdcConsultarSolicitacaoAmbientePartc
     *            Adaptador ConsultarSolicitacaoAmbientePartc
     */
    public void setConsultarSolicitacaoAmbientePartcPDCAdapter(IConsultarSolicitacaoAmbientePartcPDCAdapter pdcConsultarSolicitacaoAmbientePartc) {
        this.pdcConsultarSolicitacaoAmbientePartc = pdcConsultarSolicitacaoAmbientePartc;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAlteracaoAmbientePartc.
     * 
     * @return Adaptador ExcluirAlteracaoAmbientePartc
     */
    public IExcluirAlteracaoAmbientePartcPDCAdapter getExcluirAlteracaoAmbientePartcPDCAdapter() {
        return pdcExcluirAlteracaoAmbientePartc;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAlteracaoAmbientePartc.
     * 
     * @param pdcExcluirAlteracaoAmbientePartc
     *            Adaptador ExcluirAlteracaoAmbientePartc
     */
    public void setExcluirAlteracaoAmbientePartcPDCAdapter(IExcluirAlteracaoAmbientePartcPDCAdapter pdcExcluirAlteracaoAmbientePartc) {
        this.pdcExcluirAlteracaoAmbientePartc = pdcExcluirAlteracaoAmbientePartc;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharAmbiente.
     * 
     * @return Adaptador DetalharAmbiente
     */
    public IDetalharAmbientePDCAdapter getDetalharAmbientePDCAdapter() {
        return pdcDetalharAmbiente;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharAmbiente.
     * 
     * @param pdcDetalharAmbiente
     *            Adaptador DetalharAmbiente
     */
    public void setDetalharAmbientePDCAdapter(IDetalharAmbientePDCAdapter pdcDetalharAmbiente) {
        this.pdcDetalharAmbiente = pdcDetalharAmbiente;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConManAmbPart.
     * 
     * @return Adaptador ConsultarConManAmbPart
     */
    public IConsultarConManAmbPartPDCAdapter getConsultarConManAmbPartPDCAdapter() {
        return pdcConsultarConManAmbPart;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConManAmbPart.
     * 
     * @param pdcConsultarConManAmbPart
     *            Adaptador ConsultarConManAmbPart
     */
    public void setConsultarConManAmbPartPDCAdapter(IConsultarConManAmbPartPDCAdapter pdcConsultarConManAmbPart) {
        this.pdcConsultarConManAmbPart = pdcConsultarConManAmbPart;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConManAmbPart.
     * 
     * @return Adaptador ListarConManAmbPart
     */
    public IListarConManAmbPartPDCAdapter getListarConManAmbPartPDCAdapter() {
        return pdcListarConManAmbPart;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConManAmbPart.
     * 
     * @param pdcListarConManAmbPart
     *            Adaptador ListarConManAmbPart
     */
    public void setListarConManAmbPartPDCAdapter(IListarConManAmbPartPDCAdapter pdcListarConManAmbPart) {
        this.pdcListarConManAmbPart = pdcListarConManAmbPart;
    }
    /**
     * M�todo invocado para obter um adaptador VerificaraAtributoSoltc.
     * 
     * @return Adaptador VerificaraAtributoSoltc
     */
    public IVerificaraAtributoSoltcPDCAdapter getVerificaraAtributoSoltcPDCAdapter() {
        return pdcVerificaraAtributoSoltc;
    }

    /**
     * M�todo invocado para establecer um adaptador VerificaraAtributoSoltc.
     * 
     * @param pdcVerificaraAtributoSoltc
     *            Adaptador VerificaraAtributoSoltc
     */
    public void setVerificaraAtributoSoltcPDCAdapter(IVerificaraAtributoSoltcPDCAdapter pdcVerificaraAtributoSoltc) {
        this.pdcVerificaraAtributoSoltc = pdcVerificaraAtributoSoltc;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarContaContrato.
     * 
     * @return Adaptador ConsultarContaContrato
     */
    public IConsultarContaContratoPDCAdapter getConsultarContaContratoPDCAdapter() {
        return pdcConsultarContaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarContaContrato.
     * 
     * @param pdcConsultarContaContrato
     *            Adaptador ConsultarContaContrato
     */
    public void setConsultarContaContratoPDCAdapter(IConsultarContaContratoPDCAdapter pdcConsultarContaContrato) {
        this.pdcConsultarContaContrato = pdcConsultarContaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAssocTrocaArqParticipante.
     * 
     * @return Adaptador ListarAssocTrocaArqParticipante
     */
    public IListarAssocTrocaArqParticipantePDCAdapter getListarAssocTrocaArqParticipantePDCAdapter() {
        return pdcListarAssocTrocaArqParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAssocTrocaArqParticipante.
     * 
     * @param pdcListarAssocTrocaArqParticipante
     *            Adaptador ListarAssocTrocaArqParticipante
     */
    public void setListarAssocTrocaArqParticipantePDCAdapter(IListarAssocTrocaArqParticipantePDCAdapter pdcListarAssocTrocaArqParticipante) {
        this.pdcListarAssocTrocaArqParticipante = pdcListarAssocTrocaArqParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParticipante.
     * 
     * @return Adaptador ListarParticipante
     */
    public IListarParticipantePDCAdapter getListarParticipantePDCAdapter() {
        return pdcListarParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParticipante.
     * 
     * @param pdcListarParticipante
     *            Adaptador ListarParticipante
     */
    public void setListarParticipantePDCAdapter(IListarParticipantePDCAdapter pdcListarParticipante) {
        this.pdcListarParticipante = pdcListarParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistAssocTrocaArqParticipante.
     * 
     * @return Adaptador ListarHistAssocTrocaArqParticipante
     */
    public IListarHistAssocTrocaArqParticipantePDCAdapter getListarHistAssocTrocaArqParticipantePDCAdapter() {
        return pdcListarHistAssocTrocaArqParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistAssocTrocaArqParticipante.
     * 
     * @param pdcListarHistAssocTrocaArqParticipante
     *            Adaptador ListarHistAssocTrocaArqParticipante
     */
    public void setListarHistAssocTrocaArqParticipantePDCAdapter(IListarHistAssocTrocaArqParticipantePDCAdapter pdcListarHistAssocTrocaArqParticipante) {
        this.pdcListarHistAssocTrocaArqParticipante = pdcListarHistAssocTrocaArqParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaPendenciaContrato.
     * 
     * @return Adaptador ConsultarListaPendenciaContrato
     */
    public IConsultarListaPendenciaContratoPDCAdapter getConsultarListaPendenciaContratoPDCAdapter() {
        return pdcConsultarListaPendenciaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaPendenciaContrato.
     * 
     * @param pdcConsultarListaPendenciaContrato
     *            Adaptador ConsultarListaPendenciaContrato
     */
    public void setConsultarListaPendenciaContratoPDCAdapter(IConsultarListaPendenciaContratoPDCAdapter pdcConsultarListaPendenciaContrato) {
        this.pdcConsultarListaPendenciaContrato = pdcConsultarListaPendenciaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParticipantes.
     * 
     * @return Adaptador ListarParticipantes
     */
    public IListarParticipantesPDCAdapter getListarParticipantesPDCAdapter() {
        return pdcListarParticipantes;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParticipantes.
     * 
     * @param pdcListarParticipantes
     *            Adaptador ListarParticipantes
     */
    public void setListarParticipantesPDCAdapter(IListarParticipantesPDCAdapter pdcListarParticipantes) {
        this.pdcListarParticipantes = pdcListarParticipantes;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConManParticipantes.
     * 
     * @return Adaptador ConsultarConManParticipantes
     */
    public IConsultarConManParticipantesPDCAdapter getConsultarConManParticipantesPDCAdapter() {
        return pdcConsultarConManParticipantes;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConManParticipantes.
     * 
     * @param pdcConsultarConManParticipantes
     *            Adaptador ConsultarConManParticipantes
     */
    public void setConsultarConManParticipantesPDCAdapter(IConsultarConManParticipantesPDCAdapter pdcConsultarConManParticipantes) {
        this.pdcConsultarConManParticipantes = pdcConsultarConManParticipantes;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirContratoMulticanal.
     * 
     * @return Adaptador ImprimirContratoMulticanal
     */
    public IImprimirContratoMulticanalPDCAdapter getImprimirContratoMulticanalPDCAdapter() {
        return pdcImprimirContratoMulticanal;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirContratoMulticanal.
     * 
     * @param pdcImprimirContratoMulticanal
     *            Adaptador ImprimirContratoMulticanal
     */
    public void setImprimirContratoMulticanalPDCAdapter(IImprimirContratoMulticanalPDCAdapter pdcImprimirContratoMulticanal) {
        this.pdcImprimirContratoMulticanal = pdcImprimirContratoMulticanal;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarTarifaAcimaPadrao.
     * 
     * @return Adaptador AlterarTarifaAcimaPadrao
     */
    public IAlterarTarifaAcimaPadraoPDCAdapter getAlterarTarifaAcimaPadraoPDCAdapter() {
        return pdcAlterarTarifaAcimaPadrao;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarTarifaAcimaPadrao.
     * 
     * @param pdcAlterarTarifaAcimaPadrao
     *            Adaptador AlterarTarifaAcimaPadrao
     */
    public void setAlterarTarifaAcimaPadraoPDCAdapter(IAlterarTarifaAcimaPadraoPDCAdapter pdcAlterarTarifaAcimaPadrao) {
        this.pdcAlterarTarifaAcimaPadrao = pdcAlterarTarifaAcimaPadrao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSoliRelatorioPagamento.
     * 
     * @return Adaptador ListarSoliRelatorioPagamento
     */
    public IListarSoliRelatorioPagamentoPDCAdapter getListarSoliRelatorioPagamentoPDCAdapter() {
        return pdcListarSoliRelatorioPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSoliRelatorioPagamento.
     * 
     * @param pdcListarSoliRelatorioPagamento
     *            Adaptador ListarSoliRelatorioPagamento
     */
    public void setListarSoliRelatorioPagamentoPDCAdapter(IListarSoliRelatorioPagamentoPDCAdapter pdcListarSoliRelatorioPagamento) {
        this.pdcListarSoliRelatorioPagamento = pdcListarSoliRelatorioPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSoliRelatorioPagamento.
     * 
     * @return Adaptador DetalharSoliRelatorioPagamento
     */
    public IDetalharSoliRelatorioPagamentoPDCAdapter getDetalharSoliRelatorioPagamentoPDCAdapter() {
        return pdcDetalharSoliRelatorioPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSoliRelatorioPagamento.
     * 
     * @param pdcDetalharSoliRelatorioPagamento
     *            Adaptador DetalharSoliRelatorioPagamento
     */
    public void setDetalharSoliRelatorioPagamentoPDCAdapter(IDetalharSoliRelatorioPagamentoPDCAdapter pdcDetalharSoliRelatorioPagamento) {
        this.pdcDetalharSoliRelatorioPagamento = pdcDetalharSoliRelatorioPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSoliRelatorioPagamento.
     * 
     * @return Adaptador ExcluirSoliRelatorioPagamento
     */
    public IExcluirSoliRelatorioPagamentoPDCAdapter getExcluirSoliRelatorioPagamentoPDCAdapter() {
        return pdcExcluirSoliRelatorioPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSoliRelatorioPagamento.
     * 
     * @param pdcExcluirSoliRelatorioPagamento
     *            Adaptador ExcluirSoliRelatorioPagamento
     */
    public void setExcluirSoliRelatorioPagamentoPDCAdapter(IExcluirSoliRelatorioPagamentoPDCAdapter pdcExcluirSoliRelatorioPagamento) {
        this.pdcExcluirSoliRelatorioPagamento = pdcExcluirSoliRelatorioPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSoliRelatorioPagamentoValidacao.
     * 
     * @return Adaptador IncluirSoliRelatorioPagamentoValidacao
     */
    public IIncluirSoliRelatorioPagamentoValidacaoPDCAdapter getIncluirSoliRelatorioPagamentoValidacaoPDCAdapter() {
        return pdcIncluirSoliRelatorioPagamentoValidacao;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSoliRelatorioPagamentoValidacao.
     * 
     * @param pdcIncluirSoliRelatorioPagamentoValidacao
     *            Adaptador IncluirSoliRelatorioPagamentoValidacao
     */
    public void setIncluirSoliRelatorioPagamentoValidacaoPDCAdapter(IIncluirSoliRelatorioPagamentoValidacaoPDCAdapter pdcIncluirSoliRelatorioPagamentoValidacao) {
        this.pdcIncluirSoliRelatorioPagamentoValidacao = pdcIncluirSoliRelatorioPagamentoValidacao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSoliRelatorioPagamento.
     * 
     * @return Adaptador IncluirSoliRelatorioPagamento
     */
    public IIncluirSoliRelatorioPagamentoPDCAdapter getIncluirSoliRelatorioPagamentoPDCAdapter() {
        return pdcIncluirSoliRelatorioPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSoliRelatorioPagamento.
     * 
     * @param pdcIncluirSoliRelatorioPagamento
     *            Adaptador IncluirSoliRelatorioPagamento
     */
    public void setIncluirSoliRelatorioPagamentoPDCAdapter(IIncluirSoliRelatorioPagamentoPDCAdapter pdcIncluirSoliRelatorioPagamento) {
        this.pdcIncluirSoliRelatorioPagamento = pdcIncluirSoliRelatorioPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSoliRelatorioOrgaosPublicos.
     * 
     * @return Adaptador ListarSoliRelatorioOrgaosPublicos
     */
    public IListarSoliRelatorioOrgaosPublicosPDCAdapter getListarSoliRelatorioOrgaosPublicosPDCAdapter() {
        return pdcListarSoliRelatorioOrgaosPublicos;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSoliRelatorioOrgaosPublicos.
     * 
     * @param pdcListarSoliRelatorioOrgaosPublicos
     *            Adaptador ListarSoliRelatorioOrgaosPublicos
     */
    public void setListarSoliRelatorioOrgaosPublicosPDCAdapter(IListarSoliRelatorioOrgaosPublicosPDCAdapter pdcListarSoliRelatorioOrgaosPublicos) {
        this.pdcListarSoliRelatorioOrgaosPublicos = pdcListarSoliRelatorioOrgaosPublicos;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSoliRelatorioOrgaosPublicos.
     * 
     * @return Adaptador ExcluirSoliRelatorioOrgaosPublicos
     */
    public IExcluirSoliRelatorioOrgaosPublicosPDCAdapter getExcluirSoliRelatorioOrgaosPublicosPDCAdapter() {
        return pdcExcluirSoliRelatorioOrgaosPublicos;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSoliRelatorioOrgaosPublicos.
     * 
     * @param pdcExcluirSoliRelatorioOrgaosPublicos
     *            Adaptador ExcluirSoliRelatorioOrgaosPublicos
     */
    public void setExcluirSoliRelatorioOrgaosPublicosPDCAdapter(IExcluirSoliRelatorioOrgaosPublicosPDCAdapter pdcExcluirSoliRelatorioOrgaosPublicos) {
        this.pdcExcluirSoliRelatorioOrgaosPublicos = pdcExcluirSoliRelatorioOrgaosPublicos;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharParticipantesOrgaoPublico.
     * 
     * @return Adaptador DetalharParticipantesOrgaoPublico
     */
    public IDetalharParticipantesOrgaoPublicoPDCAdapter getDetalharParticipantesOrgaoPublicoPDCAdapter() {
        return pdcDetalharParticipantesOrgaoPublico;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharParticipantesOrgaoPublico.
     * 
     * @param pdcDetalharParticipantesOrgaoPublico
     *            Adaptador DetalharParticipantesOrgaoPublico
     */
    public void setDetalharParticipantesOrgaoPublicoPDCAdapter(IDetalharParticipantesOrgaoPublicoPDCAdapter pdcDetalharParticipantesOrgaoPublico) {
        this.pdcDetalharParticipantesOrgaoPublico = pdcDetalharParticipantesOrgaoPublico;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirParticipantesOrgaoPublico.
     * 
     * @return Adaptador ExcluirParticipantesOrgaoPublico
     */
    public IExcluirParticipantesOrgaoPublicoPDCAdapter getExcluirParticipantesOrgaoPublicoPDCAdapter() {
        return pdcExcluirParticipantesOrgaoPublico;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirParticipantesOrgaoPublico.
     * 
     * @param pdcExcluirParticipantesOrgaoPublico
     *            Adaptador ExcluirParticipantesOrgaoPublico
     */
    public void setExcluirParticipantesOrgaoPublicoPDCAdapter(IExcluirParticipantesOrgaoPublicoPDCAdapter pdcExcluirParticipantesOrgaoPublico) {
        this.pdcExcluirParticipantesOrgaoPublico = pdcExcluirParticipantesOrgaoPublico;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarParticipantesOrgaoPublico.
     * 
     * @return Adaptador AlterarParticipantesOrgaoPublico
     */
    public IAlterarParticipantesOrgaoPublicoPDCAdapter getAlterarParticipantesOrgaoPublicoPDCAdapter() {
        return pdcAlterarParticipantesOrgaoPublico;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarParticipantesOrgaoPublico.
     * 
     * @param pdcAlterarParticipantesOrgaoPublico
     *            Adaptador AlterarParticipantesOrgaoPublico
     */
    public void setAlterarParticipantesOrgaoPublicoPDCAdapter(IAlterarParticipantesOrgaoPublicoPDCAdapter pdcAlterarParticipantesOrgaoPublico) {
        this.pdcAlterarParticipantesOrgaoPublico = pdcAlterarParticipantesOrgaoPublico;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarContratoPagSalPartOrgaoPublico.
     * 
     * @return Adaptador ValidarContratoPagSalPartOrgaoPublico
     */
    public IValidarContratoPagSalPartOrgaoPublicoPDCAdapter getValidarContratoPagSalPartOrgaoPublicoPDCAdapter() {
        return pdcValidarContratoPagSalPartOrgaoPublico;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarContratoPagSalPartOrgaoPublico.
     * 
     * @param pdcValidarContratoPagSalPartOrgaoPublico
     *            Adaptador ValidarContratoPagSalPartOrgaoPublico
     */
    public void setValidarContratoPagSalPartOrgaoPublicoPDCAdapter(IValidarContratoPagSalPartOrgaoPublicoPDCAdapter pdcValidarContratoPagSalPartOrgaoPublico) {
        this.pdcValidarContratoPagSalPartOrgaoPublico = pdcValidarContratoPagSalPartOrgaoPublico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarCtasVincPartOrgaoPublico.
     * 
     * @return Adaptador ListarCtasVincPartOrgaoPublico
     */
    public IListarCtasVincPartOrgaoPublicoPDCAdapter getListarCtasVincPartOrgaoPublicoPDCAdapter() {
        return pdcListarCtasVincPartOrgaoPublico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarCtasVincPartOrgaoPublico.
     * 
     * @param pdcListarCtasVincPartOrgaoPublico
     *            Adaptador ListarCtasVincPartOrgaoPublico
     */
    public void setListarCtasVincPartOrgaoPublicoPDCAdapter(IListarCtasVincPartOrgaoPublicoPDCAdapter pdcListarCtasVincPartOrgaoPublico) {
        this.pdcListarCtasVincPartOrgaoPublico = pdcListarCtasVincPartOrgaoPublico;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirParticipantesOrgaoPublico.
     * 
     * @return Adaptador IncluirParticipantesOrgaoPublico
     */
    public IIncluirParticipantesOrgaoPublicoPDCAdapter getIncluirParticipantesOrgaoPublicoPDCAdapter() {
        return pdcIncluirParticipantesOrgaoPublico;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirParticipantesOrgaoPublico.
     * 
     * @param pdcIncluirParticipantesOrgaoPublico
     *            Adaptador IncluirParticipantesOrgaoPublico
     */
    public void setIncluirParticipantesOrgaoPublicoPDCAdapter(IIncluirParticipantesOrgaoPublicoPDCAdapter pdcIncluirParticipantesOrgaoPublico) {
        this.pdcIncluirParticipantesOrgaoPublico = pdcIncluirParticipantesOrgaoPublico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarUsuarioSoliRelatorioOrgaosPublicos.
     * 
     * @return Adaptador ConsultarUsuarioSoliRelatorioOrgaosPublicos
     */
    public IConsultarUsuarioSoliRelatorioOrgaosPublicosPDCAdapter getConsultarUsuarioSoliRelatorioOrgaosPublicosPDCAdapter() {
        return pdcConsultarUsuarioSoliRelatorioOrgaosPublicos;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarUsuarioSoliRelatorioOrgaosPublicos.
     * 
     * @param pdcConsultarUsuarioSoliRelatorioOrgaosPublicos
     *            Adaptador ConsultarUsuarioSoliRelatorioOrgaosPublicos
     */
    public void setConsultarUsuarioSoliRelatorioOrgaosPublicosPDCAdapter(IConsultarUsuarioSoliRelatorioOrgaosPublicosPDCAdapter pdcConsultarUsuarioSoliRelatorioOrgaosPublicos) {
        this.pdcConsultarUsuarioSoliRelatorioOrgaosPublicos = pdcConsultarUsuarioSoliRelatorioOrgaosPublicos;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSoliRelatorioOrgaosPublicos.
     * 
     * @return Adaptador IncluirSoliRelatorioOrgaosPublicos
     */
    public IIncluirSoliRelatorioOrgaosPublicosPDCAdapter getIncluirSoliRelatorioOrgaosPublicosPDCAdapter() {
        return pdcIncluirSoliRelatorioOrgaosPublicos;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSoliRelatorioOrgaosPublicos.
     * 
     * @param pdcIncluirSoliRelatorioOrgaosPublicos
     *            Adaptador IncluirSoliRelatorioOrgaosPublicos
     */
    public void setIncluirSoliRelatorioOrgaosPublicosPDCAdapter(IIncluirSoliRelatorioOrgaosPublicosPDCAdapter pdcIncluirSoliRelatorioOrgaosPublicos) {
        this.pdcIncluirSoliRelatorioOrgaosPublicos = pdcIncluirSoliRelatorioOrgaosPublicos;
    }
    /**
     * M�todo invocado para obter um adaptador EstornarPagamentosOP.
     * 
     * @return Adaptador EstornarPagamentosOP
     */
    public IEstornarPagamentosOPPDCAdapter getEstornarPagamentosOPPDCAdapter() {
        return pdcEstornarPagamentosOP;
    }

    /**
     * M�todo invocado para establecer um adaptador EstornarPagamentosOP.
     * 
     * @param pdcEstornarPagamentosOP
     *            Adaptador EstornarPagamentosOP
     */
    public void setEstornarPagamentosOPPDCAdapter(IEstornarPagamentosOPPDCAdapter pdcEstornarPagamentosOP) {
        this.pdcEstornarPagamentosOP = pdcEstornarPagamentosOP;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalhesSolicitacaoOP.
     * 
     * @return Adaptador ConsultarDetalhesSolicitacaoOP
     */
    public IConsultarDetalhesSolicitacaoOPPDCAdapter getConsultarDetalhesSolicitacaoOPPDCAdapter() {
        return pdcConsultarDetalhesSolicitacaoOP;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalhesSolicitacaoOP.
     * 
     * @param pdcConsultarDetalhesSolicitacaoOP
     *            Adaptador ConsultarDetalhesSolicitacaoOP
     */
    public void setConsultarDetalhesSolicitacaoOPPDCAdapter(IConsultarDetalhesSolicitacaoOPPDCAdapter pdcConsultarDetalhesSolicitacaoOP) {
        this.pdcConsultarDetalhesSolicitacaoOP = pdcConsultarDetalhesSolicitacaoOP;
    }
    /**
     * M�todo invocado para obter um adaptador ConsSolicitacaoEstornoPagtosOP.
     * 
     * @return Adaptador ConsSolicitacaoEstornoPagtosOP
     */
    public IConsSolicitacaoEstornoPagtosOPPDCAdapter getConsSolicitacaoEstornoPagtosOPPDCAdapter() {
        return pdcConsSolicitacaoEstornoPagtosOP;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsSolicitacaoEstornoPagtosOP.
     * 
     * @param pdcConsSolicitacaoEstornoPagtosOP
     *            Adaptador ConsSolicitacaoEstornoPagtosOP
     */
    public void setConsSolicitacaoEstornoPagtosOPPDCAdapter(IConsSolicitacaoEstornoPagtosOPPDCAdapter pdcConsSolicitacaoEstornoPagtosOP) {
        this.pdcConsSolicitacaoEstornoPagtosOP = pdcConsSolicitacaoEstornoPagtosOP;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaServicosPgto.
     * 
     * @return Adaptador ConsultarListaServicosPgto
     */
    public IConsultarListaServicosPgtoPDCAdapter getConsultarListaServicosPgtoPDCAdapter() {
        return pdcConsultarListaServicosPgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaServicosPgto.
     * 
     * @param pdcConsultarListaServicosPgto
     *            Adaptador ConsultarListaServicosPgto
     */
    public void setConsultarListaServicosPgtoPDCAdapter(IConsultarListaServicosPgtoPDCAdapter pdcConsultarListaServicosPgto) {
        this.pdcConsultarListaServicosPgto = pdcConsultarListaServicosPgto;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaModalidades.
     * 
     * @return Adaptador ConsultarListaModalidades
     */
    public IConsultarListaModalidadesPDCAdapter getConsultarListaModalidadesPDCAdapter() {
        return pdcConsultarListaModalidades;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaModalidades.
     * 
     * @param pdcConsultarListaModalidades
     *            Adaptador ConsultarListaModalidades
     */
    public void setConsultarListaModalidadesPDCAdapter(IConsultarListaModalidadesPDCAdapter pdcConsultarListaModalidades) {
        this.pdcConsultarListaModalidades = pdcConsultarListaModalidades;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarAgenciaGestora.
     * 
     * @return Adaptador AlterarAgenciaGestora
     */
    public IAlterarAgenciaGestoraPDCAdapter getAlterarAgenciaGestoraPDCAdapter() {
        return pdcAlterarAgenciaGestora;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarAgenciaGestora.
     * 
     * @param pdcAlterarAgenciaGestora
     *            Adaptador AlterarAgenciaGestora
     */
    public void setAlterarAgenciaGestoraPDCAdapter(IAlterarAgenciaGestoraPDCAdapter pdcAlterarAgenciaGestora) {
        this.pdcAlterarAgenciaGestora = pdcAlterarAgenciaGestora;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParticipantesOrgaoPublico.
     * 
     * @return Adaptador ListarParticipantesOrgaoPublico
     */
    public IListarParticipantesOrgaoPublicoPDCAdapter getListarParticipantesOrgaoPublicoPDCAdapter() {
        return pdcListarParticipantesOrgaoPublico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParticipantesOrgaoPublico.
     * 
     * @param pdcListarParticipantesOrgaoPublico
     *            Adaptador ListarParticipantesOrgaoPublico
     */
    public void setListarParticipantesOrgaoPublicoPDCAdapter(IListarParticipantesOrgaoPublicoPDCAdapter pdcListarParticipantesOrgaoPublico) {
        this.pdcListarParticipantesOrgaoPublico = pdcListarParticipantesOrgaoPublico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarSoliRelatorioEmpresaAcompanhada.
     * 
     * @return Adaptador ListarSoliRelatorioEmpresaAcompanhada
     */
    public IListarSoliRelatorioEmpresaAcompanhadaPDCAdapter getListarSoliRelatorioEmpresaAcompanhadaPDCAdapter() {
        return pdcListarSoliRelatorioEmpresaAcompanhada;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarSoliRelatorioEmpresaAcompanhada.
     * 
     * @param pdcListarSoliRelatorioEmpresaAcompanhada
     *            Adaptador ListarSoliRelatorioEmpresaAcompanhada
     */
    public void setListarSoliRelatorioEmpresaAcompanhadaPDCAdapter(IListarSoliRelatorioEmpresaAcompanhadaPDCAdapter pdcListarSoliRelatorioEmpresaAcompanhada) {
        this.pdcListarSoliRelatorioEmpresaAcompanhada = pdcListarSoliRelatorioEmpresaAcompanhada;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSoliRelatorioEmpresaAcompanhada.
     * 
     * @return Adaptador IncluirSoliRelatorioEmpresaAcompanhada
     */
    public IIncluirSoliRelatorioEmpresaAcompanhadaPDCAdapter getIncluirSoliRelatorioEmpresaAcompanhadaPDCAdapter() {
        return pdcIncluirSoliRelatorioEmpresaAcompanhada;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSoliRelatorioEmpresaAcompanhada.
     * 
     * @param pdcIncluirSoliRelatorioEmpresaAcompanhada
     *            Adaptador IncluirSoliRelatorioEmpresaAcompanhada
     */
    public void setIncluirSoliRelatorioEmpresaAcompanhadaPDCAdapter(IIncluirSoliRelatorioEmpresaAcompanhadaPDCAdapter pdcIncluirSoliRelatorioEmpresaAcompanhada) {
        this.pdcIncluirSoliRelatorioEmpresaAcompanhada = pdcIncluirSoliRelatorioEmpresaAcompanhada;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSoliRelatorioEmpresaAcompanhada.
     * 
     * @return Adaptador ExcluirSoliRelatorioEmpresaAcompanhada
     */
    public IExcluirSoliRelatorioEmpresaAcompanhadaPDCAdapter getExcluirSoliRelatorioEmpresaAcompanhadaPDCAdapter() {
        return pdcExcluirSoliRelatorioEmpresaAcompanhada;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSoliRelatorioEmpresaAcompanhada.
     * 
     * @param pdcExcluirSoliRelatorioEmpresaAcompanhada
     *            Adaptador ExcluirSoliRelatorioEmpresaAcompanhada
     */
    public void setExcluirSoliRelatorioEmpresaAcompanhadaPDCAdapter(IExcluirSoliRelatorioEmpresaAcompanhadaPDCAdapter pdcExcluirSoliRelatorioEmpresaAcompanhada) {
        this.pdcExcluirSoliRelatorioEmpresaAcompanhada = pdcExcluirSoliRelatorioEmpresaAcompanhada;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarEmpresaAcompanhada.
     * 
     * @return Adaptador AlterarEmpresaAcompanhada
     */
    public IAlterarEmpresaAcompanhadaPDCAdapter getAlterarEmpresaAcompanhadaPDCAdapter() {
        return pdcAlterarEmpresaAcompanhada;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarEmpresaAcompanhada.
     * 
     * @param pdcAlterarEmpresaAcompanhada
     *            Adaptador AlterarEmpresaAcompanhada
     */
    public void setAlterarEmpresaAcompanhadaPDCAdapter(IAlterarEmpresaAcompanhadaPDCAdapter pdcAlterarEmpresaAcompanhada) {
        this.pdcAlterarEmpresaAcompanhada = pdcAlterarEmpresaAcompanhada;
    }
    /**
     * M�todo invocado para obter um adaptador ConsistirDadosLogicos.
     * 
     * @return Adaptador ConsistirDadosLogicos
     */
    public IConsistirDadosLogicosPDCAdapter getConsistirDadosLogicosPDCAdapter() {
        return pdcConsistirDadosLogicos;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsistirDadosLogicos.
     * 
     * @param pdcConsistirDadosLogicos
     *            Adaptador ConsistirDadosLogicos
     */
    public void setConsistirDadosLogicosPDCAdapter(IConsistirDadosLogicosPDCAdapter pdcConsistirDadosLogicos) {
        this.pdcConsistirDadosLogicos = pdcConsistirDadosLogicos;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirEmpresaAcompanhada.
     * 
     * @return Adaptador ExcluirEmpresaAcompanhada
     */
    public IExcluirEmpresaAcompanhadaPDCAdapter getExcluirEmpresaAcompanhadaPDCAdapter() {
        return pdcExcluirEmpresaAcompanhada;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirEmpresaAcompanhada.
     * 
     * @param pdcExcluirEmpresaAcompanhada
     *            Adaptador ExcluirEmpresaAcompanhada
     */
    public void setExcluirEmpresaAcompanhadaPDCAdapter(IExcluirEmpresaAcompanhadaPDCAdapter pdcExcluirEmpresaAcompanhada) {
        this.pdcExcluirEmpresaAcompanhada = pdcExcluirEmpresaAcompanhada;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirEmpresaAcompanhada.
     * 
     * @return Adaptador IncluirEmpresaAcompanhada
     */
    public IIncluirEmpresaAcompanhadaPDCAdapter getIncluirEmpresaAcompanhadaPDCAdapter() {
        return pdcIncluirEmpresaAcompanhada;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirEmpresaAcompanhada.
     * 
     * @param pdcIncluirEmpresaAcompanhada
     *            Adaptador IncluirEmpresaAcompanhada
     */
    public void setIncluirEmpresaAcompanhadaPDCAdapter(IIncluirEmpresaAcompanhadaPDCAdapter pdcIncluirEmpresaAcompanhada) {
        this.pdcIncluirEmpresaAcompanhada = pdcIncluirEmpresaAcompanhada;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharCnpjFicticio.
     * 
     * @return Adaptador DetalharCnpjFicticio
     */
    public IDetalharCnpjFicticioPDCAdapter getDetalharCnpjFicticioPDCAdapter() {
        return pdcDetalharCnpjFicticio;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharCnpjFicticio.
     * 
     * @param pdcDetalharCnpjFicticio
     *            Adaptador DetalharCnpjFicticio
     */
    public void setDetalharCnpjFicticioPDCAdapter(IDetalharCnpjFicticioPDCAdapter pdcDetalharCnpjFicticio) {
        this.pdcDetalharCnpjFicticio = pdcDetalharCnpjFicticio;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirCnpjFicticio.
     * 
     * @return Adaptador ExcluirCnpjFicticio
     */
    public IExcluirCnpjFicticioPDCAdapter getExcluirCnpjFicticioPDCAdapter() {
        return pdcExcluirCnpjFicticio;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirCnpjFicticio.
     * 
     * @param pdcExcluirCnpjFicticio
     *            Adaptador ExcluirCnpjFicticio
     */
    public void setExcluirCnpjFicticioPDCAdapter(IExcluirCnpjFicticioPDCAdapter pdcExcluirCnpjFicticio) {
        this.pdcExcluirCnpjFicticio = pdcExcluirCnpjFicticio;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirCnpjFicticio.
     * 
     * @return Adaptador IncluirCnpjFicticio
     */
    public IIncluirCnpjFicticioPDCAdapter getIncluirCnpjFicticioPDCAdapter() {
        return pdcIncluirCnpjFicticio;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirCnpjFicticio.
     * 
     * @param pdcIncluirCnpjFicticio
     *            Adaptador IncluirCnpjFicticio
     */
    public void setIncluirCnpjFicticioPDCAdapter(IIncluirCnpjFicticioPDCAdapter pdcIncluirCnpjFicticio) {
        this.pdcIncluirCnpjFicticio = pdcIncluirCnpjFicticio;
    }
    /**
     * M�todo invocado para obter um adaptador ListarCnpjFicticio.
     * 
     * @return Adaptador ListarCnpjFicticio
     */
    public IListarCnpjFicticioPDCAdapter getListarCnpjFicticioPDCAdapter() {
        return pdcListarCnpjFicticio;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarCnpjFicticio.
     * 
     * @param pdcListarCnpjFicticio
     *            Adaptador ListarCnpjFicticio
     */
    public void setListarCnpjFicticioPDCAdapter(IListarCnpjFicticioPDCAdapter pdcListarCnpjFicticio) {
        this.pdcListarCnpjFicticio = pdcListarCnpjFicticio;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarCnpjFicticio.
     * 
     * @return Adaptador ValidarCnpjFicticio
     */
    public IValidarCnpjFicticioPDCAdapter getValidarCnpjFicticioPDCAdapter() {
        return pdcValidarCnpjFicticio;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarCnpjFicticio.
     * 
     * @param pdcValidarCnpjFicticio
     *            Adaptador ValidarCnpjFicticio
     */
    public void setValidarCnpjFicticioPDCAdapter(IValidarCnpjFicticioPDCAdapter pdcValidarCnpjFicticio) {
        this.pdcValidarCnpjFicticio = pdcValidarCnpjFicticio;
    }
    /**
     * M�todo invocado para obter um adaptador ListarModalidadesEstorno.
     * 
     * @return Adaptador ListarModalidadesEstorno
     */
    public IListarModalidadesEstornoPDCAdapter getListarModalidadesEstornoPDCAdapter() {
        return pdcListarModalidadesEstorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarModalidadesEstorno.
     * 
     * @param pdcListarModalidadesEstorno
     *            Adaptador ListarModalidadesEstorno
     */
    public void setListarModalidadesEstornoPDCAdapter(IListarModalidadesEstornoPDCAdapter pdcListarModalidadesEstorno) {
        this.pdcListarModalidadesEstorno = pdcListarModalidadesEstorno;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContratosRenegociacao.
     * 
     * @return Adaptador ListarContratosRenegociacao
     */
    public IListarContratosRenegociacaoPDCAdapter getListarContratosRenegociacaoPDCAdapter() {
        return pdcListarContratosRenegociacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContratosRenegociacao.
     * 
     * @param pdcListarContratosRenegociacao
     *            Adaptador ListarContratosRenegociacao
     */
    public void setListarContratosRenegociacaoPDCAdapter(IListarContratosRenegociacaoPDCAdapter pdcListarContratosRenegociacao) {
        this.pdcListarContratosRenegociacao = pdcListarContratosRenegociacao;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarEmpresaAcompanhada.
     * 
     * @return Adaptador ConsultarEmpresaAcompanhada
     */
    public IConsultarEmpresaAcompanhadaPDCAdapter getConsultarEmpresaAcompanhadaPDCAdapter() {
        return pdcConsultarEmpresaAcompanhada;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarEmpresaAcompanhada.
     * 
     * @param pdcConsultarEmpresaAcompanhada
     *            Adaptador ConsultarEmpresaAcompanhada
     */
    public void setConsultarEmpresaAcompanhadaPDCAdapter(IConsultarEmpresaAcompanhadaPDCAdapter pdcConsultarEmpresaAcompanhada) {
        this.pdcConsultarEmpresaAcompanhada = pdcConsultarEmpresaAcompanhada;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoArquivoRetorno.
     * 
     * @return Adaptador ListarTipoArquivoRetorno
     */
    public IListarTipoArquivoRetornoPDCAdapter getListarTipoArquivoRetornoPDCAdapter() {
        return pdcListarTipoArquivoRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoArquivoRetorno.
     * 
     * @param pdcListarTipoArquivoRetorno
     *            Adaptador ListarTipoArquivoRetorno
     */
    public void setListarTipoArquivoRetornoPDCAdapter(IListarTipoArquivoRetornoPDCAdapter pdcListarTipoArquivoRetorno) {
        this.pdcListarTipoArquivoRetorno = pdcListarTipoArquivoRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaTipoRetornoLayout.
     * 
     * @return Adaptador ConsultarListaTipoRetornoLayout
     */
    public IConsultarListaTipoRetornoLayoutPDCAdapter getConsultarListaTipoRetornoLayoutPDCAdapter() {
        return pdcConsultarListaTipoRetornoLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaTipoRetornoLayout.
     * 
     * @param pdcConsultarListaTipoRetornoLayout
     *            Adaptador ConsultarListaTipoRetornoLayout
     */
    public void setConsultarListaTipoRetornoLayoutPDCAdapter(IConsultarListaTipoRetornoLayoutPDCAdapter pdcConsultarListaTipoRetornoLayout) {
        this.pdcConsultarListaTipoRetornoLayout = pdcConsultarListaTipoRetornoLayout;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolEmiAviMovtoFavorecido.
     * 
     * @return Adaptador DetalharSolEmiAviMovtoFavorecido
     */
    public IDetalharSolEmiAviMovtoFavorecidoPDCAdapter getDetalharSolEmiAviMovtoFavorecidoPDCAdapter() {
        return pdcDetalharSolEmiAviMovtoFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolEmiAviMovtoFavorecido.
     * 
     * @param pdcDetalharSolEmiAviMovtoFavorecido
     *            Adaptador DetalharSolEmiAviMovtoFavorecido
     */
    public void setDetalharSolEmiAviMovtoFavorecidoPDCAdapter(IDetalharSolEmiAviMovtoFavorecidoPDCAdapter pdcDetalharSolEmiAviMovtoFavorecido) {
        this.pdcDetalharSolEmiAviMovtoFavorecido = pdcDetalharSolEmiAviMovtoFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolEmiAviMovtoCliePagador.
     * 
     * @return Adaptador DetalharSolEmiAviMovtoCliePagador
     */
    public IDetalharSolEmiAviMovtoCliePagadorPDCAdapter getDetalharSolEmiAviMovtoCliePagadorPDCAdapter() {
        return pdcDetalharSolEmiAviMovtoCliePagador;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolEmiAviMovtoCliePagador.
     * 
     * @param pdcDetalharSolEmiAviMovtoCliePagador
     *            Adaptador DetalharSolEmiAviMovtoCliePagador
     */
    public void setDetalharSolEmiAviMovtoCliePagadorPDCAdapter(IDetalharSolEmiAviMovtoCliePagadorPDCAdapter pdcDetalharSolEmiAviMovtoCliePagador) {
        this.pdcDetalharSolEmiAviMovtoCliePagador = pdcDetalharSolEmiAviMovtoCliePagador;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicitacaoRastreamento.
     * 
     * @return Adaptador DetalharSolicitacaoRastreamento
     */
    public IDetalharSolicitacaoRastreamentoPDCAdapter getDetalharSolicitacaoRastreamentoPDCAdapter() {
        return pdcDetalharSolicitacaoRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicitacaoRastreamento.
     * 
     * @param pdcDetalharSolicitacaoRastreamento
     *            Adaptador DetalharSolicitacaoRastreamento
     */
    public void setDetalharSolicitacaoRastreamentoPDCAdapter(IDetalharSolicitacaoRastreamentoPDCAdapter pdcDetalharSolicitacaoRastreamento) {
        this.pdcDetalharSolicitacaoRastreamento = pdcDetalharSolicitacaoRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolBaseFavorecido.
     * 
     * @return Adaptador DetalharSolBaseFavorecido
     */
    public IDetalharSolBaseFavorecidoPDCAdapter getDetalharSolBaseFavorecidoPDCAdapter() {
        return pdcDetalharSolBaseFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolBaseFavorecido.
     * 
     * @param pdcDetalharSolBaseFavorecido
     *            Adaptador DetalharSolBaseFavorecido
     */
    public void setDetalharSolBaseFavorecidoPDCAdapter(IDetalharSolBaseFavorecidoPDCAdapter pdcDetalharSolBaseFavorecido) {
        this.pdcDetalharSolBaseFavorecido = pdcDetalharSolBaseFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolBasePagto.
     * 
     * @return Adaptador DetalharSolBasePagto
     */
    public IDetalharSolBasePagtoPDCAdapter getDetalharSolBasePagtoPDCAdapter() {
        return pdcDetalharSolBasePagto;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolBasePagto.
     * 
     * @param pdcDetalharSolBasePagto
     *            Adaptador DetalharSolBasePagto
     */
    public void setDetalharSolBasePagtoPDCAdapter(IDetalharSolBasePagtoPDCAdapter pdcDetalharSolBasePagto) {
        this.pdcDetalharSolBasePagto = pdcDetalharSolBasePagto;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolBaseRetransmissao.
     * 
     * @return Adaptador DetalharSolBaseRetransmissao
     */
    public IDetalharSolBaseRetransmissaoPDCAdapter getDetalharSolBaseRetransmissaoPDCAdapter() {
        return pdcDetalharSolBaseRetransmissao;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolBaseRetransmissao.
     * 
     * @param pdcDetalharSolBaseRetransmissao
     *            Adaptador DetalharSolBaseRetransmissao
     */
    public void setDetalharSolBaseRetransmissaoPDCAdapter(IDetalharSolBaseRetransmissaoPDCAdapter pdcDetalharSolBaseRetransmissao) {
        this.pdcDetalharSolBaseRetransmissao = pdcDetalharSolBaseRetransmissao;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolBaseSistema.
     * 
     * @return Adaptador DetalharSolBaseSistema
     */
    public IDetalharSolBaseSistemaPDCAdapter getDetalharSolBaseSistemaPDCAdapter() {
        return pdcDetalharSolBaseSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolBaseSistema.
     * 
     * @param pdcDetalharSolBaseSistema
     *            Adaptador DetalharSolBaseSistema
     */
    public void setDetalharSolBaseSistemaPDCAdapter(IDetalharSolBaseSistemaPDCAdapter pdcDetalharSolBaseSistema) {
        this.pdcDetalharSolBaseSistema = pdcDetalharSolBaseSistema;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosSolicitacaoEstorno.
     * 
     * @return Adaptador ConsultarPagtosSolicitacaoEstorno
     */
    public IConsultarPagtosSolicitacaoEstornoPDCAdapter getConsultarPagtosSolicitacaoEstornoPDCAdapter() {
        return pdcConsultarPagtosSolicitacaoEstorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosSolicitacaoEstorno.
     * 
     * @param pdcConsultarPagtosSolicitacaoEstorno
     *            Adaptador ConsultarPagtosSolicitacaoEstorno
     */
    public void setConsultarPagtosSolicitacaoEstornoPDCAdapter(IConsultarPagtosSolicitacaoEstornoPDCAdapter pdcConsultarPagtosSolicitacaoEstorno) {
        this.pdcConsultarPagtosSolicitacaoEstorno = pdcConsultarPagtosSolicitacaoEstorno;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarServicoComposto.
     * 
     * @return Adaptador AlterarServicoComposto
     */
    public IAlterarServicoCompostoPDCAdapter getAlterarServicoCompostoPDCAdapter() {
        return pdcAlterarServicoComposto;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarServicoComposto.
     * 
     * @param pdcAlterarServicoComposto
     *            Adaptador AlterarServicoComposto
     */
    public void setAlterarServicoCompostoPDCAdapter(IAlterarServicoCompostoPDCAdapter pdcAlterarServicoComposto) {
        this.pdcAlterarServicoComposto = pdcAlterarServicoComposto;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharServicoComposto.
     * 
     * @return Adaptador DetalharServicoComposto
     */
    public IDetalharServicoCompostoPDCAdapter getDetalharServicoCompostoPDCAdapter() {
        return pdcDetalharServicoComposto;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharServicoComposto.
     * 
     * @param pdcDetalharServicoComposto
     *            Adaptador DetalharServicoComposto
     */
    public void setDetalharServicoCompostoPDCAdapter(IDetalharServicoCompostoPDCAdapter pdcDetalharServicoComposto) {
        this.pdcDetalharServicoComposto = pdcDetalharServicoComposto;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirServicoComposto.
     * 
     * @return Adaptador ExcluirServicoComposto
     */
    public IExcluirServicoCompostoPDCAdapter getExcluirServicoCompostoPDCAdapter() {
        return pdcExcluirServicoComposto;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirServicoComposto.
     * 
     * @param pdcExcluirServicoComposto
     *            Adaptador ExcluirServicoComposto
     */
    public void setExcluirServicoCompostoPDCAdapter(IExcluirServicoCompostoPDCAdapter pdcExcluirServicoComposto) {
        this.pdcExcluirServicoComposto = pdcExcluirServicoComposto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirServicoComposto.
     * 
     * @return Adaptador IncluirServicoComposto
     */
    public IIncluirServicoCompostoPDCAdapter getIncluirServicoCompostoPDCAdapter() {
        return pdcIncluirServicoComposto;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirServicoComposto.
     * 
     * @param pdcIncluirServicoComposto
     *            Adaptador IncluirServicoComposto
     */
    public void setIncluirServicoCompostoPDCAdapter(IIncluirServicoCompostoPDCAdapter pdcIncluirServicoComposto) {
        this.pdcIncluirServicoComposto = pdcIncluirServicoComposto;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoComposto.
     * 
     * @return Adaptador ListarServicoComposto
     */
    public IListarServicoCompostoPDCAdapter getListarServicoCompostoPDCAdapter() {
        return pdcListarServicoComposto;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoComposto.
     * 
     * @param pdcListarServicoComposto
     *            Adaptador ListarServicoComposto
     */
    public void setListarServicoCompostoPDCAdapter(IListarServicoCompostoPDCAdapter pdcListarServicoComposto) {
        this.pdcListarServicoComposto = pdcListarServicoComposto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolEmiAviMovtoFavorecido.
     * 
     * @return Adaptador IncluirSolEmiAviMovtoFavorecido
     */
    public IIncluirSolEmiAviMovtoFavorecidoPDCAdapter getIncluirSolEmiAviMovtoFavorecidoPDCAdapter() {
        return pdcIncluirSolEmiAviMovtoFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolEmiAviMovtoFavorecido.
     * 
     * @param pdcIncluirSolEmiAviMovtoFavorecido
     *            Adaptador IncluirSolEmiAviMovtoFavorecido
     */
    public void setIncluirSolEmiAviMovtoFavorecidoPDCAdapter(IIncluirSolEmiAviMovtoFavorecidoPDCAdapter pdcIncluirSolEmiAviMovtoFavorecido) {
        this.pdcIncluirSolEmiAviMovtoFavorecido = pdcIncluirSolEmiAviMovtoFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicitacaoRastreamento.
     * 
     * @return Adaptador IncluirSolicitacaoRastreamento
     */
    public IIncluirSolicitacaoRastreamentoPDCAdapter getIncluirSolicitacaoRastreamentoPDCAdapter() {
        return pdcIncluirSolicitacaoRastreamento;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicitacaoRastreamento.
     * 
     * @param pdcIncluirSolicitacaoRastreamento
     *            Adaptador IncluirSolicitacaoRastreamento
     */
    public void setIncluirSolicitacaoRastreamentoPDCAdapter(IIncluirSolicitacaoRastreamentoPDCAdapter pdcIncluirSolicitacaoRastreamento) {
        this.pdcIncluirSolicitacaoRastreamento = pdcIncluirSolicitacaoRastreamento;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolBaseSistema.
     * 
     * @return Adaptador IncluirSolBaseSistema
     */
    public IIncluirSolBaseSistemaPDCAdapter getIncluirSolBaseSistemaPDCAdapter() {
        return pdcIncluirSolBaseSistema;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolBaseSistema.
     * 
     * @param pdcIncluirSolBaseSistema
     *            Adaptador IncluirSolBaseSistema
     */
    public void setIncluirSolBaseSistemaPDCAdapter(IIncluirSolBaseSistemaPDCAdapter pdcIncluirSolBaseSistema) {
        this.pdcIncluirSolBaseSistema = pdcIncluirSolBaseSistema;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolBaseFavorecido.
     * 
     * @return Adaptador IncluirSolBaseFavorecido
     */
    public IIncluirSolBaseFavorecidoPDCAdapter getIncluirSolBaseFavorecidoPDCAdapter() {
        return pdcIncluirSolBaseFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolBaseFavorecido.
     * 
     * @param pdcIncluirSolBaseFavorecido
     *            Adaptador IncluirSolBaseFavorecido
     */
    public void setIncluirSolBaseFavorecidoPDCAdapter(IIncluirSolBaseFavorecidoPDCAdapter pdcIncluirSolBaseFavorecido) {
        this.pdcIncluirSolBaseFavorecido = pdcIncluirSolBaseFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarAgenciaOpeTarifaPadrao.
     * 
     * @return Adaptador ConsultarAgenciaOpeTarifaPadrao
     */
    public IConsultarAgenciaOpeTarifaPadraoPDCAdapter getConsultarAgenciaOpeTarifaPadraoPDCAdapter() {
        return pdcConsultarAgenciaOpeTarifaPadrao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarAgenciaOpeTarifaPadrao.
     * 
     * @param pdcConsultarAgenciaOpeTarifaPadrao
     *            Adaptador ConsultarAgenciaOpeTarifaPadrao
     */
    public void setConsultarAgenciaOpeTarifaPadraoPDCAdapter(IConsultarAgenciaOpeTarifaPadraoPDCAdapter pdcConsultarAgenciaOpeTarifaPadrao) {
        this.pdcConsultarAgenciaOpeTarifaPadrao = pdcConsultarAgenciaOpeTarifaPadrao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolBasePagto.
     * 
     * @return Adaptador IncluirSolBasePagto
     */
    public IIncluirSolBasePagtoPDCAdapter getIncluirSolBasePagtoPDCAdapter() {
        return pdcIncluirSolBasePagto;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolBasePagto.
     * 
     * @param pdcIncluirSolBasePagto
     *            Adaptador IncluirSolBasePagto
     */
    public void setIncluirSolBasePagtoPDCAdapter(IIncluirSolBasePagtoPDCAdapter pdcIncluirSolBasePagto) {
        this.pdcIncluirSolBasePagto = pdcIncluirSolBasePagto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolBaseRetransmissao.
     * 
     * @return Adaptador IncluirSolBaseRetransmissao
     */
    public IIncluirSolBaseRetransmissaoPDCAdapter getIncluirSolBaseRetransmissaoPDCAdapter() {
        return pdcIncluirSolBaseRetransmissao;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolBaseRetransmissao.
     * 
     * @param pdcIncluirSolBaseRetransmissao
     *            Adaptador IncluirSolBaseRetransmissao
     */
    public void setIncluirSolBaseRetransmissaoPDCAdapter(IIncluirSolBaseRetransmissaoPDCAdapter pdcIncluirSolBaseRetransmissao) {
        this.pdcIncluirSolBaseRetransmissao = pdcIncluirSolBaseRetransmissao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarArqRetransmissao.
     * 
     * @return Adaptador ListarArqRetransmissao
     */
    public IListarArqRetransmissaoPDCAdapter getListarArqRetransmissaoPDCAdapter() {
        return pdcListarArqRetransmissao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarArqRetransmissao.
     * 
     * @param pdcListarArqRetransmissao
     *            Adaptador ListarArqRetransmissao
     */
    public void setListarArqRetransmissaoPDCAdapter(IListarArqRetransmissaoPDCAdapter pdcListarArqRetransmissao) {
        this.pdcListarArqRetransmissao = pdcListarArqRetransmissao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolEmiAviMovtoCliePagador.
     * 
     * @return Adaptador IncluirSolEmiAviMovtoCliePagador
     */
    public IIncluirSolEmiAviMovtoCliePagadorPDCAdapter getIncluirSolEmiAviMovtoCliePagadorPDCAdapter() {
        return pdcIncluirSolEmiAviMovtoCliePagador;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolEmiAviMovtoCliePagador.
     * 
     * @param pdcIncluirSolEmiAviMovtoCliePagador
     *            Adaptador IncluirSolEmiAviMovtoCliePagador
     */
    public void setIncluirSolEmiAviMovtoCliePagadorPDCAdapter(IIncluirSolEmiAviMovtoCliePagadorPDCAdapter pdcIncluirSolEmiAviMovtoCliePagador) {
        this.pdcIncluirSolEmiAviMovtoCliePagador = pdcIncluirSolEmiAviMovtoCliePagador;
    }
    /**
     * M�todo invocado para obter um adaptador ListarOperacaoContaDebTarifa.
     * 
     * @return Adaptador ListarOperacaoContaDebTarifa
     */
    public IListarOperacaoContaDebTarifaPDCAdapter getListarOperacaoContaDebTarifaPDCAdapter() {
        return pdcListarOperacaoContaDebTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarOperacaoContaDebTarifa.
     * 
     * @param pdcListarOperacaoContaDebTarifa
     *            Adaptador ListarOperacaoContaDebTarifa
     */
    public void setListarOperacaoContaDebTarifaPDCAdapter(IListarOperacaoContaDebTarifaPDCAdapter pdcListarOperacaoContaDebTarifa) {
        this.pdcListarOperacaoContaDebTarifa = pdcListarOperacaoContaDebTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistoricoDebTarifa.
     * 
     * @return Adaptador ListarHistoricoDebTarifa
     */
    public IListarHistoricoDebTarifaPDCAdapter getListarHistoricoDebTarifaPDCAdapter() {
        return pdcListarHistoricoDebTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistoricoDebTarifa.
     * 
     * @param pdcListarHistoricoDebTarifa
     *            Adaptador ListarHistoricoDebTarifa
     */
    public void setListarHistoricoDebTarifaPDCAdapter(IListarHistoricoDebTarifaPDCAdapter pdcListarHistoricoDebTarifa) {
        this.pdcListarHistoricoDebTarifa = pdcListarHistoricoDebTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContaDebTarifa.
     * 
     * @return Adaptador ListarContaDebTarifa
     */
    public IListarContaDebTarifaPDCAdapter getListarContaDebTarifaPDCAdapter() {
        return pdcListarContaDebTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContaDebTarifa.
     * 
     * @param pdcListarContaDebTarifa
     *            Adaptador ListarContaDebTarifa
     */
    public void setListarContaDebTarifaPDCAdapter(IListarContaDebTarifaPDCAdapter pdcListarContaDebTarifa) {
        this.pdcListarContaDebTarifa = pdcListarContaDebTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistoricoDebTarifa.
     * 
     * @return Adaptador DetalharHistoricoDebTarifa
     */
    public IDetalharHistoricoDebTarifaPDCAdapter getDetalharHistoricoDebTarifaPDCAdapter() {
        return pdcDetalharHistoricoDebTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistoricoDebTarifa.
     * 
     * @param pdcDetalharHistoricoDebTarifa
     *            Adaptador DetalharHistoricoDebTarifa
     */
    public void setDetalharHistoricoDebTarifaPDCAdapter(IDetalharHistoricoDebTarifaPDCAdapter pdcDetalharHistoricoDebTarifa) {
        this.pdcDetalharHistoricoDebTarifa = pdcDetalharHistoricoDebTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarContaDebTarifa.
     * 
     * @return Adaptador ConsultarContaDebTarifa
     */
    public IConsultarContaDebTarifaPDCAdapter getConsultarContaDebTarifaPDCAdapter() {
        return pdcConsultarContaDebTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarContaDebTarifa.
     * 
     * @param pdcConsultarContaDebTarifa
     *            Adaptador ConsultarContaDebTarifa
     */
    public void setConsultarContaDebTarifaPDCAdapter(IConsultarContaDebTarifaPDCAdapter pdcConsultarContaDebTarifa) {
        this.pdcConsultarContaDebTarifa = pdcConsultarContaDebTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicExclusaoLotePgto.
     * 
     * @return Adaptador DetalharSolicExclusaoLotePgto
     */
    public IDetalharSolicExclusaoLotePgtoPDCAdapter getDetalharSolicExclusaoLotePgtoPDCAdapter() {
        return pdcDetalharSolicExclusaoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicExclusaoLotePgto.
     * 
     * @param pdcDetalharSolicExclusaoLotePgto
     *            Adaptador DetalharSolicExclusaoLotePgto
     */
    public void setDetalharSolicExclusaoLotePgtoPDCAdapter(IDetalharSolicExclusaoLotePgtoPDCAdapter pdcDetalharSolicExclusaoLotePgto) {
        this.pdcDetalharSolicExclusaoLotePgto = pdcDetalharSolicExclusaoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicExclusaoLotePgto.
     * 
     * @return Adaptador ExcluirSolicExclusaoLotePgto
     */
    public IExcluirSolicExclusaoLotePgtoPDCAdapter getExcluirSolicExclusaoLotePgtoPDCAdapter() {
        return pdcExcluirSolicExclusaoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicExclusaoLotePgto.
     * 
     * @param pdcExcluirSolicExclusaoLotePgto
     *            Adaptador ExcluirSolicExclusaoLotePgto
     */
    public void setExcluirSolicExclusaoLotePgtoPDCAdapter(IExcluirSolicExclusaoLotePgtoPDCAdapter pdcExcluirSolicExclusaoLotePgto) {
        this.pdcExcluirSolicExclusaoLotePgto = pdcExcluirSolicExclusaoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicExclusaoLotePgto.
     * 
     * @return Adaptador IncluirSolicExclusaoLotePgto
     */
    public IIncluirSolicExclusaoLotePgtoPDCAdapter getIncluirSolicExclusaoLotePgtoPDCAdapter() {
        return pdcIncluirSolicExclusaoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicExclusaoLotePgto.
     * 
     * @param pdcIncluirSolicExclusaoLotePgto
     *            Adaptador IncluirSolicExclusaoLotePgto
     */
    public void setIncluirSolicExclusaoLotePgtoPDCAdapter(IIncluirSolicExclusaoLotePgtoPDCAdapter pdcIncluirSolicExclusaoLotePgto) {
        this.pdcIncluirSolicExclusaoLotePgto = pdcIncluirSolicExclusaoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarOcorrenciasSolicLote.
     * 
     * @return Adaptador ConsultarOcorrenciasSolicLote
     */
    public IConsultarOcorrenciasSolicLotePDCAdapter getConsultarOcorrenciasSolicLotePDCAdapter() {
        return pdcConsultarOcorrenciasSolicLote;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarOcorrenciasSolicLote.
     * 
     * @param pdcConsultarOcorrenciasSolicLote
     *            Adaptador ConsultarOcorrenciasSolicLote
     */
    public void setConsultarOcorrenciasSolicLotePDCAdapter(IConsultarOcorrenciasSolicLotePDCAdapter pdcConsultarOcorrenciasSolicLote) {
        this.pdcConsultarOcorrenciasSolicLote = pdcConsultarOcorrenciasSolicLote;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarQtdeValorPgtoPrevistoSolic.
     * 
     * @return Adaptador ConsultarQtdeValorPgtoPrevistoSolic
     */
    public IConsultarQtdeValorPgtoPrevistoSolicPDCAdapter getConsultarQtdeValorPgtoPrevistoSolicPDCAdapter() {
        return pdcConsultarQtdeValorPgtoPrevistoSolic;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarQtdeValorPgtoPrevistoSolic.
     * 
     * @param pdcConsultarQtdeValorPgtoPrevistoSolic
     *            Adaptador ConsultarQtdeValorPgtoPrevistoSolic
     */
    public void setConsultarQtdeValorPgtoPrevistoSolicPDCAdapter(IConsultarQtdeValorPgtoPrevistoSolicPDCAdapter pdcConsultarQtdeValorPgtoPrevistoSolic) {
        this.pdcConsultarQtdeValorPgtoPrevistoSolic = pdcConsultarQtdeValorPgtoPrevistoSolic;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirContaDebTarifa.
     * 
     * @return Adaptador IncluirContaDebTarifa
     */
    public IIncluirContaDebTarifaPDCAdapter getIncluirContaDebTarifaPDCAdapter() {
        return pdcIncluirContaDebTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirContaDebTarifa.
     * 
     * @param pdcIncluirContaDebTarifa
     *            Adaptador IncluirContaDebTarifa
     */
    public void setIncluirContaDebTarifaPDCAdapter(IIncluirContaDebTarifaPDCAdapter pdcIncluirContaDebTarifa) {
        this.pdcIncluirContaDebTarifa = pdcIncluirContaDebTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicLiberacaoLotePgto.
     * 
     * @return Adaptador IncluirSolicLiberacaoLotePgto
     */
    public IIncluirSolicLiberacaoLotePgtoPDCAdapter getIncluirSolicLiberacaoLotePgtoPDCAdapter() {
        return pdcIncluirSolicLiberacaoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicLiberacaoLotePgto.
     * 
     * @param pdcIncluirSolicLiberacaoLotePgto
     *            Adaptador IncluirSolicLiberacaoLotePgto
     */
    public void setIncluirSolicLiberacaoLotePgtoPDCAdapter(IIncluirSolicLiberacaoLotePgtoPDCAdapter pdcIncluirSolicLiberacaoLotePgto) {
        this.pdcIncluirSolicLiberacaoLotePgto = pdcIncluirSolicLiberacaoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicLiberacaoLotePgto.
     * 
     * @return Adaptador DetalharSolicLiberacaoLotePgto
     */
    public IDetalharSolicLiberacaoLotePgtoPDCAdapter getDetalharSolicLiberacaoLotePgtoPDCAdapter() {
        return pdcDetalharSolicLiberacaoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicLiberacaoLotePgto.
     * 
     * @param pdcDetalharSolicLiberacaoLotePgto
     *            Adaptador DetalharSolicLiberacaoLotePgto
     */
    public void setDetalharSolicLiberacaoLotePgtoPDCAdapter(IDetalharSolicLiberacaoLotePgtoPDCAdapter pdcDetalharSolicLiberacaoLotePgto) {
        this.pdcDetalharSolicLiberacaoLotePgto = pdcDetalharSolicLiberacaoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicLiberacaoLotePgto.
     * 
     * @return Adaptador ExcluirSolicLiberacaoLotePgto
     */
    public IExcluirSolicLiberacaoLotePgtoPDCAdapter getExcluirSolicLiberacaoLotePgtoPDCAdapter() {
        return pdcExcluirSolicLiberacaoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicLiberacaoLotePgto.
     * 
     * @param pdcExcluirSolicLiberacaoLotePgto
     *            Adaptador ExcluirSolicLiberacaoLotePgto
     */
    public void setExcluirSolicLiberacaoLotePgtoPDCAdapter(IExcluirSolicLiberacaoLotePgtoPDCAdapter pdcExcluirSolicLiberacaoLotePgto) {
        this.pdcExcluirSolicLiberacaoLotePgto = pdcExcluirSolicLiberacaoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicCancelamentoLotePgto.
     * 
     * @return Adaptador IncluirSolicCancelamentoLotePgto
     */
    public IIncluirSolicCancelamentoLotePgtoPDCAdapter getIncluirSolicCancelamentoLotePgtoPDCAdapter() {
        return pdcIncluirSolicCancelamentoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicCancelamentoLotePgto.
     * 
     * @param pdcIncluirSolicCancelamentoLotePgto
     *            Adaptador IncluirSolicCancelamentoLotePgto
     */
    public void setIncluirSolicCancelamentoLotePgtoPDCAdapter(IIncluirSolicCancelamentoLotePgtoPDCAdapter pdcIncluirSolicCancelamentoLotePgto) {
        this.pdcIncluirSolicCancelamentoLotePgto = pdcIncluirSolicCancelamentoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicCancelamentoLotePgto.
     * 
     * @return Adaptador DetalharSolicCancelamentoLotePgto
     */
    public IDetalharSolicCancelamentoLotePgtoPDCAdapter getDetalharSolicCancelamentoLotePgtoPDCAdapter() {
        return pdcDetalharSolicCancelamentoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicCancelamentoLotePgto.
     * 
     * @param pdcDetalharSolicCancelamentoLotePgto
     *            Adaptador DetalharSolicCancelamentoLotePgto
     */
    public void setDetalharSolicCancelamentoLotePgtoPDCAdapter(IDetalharSolicCancelamentoLotePgtoPDCAdapter pdcDetalharSolicCancelamentoLotePgto) {
        this.pdcDetalharSolicCancelamentoLotePgto = pdcDetalharSolicCancelamentoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicCancelamentoLotePgto.
     * 
     * @return Adaptador ExcluirSolicCancelamentoLotePgto
     */
    public IExcluirSolicCancelamentoLotePgtoPDCAdapter getExcluirSolicCancelamentoLotePgtoPDCAdapter() {
        return pdcExcluirSolicCancelamentoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicCancelamentoLotePgto.
     * 
     * @param pdcExcluirSolicCancelamentoLotePgto
     *            Adaptador ExcluirSolicCancelamentoLotePgto
     */
    public void setExcluirSolicCancelamentoLotePgtoPDCAdapter(IExcluirSolicCancelamentoLotePgtoPDCAdapter pdcExcluirSolicCancelamentoLotePgto) {
        this.pdcExcluirSolicCancelamentoLotePgto = pdcExcluirSolicCancelamentoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContasPossiveisInclusao.
     * 
     * @return Adaptador ListarContasPossiveisInclusao
     */
    public IListarContasPossiveisInclusaoPDCAdapter getListarContasPossiveisInclusaoPDCAdapter() {
        return pdcListarContasPossiveisInclusao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContasPossiveisInclusao.
     * 
     * @param pdcListarContasPossiveisInclusao
     *            Adaptador ListarContasPossiveisInclusao
     */
    public void setListarContasPossiveisInclusaoPDCAdapter(IListarContasPossiveisInclusaoPDCAdapter pdcListarContasPossiveisInclusao) {
        this.pdcListarContasPossiveisInclusao = pdcListarContasPossiveisInclusao;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirContratoPgit.
     * 
     * @return Adaptador IncluirContratoPgit
     */
    public IIncluirContratoPgitPDCAdapter getIncluirContratoPgitPDCAdapter() {
        return pdcIncluirContratoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirContratoPgit.
     * 
     * @param pdcIncluirContratoPgit
     *            Adaptador IncluirContratoPgit
     */
    public void setIncluirContratoPgitPDCAdapter(IIncluirContratoPgitPDCAdapter pdcIncluirContratoPgit) {
        this.pdcIncluirContratoPgit = pdcIncluirContratoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagamentosConsolidados.
     * 
     * @return Adaptador ConsultarPagamentosConsolidados
     */
    public IConsultarPagamentosConsolidadosPDCAdapter getConsultarPagamentosConsolidadosPDCAdapter() {
        return pdcConsultarPagamentosConsolidados;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagamentosConsolidados.
     * 
     * @param pdcConsultarPagamentosConsolidados
     *            Adaptador ConsultarPagamentosConsolidados
     */
    public void setConsultarPagamentosConsolidadosPDCAdapter(IConsultarPagamentosConsolidadosPDCAdapter pdcConsultarPagamentosConsolidados) {
        this.pdcConsultarPagamentosConsolidados = pdcConsultarPagamentosConsolidados;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarConfContaContrato.
     * 
     * @return Adaptador AlterarConfContaContrato
     */
    public IAlterarConfContaContratoPDCAdapter getAlterarConfContaContratoPDCAdapter() {
        return pdcAlterarConfContaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarConfContaContrato.
     * 
     * @param pdcAlterarConfContaContrato
     *            Adaptador AlterarConfContaContrato
     */
    public void setAlterarConfContaContratoPDCAdapter(IAlterarConfContaContratoPDCAdapter pdcAlterarConfContaContrato) {
        this.pdcAlterarConfContaContrato = pdcAlterarConfContaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLimiteIntraPgit.
     * 
     * @return Adaptador ConsultarLimiteIntraPgit
     */
    public IConsultarLimiteIntraPgitPDCAdapter getConsultarLimiteIntraPgitPDCAdapter() {
        return pdcConsultarLimiteIntraPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLimiteIntraPgit.
     * 
     * @param pdcConsultarLimiteIntraPgit
     *            Adaptador ConsultarLimiteIntraPgit
     */
    public void setConsultarLimiteIntraPgitPDCAdapter(IConsultarLimiteIntraPgitPDCAdapter pdcConsultarLimiteIntraPgit) {
        this.pdcConsultarLimiteIntraPgit = pdcConsultarLimiteIntraPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarContrato.
     * 
     * @return Adaptador ConsultarContrato
     */
    public IConsultarContratoPDCAdapter getConsultarContratoPDCAdapter() {
        return pdcConsultarContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarContrato.
     * 
     * @param pdcConsultarContrato
     *            Adaptador ConsultarContrato
     */
    public void setConsultarContratoPDCAdapter(IConsultarContratoPDCAdapter pdcConsultarContrato) {
        this.pdcConsultarContrato = pdcConsultarContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicitacaoMudancaAmbPartc.
     * 
     * @return Adaptador ConsultarSolicitacaoMudancaAmbPartc
     */
    public IConsultarSolicitacaoMudancaAmbPartcPDCAdapter getConsultarSolicitacaoMudancaAmbPartcPDCAdapter() {
        return pdcConsultarSolicitacaoMudancaAmbPartc;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicitacaoMudancaAmbPartc.
     * 
     * @param pdcConsultarSolicitacaoMudancaAmbPartc
     *            Adaptador ConsultarSolicitacaoMudancaAmbPartc
     */
    public void setConsultarSolicitacaoMudancaAmbPartcPDCAdapter(IConsultarSolicitacaoMudancaAmbPartcPDCAdapter pdcConsultarSolicitacaoMudancaAmbPartc) {
        this.pdcConsultarSolicitacaoMudancaAmbPartc = pdcConsultarSolicitacaoMudancaAmbPartc;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAmbiente.
     * 
     * @return Adaptador ListarAmbiente
     */
    public IListarAmbientePDCAdapter getListarAmbientePDCAdapter() {
        return pdcListarAmbiente;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAmbiente.
     * 
     * @param pdcListarAmbiente
     *            Adaptador ListarAmbiente
     */
    public void setListarAmbientePDCAdapter(IListarAmbientePDCAdapter pdcListarAmbiente) {
        this.pdcListarAmbiente = pdcListarAmbiente;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoTituloBradesco.
     * 
     * @return Adaptador DetalharPagtoTituloBradesco
     */
    public IDetalharPagtoTituloBradescoPDCAdapter getDetalharPagtoTituloBradescoPDCAdapter() {
        return pdcDetalharPagtoTituloBradesco;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoTituloBradesco.
     * 
     * @param pdcDetalharPagtoTituloBradesco
     *            Adaptador DetalharPagtoTituloBradesco
     */
    public void setDetalharPagtoTituloBradescoPDCAdapter(IDetalharPagtoTituloBradescoPDCAdapter pdcDetalharPagtoTituloBradesco) {
        this.pdcDetalharPagtoTituloBradesco = pdcDetalharPagtoTituloBradesco;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoTituloOutrosBancos.
     * 
     * @return Adaptador DetalharPagtoTituloOutrosBancos
     */
    public IDetalharPagtoTituloOutrosBancosPDCAdapter getDetalharPagtoTituloOutrosBancosPDCAdapter() {
        return pdcDetalharPagtoTituloOutrosBancos;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoTituloOutrosBancos.
     * 
     * @param pdcDetalharPagtoTituloOutrosBancos
     *            Adaptador DetalharPagtoTituloOutrosBancos
     */
    public void setDetalharPagtoTituloOutrosBancosPDCAdapter(IDetalharPagtoTituloOutrosBancosPDCAdapter pdcDetalharPagtoTituloOutrosBancos) {
        this.pdcDetalharPagtoTituloOutrosBancos = pdcDetalharPagtoTituloOutrosBancos;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoDARF.
     * 
     * @return Adaptador DetalharPagtoDARF
     */
    public IDetalharPagtoDARFPDCAdapter getDetalharPagtoDARFPDCAdapter() {
        return pdcDetalharPagtoDARF;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoDARF.
     * 
     * @param pdcDetalharPagtoDARF
     *            Adaptador DetalharPagtoDARF
     */
    public void setDetalharPagtoDARFPDCAdapter(IDetalharPagtoDARFPDCAdapter pdcDetalharPagtoDARF) {
        this.pdcDetalharPagtoDARF = pdcDetalharPagtoDARF;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoGARE.
     * 
     * @return Adaptador DetalharPagtoGARE
     */
    public IDetalharPagtoGAREPDCAdapter getDetalharPagtoGAREPDCAdapter() {
        return pdcDetalharPagtoGARE;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoGARE.
     * 
     * @param pdcDetalharPagtoGARE
     *            Adaptador DetalharPagtoGARE
     */
    public void setDetalharPagtoGAREPDCAdapter(IDetalharPagtoGAREPDCAdapter pdcDetalharPagtoGARE) {
        this.pdcDetalharPagtoGARE = pdcDetalharPagtoGARE;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoGPS.
     * 
     * @return Adaptador DetalharPagtoGPS
     */
    public IDetalharPagtoGPSPDCAdapter getDetalharPagtoGPSPDCAdapter() {
        return pdcDetalharPagtoGPS;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoGPS.
     * 
     * @param pdcDetalharPagtoGPS
     *            Adaptador DetalharPagtoGPS
     */
    public void setDetalharPagtoGPSPDCAdapter(IDetalharPagtoGPSPDCAdapter pdcDetalharPagtoGPS) {
        this.pdcDetalharPagtoGPS = pdcDetalharPagtoGPS;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoCodigoBarras.
     * 
     * @return Adaptador DetalharPagtoCodigoBarras
     */
    public IDetalharPagtoCodigoBarrasPDCAdapter getDetalharPagtoCodigoBarrasPDCAdapter() {
        return pdcDetalharPagtoCodigoBarras;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoCodigoBarras.
     * 
     * @param pdcDetalharPagtoCodigoBarras
     *            Adaptador DetalharPagtoCodigoBarras
     */
    public void setDetalharPagtoCodigoBarrasPDCAdapter(IDetalharPagtoCodigoBarrasPDCAdapter pdcDetalharPagtoCodigoBarras) {
        this.pdcDetalharPagtoCodigoBarras = pdcDetalharPagtoCodigoBarras;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoOrdemPagto.
     * 
     * @return Adaptador DetalharManPagtoOrdemPagto
     */
    public IDetalharManPagtoOrdemPagtoPDCAdapter getDetalharManPagtoOrdemPagtoPDCAdapter() {
        return pdcDetalharManPagtoOrdemPagto;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoOrdemPagto.
     * 
     * @param pdcDetalharManPagtoOrdemPagto
     *            Adaptador DetalharManPagtoOrdemPagto
     */
    public void setDetalharManPagtoOrdemPagtoPDCAdapter(IDetalharManPagtoOrdemPagtoPDCAdapter pdcDetalharManPagtoOrdemPagto) {
        this.pdcDetalharManPagtoOrdemPagto = pdcDetalharManPagtoOrdemPagto;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoTituloBradesco.
     * 
     * @return Adaptador DetalharManPagtoTituloBradesco
     */
    public IDetalharManPagtoTituloBradescoPDCAdapter getDetalharManPagtoTituloBradescoPDCAdapter() {
        return pdcDetalharManPagtoTituloBradesco;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoTituloBradesco.
     * 
     * @param pdcDetalharManPagtoTituloBradesco
     *            Adaptador DetalharManPagtoTituloBradesco
     */
    public void setDetalharManPagtoTituloBradescoPDCAdapter(IDetalharManPagtoTituloBradescoPDCAdapter pdcDetalharManPagtoTituloBradesco) {
        this.pdcDetalharManPagtoTituloBradesco = pdcDetalharManPagtoTituloBradesco;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoTituloOutrosBancos.
     * 
     * @return Adaptador DetalharManPagtoTituloOutrosBancos
     */
    public IDetalharManPagtoTituloOutrosBancosPDCAdapter getDetalharManPagtoTituloOutrosBancosPDCAdapter() {
        return pdcDetalharManPagtoTituloOutrosBancos;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoTituloOutrosBancos.
     * 
     * @param pdcDetalharManPagtoTituloOutrosBancos
     *            Adaptador DetalharManPagtoTituloOutrosBancos
     */
    public void setDetalharManPagtoTituloOutrosBancosPDCAdapter(IDetalharManPagtoTituloOutrosBancosPDCAdapter pdcDetalharManPagtoTituloOutrosBancos) {
        this.pdcDetalharManPagtoTituloOutrosBancos = pdcDetalharManPagtoTituloOutrosBancos;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoDARF.
     * 
     * @return Adaptador DetalharManPagtoDARF
     */
    public IDetalharManPagtoDARFPDCAdapter getDetalharManPagtoDARFPDCAdapter() {
        return pdcDetalharManPagtoDARF;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoDARF.
     * 
     * @param pdcDetalharManPagtoDARF
     *            Adaptador DetalharManPagtoDARF
     */
    public void setDetalharManPagtoDARFPDCAdapter(IDetalharManPagtoDARFPDCAdapter pdcDetalharManPagtoDARF) {
        this.pdcDetalharManPagtoDARF = pdcDetalharManPagtoDARF;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoGARE.
     * 
     * @return Adaptador DetalharManPagtoGARE
     */
    public IDetalharManPagtoGAREPDCAdapter getDetalharManPagtoGAREPDCAdapter() {
        return pdcDetalharManPagtoGARE;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoGARE.
     * 
     * @param pdcDetalharManPagtoGARE
     *            Adaptador DetalharManPagtoGARE
     */
    public void setDetalharManPagtoGAREPDCAdapter(IDetalharManPagtoGAREPDCAdapter pdcDetalharManPagtoGARE) {
        this.pdcDetalharManPagtoGARE = pdcDetalharManPagtoGARE;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoGPS.
     * 
     * @return Adaptador DetalharManPagtoGPS
     */
    public IDetalharManPagtoGPSPDCAdapter getDetalharManPagtoGPSPDCAdapter() {
        return pdcDetalharManPagtoGPS;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoGPS.
     * 
     * @param pdcDetalharManPagtoGPS
     *            Adaptador DetalharManPagtoGPS
     */
    public void setDetalharManPagtoGPSPDCAdapter(IDetalharManPagtoGPSPDCAdapter pdcDetalharManPagtoGPS) {
        this.pdcDetalharManPagtoGPS = pdcDetalharManPagtoGPS;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoCodigoBarras.
     * 
     * @return Adaptador DetalharManPagtoCodigoBarras
     */
    public IDetalharManPagtoCodigoBarrasPDCAdapter getDetalharManPagtoCodigoBarrasPDCAdapter() {
        return pdcDetalharManPagtoCodigoBarras;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoCodigoBarras.
     * 
     * @param pdcDetalharManPagtoCodigoBarras
     *            Adaptador DetalharManPagtoCodigoBarras
     */
    public void setDetalharManPagtoCodigoBarrasPDCAdapter(IDetalharManPagtoCodigoBarrasPDCAdapter pdcDetalharManPagtoCodigoBarras) {
        this.pdcDetalharManPagtoCodigoBarras = pdcDetalharManPagtoCodigoBarras;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicAntProcLotePgtoAgda.
     * 
     * @return Adaptador DetalharSolicAntProcLotePgtoAgda
     */
    public IDetalharSolicAntProcLotePgtoAgdaPDCAdapter getDetalharSolicAntProcLotePgtoAgdaPDCAdapter() {
        return pdcDetalharSolicAntProcLotePgtoAgda;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicAntProcLotePgtoAgda.
     * 
     * @param pdcDetalharSolicAntProcLotePgtoAgda
     *            Adaptador DetalharSolicAntProcLotePgtoAgda
     */
    public void setDetalharSolicAntProcLotePgtoAgdaPDCAdapter(IDetalharSolicAntProcLotePgtoAgdaPDCAdapter pdcDetalharSolicAntProcLotePgtoAgda) {
        this.pdcDetalharSolicAntProcLotePgtoAgda = pdcDetalharSolicAntProcLotePgtoAgda;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicAntProcLotePgtoAgda.
     * 
     * @return Adaptador ExcluirSolicAntProcLotePgtoAgda
     */
    public IExcluirSolicAntProcLotePgtoAgdaPDCAdapter getExcluirSolicAntProcLotePgtoAgdaPDCAdapter() {
        return pdcExcluirSolicAntProcLotePgtoAgda;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicAntProcLotePgtoAgda.
     * 
     * @param pdcExcluirSolicAntProcLotePgtoAgda
     *            Adaptador ExcluirSolicAntProcLotePgtoAgda
     */
    public void setExcluirSolicAntProcLotePgtoAgdaPDCAdapter(IExcluirSolicAntProcLotePgtoAgdaPDCAdapter pdcExcluirSolicAntProcLotePgtoAgda) {
        this.pdcExcluirSolicAntProcLotePgtoAgda = pdcExcluirSolicAntProcLotePgtoAgda;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicAntProcAgda.
     * 
     * @return Adaptador IncluirSolicAntProcAgda
     */
    public IIncluirSolicAntProcAgdaPDCAdapter getIncluirSolicAntProcAgdaPDCAdapter() {
        return pdcIncluirSolicAntProcAgda;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicAntProcAgda.
     * 
     * @param pdcIncluirSolicAntProcAgda
     *            Adaptador IncluirSolicAntProcAgda
     */
    public void setIncluirSolicAntProcAgdaPDCAdapter(IIncluirSolicAntProcAgdaPDCAdapter pdcIncluirSolicAntProcAgda) {
        this.pdcIncluirSolicAntProcAgda = pdcIncluirSolicAntProcAgda;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirSolicAntPosDtPgto.
     * 
     * @return Adaptador ExcluirSolicAntPosDtPgto
     */
    public IExcluirSolicAntPosDtPgtoPDCAdapter getExcluirSolicAntPosDtPgtoPDCAdapter() {
        return pdcExcluirSolicAntPosDtPgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirSolicAntPosDtPgto.
     * 
     * @param pdcExcluirSolicAntPosDtPgto
     *            Adaptador ExcluirSolicAntPosDtPgto
     */
    public void setExcluirSolicAntPosDtPgtoPDCAdapter(IExcluirSolicAntPosDtPgtoPDCAdapter pdcExcluirSolicAntPosDtPgto) {
        this.pdcExcluirSolicAntPosDtPgto = pdcExcluirSolicAntPosDtPgto;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharServicoRelacionado.
     * 
     * @return Adaptador DetalharServicoRelacionado
     */
    public IDetalharServicoRelacionadoPDCAdapter getDetalharServicoRelacionadoPDCAdapter() {
        return pdcDetalharServicoRelacionado;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharServicoRelacionado.
     * 
     * @param pdcDetalharServicoRelacionado
     *            Adaptador DetalharServicoRelacionado
     */
    public void setDetalharServicoRelacionadoPDCAdapter(IDetalharServicoRelacionadoPDCAdapter pdcDetalharServicoRelacionado) {
        this.pdcDetalharServicoRelacionado = pdcDetalharServicoRelacionado;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarServicoRelacionado.
     * 
     * @return Adaptador AlterarServicoRelacionado
     */
    public IAlterarServicoRelacionadoPDCAdapter getAlterarServicoRelacionadoPDCAdapter() {
        return pdcAlterarServicoRelacionado;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarServicoRelacionado.
     * 
     * @param pdcAlterarServicoRelacionado
     *            Adaptador AlterarServicoRelacionado
     */
    public void setAlterarServicoRelacionadoPDCAdapter(IAlterarServicoRelacionadoPDCAdapter pdcAlterarServicoRelacionado) {
        this.pdcAlterarServicoRelacionado = pdcAlterarServicoRelacionado;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirServicoRelacionado.
     * 
     * @return Adaptador IncluirServicoRelacionado
     */
    public IIncluirServicoRelacionadoPDCAdapter getIncluirServicoRelacionadoPDCAdapter() {
        return pdcIncluirServicoRelacionado;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirServicoRelacionado.
     * 
     * @param pdcIncluirServicoRelacionado
     *            Adaptador IncluirServicoRelacionado
     */
    public void setIncluirServicoRelacionadoPDCAdapter(IIncluirServicoRelacionadoPDCAdapter pdcIncluirServicoRelacionado) {
        this.pdcIncluirServicoRelacionado = pdcIncluirServicoRelacionado;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarUltimoNumeroRemessa.
     * 
     * @return Adaptador AlterarUltimoNumeroRemessa
     */
    public IAlterarUltimoNumeroRemessaPDCAdapter getAlterarUltimoNumeroRemessaPDCAdapter() {
        return pdcAlterarUltimoNumeroRemessa;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarUltimoNumeroRemessa.
     * 
     * @param pdcAlterarUltimoNumeroRemessa
     *            Adaptador AlterarUltimoNumeroRemessa
     */
    public void setAlterarUltimoNumeroRemessaPDCAdapter(IAlterarUltimoNumeroRemessaPDCAdapter pdcAlterarUltimoNumeroRemessa) {
        this.pdcAlterarUltimoNumeroRemessa = pdcAlterarUltimoNumeroRemessa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarUltimoNumeroRemessa.
     * 
     * @return Adaptador ConsultarUltimoNumeroRemessa
     */
    public IConsultarUltimoNumeroRemessaPDCAdapter getConsultarUltimoNumeroRemessaPDCAdapter() {
        return pdcConsultarUltimoNumeroRemessa;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarUltimoNumeroRemessa.
     * 
     * @param pdcConsultarUltimoNumeroRemessa
     *            Adaptador ConsultarUltimoNumeroRemessa
     */
    public void setConsultarUltimoNumeroRemessaPDCAdapter(IConsultarUltimoNumeroRemessaPDCAdapter pdcConsultarUltimoNumeroRemessa) {
        this.pdcConsultarUltimoNumeroRemessa = pdcConsultarUltimoNumeroRemessa;
    }
    /**
     * M�todo invocado para obter um adaptador ListarUltimoNumeroRemessa.
     * 
     * @return Adaptador ListarUltimoNumeroRemessa
     */
    public IListarUltimoNumeroRemessaPDCAdapter getListarUltimoNumeroRemessaPDCAdapter() {
        return pdcListarUltimoNumeroRemessa;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarUltimoNumeroRemessa.
     * 
     * @param pdcListarUltimoNumeroRemessa
     *            Adaptador ListarUltimoNumeroRemessa
     */
    public void setListarUltimoNumeroRemessaPDCAdapter(IListarUltimoNumeroRemessaPDCAdapter pdcListarUltimoNumeroRemessa) {
        this.pdcListarUltimoNumeroRemessa = pdcListarUltimoNumeroRemessa;
    }
    /**
     * M�todo invocado para obter um adaptador ListarMunicipio.
     * 
     * @return Adaptador ListarMunicipio
     */
    public IListarMunicipioPDCAdapter getListarMunicipioPDCAdapter() {
        return pdcListarMunicipio;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarMunicipio.
     * 
     * @param pdcListarMunicipio
     *            Adaptador ListarMunicipio
     */
    public void setListarMunicipioPDCAdapter(IListarMunicipioPDCAdapter pdcListarMunicipio) {
        this.pdcListarMunicipio = pdcListarMunicipio;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarUltimoNumeroRetorno.
     * 
     * @return Adaptador AlterarUltimoNumeroRetorno
     */
    public IAlterarUltimoNumeroRetornoPDCAdapter getAlterarUltimoNumeroRetornoPDCAdapter() {
        return pdcAlterarUltimoNumeroRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarUltimoNumeroRetorno.
     * 
     * @param pdcAlterarUltimoNumeroRetorno
     *            Adaptador AlterarUltimoNumeroRetorno
     */
    public void setAlterarUltimoNumeroRetornoPDCAdapter(IAlterarUltimoNumeroRetornoPDCAdapter pdcAlterarUltimoNumeroRetorno) {
        this.pdcAlterarUltimoNumeroRetorno = pdcAlterarUltimoNumeroRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarUltimoNumeroRetorno.
     * 
     * @return Adaptador ConsultarUltimoNumeroRetorno
     */
    public IConsultarUltimoNumeroRetornoPDCAdapter getConsultarUltimoNumeroRetornoPDCAdapter() {
        return pdcConsultarUltimoNumeroRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarUltimoNumeroRetorno.
     * 
     * @param pdcConsultarUltimoNumeroRetorno
     *            Adaptador ConsultarUltimoNumeroRetorno
     */
    public void setConsultarUltimoNumeroRetornoPDCAdapter(IConsultarUltimoNumeroRetornoPDCAdapter pdcConsultarUltimoNumeroRetorno) {
        this.pdcConsultarUltimoNumeroRetorno = pdcConsultarUltimoNumeroRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ListarUltimoNumeroRetorno.
     * 
     * @return Adaptador ListarUltimoNumeroRetorno
     */
    public IListarUltimoNumeroRetornoPDCAdapter getListarUltimoNumeroRetornoPDCAdapter() {
        return pdcListarUltimoNumeroRetorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarUltimoNumeroRetorno.
     * 
     * @param pdcListarUltimoNumeroRetorno
     *            Adaptador ListarUltimoNumeroRetorno
     */
    public void setListarUltimoNumeroRetornoPDCAdapter(IListarUltimoNumeroRetornoPDCAdapter pdcListarUltimoNumeroRetorno) {
        this.pdcListarUltimoNumeroRetorno = pdcListarUltimoNumeroRetorno;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicAntProcLotePgtoAgda.
     * 
     * @return Adaptador ConsultarSolicAntProcLotePgtoAgda
     */
    public IConsultarSolicAntProcLotePgtoAgdaPDCAdapter getConsultarSolicAntProcLotePgtoAgdaPDCAdapter() {
        return pdcConsultarSolicAntProcLotePgtoAgda;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicAntProcLotePgtoAgda.
     * 
     * @param pdcConsultarSolicAntProcLotePgtoAgda
     *            Adaptador ConsultarSolicAntProcLotePgtoAgda
     */
    public void setConsultarSolicAntProcLotePgtoAgdaPDCAdapter(IConsultarSolicAntProcLotePgtoAgdaPDCAdapter pdcConsultarSolicAntProcLotePgtoAgda) {
        this.pdcConsultarSolicAntProcLotePgtoAgda = pdcConsultarSolicAntProcLotePgtoAgda;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicExclusaoLotePgto.
     * 
     * @return Adaptador ConsultarSolicExclusaoLotePgto
     */
    public IConsultarSolicExclusaoLotePgtoPDCAdapter getConsultarSolicExclusaoLotePgtoPDCAdapter() {
        return pdcConsultarSolicExclusaoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicExclusaoLotePgto.
     * 
     * @param pdcConsultarSolicExclusaoLotePgto
     *            Adaptador ConsultarSolicExclusaoLotePgto
     */
    public void setConsultarSolicExclusaoLotePgtoPDCAdapter(IConsultarSolicExclusaoLotePgtoPDCAdapter pdcConsultarSolicExclusaoLotePgto) {
        this.pdcConsultarSolicExclusaoLotePgto = pdcConsultarSolicExclusaoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicAntPosDtPgto.
     * 
     * @return Adaptador ConsultarSolicAntPosDtPgto
     */
    public IConsultarSolicAntPosDtPgtoPDCAdapter getConsultarSolicAntPosDtPgtoPDCAdapter() {
        return pdcConsultarSolicAntPosDtPgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicAntPosDtPgto.
     * 
     * @param pdcConsultarSolicAntPosDtPgto
     *            Adaptador ConsultarSolicAntPosDtPgto
     */
    public void setConsultarSolicAntPosDtPgtoPDCAdapter(IConsultarSolicAntPosDtPgtoPDCAdapter pdcConsultarSolicAntPosDtPgto) {
        this.pdcConsultarSolicAntPosDtPgto = pdcConsultarSolicAntPosDtPgto;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicCancelamentoLotePgto.
     * 
     * @return Adaptador ConsultarSolicCancelamentoLotePgto
     */
    public IConsultarSolicCancelamentoLotePgtoPDCAdapter getConsultarSolicCancelamentoLotePgtoPDCAdapter() {
        return pdcConsultarSolicCancelamentoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicCancelamentoLotePgto.
     * 
     * @param pdcConsultarSolicCancelamentoLotePgto
     *            Adaptador ConsultarSolicCancelamentoLotePgto
     */
    public void setConsultarSolicCancelamentoLotePgtoPDCAdapter(IConsultarSolicCancelamentoLotePgtoPDCAdapter pdcConsultarSolicCancelamentoLotePgto) {
        this.pdcConsultarSolicCancelamentoLotePgto = pdcConsultarSolicCancelamentoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLotesIncSolic.
     * 
     * @return Adaptador ConsultarLotesIncSolic
     */
    public IConsultarLotesIncSolicPDCAdapter getConsultarLotesIncSolicPDCAdapter() {
        return pdcConsultarLotesIncSolic;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLotesIncSolic.
     * 
     * @param pdcConsultarLotesIncSolic
     *            Adaptador ConsultarLotesIncSolic
     */
    public void setConsultarLotesIncSolicPDCAdapter(IConsultarLotesIncSolicPDCAdapter pdcConsultarLotesIncSolic) {
        this.pdcConsultarLotesIncSolic = pdcConsultarLotesIncSolic;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirSolicAntPosDtPgto.
     * 
     * @return Adaptador IncluirSolicAntPosDtPgto
     */
    public IIncluirSolicAntPosDtPgtoPDCAdapter getIncluirSolicAntPosDtPgtoPDCAdapter() {
        return pdcIncluirSolicAntPosDtPgto;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirSolicAntPosDtPgto.
     * 
     * @param pdcIncluirSolicAntPosDtPgto
     *            Adaptador IncluirSolicAntPosDtPgto
     */
    public void setIncluirSolicAntPosDtPgtoPDCAdapter(IIncluirSolicAntPosDtPgtoPDCAdapter pdcIncluirSolicAntPosDtPgto) {
        this.pdcIncluirSolicAntPosDtPgto = pdcIncluirSolicAntPosDtPgto;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicLiberacaoLotePgto.
     * 
     * @return Adaptador ConsultarSolicLiberacaoLotePgto
     */
    public IConsultarSolicLiberacaoLotePgtoPDCAdapter getConsultarSolicLiberacaoLotePgtoPDCAdapter() {
        return pdcConsultarSolicLiberacaoLotePgto;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicLiberacaoLotePgto.
     * 
     * @param pdcConsultarSolicLiberacaoLotePgto
     *            Adaptador ConsultarSolicLiberacaoLotePgto
     */
    public void setConsultarSolicLiberacaoLotePgtoPDCAdapter(IConsultarSolicLiberacaoLotePgtoPDCAdapter pdcConsultarSolicLiberacaoLotePgto) {
        this.pdcConsultarSolicLiberacaoLotePgto = pdcConsultarSolicLiberacaoLotePgto;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharSolicAntPosDtPgto.
     * 
     * @return Adaptador DetalharSolicAntPosDtPgto
     */
    public IDetalharSolicAntPosDtPgtoPDCAdapter getDetalharSolicAntPosDtPgtoPDCAdapter() {
        return pdcDetalharSolicAntPosDtPgto;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharSolicAntPosDtPgto.
     * 
     * @param pdcDetalharSolicAntPosDtPgto
     *            Adaptador DetalharSolicAntPosDtPgto
     */
    public void setDetalharSolicAntPosDtPgtoPDCAdapter(IDetalharSolicAntPosDtPgtoPDCAdapter pdcDetalharSolicAntPosDtPgto) {
        this.pdcDetalharSolicAntPosDtPgto = pdcDetalharSolicAntPosDtPgto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAssVersaoLayoutLoteServico.
     * 
     * @return Adaptador IncluirAssVersaoLayoutLoteServico
     */
    public IIncluirAssVersaoLayoutLoteServicoPDCAdapter getIncluirAssVersaoLayoutLoteServicoPDCAdapter() {
        return pdcIncluirAssVersaoLayoutLoteServico;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAssVersaoLayoutLoteServico.
     * 
     * @param pdcIncluirAssVersaoLayoutLoteServico
     *            Adaptador IncluirAssVersaoLayoutLoteServico
     */
    public void setIncluirAssVersaoLayoutLoteServicoPDCAdapter(IIncluirAssVersaoLayoutLoteServicoPDCAdapter pdcIncluirAssVersaoLayoutLoteServico) {
        this.pdcIncluirAssVersaoLayoutLoteServico = pdcIncluirAssVersaoLayoutLoteServico;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirAssVersaoLayoutLoteServico.
     * 
     * @return Adaptador ExcluirAssVersaoLayoutLoteServico
     */
    public IExcluirAssVersaoLayoutLoteServicoPDCAdapter getExcluirAssVersaoLayoutLoteServicoPDCAdapter() {
        return pdcExcluirAssVersaoLayoutLoteServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirAssVersaoLayoutLoteServico.
     * 
     * @param pdcExcluirAssVersaoLayoutLoteServico
     *            Adaptador ExcluirAssVersaoLayoutLoteServico
     */
    public void setExcluirAssVersaoLayoutLoteServicoPDCAdapter(IExcluirAssVersaoLayoutLoteServicoPDCAdapter pdcExcluirAssVersaoLayoutLoteServico) {
        this.pdcExcluirAssVersaoLayoutLoteServico = pdcExcluirAssVersaoLayoutLoteServico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarAssVersaoLayoutLoteServico.
     * 
     * @return Adaptador ConsultarAssVersaoLayoutLoteServico
     */
    public IConsultarAssVersaoLayoutLoteServicoPDCAdapter getConsultarAssVersaoLayoutLoteServicoPDCAdapter() {
        return pdcConsultarAssVersaoLayoutLoteServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarAssVersaoLayoutLoteServico.
     * 
     * @param pdcConsultarAssVersaoLayoutLoteServico
     *            Adaptador ConsultarAssVersaoLayoutLoteServico
     */
    public void setConsultarAssVersaoLayoutLoteServicoPDCAdapter(IConsultarAssVersaoLayoutLoteServicoPDCAdapter pdcConsultarAssVersaoLayoutLoteServico) {
        this.pdcConsultarAssVersaoLayoutLoteServico = pdcConsultarAssVersaoLayoutLoteServico;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarSolicitacaoEstornoPagtos.
     * 
     * @return Adaptador ConsultarSolicitacaoEstornoPagtos
     */
    public IConsultarSolicitacaoEstornoPagtosPDCAdapter getConsultarSolicitacaoEstornoPagtosPDCAdapter() {
        return pdcConsultarSolicitacaoEstornoPagtos;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarSolicitacaoEstornoPagtos.
     * 
     * @param pdcConsultarSolicitacaoEstornoPagtos
     *            Adaptador ConsultarSolicitacaoEstornoPagtos
     */
    public void setConsultarSolicitacaoEstornoPagtosPDCAdapter(IConsultarSolicitacaoEstornoPagtosPDCAdapter pdcConsultarSolicitacaoEstornoPagtos) {
        this.pdcConsultarSolicitacaoEstornoPagtos = pdcConsultarSolicitacaoEstornoPagtos;
    }
    /**
     * M�todo invocado para obter um adaptador ListarNumeroVersaoLayout.
     * 
     * @return Adaptador ListarNumeroVersaoLayout
     */
    public IListarNumeroVersaoLayoutPDCAdapter getListarNumeroVersaoLayoutPDCAdapter() {
        return pdcListarNumeroVersaoLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarNumeroVersaoLayout.
     * 
     * @param pdcListarNumeroVersaoLayout
     *            Adaptador ListarNumeroVersaoLayout
     */
    public void setListarNumeroVersaoLayoutPDCAdapter(IListarNumeroVersaoLayoutPDCAdapter pdcListarNumeroVersaoLayout) {
        this.pdcListarNumeroVersaoLayout = pdcListarNumeroVersaoLayout;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirLayout.
     * 
     * @return Adaptador ExcluirLayout
     */
    public IExcluirLayoutPDCAdapter getExcluirLayoutPDCAdapter() {
        return pdcExcluirLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirLayout.
     * 
     * @param pdcExcluirLayout
     *            Adaptador ExcluirLayout
     */
    public void setExcluirLayoutPDCAdapter(IExcluirLayoutPDCAdapter pdcExcluirLayout) {
        this.pdcExcluirLayout = pdcExcluirLayout;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConfigPadraoAtribTipoServMod.
     * 
     * @return Adaptador ConsultarConfigPadraoAtribTipoServMod
     */
    public IConsultarConfigPadraoAtribTipoServModPDCAdapter getConsultarConfigPadraoAtribTipoServModPDCAdapter() {
        return pdcConsultarConfigPadraoAtribTipoServMod;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConfigPadraoAtribTipoServMod.
     * 
     * @param pdcConsultarConfigPadraoAtribTipoServMod
     *            Adaptador ConsultarConfigPadraoAtribTipoServMod
     */
    public void setConsultarConfigPadraoAtribTipoServModPDCAdapter(IConsultarConfigPadraoAtribTipoServModPDCAdapter pdcConsultarConfigPadraoAtribTipoServMod) {
        this.pdcConsultarConfigPadraoAtribTipoServMod = pdcConsultarConfigPadraoAtribTipoServMod;
    }
    /**
     * M�todo invocado para obter um adaptador EfetuarValidacaoConfigAtribModalidade.
     * 
     * @return Adaptador EfetuarValidacaoConfigAtribModalidade
     */
    public IEfetuarValidacaoConfigAtribModalidadePDCAdapter getEfetuarValidacaoConfigAtribModalidadePDCAdapter() {
        return pdcEfetuarValidacaoConfigAtribModalidade;
    }

    /**
     * M�todo invocado para establecer um adaptador EfetuarValidacaoConfigAtribModalidade.
     * 
     * @param pdcEfetuarValidacaoConfigAtribModalidade
     *            Adaptador EfetuarValidacaoConfigAtribModalidade
     */
    public void setEfetuarValidacaoConfigAtribModalidadePDCAdapter(IEfetuarValidacaoConfigAtribModalidadePDCAdapter pdcEfetuarValidacaoConfigAtribModalidade) {
        this.pdcEfetuarValidacaoConfigAtribModalidade = pdcEfetuarValidacaoConfigAtribModalidade;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosEstornoOP.
     * 
     * @return Adaptador ConsultarPagtosEstornoOP
     */
    public IConsultarPagtosEstornoOPPDCAdapter getConsultarPagtosEstornoOPPDCAdapter() {
        return pdcConsultarPagtosEstornoOP;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosEstornoOP.
     * 
     * @param pdcConsultarPagtosEstornoOP
     *            Adaptador ConsultarPagtosEstornoOP
     */
    public void setConsultarPagtosEstornoOPPDCAdapter(IConsultarPagtosEstornoOPPDCAdapter pdcConsultarPagtosEstornoOP) {
        this.pdcConsultarPagtosEstornoOP = pdcConsultarPagtosEstornoOP;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarPropostaAndamento.
     * 
     * @return Adaptador ValidarPropostaAndamento
     */
    public IValidarPropostaAndamentoPDCAdapter getValidarPropostaAndamentoPDCAdapter() {
        return pdcValidarPropostaAndamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarPropostaAndamento.
     * 
     * @param pdcValidarPropostaAndamento
     *            Adaptador ValidarPropostaAndamento
     */
    public void setValidarPropostaAndamentoPDCAdapter(IValidarPropostaAndamentoPDCAdapter pdcValidarPropostaAndamento) {
        this.pdcValidarPropostaAndamento = pdcValidarPropostaAndamento;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConfigTipoServicoModalidade.
     * 
     * @return Adaptador ConsultarConfigTipoServicoModalidade
     */
    public IConsultarConfigTipoServicoModalidadePDCAdapter getConsultarConfigTipoServicoModalidadePDCAdapter() {
        return pdcConsultarConfigTipoServicoModalidade;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConfigTipoServicoModalidade.
     * 
     * @param pdcConsultarConfigTipoServicoModalidade
     *            Adaptador ConsultarConfigTipoServicoModalidade
     */
    public void setConsultarConfigTipoServicoModalidadePDCAdapter(IConsultarConfigTipoServicoModalidadePDCAdapter pdcConsultarConfigTipoServicoModalidade) {
        this.pdcConsultarConfigTipoServicoModalidade = pdcConsultarConfigTipoServicoModalidade;
    }
    /**
     * M�todo invocado para obter um adaptador ListarAssVersaoLayoutLoteServico.
     * 
     * @return Adaptador ListarAssVersaoLayoutLoteServico
     */
    public IListarAssVersaoLayoutLoteServicoPDCAdapter getListarAssVersaoLayoutLoteServicoPDCAdapter() {
        return pdcListarAssVersaoLayoutLoteServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarAssVersaoLayoutLoteServico.
     * 
     * @param pdcListarAssVersaoLayoutLoteServico
     *            Adaptador ListarAssVersaoLayoutLoteServico
     */
    public void setListarAssVersaoLayoutLoteServicoPDCAdapter(IListarAssVersaoLayoutLoteServicoPDCAdapter pdcListarAssVersaoLayoutLoteServico) {
        this.pdcListarAssVersaoLayoutLoteServico = pdcListarAssVersaoLayoutLoteServico;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirLayout.
     * 
     * @return Adaptador IncluirLayout
     */
    public IIncluirLayoutPDCAdapter getIncluirLayoutPDCAdapter() {
        return pdcIncluirLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirLayout.
     * 
     * @param pdcIncluirLayout
     *            Adaptador IncluirLayout
     */
    public void setIncluirLayoutPDCAdapter(IIncluirLayoutPDCAdapter pdcIncluirLayout) {
        this.pdcIncluirLayout = pdcIncluirLayout;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaLayoutArquivoContrato.
     * 
     * @return Adaptador ConsultarListaLayoutArquivoContrato
     */
    public IConsultarListaLayoutArquivoContratoPDCAdapter getConsultarListaLayoutArquivoContratoPDCAdapter() {
        return pdcConsultarListaLayoutArquivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaLayoutArquivoContrato.
     * 
     * @param pdcConsultarListaLayoutArquivoContrato
     *            Adaptador ConsultarListaLayoutArquivoContrato
     */
    public void setConsultarListaLayoutArquivoContratoPDCAdapter(IConsultarListaLayoutArquivoContratoPDCAdapter pdcConsultarListaLayoutArquivoContrato) {
        this.pdcConsultarListaLayoutArquivoContrato = pdcConsultarListaLayoutArquivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosEstorno.
     * 
     * @return Adaptador ConsultarPagtosEstorno
     */
    public IConsultarPagtosEstornoPDCAdapter getConsultarPagtosEstornoPDCAdapter() {
        return pdcConsultarPagtosEstorno;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosEstorno.
     * 
     * @param pdcConsultarPagtosEstorno
     *            Adaptador ConsultarPagtosEstorno
     */
    public void setConsultarPagtosEstornoPDCAdapter(IConsultarPagtosEstornoPDCAdapter pdcConsultarPagtosEstorno) {
        this.pdcConsultarPagtosEstorno = pdcConsultarPagtosEstorno;
    }
    /**
     * M�todo invocado para obter um adaptador ListarModalidadeTipoServico.
     * 
     * @return Adaptador ListarModalidadeTipoServico
     */
    public IListarModalidadeTipoServicoPDCAdapter getListarModalidadeTipoServicoPDCAdapter() {
        return pdcListarModalidadeTipoServico;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarModalidadeTipoServico.
     * 
     * @param pdcListarModalidadeTipoServico
     *            Adaptador ListarModalidadeTipoServico
     */
    public void setListarModalidadeTipoServicoPDCAdapter(IListarModalidadeTipoServicoPDCAdapter pdcListarModalidadeTipoServico) {
        this.pdcListarModalidadeTipoServico = pdcListarModalidadeTipoServico;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoRelacionadoCdps.
     * 
     * @return Adaptador ListarServicoRelacionadoCdps
     */
    public IListarServicoRelacionadoCdpsPDCAdapter getListarServicoRelacionadoCdpsPDCAdapter() {
        return pdcListarServicoRelacionadoCdps;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoRelacionadoCdps.
     * 
     * @param pdcListarServicoRelacionadoCdps
     *            Adaptador ListarServicoRelacionadoCdps
     */
    public void setListarServicoRelacionadoCdpsPDCAdapter(IListarServicoRelacionadoCdpsPDCAdapter pdcListarServicoRelacionadoCdps) {
        this.pdcListarServicoRelacionadoCdps = pdcListarServicoRelacionadoCdps;
    }
    /**
     * M�todo invocado para obter um adaptador EstornarPagamentos.
     * 
     * @return Adaptador EstornarPagamentos
     */
    public IEstornarPagamentosPDCAdapter getEstornarPagamentosPDCAdapter() {
        return pdcEstornarPagamentos;
    }

    /**
     * M�todo invocado para establecer um adaptador EstornarPagamentos.
     * 
     * @param pdcEstornarPagamentos
     *            Adaptador EstornarPagamentos
     */
    public void setEstornarPagamentosPDCAdapter(IEstornarPagamentosPDCAdapter pdcEstornarPagamentos) {
        this.pdcEstornarPagamentos = pdcEstornarPagamentos;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarPerfilContrato.
     * 
     * @return Adaptador AlterarPerfilContrato
     */
    public IAlterarPerfilContratoPDCAdapter getAlterarPerfilContratoPDCAdapter() {
        return pdcAlterarPerfilContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarPerfilContrato.
     * 
     * @param pdcAlterarPerfilContrato
     *            Adaptador AlterarPerfilContrato
     */
    public void setAlterarPerfilContratoPDCAdapter(IAlterarPerfilContratoPDCAdapter pdcAlterarPerfilContrato) {
        this.pdcAlterarPerfilContrato = pdcAlterarPerfilContrato;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirHorarioLimite.
     * 
     * @return Adaptador IncluirHorarioLimite
     */
    public IIncluirHorarioLimitePDCAdapter getIncluirHorarioLimitePDCAdapter() {
        return pdcIncluirHorarioLimite;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirHorarioLimite.
     * 
     * @param pdcIncluirHorarioLimite
     *            Adaptador IncluirHorarioLimite
     */
    public void setIncluirHorarioLimitePDCAdapter(IIncluirHorarioLimitePDCAdapter pdcIncluirHorarioLimite) {
        this.pdcIncluirHorarioLimite = pdcIncluirHorarioLimite;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharListaDebito.
     * 
     * @return Adaptador DetalharListaDebito
     */
    public IDetalharListaDebitoPDCAdapter getDetalharListaDebitoPDCAdapter() {
        return pdcDetalharListaDebito;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharListaDebito.
     * 
     * @param pdcDetalharListaDebito
     *            Adaptador DetalharListaDebito
     */
    public void setDetalharListaDebitoPDCAdapter(IDetalharListaDebitoPDCAdapter pdcDetalharListaDebito) {
        this.pdcDetalharListaDebito = pdcDetalharListaDebito;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharFolha.
     * 
     * @return Adaptador DetalharFolha
     */
    public IDetalharFolhaPDCAdapter getDetalharFolhaPDCAdapter() {
        return pdcDetalharFolha;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharFolha.
     * 
     * @param pdcDetalharFolha
     *            Adaptador DetalharFolha
     */
    public void setDetalharFolhaPDCAdapter(IDetalharFolhaPDCAdapter pdcDetalharFolha) {
        this.pdcDetalharFolha = pdcDetalharFolha;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoRelacionadoCdpsIncluir.
     * 
     * @return Adaptador ListarServicoRelacionadoCdpsIncluir
     */
    public IListarServicoRelacionadoCdpsIncluirPDCAdapter getListarServicoRelacionadoCdpsIncluirPDCAdapter() {
        return pdcListarServicoRelacionadoCdpsIncluir;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoRelacionadoCdpsIncluir.
     * 
     * @param pdcListarServicoRelacionadoCdpsIncluir
     *            Adaptador ListarServicoRelacionadoCdpsIncluir
     */
    public void setListarServicoRelacionadoCdpsIncluirPDCAdapter(IListarServicoRelacionadoCdpsIncluirPDCAdapter pdcListarServicoRelacionadoCdpsIncluir) {
        this.pdcListarServicoRelacionadoCdpsIncluir = pdcListarServicoRelacionadoCdpsIncluir;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirRepresentante.
     * 
     * @return Adaptador IncluirRepresentante
     */
    public IIncluirRepresentantePDCAdapter getIncluirRepresentantePDCAdapter() {
        return pdcIncluirRepresentante;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirRepresentante.
     * 
     * @param pdcIncluirRepresentante
     *            Adaptador IncluirRepresentante
     */
    public void setIncluirRepresentantePDCAdapter(IIncluirRepresentantePDCAdapter pdcIncluirRepresentante) {
        this.pdcIncluirRepresentante = pdcIncluirRepresentante;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarUsuario.
     * 
     * @return Adaptador ValidarUsuario
     */
    public IValidarUsuarioPDCAdapter getValidarUsuarioPDCAdapter() {
        return pdcValidarUsuario;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarUsuario.
     * 
     * @param pdcValidarUsuario
     *            Adaptador ValidarUsuario
     */
    public void setValidarUsuarioPDCAdapter(IValidarUsuarioPDCAdapter pdcValidarUsuario) {
        this.pdcValidarUsuario = pdcValidarUsuario;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagamentosConsolidados.
     * 
     * @return Adaptador DetalharPagamentosConsolidados
     */
    public IDetalharPagamentosConsolidadosPDCAdapter getDetalharPagamentosConsolidadosPDCAdapter() {
        return pdcDetalharPagamentosConsolidados;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagamentosConsolidados.
     * 
     * @param pdcDetalharPagamentosConsolidados
     *            Adaptador DetalharPagamentosConsolidados
     */
    public void setDetalharPagamentosConsolidadosPDCAdapter(IDetalharPagamentosConsolidadosPDCAdapter pdcDetalharPagamentosConsolidados) {
        this.pdcDetalharPagamentosConsolidados = pdcDetalharPagamentosConsolidados;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagamentoFavorecido.
     * 
     * @return Adaptador ConsultarPagamentoFavorecido
     */
    public IConsultarPagamentoFavorecidoPDCAdapter getConsultarPagamentoFavorecidoPDCAdapter() {
        return pdcConsultarPagamentoFavorecido;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagamentoFavorecido.
     * 
     * @param pdcConsultarPagamentoFavorecido
     *            Adaptador ConsultarPagamentoFavorecido
     */
    public void setConsultarPagamentoFavorecidoPDCAdapter(IConsultarPagamentoFavorecidoPDCAdapter pdcConsultarPagamentoFavorecido) {
        this.pdcConsultarPagamentoFavorecido = pdcConsultarPagamentoFavorecido;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagamentosIndividuais.
     * 
     * @return Adaptador ConsultarPagamentosIndividuais
     */
    public IConsultarPagamentosIndividuaisPDCAdapter getConsultarPagamentosIndividuaisPDCAdapter() {
        return pdcConsultarPagamentosIndividuais;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagamentosIndividuais.
     * 
     * @param pdcConsultarPagamentosIndividuais
     *            Adaptador ConsultarPagamentosIndividuais
     */
    public void setConsultarPagamentosIndividuaisPDCAdapter(IConsultarPagamentosIndividuaisPDCAdapter pdcConsultarPagamentosIndividuais) {
        this.pdcConsultarPagamentosIndividuais = pdcConsultarPagamentosIndividuais;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarVinculacaoConvenioContaSalario.
     * 
     * @return Adaptador ValidarVinculacaoConvenioContaSalario
     */
    public IValidarVinculacaoConvenioContaSalarioPDCAdapter getValidarVinculacaoConvenioContaSalarioPDCAdapter() {
        return pdcValidarVinculacaoConvenioContaSalario;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarVinculacaoConvenioContaSalario.
     * 
     * @param pdcValidarVinculacaoConvenioContaSalario
     *            Adaptador ValidarVinculacaoConvenioContaSalario
     */
    public void setValidarVinculacaoConvenioContaSalarioPDCAdapter(IValidarVinculacaoConvenioContaSalarioPDCAdapter pdcValidarVinculacaoConvenioContaSalario) {
        this.pdcValidarVinculacaoConvenioContaSalario = pdcValidarVinculacaoConvenioContaSalario;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoLayoutArquivo.
     * 
     * @return Adaptador ListarTipoLayoutArquivo
     */
    public IListarTipoLayoutArquivoPDCAdapter getListarTipoLayoutArquivoPDCAdapter() {
        return pdcListarTipoLayoutArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoLayoutArquivo.
     * 
     * @param pdcListarTipoLayoutArquivo
     *            Adaptador ListarTipoLayoutArquivo
     */
    public void setListarTipoLayoutArquivoPDCAdapter(IListarTipoLayoutArquivoPDCAdapter pdcListarTipoLayoutArquivo) {
        this.pdcListarTipoLayoutArquivo = pdcListarTipoLayoutArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConManLayout.
     * 
     * @return Adaptador ConsultarConManLayout
     */
    public IConsultarConManLayoutPDCAdapter getConsultarConManLayoutPDCAdapter() {
        return pdcConsultarConManLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConManLayout.
     * 
     * @param pdcConsultarConManLayout
     *            Adaptador ConsultarConManLayout
     */
    public void setConsultarConManLayoutPDCAdapter(IConsultarConManLayoutPDCAdapter pdcConsultarConManLayout) {
        this.pdcConsultarConManLayout = pdcConsultarConManLayout;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPiramide.
     * 
     * @return Adaptador DetalharPiramide
     */
    public IDetalharPiramidePDCAdapter getDetalharPiramidePDCAdapter() {
        return pdcDetalharPiramide;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPiramide.
     * 
     * @param pdcDetalharPiramide
     *            Adaptador DetalharPiramide
     */
    public void setDetalharPiramidePDCAdapter(IDetalharPiramidePDCAdapter pdcDetalharPiramide) {
        this.pdcDetalharPiramide = pdcDetalharPiramide;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirPerfilTrocaArquivo.
     * 
     * @return Adaptador ExcluirPerfilTrocaArquivo
     */
    public IExcluirPerfilTrocaArquivoPDCAdapter getExcluirPerfilTrocaArquivoPDCAdapter() {
        return pdcExcluirPerfilTrocaArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirPerfilTrocaArquivo.
     * 
     * @param pdcExcluirPerfilTrocaArquivo
     *            Adaptador ExcluirPerfilTrocaArquivo
     */
    public void setExcluirPerfilTrocaArquivoPDCAdapter(IExcluirPerfilTrocaArquivoPDCAdapter pdcExcluirPerfilTrocaArquivo) {
        this.pdcExcluirPerfilTrocaArquivo = pdcExcluirPerfilTrocaArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirLayoutArqServico.
     * 
     * @return Adaptador IncluirLayoutArqServico
     */
    public IIncluirLayoutArqServicoPDCAdapter getIncluirLayoutArqServicoPDCAdapter() {
        return pdcIncluirLayoutArqServico;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirLayoutArqServico.
     * 
     * @param pdcIncluirLayoutArqServico
     *            Adaptador IncluirLayoutArqServico
     */
    public void setIncluirLayoutArqServicoPDCAdapter(IIncluirLayoutArqServicoPDCAdapter pdcIncluirLayoutArqServico) {
        this.pdcIncluirLayoutArqServico = pdcIncluirLayoutArqServico;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharLayoutArqServico.
     * 
     * @return Adaptador DetalharLayoutArqServico
     */
    public IDetalharLayoutArqServicoPDCAdapter getDetalharLayoutArqServicoPDCAdapter() {
        return pdcDetalharLayoutArqServico;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharLayoutArqServico.
     * 
     * @param pdcDetalharLayoutArqServico
     *            Adaptador DetalharLayoutArqServico
     */
    public void setDetalharLayoutArqServicoPDCAdapter(IDetalharLayoutArqServicoPDCAdapter pdcDetalharLayoutArqServico) {
        this.pdcDetalharLayoutArqServico = pdcDetalharLayoutArqServico;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarAgencia.
     * 
     * @return Adaptador ValidarAgencia
     */
    public IValidarAgenciaPDCAdapter getValidarAgenciaPDCAdapter() {
        return pdcValidarAgencia;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarAgencia.
     * 
     * @param pdcValidarAgencia
     *            Adaptador ValidarAgencia
     */
    public void setValidarAgenciaPDCAdapter(IValidarAgenciaPDCAdapter pdcValidarAgencia) {
        this.pdcValidarAgencia = pdcValidarAgencia;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagotsVencNaoPagos.
     * 
     * @return Adaptador ConsultarPagotsVencNaoPagos
     */
    public IConsultarPagotsVencNaoPagosPDCAdapter getConsultarPagotsVencNaoPagosPDCAdapter() {
        return pdcConsultarPagotsVencNaoPagos;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagotsVencNaoPagos.
     * 
     * @param pdcConsultarPagotsVencNaoPagos
     *            Adaptador ConsultarPagotsVencNaoPagos
     */
    public void setConsultarPagotsVencNaoPagosPDCAdapter(IConsultarPagotsVencNaoPagosPDCAdapter pdcConsultarPagotsVencNaoPagos) {
        this.pdcConsultarPagotsVencNaoPagos = pdcConsultarPagotsVencNaoPagos;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLibPagtosIndSemConsSaldo.
     * 
     * @return Adaptador ConsultarLibPagtosIndSemConsSaldo
     */
    public IConsultarLibPagtosIndSemConsSaldoPDCAdapter getConsultarLibPagtosIndSemConsSaldoPDCAdapter() {
        return pdcConsultarLibPagtosIndSemConsSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLibPagtosIndSemConsSaldo.
     * 
     * @param pdcConsultarLibPagtosIndSemConsSaldo
     *            Adaptador ConsultarLibPagtosIndSemConsSaldo
     */
    public void setConsultarLibPagtosIndSemConsSaldoPDCAdapter(IConsultarLibPagtosIndSemConsSaldoPDCAdapter pdcConsultarLibPagtosIndSemConsSaldo) {
        this.pdcConsultarLibPagtosIndSemConsSaldo = pdcConsultarLibPagtosIndSemConsSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtoPendIndividual.
     * 
     * @return Adaptador ConsultarPagtoPendIndividual
     */
    public IConsultarPagtoPendIndividualPDCAdapter getConsultarPagtoPendIndividualPDCAdapter() {
        return pdcConsultarPagtoPendIndividual;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtoPendIndividual.
     * 
     * @param pdcConsultarPagtoPendIndividual
     *            Adaptador ConsultarPagtoPendIndividual
     */
    public void setConsultarPagtoPendIndividualPDCAdapter(IConsultarPagtoPendIndividualPDCAdapter pdcConsultarPagtoPendIndividual) {
        this.pdcConsultarPagtoPendIndividual = pdcConsultarPagtoPendIndividual;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosIndAutorizados.
     * 
     * @return Adaptador ConsultarPagtosIndAutorizados
     */
    public IConsultarPagtosIndAutorizadosPDCAdapter getConsultarPagtosIndAutorizadosPDCAdapter() {
        return pdcConsultarPagtosIndAutorizados;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosIndAutorizados.
     * 
     * @param pdcConsultarPagtosIndAutorizados
     *            Adaptador ConsultarPagtosIndAutorizados
     */
    public void setConsultarPagtosIndAutorizadosPDCAdapter(IConsultarPagtosIndAutorizadosPDCAdapter pdcConsultarPagtosIndAutorizados) {
        this.pdcConsultarPagtosIndAutorizados = pdcConsultarPagtosIndAutorizados;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagamentos.
     * 
     * @return Adaptador DetalharPagamentos
     */
    public IDetalharPagamentosPDCAdapter getDetalharPagamentosPDCAdapter() {
        return pdcDetalharPagamentos;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagamentos.
     * 
     * @param pdcDetalharPagamentos
     *            Adaptador DetalharPagamentos
     */
    public void setDetalharPagamentosPDCAdapter(IDetalharPagamentosPDCAdapter pdcDetalharPagamentos) {
        this.pdcDetalharPagamentos = pdcDetalharPagamentos;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosIndParaCancelamento.
     * 
     * @return Adaptador ConsultarPagtosIndParaCancelamento
     */
    public IConsultarPagtosIndParaCancelamentoPDCAdapter getConsultarPagtosIndParaCancelamentoPDCAdapter() {
        return pdcConsultarPagtosIndParaCancelamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosIndParaCancelamento.
     * 
     * @param pdcConsultarPagtosIndParaCancelamento
     *            Adaptador ConsultarPagtosIndParaCancelamento
     */
    public void setConsultarPagtosIndParaCancelamentoPDCAdapter(IConsultarPagtosIndParaCancelamentoPDCAdapter pdcConsultarPagtosIndParaCancelamento) {
        this.pdcConsultarPagtosIndParaCancelamento = pdcConsultarPagtosIndParaCancelamento;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarCanLibPagtosIndSemConsSaldo.
     * 
     * @return Adaptador ConsultarCanLibPagtosIndSemConsSaldo
     */
    public IConsultarCanLibPagtosIndSemConsSaldoPDCAdapter getConsultarCanLibPagtosIndSemConsSaldoPDCAdapter() {
        return pdcConsultarCanLibPagtosIndSemConsSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarCanLibPagtosIndSemConsSaldo.
     * 
     * @param pdcConsultarCanLibPagtosIndSemConsSaldo
     *            Adaptador ConsultarCanLibPagtosIndSemConsSaldo
     */
    public void setConsultarCanLibPagtosIndSemConsSaldoPDCAdapter(IConsultarCanLibPagtosIndSemConsSaldoPDCAdapter pdcConsultarCanLibPagtosIndSemConsSaldo) {
        this.pdcConsultarCanLibPagtosIndSemConsSaldo = pdcConsultarCanLibPagtosIndSemConsSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtoIndPendAutorizacao.
     * 
     * @return Adaptador ConsultarPagtoIndPendAutorizacao
     */
    public IConsultarPagtoIndPendAutorizacaoPDCAdapter getConsultarPagtoIndPendAutorizacaoPDCAdapter() {
        return pdcConsultarPagtoIndPendAutorizacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtoIndPendAutorizacao.
     * 
     * @param pdcConsultarPagtoIndPendAutorizacao
     *            Adaptador ConsultarPagtoIndPendAutorizacao
     */
    public void setConsultarPagtoIndPendAutorizacaoPDCAdapter(IConsultarPagtoIndPendAutorizacaoPDCAdapter pdcConsultarPagtoIndPendAutorizacao) {
        this.pdcConsultarPagtoIndPendAutorizacao = pdcConsultarPagtoIndPendAutorizacao;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarPagtosIndAntPostergacao.
     * 
     * @return Adaptador ConsultarPagtosIndAntPostergacao
     */
    public IConsultarPagtosIndAntPostergacaoPDCAdapter getConsultarPagtosIndAntPostergacaoPDCAdapter() {
        return pdcConsultarPagtosIndAntPostergacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarPagtosIndAntPostergacao.
     * 
     * @param pdcConsultarPagtosIndAntPostergacao
     *            Adaptador ConsultarPagtosIndAntPostergacao
     */
    public void setConsultarPagtosIndAntPostergacaoPDCAdapter(IConsultarPagtosIndAntPostergacaoPDCAdapter pdcConsultarPagtosIndAntPostergacao) {
        this.pdcConsultarPagtosIndAntPostergacao = pdcConsultarPagtosIndAntPostergacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarRepresentante.
     * 
     * @return Adaptador ListarRepresentante
     */
    public IListarRepresentantePDCAdapter getListarRepresentantePDCAdapter() {
        return pdcListarRepresentante;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarRepresentante.
     * 
     * @param pdcListarRepresentante
     *            Adaptador ListarRepresentante
     */
    public void setListarRepresentantePDCAdapter(IListarRepresentantePDCAdapter pdcListarRepresentante) {
        this.pdcListarRepresentante = pdcListarRepresentante;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarArquivoServico.
     * 
     * @return Adaptador AlterarArquivoServico
     */
    public IAlterarArquivoServicoPDCAdapter getAlterarArquivoServicoPDCAdapter() {
        return pdcAlterarArquivoServico;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarArquivoServico.
     * 
     * @param pdcAlterarArquivoServico
     *            Adaptador AlterarArquivoServico
     */
    public void setAlterarArquivoServicoPDCAdapter(IAlterarArquivoServicoPDCAdapter pdcAlterarArquivoServico) {
        this.pdcAlterarArquivoServico = pdcAlterarArquivoServico;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoCreditoConta.
     * 
     * @return Adaptador DetalharPagtoCreditoConta
     */
    public IDetalharPagtoCreditoContaPDCAdapter getDetalharPagtoCreditoContaPDCAdapter() {
        return pdcDetalharPagtoCreditoConta;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoCreditoConta.
     * 
     * @param pdcDetalharPagtoCreditoConta
     *            Adaptador DetalharPagtoCreditoConta
     */
    public void setDetalharPagtoCreditoContaPDCAdapter(IDetalharPagtoCreditoContaPDCAdapter pdcDetalharPagtoCreditoConta) {
        this.pdcDetalharPagtoCreditoConta = pdcDetalharPagtoCreditoConta;
    }
    /**
     * M�todo invocado para obter um adaptador VerificarAtributosServicoModalidade.
     * 
     * @return Adaptador VerificarAtributosServicoModalidade
     */
    public IVerificarAtributosServicoModalidadePDCAdapter getVerificarAtributosServicoModalidadePDCAdapter() {
        return pdcVerificarAtributosServicoModalidade;
    }

    /**
     * M�todo invocado para establecer um adaptador VerificarAtributosServicoModalidade.
     * 
     * @param pdcVerificarAtributosServicoModalidade
     *            Adaptador VerificarAtributosServicoModalidade
     */
    public void setVerificarAtributosServicoModalidadePDCAdapter(IVerificarAtributosServicoModalidadePDCAdapter pdcVerificarAtributosServicoModalidade) {
        this.pdcVerificarAtributosServicoModalidade = pdcVerificarAtributosServicoModalidade;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarListaPerfilTrocaArquivoLoop.
     * 
     * @return Adaptador ConsultarListaPerfilTrocaArquivoLoop
     */
    public IConsultarListaPerfilTrocaArquivoLoopPDCAdapter getConsultarListaPerfilTrocaArquivoLoopPDCAdapter() {
        return pdcConsultarListaPerfilTrocaArquivoLoop;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarListaPerfilTrocaArquivoLoop.
     * 
     * @param pdcConsultarListaPerfilTrocaArquivoLoop
     *            Adaptador ConsultarListaPerfilTrocaArquivoLoop
     */
    public void setConsultarListaPerfilTrocaArquivoLoopPDCAdapter(IConsultarListaPerfilTrocaArquivoLoopPDCAdapter pdcConsultarListaPerfilTrocaArquivoLoop) {
        this.pdcConsultarListaPerfilTrocaArquivoLoop = pdcConsultarListaPerfilTrocaArquivoLoop;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoProcessamentoLayoutArquivo.
     * 
     * @return Adaptador ListarTipoProcessamentoLayoutArquivo
     */
    public IListarTipoProcessamentoLayoutArquivoPDCAdapter getListarTipoProcessamentoLayoutArquivoPDCAdapter() {
        return pdcListarTipoProcessamentoLayoutArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoProcessamentoLayoutArquivo.
     * 
     * @param pdcListarTipoProcessamentoLayoutArquivo
     *            Adaptador ListarTipoProcessamentoLayoutArquivo
     */
    public void setListarTipoProcessamentoLayoutArquivoPDCAdapter(IListarTipoProcessamentoLayoutArquivoPDCAdapter pdcListarTipoProcessamentoLayoutArquivo) {
        this.pdcListarTipoProcessamentoLayoutArquivo = pdcListarTipoProcessamentoLayoutArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarTarifaContratoLoop.
     * 
     * @return Adaptador ConsultarTarifaContratoLoop
     */
    public IConsultarTarifaContratoLoopPDCAdapter getConsultarTarifaContratoLoopPDCAdapter() {
        return pdcConsultarTarifaContratoLoop;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarTarifaContratoLoop.
     * 
     * @param pdcConsultarTarifaContratoLoop
     *            Adaptador ConsultarTarifaContratoLoop
     */
    public void setConsultarTarifaContratoLoopPDCAdapter(IConsultarTarifaContratoLoopPDCAdapter pdcConsultarTarifaContratoLoop) {
        this.pdcConsultarTarifaContratoLoop = pdcConsultarTarifaContratoLoop;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarConfiguracaoLayoutArqContrato.
     * 
     * @return Adaptador AlterarConfiguracaoLayoutArqContrato
     */
    public IAlterarConfiguracaoLayoutArqContratoPDCAdapter getAlterarConfiguracaoLayoutArqContratoPDCAdapter() {
        return pdcAlterarConfiguracaoLayoutArqContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarConfiguracaoLayoutArqContrato.
     * 
     * @param pdcAlterarConfiguracaoLayoutArqContrato
     *            Adaptador AlterarConfiguracaoLayoutArqContrato
     */
    public void setAlterarConfiguracaoLayoutArqContratoPDCAdapter(IAlterarConfiguracaoLayoutArqContratoPDCAdapter pdcAlterarConfiguracaoLayoutArqContrato) {
        this.pdcAlterarConfiguracaoLayoutArqContrato = pdcAlterarConfiguracaoLayoutArqContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLayoutArquivoContrato.
     * 
     * @return Adaptador ConsultarLayoutArquivoContrato
     */
    public IConsultarLayoutArquivoContratoPDCAdapter getConsultarLayoutArquivoContratoPDCAdapter() {
        return pdcConsultarLayoutArquivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLayoutArquivoContrato.
     * 
     * @param pdcConsultarLayoutArquivoContrato
     *            Adaptador ConsultarLayoutArquivoContrato
     */
    public void setConsultarLayoutArquivoContratoPDCAdapter(IConsultarLayoutArquivoContratoPDCAdapter pdcConsultarLayoutArquivoContrato) {
        this.pdcConsultarLayoutArquivoContrato = pdcConsultarLayoutArquivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ValidarQtdDiasCobrancaApsApuracao.
     * 
     * @return Adaptador ValidarQtdDiasCobrancaApsApuracao
     */
    public IValidarQtdDiasCobrancaApsApuracaoPDCAdapter getValidarQtdDiasCobrancaApsApuracaoPDCAdapter() {
        return pdcValidarQtdDiasCobrancaApsApuracao;
    }

    /**
     * M�todo invocado para establecer um adaptador ValidarQtdDiasCobrancaApsApuracao.
     * 
     * @param pdcValidarQtdDiasCobrancaApsApuracao
     *            Adaptador ValidarQtdDiasCobrancaApsApuracao
     */
    public void setValidarQtdDiasCobrancaApsApuracaoPDCAdapter(IValidarQtdDiasCobrancaApsApuracaoPDCAdapter pdcValidarQtdDiasCobrancaApsApuracao) {
        this.pdcValidarQtdDiasCobrancaApsApuracao = pdcValidarQtdDiasCobrancaApsApuracao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarFaixaSalarialProposta.
     * 
     * @return Adaptador ListarFaixaSalarialProposta
     */
    public IListarFaixaSalarialPropostaPDCAdapter getListarFaixaSalarialPropostaPDCAdapter() {
        return pdcListarFaixaSalarialProposta;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarFaixaSalarialProposta.
     * 
     * @param pdcListarFaixaSalarialProposta
     *            Adaptador ListarFaixaSalarialProposta
     */
    public void setListarFaixaSalarialPropostaPDCAdapter(IListarFaixaSalarialPropostaPDCAdapter pdcListarFaixaSalarialProposta) {
        this.pdcListarFaixaSalarialProposta = pdcListarFaixaSalarialProposta;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoRelacionadoPgitCdps.
     * 
     * @return Adaptador ListarServicoRelacionadoPgitCdps
     */
    public IListarServicoRelacionadoPgitCdpsPDCAdapter getListarServicoRelacionadoPgitCdpsPDCAdapter() {
        return pdcListarServicoRelacionadoPgitCdps;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoRelacionadoPgitCdps.
     * 
     * @param pdcListarServicoRelacionadoPgitCdps
     *            Adaptador ListarServicoRelacionadoPgitCdps
     */
    public void setListarServicoRelacionadoPgitCdpsPDCAdapter(IListarServicoRelacionadoPgitCdpsPDCAdapter pdcListarServicoRelacionadoPgitCdps) {
        this.pdcListarServicoRelacionadoPgitCdps = pdcListarServicoRelacionadoPgitCdps;
    }
    /**
     * M�todo invocado para obter um adaptador ListarServicoRelacionadoPgit.
     * 
     * @return Adaptador ListarServicoRelacionadoPgit
     */
    public IListarServicoRelacionadoPgitPDCAdapter getListarServicoRelacionadoPgitPDCAdapter() {
        return pdcListarServicoRelacionadoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarServicoRelacionadoPgit.
     * 
     * @param pdcListarServicoRelacionadoPgit
     *            Adaptador ListarServicoRelacionadoPgit
     */
    public void setListarServicoRelacionadoPgitPDCAdapter(IListarServicoRelacionadoPgitPDCAdapter pdcListarServicoRelacionadoPgit) {
        this.pdcListarServicoRelacionadoPgit = pdcListarServicoRelacionadoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirAnexoSegundoContrato.
     * 
     * @return Adaptador ImprimirAnexoSegundoContrato
     */
    public IImprimirAnexoSegundoContratoPDCAdapter getImprimirAnexoSegundoContratoPDCAdapter() {
        return pdcImprimirAnexoSegundoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirAnexoSegundoContrato.
     * 
     * @param pdcImprimirAnexoSegundoContrato
     *            Adaptador ImprimirAnexoSegundoContrato
     */
    public void setImprimirAnexoSegundoContratoPDCAdapter(IImprimirAnexoSegundoContratoPDCAdapter pdcImprimirAnexoSegundoContrato) {
        this.pdcImprimirAnexoSegundoContrato = pdcImprimirAnexoSegundoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharVincMsgLancOper.
     * 
     * @return Adaptador DetalharVincMsgLancOper
     */
    public IDetalharVincMsgLancOperPDCAdapter getDetalharVincMsgLancOperPDCAdapter() {
        return pdcDetalharVincMsgLancOper;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharVincMsgLancOper.
     * 
     * @param pdcDetalharVincMsgLancOper
     *            Adaptador DetalharVincMsgLancOper
     */
    public void setDetalharVincMsgLancOperPDCAdapter(IDetalharVincMsgLancOperPDCAdapter pdcDetalharVincMsgLancOper) {
        this.pdcDetalharVincMsgLancOper = pdcDetalharVincMsgLancOper;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTiposPagto.
     * 
     * @return Adaptador ListarTiposPagto
     */
    public IListarTiposPagtoPDCAdapter getListarTiposPagtoPDCAdapter() {
        return pdcListarTiposPagto;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTiposPagto.
     * 
     * @param pdcListarTiposPagto
     *            Adaptador ListarTiposPagto
     */
    public void setListarTiposPagtoPDCAdapter(IListarTiposPagtoPDCAdapter pdcListarTiposPagto) {
        this.pdcListarTiposPagto = pdcListarTiposPagto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirVincMsgLancOper.
     * 
     * @return Adaptador IncluirVincMsgLancOper
     */
    public IIncluirVincMsgLancOperPDCAdapter getIncluirVincMsgLancOperPDCAdapter() {
        return pdcIncluirVincMsgLancOper;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirVincMsgLancOper.
     * 
     * @param pdcIncluirVincMsgLancOper
     *            Adaptador IncluirVincMsgLancOper
     */
    public void setIncluirVincMsgLancOperPDCAdapter(IIncluirVincMsgLancOperPDCAdapter pdcIncluirVincMsgLancOper) {
        this.pdcIncluirVincMsgLancOper = pdcIncluirVincMsgLancOper;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContratoMae.
     * 
     * @return Adaptador ListarContratoMae
     */
    public IListarContratoMaePDCAdapter getListarContratoMaePDCAdapter() {
        return pdcListarContratoMae;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContratoMae.
     * 
     * @param pdcListarContratoMae
     *            Adaptador ListarContratoMae
     */
    public void setListarContratoMaePDCAdapter(IListarContratoMaePDCAdapter pdcListarContratoMae) {
        this.pdcListarContratoMae = pdcListarContratoMae;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirVincTrocaArqParticipante.
     * 
     * @return Adaptador IncluirVincTrocaArqParticipante
     */
    public IIncluirVincTrocaArqParticipantePDCAdapter getIncluirVincTrocaArqParticipantePDCAdapter() {
        return pdcIncluirVincTrocaArqParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirVincTrocaArqParticipante.
     * 
     * @param pdcIncluirVincTrocaArqParticipante
     *            Adaptador IncluirVincTrocaArqParticipante
     */
    public void setIncluirVincTrocaArqParticipantePDCAdapter(IIncluirVincTrocaArqParticipantePDCAdapter pdcIncluirVincTrocaArqParticipante) {
        this.pdcIncluirVincTrocaArqParticipante = pdcIncluirVincTrocaArqParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirVincTrocaArqParticipante.
     * 
     * @return Adaptador ExcluirVincTrocaArqParticipante
     */
    public IExcluirVincTrocaArqParticipantePDCAdapter getExcluirVincTrocaArqParticipantePDCAdapter() {
        return pdcExcluirVincTrocaArqParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirVincTrocaArqParticipante.
     * 
     * @param pdcExcluirVincTrocaArqParticipante
     *            Adaptador ExcluirVincTrocaArqParticipante
     */
    public void setExcluirVincTrocaArqParticipantePDCAdapter(IExcluirVincTrocaArqParticipantePDCAdapter pdcExcluirVincTrocaArqParticipante) {
        this.pdcExcluirVincTrocaArqParticipante = pdcExcluirVincTrocaArqParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharVincTrocaArqParticipante.
     * 
     * @return Adaptador DetalharVincTrocaArqParticipante
     */
    public IDetalharVincTrocaArqParticipantePDCAdapter getDetalharVincTrocaArqParticipantePDCAdapter() {
        return pdcDetalharVincTrocaArqParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharVincTrocaArqParticipante.
     * 
     * @param pdcDetalharVincTrocaArqParticipante
     *            Adaptador DetalharVincTrocaArqParticipante
     */
    public void setDetalharVincTrocaArqParticipantePDCAdapter(IDetalharVincTrocaArqParticipantePDCAdapter pdcDetalharVincTrocaArqParticipante) {
        this.pdcDetalharVincTrocaArqParticipante = pdcDetalharVincTrocaArqParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistAssocTrocaArqParticipante.
     * 
     * @return Adaptador DetalharHistAssocTrocaArqParticipante
     */
    public IDetalharHistAssocTrocaArqParticipantePDCAdapter getDetalharHistAssocTrocaArqParticipantePDCAdapter() {
        return pdcDetalharHistAssocTrocaArqParticipante;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistAssocTrocaArqParticipante.
     * 
     * @param pdcDetalharHistAssocTrocaArqParticipante
     *            Adaptador DetalharHistAssocTrocaArqParticipante
     */
    public void setDetalharHistAssocTrocaArqParticipantePDCAdapter(IDetalharHistAssocTrocaArqParticipantePDCAdapter pdcDetalharHistAssocTrocaArqParticipante) {
        this.pdcDetalharHistAssocTrocaArqParticipante = pdcDetalharHistAssocTrocaArqParticipante;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarPerfilTrocaArquivo.
     * 
     * @return Adaptador AlterarPerfilTrocaArquivo
     */
    public IAlterarPerfilTrocaArquivoPDCAdapter getAlterarPerfilTrocaArquivoPDCAdapter() {
        return pdcAlterarPerfilTrocaArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarPerfilTrocaArquivo.
     * 
     * @param pdcAlterarPerfilTrocaArquivo
     *            Adaptador AlterarPerfilTrocaArquivo
     */
    public void setAlterarPerfilTrocaArquivoPDCAdapter(IAlterarPerfilTrocaArquivoPDCAdapter pdcAlterarPerfilTrocaArquivo) {
        this.pdcAlterarPerfilTrocaArquivo = pdcAlterarPerfilTrocaArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirPerfilTrocaArquivo.
     * 
     * @return Adaptador IncluirPerfilTrocaArquivo
     */
    public IIncluirPerfilTrocaArquivoPDCAdapter getIncluirPerfilTrocaArquivoPDCAdapter() {
        return pdcIncluirPerfilTrocaArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirPerfilTrocaArquivo.
     * 
     * @param pdcIncluirPerfilTrocaArquivo
     *            Adaptador IncluirPerfilTrocaArquivo
     */
    public void setIncluirPerfilTrocaArquivoPDCAdapter(IIncluirPerfilTrocaArquivoPDCAdapter pdcIncluirPerfilTrocaArquivo) {
        this.pdcIncluirPerfilTrocaArquivo = pdcIncluirPerfilTrocaArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistoricoPerfilTrocaArquivo.
     * 
     * @return Adaptador DetalharHistoricoPerfilTrocaArquivo
     */
    public IDetalharHistoricoPerfilTrocaArquivoPDCAdapter getDetalharHistoricoPerfilTrocaArquivoPDCAdapter() {
        return pdcDetalharHistoricoPerfilTrocaArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistoricoPerfilTrocaArquivo.
     * 
     * @param pdcDetalharHistoricoPerfilTrocaArquivo
     *            Adaptador DetalharHistoricoPerfilTrocaArquivo
     */
    public void setDetalharHistoricoPerfilTrocaArquivoPDCAdapter(IDetalharHistoricoPerfilTrocaArquivoPDCAdapter pdcDetalharHistoricoPerfilTrocaArquivo) {
        this.pdcDetalharHistoricoPerfilTrocaArquivo = pdcDetalharHistoricoPerfilTrocaArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharContratoMigradoHsbc.
     * 
     * @return Adaptador DetalharContratoMigradoHsbc
     */
    public IDetalharContratoMigradoHsbcPDCAdapter getDetalharContratoMigradoHsbcPDCAdapter() {
        return pdcDetalharContratoMigradoHsbc;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharContratoMigradoHsbc.
     * 
     * @param pdcDetalharContratoMigradoHsbc
     *            Adaptador DetalharContratoMigradoHsbc
     */
    public void setDetalharContratoMigradoHsbcPDCAdapter(IDetalharContratoMigradoHsbcPDCAdapter pdcDetalharContratoMigradoHsbc) {
        this.pdcDetalharContratoMigradoHsbc = pdcDetalharContratoMigradoHsbc;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParametroContrato.
     * 
     * @return Adaptador ListarParametroContrato
     */
    public IListarParametroContratoPDCAdapter getListarParametroContratoPDCAdapter() {
        return pdcListarParametroContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParametroContrato.
     * 
     * @param pdcListarParametroContrato
     *            Adaptador ListarParametroContrato
     */
    public void setListarParametroContratoPDCAdapter(IListarParametroContratoPDCAdapter pdcListarParametroContrato) {
        this.pdcListarParametroContrato = pdcListarParametroContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParametros.
     * 
     * @return Adaptador ListarParametros
     */
    public IListarParametrosPDCAdapter getListarParametrosPDCAdapter() {
        return pdcListarParametros;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParametros.
     * 
     * @param pdcListarParametros
     *            Adaptador ListarParametros
     */
    public void setListarParametrosPDCAdapter(IListarParametrosPDCAdapter pdcListarParametros) {
        this.pdcListarParametros = pdcListarParametros;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirParametroContrato.
     * 
     * @return Adaptador ExcluirParametroContrato
     */
    public IExcluirParametroContratoPDCAdapter getExcluirParametroContratoPDCAdapter() {
        return pdcExcluirParametroContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirParametroContrato.
     * 
     * @param pdcExcluirParametroContrato
     *            Adaptador ExcluirParametroContrato
     */
    public void setExcluirParametroContratoPDCAdapter(IExcluirParametroContratoPDCAdapter pdcExcluirParametroContrato) {
        this.pdcExcluirParametroContrato = pdcExcluirParametroContrato;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirParametroContrato.
     * 
     * @return Adaptador IncluirParametroContrato
     */
    public IIncluirParametroContratoPDCAdapter getIncluirParametroContratoPDCAdapter() {
        return pdcIncluirParametroContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirParametroContrato.
     * 
     * @param pdcIncluirParametroContrato
     *            Adaptador IncluirParametroContrato
     */
    public void setIncluirParametroContratoPDCAdapter(IIncluirParametroContratoPDCAdapter pdcIncluirParametroContrato) {
        this.pdcIncluirParametroContrato = pdcIncluirParametroContrato;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirVincContratoMaeContratoFilho.
     * 
     * @return Adaptador IncluirVincContratoMaeContratoFilho
     */
    public IIncluirVincContratoMaeContratoFilhoPDCAdapter getIncluirVincContratoMaeContratoFilhoPDCAdapter() {
        return pdcIncluirVincContratoMaeContratoFilho;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirVincContratoMaeContratoFilho.
     * 
     * @param pdcIncluirVincContratoMaeContratoFilho
     *            Adaptador IncluirVincContratoMaeContratoFilho
     */
    public void setIncluirVincContratoMaeContratoFilhoPDCAdapter(IIncluirVincContratoMaeContratoFilhoPDCAdapter pdcIncluirVincContratoMaeContratoFilho) {
        this.pdcIncluirVincContratoMaeContratoFilho = pdcIncluirVincContratoMaeContratoFilho;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharTipoRetornoLayout.
     * 
     * @return Adaptador DetalharTipoRetornoLayout
     */
    public IDetalharTipoRetornoLayoutPDCAdapter getDetalharTipoRetornoLayoutPDCAdapter() {
        return pdcDetalharTipoRetornoLayout;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharTipoRetornoLayout.
     * 
     * @param pdcDetalharTipoRetornoLayout
     *            Adaptador DetalharTipoRetornoLayout
     */
    public void setDetalharTipoRetornoLayoutPDCAdapter(IDetalharTipoRetornoLayoutPDCAdapter pdcDetalharTipoRetornoLayout) {
        this.pdcDetalharTipoRetornoLayout = pdcDetalharTipoRetornoLayout;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistoricoLayoutContrato.
     * 
     * @return Adaptador DetalharHistoricoLayoutContrato
     */
    public IDetalharHistoricoLayoutContratoPDCAdapter getDetalharHistoricoLayoutContratoPDCAdapter() {
        return pdcDetalharHistoricoLayoutContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistoricoLayoutContrato.
     * 
     * @param pdcDetalharHistoricoLayoutContrato
     *            Adaptador DetalharHistoricoLayoutContrato
     */
    public void setDetalharHistoricoLayoutContratoPDCAdapter(IDetalharHistoricoLayoutContratoPDCAdapter pdcDetalharHistoricoLayoutContrato) {
        this.pdcDetalharHistoricoLayoutContrato = pdcDetalharHistoricoLayoutContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLimiteConta.
     * 
     * @return Adaptador ConsultarLimiteConta
     */
    public IConsultarLimiteContaPDCAdapter getConsultarLimiteContaPDCAdapter() {
        return pdcConsultarLimiteConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLimiteConta.
     * 
     * @param pdcConsultarLimiteConta
     *            Adaptador ConsultarLimiteConta
     */
    public void setConsultarLimiteContaPDCAdapter(IConsultarLimiteContaPDCAdapter pdcConsultarLimiteConta) {
        this.pdcConsultarLimiteConta = pdcConsultarLimiteConta;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarLimiteHistorico.
     * 
     * @return Adaptador ConsultarLimiteHistorico
     */
    public IConsultarLimiteHistoricoPDCAdapter getConsultarLimiteHistoricoPDCAdapter() {
        return pdcConsultarLimiteHistorico;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarLimiteHistorico.
     * 
     * @param pdcConsultarLimiteHistorico
     *            Adaptador ConsultarLimiteHistorico
     */
    public void setConsultarLimiteHistoricoPDCAdapter(IConsultarLimiteHistoricoPDCAdapter pdcConsultarLimiteHistorico) {
        this.pdcConsultarLimiteHistorico = pdcConsultarLimiteHistorico;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPerfilTrocaArquivo.
     * 
     * @return Adaptador DetalharPerfilTrocaArquivo
     */
    public IDetalharPerfilTrocaArquivoPDCAdapter getDetalharPerfilTrocaArquivoPDCAdapter() {
        return pdcDetalharPerfilTrocaArquivo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPerfilTrocaArquivo.
     * 
     * @param pdcDetalharPerfilTrocaArquivo
     *            Adaptador DetalharPerfilTrocaArquivo
     */
    public void setDetalharPerfilTrocaArquivoPDCAdapter(IDetalharPerfilTrocaArquivoPDCAdapter pdcDetalharPerfilTrocaArquivo) {
        this.pdcDetalharPerfilTrocaArquivo = pdcDetalharPerfilTrocaArquivo;
    }
    /**
     * M�todo invocado para obter um adaptador ListarLayoutsContrato.
     * 
     * @return Adaptador ListarLayoutsContrato
     */
    public IListarLayoutsContratoPDCAdapter getListarLayoutsContratoPDCAdapter() {
        return pdcListarLayoutsContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarLayoutsContrato.
     * 
     * @param pdcListarLayoutsContrato
     *            Adaptador ListarLayoutsContrato
     */
    public void setListarLayoutsContratoPDCAdapter(IListarLayoutsContratoPDCAdapter pdcListarLayoutsContrato) {
        this.pdcListarLayoutsContrato = pdcListarLayoutsContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarManutencaoConfigContrato.
     * 
     * @return Adaptador ConsultarManutencaoConfigContrato
     */
    public IConsultarManutencaoConfigContratoPDCAdapter getConsultarManutencaoConfigContratoPDCAdapter() {
        return pdcConsultarManutencaoConfigContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarManutencaoConfigContrato.
     * 
     * @param pdcConsultarManutencaoConfigContrato
     *            Adaptador ConsultarManutencaoConfigContrato
     */
    public void setConsultarManutencaoConfigContratoPDCAdapter(IConsultarManutencaoConfigContratoPDCAdapter pdcConsultarManutencaoConfigContrato) {
        this.pdcConsultarManutencaoConfigContrato = pdcConsultarManutencaoConfigContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarManutencaoServicoContrato.
     * 
     * @return Adaptador ConsultarManutencaoServicoContrato
     */
    public IConsultarManutencaoServicoContratoPDCAdapter getConsultarManutencaoServicoContratoPDCAdapter() {
        return pdcConsultarManutencaoServicoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarManutencaoServicoContrato.
     * 
     * @param pdcConsultarManutencaoServicoContrato
     *            Adaptador ConsultarManutencaoServicoContrato
     */
    public void setConsultarManutencaoServicoContratoPDCAdapter(IConsultarManutencaoServicoContratoPDCAdapter pdcConsultarManutencaoServicoContrato) {
        this.pdcConsultarManutencaoServicoContrato = pdcConsultarManutencaoServicoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirContratoIntranet.
     * 
     * @return Adaptador ImprimirContratoIntranet
     */
    public IImprimirContratoIntranetPDCAdapter getImprimirContratoIntranetPDCAdapter() {
        return pdcImprimirContratoIntranet;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirContratoIntranet.
     * 
     * @param pdcImprimirContratoIntranet
     *            Adaptador ImprimirContratoIntranet
     */
    public void setImprimirContratoIntranetPDCAdapter(IImprimirContratoIntranetPDCAdapter pdcImprimirContratoIntranet) {
        this.pdcImprimirContratoIntranet = pdcImprimirContratoIntranet;
    }
    /**
     * M�todo invocado para obter um adaptador ListarComprovanteSalarialIntranet.
     * 
     * @return Adaptador ListarComprovanteSalarialIntranet
     */
    public IListarComprovanteSalarialIntranetPDCAdapter getListarComprovanteSalarialIntranetPDCAdapter() {
        return pdcListarComprovanteSalarialIntranet;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarComprovanteSalarialIntranet.
     * 
     * @param pdcListarComprovanteSalarialIntranet
     *            Adaptador ListarComprovanteSalarialIntranet
     */
    public void setListarComprovanteSalarialIntranetPDCAdapter(IListarComprovanteSalarialIntranetPDCAdapter pdcListarComprovanteSalarialIntranet) {
        this.pdcListarComprovanteSalarialIntranet = pdcListarComprovanteSalarialIntranet;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirAditivoContrato.
     * 
     * @return Adaptador IncluirAditivoContrato
     */
    public IIncluirAditivoContratoPDCAdapter getIncluirAditivoContratoPDCAdapter() {
        return pdcIncluirAditivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirAditivoContrato.
     * 
     * @param pdcIncluirAditivoContrato
     *            Adaptador IncluirAditivoContrato
     */
    public void setIncluirAditivoContratoPDCAdapter(IIncluirAditivoContratoPDCAdapter pdcIncluirAditivoContrato) {
        this.pdcIncluirAditivoContrato = pdcIncluirAditivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharAditivoContrato.
     * 
     * @return Adaptador DetalharAditivoContrato
     */
    public IDetalharAditivoContratoPDCAdapter getDetalharAditivoContratoPDCAdapter() {
        return pdcDetalharAditivoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharAditivoContrato.
     * 
     * @param pdcDetalharAditivoContrato
     *            Adaptador DetalharAditivoContrato
     */
    public void setDetalharAditivoContratoPDCAdapter(IDetalharAditivoContratoPDCAdapter pdcDetalharAditivoContrato) {
        this.pdcDetalharAditivoContrato = pdcDetalharAditivoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarAutorizantesNetEmpresa.
     * 
     * @return Adaptador ConsultarAutorizantesNetEmpresa
     */
    public IConsultarAutorizantesNetEmpresaPDCAdapter getConsultarAutorizantesNetEmpresaPDCAdapter() {
        return pdcConsultarAutorizantesNetEmpresa;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarAutorizantesNetEmpresa.
     * 
     * @param pdcConsultarAutorizantesNetEmpresa
     *            Adaptador ConsultarAutorizantesNetEmpresa
     */
    public void setConsultarAutorizantesNetEmpresaPDCAdapter(IConsultarAutorizantesNetEmpresaPDCAdapter pdcConsultarAutorizantesNetEmpresa) {
        this.pdcConsultarAutorizantesNetEmpresa = pdcConsultarAutorizantesNetEmpresa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarServicosCatalogo.
     * 
     * @return Adaptador ConsultarServicosCatalogo
     */
    public IConsultarServicosCatalogoPDCAdapter getConsultarServicosCatalogoPDCAdapter() {
        return pdcConsultarServicosCatalogo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarServicosCatalogo.
     * 
     * @param pdcConsultarServicosCatalogo
     *            Adaptador ConsultarServicosCatalogo
     */
    public void setConsultarServicosCatalogoPDCAdapter(IConsultarServicosCatalogoPDCAdapter pdcConsultarServicosCatalogo) {
        this.pdcConsultarServicosCatalogo = pdcConsultarServicosCatalogo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarAutenticacao.
     * 
     * @return Adaptador ConsultarAutenticacao
     */
    public IConsultarAutenticacaoPDCAdapter getConsultarAutenticacaoPDCAdapter() {
        return pdcConsultarAutenticacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarAutenticacao.
     * 
     * @param pdcConsultarAutenticacao
     *            Adaptador ConsultarAutenticacao
     */
    public void setConsultarAutenticacaoPDCAdapter(IConsultarAutenticacaoPDCAdapter pdcConsultarAutenticacao) {
        this.pdcConsultarAutenticacao = pdcConsultarAutenticacao;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDadosBasicoContrato.
     * 
     * @return Adaptador ConsultarDadosBasicoContrato
     */
    public IConsultarDadosBasicoContratoPDCAdapter getConsultarDadosBasicoContratoPDCAdapter() {
        return pdcConsultarDadosBasicoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDadosBasicoContrato.
     * 
     * @param pdcConsultarDadosBasicoContrato
     *            Adaptador ConsultarDadosBasicoContrato
     */
    public void setConsultarDadosBasicoContratoPDCAdapter(IConsultarDadosBasicoContratoPDCAdapter pdcConsultarDadosBasicoContrato) {
        this.pdcConsultarDadosBasicoContrato = pdcConsultarDadosBasicoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarDadosBasicoContrato.
     * 
     * @return Adaptador AlterarDadosBasicoContrato
     */
    public IAlterarDadosBasicoContratoPDCAdapter getAlterarDadosBasicoContratoPDCAdapter() {
        return pdcAlterarDadosBasicoContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarDadosBasicoContrato.
     * 
     * @param pdcAlterarDadosBasicoContrato
     *            Adaptador AlterarDadosBasicoContrato
     */
    public void setAlterarDadosBasicoContratoPDCAdapter(IAlterarDadosBasicoContratoPDCAdapter pdcAlterarDadosBasicoContrato) {
        this.pdcAlterarDadosBasicoContrato = pdcAlterarDadosBasicoContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConManDadosBasicos.
     * 
     * @return Adaptador ConsultarConManDadosBasicos
     */
    public IConsultarConManDadosBasicosPDCAdapter getConsultarConManDadosBasicosPDCAdapter() {
        return pdcConsultarConManDadosBasicos;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConManDadosBasicos.
     * 
     * @param pdcConsultarConManDadosBasicos
     *            Adaptador ConsultarConManDadosBasicos
     */
    public void setConsultarConManDadosBasicosPDCAdapter(IConsultarConManDadosBasicosPDCAdapter pdcConsultarConManDadosBasicos) {
        this.pdcConsultarConManDadosBasicos = pdcConsultarConManDadosBasicos;
    }
    /**
     * M�todo invocado para obter um adaptador VerificarAtributosDadosBasicos.
     * 
     * @return Adaptador VerificarAtributosDadosBasicos
     */
    public IVerificarAtributosDadosBasicosPDCAdapter getVerificarAtributosDadosBasicosPDCAdapter() {
        return pdcVerificarAtributosDadosBasicos;
    }

    /**
     * M�todo invocado para establecer um adaptador VerificarAtributosDadosBasicos.
     * 
     * @param pdcVerificarAtributosDadosBasicos
     *            Adaptador VerificarAtributosDadosBasicos
     */
    public void setVerificarAtributosDadosBasicosPDCAdapter(IVerificarAtributosDadosBasicosPDCAdapter pdcVerificarAtributosDadosBasicos) {
        this.pdcVerificarAtributosDadosBasicos = pdcVerificarAtributosDadosBasicos;
    }
    /**
     * M�todo invocado para obter um adaptador ConContratoMigrado.
     * 
     * @return Adaptador ConContratoMigrado
     */
    public IConContratoMigradoPDCAdapter getConContratoMigradoPDCAdapter() {
        return pdcConContratoMigrado;
    }

    /**
     * M�todo invocado para establecer um adaptador ConContratoMigrado.
     * 
     * @param pdcConContratoMigrado
     *            Adaptador ConContratoMigrado
     */
    public void setConContratoMigradoPDCAdapter(IConContratoMigradoPDCAdapter pdcConContratoMigrado) {
        this.pdcConContratoMigrado = pdcConContratoMigrado;
    }
    /**
     * M�todo invocado para obter um adaptador ConRelacaoConManutencoes.
     * 
     * @return Adaptador ConRelacaoConManutencoes
     */
    public IConRelacaoConManutencoesPDCAdapter getConRelacaoConManutencoesPDCAdapter() {
        return pdcConRelacaoConManutencoes;
    }

    /**
     * M�todo invocado para establecer um adaptador ConRelacaoConManutencoes.
     * 
     * @param pdcConRelacaoConManutencoes
     *            Adaptador ConRelacaoConManutencoes
     */
    public void setConRelacaoConManutencoesPDCAdapter(IConRelacaoConManutencoesPDCAdapter pdcConRelacaoConManutencoes) {
        this.pdcConRelacaoConManutencoes = pdcConRelacaoConManutencoes;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirComprovanteCodigoBarra.
     * 
     * @return Adaptador ImprimirComprovanteCodigoBarra
     */
    public IImprimirComprovanteCodigoBarraPDCAdapter getImprimirComprovanteCodigoBarraPDCAdapter() {
        return pdcImprimirComprovanteCodigoBarra;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirComprovanteCodigoBarra.
     * 
     * @param pdcImprimirComprovanteCodigoBarra
     *            Adaptador ImprimirComprovanteCodigoBarra
     */
    public void setImprimirComprovanteCodigoBarraPDCAdapter(IImprimirComprovanteCodigoBarraPDCAdapter pdcImprimirComprovanteCodigoBarra) {
        this.pdcImprimirComprovanteCodigoBarra = pdcImprimirComprovanteCodigoBarra;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarDetalhesSolicitacao.
     * 
     * @return Adaptador ConsultarDetalhesSolicitacao
     */
    public IConsultarDetalhesSolicitacaoPDCAdapter getConsultarDetalhesSolicitacaoPDCAdapter() {
        return pdcConsultarDetalhesSolicitacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarDetalhesSolicitacao.
     * 
     * @param pdcConsultarDetalhesSolicitacao
     *            Adaptador ConsultarDetalhesSolicitacao
     */
    public void setConsultarDetalhesSolicitacaoPDCAdapter(IConsultarDetalhesSolicitacaoPDCAdapter pdcConsultarDetalhesSolicitacao) {
        this.pdcConsultarDetalhesSolicitacao = pdcConsultarDetalhesSolicitacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParticipantesIntranet.
     * 
     * @return Adaptador ListarParticipantesIntranet
     */
    public IListarParticipantesIntranetPDCAdapter getListarParticipantesIntranetPDCAdapter() {
        return pdcListarParticipantesIntranet;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParticipantesIntranet.
     * 
     * @param pdcListarParticipantesIntranet
     *            Adaptador ListarParticipantesIntranet
     */
    public void setListarParticipantesIntranetPDCAdapter(IListarParticipantesIntranetPDCAdapter pdcListarParticipantesIntranet) {
        this.pdcListarParticipantesIntranet = pdcListarParticipantesIntranet;
    }
    /**
     * M�todo invocado para obter um adaptador ListarLiquidacaoPagamento.
     * 
     * @return Adaptador ListarLiquidacaoPagamento
     */
    public IListarLiquidacaoPagamentoPDCAdapter getListarLiquidacaoPagamentoPDCAdapter() {
        return pdcListarLiquidacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarLiquidacaoPagamento.
     * 
     * @param pdcListarLiquidacaoPagamento
     *            Adaptador ListarLiquidacaoPagamento
     */
    public void setListarLiquidacaoPagamentoPDCAdapter(IListarLiquidacaoPagamentoPDCAdapter pdcListarLiquidacaoPagamento) {
        this.pdcListarLiquidacaoPagamento = pdcListarLiquidacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharLiquidacaoPagamento.
     * 
     * @return Adaptador DetalharLiquidacaoPagamento
     */
    public IDetalharLiquidacaoPagamentoPDCAdapter getDetalharLiquidacaoPagamentoPDCAdapter() {
        return pdcDetalharLiquidacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharLiquidacaoPagamento.
     * 
     * @param pdcDetalharLiquidacaoPagamento
     *            Adaptador DetalharLiquidacaoPagamento
     */
    public void setDetalharLiquidacaoPagamentoPDCAdapter(IDetalharLiquidacaoPagamentoPDCAdapter pdcDetalharLiquidacaoPagamento) {
        this.pdcDetalharLiquidacaoPagamento = pdcDetalharLiquidacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarLiquidacaoPagamento.
     * 
     * @return Adaptador AlterarLiquidacaoPagamento
     */
    public IAlterarLiquidacaoPagamentoPDCAdapter getAlterarLiquidacaoPagamentoPDCAdapter() {
        return pdcAlterarLiquidacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarLiquidacaoPagamento.
     * 
     * @param pdcAlterarLiquidacaoPagamento
     *            Adaptador AlterarLiquidacaoPagamento
     */
    public void setAlterarLiquidacaoPagamentoPDCAdapter(IAlterarLiquidacaoPagamentoPDCAdapter pdcAlterarLiquidacaoPagamento) {
        this.pdcAlterarLiquidacaoPagamento = pdcAlterarLiquidacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirLiquidacaoPagamento.
     * 
     * @return Adaptador IncluirLiquidacaoPagamento
     */
    public IIncluirLiquidacaoPagamentoPDCAdapter getIncluirLiquidacaoPagamentoPDCAdapter() {
        return pdcIncluirLiquidacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirLiquidacaoPagamento.
     * 
     * @param pdcIncluirLiquidacaoPagamento
     *            Adaptador IncluirLiquidacaoPagamento
     */
    public void setIncluirLiquidacaoPagamentoPDCAdapter(IIncluirLiquidacaoPagamentoPDCAdapter pdcIncluirLiquidacaoPagamento) {
        this.pdcIncluirLiquidacaoPagamento = pdcIncluirLiquidacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistLiquidacaoPagamento.
     * 
     * @return Adaptador DetalharHistLiquidacaoPagamento
     */
    public IDetalharHistLiquidacaoPagamentoPDCAdapter getDetalharHistLiquidacaoPagamentoPDCAdapter() {
        return pdcDetalharHistLiquidacaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistLiquidacaoPagamento.
     * 
     * @param pdcDetalharHistLiquidacaoPagamento
     *            Adaptador DetalharHistLiquidacaoPagamento
     */
    public void setDetalharHistLiquidacaoPagamentoPDCAdapter(IDetalharHistLiquidacaoPagamentoPDCAdapter pdcDetalharHistLiquidacaoPagamento) {
        this.pdcDetalharHistLiquidacaoPagamento = pdcDetalharHistLiquidacaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoTED.
     * 
     * @return Adaptador DetalharPagtoTED
     */
    public IDetalharPagtoTEDPDCAdapter getDetalharPagtoTEDPDCAdapter() {
        return pdcDetalharPagtoTED;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoTED.
     * 
     * @param pdcDetalharPagtoTED
     *            Adaptador DetalharPagtoTED
     */
    public void setDetalharPagtoTEDPDCAdapter(IDetalharPagtoTEDPDCAdapter pdcDetalharPagtoTED) {
        this.pdcDetalharPagtoTED = pdcDetalharPagtoTED;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoDOC.
     * 
     * @return Adaptador DetalharManPagtoDOC
     */
    public IDetalharManPagtoDOCPDCAdapter getDetalharManPagtoDOCPDCAdapter() {
        return pdcDetalharManPagtoDOC;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoDOC.
     * 
     * @param pdcDetalharManPagtoDOC
     *            Adaptador DetalharManPagtoDOC
     */
    public void setDetalharManPagtoDOCPDCAdapter(IDetalharManPagtoDOCPDCAdapter pdcDetalharManPagtoDOC) {
        this.pdcDetalharManPagtoDOC = pdcDetalharManPagtoDOC;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoDOC.
     * 
     * @return Adaptador DetalharPagtoDOC
     */
    public IDetalharPagtoDOCPDCAdapter getDetalharPagtoDOCPDCAdapter() {
        return pdcDetalharPagtoDOC;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoDOC.
     * 
     * @param pdcDetalharPagtoDOC
     *            Adaptador DetalharPagtoDOC
     */
    public void setDetalharPagtoDOCPDCAdapter(IDetalharPagtoDOCPDCAdapter pdcDetalharPagtoDOC) {
        this.pdcDetalharPagtoDOC = pdcDetalharPagtoDOC;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharManPagtoTED.
     * 
     * @return Adaptador DetalharManPagtoTED
     */
    public IDetalharManPagtoTEDPDCAdapter getDetalharManPagtoTEDPDCAdapter() {
        return pdcDetalharManPagtoTED;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharManPagtoTED.
     * 
     * @param pdcDetalharManPagtoTED
     *            Adaptador DetalharManPagtoTED
     */
    public void setDetalharManPagtoTEDPDCAdapter(IDetalharManPagtoTEDPDCAdapter pdcDetalharManPagtoTED) {
        this.pdcDetalharManPagtoTED = pdcDetalharManPagtoTED;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarHistConsultaSaldo.
     * 
     * @return Adaptador ConsultarHistConsultaSaldo
     */
    public IConsultarHistConsultaSaldoPDCAdapter getConsultarHistConsultaSaldoPDCAdapter() {
        return pdcConsultarHistConsultaSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarHistConsultaSaldo.
     * 
     * @param pdcConsultarHistConsultaSaldo
     *            Adaptador ConsultarHistConsultaSaldo
     */
    public void setConsultarHistConsultaSaldoPDCAdapter(IConsultarHistConsultaSaldoPDCAdapter pdcConsultarHistConsultaSaldo) {
        this.pdcConsultarHistConsultaSaldo = pdcConsultarHistConsultaSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistConsultaSaldo.
     * 
     * @return Adaptador DetalharHistConsultaSaldo
     */
    public IDetalharHistConsultaSaldoPDCAdapter getDetalharHistConsultaSaldoPDCAdapter() {
        return pdcDetalharHistConsultaSaldo;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistConsultaSaldo.
     * 
     * @param pdcDetalharHistConsultaSaldo
     *            Adaptador DetalharHistConsultaSaldo
     */
    public void setDetalharHistConsultaSaldoPDCAdapter(IDetalharHistConsultaSaldoPDCAdapter pdcDetalharHistConsultaSaldo) {
        this.pdcDetalharHistConsultaSaldo = pdcDetalharHistConsultaSaldo;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarImprimirPgtoIndividual.
     * 
     * @return Adaptador ConsultarImprimirPgtoIndividual
     */
    public IConsultarImprimirPgtoIndividualPDCAdapter getConsultarImprimirPgtoIndividualPDCAdapter() {
        return pdcConsultarImprimirPgtoIndividual;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarImprimirPgtoIndividual.
     * 
     * @param pdcConsultarImprimirPgtoIndividual
     *            Adaptador ConsultarImprimirPgtoIndividual
     */
    public void setConsultarImprimirPgtoIndividualPDCAdapter(IConsultarImprimirPgtoIndividualPDCAdapter pdcConsultarImprimirPgtoIndividual) {
        this.pdcConsultarImprimirPgtoIndividual = pdcConsultarImprimirPgtoIndividual;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirPagamentosIndividuais.
     * 
     * @return Adaptador ImprimirPagamentosIndividuais
     */
    public IImprimirPagamentosIndividuaisPDCAdapter getImprimirPagamentosIndividuaisPDCAdapter() {
        return pdcImprimirPagamentosIndividuais;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirPagamentosIndividuais.
     * 
     * @param pdcImprimirPagamentosIndividuais
     *            Adaptador ImprimirPagamentosIndividuais
     */
    public void setImprimirPagamentosIndividuaisPDCAdapter(IImprimirPagamentosIndividuaisPDCAdapter pdcImprimirPagamentosIndividuais) {
        this.pdcImprimirPagamentosIndividuais = pdcImprimirPagamentosIndividuais;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarImprimirPgtoConsolidado.
     * 
     * @return Adaptador ConsultarImprimirPgtoConsolidado
     */
    public IConsultarImprimirPgtoConsolidadoPDCAdapter getConsultarImprimirPgtoConsolidadoPDCAdapter() {
        return pdcConsultarImprimirPgtoConsolidado;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarImprimirPgtoConsolidado.
     * 
     * @param pdcConsultarImprimirPgtoConsolidado
     *            Adaptador ConsultarImprimirPgtoConsolidado
     */
    public void setConsultarImprimirPgtoConsolidadoPDCAdapter(IConsultarImprimirPgtoConsolidadoPDCAdapter pdcConsultarImprimirPgtoConsolidado) {
        this.pdcConsultarImprimirPgtoConsolidado = pdcConsultarImprimirPgtoConsolidado;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirPagamentosConsolidados.
     * 
     * @return Adaptador ImprimirPagamentosConsolidados
     */
    public IImprimirPagamentosConsolidadosPDCAdapter getImprimirPagamentosConsolidadosPDCAdapter() {
        return pdcImprimirPagamentosConsolidados;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirPagamentosConsolidados.
     * 
     * @param pdcImprimirPagamentosConsolidados
     *            Adaptador ImprimirPagamentosConsolidados
     */
    public void setImprimirPagamentosConsolidadosPDCAdapter(IImprimirPagamentosConsolidadosPDCAdapter pdcImprimirPagamentosConsolidados) {
        this.pdcImprimirPagamentosConsolidados = pdcImprimirPagamentosConsolidados;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarManutencaoPagamento.
     * 
     * @return Adaptador ConsultarManutencaoPagamento
     */
    public IConsultarManutencaoPagamentoPDCAdapter getConsultarManutencaoPagamentoPDCAdapter() {
        return pdcConsultarManutencaoPagamento;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarManutencaoPagamento.
     * 
     * @param pdcConsultarManutencaoPagamento
     *            Adaptador ConsultarManutencaoPagamento
     */
    public void setConsultarManutencaoPagamentoPDCAdapter(IConsultarManutencaoPagamentoPDCAdapter pdcConsultarManutencaoPagamento) {
        this.pdcConsultarManutencaoPagamento = pdcConsultarManutencaoPagamento;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultaPostergarConsolidadoSolicitacao.
     * 
     * @return Adaptador ConsultaPostergarConsolidadoSolicitacao
     */
    public IConsultaPostergarConsolidadoSolicitacaoPDCAdapter getConsultaPostergarConsolidadoSolicitacaoPDCAdapter() {
        return pdcConsultaPostergarConsolidadoSolicitacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultaPostergarConsolidadoSolicitacao.
     * 
     * @param pdcConsultaPostergarConsolidadoSolicitacao
     *            Adaptador ConsultaPostergarConsolidadoSolicitacao
     */
    public void setConsultaPostergarConsolidadoSolicitacaoPDCAdapter(IConsultaPostergarConsolidadoSolicitacaoPDCAdapter pdcConsultaPostergarConsolidadoSolicitacao) {
        this.pdcConsultaPostergarConsolidadoSolicitacao = pdcConsultaPostergarConsolidadoSolicitacao;
    }
    /**
     * M�todo invocado para obter um adaptador DetalhePostergarConsolidadoSolicitacao.
     * 
     * @return Adaptador DetalhePostergarConsolidadoSolicitacao
     */
    public IDetalhePostergarConsolidadoSolicitacaoPDCAdapter getDetalhePostergarConsolidadoSolicitacaoPDCAdapter() {
        return pdcDetalhePostergarConsolidadoSolicitacao;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalhePostergarConsolidadoSolicitacao.
     * 
     * @param pdcDetalhePostergarConsolidadoSolicitacao
     *            Adaptador DetalhePostergarConsolidadoSolicitacao
     */
    public void setDetalhePostergarConsolidadoSolicitacaoPDCAdapter(IDetalhePostergarConsolidadoSolicitacaoPDCAdapter pdcDetalhePostergarConsolidadoSolicitacao) {
        this.pdcDetalhePostergarConsolidadoSolicitacao = pdcDetalhePostergarConsolidadoSolicitacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarPostergarConsolidadoSolicitacao.
     * 
     * @return Adaptador ListarPostergarConsolidadoSolicitacao
     */
    public IListarPostergarConsolidadoSolicitacaoPDCAdapter getListarPostergarConsolidadoSolicitacaoPDCAdapter() {
        return pdcListarPostergarConsolidadoSolicitacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarPostergarConsolidadoSolicitacao.
     * 
     * @param pdcListarPostergarConsolidadoSolicitacao
     *            Adaptador ListarPostergarConsolidadoSolicitacao
     */
    public void setListarPostergarConsolidadoSolicitacaoPDCAdapter(IListarPostergarConsolidadoSolicitacaoPDCAdapter pdcListarPostergarConsolidadoSolicitacao) {
        this.pdcListarPostergarConsolidadoSolicitacao = pdcListarPostergarConsolidadoSolicitacao;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirPostergarConsolidadoSolicitacao.
     * 
     * @return Adaptador ExcluirPostergarConsolidadoSolicitacao
     */
    public IExcluirPostergarConsolidadoSolicitacaoPDCAdapter getExcluirPostergarConsolidadoSolicitacaoPDCAdapter() {
        return pdcExcluirPostergarConsolidadoSolicitacao;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirPostergarConsolidadoSolicitacao.
     * 
     * @param pdcExcluirPostergarConsolidadoSolicitacao
     *            Adaptador ExcluirPostergarConsolidadoSolicitacao
     */
    public void setExcluirPostergarConsolidadoSolicitacaoPDCAdapter(IExcluirPostergarConsolidadoSolicitacaoPDCAdapter pdcExcluirPostergarConsolidadoSolicitacao) {
        this.pdcExcluirPostergarConsolidadoSolicitacao = pdcExcluirPostergarConsolidadoSolicitacao;
    }
    /**
     * M�todo invocado para obter um adaptador ListarParticipantesLoop.
     * 
     * @return Adaptador ListarParticipantesLoop
     */
    public IListarParticipantesLoopPDCAdapter getListarParticipantesLoopPDCAdapter() {
        return pdcListarParticipantesLoop;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarParticipantesLoop.
     * 
     * @param pdcListarParticipantesLoop
     *            Adaptador ListarParticipantesLoop
     */
    public void setListarParticipantesLoopPDCAdapter(IListarParticipantesLoopPDCAdapter pdcListarParticipantesLoop) {
        this.pdcListarParticipantesLoop = pdcListarParticipantesLoop;
    }
    /**
     * M�todo invocado para obter um adaptador ListarTipoRetirada.
     * 
     * @return Adaptador ListarTipoRetirada
     */
    public IListarTipoRetiradaPDCAdapter getListarTipoRetiradaPDCAdapter() {
        return pdcListarTipoRetirada;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarTipoRetirada.
     * 
     * @param pdcListarTipoRetirada
     *            Adaptador ListarTipoRetirada
     */
    public void setListarTipoRetiradaPDCAdapter(IListarTipoRetiradaPDCAdapter pdcListarTipoRetirada) {
        this.pdcListarTipoRetirada = pdcListarTipoRetirada;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarOrdemPagtoPorAgePagadora.
     * 
     * @return Adaptador ConsultarOrdemPagtoPorAgePagadora
     */
    public IConsultarOrdemPagtoPorAgePagadoraPDCAdapter getConsultarOrdemPagtoPorAgePagadoraPDCAdapter() {
        return pdcConsultarOrdemPagtoPorAgePagadora;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarOrdemPagtoPorAgePagadora.
     * 
     * @param pdcConsultarOrdemPagtoPorAgePagadora
     *            Adaptador ConsultarOrdemPagtoPorAgePagadora
     */
    public void setConsultarOrdemPagtoPorAgePagadoraPDCAdapter(IConsultarOrdemPagtoPorAgePagadoraPDCAdapter pdcConsultarOrdemPagtoPorAgePagadora) {
        this.pdcConsultarOrdemPagtoPorAgePagadora = pdcConsultarOrdemPagtoPorAgePagadora;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirOperacaoServicoPagtoIntegrado.
     * 
     * @return Adaptador IncluirOperacaoServicoPagtoIntegrado
     */
    public IIncluirOperacaoServicoPagtoIntegradoPDCAdapter getIncluirOperacaoServicoPagtoIntegradoPDCAdapter() {
        return pdcIncluirOperacaoServicoPagtoIntegrado;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirOperacaoServicoPagtoIntegrado.
     * 
     * @param pdcIncluirOperacaoServicoPagtoIntegrado
     *            Adaptador IncluirOperacaoServicoPagtoIntegrado
     */
    public void setIncluirOperacaoServicoPagtoIntegradoPDCAdapter(IIncluirOperacaoServicoPagtoIntegradoPDCAdapter pdcIncluirOperacaoServicoPagtoIntegrado) {
        this.pdcIncluirOperacaoServicoPagtoIntegrado = pdcIncluirOperacaoServicoPagtoIntegrado;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharOperacaoServicoPagtoIntegrado.
     * 
     * @return Adaptador DetalharOperacaoServicoPagtoIntegrado
     */
    public IDetalharOperacaoServicoPagtoIntegradoPDCAdapter getDetalharOperacaoServicoPagtoIntegradoPDCAdapter() {
        return pdcDetalharOperacaoServicoPagtoIntegrado;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharOperacaoServicoPagtoIntegrado.
     * 
     * @param pdcDetalharOperacaoServicoPagtoIntegrado
     *            Adaptador DetalharOperacaoServicoPagtoIntegrado
     */
    public void setDetalharOperacaoServicoPagtoIntegradoPDCAdapter(IDetalharOperacaoServicoPagtoIntegradoPDCAdapter pdcDetalharOperacaoServicoPagtoIntegrado) {
        this.pdcDetalharOperacaoServicoPagtoIntegrado = pdcDetalharOperacaoServicoPagtoIntegrado;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarOperacaoServicoPagtoIntegrado.
     * 
     * @return Adaptador AlterarOperacaoServicoPagtoIntegrado
     */
    public IAlterarOperacaoServicoPagtoIntegradoPDCAdapter getAlterarOperacaoServicoPagtoIntegradoPDCAdapter() {
        return pdcAlterarOperacaoServicoPagtoIntegrado;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarOperacaoServicoPagtoIntegrado.
     * 
     * @param pdcAlterarOperacaoServicoPagtoIntegrado
     *            Adaptador AlterarOperacaoServicoPagtoIntegrado
     */
    public void setAlterarOperacaoServicoPagtoIntegradoPDCAdapter(IAlterarOperacaoServicoPagtoIntegradoPDCAdapter pdcAlterarOperacaoServicoPagtoIntegrado) {
        this.pdcAlterarOperacaoServicoPagtoIntegrado = pdcAlterarOperacaoServicoPagtoIntegrado;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarTarifaCatalogo.
     * 
     * @return Adaptador ConsultarTarifaCatalogo
     */
    public IConsultarTarifaCatalogoPDCAdapter getConsultarTarifaCatalogoPDCAdapter() {
        return pdcConsultarTarifaCatalogo;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarTarifaCatalogo.
     * 
     * @param pdcConsultarTarifaCatalogo
     *            Adaptador ConsultarTarifaCatalogo
     */
    public void setConsultarTarifaCatalogoPDCAdapter(IConsultarTarifaCatalogoPDCAdapter pdcConsultarTarifaCatalogo) {
        this.pdcConsultarTarifaCatalogo = pdcConsultarTarifaCatalogo;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHistoricoOperacaoServicoPagtoIntegrado.
     * 
     * @return Adaptador DetalharHistoricoOperacaoServicoPagtoIntegrado
     */
    public IDetalharHistoricoOperacaoServicoPagtoIntegradoPDCAdapter getDetalharHistoricoOperacaoServicoPagtoIntegradoPDCAdapter() {
        return pdcDetalharHistoricoOperacaoServicoPagtoIntegrado;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHistoricoOperacaoServicoPagtoIntegrado.
     * 
     * @param pdcDetalharHistoricoOperacaoServicoPagtoIntegrado
     *            Adaptador DetalharHistoricoOperacaoServicoPagtoIntegrado
     */
    public void setDetalharHistoricoOperacaoServicoPagtoIntegradoPDCAdapter(IDetalharHistoricoOperacaoServicoPagtoIntegradoPDCAdapter pdcDetalharHistoricoOperacaoServicoPagtoIntegrado) {
        this.pdcDetalharHistoricoOperacaoServicoPagtoIntegrado = pdcDetalharHistoricoOperacaoServicoPagtoIntegrado;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHistoricoOperacaoServicoPagtoIntegrado.
     * 
     * @return Adaptador ListarHistoricoOperacaoServicoPagtoIntegrado
     */
    public IListarHistoricoOperacaoServicoPagtoIntegradoPDCAdapter getListarHistoricoOperacaoServicoPagtoIntegradoPDCAdapter() {
        return pdcListarHistoricoOperacaoServicoPagtoIntegrado;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHistoricoOperacaoServicoPagtoIntegrado.
     * 
     * @param pdcListarHistoricoOperacaoServicoPagtoIntegrado
     *            Adaptador ListarHistoricoOperacaoServicoPagtoIntegrado
     */
    public void setListarHistoricoOperacaoServicoPagtoIntegradoPDCAdapter(IListarHistoricoOperacaoServicoPagtoIntegradoPDCAdapter pdcListarHistoricoOperacaoServicoPagtoIntegrado) {
        this.pdcListarHistoricoOperacaoServicoPagtoIntegrado = pdcListarHistoricoOperacaoServicoPagtoIntegrado;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirParticipanteContratoPgit.
     * 
     * @return Adaptador IncluirParticipanteContratoPgit
     */
    public IIncluirParticipanteContratoPgitPDCAdapter getIncluirParticipanteContratoPgitPDCAdapter() {
        return pdcIncluirParticipanteContratoPgit;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirParticipanteContratoPgit.
     * 
     * @param pdcIncluirParticipanteContratoPgit
     *            Adaptador IncluirParticipanteContratoPgit
     */
    public void setIncluirParticipanteContratoPgitPDCAdapter(IIncluirParticipanteContratoPgitPDCAdapter pdcIncluirParticipanteContratoPgit) {
        this.pdcIncluirParticipanteContratoPgit = pdcIncluirParticipanteContratoPgit;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContasExclusao.
     * 
     * @return Adaptador ListarContasExclusao
     */
    public IListarContasExclusaoPDCAdapter getListarContasExclusaoPDCAdapter() {
        return pdcListarContasExclusao;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContasExclusao.
     * 
     * @param pdcListarContasExclusao
     *            Adaptador ListarContasExclusao
     */
    public void setListarContasExclusaoPDCAdapter(IListarContasExclusaoPDCAdapter pdcListarContasExclusao) {
        this.pdcListarContasExclusao = pdcListarContasExclusao;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirContaContrato.
     * 
     * @return Adaptador ExcluirContaContrato
     */
    public IExcluirContaContratoPDCAdapter getExcluirContaContratoPDCAdapter() {
        return pdcExcluirContaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirContaContrato.
     * 
     * @param pdcExcluirContaContrato
     *            Adaptador ExcluirContaContrato
     */
    public void setExcluirContaContratoPDCAdapter(IExcluirContaContratoPDCAdapter pdcExcluirContaContrato) {
        this.pdcExcluirContaContrato = pdcExcluirContaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContaComplementar.
     * 
     * @return Adaptador ListarContaComplementar
     */
    public IListarContaComplementarPDCAdapter getListarContaComplementarPDCAdapter() {
        return pdcListarContaComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContaComplementar.
     * 
     * @param pdcListarContaComplementar
     *            Adaptador ListarContaComplementar
     */
    public void setListarContaComplementarPDCAdapter(IListarContaComplementarPDCAdapter pdcListarContaComplementar) {
        this.pdcListarContaComplementar = pdcListarContaComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarParticipantesContrato.
     * 
     * @return Adaptador ConsultarParticipantesContrato
     */
    public IConsultarParticipantesContratoPDCAdapter getConsultarParticipantesContratoPDCAdapter() {
        return pdcConsultarParticipantesContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarParticipantesContrato.
     * 
     * @param pdcConsultarParticipantesContrato
     *            Adaptador ConsultarParticipantesContrato
     */
    public void setConsultarParticipantesContratoPDCAdapter(IConsultarParticipantesContratoPDCAdapter pdcConsultarParticipantesContrato) {
        this.pdcConsultarParticipantesContrato = pdcConsultarParticipantesContrato;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirContaComplementar.
     * 
     * @return Adaptador IncluirContaComplementar
     */
    public IIncluirContaComplementarPDCAdapter getIncluirContaComplementarPDCAdapter() {
        return pdcIncluirContaComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirContaComplementar.
     * 
     * @param pdcIncluirContaComplementar
     *            Adaptador IncluirContaComplementar
     */
    public void setIncluirContaComplementarPDCAdapter(IIncluirContaComplementarPDCAdapter pdcIncluirContaComplementar) {
        this.pdcIncluirContaComplementar = pdcIncluirContaComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirContaComplementar.
     * 
     * @return Adaptador ExcluirContaComplementar
     */
    public IExcluirContaComplementarPDCAdapter getExcluirContaComplementarPDCAdapter() {
        return pdcExcluirContaComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirContaComplementar.
     * 
     * @param pdcExcluirContaComplementar
     *            Adaptador ExcluirContaComplementar
     */
    public void setExcluirContaComplementarPDCAdapter(IExcluirContaComplementarPDCAdapter pdcExcluirContaComplementar) {
        this.pdcExcluirContaComplementar = pdcExcluirContaComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharContaComplementar.
     * 
     * @return Adaptador DetalharContaComplementar
     */
    public IDetalharContaComplementarPDCAdapter getDetalharContaComplementarPDCAdapter() {
        return pdcDetalharContaComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharContaComplementar.
     * 
     * @param pdcDetalharContaComplementar
     *            Adaptador DetalharContaComplementar
     */
    public void setDetalharContaComplementarPDCAdapter(IDetalharContaComplementarPDCAdapter pdcDetalharContaComplementar) {
        this.pdcDetalharContaComplementar = pdcDetalharContaComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarContaComplementar.
     * 
     * @return Adaptador AlterarContaComplementar
     */
    public IAlterarContaComplementarPDCAdapter getAlterarContaComplementarPDCAdapter() {
        return pdcAlterarContaComplementar;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarContaComplementar.
     * 
     * @param pdcAlterarContaComplementar
     *            Adaptador AlterarContaComplementar
     */
    public void setAlterarContaComplementarPDCAdapter(IAlterarContaComplementarPDCAdapter pdcAlterarContaComplementar) {
        this.pdcAlterarContaComplementar = pdcAlterarContaComplementar;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContasRelacionadasConta.
     * 
     * @return Adaptador ListarContasRelacionadasConta
     */
    public IListarContasRelacionadasContaPDCAdapter getListarContasRelacionadasContaPDCAdapter() {
        return pdcListarContasRelacionadasConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContasRelacionadasConta.
     * 
     * @param pdcListarContasRelacionadasConta
     *            Adaptador ListarContasRelacionadasConta
     */
    public void setListarContasRelacionadasContaPDCAdapter(IListarContasRelacionadasContaPDCAdapter pdcListarContasRelacionadasConta) {
        this.pdcListarContasRelacionadasConta = pdcListarContasRelacionadasConta;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContasRelacionadasParaContrato.
     * 
     * @return Adaptador ListarContasRelacionadasParaContrato
     */
    public IListarContasRelacionadasParaContratoPDCAdapter getListarContasRelacionadasParaContratoPDCAdapter() {
        return pdcListarContasRelacionadasParaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContasRelacionadasParaContrato.
     * 
     * @param pdcListarContasRelacionadasParaContrato
     *            Adaptador ListarContasRelacionadasParaContrato
     */
    public void setListarContasRelacionadasParaContratoPDCAdapter(IListarContasRelacionadasParaContratoPDCAdapter pdcListarContasRelacionadasParaContrato) {
        this.pdcListarContasRelacionadasParaContrato = pdcListarContasRelacionadasParaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirRelaciAssoc.
     * 
     * @return Adaptador ExcluirRelaciAssoc
     */
    public IExcluirRelaciAssocPDCAdapter getExcluirRelaciAssocPDCAdapter() {
        return pdcExcluirRelaciAssoc;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirRelaciAssoc.
     * 
     * @param pdcExcluirRelaciAssoc
     *            Adaptador ExcluirRelaciAssoc
     */
    public void setExcluirRelaciAssocPDCAdapter(IExcluirRelaciAssocPDCAdapter pdcExcluirRelaciAssoc) {
        this.pdcExcluirRelaciAssoc = pdcExcluirRelaciAssoc;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharPagtoOrdemPagto.
     * 
     * @return Adaptador DetalharPagtoOrdemPagto
     */
    public IDetalharPagtoOrdemPagtoPDCAdapter getDetalharPagtoOrdemPagtoPDCAdapter() {
        return pdcDetalharPagtoOrdemPagto;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharPagtoOrdemPagto.
     * 
     * @param pdcDetalharPagtoOrdemPagto
     *            Adaptador DetalharPagtoOrdemPagto
     */
    public void setDetalharPagtoOrdemPagtoPDCAdapter(IDetalharPagtoOrdemPagtoPDCAdapter pdcDetalharPagtoOrdemPagto) {
        this.pdcDetalharPagtoOrdemPagto = pdcDetalharPagtoOrdemPagto;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirRelacContaContrato.
     * 
     * @return Adaptador IncluirRelacContaContrato
     */
    public IIncluirRelacContaContratoPDCAdapter getIncluirRelacContaContratoPDCAdapter() {
        return pdcIncluirRelacContaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirRelacContaContrato.
     * 
     * @param pdcIncluirRelacContaContrato
     *            Adaptador IncluirRelacContaContrato
     */
    public void setIncluirRelacContaContratoPDCAdapter(IIncluirRelacContaContratoPDCAdapter pdcIncluirRelacContaContrato) {
        this.pdcIncluirRelacContaContrato = pdcIncluirRelacContaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador SubstituirRelacContaContrato.
     * 
     * @return Adaptador SubstituirRelacContaContrato
     */
    public ISubstituirRelacContaContratoPDCAdapter getSubstituirRelacContaContratoPDCAdapter() {
        return pdcSubstituirRelacContaContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador SubstituirRelacContaContrato.
     * 
     * @param pdcSubstituirRelacContaContrato
     *            Adaptador SubstituirRelacContaContrato
     */
    public void setSubstituirRelacContaContratoPDCAdapter(ISubstituirRelacContaContratoPDCAdapter pdcSubstituirRelacContaContrato) {
        this.pdcSubstituirRelacContaContrato = pdcSubstituirRelacContaContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContasVinculadasContrato.
     * 
     * @return Adaptador ListarContasVinculadasContrato
     */
    public IListarContasVinculadasContratoPDCAdapter getListarContasVinculadasContratoPDCAdapter() {
        return pdcListarContasVinculadasContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContasVinculadasContrato.
     * 
     * @param pdcListarContasVinculadasContrato
     *            Adaptador ListarContasVinculadasContrato
     */
    public void setListarContasVinculadasContratoPDCAdapter(IListarContasVinculadasContratoPDCAdapter pdcListarContasVinculadasContrato) {
        this.pdcListarContasVinculadasContrato = pdcListarContasVinculadasContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContasVinculadasContratoLoop.
     * 
     * @return Adaptador ListarContasVinculadasContratoLoop
     */
    public IListarContasVinculadasContratoLoopPDCAdapter getListarContasVinculadasContratoLoopPDCAdapter() {
        return pdcListarContasVinculadasContratoLoop;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContasVinculadasContratoLoop.
     * 
     * @param pdcListarContasVinculadasContratoLoop
     *            Adaptador ListarContasVinculadasContratoLoop
     */
    public void setListarContasVinculadasContratoLoopPDCAdapter(IListarContasVinculadasContratoLoopPDCAdapter pdcListarContasVinculadasContratoLoop) {
        this.pdcListarContasVinculadasContratoLoop = pdcListarContasVinculadasContratoLoop;
    }
    /**
     * M�todo invocado para obter um adaptador ListarContasRelacionadasHist.
     * 
     * @return Adaptador ListarContasRelacionadasHist
     */
    public IListarContasRelacionadasHistPDCAdapter getListarContasRelacionadasHistPDCAdapter() {
        return pdcListarContasRelacionadasHist;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarContasRelacionadasHist.
     * 
     * @param pdcListarContasRelacionadasHist
     *            Adaptador ListarContasRelacionadasHist
     */
    public void setListarContasRelacionadasHistPDCAdapter(IListarContasRelacionadasHistPDCAdapter pdcListarContasRelacionadasHist) {
        this.pdcListarContasRelacionadasHist = pdcListarContasRelacionadasHist;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharContaRelacionadaHist.
     * 
     * @return Adaptador DetalharContaRelacionadaHist
     */
    public IDetalharContaRelacionadaHistPDCAdapter getDetalharContaRelacionadaHistPDCAdapter() {
        return pdcDetalharContaRelacionadaHist;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharContaRelacionadaHist.
     * 
     * @param pdcDetalharContaRelacionadaHist
     *            Adaptador DetalharContaRelacionadaHist
     */
    public void setDetalharContaRelacionadaHistPDCAdapter(IDetalharContaRelacionadaHistPDCAdapter pdcDetalharContaRelacionadaHist) {
        this.pdcDetalharContaRelacionadaHist = pdcDetalharContaRelacionadaHist;
    }
    /**
     * M�todo invocado para obter um adaptador IncluirPagamentoSalario.
     * 
     * @return Adaptador IncluirPagamentoSalario
     */
    public IIncluirPagamentoSalarioPDCAdapter getIncluirPagamentoSalarioPDCAdapter() {
        return pdcIncluirPagamentoSalario;
    }

    /**
     * M�todo invocado para establecer um adaptador IncluirPagamentoSalario.
     * 
     * @param pdcIncluirPagamentoSalario
     *            Adaptador IncluirPagamentoSalario
     */
    public void setIncluirPagamentoSalarioPDCAdapter(IIncluirPagamentoSalarioPDCAdapter pdcIncluirPagamentoSalario) {
        this.pdcIncluirPagamentoSalario = pdcIncluirPagamentoSalario;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharTipoServModContrato.
     * 
     * @return Adaptador DetalharTipoServModContrato
     */
    public IDetalharTipoServModContratoPDCAdapter getDetalharTipoServModContratoPDCAdapter() {
        return pdcDetalharTipoServModContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharTipoServModContrato.
     * 
     * @param pdcDetalharTipoServModContrato
     *            Adaptador DetalharTipoServModContrato
     */
    public void setDetalharTipoServModContratoPDCAdapter(IDetalharTipoServModContratoPDCAdapter pdcDetalharTipoServModContrato) {
        this.pdcDetalharTipoServModContrato = pdcDetalharTipoServModContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConManServicos.
     * 
     * @return Adaptador ConsultarConManServicos
     */
    public IConsultarConManServicosPDCAdapter getConsultarConManServicosPDCAdapter() {
        return pdcConsultarConManServicos;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConManServicos.
     * 
     * @param pdcConsultarConManServicos
     *            Adaptador ConsultarConManServicos
     */
    public void setConsultarConManServicosPDCAdapter(IConsultarConManServicosPDCAdapter pdcConsultarConManServicos) {
        this.pdcConsultarConManServicos = pdcConsultarConManServicos;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarConfiguracaoTipoServModContrato.
     * 
     * @return Adaptador AlterarConfiguracaoTipoServModContrato
     */
    public IAlterarConfiguracaoTipoServModContratoPDCAdapter getAlterarConfiguracaoTipoServModContratoPDCAdapter() {
        return pdcAlterarConfiguracaoTipoServModContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarConfiguracaoTipoServModContrato.
     * 
     * @param pdcAlterarConfiguracaoTipoServModContrato
     *            Adaptador AlterarConfiguracaoTipoServModContrato
     */
    public void setAlterarConfiguracaoTipoServModContratoPDCAdapter(IAlterarConfiguracaoTipoServModContratoPDCAdapter pdcAlterarConfiguracaoTipoServModContrato) {
        this.pdcAlterarConfiguracaoTipoServModContrato = pdcAlterarConfiguracaoTipoServModContrato;
    }
    /**
     * M�todo invocado para obter um adaptador AlterarHorarioLimite.
     * 
     * @return Adaptador AlterarHorarioLimite
     */
    public IAlterarHorarioLimitePDCAdapter getAlterarHorarioLimitePDCAdapter() {
        return pdcAlterarHorarioLimite;
    }

    /**
     * M�todo invocado para establecer um adaptador AlterarHorarioLimite.
     * 
     * @param pdcAlterarHorarioLimite
     *            Adaptador AlterarHorarioLimite
     */
    public void setAlterarHorarioLimitePDCAdapter(IAlterarHorarioLimitePDCAdapter pdcAlterarHorarioLimite) {
        this.pdcAlterarHorarioLimite = pdcAlterarHorarioLimite;
    }
    /**
     * M�todo invocado para obter um adaptador ListarHorarioLimite.
     * 
     * @return Adaptador ListarHorarioLimite
     */
    public IListarHorarioLimitePDCAdapter getListarHorarioLimitePDCAdapter() {
        return pdcListarHorarioLimite;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarHorarioLimite.
     * 
     * @param pdcListarHorarioLimite
     *            Adaptador ListarHorarioLimite
     */
    public void setListarHorarioLimitePDCAdapter(IListarHorarioLimitePDCAdapter pdcListarHorarioLimite) {
        this.pdcListarHorarioLimite = pdcListarHorarioLimite;
    }
    /**
     * M�todo invocado para obter um adaptador DetalharHorarioLimite.
     * 
     * @return Adaptador DetalharHorarioLimite
     */
    public IDetalharHorarioLimitePDCAdapter getDetalharHorarioLimitePDCAdapter() {
        return pdcDetalharHorarioLimite;
    }

    /**
     * M�todo invocado para establecer um adaptador DetalharHorarioLimite.
     * 
     * @param pdcDetalharHorarioLimite
     *            Adaptador DetalharHorarioLimite
     */
    public void setDetalharHorarioLimitePDCAdapter(IDetalharHorarioLimitePDCAdapter pdcDetalharHorarioLimite) {
        this.pdcDetalharHorarioLimite = pdcDetalharHorarioLimite;
    }
    /**
     * M�todo invocado para obter um adaptador ExcluirHorarioLimite.
     * 
     * @return Adaptador ExcluirHorarioLimite
     */
    public IExcluirHorarioLimitePDCAdapter getExcluirHorarioLimitePDCAdapter() {
        return pdcExcluirHorarioLimite;
    }

    /**
     * M�todo invocado para establecer um adaptador ExcluirHorarioLimite.
     * 
     * @param pdcExcluirHorarioLimite
     *            Adaptador ExcluirHorarioLimite
     */
    public void setExcluirHorarioLimitePDCAdapter(IExcluirHorarioLimitePDCAdapter pdcExcluirHorarioLimite) {
        this.pdcExcluirHorarioLimite = pdcExcluirHorarioLimite;
    }
    /**
     * M�todo invocado para obter um adaptador ListarVincConvnCtaSalario.
     * 
     * @return Adaptador ListarVincConvnCtaSalario
     */
    public IListarVincConvnCtaSalarioPDCAdapter getListarVincConvnCtaSalarioPDCAdapter() {
        return pdcListarVincConvnCtaSalario;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarVincConvnCtaSalario.
     * 
     * @param pdcListarVincConvnCtaSalario
     *            Adaptador ListarVincConvnCtaSalario
     */
    public void setListarVincConvnCtaSalarioPDCAdapter(IListarVincConvnCtaSalarioPDCAdapter pdcListarVincConvnCtaSalario) {
        this.pdcListarVincConvnCtaSalario = pdcListarVincConvnCtaSalario;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConvenioContaSalario.
     * 
     * @return Adaptador ListarConvenioContaSalario
     */
    public IListarConvenioContaSalarioPDCAdapter getListarConvenioContaSalarioPDCAdapter() {
        return pdcListarConvenioContaSalario;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConvenioContaSalario.
     * 
     * @param pdcListarConvenioContaSalario
     *            Adaptador ListarConvenioContaSalario
     */
    public void setListarConvenioContaSalarioPDCAdapter(IListarConvenioContaSalarioPDCAdapter pdcListarConvenioContaSalario) {
        this.pdcListarConvenioContaSalario = pdcListarConvenioContaSalario;
    }
    /**
     * M�todo invocado para obter um adaptador ListarDadosCtaConvn.
     * 
     * @return Adaptador ListarDadosCtaConvn
     */
    public IListarDadosCtaConvnPDCAdapter getListarDadosCtaConvnPDCAdapter() {
        return pdcListarDadosCtaConvn;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarDadosCtaConvn.
     * 
     * @param pdcListarDadosCtaConvn
     *            Adaptador ListarDadosCtaConvn
     */
    public void setListarDadosCtaConvnPDCAdapter(IListarDadosCtaConvnPDCAdapter pdcListarDadosCtaConvn) {
        this.pdcListarDadosCtaConvn = pdcListarDadosCtaConvn;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirAnexoPrimeiroContrato.
     * 
     * @return Adaptador ImprimirAnexoPrimeiroContrato
     */
    public IImprimirAnexoPrimeiroContratoPDCAdapter getImprimirAnexoPrimeiroContratoPDCAdapter() {
        return pdcImprimirAnexoPrimeiroContrato;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirAnexoPrimeiroContrato.
     * 
     * @param pdcImprimirAnexoPrimeiroContrato
     *            Adaptador ImprimirAnexoPrimeiroContrato
     */
    public void setImprimirAnexoPrimeiroContratoPDCAdapter(IImprimirAnexoPrimeiroContratoPDCAdapter pdcImprimirAnexoPrimeiroContrato) {
        this.pdcImprimirAnexoPrimeiroContrato = pdcImprimirAnexoPrimeiroContrato;
    }
    /**
     * M�todo invocado para obter um adaptador ImprimirAnexoPrimeiroContratoLoop.
     * 
     * @return Adaptador ImprimirAnexoPrimeiroContratoLoop
     */
    public IImprimirAnexoPrimeiroContratoLoopPDCAdapter getImprimirAnexoPrimeiroContratoLoopPDCAdapter() {
        return pdcImprimirAnexoPrimeiroContratoLoop;
    }

    /**
     * M�todo invocado para establecer um adaptador ImprimirAnexoPrimeiroContratoLoop.
     * 
     * @param pdcImprimirAnexoPrimeiroContratoLoop
     *            Adaptador ImprimirAnexoPrimeiroContratoLoop
     */
    public void setImprimirAnexoPrimeiroContratoLoopPDCAdapter(IImprimirAnexoPrimeiroContratoLoopPDCAdapter pdcImprimirAnexoPrimeiroContratoLoop) {
        this.pdcImprimirAnexoPrimeiroContratoLoop = pdcImprimirAnexoPrimeiroContratoLoop;
    }
    /**
     * M�todo invocado para obter um adaptador ConfCestaTarifa.
     * 
     * @return Adaptador ConfCestaTarifa
     */
    public IConfCestaTarifaPDCAdapter getConfCestaTarifaPDCAdapter() {
        return pdcConfCestaTarifa;
    }

    /**
     * M�todo invocado para establecer um adaptador ConfCestaTarifa.
     * 
     * @param pdcConfCestaTarifa
     *            Adaptador ConfCestaTarifa
     */
    public void setConfCestaTarifaPDCAdapter(IConfCestaTarifaPDCAdapter pdcConfCestaTarifa) {
        this.pdcConfCestaTarifa = pdcConfCestaTarifa;
    }
    /**
     * M�todo invocado para obter um adaptador ConsultarConManConta.
     * 
     * @return Adaptador ConsultarConManConta
     */
    public IConsultarConManContaPDCAdapter getConsultarConManContaPDCAdapter() {
        return pdcConsultarConManConta;
    }

    /**
     * M�todo invocado para establecer um adaptador ConsultarConManConta.
     * 
     * @param pdcConsultarConManConta
     *            Adaptador ConsultarConManConta
     */
    public void setConsultarConManContaPDCAdapter(IConsultarConManContaPDCAdapter pdcConsultarConManConta) {
        this.pdcConsultarConManConta = pdcConsultarConManConta;
    }
    /**
     * M�todo invocado para obter um adaptador ListarConvenioContaSalarioNovo.
     * 
     * @return Adaptador ListarConvenioContaSalarioNovo
     */
    public IListarConvenioContaSalarioNovoPDCAdapter getListarConvenioContaSalarioNovoPDCAdapter() {
        return pdcListarConvenioContaSalarioNovo;
    }

    /**
     * M�todo invocado para establecer um adaptador ListarConvenioContaSalarioNovo.
     * 
     * @param pdcListarConvenioContaSalarioNovo
     *            Adaptador ListarConvenioContaSalarioNovo
     */
    public void setListarConvenioContaSalarioNovoPDCAdapter(IListarConvenioContaSalarioNovoPDCAdapter pdcListarConvenioContaSalarioNovo) {
        this.pdcListarConvenioContaSalarioNovo = pdcListarConvenioContaSalarioNovo;
    }
    
    /**
     * Construtor.
     * 
     */
    private FactoryAdapter() {

    };

    /**
     * M�todo utilizado para obter uma inst�ncia da F�brica.
     * 
     * @return Objeto FactoryAdapter (a f�brica de adaptadores).
     */
    public static FactoryAdapter getInstance() {
        return new FactoryAdapter();
    }
}
