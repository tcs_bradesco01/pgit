package br.com.bradesco.web.pgit.service.business.confcestastarifas;

import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.ConfCestaTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.ConfCestaTarifaSaidaDTO;

public interface IConfCestaTarifaService {
	
	ConfCestaTarifaSaidaDTO consultarConfCestaTarifa(ConfCestaTarifaEntradaDTO entrada);

}
