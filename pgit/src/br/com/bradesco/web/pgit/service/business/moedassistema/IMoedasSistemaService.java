/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.moedassistema;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterMoedasSistema
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IMoedasSistemaService {

    
	/**
	 * Detalhar moeda sistema.
	 *
	 * @param moedaSistemaEntradaDTO the moeda sistema entrada dto
	 * @return the moeda sistema saida dto
	 */
	MoedaSistemaSaidaDTO detalharMoedaSistema(MoedaSistemaEntradaDTO moedaSistemaEntradaDTO);
	
	/**
	 * Listar moeda sistema.
	 *
	 * @param moedaSistemaEntradaDTO the moeda sistema entrada dto
	 * @return the list< moeda sistema saida dt o>
	 */
	List<MoedaSistemaSaidaDTO> listarMoedaSistema(MoedaSistemaEntradaDTO moedaSistemaEntradaDTO);
	
	/**
	 * Excluir moeda sistema.
	 *
	 * @param moedaSistemaEntradaDTO the moeda sistema entrada dto
	 * @return the moeda sistema saida dto
	 */
	MoedaSistemaSaidaDTO excluirMoedaSistema(MoedaSistemaEntradaDTO moedaSistemaEntradaDTO);
	
	/**
	 * Incluir moeda sistema.
	 *
	 * @param moedaSistemaEntradaDTO the moeda sistema entrada dto
	 * @return the moeda sistema saida dto
	 */
	MoedaSistemaSaidaDTO incluirMoedaSistema(MoedaSistemaEntradaDTO moedaSistemaEntradaDTO);
	 
}

