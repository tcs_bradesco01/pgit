/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;


/**
 * Nome: ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO {

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdClub. */
    private Long cdClub;

    /** Atributo cdCpfCnpj. */
    private Long cdCpfCnpj;

    /** Atributo cdCnpjContrato. */
    private Integer cdCnpjContrato;

    /** Atributo cdCpfCnpjDigito. */
    private Integer cdCpfCnpjDigito;

    /** Atributo cdRazaoSocial. */
    private String cdRazaoSocial;

    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;

    /** Atributo dsTipoParticipante. */
    private String dsTipoParticipante;

    /** Atributo cdDescricaoSituacaoParticapante. */
    private String cdDescricaoSituacaoParticapante;

    /** Atributo dsTipoServico. */
    private String dsTipoServico;

    /** Atributo dsModalidadeServico. */
    private String dsModalidadeServico;

    /** Atributo dsOperacaoCatalogo. */
    private String dsOperacaoCatalogo;

    /** Atributo cdFinalidadeEnderecoContrato. */
    private Integer cdFinalidadeEnderecoContrato;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo cdEspeceEndereco. */
    private Integer cdEspeceEndereco;

    /** Atributo nrSequenciaEndereco. */
    private Integer nrSequenciaEndereco;

    /** Atributo cdUsoEnderecoPessoa. */
    private Integer cdUsoEnderecoPessoa;

    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
        this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
        return this.cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set: cdClub.
     *
     * @param cdClub the cd club
     */
    public void setCdClub(Long cdClub) {
        this.cdClub = cdClub;
    }

    /**
     * Get: cdClub.
     *
     * @return cdClub
     */
    public Long getCdClub() {
        return this.cdClub;
    }

    /**
     * Set: cdCpfCnpj.
     *
     * @param cdCpfCnpj the cd cpf cnpj
     */
    public void setCdCpfCnpj(Long cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    /**
     * Get: cdCpfCnpj.
     *
     * @return cdCpfCnpj
     */
    public Long getCdCpfCnpj() {
        return this.cdCpfCnpj;
    }

    /**
     * Set: cdCnpjContrato.
     *
     * @param cdCnpjContrato the cd cnpj contrato
     */
    public void setCdCnpjContrato(Integer cdCnpjContrato) {
        this.cdCnpjContrato = cdCnpjContrato;
    }

    /**
     * Get: cdCnpjContrato.
     *
     * @return cdCnpjContrato
     */
    public Integer getCdCnpjContrato() {
        return this.cdCnpjContrato;
    }

    /**
     * Set: cdCpfCnpjDigito.
     *
     * @param cdCpfCnpjDigito the cd cpf cnpj digito
     */
    public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito) {
        this.cdCpfCnpjDigito = cdCpfCnpjDigito;
    }

    /**
     * Get: cdCpfCnpjDigito.
     *
     * @return cdCpfCnpjDigito
     */
    public Integer getCdCpfCnpjDigito() {
        return this.cdCpfCnpjDigito;
    }

    /**
     * Set: cdRazaoSocial.
     *
     * @param cdRazaoSocial the cd razao social
     */
    public void setCdRazaoSocial(String cdRazaoSocial) {
        this.cdRazaoSocial = cdRazaoSocial;
    }

    /**
     * Get: cdRazaoSocial.
     *
     * @return cdRazaoSocial
     */
    public String getCdRazaoSocial() {
        return this.cdRazaoSocial;
    }

    /**
     * Set: cdTipoParticipacaoPessoa.
     *
     * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
     */
    public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
        this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
    }

    /**
     * Get: cdTipoParticipacaoPessoa.
     *
     * @return cdTipoParticipacaoPessoa
     */
    public Integer getCdTipoParticipacaoPessoa() {
        return this.cdTipoParticipacaoPessoa;
    }

    /**
     * Set: dsTipoParticipante.
     *
     * @param dsTipoParticipante the ds tipo participante
     */
    public void setDsTipoParticipante(String dsTipoParticipante) {
        this.dsTipoParticipante = dsTipoParticipante;
    }

    /**
     * Get: dsTipoParticipante.
     *
     * @return dsTipoParticipante
     */
    public String getDsTipoParticipante() {
        return this.dsTipoParticipante;
    }

    /**
     * Set: cdDescricaoSituacaoParticapante.
     *
     * @param cdDescricaoSituacaoParticapante the cd descricao situacao particapante
     */
    public void setCdDescricaoSituacaoParticapante(String cdDescricaoSituacaoParticapante) {
        this.cdDescricaoSituacaoParticapante = cdDescricaoSituacaoParticapante;
    }

    /**
     * Get: cdDescricaoSituacaoParticapante.
     *
     * @return cdDescricaoSituacaoParticapante
     */
    public String getCdDescricaoSituacaoParticapante() {
        return this.cdDescricaoSituacaoParticapante;
    }

    /**
     * Set: dsTipoServico.
     *
     * @param dsTipoServico the ds tipo servico
     */
    public void setDsTipoServico(String dsTipoServico) {
        this.dsTipoServico = dsTipoServico;
    }

    /**
     * Get: dsTipoServico.
     *
     * @return dsTipoServico
     */
    public String getDsTipoServico() {
        return this.dsTipoServico;
    }

    /**
     * Set: dsModalidadeServico.
     *
     * @param dsModalidadeServico the ds modalidade servico
     */
    public void setDsModalidadeServico(String dsModalidadeServico) {
        this.dsModalidadeServico = dsModalidadeServico;
    }

    /**
     * Get: dsModalidadeServico.
     *
     * @return dsModalidadeServico
     */
    public String getDsModalidadeServico() {
        return this.dsModalidadeServico;
    }

    /**
     * Set: dsOperacaoCatalogo.
     *
     * @param dsOperacaoCatalogo the ds operacao catalogo
     */
    public void setDsOperacaoCatalogo(String dsOperacaoCatalogo) {
        this.dsOperacaoCatalogo = dsOperacaoCatalogo;
    }

    /**
     * Get: dsOperacaoCatalogo.
     *
     * @return dsOperacaoCatalogo
     */
    public String getDsOperacaoCatalogo() {
        return this.dsOperacaoCatalogo;
    }

    /**
     * Set: cdFinalidadeEnderecoContrato.
     *
     * @param cdFinalidadeEnderecoContrato the cd finalidade endereco contrato
     */
    public void setCdFinalidadeEnderecoContrato(Integer cdFinalidadeEnderecoContrato) {
        this.cdFinalidadeEnderecoContrato = cdFinalidadeEnderecoContrato;
    }

    /**
     * Get: cdFinalidadeEnderecoContrato.
     *
     * @return cdFinalidadeEnderecoContrato
     */
    public Integer getCdFinalidadeEnderecoContrato() {
        return this.cdFinalidadeEnderecoContrato;
    }

    /**
     * Set: hrInclusaoRegistro.
     *
     * @param hrInclusaoRegistro the hr inclusao registro
     */
    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
        this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    /**
     * Get: hrInclusaoRegistro.
     *
     * @return hrInclusaoRegistro
     */
    public String getHrInclusaoRegistro() {
        return this.hrInclusaoRegistro;
    }

    /**
     * Get: cdEspeceEndereco.
     *
     * @return cdEspeceEndereco
     */
    public Integer getCdEspeceEndereco() {
		return cdEspeceEndereco;
	}

	/**
	 * Set: cdEspeceEndereco.
	 *
	 * @param cdEspeceEndereco the cd espece endereco
	 */
	public void setCdEspeceEndereco(Integer cdEspeceEndereco) {
		this.cdEspeceEndereco = cdEspeceEndereco;
	}

	/**
	 * Get: nrSequenciaEndereco.
	 *
	 * @return nrSequenciaEndereco
	 */
	public Integer getNrSequenciaEndereco() {
		return nrSequenciaEndereco;
	}

	/**
	 * Set: nrSequenciaEndereco.
	 *
	 * @param nrSequenciaEndereco the nr sequencia endereco
	 */
	public void setNrSequenciaEndereco(Integer nrSequenciaEndereco) {
		this.nrSequenciaEndereco = nrSequenciaEndereco;
	}

	/**
	 * Get: cdUsoEnderecoPessoa.
	 *
	 * @return cdUsoEnderecoPessoa
	 */
	public Integer getCdUsoEnderecoPessoa() {
		return cdUsoEnderecoPessoa;
	}

	/**
	 * Set: cdUsoEnderecoPessoa.
	 *
	 * @param cdUsoEnderecoPessoa the cd uso endereco pessoa
	 */
	public void setCdUsoEnderecoPessoa(Integer cdUsoEnderecoPessoa) {
		this.cdUsoEnderecoPessoa = cdUsoEnderecoPessoa;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
    	return CpfCnpjUtils.formatarCpfCnpj(getCdCpfCnpj(), getCdCnpjContrato(), getCdCpfCnpjDigito());
    }

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
}