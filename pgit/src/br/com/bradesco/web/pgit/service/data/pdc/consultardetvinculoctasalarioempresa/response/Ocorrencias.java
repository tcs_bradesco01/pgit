/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTransmissaoAutomatica
     */
    private java.lang.String _cdTransmissaoAutomatica;

    /**
     * Field _nrCpf
     */
    private long _nrCpf = 0;

    /**
     * keeps track of state for field: _nrCpf
     */
    private boolean _has_nrCpf;

    /**
     * Field _cdControleCpf
     */
    private java.lang.String _cdControleCpf;

    /**
     * Field _cdDigitoCpf
     */
    private int _cdDigitoCpf = 0;

    /**
     * keeps track of state for field: _cdDigitoCpf
     */
    private boolean _has_cdDigitoCpf;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;

    /**
     * Field _cdTipoContaVinculo
     */
    private java.lang.String _cdTipoContaVinculo;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExter
     */
    private java.lang.String _cdUsuarioInclusaoExter;

    /**
     * Field _dtInclusa
     */
    private java.lang.String _dtInclusa;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperCanalInclusao
     */
    private java.lang.String _cdOperCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExter
     */
    private java.lang.String _cdUsuarioManutencaoExter;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperCanalManutencao
     */
    private java.lang.String _cdOperCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdDigitoCpf
     * 
     */
    public void deleteCdDigitoCpf()
    {
        this._has_cdDigitoCpf= false;
    } //-- void deleteCdDigitoCpf() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteNrCpf
     * 
     */
    public void deleteNrCpf()
    {
        this._has_nrCpf= false;
    } //-- void deleteNrCpf() 

    /**
     * Returns the value of field 'cdControleCpf'.
     * 
     * @return String
     * @return the value of field 'cdControleCpf'.
     */
    public java.lang.String getCdControleCpf()
    {
        return this._cdControleCpf;
    } //-- java.lang.String getCdControleCpf() 

    /**
     * Returns the value of field 'cdDigitoCpf'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpf'.
     */
    public int getCdDigitoCpf()
    {
        return this._cdDigitoCpf;
    } //-- int getCdDigitoCpf() 

    /**
     * Returns the value of field 'cdOperCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalInclusao'.
     */
    public java.lang.String getCdOperCanalInclusao()
    {
        return this._cdOperCanalInclusao;
    } //-- java.lang.String getCdOperCanalInclusao() 

    /**
     * Returns the value of field 'cdOperCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalManutencao'.
     */
    public java.lang.String getCdOperCanalManutencao()
    {
        return this._cdOperCanalManutencao;
    } //-- java.lang.String getCdOperCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContaVinculo'.
     * 
     * @return String
     * @return the value of field 'cdTipoContaVinculo'.
     */
    public java.lang.String getCdTipoContaVinculo()
    {
        return this._cdTipoContaVinculo;
    } //-- java.lang.String getCdTipoContaVinculo() 

    /**
     * Returns the value of field 'cdTransmissaoAutomatica'.
     * 
     * @return String
     * @return the value of field 'cdTransmissaoAutomatica'.
     */
    public java.lang.String getCdTransmissaoAutomatica()
    {
        return this._cdTransmissaoAutomatica;
    } //-- java.lang.String getCdTransmissaoAutomatica() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExter'.
     */
    public java.lang.String getCdUsuarioInclusaoExter()
    {
        return this._cdUsuarioInclusaoExter;
    } //-- java.lang.String getCdUsuarioInclusaoExter() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExter'.
     */
    public java.lang.String getCdUsuarioManutencaoExter()
    {
        return this._cdUsuarioManutencaoExter;
    } //-- java.lang.String getCdUsuarioManutencaoExter() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dtInclusa'.
     * 
     * @return String
     * @return the value of field 'dtInclusa'.
     */
    public java.lang.String getDtInclusa()
    {
        return this._dtInclusa;
    } //-- java.lang.String getDtInclusa() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'nrCpf'.
     * 
     * @return long
     * @return the value of field 'nrCpf'.
     */
    public long getNrCpf()
    {
        return this._nrCpf;
    } //-- long getNrCpf() 

    /**
     * Method hasCdDigitoCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpf()
    {
        return this._has_cdDigitoCpf;
    } //-- boolean hasCdDigitoCpf() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasNrCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrCpf()
    {
        return this._has_nrCpf;
    } //-- boolean hasNrCpf() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCpf'.
     * 
     * @param cdControleCpf the value of field 'cdControleCpf'.
     */
    public void setCdControleCpf(java.lang.String cdControleCpf)
    {
        this._cdControleCpf = cdControleCpf;
    } //-- void setCdControleCpf(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoCpf'.
     * 
     * @param cdDigitoCpf the value of field 'cdDigitoCpf'.
     */
    public void setCdDigitoCpf(int cdDigitoCpf)
    {
        this._cdDigitoCpf = cdDigitoCpf;
        this._has_cdDigitoCpf = true;
    } //-- void setCdDigitoCpf(int) 

    /**
     * Sets the value of field 'cdOperCanalInclusao'.
     * 
     * @param cdOperCanalInclusao the value of field
     * 'cdOperCanalInclusao'.
     */
    public void setCdOperCanalInclusao(java.lang.String cdOperCanalInclusao)
    {
        this._cdOperCanalInclusao = cdOperCanalInclusao;
    } //-- void setCdOperCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperCanalManutencao'.
     * 
     * @param cdOperCanalManutencao the value of field
     * 'cdOperCanalManutencao'.
     */
    public void setCdOperCanalManutencao(java.lang.String cdOperCanalManutencao)
    {
        this._cdOperCanalManutencao = cdOperCanalManutencao;
    } //-- void setCdOperCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContaVinculo'.
     * 
     * @param cdTipoContaVinculo the value of field
     * 'cdTipoContaVinculo'.
     */
    public void setCdTipoContaVinculo(java.lang.String cdTipoContaVinculo)
    {
        this._cdTipoContaVinculo = cdTipoContaVinculo;
    } //-- void setCdTipoContaVinculo(java.lang.String) 

    /**
     * Sets the value of field 'cdTransmissaoAutomatica'.
     * 
     * @param cdTransmissaoAutomatica the value of field
     * 'cdTransmissaoAutomatica'.
     */
    public void setCdTransmissaoAutomatica(java.lang.String cdTransmissaoAutomatica)
    {
        this._cdTransmissaoAutomatica = cdTransmissaoAutomatica;
    } //-- void setCdTransmissaoAutomatica(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @param cdUsuarioInclusaoExter the value of field
     * 'cdUsuarioInclusaoExter'.
     */
    public void setCdUsuarioInclusaoExter(java.lang.String cdUsuarioInclusaoExter)
    {
        this._cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
    } //-- void setCdUsuarioInclusaoExter(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @param cdUsuarioManutencaoExter the value of field
     * 'cdUsuarioManutencaoExter'.
     */
    public void setCdUsuarioManutencaoExter(java.lang.String cdUsuarioManutencaoExter)
    {
        this._cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
    } //-- void setCdUsuarioManutencaoExter(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusa'.
     * 
     * @param dtInclusa the value of field 'dtInclusa'.
     */
    public void setDtInclusa(java.lang.String dtInclusa)
    {
        this._dtInclusa = dtInclusa;
    } //-- void setDtInclusa(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrCpf'.
     * 
     * @param nrCpf the value of field 'nrCpf'.
     */
    public void setNrCpf(long nrCpf)
    {
        this._nrCpf = nrCpf;
        this._has_nrCpf = true;
    } //-- void setNrCpf(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetvinculoctasalarioempresa.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
