/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetalheTipoComprovanteResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalheTipoComprovanteResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoComprovanteSalarial
     */
    private int _cdTipoComprovanteSalarial = 0;

    /**
     * keeps track of state for field: _cdTipoComprovanteSalarial
     */
    private boolean _has_cdTipoComprovanteSalarial;

    /**
     * Field _dsTipoComprovanteSalarial
     */
    private java.lang.String _dsTipoComprovanteSalarial;

    /**
     * Field _cdControleMensagemSalarial
     */
    private int _cdControleMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdControleMensagemSalarial
     */
    private boolean _has_cdControleMensagemSalarial;

    /**
     * Field _rsControleMensagemSalarial
     */
    private java.lang.String _rsControleMensagemSalarial;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nrOperacaoFluxoInclusao
     */
    private java.lang.String _nrOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nrOperacaoFluxoManutencao
     */
    private java.lang.String _nrOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalheTipoComprovanteResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.response.ConsultarDetalheTipoComprovanteResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdControleMensagemSalarial
     * 
     */
    public void deleteCdControleMensagemSalarial()
    {
        this._has_cdControleMensagemSalarial= false;
    } //-- void deleteCdControleMensagemSalarial() 

    /**
     * Method deleteCdTipoComprovanteSalarial
     * 
     */
    public void deleteCdTipoComprovanteSalarial()
    {
        this._has_cdTipoComprovanteSalarial= false;
    } //-- void deleteCdTipoComprovanteSalarial() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdControleMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdControleMensagemSalarial'.
     */
    public int getCdControleMensagemSalarial()
    {
        return this._cdControleMensagemSalarial;
    } //-- int getCdControleMensagemSalarial() 

    /**
     * Returns the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @return int
     * @return the value of field 'cdTipoComprovanteSalarial'.
     */
    public int getCdTipoComprovanteSalarial()
    {
        return this._cdTipoComprovanteSalarial;
    } //-- int getCdTipoComprovanteSalarial() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsTipoComprovanteSalarial'.
     * 
     * @return String
     * @return the value of field 'dsTipoComprovanteSalarial'.
     */
    public java.lang.String getDsTipoComprovanteSalarial()
    {
        return this._dsTipoComprovanteSalarial;
    } //-- java.lang.String getDsTipoComprovanteSalarial() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoInclusao'.
     */
    public java.lang.String getNrOperacaoFluxoInclusao()
    {
        return this._nrOperacaoFluxoInclusao;
    } //-- java.lang.String getNrOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoManutencao'.
     */
    public java.lang.String getNrOperacaoFluxoManutencao()
    {
        return this._nrOperacaoFluxoManutencao;
    } //-- java.lang.String getNrOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'rsControleMensagemSalarial'.
     * 
     * @return String
     * @return the value of field 'rsControleMensagemSalarial'.
     */
    public java.lang.String getRsControleMensagemSalarial()
    {
        return this._rsControleMensagemSalarial;
    } //-- java.lang.String getRsControleMensagemSalarial() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdControleMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleMensagemSalarial()
    {
        return this._has_cdControleMensagemSalarial;
    } //-- boolean hasCdControleMensagemSalarial() 

    /**
     * Method hasCdTipoComprovanteSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoComprovanteSalarial()
    {
        return this._has_cdTipoComprovanteSalarial;
    } //-- boolean hasCdTipoComprovanteSalarial() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdControleMensagemSalarial'.
     * 
     * @param cdControleMensagemSalarial the value of field
     * 'cdControleMensagemSalarial'.
     */
    public void setCdControleMensagemSalarial(int cdControleMensagemSalarial)
    {
        this._cdControleMensagemSalarial = cdControleMensagemSalarial;
        this._has_cdControleMensagemSalarial = true;
    } //-- void setCdControleMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @param cdTipoComprovanteSalarial the value of field
     * 'cdTipoComprovanteSalarial'.
     */
    public void setCdTipoComprovanteSalarial(int cdTipoComprovanteSalarial)
    {
        this._cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
        this._has_cdTipoComprovanteSalarial = true;
    } //-- void setCdTipoComprovanteSalarial(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoComprovanteSalarial'.
     * 
     * @param dsTipoComprovanteSalarial the value of field
     * 'dsTipoComprovanteSalarial'.
     */
    public void setDsTipoComprovanteSalarial(java.lang.String dsTipoComprovanteSalarial)
    {
        this._dsTipoComprovanteSalarial = dsTipoComprovanteSalarial;
    } //-- void setDsTipoComprovanteSalarial(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @param nrOperacaoFluxoInclusao the value of field
     * 'nrOperacaoFluxoInclusao'.
     */
    public void setNrOperacaoFluxoInclusao(java.lang.String nrOperacaoFluxoInclusao)
    {
        this._nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    } //-- void setNrOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @param nrOperacaoFluxoManutencao the value of field
     * 'nrOperacaoFluxoManutencao'.
     */
    public void setNrOperacaoFluxoManutencao(java.lang.String nrOperacaoFluxoManutencao)
    {
        this._nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
    } //-- void setNrOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'rsControleMensagemSalarial'.
     * 
     * @param rsControleMensagemSalarial the value of field
     * 'rsControleMensagemSalarial'.
     */
    public void setRsControleMensagemSalarial(java.lang.String rsControleMensagemSalarial)
    {
        this._rsControleMensagemSalarial = rsControleMensagemSalarial;
    } //-- void setRsControleMensagemSalarial(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalheTipoComprovanteResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.response.ConsultarDetalheTipoComprovanteResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.response.ConsultarDetalheTipoComprovanteResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.response.ConsultarDetalheTipoComprovanteResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhetipocomprovante.response.ConsultarDetalheTipoComprovanteResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
