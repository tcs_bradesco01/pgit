package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 23/05/17.
 *
 * @author : todo!
 * @version :
 */
public class ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO {

    /** The cd solicitacao pagamento integrado. */
    private Integer cdSolicitacaoPagamentoIntegrado;

    /** The nr solicitacao pagamento integrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;

    /** The cdpessoa juridica contrato. */
    private Long cdpessoaJuridicaContrato;

    /** The ds pessoa juridica contrato. */
    private String dsPessoaJuridicaContrato;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio;

    /** The cd situacao solicitacao pagamento. */
    private Integer cdSituacaoSolicitacaoPagamento;

    /** The ds situacao solicitacao pagamento. */
    private String dsSituacaoSolicitacaoPagamento;

    /** The cd motivo solicitacao. */
    private Integer cdMotivoSolicitacao;

    /** The ds motivo solicitacao. */
    private String dsMotivoSolicitacao;

    /** The dt inclusao solicitacao. */
    private String dtInclusaoSolicitacao;

    /** The hr inclusao solicitacao. */
    private String hrInclusaoSolicitacao;

    /** The ds tipo solicitacao. */
    private String dsTipoSolicitacao;

    /** The nr arquivo remessa. */
    private Long nrArquivoRemessa;

    /** The cd produto servico operacao. */
    private Integer cdProdutoServicoOperacao;

    /** The ds produto servico operacao. */
    private String dsProdutoServicoOperacao;

    /** The cd produto operacao relacionado. */
    private Integer cdProdutoOperacaoRelacionado;

    /** The ds produto operacao relacionado. */
    private String dsProdutoOperacaoRelacionado;

    /**
     * Get: dtInclusaoSolicitacaoFormatada.
     *
     * @return dtInclusaoSolicitacaoFormatada
     */
    public String getDtInclusaoSolicitacaoFormatada(){
        return getDtInclusaoSolicitacao().replace(".", "/") + " - " + getHrInclusaoSolicitacao();
    }
    /**
     * Set cd solicitacao pagamento integrado.
     *
     * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
     */
    public void setCdSolicitacaoPagamentoIntegrado(Integer cdSolicitacaoPagamentoIntegrado) {
        this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get cd solicitacao pagamento integrado.
     *
     * @return the cd solicitacao pagamento integrado
     */
    public Integer getCdSolicitacaoPagamentoIntegrado() {
        return this.cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set nr solicitacao pagamento integrado.
     *
     * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
     */
    public void setNrSolicitacaoPagamentoIntegrado(Integer nrSolicitacaoPagamentoIntegrado) {
        this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get nr solicitacao pagamento integrado.
     *
     * @return the nr solicitacao pagamento integrado
     */
    public Integer getNrSolicitacaoPagamentoIntegrado() {
        return this.nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set cdpessoa juridica contrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get cdpessoa juridica contrato.
     *
     * @return the cdpessoa juridica contrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set ds pessoa juridica contrato.
     *
     * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
     */
    public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
        this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
    }

    /**
     * Get ds pessoa juridica contrato.
     *
     * @return the ds pessoa juridica contrato
     */
    public String getDsPessoaJuridicaContrato() {
        return this.dsPessoaJuridicaContrato;
    }

    /**
     * Set cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set cd situacao solicitacao pagamento.
     *
     * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
     */
    public void setCdSituacaoSolicitacaoPagamento(Integer cdSituacaoSolicitacaoPagamento) {
        this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
    }

    /**
     * Get cd situacao solicitacao pagamento.
     *
     * @return the cd situacao solicitacao pagamento
     */
    public Integer getCdSituacaoSolicitacaoPagamento() {
        return this.cdSituacaoSolicitacaoPagamento;
    }

    /**
     * Set ds situacao solicitacao pagamento.
     *
     * @param dsSituacaoSolicitacaoPagamento the ds situacao solicitacao pagamento
     */
    public void setDsSituacaoSolicitacaoPagamento(String dsSituacaoSolicitacaoPagamento) {
        this.dsSituacaoSolicitacaoPagamento = dsSituacaoSolicitacaoPagamento;
    }

    /**
     * Get ds situacao solicitacao pagamento.
     *
     * @return the ds situacao solicitacao pagamento
     */
    public String getDsSituacaoSolicitacaoPagamento() {
        return this.dsSituacaoSolicitacaoPagamento;
    }

    /**
     * Set cd motivo solicitacao.
     *
     * @param cdMotivoSolicitacao the cd motivo solicitacao
     */
    public void setCdMotivoSolicitacao(Integer cdMotivoSolicitacao) {
        this.cdMotivoSolicitacao = cdMotivoSolicitacao;
    }

    /**
     * Get cd motivo solicitacao.
     *
     * @return the cd motivo solicitacao
     */
    public Integer getCdMotivoSolicitacao() {
        return this.cdMotivoSolicitacao;
    }

    /**
     * Set ds motivo solicitacao.
     *
     * @param dsMotivoSolicitacao the ds motivo solicitacao
     */
    public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
        this.dsMotivoSolicitacao = dsMotivoSolicitacao;
    }

    /**
     * Get ds motivo solicitacao.
     *
     * @return the ds motivo solicitacao
     */
    public String getDsMotivoSolicitacao() {
        return this.dsMotivoSolicitacao;
    }

    /**
     * Set dt inclusao solicitacao.
     *
     * @param dtInclusaoSolicitacao the dt inclusao solicitacao
     */
    public void setDtInclusaoSolicitacao(String dtInclusaoSolicitacao) {
        this.dtInclusaoSolicitacao = dtInclusaoSolicitacao;
    }

    /**
     * Get dt inclusao solicitacao.
     *
     * @return the dt inclusao solicitacao
     */
    public String getDtInclusaoSolicitacao() {
        return this.dtInclusaoSolicitacao;
    }

    /**
     * Set hr inclusao solicitacao.
     *
     * @param hrInclusaoSolicitacao the hr inclusao solicitacao
     */
    public void setHrInclusaoSolicitacao(String hrInclusaoSolicitacao) {
        this.hrInclusaoSolicitacao = hrInclusaoSolicitacao;
    }

    /**
     * Get hr inclusao solicitacao.
     *
     * @return the hr inclusao solicitacao
     */
    public String getHrInclusaoSolicitacao() {
        return this.hrInclusaoSolicitacao;
    }

    /**
     * Set ds tipo solicitacao.
     *
     * @param dsTipoSolicitacao the ds tipo solicitacao
     */
    public void setDsTipoSolicitacao(String dsTipoSolicitacao) {
        this.dsTipoSolicitacao = dsTipoSolicitacao;
    }

    /**
     * Get ds tipo solicitacao.
     *
     * @return the ds tipo solicitacao
     */
    public String getDsTipoSolicitacao() {
        return this.dsTipoSolicitacao;
    }

    /**
     * Set nr arquivo remessa.
     *
     * @param nrArquivoRemessa the nr arquivo remessa
     */
    public void setNrArquivoRemessa(Long nrArquivoRemessa) {
        this.nrArquivoRemessa = nrArquivoRemessa;
    }

    /**
     * Get nr arquivo remessa.
     *
     * @return the nr arquivo remessa
     */
    public Long getNrArquivoRemessa() {
        return this.nrArquivoRemessa;
    }

    /**
     * Set cd produto servico operacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get cd produto servico operacao.
     *
     * @return the cd produto servico operacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    /**
     * Set ds produto servico operacao.
     *
     * @param dsProdutoServicoOperacao the ds produto servico operacao
     */
    public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
        this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    }

    /**
     * Get ds produto servico operacao.
     *
     * @return the ds produto servico operacao
     */
    public String getDsProdutoServicoOperacao() {
        return this.dsProdutoServicoOperacao;
    }

    /**
     * Set cd produto operacao relacionado.
     *
     * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Get cd produto operacao relacionado.
     *
     * @return the cd produto operacao relacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }

    /**
     * Set ds produto operacao relacionado.
     *
     * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
     */
    public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
        this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    }

    /**
     * Get ds produto operacao relacionado.
     *
     * @return the ds produto operacao relacionado
     */
    public String getDsProdutoOperacaoRelacionado() {
        return this.dsProdutoOperacaoRelacionado;
    }
}