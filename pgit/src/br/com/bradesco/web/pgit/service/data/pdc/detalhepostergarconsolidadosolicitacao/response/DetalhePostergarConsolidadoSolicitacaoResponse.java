/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalhePostergarConsolidadoSolicitacaoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalhePostergarConsolidadoSolicitacaoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdSolicitacaoPagamentoIntegrado
     */
    private int _cdSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdSolicitacaoPagamentoIntegrado
     */
    private boolean _has_cdSolicitacaoPagamentoIntegrado;

    /**
     * Field _nrSolicitacaoPagamentoIntegrado
     */
    private int _nrSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _nrSolicitacaoPagamentoIntegrado
     */
    private boolean _has_nrSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _dsSituacaoSolicitacaoPagamento
     */
    private java.lang.String _dsSituacaoSolicitacaoPagamento;

    /**
     * Field _cdMotivoSolicitacao
     */
    private int _cdMotivoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSolicitacao
     */
    private boolean _has_cdMotivoSolicitacao;

    /**
     * Field _dsMotivoSolicitacao
     */
    private java.lang.String _dsMotivoSolicitacao;

    /**
     * Field _dsTipoSolicitacao
     */
    private java.lang.String _dsTipoSolicitacao;

    /**
     * Field _dtAgendamentoPagamento
     */
    private java.lang.String _dtAgendamentoPagamento;

    /**
     * Field _dtPrevistaAgendaPagamento
     */
    private java.lang.String _dtPrevistaAgendaPagamento;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperRelacionado
     */
    private java.lang.String _dsProdutoOperRelacionado;

    /**
     * Field _nrArquivoRemssaPagamento
     */
    private long _nrArquivoRemssaPagamento = 0;

    /**
     * keeps track of state for field: _nrArquivoRemssaPagamento
     */
    private boolean _has_nrArquivoRemssaPagamento;

    /**
     * Field _cdSituacaoPagamentoCliente
     */
    private int _cdSituacaoPagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdSituacaoPagamentoCliente
     */
    private boolean _has_cdSituacaoPagamentoCliente;

    /**
     * Field _dsSituacaoPagamentoCliente
     */
    private java.lang.String _dsSituacaoPagamentoCliente;

    /**
     * Field _cdTipoContaPagador
     */
    private int _cdTipoContaPagador = 0;

    /**
     * keeps track of state for field: _cdTipoContaPagador
     */
    private boolean _has_cdTipoContaPagador;

    /**
     * Field _dsTipoContaPagador
     */
    private java.lang.String _dsTipoContaPagador;

    /**
     * Field _cdBancoPagador
     */
    private int _cdBancoPagador = 0;

    /**
     * keeps track of state for field: _cdBancoPagador
     */
    private boolean _has_cdBancoPagador;

    /**
     * Field _dsBancoPagador
     */
    private java.lang.String _dsBancoPagador;

    /**
     * Field _cdAgenciaBancariaPagador
     */
    private int _cdAgenciaBancariaPagador = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancariaPagador
     */
    private boolean _has_cdAgenciaBancariaPagador;

    /**
     * Field _dsAgenciaBancariaPagador
     */
    private java.lang.String _dsAgenciaBancariaPagador;

    /**
     * Field _cdDigitoAgenciaPagador
     */
    private java.lang.String _cdDigitoAgenciaPagador;

    /**
     * Field _cdContaBancariaPagador
     */
    private long _cdContaBancariaPagador = 0;

    /**
     * keeps track of state for field: _cdContaBancariaPagador
     */
    private boolean _has_cdContaBancariaPagador;

    /**
     * Field _cdDigitoContaPagador
     */
    private java.lang.String _cdDigitoContaPagador;

    /**
     * Field _cdTipoInscricaoPagador
     */
    private int _cdTipoInscricaoPagador = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoPagador
     */
    private boolean _has_cdTipoInscricaoPagador;

    /**
     * Field _cdCpfCnpjPagador
     */
    private long _cdCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPagador
     */
    private boolean _has_cdCpfCnpjPagador;

    /**
     * Field _cdFilialCpfCnpjPagador
     */
    private int _cdFilialCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjPagador
     */
    private boolean _has_cdFilialCpfCnpjPagador;

    /**
     * Field _cdControleCpfCnpjPagador
     */
    private int _cdControleCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpjPagador
     */
    private boolean _has_cdControleCpfCnpjPagador;

    /**
     * Field _dsComplementoPagador
     */
    private java.lang.String _dsComplementoPagador;

    /**
     * Field _qtdPagamentoPrevtSolicitacao
     */
    private long _qtdPagamentoPrevtSolicitacao = 0;

    /**
     * keeps track of state for field: _qtdPagamentoPrevtSolicitacao
     */
    private boolean _has_qtdPagamentoPrevtSolicitacao;

    /**
     * Field _qtdPagamentoEfetvSolicitacao
     */
    private long _qtdPagamentoEfetvSolicitacao = 0;

    /**
     * keeps track of state for field: _qtdPagamentoEfetvSolicitacao
     */
    private boolean _has_qtdPagamentoEfetvSolicitacao;

    /**
     * Field _qtdePagamentosProcessados
     */
    private long _qtdePagamentosProcessados = 0;

    /**
     * keeps track of state for field: _qtdePagamentosProcessados
     */
    private boolean _has_qtdePagamentosProcessados;

    /**
     * Field _vlPagamentoPrevtSolicitacao
     */
    private java.math.BigDecimal _vlPagamentoPrevtSolicitacao = new java.math.BigDecimal("0");

    /**
     * Field _vlPagamentoEfetivoSolicitacao
     */
    private java.math.BigDecimal _vlPagamentoEfetivoSolicitacao = new java.math.BigDecimal("0");

    /**
     * Field _vlrPagamentosProcessados
     */
    private java.math.BigDecimal _vlrPagamentosProcessados = new java.math.BigDecimal("0");

    /**
     * Field _dtAtendimentoSolicitacaoPagamento
     */
    private java.lang.String _dtAtendimentoSolicitacaoPagamento;

    /**
     * Field _hrAtendimentoSolicitacaoPagamento
     */
    private java.lang.String _hrAtendimentoSolicitacaoPagamento;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nrOperacaoFluxoInclusao
     */
    private java.lang.String _nrOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalhePostergarConsolidadoSolicitacaoResponse() 
     {
        super();
        setVlPagamentoPrevtSolicitacao(new java.math.BigDecimal("0"));
        setVlPagamentoEfetivoSolicitacao(new java.math.BigDecimal("0"));
        setVlrPagamentosProcessados(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.response.DetalhePostergarConsolidadoSolicitacaoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaBancariaPagador
     * 
     */
    public void deleteCdAgenciaBancariaPagador()
    {
        this._has_cdAgenciaBancariaPagador= false;
    } //-- void deleteCdAgenciaBancariaPagador() 

    /**
     * Method deleteCdBancoPagador
     * 
     */
    public void deleteCdBancoPagador()
    {
        this._has_cdBancoPagador= false;
    } //-- void deleteCdBancoPagador() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdContaBancariaPagador
     * 
     */
    public void deleteCdContaBancariaPagador()
    {
        this._has_cdContaBancariaPagador= false;
    } //-- void deleteCdContaBancariaPagador() 

    /**
     * Method deleteCdControleCpfCnpjPagador
     * 
     */
    public void deleteCdControleCpfCnpjPagador()
    {
        this._has_cdControleCpfCnpjPagador= false;
    } //-- void deleteCdControleCpfCnpjPagador() 

    /**
     * Method deleteCdCpfCnpjPagador
     * 
     */
    public void deleteCdCpfCnpjPagador()
    {
        this._has_cdCpfCnpjPagador= false;
    } //-- void deleteCdCpfCnpjPagador() 

    /**
     * Method deleteCdFilialCpfCnpjPagador
     * 
     */
    public void deleteCdFilialCpfCnpjPagador()
    {
        this._has_cdFilialCpfCnpjPagador= false;
    } //-- void deleteCdFilialCpfCnpjPagador() 

    /**
     * Method deleteCdMotivoSolicitacao
     * 
     */
    public void deleteCdMotivoSolicitacao()
    {
        this._has_cdMotivoSolicitacao= false;
    } //-- void deleteCdMotivoSolicitacao() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdSituacaoPagamentoCliente
     * 
     */
    public void deleteCdSituacaoPagamentoCliente()
    {
        this._has_cdSituacaoPagamentoCliente= false;
    } //-- void deleteCdSituacaoPagamentoCliente() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteCdSolicitacaoPagamentoIntegrado()
    {
        this._has_cdSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteCdTipoContaPagador
     * 
     */
    public void deleteCdTipoContaPagador()
    {
        this._has_cdTipoContaPagador= false;
    } //-- void deleteCdTipoContaPagador() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoInscricaoPagador
     * 
     */
    public void deleteCdTipoInscricaoPagador()
    {
        this._has_cdTipoInscricaoPagador= false;
    } //-- void deleteCdTipoInscricaoPagador() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrArquivoRemssaPagamento
     * 
     */
    public void deleteNrArquivoRemssaPagamento()
    {
        this._has_nrArquivoRemssaPagamento= false;
    } //-- void deleteNrArquivoRemssaPagamento() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteNrSolicitacaoPagamentoIntegrado()
    {
        this._has_nrSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteQtdPagamentoEfetvSolicitacao
     * 
     */
    public void deleteQtdPagamentoEfetvSolicitacao()
    {
        this._has_qtdPagamentoEfetvSolicitacao= false;
    } //-- void deleteQtdPagamentoEfetvSolicitacao() 

    /**
     * Method deleteQtdPagamentoPrevtSolicitacao
     * 
     */
    public void deleteQtdPagamentoPrevtSolicitacao()
    {
        this._has_qtdPagamentoPrevtSolicitacao= false;
    } //-- void deleteQtdPagamentoPrevtSolicitacao() 

    /**
     * Method deleteQtdePagamentosProcessados
     * 
     */
    public void deleteQtdePagamentosProcessados()
    {
        this._has_qtdePagamentosProcessados= false;
    } //-- void deleteQtdePagamentosProcessados() 

    /**
     * Returns the value of field 'cdAgenciaBancariaPagador'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancariaPagador'.
     */
    public int getCdAgenciaBancariaPagador()
    {
        return this._cdAgenciaBancariaPagador;
    } //-- int getCdAgenciaBancariaPagador() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdBancoPagador'.
     * 
     * @return int
     * @return the value of field 'cdBancoPagador'.
     */
    public int getCdBancoPagador()
    {
        return this._cdBancoPagador;
    } //-- int getCdBancoPagador() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdContaBancariaPagador'.
     * 
     * @return long
     * @return the value of field 'cdContaBancariaPagador'.
     */
    public long getCdContaBancariaPagador()
    {
        return this._cdContaBancariaPagador;
    } //-- long getCdContaBancariaPagador() 

    /**
     * Returns the value of field 'cdControleCpfCnpjPagador'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpjPagador'.
     */
    public int getCdControleCpfCnpjPagador()
    {
        return this._cdControleCpfCnpjPagador;
    } //-- int getCdControleCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdCpfCnpjPagador'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPagador'.
     */
    public long getCdCpfCnpjPagador()
    {
        return this._cdCpfCnpjPagador;
    } //-- long getCdCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdDigitoAgenciaPagador'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgenciaPagador'.
     */
    public java.lang.String getCdDigitoAgenciaPagador()
    {
        return this._cdDigitoAgenciaPagador;
    } //-- java.lang.String getCdDigitoAgenciaPagador() 

    /**
     * Returns the value of field 'cdDigitoContaPagador'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaPagador'.
     */
    public java.lang.String getCdDigitoContaPagador()
    {
        return this._cdDigitoContaPagador;
    } //-- java.lang.String getCdDigitoContaPagador() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjPagador'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjPagador'.
     */
    public int getCdFilialCpfCnpjPagador()
    {
        return this._cdFilialCpfCnpjPagador;
    } //-- int getCdFilialCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdMotivoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSolicitacao'.
     */
    public int getCdMotivoSolicitacao()
    {
        return this._cdMotivoSolicitacao;
    } //-- int getCdMotivoSolicitacao() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdSituacaoPagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPagamentoCliente'.
     */
    public int getCdSituacaoPagamentoCliente()
    {
        return this._cdSituacaoPagamentoCliente;
    } //-- int getCdSituacaoPagamentoCliente() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamentoIntegrado'.
     */
    public int getCdSolicitacaoPagamentoIntegrado()
    {
        return this._cdSolicitacaoPagamentoIntegrado;
    } //-- int getCdSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdTipoContaPagador'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaPagador'.
     */
    public int getCdTipoContaPagador()
    {
        return this._cdTipoContaPagador;
    } //-- int getCdTipoContaPagador() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoInscricaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoPagador'.
     */
    public int getCdTipoInscricaoPagador()
    {
        return this._cdTipoInscricaoPagador;
    } //-- int getCdTipoInscricaoPagador() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaBancariaPagador'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaBancariaPagador'.
     */
    public java.lang.String getDsAgenciaBancariaPagador()
    {
        return this._dsAgenciaBancariaPagador;
    } //-- java.lang.String getDsAgenciaBancariaPagador() 

    /**
     * Returns the value of field 'dsBancoPagador'.
     * 
     * @return String
     * @return the value of field 'dsBancoPagador'.
     */
    public java.lang.String getDsBancoPagador()
    {
        return this._dsBancoPagador;
    } //-- java.lang.String getDsBancoPagador() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsComplementoPagador'.
     * 
     * @return String
     * @return the value of field 'dsComplementoPagador'.
     */
    public java.lang.String getDsComplementoPagador()
    {
        return this._dsComplementoPagador;
    } //-- java.lang.String getDsComplementoPagador() 

    /**
     * Returns the value of field 'dsMotivoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSolicitacao'.
     */
    public java.lang.String getDsMotivoSolicitacao()
    {
        return this._dsMotivoSolicitacao;
    } //-- java.lang.String getDsMotivoSolicitacao() 

    /**
     * Returns the value of field 'dsProdutoOperRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperRelacionado'.
     */
    public java.lang.String getDsProdutoOperRelacionado()
    {
        return this._dsProdutoOperRelacionado;
    } //-- java.lang.String getDsProdutoOperRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsSituacaoPagamentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPagamentoCliente'.
     */
    public java.lang.String getDsSituacaoPagamentoCliente()
    {
        return this._dsSituacaoPagamentoCliente;
    } //-- java.lang.String getDsSituacaoPagamentoCliente() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacaoPagamento'.
     */
    public java.lang.String getDsSituacaoSolicitacaoPagamento()
    {
        return this._dsSituacaoSolicitacaoPagamento;
    } //-- java.lang.String getDsSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'dsTipoContaPagador'.
     * 
     * @return String
     * @return the value of field 'dsTipoContaPagador'.
     */
    public java.lang.String getDsTipoContaPagador()
    {
        return this._dsTipoContaPagador;
    } //-- java.lang.String getDsTipoContaPagador() 

    /**
     * Returns the value of field 'dsTipoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsTipoSolicitacao'.
     */
    public java.lang.String getDsTipoSolicitacao()
    {
        return this._dsTipoSolicitacao;
    } //-- java.lang.String getDsTipoSolicitacao() 

    /**
     * Returns the value of field 'dtAgendamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtAgendamentoPagamento'.
     */
    public java.lang.String getDtAgendamentoPagamento()
    {
        return this._dtAgendamentoPagamento;
    } //-- java.lang.String getDtAgendamentoPagamento() 

    /**
     * Returns the value of field
     * 'dtAtendimentoSolicitacaoPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'dtAtendimentoSolicitacaoPagamento'.
     */
    public java.lang.String getDtAtendimentoSolicitacaoPagamento()
    {
        return this._dtAtendimentoSolicitacaoPagamento;
    } //-- java.lang.String getDtAtendimentoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'dtPrevistaAgendaPagamento'.
     * 
     * @return String
     * @return the value of field 'dtPrevistaAgendaPagamento'.
     */
    public java.lang.String getDtPrevistaAgendaPagamento()
    {
        return this._dtPrevistaAgendaPagamento;
    } //-- java.lang.String getDtPrevistaAgendaPagamento() 

    /**
     * Returns the value of field
     * 'hrAtendimentoSolicitacaoPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'hrAtendimentoSolicitacaoPagamento'.
     */
    public java.lang.String getHrAtendimentoSolicitacaoPagamento()
    {
        return this._hrAtendimentoSolicitacaoPagamento;
    } //-- java.lang.String getHrAtendimentoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrArquivoRemssaPagamento'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemssaPagamento'.
     */
    public long getNrArquivoRemssaPagamento()
    {
        return this._nrArquivoRemssaPagamento;
    } //-- long getNrArquivoRemssaPagamento() 

    /**
     * Returns the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoInclusao'.
     */
    public java.lang.String getNrOperacaoFluxoInclusao()
    {
        return this._nrOperacaoFluxoInclusao;
    } //-- java.lang.String getNrOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamentoIntegrado'.
     */
    public int getNrSolicitacaoPagamentoIntegrado()
    {
        return this._nrSolicitacaoPagamentoIntegrado;
    } //-- int getNrSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'qtdPagamentoEfetvSolicitacao'.
     * 
     * @return long
     * @return the value of field 'qtdPagamentoEfetvSolicitacao'.
     */
    public long getQtdPagamentoEfetvSolicitacao()
    {
        return this._qtdPagamentoEfetvSolicitacao;
    } //-- long getQtdPagamentoEfetvSolicitacao() 

    /**
     * Returns the value of field 'qtdPagamentoPrevtSolicitacao'.
     * 
     * @return long
     * @return the value of field 'qtdPagamentoPrevtSolicitacao'.
     */
    public long getQtdPagamentoPrevtSolicitacao()
    {
        return this._qtdPagamentoPrevtSolicitacao;
    } //-- long getQtdPagamentoPrevtSolicitacao() 

    /**
     * Returns the value of field 'qtdePagamentosProcessados'.
     * 
     * @return long
     * @return the value of field 'qtdePagamentosProcessados'.
     */
    public long getQtdePagamentosProcessados()
    {
        return this._qtdePagamentosProcessados;
    } //-- long getQtdePagamentosProcessados() 

    /**
     * Returns the value of field 'vlPagamentoEfetivoSolicitacao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoEfetivoSolicitacao'.
     */
    public java.math.BigDecimal getVlPagamentoEfetivoSolicitacao()
    {
        return this._vlPagamentoEfetivoSolicitacao;
    } //-- java.math.BigDecimal getVlPagamentoEfetivoSolicitacao() 

    /**
     * Returns the value of field 'vlPagamentoPrevtSolicitacao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoPrevtSolicitacao'.
     */
    public java.math.BigDecimal getVlPagamentoPrevtSolicitacao()
    {
        return this._vlPagamentoPrevtSolicitacao;
    } //-- java.math.BigDecimal getVlPagamentoPrevtSolicitacao() 

    /**
     * Returns the value of field 'vlrPagamentosProcessados'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrPagamentosProcessados'.
     */
    public java.math.BigDecimal getVlrPagamentosProcessados()
    {
        return this._vlrPagamentosProcessados;
    } //-- java.math.BigDecimal getVlrPagamentosProcessados() 

    /**
     * Method hasCdAgenciaBancariaPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancariaPagador()
    {
        return this._has_cdAgenciaBancariaPagador;
    } //-- boolean hasCdAgenciaBancariaPagador() 

    /**
     * Method hasCdBancoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoPagador()
    {
        return this._has_cdBancoPagador;
    } //-- boolean hasCdBancoPagador() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdContaBancariaPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancariaPagador()
    {
        return this._has_cdContaBancariaPagador;
    } //-- boolean hasCdContaBancariaPagador() 

    /**
     * Method hasCdControleCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpjPagador()
    {
        return this._has_cdControleCpfCnpjPagador;
    } //-- boolean hasCdControleCpfCnpjPagador() 

    /**
     * Method hasCdCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPagador()
    {
        return this._has_cdCpfCnpjPagador;
    } //-- boolean hasCdCpfCnpjPagador() 

    /**
     * Method hasCdFilialCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjPagador()
    {
        return this._has_cdFilialCpfCnpjPagador;
    } //-- boolean hasCdFilialCpfCnpjPagador() 

    /**
     * Method hasCdMotivoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSolicitacao()
    {
        return this._has_cdMotivoSolicitacao;
    } //-- boolean hasCdMotivoSolicitacao() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdSituacaoPagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPagamentoCliente()
    {
        return this._has_cdSituacaoPagamentoCliente;
    } //-- boolean hasCdSituacaoPagamentoCliente() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamentoIntegrado()
    {
        return this._has_cdSolicitacaoPagamentoIntegrado;
    } //-- boolean hasCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdTipoContaPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaPagador()
    {
        return this._has_cdTipoContaPagador;
    } //-- boolean hasCdTipoContaPagador() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoInscricaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoPagador()
    {
        return this._has_cdTipoInscricaoPagador;
    } //-- boolean hasCdTipoInscricaoPagador() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrArquivoRemssaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemssaPagamento()
    {
        return this._has_nrArquivoRemssaPagamento;
    } //-- boolean hasNrArquivoRemssaPagamento() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamentoIntegrado()
    {
        return this._has_nrSolicitacaoPagamentoIntegrado;
    } //-- boolean hasNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasQtdPagamentoEfetvSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdPagamentoEfetvSolicitacao()
    {
        return this._has_qtdPagamentoEfetvSolicitacao;
    } //-- boolean hasQtdPagamentoEfetvSolicitacao() 

    /**
     * Method hasQtdPagamentoPrevtSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdPagamentoPrevtSolicitacao()
    {
        return this._has_qtdPagamentoPrevtSolicitacao;
    } //-- boolean hasQtdPagamentoPrevtSolicitacao() 

    /**
     * Method hasQtdePagamentosProcessados
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdePagamentosProcessados()
    {
        return this._has_qtdePagamentosProcessados;
    } //-- boolean hasQtdePagamentosProcessados() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaBancariaPagador'.
     * 
     * @param cdAgenciaBancariaPagador the value of field
     * 'cdAgenciaBancariaPagador'.
     */
    public void setCdAgenciaBancariaPagador(int cdAgenciaBancariaPagador)
    {
        this._cdAgenciaBancariaPagador = cdAgenciaBancariaPagador;
        this._has_cdAgenciaBancariaPagador = true;
    } //-- void setCdAgenciaBancariaPagador(int) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdBancoPagador'.
     * 
     * @param cdBancoPagador the value of field 'cdBancoPagador'.
     */
    public void setCdBancoPagador(int cdBancoPagador)
    {
        this._cdBancoPagador = cdBancoPagador;
        this._has_cdBancoPagador = true;
    } //-- void setCdBancoPagador(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdContaBancariaPagador'.
     * 
     * @param cdContaBancariaPagador the value of field
     * 'cdContaBancariaPagador'.
     */
    public void setCdContaBancariaPagador(long cdContaBancariaPagador)
    {
        this._cdContaBancariaPagador = cdContaBancariaPagador;
        this._has_cdContaBancariaPagador = true;
    } //-- void setCdContaBancariaPagador(long) 

    /**
     * Sets the value of field 'cdControleCpfCnpjPagador'.
     * 
     * @param cdControleCpfCnpjPagador the value of field
     * 'cdControleCpfCnpjPagador'.
     */
    public void setCdControleCpfCnpjPagador(int cdControleCpfCnpjPagador)
    {
        this._cdControleCpfCnpjPagador = cdControleCpfCnpjPagador;
        this._has_cdControleCpfCnpjPagador = true;
    } //-- void setCdControleCpfCnpjPagador(int) 

    /**
     * Sets the value of field 'cdCpfCnpjPagador'.
     * 
     * @param cdCpfCnpjPagador the value of field 'cdCpfCnpjPagador'
     */
    public void setCdCpfCnpjPagador(long cdCpfCnpjPagador)
    {
        this._cdCpfCnpjPagador = cdCpfCnpjPagador;
        this._has_cdCpfCnpjPagador = true;
    } //-- void setCdCpfCnpjPagador(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaPagador'.
     * 
     * @param cdDigitoAgenciaPagador the value of field
     * 'cdDigitoAgenciaPagador'.
     */
    public void setCdDigitoAgenciaPagador(java.lang.String cdDigitoAgenciaPagador)
    {
        this._cdDigitoAgenciaPagador = cdDigitoAgenciaPagador;
    } //-- void setCdDigitoAgenciaPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaPagador'.
     * 
     * @param cdDigitoContaPagador the value of field
     * 'cdDigitoContaPagador'.
     */
    public void setCdDigitoContaPagador(java.lang.String cdDigitoContaPagador)
    {
        this._cdDigitoContaPagador = cdDigitoContaPagador;
    } //-- void setCdDigitoContaPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjPagador'.
     * 
     * @param cdFilialCpfCnpjPagador the value of field
     * 'cdFilialCpfCnpjPagador'.
     */
    public void setCdFilialCpfCnpjPagador(int cdFilialCpfCnpjPagador)
    {
        this._cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
        this._has_cdFilialCpfCnpjPagador = true;
    } //-- void setCdFilialCpfCnpjPagador(int) 

    /**
     * Sets the value of field 'cdMotivoSolicitacao'.
     * 
     * @param cdMotivoSolicitacao the value of field
     * 'cdMotivoSolicitacao'.
     */
    public void setCdMotivoSolicitacao(int cdMotivoSolicitacao)
    {
        this._cdMotivoSolicitacao = cdMotivoSolicitacao;
        this._has_cdMotivoSolicitacao = true;
    } //-- void setCdMotivoSolicitacao(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdSituacaoPagamentoCliente'.
     * 
     * @param cdSituacaoPagamentoCliente the value of field
     * 'cdSituacaoPagamentoCliente'.
     */
    public void setCdSituacaoPagamentoCliente(int cdSituacaoPagamentoCliente)
    {
        this._cdSituacaoPagamentoCliente = cdSituacaoPagamentoCliente;
        this._has_cdSituacaoPagamentoCliente = true;
    } //-- void setCdSituacaoPagamentoCliente(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @param cdSolicitacaoPagamentoIntegrado the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     */
    public void setCdSolicitacaoPagamentoIntegrado(int cdSolicitacaoPagamentoIntegrado)
    {
        this._cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
        this._has_cdSolicitacaoPagamentoIntegrado = true;
    } //-- void setCdSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdTipoContaPagador'.
     * 
     * @param cdTipoContaPagador the value of field
     * 'cdTipoContaPagador'.
     */
    public void setCdTipoContaPagador(int cdTipoContaPagador)
    {
        this._cdTipoContaPagador = cdTipoContaPagador;
        this._has_cdTipoContaPagador = true;
    } //-- void setCdTipoContaPagador(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoPagador'.
     * 
     * @param cdTipoInscricaoPagador the value of field
     * 'cdTipoInscricaoPagador'.
     */
    public void setCdTipoInscricaoPagador(int cdTipoInscricaoPagador)
    {
        this._cdTipoInscricaoPagador = cdTipoInscricaoPagador;
        this._has_cdTipoInscricaoPagador = true;
    } //-- void setCdTipoInscricaoPagador(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaBancariaPagador'.
     * 
     * @param dsAgenciaBancariaPagador the value of field
     * 'dsAgenciaBancariaPagador'.
     */
    public void setDsAgenciaBancariaPagador(java.lang.String dsAgenciaBancariaPagador)
    {
        this._dsAgenciaBancariaPagador = dsAgenciaBancariaPagador;
    } //-- void setDsAgenciaBancariaPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoPagador'.
     * 
     * @param dsBancoPagador the value of field 'dsBancoPagador'.
     */
    public void setDsBancoPagador(java.lang.String dsBancoPagador)
    {
        this._dsBancoPagador = dsBancoPagador;
    } //-- void setDsBancoPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoPagador'.
     * 
     * @param dsComplementoPagador the value of field
     * 'dsComplementoPagador'.
     */
    public void setDsComplementoPagador(java.lang.String dsComplementoPagador)
    {
        this._dsComplementoPagador = dsComplementoPagador;
    } //-- void setDsComplementoPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSolicitacao'.
     * 
     * @param dsMotivoSolicitacao the value of field
     * 'dsMotivoSolicitacao'.
     */
    public void setDsMotivoSolicitacao(java.lang.String dsMotivoSolicitacao)
    {
        this._dsMotivoSolicitacao = dsMotivoSolicitacao;
    } //-- void setDsMotivoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperRelacionado'.
     * 
     * @param dsProdutoOperRelacionado the value of field
     * 'dsProdutoOperRelacionado'.
     */
    public void setDsProdutoOperRelacionado(java.lang.String dsProdutoOperRelacionado)
    {
        this._dsProdutoOperRelacionado = dsProdutoOperRelacionado;
    } //-- void setDsProdutoOperRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPagamentoCliente'.
     * 
     * @param dsSituacaoPagamentoCliente the value of field
     * 'dsSituacaoPagamentoCliente'.
     */
    public void setDsSituacaoPagamentoCliente(java.lang.String dsSituacaoPagamentoCliente)
    {
        this._dsSituacaoPagamentoCliente = dsSituacaoPagamentoCliente;
    } //-- void setDsSituacaoPagamentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacaoPagamento'.
     * 
     * @param dsSituacaoSolicitacaoPagamento the value of field
     * 'dsSituacaoSolicitacaoPagamento'.
     */
    public void setDsSituacaoSolicitacaoPagamento(java.lang.String dsSituacaoSolicitacaoPagamento)
    {
        this._dsSituacaoSolicitacaoPagamento = dsSituacaoSolicitacaoPagamento;
    } //-- void setDsSituacaoSolicitacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContaPagador'.
     * 
     * @param dsTipoContaPagador the value of field
     * 'dsTipoContaPagador'.
     */
    public void setDsTipoContaPagador(java.lang.String dsTipoContaPagador)
    {
        this._dsTipoContaPagador = dsTipoContaPagador;
    } //-- void setDsTipoContaPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoSolicitacao'.
     * 
     * @param dsTipoSolicitacao the value of field
     * 'dsTipoSolicitacao'.
     */
    public void setDsTipoSolicitacao(java.lang.String dsTipoSolicitacao)
    {
        this._dsTipoSolicitacao = dsTipoSolicitacao;
    } //-- void setDsTipoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtAgendamentoPagamento'.
     * 
     * @param dtAgendamentoPagamento the value of field
     * 'dtAgendamentoPagamento'.
     */
    public void setDtAgendamentoPagamento(java.lang.String dtAgendamentoPagamento)
    {
        this._dtAgendamentoPagamento = dtAgendamentoPagamento;
    } //-- void setDtAgendamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtAtendimentoSolicitacaoPagamento'.
     * 
     * @param dtAtendimentoSolicitacaoPagamento the value of field
     * 'dtAtendimentoSolicitacaoPagamento'.
     */
    public void setDtAtendimentoSolicitacaoPagamento(java.lang.String dtAtendimentoSolicitacaoPagamento)
    {
        this._dtAtendimentoSolicitacaoPagamento = dtAtendimentoSolicitacaoPagamento;
    } //-- void setDtAtendimentoSolicitacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtPrevistaAgendaPagamento'.
     * 
     * @param dtPrevistaAgendaPagamento the value of field
     * 'dtPrevistaAgendaPagamento'.
     */
    public void setDtPrevistaAgendaPagamento(java.lang.String dtPrevistaAgendaPagamento)
    {
        this._dtPrevistaAgendaPagamento = dtPrevistaAgendaPagamento;
    } //-- void setDtPrevistaAgendaPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrAtendimentoSolicitacaoPagamento'.
     * 
     * @param hrAtendimentoSolicitacaoPagamento the value of field
     * 'hrAtendimentoSolicitacaoPagamento'.
     */
    public void setHrAtendimentoSolicitacaoPagamento(java.lang.String hrAtendimentoSolicitacaoPagamento)
    {
        this._hrAtendimentoSolicitacaoPagamento = hrAtendimentoSolicitacaoPagamento;
    } //-- void setHrAtendimentoSolicitacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemssaPagamento'.
     * 
     * @param nrArquivoRemssaPagamento the value of field
     * 'nrArquivoRemssaPagamento'.
     */
    public void setNrArquivoRemssaPagamento(long nrArquivoRemssaPagamento)
    {
        this._nrArquivoRemssaPagamento = nrArquivoRemssaPagamento;
        this._has_nrArquivoRemssaPagamento = true;
    } //-- void setNrArquivoRemssaPagamento(long) 

    /**
     * Sets the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @param nrOperacaoFluxoInclusao the value of field
     * 'nrOperacaoFluxoInclusao'.
     */
    public void setNrOperacaoFluxoInclusao(java.lang.String nrOperacaoFluxoInclusao)
    {
        this._nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    } //-- void setNrOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @param nrSolicitacaoPagamentoIntegrado the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     */
    public void setNrSolicitacaoPagamentoIntegrado(int nrSolicitacaoPagamentoIntegrado)
    {
        this._nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
        this._has_nrSolicitacaoPagamentoIntegrado = true;
    } //-- void setNrSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'qtdPagamentoEfetvSolicitacao'.
     * 
     * @param qtdPagamentoEfetvSolicitacao the value of field
     * 'qtdPagamentoEfetvSolicitacao'.
     */
    public void setQtdPagamentoEfetvSolicitacao(long qtdPagamentoEfetvSolicitacao)
    {
        this._qtdPagamentoEfetvSolicitacao = qtdPagamentoEfetvSolicitacao;
        this._has_qtdPagamentoEfetvSolicitacao = true;
    } //-- void setQtdPagamentoEfetvSolicitacao(long) 

    /**
     * Sets the value of field 'qtdPagamentoPrevtSolicitacao'.
     * 
     * @param qtdPagamentoPrevtSolicitacao the value of field
     * 'qtdPagamentoPrevtSolicitacao'.
     */
    public void setQtdPagamentoPrevtSolicitacao(long qtdPagamentoPrevtSolicitacao)
    {
        this._qtdPagamentoPrevtSolicitacao = qtdPagamentoPrevtSolicitacao;
        this._has_qtdPagamentoPrevtSolicitacao = true;
    } //-- void setQtdPagamentoPrevtSolicitacao(long) 

    /**
     * Sets the value of field 'qtdePagamentosProcessados'.
     * 
     * @param qtdePagamentosProcessados the value of field
     * 'qtdePagamentosProcessados'.
     */
    public void setQtdePagamentosProcessados(long qtdePagamentosProcessados)
    {
        this._qtdePagamentosProcessados = qtdePagamentosProcessados;
        this._has_qtdePagamentosProcessados = true;
    } //-- void setQtdePagamentosProcessados(long) 

    /**
     * Sets the value of field 'vlPagamentoEfetivoSolicitacao'.
     * 
     * @param vlPagamentoEfetivoSolicitacao the value of field
     * 'vlPagamentoEfetivoSolicitacao'.
     */
    public void setVlPagamentoEfetivoSolicitacao(java.math.BigDecimal vlPagamentoEfetivoSolicitacao)
    {
        this._vlPagamentoEfetivoSolicitacao = vlPagamentoEfetivoSolicitacao;
    } //-- void setVlPagamentoEfetivoSolicitacao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoPrevtSolicitacao'.
     * 
     * @param vlPagamentoPrevtSolicitacao the value of field
     * 'vlPagamentoPrevtSolicitacao'.
     */
    public void setVlPagamentoPrevtSolicitacao(java.math.BigDecimal vlPagamentoPrevtSolicitacao)
    {
        this._vlPagamentoPrevtSolicitacao = vlPagamentoPrevtSolicitacao;
    } //-- void setVlPagamentoPrevtSolicitacao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrPagamentosProcessados'.
     * 
     * @param vlrPagamentosProcessados the value of field
     * 'vlrPagamentosProcessados'.
     */
    public void setVlrPagamentosProcessados(java.math.BigDecimal vlrPagamentosProcessados)
    {
        this._vlrPagamentosProcessados = vlrPagamentosProcessados;
    } //-- void setVlrPagamentosProcessados(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalhePostergarConsolidadoSolicitacaoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.response.DetalhePostergarConsolidadoSolicitacaoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.response.DetalhePostergarConsolidadoSolicitacaoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.response.DetalhePostergarConsolidadoSolicitacaoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhepostergarconsolidadosolicitacao.response.DetalhePostergarConsolidadoSolicitacaoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
