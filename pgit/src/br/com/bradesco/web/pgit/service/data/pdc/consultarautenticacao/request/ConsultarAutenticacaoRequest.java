/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarAutenticacaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarAutenticacaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _vlrPagamentoClientePagador
     */
    private java.math.BigDecimal _vlrPagamentoClientePagador = new java.math.BigDecimal("0");

    /**
     * Field _dtPagamento
     */
    private java.lang.String _dtPagamento;

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _cdFlagModalidade
     */
    private int _cdFlagModalidade = 0;

    /**
     * keeps track of state for field: _cdFlagModalidade
     */
    private boolean _has_cdFlagModalidade;

    /**
     * Field _nrPagamento
     */
    private java.lang.String _nrPagamento;

    /**
     * Field _cdFlCodigoBarras
     */
    private java.lang.String _cdFlCodigoBarras;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarAutenticacaoRequest() 
     {
        super();
        setVlrPagamentoClientePagador(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.request.ConsultarAutenticacaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdFlagModalidade
     * 
     */
    public void deleteCdFlagModalidade()
    {
        this._has_cdFlagModalidade= false;
    } //-- void deleteCdFlagModalidade() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFlCodigoBarras'.
     * 
     * @return String
     * @return the value of field 'cdFlCodigoBarras'.
     */
    public java.lang.String getCdFlCodigoBarras()
    {
        return this._cdFlCodigoBarras;
    } //-- java.lang.String getCdFlCodigoBarras() 

    /**
     * Returns the value of field 'cdFlagModalidade'.
     * 
     * @return int
     * @return the value of field 'cdFlagModalidade'.
     */
    public int getCdFlagModalidade()
    {
        return this._cdFlagModalidade;
    } //-- int getCdFlagModalidade() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'dtPagamento'.
     * 
     * @return String
     * @return the value of field 'dtPagamento'.
     */
    public java.lang.String getDtPagamento()
    {
        return this._dtPagamento;
    } //-- java.lang.String getDtPagamento() 

    /**
     * Returns the value of field 'nrPagamento'.
     * 
     * @return String
     * @return the value of field 'nrPagamento'.
     */
    public java.lang.String getNrPagamento()
    {
        return this._nrPagamento;
    } //-- java.lang.String getNrPagamento() 

    /**
     * Returns the value of field 'vlrPagamentoClientePagador'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrPagamentoClientePagador'.
     */
    public java.math.BigDecimal getVlrPagamentoClientePagador()
    {
        return this._vlrPagamentoClientePagador;
    } //-- java.math.BigDecimal getVlrPagamentoClientePagador() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdFlagModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFlagModalidade()
    {
        return this._has_cdFlagModalidade;
    } //-- boolean hasCdFlagModalidade() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFlCodigoBarras'.
     * 
     * @param cdFlCodigoBarras the value of field 'cdFlCodigoBarras'
     */
    public void setCdFlCodigoBarras(java.lang.String cdFlCodigoBarras)
    {
        this._cdFlCodigoBarras = cdFlCodigoBarras;
    } //-- void setCdFlCodigoBarras(java.lang.String) 

    /**
     * Sets the value of field 'cdFlagModalidade'.
     * 
     * @param cdFlagModalidade the value of field 'cdFlagModalidade'
     */
    public void setCdFlagModalidade(int cdFlagModalidade)
    {
        this._cdFlagModalidade = cdFlagModalidade;
        this._has_cdFlagModalidade = true;
    } //-- void setCdFlagModalidade(int) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'dtPagamento'.
     * 
     * @param dtPagamento the value of field 'dtPagamento'.
     */
    public void setDtPagamento(java.lang.String dtPagamento)
    {
        this._dtPagamento = dtPagamento;
    } //-- void setDtPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrPagamento'.
     * 
     * @param nrPagamento the value of field 'nrPagamento'.
     */
    public void setNrPagamento(java.lang.String nrPagamento)
    {
        this._nrPagamento = nrPagamento;
    } //-- void setNrPagamento(java.lang.String) 

    /**
     * Sets the value of field 'vlrPagamentoClientePagador'.
     * 
     * @param vlrPagamentoClientePagador the value of field
     * 'vlrPagamentoClientePagador'.
     */
    public void setVlrPagamentoClientePagador(java.math.BigDecimal vlrPagamentoClientePagador)
    {
        this._vlrPagamentoClientePagador = vlrPagamentoClientePagador;
    } //-- void setVlrPagamentoClientePagador(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarAutenticacaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.request.ConsultarAutenticacaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.request.ConsultarAutenticacaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.request.ConsultarAutenticacaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarautenticacao.request.ConsultarAutenticacaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
