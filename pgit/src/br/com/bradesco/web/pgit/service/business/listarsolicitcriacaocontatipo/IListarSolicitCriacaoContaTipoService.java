/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.DetalharSolicCriacaoContaTipoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.DetalharSolicCriacaoContaTipoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ExcluirSolicCriacaoContaTipoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ExcluirSolicCriacaoContaTipoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.IncluirSolicCriacaoContaTipoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.IncluirSolicCriacaoContaTipoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ListarSolicCriacaoContaTipoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ListarSolicCriacaoContaTipoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ListarSolicitCriacaoContaTipo
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IListarSolicitCriacaoContaTipoService {
	
	/**
	 * Consultar solicitacao conta tipo998.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar solic criacao conta tipo saida dt o>
	 */
	List<ListarSolicCriacaoContaTipoSaidaDTO> consultarSolicitacaoContaTipo998(ListarSolicCriacaoContaTipoEntradaDTO entradaDTO);
	
	/**
	 * Detalhar solicitacao conta tipo998.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar solic criacao conta tipo saida dto
	 */
	DetalharSolicCriacaoContaTipoSaidaDTO detalharSolicitacaoContaTipo998(DetalharSolicCriacaoContaTipoEntradaDTO entradaDTO);
	
	/**
	 * Incluir solicitacao conta tipo998.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the incluir solic criacao conta tipo saida dto
	 */
	IncluirSolicCriacaoContaTipoSaidaDTO incluirSolicitacaoContaTipo998(IncluirSolicCriacaoContaTipoEntradaDTO entradaDTO);
	
	/**
	 * Excluir solicitacao conta tipo998.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir solic criacao conta tipo saida dto
	 */
	ExcluirSolicCriacaoContaTipoSaidaDTO excluirSolicitacaoContaTipo998(ExcluirSolicCriacaoContaTipoEntradaDTO entradaDTO);
	
}

