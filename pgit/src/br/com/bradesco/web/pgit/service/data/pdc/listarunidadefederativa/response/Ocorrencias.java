/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdUnidadeFederativa
     */
    private int _cdUnidadeFederativa = 0;

    /**
     * keeps track of state for field: _cdUnidadeFederativa
     */
    private boolean _has_cdUnidadeFederativa;

    /**
     * Field _dsUnidadeFederativa
     */
    private java.lang.String _dsUnidadeFederativa;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdUnidadeFederativa
     * 
     */
    public void deleteCdUnidadeFederativa()
    {
        this._has_cdUnidadeFederativa= false;
    } //-- void deleteCdUnidadeFederativa() 

    /**
     * Returns the value of field 'cdUnidadeFederativa'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeFederativa'.
     */
    public int getCdUnidadeFederativa()
    {
        return this._cdUnidadeFederativa;
    } //-- int getCdUnidadeFederativa() 

    /**
     * Returns the value of field 'dsUnidadeFederativa'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeFederativa'.
     */
    public java.lang.String getDsUnidadeFederativa()
    {
        return this._dsUnidadeFederativa;
    } //-- java.lang.String getDsUnidadeFederativa() 

    /**
     * Method hasCdUnidadeFederativa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeFederativa()
    {
        return this._has_cdUnidadeFederativa;
    } //-- boolean hasCdUnidadeFederativa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdUnidadeFederativa'.
     * 
     * @param cdUnidadeFederativa the value of field
     * 'cdUnidadeFederativa'.
     */
    public void setCdUnidadeFederativa(int cdUnidadeFederativa)
    {
        this._cdUnidadeFederativa = cdUnidadeFederativa;
        this._has_cdUnidadeFederativa = true;
    } //-- void setCdUnidadeFederativa(int) 

    /**
     * Sets the value of field 'dsUnidadeFederativa'.
     * 
     * @param dsUnidadeFederativa the value of field
     * 'dsUnidadeFederativa'.
     */
    public void setDsUnidadeFederativa(java.lang.String dsUnidadeFederativa)
    {
        this._dsUnidadeFederativa = dsUnidadeFederativa;
    } //-- void setDsUnidadeFederativa(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarunidadefederativa.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
