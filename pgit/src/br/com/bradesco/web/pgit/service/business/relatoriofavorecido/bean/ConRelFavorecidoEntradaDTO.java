/*
 * Nome: br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.utils.DateUtils;

/**
 * Nome: ConRelFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConRelFavorecidoEntradaDTO {
	
	/** Atributo tpSolicitacao. */
	private Integer tpSolicitacao;
	
	/** Atributo dtInicioSolicitacao. */
	private Date dtInicioSolicitacao = new Date();
	
	/** Atributo dtFimSolicitacao. */
	private Date dtFimSolicitacao = new Date();

	/**
	 * Con rel favorecido entrada dto.
	 */
	public ConRelFavorecidoEntradaDTO() {
		super();
	}
	

	/**
	 * Get: dtFimSolicitacao.
	 *
	 * @return dtFimSolicitacao
	 */
	public Date getDtFimSolicitacao() {
		return dtFimSolicitacao;
	}

	/**
	 * Set: dtFimSolicitacao.
	 *
	 * @param dtFimSolicitacao the dt fim solicitacao
	 */
	public void setDtFimSolicitacao(Date dtFimSolicitacao) {
		this.dtFimSolicitacao = dtFimSolicitacao;
	}

	/**
	 * Get: dtInicioSolicitacao.
	 *
	 * @return dtInicioSolicitacao
	 */
	public Date getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}

	/**
	 * Set: dtInicioSolicitacao.
	 *
	 * @param dtInicioSolicitacao the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(Date dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}

	/**
	 * Get: tpSolicitacao.
	 *
	 * @return tpSolicitacao
	 */
	public Integer getTpSolicitacao() {
		return tpSolicitacao;
	}

	/**
	 * Set: tpSolicitacao.
	 *
	 * @param tpSolicitacao the tp solicitacao
	 */
	public void setTpSolicitacao(Integer tpSolicitacao) {
		this.tpSolicitacao = tpSolicitacao;
	}
	
	/**
	 * Get: dtFimSolicitacaoConv.
	 *
	 * @return dtFimSolicitacaoConv
	 */
	public String getDtFimSolicitacaoConv(){
		return DateUtils.formartarData(dtFimSolicitacao, "dd.MM.yyyy");
	}
	
	/**
	 * Get: dtInicioSolicitacaoConv.
	 *
	 * @return dtInicioSolicitacaoConv
	 */
	public String getDtInicioSolicitacaoConv(){
		return DateUtils.formartarData(dtInicioSolicitacao, "dd.MM.yyyy");
	}
}
