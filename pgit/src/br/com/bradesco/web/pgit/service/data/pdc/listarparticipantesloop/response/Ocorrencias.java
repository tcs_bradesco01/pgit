/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdFilialCnpj
     */
    private int _cdFilialCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCnpj
     */
    private boolean _has_cdFilialCnpj;

    /**
     * Field _cdControleCnpj
     */
    private int _cdControleCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCnpj
     */
    private boolean _has_cdControleCnpj;

    /**
     * Field _nmRazao
     */
    private java.lang.String _nmRazao;

    /**
     * Field _cdGrupoEconomico
     */
    private long _cdGrupoEconomico = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomico
     */
    private boolean _has_cdGrupoEconomico;

    /**
     * Field _dsGrupoEconomico
     */
    private java.lang.String _dsGrupoEconomico;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdSegmentoEconomico
     */
    private int _cdSegmentoEconomico = 0;

    /**
     * keeps track of state for field: _cdSegmentoEconomico
     */
    private boolean _has_cdSegmentoEconomico;

    /**
     * Field _dsSegmentoEconomico
     */
    private java.lang.String _dsSegmentoEconomico;

    /**
     * Field _cdSubSegmento
     */
    private int _cdSubSegmento = 0;

    /**
     * keeps track of state for field: _cdSubSegmento
     */
    private boolean _has_cdSubSegmento;

    /**
     * Field _dsSubSegmento
     */
    private java.lang.String _dsSubSegmento;

    /**
     * Field _cdTipoParticipacao
     */
    private int _cdTipoParticipacao = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacao
     */
    private boolean _has_cdTipoParticipacao;

    /**
     * Field _dsTipoParticipacao
     */
    private java.lang.String _dsTipoParticipacao;

    /**
     * Field _cdSituacaoParticipacao
     */
    private int _cdSituacaoParticipacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoParticipacao
     */
    private boolean _has_cdSituacaoParticipacao;

    /**
     * Field _dsSituacaoParticipacao
     */
    private java.lang.String _dsSituacaoParticipacao;

    /**
     * Field _cdMotivoParticipacao
     */
    private int _cdMotivoParticipacao = 0;

    /**
     * keeps track of state for field: _cdMotivoParticipacao
     */
    private boolean _has_cdMotivoParticipacao;

    /**
     * Field _dsMotivoParticipacao
     */
    private java.lang.String _dsMotivoParticipacao;

    /**
     * Field _cdClasificacaoParticipante
     */
    private int _cdClasificacaoParticipante = 0;

    /**
     * keeps track of state for field: _cdClasificacaoParticipante
     */
    private boolean _has_cdClasificacaoParticipante;

    /**
     * Field _dsClasificacaoParticipante
     */
    private java.lang.String _dsClasificacaoParticipante;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioExternoInclusao
     */
    private java.lang.String _cdUsuarioExternoInclusao;

    /**
     * Field _hrManutencaoRegistroInclusao
     */
    private java.lang.String _hrManutencaoRegistroInclusao;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _cdUsuarioAlteracao
     */
    private java.lang.String _cdUsuarioAlteracao;

    /**
     * Field _cdUsuarioExternoAlteracao
     */
    private java.lang.String _cdUsuarioExternoAlteracao;

    /**
     * Field _hrManutencaoRegistroAlteracao
     */
    private java.lang.String _hrManutencaoRegistroAlteracao;

    /**
     * Field _cdCanalAlteracao
     */
    private int _cdCanalAlteracao = 0;

    /**
     * keeps track of state for field: _cdCanalAlteracao
     */
    private boolean _has_cdCanalAlteracao;

    /**
     * Field _dsCanalAlteracao
     */
    private java.lang.String _dsCanalAlteracao;

    /**
     * Field _nmOperacaoFluxoAlteracao
     */
    private java.lang.String _nmOperacaoFluxoAlteracao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdCanalAlteracao
     * 
     */
    public void deleteCdCanalAlteracao()
    {
        this._has_cdCanalAlteracao= false;
    } //-- void deleteCdCanalAlteracao() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdClasificacaoParticipante
     * 
     */
    public void deleteCdClasificacaoParticipante()
    {
        this._has_cdClasificacaoParticipante= false;
    } //-- void deleteCdClasificacaoParticipante() 

    /**
     * Method deleteCdControleCnpj
     * 
     */
    public void deleteCdControleCnpj()
    {
        this._has_cdControleCnpj= false;
    } //-- void deleteCdControleCnpj() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdFilialCnpj
     * 
     */
    public void deleteCdFilialCnpj()
    {
        this._has_cdFilialCnpj= false;
    } //-- void deleteCdFilialCnpj() 

    /**
     * Method deleteCdGrupoEconomico
     * 
     */
    public void deleteCdGrupoEconomico()
    {
        this._has_cdGrupoEconomico= false;
    } //-- void deleteCdGrupoEconomico() 

    /**
     * Method deleteCdMotivoParticipacao
     * 
     */
    public void deleteCdMotivoParticipacao()
    {
        this._has_cdMotivoParticipacao= false;
    } //-- void deleteCdMotivoParticipacao() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdSegmentoEconomico
     * 
     */
    public void deleteCdSegmentoEconomico()
    {
        this._has_cdSegmentoEconomico= false;
    } //-- void deleteCdSegmentoEconomico() 

    /**
     * Method deleteCdSituacaoParticipacao
     * 
     */
    public void deleteCdSituacaoParticipacao()
    {
        this._has_cdSituacaoParticipacao= false;
    } //-- void deleteCdSituacaoParticipacao() 

    /**
     * Method deleteCdSubSegmento
     * 
     */
    public void deleteCdSubSegmento()
    {
        this._has_cdSubSegmento= false;
    } //-- void deleteCdSubSegmento() 

    /**
     * Method deleteCdTipoParticipacao
     * 
     */
    public void deleteCdTipoParticipacao()
    {
        this._has_cdTipoParticipacao= false;
    } //-- void deleteCdTipoParticipacao() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdCanalAlteracao'.
     * 
     * @return int
     * @return the value of field 'cdCanalAlteracao'.
     */
    public int getCdCanalAlteracao()
    {
        return this._cdCanalAlteracao;
    } //-- int getCdCanalAlteracao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdClasificacaoParticipante'.
     * 
     * @return int
     * @return the value of field 'cdClasificacaoParticipante'.
     */
    public int getCdClasificacaoParticipante()
    {
        return this._cdClasificacaoParticipante;
    } //-- int getCdClasificacaoParticipante() 

    /**
     * Returns the value of field 'cdControleCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpj'.
     */
    public int getCdControleCnpj()
    {
        return this._cdControleCnpj;
    } //-- int getCdControleCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpj'.
     */
    public int getCdFilialCnpj()
    {
        return this._cdFilialCnpj;
    } //-- int getCdFilialCnpj() 

    /**
     * Returns the value of field 'cdGrupoEconomico'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomico'.
     */
    public long getCdGrupoEconomico()
    {
        return this._cdGrupoEconomico;
    } //-- long getCdGrupoEconomico() 

    /**
     * Returns the value of field 'cdMotivoParticipacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoParticipacao'.
     */
    public int getCdMotivoParticipacao()
    {
        return this._cdMotivoParticipacao;
    } //-- int getCdMotivoParticipacao() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdSegmentoEconomico'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoEconomico'.
     */
    public int getCdSegmentoEconomico()
    {
        return this._cdSegmentoEconomico;
    } //-- int getCdSegmentoEconomico() 

    /**
     * Returns the value of field 'cdSituacaoParticipacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoParticipacao'.
     */
    public int getCdSituacaoParticipacao()
    {
        return this._cdSituacaoParticipacao;
    } //-- int getCdSituacaoParticipacao() 

    /**
     * Returns the value of field 'cdSubSegmento'.
     * 
     * @return int
     * @return the value of field 'cdSubSegmento'.
     */
    public int getCdSubSegmento()
    {
        return this._cdSubSegmento;
    } //-- int getCdSubSegmento() 

    /**
     * Returns the value of field 'cdTipoParticipacao'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacao'.
     */
    public int getCdTipoParticipacao()
    {
        return this._cdTipoParticipacao;
    } //-- int getCdTipoParticipacao() 

    /**
     * Returns the value of field 'cdUsuarioAlteracao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioAlteracao'.
     */
    public java.lang.String getCdUsuarioAlteracao()
    {
        return this._cdUsuarioAlteracao;
    } //-- java.lang.String getCdUsuarioAlteracao() 

    /**
     * Returns the value of field 'cdUsuarioExternoAlteracao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExternoAlteracao'.
     */
    public java.lang.String getCdUsuarioExternoAlteracao()
    {
        return this._cdUsuarioExternoAlteracao;
    } //-- java.lang.String getCdUsuarioExternoAlteracao() 

    /**
     * Returns the value of field 'cdUsuarioExternoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExternoInclusao'.
     */
    public java.lang.String getCdUsuarioExternoInclusao()
    {
        return this._cdUsuarioExternoInclusao;
    } //-- java.lang.String getCdUsuarioExternoInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsCanalAlteracao'.
     * 
     * @return String
     * @return the value of field 'dsCanalAlteracao'.
     */
    public java.lang.String getDsCanalAlteracao()
    {
        return this._dsCanalAlteracao;
    } //-- java.lang.String getDsCanalAlteracao() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsClasificacaoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsClasificacaoParticipante'.
     */
    public java.lang.String getDsClasificacaoParticipante()
    {
        return this._dsClasificacaoParticipante;
    } //-- java.lang.String getDsClasificacaoParticipante() 

    /**
     * Returns the value of field 'dsGrupoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomico'.
     */
    public java.lang.String getDsGrupoEconomico()
    {
        return this._dsGrupoEconomico;
    } //-- java.lang.String getDsGrupoEconomico() 

    /**
     * Returns the value of field 'dsMotivoParticipacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoParticipacao'.
     */
    public java.lang.String getDsMotivoParticipacao()
    {
        return this._dsMotivoParticipacao;
    } //-- java.lang.String getDsMotivoParticipacao() 

    /**
     * Returns the value of field 'dsSegmentoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoEconomico'.
     */
    public java.lang.String getDsSegmentoEconomico()
    {
        return this._dsSegmentoEconomico;
    } //-- java.lang.String getDsSegmentoEconomico() 

    /**
     * Returns the value of field 'dsSituacaoParticipacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoParticipacao'.
     */
    public java.lang.String getDsSituacaoParticipacao()
    {
        return this._dsSituacaoParticipacao;
    } //-- java.lang.String getDsSituacaoParticipacao() 

    /**
     * Returns the value of field 'dsSubSegmento'.
     * 
     * @return String
     * @return the value of field 'dsSubSegmento'.
     */
    public java.lang.String getDsSubSegmento()
    {
        return this._dsSubSegmento;
    } //-- java.lang.String getDsSubSegmento() 

    /**
     * Returns the value of field 'dsTipoParticipacao'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipacao'.
     */
    public java.lang.String getDsTipoParticipacao()
    {
        return this._dsTipoParticipacao;
    } //-- java.lang.String getDsTipoParticipacao() 

    /**
     * Returns the value of field 'hrManutencaoRegistroAlteracao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroAlteracao'.
     */
    public java.lang.String getHrManutencaoRegistroAlteracao()
    {
        return this._hrManutencaoRegistroAlteracao;
    } //-- java.lang.String getHrManutencaoRegistroAlteracao() 

    /**
     * Returns the value of field 'hrManutencaoRegistroInclusao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroInclusao'.
     */
    public java.lang.String getHrManutencaoRegistroInclusao()
    {
        return this._hrManutencaoRegistroInclusao;
    } //-- java.lang.String getHrManutencaoRegistroInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoAlteracao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoAlteracao'.
     */
    public java.lang.String getNmOperacaoFluxoAlteracao()
    {
        return this._nmOperacaoFluxoAlteracao;
    } //-- java.lang.String getNmOperacaoFluxoAlteracao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmRazao'.
     * 
     * @return String
     * @return the value of field 'nmRazao'.
     */
    public java.lang.String getNmRazao()
    {
        return this._nmRazao;
    } //-- java.lang.String getNmRazao() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdCanalAlteracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalAlteracao()
    {
        return this._has_cdCanalAlteracao;
    } //-- boolean hasCdCanalAlteracao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdClasificacaoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClasificacaoParticipante()
    {
        return this._has_cdClasificacaoParticipante;
    } //-- boolean hasCdClasificacaoParticipante() 

    /**
     * Method hasCdControleCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpj()
    {
        return this._has_cdControleCnpj;
    } //-- boolean hasCdControleCnpj() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpj()
    {
        return this._has_cdFilialCnpj;
    } //-- boolean hasCdFilialCnpj() 

    /**
     * Method hasCdGrupoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomico()
    {
        return this._has_cdGrupoEconomico;
    } //-- boolean hasCdGrupoEconomico() 

    /**
     * Method hasCdMotivoParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoParticipacao()
    {
        return this._has_cdMotivoParticipacao;
    } //-- boolean hasCdMotivoParticipacao() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdSegmentoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoEconomico()
    {
        return this._has_cdSegmentoEconomico;
    } //-- boolean hasCdSegmentoEconomico() 

    /**
     * Method hasCdSituacaoParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoParticipacao()
    {
        return this._has_cdSituacaoParticipacao;
    } //-- boolean hasCdSituacaoParticipacao() 

    /**
     * Method hasCdSubSegmento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubSegmento()
    {
        return this._has_cdSubSegmento;
    } //-- boolean hasCdSubSegmento() 

    /**
     * Method hasCdTipoParticipacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacao()
    {
        return this._has_cdTipoParticipacao;
    } //-- boolean hasCdTipoParticipacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdCanalAlteracao'.
     * 
     * @param cdCanalAlteracao the value of field 'cdCanalAlteracao'
     */
    public void setCdCanalAlteracao(int cdCanalAlteracao)
    {
        this._cdCanalAlteracao = cdCanalAlteracao;
        this._has_cdCanalAlteracao = true;
    } //-- void setCdCanalAlteracao(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdClasificacaoParticipante'.
     * 
     * @param cdClasificacaoParticipante the value of field
     * 'cdClasificacaoParticipante'.
     */
    public void setCdClasificacaoParticipante(int cdClasificacaoParticipante)
    {
        this._cdClasificacaoParticipante = cdClasificacaoParticipante;
        this._has_cdClasificacaoParticipante = true;
    } //-- void setCdClasificacaoParticipante(int) 

    /**
     * Sets the value of field 'cdControleCnpj'.
     * 
     * @param cdControleCnpj the value of field 'cdControleCnpj'.
     */
    public void setCdControleCnpj(int cdControleCnpj)
    {
        this._cdControleCnpj = cdControleCnpj;
        this._has_cdControleCnpj = true;
    } //-- void setCdControleCnpj(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdFilialCnpj'.
     * 
     * @param cdFilialCnpj the value of field 'cdFilialCnpj'.
     */
    public void setCdFilialCnpj(int cdFilialCnpj)
    {
        this._cdFilialCnpj = cdFilialCnpj;
        this._has_cdFilialCnpj = true;
    } //-- void setCdFilialCnpj(int) 

    /**
     * Sets the value of field 'cdGrupoEconomico'.
     * 
     * @param cdGrupoEconomico the value of field 'cdGrupoEconomico'
     */
    public void setCdGrupoEconomico(long cdGrupoEconomico)
    {
        this._cdGrupoEconomico = cdGrupoEconomico;
        this._has_cdGrupoEconomico = true;
    } //-- void setCdGrupoEconomico(long) 

    /**
     * Sets the value of field 'cdMotivoParticipacao'.
     * 
     * @param cdMotivoParticipacao the value of field
     * 'cdMotivoParticipacao'.
     */
    public void setCdMotivoParticipacao(int cdMotivoParticipacao)
    {
        this._cdMotivoParticipacao = cdMotivoParticipacao;
        this._has_cdMotivoParticipacao = true;
    } //-- void setCdMotivoParticipacao(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdSegmentoEconomico'.
     * 
     * @param cdSegmentoEconomico the value of field
     * 'cdSegmentoEconomico'.
     */
    public void setCdSegmentoEconomico(int cdSegmentoEconomico)
    {
        this._cdSegmentoEconomico = cdSegmentoEconomico;
        this._has_cdSegmentoEconomico = true;
    } //-- void setCdSegmentoEconomico(int) 

    /**
     * Sets the value of field 'cdSituacaoParticipacao'.
     * 
     * @param cdSituacaoParticipacao the value of field
     * 'cdSituacaoParticipacao'.
     */
    public void setCdSituacaoParticipacao(int cdSituacaoParticipacao)
    {
        this._cdSituacaoParticipacao = cdSituacaoParticipacao;
        this._has_cdSituacaoParticipacao = true;
    } //-- void setCdSituacaoParticipacao(int) 

    /**
     * Sets the value of field 'cdSubSegmento'.
     * 
     * @param cdSubSegmento the value of field 'cdSubSegmento'.
     */
    public void setCdSubSegmento(int cdSubSegmento)
    {
        this._cdSubSegmento = cdSubSegmento;
        this._has_cdSubSegmento = true;
    } //-- void setCdSubSegmento(int) 

    /**
     * Sets the value of field 'cdTipoParticipacao'.
     * 
     * @param cdTipoParticipacao the value of field
     * 'cdTipoParticipacao'.
     */
    public void setCdTipoParticipacao(int cdTipoParticipacao)
    {
        this._cdTipoParticipacao = cdTipoParticipacao;
        this._has_cdTipoParticipacao = true;
    } //-- void setCdTipoParticipacao(int) 

    /**
     * Sets the value of field 'cdUsuarioAlteracao'.
     * 
     * @param cdUsuarioAlteracao the value of field
     * 'cdUsuarioAlteracao'.
     */
    public void setCdUsuarioAlteracao(java.lang.String cdUsuarioAlteracao)
    {
        this._cdUsuarioAlteracao = cdUsuarioAlteracao;
    } //-- void setCdUsuarioAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioExternoAlteracao'.
     * 
     * @param cdUsuarioExternoAlteracao the value of field
     * 'cdUsuarioExternoAlteracao'.
     */
    public void setCdUsuarioExternoAlteracao(java.lang.String cdUsuarioExternoAlteracao)
    {
        this._cdUsuarioExternoAlteracao = cdUsuarioExternoAlteracao;
    } //-- void setCdUsuarioExternoAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioExternoInclusao'.
     * 
     * @param cdUsuarioExternoInclusao the value of field
     * 'cdUsuarioExternoInclusao'.
     */
    public void setCdUsuarioExternoInclusao(java.lang.String cdUsuarioExternoInclusao)
    {
        this._cdUsuarioExternoInclusao = cdUsuarioExternoInclusao;
    } //-- void setCdUsuarioExternoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalAlteracao'.
     * 
     * @param dsCanalAlteracao the value of field 'dsCanalAlteracao'
     */
    public void setDsCanalAlteracao(java.lang.String dsCanalAlteracao)
    {
        this._dsCanalAlteracao = dsCanalAlteracao;
    } //-- void setDsCanalAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsClasificacaoParticipante'.
     * 
     * @param dsClasificacaoParticipante the value of field
     * 'dsClasificacaoParticipante'.
     */
    public void setDsClasificacaoParticipante(java.lang.String dsClasificacaoParticipante)
    {
        this._dsClasificacaoParticipante = dsClasificacaoParticipante;
    } //-- void setDsClasificacaoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomico'.
     * 
     * @param dsGrupoEconomico the value of field 'dsGrupoEconomico'
     */
    public void setDsGrupoEconomico(java.lang.String dsGrupoEconomico)
    {
        this._dsGrupoEconomico = dsGrupoEconomico;
    } //-- void setDsGrupoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoParticipacao'.
     * 
     * @param dsMotivoParticipacao the value of field
     * 'dsMotivoParticipacao'.
     */
    public void setDsMotivoParticipacao(java.lang.String dsMotivoParticipacao)
    {
        this._dsMotivoParticipacao = dsMotivoParticipacao;
    } //-- void setDsMotivoParticipacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoEconomico'.
     * 
     * @param dsSegmentoEconomico the value of field
     * 'dsSegmentoEconomico'.
     */
    public void setDsSegmentoEconomico(java.lang.String dsSegmentoEconomico)
    {
        this._dsSegmentoEconomico = dsSegmentoEconomico;
    } //-- void setDsSegmentoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoParticipacao'.
     * 
     * @param dsSituacaoParticipacao the value of field
     * 'dsSituacaoParticipacao'.
     */
    public void setDsSituacaoParticipacao(java.lang.String dsSituacaoParticipacao)
    {
        this._dsSituacaoParticipacao = dsSituacaoParticipacao;
    } //-- void setDsSituacaoParticipacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSubSegmento'.
     * 
     * @param dsSubSegmento the value of field 'dsSubSegmento'.
     */
    public void setDsSubSegmento(java.lang.String dsSubSegmento)
    {
        this._dsSubSegmento = dsSubSegmento;
    } //-- void setDsSubSegmento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipacao'.
     * 
     * @param dsTipoParticipacao the value of field
     * 'dsTipoParticipacao'.
     */
    public void setDsTipoParticipacao(java.lang.String dsTipoParticipacao)
    {
        this._dsTipoParticipacao = dsTipoParticipacao;
    } //-- void setDsTipoParticipacao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroAlteracao'.
     * 
     * @param hrManutencaoRegistroAlteracao the value of field
     * 'hrManutencaoRegistroAlteracao'.
     */
    public void setHrManutencaoRegistroAlteracao(java.lang.String hrManutencaoRegistroAlteracao)
    {
        this._hrManutencaoRegistroAlteracao = hrManutencaoRegistroAlteracao;
    } //-- void setHrManutencaoRegistroAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroInclusao'.
     * 
     * @param hrManutencaoRegistroInclusao the value of field
     * 'hrManutencaoRegistroInclusao'.
     */
    public void setHrManutencaoRegistroInclusao(java.lang.String hrManutencaoRegistroInclusao)
    {
        this._hrManutencaoRegistroInclusao = hrManutencaoRegistroInclusao;
    } //-- void setHrManutencaoRegistroInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoAlteracao'.
     * 
     * @param nmOperacaoFluxoAlteracao the value of field
     * 'nmOperacaoFluxoAlteracao'.
     */
    public void setNmOperacaoFluxoAlteracao(java.lang.String nmOperacaoFluxoAlteracao)
    {
        this._nmOperacaoFluxoAlteracao = nmOperacaoFluxoAlteracao;
    } //-- void setNmOperacaoFluxoAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmRazao'.
     * 
     * @param nmRazao the value of field 'nmRazao'.
     */
    public void setNmRazao(java.lang.String nmRazao)
    {
        this._nmRazao = nmRazao;
    } //-- void setNmRazao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesloop.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
