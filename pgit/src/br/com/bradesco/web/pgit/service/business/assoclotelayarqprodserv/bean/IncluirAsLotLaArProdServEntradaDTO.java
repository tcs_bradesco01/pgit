/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean;

/**
 * Nome: IncluirAsLotLaArProdServEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirAsLotLaArProdServEntradaDTO {
	
	/** Atributo tipoLote. */
	private int tipoLote;
	
	/** Atributo tipoLayoutArquivo. */
	private int tipoLayoutArquivo;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public int getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}
	
	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(int tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}
	
	/**
	 * Get: tipoLote.
	 *
	 * @return tipoLote
	 */
	public int getTipoLote() {
		return tipoLote;
	}
	
	/**
	 * Set: tipoLote.
	 *
	 * @param tipoLote the tipo lote
	 */
	public void setTipoLote(int tipoLote) {
		this.tipoLote = tipoLote;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	

}
