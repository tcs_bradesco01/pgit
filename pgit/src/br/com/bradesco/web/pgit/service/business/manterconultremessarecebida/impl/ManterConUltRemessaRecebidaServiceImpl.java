/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.IManterConUltRemessaRecebidaService;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.IManterConUltRemessaRecebidaServiceConstants;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.AlterarUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.AlterarUltimoNumeroRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.DetalharUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.DetalharUltimoNumeroRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.ListarUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.ListarUltimoNumeroRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarultimonumeroremessa.request.AlterarUltimoNumeroRemessaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarultimonumeroremessa.response.AlterarUltimoNumeroRemessaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.request.ConsultarUltimoNumeroRemessaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarultimonumeroremessa.response.ConsultarUltimoNumeroRemessaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimonumeroremessa.request.ListarUltimoNumeroRemessaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarultimonumeroremessa.response.ListarUltimoNumeroRemessaResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterConUltRemessaRecebida
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterConUltRemessaRecebidaServiceImpl implements
		IManterConUltRemessaRecebidaService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Construtor.
	 */
	public ManterConUltRemessaRecebidaServiceImpl() {

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.IManterConUltRemessaRecebidaService#listarUltimoNumeroRemessa(br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.ListarUltimoNumeroRemessaEntradaDTO)
	 */
	public List<ListarUltimoNumeroRemessaSaidaDTO> listarUltimoNumeroRemessa(
			ListarUltimoNumeroRemessaEntradaDTO entrada) {
		ListarUltimoNumeroRemessaRequest request = new ListarUltimoNumeroRemessaRequest();
		ListarUltimoNumeroRemessaResponse response = new ListarUltimoNumeroRemessaResponse();
		List<ListarUltimoNumeroRemessaSaidaDTO> listaRetorno = new ArrayList<ListarUltimoNumeroRemessaSaidaDTO>();

		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada
				.getCdPerfilTrocaArquivo()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoLoteLayout()));
		request
				.setNumeroOcorrencias(IManterConUltRemessaRecebidaServiceConstants.NUMERO_OCORRENCIAS_LISTA);

		response = getFactoryAdapter().getListarUltimoNumeroRemessaPDCAdapter()
				.invokeProcess(request);

		ListarUltimoNumeroRemessaSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ListarUltimoNumeroRemessaSaidaDTO();
			saidaDTO.setCdPerfilTrocaArquivo(response.getOcorrencias(i)
					.getCdPerfilTrocaArquivo());
			saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(i)
					.getCdTipoLayoutArquivo());
			saidaDTO.setCdTipoLoteLayout(response.getOcorrencias(i)
					.getCdTipoLoteLayout());
			saidaDTO.setDsTipoLayoutArquivo(PgitUtil
					.verificaStringNula(response.getOcorrencias(i)
							.getDsTipoLayoutArquivo()));
			saidaDTO.setDsTipoLoteLayout(PgitUtil.verificaStringNula(response
					.getOcorrencias(i).getDsTipoLoteLayout()));
			saidaDTO.setNrUltimoArquivo(response.getOcorrencias(i)
					.getNrUltimoArquivo());

			saidaDTO.setCdClub(PgitUtil.verificaLongNulo(response
					.getOcorrencias(i).getCdClub()));
			saidaDTO.setCdCorpoCpfCnpj(PgitUtil.verificaStringNula(response
					.getOcorrencias(i).getCdCorpoCpfCnpj()));

			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;
	}

	/**
	 * Get: factoryAdapter.
	 * 
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 * 
	 * @param factoryAdapter
	 *            the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.IManterConUltRemessaRecebidaService#alterarUltimoNumeroRemessa(br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.AlterarUltimoNumeroRemessaEntradaDTO)
	 */
	public AlterarUltimoNumeroRemessaSaidaDTO alterarUltimoNumeroRemessa(
			AlterarUltimoNumeroRemessaEntradaDTO entrada) {
		AlterarUltimoNumeroRemessaRequest request = new AlterarUltimoNumeroRemessaRequest();
		AlterarUltimoNumeroRemessaResponse response = new AlterarUltimoNumeroRemessaResponse();

		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada
				.getCdPerfilTrocaArquivo()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoLoteLayout()));
		request.setNrMaximoArquivo(PgitUtil.verificaLongNulo(entrada
				.getNrMaximoArquivo()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNrUltimoArquivo(PgitUtil.verificaLongNulo(entrada
				.getNrUltimoArquivo()));
		request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));

		response = getFactoryAdapter()
				.getAlterarUltimoNumeroRemessaPDCAdapter().invokeProcess(
						request);

		AlterarUltimoNumeroRemessaSaidaDTO saidaDTO = new AlterarUltimoNumeroRemessaSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.IManterConUltRemessaRecebidaService#detalharUltimoNumeroRemessa(br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.DetalharUltimoNumeroRemessaEntradaDTO)
	 */
	public DetalharUltimoNumeroRemessaSaidaDTO detalharUltimoNumeroRemessa(
			DetalharUltimoNumeroRemessaEntradaDTO entrada) {
		ConsultarUltimoNumeroRemessaRequest request = new ConsultarUltimoNumeroRemessaRequest();
		ConsultarUltimoNumeroRemessaResponse response = new ConsultarUltimoNumeroRemessaResponse();

		request.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(entrada
				.getCdPerfilTrocaArquivo()));
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoLayoutArquivo()));
		request.setCdTipoLoteLayout(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoLoteLayout()));
		request.setCdClub(PgitUtil.verificaLongNulo(entrada.getCdClub()));

		response = getFactoryAdapter()
				.getConsultarUltimoNumeroRemessaPDCAdapter().invokeProcess(
						request);
		DetalharUltimoNumeroRemessaSaidaDTO saidaDTO = new DetalharUltimoNumeroRemessaSaidaDTO();

		saidaDTO.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saidaDTO.setCdTipoLoteLayout(response.getCdTipoLoteLayout());
		saidaDTO.setDsNivelControle(PgitUtil.verificaStringNula(response
				.getDsNivelControle()));
		saidaDTO.setDsTipoControle(PgitUtil.verificaStringNula(response
				.getDsTipoControle()));
		saidaDTO.setDsTipoLayoutArquivo(PgitUtil.verificaStringNula(response
				.getDsTipoLayoutArquivo()));
		saidaDTO.setDsTipoLoteLayout(PgitUtil.verificaStringNula(response
				.getDsTipoLoteLayout()));
		saidaDTO
				.setNrMaximoArquivoRemessa(response.getNrMaximoArquivoRemessa());
		saidaDTO.setNrUltimoNumeroArquivoRemessa(response
				.getNrUltimoNumeroArquivoRemessa());
		saidaDTO.setCdCorpoCpfCnpj(response.getCdCorpoCpfCnpj());

		return saidaDTO;
	}

}
