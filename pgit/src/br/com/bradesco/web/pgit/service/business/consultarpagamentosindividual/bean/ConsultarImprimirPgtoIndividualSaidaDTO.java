/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/03/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

/**
 * Nome: ConsultarImprimirPgtoIndividualSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public class ConsultarImprimirPgtoIndividualSaidaDTO {

    /**
     * Atributo String
     */
    private String codMensagem = null;

    /**
     * Atributo String
     */
    private String mensagem = null;

    /**
     * Atributo Long
     */
    private Long qtdeRegistros = null;

    /**
     * Atributo String
     */
    private String indicadorImpressao = null;

    /**
     * Nome: getCodMensagem
     * 
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }

    /**
     * Nome: setCodMensagem
     * 
     * @param codMensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Nome: getMensagem
     * 
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Nome: setMensagem
     * 
     * @param mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Nome: getQtdeRegistros
     * 
     * @return qtdeRegistros
     */
    public Long getQtdeRegistros() {
        return qtdeRegistros;
    }

    /**
     * Nome: setQtdeRegistros
     * 
     * @param qtdeRegistros
     */
    public void setQtdeRegistros(Long qtdeRegistros) {
        this.qtdeRegistros = qtdeRegistros;
    }

    /**
     * Nome: getIndicadorImpressao
     * 
     * @return indicadorImpressao
     */
    public String getIndicadorImpressao() {
        return indicadorImpressao;
    }

    /**
     * Nome: setIndicadorImpressao
     * 
     * @param indicadorImpressao
     */
    public void setIndicadorImpressao(String indicadorImpressao) {
        this.indicadorImpressao = indicadorImpressao;
    }
}
