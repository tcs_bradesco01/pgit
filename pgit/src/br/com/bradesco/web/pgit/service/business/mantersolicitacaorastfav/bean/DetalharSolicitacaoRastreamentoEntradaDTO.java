/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean;

/**
 * Nome: DetalharSolicitacaoRastreamentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolicitacaoRastreamentoEntradaDTO {
	
	/** Atributo cdSolicitacaoPagamento. */
	private Integer cdSolicitacaoPagamento;
    
    /** Atributo nrSolicitacaoPagamento. */
    private Integer nrSolicitacaoPagamento;
	
    /**
     * Detalhar solicitacao rastreamento entrada dto.
     */
    public DetalharSolicitacaoRastreamentoEntradaDTO() {
		super();
	}
    
    /**
     * Detalhar solicitacao rastreamento entrada dto.
     *
     * @param cdSolicitacaoPagamento the cd solicitacao pagamento
     * @param nrSolicitacaoPagamento the nr solicitacao pagamento
     */
    public DetalharSolicitacaoRastreamentoEntradaDTO(Integer cdSolicitacaoPagamento, Integer nrSolicitacaoPagamento) {
    	
    	this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
        this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
		
	}
    
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}
	
	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}
	
	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}

}
