/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/


import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;


/**
 * Class OcorrenciasEntrada.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasEntrada implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFinalidadeContaPagamento
     */
    private int _cdFinalidadeContaPagamento = 0;

    /**
     * keeps track of state for field: _cdFinalidadeContaPagamento
     */
    private boolean _has_cdFinalidadeContaPagamento;

    /**
     * Field _cdTipoAcaoFinalidade
     */
    private java.lang.String _cdTipoAcaoFinalidade;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasEntrada() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.OcorrenciasEntrada()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFinalidadeContaPagamento
     * 
     */
    public void deleteCdFinalidadeContaPagamento()
    {
        this._has_cdFinalidadeContaPagamento= false;
    } //-- void deleteCdFinalidadeContaPagamento() 

    /**
     * Returns the value of field 'cdFinalidadeContaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFinalidadeContaPagamento'.
     */
    public int getCdFinalidadeContaPagamento()
    {
        return this._cdFinalidadeContaPagamento;
    } //-- int getCdFinalidadeContaPagamento() 

    /**
     * Returns the value of field 'cdTipoAcaoFinalidade'.
     * 
     * @return String
     * @return the value of field 'cdTipoAcaoFinalidade'.
     */
    public java.lang.String getCdTipoAcaoFinalidade()
    {
        return this._cdTipoAcaoFinalidade;
    } //-- java.lang.String getCdTipoAcaoFinalidade() 

    /**
     * Method hasCdFinalidadeContaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidadeContaPagamento()
    {
        return this._has_cdFinalidadeContaPagamento;
    } //-- boolean hasCdFinalidadeContaPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFinalidadeContaPagamento'.
     * 
     * @param cdFinalidadeContaPagamento the value of field
     * 'cdFinalidadeContaPagamento'.
     */
    public void setCdFinalidadeContaPagamento(int cdFinalidadeContaPagamento)
    {
        this._cdFinalidadeContaPagamento = cdFinalidadeContaPagamento;
        this._has_cdFinalidadeContaPagamento = true;
    } //-- void setCdFinalidadeContaPagamento(int) 

    /**
     * Sets the value of field 'cdTipoAcaoFinalidade'.
     * 
     * @param cdTipoAcaoFinalidade the value of field
     * 'cdTipoAcaoFinalidade'.
     */
    public void setCdTipoAcaoFinalidade(java.lang.String cdTipoAcaoFinalidade)
    {
        this._cdTipoAcaoFinalidade = cdTipoAcaoFinalidade;
    } //-- void setCdTipoAcaoFinalidade(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasEntrada
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.OcorrenciasEntrada unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.OcorrenciasEntrada) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.OcorrenciasEntrada.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.OcorrenciasEntrada unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
