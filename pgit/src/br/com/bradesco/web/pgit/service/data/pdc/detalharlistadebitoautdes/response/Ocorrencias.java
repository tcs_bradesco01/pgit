/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaLista
     */
    private long _cdPessoaJuridicaLista = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaLista
     */
    private boolean _has_cdPessoaJuridicaLista;

    /**
     * Field _cdTipoContratoLista
     */
    private int _cdTipoContratoLista = 0;

    /**
     * keeps track of state for field: _cdTipoContratoLista
     */
    private boolean _has_cdTipoContratoLista;

    /**
     * Field _nrSequenciaContratoLista
     */
    private long _nrSequenciaContratoLista = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoLista
     */
    private boolean _has_nrSequenciaContratoLista;

    /**
     * Field _cdTipoCanalLista
     */
    private int _cdTipoCanalLista = 0;

    /**
     * keeps track of state for field: _cdTipoCanalLista
     */
    private boolean _has_cdTipoCanalLista;

    /**
     * Field _cdListaDebitoPagamento
     */
    private long _cdListaDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdListaDebitoPagamento
     */
    private boolean _has_cdListaDebitoPagamento;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegoci
     */
    private long _nrSequenciaContratoNegoci = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegoci
     */
    private boolean _has_nrSequenciaContratoNegoci;

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _vlPagamento
     */
    private java.math.BigDecimal _vlPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _dsPessoaComplemento
     */
    private java.lang.String _dsPessoaComplemento;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _dsSituacaoPagamento
     */
    private java.lang.String _dsSituacaoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdListaDebitoPagamento
     * 
     */
    public void deleteCdListaDebitoPagamento()
    {
        this._has_cdListaDebitoPagamento= false;
    } //-- void deleteCdListaDebitoPagamento() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaLista
     * 
     */
    public void deleteCdPessoaJuridicaLista()
    {
        this._has_cdPessoaJuridicaLista= false;
    } //-- void deleteCdPessoaJuridicaLista() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Method deleteCdTipoCanalLista
     * 
     */
    public void deleteCdTipoCanalLista()
    {
        this._has_cdTipoCanalLista= false;
    } //-- void deleteCdTipoCanalLista() 

    /**
     * Method deleteCdTipoContratoLista
     * 
     */
    public void deleteCdTipoContratoLista()
    {
        this._has_cdTipoContratoLista= false;
    } //-- void deleteCdTipoContratoLista() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoLista
     * 
     */
    public void deleteNrSequenciaContratoLista()
    {
        this._has_nrSequenciaContratoLista= false;
    } //-- void deleteNrSequenciaContratoLista() 

    /**
     * Method deleteNrSequenciaContratoNegoci
     * 
     */
    public void deleteNrSequenciaContratoNegoci()
    {
        this._has_nrSequenciaContratoNegoci= false;
    } //-- void deleteNrSequenciaContratoNegoci() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdListaDebitoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdListaDebitoPagamento'.
     */
    public long getCdListaDebitoPagamento()
    {
        return this._cdListaDebitoPagamento;
    } //-- long getCdListaDebitoPagamento() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaLista'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaLista'.
     */
    public long getCdPessoaJuridicaLista()
    {
        return this._cdPessoaJuridicaLista;
    } //-- long getCdPessoaJuridicaLista() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'cdTipoCanalLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalLista'.
     */
    public int getCdTipoCanalLista()
    {
        return this._cdTipoCanalLista;
    } //-- int getCdTipoCanalLista() 

    /**
     * Returns the value of field 'cdTipoContratoLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoLista'.
     */
    public int getCdTipoContratoLista()
    {
        return this._cdTipoContratoLista;
    } //-- int getCdTipoContratoLista() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsPessoaComplemento'.
     * 
     * @return String
     * @return the value of field 'dsPessoaComplemento'.
     */
    public java.lang.String getDsPessoaComplemento()
    {
        return this._dsPessoaComplemento;
    } //-- java.lang.String getDsPessoaComplemento() 

    /**
     * Returns the value of field 'dsSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPagamento'.
     */
    public java.lang.String getDsSituacaoPagamento()
    {
        return this._dsSituacaoPagamento;
    } //-- java.lang.String getDsSituacaoPagamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoLista'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoLista'.
     */
    public long getNrSequenciaContratoLista()
    {
        return this._nrSequenciaContratoLista;
    } //-- long getNrSequenciaContratoLista() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegoci'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegoci'.
     */
    public long getNrSequenciaContratoNegoci()
    {
        return this._nrSequenciaContratoNegoci;
    } //-- long getNrSequenciaContratoNegoci() 

    /**
     * Returns the value of field 'vlPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamento'.
     */
    public java.math.BigDecimal getVlPagamento()
    {
        return this._vlPagamento;
    } //-- java.math.BigDecimal getVlPagamento() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdListaDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdListaDebitoPagamento()
    {
        return this._has_cdListaDebitoPagamento;
    } //-- boolean hasCdListaDebitoPagamento() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaLista()
    {
        return this._has_cdPessoaJuridicaLista;
    } //-- boolean hasCdPessoaJuridicaLista() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method hasCdTipoCanalLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalLista()
    {
        return this._has_cdTipoCanalLista;
    } //-- boolean hasCdTipoCanalLista() 

    /**
     * Method hasCdTipoContratoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoLista()
    {
        return this._has_cdTipoContratoLista;
    } //-- boolean hasCdTipoContratoLista() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoLista()
    {
        return this._has_nrSequenciaContratoLista;
    } //-- boolean hasNrSequenciaContratoLista() 

    /**
     * Method hasNrSequenciaContratoNegoci
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegoci()
    {
        return this._has_nrSequenciaContratoNegoci;
    } //-- boolean hasNrSequenciaContratoNegoci() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdListaDebitoPagamento'.
     * 
     * @param cdListaDebitoPagamento the value of field
     * 'cdListaDebitoPagamento'.
     */
    public void setCdListaDebitoPagamento(long cdListaDebitoPagamento)
    {
        this._cdListaDebitoPagamento = cdListaDebitoPagamento;
        this._has_cdListaDebitoPagamento = true;
    } //-- void setCdListaDebitoPagamento(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaLista'.
     * 
     * @param cdPessoaJuridicaLista the value of field
     * 'cdPessoaJuridicaLista'.
     */
    public void setCdPessoaJuridicaLista(long cdPessoaJuridicaLista)
    {
        this._cdPessoaJuridicaLista = cdPessoaJuridicaLista;
        this._has_cdPessoaJuridicaLista = true;
    } //-- void setCdPessoaJuridicaLista(long) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'cdTipoCanalLista'.
     * 
     * @param cdTipoCanalLista the value of field 'cdTipoCanalLista'
     */
    public void setCdTipoCanalLista(int cdTipoCanalLista)
    {
        this._cdTipoCanalLista = cdTipoCanalLista;
        this._has_cdTipoCanalLista = true;
    } //-- void setCdTipoCanalLista(int) 

    /**
     * Sets the value of field 'cdTipoContratoLista'.
     * 
     * @param cdTipoContratoLista the value of field
     * 'cdTipoContratoLista'.
     */
    public void setCdTipoContratoLista(int cdTipoContratoLista)
    {
        this._cdTipoContratoLista = cdTipoContratoLista;
        this._has_cdTipoContratoLista = true;
    } //-- void setCdTipoContratoLista(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaComplemento'.
     * 
     * @param dsPessoaComplemento the value of field
     * 'dsPessoaComplemento'.
     */
    public void setDsPessoaComplemento(java.lang.String dsPessoaComplemento)
    {
        this._dsPessoaComplemento = dsPessoaComplemento;
    } //-- void setDsPessoaComplemento(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPagamento'.
     * 
     * @param dsSituacaoPagamento the value of field
     * 'dsSituacaoPagamento'.
     */
    public void setDsSituacaoPagamento(java.lang.String dsSituacaoPagamento)
    {
        this._dsSituacaoPagamento = dsSituacaoPagamento;
    } //-- void setDsSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoLista'.
     * 
     * @param nrSequenciaContratoLista the value of field
     * 'nrSequenciaContratoLista'.
     */
    public void setNrSequenciaContratoLista(long nrSequenciaContratoLista)
    {
        this._nrSequenciaContratoLista = nrSequenciaContratoLista;
        this._has_nrSequenciaContratoLista = true;
    } //-- void setNrSequenciaContratoLista(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegoci'.
     * 
     * @param nrSequenciaContratoNegoci the value of field
     * 'nrSequenciaContratoNegoci'.
     */
    public void setNrSequenciaContratoNegoci(long nrSequenciaContratoNegoci)
    {
        this._nrSequenciaContratoNegoci = nrSequenciaContratoNegoci;
        this._has_nrSequenciaContratoNegoci = true;
    } //-- void setNrSequenciaContratoNegoci(long) 

    /**
     * Sets the value of field 'vlPagamento'.
     * 
     * @param vlPagamento the value of field 'vlPagamento'.
     */
    public void setVlPagamento(java.math.BigDecimal vlPagamento)
    {
        this._vlPagamento = vlPagamento;
    } //-- void setVlPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebitoautdes.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
