/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.DetalharSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.DetalharSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ExcluirSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ExcluirSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.IncluirSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.IncluirSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ListarSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ListarSolBaseSistemaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterSolicGeracaoArqRetBaseSistema
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterSolicGeracaoArqRetBaseSistemaService {

    /**
     * Listar sol base sistema.
     *
     * @param entrada the entrada
     * @return the list< listar sol base sistema saida dt o>
     */
    List<ListarSolBaseSistemaSaidaDTO> listarSolBaseSistema (ListarSolBaseSistemaEntradaDTO entrada);
    
    /**
     * Detalhar sol base sistema.
     *
     * @param entrada the entrada
     * @return the detalhar sol base sistema saida dto
     */
    DetalharSolBaseSistemaSaidaDTO detalharSolBaseSistema (DetalharSolBaseSistemaEntradaDTO entrada);
    
    /**
     * Excluir sol base sistema.
     *
     * @param entrada the entrada
     * @return the excluir sol base sistema saida dto
     */
    ExcluirSolBaseSistemaSaidaDTO excluirSolBaseSistema (ExcluirSolBaseSistemaEntradaDTO entrada);
    
    /**
     * Incluir sol base sistema.
     *
     * @param entrada the entrada
     * @return the incluir sol base sistema saida dto
     */
    IncluirSolBaseSistemaSaidaDTO incluirSolBaseSistema (IncluirSolBaseSistemaEntradaDTO entrada);
    
}

