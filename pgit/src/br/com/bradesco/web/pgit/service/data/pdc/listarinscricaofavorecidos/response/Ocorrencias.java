/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoInscricaoFavorecidos
     */
    private int _cdTipoInscricaoFavorecidos = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoFavorecidos
     */
    private boolean _has_cdTipoInscricaoFavorecidos;

    /**
     * Field _dsTipoInscricaoFavorecidos
     */
    private java.lang.String _dsTipoInscricaoFavorecidos;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoInscricaoFavorecidos
     * 
     */
    public void deleteCdTipoInscricaoFavorecidos()
    {
        this._has_cdTipoInscricaoFavorecidos= false;
    } //-- void deleteCdTipoInscricaoFavorecidos() 

    /**
     * Returns the value of field 'cdTipoInscricaoFavorecidos'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoFavorecidos'.
     */
    public int getCdTipoInscricaoFavorecidos()
    {
        return this._cdTipoInscricaoFavorecidos;
    } //-- int getCdTipoInscricaoFavorecidos() 

    /**
     * Returns the value of field 'dsTipoInscricaoFavorecidos'.
     * 
     * @return String
     * @return the value of field 'dsTipoInscricaoFavorecidos'.
     */
    public java.lang.String getDsTipoInscricaoFavorecidos()
    {
        return this._dsTipoInscricaoFavorecidos;
    } //-- java.lang.String getDsTipoInscricaoFavorecidos() 

    /**
     * Method hasCdTipoInscricaoFavorecidos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoFavorecidos()
    {
        return this._has_cdTipoInscricaoFavorecidos;
    } //-- boolean hasCdTipoInscricaoFavorecidos() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecidos'.
     * 
     * @param cdTipoInscricaoFavorecidos the value of field
     * 'cdTipoInscricaoFavorecidos'.
     */
    public void setCdTipoInscricaoFavorecidos(int cdTipoInscricaoFavorecidos)
    {
        this._cdTipoInscricaoFavorecidos = cdTipoInscricaoFavorecidos;
        this._has_cdTipoInscricaoFavorecidos = true;
    } //-- void setCdTipoInscricaoFavorecidos(int) 

    /**
     * Sets the value of field 'dsTipoInscricaoFavorecidos'.
     * 
     * @param dsTipoInscricaoFavorecidos the value of field
     * 'dsTipoInscricaoFavorecidos'.
     */
    public void setDsTipoInscricaoFavorecidos(java.lang.String dsTipoInscricaoFavorecidos)
    {
        this._dsTipoInscricaoFavorecidos = dsTipoInscricaoFavorecidos;
    } //-- void setDsTipoInscricaoFavorecidos(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarinscricaofavorecidos.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
