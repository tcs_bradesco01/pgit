/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

/**
 * Nome: VinculoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class VinculoDTO {

	/** Atributo nrCpfCnpjVinc. */
	private String nrCpfCnpjVinc;
	
	/** Atributo nmRazaoSocialVinc. */
	private String nmRazaoSocialVinc;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo dsBanco. */
	private String dsBanco;
	
	/** Atributo cdAgenciaBancaria. */
	private Integer cdAgenciaBancaria;
	
	/** Atributo dsAgenciaBancaria. */
	private String dsAgenciaBancaria;
	
	/** Atributo cdContaBancaria. */
	private Long cdContaBancaria;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdTipoConta. */
	private String cdTipoConta;
	
	/** Atributo dsAcaoManutencaoConta. */
	private String dsAcaoManutencaoConta;
	
	/**
	 * Get: nrCpfCnpjVinc.
	 *
	 * @return nrCpfCnpjVinc
	 */
	public String getNrCpfCnpjVinc() {
		return nrCpfCnpjVinc;
	}
	
	/**
	 * Set: nrCpfCnpjVinc.
	 *
	 * @param nrCpfCnpjVinc the nr cpf cnpj vinc
	 */
	public void setNrCpfCnpjVinc(String nrCpfCnpjVinc) {
		this.nrCpfCnpjVinc = nrCpfCnpjVinc;
	}
	
	/**
	 * Get: nmRazaoSocialVinc.
	 *
	 * @return nmRazaoSocialVinc
	 */
	public String getNmRazaoSocialVinc() {
		return nmRazaoSocialVinc;
	}
	
	/**
	 * Set: nmRazaoSocialVinc.
	 *
	 * @param nmRazaoSocialVinc the nm razao social vinc
	 */
	public void setNmRazaoSocialVinc(String nmRazaoSocialVinc) {
		this.nmRazaoSocialVinc = nmRazaoSocialVinc;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: cdAgenciaBancaria.
	 *
	 * @return cdAgenciaBancaria
	 */
	public Integer getCdAgenciaBancaria() {
		return cdAgenciaBancaria;
	}
	
	/**
	 * Set: cdAgenciaBancaria.
	 *
	 * @param cdAgenciaBancaria the cd agencia bancaria
	 */
	public void setCdAgenciaBancaria(Integer cdAgenciaBancaria) {
		this.cdAgenciaBancaria = cdAgenciaBancaria;
	}
	
	/**
	 * Get: dsAgenciaBancaria.
	 *
	 * @return dsAgenciaBancaria
	 */
	public String getDsAgenciaBancaria() {
		return dsAgenciaBancaria;
	}
	
	/**
	 * Set: dsAgenciaBancaria.
	 *
	 * @param dsAgenciaBancaria the ds agencia bancaria
	 */
	public void setDsAgenciaBancaria(String dsAgenciaBancaria) {
		this.dsAgenciaBancaria = dsAgenciaBancaria;
	}
	
	/**
	 * Get: cdContaBancaria.
	 *
	 * @return cdContaBancaria
	 */
	public Long getCdContaBancaria() {
		return cdContaBancaria;
	}
	
	/**
	 * Set: cdContaBancaria.
	 *
	 * @param cdContaBancaria the cd conta bancaria
	 */
	public void setCdContaBancaria(Long cdContaBancaria) {
		this.cdContaBancaria = cdContaBancaria;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public String getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(String cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: dsAcaoManutencaoConta.
	 *
	 * @return dsAcaoManutencaoConta
	 */
	public String getDsAcaoManutencaoConta() {
		return dsAcaoManutencaoConta;
	}
	
	/**
	 * Set: dsAcaoManutencaoConta.
	 *
	 * @param dsAcaoManutencaoConta the ds acao manutencao conta
	 */
	public void setDsAcaoManutencaoConta(String dsAcaoManutencaoConta) {
		this.dsAcaoManutencaoConta = dsAcaoManutencaoConta;
	}

	
}
