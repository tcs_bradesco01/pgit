/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

/**
 * Nome: ConsultarBancoAgenciaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarBancoAgenciaSaidaDTO {
	
    /** Atributo dsBanco. */
    private String dsBanco;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo cdEndereco. */
    private Long cdEndereco;
    
    /** Atributo logradouro. */
    private String logradouro;
    
    /** Atributo numero. */
    private Integer numero;
    
    /** Atributo bairro. */
    private String bairro;
    
    /** Atributo complemento. */
    private String complemento;
    
    /** Atributo cidade. */
    private String cidade;
    
    /** Atributo estado. */
    private String estado;
    
    /** Atributo pais. */
    private String pais;
    
    /** Atributo contato. */
    private String contato;
    
    /** Atributo email. */
    private String email;
    
    /** Atributo cep. */
    private Integer cep;
    
    /** Atributo complementoCep. */
    private Integer complementoCep;
	
    /**
     * Consultar banco agencia saida dto.
     */
    public ConsultarBancoAgenciaSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Consultar banco agencia saida dto.
	 *
	 * @param dsBanco the ds banco
	 * @param dsAgencia the ds agencia
	 */
	public ConsultarBancoAgenciaSaidaDTO(String dsBanco, String dsAgencia) {
		this.dsBanco = dsBanco;
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}

	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}

	/**
	 * Get: bairro.
	 *
	 * @return bairro
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * Set: bairro.
	 *
	 * @param bairro the bairro
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * Get: cdEndereco.
	 *
	 * @return cdEndereco
	 */
	public Long getCdEndereco() {
		return cdEndereco;
	}

	/**
	 * Set: cdEndereco.
	 *
	 * @param cdEndereco the cd endereco
	 */
	public void setCdEndereco(Long cdEndereco) {
		this.cdEndereco = cdEndereco;
	}

	/**
	 * Get: cep.
	 *
	 * @return cep
	 */
	public Integer getCep() {
		return cep;
	}

	/**
	 * Set: cep.
	 *
	 * @param cep the cep
	 */
	public void setCep(Integer cep) {
		this.cep = cep;
	}

	/**
	 * Get: cidade.
	 *
	 * @return cidade
	 */
	public String getCidade() {
		return cidade;
	}

	/**
	 * Set: cidade.
	 *
	 * @param cidade the cidade
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: complemento.
	 *
	 * @return complemento
	 */
	public String getComplemento() {
		return complemento;
	}

	/**
	 * Set: complemento.
	 *
	 * @param complemento the complemento
	 */
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	/**
	 * Get: complementoCep.
	 *
	 * @return complementoCep
	 */
	public Integer getComplementoCep() {
		return complementoCep;
	}

	/**
	 * Set: complementoCep.
	 *
	 * @param complementoCep the complemento cep
	 */
	public void setComplementoCep(Integer complementoCep) {
		this.complementoCep = complementoCep;
	}

	/**
	 * Get: contato.
	 *
	 * @return contato
	 */
	public String getContato() {
		return contato;
	}

	/**
	 * Set: contato.
	 *
	 * @param contato the contato
	 */
	public void setContato(String contato) {
		this.contato = contato;
	}

	/**
	 * Get: email.
	 *
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Set: email.
	 *
	 * @param email the email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Get: estado.
	 *
	 * @return estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * Set: estado.
	 *
	 * @param estado the estado
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * Get: logradouro.
	 *
	 * @return logradouro
	 */
	public String getLogradouro() {
		return logradouro;
	}

	/**
	 * Set: logradouro.
	 *
	 * @param logradouro the logradouro
	 */
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public Integer getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	/**
	 * Get: pais.
	 *
	 * @return pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * Set: pais.
	 *
	 * @param pais the pais
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
}
