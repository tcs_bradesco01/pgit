/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharVinculacaoMsgLayoutSistemaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharVinculacaoMsgLayoutSistemaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _nrMensagemArquivoRetorno
     */
    private int _nrMensagemArquivoRetorno = 0;

    /**
     * keeps track of state for field: _nrMensagemArquivoRetorno
     */
    private boolean _has_nrMensagemArquivoRetorno;

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _dsCodigoSistema
     */
    private java.lang.String _dsCodigoSistema;

    /**
     * Field _nrEventoMensagemNegocio
     */
    private int _nrEventoMensagemNegocio = 0;

    /**
     * keeps track of state for field: _nrEventoMensagemNegocio
     */
    private boolean _has_nrEventoMensagemNegocio;

    /**
     * Field _dsEventoMensagemNegocio
     */
    private java.lang.String _dsEventoMensagemNegocio;

    /**
     * Field _cdRecursoGeradorMensagem
     */
    private int _cdRecursoGeradorMensagem = 0;

    /**
     * keeps track of state for field: _cdRecursoGeradorMensagem
     */
    private boolean _has_cdRecursoGeradorMensagem;

    /**
     * Field _dsRecGedorMensagem
     */
    private java.lang.String _dsRecGedorMensagem;

    /**
     * Field _cdIdiomaTextoMensagem
     */
    private int _cdIdiomaTextoMensagem = 0;

    /**
     * keeps track of state for field: _cdIdiomaTextoMensagem
     */
    private boolean _has_cdIdiomaTextoMensagem;

    /**
     * Field _dsIdiomaTextoMensagem
     */
    private java.lang.String _dsIdiomaTextoMensagem;

    /**
     * Field _cdMensagemArquivoRetorno
     */
    private java.lang.String _cdMensagemArquivoRetorno;

    /**
     * Field _dsMensagemArquivoRetorno
     */
    private java.lang.String _dsMensagemArquivoRetorno;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharVinculacaoMsgLayoutSistemaResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.response.DetalharVinculacaoMsgLayoutSistemaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdIdiomaTextoMensagem
     * 
     */
    public void deleteCdIdiomaTextoMensagem()
    {
        this._has_cdIdiomaTextoMensagem= false;
    } //-- void deleteCdIdiomaTextoMensagem() 

    /**
     * Method deleteCdRecursoGeradorMensagem
     * 
     */
    public void deleteCdRecursoGeradorMensagem()
    {
        this._has_cdRecursoGeradorMensagem= false;
    } //-- void deleteCdRecursoGeradorMensagem() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrEventoMensagemNegocio
     * 
     */
    public void deleteNrEventoMensagemNegocio()
    {
        this._has_nrEventoMensagemNegocio= false;
    } //-- void deleteNrEventoMensagemNegocio() 

    /**
     * Method deleteNrMensagemArquivoRetorno
     * 
     */
    public void deleteNrMensagemArquivoRetorno()
    {
        this._has_nrMensagemArquivoRetorno= false;
    } //-- void deleteNrMensagemArquivoRetorno() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaTextoMensagem'.
     */
    public int getCdIdiomaTextoMensagem()
    {
        return this._cdIdiomaTextoMensagem;
    } //-- int getCdIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'cdMensagemArquivoRetorno'.
     */
    public java.lang.String getCdMensagemArquivoRetorno()
    {
        return this._cdMensagemArquivoRetorno;
    } //-- java.lang.String getCdMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'cdRecursoGeradorMensagem'.
     * 
     * @return int
     * @return the value of field 'cdRecursoGeradorMensagem'.
     */
    public int getCdRecursoGeradorMensagem()
    {
        return this._cdRecursoGeradorMensagem;
    } //-- int getCdRecursoGeradorMensagem() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCodigoSistema'.
     * 
     * @return String
     * @return the value of field 'dsCodigoSistema'.
     */
    public java.lang.String getDsCodigoSistema()
    {
        return this._dsCodigoSistema;
    } //-- java.lang.String getDsCodigoSistema() 

    /**
     * Returns the value of field 'dsEventoMensagemNegocio'.
     * 
     * @return String
     * @return the value of field 'dsEventoMensagemNegocio'.
     */
    public java.lang.String getDsEventoMensagemNegocio()
    {
        return this._dsEventoMensagemNegocio;
    } //-- java.lang.String getDsEventoMensagemNegocio() 

    /**
     * Returns the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @return String
     * @return the value of field 'dsIdiomaTextoMensagem'.
     */
    public java.lang.String getDsIdiomaTextoMensagem()
    {
        return this._dsIdiomaTextoMensagem;
    } //-- java.lang.String getDsIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'dsMensagemArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMensagemArquivoRetorno'.
     */
    public java.lang.String getDsMensagemArquivoRetorno()
    {
        return this._dsMensagemArquivoRetorno;
    } //-- java.lang.String getDsMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'dsRecGedorMensagem'.
     * 
     * @return String
     * @return the value of field 'dsRecGedorMensagem'.
     */
    public java.lang.String getDsRecGedorMensagem()
    {
        return this._dsRecGedorMensagem;
    } //-- java.lang.String getDsRecGedorMensagem() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrEventoMensagemNegocio'.
     * 
     * @return int
     * @return the value of field 'nrEventoMensagemNegocio'.
     */
    public int getNrEventoMensagemNegocio()
    {
        return this._nrEventoMensagemNegocio;
    } //-- int getNrEventoMensagemNegocio() 

    /**
     * Returns the value of field 'nrMensagemArquivoRetorno'.
     * 
     * @return int
     * @return the value of field 'nrMensagemArquivoRetorno'.
     */
    public int getNrMensagemArquivoRetorno()
    {
        return this._nrMensagemArquivoRetorno;
    } //-- int getNrMensagemArquivoRetorno() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdIdiomaTextoMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaTextoMensagem()
    {
        return this._has_cdIdiomaTextoMensagem;
    } //-- boolean hasCdIdiomaTextoMensagem() 

    /**
     * Method hasCdRecursoGeradorMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRecursoGeradorMensagem()
    {
        return this._has_cdRecursoGeradorMensagem;
    } //-- boolean hasCdRecursoGeradorMensagem() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrEventoMensagemNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoMensagemNegocio()
    {
        return this._has_nrEventoMensagemNegocio;
    } //-- boolean hasNrEventoMensagemNegocio() 

    /**
     * Method hasNrMensagemArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMensagemArquivoRetorno()
    {
        return this._has_nrMensagemArquivoRetorno;
    } //-- boolean hasNrMensagemArquivoRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @param cdIdiomaTextoMensagem the value of field
     * 'cdIdiomaTextoMensagem'.
     */
    public void setCdIdiomaTextoMensagem(int cdIdiomaTextoMensagem)
    {
        this._cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
        this._has_cdIdiomaTextoMensagem = true;
    } //-- void setCdIdiomaTextoMensagem(int) 

    /**
     * Sets the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @param cdMensagemArquivoRetorno the value of field
     * 'cdMensagemArquivoRetorno'.
     */
    public void setCdMensagemArquivoRetorno(java.lang.String cdMensagemArquivoRetorno)
    {
        this._cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
    } //-- void setCdMensagemArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdRecursoGeradorMensagem'.
     * 
     * @param cdRecursoGeradorMensagem the value of field
     * 'cdRecursoGeradorMensagem'.
     */
    public void setCdRecursoGeradorMensagem(int cdRecursoGeradorMensagem)
    {
        this._cdRecursoGeradorMensagem = cdRecursoGeradorMensagem;
        this._has_cdRecursoGeradorMensagem = true;
    } //-- void setCdRecursoGeradorMensagem(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoSistema'.
     * 
     * @param dsCodigoSistema the value of field 'dsCodigoSistema'.
     */
    public void setDsCodigoSistema(java.lang.String dsCodigoSistema)
    {
        this._dsCodigoSistema = dsCodigoSistema;
    } //-- void setDsCodigoSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsEventoMensagemNegocio'.
     * 
     * @param dsEventoMensagemNegocio the value of field
     * 'dsEventoMensagemNegocio'.
     */
    public void setDsEventoMensagemNegocio(java.lang.String dsEventoMensagemNegocio)
    {
        this._dsEventoMensagemNegocio = dsEventoMensagemNegocio;
    } //-- void setDsEventoMensagemNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @param dsIdiomaTextoMensagem the value of field
     * 'dsIdiomaTextoMensagem'.
     */
    public void setDsIdiomaTextoMensagem(java.lang.String dsIdiomaTextoMensagem)
    {
        this._dsIdiomaTextoMensagem = dsIdiomaTextoMensagem;
    } //-- void setDsIdiomaTextoMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemArquivoRetorno'.
     * 
     * @param dsMensagemArquivoRetorno the value of field
     * 'dsMensagemArquivoRetorno'.
     */
    public void setDsMensagemArquivoRetorno(java.lang.String dsMensagemArquivoRetorno)
    {
        this._dsMensagemArquivoRetorno = dsMensagemArquivoRetorno;
    } //-- void setDsMensagemArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsRecGedorMensagem'.
     * 
     * @param dsRecGedorMensagem the value of field
     * 'dsRecGedorMensagem'.
     */
    public void setDsRecGedorMensagem(java.lang.String dsRecGedorMensagem)
    {
        this._dsRecGedorMensagem = dsRecGedorMensagem;
    } //-- void setDsRecGedorMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrEventoMensagemNegocio'.
     * 
     * @param nrEventoMensagemNegocio the value of field
     * 'nrEventoMensagemNegocio'.
     */
    public void setNrEventoMensagemNegocio(int nrEventoMensagemNegocio)
    {
        this._nrEventoMensagemNegocio = nrEventoMensagemNegocio;
        this._has_nrEventoMensagemNegocio = true;
    } //-- void setNrEventoMensagemNegocio(int) 

    /**
     * Sets the value of field 'nrMensagemArquivoRetorno'.
     * 
     * @param nrMensagemArquivoRetorno the value of field
     * 'nrMensagemArquivoRetorno'.
     */
    public void setNrMensagemArquivoRetorno(int nrMensagemArquivoRetorno)
    {
        this._nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
        this._has_nrMensagemArquivoRetorno = true;
    } //-- void setNrMensagemArquivoRetorno(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharVinculacaoMsgLayoutSistemaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.response.DetalharVinculacaoMsgLayoutSistemaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.response.DetalharVinculacaoMsgLayoutSistemaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.response.DetalharVinculacaoMsgLayoutSistemaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharvinculacaomsglayoutsistema.response.DetalharVinculacaoMsgLayoutSistemaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
