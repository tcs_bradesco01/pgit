/*
 * Nome: br.com.bradesco.web.pgit.service.business.tipocomprovante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.tipocomprovante.bean;

/**
 * Nome: ListarTipoComprovanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoComprovanteEntradaDTO {
	
	/** Atributo cdTipoComprovante. */
	private int cdTipoComprovante;

	/**
	 * Get: cdTipoComprovante.
	 *
	 * @return cdTipoComprovante
	 */
	public int getCdTipoComprovante() {
		return cdTipoComprovante;
	}

	/**
	 * Set: cdTipoComprovante.
	 *
	 * @param cdTipoComprovante the cd tipo comprovante
	 */
	public void setCdTipoComprovante(int cdTipoComprovante) {
		this.cdTipoComprovante = cdTipoComprovante;
	}
	
	

}
