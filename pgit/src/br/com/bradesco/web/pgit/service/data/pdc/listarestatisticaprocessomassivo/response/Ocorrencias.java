/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdProcessoSistema
     */
    private int _cdProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdProcessoSistema
     */
    private boolean _has_cdProcessoSistema;

    /**
     * Field _dsTipoProcessoSistema
     */
    private java.lang.String _dsTipoProcessoSistema;

    /**
     * Field _hrInicioEstatisticaProcessoInicio
     */
    private java.lang.String _hrInicioEstatisticaProcessoInicio;

    /**
     * Field _hrInicioEstatisticaProcessoFim
     */
    private java.lang.String _hrInicioEstatisticaProcessoFim;

    /**
     * Field _cdSituacaoEstatisticaSistema
     */
    private int _cdSituacaoEstatisticaSistema = 0;

    /**
     * keeps track of state for field: _cdSituacaoEstatisticaSistema
     */
    private boolean _has_cdSituacaoEstatisticaSistema;

    /**
     * Field _dsSituacaoEstatisticaSistema
     */
    private java.lang.String _dsSituacaoEstatisticaSistema;

    /**
     * Field _qtRegistrosProcessoSistema
     */
    private int _qtRegistrosProcessoSistema = 0;

    /**
     * keeps track of state for field: _qtRegistrosProcessoSistema
     */
    private boolean _has_qtRegistrosProcessoSistema;

    /**
     * Field _vlRegistroProcessoSistema
     */
    private java.math.BigDecimal _vlRegistroProcessoSistema = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlRegistroProcessoSistema(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProcessoSistema
     * 
     */
    public void deleteCdProcessoSistema()
    {
        this._has_cdProcessoSistema= false;
    } //-- void deleteCdProcessoSistema() 

    /**
     * Method deleteCdSituacaoEstatisticaSistema
     * 
     */
    public void deleteCdSituacaoEstatisticaSistema()
    {
        this._has_cdSituacaoEstatisticaSistema= false;
    } //-- void deleteCdSituacaoEstatisticaSistema() 

    /**
     * Method deleteQtRegistrosProcessoSistema
     * 
     */
    public void deleteQtRegistrosProcessoSistema()
    {
        this._has_qtRegistrosProcessoSistema= false;
    } //-- void deleteQtRegistrosProcessoSistema() 

    /**
     * Returns the value of field 'cdProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdProcessoSistema'.
     */
    public int getCdProcessoSistema()
    {
        return this._cdProcessoSistema;
    } //-- int getCdProcessoSistema() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdSituacaoEstatisticaSistema'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoEstatisticaSistema'.
     */
    public int getCdSituacaoEstatisticaSistema()
    {
        return this._cdSituacaoEstatisticaSistema;
    } //-- int getCdSituacaoEstatisticaSistema() 

    /**
     * Returns the value of field 'dsSituacaoEstatisticaSistema'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoEstatisticaSistema'.
     */
    public java.lang.String getDsSituacaoEstatisticaSistema()
    {
        return this._dsSituacaoEstatisticaSistema;
    } //-- java.lang.String getDsSituacaoEstatisticaSistema() 

    /**
     * Returns the value of field 'dsTipoProcessoSistema'.
     * 
     * @return String
     * @return the value of field 'dsTipoProcessoSistema'.
     */
    public java.lang.String getDsTipoProcessoSistema()
    {
        return this._dsTipoProcessoSistema;
    } //-- java.lang.String getDsTipoProcessoSistema() 

    /**
     * Returns the value of field 'hrInicioEstatisticaProcessoFim'.
     * 
     * @return String
     * @return the value of field 'hrInicioEstatisticaProcessoFim'.
     */
    public java.lang.String getHrInicioEstatisticaProcessoFim()
    {
        return this._hrInicioEstatisticaProcessoFim;
    } //-- java.lang.String getHrInicioEstatisticaProcessoFim() 

    /**
     * Returns the value of field
     * 'hrInicioEstatisticaProcessoInicio'.
     * 
     * @return String
     * @return the value of field
     * 'hrInicioEstatisticaProcessoInicio'.
     */
    public java.lang.String getHrInicioEstatisticaProcessoInicio()
    {
        return this._hrInicioEstatisticaProcessoInicio;
    } //-- java.lang.String getHrInicioEstatisticaProcessoInicio() 

    /**
     * Returns the value of field 'qtRegistrosProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'qtRegistrosProcessoSistema'.
     */
    public int getQtRegistrosProcessoSistema()
    {
        return this._qtRegistrosProcessoSistema;
    } //-- int getQtRegistrosProcessoSistema() 

    /**
     * Returns the value of field 'vlRegistroProcessoSistema'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlRegistroProcessoSistema'.
     */
    public java.math.BigDecimal getVlRegistroProcessoSistema()
    {
        return this._vlRegistroProcessoSistema;
    } //-- java.math.BigDecimal getVlRegistroProcessoSistema() 

    /**
     * Method hasCdProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProcessoSistema()
    {
        return this._has_cdProcessoSistema;
    } //-- boolean hasCdProcessoSistema() 

    /**
     * Method hasCdSituacaoEstatisticaSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoEstatisticaSistema()
    {
        return this._has_cdSituacaoEstatisticaSistema;
    } //-- boolean hasCdSituacaoEstatisticaSistema() 

    /**
     * Method hasQtRegistrosProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtRegistrosProcessoSistema()
    {
        return this._has_qtRegistrosProcessoSistema;
    } //-- boolean hasQtRegistrosProcessoSistema() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProcessoSistema'.
     * 
     * @param cdProcessoSistema the value of field
     * 'cdProcessoSistema'.
     */
    public void setCdProcessoSistema(int cdProcessoSistema)
    {
        this._cdProcessoSistema = cdProcessoSistema;
        this._has_cdProcessoSistema = true;
    } //-- void setCdProcessoSistema(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoEstatisticaSistema'.
     * 
     * @param cdSituacaoEstatisticaSistema the value of field
     * 'cdSituacaoEstatisticaSistema'.
     */
    public void setCdSituacaoEstatisticaSistema(int cdSituacaoEstatisticaSistema)
    {
        this._cdSituacaoEstatisticaSistema = cdSituacaoEstatisticaSistema;
        this._has_cdSituacaoEstatisticaSistema = true;
    } //-- void setCdSituacaoEstatisticaSistema(int) 

    /**
     * Sets the value of field 'dsSituacaoEstatisticaSistema'.
     * 
     * @param dsSituacaoEstatisticaSistema the value of field
     * 'dsSituacaoEstatisticaSistema'.
     */
    public void setDsSituacaoEstatisticaSistema(java.lang.String dsSituacaoEstatisticaSistema)
    {
        this._dsSituacaoEstatisticaSistema = dsSituacaoEstatisticaSistema;
    } //-- void setDsSituacaoEstatisticaSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoProcessoSistema'.
     * 
     * @param dsTipoProcessoSistema the value of field
     * 'dsTipoProcessoSistema'.
     */
    public void setDsTipoProcessoSistema(java.lang.String dsTipoProcessoSistema)
    {
        this._dsTipoProcessoSistema = dsTipoProcessoSistema;
    } //-- void setDsTipoProcessoSistema(java.lang.String) 

    /**
     * Sets the value of field 'hrInicioEstatisticaProcessoFim'.
     * 
     * @param hrInicioEstatisticaProcessoFim the value of field
     * 'hrInicioEstatisticaProcessoFim'.
     */
    public void setHrInicioEstatisticaProcessoFim(java.lang.String hrInicioEstatisticaProcessoFim)
    {
        this._hrInicioEstatisticaProcessoFim = hrInicioEstatisticaProcessoFim;
    } //-- void setHrInicioEstatisticaProcessoFim(java.lang.String) 

    /**
     * Sets the value of field 'hrInicioEstatisticaProcessoInicio'.
     * 
     * @param hrInicioEstatisticaProcessoInicio the value of field
     * 'hrInicioEstatisticaProcessoInicio'.
     */
    public void setHrInicioEstatisticaProcessoInicio(java.lang.String hrInicioEstatisticaProcessoInicio)
    {
        this._hrInicioEstatisticaProcessoInicio = hrInicioEstatisticaProcessoInicio;
    } //-- void setHrInicioEstatisticaProcessoInicio(java.lang.String) 

    /**
     * Sets the value of field 'qtRegistrosProcessoSistema'.
     * 
     * @param qtRegistrosProcessoSistema the value of field
     * 'qtRegistrosProcessoSistema'.
     */
    public void setQtRegistrosProcessoSistema(int qtRegistrosProcessoSistema)
    {
        this._qtRegistrosProcessoSistema = qtRegistrosProcessoSistema;
        this._has_qtRegistrosProcessoSistema = true;
    } //-- void setQtRegistrosProcessoSistema(int) 

    /**
     * Sets the value of field 'vlRegistroProcessoSistema'.
     * 
     * @param vlRegistroProcessoSistema the value of field
     * 'vlRegistroProcessoSistema'.
     */
    public void setVlRegistroProcessoSistema(java.math.BigDecimal vlRegistroProcessoSistema)
    {
        this._vlRegistroProcessoSistema = vlRegistroProcessoSistema;
    } //-- void setVlRegistroProcessoSistema(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
