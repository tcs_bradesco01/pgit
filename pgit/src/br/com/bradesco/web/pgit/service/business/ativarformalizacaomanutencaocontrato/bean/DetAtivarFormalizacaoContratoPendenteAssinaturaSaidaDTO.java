/*
 * Nome: br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean;

import java.util.Date;

/**
 * Nome: DetAtivarFormalizacaoContratoPendenteAssinaturaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetAtivarFormalizacaoContratoPendenteAssinaturaSaidaDTO {

	/** Atributo numFormalizacao. */
	private int numFormalizacao;
	
	/** Atributo dataHoraInclusao. */
	private Date dataHoraInclusao;
	
	/** Atributo subContratoComercial. */
	private String subContratoComercial;
	
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public Date getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(Date dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: numFormalizacao.
	 *
	 * @return numFormalizacao
	 */
	public int getNumFormalizacao() {
		return numFormalizacao;
	}
	
	/**
	 * Set: numFormalizacao.
	 *
	 * @param numFormalizacao the num formalizacao
	 */
	public void setNumFormalizacao(int numFormalizacao) {
		this.numFormalizacao = numFormalizacao;
	}
	
	/**
	 * Get: subContratoComercial.
	 *
	 * @return subContratoComercial
	 */
	public String getSubContratoComercial() {
		return subContratoComercial;
	}
	
	/**
	 * Set: subContratoComercial.
	 *
	 * @param subContratoComercial the sub contrato comercial
	 */
	public void setSubContratoComercial(String subContratoComercial) {
		this.subContratoComercial = subContratoComercial;
	}
}
