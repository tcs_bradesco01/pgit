/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;


/**
 * Nome: ListarEnderecoParticipanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarEnderecoParticipanteEntradaDTO {

    /** Atributo maxOcorrencias. */
    private Integer maxOcorrencias;

    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;

    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;

    /** Atributo cdOperacaoProdutoServico. */
    private Integer cdOperacaoProdutoServico;

    /** Atributo cdTipoServico. */
    private Integer cdTipoServico;

    /** Atributo cdClub. */
    private Long cdClub;

    /**
     * Set: maxOcorrencias.
     *
     * @param maxOcorrencias the max ocorrencias
     */
    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    /**
     * Get: maxOcorrencias.
     *
     * @return maxOcorrencias
     */
    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    /**
     * Set: cdpessoaJuridicaContrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get: cdpessoaJuridicaContrato.
     *
     * @return cdpessoaJuridicaContrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoOperacaoRelacionado.
     *
     * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Get: cdProdutoOperacaoRelacionado.
     *
     * @return cdProdutoOperacaoRelacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }

    /**
     * Set: cdOperacaoProdutoServico.
     *
     * @param cdOperacaoProdutoServico the cd operacao produto servico
     */
    public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
        this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
    }

    /**
     * Get: cdOperacaoProdutoServico.
     *
     * @return cdOperacaoProdutoServico
     */
    public Integer getCdOperacaoProdutoServico() {
        return this.cdOperacaoProdutoServico;
    }

    /**
     * Set: cdClub.
     *
     * @param cdClub the cd club
     */
    public void setCdClub(Long cdClub) {
        this.cdClub = cdClub;
    }

    /**
     * Get: cdClub.
     *
     * @return cdClub
     */
    public Long getCdClub() {
        return this.cdClub;
    }

	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}

	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
}