/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.io.Serializable;

/**
 * Nome: EmpresaConglomeradoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmpresaConglomeradoEntradaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -2995237772025257574L;

	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;

    /** Atributo cdSituacao. */
    private Integer cdSituacao;

    /**
     * Empresa conglomerado entrada dto.
     */
    public EmpresaConglomeradoEntradaDTO() {
    	
    }

	/**
	 * Empresa conglomerado entrada dto.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public EmpresaConglomeradoEntradaDTO(Integer cdSituacao) {
		super();
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}

	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}

}
