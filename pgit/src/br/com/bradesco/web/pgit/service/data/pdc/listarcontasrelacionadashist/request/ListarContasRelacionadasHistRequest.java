/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarContasRelacionadasHistRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarContasRelacionadasHistRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdPessoaJuridicaNegocio
     */
    private long _cdPessoaJuridicaNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaNegocio
     */
    private boolean _has_cdPessoaJuridicaNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdPessoaJuridicaVinculo
     */
    private long _cdPessoaJuridicaVinculo = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaVinculo
     */
    private boolean _has_cdPessoaJuridicaVinculo;

    /**
     * Field _cdTipoContratoVinculo
     */
    private int _cdTipoContratoVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinculo
     */
    private boolean _has_cdTipoContratoVinculo;

    /**
     * Field _nrSequenciaContratoVinculo
     */
    private long _nrSequenciaContratoVinculo = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoVinculo
     */
    private boolean _has_nrSequenciaContratoVinculo;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _cdTipoRelacionamentoConta
     */
    private int _cdTipoRelacionamentoConta = 0;

    /**
     * keeps track of state for field: _cdTipoRelacionamentoConta
     */
    private boolean _has_cdTipoRelacionamentoConta;

    /**
     * Field _cdBancoRelacionamento
     */
    private int _cdBancoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdBancoRelacionamento
     */
    private boolean _has_cdBancoRelacionamento;

    /**
     * Field _cdAgenciaRelacionamento
     */
    private int _cdAgenciaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdAgenciaRelacionamento
     */
    private boolean _has_cdAgenciaRelacionamento;

    /**
     * Field _cdContaRelacionamento
     */
    private long _cdContaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdContaRelacionamento
     */
    private boolean _has_cdContaRelacionamento;

    /**
     * Field _cdDigitoContaRelacionamento
     */
    private java.lang.String _cdDigitoContaRelacionamento;

    /**
     * Field _cdCorpoCpfCnpjParticipante
     */
    private long _cdCorpoCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpjParticipante
     */
    private boolean _has_cdCorpoCpfCnpjParticipante;

    /**
     * Field _cdFilialCpfCnpjParticipante
     */
    private int _cdFilialCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjParticipante
     */
    private boolean _has_cdFilialCpfCnpjParticipante;

    /**
     * Field _cdDigitoCpfCnpjParticipante
     */
    private int _cdDigitoCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdDigitoCpfCnpjParticipante
     */
    private boolean _has_cdDigitoCpfCnpjParticipante;

    /**
     * Field _dtInicioManutencao
     */
    private int _dtInicioManutencao = 0;

    /**
     * keeps track of state for field: _dtInicioManutencao
     */
    private boolean _has_dtInicioManutencao;

    /**
     * Field _dtFimManutencao
     */
    private int _dtFimManutencao = 0;

    /**
     * keeps track of state for field: _dtFimManutencao
     */
    private boolean _has_dtFimManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarContasRelacionadasHistRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.request.ListarContasRelacionadasHistRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaRelacionamento
     * 
     */
    public void deleteCdAgenciaRelacionamento()
    {
        this._has_cdAgenciaRelacionamento= false;
    } //-- void deleteCdAgenciaRelacionamento() 

    /**
     * Method deleteCdBancoRelacionamento
     * 
     */
    public void deleteCdBancoRelacionamento()
    {
        this._has_cdBancoRelacionamento= false;
    } //-- void deleteCdBancoRelacionamento() 

    /**
     * Method deleteCdContaRelacionamento
     * 
     */
    public void deleteCdContaRelacionamento()
    {
        this._has_cdContaRelacionamento= false;
    } //-- void deleteCdContaRelacionamento() 

    /**
     * Method deleteCdCorpoCpfCnpjParticipante
     * 
     */
    public void deleteCdCorpoCpfCnpjParticipante()
    {
        this._has_cdCorpoCpfCnpjParticipante= false;
    } //-- void deleteCdCorpoCpfCnpjParticipante() 

    /**
     * Method deleteCdDigitoCpfCnpjParticipante
     * 
     */
    public void deleteCdDigitoCpfCnpjParticipante()
    {
        this._has_cdDigitoCpfCnpjParticipante= false;
    } //-- void deleteCdDigitoCpfCnpjParticipante() 

    /**
     * Method deleteCdFilialCpfCnpjParticipante
     * 
     */
    public void deleteCdFilialCpfCnpjParticipante()
    {
        this._has_cdFilialCpfCnpjParticipante= false;
    } //-- void deleteCdFilialCpfCnpjParticipante() 

    /**
     * Method deleteCdPessoaJuridicaNegocio
     * 
     */
    public void deleteCdPessoaJuridicaNegocio()
    {
        this._has_cdPessoaJuridicaNegocio= false;
    } //-- void deleteCdPessoaJuridicaNegocio() 

    /**
     * Method deleteCdPessoaJuridicaVinculo
     * 
     */
    public void deleteCdPessoaJuridicaVinculo()
    {
        this._has_cdPessoaJuridicaVinculo= false;
    } //-- void deleteCdPessoaJuridicaVinculo() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoVinculo
     * 
     */
    public void deleteCdTipoContratoVinculo()
    {
        this._has_cdTipoContratoVinculo= false;
    } //-- void deleteCdTipoContratoVinculo() 

    /**
     * Method deleteCdTipoRelacionamentoConta
     * 
     */
    public void deleteCdTipoRelacionamentoConta()
    {
        this._has_cdTipoRelacionamentoConta= false;
    } //-- void deleteCdTipoRelacionamentoConta() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteDtFimManutencao
     * 
     */
    public void deleteDtFimManutencao()
    {
        this._has_dtFimManutencao= false;
    } //-- void deleteDtFimManutencao() 

    /**
     * Method deleteDtInicioManutencao
     * 
     */
    public void deleteDtInicioManutencao()
    {
        this._has_dtInicioManutencao= false;
    } //-- void deleteDtInicioManutencao() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoVinculo
     * 
     */
    public void deleteNrSequenciaContratoVinculo()
    {
        this._has_nrSequenciaContratoVinculo= false;
    } //-- void deleteNrSequenciaContratoVinculo() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdAgenciaRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaRelacionamento'.
     */
    public int getCdAgenciaRelacionamento()
    {
        return this._cdAgenciaRelacionamento;
    } //-- int getCdAgenciaRelacionamento() 

    /**
     * Returns the value of field 'cdBancoRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdBancoRelacionamento'.
     */
    public int getCdBancoRelacionamento()
    {
        return this._cdBancoRelacionamento;
    } //-- int getCdBancoRelacionamento() 

    /**
     * Returns the value of field 'cdContaRelacionamento'.
     * 
     * @return long
     * @return the value of field 'cdContaRelacionamento'.
     */
    public long getCdContaRelacionamento()
    {
        return this._cdContaRelacionamento;
    } //-- long getCdContaRelacionamento() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpjParticipante'.
     */
    public long getCdCorpoCpfCnpjParticipante()
    {
        return this._cdCorpoCpfCnpjParticipante;
    } //-- long getCdCorpoCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdDigitoContaRelacionamento'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaRelacionamento'.
     */
    public java.lang.String getCdDigitoContaRelacionamento()
    {
        return this._cdDigitoContaRelacionamento;
    } //-- java.lang.String getCdDigitoContaRelacionamento() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpfCnpjParticipante'.
     */
    public int getCdDigitoCpfCnpjParticipante()
    {
        return this._cdDigitoCpfCnpjParticipante;
    } //-- int getCdDigitoCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjParticipante'.
     */
    public int getCdFilialCpfCnpjParticipante()
    {
        return this._cdFilialCpfCnpjParticipante;
    } //-- int getCdFilialCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaNegocio'.
     */
    public long getCdPessoaJuridicaNegocio()
    {
        return this._cdPessoaJuridicaNegocio;
    } //-- long getCdPessoaJuridicaNegocio() 

    /**
     * Returns the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaVinculo'.
     */
    public long getCdPessoaJuridicaVinculo()
    {
        return this._cdPessoaJuridicaVinculo;
    } //-- long getCdPessoaJuridicaVinculo() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinculo'.
     */
    public int getCdTipoContratoVinculo()
    {
        return this._cdTipoContratoVinculo;
    } //-- int getCdTipoContratoVinculo() 

    /**
     * Returns the value of field 'cdTipoRelacionamentoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelacionamentoConta'.
     */
    public int getCdTipoRelacionamentoConta()
    {
        return this._cdTipoRelacionamentoConta;
    } //-- int getCdTipoRelacionamentoConta() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'dtFimManutencao'.
     * 
     * @return int
     * @return the value of field 'dtFimManutencao'.
     */
    public int getDtFimManutencao()
    {
        return this._dtFimManutencao;
    } //-- int getDtFimManutencao() 

    /**
     * Returns the value of field 'dtInicioManutencao'.
     * 
     * @return int
     * @return the value of field 'dtInicioManutencao'.
     */
    public int getDtInicioManutencao()
    {
        return this._dtInicioManutencao;
    } //-- int getDtInicioManutencao() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoVinculo'.
     */
    public long getNrSequenciaContratoVinculo()
    {
        return this._nrSequenciaContratoVinculo;
    } //-- long getNrSequenciaContratoVinculo() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdAgenciaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaRelacionamento()
    {
        return this._has_cdAgenciaRelacionamento;
    } //-- boolean hasCdAgenciaRelacionamento() 

    /**
     * Method hasCdBancoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoRelacionamento()
    {
        return this._has_cdBancoRelacionamento;
    } //-- boolean hasCdBancoRelacionamento() 

    /**
     * Method hasCdContaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaRelacionamento()
    {
        return this._has_cdContaRelacionamento;
    } //-- boolean hasCdContaRelacionamento() 

    /**
     * Method hasCdCorpoCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpjParticipante()
    {
        return this._has_cdCorpoCpfCnpjParticipante;
    } //-- boolean hasCdCorpoCpfCnpjParticipante() 

    /**
     * Method hasCdDigitoCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpfCnpjParticipante()
    {
        return this._has_cdDigitoCpfCnpjParticipante;
    } //-- boolean hasCdDigitoCpfCnpjParticipante() 

    /**
     * Method hasCdFilialCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjParticipante()
    {
        return this._has_cdFilialCpfCnpjParticipante;
    } //-- boolean hasCdFilialCpfCnpjParticipante() 

    /**
     * Method hasCdPessoaJuridicaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaNegocio()
    {
        return this._has_cdPessoaJuridicaNegocio;
    } //-- boolean hasCdPessoaJuridicaNegocio() 

    /**
     * Method hasCdPessoaJuridicaVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaVinculo()
    {
        return this._has_cdPessoaJuridicaVinculo;
    } //-- boolean hasCdPessoaJuridicaVinculo() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinculo()
    {
        return this._has_cdTipoContratoVinculo;
    } //-- boolean hasCdTipoContratoVinculo() 

    /**
     * Method hasCdTipoRelacionamentoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelacionamentoConta()
    {
        return this._has_cdTipoRelacionamentoConta;
    } //-- boolean hasCdTipoRelacionamentoConta() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasDtFimManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDtFimManutencao()
    {
        return this._has_dtFimManutencao;
    } //-- boolean hasDtFimManutencao() 

    /**
     * Method hasDtInicioManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDtInicioManutencao()
    {
        return this._has_dtInicioManutencao;
    } //-- boolean hasDtInicioManutencao() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoVinculo()
    {
        return this._has_nrSequenciaContratoVinculo;
    } //-- boolean hasNrSequenciaContratoVinculo() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaRelacionamento'.
     * 
     * @param cdAgenciaRelacionamento the value of field
     * 'cdAgenciaRelacionamento'.
     */
    public void setCdAgenciaRelacionamento(int cdAgenciaRelacionamento)
    {
        this._cdAgenciaRelacionamento = cdAgenciaRelacionamento;
        this._has_cdAgenciaRelacionamento = true;
    } //-- void setCdAgenciaRelacionamento(int) 

    /**
     * Sets the value of field 'cdBancoRelacionamento'.
     * 
     * @param cdBancoRelacionamento the value of field
     * 'cdBancoRelacionamento'.
     */
    public void setCdBancoRelacionamento(int cdBancoRelacionamento)
    {
        this._cdBancoRelacionamento = cdBancoRelacionamento;
        this._has_cdBancoRelacionamento = true;
    } //-- void setCdBancoRelacionamento(int) 

    /**
     * Sets the value of field 'cdContaRelacionamento'.
     * 
     * @param cdContaRelacionamento the value of field
     * 'cdContaRelacionamento'.
     */
    public void setCdContaRelacionamento(long cdContaRelacionamento)
    {
        this._cdContaRelacionamento = cdContaRelacionamento;
        this._has_cdContaRelacionamento = true;
    } //-- void setCdContaRelacionamento(long) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpjParticipante'.
     * 
     * @param cdCorpoCpfCnpjParticipante the value of field
     * 'cdCorpoCpfCnpjParticipante'.
     */
    public void setCdCorpoCpfCnpjParticipante(long cdCorpoCpfCnpjParticipante)
    {
        this._cdCorpoCpfCnpjParticipante = cdCorpoCpfCnpjParticipante;
        this._has_cdCorpoCpfCnpjParticipante = true;
    } //-- void setCdCorpoCpfCnpjParticipante(long) 

    /**
     * Sets the value of field 'cdDigitoContaRelacionamento'.
     * 
     * @param cdDigitoContaRelacionamento the value of field
     * 'cdDigitoContaRelacionamento'.
     */
    public void setCdDigitoContaRelacionamento(java.lang.String cdDigitoContaRelacionamento)
    {
        this._cdDigitoContaRelacionamento = cdDigitoContaRelacionamento;
    } //-- void setCdDigitoContaRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpjParticipante'.
     * 
     * @param cdDigitoCpfCnpjParticipante the value of field
     * 'cdDigitoCpfCnpjParticipante'.
     */
    public void setCdDigitoCpfCnpjParticipante(int cdDigitoCpfCnpjParticipante)
    {
        this._cdDigitoCpfCnpjParticipante = cdDigitoCpfCnpjParticipante;
        this._has_cdDigitoCpfCnpjParticipante = true;
    } //-- void setCdDigitoCpfCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjParticipante'.
     * 
     * @param cdFilialCpfCnpjParticipante the value of field
     * 'cdFilialCpfCnpjParticipante'.
     */
    public void setCdFilialCpfCnpjParticipante(int cdFilialCpfCnpjParticipante)
    {
        this._cdFilialCpfCnpjParticipante = cdFilialCpfCnpjParticipante;
        this._has_cdFilialCpfCnpjParticipante = true;
    } //-- void setCdFilialCpfCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @param cdPessoaJuridicaNegocio the value of field
     * 'cdPessoaJuridicaNegocio'.
     */
    public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio)
    {
        this._cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
        this._has_cdPessoaJuridicaNegocio = true;
    } //-- void setCdPessoaJuridicaNegocio(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @param cdPessoaJuridicaVinculo the value of field
     * 'cdPessoaJuridicaVinculo'.
     */
    public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo)
    {
        this._cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
        this._has_cdPessoaJuridicaVinculo = true;
    } //-- void setCdPessoaJuridicaVinculo(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinculo'.
     * 
     * @param cdTipoContratoVinculo the value of field
     * 'cdTipoContratoVinculo'.
     */
    public void setCdTipoContratoVinculo(int cdTipoContratoVinculo)
    {
        this._cdTipoContratoVinculo = cdTipoContratoVinculo;
        this._has_cdTipoContratoVinculo = true;
    } //-- void setCdTipoContratoVinculo(int) 

    /**
     * Sets the value of field 'cdTipoRelacionamentoConta'.
     * 
     * @param cdTipoRelacionamentoConta the value of field
     * 'cdTipoRelacionamentoConta'.
     */
    public void setCdTipoRelacionamentoConta(int cdTipoRelacionamentoConta)
    {
        this._cdTipoRelacionamentoConta = cdTipoRelacionamentoConta;
        this._has_cdTipoRelacionamentoConta = true;
    } //-- void setCdTipoRelacionamentoConta(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'dtFimManutencao'.
     * 
     * @param dtFimManutencao the value of field 'dtFimManutencao'.
     */
    public void setDtFimManutencao(int dtFimManutencao)
    {
        this._dtFimManutencao = dtFimManutencao;
        this._has_dtFimManutencao = true;
    } //-- void setDtFimManutencao(int) 

    /**
     * Sets the value of field 'dtInicioManutencao'.
     * 
     * @param dtInicioManutencao the value of field
     * 'dtInicioManutencao'.
     */
    public void setDtInicioManutencao(int dtInicioManutencao)
    {
        this._dtInicioManutencao = dtInicioManutencao;
        this._has_dtInicioManutencao = true;
    } //-- void setDtInicioManutencao(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @param nrSequenciaContratoVinculo the value of field
     * 'nrSequenciaContratoVinculo'.
     */
    public void setNrSequenciaContratoVinculo(long nrSequenciaContratoVinculo)
    {
        this._nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
        this._has_nrSequenciaContratoVinculo = true;
    } //-- void setNrSequenciaContratoVinculo(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarContasRelacionadasHistRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.request.ListarContasRelacionadasHistRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.request.ListarContasRelacionadasHistRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.request.ListarContasRelacionadasHistRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.request.ListarContasRelacionadasHistRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
