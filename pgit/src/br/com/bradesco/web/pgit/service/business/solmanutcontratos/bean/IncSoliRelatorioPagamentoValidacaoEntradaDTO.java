/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

/**
 * Nome: IncSoliRelatorioPagamentoValidacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncSoliRelatorioPagamentoValidacaoEntradaDTO {
	
    /** Atributo tpRelatorio. */
    private Integer tpRelatorio;
    
    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dtIniPagamento. */
    private String dtIniPagamento;
    
    /** Atributo dtFimPagamento. */
    private String dtFimPagamento;
    
    /** Atributo cdCpfCnpjParticipante. */
    private Long cdCpfCnpjParticipante;
    
    /** Atributo cdFilialCnpjParticipante. */
    private Integer cdFilialCnpjParticipante;
    
    /** Atributo cdControleCpfParticipante. */
    private Integer cdControleCpfParticipante;
    
    /** Atributo cdPessoa. */
    private Long cdPessoa;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo cdRelacionamentoProduto. */
    private Integer cdRelacionamentoProduto;
    
	/**
	 * Get: tpRelatorio.
	 *
	 * @return tpRelatorio
	 */
	public Integer getTpRelatorio() {
		return tpRelatorio;
	}
	
	/**
	 * Set: tpRelatorio.
	 *
	 * @param tpRelatorio the tp relatorio
	 */
	public void setTpRelatorio(Integer tpRelatorio) {
		this.tpRelatorio = tpRelatorio;
	}
	
	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: dtIniPagamento.
	 *
	 * @return dtIniPagamento
	 */
	public String getDtIniPagamento() {
		return dtIniPagamento;
	}
	
	/**
	 * Set: dtIniPagamento.
	 *
	 * @param dtIniPagamento the dt ini pagamento
	 */
	public void setDtIniPagamento(String dtIniPagamento) {
		this.dtIniPagamento = dtIniPagamento;
	}
	
	/**
	 * Get: dtFimPagamento.
	 *
	 * @return dtFimPagamento
	 */
	public String getDtFimPagamento() {
		return dtFimPagamento;
	}
	
	/**
	 * Set: dtFimPagamento.
	 *
	 * @param dtFimPagamento the dt fim pagamento
	 */
	public void setDtFimPagamento(String dtFimPagamento) {
		this.dtFimPagamento = dtFimPagamento;
	}
	
	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public Long getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(Long cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdFilialCnpjParticipante.
	 *
	 * @return cdFilialCnpjParticipante
	 */
	public Integer getCdFilialCnpjParticipante() {
		return cdFilialCnpjParticipante;
	}
	
	/**
	 * Set: cdFilialCnpjParticipante.
	 *
	 * @param cdFilialCnpjParticipante the cd filial cnpj participante
	 */
	public void setCdFilialCnpjParticipante(Integer cdFilialCnpjParticipante) {
		this.cdFilialCnpjParticipante = cdFilialCnpjParticipante;
	}
	
	/**
	 * Get: cdControleCpfParticipante.
	 *
	 * @return cdControleCpfParticipante
	 */
	public Integer getCdControleCpfParticipante() {
		return cdControleCpfParticipante;
	}
	
	/**
	 * Set: cdControleCpfParticipante.
	 *
	 * @param cdControleCpfParticipante the cd controle cpf participante
	 */
	public void setCdControleCpfParticipante(Integer cdControleCpfParticipante) {
		this.cdControleCpfParticipante = cdControleCpfParticipante;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

}
