/*
 * Nome: br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagtoPendConsolidadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtoPendConsolidadoSaidaDTO {
	
	/** Atributo cdMensagem. */
	private String cdMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrContrato. */
	private Long nrContrato;
	
	/** Atributo nrCnpjCpf. */
	private Long nrCnpjCpf;
	
	/** Atributo nrFilialCnpjCpf. */
	private Integer nrFilialCnpjCpf;
	
	/** Atributo cdDigitoCnpjCpf. */
	private Integer cdDigitoCnpjCpf;
	
	/** Atributo nrRemessa. */
	private Long nrRemessa;
	
	/** Atributo dtPagamento. */
	private String dtPagamento;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo cdAgencia. */
	private Integer cdAgencia;
	
	/** Atributo cdDigitoAgencia. */
	private Integer cdDigitoAgencia;
	
	/** Atributo cdConta. */
	private Long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdPessoaJuridicaDebito. */
	private Long cdPessoaJuridicaDebito;
	
	/** Atributo cdTipoContratoDebito. */
	private Integer cdTipoContratoDebito;
	
	/** Atributo nrSequenciaContratoDebito. */
	private Long nrSequenciaContratoDebito;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo dsResumoProduto. */
	private String dsResumoProduto;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo dsOperacao. */
	private String dsOperacao;
	
	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento;
	
	/** Atributo dsSituacaoOperacaoPagamento. */
	private String dsSituacaoOperacaoPagamento;
	
	/** Atributo cdMotivoOperacaoPagamento. */
	private Integer cdMotivoOperacaoPagamento;
	
	/** Atributo dsMotivoOperacaoPagamento. */
	private String dsMotivoOperacaoPagamento;
	
	/** Atributo qtPagamento. */
	private Long qtPagamento;
	
	/** Atributo vlPagamento. */
	private BigDecimal vlPagamento;
	
	/** Atributo empresaConglomeradoFormatada. */
	private String empresaConglomeradoFormatada;
	
	/** Atributo numeroContratoFormatado. */
	private String numeroContratoFormatado;
	
	/** Atributo cnpjCpfFormatado. */
	private String cnpjCpfFormatado;
	
	/** Atributo contaDebitoFormatada. */
	private String contaDebitoFormatada;
	
	/** Atributo tipoServicoFormatado. */
	private String tipoServicoFormatado;
	
	/** Atributo modalidadeFormatada. */
	private String modalidadeFormatada;
	
	/** Atributo remessaFormatada. */
	private String remessaFormatada;
	
	/** Atributo qtddPagamentoFormatado. */
	private String qtddPagamentoFormatado;
	
	/** Atributo dtPagamentoFormatada. */
	private String dtPagamentoFormatada;
	
	/**
	 * Get: dtPagamentoFormatada.
	 *
	 * @return dtPagamentoFormatada
	 */
	public String getDtPagamentoFormatada() {
		return dtPagamentoFormatada;
	}
	
	/**
	 * Set: dtPagamentoFormatada.
	 *
	 * @param dtPagamentoFormatada the dt pagamento formatada
	 */
	public void setDtPagamentoFormatada(String dtPagamentoFormatada) {
		this.dtPagamentoFormatada = dtPagamentoFormatada;
	}
	
	/**
	 * Get: qtddPagamentoFormatado.
	 *
	 * @return qtddPagamentoFormatado
	 */
	public String getQtddPagamentoFormatado() {
		return qtddPagamentoFormatado;
	}
	
	/**
	 * Set: qtddPagamentoFormatado.
	 *
	 * @param qtddPagamentoFormatado the qtdd pagamento formatado
	 */
	public void setQtddPagamentoFormatado(String qtddPagamentoFormatado) {
		this.qtddPagamentoFormatado = qtddPagamentoFormatado;
	}
	
	/**
	 * Get: remessaFormatada.
	 *
	 * @return remessaFormatada
	 */
	public String getRemessaFormatada() {
		return remessaFormatada;
	}
	
	/**
	 * Set: remessaFormatada.
	 *
	 * @param remessaFormatada the remessa formatada
	 */
	public void setRemessaFormatada(String remessaFormatada) {
		this.remessaFormatada = remessaFormatada;
	}
	
	/**
	 * Get: numeroContratoFormatado.
	 *
	 * @return numeroContratoFormatado
	 */
	public String getNumeroContratoFormatado() {
		return numeroContratoFormatado;
	}
	
	/**
	 * Set: numeroContratoFormatado.
	 *
	 * @param numeroContratoFormatado the numero contrato formatado
	 */
	public void setNumeroContratoFormatado(String numeroContratoFormatado) {
		this.numeroContratoFormatado = numeroContratoFormatado;
	}
	
	/**
	 * Get: modalidadeFormatada.
	 *
	 * @return modalidadeFormatada
	 */
	public String getModalidadeFormatada() {
		return modalidadeFormatada;
	}
	
	/**
	 * Set: modalidadeFormatada.
	 *
	 * @param modalidadeFormatada the modalidade formatada
	 */
	public void setModalidadeFormatada(String modalidadeFormatada) {
		this.modalidadeFormatada = modalidadeFormatada;
	}
	
	/**
	 * Get: contaDebitoFormatada.
	 *
	 * @return contaDebitoFormatada
	 */
	public String getContaDebitoFormatada() {
		return contaDebitoFormatada;
	}
	
	/**
	 * Set: contaDebitoFormatada.
	 *
	 * @param contaDebitoFormatada the conta debito formatada
	 */
	public void setContaDebitoFormatada(String contaDebitoFormatada) {
		this.contaDebitoFormatada = contaDebitoFormatada;
	}
	
	/**
	 * Get: cnpjCpfFormatado.
	 *
	 * @return cnpjCpfFormatado
	 */
	public String getCnpjCpfFormatado() {
		return cnpjCpfFormatado;
	}
	
	/**
	 * Set: cnpjCpfFormatado.
	 *
	 * @param cnpjCpfFormatado the cnpj cpf formatado
	 */
	public void setCnpjCpfFormatado(String cnpjCpfFormatado) {
		this.cnpjCpfFormatado = cnpjCpfFormatado;
	}
	
	/**
	 * Get: empresaConglomeradoFormatada.
	 *
	 * @return empresaConglomeradoFormatada
	 */
	public String getEmpresaConglomeradoFormatada() {
		return empresaConglomeradoFormatada;
	}
	
	/**
	 * Set: empresaConglomeradoFormatada.
	 *
	 * @param empresaConglomeradoFormatada the empresa conglomerado formatada
	 */
	public void setEmpresaConglomeradoFormatada(String empresaConglomeradoFormatada) {
		this.empresaConglomeradoFormatada = empresaConglomeradoFormatada;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoCnpjCpf.
	 *
	 * @return cdDigitoCnpjCpf
	 */
	public Integer getCdDigitoCnpjCpf() {
		return cdDigitoCnpjCpf;
	}
	
	/**
	 * Set: cdDigitoCnpjCpf.
	 *
	 * @param cdDigitoCnpjCpf the cd digito cnpj cpf
	 */
	public void setCdDigitoCnpjCpf(Integer cdDigitoCnpjCpf) {
		this.cdDigitoCnpjCpf = cdDigitoCnpjCpf;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdMensagem.
	 *
	 * @return cdMensagem
	 */
	public String getCdMensagem() {
		return cdMensagem;
	}
	
	/**
	 * Set: cdMensagem.
	 *
	 * @param cdMensagem the cd mensagem
	 */
	public void setCdMensagem(String cdMensagem) {
		this.cdMensagem = cdMensagem;
	}
	
	/**
	 * Get: cdMotivoOperacaoPagamento.
	 *
	 * @return cdMotivoOperacaoPagamento
	 */
	public Integer getCdMotivoOperacaoPagamento() {
		return cdMotivoOperacaoPagamento;
	}
	
	/**
	 * Set: cdMotivoOperacaoPagamento.
	 *
	 * @param cdMotivoOperacaoPagamento the cd motivo operacao pagamento
	 */
	public void setCdMotivoOperacaoPagamento(Integer cdMotivoOperacaoPagamento) {
		this.cdMotivoOperacaoPagamento = cdMotivoOperacaoPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdPessoaJuridicaDebito.
	 *
	 * @return cdPessoaJuridicaDebito
	 */
	public Long getCdPessoaJuridicaDebito() {
		return cdPessoaJuridicaDebito;
	}
	
	/**
	 * Set: cdPessoaJuridicaDebito.
	 *
	 * @param cdPessoaJuridicaDebito the cd pessoa juridica debito
	 */
	public void setCdPessoaJuridicaDebito(Long cdPessoaJuridicaDebito) {
		this.cdPessoaJuridicaDebito = cdPessoaJuridicaDebito;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}
	
	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}
	
	/**
	 * Get: dsMotivoOperacaoPagamento.
	 *
	 * @return dsMotivoOperacaoPagamento
	 */
	public String getDsMotivoOperacaoPagamento() {
		return dsMotivoOperacaoPagamento;
	}
	
	/**
	 * Set: dsMotivoOperacaoPagamento.
	 *
	 * @param dsMotivoOperacaoPagamento the ds motivo operacao pagamento
	 */
	public void setDsMotivoOperacaoPagamento(String dsMotivoOperacaoPagamento) {
		this.dsMotivoOperacaoPagamento = dsMotivoOperacaoPagamento;
	}
	
	/**
	 * Get: dsOperacao.
	 *
	 * @return dsOperacao
	 */
	public String getDsOperacao() {
		return dsOperacao;
	}
	
	/**
	 * Set: dsOperacao.
	 *
	 * @param dsOperacao the ds operacao
	 */
	public void setDsOperacao(String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dsResumoProduto.
	 *
	 * @return dsResumoProduto
	 */
	public String getDsResumoProduto() {
		return dsResumoProduto;
	}
	
	/**
	 * Set: dsResumoProduto.
	 *
	 * @param dsResumoProduto the ds resumo produto
	 */
	public void setDsResumoProduto(String dsResumoProduto) {
		this.dsResumoProduto = dsResumoProduto;
	}
	
	/**
	 * Get: dsSituacaoOperacaoPagamento.
	 *
	 * @return dsSituacaoOperacaoPagamento
	 */
	public String getDsSituacaoOperacaoPagamento() {
		return dsSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: dsSituacaoOperacaoPagamento.
	 *
	 * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
	 */
	public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
		this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}
	
	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public Long getNrCnpjCpf() {
		return nrCnpjCpf;
	}
	
	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(Long nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}
	
	/**
	 * Get: nrContrato.
	 *
	 * @return nrContrato
	 */
	public Long getNrContrato() {
		return nrContrato;
	}
	
	/**
	 * Set: nrContrato.
	 *
	 * @param nrContrato the nr contrato
	 */
	public void setNrContrato(Long nrContrato) {
		this.nrContrato = nrContrato;
	}
	
	/**
	 * Get: nrFilialCnpjCpf.
	 *
	 * @return nrFilialCnpjCpf
	 */
	public Integer getNrFilialCnpjCpf() {
		return nrFilialCnpjCpf;
	}
	
	/**
	 * Set: nrFilialCnpjCpf.
	 *
	 * @param nrFilialCnpjCpf the nr filial cnpj cpf
	 */
	public void setNrFilialCnpjCpf(Integer nrFilialCnpjCpf) {
		this.nrFilialCnpjCpf = nrFilialCnpjCpf;
	}
	
	/**
	 * Get: nrRemessa.
	 *
	 * @return nrRemessa
	 */
	public Long getNrRemessa() {
		return nrRemessa;
	}
	
	/**
	 * Set: nrRemessa.
	 *
	 * @param nrRemessa the nr remessa
	 */
	public void setNrRemessa(Long nrRemessa) {
		this.nrRemessa = nrRemessa;
	}
	
	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}
	
	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}
	
	/**
	 * Get: qtPagamento.
	 *
	 * @return qtPagamento
	 */
	public Long getQtPagamento() {
		return qtPagamento;
	}
	
	/**
	 * Set: qtPagamento.
	 *
	 * @param qtPagamento the qt pagamento
	 */
	public void setQtPagamento(Long qtPagamento) {
		this.qtPagamento = qtPagamento;
	}
	
	/**
	 * Get: vlPagamento.
	 *
	 * @return vlPagamento
	 */
	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}
	
	/**
	 * Set: vlPagamento.
	 *
	 * @param vlPagamento the vl pagamento
	 */
	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}
	
	/**
	 * Get: tipoServicoFormatado.
	 *
	 * @return tipoServicoFormatado
	 */
	public String getTipoServicoFormatado() {
		return tipoServicoFormatado;
	}
	
	/**
	 * Set: tipoServicoFormatado.
	 *
	 * @param tipoServicoFormatado the tipo servico formatado
	 */
	public void setTipoServicoFormatado(String tipoServicoFormatado) {
		this.tipoServicoFormatado = tipoServicoFormatado;
	}
	
	


}
