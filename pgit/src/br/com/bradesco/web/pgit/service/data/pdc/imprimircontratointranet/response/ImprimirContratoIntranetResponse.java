/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ImprimirContratoIntranetResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ImprimirContratoIntranetResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _dsLogradouroPagador
     */
    private java.lang.String _dsLogradouroPagador;

    /**
     * Field _dsNumeroLogradouroPagador
     */
    private java.lang.String _dsNumeroLogradouroPagador;

    /**
     * Field _dsComplementoLogradouroPagador
     */
    private java.lang.String _dsComplementoLogradouroPagador;

    /**
     * Field _dsBairroClientePagador
     */
    private java.lang.String _dsBairroClientePagador;

    /**
     * Field _dsMunicipioClientePagador
     */
    private java.lang.String _dsMunicipioClientePagador;

    /**
     * Field _cdSiglaUfPagador
     */
    private java.lang.String _cdSiglaUfPagador;

    /**
     * Field _cdCepPagador
     */
    private int _cdCepPagador = 0;

    /**
     * keeps track of state for field: _cdCepPagador
     */
    private boolean _has_cdCepPagador;

    /**
     * Field _cdCepComplementoPagador
     */
    private int _cdCepComplementoPagador = 0;

    /**
     * keeps track of state for field: _cdCepComplementoPagador
     */
    private boolean _has_cdCepComplementoPagador;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdAgenciaGestora
     */
    private int _cdAgenciaGestora = 0;

    /**
     * keeps track of state for field: _cdAgenciaGestora
     */
    private boolean _has_cdAgenciaGestora;

    /**
     * Field _dsAgenciaGestora
     */
    private java.lang.String _dsAgenciaGestora;

    /**
     * Field _dsMunicipioAgenciaGestora
     */
    private java.lang.String _dsMunicipioAgenciaGestora;

    /**
     * Field _dsSiglaUfParticipante
     */
    private java.lang.String _dsSiglaUfParticipante;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _dtGeracaoContratoNegocio
     */
    private java.lang.String _dtGeracaoContratoNegocio;

    /**
     * Field _cdTransferenciaContaSalarial
     */
    private java.lang.String _cdTransferenciaContaSalarial;

    /**
     * Field _hrLimiteTransmissaoArquivo
     */
    private java.lang.String _hrLimiteTransmissaoArquivo;

    /**
     * Field _hrLimitePagamentoRemessa
     */
    private java.lang.String _hrLimitePagamentoRemessa;

    /**
     * Field _hrLimiteSuperiorBoleto
     */
    private java.lang.String _hrLimiteSuperiorBoleto;

    /**
     * Field _hrLimiteInferiorBoleto
     */
    private java.lang.String _hrLimiteInferiorBoleto;

    /**
     * Field _hrLimitePagamentoSuperior
     */
    private java.lang.String _hrLimitePagamentoSuperior;

    /**
     * Field _hrLimitePagamentoInferior
     */
    private java.lang.String _hrLimitePagamentoInferior;

    /**
     * Field _hrLimiteEfetivacaoPagamento
     */
    private java.lang.String _hrLimiteEfetivacaoPagamento;

    /**
     * Field _hrLimiteConsultaPagamento
     */
    private java.lang.String _hrLimiteConsultaPagamento;

    /**
     * Field _hrCicloEfetivacaoPagamento
     */
    private java.lang.String _hrCicloEfetivacaoPagamento;

    /**
     * Field _nrQuantidadeLimiteCobrancaImpressao
     */
    private java.lang.String _nrQuantidadeLimiteCobrancaImpressao;

    /**
     * Field _dsDisponibilizacaoComprovanteBanco
     */
    private java.lang.String _dsDisponibilizacaoComprovanteBanco;

    /**
     * Field _hrLimiteApresentaCarta
     */
    private java.lang.String _hrLimiteApresentaCarta;

    /**
     * Field _nrMontanteBoleto
     */
    private java.lang.String _nrMontanteBoleto;

    /**
     * Field _dsFrasePadraoCaput
     */
    private java.lang.String _dsFrasePadraoCaput;

    /**
     * Field _dsDetalheModalidadeDoc
     */
    private java.lang.String _dsDetalheModalidadeDoc;

    /**
     * Field _dsDetalheModalidadeTed
     */
    private java.lang.String _dsDetalheModalidadeTed;

    /**
     * Field _dsDetalheModalidadeOp
     */
    private java.lang.String _dsDetalheModalidadeOp;

    /**
     * Field _cdIndicadorServicoFornecedor
     */
    private java.lang.String _cdIndicadorServicoFornecedor;

    /**
     * Field _cdProdutoServicoFornecedor
     */
    private int _cdProdutoServicoFornecedor = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoFornecedor
     */
    private boolean _has_cdProdutoServicoFornecedor;

    /**
     * Field _dsProdutoServicoFornecedor
     */
    private java.lang.String _dsProdutoServicoFornecedor;

    /**
     * Field _cdFloatServicoAplicacaoFloatingFornecedor
     */
    private int _cdFloatServicoAplicacaoFloatingFornecedor = 0;

    /**
     * keeps track of state for field:
     * _cdFloatServicoAplicacaoFloatingFornecedor
     */
    private boolean _has_cdFloatServicoAplicacaoFloatingFornecedor;

    /**
     * Field _dsFloatServicoAplicacaoFloatingFornecedor
     */
    private java.lang.String _dsFloatServicoAplicacaoFloatingFornecedor;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;

    /**
     * Field _cdIndicadorServicoTributos
     */
    private java.lang.String _cdIndicadorServicoTributos;

    /**
     * Field _cdProdutoServicoTributos
     */
    private int _cdProdutoServicoTributos = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoTributos
     */
    private boolean _has_cdProdutoServicoTributos;

    /**
     * Field _dsProdutoServicoTributos
     */
    private java.lang.String _dsProdutoServicoTributos;

    /**
     * Field _ocorrencias1List
     */
    private java.util.Vector _ocorrencias1List;

    /**
     * Field _cdIndicadorServicoSalario
     */
    private java.lang.String _cdIndicadorServicoSalario;

    /**
     * Field _cdProdutoServicoSalario
     */
    private int _cdProdutoServicoSalario = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoSalario
     */
    private boolean _has_cdProdutoServicoSalario;

    /**
     * Field _dsProdutoServicoSalario
     */
    private java.lang.String _dsProdutoServicoSalario;

    /**
     * Field _cdIndicadorComprovanteSalarial
     */
    private java.lang.String _cdIndicadorComprovanteSalarial;

    /**
     * Field _cdFloatingServicoAplicacaoFloatSalario
     */
    private int _cdFloatingServicoAplicacaoFloatSalario = 0;

    /**
     * keeps track of state for field:
     * _cdFloatingServicoAplicacaoFloatSalario
     */
    private boolean _has_cdFloatingServicoAplicacaoFloatSalario;

    /**
     * Field _dsFloatingServicoAplicacaoFloatSalario
     */
    private java.lang.String _dsFloatingServicoAplicacaoFloatSalario;

    /**
     * Field _ocorrencias2List
     */
    private java.util.Vector _ocorrencias2List;

    /**
     * Field _ocorrencias3List
     */
    private java.util.Vector _ocorrencias3List;

    /**
     * Field _ocorrencias4List
     */
    private java.util.Vector _ocorrencias4List;

    /**
     * Field _ocorrencias5List
     */
    private java.util.Vector _ocorrencias5List;

    /**
     * Field _ocorrencias6List
     */
    private java.util.Vector _ocorrencias6List;

    /**
     * Field _ocorrencias7List
     */
    private java.util.Vector _ocorrencias7List;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImprimirContratoIntranetResponse() 
     {
        super();
        _ocorrenciasList = new Vector();
        _ocorrencias1List = new Vector();
        _ocorrencias2List = new Vector();
        _ocorrencias3List = new Vector();
        _ocorrencias4List = new Vector();
        _ocorrencias5List = new Vector();
        _ocorrencias6List = new Vector();
        _ocorrencias7List = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.ImprimirContratoIntranetResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 7)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 7");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 7)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 7");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param vOcorrencias1
     */
    public void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias1List.size() < 4)) {
            throw new IndexOutOfBoundsException("addOcorrencias1 has a maximum of 4");
        }
        _ocorrencias1List.addElement(vOcorrencias1);
    } //-- void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void addOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias1List.size() < 4)) {
            throw new IndexOutOfBoundsException("addOcorrencias1 has a maximum of 4");
        }
        _ocorrencias1List.insertElementAt(vOcorrencias1, index);
    } //-- void addOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param vOcorrencias2
     */
    public void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias2List.size() < 4)) {
            throw new IndexOutOfBoundsException("addOcorrencias2 has a maximum of 4");
        }
        _ocorrencias2List.addElement(vOcorrencias2);
    } //-- void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void addOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias2List.size() < 4)) {
            throw new IndexOutOfBoundsException("addOcorrencias2 has a maximum of 4");
        }
        _ocorrencias2List.insertElementAt(vOcorrencias2, index);
    } //-- void addOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2) 

    /**
     * Method addOcorrencias3
     * 
     * 
     * 
     * @param vOcorrencias3
     */
    public void addOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias3List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias3 has a maximum of 20");
        }
        _ocorrencias3List.addElement(vOcorrencias3);
    } //-- void addOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3) 

    /**
     * Method addOcorrencias3
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias3
     */
    public void addOcorrencias3(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias3List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias3 has a maximum of 20");
        }
        _ocorrencias3List.insertElementAt(vOcorrencias3, index);
    } //-- void addOcorrencias3(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3) 

    /**
     * Method addOcorrencias4
     * 
     * 
     * 
     * @param vOcorrencias4
     */
    public void addOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias4List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias4 has a maximum of 20");
        }
        _ocorrencias4List.addElement(vOcorrencias4);
    } //-- void addOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4) 

    /**
     * Method addOcorrencias4
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias4
     */
    public void addOcorrencias4(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias4List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias4 has a maximum of 20");
        }
        _ocorrencias4List.insertElementAt(vOcorrencias4, index);
    } //-- void addOcorrencias4(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4) 

    /**
     * Method addOcorrencias5
     * 
     * 
     * 
     * @param vOcorrencias5
     */
    public void addOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias5List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias5 has a maximum of 20");
        }
        _ocorrencias5List.addElement(vOcorrencias5);
    } //-- void addOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5) 

    /**
     * Method addOcorrencias5
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias5
     */
    public void addOcorrencias5(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias5List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias5 has a maximum of 20");
        }
        _ocorrencias5List.insertElementAt(vOcorrencias5, index);
    } //-- void addOcorrencias5(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5) 

    /**
     * Method addOcorrencias6
     * 
     * 
     * 
     * @param vOcorrencias6
     */
    public void addOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias6List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias6 has a maximum of 20");
        }
        _ocorrencias6List.addElement(vOcorrencias6);
    } //-- void addOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6) 

    /**
     * Method addOcorrencias6
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias6
     */
    public void addOcorrencias6(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias6List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias6 has a maximum of 20");
        }
        _ocorrencias6List.insertElementAt(vOcorrencias6, index);
    } //-- void addOcorrencias6(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6) 

    /**
     * Method addOcorrencias7
     * 
     * 
     * 
     * @param vOcorrencias7
     */
    public void addOcorrencias7(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 vOcorrencias7)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias7List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias7 has a maximum of 20");
        }
        _ocorrencias7List.addElement(vOcorrencias7);
    } //-- void addOcorrencias7(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7) 

    /**
     * Method addOcorrencias7
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias7
     */
    public void addOcorrencias7(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 vOcorrencias7)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias7List.size() < 20)) {
            throw new IndexOutOfBoundsException("addOcorrencias7 has a maximum of 20");
        }
        _ocorrencias7List.insertElementAt(vOcorrencias7, index);
    } //-- void addOcorrencias7(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7) 

    /**
     * Method deleteCdAgenciaGestora
     * 
     */
    public void deleteCdAgenciaGestora()
    {
        this._has_cdAgenciaGestora= false;
    } //-- void deleteCdAgenciaGestora() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdCepComplementoPagador
     * 
     */
    public void deleteCdCepComplementoPagador()
    {
        this._has_cdCepComplementoPagador= false;
    } //-- void deleteCdCepComplementoPagador() 

    /**
     * Method deleteCdCepPagador
     * 
     */
    public void deleteCdCepPagador()
    {
        this._has_cdCepPagador= false;
    } //-- void deleteCdCepPagador() 

    /**
     * Method deleteCdFloatServicoAplicacaoFloatingFornecedor
     * 
     */
    public void deleteCdFloatServicoAplicacaoFloatingFornecedor()
    {
        this._has_cdFloatServicoAplicacaoFloatingFornecedor= false;
    } //-- void deleteCdFloatServicoAplicacaoFloatingFornecedor() 

    /**
     * Method deleteCdFloatingServicoAplicacaoFloatSalario
     * 
     */
    public void deleteCdFloatingServicoAplicacaoFloatSalario()
    {
        this._has_cdFloatingServicoAplicacaoFloatSalario= false;
    } //-- void deleteCdFloatingServicoAplicacaoFloatSalario() 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdProdutoServicoFornecedor
     * 
     */
    public void deleteCdProdutoServicoFornecedor()
    {
        this._has_cdProdutoServicoFornecedor= false;
    } //-- void deleteCdProdutoServicoFornecedor() 

    /**
     * Method deleteCdProdutoServicoSalario
     * 
     */
    public void deleteCdProdutoServicoSalario()
    {
        this._has_cdProdutoServicoSalario= false;
    } //-- void deleteCdProdutoServicoSalario() 

    /**
     * Method deleteCdProdutoServicoTributos
     * 
     */
    public void deleteCdProdutoServicoTributos()
    {
        this._has_cdProdutoServicoTributos= false;
    } //-- void deleteCdProdutoServicoTributos() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Method enumerateOcorrencias1
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias1()
    {
        return _ocorrencias1List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias1() 

    /**
     * Method enumerateOcorrencias2
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias2()
    {
        return _ocorrencias2List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias2() 

    /**
     * Method enumerateOcorrencias3
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias3()
    {
        return _ocorrencias3List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias3() 

    /**
     * Method enumerateOcorrencias4
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias4()
    {
        return _ocorrencias4List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias4() 

    /**
     * Method enumerateOcorrencias5
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias5()
    {
        return _ocorrencias5List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias5() 

    /**
     * Method enumerateOcorrencias6
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias6()
    {
        return _ocorrencias6List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias6() 

    /**
     * Method enumerateOcorrencias7
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias7()
    {
        return _ocorrencias7List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias7() 

    /**
     * Returns the value of field 'cdAgenciaGestora'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaGestora'.
     */
    public int getCdAgenciaGestora()
    {
        return this._cdAgenciaGestora;
    } //-- int getCdAgenciaGestora() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdCepComplementoPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepComplementoPagador'.
     */
    public int getCdCepComplementoPagador()
    {
        return this._cdCepComplementoPagador;
    } //-- int getCdCepComplementoPagador() 

    /**
     * Returns the value of field 'cdCepPagador'.
     * 
     * @return int
     * @return the value of field 'cdCepPagador'.
     */
    public int getCdCepPagador()
    {
        return this._cdCepPagador;
    } //-- int getCdCepPagador() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field
     * 'cdFloatServicoAplicacaoFloatingFornecedor'.
     * 
     * @return int
     * @return the value of field
     * 'cdFloatServicoAplicacaoFloatingFornecedor'.
     */
    public int getCdFloatServicoAplicacaoFloatingFornecedor()
    {
        return this._cdFloatServicoAplicacaoFloatingFornecedor;
    } //-- int getCdFloatServicoAplicacaoFloatingFornecedor() 

    /**
     * Returns the value of field
     * 'cdFloatingServicoAplicacaoFloatSalario'.
     * 
     * @return int
     * @return the value of field
     * 'cdFloatingServicoAplicacaoFloatSalario'.
     */
    public int getCdFloatingServicoAplicacaoFloatSalario()
    {
        return this._cdFloatingServicoAplicacaoFloatSalario;
    } //-- int getCdFloatingServicoAplicacaoFloatSalario() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdIndicadorComprovanteSalarial'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorComprovanteSalarial'.
     */
    public java.lang.String getCdIndicadorComprovanteSalarial()
    {
        return this._cdIndicadorComprovanteSalarial;
    } //-- java.lang.String getCdIndicadorComprovanteSalarial() 

    /**
     * Returns the value of field 'cdIndicadorServicoFornecedor'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorServicoFornecedor'.
     */
    public java.lang.String getCdIndicadorServicoFornecedor()
    {
        return this._cdIndicadorServicoFornecedor;
    } //-- java.lang.String getCdIndicadorServicoFornecedor() 

    /**
     * Returns the value of field 'cdIndicadorServicoSalario'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorServicoSalario'.
     */
    public java.lang.String getCdIndicadorServicoSalario()
    {
        return this._cdIndicadorServicoSalario;
    } //-- java.lang.String getCdIndicadorServicoSalario() 

    /**
     * Returns the value of field 'cdIndicadorServicoTributos'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorServicoTributos'.
     */
    public java.lang.String getCdIndicadorServicoTributos()
    {
        return this._cdIndicadorServicoTributos;
    } //-- java.lang.String getCdIndicadorServicoTributos() 

    /**
     * Returns the value of field 'cdProdutoServicoFornecedor'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoFornecedor'.
     */
    public int getCdProdutoServicoFornecedor()
    {
        return this._cdProdutoServicoFornecedor;
    } //-- int getCdProdutoServicoFornecedor() 

    /**
     * Returns the value of field 'cdProdutoServicoSalario'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoSalario'.
     */
    public int getCdProdutoServicoSalario()
    {
        return this._cdProdutoServicoSalario;
    } //-- int getCdProdutoServicoSalario() 

    /**
     * Returns the value of field 'cdProdutoServicoTributos'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoTributos'.
     */
    public int getCdProdutoServicoTributos()
    {
        return this._cdProdutoServicoTributos;
    } //-- int getCdProdutoServicoTributos() 

    /**
     * Returns the value of field 'cdSiglaUfPagador'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUfPagador'.
     */
    public java.lang.String getCdSiglaUfPagador()
    {
        return this._cdSiglaUfPagador;
    } //-- java.lang.String getCdSiglaUfPagador() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTransferenciaContaSalarial'.
     * 
     * @return String
     * @return the value of field 'cdTransferenciaContaSalarial'.
     */
    public java.lang.String getCdTransferenciaContaSalarial()
    {
        return this._cdTransferenciaContaSalarial;
    } //-- java.lang.String getCdTransferenciaContaSalarial() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaGestora'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaGestora'.
     */
    public java.lang.String getDsAgenciaGestora()
    {
        return this._dsAgenciaGestora;
    } //-- java.lang.String getDsAgenciaGestora() 

    /**
     * Returns the value of field 'dsBairroClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsBairroClientePagador'.
     */
    public java.lang.String getDsBairroClientePagador()
    {
        return this._dsBairroClientePagador;
    } //-- java.lang.String getDsBairroClientePagador() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsComplementoLogradouroPagador'.
     */
    public java.lang.String getDsComplementoLogradouroPagador()
    {
        return this._dsComplementoLogradouroPagador;
    } //-- java.lang.String getDsComplementoLogradouroPagador() 

    /**
     * Returns the value of field 'dsDetalheModalidadeDoc'.
     * 
     * @return String
     * @return the value of field 'dsDetalheModalidadeDoc'.
     */
    public java.lang.String getDsDetalheModalidadeDoc()
    {
        return this._dsDetalheModalidadeDoc;
    } //-- java.lang.String getDsDetalheModalidadeDoc() 

    /**
     * Returns the value of field 'dsDetalheModalidadeOp'.
     * 
     * @return String
     * @return the value of field 'dsDetalheModalidadeOp'.
     */
    public java.lang.String getDsDetalheModalidadeOp()
    {
        return this._dsDetalheModalidadeOp;
    } //-- java.lang.String getDsDetalheModalidadeOp() 

    /**
     * Returns the value of field 'dsDetalheModalidadeTed'.
     * 
     * @return String
     * @return the value of field 'dsDetalheModalidadeTed'.
     */
    public java.lang.String getDsDetalheModalidadeTed()
    {
        return this._dsDetalheModalidadeTed;
    } //-- java.lang.String getDsDetalheModalidadeTed() 

    /**
     * Returns the value of field
     * 'dsDisponibilizacaoComprovanteBanco'.
     * 
     * @return String
     * @return the value of field
     * 'dsDisponibilizacaoComprovanteBanco'.
     */
    public java.lang.String getDsDisponibilizacaoComprovanteBanco()
    {
        return this._dsDisponibilizacaoComprovanteBanco;
    } //-- java.lang.String getDsDisponibilizacaoComprovanteBanco() 

    /**
     * Returns the value of field
     * 'dsFloatServicoAplicacaoFloatingFornecedor'.
     * 
     * @return String
     * @return the value of field
     * 'dsFloatServicoAplicacaoFloatingFornecedor'.
     */
    public java.lang.String getDsFloatServicoAplicacaoFloatingFornecedor()
    {
        return this._dsFloatServicoAplicacaoFloatingFornecedor;
    } //-- java.lang.String getDsFloatServicoAplicacaoFloatingFornecedor() 

    /**
     * Returns the value of field
     * 'dsFloatingServicoAplicacaoFloatSalario'.
     * 
     * @return String
     * @return the value of field
     * 'dsFloatingServicoAplicacaoFloatSalario'.
     */
    public java.lang.String getDsFloatingServicoAplicacaoFloatSalario()
    {
        return this._dsFloatingServicoAplicacaoFloatSalario;
    } //-- java.lang.String getDsFloatingServicoAplicacaoFloatSalario() 

    /**
     * Returns the value of field 'dsFrasePadraoCaput'.
     * 
     * @return String
     * @return the value of field 'dsFrasePadraoCaput'.
     */
    public java.lang.String getDsFrasePadraoCaput()
    {
        return this._dsFrasePadraoCaput;
    } //-- java.lang.String getDsFrasePadraoCaput() 

    /**
     * Returns the value of field 'dsLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroPagador'.
     */
    public java.lang.String getDsLogradouroPagador()
    {
        return this._dsLogradouroPagador;
    } //-- java.lang.String getDsLogradouroPagador() 

    /**
     * Returns the value of field 'dsMunicipioAgenciaGestora'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioAgenciaGestora'.
     */
    public java.lang.String getDsMunicipioAgenciaGestora()
    {
        return this._dsMunicipioAgenciaGestora;
    } //-- java.lang.String getDsMunicipioAgenciaGestora() 

    /**
     * Returns the value of field 'dsMunicipioClientePagador'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioClientePagador'.
     */
    public java.lang.String getDsMunicipioClientePagador()
    {
        return this._dsMunicipioClientePagador;
    } //-- java.lang.String getDsMunicipioClientePagador() 

    /**
     * Returns the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @return String
     * @return the value of field 'dsNumeroLogradouroPagador'.
     */
    public java.lang.String getDsNumeroLogradouroPagador()
    {
        return this._dsNumeroLogradouroPagador;
    } //-- java.lang.String getDsNumeroLogradouroPagador() 

    /**
     * Returns the value of field 'dsProdutoServicoFornecedor'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoFornecedor'.
     */
    public java.lang.String getDsProdutoServicoFornecedor()
    {
        return this._dsProdutoServicoFornecedor;
    } //-- java.lang.String getDsProdutoServicoFornecedor() 

    /**
     * Returns the value of field 'dsProdutoServicoSalario'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoSalario'.
     */
    public java.lang.String getDsProdutoServicoSalario()
    {
        return this._dsProdutoServicoSalario;
    } //-- java.lang.String getDsProdutoServicoSalario() 

    /**
     * Returns the value of field 'dsProdutoServicoTributos'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoTributos'.
     */
    public java.lang.String getDsProdutoServicoTributos()
    {
        return this._dsProdutoServicoTributos;
    } //-- java.lang.String getDsProdutoServicoTributos() 

    /**
     * Returns the value of field 'dsSiglaUfParticipante'.
     * 
     * @return String
     * @return the value of field 'dsSiglaUfParticipante'.
     */
    public java.lang.String getDsSiglaUfParticipante()
    {
        return this._dsSiglaUfParticipante;
    } //-- java.lang.String getDsSiglaUfParticipante() 

    /**
     * Returns the value of field 'dtGeracaoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dtGeracaoContratoNegocio'.
     */
    public java.lang.String getDtGeracaoContratoNegocio()
    {
        return this._dtGeracaoContratoNegocio;
    } //-- java.lang.String getDtGeracaoContratoNegocio() 

    /**
     * Returns the value of field 'hrCicloEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrCicloEfetivacaoPagamento'.
     */
    public java.lang.String getHrCicloEfetivacaoPagamento()
    {
        return this._hrCicloEfetivacaoPagamento;
    } //-- java.lang.String getHrCicloEfetivacaoPagamento() 

    /**
     * Returns the value of field 'hrLimiteApresentaCarta'.
     * 
     * @return String
     * @return the value of field 'hrLimiteApresentaCarta'.
     */
    public java.lang.String getHrLimiteApresentaCarta()
    {
        return this._hrLimiteApresentaCarta;
    } //-- java.lang.String getHrLimiteApresentaCarta() 

    /**
     * Returns the value of field 'hrLimiteConsultaPagamento'.
     * 
     * @return String
     * @return the value of field 'hrLimiteConsultaPagamento'.
     */
    public java.lang.String getHrLimiteConsultaPagamento()
    {
        return this._hrLimiteConsultaPagamento;
    } //-- java.lang.String getHrLimiteConsultaPagamento() 

    /**
     * Returns the value of field 'hrLimiteEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrLimiteEfetivacaoPagamento'.
     */
    public java.lang.String getHrLimiteEfetivacaoPagamento()
    {
        return this._hrLimiteEfetivacaoPagamento;
    } //-- java.lang.String getHrLimiteEfetivacaoPagamento() 

    /**
     * Returns the value of field 'hrLimiteInferiorBoleto'.
     * 
     * @return String
     * @return the value of field 'hrLimiteInferiorBoleto'.
     */
    public java.lang.String getHrLimiteInferiorBoleto()
    {
        return this._hrLimiteInferiorBoleto;
    } //-- java.lang.String getHrLimiteInferiorBoleto() 

    /**
     * Returns the value of field 'hrLimitePagamentoInferior'.
     * 
     * @return String
     * @return the value of field 'hrLimitePagamentoInferior'.
     */
    public java.lang.String getHrLimitePagamentoInferior()
    {
        return this._hrLimitePagamentoInferior;
    } //-- java.lang.String getHrLimitePagamentoInferior() 

    /**
     * Returns the value of field 'hrLimitePagamentoRemessa'.
     * 
     * @return String
     * @return the value of field 'hrLimitePagamentoRemessa'.
     */
    public java.lang.String getHrLimitePagamentoRemessa()
    {
        return this._hrLimitePagamentoRemessa;
    } //-- java.lang.String getHrLimitePagamentoRemessa() 

    /**
     * Returns the value of field 'hrLimitePagamentoSuperior'.
     * 
     * @return String
     * @return the value of field 'hrLimitePagamentoSuperior'.
     */
    public java.lang.String getHrLimitePagamentoSuperior()
    {
        return this._hrLimitePagamentoSuperior;
    } //-- java.lang.String getHrLimitePagamentoSuperior() 

    /**
     * Returns the value of field 'hrLimiteSuperiorBoleto'.
     * 
     * @return String
     * @return the value of field 'hrLimiteSuperiorBoleto'.
     */
    public java.lang.String getHrLimiteSuperiorBoleto()
    {
        return this._hrLimiteSuperiorBoleto;
    } //-- java.lang.String getHrLimiteSuperiorBoleto() 

    /**
     * Returns the value of field 'hrLimiteTransmissaoArquivo'.
     * 
     * @return String
     * @return the value of field 'hrLimiteTransmissaoArquivo'.
     */
    public java.lang.String getHrLimiteTransmissaoArquivo()
    {
        return this._hrLimiteTransmissaoArquivo;
    } //-- java.lang.String getHrLimiteTransmissaoArquivo() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'nrMontanteBoleto'.
     * 
     * @return String
     * @return the value of field 'nrMontanteBoleto'.
     */
    public java.lang.String getNrMontanteBoleto()
    {
        return this._nrMontanteBoleto;
    } //-- java.lang.String getNrMontanteBoleto() 

    /**
     * Returns the value of field
     * 'nrQuantidadeLimiteCobrancaImpressao'.
     * 
     * @return String
     * @return the value of field
     * 'nrQuantidadeLimiteCobrancaImpressao'.
     */
    public java.lang.String getNrQuantidadeLimiteCobrancaImpressao()
    {
        return this._nrQuantidadeLimiteCobrancaImpressao;
    } //-- java.lang.String getNrQuantidadeLimiteCobrancaImpressao() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 getOcorrencias1(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias1: Index value '"+index+"' not in range [0.."+(_ocorrencias1List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 getOcorrencias1(int) 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1[] getOcorrencias1()
    {
        int size = _ocorrencias1List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1[] getOcorrencias1() 

    /**
     * Method getOcorrencias1Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias1Count()
    {
        return _ocorrencias1List.size();
    } //-- int getOcorrencias1Count() 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 getOcorrencias2(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias2: Index value '"+index+"' not in range [0.."+(_ocorrencias2List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 getOcorrencias2(int) 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2[] getOcorrencias2()
    {
        int size = _ocorrencias2List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2[] getOcorrencias2() 

    /**
     * Method getOcorrencias2Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias2Count()
    {
        return _ocorrencias2List.size();
    } //-- int getOcorrencias2Count() 

    /**
     * Method getOcorrencias3
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3 getOcorrencias3(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias3List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias3: Index value '"+index+"' not in range [0.."+(_ocorrencias3List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3) _ocorrencias3List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3 getOcorrencias3(int) 

    /**
     * Method getOcorrencias3
     * 
     * 
     * 
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3[] getOcorrencias3()
    {
        int size = _ocorrencias3List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3) _ocorrencias3List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3[] getOcorrencias3() 

    /**
     * Method getOcorrencias3Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias3Count()
    {
        return _ocorrencias3List.size();
    } //-- int getOcorrencias3Count() 

    /**
     * Method getOcorrencias4
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 getOcorrencias4(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias4List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias4: Index value '"+index+"' not in range [0.."+(_ocorrencias4List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4) _ocorrencias4List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 getOcorrencias4(int) 

    /**
     * Method getOcorrencias4
     * 
     * 
     * 
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4[] getOcorrencias4()
    {
        int size = _ocorrencias4List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4) _ocorrencias4List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4[] getOcorrencias4() 

    /**
     * Method getOcorrencias4Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias4Count()
    {
        return _ocorrencias4List.size();
    } //-- int getOcorrencias4Count() 

    /**
     * Method getOcorrencias5
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 getOcorrencias5(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias5List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias5: Index value '"+index+"' not in range [0.."+(_ocorrencias5List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5) _ocorrencias5List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 getOcorrencias5(int) 

    /**
     * Method getOcorrencias5
     * 
     * 
     * 
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5[] getOcorrencias5()
    {
        int size = _ocorrencias5List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5) _ocorrencias5List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5[] getOcorrencias5() 

    /**
     * Method getOcorrencias5Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias5Count()
    {
        return _ocorrencias5List.size();
    } //-- int getOcorrencias5Count() 

    /**
     * Method getOcorrencias6
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 getOcorrencias6(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias6List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias6: Index value '"+index+"' not in range [0.."+(_ocorrencias6List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6) _ocorrencias6List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 getOcorrencias6(int) 

    /**
     * Method getOcorrencias6
     * 
     * 
     * 
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6[] getOcorrencias6()
    {
        int size = _ocorrencias6List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6) _ocorrencias6List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6[] getOcorrencias6() 

    /**
     * Method getOcorrencias6Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias6Count()
    {
        return _ocorrencias6List.size();
    } //-- int getOcorrencias6Count() 

    /**
     * Method getOcorrencias7
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias7
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 getOcorrencias7(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias7List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias7: Index value '"+index+"' not in range [0.."+(_ocorrencias7List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7) _ocorrencias7List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 getOcorrencias7(int) 

    /**
     * Method getOcorrencias7
     * 
     * 
     * 
     * @return Ocorrencias7
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7[] getOcorrencias7()
    {
        int size = _ocorrencias7List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7) _ocorrencias7List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7[] getOcorrencias7() 

    /**
     * Method getOcorrencias7Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias7Count()
    {
        return _ocorrencias7List.size();
    } //-- int getOcorrencias7Count() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdAgenciaGestora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaGestora()
    {
        return this._has_cdAgenciaGestora;
    } //-- boolean hasCdAgenciaGestora() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdCepComplementoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplementoPagador()
    {
        return this._has_cdCepComplementoPagador;
    } //-- boolean hasCdCepComplementoPagador() 

    /**
     * Method hasCdCepPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepPagador()
    {
        return this._has_cdCepPagador;
    } //-- boolean hasCdCepPagador() 

    /**
     * Method hasCdFloatServicoAplicacaoFloatingFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFloatServicoAplicacaoFloatingFornecedor()
    {
        return this._has_cdFloatServicoAplicacaoFloatingFornecedor;
    } //-- boolean hasCdFloatServicoAplicacaoFloatingFornecedor() 

    /**
     * Method hasCdFloatingServicoAplicacaoFloatSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFloatingServicoAplicacaoFloatSalario()
    {
        return this._has_cdFloatingServicoAplicacaoFloatSalario;
    } //-- boolean hasCdFloatingServicoAplicacaoFloatSalario() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdProdutoServicoFornecedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoFornecedor()
    {
        return this._has_cdProdutoServicoFornecedor;
    } //-- boolean hasCdProdutoServicoFornecedor() 

    /**
     * Method hasCdProdutoServicoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoSalario()
    {
        return this._has_cdProdutoServicoSalario;
    } //-- boolean hasCdProdutoServicoSalario() 

    /**
     * Method hasCdProdutoServicoTributos
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoTributos()
    {
        return this._has_cdProdutoServicoTributos;
    } //-- boolean hasCdProdutoServicoTributos() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeAllOcorrencias1
     * 
     */
    public void removeAllOcorrencias1()
    {
        _ocorrencias1List.removeAllElements();
    } //-- void removeAllOcorrencias1() 

    /**
     * Method removeAllOcorrencias2
     * 
     */
    public void removeAllOcorrencias2()
    {
        _ocorrencias2List.removeAllElements();
    } //-- void removeAllOcorrencias2() 

    /**
     * Method removeAllOcorrencias3
     * 
     */
    public void removeAllOcorrencias3()
    {
        _ocorrencias3List.removeAllElements();
    } //-- void removeAllOcorrencias3() 

    /**
     * Method removeAllOcorrencias4
     * 
     */
    public void removeAllOcorrencias4()
    {
        _ocorrencias4List.removeAllElements();
    } //-- void removeAllOcorrencias4() 

    /**
     * Method removeAllOcorrencias5
     * 
     */
    public void removeAllOcorrencias5()
    {
        _ocorrencias5List.removeAllElements();
    } //-- void removeAllOcorrencias5() 

    /**
     * Method removeAllOcorrencias6
     * 
     */
    public void removeAllOcorrencias6()
    {
        _ocorrencias6List.removeAllElements();
    } //-- void removeAllOcorrencias6() 

    /**
     * Method removeAllOcorrencias7
     * 
     */
    public void removeAllOcorrencias7()
    {
        _ocorrencias7List.removeAllElements();
    } //-- void removeAllOcorrencias7() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Method removeOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 removeOcorrencias1(int index)
    {
        java.lang.Object obj = _ocorrencias1List.elementAt(index);
        _ocorrencias1List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 removeOcorrencias1(int) 

    /**
     * Method removeOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 removeOcorrencias2(int index)
    {
        java.lang.Object obj = _ocorrencias2List.elementAt(index);
        _ocorrencias2List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 removeOcorrencias2(int) 

    /**
     * Method removeOcorrencias3
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3 removeOcorrencias3(int index)
    {
        java.lang.Object obj = _ocorrencias3List.elementAt(index);
        _ocorrencias3List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3 removeOcorrencias3(int) 

    /**
     * Method removeOcorrencias4
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 removeOcorrencias4(int index)
    {
        java.lang.Object obj = _ocorrencias4List.elementAt(index);
        _ocorrencias4List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 removeOcorrencias4(int) 

    /**
     * Method removeOcorrencias5
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 removeOcorrencias5(int index)
    {
        java.lang.Object obj = _ocorrencias5List.elementAt(index);
        _ocorrencias5List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 removeOcorrencias5(int) 

    /**
     * Method removeOcorrencias6
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 removeOcorrencias6(int index)
    {
        java.lang.Object obj = _ocorrencias6List.elementAt(index);
        _ocorrencias6List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 removeOcorrencias6(int) 

    /**
     * Method removeOcorrencias7
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias7
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 removeOcorrencias7(int index)
    {
        java.lang.Object obj = _ocorrencias7List.elementAt(index);
        _ocorrencias7List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 removeOcorrencias7(int) 

    /**
     * Sets the value of field 'cdAgenciaGestora'.
     * 
     * @param cdAgenciaGestora the value of field 'cdAgenciaGestora'
     */
    public void setCdAgenciaGestora(int cdAgenciaGestora)
    {
        this._cdAgenciaGestora = cdAgenciaGestora;
        this._has_cdAgenciaGestora = true;
    } //-- void setCdAgenciaGestora(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdCepComplementoPagador'.
     * 
     * @param cdCepComplementoPagador the value of field
     * 'cdCepComplementoPagador'.
     */
    public void setCdCepComplementoPagador(int cdCepComplementoPagador)
    {
        this._cdCepComplementoPagador = cdCepComplementoPagador;
        this._has_cdCepComplementoPagador = true;
    } //-- void setCdCepComplementoPagador(int) 

    /**
     * Sets the value of field 'cdCepPagador'.
     * 
     * @param cdCepPagador the value of field 'cdCepPagador'.
     */
    public void setCdCepPagador(int cdCepPagador)
    {
        this._cdCepPagador = cdCepPagador;
        this._has_cdCepPagador = true;
    } //-- void setCdCepPagador(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdFloatServicoAplicacaoFloatingFornecedor'.
     * 
     * @param cdFloatServicoAplicacaoFloatingFornecedor the value
     * of field 'cdFloatServicoAplicacaoFloatingFornecedor'.
     */
    public void setCdFloatServicoAplicacaoFloatingFornecedor(int cdFloatServicoAplicacaoFloatingFornecedor)
    {
        this._cdFloatServicoAplicacaoFloatingFornecedor = cdFloatServicoAplicacaoFloatingFornecedor;
        this._has_cdFloatServicoAplicacaoFloatingFornecedor = true;
    } //-- void setCdFloatServicoAplicacaoFloatingFornecedor(int) 

    /**
     * Sets the value of field
     * 'cdFloatingServicoAplicacaoFloatSalario'.
     * 
     * @param cdFloatingServicoAplicacaoFloatSalario the value of
     * field 'cdFloatingServicoAplicacaoFloatSalario'.
     */
    public void setCdFloatingServicoAplicacaoFloatSalario(int cdFloatingServicoAplicacaoFloatSalario)
    {
        this._cdFloatingServicoAplicacaoFloatSalario = cdFloatingServicoAplicacaoFloatSalario;
        this._has_cdFloatingServicoAplicacaoFloatSalario = true;
    } //-- void setCdFloatingServicoAplicacaoFloatSalario(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdIndicadorComprovanteSalarial'.
     * 
     * @param cdIndicadorComprovanteSalarial the value of field
     * 'cdIndicadorComprovanteSalarial'.
     */
    public void setCdIndicadorComprovanteSalarial(java.lang.String cdIndicadorComprovanteSalarial)
    {
        this._cdIndicadorComprovanteSalarial = cdIndicadorComprovanteSalarial;
    } //-- void setCdIndicadorComprovanteSalarial(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorServicoFornecedor'.
     * 
     * @param cdIndicadorServicoFornecedor the value of field
     * 'cdIndicadorServicoFornecedor'.
     */
    public void setCdIndicadorServicoFornecedor(java.lang.String cdIndicadorServicoFornecedor)
    {
        this._cdIndicadorServicoFornecedor = cdIndicadorServicoFornecedor;
    } //-- void setCdIndicadorServicoFornecedor(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorServicoSalario'.
     * 
     * @param cdIndicadorServicoSalario the value of field
     * 'cdIndicadorServicoSalario'.
     */
    public void setCdIndicadorServicoSalario(java.lang.String cdIndicadorServicoSalario)
    {
        this._cdIndicadorServicoSalario = cdIndicadorServicoSalario;
    } //-- void setCdIndicadorServicoSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorServicoTributos'.
     * 
     * @param cdIndicadorServicoTributos the value of field
     * 'cdIndicadorServicoTributos'.
     */
    public void setCdIndicadorServicoTributos(java.lang.String cdIndicadorServicoTributos)
    {
        this._cdIndicadorServicoTributos = cdIndicadorServicoTributos;
    } //-- void setCdIndicadorServicoTributos(java.lang.String) 

    /**
     * Sets the value of field 'cdProdutoServicoFornecedor'.
     * 
     * @param cdProdutoServicoFornecedor the value of field
     * 'cdProdutoServicoFornecedor'.
     */
    public void setCdProdutoServicoFornecedor(int cdProdutoServicoFornecedor)
    {
        this._cdProdutoServicoFornecedor = cdProdutoServicoFornecedor;
        this._has_cdProdutoServicoFornecedor = true;
    } //-- void setCdProdutoServicoFornecedor(int) 

    /**
     * Sets the value of field 'cdProdutoServicoSalario'.
     * 
     * @param cdProdutoServicoSalario the value of field
     * 'cdProdutoServicoSalario'.
     */
    public void setCdProdutoServicoSalario(int cdProdutoServicoSalario)
    {
        this._cdProdutoServicoSalario = cdProdutoServicoSalario;
        this._has_cdProdutoServicoSalario = true;
    } //-- void setCdProdutoServicoSalario(int) 

    /**
     * Sets the value of field 'cdProdutoServicoTributos'.
     * 
     * @param cdProdutoServicoTributos the value of field
     * 'cdProdutoServicoTributos'.
     */
    public void setCdProdutoServicoTributos(int cdProdutoServicoTributos)
    {
        this._cdProdutoServicoTributos = cdProdutoServicoTributos;
        this._has_cdProdutoServicoTributos = true;
    } //-- void setCdProdutoServicoTributos(int) 

    /**
     * Sets the value of field 'cdSiglaUfPagador'.
     * 
     * @param cdSiglaUfPagador the value of field 'cdSiglaUfPagador'
     */
    public void setCdSiglaUfPagador(java.lang.String cdSiglaUfPagador)
    {
        this._cdSiglaUfPagador = cdSiglaUfPagador;
    } //-- void setCdSiglaUfPagador(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTransferenciaContaSalarial'.
     * 
     * @param cdTransferenciaContaSalarial the value of field
     * 'cdTransferenciaContaSalarial'.
     */
    public void setCdTransferenciaContaSalarial(java.lang.String cdTransferenciaContaSalarial)
    {
        this._cdTransferenciaContaSalarial = cdTransferenciaContaSalarial;
    } //-- void setCdTransferenciaContaSalarial(java.lang.String) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaGestora'.
     * 
     * @param dsAgenciaGestora the value of field 'dsAgenciaGestora'
     */
    public void setDsAgenciaGestora(java.lang.String dsAgenciaGestora)
    {
        this._dsAgenciaGestora = dsAgenciaGestora;
    } //-- void setDsAgenciaGestora(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroClientePagador'.
     * 
     * @param dsBairroClientePagador the value of field
     * 'dsBairroClientePagador'.
     */
    public void setDsBairroClientePagador(java.lang.String dsBairroClientePagador)
    {
        this._dsBairroClientePagador = dsBairroClientePagador;
    } //-- void setDsBairroClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsComplementoLogradouroPagador'.
     * 
     * @param dsComplementoLogradouroPagador the value of field
     * 'dsComplementoLogradouroPagador'.
     */
    public void setDsComplementoLogradouroPagador(java.lang.String dsComplementoLogradouroPagador)
    {
        this._dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
    } //-- void setDsComplementoLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsDetalheModalidadeDoc'.
     * 
     * @param dsDetalheModalidadeDoc the value of field
     * 'dsDetalheModalidadeDoc'.
     */
    public void setDsDetalheModalidadeDoc(java.lang.String dsDetalheModalidadeDoc)
    {
        this._dsDetalheModalidadeDoc = dsDetalheModalidadeDoc;
    } //-- void setDsDetalheModalidadeDoc(java.lang.String) 

    /**
     * Sets the value of field 'dsDetalheModalidadeOp'.
     * 
     * @param dsDetalheModalidadeOp the value of field
     * 'dsDetalheModalidadeOp'.
     */
    public void setDsDetalheModalidadeOp(java.lang.String dsDetalheModalidadeOp)
    {
        this._dsDetalheModalidadeOp = dsDetalheModalidadeOp;
    } //-- void setDsDetalheModalidadeOp(java.lang.String) 

    /**
     * Sets the value of field 'dsDetalheModalidadeTed'.
     * 
     * @param dsDetalheModalidadeTed the value of field
     * 'dsDetalheModalidadeTed'.
     */
    public void setDsDetalheModalidadeTed(java.lang.String dsDetalheModalidadeTed)
    {
        this._dsDetalheModalidadeTed = dsDetalheModalidadeTed;
    } //-- void setDsDetalheModalidadeTed(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsDisponibilizacaoComprovanteBanco'.
     * 
     * @param dsDisponibilizacaoComprovanteBanco the value of field
     * 'dsDisponibilizacaoComprovanteBanco'.
     */
    public void setDsDisponibilizacaoComprovanteBanco(java.lang.String dsDisponibilizacaoComprovanteBanco)
    {
        this._dsDisponibilizacaoComprovanteBanco = dsDisponibilizacaoComprovanteBanco;
    } //-- void setDsDisponibilizacaoComprovanteBanco(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsFloatServicoAplicacaoFloatingFornecedor'.
     * 
     * @param dsFloatServicoAplicacaoFloatingFornecedor the value
     * of field 'dsFloatServicoAplicacaoFloatingFornecedor'.
     */
    public void setDsFloatServicoAplicacaoFloatingFornecedor(java.lang.String dsFloatServicoAplicacaoFloatingFornecedor)
    {
        this._dsFloatServicoAplicacaoFloatingFornecedor = dsFloatServicoAplicacaoFloatingFornecedor;
    } //-- void setDsFloatServicoAplicacaoFloatingFornecedor(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsFloatingServicoAplicacaoFloatSalario'.
     * 
     * @param dsFloatingServicoAplicacaoFloatSalario the value of
     * field 'dsFloatingServicoAplicacaoFloatSalario'.
     */
    public void setDsFloatingServicoAplicacaoFloatSalario(java.lang.String dsFloatingServicoAplicacaoFloatSalario)
    {
        this._dsFloatingServicoAplicacaoFloatSalario = dsFloatingServicoAplicacaoFloatSalario;
    } //-- void setDsFloatingServicoAplicacaoFloatSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsFrasePadraoCaput'.
     * 
     * @param dsFrasePadraoCaput the value of field
     * 'dsFrasePadraoCaput'.
     */
    public void setDsFrasePadraoCaput(java.lang.String dsFrasePadraoCaput)
    {
        this._dsFrasePadraoCaput = dsFrasePadraoCaput;
    } //-- void setDsFrasePadraoCaput(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroPagador'.
     * 
     * @param dsLogradouroPagador the value of field
     * 'dsLogradouroPagador'.
     */
    public void setDsLogradouroPagador(java.lang.String dsLogradouroPagador)
    {
        this._dsLogradouroPagador = dsLogradouroPagador;
    } //-- void setDsLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioAgenciaGestora'.
     * 
     * @param dsMunicipioAgenciaGestora the value of field
     * 'dsMunicipioAgenciaGestora'.
     */
    public void setDsMunicipioAgenciaGestora(java.lang.String dsMunicipioAgenciaGestora)
    {
        this._dsMunicipioAgenciaGestora = dsMunicipioAgenciaGestora;
    } //-- void setDsMunicipioAgenciaGestora(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioClientePagador'.
     * 
     * @param dsMunicipioClientePagador the value of field
     * 'dsMunicipioClientePagador'.
     */
    public void setDsMunicipioClientePagador(java.lang.String dsMunicipioClientePagador)
    {
        this._dsMunicipioClientePagador = dsMunicipioClientePagador;
    } //-- void setDsMunicipioClientePagador(java.lang.String) 

    /**
     * Sets the value of field 'dsNumeroLogradouroPagador'.
     * 
     * @param dsNumeroLogradouroPagador the value of field
     * 'dsNumeroLogradouroPagador'.
     */
    public void setDsNumeroLogradouroPagador(java.lang.String dsNumeroLogradouroPagador)
    {
        this._dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
    } //-- void setDsNumeroLogradouroPagador(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoFornecedor'.
     * 
     * @param dsProdutoServicoFornecedor the value of field
     * 'dsProdutoServicoFornecedor'.
     */
    public void setDsProdutoServicoFornecedor(java.lang.String dsProdutoServicoFornecedor)
    {
        this._dsProdutoServicoFornecedor = dsProdutoServicoFornecedor;
    } //-- void setDsProdutoServicoFornecedor(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoSalario'.
     * 
     * @param dsProdutoServicoSalario the value of field
     * 'dsProdutoServicoSalario'.
     */
    public void setDsProdutoServicoSalario(java.lang.String dsProdutoServicoSalario)
    {
        this._dsProdutoServicoSalario = dsProdutoServicoSalario;
    } //-- void setDsProdutoServicoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoTributos'.
     * 
     * @param dsProdutoServicoTributos the value of field
     * 'dsProdutoServicoTributos'.
     */
    public void setDsProdutoServicoTributos(java.lang.String dsProdutoServicoTributos)
    {
        this._dsProdutoServicoTributos = dsProdutoServicoTributos;
    } //-- void setDsProdutoServicoTributos(java.lang.String) 

    /**
     * Sets the value of field 'dsSiglaUfParticipante'.
     * 
     * @param dsSiglaUfParticipante the value of field
     * 'dsSiglaUfParticipante'.
     */
    public void setDsSiglaUfParticipante(java.lang.String dsSiglaUfParticipante)
    {
        this._dsSiglaUfParticipante = dsSiglaUfParticipante;
    } //-- void setDsSiglaUfParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dtGeracaoContratoNegocio'.
     * 
     * @param dtGeracaoContratoNegocio the value of field
     * 'dtGeracaoContratoNegocio'.
     */
    public void setDtGeracaoContratoNegocio(java.lang.String dtGeracaoContratoNegocio)
    {
        this._dtGeracaoContratoNegocio = dtGeracaoContratoNegocio;
    } //-- void setDtGeracaoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'hrCicloEfetivacaoPagamento'.
     * 
     * @param hrCicloEfetivacaoPagamento the value of field
     * 'hrCicloEfetivacaoPagamento'.
     */
    public void setHrCicloEfetivacaoPagamento(java.lang.String hrCicloEfetivacaoPagamento)
    {
        this._hrCicloEfetivacaoPagamento = hrCicloEfetivacaoPagamento;
    } //-- void setHrCicloEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteApresentaCarta'.
     * 
     * @param hrLimiteApresentaCarta the value of field
     * 'hrLimiteApresentaCarta'.
     */
    public void setHrLimiteApresentaCarta(java.lang.String hrLimiteApresentaCarta)
    {
        this._hrLimiteApresentaCarta = hrLimiteApresentaCarta;
    } //-- void setHrLimiteApresentaCarta(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteConsultaPagamento'.
     * 
     * @param hrLimiteConsultaPagamento the value of field
     * 'hrLimiteConsultaPagamento'.
     */
    public void setHrLimiteConsultaPagamento(java.lang.String hrLimiteConsultaPagamento)
    {
        this._hrLimiteConsultaPagamento = hrLimiteConsultaPagamento;
    } //-- void setHrLimiteConsultaPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteEfetivacaoPagamento'.
     * 
     * @param hrLimiteEfetivacaoPagamento the value of field
     * 'hrLimiteEfetivacaoPagamento'.
     */
    public void setHrLimiteEfetivacaoPagamento(java.lang.String hrLimiteEfetivacaoPagamento)
    {
        this._hrLimiteEfetivacaoPagamento = hrLimiteEfetivacaoPagamento;
    } //-- void setHrLimiteEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteInferiorBoleto'.
     * 
     * @param hrLimiteInferiorBoleto the value of field
     * 'hrLimiteInferiorBoleto'.
     */
    public void setHrLimiteInferiorBoleto(java.lang.String hrLimiteInferiorBoleto)
    {
        this._hrLimiteInferiorBoleto = hrLimiteInferiorBoleto;
    } //-- void setHrLimiteInferiorBoleto(java.lang.String) 

    /**
     * Sets the value of field 'hrLimitePagamentoInferior'.
     * 
     * @param hrLimitePagamentoInferior the value of field
     * 'hrLimitePagamentoInferior'.
     */
    public void setHrLimitePagamentoInferior(java.lang.String hrLimitePagamentoInferior)
    {
        this._hrLimitePagamentoInferior = hrLimitePagamentoInferior;
    } //-- void setHrLimitePagamentoInferior(java.lang.String) 

    /**
     * Sets the value of field 'hrLimitePagamentoRemessa'.
     * 
     * @param hrLimitePagamentoRemessa the value of field
     * 'hrLimitePagamentoRemessa'.
     */
    public void setHrLimitePagamentoRemessa(java.lang.String hrLimitePagamentoRemessa)
    {
        this._hrLimitePagamentoRemessa = hrLimitePagamentoRemessa;
    } //-- void setHrLimitePagamentoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'hrLimitePagamentoSuperior'.
     * 
     * @param hrLimitePagamentoSuperior the value of field
     * 'hrLimitePagamentoSuperior'.
     */
    public void setHrLimitePagamentoSuperior(java.lang.String hrLimitePagamentoSuperior)
    {
        this._hrLimitePagamentoSuperior = hrLimitePagamentoSuperior;
    } //-- void setHrLimitePagamentoSuperior(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteSuperiorBoleto'.
     * 
     * @param hrLimiteSuperiorBoleto the value of field
     * 'hrLimiteSuperiorBoleto'.
     */
    public void setHrLimiteSuperiorBoleto(java.lang.String hrLimiteSuperiorBoleto)
    {
        this._hrLimiteSuperiorBoleto = hrLimiteSuperiorBoleto;
    } //-- void setHrLimiteSuperiorBoleto(java.lang.String) 

    /**
     * Sets the value of field 'hrLimiteTransmissaoArquivo'.
     * 
     * @param hrLimiteTransmissaoArquivo the value of field
     * 'hrLimiteTransmissaoArquivo'.
     */
    public void setHrLimiteTransmissaoArquivo(java.lang.String hrLimiteTransmissaoArquivo)
    {
        this._hrLimiteTransmissaoArquivo = hrLimiteTransmissaoArquivo;
    } //-- void setHrLimiteTransmissaoArquivo(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrMontanteBoleto'.
     * 
     * @param nrMontanteBoleto the value of field 'nrMontanteBoleto'
     */
    public void setNrMontanteBoleto(java.lang.String nrMontanteBoleto)
    {
        this._nrMontanteBoleto = nrMontanteBoleto;
    } //-- void setNrMontanteBoleto(java.lang.String) 

    /**
     * Sets the value of field
     * 'nrQuantidadeLimiteCobrancaImpressao'.
     * 
     * @param nrQuantidadeLimiteCobrancaImpressao the value of
     * field 'nrQuantidadeLimiteCobrancaImpressao'.
     */
    public void setNrQuantidadeLimiteCobrancaImpressao(java.lang.String nrQuantidadeLimiteCobrancaImpressao)
    {
        this._nrQuantidadeLimiteCobrancaImpressao = nrQuantidadeLimiteCobrancaImpressao;
    } //-- void setNrQuantidadeLimiteCobrancaImpressao(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 7)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 7");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void setOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias1: Index value '"+index+"' not in range [0.." + (_ocorrencias1List.size() - 1) + "]");
        }
        if (!(index < 4)) {
            throw new IndexOutOfBoundsException("setOcorrencias1 has a maximum of 4");
        }
        _ocorrencias1List.setElementAt(vOcorrencias1, index);
    } //-- void setOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param ocorrencias1Array
     */
    public void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1[] ocorrencias1Array)
    {
        //-- copy array
        _ocorrencias1List.removeAllElements();
        for (int i = 0; i < ocorrencias1Array.length; i++) {
            _ocorrencias1List.addElement(ocorrencias1Array[i]);
        }
    } //-- void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias1) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void setOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias2: Index value '"+index+"' not in range [0.." + (_ocorrencias2List.size() - 1) + "]");
        }
        if (!(index < 4)) {
            throw new IndexOutOfBoundsException("setOcorrencias2 has a maximum of 4");
        }
        _ocorrencias2List.setElementAt(vOcorrencias2, index);
    } //-- void setOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param ocorrencias2Array
     */
    public void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2[] ocorrencias2Array)
    {
        //-- copy array
        _ocorrencias2List.removeAllElements();
        for (int i = 0; i < ocorrencias2Array.length; i++) {
            _ocorrencias2List.addElement(ocorrencias2Array[i]);
        }
    } //-- void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias2) 

    /**
     * Method setOcorrencias3
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias3
     */
    public void setOcorrencias3(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias3List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias3: Index value '"+index+"' not in range [0.." + (_ocorrencias3List.size() - 1) + "]");
        }
        if (!(index < 20)) {
            throw new IndexOutOfBoundsException("setOcorrencias3 has a maximum of 20");
        }
        _ocorrencias3List.setElementAt(vOcorrencias3, index);
    } //-- void setOcorrencias3(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3) 

    /**
     * Method setOcorrencias3
     * 
     * 
     * 
     * @param ocorrencias3Array
     */
    public void setOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3[] ocorrencias3Array)
    {
        //-- copy array
        _ocorrencias3List.removeAllElements();
        for (int i = 0; i < ocorrencias3Array.length; i++) {
            _ocorrencias3List.addElement(ocorrencias3Array[i]);
        }
    } //-- void setOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias3) 

    /**
     * Method setOcorrencias4
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias4
     */
    public void setOcorrencias4(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias4List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias4: Index value '"+index+"' not in range [0.." + (_ocorrencias4List.size() - 1) + "]");
        }
        if (!(index < 20)) {
            throw new IndexOutOfBoundsException("setOcorrencias4 has a maximum of 20");
        }
        _ocorrencias4List.setElementAt(vOcorrencias4, index);
    } //-- void setOcorrencias4(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4) 

    /**
     * Method setOcorrencias4
     * 
     * 
     * 
     * @param ocorrencias4Array
     */
    public void setOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4[] ocorrencias4Array)
    {
        //-- copy array
        _ocorrencias4List.removeAllElements();
        for (int i = 0; i < ocorrencias4Array.length; i++) {
            _ocorrencias4List.addElement(ocorrencias4Array[i]);
        }
    } //-- void setOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4) 

    /**
     * Method setOcorrencias5
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias5
     */
    public void setOcorrencias5(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias5List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias5: Index value '"+index+"' not in range [0.." + (_ocorrencias5List.size() - 1) + "]");
        }
        if (!(index < 20)) {
            throw new IndexOutOfBoundsException("setOcorrencias5 has a maximum of 20");
        }
        _ocorrencias5List.setElementAt(vOcorrencias5, index);
    } //-- void setOcorrencias5(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5) 

    /**
     * Method setOcorrencias5
     * 
     * 
     * 
     * @param ocorrencias5Array
     */
    public void setOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5[] ocorrencias5Array)
    {
        //-- copy array
        _ocorrencias5List.removeAllElements();
        for (int i = 0; i < ocorrencias5Array.length; i++) {
            _ocorrencias5List.addElement(ocorrencias5Array[i]);
        }
    } //-- void setOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias5) 

    /**
     * Method setOcorrencias6
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias6
     */
    public void setOcorrencias6(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias6List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias6: Index value '"+index+"' not in range [0.." + (_ocorrencias6List.size() - 1) + "]");
        }
        if (!(index < 20)) {
            throw new IndexOutOfBoundsException("setOcorrencias6 has a maximum of 20");
        }
        _ocorrencias6List.setElementAt(vOcorrencias6, index);
    } //-- void setOcorrencias6(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6) 

    /**
     * Method setOcorrencias6
     * 
     * 
     * 
     * @param ocorrencias6Array
     */
    public void setOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6[] ocorrencias6Array)
    {
        //-- copy array
        _ocorrencias6List.removeAllElements();
        for (int i = 0; i < ocorrencias6Array.length; i++) {
            _ocorrencias6List.addElement(ocorrencias6Array[i]);
        }
    } //-- void setOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias6) 

    /**
     * Method setOcorrencias7
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias7
     */
    public void setOcorrencias7(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7 vOcorrencias7)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias7List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias7: Index value '"+index+"' not in range [0.." + (_ocorrencias7List.size() - 1) + "]");
        }
        if (!(index < 20)) {
            throw new IndexOutOfBoundsException("setOcorrencias7 has a maximum of 20");
        }
        _ocorrencias7List.setElementAt(vOcorrencias7, index);
    } //-- void setOcorrencias7(int, br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7) 

    /**
     * Method setOcorrencias7
     * 
     * 
     * 
     * @param ocorrencias7Array
     */
    public void setOcorrencias7(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7[] ocorrencias7Array)
    {
        //-- copy array
        _ocorrencias7List.removeAllElements();
        for (int i = 0; i < ocorrencias7Array.length; i++) {
            _ocorrencias7List.addElement(ocorrencias7Array[i]);
        }
    } //-- void setOcorrencias7(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias7) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ImprimirContratoIntranetResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.ImprimirContratoIntranetResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.ImprimirContratoIntranetResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.ImprimirContratoIntranetResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.ImprimirContratoIntranetResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
