/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarFaixaSalarialPropostaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarFaixaSalarialPropostaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdUsuario
     */
    private java.lang.String _cdUsuario;

    /**
     * Field _cdDependencia
     */
    private int _cdDependencia = 0;

    /**
     * keeps track of state for field: _cdDependencia
     */
    private boolean _has_cdDependencia;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarFaixaSalarialPropostaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.request.ListarFaixaSalarialPropostaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdDependencia
     * 
     */
    public void deleteCdDependencia()
    {
        this._has_cdDependencia= false;
    } //-- void deleteCdDependencia() 

    /**
     * Returns the value of field 'cdDependencia'.
     * 
     * @return int
     * @return the value of field 'cdDependencia'.
     */
    public int getCdDependencia()
    {
        return this._cdDependencia;
    } //-- int getCdDependencia() 

    /**
     * Returns the value of field 'cdUsuario'.
     * 
     * @return String
     * @return the value of field 'cdUsuario'.
     */
    public java.lang.String getCdUsuario()
    {
        return this._cdUsuario;
    } //-- java.lang.String getCdUsuario() 

    /**
     * Method hasCdDependencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDependencia()
    {
        return this._has_cdDependencia;
    } //-- boolean hasCdDependencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdDependencia'.
     * 
     * @param cdDependencia the value of field 'cdDependencia'.
     */
    public void setCdDependencia(int cdDependencia)
    {
        this._cdDependencia = cdDependencia;
        this._has_cdDependencia = true;
    } //-- void setCdDependencia(int) 

    /**
     * Sets the value of field 'cdUsuario'.
     * 
     * @param cdUsuario the value of field 'cdUsuario'.
     */
    public void setCdUsuario(java.lang.String cdUsuario)
    {
        this._cdUsuario = cdUsuario;
    } //-- void setCdUsuario(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarFaixaSalarialPropostaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.request.ListarFaixaSalarialPropostaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.request.ListarFaixaSalarialPropostaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.request.ListarFaixaSalarialPropostaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.request.ListarFaixaSalarialPropostaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
