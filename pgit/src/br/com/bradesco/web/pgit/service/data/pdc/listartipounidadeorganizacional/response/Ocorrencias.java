/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoUnidade
     */
    private int _cdTipoUnidade = 0;

    /**
     * keeps track of state for field: _cdTipoUnidade
     */
    private boolean _has_cdTipoUnidade;

    /**
     * Field _dsTipoUnidade
     */
    private java.lang.String _dsTipoUnidade;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoUnidade
     * 
     */
    public void deleteCdTipoUnidade()
    {
        this._has_cdTipoUnidade= false;
    } //-- void deleteCdTipoUnidade() 

    /**
     * Returns the value of field 'cdTipoUnidade'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidade'.
     */
    public int getCdTipoUnidade()
    {
        return this._cdTipoUnidade;
    } //-- int getCdTipoUnidade() 

    /**
     * Returns the value of field 'dsTipoUnidade'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidade'.
     */
    public java.lang.String getDsTipoUnidade()
    {
        return this._dsTipoUnidade;
    } //-- java.lang.String getDsTipoUnidade() 

    /**
     * Method hasCdTipoUnidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidade()
    {
        return this._has_cdTipoUnidade;
    } //-- boolean hasCdTipoUnidade() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoUnidade'.
     * 
     * @param cdTipoUnidade the value of field 'cdTipoUnidade'.
     */
    public void setCdTipoUnidade(int cdTipoUnidade)
    {
        this._cdTipoUnidade = cdTipoUnidade;
        this._has_cdTipoUnidade = true;
    } //-- void setCdTipoUnidade(int) 

    /**
     * Sets the value of field 'dsTipoUnidade'.
     * 
     * @param dsTipoUnidade the value of field 'dsTipoUnidade'.
     */
    public void setDsTipoUnidade(java.lang.String dsTipoUnidade)
    {
        this._dsTipoUnidade = dsTipoUnidade;
    } //-- void setDsTipoUnidade(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipounidadeorganizacional.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
