/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.impl;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.IImprimirFormalizacaoAlteracaoContratoService;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.OcorrenciasContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.OcorrenciasParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.OcorrenciasServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.request.ImprimirAditivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.ImprimirAditivoContratoResponse;
import br.com.bradesco.web.pgit.service.report.mantercontrato.TermoAditivoReport;

import com.lowagie.text.DocumentException;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ImprimirFormalizacaoAlteracaoContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ImprimirFormalizacaoAlteracaoContratoServiceImpl implements IImprimirFormalizacaoAlteracaoContratoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public ImprimirFormalizacaoAlteracaoContratoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.IImprimirFormalizacaoAlteracaoContrato#sampleImprimirFormalizacaoAlteracaoContrato()
     */
    public void sampleImprimirFormalizacaoAlteracaoContrato() {
        // TODO: Implementa�ao
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.IImprimirFormalizacaoAlteracaoContratoService#imprimirAditivoContrato(br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoEntradaDTO)
     */
    public ImprimirAditivoContratoSaidaDTO imprimirAditivoContrato (ImprimirAditivoContratoEntradaDTO imprimirAditivoContratoEntradaDTO) {
    	ImprimirAditivoContratoRequest request = new ImprimirAditivoContratoRequest();
 
    	request.setCdClubPessoa(imprimirAditivoContratoEntradaDTO.getCdClubPessoa());
    	request.setCdControleCpfCnpj(imprimirAditivoContratoEntradaDTO.getCdControleCpfCnpj());
    	request.setCdCorpoCpfCnpj(imprimirAditivoContratoEntradaDTO.getCdCorpoCpfCnpj());
    	request.setCdCpfCnpjParticipante(imprimirAditivoContratoEntradaDTO.getCdCpfCnpjParticipante());
    	request.setCdDigitoCpfCnpj(imprimirAditivoContratoEntradaDTO.getCdDigitoCpfCnpj());
    	request.setCdFuncionarioBradesco(imprimirAditivoContratoEntradaDTO.getCdFuncionarioBradesco());
    	request.setCdPessoaJuridicaContrato(imprimirAditivoContratoEntradaDTO.getCdPessoaJuridicaContrato());
    	request.setCdTipoContratoNegocio(imprimirAditivoContratoEntradaDTO.getCdTipoContratoNegocio());
    	request.setNmEmpresa(imprimirAditivoContratoEntradaDTO.getNmEmpresa());
    	request.setNmParticipante(imprimirAditivoContratoEntradaDTO.getNmParticipante());
    	request.setNrAditivo(imprimirAditivoContratoEntradaDTO.getNrAditivo());
    	request.setNrSequenciaContratoNegocio(imprimirAditivoContratoEntradaDTO.getNrSequenciaContratoNegocio());
    	
       	ImprimirAditivoContratoResponse response = getFactoryAdapter().getImprimirAditivoContratoPDCAdapter().invokeProcess(request);
        	
       	if (response == null) {
       		return new ImprimirAditivoContratoSaidaDTO();
       	}
       	
       	List<String> dsServicoContrato = new ArrayList<String>();
       	List<OcorrenciasParticipanteSaidaDTO> ocorrenciasParticipante = new ArrayList<OcorrenciasParticipanteSaidaDTO>();
       	List<OcorrenciasContaSaidaDTO> ocorrenciasConta = new ArrayList<OcorrenciasContaSaidaDTO>();
       	List<OcorrenciasServicoSaidaDTO> ocorrenciasServico = new ArrayList<OcorrenciasServicoSaidaDTO>();
       	
       	for (int index = 0; index < response.getOcorrenciasServicoCount(); index ++) {
       		dsServicoContrato.add(response.getOcorrenciasServico(index).getDsServicoContrato());
       	}
       	
       	for (int index = 0; index < response.getOcorrenciasParticipanteCount(); index++) {
       		ocorrenciasParticipante.add(new OcorrenciasParticipanteSaidaDTO(response.getOcorrenciasParticipante(index)));
       	}

       	for (int index = 0; index < response.getOcorrenciasServico2Count(); index++) {
       		ocorrenciasServico.add(new OcorrenciasServicoSaidaDTO(response.getOcorrenciasServico2(index)));
       	}       	 
       	
		return new ImprimirAditivoContratoSaidaDTO(response, dsServicoContrato, ocorrenciasParticipante, ocorrenciasConta, ocorrenciasServico);
    }	
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.IImprimirFormalizacaoAlteracaoContratoService#gerarContratoAditivo(br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoSaidaDTO, java.io.OutputStream)
     */
    public void gerarContratoAditivo(ImprimirAditivoContratoSaidaDTO saidaContratoAditivo, OutputStream outputStream) throws DocumentException {
	   new TermoAditivoReport(saidaContratoAditivo).gerarPdf(outputStream);
    }
}

