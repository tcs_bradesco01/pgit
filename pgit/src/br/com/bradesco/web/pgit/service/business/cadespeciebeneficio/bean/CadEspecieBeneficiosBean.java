/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadespeciebeneficio.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadespeciebeneficio.bean;

import br.com.bradesco.web.pgit.service.business.cadespeciebeneficio.ICadEspecieBeneficioService;

/**
 * Nome: CadEspecieBeneficiosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CadEspecieBeneficiosBean {
	
	/** Atributo codigoEspecie. */
	private String codigoEspecie;
	
	/** Atributo manterCadastroEspecieBeneficioImpl. */
	private ICadEspecieBeneficioService manterCadastroEspecieBeneficioImpl;
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo descricao. */
	private String descricao;
	
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Get: codigoEspecie.
	 *
	 * @return codigoEspecie
	 */
	public String getCodigoEspecie() {
		return codigoEspecie;
	}

	/**
	 * Set: codigoEspecie.
	 *
	 * @param codigoEspecie the codigo especie
	 */
	public void setCodigoEspecie(String codigoEspecie) {
		this.codigoEspecie = codigoEspecie;
	}
	
	/**
	 * Get: manterCadastroEspecieBeneficioImpl.
	 *
	 * @return manterCadastroEspecieBeneficioImpl
	 */
	public ICadEspecieBeneficioService getManterCadastroEspecieBeneficioImpl() {
		return manterCadastroEspecieBeneficioImpl;
	}

	/**
	 * Set: manterCadastroEspecieBeneficioImpl.
	 *
	 * @param manterCadastroEspecieBeneficioImpl the manter cadastro especie beneficio impl
	 */
	public void setManterCadastroEspecieBeneficioImpl(
			ICadEspecieBeneficioService manterCadastroEspecieBeneficioImpl) {
		this.manterCadastroEspecieBeneficioImpl = manterCadastroEspecieBeneficioImpl;
	}
	
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {		
		this.setCodigoEspecie("");
		return "ok";		
	}
	
	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {		
		return "ok";		
	}	
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar(){
		return "AVANCAR_ALTERAR";
	}
	
	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir(){
		return "AVANCAR_EXCLUIR";
	}	

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar(){
		return "VOLTAR_CONSULTAR";
	}
	
	/**
	 * Confirmar inclusao.
	 *
	 * @return the string
	 */
	public String confirmarInclusao(){
		return "ok";
	}
	
	/**
	 * Confirmar exclusao.
	 *
	 * @return the string
	 */
	public String confirmarExclusao(){
		return "ok";
	}
	
	/**
	 * Confirmar alteracao.
	 *
	 * @return the string
	 */
	public String confirmarAlteracao(){
		return "ok";
	}


}
