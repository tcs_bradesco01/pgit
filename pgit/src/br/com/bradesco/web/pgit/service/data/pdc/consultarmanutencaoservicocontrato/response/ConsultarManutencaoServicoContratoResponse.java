/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarManutencaoServicoContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarManutencaoServicoContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoManutencaoContrato
     */
    private int _cdTipoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoManutencaoContrato
     */
    private boolean _has_cdTipoManutencaoContrato;

    /**
     * Field _nrManutencaoContratoNegocio
     */
    private long _nrManutencaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrManutencaoContratoNegocio
     */
    private boolean _has_nrManutencaoContratoNegocio;

    /**
     * Field _cdSituacaoManutencaoContrato
     */
    private int _cdSituacaoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoManutencaoContrato
     */
    private boolean _has_cdSituacaoManutencaoContrato;

    /**
     * Field _dsSituacaoManutencaoContrato
     */
    private java.lang.String _dsSituacaoManutencaoContrato;

    /**
     * Field _cdMotivoManutencaoContrato
     */
    private int _cdMotivoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdMotivoManutencaoContrato
     */
    private boolean _has_cdMotivoManutencaoContrato;

    /**
     * Field _dsMotivoManutencaoContrato
     */
    private java.lang.String _dsMotivoManutencaoContrato;

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _cdTipoServico
     */
    private int _cdTipoServico = 0;

    /**
     * keeps track of state for field: _cdTipoServico
     */
    private boolean _has_cdTipoServico;

    /**
     * Field _cdModalidadeServico
     */
    private int _cdModalidadeServico = 0;

    /**
     * keeps track of state for field: _cdModalidadeServico
     */
    private boolean _has_cdModalidadeServico;

    /**
     * Field _dsTipoServico
     */
    private java.lang.String _dsTipoServico;

    /**
     * Field _dsModalidadeServico
     */
    private java.lang.String _dsModalidadeServico;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _nrOperacaoFluxoInclusao
     */
    private java.lang.String _nrOperacaoFluxoInclusao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _nrOperacaoFluxoManutencao
     */
    private java.lang.String _nrOperacaoFluxoManutencao;

    /**
     * Field _cdTipoDataFloat
     */
    private int _cdTipoDataFloat = 0;

    /**
     * keeps track of state for field: _cdTipoDataFloat
     */
    private boolean _has_cdTipoDataFloat;

    /**
     * Field _dsTipoDataFloat
     */
    private java.lang.String _dsTipoDataFloat;

    /**
     * Field _cdFloatServicoContratado
     */
    private int _cdFloatServicoContratado = 0;

    /**
     * keeps track of state for field: _cdFloatServicoContratado
     */
    private boolean _has_cdFloatServicoContratado;

    /**
     * Field _dsFloarServicoContratado
     */
    private java.lang.String _dsFloarServicoContratado;

    /**
     * Field _cdMomentoProcessamentoPagamento
     */
    private int _cdMomentoProcessamentoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoProcessamentoPagamento
     */
    private boolean _has_cdMomentoProcessamentoPagamento;

    /**
     * Field _dsMomentoProcessamentoPagamento
     */
    private java.lang.String _dsMomentoProcessamentoPagamento;

    /**
     * Field _cdPagamentoNaoUtil
     */
    private int _cdPagamentoNaoUtil = 0;

    /**
     * keeps track of state for field: _cdPagamentoNaoUtil
     */
    private boolean _has_cdPagamentoNaoUtil;

    /**
     * Field _dsPagamentoNaoUtil
     */
    private java.lang.String _dsPagamentoNaoUtil;

    /**
     * Field _cdIndicadorFeriadoLocal
     */
    private int _cdIndicadorFeriadoLocal = 0;

    /**
     * keeps track of state for field: _cdIndicadorFeriadoLocal
     */
    private boolean _has_cdIndicadorFeriadoLocal;

    /**
     * Field _dsIndicadorFeriadoLocal
     */
    private java.lang.String _dsIndicadorFeriadoLocal;

    /**
     * Field _cdIndicadorDiaFloat
     */
    private int _cdIndicadorDiaFloat = 0;

    /**
     * keeps track of state for field: _cdIndicadorDiaFloat
     */
    private boolean _has_cdIndicadorDiaFloat;

    /**
     * Field _qtdeDiaFloatPagamento
     */
    private int _qtdeDiaFloatPagamento = 0;

    /**
     * keeps track of state for field: _qtdeDiaFloatPagamento
     */
    private boolean _has_qtdeDiaFloatPagamento;

    /**
     * Field _cdIndicadorDiaRepique
     */
    private int _cdIndicadorDiaRepique = 0;

    /**
     * keeps track of state for field: _cdIndicadorDiaRepique
     */
    private boolean _has_cdIndicadorDiaRepique;

    /**
     * Field _qtdeDiaRepiqueCons
     */
    private int _qtdeDiaRepiqueCons = 0;

    /**
     * keeps track of state for field: _qtdeDiaRepiqueCons
     */
    private boolean _has_qtdeDiaRepiqueCons;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarManutencaoServicoContratoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.response.ConsultarManutencaoServicoContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdFloatServicoContratado
     * 
     */
    public void deleteCdFloatServicoContratado()
    {
        this._has_cdFloatServicoContratado= false;
    } //-- void deleteCdFloatServicoContratado() 

    /**
     * Method deleteCdIndicadorDiaFloat
     * 
     */
    public void deleteCdIndicadorDiaFloat()
    {
        this._has_cdIndicadorDiaFloat= false;
    } //-- void deleteCdIndicadorDiaFloat() 

    /**
     * Method deleteCdIndicadorDiaRepique
     * 
     */
    public void deleteCdIndicadorDiaRepique()
    {
        this._has_cdIndicadorDiaRepique= false;
    } //-- void deleteCdIndicadorDiaRepique() 

    /**
     * Method deleteCdIndicadorFeriadoLocal
     * 
     */
    public void deleteCdIndicadorFeriadoLocal()
    {
        this._has_cdIndicadorFeriadoLocal= false;
    } //-- void deleteCdIndicadorFeriadoLocal() 

    /**
     * Method deleteCdModalidadeServico
     * 
     */
    public void deleteCdModalidadeServico()
    {
        this._has_cdModalidadeServico= false;
    } //-- void deleteCdModalidadeServico() 

    /**
     * Method deleteCdMomentoProcessamentoPagamento
     * 
     */
    public void deleteCdMomentoProcessamentoPagamento()
    {
        this._has_cdMomentoProcessamentoPagamento= false;
    } //-- void deleteCdMomentoProcessamentoPagamento() 

    /**
     * Method deleteCdMotivoManutencaoContrato
     * 
     */
    public void deleteCdMotivoManutencaoContrato()
    {
        this._has_cdMotivoManutencaoContrato= false;
    } //-- void deleteCdMotivoManutencaoContrato() 

    /**
     * Method deleteCdPagamentoNaoUtil
     * 
     */
    public void deleteCdPagamentoNaoUtil()
    {
        this._has_cdPagamentoNaoUtil= false;
    } //-- void deleteCdPagamentoNaoUtil() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoManutencaoContrato
     * 
     */
    public void deleteCdSituacaoManutencaoContrato()
    {
        this._has_cdSituacaoManutencaoContrato= false;
    } //-- void deleteCdSituacaoManutencaoContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoDataFloat
     * 
     */
    public void deleteCdTipoDataFloat()
    {
        this._has_cdTipoDataFloat= false;
    } //-- void deleteCdTipoDataFloat() 

    /**
     * Method deleteCdTipoManutencaoContrato
     * 
     */
    public void deleteCdTipoManutencaoContrato()
    {
        this._has_cdTipoManutencaoContrato= false;
    } //-- void deleteCdTipoManutencaoContrato() 

    /**
     * Method deleteCdTipoServico
     * 
     */
    public void deleteCdTipoServico()
    {
        this._has_cdTipoServico= false;
    } //-- void deleteCdTipoServico() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Method deleteNrManutencaoContratoNegocio
     * 
     */
    public void deleteNrManutencaoContratoNegocio()
    {
        this._has_nrManutencaoContratoNegocio= false;
    } //-- void deleteNrManutencaoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtdeDiaFloatPagamento
     * 
     */
    public void deleteQtdeDiaFloatPagamento()
    {
        this._has_qtdeDiaFloatPagamento= false;
    } //-- void deleteQtdeDiaFloatPagamento() 

    /**
     * Method deleteQtdeDiaRepiqueCons
     * 
     */
    public void deleteQtdeDiaRepiqueCons()
    {
        this._has_qtdeDiaRepiqueCons= false;
    } //-- void deleteQtdeDiaRepiqueCons() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdFloatServicoContratado'.
     * 
     * @return int
     * @return the value of field 'cdFloatServicoContratado'.
     */
    public int getCdFloatServicoContratado()
    {
        return this._cdFloatServicoContratado;
    } //-- int getCdFloatServicoContratado() 

    /**
     * Returns the value of field 'cdIndicadorDiaFloat'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorDiaFloat'.
     */
    public int getCdIndicadorDiaFloat()
    {
        return this._cdIndicadorDiaFloat;
    } //-- int getCdIndicadorDiaFloat() 

    /**
     * Returns the value of field 'cdIndicadorDiaRepique'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorDiaRepique'.
     */
    public int getCdIndicadorDiaRepique()
    {
        return this._cdIndicadorDiaRepique;
    } //-- int getCdIndicadorDiaRepique() 

    /**
     * Returns the value of field 'cdIndicadorFeriadoLocal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFeriadoLocal'.
     */
    public int getCdIndicadorFeriadoLocal()
    {
        return this._cdIndicadorFeriadoLocal;
    } //-- int getCdIndicadorFeriadoLocal() 

    /**
     * Returns the value of field 'cdModalidadeServico'.
     * 
     * @return int
     * @return the value of field 'cdModalidadeServico'.
     */
    public int getCdModalidadeServico()
    {
        return this._cdModalidadeServico;
    } //-- int getCdModalidadeServico() 

    /**
     * Returns the value of field
     * 'cdMomentoProcessamentoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoProcessamentoPagamento'.
     */
    public int getCdMomentoProcessamentoPagamento()
    {
        return this._cdMomentoProcessamentoPagamento;
    } //-- int getCdMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdMotivoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdMotivoManutencaoContrato'.
     */
    public int getCdMotivoManutencaoContrato()
    {
        return this._cdMotivoManutencaoContrato;
    } //-- int getCdMotivoManutencaoContrato() 

    /**
     * Returns the value of field 'cdPagamentoNaoUtil'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoNaoUtil'.
     */
    public int getCdPagamentoNaoUtil()
    {
        return this._cdPagamentoNaoUtil;
    } //-- int getCdPagamentoNaoUtil() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoManutencaoContrato'.
     */
    public int getCdSituacaoManutencaoContrato()
    {
        return this._cdSituacaoManutencaoContrato;
    } //-- int getCdSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoDataFloat'.
     * 
     * @return int
     * @return the value of field 'cdTipoDataFloat'.
     */
    public int getCdTipoDataFloat()
    {
        return this._cdTipoDataFloat;
    } //-- int getCdTipoDataFloat() 

    /**
     * Returns the value of field 'cdTipoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencaoContrato'.
     */
    public int getCdTipoManutencaoContrato()
    {
        return this._cdTipoManutencaoContrato;
    } //-- int getCdTipoManutencaoContrato() 

    /**
     * Returns the value of field 'cdTipoServico'.
     * 
     * @return int
     * @return the value of field 'cdTipoServico'.
     */
    public int getCdTipoServico()
    {
        return this._cdTipoServico;
    } //-- int getCdTipoServico() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsFloarServicoContratado'.
     * 
     * @return String
     * @return the value of field 'dsFloarServicoContratado'.
     */
    public java.lang.String getDsFloarServicoContratado()
    {
        return this._dsFloarServicoContratado;
    } //-- java.lang.String getDsFloarServicoContratado() 

    /**
     * Returns the value of field 'dsIndicadorFeriadoLocal'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorFeriadoLocal'.
     */
    public java.lang.String getDsIndicadorFeriadoLocal()
    {
        return this._dsIndicadorFeriadoLocal;
    } //-- java.lang.String getDsIndicadorFeriadoLocal() 

    /**
     * Returns the value of field 'dsModalidadeServico'.
     * 
     * @return String
     * @return the value of field 'dsModalidadeServico'.
     */
    public java.lang.String getDsModalidadeServico()
    {
        return this._dsModalidadeServico;
    } //-- java.lang.String getDsModalidadeServico() 

    /**
     * Returns the value of field
     * 'dsMomentoProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoProcessamentoPagamento'.
     */
    public java.lang.String getDsMomentoProcessamentoPagamento()
    {
        return this._dsMomentoProcessamentoPagamento;
    } //-- java.lang.String getDsMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'dsMotivoManutencaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsMotivoManutencaoContrato'.
     */
    public java.lang.String getDsMotivoManutencaoContrato()
    {
        return this._dsMotivoManutencaoContrato;
    } //-- java.lang.String getDsMotivoManutencaoContrato() 

    /**
     * Returns the value of field 'dsPagamentoNaoUtil'.
     * 
     * @return String
     * @return the value of field 'dsPagamentoNaoUtil'.
     */
    public java.lang.String getDsPagamentoNaoUtil()
    {
        return this._dsPagamentoNaoUtil;
    } //-- java.lang.String getDsPagamentoNaoUtil() 

    /**
     * Returns the value of field 'dsSituacaoManutencaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoManutencaoContrato'.
     */
    public java.lang.String getDsSituacaoManutencaoContrato()
    {
        return this._dsSituacaoManutencaoContrato;
    } //-- java.lang.String getDsSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'dsTipoDataFloat'.
     * 
     * @return String
     * @return the value of field 'dsTipoDataFloat'.
     */
    public java.lang.String getDsTipoDataFloat()
    {
        return this._dsTipoDataFloat;
    } //-- java.lang.String getDsTipoDataFloat() 

    /**
     * Returns the value of field 'dsTipoServico'.
     * 
     * @return String
     * @return the value of field 'dsTipoServico'.
     */
    public java.lang.String getDsTipoServico()
    {
        return this._dsTipoServico;
    } //-- java.lang.String getDsTipoServico() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrManutencaoContratoNegocio'.
     */
    public long getNrManutencaoContratoNegocio()
    {
        return this._nrManutencaoContratoNegocio;
    } //-- long getNrManutencaoContratoNegocio() 

    /**
     * Returns the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoInclusao'.
     */
    public java.lang.String getNrOperacaoFluxoInclusao()
    {
        return this._nrOperacaoFluxoInclusao;
    } //-- java.lang.String getNrOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoManutencao'.
     */
    public java.lang.String getNrOperacaoFluxoManutencao()
    {
        return this._nrOperacaoFluxoManutencao;
    } //-- java.lang.String getNrOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'qtdeDiaFloatPagamento'.
     * 
     * @return int
     * @return the value of field 'qtdeDiaFloatPagamento'.
     */
    public int getQtdeDiaFloatPagamento()
    {
        return this._qtdeDiaFloatPagamento;
    } //-- int getQtdeDiaFloatPagamento() 

    /**
     * Returns the value of field 'qtdeDiaRepiqueCons'.
     * 
     * @return int
     * @return the value of field 'qtdeDiaRepiqueCons'.
     */
    public int getQtdeDiaRepiqueCons()
    {
        return this._qtdeDiaRepiqueCons;
    } //-- int getQtdeDiaRepiqueCons() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdFloatServicoContratado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFloatServicoContratado()
    {
        return this._has_cdFloatServicoContratado;
    } //-- boolean hasCdFloatServicoContratado() 

    /**
     * Method hasCdIndicadorDiaFloat
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorDiaFloat()
    {
        return this._has_cdIndicadorDiaFloat;
    } //-- boolean hasCdIndicadorDiaFloat() 

    /**
     * Method hasCdIndicadorDiaRepique
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorDiaRepique()
    {
        return this._has_cdIndicadorDiaRepique;
    } //-- boolean hasCdIndicadorDiaRepique() 

    /**
     * Method hasCdIndicadorFeriadoLocal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFeriadoLocal()
    {
        return this._has_cdIndicadorFeriadoLocal;
    } //-- boolean hasCdIndicadorFeriadoLocal() 

    /**
     * Method hasCdModalidadeServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidadeServico()
    {
        return this._has_cdModalidadeServico;
    } //-- boolean hasCdModalidadeServico() 

    /**
     * Method hasCdMomentoProcessamentoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoProcessamentoPagamento()
    {
        return this._has_cdMomentoProcessamentoPagamento;
    } //-- boolean hasCdMomentoProcessamentoPagamento() 

    /**
     * Method hasCdMotivoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoManutencaoContrato()
    {
        return this._has_cdMotivoManutencaoContrato;
    } //-- boolean hasCdMotivoManutencaoContrato() 

    /**
     * Method hasCdPagamentoNaoUtil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoNaoUtil()
    {
        return this._has_cdPagamentoNaoUtil;
    } //-- boolean hasCdPagamentoNaoUtil() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoManutencaoContrato()
    {
        return this._has_cdSituacaoManutencaoContrato;
    } //-- boolean hasCdSituacaoManutencaoContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoDataFloat
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDataFloat()
    {
        return this._has_cdTipoDataFloat;
    } //-- boolean hasCdTipoDataFloat() 

    /**
     * Method hasCdTipoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencaoContrato()
    {
        return this._has_cdTipoManutencaoContrato;
    } //-- boolean hasCdTipoManutencaoContrato() 

    /**
     * Method hasCdTipoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServico()
    {
        return this._has_cdTipoServico;
    } //-- boolean hasCdTipoServico() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method hasNrManutencaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrManutencaoContratoNegocio()
    {
        return this._has_nrManutencaoContratoNegocio;
    } //-- boolean hasNrManutencaoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtdeDiaFloatPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeDiaFloatPagamento()
    {
        return this._has_qtdeDiaFloatPagamento;
    } //-- boolean hasQtdeDiaFloatPagamento() 

    /**
     * Method hasQtdeDiaRepiqueCons
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeDiaRepiqueCons()
    {
        return this._has_qtdeDiaRepiqueCons;
    } //-- boolean hasQtdeDiaRepiqueCons() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdFloatServicoContratado'.
     * 
     * @param cdFloatServicoContratado the value of field
     * 'cdFloatServicoContratado'.
     */
    public void setCdFloatServicoContratado(int cdFloatServicoContratado)
    {
        this._cdFloatServicoContratado = cdFloatServicoContratado;
        this._has_cdFloatServicoContratado = true;
    } //-- void setCdFloatServicoContratado(int) 

    /**
     * Sets the value of field 'cdIndicadorDiaFloat'.
     * 
     * @param cdIndicadorDiaFloat the value of field
     * 'cdIndicadorDiaFloat'.
     */
    public void setCdIndicadorDiaFloat(int cdIndicadorDiaFloat)
    {
        this._cdIndicadorDiaFloat = cdIndicadorDiaFloat;
        this._has_cdIndicadorDiaFloat = true;
    } //-- void setCdIndicadorDiaFloat(int) 

    /**
     * Sets the value of field 'cdIndicadorDiaRepique'.
     * 
     * @param cdIndicadorDiaRepique the value of field
     * 'cdIndicadorDiaRepique'.
     */
    public void setCdIndicadorDiaRepique(int cdIndicadorDiaRepique)
    {
        this._cdIndicadorDiaRepique = cdIndicadorDiaRepique;
        this._has_cdIndicadorDiaRepique = true;
    } //-- void setCdIndicadorDiaRepique(int) 

    /**
     * Sets the value of field 'cdIndicadorFeriadoLocal'.
     * 
     * @param cdIndicadorFeriadoLocal the value of field
     * 'cdIndicadorFeriadoLocal'.
     */
    public void setCdIndicadorFeriadoLocal(int cdIndicadorFeriadoLocal)
    {
        this._cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
        this._has_cdIndicadorFeriadoLocal = true;
    } //-- void setCdIndicadorFeriadoLocal(int) 

    /**
     * Sets the value of field 'cdModalidadeServico'.
     * 
     * @param cdModalidadeServico the value of field
     * 'cdModalidadeServico'.
     */
    public void setCdModalidadeServico(int cdModalidadeServico)
    {
        this._cdModalidadeServico = cdModalidadeServico;
        this._has_cdModalidadeServico = true;
    } //-- void setCdModalidadeServico(int) 

    /**
     * Sets the value of field 'cdMomentoProcessamentoPagamento'.
     * 
     * @param cdMomentoProcessamentoPagamento the value of field
     * 'cdMomentoProcessamentoPagamento'.
     */
    public void setCdMomentoProcessamentoPagamento(int cdMomentoProcessamentoPagamento)
    {
        this._cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
        this._has_cdMomentoProcessamentoPagamento = true;
    } //-- void setCdMomentoProcessamentoPagamento(int) 

    /**
     * Sets the value of field 'cdMotivoManutencaoContrato'.
     * 
     * @param cdMotivoManutencaoContrato the value of field
     * 'cdMotivoManutencaoContrato'.
     */
    public void setCdMotivoManutencaoContrato(int cdMotivoManutencaoContrato)
    {
        this._cdMotivoManutencaoContrato = cdMotivoManutencaoContrato;
        this._has_cdMotivoManutencaoContrato = true;
    } //-- void setCdMotivoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdPagamentoNaoUtil'.
     * 
     * @param cdPagamentoNaoUtil the value of field
     * 'cdPagamentoNaoUtil'.
     */
    public void setCdPagamentoNaoUtil(int cdPagamentoNaoUtil)
    {
        this._cdPagamentoNaoUtil = cdPagamentoNaoUtil;
        this._has_cdPagamentoNaoUtil = true;
    } //-- void setCdPagamentoNaoUtil(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @param cdSituacaoManutencaoContrato the value of field
     * 'cdSituacaoManutencaoContrato'.
     */
    public void setCdSituacaoManutencaoContrato(int cdSituacaoManutencaoContrato)
    {
        this._cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
        this._has_cdSituacaoManutencaoContrato = true;
    } //-- void setCdSituacaoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoDataFloat'.
     * 
     * @param cdTipoDataFloat the value of field 'cdTipoDataFloat'.
     */
    public void setCdTipoDataFloat(int cdTipoDataFloat)
    {
        this._cdTipoDataFloat = cdTipoDataFloat;
        this._has_cdTipoDataFloat = true;
    } //-- void setCdTipoDataFloat(int) 

    /**
     * Sets the value of field 'cdTipoManutencaoContrato'.
     * 
     * @param cdTipoManutencaoContrato the value of field
     * 'cdTipoManutencaoContrato'.
     */
    public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato)
    {
        this._cdTipoManutencaoContrato = cdTipoManutencaoContrato;
        this._has_cdTipoManutencaoContrato = true;
    } //-- void setCdTipoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoServico'.
     * 
     * @param cdTipoServico the value of field 'cdTipoServico'.
     */
    public void setCdTipoServico(int cdTipoServico)
    {
        this._cdTipoServico = cdTipoServico;
        this._has_cdTipoServico = true;
    } //-- void setCdTipoServico(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsFloarServicoContratado'.
     * 
     * @param dsFloarServicoContratado the value of field
     * 'dsFloarServicoContratado'.
     */
    public void setDsFloarServicoContratado(java.lang.String dsFloarServicoContratado)
    {
        this._dsFloarServicoContratado = dsFloarServicoContratado;
    } //-- void setDsFloarServicoContratado(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorFeriadoLocal'.
     * 
     * @param dsIndicadorFeriadoLocal the value of field
     * 'dsIndicadorFeriadoLocal'.
     */
    public void setDsIndicadorFeriadoLocal(java.lang.String dsIndicadorFeriadoLocal)
    {
        this._dsIndicadorFeriadoLocal = dsIndicadorFeriadoLocal;
    } //-- void setDsIndicadorFeriadoLocal(java.lang.String) 

    /**
     * Sets the value of field 'dsModalidadeServico'.
     * 
     * @param dsModalidadeServico the value of field
     * 'dsModalidadeServico'.
     */
    public void setDsModalidadeServico(java.lang.String dsModalidadeServico)
    {
        this._dsModalidadeServico = dsModalidadeServico;
    } //-- void setDsModalidadeServico(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoProcessamentoPagamento'.
     * 
     * @param dsMomentoProcessamentoPagamento the value of field
     * 'dsMomentoProcessamentoPagamento'.
     */
    public void setDsMomentoProcessamentoPagamento(java.lang.String dsMomentoProcessamentoPagamento)
    {
        this._dsMomentoProcessamentoPagamento = dsMomentoProcessamentoPagamento;
    } //-- void setDsMomentoProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoManutencaoContrato'.
     * 
     * @param dsMotivoManutencaoContrato the value of field
     * 'dsMotivoManutencaoContrato'.
     */
    public void setDsMotivoManutencaoContrato(java.lang.String dsMotivoManutencaoContrato)
    {
        this._dsMotivoManutencaoContrato = dsMotivoManutencaoContrato;
    } //-- void setDsMotivoManutencaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsPagamentoNaoUtil'.
     * 
     * @param dsPagamentoNaoUtil the value of field
     * 'dsPagamentoNaoUtil'.
     */
    public void setDsPagamentoNaoUtil(java.lang.String dsPagamentoNaoUtil)
    {
        this._dsPagamentoNaoUtil = dsPagamentoNaoUtil;
    } //-- void setDsPagamentoNaoUtil(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoManutencaoContrato'.
     * 
     * @param dsSituacaoManutencaoContrato the value of field
     * 'dsSituacaoManutencaoContrato'.
     */
    public void setDsSituacaoManutencaoContrato(java.lang.String dsSituacaoManutencaoContrato)
    {
        this._dsSituacaoManutencaoContrato = dsSituacaoManutencaoContrato;
    } //-- void setDsSituacaoManutencaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDataFloat'.
     * 
     * @param dsTipoDataFloat the value of field 'dsTipoDataFloat'.
     */
    public void setDsTipoDataFloat(java.lang.String dsTipoDataFloat)
    {
        this._dsTipoDataFloat = dsTipoDataFloat;
    } //-- void setDsTipoDataFloat(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoServico'.
     * 
     * @param dsTipoServico the value of field 'dsTipoServico'.
     */
    public void setDsTipoServico(java.lang.String dsTipoServico)
    {
        this._dsTipoServico = dsTipoServico;
    } //-- void setDsTipoServico(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @param nrManutencaoContratoNegocio the value of field
     * 'nrManutencaoContratoNegocio'.
     */
    public void setNrManutencaoContratoNegocio(long nrManutencaoContratoNegocio)
    {
        this._nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
        this._has_nrManutencaoContratoNegocio = true;
    } //-- void setNrManutencaoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @param nrOperacaoFluxoInclusao the value of field
     * 'nrOperacaoFluxoInclusao'.
     */
    public void setNrOperacaoFluxoInclusao(java.lang.String nrOperacaoFluxoInclusao)
    {
        this._nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    } //-- void setNrOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @param nrOperacaoFluxoManutencao the value of field
     * 'nrOperacaoFluxoManutencao'.
     */
    public void setNrOperacaoFluxoManutencao(java.lang.String nrOperacaoFluxoManutencao)
    {
        this._nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
    } //-- void setNrOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'qtdeDiaFloatPagamento'.
     * 
     * @param qtdeDiaFloatPagamento the value of field
     * 'qtdeDiaFloatPagamento'.
     */
    public void setQtdeDiaFloatPagamento(int qtdeDiaFloatPagamento)
    {
        this._qtdeDiaFloatPagamento = qtdeDiaFloatPagamento;
        this._has_qtdeDiaFloatPagamento = true;
    } //-- void setQtdeDiaFloatPagamento(int) 

    /**
     * Sets the value of field 'qtdeDiaRepiqueCons'.
     * 
     * @param qtdeDiaRepiqueCons the value of field
     * 'qtdeDiaRepiqueCons'.
     */
    public void setQtdeDiaRepiqueCons(int qtdeDiaRepiqueCons)
    {
        this._qtdeDiaRepiqueCons = qtdeDiaRepiqueCons;
        this._has_qtdeDiaRepiqueCons = true;
    } //-- void setQtdeDiaRepiqueCons(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarManutencaoServicoContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.response.ConsultarManutencaoServicoContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.response.ConsultarManutencaoServicoContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.response.ConsultarManutencaoServicoContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoservicocontrato.response.ConsultarManutencaoServicoContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
