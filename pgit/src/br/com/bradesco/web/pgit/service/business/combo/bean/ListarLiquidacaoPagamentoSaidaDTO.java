/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarLiquidacaoPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarLiquidacaoPagamentoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroConsultas. */
	private Integer numeroConsultas;
	
	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo dsFormaLiquidacao. */
	private String dsFormaLiquidacao;
	
	/** Atributo cdSistema. */
	private String cdSistema;
	
	/** Atributo cdPrioridadeDebito. */
	private Integer cdPrioridadeDebito;
	
	/** Atributo cdControleHora. */
	private Integer cdControleHora;
	
	/** Atributo qtMinutosMargemSeguranca. */
	private Integer qtMinutosMargemSeguranca;
	
	/** Atributo hrLimiteProcessamento. */
	private String hrLimiteProcessamento;
	
	/** Atributo hrConsultasSaldoPagamento. */
	private String hrConsultasSaldoPagamento;
	
	/** Atributo hrConsultaFolhaPgto. */
	private String hrConsultaFolhaPgto;
	
	/** Atributo qtMinutosValorSuperior. */
	private Integer qtMinutosValorSuperior;
	
	/** Atributo hrLimiteValorSuperior. */
	private String hrLimiteValorSuperior;

	/** Atributo qtdTempoAgendamentoCobranca. */
	private Integer qtdTempoAgendamentoCobranca = null;

	/** Atributo qtdTempoEfetivacaoCiclicaCobranca. */
	private Integer qtdTempoEfetivacaoCiclicaCobranca = null;

	/** Atributo qtdTempoEfetivacaoDiariaCobranca. */
	private Integer qtdTempoEfetivacaoDiariaCobranca = null;

	/** Atributo qtdLimiteProcessamentoExcedido. */
	private Integer qtdLimiteProcessamentoExcedido = null;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}

	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: cdPrioridadeDebito.
	 *
	 * @return cdPrioridadeDebito
	 */
	public Integer getCdPrioridadeDebito() {
		return cdPrioridadeDebito;
	}

	/**
	 * Set: cdPrioridadeDebito.
	 *
	 * @param cdPrioridadeDebito the cd prioridade debito
	 */
	public void setCdPrioridadeDebito(Integer cdPrioridadeDebito) {
		this.cdPrioridadeDebito = cdPrioridadeDebito;
	}

	/**
	 * Get: cdControleHora.
	 *
	 * @return cdControleHora
	 */
	public Integer getCdControleHora() {
		return cdControleHora;
	}

	/**
	 * Set: cdControleHora.
	 *
	 * @param cdControleHora the cd controle hora
	 */
	public void setCdControleHora(Integer cdControleHora) {
		this.cdControleHora = cdControleHora;
	}

	/**
	 * Get: qtMinutosMargemSeguranca.
	 *
	 * @return qtMinutosMargemSeguranca
	 */
	public Integer getQtMinutosMargemSeguranca() {
		return qtMinutosMargemSeguranca;
	}

	/**
	 * Set: qtMinutosMargemSeguranca.
	 *
	 * @param qtMinutosMargemSeguranca the qt minutos margem seguranca
	 */
	public void setQtMinutosMargemSeguranca(Integer qtMinutosMargemSeguranca) {
		this.qtMinutosMargemSeguranca = qtMinutosMargemSeguranca;
	}

	/**
	 * Get: hrLimiteProcessamento.
	 *
	 * @return hrLimiteProcessamento
	 */
	public String getHrLimiteProcessamento() {
		return hrLimiteProcessamento;
	}

	/**
	 * Set: hrLimiteProcessamento.
	 *
	 * @param hrLimiteProcessamento the hr limite processamento
	 */
	public void setHrLimiteProcessamento(String hrLimiteProcessamento) {
		this.hrLimiteProcessamento = hrLimiteProcessamento;
	}

	/**
	 * Get: hrConsultasSaldoPagamento.
	 *
	 * @return hrConsultasSaldoPagamento
	 */
	public String getHrConsultasSaldoPagamento() {
		return hrConsultasSaldoPagamento;
	}

	/**
	 * Set: hrConsultasSaldoPagamento.
	 *
	 * @param hrConsultasSaldoPagamento the hr consultas saldo pagamento
	 */
	public void setHrConsultasSaldoPagamento(String hrConsultasSaldoPagamento) {
		this.hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
	}

	/**
	 * Get: hrConsultaFolhaPgto.
	 *
	 * @return hrConsultaFolhaPgto
	 */
	public String getHrConsultaFolhaPgto() {
		return hrConsultaFolhaPgto;
	}

	/**
	 * Set: hrConsultaFolhaPgto.
	 *
	 * @param hrConsultaFolhaPgto the hr consulta folha pgto
	 */
	public void setHrConsultaFolhaPgto(String hrConsultaFolhaPgto) {
		this.hrConsultaFolhaPgto = hrConsultaFolhaPgto;
	}

	/**
	 * Get: qtMinutosValorSuperior.
	 *
	 * @return qtMinutosValorSuperior
	 */
	public Integer getQtMinutosValorSuperior() {
		return qtMinutosValorSuperior;
	}

	/**
	 * Set: qtMinutosValorSuperior.
	 *
	 * @param qtMinutosValorSuperior the qt minutos valor superior
	 */
	public void setQtMinutosValorSuperior(Integer qtMinutosValorSuperior) {
		this.qtMinutosValorSuperior = qtMinutosValorSuperior;
	}

	/**
	 * Get: hrLimiteValorSuperior.
	 *
	 * @return hrLimiteValorSuperior
	 */
	public String getHrLimiteValorSuperior() {
		return hrLimiteValorSuperior;
	}

	/**
	 * Set: hrLimiteValorSuperior.
	 *
	 * @param hrLimiteValorSuperior the hr limite valor superior
	 */
	public void setHrLimiteValorSuperior(String hrLimiteValorSuperior) {
		this.hrLimiteValorSuperior = hrLimiteValorSuperior;
	}

    /**
     * Nome: getQtdTempoAgendamentoCobranca
     *
     * @return qtdTempoAgendamentoCobranca
     */
    public Integer getQtdTempoAgendamentoCobranca() {
        return qtdTempoAgendamentoCobranca;
    }

    /**
     * Nome: setQtdTempoAgendamentoCobranca
     *
     * @param qtdTempoAgendamentoCobranca
     */
    public void setQtdTempoAgendamentoCobranca(Integer qtdTempoAgendamentoCobranca) {
        this.qtdTempoAgendamentoCobranca = qtdTempoAgendamentoCobranca;
    }

    /**
     * Nome: getQtdTempoEfetivacaoCiclicaCobranca
     *
     * @return qtdTempoEfetivacaoCiclicaCobranca
     */
    public Integer getQtdTempoEfetivacaoCiclicaCobranca() {
        return qtdTempoEfetivacaoCiclicaCobranca;
    }

    /**
     * Nome: setQtdTempoEfetivacaoCiclicaCobranca
     *
     * @param qtdTempoEfetivacaoCiclicaCobranca
     */
    public void setQtdTempoEfetivacaoCiclicaCobranca(Integer qtdTempoEfetivacaoCiclicaCobranca) {
        this.qtdTempoEfetivacaoCiclicaCobranca = qtdTempoEfetivacaoCiclicaCobranca;
    }

    /**
     * Nome: getQtdTempoEfetivacaoDiariaCobranca
     *
     * @return qtdTempoEfetivacaoDiariaCobranca
     */
    public Integer getQtdTempoEfetivacaoDiariaCobranca() {
        return qtdTempoEfetivacaoDiariaCobranca;
    }

    /**
     * Nome: setQtdTempoEfetivacaoDiariaCobranca
     *
     * @param qtdTempoEfetivacaoDiariaCobranca
     */
    public void setQtdTempoEfetivacaoDiariaCobranca(Integer qtdTempoEfetivacaoDiariaCobranca) {
        this.qtdTempoEfetivacaoDiariaCobranca = qtdTempoEfetivacaoDiariaCobranca;
    }

	public Integer getQtdLimiteProcessamentoExcedido() {
		return qtdLimiteProcessamentoExcedido;
	}

	public void setQtdLimiteProcessamentoExcedido(
			Integer qtdLimiteProcessamentoExcedido) {
		this.qtdLimiteProcessamentoExcedido = qtdLimiteProcessamentoExcedido;
	}
    
    
}