/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 10/03/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public class ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO {

    /**
     * Atributo Integer
     */
    private Integer cdProdutoServicoOperacional = null;

    /**
     * Atributo String
     */
    private String dsProdutoServicoOperacao = null;

    /**
     * Atributo Integer
     */
    private Integer cdProdutoOperacionalRelacionado = null;

    /**
     * Atributo String
     */
    private String dsProdutoOperacaoRelacionado = null;

    /**
     * Atributo Integer
     */
    private Integer cdOperacaoProdutoServico = null;

    /**
     * Atributo String
     */
    private String dsOperacaoProdutoServico = null;

    /**
     * Atributo String
     */
    private String hrInclusaoRegistroHistorico = null;

    /**
     * Atributo String
     */
    private String cdUsuarioManutencao = null;

    /**
     * Atributo String
     */
    private String cdUsuarioManutencaoExterno = null;

    /**
     * Atributo Integer
     */
    private Integer cdIndicadorTipoManutencao = null;

    /**
     * Atributo String
     */
    private String dsIndicadorTipoManutencao = null;

    /**
     * Nome: getCdProdutoServicoOperacional
     * 
     * @return cdProdutoServicoOperacional
     */
    public Integer getCdProdutoServicoOperacional() {
        return cdProdutoServicoOperacional;
    }

    /**
     * Nome: setCdProdutoServicoOperacional
     * 
     * @param cdProdutoServicoOperacional
     */
    public void setCdProdutoServicoOperacional(Integer cdProdutoServicoOperacional) {
        this.cdProdutoServicoOperacional = cdProdutoServicoOperacional;
    }

    /**
     * Nome: getDsProdutoServicoOperacao
     * 
     * @return dsProdutoServicoOperacao
     */
    public String getDsProdutoServicoOperacao() {
        return dsProdutoServicoOperacao;
    }

    /**
     * Nome: setDsProdutoServicoOperacao
     * 
     * @param dsProdutoServicoOperacao
     */
    public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
        this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    }

    /**
     * Nome: getCdProdutoOperacionalRelacionado
     * 
     * @return cdProdutoOperacionalRelacionado
     */
    public Integer getCdProdutoOperacionalRelacionado() {
        return cdProdutoOperacionalRelacionado;
    }

    /**
     * Nome: setCdProdutoOperacionalRelacionado
     * 
     * @param cdProdutoOperacionalRelacionado
     */
    public void setCdProdutoOperacionalRelacionado(Integer cdProdutoOperacionalRelacionado) {
        this.cdProdutoOperacionalRelacionado = cdProdutoOperacionalRelacionado;
    }

    /**
     * Nome: getDsProdutoOperacaoRelacionado
     * 
     * @return dsProdutoOperacaoRelacionado
     */
    public String getDsProdutoOperacaoRelacionado() {
        return dsProdutoOperacaoRelacionado;
    }

    /**
     * Nome: setDsProdutoOperacaoRelacionado
     * 
     * @param dsProdutoOperacaoRelacionado
     */
    public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
        this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    }

    /**
     * Nome: getCdOperacaoProdutoServico
     * 
     * @return cdOperacaoProdutoServico
     */
    public Integer getCdOperacaoProdutoServico() {
        return cdOperacaoProdutoServico;
    }

    /**
     * Nome: setCdOperacaoProdutoServico
     * 
     * @param cdOperacaoProdutoServico
     */
    public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
        this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
    }

    /**
     * Nome: getDsOperacaoProdutoServico
     * 
     * @return dsOperacaoProdutoServico
     */
    public String getDsOperacaoProdutoServico() {
        return dsOperacaoProdutoServico;
    }

    /**
     * Nome: setDsOperacaoProdutoServico
     * 
     * @param dsOperacaoProdutoServico
     */
    public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
        this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    }

    /**
     * Nome: getHrInclusaoRegistroHistorico
     * 
     * @return hrInclusaoRegistroHistorico
     */
    public String getHrInclusaoRegistroHistorico() {
        return hrInclusaoRegistroHistorico;
    }

    /**
     * Nome: setHrInclusaoRegistroHistorico
     * 
     * @param hrInclusaoRegistroHistorico
     */
    public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
        this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
    }

    /**
     * Nome: getCdUsuarioManutencao
     * 
     * @return cdUsuarioManutencao
     */
    public String getCdUsuarioManutencao() {
        return cdUsuarioManutencao;
    }

    /**
     * Nome: setCdUsuarioManutencao
     * 
     * @param cdUsuarioManutencao
     */
    public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
        this.cdUsuarioManutencao = cdUsuarioManutencao;
    }

    /**
     * Nome: getCdUsuarioManutencaoExterno
     * 
     * @return cdUsuarioManutencaoExterno
     */
    public String getCdUsuarioManutencaoExterno() {
        return cdUsuarioManutencaoExterno;
    }

    /**
     * Nome: setCdUsuarioManutencaoExterno
     * 
     * @param cdUsuarioManutencaoExterno
     */
    public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
        this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    }

    /**
     * Nome: getCdIndicadorTipoManutencao
     * 
     * @return cdIndicadorTipoManutencao
     */
    public Integer getCdIndicadorTipoManutencao() {
        return cdIndicadorTipoManutencao;
    }

    /**
     * Nome: setCdIndicadorTipoManutencao
     * 
     * @param cdIndicadorTipoManutencao
     */
    public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
        this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
    }

    /**
     * Nome: getDsIndicadorTipoManutencao
     * 
     * @return dsIndicadorTipoManutencao
     */
    public String getDsIndicadorTipoManutencao() {
        return dsIndicadorTipoManutencao;
    }

    /**
     * Nome: setDsIndicadorTipoManutencao
     * 
     * @param dsIndicadorTipoManutencao
     */
    public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
        this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    }
    
    /**
     * Get: tipoServicoFormatado.
     *
     * @return tipoServicoFormatado
     */
    public String getTipoServicoFormatado() {
        return PgitUtil.concatenarCampos(getCdProdutoServicoOperacional(), getDsProdutoServicoOperacao());
    }
    
    /**
     * Nome: getModalidadeServicoFormatado
     * 
     * @return
     */
    public String getModalidadeServicoFormatado() {
        return PgitUtil.concatenarCampos(getCdProdutoOperacionalRelacionado(), getDsProdutoOperacaoRelacionado());
    }
    
    /**
     * Get: operacaoCatalogoFormatado.
     *
     * @return operacaoCatalogoFormatado
     */
    public String getOperacaoCatalogoFormatado() {
        return PgitUtil.concatenarCampos(getCdOperacaoProdutoServico(), getDsOperacaoProdutoServico());
    }
    
    /**
     * Nome: getUsuarioManutencao
     * 
     * @return
     */
    public String getUsuarioManutencao() {
        if(getCdUsuarioManutencao().equals("")){
            return getCdUsuarioManutencaoExterno();
        }
        
        return getCdUsuarioManutencao();
    }
    
    /**
     * Nome: getDataHoraManutencaoFormatada
     * 
     * @return
     */
    public String getDataHoraManutencaoFormatada() {
        return FormatarData.formatarDataTrilha(getHrInclusaoRegistroHistorico());
    }
    
}
