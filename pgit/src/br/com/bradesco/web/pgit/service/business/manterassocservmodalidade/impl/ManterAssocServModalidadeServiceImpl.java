/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.IManterAssocServModalidadeService;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.IManterAssocServModalidadeServiceConstants;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.DetalharAssocServModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.DetalharAssocServModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ExcluirAssocServModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ExcluirAssocServModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.IncluirAssocServModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.IncluirAssocServModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ListarAssocServModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ListarAssocServModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharassloteoperacaoservico.request.DetalharAssLoteOperacaoServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharassloteoperacaoservico.response.DetalharAssLoteOperacaoServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassloteoperacaoservico.request.ExcluirAssLoteOperacaoServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirassloteoperacaoservico.response.ExcluirAssLoteOperacaoServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassloteoperacaoservico.request.IncluirAssLoteOperacaoServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirassloteoperacaoservico.response.IncluirAssLoteOperacaoServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.request.ListarAssLoteOperacaoServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarassloteoperacaoservico.response.ListarAssLoteOperacaoServicoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterAssocServModalidade
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterAssocServModalidadeServiceImpl implements IManterAssocServModalidadeService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.IManterAssocServModalidadeService#listarAssocServModalidade(br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ListarAssocServModalidadeEntradaDTO)
	 */
	public List<ListarAssocServModalidadeSaidaDTO>listarAssocServModalidade(ListarAssocServModalidadeEntradaDTO listarAssocServModalidadeEntradaDTO) {
		
		List<ListarAssocServModalidadeSaidaDTO> listaRetorno = new ArrayList<ListarAssocServModalidadeSaidaDTO>();		
		ListarAssLoteOperacaoServicoRequest request = new ListarAssLoteOperacaoServicoRequest();
		ListarAssLoteOperacaoServicoResponse  response = new ListarAssLoteOperacaoServicoResponse();
		
		request.setCdTipoLayoutArquivo(listarAssocServModalidadeEntradaDTO.getTipoLayoutArquivo());
		request.setCdTipoLoteLayout(listarAssocServModalidadeEntradaDTO.getTipoLote());
		request.setCdProdutoServicoOper(listarAssocServModalidadeEntradaDTO.getTipoServico());		
		request.setCdRelacionadoProduto(listarAssocServModalidadeEntradaDTO.getTipoRelacionamento());
		request.setCdProdutoOperRelacionado(listarAssocServModalidadeEntradaDTO.getModalidadeServico());
		request.setQtConsultas(IManterAssocServModalidadeServiceConstants.QTDE_CONSULTAS_LISTAR);
		
		
		response = getFactoryAdapter().getListarAssLoteOperacaoServicoPDCAdapter().invokeProcess(request);

		ListarAssocServModalidadeSaidaDTO saidaDTO;
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarAssocServModalidadeSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
			saidaDTO.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
			saidaDTO.setCdTipoLote(response.getOcorrencias(i).getCdTipoLoteLayout());
			saidaDTO.setDsTipoLote(response.getOcorrencias(i).getDsTipoLoteLayout());
			saidaDTO.setCdModalidadeServico(response.getOcorrencias(i).getCdProdutoOperRelacionado());
			saidaDTO.setDsModalidadeServico(response.getOcorrencias(i).getDsProdutoOperRelacionado());
			saidaDTO.setCdTipoServico(response.getOcorrencias(i).getCdProdutoServicoOper());
			saidaDTO.setDsTipoServico(response.getOcorrencias(i).getDsProdutoServicoOper());
			saidaDTO.setCdTipoRelacionamento(response.getOcorrencias(i).getCdRelacionadoProduto());
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.IManterAssocServModalidadeService#detalharAssocServModalidade(br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.DetalharAssocServModalidadeEntradaDTO)
	 */
	public DetalharAssocServModalidadeSaidaDTO detalharAssocServModalidade(DetalharAssocServModalidadeEntradaDTO detalharAssocServModalidadeEntradaDTO) {
		
		DetalharAssocServModalidadeSaidaDTO saidaDTO = new DetalharAssocServModalidadeSaidaDTO();
		
		DetalharAssLoteOperacaoServicoRequest request = new DetalharAssLoteOperacaoServicoRequest();
		DetalharAssLoteOperacaoServicoResponse response = new DetalharAssLoteOperacaoServicoResponse();
			
		request.setCdTipoLayoutArquivo(detalharAssocServModalidadeEntradaDTO.getTipoLayoutArquivo());
		request.setCdTipoLoteLayout(detalharAssocServModalidadeEntradaDTO.getTipoLote());
		request.setCdProdutoServicoOper(detalharAssocServModalidadeEntradaDTO.getTipoServico());
		request.setCdProdutoOperRelacionado(detalharAssocServModalidadeEntradaDTO.getModalidadeServico());
		request.setCdRelacionadoProduto(detalharAssocServModalidadeEntradaDTO.getTipoRelacionamento());
		request.setQtConsultas(0);
		
		response = getFactoryAdapter().getDetalharAssLoteOperacaoServicoPDCAdapter().invokeProcess(request);
	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		
		saidaDTO.setCdTipoLayoutArquivo(response.getOcorrencias(0).getCdTipoLayoutArquivo());
		saidaDTO.setDsTipoLayoutArquivo(response.getOcorrencias(0).getDsTipoLayoutArquivo());
		saidaDTO.setCdTipoLote(response.getOcorrencias(0).getCdTipoLoteLayout());
		saidaDTO.setDsTipoLote(response.getOcorrencias(0).getDsTipoLoteLayout());
		saidaDTO.setCdTipoServico(response.getOcorrencias(0).getCdProdutoServicoOper());
		saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOper());
		saidaDTO.setCdModalidadeServico(response.getOcorrencias(0).getCdProdutoOperRelacionado());
		saidaDTO.setDsModalidadeServico(response.getOcorrencias(0).getDsProdutoOperRelacionado());
		saidaDTO.setCdTipoRelacionamento(response.getOcorrencias(0).getCdRelacionadoProduto());
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao()); 
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegrcInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao()); 
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
	
		
		
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.IManterAssocServModalidadeService#incluirAssocServModalidade(br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.IncluirAssocServModalidadeEntradaDTO)
	 */
	public IncluirAssocServModalidadeSaidaDTO incluirAssocServModalidade(IncluirAssocServModalidadeEntradaDTO incluirAssocServModalidadeEntradaDTO) {
		
		IncluirAssocServModalidadeSaidaDTO incluirAssocServModalidadeSaidaDTO = new IncluirAssocServModalidadeSaidaDTO();
		
		IncluirAssLoteOperacaoServicoRequest incluirAssLoteOperacaoServicoRequest = new IncluirAssLoteOperacaoServicoRequest();
		IncluirAssLoteOperacaoServicoResponse incluirAssLoteOperacaoServicoResponse = new IncluirAssLoteOperacaoServicoResponse();
	
	
		incluirAssLoteOperacaoServicoRequest.setCdTipoLayoutArquivo(incluirAssocServModalidadeEntradaDTO.getTipoLayoutArquivo());
		incluirAssLoteOperacaoServicoRequest.setCdTipoLoteLayout(incluirAssocServModalidadeEntradaDTO.getTipoLote());
		incluirAssLoteOperacaoServicoRequest.setCdProdutoServicoOper(incluirAssocServModalidadeEntradaDTO.getTipoServico());
		incluirAssLoteOperacaoServicoRequest.setCdRelacionadoProduto(incluirAssocServModalidadeEntradaDTO.getTipoRelacionamento());
		incluirAssLoteOperacaoServicoRequest.setCdProdutoOperRelacionado(incluirAssocServModalidadeEntradaDTO.getModalidadeServico());
		incluirAssLoteOperacaoServicoRequest.setQtConsultas(0);				
	
		
		incluirAssLoteOperacaoServicoResponse = getFactoryAdapter().getIncluirAssLoteOperacaoServicoPDCAdapter().invokeProcess(incluirAssLoteOperacaoServicoRequest);
		
		incluirAssocServModalidadeSaidaDTO.setCodMensagem(incluirAssLoteOperacaoServicoResponse.getCodMensagem());
		incluirAssocServModalidadeSaidaDTO.setMensagem(incluirAssLoteOperacaoServicoResponse.getMensagem());
	
		return incluirAssocServModalidadeSaidaDTO;
	}
	

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.IManterAssocServModalidadeService#excluirAssocServModalidade(br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean.ExcluirAssocServModalidadeEntradaDTO)
	 */
	public ExcluirAssocServModalidadeSaidaDTO excluirAssocServModalidade(ExcluirAssocServModalidadeEntradaDTO excluirAssocServModalidadeEntradaDTO) {
		
		ExcluirAssocServModalidadeSaidaDTO excluirAssocServModalidadeSaidaDTO = new ExcluirAssocServModalidadeSaidaDTO();
		
		ExcluirAssLoteOperacaoServicoRequest excluirAssLoteOperacaoServicoRequest = new ExcluirAssLoteOperacaoServicoRequest();
		ExcluirAssLoteOperacaoServicoResponse excluirAssLoteOperacaoServicoResponse = new ExcluirAssLoteOperacaoServicoResponse();
	
	
		excluirAssLoteOperacaoServicoRequest.setCdTipoLayoutArquivo(excluirAssocServModalidadeEntradaDTO.getTipoLayoutArquivo());
		excluirAssLoteOperacaoServicoRequest.setCdTipoLoteLayout(excluirAssocServModalidadeEntradaDTO.getTipoLote());
		excluirAssLoteOperacaoServicoRequest.setCdProdutoServicoOper(excluirAssocServModalidadeEntradaDTO.getTipoServico());
		excluirAssLoteOperacaoServicoRequest.setCdRelacionadoProduto(excluirAssocServModalidadeEntradaDTO.getTipoRelacionamento());
		excluirAssLoteOperacaoServicoRequest.setCdProdutoOperRelacionado(excluirAssocServModalidadeEntradaDTO.getModalidadeServico());
		excluirAssLoteOperacaoServicoRequest.setQtConsultas(0);				
	
		
		excluirAssLoteOperacaoServicoResponse = getFactoryAdapter().getExcluirAssLoteOperacaoServicoPDCAdapter().invokeProcess(excluirAssLoteOperacaoServicoRequest);
		
		excluirAssocServModalidadeSaidaDTO.setCodMensagem(excluirAssLoteOperacaoServicoResponse.getCodMensagem());
		excluirAssocServModalidadeSaidaDTO.setMensagem(excluirAssLoteOperacaoServicoResponse.getMensagem());
	
		return excluirAssocServModalidadeSaidaDTO;
	}
}

