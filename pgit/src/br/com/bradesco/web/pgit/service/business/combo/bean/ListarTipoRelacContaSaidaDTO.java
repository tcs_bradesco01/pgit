/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarTipoRelacContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoRelacContaSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo tpRelacionamentoContrato. */
    private Integer tpRelacionamentoContrato;
    
    /** Atributo rsTipoRelacionamentoContrato. */
    private String  rsTipoRelacionamentoContrato;
    
    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }
    
    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }
    
    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }
    
    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    
    /**
     * Get: rsTipoRelacionamentoContrato.
     *
     * @return rsTipoRelacionamentoContrato
     */
    public String getRsTipoRelacionamentoContrato() {
        return rsTipoRelacionamentoContrato;
    }
    
    /**
     * Set: rsTipoRelacionamentoContrato.
     *
     * @param rsTipoRelacionamentoContrato the rs tipo relacionamento contrato
     */
    public void setRsTipoRelacionamentoContrato(String rsTipoRelacionamentoContrato) {
        this.rsTipoRelacionamentoContrato = rsTipoRelacionamentoContrato;
    }
    
    /**
     * Get: tpRelacionamentoContrato.
     *
     * @return tpRelacionamentoContrato
     */
    public Integer getTpRelacionamentoContrato() {
        return tpRelacionamentoContrato;
    }
    
    /**
     * Set: tpRelacionamentoContrato.
     *
     * @param tpRelacionamentoContrato the tp relacionamento contrato
     */
    public void setTpRelacionamentoContrato(Integer tpRelacionamentoContrato) {
        this.tpRelacionamentoContrato = tpRelacionamentoContrato;
    }
    
    
}
