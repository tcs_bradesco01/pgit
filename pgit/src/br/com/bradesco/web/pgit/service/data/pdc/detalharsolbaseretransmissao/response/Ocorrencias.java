/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cpfCnpjPessoa
     */
    private java.lang.String _cpfCnpjPessoa;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _cdClienteTransferenciaArquivo
     */
    private long _cdClienteTransferenciaArquivo = 0;

    /**
     * keeps track of state for field: _cdClienteTransferenciaArquiv
     */
    private boolean _has_cdClienteTransferenciaArquivo;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _nrSequenciaRetorno
     */
    private long _nrSequenciaRetorno = 0;

    /**
     * keeps track of state for field: _nrSequenciaRetorno
     */
    private boolean _has_nrSequenciaRetorno;

    /**
     * Field _cdOperacao
     */
    private java.lang.String _cdOperacao;

    /**
     * Field _dsArquivoRetorno
     */
    private java.lang.String _dsArquivoRetorno;

    /**
     * Field _hrInclusaoRetorno
     */
    private java.lang.String _hrInclusaoRetorno;

    /**
     * Field _dsSituacaoProcessamento
     */
    private java.lang.String _dsSituacaoProcessamento;

    /**
     * Field _qtdeRegistrosOcorrencias
     */
    private long _qtdeRegistrosOcorrencias = 0;

    /**
     * keeps track of state for field: _qtdeRegistrosOcorrencias
     */
    private boolean _has_qtdeRegistrosOcorrencias;

    /**
     * Field _vlrRegistros
     */
    private java.math.BigDecimal _vlrRegistros = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlrRegistros(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClienteTransferenciaArquivo
     * 
     */
    public void deleteCdClienteTransferenciaArquivo()
    {
        this._has_cdClienteTransferenciaArquivo= false;
    } //-- void deleteCdClienteTransferenciaArquivo() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrSequenciaRetorno
     * 
     */
    public void deleteNrSequenciaRetorno()
    {
        this._has_nrSequenciaRetorno= false;
    } //-- void deleteNrSequenciaRetorno() 

    /**
     * Method deleteQtdeRegistrosOcorrencias
     * 
     */
    public void deleteQtdeRegistrosOcorrencias()
    {
        this._has_qtdeRegistrosOcorrencias= false;
    } //-- void deleteQtdeRegistrosOcorrencias() 

    /**
     * Returns the value of field 'cdClienteTransferenciaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdClienteTransferenciaArquivo'.
     */
    public long getCdClienteTransferenciaArquivo()
    {
        return this._cdClienteTransferenciaArquivo;
    } //-- long getCdClienteTransferenciaArquivo() 

    /**
     * Returns the value of field 'cdOperacao'.
     * 
     * @return String
     * @return the value of field 'cdOperacao'.
     */
    public java.lang.String getCdOperacao()
    {
        return this._cdOperacao;
    } //-- java.lang.String getCdOperacao() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cpfCnpjPessoa'.
     * 
     * @return String
     * @return the value of field 'cpfCnpjPessoa'.
     */
    public java.lang.String getCpfCnpjPessoa()
    {
        return this._cpfCnpjPessoa;
    } //-- java.lang.String getCpfCnpjPessoa() 

    /**
     * Returns the value of field 'dsArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsArquivoRetorno'.
     */
    public java.lang.String getDsArquivoRetorno()
    {
        return this._dsArquivoRetorno;
    } //-- java.lang.String getDsArquivoRetorno() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsSituacaoProcessamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoProcessamento'.
     */
    public java.lang.String getDsSituacaoProcessamento()
    {
        return this._dsSituacaoProcessamento;
    } //-- java.lang.String getDsSituacaoProcessamento() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusaoRetorno'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRetorno'.
     */
    public java.lang.String getHrInclusaoRetorno()
    {
        return this._hrInclusaoRetorno;
    } //-- java.lang.String getHrInclusaoRetorno() 

    /**
     * Returns the value of field 'nrSequenciaRetorno'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaRetorno'.
     */
    public long getNrSequenciaRetorno()
    {
        return this._nrSequenciaRetorno;
    } //-- long getNrSequenciaRetorno() 

    /**
     * Returns the value of field 'qtdeRegistrosOcorrencias'.
     * 
     * @return long
     * @return the value of field 'qtdeRegistrosOcorrencias'.
     */
    public long getQtdeRegistrosOcorrencias()
    {
        return this._qtdeRegistrosOcorrencias;
    } //-- long getQtdeRegistrosOcorrencias() 

    /**
     * Returns the value of field 'vlrRegistros'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrRegistros'.
     */
    public java.math.BigDecimal getVlrRegistros()
    {
        return this._vlrRegistros;
    } //-- java.math.BigDecimal getVlrRegistros() 

    /**
     * Method hasCdClienteTransferenciaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClienteTransferenciaArquivo()
    {
        return this._has_cdClienteTransferenciaArquivo;
    } //-- boolean hasCdClienteTransferenciaArquivo() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrSequenciaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaRetorno()
    {
        return this._has_nrSequenciaRetorno;
    } //-- boolean hasNrSequenciaRetorno() 

    /**
     * Method hasQtdeRegistrosOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeRegistrosOcorrencias()
    {
        return this._has_qtdeRegistrosOcorrencias;
    } //-- boolean hasQtdeRegistrosOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClienteTransferenciaArquivo'.
     * 
     * @param cdClienteTransferenciaArquivo the value of field
     * 'cdClienteTransferenciaArquivo'.
     */
    public void setCdClienteTransferenciaArquivo(long cdClienteTransferenciaArquivo)
    {
        this._cdClienteTransferenciaArquivo = cdClienteTransferenciaArquivo;
        this._has_cdClienteTransferenciaArquivo = true;
    } //-- void setCdClienteTransferenciaArquivo(long) 

    /**
     * Sets the value of field 'cdOperacao'.
     * 
     * @param cdOperacao the value of field 'cdOperacao'.
     */
    public void setCdOperacao(java.lang.String cdOperacao)
    {
        this._cdOperacao = cdOperacao;
    } //-- void setCdOperacao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cpfCnpjPessoa'.
     * 
     * @param cpfCnpjPessoa the value of field 'cpfCnpjPessoa'.
     */
    public void setCpfCnpjPessoa(java.lang.String cpfCnpjPessoa)
    {
        this._cpfCnpjPessoa = cpfCnpjPessoa;
    } //-- void setCpfCnpjPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsArquivoRetorno'.
     * 
     * @param dsArquivoRetorno the value of field 'dsArquivoRetorno'
     */
    public void setDsArquivoRetorno(java.lang.String dsArquivoRetorno)
    {
        this._dsArquivoRetorno = dsArquivoRetorno;
    } //-- void setDsArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoProcessamento'.
     * 
     * @param dsSituacaoProcessamento the value of field
     * 'dsSituacaoProcessamento'.
     */
    public void setDsSituacaoProcessamento(java.lang.String dsSituacaoProcessamento)
    {
        this._dsSituacaoProcessamento = dsSituacaoProcessamento;
    } //-- void setDsSituacaoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRetorno'.
     * 
     * @param hrInclusaoRetorno the value of field
     * 'hrInclusaoRetorno'.
     */
    public void setHrInclusaoRetorno(java.lang.String hrInclusaoRetorno)
    {
        this._hrInclusaoRetorno = hrInclusaoRetorno;
    } //-- void setHrInclusaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaRetorno'.
     * 
     * @param nrSequenciaRetorno the value of field
     * 'nrSequenciaRetorno'.
     */
    public void setNrSequenciaRetorno(long nrSequenciaRetorno)
    {
        this._nrSequenciaRetorno = nrSequenciaRetorno;
        this._has_nrSequenciaRetorno = true;
    } //-- void setNrSequenciaRetorno(long) 

    /**
     * Sets the value of field 'qtdeRegistrosOcorrencias'.
     * 
     * @param qtdeRegistrosOcorrencias the value of field
     * 'qtdeRegistrosOcorrencias'.
     */
    public void setQtdeRegistrosOcorrencias(long qtdeRegistrosOcorrencias)
    {
        this._qtdeRegistrosOcorrencias = qtdeRegistrosOcorrencias;
        this._has_qtdeRegistrosOcorrencias = true;
    } //-- void setQtdeRegistrosOcorrencias(long) 

    /**
     * Sets the value of field 'vlrRegistros'.
     * 
     * @param vlrRegistros the value of field 'vlrRegistros'.
     */
    public void setVlrRegistros(java.math.BigDecimal vlrRegistros)
    {
        this._vlrRegistros = vlrRegistros;
    } //-- void setVlrRegistros(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolbaseretransmissao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
