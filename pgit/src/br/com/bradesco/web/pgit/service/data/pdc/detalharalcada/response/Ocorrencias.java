/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _dsSistema
     */
    private java.lang.String _dsSistema;

    /**
     * Field _cdProgPagamentoIntegrado
     */
    private java.lang.String _cdProgPagamentoIntegrado;

    /**
     * Field _cdTipoAcaoPagamento
     */
    private int _cdTipoAcaoPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoAcaoPagamento
     */
    private boolean _has_cdTipoAcaoPagamento;

    /**
     * Field _dsTipoAcaoPagamento
     */
    private java.lang.String _dsTipoAcaoPagamento;

    /**
     * Field _cdTipoAcessoAlcada
     */
    private int _cdTipoAcessoAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoAcessoAlcada
     */
    private boolean _has_cdTipoAcessoAlcada;

    /**
     * Field _cdTipoRegraAlcada
     */
    private int _cdTipoRegraAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoRegraAlcada
     */
    private boolean _has_cdTipoRegraAlcada;

    /**
     * Field _cdTipoTratoAlcada
     */
    private int _cdTipoTratoAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoTratoAlcada
     */
    private boolean _has_cdTipoTratoAlcada;

    /**
     * Field _cdSistemaRetorno
     */
    private java.lang.String _cdSistemaRetorno;

    /**
     * Field _dsSistemaRetorno
     */
    private java.lang.String _dsSistemaRetorno;

    /**
     * Field _cdProgRetornoPagamento
     */
    private java.lang.String _cdProgRetornoPagamento;

    /**
     * Field _cdProdutoOperDeflt
     */
    private int _cdProdutoOperDeflt = 0;

    /**
     * keeps track of state for field: _cdProdutoOperDeflt
     */
    private boolean _has_cdProdutoOperDeflt;

    /**
     * Field _dsProdutoDeflt
     */
    private java.lang.String _dsProdutoDeflt;

    /**
     * Field _cdOperProdutoServico
     */
    private int _cdOperProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperProdutoServico
     */
    private boolean _has_cdOperProdutoServico;

    /**
     * Field _dsCdOperServico
     */
    private java.lang.String _dsCdOperServico;

    /**
     * Field _cdRegraAlcada
     */
    private int _cdRegraAlcada = 0;

    /**
     * keeps track of state for field: _cdRegraAlcada
     */
    private boolean _has_cdRegraAlcada;

    /**
     * Field _dsRegraAlcada
     */
    private java.lang.String _dsRegraAlcada;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenSegrcInclusao
     */
    private java.lang.String _cdAutenSegrcInclusao;

    /**
     * Field _nmOperFluxoInclusao
     */
    private java.lang.String _nmOperFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenSegrcManutencao
     */
    private java.lang.String _cdAutenSegrcManutencao;

    /**
     * Field _nmOperFluxoManutencao
     */
    private java.lang.String _nmOperFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdOperProdutoServico
     * 
     */
    public void deleteCdOperProdutoServico()
    {
        this._has_cdOperProdutoServico= false;
    } //-- void deleteCdOperProdutoServico() 

    /**
     * Method deleteCdProdutoOperDeflt
     * 
     */
    public void deleteCdProdutoOperDeflt()
    {
        this._has_cdProdutoOperDeflt= false;
    } //-- void deleteCdProdutoOperDeflt() 

    /**
     * Method deleteCdRegraAlcada
     * 
     */
    public void deleteCdRegraAlcada()
    {
        this._has_cdRegraAlcada= false;
    } //-- void deleteCdRegraAlcada() 

    /**
     * Method deleteCdTipoAcaoPagamento
     * 
     */
    public void deleteCdTipoAcaoPagamento()
    {
        this._has_cdTipoAcaoPagamento= false;
    } //-- void deleteCdTipoAcaoPagamento() 

    /**
     * Method deleteCdTipoAcessoAlcada
     * 
     */
    public void deleteCdTipoAcessoAlcada()
    {
        this._has_cdTipoAcessoAlcada= false;
    } //-- void deleteCdTipoAcessoAlcada() 

    /**
     * Method deleteCdTipoRegraAlcada
     * 
     */
    public void deleteCdTipoRegraAlcada()
    {
        this._has_cdTipoRegraAlcada= false;
    } //-- void deleteCdTipoRegraAlcada() 

    /**
     * Method deleteCdTipoTratoAlcada
     * 
     */
    public void deleteCdTipoTratoAlcada()
    {
        this._has_cdTipoTratoAlcada= false;
    } //-- void deleteCdTipoTratoAlcada() 

    /**
     * Returns the value of field 'cdAutenSegrcInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcInclusao'.
     */
    public java.lang.String getCdAutenSegrcInclusao()
    {
        return this._cdAutenSegrcInclusao;
    } //-- java.lang.String getCdAutenSegrcInclusao() 

    /**
     * Returns the value of field 'cdAutenSegrcManutencao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcManutencao'.
     */
    public java.lang.String getCdAutenSegrcManutencao()
    {
        return this._cdAutenSegrcManutencao;
    } //-- java.lang.String getCdAutenSegrcManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdOperProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperProdutoServico'.
     */
    public int getCdOperProdutoServico()
    {
        return this._cdOperProdutoServico;
    } //-- int getCdOperProdutoServico() 

    /**
     * Returns the value of field 'cdProdutoOperDeflt'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperDeflt'.
     */
    public int getCdProdutoOperDeflt()
    {
        return this._cdProdutoOperDeflt;
    } //-- int getCdProdutoOperDeflt() 

    /**
     * Returns the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'cdProgPagamentoIntegrado'.
     */
    public java.lang.String getCdProgPagamentoIntegrado()
    {
        return this._cdProgPagamentoIntegrado;
    } //-- java.lang.String getCdProgPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdProgRetornoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdProgRetornoPagamento'.
     */
    public java.lang.String getCdProgRetornoPagamento()
    {
        return this._cdProgRetornoPagamento;
    } //-- java.lang.String getCdProgRetornoPagamento() 

    /**
     * Returns the value of field 'cdRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'cdRegraAlcada'.
     */
    public int getCdRegraAlcada()
    {
        return this._cdRegraAlcada;
    } //-- int getCdRegraAlcada() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdSistemaRetorno'.
     * 
     * @return String
     * @return the value of field 'cdSistemaRetorno'.
     */
    public java.lang.String getCdSistemaRetorno()
    {
        return this._cdSistemaRetorno;
    } //-- java.lang.String getCdSistemaRetorno() 

    /**
     * Returns the value of field 'cdTipoAcaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcaoPagamento'.
     */
    public int getCdTipoAcaoPagamento()
    {
        return this._cdTipoAcaoPagamento;
    } //-- int getCdTipoAcaoPagamento() 

    /**
     * Returns the value of field 'cdTipoAcessoAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcessoAlcada'.
     */
    public int getCdTipoAcessoAlcada()
    {
        return this._cdTipoAcessoAlcada;
    } //-- int getCdTipoAcessoAlcada() 

    /**
     * Returns the value of field 'cdTipoRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoRegraAlcada'.
     */
    public int getCdTipoRegraAlcada()
    {
        return this._cdTipoRegraAlcada;
    } //-- int getCdTipoRegraAlcada() 

    /**
     * Returns the value of field 'cdTipoTratoAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoTratoAlcada'.
     */
    public int getCdTipoTratoAlcada()
    {
        return this._cdTipoTratoAlcada;
    } //-- int getCdTipoTratoAlcada() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCdOperServico'.
     * 
     * @return String
     * @return the value of field 'dsCdOperServico'.
     */
    public java.lang.String getDsCdOperServico()
    {
        return this._dsCdOperServico;
    } //-- java.lang.String getDsCdOperServico() 

    /**
     * Returns the value of field 'dsProdutoDeflt'.
     * 
     * @return String
     * @return the value of field 'dsProdutoDeflt'.
     */
    public java.lang.String getDsProdutoDeflt()
    {
        return this._dsProdutoDeflt;
    } //-- java.lang.String getDsProdutoDeflt() 

    /**
     * Returns the value of field 'dsRegraAlcada'.
     * 
     * @return String
     * @return the value of field 'dsRegraAlcada'.
     */
    public java.lang.String getDsRegraAlcada()
    {
        return this._dsRegraAlcada;
    } //-- java.lang.String getDsRegraAlcada() 

    /**
     * Returns the value of field 'dsSistema'.
     * 
     * @return String
     * @return the value of field 'dsSistema'.
     */
    public java.lang.String getDsSistema()
    {
        return this._dsSistema;
    } //-- java.lang.String getDsSistema() 

    /**
     * Returns the value of field 'dsSistemaRetorno'.
     * 
     * @return String
     * @return the value of field 'dsSistemaRetorno'.
     */
    public java.lang.String getDsSistemaRetorno()
    {
        return this._dsSistemaRetorno;
    } //-- java.lang.String getDsSistemaRetorno() 

    /**
     * Returns the value of field 'dsTipoAcaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsTipoAcaoPagamento'.
     */
    public java.lang.String getDsTipoAcaoPagamento()
    {
        return this._dsTipoAcaoPagamento;
    } //-- java.lang.String getDsTipoAcaoPagamento() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nmOperFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoInclusao'.
     */
    public java.lang.String getNmOperFluxoInclusao()
    {
        return this._nmOperFluxoInclusao;
    } //-- java.lang.String getNmOperFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoManutencao'.
     */
    public java.lang.String getNmOperFluxoManutencao()
    {
        return this._nmOperFluxoManutencao;
    } //-- java.lang.String getNmOperFluxoManutencao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdOperProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperProdutoServico()
    {
        return this._has_cdOperProdutoServico;
    } //-- boolean hasCdOperProdutoServico() 

    /**
     * Method hasCdProdutoOperDeflt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperDeflt()
    {
        return this._has_cdProdutoOperDeflt;
    } //-- boolean hasCdProdutoOperDeflt() 

    /**
     * Method hasCdRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAlcada()
    {
        return this._has_cdRegraAlcada;
    } //-- boolean hasCdRegraAlcada() 

    /**
     * Method hasCdTipoAcaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcaoPagamento()
    {
        return this._has_cdTipoAcaoPagamento;
    } //-- boolean hasCdTipoAcaoPagamento() 

    /**
     * Method hasCdTipoAcessoAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcessoAlcada()
    {
        return this._has_cdTipoAcessoAlcada;
    } //-- boolean hasCdTipoAcessoAlcada() 

    /**
     * Method hasCdTipoRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRegraAlcada()
    {
        return this._has_cdTipoRegraAlcada;
    } //-- boolean hasCdTipoRegraAlcada() 

    /**
     * Method hasCdTipoTratoAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoTratoAlcada()
    {
        return this._has_cdTipoTratoAlcada;
    } //-- boolean hasCdTipoTratoAlcada() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenSegrcInclusao'.
     * 
     * @param cdAutenSegrcInclusao the value of field
     * 'cdAutenSegrcInclusao'.
     */
    public void setCdAutenSegrcInclusao(java.lang.String cdAutenSegrcInclusao)
    {
        this._cdAutenSegrcInclusao = cdAutenSegrcInclusao;
    } //-- void setCdAutenSegrcInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenSegrcManutencao'.
     * 
     * @param cdAutenSegrcManutencao the value of field
     * 'cdAutenSegrcManutencao'.
     */
    public void setCdAutenSegrcManutencao(java.lang.String cdAutenSegrcManutencao)
    {
        this._cdAutenSegrcManutencao = cdAutenSegrcManutencao;
    } //-- void setCdAutenSegrcManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdOperProdutoServico'.
     * 
     * @param cdOperProdutoServico the value of field
     * 'cdOperProdutoServico'.
     */
    public void setCdOperProdutoServico(int cdOperProdutoServico)
    {
        this._cdOperProdutoServico = cdOperProdutoServico;
        this._has_cdOperProdutoServico = true;
    } //-- void setCdOperProdutoServico(int) 

    /**
     * Sets the value of field 'cdProdutoOperDeflt'.
     * 
     * @param cdProdutoOperDeflt the value of field
     * 'cdProdutoOperDeflt'.
     */
    public void setCdProdutoOperDeflt(int cdProdutoOperDeflt)
    {
        this._cdProdutoOperDeflt = cdProdutoOperDeflt;
        this._has_cdProdutoOperDeflt = true;
    } //-- void setCdProdutoOperDeflt(int) 

    /**
     * Sets the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @param cdProgPagamentoIntegrado the value of field
     * 'cdProgPagamentoIntegrado'.
     */
    public void setCdProgPagamentoIntegrado(java.lang.String cdProgPagamentoIntegrado)
    {
        this._cdProgPagamentoIntegrado = cdProgPagamentoIntegrado;
    } //-- void setCdProgPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'cdProgRetornoPagamento'.
     * 
     * @param cdProgRetornoPagamento the value of field
     * 'cdProgRetornoPagamento'.
     */
    public void setCdProgRetornoPagamento(java.lang.String cdProgRetornoPagamento)
    {
        this._cdProgRetornoPagamento = cdProgRetornoPagamento;
    } //-- void setCdProgRetornoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdRegraAlcada'.
     * 
     * @param cdRegraAlcada the value of field 'cdRegraAlcada'.
     */
    public void setCdRegraAlcada(int cdRegraAlcada)
    {
        this._cdRegraAlcada = cdRegraAlcada;
        this._has_cdRegraAlcada = true;
    } //-- void setCdRegraAlcada(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdSistemaRetorno'.
     * 
     * @param cdSistemaRetorno the value of field 'cdSistemaRetorno'
     */
    public void setCdSistemaRetorno(java.lang.String cdSistemaRetorno)
    {
        this._cdSistemaRetorno = cdSistemaRetorno;
    } //-- void setCdSistemaRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoAcaoPagamento'.
     * 
     * @param cdTipoAcaoPagamento the value of field
     * 'cdTipoAcaoPagamento'.
     */
    public void setCdTipoAcaoPagamento(int cdTipoAcaoPagamento)
    {
        this._cdTipoAcaoPagamento = cdTipoAcaoPagamento;
        this._has_cdTipoAcaoPagamento = true;
    } //-- void setCdTipoAcaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoAcessoAlcada'.
     * 
     * @param cdTipoAcessoAlcada the value of field
     * 'cdTipoAcessoAlcada'.
     */
    public void setCdTipoAcessoAlcada(int cdTipoAcessoAlcada)
    {
        this._cdTipoAcessoAlcada = cdTipoAcessoAlcada;
        this._has_cdTipoAcessoAlcada = true;
    } //-- void setCdTipoAcessoAlcada(int) 

    /**
     * Sets the value of field 'cdTipoRegraAlcada'.
     * 
     * @param cdTipoRegraAlcada the value of field
     * 'cdTipoRegraAlcada'.
     */
    public void setCdTipoRegraAlcada(int cdTipoRegraAlcada)
    {
        this._cdTipoRegraAlcada = cdTipoRegraAlcada;
        this._has_cdTipoRegraAlcada = true;
    } //-- void setCdTipoRegraAlcada(int) 

    /**
     * Sets the value of field 'cdTipoTratoAlcada'.
     * 
     * @param cdTipoTratoAlcada the value of field
     * 'cdTipoTratoAlcada'.
     */
    public void setCdTipoTratoAlcada(int cdTipoTratoAlcada)
    {
        this._cdTipoTratoAlcada = cdTipoTratoAlcada;
        this._has_cdTipoTratoAlcada = true;
    } //-- void setCdTipoTratoAlcada(int) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCdOperServico'.
     * 
     * @param dsCdOperServico the value of field 'dsCdOperServico'.
     */
    public void setDsCdOperServico(java.lang.String dsCdOperServico)
    {
        this._dsCdOperServico = dsCdOperServico;
    } //-- void setDsCdOperServico(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoDeflt'.
     * 
     * @param dsProdutoDeflt the value of field 'dsProdutoDeflt'.
     */
    public void setDsProdutoDeflt(java.lang.String dsProdutoDeflt)
    {
        this._dsProdutoDeflt = dsProdutoDeflt;
    } //-- void setDsProdutoDeflt(java.lang.String) 

    /**
     * Sets the value of field 'dsRegraAlcada'.
     * 
     * @param dsRegraAlcada the value of field 'dsRegraAlcada'.
     */
    public void setDsRegraAlcada(java.lang.String dsRegraAlcada)
    {
        this._dsRegraAlcada = dsRegraAlcada;
    } //-- void setDsRegraAlcada(java.lang.String) 

    /**
     * Sets the value of field 'dsSistema'.
     * 
     * @param dsSistema the value of field 'dsSistema'.
     */
    public void setDsSistema(java.lang.String dsSistema)
    {
        this._dsSistema = dsSistema;
    } //-- void setDsSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsSistemaRetorno'.
     * 
     * @param dsSistemaRetorno the value of field 'dsSistemaRetorno'
     */
    public void setDsSistemaRetorno(java.lang.String dsSistemaRetorno)
    {
        this._dsSistemaRetorno = dsSistemaRetorno;
    } //-- void setDsSistemaRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoAcaoPagamento'.
     * 
     * @param dsTipoAcaoPagamento the value of field
     * 'dsTipoAcaoPagamento'.
     */
    public void setDsTipoAcaoPagamento(java.lang.String dsTipoAcaoPagamento)
    {
        this._dsTipoAcaoPagamento = dsTipoAcaoPagamento;
    } //-- void setDsTipoAcaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoInclusao'.
     * 
     * @param nmOperFluxoInclusao the value of field
     * 'nmOperFluxoInclusao'.
     */
    public void setNmOperFluxoInclusao(java.lang.String nmOperFluxoInclusao)
    {
        this._nmOperFluxoInclusao = nmOperFluxoInclusao;
    } //-- void setNmOperFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoManutencao'.
     * 
     * @param nmOperFluxoManutencao the value of field
     * 'nmOperFluxoManutencao'.
     */
    public void setNmOperFluxoManutencao(java.lang.String nmOperFluxoManutencao)
    {
        this._nmOperFluxoManutencao = nmOperFluxoManutencao;
    } //-- void setNmOperFluxoManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharalcada.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
