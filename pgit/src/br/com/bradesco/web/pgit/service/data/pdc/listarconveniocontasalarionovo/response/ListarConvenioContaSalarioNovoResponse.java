/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarConvenioContaSalarioNovoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ListarConvenioContaSalarioNovoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _indicadorConvenioNovo
     */
    private java.lang.String _indicadorConvenioNovo;

    /**
     * Field _usuarioInclusao
     */
    private java.lang.String _usuarioInclusao = "0";

    /**
     * Field _horarioInclusao
     */
    private java.lang.String _horarioInclusao;

    /**
     * Field _descCanalInclusao
     */
    private java.lang.String _descCanalInclusao;

    /**
     * Field _descCanalManutencao
     */
    private java.lang.String _descCanalManutencao;

    /**
     * Field _tipoCanalInclusao
     */
    private int _tipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _tipoCanalInclusao
     */
    private boolean _has_tipoCanalInclusao;

    /**
     * Field _tipoCanalManutencao
     */
    private int _tipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _tipoCanalManutencao
     */
    private boolean _has_tipoCanalManutencao;

    /**
     * Field _usuarioManutencao
     */
    private java.lang.String _usuarioManutencao;

    /**
     * Field _horarioManutencao
     */
    private java.lang.String _horarioManutencao;

    /**
     * Field _numOcorrencias
     */
    private int _numOcorrencias = 0;

    /**
     * keeps track of state for field: _numOcorrencias
     */
    private boolean _has_numOcorrencias;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarConvenioContaSalarioNovoResponse() 
     {
        super();
        setUsuarioInclusao("0");
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.ListarConvenioContaSalarioNovoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 50)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 50");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 50)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 50");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias) 

    /**
     * Method deleteNumOcorrencias
     * 
     */
    public void deleteNumOcorrencias()
    {
        this._has_numOcorrencias= false;
    } //-- void deleteNumOcorrencias() 

    /**
     * Method deleteTipoCanalInclusao
     * 
     */
    public void deleteTipoCanalInclusao()
    {
        this._has_tipoCanalInclusao= false;
    } //-- void deleteTipoCanalInclusao() 

    /**
     * Method deleteTipoCanalManutencao
     * 
     */
    public void deleteTipoCanalManutencao()
    {
        this._has_tipoCanalManutencao= false;
    } //-- void deleteTipoCanalManutencao() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'descCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'descCanalInclusao'.
     */
    public java.lang.String getDescCanalInclusao()
    {
        return this._descCanalInclusao;
    } //-- java.lang.String getDescCanalInclusao() 

    /**
     * Returns the value of field 'descCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'descCanalManutencao'.
     */
    public java.lang.String getDescCanalManutencao()
    {
        return this._descCanalManutencao;
    } //-- java.lang.String getDescCanalManutencao() 

    /**
     * Returns the value of field 'horarioInclusao'.
     * 
     * @return String
     * @return the value of field 'horarioInclusao'.
     */
    public java.lang.String getHorarioInclusao()
    {
        return this._horarioInclusao;
    } //-- java.lang.String getHorarioInclusao() 

    /**
     * Returns the value of field 'horarioManutencao'.
     * 
     * @return String
     * @return the value of field 'horarioManutencao'.
     */
    public java.lang.String getHorarioManutencao()
    {
        return this._horarioManutencao;
    } //-- java.lang.String getHorarioManutencao() 

    /**
     * Returns the value of field 'indicadorConvenioNovo'.
     * 
     * @return String
     * @return the value of field 'indicadorConvenioNovo'.
     */
    public java.lang.String getIndicadorConvenioNovo()
    {
        return this._indicadorConvenioNovo;
    } //-- java.lang.String getIndicadorConvenioNovo() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numOcorrencias'.
     */
    public int getNumOcorrencias()
    {
        return this._numOcorrencias;
    } //-- int getNumOcorrencias() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'tipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'tipoCanalInclusao'.
     */
    public int getTipoCanalInclusao()
    {
        return this._tipoCanalInclusao;
    } //-- int getTipoCanalInclusao() 

    /**
     * Returns the value of field 'tipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'tipoCanalManutencao'.
     */
    public int getTipoCanalManutencao()
    {
        return this._tipoCanalManutencao;
    } //-- int getTipoCanalManutencao() 

    /**
     * Returns the value of field 'usuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'usuarioInclusao'.
     */
    public java.lang.String getUsuarioInclusao()
    {
        return this._usuarioInclusao;
    } //-- java.lang.String getUsuarioInclusao() 

    /**
     * Returns the value of field 'usuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'usuarioManutencao'.
     */
    public java.lang.String getUsuarioManutencao()
    {
        return this._usuarioManutencao;
    } //-- java.lang.String getUsuarioManutencao() 

    /**
     * Method hasNumOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumOcorrencias()
    {
        return this._has_numOcorrencias;
    } //-- boolean hasNumOcorrencias() 

    /**
     * Method hasTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasTipoCanalInclusao()
    {
        return this._has_tipoCanalInclusao;
    } //-- boolean hasTipoCanalInclusao() 

    /**
     * Method hasTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasTipoCanalManutencao()
    {
        return this._has_tipoCanalManutencao;
    } //-- boolean hasTipoCanalManutencao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'descCanalInclusao'.
     * 
     * @param descCanalInclusao the value of field
     * 'descCanalInclusao'.
     */
    public void setDescCanalInclusao(java.lang.String descCanalInclusao)
    {
        this._descCanalInclusao = descCanalInclusao;
    } //-- void setDescCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'descCanalManutencao'.
     * 
     * @param descCanalManutencao the value of field
     * 'descCanalManutencao'.
     */
    public void setDescCanalManutencao(java.lang.String descCanalManutencao)
    {
        this._descCanalManutencao = descCanalManutencao;
    } //-- void setDescCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'horarioInclusao'.
     * 
     * @param horarioInclusao the value of field 'horarioInclusao'.
     */
    public void setHorarioInclusao(java.lang.String horarioInclusao)
    {
        this._horarioInclusao = horarioInclusao;
    } //-- void setHorarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'horarioManutencao'.
     * 
     * @param horarioManutencao the value of field
     * 'horarioManutencao'.
     */
    public void setHorarioManutencao(java.lang.String horarioManutencao)
    {
        this._horarioManutencao = horarioManutencao;
    } //-- void setHorarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'indicadorConvenioNovo'.
     * 
     * @param indicadorConvenioNovo the value of field
     * 'indicadorConvenioNovo'.
     */
    public void setIndicadorConvenioNovo(java.lang.String indicadorConvenioNovo)
    {
        this._indicadorConvenioNovo = indicadorConvenioNovo;
    } //-- void setIndicadorConvenioNovo(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numOcorrencias'.
     * 
     * @param numOcorrencias the value of field 'numOcorrencias'.
     */
    public void setNumOcorrencias(int numOcorrencias)
    {
        this._numOcorrencias = numOcorrencias;
        this._has_numOcorrencias = true;
    } //-- void setNumOcorrencias(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 50)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 50");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.Ocorrencias) 

    /**
     * Sets the value of field 'tipoCanalInclusao'.
     * 
     * @param tipoCanalInclusao the value of field
     * 'tipoCanalInclusao'.
     */
    public void setTipoCanalInclusao(int tipoCanalInclusao)
    {
        this._tipoCanalInclusao = tipoCanalInclusao;
        this._has_tipoCanalInclusao = true;
    } //-- void setTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'tipoCanalManutencao'.
     * 
     * @param tipoCanalManutencao the value of field
     * 'tipoCanalManutencao'.
     */
    public void setTipoCanalManutencao(int tipoCanalManutencao)
    {
        this._tipoCanalManutencao = tipoCanalManutencao;
        this._has_tipoCanalManutencao = true;
    } //-- void setTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'usuarioInclusao'.
     * 
     * @param usuarioInclusao the value of field 'usuarioInclusao'.
     */
    public void setUsuarioInclusao(java.lang.String usuarioInclusao)
    {
        this._usuarioInclusao = usuarioInclusao;
    } //-- void setUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'usuarioManutencao'.
     * 
     * @param usuarioManutencao the value of field
     * 'usuarioManutencao'.
     */
    public void setUsuarioManutencao(java.lang.String usuarioManutencao)
    {
        this._usuarioManutencao = usuarioManutencao;
    } //-- void setUsuarioManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarConvenioContaSalarioNovoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.ListarConvenioContaSalarioNovoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.ListarConvenioContaSalarioNovoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.ListarConvenioContaSalarioNovoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalarionovo.response.ListarConvenioContaSalarioNovoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
