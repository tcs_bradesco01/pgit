/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;


/**
 * Nome: SubstituirVincEnderecoParticipanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SubstituirVincEnderecoParticipanteEntradaDTO {

    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;

    /** Atributo cdPessoa. */
    private Long cdPessoa;

    /** Atributo cdFinalidadeEnderecoContrato. */
    private Integer cdFinalidadeEnderecoContrato;

    /** Atributo cdPessoaEndereco. */
    private Long cdPessoaEndereco;

    /** Atributo cdPessoaJuridicaEndereco. */
    private Long cdPessoaJuridicaEndereco;

    /** Atributo nrSequenciaEndereco. */
    private Integer nrSequenciaEndereco;

    /** Atributo cdUsoEnderecoPessoa. */
    private Integer cdUsoEnderecoPessoa;

    /** Atributo cdEspecieEndereco. */
    private Integer cdEspecieEndereco;

    /** Atributo cdUsoPostal. */
    private Integer cdUsoPostal;

    /**
     * Set: cdpessoaJuridicaContrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get: cdpessoaJuridicaContrato.
     *
     * @return cdpessoaJuridicaContrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set: cdTipoParticipacaoPessoa.
     *
     * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
     */
    public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
        this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
    }

    /**
     * Get: cdTipoParticipacaoPessoa.
     *
     * @return cdTipoParticipacaoPessoa
     */
    public Integer getCdTipoParticipacaoPessoa() {
        return this.cdTipoParticipacaoPessoa;
    }

    /**
     * Set: cdPessoa.
     *
     * @param cdPessoa the cd pessoa
     */
    public void setCdPessoa(Long cdPessoa) {
        this.cdPessoa = cdPessoa;
    }

    /**
     * Get: cdPessoa.
     *
     * @return cdPessoa
     */
    public Long getCdPessoa() {
        return this.cdPessoa;
    }

    /**
     * Set: cdFinalidadeEnderecoContrato.
     *
     * @param cdFinalidadeEnderecoContrato the cd finalidade endereco contrato
     */
    public void setCdFinalidadeEnderecoContrato(Integer cdFinalidadeEnderecoContrato) {
        this.cdFinalidadeEnderecoContrato = cdFinalidadeEnderecoContrato;
    }

    /**
     * Get: cdFinalidadeEnderecoContrato.
     *
     * @return cdFinalidadeEnderecoContrato
     */
    public Integer getCdFinalidadeEnderecoContrato() {
        return this.cdFinalidadeEnderecoContrato;
    }

    /**
     * Set: cdPessoaEndereco.
     *
     * @param cdPessoaEndereco the cd pessoa endereco
     */
    public void setCdPessoaEndereco(Long cdPessoaEndereco) {
        this.cdPessoaEndereco = cdPessoaEndereco;
    }

    /**
     * Get: cdPessoaEndereco.
     *
     * @return cdPessoaEndereco
     */
    public Long getCdPessoaEndereco() {
        return this.cdPessoaEndereco;
    }

    /**
     * Set: cdPessoaJuridicaEndereco.
     *
     * @param cdPessoaJuridicaEndereco the cd pessoa juridica endereco
     */
    public void setCdPessoaJuridicaEndereco(Long cdPessoaJuridicaEndereco) {
        this.cdPessoaJuridicaEndereco = cdPessoaJuridicaEndereco;
    }

    /**
     * Get: cdPessoaJuridicaEndereco.
     *
     * @return cdPessoaJuridicaEndereco
     */
    public Long getCdPessoaJuridicaEndereco() {
        return this.cdPessoaJuridicaEndereco;
    }

    /**
     * Set: nrSequenciaEndereco.
     *
     * @param nrSequenciaEndereco the nr sequencia endereco
     */
    public void setNrSequenciaEndereco(Integer nrSequenciaEndereco) {
        this.nrSequenciaEndereco = nrSequenciaEndereco;
    }

    /**
     * Get: nrSequenciaEndereco.
     *
     * @return nrSequenciaEndereco
     */
    public Integer getNrSequenciaEndereco() {
        return this.nrSequenciaEndereco;
    }

    /**
     * Set: cdUsoEnderecoPessoa.
     *
     * @param cdUsoEnderecoPessoa the cd uso endereco pessoa
     */
    public void setCdUsoEnderecoPessoa(Integer cdUsoEnderecoPessoa) {
        this.cdUsoEnderecoPessoa = cdUsoEnderecoPessoa;
    }

    /**
     * Get: cdUsoEnderecoPessoa.
     *
     * @return cdUsoEnderecoPessoa
     */
    public Integer getCdUsoEnderecoPessoa() {
        return this.cdUsoEnderecoPessoa;
    }

	/**
	 * Get: cdEspecieEndereco.
	 *
	 * @return cdEspecieEndereco
	 */
	public Integer getCdEspecieEndereco() {
		return cdEspecieEndereco;
	}

	/**
	 * Set: cdEspecieEndereco.
	 *
	 * @param cdEspecieEndereco the cd especie endereco
	 */
	public void setCdEspecieEndereco(Integer cdEspecieEndereco) {
		this.cdEspecieEndereco = cdEspecieEndereco;
	}

	/**
	 * Get: cdUsoPostal.
	 *
	 * @return cdUsoPostal
	 */
	public Integer getCdUsoPostal() {
		return cdUsoPostal;
	}

	/**
	 * Set: cdUsoPostal.
	 *
	 * @param cdUsoPostal the cd uso postal
	 */
	public void setCdUsoPostal(Integer cdUsoPostal) {
		this.cdUsoPostal = cdUsoPostal;
	}
}