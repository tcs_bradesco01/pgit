/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.AnteciparPostergarPagtosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.AnteciparPostergarPagtosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.ConsultarPagtosIndAntPostEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosageind.bean.ConsultarPagtosIndAntPostSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AnteciparPostergarPagtosAgeInd
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAnteciparPostergarPagtosAgeIndService {
	
	/**
	 * Consultar pagtos ind ant postergacao.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar pagtos ind ant post saida dt o>
	 */
	List<ConsultarPagtosIndAntPostSaidaDTO> consultarPagtosIndAntPostergacao(ConsultarPagtosIndAntPostEntradaDTO entradaDTO);	
	
	/**
	 * Antecipar postergar pagtos individuais.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the antecipar postergar pagtos individuais saida dto
	 */
	AnteciparPostergarPagtosIndividuaisSaidaDTO anteciparPostergarPagtosIndividuais(AnteciparPostergarPagtosIndividuaisEntradaDTO entradaDTO);
}

