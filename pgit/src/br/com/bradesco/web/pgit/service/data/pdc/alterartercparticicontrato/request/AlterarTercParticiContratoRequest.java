/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarTercParticiContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarTercParticiContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _nrRastreaTerceiro
     */
    private int _nrRastreaTerceiro = 0;

    /**
     * keeps track of state for field: _nrRastreaTerceiro
     */
    private boolean _has_nrRastreaTerceiro;

    /**
     * Field _cdCpfCnpjTerceiro
     */
    private long _cdCpfCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjTerceiro
     */
    private boolean _has_cdCpfCnpjTerceiro;

    /**
     * Field _cdFilialCnpjTerceiro
     */
    private int _cdFilialCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjTerceiro
     */
    private boolean _has_cdFilialCnpjTerceiro;

    /**
     * Field _cdControleCnpjTerceiro
     */
    private int _cdControleCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdControleCnpjTerceiro
     */
    private boolean _has_cdControleCnpjTerceiro;

    /**
     * Field _cdIndicadorAgendaTitulo
     */
    private int _cdIndicadorAgendaTitulo = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaTitulo
     */
    private boolean _has_cdIndicadorAgendaTitulo;

    /**
     * Field _cdIndicadorTerceiro
     */
    private int _cdIndicadorTerceiro = 0;

    /**
     * keeps track of state for field: _cdIndicadorTerceiro
     */
    private boolean _has_cdIndicadorTerceiro;

    /**
     * Field _cdIndicadorPapeletaTerceiro
     */
    private int _cdIndicadorPapeletaTerceiro = 0;

    /**
     * keeps track of state for field: _cdIndicadorPapeletaTerceiro
     */
    private boolean _has_cdIndicadorPapeletaTerceiro;

    /**
     * Field _dtInicioRastreaTerceiro
     */
    private java.lang.String _dtInicioRastreaTerceiro;

    /**
     * Field _cdIndicadorBloqueioRastreabilidade
     */
    private java.lang.String _cdIndicadorBloqueioRastreabilidade;

    /**
     * Field _dtInicioPapeletaTerceiro
     */
    private java.lang.String _dtInicioPapeletaTerceiro;

    /**
     * Field _cdIndicadorBloqueioPapeleta
     */
    private java.lang.String _cdIndicadorBloqueioPapeleta;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarTercParticiContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.request.AlterarTercParticiContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCnpjTerceiro
     * 
     */
    public void deleteCdControleCnpjTerceiro()
    {
        this._has_cdControleCnpjTerceiro= false;
    } //-- void deleteCdControleCnpjTerceiro() 

    /**
     * Method deleteCdCpfCnpjTerceiro
     * 
     */
    public void deleteCdCpfCnpjTerceiro()
    {
        this._has_cdCpfCnpjTerceiro= false;
    } //-- void deleteCdCpfCnpjTerceiro() 

    /**
     * Method deleteCdFilialCnpjTerceiro
     * 
     */
    public void deleteCdFilialCnpjTerceiro()
    {
        this._has_cdFilialCnpjTerceiro= false;
    } //-- void deleteCdFilialCnpjTerceiro() 

    /**
     * Method deleteCdIndicadorAgendaTitulo
     * 
     */
    public void deleteCdIndicadorAgendaTitulo()
    {
        this._has_cdIndicadorAgendaTitulo= false;
    } //-- void deleteCdIndicadorAgendaTitulo() 

    /**
     * Method deleteCdIndicadorPapeletaTerceiro
     * 
     */
    public void deleteCdIndicadorPapeletaTerceiro()
    {
        this._has_cdIndicadorPapeletaTerceiro= false;
    } //-- void deleteCdIndicadorPapeletaTerceiro() 

    /**
     * Method deleteCdIndicadorTerceiro
     * 
     */
    public void deleteCdIndicadorTerceiro()
    {
        this._has_cdIndicadorTerceiro= false;
    } //-- void deleteCdIndicadorTerceiro() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrRastreaTerceiro
     * 
     */
    public void deleteNrRastreaTerceiro()
    {
        this._has_nrRastreaTerceiro= false;
    } //-- void deleteNrRastreaTerceiro() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdControleCnpjTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjTerceiro'.
     */
    public int getCdControleCnpjTerceiro()
    {
        return this._cdControleCnpjTerceiro;
    } //-- int getCdControleCnpjTerceiro() 

    /**
     * Returns the value of field 'cdCpfCnpjTerceiro'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjTerceiro'.
     */
    public long getCdCpfCnpjTerceiro()
    {
        return this._cdCpfCnpjTerceiro;
    } //-- long getCdCpfCnpjTerceiro() 

    /**
     * Returns the value of field 'cdFilialCnpjTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjTerceiro'.
     */
    public int getCdFilialCnpjTerceiro()
    {
        return this._cdFilialCnpjTerceiro;
    } //-- int getCdFilialCnpjTerceiro() 

    /**
     * Returns the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaTitulo'.
     */
    public int getCdIndicadorAgendaTitulo()
    {
        return this._cdIndicadorAgendaTitulo;
    } //-- int getCdIndicadorAgendaTitulo() 

    /**
     * Returns the value of field 'cdIndicadorBloqueioPapeleta'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorBloqueioPapeleta'.
     */
    public java.lang.String getCdIndicadorBloqueioPapeleta()
    {
        return this._cdIndicadorBloqueioPapeleta;
    } //-- java.lang.String getCdIndicadorBloqueioPapeleta() 

    /**
     * Returns the value of field
     * 'cdIndicadorBloqueioRastreabilidade'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorBloqueioRastreabilidade'.
     */
    public java.lang.String getCdIndicadorBloqueioRastreabilidade()
    {
        return this._cdIndicadorBloqueioRastreabilidade;
    } //-- java.lang.String getCdIndicadorBloqueioRastreabilidade() 

    /**
     * Returns the value of field 'cdIndicadorPapeletaTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorPapeletaTerceiro'.
     */
    public int getCdIndicadorPapeletaTerceiro()
    {
        return this._cdIndicadorPapeletaTerceiro;
    } //-- int getCdIndicadorPapeletaTerceiro() 

    /**
     * Returns the value of field 'cdIndicadorTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTerceiro'.
     */
    public int getCdIndicadorTerceiro()
    {
        return this._cdIndicadorTerceiro;
    } //-- int getCdIndicadorTerceiro() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dtInicioPapeletaTerceiro'.
     * 
     * @return String
     * @return the value of field 'dtInicioPapeletaTerceiro'.
     */
    public java.lang.String getDtInicioPapeletaTerceiro()
    {
        return this._dtInicioPapeletaTerceiro;
    } //-- java.lang.String getDtInicioPapeletaTerceiro() 

    /**
     * Returns the value of field 'dtInicioRastreaTerceiro'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreaTerceiro'.
     */
    public java.lang.String getDtInicioRastreaTerceiro()
    {
        return this._dtInicioRastreaTerceiro;
    } //-- java.lang.String getDtInicioRastreaTerceiro() 

    /**
     * Returns the value of field 'nrRastreaTerceiro'.
     * 
     * @return int
     * @return the value of field 'nrRastreaTerceiro'.
     */
    public int getNrRastreaTerceiro()
    {
        return this._nrRastreaTerceiro;
    } //-- int getNrRastreaTerceiro() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdControleCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjTerceiro()
    {
        return this._has_cdControleCnpjTerceiro;
    } //-- boolean hasCdControleCnpjTerceiro() 

    /**
     * Method hasCdCpfCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjTerceiro()
    {
        return this._has_cdCpfCnpjTerceiro;
    } //-- boolean hasCdCpfCnpjTerceiro() 

    /**
     * Method hasCdFilialCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjTerceiro()
    {
        return this._has_cdFilialCnpjTerceiro;
    } //-- boolean hasCdFilialCnpjTerceiro() 

    /**
     * Method hasCdIndicadorAgendaTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaTitulo()
    {
        return this._has_cdIndicadorAgendaTitulo;
    } //-- boolean hasCdIndicadorAgendaTitulo() 

    /**
     * Method hasCdIndicadorPapeletaTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorPapeletaTerceiro()
    {
        return this._has_cdIndicadorPapeletaTerceiro;
    } //-- boolean hasCdIndicadorPapeletaTerceiro() 

    /**
     * Method hasCdIndicadorTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTerceiro()
    {
        return this._has_cdIndicadorTerceiro;
    } //-- boolean hasCdIndicadorTerceiro() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrRastreaTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrRastreaTerceiro()
    {
        return this._has_nrRastreaTerceiro;
    } //-- boolean hasNrRastreaTerceiro() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCnpjTerceiro'.
     * 
     * @param cdControleCnpjTerceiro the value of field
     * 'cdControleCnpjTerceiro'.
     */
    public void setCdControleCnpjTerceiro(int cdControleCnpjTerceiro)
    {
        this._cdControleCnpjTerceiro = cdControleCnpjTerceiro;
        this._has_cdControleCnpjTerceiro = true;
    } //-- void setCdControleCnpjTerceiro(int) 

    /**
     * Sets the value of field 'cdCpfCnpjTerceiro'.
     * 
     * @param cdCpfCnpjTerceiro the value of field
     * 'cdCpfCnpjTerceiro'.
     */
    public void setCdCpfCnpjTerceiro(long cdCpfCnpjTerceiro)
    {
        this._cdCpfCnpjTerceiro = cdCpfCnpjTerceiro;
        this._has_cdCpfCnpjTerceiro = true;
    } //-- void setCdCpfCnpjTerceiro(long) 

    /**
     * Sets the value of field 'cdFilialCnpjTerceiro'.
     * 
     * @param cdFilialCnpjTerceiro the value of field
     * 'cdFilialCnpjTerceiro'.
     */
    public void setCdFilialCnpjTerceiro(int cdFilialCnpjTerceiro)
    {
        this._cdFilialCnpjTerceiro = cdFilialCnpjTerceiro;
        this._has_cdFilialCnpjTerceiro = true;
    } //-- void setCdFilialCnpjTerceiro(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @param cdIndicadorAgendaTitulo the value of field
     * 'cdIndicadorAgendaTitulo'.
     */
    public void setCdIndicadorAgendaTitulo(int cdIndicadorAgendaTitulo)
    {
        this._cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
        this._has_cdIndicadorAgendaTitulo = true;
    } //-- void setCdIndicadorAgendaTitulo(int) 

    /**
     * Sets the value of field 'cdIndicadorBloqueioPapeleta'.
     * 
     * @param cdIndicadorBloqueioPapeleta the value of field
     * 'cdIndicadorBloqueioPapeleta'.
     */
    public void setCdIndicadorBloqueioPapeleta(java.lang.String cdIndicadorBloqueioPapeleta)
    {
        this._cdIndicadorBloqueioPapeleta = cdIndicadorBloqueioPapeleta;
    } //-- void setCdIndicadorBloqueioPapeleta(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorBloqueioRastreabilidade'.
     * 
     * @param cdIndicadorBloqueioRastreabilidade the value of field
     * 'cdIndicadorBloqueioRastreabilidade'.
     */
    public void setCdIndicadorBloqueioRastreabilidade(java.lang.String cdIndicadorBloqueioRastreabilidade)
    {
        this._cdIndicadorBloqueioRastreabilidade = cdIndicadorBloqueioRastreabilidade;
    } //-- void setCdIndicadorBloqueioRastreabilidade(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPapeletaTerceiro'.
     * 
     * @param cdIndicadorPapeletaTerceiro the value of field
     * 'cdIndicadorPapeletaTerceiro'.
     */
    public void setCdIndicadorPapeletaTerceiro(int cdIndicadorPapeletaTerceiro)
    {
        this._cdIndicadorPapeletaTerceiro = cdIndicadorPapeletaTerceiro;
        this._has_cdIndicadorPapeletaTerceiro = true;
    } //-- void setCdIndicadorPapeletaTerceiro(int) 

    /**
     * Sets the value of field 'cdIndicadorTerceiro'.
     * 
     * @param cdIndicadorTerceiro the value of field
     * 'cdIndicadorTerceiro'.
     */
    public void setCdIndicadorTerceiro(int cdIndicadorTerceiro)
    {
        this._cdIndicadorTerceiro = cdIndicadorTerceiro;
        this._has_cdIndicadorTerceiro = true;
    } //-- void setCdIndicadorTerceiro(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dtInicioPapeletaTerceiro'.
     * 
     * @param dtInicioPapeletaTerceiro the value of field
     * 'dtInicioPapeletaTerceiro'.
     */
    public void setDtInicioPapeletaTerceiro(java.lang.String dtInicioPapeletaTerceiro)
    {
        this._dtInicioPapeletaTerceiro = dtInicioPapeletaTerceiro;
    } //-- void setDtInicioPapeletaTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreaTerceiro'.
     * 
     * @param dtInicioRastreaTerceiro the value of field
     * 'dtInicioRastreaTerceiro'.
     */
    public void setDtInicioRastreaTerceiro(java.lang.String dtInicioRastreaTerceiro)
    {
        this._dtInicioRastreaTerceiro = dtInicioRastreaTerceiro;
    } //-- void setDtInicioRastreaTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'nrRastreaTerceiro'.
     * 
     * @param nrRastreaTerceiro the value of field
     * 'nrRastreaTerceiro'.
     */
    public void setNrRastreaTerceiro(int nrRastreaTerceiro)
    {
        this._nrRastreaTerceiro = nrRastreaTerceiro;
        this._has_nrRastreaTerceiro = true;
    } //-- void setNrRastreaTerceiro(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarTercParticiContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.request.AlterarTercParticiContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.request.AlterarTercParticiContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.request.AlterarTercParticiContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterartercparticicontrato.request.AlterarTercParticiContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
