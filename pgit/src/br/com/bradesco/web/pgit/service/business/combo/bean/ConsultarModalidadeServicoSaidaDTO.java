/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ConsultarModalidadeServicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarModalidadeServicoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdModalidadePagamentoCliente. */
	private Integer cdModalidadePagamentoCliente;
	
	/** Atributo dsModalidadePagamentoCliente. */
	private String dsModalidadePagamentoCliente;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas(){
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas){
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdModalidadePagamentoCliente.
	 *
	 * @return cdModalidadePagamentoCliente
	 */
	public Integer getCdModalidadePagamentoCliente(){
		return cdModalidadePagamentoCliente;
	}

	/**
	 * Set: cdModalidadePagamentoCliente.
	 *
	 * @param cdModalidadePagamentoCliente the cd modalidade pagamento cliente
	 */
	public void setCdModalidadePagamentoCliente(Integer cdModalidadePagamentoCliente){
		this.cdModalidadePagamentoCliente = cdModalidadePagamentoCliente;
	}

	/**
	 * Get: dsModalidadePagamentoCliente.
	 *
	 * @return dsModalidadePagamentoCliente
	 */
	public String getDsModalidadePagamentoCliente(){
		return dsModalidadePagamentoCliente;
	}

	/**
	 * Set: dsModalidadePagamentoCliente.
	 *
	 * @param dsModalidadePagamentoCliente the ds modalidade pagamento cliente
	 */
	public void setDsModalidadePagamentoCliente(String dsModalidadePagamentoCliente){
		this.dsModalidadePagamentoCliente = dsModalidadePagamentoCliente;
	}
}