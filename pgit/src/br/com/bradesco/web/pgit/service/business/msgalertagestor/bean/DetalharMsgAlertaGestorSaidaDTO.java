/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgalertagestor.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgalertagestor.bean;

/**
 * Nome: DetalharMsgAlertaGestorSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharMsgAlertaGestorSaidaDTO {
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo cdTipoProcesso. */
	private int cdTipoProcesso;
	
	/** Atributo dsTipoProcesso. */
	private String dsTipoProcesso;
	
	/** Atributo cdFuncionario. */
	private long cdFuncionario;
	
	/** Atributo dsFuncionario. */
	private String dsFuncionario;
	
	/** Atributo cdTipoMensagem. */
	private int cdTipoMensagem;
	
	/** Atributo dsTipoMensagem. */
	private String dsTipoMensagem;
	
	/** Atributo prefixo. */
	private String prefixo;
	
	/** Atributo cdRecurso. */
	private int cdRecurso;
	
	/** Atributo dsRecurso. */
	private String dsRecurso;
	
	/** Atributo cdIdioma. */
	private int cdIdioma;
	
	/** Atributo dsIdioma. */
	private String dsIdioma;
	
	/** Atributo cdIndicadorMeioTransmissao. */
	private int cdIndicadorMeioTransmissao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;	
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdProcSist. */
	private int cdProcSist;
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: cdFuncionario.
	 *
	 * @return cdFuncionario
	 */
	public long getCdFuncionario() {
		return cdFuncionario;
	}
	
	/**
	 * Set: cdFuncionario.
	 *
	 * @param cdFuncionario the cd funcionario
	 */
	public void setCdFuncionario(long cdFuncionario) {
		this.cdFuncionario = cdFuncionario;
	}
	
	/**
	 * Get: cdIdioma.
	 *
	 * @return cdIdioma
	 */
	public int getCdIdioma() {
		return cdIdioma;
	}
	
	/**
	 * Set: cdIdioma.
	 *
	 * @param cdIdioma the cd idioma
	 */
	public void setCdIdioma(int cdIdioma) {
		this.cdIdioma = cdIdioma;
	}
	
	/**
	 * Get: cdRecurso.
	 *
	 * @return cdRecurso
	 */
	public int getCdRecurso() {
		return cdRecurso;
	}
	
	/**
	 * Set: cdRecurso.
	 *
	 * @param cdRecurso the cd recurso
	 */
	public void setCdRecurso(int cdRecurso) {
		this.cdRecurso = cdRecurso;
	}
	
	/**
	 * Get: cdTipoMensagem.
	 *
	 * @return cdTipoMensagem
	 */
	public int getCdTipoMensagem() {
		return cdTipoMensagem;
	}
	
	/**
	 * Set: cdTipoMensagem.
	 *
	 * @param cdTipoMensagem the cd tipo mensagem
	 */
	public void setCdTipoMensagem(int cdTipoMensagem) {
		this.cdTipoMensagem = cdTipoMensagem;
	}
	
	/**
	 * Get: cdTipoProcesso.
	 *
	 * @return cdTipoProcesso
	 */
	public int getCdTipoProcesso() {
		return cdTipoProcesso;
	}
	
	/**
	 * Set: cdTipoProcesso.
	 *
	 * @param cdTipoProcesso the cd tipo processo
	 */
	public void setCdTipoProcesso(int cdTipoProcesso) {
		this.cdTipoProcesso = cdTipoProcesso;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: dsFuncionario.
	 *
	 * @return dsFuncionario
	 */
	public String getDsFuncionario() {
		return dsFuncionario;
	}
	
	/**
	 * Set: dsFuncionario.
	 *
	 * @param dsFuncionario the ds funcionario
	 */
	public void setDsFuncionario(String dsFuncionario) {
		this.dsFuncionario = dsFuncionario;
	}
	
	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}
	
	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}
	
	/**
	 * Get: dsRecurso.
	 *
	 * @return dsRecurso
	 */
	public String getDsRecurso() {
		return dsRecurso;
	}
	
	/**
	 * Set: dsRecurso.
	 *
	 * @param dsRecurso the ds recurso
	 */
	public void setDsRecurso(String dsRecurso) {
		this.dsRecurso = dsRecurso;
	}
	
	/**
	 * Get: dsTipoMensagem.
	 *
	 * @return dsTipoMensagem
	 */
	public String getDsTipoMensagem() {
		return dsTipoMensagem;
	}
	
	/**
	 * Set: dsTipoMensagem.
	 *
	 * @param dsTipoMensagem the ds tipo mensagem
	 */
	public void setDsTipoMensagem(String dsTipoMensagem) {
		this.dsTipoMensagem = dsTipoMensagem;
	}
	
	/**
	 * Get: dsTipoProcesso.
	 *
	 * @return dsTipoProcesso
	 */
	public String getDsTipoProcesso() {
		return dsTipoProcesso;
	}
	
	/**
	 * Set: dsTipoProcesso.
	 *
	 * @param dsTipoProcesso the ds tipo processo
	 */
	public void setDsTipoProcesso(String dsTipoProcesso) {
		this.dsTipoProcesso = dsTipoProcesso;
	}
	
	/**
	 * Get: prefixo.
	 *
	 * @return prefixo
	 */
	public String getPrefixo() {
		return prefixo;
	}
	
	/**
	 * Set: prefixo.
	 *
	 * @param prefixo the prefixo
	 */
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	
	/**
	 * Get: cdIndicadorMeioTransmissao.
	 *
	 * @return cdIndicadorMeioTransmissao
	 */
	public int getCdIndicadorMeioTransmissao() {
		return cdIndicadorMeioTransmissao;
	}
	
	/**
	 * Set: cdIndicadorMeioTransmissao.
	 *
	 * @param cdIndicadorMeioTransmissao the cd indicador meio transmissao
	 */
	public void setCdIndicadorMeioTransmissao(int cdIndicadorMeioTransmissao) {
		this.cdIndicadorMeioTransmissao = cdIndicadorMeioTransmissao;
	}
	
	/**
	 * Get: cdProcSist.
	 *
	 * @return cdProcSist
	 */
	public int getCdProcSist() {
		return cdProcSist;
	}
	
	/**
	 * Set: cdProcSist.
	 *
	 * @param cdProcSist the cd proc sist
	 */
	public void setCdProcSist(int cdProcSist) {
		this.cdProcSist = cdProcSist;
	}

	
	

}
