/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

/**
 * Nome: ConsultarParticipanteContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarParticipanteContratoEntradaDTO {
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratonegocio. */
    private Long nrSequenciaContratonegocio;
    
    /** Atributo cdControleCpfParticipante. */
    private Integer cdControleCpfParticipante;
    
    /** Atributo cdCpfCnpjParticipante. */
    private Long cdCpfCnpjParticipante;
    
    /** Atributo cdFilialCnpjParticipante. */
    private Integer cdFilialCnpjParticipante;
    
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratonegocio.
	 *
	 * @return nrSequenciaContratonegocio
	 */
	public Long getNrSequenciaContratonegocio() {
		return nrSequenciaContratonegocio;
	}
	
	/**
	 * Set: nrSequenciaContratonegocio.
	 *
	 * @param nrSequenciaContratonegocio the nr sequencia contratonegocio
	 */
	public void setNrSequenciaContratonegocio(Long nrSequenciaContratonegocio) {
		this.nrSequenciaContratonegocio = nrSequenciaContratonegocio;
	}
	
	/**
	 * Get: cdControleCpfParticipante.
	 *
	 * @return cdControleCpfParticipante
	 */
	public Integer getCdControleCpfParticipante() {
		return cdControleCpfParticipante;
	}
	
	/**
	 * Set: cdControleCpfParticipante.
	 *
	 * @param cdControleCpfParticipante the cd controle cpf participante
	 */
	public void setCdControleCpfParticipante(Integer cdControleCpfParticipante) {
		this.cdControleCpfParticipante = cdControleCpfParticipante;
	}
	
	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public Long getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(Long cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdFilialCnpjParticipante.
	 *
	 * @return cdFilialCnpjParticipante
	 */
	public Integer getCdFilialCnpjParticipante() {
		return cdFilialCnpjParticipante;
	}
	
	/**
	 * Set: cdFilialCnpjParticipante.
	 *
	 * @param cdFilialCnpjParticipante the cd filial cnpj participante
	 */
	public void setCdFilialCnpjParticipante(Integer cdFilialCnpjParticipante) {
		this.cdFilialCnpjParticipante = cdFilialCnpjParticipante;
	}
	
	
    
}
