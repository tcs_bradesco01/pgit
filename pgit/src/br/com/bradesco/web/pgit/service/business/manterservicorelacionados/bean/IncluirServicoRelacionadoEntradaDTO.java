/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean;

/**
 * Nome: IncluirServicoRelacionadoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class IncluirServicoRelacionadoEntradaDTO {

	private Integer codTipoServico;
	private Integer codModalidadeServico;
	private Integer codTipoRelacionamento;
	private Integer codServicoComposto;
	private Integer codNaturezaServico;
	private Integer nrOrdenacaoServicoRelacionado;
	private Integer cdIndicadorServicoNegociavel;
	private Integer cdIndicadorNegociavelServico;
	private Integer cdIndicadorContigenciaServico;
	private Integer cdIndicadorManutencaoServico;

	public Integer getCodTipoServico() {
		return codTipoServico;
	}

	public void setCodTipoServico(Integer codTipoServico) {
		this.codTipoServico = codTipoServico;
	}

	public Integer getCodModalidadeServico() {
		return codModalidadeServico;
	}

	public void setCodModalidadeServico(Integer codModalidadeServico) {
		this.codModalidadeServico = codModalidadeServico;
	}

	public Integer getCodTipoRelacionamento() {
		return codTipoRelacionamento;
	}

	public void setCodTipoRelacionamento(Integer codTipoRelacionamento) {
		this.codTipoRelacionamento = codTipoRelacionamento;
	}

	public Integer getCodServicoComposto() {
		return codServicoComposto;
	}

	public void setCodServicoComposto(Integer codServicoComposto) {
		this.codServicoComposto = codServicoComposto;
	}

	public Integer getCodNaturezaServico() {
		return codNaturezaServico;
	}

	public void setCodNaturezaServico(Integer codNaturezaServico) {
		this.codNaturezaServico = codNaturezaServico;
	}

	public Integer getNrOrdenacaoServicoRelacionado() {
		return nrOrdenacaoServicoRelacionado;
	}

	public void setNrOrdenacaoServicoRelacionado(
			Integer nrOrdenacaoServicoRelacionado) {
		this.nrOrdenacaoServicoRelacionado = nrOrdenacaoServicoRelacionado;
	}

	public Integer getCdIndicadorServicoNegociavel() {
		return cdIndicadorServicoNegociavel;
	}

	public void setCdIndicadorServicoNegociavel(
			Integer cdIndicadorServicoNegociavel) {
		this.cdIndicadorServicoNegociavel = cdIndicadorServicoNegociavel;
	}

	public Integer getCdIndicadorNegociavelServico() {
		return cdIndicadorNegociavelServico;
	}

	public void setCdIndicadorNegociavelServico(
			Integer cdIndicadorNegociavelServico) {
		this.cdIndicadorNegociavelServico = cdIndicadorNegociavelServico;
	}

	public Integer getCdIndicadorContigenciaServico() {
		return cdIndicadorContigenciaServico;
	}

	public void setCdIndicadorContigenciaServico(
			Integer cdIndicadorContigenciaServico) {
		this.cdIndicadorContigenciaServico = cdIndicadorContigenciaServico;
	}

	public Integer getCdIndicadorManutencaoServico() {
		return cdIndicadorManutencaoServico;
	}

	public void setCdIndicadorManutencaoServico(
			Integer cdIndicadorManutencaoServico) {
		this.cdIndicadorManutencaoServico = cdIndicadorManutencaoServico;
	}

}
