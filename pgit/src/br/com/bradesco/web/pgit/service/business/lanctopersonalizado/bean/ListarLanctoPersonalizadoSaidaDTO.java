/*
 * Nome: br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean;

/**
 * Nome: ListarLanctoPersonalizadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarLanctoPersonalizadoSaidaDTO {
	
	/** Atributo codLancamento. */
	private Integer codLancamento;
	
	/** Atributo restricao. */
	private Integer restricao;
	
	/** Atributo tipoLancamento. */
	private Integer tipoLancamento;
	
	/** Atributo cdLancDebito. */
	private Integer cdLancDebito;
	
	/** Atributo dsLancDebito. */
	private String dsLancDebito;
	
	/** Atributo cdLancCredito. */
	private Integer cdLancCredito;
	
	/** Atributo dsLancCredito. */
	private String dsLancCredito;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdTipoModalidade. */
	private Integer cdTipoModalidade;
	
	/** Atributo dsTipoModalidade. */
	private String dsTipoModalidade;
	
	/** Atributo cdTipoRelacionamento. */
	private Integer cdTipoRelacionamento;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdLancCredito.
	 *
	 * @return cdLancCredito
	 */
	public Integer getCdLancCredito() {
		return cdLancCredito;
	}
	
	/**
	 * Set: cdLancCredito.
	 *
	 * @param cdLancCredito the cd lanc credito
	 */
	public void setCdLancCredito(Integer cdLancCredito) {
		this.cdLancCredito = cdLancCredito;
	}
	
	/**
	 * Get: cdLancDebito.
	 *
	 * @return cdLancDebito
	 */
	public Integer getCdLancDebito() {
		return cdLancDebito;
	}
	
	/**
	 * Set: cdLancDebito.
	 *
	 * @param cdLancDebito the cd lanc debito
	 */
	public void setCdLancDebito(Integer cdLancDebito) {
		this.cdLancDebito = cdLancDebito;
	}
	
	/**
	 * Get: cdTipoModalidade.
	 *
	 * @return cdTipoModalidade
	 */
	public Integer getCdTipoModalidade() {
		return cdTipoModalidade;
	}
	
	/**
	 * Set: cdTipoModalidade.
	 *
	 * @param cdTipoModalidade the cd tipo modalidade
	 */
	public void setCdTipoModalidade(Integer cdTipoModalidade) {
		this.cdTipoModalidade = cdTipoModalidade;
	}
	
	/**
	 * Get: cdTipoRelacionamento.
	 *
	 * @return cdTipoRelacionamento
	 */
	public Integer getCdTipoRelacionamento() {
		return cdTipoRelacionamento;
	}
	
	/**
	 * Set: cdTipoRelacionamento.
	 *
	 * @param cdTipoRelacionamento the cd tipo relacionamento
	 */
	public void setCdTipoRelacionamento(Integer cdTipoRelacionamento) {
		this.cdTipoRelacionamento = cdTipoRelacionamento;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codLancamento.
	 *
	 * @return codLancamento
	 */
	public Integer getCodLancamento() {
		return codLancamento;
	}
	
	/**
	 * Set: codLancamento.
	 *
	 * @param codLancamento the cod lancamento
	 */
	public void setCodLancamento(Integer codLancamento) {
		this.codLancamento = codLancamento;
	}
	
	/**
	 * Get: dsLancCredito.
	 *
	 * @return dsLancCredito
	 */
	public String getDsLancCredito() {
		return dsLancCredito;
	}
	
	/**
	 * Set: dsLancCredito.
	 *
	 * @param dsLancCredito the ds lanc credito
	 */
	public void setDsLancCredito(String dsLancCredito) {
		this.dsLancCredito = dsLancCredito;
	}
	
	/**
	 * Get: dsLancDebito.
	 *
	 * @return dsLancDebito
	 */
	public String getDsLancDebito() {
		return dsLancDebito;
	}
	
	/**
	 * Set: dsLancDebito.
	 *
	 * @param dsLancDebito the ds lanc debito
	 */
	public void setDsLancDebito(String dsLancDebito) {
		this.dsLancDebito = dsLancDebito;
	}
	
	/**
	 * Get: dsTipoModalidade.
	 *
	 * @return dsTipoModalidade
	 */
	public String getDsTipoModalidade() {
		return dsTipoModalidade;
	}
	
	/**
	 * Set: dsTipoModalidade.
	 *
	 * @param dsTipoModalidade the ds tipo modalidade
	 */
	public void setDsTipoModalidade(String dsTipoModalidade) {
		this.dsTipoModalidade = dsTipoModalidade;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public Integer getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(Integer restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: tipoLancamento.
	 *
	 * @return tipoLancamento
	 */
	public Integer getTipoLancamento() {
		return tipoLancamento;
	}
	
	/**
	 * Set: tipoLancamento.
	 *
	 * @param tipoLancamento the tipo lancamento
	 */
	public void setTipoLancamento(Integer tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

}
