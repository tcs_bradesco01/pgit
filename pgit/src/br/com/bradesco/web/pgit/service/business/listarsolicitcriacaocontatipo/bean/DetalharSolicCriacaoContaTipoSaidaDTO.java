/*
 * Nome: br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: DetalharSolicCriacaoContaTipoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolicCriacaoContaTipoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdCpfCnpjPssoa. */
    private Long cdCpfCnpjPssoa;
    
    /** Atributo cdFilialCnpjPssoa. */
    private Integer cdFilialCnpjPssoa;
    
    /** Atributo cdControleCnpjPssoa. */
    private Integer cdControleCnpjPssoa;
    
    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;
    
    /** Atributo dsNomePssoa. */
    private String dsNomePssoa;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo cdMotvoSolicit. */
    private Integer cdMotvoSolicit;
    
    /** Atributo dsMotvoSolicit. */
    private String dsMotvoSolicit;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo dsBanco. */
    private String dsBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo cdGrupoContabil. */
    private Integer cdGrupoContabil;
    
    /** Atributo cdSubGrupoContabil. */
    private Integer cdSubGrupoContabil;
    
    /** Atributo dsGrupoContabil. */
    private String dsGrupoContabil;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo dtHoraInclusao. */
    private String dtHoraInclusao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo dsTipoCanal. */
	private String dsTipoCanal;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdControleCnpjPssoa.
	 *
	 * @return cdControleCnpjPssoa
	 */
	public Integer getCdControleCnpjPssoa() {
		return cdControleCnpjPssoa;
	}
	
	/**
	 * Set: cdControleCnpjPssoa.
	 *
	 * @param cdControleCnpjPssoa the cd controle cnpj pssoa
	 */
	public void setCdControleCnpjPssoa(Integer cdControleCnpjPssoa) {
		this.cdControleCnpjPssoa = cdControleCnpjPssoa;
	}
	
	/**
	 * Get: cdCpfCnpjPssoa.
	 *
	 * @return cdCpfCnpjPssoa
	 */
	public Long getCdCpfCnpjPssoa() {
		return cdCpfCnpjPssoa;
	}
	
	/**
	 * Set: cdCpfCnpjPssoa.
	 *
	 * @param cdCpfCnpjPssoa the cd cpf cnpj pssoa
	 */
	public void setCdCpfCnpjPssoa(Long cdCpfCnpjPssoa) {
		this.cdCpfCnpjPssoa = cdCpfCnpjPssoa;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdFilialCnpjPssoa.
	 *
	 * @return cdFilialCnpjPssoa
	 */
	public Integer getCdFilialCnpjPssoa() {
		return cdFilialCnpjPssoa;
	}
	
	/**
	 * Set: cdFilialCnpjPssoa.
	 *
	 * @param cdFilialCnpjPssoa the cd filial cnpj pssoa
	 */
	public void setCdFilialCnpjPssoa(Integer cdFilialCnpjPssoa) {
		this.cdFilialCnpjPssoa = cdFilialCnpjPssoa;
	}
	
	/**
	 * Get: cdGrupoContabil.
	 *
	 * @return cdGrupoContabil
	 */
	public Integer getCdGrupoContabil() {
		return cdGrupoContabil;
	}
	
	/**
	 * Set: cdGrupoContabil.
	 *
	 * @param cdGrupoContabil the cd grupo contabil
	 */
	public void setCdGrupoContabil(Integer cdGrupoContabil) {
		this.cdGrupoContabil = cdGrupoContabil;
	}
	
	/**
	 * Get: cdMotvoSolicit.
	 *
	 * @return cdMotvoSolicit
	 */
	public Integer getCdMotvoSolicit() {
		return cdMotvoSolicit;
	}
	
	/**
	 * Set: cdMotvoSolicit.
	 *
	 * @param cdMotvoSolicit the cd motvo solicit
	 */
	public void setCdMotvoSolicit(Integer cdMotvoSolicit) {
		this.cdMotvoSolicit = cdMotvoSolicit;
	}
	
	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdSubGrupoContabil.
	 *
	 * @return cdSubGrupoContabil
	 */
	public Integer getCdSubGrupoContabil() {
		return cdSubGrupoContabil;
	}
	
	/**
	 * Set: cdSubGrupoContabil.
	 *
	 * @param cdSubGrupoContabil the cd sub grupo contabil
	 */
	public void setCdSubGrupoContabil(Integer cdSubGrupoContabil) {
		this.cdSubGrupoContabil = cdSubGrupoContabil;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: dsGrupoContabil.
	 *
	 * @return dsGrupoContabil
	 */
	public String getDsGrupoContabil() {
		return dsGrupoContabil;
	}
	
	/**
	 * Set: dsGrupoContabil.
	 *
	 * @param dsGrupoContabil the ds grupo contabil
	 */
	public void setDsGrupoContabil(String dsGrupoContabil) {
		this.dsGrupoContabil = dsGrupoContabil;
	}
	
	/**
	 * Get: dsMotvoSolicit.
	 *
	 * @return dsMotvoSolicit
	 */
	public String getDsMotvoSolicit() {
		return dsMotvoSolicit;
	}
	
	/**
	 * Set: dsMotvoSolicit.
	 *
	 * @param dsMotvoSolicit the ds motvo solicit
	 */
	public void setDsMotvoSolicit(String dsMotvoSolicit) {
		this.dsMotvoSolicit = dsMotvoSolicit;
	}
	
	/**
	 * Get: dsNomePssoa.
	 *
	 * @return dsNomePssoa
	 */
	public String getDsNomePssoa() {
		return dsNomePssoa;
	}
	
	/**
	 * Set: dsNomePssoa.
	 *
	 * @param dsNomePssoa the ds nome pssoa
	 */
	public void setDsNomePssoa(String dsNomePssoa) {
		this.dsNomePssoa = dsNomePssoa;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils.formatarCpfCnpj(cdCpfCnpjPssoa, cdFilialCnpjPssoa, cdControleCnpjPssoa);
	}
	
	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}
	
	/**
	 * Get: dtHoraInclusao.
	 *
	 * @return dtHoraInclusao
	 */
	public String getDtHoraInclusao() {
		return dtHoraInclusao;
	}
	
	/**
	 * Set: dtHoraInclusao.
	 *
	 * @param dtHoraInclusao the dt hora inclusao
	 */
	public void setDtHoraInclusao(String dtHoraInclusao) {
		this.dtHoraInclusao = dtHoraInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: dsTipoCanal.
	 *
	 * @return dsTipoCanal
	 */
	public String getDsTipoCanal() {
		return dsTipoCanal;
	}
	
	/**
	 * Set: dsTipoCanal.
	 *
	 * @param dsTipoCanal the ds tipo canal
	 */
	public void setDsTipoCanal(String dsTipoCanal) {
		this.dsTipoCanal = dsTipoCanal;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
}