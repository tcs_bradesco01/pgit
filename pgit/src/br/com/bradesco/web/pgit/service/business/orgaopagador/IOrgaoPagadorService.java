/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.orgaopagador;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.HistoricoOrgaoPagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.HistoricoOrgaoPagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorDetalheSaidaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterOrgaoPagador
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IOrgaoPagadorService {

     /**
      * Listar orgao pagador.
      *
      * @param orgaoPagadorEntradaDTO the orgao pagador entrada dto
      * @return the list< orgao pagador saida dt o>
      */
     List<OrgaoPagadorSaidaDTO> listarOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO);

     /**
      * Incluir orgao pagador.
      *
      * @param orgaoPagadorEntradaDTO the orgao pagador entrada dto
      * @return the orgao pagador saida dto
      */
     OrgaoPagadorSaidaDTO incluirOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO);

     /**
      * Excluir orgao pagador.
      *
      * @param orgaoPagadorEntradaDTO the orgao pagador entrada dto
      * @return the orgao pagador saida dto
      */
     OrgaoPagadorSaidaDTO excluirOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO);

     /**
      * Alterar orgao pagador.
      *
      * @param orgaoPagadorEntradaDTO the orgao pagador entrada dto
      * @return the orgao pagador saida dto
      */
     OrgaoPagadorSaidaDTO alterarOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO);

     /**
      * Detalhar orgao pagador.
      *
      * @param orgaoPagadorEntradaDTO the orgao pagador entrada dto
      * @return the orgao pagador detalhe saida dto
      */
     OrgaoPagadorDetalheSaidaDTO detalharOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO);

     /**
      * Bloquear orgao pagador.
      *
      * @param orgaoPagadorEntradaDTO the orgao pagador entrada dto
      * @return the orgao pagador saida dto
      */
     OrgaoPagadorSaidaDTO bloquearOrgaoPagador(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO);
    
     /**
      * Listar historico orgao pagador.
      *
      * @param historicoOrgaoPagadorEntradaDTO the historico orgao pagador entrada dto
      * @return the list< historico orgao pagador saida dt o>
      */
     List<HistoricoOrgaoPagadorSaidaDTO> listarHistoricoOrgaoPagador(HistoricoOrgaoPagadorEntradaDTO historicoOrgaoPagadorEntradaDTO);
    
     /**
      * Detalhar historico orgao pagador.
      *
      * @param historicoOrgaoPagadorEntradaDTO the historico orgao pagador entrada dto
      * @return the list< historico orgao pagador saida dt o>
      */
     List<HistoricoOrgaoPagadorSaidaDTO> detalharHistoricoOrgaoPagador(HistoricoOrgaoPagadorEntradaDTO historicoOrgaoPagadorEntradaDTO);
}

