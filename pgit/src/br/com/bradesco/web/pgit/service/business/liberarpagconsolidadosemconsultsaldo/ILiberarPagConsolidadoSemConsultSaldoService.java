/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.ConsultarLibPagtosConsSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.DetalharLibPagtosConsSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.DetalharLibPagtosConsSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarParcialSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: LiberarPagConsolidadoSemConsultSaldo
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ILiberarPagConsolidadoSemConsultSaldoService {

	/**
	 * Detalhar lib pagtos cons sem consulta saldo.
	 *
	 * @param detalharLibPagtosConsSemConsultaSaldoEntradaDTO the detalhar lib pagtos cons sem consulta saldo entrada dto
	 * @return the detalhar lib pagtos cons sem consulta saldo saida dto
	 */
	DetalharLibPagtosConsSemConsultaSaldoSaidaDTO detalharLibPagtosConsSemConsultaSaldo(DetalharLibPagtosConsSemConsultaSaldoEntradaDTO detalharLibPagtosConsSemConsultaSaldoEntradaDTO);
	
	/**
	 * Consultar lib pagtos cons sem consulta saldo.
	 *
	 * @param consultarLibPagtosConsSemConsultaSaldoEntradaDTO the consultar lib pagtos cons sem consulta saldo entrada dto
	 * @return the list< consultar lib pagtos cons sem consulta saldo saida dt o>
	 */
	List<ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO> consultarLibPagtosConsSemConsultaSaldo(ConsultarLibPagtosConsSemConsultaSaldoEntradaDTO consultarLibPagtosConsSemConsultaSaldoEntradaDTO);
	
	/**
	 * Liberar parcial.
	 *
	 * @param liberarParcialEntradaDTO the liberar parcial entrada dto
	 * @return the liberar parcial saida dto
	 */
	LiberarParcialSaidaDTO liberarParcial(LiberarParcialEntradaDTO liberarParcialEntradaDTO);
	
	/**
	 * Liberar integral.
	 *
	 * @param liberarIntegralEntradaDTO the liberar integral entrada dto
	 * @return the liberar integral saida dto
	 */
	LiberarIntegralSaidaDTO liberarIntegral(LiberarIntegralEntradaDTO liberarIntegralEntradaDTO);
}

