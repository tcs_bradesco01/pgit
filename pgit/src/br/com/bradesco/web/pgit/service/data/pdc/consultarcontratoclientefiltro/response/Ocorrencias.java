/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _dsPessoaJuridica
     */
    private java.lang.String _dsPessoaJuridica;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _dsTipoParticipacaoPessoa
     */
    private java.lang.String _dsTipoParticipacaoPessoa;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _dsTipoContrato
     */
    private java.lang.String _dsTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdPessoaParticipante
     */
    private long _cdPessoaParticipante = 0;

    /**
     * keeps track of state for field: _cdPessoaParticipante
     */
    private boolean _has_cdPessoaParticipante;

    /**
     * Field _dsPessoaParticipante
     */
    private java.lang.String _dsPessoaParticipante;

    /**
     * Field _cdCorpoCnpjRepresentante
     */
    private long _cdCorpoCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdCorpoCnpjRepresentante
     */
    private boolean _has_cdCorpoCnpjRepresentante;

    /**
     * Field _cdFilialCnpjRepresentante
     */
    private int _cdFilialCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRepresentante
     */
    private boolean _has_cdFilialCnpjRepresentante;

    /**
     * Field _cdControleCnpjRepresentante
     */
    private int _cdControleCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdControleCnpjRepresentante
     */
    private boolean _has_cdControleCnpjRepresentante;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _dsSituacaoContrato
     */
    private java.lang.String _dsSituacaoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCnpjRepresentante
     * 
     */
    public void deleteCdControleCnpjRepresentante()
    {
        this._has_cdControleCnpjRepresentante= false;
    } //-- void deleteCdControleCnpjRepresentante() 

    /**
     * Method deleteCdCorpoCnpjRepresentante
     * 
     */
    public void deleteCdCorpoCnpjRepresentante()
    {
        this._has_cdCorpoCnpjRepresentante= false;
    } //-- void deleteCdCorpoCnpjRepresentante() 

    /**
     * Method deleteCdFilialCnpjRepresentante
     * 
     */
    public void deleteCdFilialCnpjRepresentante()
    {
        this._has_cdFilialCnpjRepresentante= false;
    } //-- void deleteCdFilialCnpjRepresentante() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaParticipante
     * 
     */
    public void deleteCdPessoaParticipante()
    {
        this._has_cdPessoaParticipante= false;
    } //-- void deleteCdPessoaParticipante() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Returns the value of field 'cdControleCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjRepresentante'.
     */
    public int getCdControleCnpjRepresentante()
    {
        return this._cdControleCnpjRepresentante;
    } //-- int getCdControleCnpjRepresentante() 

    /**
     * Returns the value of field 'cdCorpoCnpjRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCnpjRepresentante'.
     */
    public long getCdCorpoCnpjRepresentante()
    {
        return this._cdCorpoCnpjRepresentante;
    } //-- long getCdCorpoCnpjRepresentante() 

    /**
     * Returns the value of field 'cdFilialCnpjRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRepresentante'.
     */
    public int getCdFilialCnpjRepresentante()
    {
        return this._cdFilialCnpjRepresentante;
    } //-- int getCdFilialCnpjRepresentante() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaParticipante'.
     * 
     * @return long
     * @return the value of field 'cdPessoaParticipante'.
     */
    public long getCdPessoaParticipante()
    {
        return this._cdPessoaParticipante;
    } //-- long getCdPessoaParticipante() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsPessoaJuridica'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridica'.
     */
    public java.lang.String getDsPessoaJuridica()
    {
        return this._dsPessoaJuridica;
    } //-- java.lang.String getDsPessoaJuridica() 

    /**
     * Returns the value of field 'dsPessoaParticipante'.
     * 
     * @return String
     * @return the value of field 'dsPessoaParticipante'.
     */
    public java.lang.String getDsPessoaParticipante()
    {
        return this._dsPessoaParticipante;
    } //-- java.lang.String getDsPessoaParticipante() 

    /**
     * Returns the value of field 'dsSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContrato'.
     */
    public java.lang.String getDsSituacaoContrato()
    {
        return this._dsSituacaoContrato;
    } //-- java.lang.String getDsSituacaoContrato() 

    /**
     * Returns the value of field 'dsTipoContrato'.
     * 
     * @return String
     * @return the value of field 'dsTipoContrato'.
     */
    public java.lang.String getDsTipoContrato()
    {
        return this._dsTipoContrato;
    } //-- java.lang.String getDsTipoContrato() 

    /**
     * Returns the value of field 'dsTipoParticipacaoPessoa'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipacaoPessoa'.
     */
    public java.lang.String getDsTipoParticipacaoPessoa()
    {
        return this._dsTipoParticipacaoPessoa;
    } //-- java.lang.String getDsTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Method hasCdControleCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjRepresentante()
    {
        return this._has_cdControleCnpjRepresentante;
    } //-- boolean hasCdControleCnpjRepresentante() 

    /**
     * Method hasCdCorpoCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCnpjRepresentante()
    {
        return this._has_cdCorpoCnpjRepresentante;
    } //-- boolean hasCdCorpoCnpjRepresentante() 

    /**
     * Method hasCdFilialCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRepresentante()
    {
        return this._has_cdFilialCnpjRepresentante;
    } //-- boolean hasCdFilialCnpjRepresentante() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaParticipante()
    {
        return this._has_cdPessoaParticipante;
    } //-- boolean hasCdPessoaParticipante() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCnpjRepresentante'.
     * 
     * @param cdControleCnpjRepresentante the value of field
     * 'cdControleCnpjRepresentante'.
     */
    public void setCdControleCnpjRepresentante(int cdControleCnpjRepresentante)
    {
        this._cdControleCnpjRepresentante = cdControleCnpjRepresentante;
        this._has_cdControleCnpjRepresentante = true;
    } //-- void setCdControleCnpjRepresentante(int) 

    /**
     * Sets the value of field 'cdCorpoCnpjRepresentante'.
     * 
     * @param cdCorpoCnpjRepresentante the value of field
     * 'cdCorpoCnpjRepresentante'.
     */
    public void setCdCorpoCnpjRepresentante(long cdCorpoCnpjRepresentante)
    {
        this._cdCorpoCnpjRepresentante = cdCorpoCnpjRepresentante;
        this._has_cdCorpoCnpjRepresentante = true;
    } //-- void setCdCorpoCnpjRepresentante(long) 

    /**
     * Sets the value of field 'cdFilialCnpjRepresentante'.
     * 
     * @param cdFilialCnpjRepresentante the value of field
     * 'cdFilialCnpjRepresentante'.
     */
    public void setCdFilialCnpjRepresentante(int cdFilialCnpjRepresentante)
    {
        this._cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
        this._has_cdFilialCnpjRepresentante = true;
    } //-- void setCdFilialCnpjRepresentante(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaParticipante'.
     * 
     * @param cdPessoaParticipante the value of field
     * 'cdPessoaParticipante'.
     */
    public void setCdPessoaParticipante(long cdPessoaParticipante)
    {
        this._cdPessoaParticipante = cdPessoaParticipante;
        this._has_cdPessoaParticipante = true;
    } //-- void setCdPessoaParticipante(long) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridica'.
     * 
     * @param dsPessoaJuridica the value of field 'dsPessoaJuridica'
     */
    public void setDsPessoaJuridica(java.lang.String dsPessoaJuridica)
    {
        this._dsPessoaJuridica = dsPessoaJuridica;
    } //-- void setDsPessoaJuridica(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaParticipante'.
     * 
     * @param dsPessoaParticipante the value of field
     * 'dsPessoaParticipante'.
     */
    public void setDsPessoaParticipante(java.lang.String dsPessoaParticipante)
    {
        this._dsPessoaParticipante = dsPessoaParticipante;
    } //-- void setDsPessoaParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContrato'.
     * 
     * @param dsSituacaoContrato the value of field
     * 'dsSituacaoContrato'.
     */
    public void setDsSituacaoContrato(java.lang.String dsSituacaoContrato)
    {
        this._dsSituacaoContrato = dsSituacaoContrato;
    } //-- void setDsSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContrato'.
     * 
     * @param dsTipoContrato the value of field 'dsTipoContrato'.
     */
    public void setDsTipoContrato(java.lang.String dsTipoContrato)
    {
        this._dsTipoContrato = dsTipoContrato;
    } //-- void setDsTipoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipacaoPessoa'.
     * 
     * @param dsTipoParticipacaoPessoa the value of field
     * 'dsTipoParticipacaoPessoa'.
     */
    public void setDsTipoParticipacaoPessoa(java.lang.String dsTipoParticipacaoPessoa)
    {
        this._dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
    } //-- void setDsTipoParticipacaoPessoa(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarcontratoclientefiltro.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
