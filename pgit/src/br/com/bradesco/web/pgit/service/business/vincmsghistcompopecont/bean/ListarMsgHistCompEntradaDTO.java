/*
 * Nome: br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean;

/**
 * Nome: ListarMsgHistCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMsgHistCompEntradaDTO {
	
	/** Atributo cdMensagemLinhaExtrato. */
	private int cdMensagemLinhaExtrato; //C�digo Hist�rico Complementar
	
	/** Atributo cdIndicadorRestricaoContrato. */
	private int cdIndicadorRestricaoContrato;
	
	/** Atributo cdTipoMensagemExtrato. */
	private int cdTipoMensagemExtrato;
	
	/** Atributo cdProdutoServicoOperacao. */
	private int cdProdutoServicoOperacao; //Tipo Servi�o	
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private int cdProdutoOperacaoRelacionado; //Modalidade Servi�o 	
	
	/** Atributo cdRelacionamentoProduto. */
	private int cdRelacionamentoProduto; // Tipo Relacionamento (campo oculto no combo Modalidade Servi�o
	
	/**
	 * Get: cdIndicadorRestricaoContrato.
	 *
	 * @return cdIndicadorRestricaoContrato
	 */
	public int getCdIndicadorRestricaoContrato() {
		return cdIndicadorRestricaoContrato;
	}
	
	/**
	 * Set: cdIndicadorRestricaoContrato.
	 *
	 * @param cdIndicadorRestricaoContrato the cd indicador restricao contrato
	 */
	public void setCdIndicadorRestricaoContrato(int cdIndicadorRestricaoContrato) {
		this.cdIndicadorRestricaoContrato = cdIndicadorRestricaoContrato;
	}
	
	/**
	 * Get: cdMensagemLinhaExtrato.
	 *
	 * @return cdMensagemLinhaExtrato
	 */
	public int getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}
	
	/**
	 * Set: cdMensagemLinhaExtrato.
	 *
	 * @param cdMensagemLinhaExtrato the cd mensagem linha extrato
	 */
	public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public int getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public int getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public int getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(int cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: cdTipoMensagemExtrato.
	 *
	 * @return cdTipoMensagemExtrato
	 */
	public int getCdTipoMensagemExtrato() {
		return cdTipoMensagemExtrato;
	}
	
	/**
	 * Set: cdTipoMensagemExtrato.
	 *
	 * @param cdTipoMensagemExtrato the cd tipo mensagem extrato
	 */
	public void setCdTipoMensagemExtrato(int cdTipoMensagemExtrato) {
		this.cdTipoMensagemExtrato = cdTipoMensagemExtrato;
	}
	
	
}
