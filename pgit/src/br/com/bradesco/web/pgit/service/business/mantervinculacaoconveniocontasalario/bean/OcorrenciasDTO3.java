package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3;

public class OcorrenciasDTO3 implements Serializable{

	private static final long serialVersionUID = 3266001588276489660L;
	
	private String dsLogradouroFilialQuest;
	private String dsBairroFilialQuest; 
	private Long cdMunicipioFilialQuest;
	private String dsMuncFilialQuest;
	private Integer cdCepFilialQuest; 
	private Integer cdCepComplQuesf; 
	private Integer cdUfFilialQuest; 
	private String cdSegmentoFilialQuest; 
	private Long qtdFuncFilalQuest;
	
	public OcorrenciasDTO3() {
		super();
	}

	public OcorrenciasDTO3(String dsLogradouroFilialQuest,
			String dsBairroFilialQuest, Long cdMunicipioFilialQuest,
			String dsMuncFilialQuest, Integer cdCepFilialQuest,
			Integer cdCepComplQuesf, Integer cdUfFilialQuest,
			String cdSegmentoFilialQuest, Long qtdFuncFilalQuest) {
		super();
		this.dsLogradouroFilialQuest = dsLogradouroFilialQuest;
		this.dsBairroFilialQuest = dsBairroFilialQuest;
		this.cdMunicipioFilialQuest = cdMunicipioFilialQuest;
		this.dsMuncFilialQuest = dsMuncFilialQuest;
		this.cdCepFilialQuest = cdCepFilialQuest;
		this.cdCepComplQuesf = cdCepComplQuesf;
		this.cdUfFilialQuest = cdUfFilialQuest;
		this.cdSegmentoFilialQuest = cdSegmentoFilialQuest;
		this.qtdFuncFilalQuest = qtdFuncFilalQuest;
	}
	
	public List<OcorrenciasDTO3> listarOcorrenciasDTO3(ListarConvenioContaSalarioResponse response) {
		
		List<OcorrenciasDTO3> listaOcorrenciasDTO3 = new ArrayList<OcorrenciasDTO3>();
		OcorrenciasDTO3 ocorrenciasDTO3 = new OcorrenciasDTO3();
		
		for(Ocorrencias3 retorno : response.getOcorrencias3()){
			ocorrenciasDTO3.setDsLogradouroFilialQuest(retorno.getDsLogradouroFilialQuest());
			ocorrenciasDTO3.setDsBairroFilialQuest(retorno.getDsBairroFilialQuest());
			ocorrenciasDTO3.setCdMunicipioFilialQuest(retorno.getCdMunicipioFilialQuest());
			ocorrenciasDTO3.setDsMuncFilialQuest(retorno.getDsMuncFilialQuest());
			ocorrenciasDTO3.setCdCepFilialQuest(retorno.getCdCepFilialQuest());
			ocorrenciasDTO3.setCdCepComplQuesf(retorno.getCdCepComplQuesf());
			ocorrenciasDTO3.setCdUfFilialQuest(retorno.getCdUfFilialQuest());
			ocorrenciasDTO3.setCdSegmentoFilialQuest(retorno.getCdSegmentoFilialQuest());
			ocorrenciasDTO3.setQtdFuncFilalQuest(retorno.getQtdFuncFilalQuest());
			listaOcorrenciasDTO3.add(ocorrenciasDTO3);
		}
		return listaOcorrenciasDTO3;
	}

	public String getDsLogradouroFilialQuest() {
		return dsLogradouroFilialQuest;
	}

	public void setDsLogradouroFilialQuest(String dsLogradouroFilialQuest) {
		this.dsLogradouroFilialQuest = dsLogradouroFilialQuest;
	}

	public String getDsBairroFilialQuest() {
		return dsBairroFilialQuest;
	}

	public void setDsBairroFilialQuest(String dsBairroFilialQuest) {
		this.dsBairroFilialQuest = dsBairroFilialQuest;
	}

	public Long getCdMunicipioFilialQuest() {
		return cdMunicipioFilialQuest;
	}

	public void setCdMunicipioFilialQuest(Long cdMunicipioFilialQuest) {
		this.cdMunicipioFilialQuest = cdMunicipioFilialQuest;
	}

	public String getDsMuncFilialQuest() {
		return dsMuncFilialQuest;
	}

	public void setDsMuncFilialQuest(String dsMuncFilialQuest) {
		this.dsMuncFilialQuest = dsMuncFilialQuest;
	}

	public Integer getCdCepFilialQuest() {
		return cdCepFilialQuest;
	}

	public void setCdCepFilialQuest(Integer cdCepFilialQuest) {
		this.cdCepFilialQuest = cdCepFilialQuest;
	}

	public Integer getCdCepComplQuesf() {
		return cdCepComplQuesf;
	}

	public void setCdCepComplQuesf(Integer cdCepComplQuesf) {
		this.cdCepComplQuesf = cdCepComplQuesf;
	}

	public Integer getCdUfFilialQuest() {
		return cdUfFilialQuest;
	}

	public void setCdUfFilialQuest(Integer cdUfFilialQuest) {
		this.cdUfFilialQuest = cdUfFilialQuest;
	}

	public String getCdSegmentoFilialQuest() {
		return cdSegmentoFilialQuest;
	}

	public void setCdSegmentoFilialQuest(String cdSegmentoFilialQuest) {
		this.cdSegmentoFilialQuest = cdSegmentoFilialQuest;
	}

	public Long getQtdFuncFilalQuest() {
		return qtdFuncFilalQuest;
	}

	public void setQtdFuncFilalQuest(Long qtdFuncFilalQuest) {
		this.qtdFuncFilalQuest = qtdFuncFilalQuest;
	}
	
	

}
