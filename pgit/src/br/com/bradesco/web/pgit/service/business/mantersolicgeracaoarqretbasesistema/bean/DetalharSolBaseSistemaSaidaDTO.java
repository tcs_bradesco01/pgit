/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharSolBaseSistemaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolBaseSistemaSaidaDTO {
		
		/** Atributo codMensagem. */
		private String codMensagem;
	    
    	/** Atributo mensagem. */
    	private String mensagem;
	    
    	/** Atributo nrSolicitacaoPagamento. */
    	private Integer nrSolicitacaoPagamento;
	    
    	/** Atributo hrSolicitacao. */
    	private String hrSolicitacao;
	    
    	/** Atributo dsSituacaoSolicitacao. */
    	private String dsSituacaoSolicitacao;
	    
    	/** Atributo resultadoProcessamento. */
    	private String resultadoProcessamento;
	    
    	/** Atributo hrAtendimentoSolicitacao. */
    	private String hrAtendimentoSolicitacao;
	    
    	/** Atributo qtdeRegistrosSolicitacao. */
    	private Long qtdeRegistrosSolicitacao;
	    
    	/** Atributo cdBaseSistema. */
    	private Integer cdBaseSistema;
	    
    	/** Atributo dsBaseSistema. */
    	private String dsBaseSistema;
	    
    	/** Atributo vlTarifaPadrao. */
    	private BigDecimal vlTarifaPadrao;
	    
    	/** Atributo numPercentualDescTarifa. */
    	private BigDecimal numPercentualDescTarifa;
	    
    	/** Atributo vlrTarifaAtual. */
    	private BigDecimal vlrTarifaAtual;
	    
    	/** Atributo cdCanalInclusao. */
    	private Integer cdCanalInclusao;
	    
    	/** Atributo dsCanalInclusao. */
    	private String dsCanalInclusao;
	    
    	/** Atributo cdAutenticacaoSegurancaInclusao. */
    	private String cdAutenticacaoSegurancaInclusao;
	    
    	/** Atributo nmOperacaoFluxoInlcusao. */
    	private String nmOperacaoFluxoInlcusao;
	    
    	/** Atributo hrInclusaoRegistro. */
    	private String hrInclusaoRegistro;
	    
    	/** Atributo cdCanalManutencao. */
    	private Integer cdCanalManutencao;
	    
    	/** Atributo dsCanalManutencao. */
    	private String dsCanalManutencao;
	    
    	/** Atributo cdAutenticacaoSegurancaManutencao. */
    	private String cdAutenticacaoSegurancaManutencao;
	    
    	/** Atributo nmOperacaoFluxoManutencao. */
    	private String nmOperacaoFluxoManutencao;
	    
    	/** Atributo hrManutencaoRegistro. */
    	private String hrManutencaoRegistro;
	    
	    
	    /** Atributo hrSolicitacaoFormatada. */
    	private String hrSolicitacaoFormatada;
	    
    	/** Atributo hrAtendimentoSolicitacaoFormatada. */
    	private String hrAtendimentoSolicitacaoFormatada;
	    
    	/** Atributo hrInclusaoRegistroFormatada. */
    	private String hrInclusaoRegistroFormatada;
	    
    	/** Atributo hrManutencaoRegistroFormatada. */
    	private String hrManutencaoRegistroFormatada;
	    
	    
		/**
		 * Get: cdAutenticacaoSegurancaInclusao.
		 *
		 * @return cdAutenticacaoSegurancaInclusao
		 */
		public String getCdAutenticacaoSegurancaInclusao() {
			return cdAutenticacaoSegurancaInclusao;
		}
		
		/**
		 * Set: cdAutenticacaoSegurancaInclusao.
		 *
		 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
		 */
		public void setCdAutenticacaoSegurancaInclusao(
				String cdAutenticacaoSegurancaInclusao) {
			this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
		}
		
		/**
		 * Get: cdAutenticacaoSegurancaManutencao.
		 *
		 * @return cdAutenticacaoSegurancaManutencao
		 */
		public String getCdAutenticacaoSegurancaManutencao() {
			return cdAutenticacaoSegurancaManutencao;
		}
		
		/**
		 * Set: cdAutenticacaoSegurancaManutencao.
		 *
		 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
		 */
		public void setCdAutenticacaoSegurancaManutencao(
				String cdAutenticacaoSegurancaManutencao) {
			this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
		}
		
		/**
		 * Get: cdBaseSistema.
		 *
		 * @return cdBaseSistema
		 */
		public Integer getCdBaseSistema() {
			return cdBaseSistema;
		}
		
		/**
		 * Set: cdBaseSistema.
		 *
		 * @param cdBaseSistema the cd base sistema
		 */
		public void setCdBaseSistema(Integer cdBaseSistema) {
			this.cdBaseSistema = cdBaseSistema;
		}
		
		/**
		 * Get: cdCanalInclusao.
		 *
		 * @return cdCanalInclusao
		 */
		public Integer getCdCanalInclusao() {
			return cdCanalInclusao;
		}
		
		/**
		 * Set: cdCanalInclusao.
		 *
		 * @param cdCanalInclusao the cd canal inclusao
		 */
		public void setCdCanalInclusao(Integer cdCanalInclusao) {
			this.cdCanalInclusao = cdCanalInclusao;
		}
		
		/**
		 * Get: cdCanalManutencao.
		 *
		 * @return cdCanalManutencao
		 */
		public Integer getCdCanalManutencao() {
			return cdCanalManutencao;
		}
		
		/**
		 * Set: cdCanalManutencao.
		 *
		 * @param cdCanalManutencao the cd canal manutencao
		 */
		public void setCdCanalManutencao(Integer cdCanalManutencao) {
			this.cdCanalManutencao = cdCanalManutencao;
		}
		
		/**
		 * Get: codMensagem.
		 *
		 * @return codMensagem
		 */
		public String getCodMensagem() {
			return codMensagem;
		}
		
		/**
		 * Set: codMensagem.
		 *
		 * @param codMensagem the cod mensagem
		 */
		public void setCodMensagem(String codMensagem) {
			this.codMensagem = codMensagem;
		}
		
		/**
		 * Get: dsBaseSistema.
		 *
		 * @return dsBaseSistema
		 */
		public String getDsBaseSistema() {
			return dsBaseSistema;
		}
		
		/**
		 * Set: dsBaseSistema.
		 *
		 * @param dsBaseSistema the ds base sistema
		 */
		public void setDsBaseSistema(String dsBaseSistema) {
			this.dsBaseSistema = dsBaseSistema;
		}
		
		/**
		 * Get: dsCanalInclusao.
		 *
		 * @return dsCanalInclusao
		 */
		public String getDsCanalInclusao() {
			return dsCanalInclusao;
		}
		
		/**
		 * Set: dsCanalInclusao.
		 *
		 * @param dsCanalInclusao the ds canal inclusao
		 */
		public void setDsCanalInclusao(String dsCanalInclusao) {
			this.dsCanalInclusao = dsCanalInclusao;
		}
		
		/**
		 * Get: dsCanalManutencao.
		 *
		 * @return dsCanalManutencao
		 */
		public String getDsCanalManutencao() {
			return dsCanalManutencao;
		}
		
		/**
		 * Set: dsCanalManutencao.
		 *
		 * @param dsCanalManutencao the ds canal manutencao
		 */
		public void setDsCanalManutencao(String dsCanalManutencao) {
			this.dsCanalManutencao = dsCanalManutencao;
		}
		
		/**
		 * Get: dsSituacaoSolicitacao.
		 *
		 * @return dsSituacaoSolicitacao
		 */
		public String getDsSituacaoSolicitacao() {
			return dsSituacaoSolicitacao;
		}
		
		/**
		 * Set: dsSituacaoSolicitacao.
		 *
		 * @param dsSituacaoSolicitacao the ds situacao solicitacao
		 */
		public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
			this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
		}
		
		/**
		 * Get: hrAtendimentoSolicitacao.
		 *
		 * @return hrAtendimentoSolicitacao
		 */
		public String getHrAtendimentoSolicitacao() {
			return hrAtendimentoSolicitacao;
		}
		
		/**
		 * Set: hrAtendimentoSolicitacao.
		 *
		 * @param hrAtendimentoSolicitacao the hr atendimento solicitacao
		 */
		public void setHrAtendimentoSolicitacao(String hrAtendimentoSolicitacao) {
			this.hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
		}
		
		/**
		 * Get: hrInclusaoRegistro.
		 *
		 * @return hrInclusaoRegistro
		 */
		public String getHrInclusaoRegistro() {
			return hrInclusaoRegistro;
		}
		
		/**
		 * Set: hrInclusaoRegistro.
		 *
		 * @param hrInclusaoRegistro the hr inclusao registro
		 */
		public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
			this.hrInclusaoRegistro = hrInclusaoRegistro;
		}
		
		/**
		 * Get: hrManutencaoRegistro.
		 *
		 * @return hrManutencaoRegistro
		 */
		public String getHrManutencaoRegistro() {
			return hrManutencaoRegistro;
		}
		
		/**
		 * Set: hrManutencaoRegistro.
		 *
		 * @param hrManutencaoRegistro the hr manutencao registro
		 */
		public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
			this.hrManutencaoRegistro = hrManutencaoRegistro;
		}
		
		/**
		 * Get: hrSolicitacao.
		 *
		 * @return hrSolicitacao
		 */
		public String getHrSolicitacao() {
			return hrSolicitacao;
		}
		
		/**
		 * Set: hrSolicitacao.
		 *
		 * @param hrSolicitacao the hr solicitacao
		 */
		public void setHrSolicitacao(String hrSolicitacao) {
			this.hrSolicitacao = hrSolicitacao;
		}
		
		/**
		 * Get: mensagem.
		 *
		 * @return mensagem
		 */
		public String getMensagem() {
			return mensagem;
		}
		
		/**
		 * Set: mensagem.
		 *
		 * @param mensagem the mensagem
		 */
		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
		
		/**
		 * Get: nmOperacaoFluxoInlcusao.
		 *
		 * @return nmOperacaoFluxoInlcusao
		 */
		public String getNmOperacaoFluxoInlcusao() {
			return nmOperacaoFluxoInlcusao;
		}
		
		/**
		 * Set: nmOperacaoFluxoInlcusao.
		 *
		 * @param nmOperacaoFluxoInlcusao the nm operacao fluxo inlcusao
		 */
		public void setNmOperacaoFluxoInlcusao(String nmOperacaoFluxoInlcusao) {
			this.nmOperacaoFluxoInlcusao = nmOperacaoFluxoInlcusao;
		}
		
		/**
		 * Get: nmOperacaoFluxoManutencao.
		 *
		 * @return nmOperacaoFluxoManutencao
		 */
		public String getNmOperacaoFluxoManutencao() {
			return nmOperacaoFluxoManutencao;
		}
		
		/**
		 * Set: nmOperacaoFluxoManutencao.
		 *
		 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
		 */
		public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
			this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
		}
		
		/**
		 * Get: nrSolicitacaoPagamento.
		 *
		 * @return nrSolicitacaoPagamento
		 */
		public Integer getNrSolicitacaoPagamento() {
			return nrSolicitacaoPagamento;
		}
		
		/**
		 * Set: nrSolicitacaoPagamento.
		 *
		 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
		 */
		public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
			this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
		}
		
		/**
		 * Get: qtdeRegistrosSolicitacao.
		 *
		 * @return qtdeRegistrosSolicitacao
		 */
		public Long getQtdeRegistrosSolicitacao() {
			return qtdeRegistrosSolicitacao;
		}
		
		/**
		 * Set: qtdeRegistrosSolicitacao.
		 *
		 * @param qtdeRegistrosSolicitacao the qtde registros solicitacao
		 */
		public void setQtdeRegistrosSolicitacao(Long qtdeRegistrosSolicitacao) {
			this.qtdeRegistrosSolicitacao = qtdeRegistrosSolicitacao;
		}
		
		/**
		 * Get: resultadoProcessamento.
		 *
		 * @return resultadoProcessamento
		 */
		public String getResultadoProcessamento() {
			return resultadoProcessamento;
		}
		
		/**
		 * Set: resultadoProcessamento.
		 *
		 * @param resultadoProcessamento the resultado processamento
		 */
		public void setResultadoProcessamento(String resultadoProcessamento) {
			this.resultadoProcessamento = resultadoProcessamento;
		}
		
		/**
		 * Get: hrAtendimentoSolicitacaoFormatada.
		 *
		 * @return hrAtendimentoSolicitacaoFormatada
		 */
		public String getHrAtendimentoSolicitacaoFormatada() {
			return hrAtendimentoSolicitacaoFormatada;
		}
		
		/**
		 * Set: hrAtendimentoSolicitacaoFormatada.
		 *
		 * @param hrAtendimentoSolicitacaoFormatada the hr atendimento solicitacao formatada
		 */
		public void setHrAtendimentoSolicitacaoFormatada(
				String hrAtendimentoSolicitacaoFormatada) {
			this.hrAtendimentoSolicitacaoFormatada = hrAtendimentoSolicitacaoFormatada;
		}
		
		/**
		 * Get: hrInclusaoRegistroFormatada.
		 *
		 * @return hrInclusaoRegistroFormatada
		 */
		public String getHrInclusaoRegistroFormatada() {
			return hrInclusaoRegistroFormatada;
		}
		
		/**
		 * Set: hrInclusaoRegistroFormatada.
		 *
		 * @param hrInclusaoRegistroFormatada the hr inclusao registro formatada
		 */
		public void setHrInclusaoRegistroFormatada(String hrInclusaoRegistroFormatada) {
			this.hrInclusaoRegistroFormatada = hrInclusaoRegistroFormatada;
		}
		
		/**
		 * Get: hrManutencaoRegistroFormatada.
		 *
		 * @return hrManutencaoRegistroFormatada
		 */
		public String getHrManutencaoRegistroFormatada() {
			return hrManutencaoRegistroFormatada;
		}
		
		/**
		 * Set: hrManutencaoRegistroFormatada.
		 *
		 * @param hrManutencaoRegistroFormatada the hr manutencao registro formatada
		 */
		public void setHrManutencaoRegistroFormatada(
				String hrManutencaoRegistroFormatada) {
			this.hrManutencaoRegistroFormatada = hrManutencaoRegistroFormatada;
		}
		
		/**
		 * Get: hrSolicitacaoFormatada.
		 *
		 * @return hrSolicitacaoFormatada
		 */
		public String getHrSolicitacaoFormatada() {
			return hrSolicitacaoFormatada;
		}
		
		/**
		 * Set: hrSolicitacaoFormatada.
		 *
		 * @param hrSolicitacaoFormatada the hr solicitacao formatada
		 */
		public void setHrSolicitacaoFormatada(String hrSolicitacaoFormatada) {
			this.hrSolicitacaoFormatada = hrSolicitacaoFormatada;
		}
		
		/**
		 * Get: vlTarifaPadrao.
		 *
		 * @return vlTarifaPadrao
		 */
		public BigDecimal getVlTarifaPadrao() {
			return vlTarifaPadrao;
		}
		
		/**
		 * Set: vlTarifaPadrao.
		 *
		 * @param vlTarifaPadrao the vl tarifa padrao
		 */
		public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
			this.vlTarifaPadrao = vlTarifaPadrao;
		}
		
		/**
		 * Get: numPercentualDescTarifa.
		 *
		 * @return numPercentualDescTarifa
		 */
		public BigDecimal getNumPercentualDescTarifa() {
			return numPercentualDescTarifa;
		}
		
		/**
		 * Set: numPercentualDescTarifa.
		 *
		 * @param numPercentualDescTarifa the num percentual desc tarifa
		 */
		public void setNumPercentualDescTarifa(BigDecimal numPercentualDescTarifa) {
			this.numPercentualDescTarifa = numPercentualDescTarifa;
		}
		
		/**
		 * Get: vlrTarifaAtual.
		 *
		 * @return vlrTarifaAtual
		 */
		public BigDecimal getVlrTarifaAtual() {
			return vlrTarifaAtual;
		}
		
		/**
		 * Set: vlrTarifaAtual.
		 *
		 * @param vlrTarifaAtual the vlr tarifa atual
		 */
		public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
			this.vlrTarifaAtual = vlrTarifaAtual;
		}
	
}
