/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;


/**
 * Nome: AlterarConfiguracaoTipoServModContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarConfiguracaoTipoServModContratoEntradaDTO {

	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;

	/** Atributo cdTipoServico. */
	private int cdTipoServico;

	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	private int cindcdFantsRepas;

	/** Atributo cdParametroTela. */
	private int cdParametroTela;

	/** Atributo rdoPermiteAcertosDados. */
	private Integer rdoPermiteAcertosDados;

	/** Atributo cdIndicadorManutencaoProcurador. */
	private Integer cdIndicadorManutencaoProcurador;

	/** Atributo cdFloatServicoContrato. */
	private Integer cdFloatServicoContrato;

	/** The cd exige aut filial. */
	private Integer cdExigeAutFilial;

	/** Atributo cdUsuario. */
	private String cdUsuario;

	private String dsFormaEnvioPagamento;

	/** Atributo dsFormaAutorizacaoPagamento. */
	private String  dsFormaAutorizacaoPagamento;

	/** Atributo cdUtilizaPreAutorizacaoPagamentos. */
	private int cdUtilizaPreAutorizacaoPagamentos;

	/** Atributo dsUtilizaPreAutorizacaoPagamentos. */
	private String dsUtilizaPreAutorizacaoPagamentos;

	/** Atributo cdTipoControleFloating. */
	private int cdTipoControleFloating;

	/** Atributo dsTipoControleFloating. */
	private String dsTipoControleFloating;

	/** Atributo emiteCartaoAntecipadoContaSalario. */
	private int emiteCartaoAntecipadoContaSalario;

	/** Atributo dsEmiteCartaoAntecipadoContaSalario. */
	private String dsEmiteCartaoAntecipadoContaSalario;

	/** Atributo cdTipoCartao. */
	private int cdTipoCartao;

	/** Atributo dsTipoCartao. */
	private String dsTipoCartao;

	/** Atributo qtdeLimiteSolicitacaoCartaoContaSalario. */
	private int qtdeLimiteSolicitacaoCartaoContaSalario;

	/** Atributo dtLimiteEnquadramentoConvenioContaSalario. */
	private String dtLimiteEnquadramentoConvenioContaSalario;

	/** Atributo cdGerarRetornoOperRealizadaInternet. */
	private int cdGerarRetornoOperRealizadaInternet;

	/** Atributo limiteEnqConvContaSal. */
	private  int limiteEnqConvContaSal;

	/** Atributo qtddLimiteCartaoContaSalarioSolicitacao. */
	private int qtddLimiteCartaoContaSalarioSolicitacao;

	/** Atributo cdTipoConsolidacaoPagamentosComprovante. */
	private int cdTipoConsolidacaoPagamentosComprovante;

	/** Atributo cdTipoCartaoContaSalario. */
	private int cdTipoCartaoContaSalario;

	/** Atributo cdEmissaoAntCartaoContSal. */
	private int cdEmissaoAntCartaoContSal;

	/** Atributo cdAbertContaBancoPostBradSeg. */
	private int cdAbertContaBancoPostBradSeg;

	/** Atributo cdUtilizacaoRastreamentoDebitoVeiculo. */
	private int  cdUtilizacaoRastreamentoDebitoVeiculo;

	/** Atributo dsUtilizacaoRastreamentoDebitoVeiculo. */
	private String dsUtilizacaoRastreamentoDebitoVeiculo;

	/** Atributo cdPeriodicidadeRastreamentoDebitoVeiculo. */
	private int cdPeriodicidadeRastreamentoDebitoVeiculo;

	/** Atributo dsPeriodicidadeRastreamentoDebitoVeiculo. */
	private String dsPeriodicidadeRastreamentoDebitoVeiculo;

	/** Atributo cdAgendamentoDebitoVeiculoRastreamento. */
	private int cdAgendamentoDebitoVeiculoRastreamento;

	/** Atributo dsAgendamentoDebitoVeiculoRastreamento. */
	private String dsAgendamentoDebitoVeiculoRastreamento;

	/** Atributo cdPeriodicidadeCobrancaTarifa. */
	private int cdPeriodicidadeCobrancaTarifa;

	/** Atributo dsPeriodicidadeCobrancaTarifa. */
	private String dsPeriodicidadeCobrancaTarifa;

	/** Atributo diaFechamentoApuracaoTarifaMensal. */
	private int diaFechamentoApuracaoTarifaMensal;

	/** Atributo qtdeDiasCobrancaTarifaAposApuracao. */
	private int qtdeDiasCobrancaTarifaAposApuracao;

	/** Atributo cdTipoReajusteTarifa. */
	private int cdTipoReajusteTarifa;

	/** Atributo dsTipoReajusteTarifa. */
	private String dsTipoReajusteTarifa;

	/** Atributo qtdeMesesReajusteAutomaticoTarifa. */
	private int qtdeMesesReajusteAutomaticoTarifa;

	/** Atributo cdIndiceEconomicoReajuste. */
	private BigDecimal cdIndiceEconomicoReajuste;

	/** Atributo cdTipoAlimentacaoPagamentos. */
	private int cdTipoAlimentacaoPagamentos;

	/** Atributo cdTipoAutorizacaoPagamento. */
	private int cdTipoAutorizacaoPagamento;

	/** Atributo dsTipoAutorizacaoPagamento. */
	private String dsTipoAutorizacaoPagamento;

	/** Atributo cdInformaAgenciaContaCredito. */
	private int cdInformaAgenciaContaCredito;

	/** Atributo dsInformaAgenciaContaCredito. */
	private String dsInformaAgenciaContaCredito;

	/** Atributo cdTipoIdentificacaoBeneficio. */
	private int cdTipoIdentificacaoBeneficio;

	/** Atributo dsTipoIdentificacaoBeneficio. */
	private String dsTipoIdentificacaoBeneficio;

	/** Atributo cdUtilizaCadastroOrgaosPagadores. */
	private int cdUtilizaCadastroOrgaosPagadores;

	/** Atributo dsUtilizaCadastroOrgaosPagadores. */
	private String dsUtilizaCadastroOrgaosPagadores;

	/** Atributo cdMantemCadastroProcuradores. */
	private int cdMantemCadastroProcuradores;

	/** Atributo dsMantemCadastroProcuradores. */
	private String dsMantemCadastroProcuradores;

	/** Atributo cdPeriodicidadeManutencaoCadastroProcurador. */
	private int cdPeriodicidadeManutencaoCadastroProcurador;

	/** Atributo dsPeriodicidadeManutencaoCadastroProcurador. */
	private String dsPeriodicidadeManutencaoCadastroProcurador;

	/** Atributo cdFormaManutencaoCadastroProcurador. */
	private int cdFormaManutencaoCadastroProcurador;

	/** Atributo dsFormaManutencaoCadastroProcurador. */
	private String dsFormaManutencaoCadastroProcurador;

	/** Atributo cdPossuiExpiracaoCredito. */
	private int cdPossuiExpiracaoCredito;

	/** Atributo dsPossuiExpiracaoCredito. */
	private String dsPossuiExpiracaoCredito;

	/** Atributo cdFormaControleExpiracaoCredito. */
	private int cdFormaControleExpiracaoCredito;

	/** Atributo dsFormaControleExpiracaoCredito. */
	private String dsFormaControleExpiracaoCredito;

	/** Atributo qtdeDiasExpiracaoCredito. */
	private int qtdeDiasExpiracaoCredito;

	/** Atributo cdTratamentoFeriadoFimVigencia. */
	private int cdTratamentoFeriadoFimVigencia;

	/** Atributo dsTratamentoFeriadoFimVigencia. */
	private String dsTratamentoFeriadoFimVigencia;

	/** Atributo cdTipoPrestacaoContaCreditoPago. */
	private int cdTipoPrestacaoContaCreditoPago;

	/** Atributo dsTipoPrestacaoContaCreditoPago. */
	private String dsTipoPrestacaoContaCreditoPago;

	/** Atributo cdFormaManutencaoCadastro. */
	private int cdFormaManutencaoCadastro;

	/** Atributo dsFormaManutencaoCadastro. */
	private String dsFormaManutencaoCadastro;

	/** Atributo qtdeDiasInativacaoFavorecido. */
	private int qtdeDiasInativacaoFavorecido;

	/** Atributo cdConsistenciaCpfCnpjFavorecido. */
	private int cdConsistenciaCpfCnpjFavorecido;

	/** Atributo dsConsistenciaCpfCnpjFavorecido. */
	private String dsConsistenciaCpfCnpjFavorecido;

	/** Atributo cdPeriodicidade. */
	private int cdPeriodicidade;

	/** Atributo dsPeriodicidade. */
	private String dsPeriodicidade;

	/** Atributo cdDestino. */
	private int cdDestino;

	/** Atributo cdPermiteCorrespondenciaAberta. */
	private int cdPermiteCorrespondenciaAberta;

	/** Atributo cdDemonstraInformacoesAreaReservada. */
	private int cdDemonstraInformacoesAreaReservada;

	/** Atributo cdAgruparCorrespondencia. */
	private int cdAgruparCorrespondencia;

	/** Atributo qtdeVias. */
	private int qtdeVias;

	/** Atributo cdTipoRejeicao. */
	private int cdTipoRejeicao;

	/** Atributo cdTipoEmissao. */
	private int cdTipoEmissao;

	/** Atributo cdMidiaDisponivelCorrentista. */
	private int cdMidiaDisponivelCorrentista;

	/** Atributo cdMidiaDisponivelNaoCorrentista. */
	private int cdMidiaDisponivelNaoCorrentista;

	/** Atributo cdTipoEnvioCorrespondencia. */
	private int cdTipoEnvioCorrespondencia;

	/** Atributo cdFrasesPreCadastratadas. */
	private int cdFrasesPreCadastratadas;

	/** Atributo cdCobrarTarifasFuncionarios. */
	private int cdCobrarTarifasFuncionarios;

	/** Atributo qtdeMesesEmissao. */
	private int qtdeMesesEmissao;

	/** Atributo qtdeLinhas. */
	private int qtdeLinhas;

	/** Atributo qtdeViasEmissaoComprovantes. */
	private int qtdeViasEmissaoComprovantes;

	/** Atributo qtdeViasPagas. */
	private int qtdeViasPagas;

	/** Atributo cdFormularioParaImpressao. */
	private int cdFormularioParaImpressao;

	/** Atributo cdPeriodicidadeCobrancaTarifaEmissaoComprovantes. */
	private int cdPeriodicidadeCobrancaTarifaEmissaoComprovantes;

	/** Atributo dsPeriodicidadeCobrancaTarifaEmissaoComprovantes. */
	private String dsPeriodicidadeCobrancaTarifaEmissaoComprovantes;

	/** Atributo cdFechamentoApuracao. */
	private int cdFechamentoApuracao;

	/** Atributo qtdeDiasCobrancaAposApuracao. */
	private int qtdeDiasCobrancaAposApuracao;

	/** Atributo cdTipoReajuste. */
	private int cdTipoReajuste;

	/** Atributo cdPeriodicidadeReajusteTarifa. */
	private int cdPeriodicidadeReajusteTarifa;

	/** Atributo dsPeriodicidadeReajusteTarifa. */
	private String dsPeriodicidadeReajusteTarifa;

	/** Atributo qtdeMesesReajusteAutomatico. */
	private int qtdeMesesReajusteAutomatico;

	/** Atributo cdIndiceReajuste. */
	private int cdIndiceReajuste;

	/** Atributo percentualReajuste. */
	private BigDecimal percentualReajuste;

	/** Atributo porcentagemBonificacaoTarifa. */
	private BigDecimal porcentagemBonificacaoTarifa;

	/** Atributo cdTipoIdentificacaoFuncionario. */
	private int cdTipoIdentificacaoFuncionario;	

	/** Atributo cdTipoConsistenciaIdentificador. */
	private int cdTipoConsistenciaIdentificador;	

	/** Atributo cdCondicaoEnquadramento. */
	private int cdCondicaoEnquadramento;	

	/** Atributo cdCriterioPrincipalEnquadramento. */
	private int cdCriterioPrincipalEnquadramento;	

	/** Atributo cdCriterioCompostoEnquadramento. */
	private int cdCriterioCompostoEnquadramento;	

	/** Atributo qtdeEtapasRecadastramento. */
	private int qtdeEtapasRecadastramento;

	/** Atributo qtdeFasesPorEtapa. */
	private int qtdeFasesPorEtapa;

	/** Atributo qtdeMesesPrazoPorFase. */
	private int qtdeMesesPrazoPorFase;

	/** Atributo qtdeMesesPrazoPorEtapa. */
	private int qtdeMesesPrazoPorEtapa;

	/** Atributo cdPermiteEnvioRemessaManutencao. */
	private int cdPermiteEnvioRemessaManutencao;

	/** Atributo cdPeriodicidadeEnvioRemessa. */
	private int cdPeriodicidadeEnvioRemessa;	

	/** Atributo cdBaseDadosUtilizada. */
	private int cdBaseDadosUtilizada;

	/** Atributo cdTipoCargaBaseDadosUtilizada. */
	private int cdTipoCargaBaseDadosUtilizada;

	/** Atributo cdPermiteAnteciparRecadastramento. */
	private int cdPermiteAnteciparRecadastramento;

	/** Atributo dtLimiteInicioVinculoCargaBase. */
	private String dtLimiteInicioVinculoCargaBase;

	/** Atributo dtInicioRecadastramento. */
	private String dtInicioRecadastramento;

	/** Atributo dtFimRecadastramento. */
	private String dtFimRecadastramento;

	/** Atributo cdPermiteAcertosDados. */
	private int cdPermiteAcertosDados;

	/** Atributo dtInicioPeriodoAcertoDados. */
	private String dtInicioPeriodoAcertoDados;

	/** Atributo dtFimPeriodoAcertoDados. */
	private String dtFimPeriodoAcertoDados;

	/** Atributo cdEmiteMensagemRecadastramentoMidiaOnLine. */
	private int cdEmiteMensagemRecadastramentoMidiaOnLine;

	/** Atributo cdMidiaMensagemRecastramentoOnLine. */
	private int cdMidiaMensagemRecastramentoOnLine;

	/** Atributo cdIndiceEconomicoReajusteTarifa. */
	private int cdIndiceEconomicoReajusteTarifa;	

	/** Atributo percentualIndiceEconomicoReajusteTarifa. */
	private BigDecimal percentualIndiceEconomicoReajusteTarifa;

	/** Atributo percentualBonificacaoTarifaPadrao. */
	private BigDecimal percentualBonificacaoTarifaPadrao;

	private int cdPeriocidadeEnvio;	

	/** Atributo cdEnderecoEnvio. */
	private int cdEnderecoEnvio;	

	/** Atributo qntDiasAntecipacao. */
	private int qntDiasAntecipacao;

	/** Atributo cdPermitirAgrupamento. */
	private int cdPermitirAgrupamento;

	/** Atributo cdTipoComprovacao. */
	private int cdTipoComprovacao;	

	/** Atributo qntdMesesPeriComprovacao. */
	private int qntdMesesPeriComprovacao;

	/** Atributo codUtilizarMsgPersOnline. */
	private int codUtilizarMsgPersOnline;	

	/** Atributo qntdDiasAvisoVenc. */
	private int qntdDiasAvisoVenc;

	/** Atributo qntdDiasAntecedeInicio. */
	private int qntdDiasAntecedeInicio;

	/** Atributo cdEnderecoUtilizadoEnvio. */
	private int cdEnderecoUtilizadoEnvio;

	/** Atributo cdTratamentoFeriado. */
	private int cdTratamentoFeriado;	

	/** Atributo cdTratamentoValorDivergente. */
	private int cdTratamentoValorDivergente;	

	/** Atributo valorMaxPagFavNaoCadastrado. */
	private BigDecimal valorMaxPagFavNaoCadastrado;

	/** Atributo cdTipoConsultaSaldo. */
	private int cdTipoConsultaSaldo;	

	/** Atributo cdControlePagFavorecido. */
	private int cdControlePagFavorecido;	

	/** Atributo cdTipoConsistenciaCpfCnpjProp. */
	private int cdTipoConsistenciaCpfCnpjProp;

	/** Atributo cdTipoEfetivacao. */
	private int cdTipoEfetivacao;

	/** Atributo cdPrioridadeCredito. */
	private int cdPrioridadeCredito;

	/** Atributo qntdMaxInconsistenciaLote. */
	private int qntdMaxInconsistenciaLote;

	/** Atributo qntdDiasRepConsulta. */
	private int qntdDiasRepConsulta;

	/** Atributo porcentInconsistenciaLote. */
	private int porcentInconsistenciaLote;

	/** Atributo permiteContingenciaPag. */
	private int permiteContingenciaPag;

	/** Atributo cdPermiteConsultaFavorecido. */
	private int cdPermiteConsultaFavorecido;	

	/** Atributo cdTipoRejeicaoAgendamento. */
	private int cdTipoRejeicaoAgendamento;

	/** Atributo cdPrioridadeDebito. */
	private int cdPrioridadeDebito;	

	/** Atributo valorMaximoPagamentoFavorecidoNaoCadastrado. */
	private BigDecimal valorMaximoPagamentoFavorecidoNaoCadastrado;	

	/** Atributo valorLimiteDiario. */
	private BigDecimal valorLimiteDiario;

	/** Atributo valorLimiteIndividual. */
	private BigDecimal valorLimiteIndividual;

	/** Atributo quantDiasRepique. */
	private int quantDiasRepique;

	/** Atributo cdTipoProcessamento. */
	private int cdTipoProcessamento;	

	/** Atributo cdTipoRejeicaoEfetivacao. */
	private int cdTipoRejeicaoEfetivacao;

	/** Atributo cdControlePagamentoFavorecido. */
	private int cdControlePagamentoFavorecido;	

	/** Atributo qtdeMaxInconsistenciaPorLote. */
	private int qtdeMaxInconsistenciaPorLote;

	/** Atributo cdTipoRastreamentoTitulos. */
	private int cdTipoRastreamentoTitulos;

	/** Atributo rdoRastrearNotasFiscais. */
	private int rdoRastrearNotasFiscais;

	/** Atributo rdoAgendarTitRastProp. */
	private int rdoAgendarTitRastProp;

	/** Atributo rdoBloquearEmissaoPap. */
	private int rdoBloquearEmissaoPap;

	/** Atributo dataInicioRastreamento. */
	private String dataInicioRastreamento;

	/** Atributo rdoRastrearTitTerceiros. */
	private int rdoRastrearTitTerceiros;

	/** Atributo rdoCapturarTitDtaRegistro. */
	private int rdoCapturarTitDtaRegistro;

	/** Atributo rdoRastrearTitRastreadoFilial. */
	private int rdoRastrearTitRastreadoFilial;

	/** Atributo dataInicioBloqPapeleta. */
	private String dataInicioBloqPapeleta;

	/** Atributo dataRegistroTitulo. */
	private String dataRegistroTitulo;

	/** Atributo rdoUtilizaCadFavControlePag. */
	private int rdoUtilizaCadFavControlePag;

	/** Atributo cdTratamentoFeriadosDtaPag. */
	private int cdTratamentoFeriadosDtaPag;

	/** Atributo dsTratamentoFeriadosDtaPag. */
	private String dsTratamentoFeriadosDtaPag;

	/** Atributo rdoPermiteFavConsultarPag. */
	private int rdoPermiteFavConsultarPag;

	/** Atributo rdoGerarLanctoFuturoDeb. */
	private int rdoGerarLanctoFuturoDeb;

	/** Atributo rdoPermitePagarVencido. */
	private int rdoPermitePagarVencido;

	/** Atributo rdoPermitePagarMenor. */
	private int rdoPermitePagarMenor;

	/** Atributo qntdLimiteDiasPagVencido. */
	private int qntdLimiteDiasPagVencido;

	/** Atributo rdoPermiteDebitoOnline. */
	private int rdoPermiteDebitoOnline;

	/** Atributo rdoGerarLanctoFuturoCred. */
	private int rdoGerarLanctoFuturoCred;

	/** Atributo rdoGerarLanctoProgramado. */
	private int rdoGerarLanctoProgramado;

	/** Atributo cdPermiteEstornoPagamento. */
	private int cdPermiteEstornoPagamento;	

	/** Atributo cdEfetuaConsistencia. */
	private int cdEfetuaConsistencia;	

	/** Atributo cdTipoTratamentoContasTransferidas. */
	private int cdTipoTratamentoContasTransferidas;	

	/** Atributo qntdMaxRegInconsistentes. */
	private int qntdMaxRegInconsistentes;

	/** Atributo percentualMaxRegInconsistenciaLote. */
	private int percentualMaxRegInconsistenciaLote;

	/** Atributo rdoOcorrenciaDebito. */
	private int rdoOcorrenciaDebito;

	/** Atributo qntdDiasExpiracao. */
	private int qntdDiasExpiracao;

	/** Atributo rdoPermiteOutrosTiposIncricaoFav. */
	private int rdoPermiteOutrosTiposIncricaoFav;

	/** Atributo cdOcorrenciaDebito. */
	private int cdOcorrenciaDebito;

	/** Atributo cdLocalEmissao. */
	private Integer cdLocalEmissao;

	/** Atributo cdConsultaSaldoValorSuperior. */
	private Integer cdConsultaSaldoValorSuperior;

	/** Atributo perInconsistenciaLote. */
	private int perInconsistenciaLote;

	/** Atributo cdPermiteContingenciaPag. */
	private int cdPermiteContingenciaPag;	



	/************************************* NOVAS *********************************/

	private Long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaCotratoNegocio. */
	private Long nrSequenciaCotratoNegocio;

	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;

	/** Atributo cdParametro. */
	private Integer cdParametro;

	/** Atributo cdProdutoOperacaroRelacionado. */
	private Integer cdProdutoOperacaroRelacionado;

	/** Atributo cdAcaoNaoVida. */
	private Integer cdAcaoNaoVida;

	/** Atributo cdAcertoDadoRecadastro. */
	private Integer cdAcertoDadoRecadastro;

	/** Atributo cdAgendaDebitoVeiculo. */
	private Integer cdAgendaDebitoVeiculo;

	/** Atributo cdAgendaPagamentoVencido. */
	private Integer cdAgendaPagamentoVencido;

	/** Atributo cdAgendaValorMenor. */
	private Integer cdAgendaValorMenor;

	/** Atributo cdAgrupamentoAviso. */
	private Integer cdAgrupamentoAviso;

	/** Atributo cdAgrupamentoComprovado. */
	private Integer cdAgrupamentoComprovado;

	/** Atributo cdAgrupamentoFormularioRecadastro. */
	private Integer cdAgrupamentoFormularioRecadastro;

	/** Atributo cdAntecRecadastroBeneficio. */
	private Integer cdAntecRecadastroBeneficio;

	/** Atributo cdAreaReservada. */
	private Integer cdAreaReservada;

	/** Atributo cdBaseRecadastroBeneficio. */
	private Integer cdBaseRecadastroBeneficio;

	/** Atributo cdBloqueioEmissaoPplta. */
	private Integer cdBloqueioEmissaoPplta;

	/** Atributo cdCapituloTituloRegistro. */
	private Integer cdCapituloTituloRegistro;

	/** Atributo cdCobrancaTarifa. */
	private Integer cdCobrancaTarifa;

	/** Atributo cdConsDebitoVeiculo. */
	private Integer cdConsDebitoVeiculo;

	/** Atributo cdConsEndereco. */
	private Integer cdConsEndereco;

	/** Atributo cdConsSaldoPagamento. */
	private Integer cdConsSaldoPagamento;

	/** Atributo cdContagemConsSaudo. */
	private Integer cdContagemConsSaudo;

	/** Atributo cdCreditoNaoUtilizado. */
	private Integer cdCreditoNaoUtilizado;

	/** Atributo cdCriterioEnquaBeneficio. */
	private Integer cdCriterioEnquaBeneficio;

	/** Atributo cdCriterioEnquaRecadastro. */
	private Integer cdCriterioEnquaRecadastro;

	/** Atributo cdCriterioRastreabilidadeTitulo. */
	private Integer cdCriterioRastreabilidadeTitulo;

	/** Atributo cdCtciaEspecieBeneficio. */
	private Integer cdCtciaEspecieBeneficio;

	/** Atributo cdCtciaIdentificacaoBeneficio. */
	private Integer cdCtciaIdentificacaoBeneficio;

	/** Atributo cdCtciaInscricaoFavorecido. */
	private Integer cdCtciaInscricaoFavorecido;

	/** Atributo cdCtciaProprietarioVeiculo. */
	private Integer cdCtciaProprietarioVeiculo;

	/** Atributo cdDispzContaCredito. */
	private Integer cdDispzContaCredito;

	/** Atributo cdDispzDiversarCrrtt. */
	private Integer cdDispzDiversarCrrtt;

	/** Atributo cdDispzDiversasNao. */
	private Integer cdDispzDiversasNao;

	/** Atributo cdDIspzSalarioCrrtt. */
	private Integer cdDIspzSalarioCrrtt;

	/** Atributo cdDispzSalarioNao. */
	private Integer cdDispzSalarioNao;

	/** Atributo cdDestinoAviso. */
	private Integer cdDestinoAviso;

	/** Atributo cdDestinoComprovante. */
	private Integer cdDestinoComprovante;

	/** Atributo cdDestinoFormularioRecadastro. */
	private Integer cdDestinoFormularioRecadastro;

	/** Atributo cdEnvelopeAberto. */
	private Integer cdEnvelopeAberto;

	/** Atributo cdFavorecidoConsPagamento. */
	private Integer cdFavorecidoConsPagamento;

	/** Atributo cdFormaAutorizacaoPagamento. */
	private Integer cdFormaAutorizacaoPagamento;

	/** Atributo cdFormaEnvioPagamento. */
	private Integer cdFormaEnvioPagamento;

	/** Atributo cdFormaEstornoCredito. */
	private Integer cdFormaEstornoCredito;

	/** Atributo cdFormaExpiracaoCredito. */
	private Integer cdFormaExpiracaoCredito;

	/** Atributo cdFormaManutencao. */
	private Integer cdFormaManutencao;

	/** Atributo cdFrasePreCadastro. */
	private Integer cdFrasePreCadastro;

	/** Atributo cdIndicadorAgendaTitulo. */
	private Integer cdIndicadorAgendaTitulo;

	/** Atributo cdIndicadorAutorizacaoCliente. */
	private Integer cdIndicadorAutorizacaoCliente;

	/** Atributo cdIndicadorAutorizacaoComplemento. */
	private Integer cdIndicadorAutorizacaoComplemento;

	/** Atributo cdIndicadorCadastroOrg. */
	private Integer cdIndicadorCadastroOrg;

	/** Atributo cdIndicadorCadastroProcd. */
	private Integer cdIndicadorCadastroProcd;

	/** Atributo cdIndicadorCataoSalario. */
	private Integer cdIndicadorCataoSalario;

	/** Atributo cdIndicadorExpiracaoCredito. */
	private Integer cdIndicadorExpiracaoCredito;

	/** Atributo cdIndicadorLancamentoPagamento. */
	private Integer cdIndicadorLancamentoPagamento;

	/** Atributo cdIndicadorMensagemPerso. */
	private Integer cdIndicadorMensagemPerso;

	/** Atributo cdLancamentoFuturoCredito. */
	private Integer cdLancamentoFuturoCredito;

	/** Atributo cdLancamentoFuturoDebito. */
	private Integer cdLancamentoFuturoDebito;

	/** Atributo cdLiberacaoLoteProcesso. */
	private Integer cdLiberacaoLoteProcesso;

	/** Atributo cdManutencaoBaseRecadastro. */
	private Integer cdManutencaoBaseRecadastro;

	/** Atributo cdMeioPagamentoDebito. */
	private Integer cdMeioPagamentoDebito;

	/** Atributo cdMidiaDisponivel. */
	private Integer cdMidiaDisponivel;

	/** Atributo cdMidiaMensagemRecadastro. */
	private Integer cdMidiaMensagemRecadastro;

	/** Atributo cdMomentoAvisoRacadastro. */
	private Integer cdMomentoAvisoRacadastro;

	/** Atributo cdMomentoCreditoEfetivacao. */
	private Integer cdMomentoCreditoEfetivacao;

	/** Atributo cdMomentoDebitoPagamento. */
	private Integer cdMomentoDebitoPagamento;

	/** Atributo cdMomentoFormularioRecadastro. */
	private Integer cdMomentoFormularioRecadastro;

	/** Atributo cdMomentoProcessamentoPagamento. */
	private Integer cdMomentoProcessamentoPagamento;

	/** Atributo cdMensagemRecadastroMidia. */
	private Integer cdMensagemRecadastroMidia;

	/** Atributo cdPermissaoDebitoOnline. */
	private Integer cdPermissaoDebitoOnline;

	/** Atributo cdNaturezaOperacaoPagamento. */
	private Integer cdNaturezaOperacaoPagamento;

	/** Atributo cdPeriodicidadeAviso. */
	private Integer cdPeriodicidadeAviso;

	/** Atributo cdPerdcComprovante. */
	private Integer cdPerdcComprovante;

	/** Atributo cdPerdcConsultaVeiculo. */
	private Integer cdPerdcConsultaVeiculo;

	/** Atributo cdPerdcEnvioRemessa. */
	private Integer cdPerdcEnvioRemessa;

	/** Atributo cdPerdcManutencaoProcd. */
	private Integer cdPerdcManutencaoProcd;

	/** Atributo cdPagamentoNaoUtilizado. */
	private Integer cdPagamentoNaoUtilizado;

	/** Atributo cdPrincipalEnquaRecadastro. */
	private Integer cdPrincipalEnquaRecadastro;

	/** Atributo cdPrioridadeEfetivacaoPagamento. */
	private Integer cdPrioridadeEfetivacaoPagamento;

	/** Atributo cdRejeicaoAgendaLote. */
	private Integer cdRejeicaoAgendaLote;

	/** Atributo cdRejeicaoEfetivacaoLote. */
	private Integer cdRejeicaoEfetivacaoLote;

	/** Atributo cdRejeicaoLote. */
	private Integer cdRejeicaoLote;

	/** Atributo cdRastreabilidadeNotaFiscal. */
	private Integer cdRastreabilidadeNotaFiscal;

	/** Atributo cdRastreabilidadeTituloTerceiro. */
	private Integer cdRastreabilidadeTituloTerceiro;

	/** Atributo cdTipoCargaRecadastro. */
	private Integer cdTipoCargaRecadastro;

	/** Atributo cdTipoCataoSalario. */
	private Integer cdTipoCataoSalario;

	/** Atributo cdTipoDataFloat. */
	private Integer cdTipoDataFloat;

	/** Atributo cdTipoDivergenciaVeiculo. */
	private Integer cdTipoDivergenciaVeiculo;

	/** Atributo cdTipoIdBeneficio. */
	private Integer cdTipoIdBeneficio;

	/** Atributo cdContratoContaTransferencia. */
	private Integer cdContratoContaTransferencia;

	/** Atributo cdUtilizacaoFavorecidoControle. */
	private Integer cdUtilizacaoFavorecidoControle;

	/** Atributo dtEnquaContaSalario. */
	private String dtEnquaContaSalario;

	/** Atributo dtInicioBloqueioPapeleta. */
	private String dtInicioBloqueioPapeleta;

	/** Atributo dtInicioRastreabilidadeTitulo. */
	private String dtInicioRastreabilidadeTitulo;

	/** Atributo dtFimAcertoRecadastro. */
	private String dtFimAcertoRecadastro;

	/** Atributo dtFimRecadastroBeneficio. */
	private String dtFimRecadastroBeneficio;

	/** Atributo dtInicioAcertoRecadastro. */
	private String dtInicioAcertoRecadastro;

	/** Atributo dtInicioRecadastroBeneficio. */
	private String dtInicioRecadastroBeneficio;

	/** Atributo dtLimiteVinculoCarga. */
	private String dtLimiteVinculoCarga;

	/** Atributo cdPercentualMaximoInconLote. */
	private Integer cdPercentualMaximoInconLote;

	/** Atributo qtAntecedencia. */
	private Integer qtAntecedencia;

	/** Atributo qtAnteriorVencimentoComprovado. */
	private Integer qtAnteriorVencimentoComprovado;

	/** Atributo qtDiaExpiracao. */
	private Integer qtDiaExpiracao;

	/** Atributo cdDiaFloatPagamento. */
	private Integer cdDiaFloatPagamento;

	/** Atributo qtDiaInatividadeFavorecido. */
	private Integer qtDiaInatividadeFavorecido;

	/** Atributo qtDiaRepiqConsulta. */
	private Integer qtDiaRepiqConsulta;

	/** Atributo qtEtapasRecadastroBeneficio. */
	private Integer qtEtapasRecadastroBeneficio;

	/** Atributo qtFaseRecadastroBeneficio. */
	private Integer qtFaseRecadastroBeneficio;

	/** Atributo qtLimiteLinha. */
	private Integer qtLimiteLinha;

	/** Atributo qtLimiteSolicitacaoCatao. */
	private Integer qtLimiteSolicitacaoCatao;

	/** Atributo qtMaximaInconLote. */
	private Integer qtMaximaInconLote;

	/** Atributo qtMaximaTituloVencido. */
	private Integer qtMaximaTituloVencido;

	/** Atributo qtMesComprovante. */
	private Integer qtMesComprovante;

	/** Atributo qtMesEtapaRecadastro. */
	private Integer qtMesEtapaRecadastro;

	/** Atributo qtMesFaseRecadastro. */
	private Integer qtMesFaseRecadastro;

	/** Atributo qtViaAviso. */
	private Integer qtViaAviso;

	/** Atributo qtViaCobranca. */
	private Integer qtViaCobranca;

	/** Atributo qtViaComprovante. */
	private Integer qtViaComprovante;

	/** Atributo vlFavorecidoNaoCadastro. */
	private BigDecimal vlFavorecidoNaoCadastro;

	/** Atributo vlLimiteDiaPagamento. */
	private BigDecimal vlLimiteDiaPagamento;

	/** Atributo vlLimiteIndividualPagamento. */
	private BigDecimal vlLimiteIndividualPagamento;

	/** Atributo dsAreaReservada. */
	private String dsAreaReservada;

	/** Atributo cdIndicadorRetornoInternet. */
	private Integer cdIndicadorRetornoInternet;

	/** Atributo cdIndicadorSeparaCanal. */
	private Integer cdIndicadorSeparaCanal;

	/** Atributo cdIndicadorListaDebito. */
	private Integer cdIndicadorListaDebito;

	/** Atributo cdTipoFormacaoLista. */
	private Integer cdTipoFormacaoLista;

	/** Atributo cdTipoConsistenciaLista. */
	private Integer cdTipoConsistenciaLista;

	/** Atributo cdTipoConsultaComprovante. */
	private Integer cdTipoConsultaComprovante;

	/** Atributo cdIndicadorBancoPostal. */
	private Integer cdIndicadorBancoPostal;

	/** Atributo cdValidacaoNomeFavorecido. */
	private Integer cdValidacaoNomeFavorecido;

	/** Atributo cdTipoContaFavorecido. */
	private Integer cdTipoContaFavorecido;

	/** Atributo cdIndLancamentoPersonalizado. */
	private Integer cdIndLancamentoPersonalizado;

	/** Atributo cdTipoIsncricaoFavorecido. */
	private Integer cdTipoIsncricaoFavorecido;

	/** Atributo cdMeioPagamentoCredito. */
	private Integer cdMeioPagamentoCredito;

	/** Atributo cdAgendaRastreabilidadeFilial. */
	private Integer cdAgendaRastreabilidadeFilial;

	/** Atributo cdIndicadorAdesaoSacador. */
	private Integer cdIndicadorAdesaoSacador;

	/** Atributo cdFormularioContratoCliente. */
	private Integer cdFormularioContratoCliente;

	/** Atributo cdIndicadorEmissaoAviso. */
	private Integer cdIndicadorEmissaoAviso;

	/** Atributo cdIndicadorSegundaLinha. */
	private Integer cdIndicadorSegundaLinha;

	/** Atributo cdusuario. */
	private String cdusuario;

	/** Atributo cdUsuarioExterno. */
	private String cdUsuarioExterno;

	/** Atributo cdCanal. */
	private Integer cdCanal;

	/** Atributo nmOperacaoFluxo. */
	private String nmOperacaoFluxo;

	/** Atributo cdEmpresaOperante. */
	private Long cdEmpresaOperante;

	/** Atributo cdDependenteOperante. */
	private Integer cdDependenteOperante;

	/** Atributo cdIndicadorFeriadoLocal. */
	private Integer cdIndicadorFeriadoLocal;

	/** Atributo cdPreenchimentoLancamentoPersonalizado. */
	private Integer cdPreenchimentoLancamentoPersonalizado;

	/** Atributo cdIndicadorAgendaGrade. */
	private Integer cdIndicadorAgendaGrade;

	/** Atributo cdTituloDdaRetorno. */
	private Integer cdTituloDdaRetorno;
	
	/** Atributo cdTituloDdaRetorno. */
	private Integer qtDiaUtilPgto;
	
	/** Atributo cdIndicadorUtilizaMora. */
	private Integer cdIndicadorUtilizaMora;

	/** Atributo vlPercentualDiferencaTolerada. */
	private BigDecimal vlPercentualDiferencaTolerada = null;
	
	private Integer cdConsistenciaCpfCnpjBenefAvalNpc;
	
	private Integer cdIndicadorTipoRetornoInternet;
	

	/**
	 * Get: cdIndicadorEmissaoAviso.
	 *
	 * @return cdIndicadorEmissaoAviso
	 */
	public Integer getCdIndicadorEmissaoAviso() {
		return cdIndicadorEmissaoAviso;
	}

	/**
	 * Set: cdIndicadorEmissaoAviso.
	 *
	 * @param cdIndicadorEmissaoAviso the cd indicador emissao aviso
	 */
	public void setCdIndicadorEmissaoAviso(Integer cdIndicadorEmissaoAviso) {
		this.cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
	}

	/**
	 * Get: cdAcaoNaoVida.
	 *
	 * @return cdAcaoNaoVida
	 */
	public Integer getCdAcaoNaoVida() {
		return cdAcaoNaoVida;
	}

	/**
	 * Set: cdAcaoNaoVida.
	 *
	 * @param cdAcaoNaoVida the cd acao nao vida
	 */
	public void setCdAcaoNaoVida(Integer cdAcaoNaoVida) {
		this.cdAcaoNaoVida = cdAcaoNaoVida;
	}

	/**
	 * Get: cdAcertoDadoRecadastro.
	 *
	 * @return cdAcertoDadoRecadastro
	 */
	public Integer getCdAcertoDadoRecadastro() {
		return cdAcertoDadoRecadastro;
	}

	/**
	 * Set: cdAcertoDadoRecadastro.
	 *
	 * @param cdAcertoDadoRecadastro the cd acerto dado recadastro
	 */
	public void setCdAcertoDadoRecadastro(Integer cdAcertoDadoRecadastro) {
		this.cdAcertoDadoRecadastro = cdAcertoDadoRecadastro;
	}

	/**
	 * Get: cdAgendaDebitoVeiculo.
	 *
	 * @return cdAgendaDebitoVeiculo
	 */
	public Integer getCdAgendaDebitoVeiculo() {
		return cdAgendaDebitoVeiculo;
	}

	/**
	 * Set: cdAgendaDebitoVeiculo.
	 *
	 * @param cdAgendaDebitoVeiculo the cd agenda debito veiculo
	 */
	public void setCdAgendaDebitoVeiculo(Integer cdAgendaDebitoVeiculo) {
		this.cdAgendaDebitoVeiculo = cdAgendaDebitoVeiculo;
	}

	/**
	 * Get: cdAgendamentoDebitoVeiculoRastreamento.
	 *
	 * @return cdAgendamentoDebitoVeiculoRastreamento
	 */
	public int getCdAgendamentoDebitoVeiculoRastreamento() {
		return cdAgendamentoDebitoVeiculoRastreamento;
	}

	/**
	 * Set: cdAgendamentoDebitoVeiculoRastreamento.
	 *
	 * @param cdAgendamentoDebitoVeiculoRastreamento the cd agendamento debito veiculo rastreamento
	 */
	public void setCdAgendamentoDebitoVeiculoRastreamento(
			int cdAgendamentoDebitoVeiculoRastreamento) {
		this.cdAgendamentoDebitoVeiculoRastreamento = cdAgendamentoDebitoVeiculoRastreamento;
	}

	/**
	 * Get: cdAgendaPagamentoVencido.
	 *
	 * @return cdAgendaPagamentoVencido
	 */
	public Integer getCdAgendaPagamentoVencido() {
		return cdAgendaPagamentoVencido;
	}

	/**
	 * Set: cdAgendaPagamentoVencido.
	 *
	 * @param cdAgendaPagamentoVencido the cd agenda pagamento vencido
	 */
	public void setCdAgendaPagamentoVencido(Integer cdAgendaPagamentoVencido) {
		this.cdAgendaPagamentoVencido = cdAgendaPagamentoVencido;
	}

	/**
	 * Get: cdAgendaRastreabilidadeFilial.
	 *
	 * @return cdAgendaRastreabilidadeFilial
	 */
	public Integer getCdAgendaRastreabilidadeFilial() {
		return cdAgendaRastreabilidadeFilial;
	}

	/**
	 * Set: cdAgendaRastreabilidadeFilial.
	 *
	 * @param cdAgendaRastreabilidadeFilial the cd agenda rastreabilidade filial
	 */
	public void setCdAgendaRastreabilidadeFilial(
			Integer cdAgendaRastreabilidadeFilial) {
		this.cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
	}

	/**
	 * Get: cdAgendaValorMenor.
	 *
	 * @return cdAgendaValorMenor
	 */
	public Integer getCdAgendaValorMenor() {
		return cdAgendaValorMenor;
	}

	/**
	 * Set: cdAgendaValorMenor.
	 *
	 * @param cdAgendaValorMenor the cd agenda valor menor
	 */
	public void setCdAgendaValorMenor(Integer cdAgendaValorMenor) {
		this.cdAgendaValorMenor = cdAgendaValorMenor;
	}

	/**
	 * Get: cdAgrupamentoAviso.
	 *
	 * @return cdAgrupamentoAviso
	 */
	public Integer getCdAgrupamentoAviso() {
		return cdAgrupamentoAviso;
	}

	/**
	 * Set: cdAgrupamentoAviso.
	 *
	 * @param cdAgrupamentoAviso the cd agrupamento aviso
	 */
	public void setCdAgrupamentoAviso(Integer cdAgrupamentoAviso) {
		this.cdAgrupamentoAviso = cdAgrupamentoAviso;
	}

	/**
	 * Get: cdAgrupamentoComprovado.
	 *
	 * @return cdAgrupamentoComprovado
	 */
	public Integer getCdAgrupamentoComprovado() {
		return cdAgrupamentoComprovado;
	}

	/**
	 * Set: cdAgrupamentoComprovado.
	 *
	 * @param cdAgrupamentoComprovado the cd agrupamento comprovado
	 */
	public void setCdAgrupamentoComprovado(Integer cdAgrupamentoComprovado) {
		this.cdAgrupamentoComprovado = cdAgrupamentoComprovado;
	}

	/**
	 * Get: cdAgrupamentoFormularioRecadastro.
	 *
	 * @return cdAgrupamentoFormularioRecadastro
	 */
	public Integer getCdAgrupamentoFormularioRecadastro() {
		return cdAgrupamentoFormularioRecadastro;
	}

	/**
	 * Set: cdAgrupamentoFormularioRecadastro.
	 *
	 * @param cdAgrupamentoFormularioRecadastro the cd agrupamento formulario recadastro
	 */
	public void setCdAgrupamentoFormularioRecadastro(
			Integer cdAgrupamentoFormularioRecadastro) {
		this.cdAgrupamentoFormularioRecadastro = cdAgrupamentoFormularioRecadastro;
	}

	/**
	 * Get: cdAgruparCorrespondencia.
	 *
	 * @return cdAgruparCorrespondencia
	 */
	public int getCdAgruparCorrespondencia() {
		return cdAgruparCorrespondencia;
	}

	/**
	 * Set: cdAgruparCorrespondencia.
	 *
	 * @param cdAgruparCorrespondencia the cd agrupar correspondencia
	 */
	public void setCdAgruparCorrespondencia(int cdAgruparCorrespondencia) {
		this.cdAgruparCorrespondencia = cdAgruparCorrespondencia;
	}

	/**
	 * Get: cdAntecRecadastroBeneficio.
	 *
	 * @return cdAntecRecadastroBeneficio
	 */
	public Integer getCdAntecRecadastroBeneficio() {
		return cdAntecRecadastroBeneficio;
	}

	/**
	 * Set: cdAntecRecadastroBeneficio.
	 *
	 * @param cdAntecRecadastroBeneficio the cd antec recadastro beneficio
	 */
	public void setCdAntecRecadastroBeneficio(Integer cdAntecRecadastroBeneficio) {
		this.cdAntecRecadastroBeneficio = cdAntecRecadastroBeneficio;
	}

	/**
	 * Get: cdAreaReservada.
	 *
	 * @return cdAreaReservada
	 */
	public Integer getCdAreaReservada() {
		return cdAreaReservada;
	}

	/**
	 * Set: cdAreaReservada.
	 *
	 * @param cdAreaReservada the cd area reservada
	 */
	public void setCdAreaReservada(Integer cdAreaReservada) {
		this.cdAreaReservada = cdAreaReservada;
	}

	/**
	 * Get: cdBaseDadosUtilizada.
	 *
	 * @return cdBaseDadosUtilizada
	 */
	public int getCdBaseDadosUtilizada() {
		return cdBaseDadosUtilizada;
	}

	/**
	 * Set: cdBaseDadosUtilizada.
	 *
	 * @param cdBaseDadosUtilizada the cd base dados utilizada
	 */
	public void setCdBaseDadosUtilizada(int cdBaseDadosUtilizada) {
		this.cdBaseDadosUtilizada = cdBaseDadosUtilizada;
	}

	/**
	 * Get: cdBaseRecadastroBeneficio.
	 *
	 * @return cdBaseRecadastroBeneficio
	 */
	public Integer getCdBaseRecadastroBeneficio() {
		return cdBaseRecadastroBeneficio;
	}

	/**
	 * Set: cdBaseRecadastroBeneficio.
	 *
	 * @param cdBaseRecadastroBeneficio the cd base recadastro beneficio
	 */
	public void setCdBaseRecadastroBeneficio(Integer cdBaseRecadastroBeneficio) {
		this.cdBaseRecadastroBeneficio = cdBaseRecadastroBeneficio;
	}

	/**
	 * Get: cdBloqueioEmissaoPplta.
	 *
	 * @return cdBloqueioEmissaoPplta
	 */
	public Integer getCdBloqueioEmissaoPplta() {
		return cdBloqueioEmissaoPplta;
	}

	/**
	 * Set: cdBloqueioEmissaoPplta.
	 *
	 * @param cdBloqueioEmissaoPplta the cd bloqueio emissao pplta
	 */
	public void setCdBloqueioEmissaoPplta(Integer cdBloqueioEmissaoPplta) {
		this.cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
	}

	/**
	 * Get: cdCanal.
	 *
	 * @return cdCanal
	 */
	public Integer getCdCanal() {
		return cdCanal;
	}

	/**
	 * Set: cdCanal.
	 *
	 * @param cdCanal the cd canal
	 */
	public void setCdCanal(Integer cdCanal) {
		this.cdCanal = cdCanal;
	}

	/**
	 * Get: cdCapituloTituloRegistro.
	 *
	 * @return cdCapituloTituloRegistro
	 */
	public Integer getCdCapituloTituloRegistro() {
		return cdCapituloTituloRegistro;
	}

	/**
	 * Set: cdCapituloTituloRegistro.
	 *
	 * @param cdCapituloTituloRegistro the cd capitulo titulo registro
	 */
	public void setCdCapituloTituloRegistro(Integer cdCapituloTituloRegistro) {
		this.cdCapituloTituloRegistro = cdCapituloTituloRegistro;
	}

	/**
	 * Get: cdCobrancaTarifa.
	 *
	 * @return cdCobrancaTarifa
	 */
	public Integer getCdCobrancaTarifa() {
		return cdCobrancaTarifa;
	}

	/**
	 * Set: cdCobrancaTarifa.
	 *
	 * @param cdCobrancaTarifa the cd cobranca tarifa
	 */
	public void setCdCobrancaTarifa(Integer cdCobrancaTarifa) {
		this.cdCobrancaTarifa = cdCobrancaTarifa;
	}

	/**
	 * Get: cdCobrarTarifasFuncionarios.
	 *
	 * @return cdCobrarTarifasFuncionarios
	 */
	public int getCdCobrarTarifasFuncionarios() {
		return cdCobrarTarifasFuncionarios;
	}

	/**
	 * Set: cdCobrarTarifasFuncionarios.
	 *
	 * @param cdCobrarTarifasFuncionarios the cd cobrar tarifas funcionarios
	 */
	public void setCdCobrarTarifasFuncionarios(int cdCobrarTarifasFuncionarios) {
		this.cdCobrarTarifasFuncionarios = cdCobrarTarifasFuncionarios;
	}

	/**
	 * Get: cdCondicaoEnquadramento.
	 *
	 * @return cdCondicaoEnquadramento
	 */
	public int getCdCondicaoEnquadramento() {
		return cdCondicaoEnquadramento;
	}

	/**
	 * Set: cdCondicaoEnquadramento.
	 *
	 * @param cdCondicaoEnquadramento the cd condicao enquadramento
	 */
	public void setCdCondicaoEnquadramento(int cdCondicaoEnquadramento) {
		this.cdCondicaoEnquadramento = cdCondicaoEnquadramento;
	}

	/**
	 * Get: cdConsDebitoVeiculo.
	 *
	 * @return cdConsDebitoVeiculo
	 */
	public Integer getCdConsDebitoVeiculo() {
		return cdConsDebitoVeiculo;
	}

	/**
	 * Set: cdConsDebitoVeiculo.
	 *
	 * @param cdConsDebitoVeiculo the cd cons debito veiculo
	 */
	public void setCdConsDebitoVeiculo(Integer cdConsDebitoVeiculo) {
		this.cdConsDebitoVeiculo = cdConsDebitoVeiculo;
	}

	/**
	 * Get: cdConsEndereco.
	 *
	 * @return cdConsEndereco
	 */
	public Integer getCdConsEndereco() {
		return cdConsEndereco;
	}

	/**
	 * Set: cdConsEndereco.
	 *
	 * @param cdConsEndereco the cd cons endereco
	 */
	public void setCdConsEndereco(Integer cdConsEndereco) {
		this.cdConsEndereco = cdConsEndereco;
	}

	/**
	 * Get: cdConsistenciaCpfCnpjFavorecido.
	 *
	 * @return cdConsistenciaCpfCnpjFavorecido
	 */
	public int getCdConsistenciaCpfCnpjFavorecido() {
		return cdConsistenciaCpfCnpjFavorecido;
	}

	/**
	 * Set: cdConsistenciaCpfCnpjFavorecido.
	 *
	 * @param cdConsistenciaCpfCnpjFavorecido the cd consistencia cpf cnpj favorecido
	 */
	public void setCdConsistenciaCpfCnpjFavorecido(
			int cdConsistenciaCpfCnpjFavorecido) {
		this.cdConsistenciaCpfCnpjFavorecido = cdConsistenciaCpfCnpjFavorecido;
	}

	/**
	 * Get: cdConsSaldoPagamento.
	 *
	 * @return cdConsSaldoPagamento
	 */
	public Integer getCdConsSaldoPagamento() {
		return cdConsSaldoPagamento;
	}

	/**
	 * Set: cdConsSaldoPagamento.
	 *
	 * @param cdConsSaldoPagamento the cd cons saldo pagamento
	 */
	public void setCdConsSaldoPagamento(Integer cdConsSaldoPagamento) {
		this.cdConsSaldoPagamento = cdConsSaldoPagamento;
	}

	/**
	 * Get: cdContagemConsSaudo.
	 *
	 * @return cdContagemConsSaudo
	 */
	public Integer getCdContagemConsSaudo() {
		return cdContagemConsSaudo;
	}

	/**
	 * Set: cdContagemConsSaudo.
	 *
	 * @param cdContagemConsSaudo the cd contagem cons saudo
	 */
	public void setCdContagemConsSaudo(Integer cdContagemConsSaudo) {
		this.cdContagemConsSaudo = cdContagemConsSaudo;
	}

	/**
	 * Get: cdContratoContaTransferencia.
	 *
	 * @return cdContratoContaTransferencia
	 */
	public Integer getCdContratoContaTransferencia() {
		return cdContratoContaTransferencia;
	}

	/**
	 * Set: cdContratoContaTransferencia.
	 *
	 * @param cdContratoContaTransferencia the cd contrato conta transferencia
	 */
	public void setCdContratoContaTransferencia(Integer cdContratoContaTransferencia) {
		this.cdContratoContaTransferencia = cdContratoContaTransferencia;
	}

	/**
	 * Get: cdControlePagamentoFavorecido.
	 *
	 * @return cdControlePagamentoFavorecido
	 */
	public int getCdControlePagamentoFavorecido() {
		return cdControlePagamentoFavorecido;
	}

	/**
	 * Set: cdControlePagamentoFavorecido.
	 *
	 * @param cdControlePagamentoFavorecido the cd controle pagamento favorecido
	 */
	public void setCdControlePagamentoFavorecido(int cdControlePagamentoFavorecido) {
		this.cdControlePagamentoFavorecido = cdControlePagamentoFavorecido;
	}

	/**
	 * Get: cdControlePagFavorecido.
	 *
	 * @return cdControlePagFavorecido
	 */
	public int getCdControlePagFavorecido() {
		return cdControlePagFavorecido;
	}

	/**
	 * Set: cdControlePagFavorecido.
	 *
	 * @param cdControlePagFavorecido the cd controle pag favorecido
	 */
	public void setCdControlePagFavorecido(int cdControlePagFavorecido) {
		this.cdControlePagFavorecido = cdControlePagFavorecido;
	}

	/**
	 * Get: cdCreditoNaoUtilizado.
	 *
	 * @return cdCreditoNaoUtilizado
	 */
	public Integer getCdCreditoNaoUtilizado() {
		return cdCreditoNaoUtilizado;
	}

	/**
	 * Set: cdCreditoNaoUtilizado.
	 *
	 * @param cdCreditoNaoUtilizado the cd credito nao utilizado
	 */
	public void setCdCreditoNaoUtilizado(Integer cdCreditoNaoUtilizado) {
		this.cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
	}

	/**
	 * Get: cdCriterioCompostoEnquadramento.
	 *
	 * @return cdCriterioCompostoEnquadramento
	 */
	public int getCdCriterioCompostoEnquadramento() {
		return cdCriterioCompostoEnquadramento;
	}

	/**
	 * Set: cdCriterioCompostoEnquadramento.
	 *
	 * @param cdCriterioCompostoEnquadramento the cd criterio composto enquadramento
	 */
	public void setCdCriterioCompostoEnquadramento(
			int cdCriterioCompostoEnquadramento) {
		this.cdCriterioCompostoEnquadramento = cdCriterioCompostoEnquadramento;
	}

	/**
	 * Get: cdCriterioEnquaBeneficio.
	 *
	 * @return cdCriterioEnquaBeneficio
	 */
	public Integer getCdCriterioEnquaBeneficio() {
		return cdCriterioEnquaBeneficio;
	}

	/**
	 * Set: cdCriterioEnquaBeneficio.
	 *
	 * @param cdCriterioEnquaBeneficio the cd criterio enqua beneficio
	 */
	public void setCdCriterioEnquaBeneficio(Integer cdCriterioEnquaBeneficio) {
		this.cdCriterioEnquaBeneficio = cdCriterioEnquaBeneficio;
	}

	/**
	 * Get: cdCriterioEnquaRecadastro.
	 *
	 * @return cdCriterioEnquaRecadastro
	 */
	public Integer getCdCriterioEnquaRecadastro() {
		return cdCriterioEnquaRecadastro;
	}

	/**
	 * Set: cdCriterioEnquaRecadastro.
	 *
	 * @param cdCriterioEnquaRecadastro the cd criterio enqua recadastro
	 */
	public void setCdCriterioEnquaRecadastro(Integer cdCriterioEnquaRecadastro) {
		this.cdCriterioEnquaRecadastro = cdCriterioEnquaRecadastro;
	}

	/**
	 * Get: cdCriterioPrincipalEnquadramento.
	 *
	 * @return cdCriterioPrincipalEnquadramento
	 */
	public int getCdCriterioPrincipalEnquadramento() {
		return cdCriterioPrincipalEnquadramento;
	}

	/**
	 * Set: cdCriterioPrincipalEnquadramento.
	 *
	 * @param cdCriterioPrincipalEnquadramento the cd criterio principal enquadramento
	 */
	public void setCdCriterioPrincipalEnquadramento(
			int cdCriterioPrincipalEnquadramento) {
		this.cdCriterioPrincipalEnquadramento = cdCriterioPrincipalEnquadramento;
	}

	/**
	 * Get: cdCriterioRastreabilidadeTitulo.
	 *
	 * @return cdCriterioRastreabilidadeTitulo
	 */
	public Integer getCdCriterioRastreabilidadeTitulo() {
		return cdCriterioRastreabilidadeTitulo;
	}

	/**
	 * Set: cdCriterioRastreabilidadeTitulo.
	 *
	 * @param cdCriterioRastreabilidadeTitulo the cd criterio rastreabilidade titulo
	 */
	public void setCdCriterioRastreabilidadeTitulo(
			Integer cdCriterioRastreabilidadeTitulo) {
		this.cdCriterioRastreabilidadeTitulo = cdCriterioRastreabilidadeTitulo;
	}

	/**
	 * Get: cdCtciaEspecieBeneficio.
	 *
	 * @return cdCtciaEspecieBeneficio
	 */
	public Integer getCdCtciaEspecieBeneficio() {
		return cdCtciaEspecieBeneficio;
	}

	/**
	 * Set: cdCtciaEspecieBeneficio.
	 *
	 * @param cdCtciaEspecieBeneficio the cd ctcia especie beneficio
	 */
	public void setCdCtciaEspecieBeneficio(Integer cdCtciaEspecieBeneficio) {
		this.cdCtciaEspecieBeneficio = cdCtciaEspecieBeneficio;
	}

	/**
	 * Get: cdCtciaIdentificacaoBeneficio.
	 *
	 * @return cdCtciaIdentificacaoBeneficio
	 */
	public Integer getCdCtciaIdentificacaoBeneficio() {
		return cdCtciaIdentificacaoBeneficio;
	}

	/**
	 * Set: cdCtciaIdentificacaoBeneficio.
	 *
	 * @param cdCtciaIdentificacaoBeneficio the cd ctcia identificacao beneficio
	 */
	public void setCdCtciaIdentificacaoBeneficio(
			Integer cdCtciaIdentificacaoBeneficio) {
		this.cdCtciaIdentificacaoBeneficio = cdCtciaIdentificacaoBeneficio;
	}

	/**
	 * Get: cdCtciaInscricaoFavorecido.
	 *
	 * @return cdCtciaInscricaoFavorecido
	 */
	public Integer getCdCtciaInscricaoFavorecido() {
		return cdCtciaInscricaoFavorecido;
	}

	/**
	 * Set: cdCtciaInscricaoFavorecido.
	 *
	 * @param cdCtciaInscricaoFavorecido the cd ctcia inscricao favorecido
	 */
	public void setCdCtciaInscricaoFavorecido(Integer cdCtciaInscricaoFavorecido) {
		this.cdCtciaInscricaoFavorecido = cdCtciaInscricaoFavorecido;
	}

	/**
	 * Get: cdCtciaProprietarioVeiculo.
	 *
	 * @return cdCtciaProprietarioVeiculo
	 */
	public Integer getCdCtciaProprietarioVeiculo() {
		return cdCtciaProprietarioVeiculo;
	}

	/**
	 * Set: cdCtciaProprietarioVeiculo.
	 *
	 * @param cdCtciaProprietarioVeiculo the cd ctcia proprietario veiculo
	 */
	public void setCdCtciaProprietarioVeiculo(Integer cdCtciaProprietarioVeiculo) {
		this.cdCtciaProprietarioVeiculo = cdCtciaProprietarioVeiculo;
	}

	/**
	 * Get: cdDemonstraInformacoesAreaReservada.
	 *
	 * @return cdDemonstraInformacoesAreaReservada
	 */
	public int getCdDemonstraInformacoesAreaReservada() {
		return cdDemonstraInformacoesAreaReservada;
	}

	/**
	 * Set: cdDemonstraInformacoesAreaReservada.
	 *
	 * @param cdDemonstraInformacoesAreaReservada the cd demonstra informacoes area reservada
	 */
	public void setCdDemonstraInformacoesAreaReservada(
			int cdDemonstraInformacoesAreaReservada) {
		this.cdDemonstraInformacoesAreaReservada = cdDemonstraInformacoesAreaReservada;
	}

	/**
	 * Get: cdDependenteOperante.
	 *
	 * @return cdDependenteOperante
	 */
	public Integer getCdDependenteOperante() {
		return cdDependenteOperante;
	}

	/**
	 * Set: cdDependenteOperante.
	 *
	 * @param cdDependenteOperante the cd dependente operante
	 */
	public void setCdDependenteOperante(Integer cdDependenteOperante) {
		this.cdDependenteOperante = cdDependenteOperante;
	}

	/**
	 * Get: cdDestino.
	 *
	 * @return cdDestino
	 */
	public int getCdDestino() {
		return cdDestino;
	}

	/**
	 * Set: cdDestino.
	 *
	 * @param cdDestino the cd destino
	 */
	public void setCdDestino(int cdDestino) {
		this.cdDestino = cdDestino;
	}

	/**
	 * Get: cdDestinoAviso.
	 *
	 * @return cdDestinoAviso
	 */
	public Integer getCdDestinoAviso() {
		return cdDestinoAviso;
	}

	/**
	 * Set: cdDestinoAviso.
	 *
	 * @param cdDestinoAviso the cd destino aviso
	 */
	public void setCdDestinoAviso(Integer cdDestinoAviso) {
		this.cdDestinoAviso = cdDestinoAviso;
	}

	/**
	 * Get: cdDestinoComprovante.
	 *
	 * @return cdDestinoComprovante
	 */
	public Integer getCdDestinoComprovante() {
		return cdDestinoComprovante;
	}

	/**
	 * Set: cdDestinoComprovante.
	 *
	 * @param cdDestinoComprovante the cd destino comprovante
	 */
	public void setCdDestinoComprovante(Integer cdDestinoComprovante) {
		this.cdDestinoComprovante = cdDestinoComprovante;
	}

	/**
	 * Get: cdDestinoFormularioRecadastro.
	 *
	 * @return cdDestinoFormularioRecadastro
	 */
	public Integer getCdDestinoFormularioRecadastro() {
		return cdDestinoFormularioRecadastro;
	}

	/**
	 * Set: cdDestinoFormularioRecadastro.
	 *
	 * @param cdDestinoFormularioRecadastro the cd destino formulario recadastro
	 */
	public void setCdDestinoFormularioRecadastro(
			Integer cdDestinoFormularioRecadastro) {
		this.cdDestinoFormularioRecadastro = cdDestinoFormularioRecadastro;
	}

	/**
	 * Get: cdDiaFloatPagamento.
	 *
	 * @return cdDiaFloatPagamento
	 */
	public Integer getCdDiaFloatPagamento() {
		return cdDiaFloatPagamento;
	}

	/**
	 * Set: cdDiaFloatPagamento.
	 *
	 * @param cdDiaFloatPagamento the cd dia float pagamento
	 */
	public void setCdDiaFloatPagamento(Integer cdDiaFloatPagamento) {
		this.cdDiaFloatPagamento = cdDiaFloatPagamento;
	}

	/**
	 * Get: cdDispzContaCredito.
	 *
	 * @return cdDispzContaCredito
	 */
	public Integer getCdDispzContaCredito() {
		return cdDispzContaCredito;
	}

	/**
	 * Set: cdDispzContaCredito.
	 *
	 * @param cdDispzContaCredito the cd dispz conta credito
	 */
	public void setCdDispzContaCredito(Integer cdDispzContaCredito) {
		this.cdDispzContaCredito = cdDispzContaCredito;
	}

	/**
	 * Get: cdDispzDiversarCrrtt.
	 *
	 * @return cdDispzDiversarCrrtt
	 */
	public Integer getCdDispzDiversarCrrtt() {
		return cdDispzDiversarCrrtt;
	}

	/**
	 * Set: cdDispzDiversarCrrtt.
	 *
	 * @param cdDispzDiversarCrrtt the cd dispz diversar crrtt
	 */
	public void setCdDispzDiversarCrrtt(Integer cdDispzDiversarCrrtt) {
		this.cdDispzDiversarCrrtt = cdDispzDiversarCrrtt;
	}

	/**
	 * Get: cdDispzDiversasNao.
	 *
	 * @return cdDispzDiversasNao
	 */
	public Integer getCdDispzDiversasNao() {
		return cdDispzDiversasNao;
	}

	/**
	 * Set: cdDispzDiversasNao.
	 *
	 * @param cdDispzDiversasNao the cd dispz diversas nao
	 */
	public void setCdDispzDiversasNao(Integer cdDispzDiversasNao) {
		this.cdDispzDiversasNao = cdDispzDiversasNao;
	}

	/**
	 * Get: cdDIspzSalarioCrrtt.
	 *
	 * @return cdDIspzSalarioCrrtt
	 */
	public Integer getCdDIspzSalarioCrrtt() {
		return cdDIspzSalarioCrrtt;
	}

	/**
	 * Set: cdDIspzSalarioCrrtt.
	 *
	 * @param cdDIspzSalarioCrrtt the cd d ispz salario crrtt
	 */
	public void setCdDIspzSalarioCrrtt(Integer cdDIspzSalarioCrrtt) {
		this.cdDIspzSalarioCrrtt = cdDIspzSalarioCrrtt;
	}

	/**
	 * Get: cdDispzSalarioNao.
	 *
	 * @return cdDispzSalarioNao
	 */
	public Integer getCdDispzSalarioNao() {
		return cdDispzSalarioNao;
	}

	/**
	 * Set: cdDispzSalarioNao.
	 *
	 * @param cdDispzSalarioNao the cd dispz salario nao
	 */
	public void setCdDispzSalarioNao(Integer cdDispzSalarioNao) {
		this.cdDispzSalarioNao = cdDispzSalarioNao;
	}

	/**
	 * Get: cdEfetuaConsistencia.
	 *
	 * @return cdEfetuaConsistencia
	 */
	public int getCdEfetuaConsistencia() {
		return cdEfetuaConsistencia;
	}

	/**
	 * Set: cdEfetuaConsistencia.
	 *
	 * @param cdEfetuaConsistencia the cd efetua consistencia
	 */
	public void setCdEfetuaConsistencia(int cdEfetuaConsistencia) {
		this.cdEfetuaConsistencia = cdEfetuaConsistencia;
	}

	/**
	 * Get: cdEmiteMensagemRecadastramentoMidiaOnLine.
	 *
	 * @return cdEmiteMensagemRecadastramentoMidiaOnLine
	 */
	public int getCdEmiteMensagemRecadastramentoMidiaOnLine() {
		return cdEmiteMensagemRecadastramentoMidiaOnLine;
	}

	/**
	 * Set: cdEmiteMensagemRecadastramentoMidiaOnLine.
	 *
	 * @param cdEmiteMensagemRecadastramentoMidiaOnLine the cd emite mensagem recadastramento midia on line
	 */
	public void setCdEmiteMensagemRecadastramentoMidiaOnLine(
			int cdEmiteMensagemRecadastramentoMidiaOnLine) {
		this.cdEmiteMensagemRecadastramentoMidiaOnLine = cdEmiteMensagemRecadastramentoMidiaOnLine;
	}

	/**
	 * Get: cdEmpresaOperante.
	 *
	 * @return cdEmpresaOperante
	 */
	public Long getCdEmpresaOperante() {
		return cdEmpresaOperante;
	}

	/**
	 * Set: cdEmpresaOperante.
	 *
	 * @param cdEmpresaOperante the cd empresa operante
	 */
	public void setCdEmpresaOperante(Long cdEmpresaOperante) {
		this.cdEmpresaOperante = cdEmpresaOperante;
	}

	/**
	 * Get: cdEnderecoEnvio.
	 *
	 * @return cdEnderecoEnvio
	 */
	public int getCdEnderecoEnvio() {
		return cdEnderecoEnvio;
	}

	/**
	 * Set: cdEnderecoEnvio.
	 *
	 * @param cdEnderecoEnvio the cd endereco envio
	 */
	public void setCdEnderecoEnvio(int cdEnderecoEnvio) {
		this.cdEnderecoEnvio = cdEnderecoEnvio;
	}

	/**
	 * Get: cdEnderecoUtilizadoEnvio.
	 *
	 * @return cdEnderecoUtilizadoEnvio
	 */
	public int getCdEnderecoUtilizadoEnvio() {
		return cdEnderecoUtilizadoEnvio;
	}

	/**
	 * Set: cdEnderecoUtilizadoEnvio.
	 *
	 * @param cdEnderecoUtilizadoEnvio the cd endereco utilizado envio
	 */
	public void setCdEnderecoUtilizadoEnvio(int cdEnderecoUtilizadoEnvio) {
		this.cdEnderecoUtilizadoEnvio = cdEnderecoUtilizadoEnvio;
	}

	/**
	 * Get: cdEnvelopeAberto.
	 *
	 * @return cdEnvelopeAberto
	 */
	public Integer getCdEnvelopeAberto() {
		return cdEnvelopeAberto;
	}

	/**
	 * Set: cdEnvelopeAberto.
	 *
	 * @param cdEnvelopeAberto the cd envelope aberto
	 */
	public void setCdEnvelopeAberto(Integer cdEnvelopeAberto) {
		this.cdEnvelopeAberto = cdEnvelopeAberto;
	}

	/**
	 * Get: cdFavorecidoConsPagamento.
	 *
	 * @return cdFavorecidoConsPagamento
	 */
	public Integer getCdFavorecidoConsPagamento() {
		return cdFavorecidoConsPagamento;
	}

	/**
	 * Set: cdFavorecidoConsPagamento.
	 *
	 * @param cdFavorecidoConsPagamento the cd favorecido cons pagamento
	 */
	public void setCdFavorecidoConsPagamento(Integer cdFavorecidoConsPagamento) {
		this.cdFavorecidoConsPagamento = cdFavorecidoConsPagamento;
	}

	/**
	 * Get: cdFechamentoApuracao.
	 *
	 * @return cdFechamentoApuracao
	 */
	public int getCdFechamentoApuracao() {
		return cdFechamentoApuracao;
	}

	/**
	 * Set: cdFechamentoApuracao.
	 *
	 * @param cdFechamentoApuracao the cd fechamento apuracao
	 */
	public void setCdFechamentoApuracao(int cdFechamentoApuracao) {
		this.cdFechamentoApuracao = cdFechamentoApuracao;
	}

	/**
	 * Get: cdFormaAutorizacaoPagamento.
	 *
	 * @return cdFormaAutorizacaoPagamento
	 */
	public Integer getCdFormaAutorizacaoPagamento() {
		return cdFormaAutorizacaoPagamento;
	}

	/**
	 * Set: cdFormaAutorizacaoPagamento.
	 *
	 * @param cdFormaAutorizacaoPagamento the cd forma autorizacao pagamento
	 */
	public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
		this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
	}

	/**
	 * Get: cdFormaControleExpiracaoCredito.
	 *
	 * @return cdFormaControleExpiracaoCredito
	 */
	public int getCdFormaControleExpiracaoCredito() {
		return cdFormaControleExpiracaoCredito;
	}

	/**
	 * Set: cdFormaControleExpiracaoCredito.
	 *
	 * @param cdFormaControleExpiracaoCredito the cd forma controle expiracao credito
	 */
	public void setCdFormaControleExpiracaoCredito(
			int cdFormaControleExpiracaoCredito) {
		this.cdFormaControleExpiracaoCredito = cdFormaControleExpiracaoCredito;
	}

	/**
	 * Get: cdFormaEnvioPagamento.
	 *
	 * @return cdFormaEnvioPagamento
	 */
	public Integer getCdFormaEnvioPagamento() {
		return cdFormaEnvioPagamento;
	}

	/**
	 * Set: cdFormaEnvioPagamento.
	 *
	 * @param cdFormaEnvioPagamento the cd forma envio pagamento
	 */
	public void setCdFormaEnvioPagamento(Integer cdFormaEnvioPagamento) {
		this.cdFormaEnvioPagamento = cdFormaEnvioPagamento;
	}

	/**
	 * Get: cdFormaEstornoCredito.
	 *
	 * @return cdFormaEstornoCredito
	 */
	public Integer getCdFormaEstornoCredito() {
		return cdFormaEstornoCredito;
	}

	/**
	 * Set: cdFormaEstornoCredito.
	 *
	 * @param cdFormaEstornoCredito the cd forma estorno credito
	 */
	public void setCdFormaEstornoCredito(Integer cdFormaEstornoCredito) {
		this.cdFormaEstornoCredito = cdFormaEstornoCredito;
	}

	/**
	 * Get: cdFormaExpiracaoCredito.
	 *
	 * @return cdFormaExpiracaoCredito
	 */
	public Integer getCdFormaExpiracaoCredito() {
		return cdFormaExpiracaoCredito;
	}

	/**
	 * Set: cdFormaExpiracaoCredito.
	 *
	 * @param cdFormaExpiracaoCredito the cd forma expiracao credito
	 */
	public void setCdFormaExpiracaoCredito(Integer cdFormaExpiracaoCredito) {
		this.cdFormaExpiracaoCredito = cdFormaExpiracaoCredito;
	}

	/**
	 * Get: cdFormaManutencao.
	 *
	 * @return cdFormaManutencao
	 */
	public Integer getCdFormaManutencao() {
		return cdFormaManutencao;
	}

	/**
	 * Set: cdFormaManutencao.
	 *
	 * @param cdFormaManutencao the cd forma manutencao
	 */
	public void setCdFormaManutencao(Integer cdFormaManutencao) {
		this.cdFormaManutencao = cdFormaManutencao;
	}

	/**
	 * Get: cdFormaManutencaoCadastro.
	 *
	 * @return cdFormaManutencaoCadastro
	 */
	public int getCdFormaManutencaoCadastro() {
		return cdFormaManutencaoCadastro;
	}

	/**
	 * Set: cdFormaManutencaoCadastro.
	 *
	 * @param cdFormaManutencaoCadastro the cd forma manutencao cadastro
	 */
	public void setCdFormaManutencaoCadastro(int cdFormaManutencaoCadastro) {
		this.cdFormaManutencaoCadastro = cdFormaManutencaoCadastro;
	}

	/**
	 * Get: cdFormaManutencaoCadastroProcurador.
	 *
	 * @return cdFormaManutencaoCadastroProcurador
	 */
	public int getCdFormaManutencaoCadastroProcurador() {
		return cdFormaManutencaoCadastroProcurador;
	}

	/**
	 * Set: cdFormaManutencaoCadastroProcurador.
	 *
	 * @param cdFormaManutencaoCadastroProcurador the cd forma manutencao cadastro procurador
	 */
	public void setCdFormaManutencaoCadastroProcurador(
			int cdFormaManutencaoCadastroProcurador) {
		this.cdFormaManutencaoCadastroProcurador = cdFormaManutencaoCadastroProcurador;
	}

	/**
	 * Get: cdFormularioContratoCliente.
	 *
	 * @return cdFormularioContratoCliente
	 */
	public Integer getCdFormularioContratoCliente() {
		return cdFormularioContratoCliente;
	}

	/**
	 * Set: cdFormularioContratoCliente.
	 *
	 * @param cdFormularioContratoCliente the cd formulario contrato cliente
	 */
	public void setCdFormularioContratoCliente(Integer cdFormularioContratoCliente) {
		this.cdFormularioContratoCliente = cdFormularioContratoCliente;
	}

	/**
	 * Get: cdFormularioParaImpressao.
	 *
	 * @return cdFormularioParaImpressao
	 */
	public int getCdFormularioParaImpressao() {
		return cdFormularioParaImpressao;
	}

	/**
	 * Set: cdFormularioParaImpressao.
	 *
	 * @param cdFormularioParaImpressao the cd formulario para impressao
	 */
	public void setCdFormularioParaImpressao(int cdFormularioParaImpressao) {
		this.cdFormularioParaImpressao = cdFormularioParaImpressao;
	}

	/**
	 * Get: cdFrasePreCadastro.
	 *
	 * @return cdFrasePreCadastro
	 */
	public Integer getCdFrasePreCadastro() {
		return cdFrasePreCadastro;
	}

	/**
	 * Set: cdFrasePreCadastro.
	 *
	 * @param cdFrasePreCadastro the cd frase pre cadastro
	 */
	public void setCdFrasePreCadastro(Integer cdFrasePreCadastro) {
		this.cdFrasePreCadastro = cdFrasePreCadastro;
	}

	/**
	 * Get: cdFrasesPreCadastratadas.
	 *
	 * @return cdFrasesPreCadastratadas
	 */
	public int getCdFrasesPreCadastratadas() {
		return cdFrasesPreCadastratadas;
	}

	/**
	 * Set: cdFrasesPreCadastratadas.
	 *
	 * @param cdFrasesPreCadastratadas the cd frases pre cadastratadas
	 */
	public void setCdFrasesPreCadastratadas(int cdFrasesPreCadastratadas) {
		this.cdFrasesPreCadastratadas = cdFrasesPreCadastratadas;
	}

	/**
	 * Get: cdIndicadorAdesaoSacador.
	 *
	 * @return cdIndicadorAdesaoSacador
	 */
	public Integer getCdIndicadorAdesaoSacador() {
		return cdIndicadorAdesaoSacador;
	}

	/**
	 * Set: cdIndicadorAdesaoSacador.
	 *
	 * @param cdIndicadorAdesaoSacador the cd indicador adesao sacador
	 */
	public void setCdIndicadorAdesaoSacador(Integer cdIndicadorAdesaoSacador) {
		this.cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
	}

	/**
	 * Get: cdIndicadorAgendaTitulo.
	 *
	 * @return cdIndicadorAgendaTitulo
	 */
	public Integer getCdIndicadorAgendaTitulo() {
		return cdIndicadorAgendaTitulo;
	}

	/**
	 * Set: cdIndicadorAgendaTitulo.
	 *
	 * @param cdIndicadorAgendaTitulo the cd indicador agenda titulo
	 */
	public void setCdIndicadorAgendaTitulo(Integer cdIndicadorAgendaTitulo) {
		this.cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
	}

	/**
	 * Get: cdIndicadorAutorizacaoCliente.
	 *
	 * @return cdIndicadorAutorizacaoCliente
	 */
	public Integer getCdIndicadorAutorizacaoCliente() {
		return cdIndicadorAutorizacaoCliente;
	}

	/**
	 * Set: cdIndicadorAutorizacaoCliente.
	 *
	 * @param cdIndicadorAutorizacaoCliente the cd indicador autorizacao cliente
	 */
	public void setCdIndicadorAutorizacaoCliente(
			Integer cdIndicadorAutorizacaoCliente) {
		this.cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
	}

	/**
	 * Get: cdIndicadorAutorizacaoComplemento.
	 *
	 * @return cdIndicadorAutorizacaoComplemento
	 */
	public Integer getCdIndicadorAutorizacaoComplemento() {
		return cdIndicadorAutorizacaoComplemento;
	}

	/**
	 * Set: cdIndicadorAutorizacaoComplemento.
	 *
	 * @param cdIndicadorAutorizacaoComplemento the cd indicador autorizacao complemento
	 */
	public void setCdIndicadorAutorizacaoComplemento(
			Integer cdIndicadorAutorizacaoComplemento) {
		this.cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
	}

	/**
	 * Get: cdIndicadorBancoPostal.
	 *
	 * @return cdIndicadorBancoPostal
	 */
	public Integer getCdIndicadorBancoPostal() {
		return cdIndicadorBancoPostal;
	}

	/**
	 * Set: cdIndicadorBancoPostal.
	 *
	 * @param cdIndicadorBancoPostal the cd indicador banco postal
	 */
	public void setCdIndicadorBancoPostal(Integer cdIndicadorBancoPostal) {
		this.cdIndicadorBancoPostal = cdIndicadorBancoPostal;
	}

	/**
	 * Get: cdIndicadorCadastroOrg.
	 *
	 * @return cdIndicadorCadastroOrg
	 */
	public Integer getCdIndicadorCadastroOrg() {
		return cdIndicadorCadastroOrg;
	}

	/**
	 * Set: cdIndicadorCadastroOrg.
	 *
	 * @param cdIndicadorCadastroOrg the cd indicador cadastro org
	 */
	public void setCdIndicadorCadastroOrg(Integer cdIndicadorCadastroOrg) {
		this.cdIndicadorCadastroOrg = cdIndicadorCadastroOrg;
	}

	/**
	 * Get: cdIndicadorCadastroProcd.
	 *
	 * @return cdIndicadorCadastroProcd
	 */
	public Integer getCdIndicadorCadastroProcd() {
		return cdIndicadorCadastroProcd;
	}

	/**
	 * Set: cdIndicadorCadastroProcd.
	 *
	 * @param cdIndicadorCadastroProcd the cd indicador cadastro procd
	 */
	public void setCdIndicadorCadastroProcd(Integer cdIndicadorCadastroProcd) {
		this.cdIndicadorCadastroProcd = cdIndicadorCadastroProcd;
	}

	/**
	 * Get: cdIndicadorCataoSalario.
	 *
	 * @return cdIndicadorCataoSalario
	 */
	public Integer getCdIndicadorCataoSalario() {
		return cdIndicadorCataoSalario;
	}

	/**
	 * Set: cdIndicadorCataoSalario.
	 *
	 * @param cdIndicadorCataoSalario the cd indicador catao salario
	 */
	public void setCdIndicadorCataoSalario(Integer cdIndicadorCataoSalario) {
		this.cdIndicadorCataoSalario = cdIndicadorCataoSalario;
	}

	/**
	 * Get: cdIndicadorExpiracaoCredito.
	 *
	 * @return cdIndicadorExpiracaoCredito
	 */
	public Integer getCdIndicadorExpiracaoCredito() {
		return cdIndicadorExpiracaoCredito;
	}

	/**
	 * Set: cdIndicadorExpiracaoCredito.
	 *
	 * @param cdIndicadorExpiracaoCredito the cd indicador expiracao credito
	 */
	public void setCdIndicadorExpiracaoCredito(Integer cdIndicadorExpiracaoCredito) {
		this.cdIndicadorExpiracaoCredito = cdIndicadorExpiracaoCredito;
	}

	/**
	 * Get: cdIndicadorLancamentoPagamento.
	 *
	 * @return cdIndicadorLancamentoPagamento
	 */
	public Integer getCdIndicadorLancamentoPagamento() {
		return cdIndicadorLancamentoPagamento;
	}

	/**
	 * Set: cdIndicadorLancamentoPagamento.
	 *
	 * @param cdIndicadorLancamentoPagamento the cd indicador lancamento pagamento
	 */
	public void setCdIndicadorLancamentoPagamento(
			Integer cdIndicadorLancamentoPagamento) {
		this.cdIndicadorLancamentoPagamento = cdIndicadorLancamentoPagamento;
	}

	/**
	 * Get: cdIndicadorListaDebito.
	 *
	 * @return cdIndicadorListaDebito
	 */
	public Integer getCdIndicadorListaDebito() {
		return cdIndicadorListaDebito;
	}

	/**
	 * Set: cdIndicadorListaDebito.
	 *
	 * @param cdIndicadorListaDebito the cd indicador lista debito
	 */
	public void setCdIndicadorListaDebito(Integer cdIndicadorListaDebito) {
		this.cdIndicadorListaDebito = cdIndicadorListaDebito;
	}

	/**
	 * Get: cdIndicadorMensagemPerso.
	 *
	 * @return cdIndicadorMensagemPerso
	 */
	public Integer getCdIndicadorMensagemPerso() {
		return cdIndicadorMensagemPerso;
	}

	/**
	 * Set: cdIndicadorMensagemPerso.
	 *
	 * @param cdIndicadorMensagemPerso the cd indicador mensagem perso
	 */
	public void setCdIndicadorMensagemPerso(Integer cdIndicadorMensagemPerso) {
		this.cdIndicadorMensagemPerso = cdIndicadorMensagemPerso;
	}

	/**
	 * Get: cdIndicadorRetornoInternet.
	 *
	 * @return cdIndicadorRetornoInternet
	 */
	public Integer getCdIndicadorRetornoInternet() {
		return cdIndicadorRetornoInternet;
	}

	/**
	 * Set: cdIndicadorRetornoInternet.
	 *
	 * @param cdIndicadorRetornoInternet the cd indicador retorno internet
	 */
	public void setCdIndicadorRetornoInternet(Integer cdIndicadorRetornoInternet) {
		this.cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
	}

	/**
	 * Get: cdIndiceEconomicoReajuste.
	 *
	 * @return cdIndiceEconomicoReajuste
	 */
	public BigDecimal getCdIndiceEconomicoReajuste() {
		return cdIndiceEconomicoReajuste;
	}

	/**
	 * Set: cdIndiceEconomicoReajuste.
	 *
	 * @param cdIndiceEconomicoReajuste the cd indice economico reajuste
	 */
	public void setCdIndiceEconomicoReajuste(BigDecimal cdIndiceEconomicoReajuste) {
		this.cdIndiceEconomicoReajuste = cdIndiceEconomicoReajuste;
	}

	/**
	 * Get: cdIndiceEconomicoReajusteTarifa.
	 *
	 * @return cdIndiceEconomicoReajusteTarifa
	 */
	public int getCdIndiceEconomicoReajusteTarifa() {
		return cdIndiceEconomicoReajusteTarifa;
	}

	/**
	 * Set: cdIndiceEconomicoReajusteTarifa.
	 *
	 * @param cdIndiceEconomicoReajusteTarifa the cd indice economico reajuste tarifa
	 */
	public void setCdIndiceEconomicoReajusteTarifa(
			int cdIndiceEconomicoReajusteTarifa) {
		this.cdIndiceEconomicoReajusteTarifa = cdIndiceEconomicoReajusteTarifa;
	}

	/**
	 * Get: cdIndiceReajuste.
	 *
	 * @return cdIndiceReajuste
	 */
	public int getCdIndiceReajuste() {
		return cdIndiceReajuste;
	}

	/**
	 * Set: cdIndiceReajuste.
	 *
	 * @param cdIndiceReajuste the cd indice reajuste
	 */
	public void setCdIndiceReajuste(int cdIndiceReajuste) {
		this.cdIndiceReajuste = cdIndiceReajuste;
	}

	/**
	 * Get: cdIndLancamentoPersonalizado.
	 *
	 * @return cdIndLancamentoPersonalizado
	 */
	public Integer getCdIndLancamentoPersonalizado() {
		return cdIndLancamentoPersonalizado;
	}

	/**
	 * Set: cdIndLancamentoPersonalizado.
	 *
	 * @param cdIndLancamentoPersonalizado the cd ind lancamento personalizado
	 */
	public void setCdIndLancamentoPersonalizado(Integer cdIndLancamentoPersonalizado) {
		this.cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
	}

	/**
	 * Get: cdInformaAgenciaContaCredito.
	 *
	 * @return cdInformaAgenciaContaCredito
	 */
	public int getCdInformaAgenciaContaCredito() {
		return cdInformaAgenciaContaCredito;
	}

	/**
	 * Set: cdInformaAgenciaContaCredito.
	 *
	 * @param cdInformaAgenciaContaCredito the cd informa agencia conta credito
	 */
	public void setCdInformaAgenciaContaCredito(int cdInformaAgenciaContaCredito) {
		this.cdInformaAgenciaContaCredito = cdInformaAgenciaContaCredito;
	}

	/**
	 * Get: cdLancamentoFuturoCredito.
	 *
	 * @return cdLancamentoFuturoCredito
	 */
	public Integer getCdLancamentoFuturoCredito() {
		return cdLancamentoFuturoCredito;
	}

	/**
	 * Set: cdLancamentoFuturoCredito.
	 *
	 * @param cdLancamentoFuturoCredito the cd lancamento futuro credito
	 */
	public void setCdLancamentoFuturoCredito(Integer cdLancamentoFuturoCredito) {
		this.cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
	}

	/**
	 * Get: cdLancamentoFuturoDebito.
	 *
	 * @return cdLancamentoFuturoDebito
	 */
	public Integer getCdLancamentoFuturoDebito() {
		return cdLancamentoFuturoDebito;
	}

	/**
	 * Set: cdLancamentoFuturoDebito.
	 *
	 * @param cdLancamentoFuturoDebito the cd lancamento futuro debito
	 */
	public void setCdLancamentoFuturoDebito(Integer cdLancamentoFuturoDebito) {
		this.cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
	}

	/**
	 * Get: cdLiberacaoLoteProcesso.
	 *
	 * @return cdLiberacaoLoteProcesso
	 */
	public Integer getCdLiberacaoLoteProcesso() {
		return cdLiberacaoLoteProcesso;
	}

	/**
	 * Set: cdLiberacaoLoteProcesso.
	 *
	 * @param cdLiberacaoLoteProcesso the cd liberacao lote processo
	 */
	public void setCdLiberacaoLoteProcesso(Integer cdLiberacaoLoteProcesso) {
		this.cdLiberacaoLoteProcesso = cdLiberacaoLoteProcesso;
	}

	/**
	 * Get: cdMantemCadastroProcuradores.
	 *
	 * @return cdMantemCadastroProcuradores
	 */
	public int getCdMantemCadastroProcuradores() {
		return cdMantemCadastroProcuradores;
	}

	/**
	 * Set: cdMantemCadastroProcuradores.
	 *
	 * @param cdMantemCadastroProcuradores the cd mantem cadastro procuradores
	 */
	public void setCdMantemCadastroProcuradores(int cdMantemCadastroProcuradores) {
		this.cdMantemCadastroProcuradores = cdMantemCadastroProcuradores;
	}

	/**
	 * Get: cdManutencaoBaseRecadastro.
	 *
	 * @return cdManutencaoBaseRecadastro
	 */
	public Integer getCdManutencaoBaseRecadastro() {
		return cdManutencaoBaseRecadastro;
	}

	/**
	 * Set: cdManutencaoBaseRecadastro.
	 *
	 * @param cdManutencaoBaseRecadastro the cd manutencao base recadastro
	 */
	public void setCdManutencaoBaseRecadastro(Integer cdManutencaoBaseRecadastro) {
		this.cdManutencaoBaseRecadastro = cdManutencaoBaseRecadastro;
	}

	/**
	 * Get: cdMeioPagamentoCredito.
	 *
	 * @return cdMeioPagamentoCredito
	 */
	public Integer getCdMeioPagamentoCredito() {
		return cdMeioPagamentoCredito;
	}

	/**
	 * Set: cdMeioPagamentoCredito.
	 *
	 * @param cdMeioPagamentoCredito the cd meio pagamento credito
	 */
	public void setCdMeioPagamentoCredito(Integer cdMeioPagamentoCredito) {
		this.cdMeioPagamentoCredito = cdMeioPagamentoCredito;
	}

	/**
	 * Get: cdMeioPagamentoDebito.
	 *
	 * @return cdMeioPagamentoDebito
	 */
	public Integer getCdMeioPagamentoDebito() {
		return cdMeioPagamentoDebito;
	}

	/**
	 * Set: cdMeioPagamentoDebito.
	 *
	 * @param cdMeioPagamentoDebito the cd meio pagamento debito
	 */
	public void setCdMeioPagamentoDebito(Integer cdMeioPagamentoDebito) {
		this.cdMeioPagamentoDebito = cdMeioPagamentoDebito;
	}

	/**
	 * Get: cdMensagemRecadastroMidia.
	 *
	 * @return cdMensagemRecadastroMidia
	 */
	public Integer getCdMensagemRecadastroMidia() {
		return cdMensagemRecadastroMidia;
	}

	/**
	 * Set: cdMensagemRecadastroMidia.
	 *
	 * @param cdMensagemRecadastroMidia the cd mensagem recadastro midia
	 */
	public void setCdMensagemRecadastroMidia(Integer cdMensagemRecadastroMidia) {
		this.cdMensagemRecadastroMidia = cdMensagemRecadastroMidia;
	}

	/**
	 * Get: cdMidiaDisponivel.
	 *
	 * @return cdMidiaDisponivel
	 */
	public Integer getCdMidiaDisponivel() {
		return cdMidiaDisponivel;
	}

	/**
	 * Set: cdMidiaDisponivel.
	 *
	 * @param cdMidiaDisponivel the cd midia disponivel
	 */
	public void setCdMidiaDisponivel(Integer cdMidiaDisponivel) {
		this.cdMidiaDisponivel = cdMidiaDisponivel;
	}

	/**
	 * Get: cdMidiaDisponivelCorrentista.
	 *
	 * @return cdMidiaDisponivelCorrentista
	 */
	public int getCdMidiaDisponivelCorrentista() {
		return cdMidiaDisponivelCorrentista;
	}

	/**
	 * Set: cdMidiaDisponivelCorrentista.
	 *
	 * @param cdMidiaDisponivelCorrentista the cd midia disponivel correntista
	 */
	public void setCdMidiaDisponivelCorrentista(int cdMidiaDisponivelCorrentista) {
		this.cdMidiaDisponivelCorrentista = cdMidiaDisponivelCorrentista;
	}

	/**
	 * Get: cdMidiaDisponivelNaoCorrentista.
	 *
	 * @return cdMidiaDisponivelNaoCorrentista
	 */
	public int getCdMidiaDisponivelNaoCorrentista() {
		return cdMidiaDisponivelNaoCorrentista;
	}

	/**
	 * Set: cdMidiaDisponivelNaoCorrentista.
	 *
	 * @param cdMidiaDisponivelNaoCorrentista the cd midia disponivel nao correntista
	 */
	public void setCdMidiaDisponivelNaoCorrentista(
			int cdMidiaDisponivelNaoCorrentista) {
		this.cdMidiaDisponivelNaoCorrentista = cdMidiaDisponivelNaoCorrentista;
	}

	/**
	 * Get: cdMidiaMensagemRecadastro.
	 *
	 * @return cdMidiaMensagemRecadastro
	 */
	public Integer getCdMidiaMensagemRecadastro() {
		return cdMidiaMensagemRecadastro;
	}

	/**
	 * Set: cdMidiaMensagemRecadastro.
	 *
	 * @param cdMidiaMensagemRecadastro the cd midia mensagem recadastro
	 */
	public void setCdMidiaMensagemRecadastro(Integer cdMidiaMensagemRecadastro) {
		this.cdMidiaMensagemRecadastro = cdMidiaMensagemRecadastro;
	}

	/**
	 * Get: cdMidiaMensagemRecastramentoOnLine.
	 *
	 * @return cdMidiaMensagemRecastramentoOnLine
	 */
	public int getCdMidiaMensagemRecastramentoOnLine() {
		return cdMidiaMensagemRecastramentoOnLine;
	}

	/**
	 * Set: cdMidiaMensagemRecastramentoOnLine.
	 *
	 * @param cdMidiaMensagemRecastramentoOnLine the cd midia mensagem recastramento on line
	 */
	public void setCdMidiaMensagemRecastramentoOnLine(
			int cdMidiaMensagemRecastramentoOnLine) {
		this.cdMidiaMensagemRecastramentoOnLine = cdMidiaMensagemRecastramentoOnLine;
	}

	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Get: cdMomentoAvisoRacadastro.
	 *
	 * @return cdMomentoAvisoRacadastro
	 */
	public Integer getCdMomentoAvisoRacadastro() {
		return cdMomentoAvisoRacadastro;
	}

	/**
	 * Set: cdMomentoAvisoRacadastro.
	 *
	 * @param cdMomentoAvisoRacadastro the cd momento aviso racadastro
	 */
	public void setCdMomentoAvisoRacadastro(Integer cdMomentoAvisoRacadastro) {
		this.cdMomentoAvisoRacadastro = cdMomentoAvisoRacadastro;
	}

	/**
	 * Get: cdMomentoCreditoEfetivacao.
	 *
	 * @return cdMomentoCreditoEfetivacao
	 */
	public Integer getCdMomentoCreditoEfetivacao() {
		return cdMomentoCreditoEfetivacao;
	}

	/**
	 * Set: cdMomentoCreditoEfetivacao.
	 *
	 * @param cdMomentoCreditoEfetivacao the cd momento credito efetivacao
	 */
	public void setCdMomentoCreditoEfetivacao(Integer cdMomentoCreditoEfetivacao) {
		this.cdMomentoCreditoEfetivacao = cdMomentoCreditoEfetivacao;
	}

	/**
	 * Get: cdMomentoDebitoPagamento.
	 *
	 * @return cdMomentoDebitoPagamento
	 */
	public Integer getCdMomentoDebitoPagamento() {
		return cdMomentoDebitoPagamento;
	}

	/**
	 * Set: cdMomentoDebitoPagamento.
	 *
	 * @param cdMomentoDebitoPagamento the cd momento debito pagamento
	 */
	public void setCdMomentoDebitoPagamento(Integer cdMomentoDebitoPagamento) {
		this.cdMomentoDebitoPagamento = cdMomentoDebitoPagamento;
	}

	/**
	 * Get: cdMomentoFormularioRecadastro.
	 *
	 * @return cdMomentoFormularioRecadastro
	 */
	public Integer getCdMomentoFormularioRecadastro() {
		return cdMomentoFormularioRecadastro;
	}

	/**
	 * Set: cdMomentoFormularioRecadastro.
	 *
	 * @param cdMomentoFormularioRecadastro the cd momento formulario recadastro
	 */
	public void setCdMomentoFormularioRecadastro(
			Integer cdMomentoFormularioRecadastro) {
		this.cdMomentoFormularioRecadastro = cdMomentoFormularioRecadastro;
	}

	/**
	 * Get: cdMomentoProcessamentoPagamento.
	 *
	 * @return cdMomentoProcessamentoPagamento
	 */
	public Integer getCdMomentoProcessamentoPagamento() {
		return cdMomentoProcessamentoPagamento;
	}

	/**
	 * Set: cdMomentoProcessamentoPagamento.
	 *
	 * @param cdMomentoProcessamentoPagamento the cd momento processamento pagamento
	 */
	public void setCdMomentoProcessamentoPagamento(
			Integer cdMomentoProcessamentoPagamento) {
		this.cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
	}

	/**
	 * Get: cdNaturezaOperacaoPagamento.
	 *
	 * @return cdNaturezaOperacaoPagamento
	 */
	public Integer getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}

	/**
	 * Set: cdNaturezaOperacaoPagamento.
	 *
	 * @param cdNaturezaOperacaoPagamento the cd natureza operacao pagamento
	 */
	public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}

	/**
	 * Get: cdOcorrenciaDebito.
	 *
	 * @return cdOcorrenciaDebito
	 */
	public int getCdOcorrenciaDebito() {
		return cdOcorrenciaDebito;
	}

	/**
	 * Set: cdOcorrenciaDebito.
	 *
	 * @param cdOcorrenciaDebito the cd ocorrencia debito
	 */
	public void setCdOcorrenciaDebito(int cdOcorrenciaDebito) {
		this.cdOcorrenciaDebito = cdOcorrenciaDebito;
	}

	/**
	 * Get: cdPagamentoNaoUtilizado.
	 *
	 * @return cdPagamentoNaoUtilizado
	 */
	public Integer getCdPagamentoNaoUtilizado() {
		return cdPagamentoNaoUtilizado;
	}

	/**
	 * Set: cdPagamentoNaoUtilizado.
	 *
	 * @param cdPagamentoNaoUtilizado the cd pagamento nao utilizado
	 */
	public void setCdPagamentoNaoUtilizado(Integer cdPagamentoNaoUtilizado) {
		this.cdPagamentoNaoUtilizado = cdPagamentoNaoUtilizado;
	}

	/**
	 * Get: cdParametro.
	 *
	 * @return cdParametro
	 */
	public Integer getCdParametro() {
		return cdParametro;
	}

	/**
	 * Set: cdParametro.
	 *
	 * @param cdParametro the cd parametro
	 */
	public void setCdParametro(Integer cdParametro) {
		this.cdParametro = cdParametro;
	}

	/**
	 * Get: cdParametroTela.
	 *
	 * @return cdParametroTela
	 */
	public int getCdParametroTela() {
		return cdParametroTela;
	}

	/**
	 * Set: cdParametroTela.
	 *
	 * @param cdParametroTela the cd parametro tela
	 */
	public void setCdParametroTela(int cdParametroTela) {
		this.cdParametroTela = cdParametroTela;
	}

	/**
	 * Get: cdPercentualMaximoInconLote.
	 *
	 * @return cdPercentualMaximoInconLote
	 */
	public Integer getCdPercentualMaximoInconLote() {
		return cdPercentualMaximoInconLote;
	}

	/**
	 * Set: cdPercentualMaximoInconLote.
	 *
	 * @param cdPercentualMaximoInconLote the cd percentual maximo incon lote
	 */
	public void setCdPercentualMaximoInconLote(Integer cdPercentualMaximoInconLote) {
		this.cdPercentualMaximoInconLote = cdPercentualMaximoInconLote;
	}

	/**
	 * Get: cdPerdcComprovante.
	 *
	 * @return cdPerdcComprovante
	 */
	public Integer getCdPerdcComprovante() {
		return cdPerdcComprovante;
	}

	/**
	 * Set: cdPerdcComprovante.
	 *
	 * @param cdPerdcComprovante the cd perdc comprovante
	 */
	public void setCdPerdcComprovante(Integer cdPerdcComprovante) {
		this.cdPerdcComprovante = cdPerdcComprovante;
	}

	/**
	 * Get: cdPerdcConsultaVeiculo.
	 *
	 * @return cdPerdcConsultaVeiculo
	 */
	public Integer getCdPerdcConsultaVeiculo() {
		return cdPerdcConsultaVeiculo;
	}

	/**
	 * Set: cdPerdcConsultaVeiculo.
	 *
	 * @param cdPerdcConsultaVeiculo the cd perdc consulta veiculo
	 */
	public void setCdPerdcConsultaVeiculo(Integer cdPerdcConsultaVeiculo) {
		this.cdPerdcConsultaVeiculo = cdPerdcConsultaVeiculo;
	}

	/**
	 * Get: cdPerdcEnvioRemessa.
	 *
	 * @return cdPerdcEnvioRemessa
	 */
	public Integer getCdPerdcEnvioRemessa() {
		return cdPerdcEnvioRemessa;
	}

	/**
	 * Set: cdPerdcEnvioRemessa.
	 *
	 * @param cdPerdcEnvioRemessa the cd perdc envio remessa
	 */
	public void setCdPerdcEnvioRemessa(Integer cdPerdcEnvioRemessa) {
		this.cdPerdcEnvioRemessa = cdPerdcEnvioRemessa;
	}

	/**
	 * Get: cdPerdcManutencaoProcd.
	 *
	 * @return cdPerdcManutencaoProcd
	 */
	public Integer getCdPerdcManutencaoProcd() {
		return cdPerdcManutencaoProcd;
	}

	/**
	 * Set: cdPerdcManutencaoProcd.
	 *
	 * @param cdPerdcManutencaoProcd the cd perdc manutencao procd
	 */
	public void setCdPerdcManutencaoProcd(Integer cdPerdcManutencaoProcd) {
		this.cdPerdcManutencaoProcd = cdPerdcManutencaoProcd;
	}

	/**
	 * Get: cdPeriocidadeEnvio.
	 *
	 * @return cdPeriocidadeEnvio
	 */
	public int getCdPeriocidadeEnvio() {
		return cdPeriocidadeEnvio;
	}

	/**
	 * Set: cdPeriocidadeEnvio.
	 *
	 * @param cdPeriocidadeEnvio the cd periocidade envio
	 */
	public void setCdPeriocidadeEnvio(int cdPeriocidadeEnvio) {
		this.cdPeriocidadeEnvio = cdPeriocidadeEnvio;
	}

	/**
	 * Get: cdPeriodicidade.
	 *
	 * @return cdPeriodicidade
	 */
	public int getCdPeriodicidade() {
		return cdPeriodicidade;
	}

	/**
	 * Set: cdPeriodicidade.
	 *
	 * @param cdPeriodicidade the cd periodicidade
	 */
	public void setCdPeriodicidade(int cdPeriodicidade) {
		this.cdPeriodicidade = cdPeriodicidade;
	}

	/**
	 * Get: cdPeriodicidadeAviso.
	 *
	 * @return cdPeriodicidadeAviso
	 */
	public Integer getCdPeriodicidadeAviso() {
		return cdPeriodicidadeAviso;
	}

	/**
	 * Set: cdPeriodicidadeAviso.
	 *
	 * @param cdPeriodicidadeAviso the cd periodicidade aviso
	 */
	public void setCdPeriodicidadeAviso(Integer cdPeriodicidadeAviso) {
		this.cdPeriodicidadeAviso = cdPeriodicidadeAviso;
	}

	/**
	 * Get: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @return cdPeriodicidadeCobrancaTarifa
	 */
	public int getCdPeriodicidadeCobrancaTarifa() {
		return cdPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Set: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @param cdPeriodicidadeCobrancaTarifa the cd periodicidade cobranca tarifa
	 */
	public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa) {
		this.cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Get: cdPeriodicidadeCobrancaTarifaEmissaoComprovantes.
	 *
	 * @return cdPeriodicidadeCobrancaTarifaEmissaoComprovantes
	 */
	public int getCdPeriodicidadeCobrancaTarifaEmissaoComprovantes() {
		return cdPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	}

	/**
	 * Set: cdPeriodicidadeCobrancaTarifaEmissaoComprovantes.
	 *
	 * @param cdPeriodicidadeCobrancaTarifaEmissaoComprovantes the cd periodicidade cobranca tarifa emissao comprovantes
	 */
	public void setCdPeriodicidadeCobrancaTarifaEmissaoComprovantes(
			int cdPeriodicidadeCobrancaTarifaEmissaoComprovantes) {
		this.cdPeriodicidadeCobrancaTarifaEmissaoComprovantes = cdPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	}

	/**
	 * Get: cdPeriodicidadeEnvioRemessa.
	 *
	 * @return cdPeriodicidadeEnvioRemessa
	 */
	public int getCdPeriodicidadeEnvioRemessa() {
		return cdPeriodicidadeEnvioRemessa;
	}

	/**
	 * Set: cdPeriodicidadeEnvioRemessa.
	 *
	 * @param cdPeriodicidadeEnvioRemessa the cd periodicidade envio remessa
	 */
	public void setCdPeriodicidadeEnvioRemessa(int cdPeriodicidadeEnvioRemessa) {
		this.cdPeriodicidadeEnvioRemessa = cdPeriodicidadeEnvioRemessa;
	}

	/**
	 * Get: cdPeriodicidadeManutencaoCadastroProcurador.
	 *
	 * @return cdPeriodicidadeManutencaoCadastroProcurador
	 */
	public int getCdPeriodicidadeManutencaoCadastroProcurador() {
		return cdPeriodicidadeManutencaoCadastroProcurador;
	}

	/**
	 * Set: cdPeriodicidadeManutencaoCadastroProcurador.
	 *
	 * @param cdPeriodicidadeManutencaoCadastroProcurador the cd periodicidade manutencao cadastro procurador
	 */
	public void setCdPeriodicidadeManutencaoCadastroProcurador(
			int cdPeriodicidadeManutencaoCadastroProcurador) {
		this.cdPeriodicidadeManutencaoCadastroProcurador = cdPeriodicidadeManutencaoCadastroProcurador;
	}

	/**
	 * Get: cdPeriodicidadeRastreamentoDebitoVeiculo.
	 *
	 * @return cdPeriodicidadeRastreamentoDebitoVeiculo
	 */
	public int getCdPeriodicidadeRastreamentoDebitoVeiculo() {
		return cdPeriodicidadeRastreamentoDebitoVeiculo;
	}

	/**
	 * Set: cdPeriodicidadeRastreamentoDebitoVeiculo.
	 *
	 * @param cdPeriodicidadeRastreamentoDebitoVeiculo the cd periodicidade rastreamento debito veiculo
	 */
	public void setCdPeriodicidadeRastreamentoDebitoVeiculo(
			int cdPeriodicidadeRastreamentoDebitoVeiculo) {
		this.cdPeriodicidadeRastreamentoDebitoVeiculo = cdPeriodicidadeRastreamentoDebitoVeiculo;
	}

	/**
	 * Get: cdPeriodicidadeReajusteTarifa.
	 *
	 * @return cdPeriodicidadeReajusteTarifa
	 */
	public int getCdPeriodicidadeReajusteTarifa() {
		return cdPeriodicidadeReajusteTarifa;
	}

	/**
	 * Set: cdPeriodicidadeReajusteTarifa.
	 *
	 * @param cdPeriodicidadeReajusteTarifa the cd periodicidade reajuste tarifa
	 */
	public void setCdPeriodicidadeReajusteTarifa(int cdPeriodicidadeReajusteTarifa) {
		this.cdPeriodicidadeReajusteTarifa = cdPeriodicidadeReajusteTarifa;
	}

	/**
	 * Get: cdPermissaoDebitoOnline.
	 *
	 * @return cdPermissaoDebitoOnline
	 */
	public Integer getCdPermissaoDebitoOnline() {
		return cdPermissaoDebitoOnline;
	}

	/**
	 * Set: cdPermissaoDebitoOnline.
	 *
	 * @param cdPermissaoDebitoOnline the cd permissao debito online
	 */
	public void setCdPermissaoDebitoOnline(Integer cdPermissaoDebitoOnline) {
		this.cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
	}

	/**
	 * Get: cdPermiteAcertosDados.
	 *
	 * @return cdPermiteAcertosDados
	 */
	public int getCdPermiteAcertosDados() {
		return cdPermiteAcertosDados;
	}

	/**
	 * Set: cdPermiteAcertosDados.
	 *
	 * @param cdPermiteAcertosDados the cd permite acertos dados
	 */
	public void setCdPermiteAcertosDados(int cdPermiteAcertosDados) {
		this.cdPermiteAcertosDados = cdPermiteAcertosDados;
	}

	/**
	 * Get: cdPermiteAnteciparRecadastramento.
	 *
	 * @return cdPermiteAnteciparRecadastramento
	 */
	public int getCdPermiteAnteciparRecadastramento() {
		return cdPermiteAnteciparRecadastramento;
	}

	/**
	 * Set: cdPermiteAnteciparRecadastramento.
	 *
	 * @param cdPermiteAnteciparRecadastramento the cd permite antecipar recadastramento
	 */
	public void setCdPermiteAnteciparRecadastramento(
			int cdPermiteAnteciparRecadastramento) {
		this.cdPermiteAnteciparRecadastramento = cdPermiteAnteciparRecadastramento;
	}

	/**
	 * Get: cdPermiteConsultaFavorecido.
	 *
	 * @return cdPermiteConsultaFavorecido
	 */
	public int getCdPermiteConsultaFavorecido() {
		return cdPermiteConsultaFavorecido;
	}

	/**
	 * Set: cdPermiteConsultaFavorecido.
	 *
	 * @param cdPermiteConsultaFavorecido the cd permite consulta favorecido
	 */
	public void setCdPermiteConsultaFavorecido(int cdPermiteConsultaFavorecido) {
		this.cdPermiteConsultaFavorecido = cdPermiteConsultaFavorecido;
	}

	/**
	 * Get: cdPermiteContingenciaPag.
	 *
	 * @return cdPermiteContingenciaPag
	 */
	public int getCdPermiteContingenciaPag() {
		return cdPermiteContingenciaPag;
	}

	/**
	 * Set: cdPermiteContingenciaPag.
	 *
	 * @param cdPermiteContingenciaPag the cd permite contingencia pag
	 */
	public void setCdPermiteContingenciaPag(int cdPermiteContingenciaPag) {
		this.cdPermiteContingenciaPag = cdPermiteContingenciaPag;
	}

	/**
	 * Get: cdPermiteCorrespondenciaAberta.
	 *
	 * @return cdPermiteCorrespondenciaAberta
	 */
	public int getCdPermiteCorrespondenciaAberta() {
		return cdPermiteCorrespondenciaAberta;
	}

	/**
	 * Set: cdPermiteCorrespondenciaAberta.
	 *
	 * @param cdPermiteCorrespondenciaAberta the cd permite correspondencia aberta
	 */
	public void setCdPermiteCorrespondenciaAberta(int cdPermiteCorrespondenciaAberta) {
		this.cdPermiteCorrespondenciaAberta = cdPermiteCorrespondenciaAberta;
	}

	/**
	 * Get: cdPermiteEnvioRemessaManutencao.
	 *
	 * @return cdPermiteEnvioRemessaManutencao
	 */
	public int getCdPermiteEnvioRemessaManutencao() {
		return cdPermiteEnvioRemessaManutencao;
	}

	/**
	 * Set: cdPermiteEnvioRemessaManutencao.
	 *
	 * @param cdPermiteEnvioRemessaManutencao the cd permite envio remessa manutencao
	 */
	public void setCdPermiteEnvioRemessaManutencao(
			int cdPermiteEnvioRemessaManutencao) {
		this.cdPermiteEnvioRemessaManutencao = cdPermiteEnvioRemessaManutencao;
	}

	/**
	 * Get: cdPermiteEstornoPagamento.
	 *
	 * @return cdPermiteEstornoPagamento
	 */
	public int getCdPermiteEstornoPagamento() {
		return cdPermiteEstornoPagamento;
	}

	/**
	 * Set: cdPermiteEstornoPagamento.
	 *
	 * @param cdPermiteEstornoPagamento the cd permite estorno pagamento
	 */
	public void setCdPermiteEstornoPagamento(int cdPermiteEstornoPagamento) {
		this.cdPermiteEstornoPagamento = cdPermiteEstornoPagamento;
	}

	/**
	 * Get: cdPermitirAgrupamento.
	 *
	 * @return cdPermitirAgrupamento
	 */
	public int getCdPermitirAgrupamento() {
		return cdPermitirAgrupamento;
	}

	/**
	 * Set: cdPermitirAgrupamento.
	 *
	 * @param cdPermitirAgrupamento the cd permitir agrupamento
	 */
	public void setCdPermitirAgrupamento(int cdPermitirAgrupamento) {
		this.cdPermitirAgrupamento = cdPermitirAgrupamento;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdPossuiExpiracaoCredito.
	 *
	 * @return cdPossuiExpiracaoCredito
	 */
	public int getCdPossuiExpiracaoCredito() {
		return cdPossuiExpiracaoCredito;
	}

	/**
	 * Set: cdPossuiExpiracaoCredito.
	 *
	 * @param cdPossuiExpiracaoCredito the cd possui expiracao credito
	 */
	public void setCdPossuiExpiracaoCredito(int cdPossuiExpiracaoCredito) {
		this.cdPossuiExpiracaoCredito = cdPossuiExpiracaoCredito;
	}

	/**
	 * Get: cdPrincipalEnquaRecadastro.
	 *
	 * @return cdPrincipalEnquaRecadastro
	 */
	public Integer getCdPrincipalEnquaRecadastro() {
		return cdPrincipalEnquaRecadastro;
	}

	/**
	 * Set: cdPrincipalEnquaRecadastro.
	 *
	 * @param cdPrincipalEnquaRecadastro the cd principal enqua recadastro
	 */
	public void setCdPrincipalEnquaRecadastro(Integer cdPrincipalEnquaRecadastro) {
		this.cdPrincipalEnquaRecadastro = cdPrincipalEnquaRecadastro;
	}

	/**
	 * Get: cdPrioridadeCredito.
	 *
	 * @return cdPrioridadeCredito
	 */
	public int getCdPrioridadeCredito() {
		return cdPrioridadeCredito;
	}

	/**
	 * Set: cdPrioridadeCredito.
	 *
	 * @param cdPrioridadeCredito the cd prioridade credito
	 */
	public void setCdPrioridadeCredito(int cdPrioridadeCredito) {
		this.cdPrioridadeCredito = cdPrioridadeCredito;
	}

	/**
	 * Get: cdPrioridadeDebito.
	 *
	 * @return cdPrioridadeDebito
	 */
	public int getCdPrioridadeDebito() {
		return cdPrioridadeDebito;
	}

	/**
	 * Set: cdPrioridadeDebito.
	 *
	 * @param cdPrioridadeDebito the cd prioridade debito
	 */
	public void setCdPrioridadeDebito(int cdPrioridadeDebito) {
		this.cdPrioridadeDebito = cdPrioridadeDebito;
	}

	/**
	 * Get: cdPrioridadeEfetivacaoPagamento.
	 *
	 * @return cdPrioridadeEfetivacaoPagamento
	 */
	public Integer getCdPrioridadeEfetivacaoPagamento() {
		return cdPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Set: cdPrioridadeEfetivacaoPagamento.
	 *
	 * @param cdPrioridadeEfetivacaoPagamento the cd prioridade efetivacao pagamento
	 */
	public void setCdPrioridadeEfetivacaoPagamento(
			Integer cdPrioridadeEfetivacaoPagamento) {
		this.cdPrioridadeEfetivacaoPagamento = cdPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Get: cdProdutoOperacaroRelacionado.
	 *
	 * @return cdProdutoOperacaroRelacionado
	 */
	public Integer getCdProdutoOperacaroRelacionado() {
		return cdProdutoOperacaroRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaroRelacionado.
	 *
	 * @param cdProdutoOperacaroRelacionado the cd produto operacaro relacionado
	 */
	public void setCdProdutoOperacaroRelacionado(
			Integer cdProdutoOperacaroRelacionado) {
		this.cdProdutoOperacaroRelacionado = cdProdutoOperacaroRelacionado;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdRastreabilidadeNotaFiscal.
	 *
	 * @return cdRastreabilidadeNotaFiscal
	 */
	public Integer getCdRastreabilidadeNotaFiscal() {
		return cdRastreabilidadeNotaFiscal;
	}

	/**
	 * Set: cdRastreabilidadeNotaFiscal.
	 *
	 * @param cdRastreabilidadeNotaFiscal the cd rastreabilidade nota fiscal
	 */
	public void setCdRastreabilidadeNotaFiscal(Integer cdRastreabilidadeNotaFiscal) {
		this.cdRastreabilidadeNotaFiscal = cdRastreabilidadeNotaFiscal;
	}

	/**
	 * Get: cdRastreabilidadeTituloTerceiro.
	 *
	 * @return cdRastreabilidadeTituloTerceiro
	 */
	public Integer getCdRastreabilidadeTituloTerceiro() {
		return cdRastreabilidadeTituloTerceiro;
	}

	/**
	 * Set: cdRastreabilidadeTituloTerceiro.
	 *
	 * @param cdRastreabilidadeTituloTerceiro the cd rastreabilidade titulo terceiro
	 */
	public void setCdRastreabilidadeTituloTerceiro(
			Integer cdRastreabilidadeTituloTerceiro) {
		this.cdRastreabilidadeTituloTerceiro = cdRastreabilidadeTituloTerceiro;
	}

	/**
	 * Get: cdRejeicaoAgendaLote.
	 *
	 * @return cdRejeicaoAgendaLote
	 */
	public Integer getCdRejeicaoAgendaLote() {
		return cdRejeicaoAgendaLote;
	}

	/**
	 * Set: cdRejeicaoAgendaLote.
	 *
	 * @param cdRejeicaoAgendaLote the cd rejeicao agenda lote
	 */
	public void setCdRejeicaoAgendaLote(Integer cdRejeicaoAgendaLote) {
		this.cdRejeicaoAgendaLote = cdRejeicaoAgendaLote;
	}

	/**
	 * Get: cdRejeicaoEfetivacaoLote.
	 *
	 * @return cdRejeicaoEfetivacaoLote
	 */
	public Integer getCdRejeicaoEfetivacaoLote() {
		return cdRejeicaoEfetivacaoLote;
	}

	/**
	 * Set: cdRejeicaoEfetivacaoLote.
	 *
	 * @param cdRejeicaoEfetivacaoLote the cd rejeicao efetivacao lote
	 */
	public void setCdRejeicaoEfetivacaoLote(Integer cdRejeicaoEfetivacaoLote) {
		this.cdRejeicaoEfetivacaoLote = cdRejeicaoEfetivacaoLote;
	}

	/**
	 * Get: cdRejeicaoLote.
	 *
	 * @return cdRejeicaoLote
	 */
	public Integer getCdRejeicaoLote() {
		return cdRejeicaoLote;
	}

	/**
	 * Set: cdRejeicaoLote.
	 *
	 * @param cdRejeicaoLote the cd rejeicao lote
	 */
	public void setCdRejeicaoLote(Integer cdRejeicaoLote) {
		this.cdRejeicaoLote = cdRejeicaoLote;
	}

	/**
	 * Get: cdTipoAlimentacaoPagamentos.
	 *
	 * @return cdTipoAlimentacaoPagamentos
	 */
	public int getCdTipoAlimentacaoPagamentos() {
		return cdTipoAlimentacaoPagamentos;
	}

	/**
	 * Set: cdTipoAlimentacaoPagamentos.
	 *
	 * @param cdTipoAlimentacaoPagamentos the cd tipo alimentacao pagamentos
	 */
	public void setCdTipoAlimentacaoPagamentos(int cdTipoAlimentacaoPagamentos) {
		this.cdTipoAlimentacaoPagamentos = cdTipoAlimentacaoPagamentos;
	}

	/**
	 * Get: cdTipoAutorizacaoPagamento.
	 *
	 * @return cdTipoAutorizacaoPagamento
	 */
	public int getCdTipoAutorizacaoPagamento() {
		return cdTipoAutorizacaoPagamento;
	}

	/**
	 * Set: cdTipoAutorizacaoPagamento.
	 *
	 * @param cdTipoAutorizacaoPagamento the cd tipo autorizacao pagamento
	 */
	public void setCdTipoAutorizacaoPagamento(int cdTipoAutorizacaoPagamento) {
		this.cdTipoAutorizacaoPagamento = cdTipoAutorizacaoPagamento;
	}

	/**
	 * Get: cdTipoCargaBaseDadosUtilizada.
	 *
	 * @return cdTipoCargaBaseDadosUtilizada
	 */
	public int getCdTipoCargaBaseDadosUtilizada() {
		return cdTipoCargaBaseDadosUtilizada;
	}

	/**
	 * Set: cdTipoCargaBaseDadosUtilizada.
	 *
	 * @param cdTipoCargaBaseDadosUtilizada the cd tipo carga base dados utilizada
	 */
	public void setCdTipoCargaBaseDadosUtilizada(int cdTipoCargaBaseDadosUtilizada) {
		this.cdTipoCargaBaseDadosUtilizada = cdTipoCargaBaseDadosUtilizada;
	}

	/**
	 * Get: cdTipoCargaRecadastro.
	 *
	 * @return cdTipoCargaRecadastro
	 */
	public Integer getCdTipoCargaRecadastro() {
		return cdTipoCargaRecadastro;
	}

	/**
	 * Set: cdTipoCargaRecadastro.
	 *
	 * @param cdTipoCargaRecadastro the cd tipo carga recadastro
	 */
	public void setCdTipoCargaRecadastro(Integer cdTipoCargaRecadastro) {
		this.cdTipoCargaRecadastro = cdTipoCargaRecadastro;
	}

	/**
	 * Get: cdTipoCartao.
	 *
	 * @return cdTipoCartao
	 */
	public int getCdTipoCartao() {
		return cdTipoCartao;
	}

	/**
	 * Set: cdTipoCartao.
	 *
	 * @param cdTipoCartao the cd tipo cartao
	 */
	public void setCdTipoCartao(int cdTipoCartao) {
		this.cdTipoCartao = cdTipoCartao;
	}

	/**
	 * Get: cdTipoCataoSalario.
	 *
	 * @return cdTipoCataoSalario
	 */
	public Integer getCdTipoCataoSalario() {
		return cdTipoCataoSalario;
	}

	/**
	 * Set: cdTipoCataoSalario.
	 *
	 * @param cdTipoCataoSalario the cd tipo catao salario
	 */
	public void setCdTipoCataoSalario(Integer cdTipoCataoSalario) {
		this.cdTipoCataoSalario = cdTipoCataoSalario;
	}

	/**
	 * Get: cdTipoComprovacao.
	 *
	 * @return cdTipoComprovacao
	 */
	public int getCdTipoComprovacao() {
		return cdTipoComprovacao;
	}

	/**
	 * Set: cdTipoComprovacao.
	 *
	 * @param cdTipoComprovacao the cd tipo comprovacao
	 */
	public void setCdTipoComprovacao(int cdTipoComprovacao) {
		this.cdTipoComprovacao = cdTipoComprovacao;
	}

	/**
	 * Get: cdTipoConsistenciaCpfCnpjProp.
	 *
	 * @return cdTipoConsistenciaCpfCnpjProp
	 */
	public int getCdTipoConsistenciaCpfCnpjProp() {
		return cdTipoConsistenciaCpfCnpjProp;
	}

	/**
	 * Set: cdTipoConsistenciaCpfCnpjProp.
	 *
	 * @param cdTipoConsistenciaCpfCnpjProp the cd tipo consistencia cpf cnpj prop
	 */
	public void setCdTipoConsistenciaCpfCnpjProp(int cdTipoConsistenciaCpfCnpjProp) {
		this.cdTipoConsistenciaCpfCnpjProp = cdTipoConsistenciaCpfCnpjProp;
	}

	/**
	 * Get: cdTipoConsistenciaIdentificador.
	 *
	 * @return cdTipoConsistenciaIdentificador
	 */
	public int getCdTipoConsistenciaIdentificador() {
		return cdTipoConsistenciaIdentificador;
	}

	/**
	 * Set: cdTipoConsistenciaIdentificador.
	 *
	 * @param cdTipoConsistenciaIdentificador the cd tipo consistencia identificador
	 */
	public void setCdTipoConsistenciaIdentificador(
			int cdTipoConsistenciaIdentificador) {
		this.cdTipoConsistenciaIdentificador = cdTipoConsistenciaIdentificador;
	}

	/**
	 * Get: cdTipoConsistenciaLista.
	 *
	 * @return cdTipoConsistenciaLista
	 */
	public Integer getCdTipoConsistenciaLista() {
		return cdTipoConsistenciaLista;
	}

	/**
	 * Set: cdTipoConsistenciaLista.
	 *
	 * @param cdTipoConsistenciaLista the cd tipo consistencia lista
	 */
	public void setCdTipoConsistenciaLista(Integer cdTipoConsistenciaLista) {
		this.cdTipoConsistenciaLista = cdTipoConsistenciaLista;
	}

	/**
	 * Get: cdTipoConsultaComprovante.
	 *
	 * @return cdTipoConsultaComprovante
	 */
	public Integer getCdTipoConsultaComprovante() {
		return cdTipoConsultaComprovante;
	}

	/**
	 * Set: cdTipoConsultaComprovante.
	 *
	 * @param cdTipoConsultaComprovante the cd tipo consulta comprovante
	 */
	public void setCdTipoConsultaComprovante(Integer cdTipoConsultaComprovante) {
		this.cdTipoConsultaComprovante = cdTipoConsultaComprovante;
	}

	/**
	 * Get: cdTipoConsultaSaldo.
	 *
	 * @return cdTipoConsultaSaldo
	 */
	public int getCdTipoConsultaSaldo() {
		return cdTipoConsultaSaldo;
	}

	/**
	 * Set: cdTipoConsultaSaldo.
	 *
	 * @param cdTipoConsultaSaldo the cd tipo consulta saldo
	 */
	public void setCdTipoConsultaSaldo(int cdTipoConsultaSaldo) {
		this.cdTipoConsultaSaldo = cdTipoConsultaSaldo;
	}

	/**
	 * Get: cdTipoContaFavorecido.
	 *
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}

	/**
	 * Set: cdTipoContaFavorecido.
	 *
	 * @param cdTipoContaFavorecido the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdTipoControleFloating.
	 *
	 * @return cdTipoControleFloating
	 */
	public int getCdTipoControleFloating() {
		return cdTipoControleFloating;
	}

	/**
	 * Set: cdTipoControleFloating.
	 *
	 * @param cdTipoControleFloating the cd tipo controle floating
	 */
	public void setCdTipoControleFloating(int cdTipoControleFloating) {
		this.cdTipoControleFloating = cdTipoControleFloating;
	}

	/**
	 * Get: cdTipoDataFloat.
	 *
	 * @return cdTipoDataFloat
	 */
	public Integer getCdTipoDataFloat() {
		return cdTipoDataFloat;
	}

	/**
	 * Set: cdTipoDataFloat.
	 *
	 * @param cdTipoDataFloat the cd tipo data float
	 */
	public void setCdTipoDataFloat(Integer cdTipoDataFloat) {
		this.cdTipoDataFloat = cdTipoDataFloat;
	}

	/**
	 * Get: cdTipoDivergenciaVeiculo.
	 *
	 * @return cdTipoDivergenciaVeiculo
	 */
	public Integer getCdTipoDivergenciaVeiculo() {
		return cdTipoDivergenciaVeiculo;
	}

	/**
	 * Set: cdTipoDivergenciaVeiculo.
	 *
	 * @param cdTipoDivergenciaVeiculo the cd tipo divergencia veiculo
	 */
	public void setCdTipoDivergenciaVeiculo(Integer cdTipoDivergenciaVeiculo) {
		this.cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
	}

	/**
	 * Get: cdTipoEfetivacao.
	 *
	 * @return cdTipoEfetivacao
	 */
	public int getCdTipoEfetivacao() {
		return cdTipoEfetivacao;
	}

	/**
	 * Set: cdTipoEfetivacao.
	 *
	 * @param cdTipoEfetivacao the cd tipo efetivacao
	 */
	public void setCdTipoEfetivacao(int cdTipoEfetivacao) {
		this.cdTipoEfetivacao = cdTipoEfetivacao;
	}

	/**
	 * Get: cdTipoEmissao.
	 *
	 * @return cdTipoEmissao
	 */
	public int getCdTipoEmissao() {
		return cdTipoEmissao;
	}

	/**
	 * Set: cdTipoEmissao.
	 *
	 * @param cdTipoEmissao the cd tipo emissao
	 */
	public void setCdTipoEmissao(int cdTipoEmissao) {
		this.cdTipoEmissao = cdTipoEmissao;
	}

	/**
	 * Get: cdTipoEnvioCorrespondencia.
	 *
	 * @return cdTipoEnvioCorrespondencia
	 */
	public int getCdTipoEnvioCorrespondencia() {
		return cdTipoEnvioCorrespondencia;
	}

	/**
	 * Set: cdTipoEnvioCorrespondencia.
	 *
	 * @param cdTipoEnvioCorrespondencia the cd tipo envio correspondencia
	 */
	public void setCdTipoEnvioCorrespondencia(int cdTipoEnvioCorrespondencia) {
		this.cdTipoEnvioCorrespondencia = cdTipoEnvioCorrespondencia;
	}

	/**
	 * Get: cdTipoFormacaoLista.
	 *
	 * @return cdTipoFormacaoLista
	 */
	public Integer getCdTipoFormacaoLista() {
		return cdTipoFormacaoLista;
	}

	/**
	 * Set: cdTipoFormacaoLista.
	 *
	 * @param cdTipoFormacaoLista the cd tipo formacao lista
	 */
	public void setCdTipoFormacaoLista(Integer cdTipoFormacaoLista) {
		this.cdTipoFormacaoLista = cdTipoFormacaoLista;
	}

	/**
	 * Get: cdTipoIdBeneficio.
	 *
	 * @return cdTipoIdBeneficio
	 */
	public Integer getCdTipoIdBeneficio() {
		return cdTipoIdBeneficio;
	}

	/**
	 * Set: cdTipoIdBeneficio.
	 *
	 * @param cdTipoIdBeneficio the cd tipo id beneficio
	 */
	public void setCdTipoIdBeneficio(Integer cdTipoIdBeneficio) {
		this.cdTipoIdBeneficio = cdTipoIdBeneficio;
	}

	/**
	 * Get: cdTipoIdentificacaoBeneficio.
	 *
	 * @return cdTipoIdentificacaoBeneficio
	 */
	public int getCdTipoIdentificacaoBeneficio() {
		return cdTipoIdentificacaoBeneficio;
	}

	/**
	 * Set: cdTipoIdentificacaoBeneficio.
	 *
	 * @param cdTipoIdentificacaoBeneficio the cd tipo identificacao beneficio
	 */
	public void setCdTipoIdentificacaoBeneficio(int cdTipoIdentificacaoBeneficio) {
		this.cdTipoIdentificacaoBeneficio = cdTipoIdentificacaoBeneficio;
	}

	/**
	 * Get: cdTipoIdentificacaoFuncionario.
	 *
	 * @return cdTipoIdentificacaoFuncionario
	 */
	public int getCdTipoIdentificacaoFuncionario() {
		return cdTipoIdentificacaoFuncionario;
	}

	/**
	 * Set: cdTipoIdentificacaoFuncionario.
	 *
	 * @param cdTipoIdentificacaoFuncionario the cd tipo identificacao funcionario
	 */
	public void setCdTipoIdentificacaoFuncionario(int cdTipoIdentificacaoFuncionario) {
		this.cdTipoIdentificacaoFuncionario = cdTipoIdentificacaoFuncionario;
	}

	/**
	 * Get: cdTipoIsncricaoFavorecido.
	 *
	 * @return cdTipoIsncricaoFavorecido
	 */
	public Integer getCdTipoIsncricaoFavorecido() {
		return cdTipoIsncricaoFavorecido;
	}

	/**
	 * Set: cdTipoIsncricaoFavorecido.
	 *
	 * @param cdTipoIsncricaoFavorecido the cd tipo isncricao favorecido
	 */
	public void setCdTipoIsncricaoFavorecido(Integer cdTipoIsncricaoFavorecido) {
		this.cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
	}

	/**
	 * Get: cdTipoPrestacaoContaCreditoPago.
	 *
	 * @return cdTipoPrestacaoContaCreditoPago
	 */
	public int getCdTipoPrestacaoContaCreditoPago() {
		return cdTipoPrestacaoContaCreditoPago;
	}

	/**
	 * Set: cdTipoPrestacaoContaCreditoPago.
	 *
	 * @param cdTipoPrestacaoContaCreditoPago the cd tipo prestacao conta credito pago
	 */
	public void setCdTipoPrestacaoContaCreditoPago(
			int cdTipoPrestacaoContaCreditoPago) {
		this.cdTipoPrestacaoContaCreditoPago = cdTipoPrestacaoContaCreditoPago;
	}

	/**
	 * Get: cdTipoProcessamento.
	 *
	 * @return cdTipoProcessamento
	 */
	public int getCdTipoProcessamento() {
		return cdTipoProcessamento;
	}

	/**
	 * Set: cdTipoProcessamento.
	 *
	 * @param cdTipoProcessamento the cd tipo processamento
	 */
	public void setCdTipoProcessamento(int cdTipoProcessamento) {
		this.cdTipoProcessamento = cdTipoProcessamento;
	}

	/**
	 * Get: cdTipoRastreamentoTitulos.
	 *
	 * @return cdTipoRastreamentoTitulos
	 */
	public int getCdTipoRastreamentoTitulos() {
		return cdTipoRastreamentoTitulos;
	}

	/**
	 * Set: cdTipoRastreamentoTitulos.
	 *
	 * @param cdTipoRastreamentoTitulos the cd tipo rastreamento titulos
	 */
	public void setCdTipoRastreamentoTitulos(int cdTipoRastreamentoTitulos) {
		this.cdTipoRastreamentoTitulos = cdTipoRastreamentoTitulos;
	}

	/**
	 * Get: cdTipoReajuste.
	 *
	 * @return cdTipoReajuste
	 */
	public int getCdTipoReajuste() {
		return cdTipoReajuste;
	}

	/**
	 * Set: cdTipoReajuste.
	 *
	 * @param cdTipoReajuste the cd tipo reajuste
	 */
	public void setCdTipoReajuste(int cdTipoReajuste) {
		this.cdTipoReajuste = cdTipoReajuste;
	}

	/**
	 * Get: cdTipoReajusteTarifa.
	 *
	 * @return cdTipoReajusteTarifa
	 */
	public int getCdTipoReajusteTarifa() {
		return cdTipoReajusteTarifa;
	}

	/**
	 * Set: cdTipoReajusteTarifa.
	 *
	 * @param cdTipoReajusteTarifa the cd tipo reajuste tarifa
	 */
	public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa) {
		this.cdTipoReajusteTarifa = cdTipoReajusteTarifa;
	}

	/**
	 * Get: cdTipoRejeicao.
	 *
	 * @return cdTipoRejeicao
	 */
	public int getCdTipoRejeicao() {
		return cdTipoRejeicao;
	}

	/**
	 * Set: cdTipoRejeicao.
	 *
	 * @param cdTipoRejeicao the cd tipo rejeicao
	 */
	public void setCdTipoRejeicao(int cdTipoRejeicao) {
		this.cdTipoRejeicao = cdTipoRejeicao;
	}

	/**
	 * Get: cdTipoRejeicaoAgendamento.
	 *
	 * @return cdTipoRejeicaoAgendamento
	 */
	public int getCdTipoRejeicaoAgendamento() {
		return cdTipoRejeicaoAgendamento;
	}

	/**
	 * Set: cdTipoRejeicaoAgendamento.
	 *
	 * @param cdTipoRejeicaoAgendamento the cd tipo rejeicao agendamento
	 */
	public void setCdTipoRejeicaoAgendamento(int cdTipoRejeicaoAgendamento) {
		this.cdTipoRejeicaoAgendamento = cdTipoRejeicaoAgendamento;
	}

	/**
	 * Get: cdTipoRejeicaoEfetivacao.
	 *
	 * @return cdTipoRejeicaoEfetivacao
	 */
	public int getCdTipoRejeicaoEfetivacao() {
		return cdTipoRejeicaoEfetivacao;
	}

	/**
	 * Set: cdTipoRejeicaoEfetivacao.
	 *
	 * @param cdTipoRejeicaoEfetivacao the cd tipo rejeicao efetivacao
	 */
	public void setCdTipoRejeicaoEfetivacao(int cdTipoRejeicaoEfetivacao) {
		this.cdTipoRejeicaoEfetivacao = cdTipoRejeicaoEfetivacao;
	}

	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}

	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}

	/**
	 * Get: cdTipoTratamentoContasTransferidas.
	 *
	 * @return cdTipoTratamentoContasTransferidas
	 */
	public int getCdTipoTratamentoContasTransferidas() {
		return cdTipoTratamentoContasTransferidas;
	}

	/**
	 * Set: cdTipoTratamentoContasTransferidas.
	 *
	 * @param cdTipoTratamentoContasTransferidas the cd tipo tratamento contas transferidas
	 */
	public void setCdTipoTratamentoContasTransferidas(
			int cdTipoTratamentoContasTransferidas) {
		this.cdTipoTratamentoContasTransferidas = cdTipoTratamentoContasTransferidas;
	}

	/**
	 * Get: cdTratamentoFeriado.
	 *
	 * @return cdTratamentoFeriado
	 */
	public int getCdTratamentoFeriado() {
		return cdTratamentoFeriado;
	}

	/**
	 * Set: cdTratamentoFeriado.
	 *
	 * @param cdTratamentoFeriado the cd tratamento feriado
	 */
	public void setCdTratamentoFeriado(int cdTratamentoFeriado) {
		this.cdTratamentoFeriado = cdTratamentoFeriado;
	}

	/**
	 * Get: cdTratamentoFeriadoFimVigencia.
	 *
	 * @return cdTratamentoFeriadoFimVigencia
	 */
	public int getCdTratamentoFeriadoFimVigencia() {
		return cdTratamentoFeriadoFimVigencia;
	}

	/**
	 * Set: cdTratamentoFeriadoFimVigencia.
	 *
	 * @param cdTratamentoFeriadoFimVigencia the cd tratamento feriado fim vigencia
	 */
	public void setCdTratamentoFeriadoFimVigencia(int cdTratamentoFeriadoFimVigencia) {
		this.cdTratamentoFeriadoFimVigencia = cdTratamentoFeriadoFimVigencia;
	}

	/**
	 * Get: cdTratamentoFeriadosDtaPag.
	 *
	 * @return cdTratamentoFeriadosDtaPag
	 */
	public int getCdTratamentoFeriadosDtaPag() {
		return cdTratamentoFeriadosDtaPag;
	}

	/**
	 * Set: cdTratamentoFeriadosDtaPag.
	 *
	 * @param cdTratamentoFeriadosDtaPag the cd tratamento feriados dta pag
	 */
	public void setCdTratamentoFeriadosDtaPag(int cdTratamentoFeriadosDtaPag) {
		this.cdTratamentoFeriadosDtaPag = cdTratamentoFeriadosDtaPag;
	}

	/**
	 * Get: cdTratamentoValorDivergente.
	 *
	 * @return cdTratamentoValorDivergente
	 */
	public int getCdTratamentoValorDivergente() {
		return cdTratamentoValorDivergente;
	}

	/**
	 * Set: cdTratamentoValorDivergente.
	 *
	 * @param cdTratamentoValorDivergente the cd tratamento valor divergente
	 */
	public void setCdTratamentoValorDivergente(int cdTratamentoValorDivergente) {
		this.cdTratamentoValorDivergente = cdTratamentoValorDivergente;
	}

	/**
	 * Get: cdusuario.
	 *
	 * @return cdusuario
	 */
	public String getCdusuario() {
		return cdusuario;
	}

	/**
	 * Set: cdusuario.
	 *
	 * @param cdusuario the cdusuario
	 */
	public void setCdusuario(String cdusuario) {
		this.cdusuario = cdusuario;
	}

	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * Get: cdUsuarioExterno.
	 *
	 * @return cdUsuarioExterno
	 */
	public String getCdUsuarioExterno() {
		return cdUsuarioExterno;
	}

	/**
	 * Set: cdUsuarioExterno.
	 *
	 * @param cdUsuarioExterno the cd usuario externo
	 */
	public void setCdUsuarioExterno(String cdUsuarioExterno) {
		this.cdUsuarioExterno = cdUsuarioExterno;
	}

	/**
	 * Get: cdUtilizaCadastroOrgaosPagadores.
	 *
	 * @return cdUtilizaCadastroOrgaosPagadores
	 */
	public int getCdUtilizaCadastroOrgaosPagadores() {
		return cdUtilizaCadastroOrgaosPagadores;
	}

	/**
	 * Set: cdUtilizaCadastroOrgaosPagadores.
	 *
	 * @param cdUtilizaCadastroOrgaosPagadores the cd utiliza cadastro orgaos pagadores
	 */
	public void setCdUtilizaCadastroOrgaosPagadores(
			int cdUtilizaCadastroOrgaosPagadores) {
		this.cdUtilizaCadastroOrgaosPagadores = cdUtilizaCadastroOrgaosPagadores;
	}

	/**
	 * Get: cdUtilizacaoFavorecidoControle.
	 *
	 * @return cdUtilizacaoFavorecidoControle
	 */
	public Integer getCdUtilizacaoFavorecidoControle() {
		return cdUtilizacaoFavorecidoControle;
	}

	/**
	 * Set: cdUtilizacaoFavorecidoControle.
	 *
	 * @param cdUtilizacaoFavorecidoControle the cd utilizacao favorecido controle
	 */
	public void setCdUtilizacaoFavorecidoControle(
			Integer cdUtilizacaoFavorecidoControle) {
		this.cdUtilizacaoFavorecidoControle = cdUtilizacaoFavorecidoControle;
	}

	/**
	 * Get: cdUtilizacaoRastreamentoDebitoVeiculo.
	 *
	 * @return cdUtilizacaoRastreamentoDebitoVeiculo
	 */
	public int getCdUtilizacaoRastreamentoDebitoVeiculo() {
		return cdUtilizacaoRastreamentoDebitoVeiculo;
	}

	/**
	 * Set: cdUtilizacaoRastreamentoDebitoVeiculo.
	 *
	 * @param cdUtilizacaoRastreamentoDebitoVeiculo the cd utilizacao rastreamento debito veiculo
	 */
	public void setCdUtilizacaoRastreamentoDebitoVeiculo(
			int cdUtilizacaoRastreamentoDebitoVeiculo) {
		this.cdUtilizacaoRastreamentoDebitoVeiculo = cdUtilizacaoRastreamentoDebitoVeiculo;
	}

	/**
	 * Get: cdUtilizaPreAutorizacaoPagamentos.
	 *
	 * @return cdUtilizaPreAutorizacaoPagamentos
	 */
	public int getCdUtilizaPreAutorizacaoPagamentos() {
		return cdUtilizaPreAutorizacaoPagamentos;
	}

	/**
	 * Set: cdUtilizaPreAutorizacaoPagamentos.
	 *
	 * @param cdUtilizaPreAutorizacaoPagamentos the cd utiliza pre autorizacao pagamentos
	 */
	public void setCdUtilizaPreAutorizacaoPagamentos(
			int cdUtilizaPreAutorizacaoPagamentos) {
		this.cdUtilizaPreAutorizacaoPagamentos = cdUtilizaPreAutorizacaoPagamentos;
	}

	/**
	 * Get: cdValidacaoNomeFavorecido.
	 *
	 * @return cdValidacaoNomeFavorecido
	 */
	public Integer getCdValidacaoNomeFavorecido() {
		return cdValidacaoNomeFavorecido;
	}

	/**
	 * Set: cdValidacaoNomeFavorecido.
	 *
	 * @param cdValidacaoNomeFavorecido the cd validacao nome favorecido
	 */
	public void setCdValidacaoNomeFavorecido(Integer cdValidacaoNomeFavorecido) {
		this.cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
	}

	/**
	 * Get: codUtilizarMsgPersOnline.
	 *
	 * @return codUtilizarMsgPersOnline
	 */
	public int getCodUtilizarMsgPersOnline() {
		return codUtilizarMsgPersOnline;
	}

	/**
	 * Set: codUtilizarMsgPersOnline.
	 *
	 * @param codUtilizarMsgPersOnline the cod utilizar msg pers online
	 */
	public void setCodUtilizarMsgPersOnline(int codUtilizarMsgPersOnline) {
		this.codUtilizarMsgPersOnline = codUtilizarMsgPersOnline;
	}

	/**
	 * Get: dataInicioBloqPapeleta.
	 *
	 * @return dataInicioBloqPapeleta
	 */
	public String getDataInicioBloqPapeleta() {
		return dataInicioBloqPapeleta;
	}

	/**
	 * Set: dataInicioBloqPapeleta.
	 *
	 * @param dataInicioBloqPapeleta the data inicio bloq papeleta
	 */
	public void setDataInicioBloqPapeleta(String dataInicioBloqPapeleta) {
		this.dataInicioBloqPapeleta = dataInicioBloqPapeleta;
	}

	/**
	 * Get: dataInicioRastreamento.
	 *
	 * @return dataInicioRastreamento
	 */
	public String getDataInicioRastreamento() {
		return dataInicioRastreamento;
	}

	/**
	 * Set: dataInicioRastreamento.
	 *
	 * @param dataInicioRastreamento the data inicio rastreamento
	 */
	public void setDataInicioRastreamento(String dataInicioRastreamento) {
		this.dataInicioRastreamento = dataInicioRastreamento;
	}

	/**
	 * Get: dataRegistroTitulo.
	 *
	 * @return dataRegistroTitulo
	 */
	public String getDataRegistroTitulo() {
		return dataRegistroTitulo;
	}

	/**
	 * Set: dataRegistroTitulo.
	 *
	 * @param dataRegistroTitulo the data registro titulo
	 */
	public void setDataRegistroTitulo(String dataRegistroTitulo) {
		this.dataRegistroTitulo = dataRegistroTitulo;
	}

	/**
	 * Get: diaFechamentoApuracaoTarifaMensal.
	 *
	 * @return diaFechamentoApuracaoTarifaMensal
	 */
	public int getDiaFechamentoApuracaoTarifaMensal() {
		return diaFechamentoApuracaoTarifaMensal;
	}

	/**
	 * Set: diaFechamentoApuracaoTarifaMensal.
	 *
	 * @param diaFechamentoApuracaoTarifaMensal the dia fechamento apuracao tarifa mensal
	 */
	public void setDiaFechamentoApuracaoTarifaMensal(
			int diaFechamentoApuracaoTarifaMensal) {
		this.diaFechamentoApuracaoTarifaMensal = diaFechamentoApuracaoTarifaMensal;
	}

	/**
	 * Get: dsAgendamentoDebitoVeiculoRastreamento.
	 *
	 * @return dsAgendamentoDebitoVeiculoRastreamento
	 */
	public String getDsAgendamentoDebitoVeiculoRastreamento() {
		return dsAgendamentoDebitoVeiculoRastreamento;
	}

	/**
	 * Set: dsAgendamentoDebitoVeiculoRastreamento.
	 *
	 * @param dsAgendamentoDebitoVeiculoRastreamento the ds agendamento debito veiculo rastreamento
	 */
	public void setDsAgendamentoDebitoVeiculoRastreamento(
			String dsAgendamentoDebitoVeiculoRastreamento) {
		this.dsAgendamentoDebitoVeiculoRastreamento = dsAgendamentoDebitoVeiculoRastreamento;
	}

	/**
	 * Get: dsAreaReservada.
	 *
	 * @return dsAreaReservada
	 */
	public String getDsAreaReservada() {
		return dsAreaReservada;
	}

	/**
	 * Set: dsAreaReservada.
	 *
	 * @param dsAreaReservada the ds area reservada
	 */
	public void setDsAreaReservada(String dsAreaReservada) {
		this.dsAreaReservada = dsAreaReservada;
	}

	/**
	 * Get: dsConsistenciaCpfCnpjFavorecido.
	 *
	 * @return dsConsistenciaCpfCnpjFavorecido
	 */
	public String getDsConsistenciaCpfCnpjFavorecido() {
		return dsConsistenciaCpfCnpjFavorecido;
	}

	/**
	 * Set: dsConsistenciaCpfCnpjFavorecido.
	 *
	 * @param dsConsistenciaCpfCnpjFavorecido the ds consistencia cpf cnpj favorecido
	 */
	public void setDsConsistenciaCpfCnpjFavorecido(
			String dsConsistenciaCpfCnpjFavorecido) {
		this.dsConsistenciaCpfCnpjFavorecido = dsConsistenciaCpfCnpjFavorecido;
	}

	/**
	 * Get: dsEmiteCartaoAntecipadoContaSalario.
	 *
	 * @return dsEmiteCartaoAntecipadoContaSalario
	 */
	public String getDsEmiteCartaoAntecipadoContaSalario() {
		return dsEmiteCartaoAntecipadoContaSalario;
	}

	/**
	 * Set: dsEmiteCartaoAntecipadoContaSalario.
	 *
	 * @param dsEmiteCartaoAntecipadoContaSalario the ds emite cartao antecipado conta salario
	 */
	public void setDsEmiteCartaoAntecipadoContaSalario(
			String dsEmiteCartaoAntecipadoContaSalario) {
		this.dsEmiteCartaoAntecipadoContaSalario = dsEmiteCartaoAntecipadoContaSalario;
	}

	/**
	 * Get: dsFormaAutorizacaoPagamento.
	 *
	 * @return dsFormaAutorizacaoPagamento
	 */
	public String getDsFormaAutorizacaoPagamento() {
		return dsFormaAutorizacaoPagamento;
	}

	/**
	 * Set: dsFormaAutorizacaoPagamento.
	 *
	 * @param dsFormaAutorizacaoPagamento the ds forma autorizacao pagamento
	 */
	public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
		this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
	}

	/**
	 * Get: dsFormaControleExpiracaoCredito.
	 *
	 * @return dsFormaControleExpiracaoCredito
	 */
	public String getDsFormaControleExpiracaoCredito() {
		return dsFormaControleExpiracaoCredito;
	}

	/**
	 * Set: dsFormaControleExpiracaoCredito.
	 *
	 * @param dsFormaControleExpiracaoCredito the ds forma controle expiracao credito
	 */
	public void setDsFormaControleExpiracaoCredito(
			String dsFormaControleExpiracaoCredito) {
		this.dsFormaControleExpiracaoCredito = dsFormaControleExpiracaoCredito;
	}

	/**
	 * Get: dsFormaEnvioPagamento.
	 *
	 * @return dsFormaEnvioPagamento
	 */
	public String getDsFormaEnvioPagamento() {
		return dsFormaEnvioPagamento;
	}

	/**
	 * Set: dsFormaEnvioPagamento.
	 *
	 * @param dsFormaEnvioPagamento the ds forma envio pagamento
	 */
	public void setDsFormaEnvioPagamento(String dsFormaEnvioPagamento) {
		this.dsFormaEnvioPagamento = dsFormaEnvioPagamento;
	}

	/**
	 * Get: dsFormaManutencaoCadastro.
	 *
	 * @return dsFormaManutencaoCadastro
	 */
	public String getDsFormaManutencaoCadastro() {
		return dsFormaManutencaoCadastro;
	}

	/**
	 * Set: dsFormaManutencaoCadastro.
	 *
	 * @param dsFormaManutencaoCadastro the ds forma manutencao cadastro
	 */
	public void setDsFormaManutencaoCadastro(String dsFormaManutencaoCadastro) {
		this.dsFormaManutencaoCadastro = dsFormaManutencaoCadastro;
	}

	/**
	 * Get: dsFormaManutencaoCadastroProcurador.
	 *
	 * @return dsFormaManutencaoCadastroProcurador
	 */
	public String getDsFormaManutencaoCadastroProcurador() {
		return dsFormaManutencaoCadastroProcurador;
	}

	/**
	 * Set: dsFormaManutencaoCadastroProcurador.
	 *
	 * @param dsFormaManutencaoCadastroProcurador the ds forma manutencao cadastro procurador
	 */
	public void setDsFormaManutencaoCadastroProcurador(
			String dsFormaManutencaoCadastroProcurador) {
		this.dsFormaManutencaoCadastroProcurador = dsFormaManutencaoCadastroProcurador;
	}

	/**
	 * Get: dsInformaAgenciaContaCredito.
	 *
	 * @return dsInformaAgenciaContaCredito
	 */
	public String getDsInformaAgenciaContaCredito() {
		return dsInformaAgenciaContaCredito;
	}

	/**
	 * Set: dsInformaAgenciaContaCredito.
	 *
	 * @param dsInformaAgenciaContaCredito the ds informa agencia conta credito
	 */
	public void setDsInformaAgenciaContaCredito(String dsInformaAgenciaContaCredito) {
		this.dsInformaAgenciaContaCredito = dsInformaAgenciaContaCredito;
	}

	/**
	 * Get: dsMantemCadastroProcuradores.
	 *
	 * @return dsMantemCadastroProcuradores
	 */
	public String getDsMantemCadastroProcuradores() {
		return dsMantemCadastroProcuradores;
	}

	/**
	 * Set: dsMantemCadastroProcuradores.
	 *
	 * @param dsMantemCadastroProcuradores the ds mantem cadastro procuradores
	 */
	public void setDsMantemCadastroProcuradores(String dsMantemCadastroProcuradores) {
		this.dsMantemCadastroProcuradores = dsMantemCadastroProcuradores;
	}

	/**
	 * Get: dsPeriodicidade.
	 *
	 * @return dsPeriodicidade
	 */
	public String getDsPeriodicidade() {
		return dsPeriodicidade;
	}

	/**
	 * Set: dsPeriodicidade.
	 *
	 * @param dsPeriodicidade the ds periodicidade
	 */
	public void setDsPeriodicidade(String dsPeriodicidade) {
		this.dsPeriodicidade = dsPeriodicidade;
	}

	/**
	 * Get: dsPeriodicidadeCobrancaTarifa.
	 *
	 * @return dsPeriodicidadeCobrancaTarifa
	 */
	public String getDsPeriodicidadeCobrancaTarifa() {
		return dsPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Set: dsPeriodicidadeCobrancaTarifa.
	 *
	 * @param dsPeriodicidadeCobrancaTarifa the ds periodicidade cobranca tarifa
	 */
	public void setDsPeriodicidadeCobrancaTarifa(
			String dsPeriodicidadeCobrancaTarifa) {
		this.dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Get: dsPeriodicidadeCobrancaTarifaEmissaoComprovantes.
	 *
	 * @return dsPeriodicidadeCobrancaTarifaEmissaoComprovantes
	 */
	public String getDsPeriodicidadeCobrancaTarifaEmissaoComprovantes() {
		return dsPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	}

	/**
	 * Set: dsPeriodicidadeCobrancaTarifaEmissaoComprovantes.
	 *
	 * @param dsPeriodicidadeCobrancaTarifaEmissaoComprovantes the ds periodicidade cobranca tarifa emissao comprovantes
	 */
	public void setDsPeriodicidadeCobrancaTarifaEmissaoComprovantes(
			String dsPeriodicidadeCobrancaTarifaEmissaoComprovantes) {
		this.dsPeriodicidadeCobrancaTarifaEmissaoComprovantes = dsPeriodicidadeCobrancaTarifaEmissaoComprovantes;
	}

	/**
	 * Get: dsPeriodicidadeManutencaoCadastroProcurador.
	 *
	 * @return dsPeriodicidadeManutencaoCadastroProcurador
	 */
	public String getDsPeriodicidadeManutencaoCadastroProcurador() {
		return dsPeriodicidadeManutencaoCadastroProcurador;
	}

	/**
	 * Set: dsPeriodicidadeManutencaoCadastroProcurador.
	 *
	 * @param dsPeriodicidadeManutencaoCadastroProcurador the ds periodicidade manutencao cadastro procurador
	 */
	public void setDsPeriodicidadeManutencaoCadastroProcurador(
			String dsPeriodicidadeManutencaoCadastroProcurador) {
		this.dsPeriodicidadeManutencaoCadastroProcurador = dsPeriodicidadeManutencaoCadastroProcurador;
	}

	/**
	 * Get: dsPeriodicidadeRastreamentoDebitoVeiculo.
	 *
	 * @return dsPeriodicidadeRastreamentoDebitoVeiculo
	 */
	public String getDsPeriodicidadeRastreamentoDebitoVeiculo() {
		return dsPeriodicidadeRastreamentoDebitoVeiculo;
	}

	/**
	 * Set: dsPeriodicidadeRastreamentoDebitoVeiculo.
	 *
	 * @param dsPeriodicidadeRastreamentoDebitoVeiculo the ds periodicidade rastreamento debito veiculo
	 */
	public void setDsPeriodicidadeRastreamentoDebitoVeiculo(
			String dsPeriodicidadeRastreamentoDebitoVeiculo) {
		this.dsPeriodicidadeRastreamentoDebitoVeiculo = dsPeriodicidadeRastreamentoDebitoVeiculo;
	}

	/**
	 * Get: dsPeriodicidadeReajusteTarifa.
	 *
	 * @return dsPeriodicidadeReajusteTarifa
	 */
	public String getDsPeriodicidadeReajusteTarifa() {
		return dsPeriodicidadeReajusteTarifa;
	}

	/**
	 * Set: dsPeriodicidadeReajusteTarifa.
	 *
	 * @param dsPeriodicidadeReajusteTarifa the ds periodicidade reajuste tarifa
	 */
	public void setDsPeriodicidadeReajusteTarifa(
			String dsPeriodicidadeReajusteTarifa) {
		this.dsPeriodicidadeReajusteTarifa = dsPeriodicidadeReajusteTarifa;
	}

	/**
	 * Get: dsPossuiExpiracaoCredito.
	 *
	 * @return dsPossuiExpiracaoCredito
	 */
	public String getDsPossuiExpiracaoCredito() {
		return dsPossuiExpiracaoCredito;
	}

	/**
	 * Set: dsPossuiExpiracaoCredito.
	 *
	 * @param dsPossuiExpiracaoCredito the ds possui expiracao credito
	 */
	public void setDsPossuiExpiracaoCredito(String dsPossuiExpiracaoCredito) {
		this.dsPossuiExpiracaoCredito = dsPossuiExpiracaoCredito;
	}

	/**
	 * Get: dsTipoAutorizacaoPagamento.
	 *
	 * @return dsTipoAutorizacaoPagamento
	 */
	public String getDsTipoAutorizacaoPagamento() {
		return dsTipoAutorizacaoPagamento;
	}

	/**
	 * Set: dsTipoAutorizacaoPagamento.
	 *
	 * @param dsTipoAutorizacaoPagamento the ds tipo autorizacao pagamento
	 */
	public void setDsTipoAutorizacaoPagamento(String dsTipoAutorizacaoPagamento) {
		this.dsTipoAutorizacaoPagamento = dsTipoAutorizacaoPagamento;
	}

	/**
	 * Get: dsTipoCartao.
	 *
	 * @return dsTipoCartao
	 */
	public String getDsTipoCartao() {
		return dsTipoCartao;
	}

	/**
	 * Set: dsTipoCartao.
	 *
	 * @param dsTipoCartao the ds tipo cartao
	 */
	public void setDsTipoCartao(String dsTipoCartao) {
		this.dsTipoCartao = dsTipoCartao;
	}

	/**
	 * Get: dsTipoControleFloating.
	 *
	 * @return dsTipoControleFloating
	 */
	public String getDsTipoControleFloating() {
		return dsTipoControleFloating;
	}

	/**
	 * Set: dsTipoControleFloating.
	 *
	 * @param dsTipoControleFloating the ds tipo controle floating
	 */
	public void setDsTipoControleFloating(String dsTipoControleFloating) {
		this.dsTipoControleFloating = dsTipoControleFloating;
	}

	/**
	 * Get: dsTipoIdentificacaoBeneficio.
	 *
	 * @return dsTipoIdentificacaoBeneficio
	 */
	public String getDsTipoIdentificacaoBeneficio() {
		return dsTipoIdentificacaoBeneficio;
	}

	/**
	 * Set: dsTipoIdentificacaoBeneficio.
	 *
	 * @param dsTipoIdentificacaoBeneficio the ds tipo identificacao beneficio
	 */
	public void setDsTipoIdentificacaoBeneficio(String dsTipoIdentificacaoBeneficio) {
		this.dsTipoIdentificacaoBeneficio = dsTipoIdentificacaoBeneficio;
	}

	/**
	 * Get: dsTipoPrestacaoContaCreditoPago.
	 *
	 * @return dsTipoPrestacaoContaCreditoPago
	 */
	public String getDsTipoPrestacaoContaCreditoPago() {
		return dsTipoPrestacaoContaCreditoPago;
	}

	/**
	 * Set: dsTipoPrestacaoContaCreditoPago.
	 *
	 * @param dsTipoPrestacaoContaCreditoPago the ds tipo prestacao conta credito pago
	 */
	public void setDsTipoPrestacaoContaCreditoPago(
			String dsTipoPrestacaoContaCreditoPago) {
		this.dsTipoPrestacaoContaCreditoPago = dsTipoPrestacaoContaCreditoPago;
	}

	/**
	 * Get: dsTipoReajusteTarifa.
	 *
	 * @return dsTipoReajusteTarifa
	 */
	public String getDsTipoReajusteTarifa() {
		return dsTipoReajusteTarifa;
	}

	/**
	 * Set: dsTipoReajusteTarifa.
	 *
	 * @param dsTipoReajusteTarifa the ds tipo reajuste tarifa
	 */
	public void setDsTipoReajusteTarifa(String dsTipoReajusteTarifa) {
		this.dsTipoReajusteTarifa = dsTipoReajusteTarifa;
	}

	/**
	 * Get: dsTratamentoFeriadoFimVigencia.
	 *
	 * @return dsTratamentoFeriadoFimVigencia
	 */
	public String getDsTratamentoFeriadoFimVigencia() {
		return dsTratamentoFeriadoFimVigencia;
	}

	/**
	 * Set: dsTratamentoFeriadoFimVigencia.
	 *
	 * @param dsTratamentoFeriadoFimVigencia the ds tratamento feriado fim vigencia
	 */
	public void setDsTratamentoFeriadoFimVigencia(
			String dsTratamentoFeriadoFimVigencia) {
		this.dsTratamentoFeriadoFimVigencia = dsTratamentoFeriadoFimVigencia;
	}

	/**
	 * Get: dsTratamentoFeriadosDtaPag.
	 *
	 * @return dsTratamentoFeriadosDtaPag
	 */
	public String getDsTratamentoFeriadosDtaPag() {
		return dsTratamentoFeriadosDtaPag;
	}

	/**
	 * Set: dsTratamentoFeriadosDtaPag.
	 *
	 * @param dsTratamentoFeriadosDtaPag the ds tratamento feriados dta pag
	 */
	public void setDsTratamentoFeriadosDtaPag(String dsTratamentoFeriadosDtaPag) {
		this.dsTratamentoFeriadosDtaPag = dsTratamentoFeriadosDtaPag;
	}

	/**
	 * Get: dsUtilizaCadastroOrgaosPagadores.
	 *
	 * @return dsUtilizaCadastroOrgaosPagadores
	 */
	public String getDsUtilizaCadastroOrgaosPagadores() {
		return dsUtilizaCadastroOrgaosPagadores;
	}

	/**
	 * Set: dsUtilizaCadastroOrgaosPagadores.
	 *
	 * @param dsUtilizaCadastroOrgaosPagadores the ds utiliza cadastro orgaos pagadores
	 */
	public void setDsUtilizaCadastroOrgaosPagadores(
			String dsUtilizaCadastroOrgaosPagadores) {
		this.dsUtilizaCadastroOrgaosPagadores = dsUtilizaCadastroOrgaosPagadores;
	}

	/**
	 * Get: dsUtilizacaoRastreamentoDebitoVeiculo.
	 *
	 * @return dsUtilizacaoRastreamentoDebitoVeiculo
	 */
	public String getDsUtilizacaoRastreamentoDebitoVeiculo() {
		return dsUtilizacaoRastreamentoDebitoVeiculo;
	}

	/**
	 * Set: dsUtilizacaoRastreamentoDebitoVeiculo.
	 *
	 * @param dsUtilizacaoRastreamentoDebitoVeiculo the ds utilizacao rastreamento debito veiculo
	 */
	public void setDsUtilizacaoRastreamentoDebitoVeiculo(
			String dsUtilizacaoRastreamentoDebitoVeiculo) {
		this.dsUtilizacaoRastreamentoDebitoVeiculo = dsUtilizacaoRastreamentoDebitoVeiculo;
	}

	/**
	 * Get: dsUtilizaPreAutorizacaoPagamentos.
	 *
	 * @return dsUtilizaPreAutorizacaoPagamentos
	 */
	public String getDsUtilizaPreAutorizacaoPagamentos() {
		return dsUtilizaPreAutorizacaoPagamentos;
	}

	/**
	 * Set: dsUtilizaPreAutorizacaoPagamentos.
	 *
	 * @param dsUtilizaPreAutorizacaoPagamentos the ds utiliza pre autorizacao pagamentos
	 */
	public void setDsUtilizaPreAutorizacaoPagamentos(
			String dsUtilizaPreAutorizacaoPagamentos) {
		this.dsUtilizaPreAutorizacaoPagamentos = dsUtilizaPreAutorizacaoPagamentos;
	}

	/**
	 * Get: dtEnquaContaSalario.
	 *
	 * @return dtEnquaContaSalario
	 */
	public String getDtEnquaContaSalario() {
		return dtEnquaContaSalario;
	}

	/**
	 * Set: dtEnquaContaSalario.
	 *
	 * @param dtEnquaContaSalario the dt enqua conta salario
	 */
	public void setDtEnquaContaSalario(String dtEnquaContaSalario) {
		this.dtEnquaContaSalario = dtEnquaContaSalario;
	}

	/**
	 * Get: dtFimAcertoRecadastro.
	 *
	 * @return dtFimAcertoRecadastro
	 */
	public String getDtFimAcertoRecadastro() {
		return dtFimAcertoRecadastro;
	}

	/**
	 * Set: dtFimAcertoRecadastro.
	 *
	 * @param dtFimAcertoRecadastro the dt fim acerto recadastro
	 */
	public void setDtFimAcertoRecadastro(String dtFimAcertoRecadastro) {
		this.dtFimAcertoRecadastro = dtFimAcertoRecadastro;
	}

	/**
	 * Get: dtFimPeriodoAcertoDados.
	 *
	 * @return dtFimPeriodoAcertoDados
	 */
	public String getDtFimPeriodoAcertoDados() {
		return dtFimPeriodoAcertoDados;
	}

	/**
	 * Set: dtFimPeriodoAcertoDados.
	 *
	 * @param dtFimPeriodoAcertoDados the dt fim periodo acerto dados
	 */
	public void setDtFimPeriodoAcertoDados(String dtFimPeriodoAcertoDados) {
		this.dtFimPeriodoAcertoDados = dtFimPeriodoAcertoDados;
	}

	/**
	 * Get: dtFimRecadastramento.
	 *
	 * @return dtFimRecadastramento
	 */
	public String getDtFimRecadastramento() {
		return dtFimRecadastramento;
	}

	/**
	 * Set: dtFimRecadastramento.
	 *
	 * @param dtFimRecadastramento the dt fim recadastramento
	 */
	public void setDtFimRecadastramento(String dtFimRecadastramento) {
		this.dtFimRecadastramento = dtFimRecadastramento;
	}

	/**
	 * Get: dtFimRecadastroBeneficio.
	 *
	 * @return dtFimRecadastroBeneficio
	 */
	public String getDtFimRecadastroBeneficio() {
		return dtFimRecadastroBeneficio;
	}

	/**
	 * Set: dtFimRecadastroBeneficio.
	 *
	 * @param dtFimRecadastroBeneficio the dt fim recadastro beneficio
	 */
	public void setDtFimRecadastroBeneficio(String dtFimRecadastroBeneficio) {
		this.dtFimRecadastroBeneficio = dtFimRecadastroBeneficio;
	}

	/**
	 * Get: dtInicioAcertoRecadastro.
	 *
	 * @return dtInicioAcertoRecadastro
	 */
	public String getDtInicioAcertoRecadastro() {
		return dtInicioAcertoRecadastro;
	}

	/**
	 * Set: dtInicioAcertoRecadastro.
	 *
	 * @param dtInicioAcertoRecadastro the dt inicio acerto recadastro
	 */
	public void setDtInicioAcertoRecadastro(String dtInicioAcertoRecadastro) {
		this.dtInicioAcertoRecadastro = dtInicioAcertoRecadastro;
	}

	/**
	 * Get: dtInicioBloqueioPapeleta.
	 *
	 * @return dtInicioBloqueioPapeleta
	 */
	public String getDtInicioBloqueioPapeleta() {
		return dtInicioBloqueioPapeleta;
	}

	/**
	 * Set: dtInicioBloqueioPapeleta.
	 *
	 * @param dtInicioBloqueioPapeleta the dt inicio bloqueio papeleta
	 */
	public void setDtInicioBloqueioPapeleta(String dtInicioBloqueioPapeleta) {
		this.dtInicioBloqueioPapeleta = dtInicioBloqueioPapeleta;
	}

	/**
	 * Get: dtInicioPeriodoAcertoDados.
	 *
	 * @return dtInicioPeriodoAcertoDados
	 */
	public String getDtInicioPeriodoAcertoDados() {
		return dtInicioPeriodoAcertoDados;
	}

	/**
	 * Set: dtInicioPeriodoAcertoDados.
	 *
	 * @param dtInicioPeriodoAcertoDados the dt inicio periodo acerto dados
	 */
	public void setDtInicioPeriodoAcertoDados(String dtInicioPeriodoAcertoDados) {
		this.dtInicioPeriodoAcertoDados = dtInicioPeriodoAcertoDados;
	}

	/**
	 * Get: dtInicioRastreabilidadeTitulo.
	 *
	 * @return dtInicioRastreabilidadeTitulo
	 */
	public String getDtInicioRastreabilidadeTitulo() {
		return dtInicioRastreabilidadeTitulo;
	}

	/**
	 * Set: dtInicioRastreabilidadeTitulo.
	 *
	 * @param dtInicioRastreabilidadeTitulo the dt inicio rastreabilidade titulo
	 */
	public void setDtInicioRastreabilidadeTitulo(
			String dtInicioRastreabilidadeTitulo) {
		this.dtInicioRastreabilidadeTitulo = dtInicioRastreabilidadeTitulo;
	}

	/**
	 * Get: dtInicioRecadastramento.
	 *
	 * @return dtInicioRecadastramento
	 */
	public String getDtInicioRecadastramento() {
		return dtInicioRecadastramento;
	}

	/**
	 * Set: dtInicioRecadastramento.
	 *
	 * @param dtInicioRecadastramento the dt inicio recadastramento
	 */
	public void setDtInicioRecadastramento(String dtInicioRecadastramento) {
		this.dtInicioRecadastramento = dtInicioRecadastramento;
	}

	/**
	 * Get: dtInicioRecadastroBeneficio.
	 *
	 * @return dtInicioRecadastroBeneficio
	 */
	public String getDtInicioRecadastroBeneficio() {
		return dtInicioRecadastroBeneficio;
	}

	/**
	 * Set: dtInicioRecadastroBeneficio.
	 *
	 * @param dtInicioRecadastroBeneficio the dt inicio recadastro beneficio
	 */
	public void setDtInicioRecadastroBeneficio(String dtInicioRecadastroBeneficio) {
		this.dtInicioRecadastroBeneficio = dtInicioRecadastroBeneficio;
	}

	/**
	 * Get: dtLimiteEnquadramentoConvenioContaSalario.
	 *
	 * @return dtLimiteEnquadramentoConvenioContaSalario
	 */
	public String getDtLimiteEnquadramentoConvenioContaSalario() {
		return dtLimiteEnquadramentoConvenioContaSalario;
	}

	/**
	 * Set: dtLimiteEnquadramentoConvenioContaSalario.
	 *
	 * @param dtLimiteEnquadramentoConvenioContaSalario the dt limite enquadramento convenio conta salario
	 */
	public void setDtLimiteEnquadramentoConvenioContaSalario(
			String dtLimiteEnquadramentoConvenioContaSalario) {
		this.dtLimiteEnquadramentoConvenioContaSalario = dtLimiteEnquadramentoConvenioContaSalario;
	}

	/**
	 * Get: dtLimiteInicioVinculoCargaBase.
	 *
	 * @return dtLimiteInicioVinculoCargaBase
	 */
	public String getDtLimiteInicioVinculoCargaBase() {
		return dtLimiteInicioVinculoCargaBase;
	}

	/**
	 * Set: dtLimiteInicioVinculoCargaBase.
	 *
	 * @param dtLimiteInicioVinculoCargaBase the dt limite inicio vinculo carga base
	 */
	public void setDtLimiteInicioVinculoCargaBase(
			String dtLimiteInicioVinculoCargaBase) {
		this.dtLimiteInicioVinculoCargaBase = dtLimiteInicioVinculoCargaBase;
	}

	/**
	 * Get: dtLimiteVinculoCarga.
	 *
	 * @return dtLimiteVinculoCarga
	 */
	public String getDtLimiteVinculoCarga() {
		return dtLimiteVinculoCarga;
	}

	/**
	 * Set: dtLimiteVinculoCarga.
	 *
	 * @param dtLimiteVinculoCarga the dt limite vinculo carga
	 */
	public void setDtLimiteVinculoCarga(String dtLimiteVinculoCarga) {
		this.dtLimiteVinculoCarga = dtLimiteVinculoCarga;
	}

	/**
	 * Get: emiteCartaoAntecipadoContaSalario.
	 *
	 * @return emiteCartaoAntecipadoContaSalario
	 */
	public int getEmiteCartaoAntecipadoContaSalario() {
		return emiteCartaoAntecipadoContaSalario;
	}

	/**
	 * Set: emiteCartaoAntecipadoContaSalario.
	 *
	 * @param emiteCartaoAntecipadoContaSalario the emite cartao antecipado conta salario
	 */
	public void setEmiteCartaoAntecipadoContaSalario(
			int emiteCartaoAntecipadoContaSalario) {
		this.emiteCartaoAntecipadoContaSalario = emiteCartaoAntecipadoContaSalario;
	}

	/**
	 * Get: nmOperacaoFluxo.
	 *
	 * @return nmOperacaoFluxo
	 */
	public String getNmOperacaoFluxo() {
		return nmOperacaoFluxo;
	}

	/**
	 * Set: nmOperacaoFluxo.
	 *
	 * @param nmOperacaoFluxo the nm operacao fluxo
	 */
	public void setNmOperacaoFluxo(String nmOperacaoFluxo) {
		this.nmOperacaoFluxo = nmOperacaoFluxo;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: nrSequenciaCotratoNegocio.
	 *
	 * @return nrSequenciaCotratoNegocio
	 */
	public Long getNrSequenciaCotratoNegocio() {
		return nrSequenciaCotratoNegocio;
	}

	/**
	 * Set: nrSequenciaCotratoNegocio.
	 *
	 * @param nrSequenciaCotratoNegocio the nr sequencia cotrato negocio
	 */
	public void setNrSequenciaCotratoNegocio(Long nrSequenciaCotratoNegocio) {
		this.nrSequenciaCotratoNegocio = nrSequenciaCotratoNegocio;
	}

	/**
	 * Get: percentualBonificacaoTarifaPadrao.
	 *
	 * @return percentualBonificacaoTarifaPadrao
	 */
	public BigDecimal getPercentualBonificacaoTarifaPadrao() {
		return percentualBonificacaoTarifaPadrao;
	}

	/**
	 * Set: percentualBonificacaoTarifaPadrao.
	 *
	 * @param percentualBonificacaoTarifaPadrao the percentual bonificacao tarifa padrao
	 */
	public void setPercentualBonificacaoTarifaPadrao(
			BigDecimal percentualBonificacaoTarifaPadrao) {
		this.percentualBonificacaoTarifaPadrao = percentualBonificacaoTarifaPadrao;
	}

	/**
	 * Get: percentualIndiceEconomicoReajusteTarifa.
	 *
	 * @return percentualIndiceEconomicoReajusteTarifa
	 */
	public BigDecimal getPercentualIndiceEconomicoReajusteTarifa() {
		return percentualIndiceEconomicoReajusteTarifa;
	}

	/**
	 * Set: percentualIndiceEconomicoReajusteTarifa.
	 *
	 * @param percentualIndiceEconomicoReajusteTarifa the percentual indice economico reajuste tarifa
	 */
	public void setPercentualIndiceEconomicoReajusteTarifa(
			BigDecimal percentualIndiceEconomicoReajusteTarifa) {
		this.percentualIndiceEconomicoReajusteTarifa = percentualIndiceEconomicoReajusteTarifa;
	}

	/**
	 * Get: percentualMaxRegInconsistenciaLote.
	 *
	 * @return percentualMaxRegInconsistenciaLote
	 */
	public int getPercentualMaxRegInconsistenciaLote() {
		return percentualMaxRegInconsistenciaLote;
	}

	/**
	 * Set: percentualMaxRegInconsistenciaLote.
	 *
	 * @param percentualMaxRegInconsistenciaLote the percentual max reg inconsistencia lote
	 */
	public void setPercentualMaxRegInconsistenciaLote(
			int percentualMaxRegInconsistenciaLote) {
		this.percentualMaxRegInconsistenciaLote = percentualMaxRegInconsistenciaLote;
	}

	/**
	 * Get: percentualReajuste.
	 *
	 * @return percentualReajuste
	 */
	public BigDecimal getPercentualReajuste() {
		return percentualReajuste;
	}

	/**
	 * Set: percentualReajuste.
	 *
	 * @param percentualReajuste the percentual reajuste
	 */
	public void setPercentualReajuste(BigDecimal percentualReajuste) {
		this.percentualReajuste = percentualReajuste;
	}

	/**
	 * Get: perInconsistenciaLote.
	 *
	 * @return perInconsistenciaLote
	 */
	public int getPerInconsistenciaLote() {
		return perInconsistenciaLote;
	}

	/**
	 * Set: perInconsistenciaLote.
	 *
	 * @param perInconsistenciaLote the per inconsistencia lote
	 */
	public void setPerInconsistenciaLote(int perInconsistenciaLote) {
		this.perInconsistenciaLote = perInconsistenciaLote;
	}

	/**
	 * Get: permiteContingenciaPag.
	 *
	 * @return permiteContingenciaPag
	 */
	public int getPermiteContingenciaPag() {
		return permiteContingenciaPag;
	}

	/**
	 * Set: permiteContingenciaPag.
	 *
	 * @param permiteContingenciaPag the permite contingencia pag
	 */
	public void setPermiteContingenciaPag(int permiteContingenciaPag) {
		this.permiteContingenciaPag = permiteContingenciaPag;
	}

	/**
	 * Get: porcentagemBonificacaoTarifa.
	 *
	 * @return porcentagemBonificacaoTarifa
	 */
	public BigDecimal getPorcentagemBonificacaoTarifa() {
		return porcentagemBonificacaoTarifa;
	}

	/**
	 * Set: porcentagemBonificacaoTarifa.
	 *
	 * @param porcentagemBonificacaoTarifa the porcentagem bonificacao tarifa
	 */
	public void setPorcentagemBonificacaoTarifa(
			BigDecimal porcentagemBonificacaoTarifa) {
		this.porcentagemBonificacaoTarifa = porcentagemBonificacaoTarifa;
	}

	/**
	 * Get: porcentInconsistenciaLote.
	 *
	 * @return porcentInconsistenciaLote
	 */
	public int getPorcentInconsistenciaLote() {
		return porcentInconsistenciaLote;
	}

	/**
	 * Set: porcentInconsistenciaLote.
	 *
	 * @param porcentInconsistenciaLote the porcent inconsistencia lote
	 */
	public void setPorcentInconsistenciaLote(int porcentInconsistenciaLote) {
		this.porcentInconsistenciaLote = porcentInconsistenciaLote;
	}

	/**
	 * Get: qntdDiasAntecedeInicio.
	 *
	 * @return qntdDiasAntecedeInicio
	 */
	public int getQntdDiasAntecedeInicio() {
		return qntdDiasAntecedeInicio;
	}

	/**
	 * Set: qntdDiasAntecedeInicio.
	 *
	 * @param qntdDiasAntecedeInicio the qntd dias antecede inicio
	 */
	public void setQntdDiasAntecedeInicio(int qntdDiasAntecedeInicio) {
		this.qntdDiasAntecedeInicio = qntdDiasAntecedeInicio;
	}

	/**
	 * Get: qntdDiasAvisoVenc.
	 *
	 * @return qntdDiasAvisoVenc
	 */
	public int getQntdDiasAvisoVenc() {
		return qntdDiasAvisoVenc;
	}

	/**
	 * Set: qntdDiasAvisoVenc.
	 *
	 * @param qntdDiasAvisoVenc the qntd dias aviso venc
	 */
	public void setQntdDiasAvisoVenc(int qntdDiasAvisoVenc) {
		this.qntdDiasAvisoVenc = qntdDiasAvisoVenc;
	}

	/**
	 * Get: qntdDiasExpiracao.
	 *
	 * @return qntdDiasExpiracao
	 */
	public int getQntdDiasExpiracao() {
		return qntdDiasExpiracao;
	}

	/**
	 * Set: qntdDiasExpiracao.
	 *
	 * @param qntdDiasExpiracao the qntd dias expiracao
	 */
	public void setQntdDiasExpiracao(int qntdDiasExpiracao) {
		this.qntdDiasExpiracao = qntdDiasExpiracao;
	}

	/**
	 * Get: qntdDiasRepConsulta.
	 *
	 * @return qntdDiasRepConsulta
	 */
	public int getQntdDiasRepConsulta() {
		return qntdDiasRepConsulta;
	}

	/**
	 * Set: qntdDiasRepConsulta.
	 *
	 * @param qntdDiasRepConsulta the qntd dias rep consulta
	 */
	public void setQntdDiasRepConsulta(int qntdDiasRepConsulta) {
		this.qntdDiasRepConsulta = qntdDiasRepConsulta;
	}

	/**
	 * Get: qntDiasAntecipacao.
	 *
	 * @return qntDiasAntecipacao
	 */
	public int getQntDiasAntecipacao() {
		return qntDiasAntecipacao;
	}

	/**
	 * Set: qntDiasAntecipacao.
	 *
	 * @param qntDiasAntecipacao the qnt dias antecipacao
	 */
	public void setQntDiasAntecipacao(int qntDiasAntecipacao) {
		this.qntDiasAntecipacao = qntDiasAntecipacao;
	}

	/**
	 * Get: qntdLimiteDiasPagVencido.
	 *
	 * @return qntdLimiteDiasPagVencido
	 */
	public int getQntdLimiteDiasPagVencido() {
		return qntdLimiteDiasPagVencido;
	}

	/**
	 * Set: qntdLimiteDiasPagVencido.
	 *
	 * @param qntdLimiteDiasPagVencido the qntd limite dias pag vencido
	 */
	public void setQntdLimiteDiasPagVencido(int qntdLimiteDiasPagVencido) {
		this.qntdLimiteDiasPagVencido = qntdLimiteDiasPagVencido;
	}

	/**
	 * Get: qntdMaxInconsistenciaLote.
	 *
	 * @return qntdMaxInconsistenciaLote
	 */
	public int getQntdMaxInconsistenciaLote() {
		return qntdMaxInconsistenciaLote;
	}

	/**
	 * Set: qntdMaxInconsistenciaLote.
	 *
	 * @param qntdMaxInconsistenciaLote the qntd max inconsistencia lote
	 */
	public void setQntdMaxInconsistenciaLote(int qntdMaxInconsistenciaLote) {
		this.qntdMaxInconsistenciaLote = qntdMaxInconsistenciaLote;
	}

	/**
	 * Get: qntdMaxRegInconsistentes.
	 *
	 * @return qntdMaxRegInconsistentes
	 */
	public int getQntdMaxRegInconsistentes() {
		return qntdMaxRegInconsistentes;
	}

	/**
	 * Set: qntdMaxRegInconsistentes.
	 *
	 * @param qntdMaxRegInconsistentes the qntd max reg inconsistentes
	 */
	public void setQntdMaxRegInconsistentes(int qntdMaxRegInconsistentes) {
		this.qntdMaxRegInconsistentes = qntdMaxRegInconsistentes;
	}

	/**
	 * Get: qntdMesesPeriComprovacao.
	 *
	 * @return qntdMesesPeriComprovacao
	 */
	public int getQntdMesesPeriComprovacao() {
		return qntdMesesPeriComprovacao;
	}

	/**
	 * Set: qntdMesesPeriComprovacao.
	 *
	 * @param qntdMesesPeriComprovacao the qntd meses peri comprovacao
	 */
	public void setQntdMesesPeriComprovacao(int qntdMesesPeriComprovacao) {
		this.qntdMesesPeriComprovacao = qntdMesesPeriComprovacao;
	}

	/**
	 * Get: qtAntecedencia.
	 *
	 * @return qtAntecedencia
	 */
	public Integer getQtAntecedencia() {
		return qtAntecedencia;
	}

	/**
	 * Set: qtAntecedencia.
	 *
	 * @param qtAntecedencia the qt antecedencia
	 */
	public void setQtAntecedencia(Integer qtAntecedencia) {
		this.qtAntecedencia = qtAntecedencia;
	}

	/**
	 * Get: qtAnteriorVencimentoComprovado.
	 *
	 * @return qtAnteriorVencimentoComprovado
	 */
	public Integer getQtAnteriorVencimentoComprovado() {
		return qtAnteriorVencimentoComprovado;
	}

	/**
	 * Set: qtAnteriorVencimentoComprovado.
	 *
	 * @param qtAnteriorVencimentoComprovado the qt anterior vencimento comprovado
	 */
	public void setQtAnteriorVencimentoComprovado(
			Integer qtAnteriorVencimentoComprovado) {
		this.qtAnteriorVencimentoComprovado = qtAnteriorVencimentoComprovado;
	}

	/**
	 * Get: qtdeDiasCobrancaAposApuracao.
	 *
	 * @return qtdeDiasCobrancaAposApuracao
	 */
	public int getQtdeDiasCobrancaAposApuracao() {
		return qtdeDiasCobrancaAposApuracao;
	}

	/**
	 * Set: qtdeDiasCobrancaAposApuracao.
	 *
	 * @param qtdeDiasCobrancaAposApuracao the qtde dias cobranca apos apuracao
	 */
	public void setQtdeDiasCobrancaAposApuracao(int qtdeDiasCobrancaAposApuracao) {
		this.qtdeDiasCobrancaAposApuracao = qtdeDiasCobrancaAposApuracao;
	}

	/**
	 * Get: qtdeDiasCobrancaTarifaAposApuracao.
	 *
	 * @return qtdeDiasCobrancaTarifaAposApuracao
	 */
	public int getQtdeDiasCobrancaTarifaAposApuracao() {
		return qtdeDiasCobrancaTarifaAposApuracao;
	}

	/**
	 * Set: qtdeDiasCobrancaTarifaAposApuracao.
	 *
	 * @param qtdeDiasCobrancaTarifaAposApuracao the qtde dias cobranca tarifa apos apuracao
	 */
	public void setQtdeDiasCobrancaTarifaAposApuracao(
			int qtdeDiasCobrancaTarifaAposApuracao) {
		this.qtdeDiasCobrancaTarifaAposApuracao = qtdeDiasCobrancaTarifaAposApuracao;
	}

	/**
	 * Get: qtdeDiasExpiracaoCredito.
	 *
	 * @return qtdeDiasExpiracaoCredito
	 */
	public int getQtdeDiasExpiracaoCredito() {
		return qtdeDiasExpiracaoCredito;
	}

	/**
	 * Set: qtdeDiasExpiracaoCredito.
	 *
	 * @param qtdeDiasExpiracaoCredito the qtde dias expiracao credito
	 */
	public void setQtdeDiasExpiracaoCredito(int qtdeDiasExpiracaoCredito) {
		this.qtdeDiasExpiracaoCredito = qtdeDiasExpiracaoCredito;
	}

	/**
	 * Get: qtdeDiasInativacaoFavorecido.
	 *
	 * @return qtdeDiasInativacaoFavorecido
	 */
	public int getQtdeDiasInativacaoFavorecido() {
		return qtdeDiasInativacaoFavorecido;
	}

	/**
	 * Set: qtdeDiasInativacaoFavorecido.
	 *
	 * @param qtdeDiasInativacaoFavorecido the qtde dias inativacao favorecido
	 */
	public void setQtdeDiasInativacaoFavorecido(int qtdeDiasInativacaoFavorecido) {
		this.qtdeDiasInativacaoFavorecido = qtdeDiasInativacaoFavorecido;
	}

	/**
	 * Get: qtdeEtapasRecadastramento.
	 *
	 * @return qtdeEtapasRecadastramento
	 */
	public int getQtdeEtapasRecadastramento() {
		return qtdeEtapasRecadastramento;
	}

	/**
	 * Set: qtdeEtapasRecadastramento.
	 *
	 * @param qtdeEtapasRecadastramento the qtde etapas recadastramento
	 */
	public void setQtdeEtapasRecadastramento(int qtdeEtapasRecadastramento) {
		this.qtdeEtapasRecadastramento = qtdeEtapasRecadastramento;
	}

	/**
	 * Get: qtdeFasesPorEtapa.
	 *
	 * @return qtdeFasesPorEtapa
	 */
	public int getQtdeFasesPorEtapa() {
		return qtdeFasesPorEtapa;
	}

	/**
	 * Set: qtdeFasesPorEtapa.
	 *
	 * @param qtdeFasesPorEtapa the qtde fases por etapa
	 */
	public void setQtdeFasesPorEtapa(int qtdeFasesPorEtapa) {
		this.qtdeFasesPorEtapa = qtdeFasesPorEtapa;
	}

	/**
	 * Get: qtdeLimiteSolicitacaoCartaoContaSalario.
	 *
	 * @return qtdeLimiteSolicitacaoCartaoContaSalario
	 */
	public int getQtdeLimiteSolicitacaoCartaoContaSalario() {
		return qtdeLimiteSolicitacaoCartaoContaSalario;
	}

	/**
	 * Set: qtdeLimiteSolicitacaoCartaoContaSalario.
	 *
	 * @param qtdeLimiteSolicitacaoCartaoContaSalario the qtde limite solicitacao cartao conta salario
	 */
	public void setQtdeLimiteSolicitacaoCartaoContaSalario(
			int qtdeLimiteSolicitacaoCartaoContaSalario) {
		this.qtdeLimiteSolicitacaoCartaoContaSalario = qtdeLimiteSolicitacaoCartaoContaSalario;
	}

	/**
	 * Get: qtdeLinhas.
	 *
	 * @return qtdeLinhas
	 */
	public int getQtdeLinhas() {
		return qtdeLinhas;
	}

	/**
	 * Set: qtdeLinhas.
	 *
	 * @param qtdeLinhas the qtde linhas
	 */
	public void setQtdeLinhas(int qtdeLinhas) {
		this.qtdeLinhas = qtdeLinhas;
	}

	/**
	 * Get: qtdeMaxInconsistenciaPorLote.
	 *
	 * @return qtdeMaxInconsistenciaPorLote
	 */
	public int getQtdeMaxInconsistenciaPorLote() {
		return qtdeMaxInconsistenciaPorLote;
	}

	/**
	 * Set: qtdeMaxInconsistenciaPorLote.
	 *
	 * @param qtdeMaxInconsistenciaPorLote the qtde max inconsistencia por lote
	 */
	public void setQtdeMaxInconsistenciaPorLote(int qtdeMaxInconsistenciaPorLote) {
		this.qtdeMaxInconsistenciaPorLote = qtdeMaxInconsistenciaPorLote;
	}

	/**
	 * Get: qtdeMesesEmissao.
	 *
	 * @return qtdeMesesEmissao
	 */
	public int getQtdeMesesEmissao() {
		return qtdeMesesEmissao;
	}

	/**
	 * Set: qtdeMesesEmissao.
	 *
	 * @param qtdeMesesEmissao the qtde meses emissao
	 */
	public void setQtdeMesesEmissao(int qtdeMesesEmissao) {
		this.qtdeMesesEmissao = qtdeMesesEmissao;
	}

	/**
	 * Get: qtdeMesesPrazoPorEtapa.
	 *
	 * @return qtdeMesesPrazoPorEtapa
	 */
	public int getQtdeMesesPrazoPorEtapa() {
		return qtdeMesesPrazoPorEtapa;
	}

	/**
	 * Set: qtdeMesesPrazoPorEtapa.
	 *
	 * @param qtdeMesesPrazoPorEtapa the qtde meses prazo por etapa
	 */
	public void setQtdeMesesPrazoPorEtapa(int qtdeMesesPrazoPorEtapa) {
		this.qtdeMesesPrazoPorEtapa = qtdeMesesPrazoPorEtapa;
	}

	/**
	 * Get: qtdeMesesPrazoPorFase.
	 *
	 * @return qtdeMesesPrazoPorFase
	 */
	public int getQtdeMesesPrazoPorFase() {
		return qtdeMesesPrazoPorFase;
	}

	/**
	 * Set: qtdeMesesPrazoPorFase.
	 *
	 * @param qtdeMesesPrazoPorFase the qtde meses prazo por fase
	 */
	public void setQtdeMesesPrazoPorFase(int qtdeMesesPrazoPorFase) {
		this.qtdeMesesPrazoPorFase = qtdeMesesPrazoPorFase;
	}

	/**
	 * Get: qtdeMesesReajusteAutomatico.
	 *
	 * @return qtdeMesesReajusteAutomatico
	 */
	public int getQtdeMesesReajusteAutomatico() {
		return qtdeMesesReajusteAutomatico;
	}

	/**
	 * Set: qtdeMesesReajusteAutomatico.
	 *
	 * @param qtdeMesesReajusteAutomatico the qtde meses reajuste automatico
	 */
	public void setQtdeMesesReajusteAutomatico(int qtdeMesesReajusteAutomatico) {
		this.qtdeMesesReajusteAutomatico = qtdeMesesReajusteAutomatico;
	}

	/**
	 * Get: qtdeMesesReajusteAutomaticoTarifa.
	 *
	 * @return qtdeMesesReajusteAutomaticoTarifa
	 */
	public int getQtdeMesesReajusteAutomaticoTarifa() {
		return qtdeMesesReajusteAutomaticoTarifa;
	}

	/**
	 * Set: qtdeMesesReajusteAutomaticoTarifa.
	 *
	 * @param qtdeMesesReajusteAutomaticoTarifa the qtde meses reajuste automatico tarifa
	 */
	public void setQtdeMesesReajusteAutomaticoTarifa(
			int qtdeMesesReajusteAutomaticoTarifa) {
		this.qtdeMesesReajusteAutomaticoTarifa = qtdeMesesReajusteAutomaticoTarifa;
	}

	/**
	 * Get: qtdeVias.
	 *
	 * @return qtdeVias
	 */
	public int getQtdeVias() {
		return qtdeVias;
	}

	/**
	 * Set: qtdeVias.
	 *
	 * @param qtdeVias the qtde vias
	 */
	public void setQtdeVias(int qtdeVias) {
		this.qtdeVias = qtdeVias;
	}

	/**
	 * Get: qtdeViasEmissaoComprovantes.
	 *
	 * @return qtdeViasEmissaoComprovantes
	 */
	public int getQtdeViasEmissaoComprovantes() {
		return qtdeViasEmissaoComprovantes;
	}

	/**
	 * Set: qtdeViasEmissaoComprovantes.
	 *
	 * @param qtdeViasEmissaoComprovantes the qtde vias emissao comprovantes
	 */
	public void setQtdeViasEmissaoComprovantes(int qtdeViasEmissaoComprovantes) {
		this.qtdeViasEmissaoComprovantes = qtdeViasEmissaoComprovantes;
	}

	/**
	 * Get: qtdeViasPagas.
	 *
	 * @return qtdeViasPagas
	 */
	public int getQtdeViasPagas() {
		return qtdeViasPagas;
	}

	/**
	 * Set: qtdeViasPagas.
	 *
	 * @param qtdeViasPagas the qtde vias pagas
	 */
	public void setQtdeViasPagas(int qtdeViasPagas) {
		this.qtdeViasPagas = qtdeViasPagas;
	}

	/**
	 * Get: qtDiaExpiracao.
	 *
	 * @return qtDiaExpiracao
	 */
	public Integer getQtDiaExpiracao() {
		return qtDiaExpiracao;
	}

	/**
	 * Set: qtDiaExpiracao.
	 *
	 * @param qtDiaExpiracao the qt dia expiracao
	 */
	public void setQtDiaExpiracao(Integer qtDiaExpiracao) {
		this.qtDiaExpiracao = qtDiaExpiracao;
	}

	/**
	 * Get: qtDiaInatividadeFavorecido.
	 *
	 * @return qtDiaInatividadeFavorecido
	 */
	public Integer getQtDiaInatividadeFavorecido() {
		return qtDiaInatividadeFavorecido;
	}

	/**
	 * Set: qtDiaInatividadeFavorecido.
	 *
	 * @param qtDiaInatividadeFavorecido the qt dia inatividade favorecido
	 */
	public void setQtDiaInatividadeFavorecido(Integer qtDiaInatividadeFavorecido) {
		this.qtDiaInatividadeFavorecido = qtDiaInatividadeFavorecido;
	}

	/**
	 * Get: qtDiaRepiqConsulta.
	 *
	 * @return qtDiaRepiqConsulta
	 */
	public Integer getQtDiaRepiqConsulta() {
		return qtDiaRepiqConsulta;
	}

	/**
	 * Set: qtDiaRepiqConsulta.
	 *
	 * @param qtDiaRepiqConsulta the qt dia repiq consulta
	 */
	public void setQtDiaRepiqConsulta(Integer qtDiaRepiqConsulta) {
		this.qtDiaRepiqConsulta = qtDiaRepiqConsulta;
	}

	/**
	 * Get: qtEtapasRecadastroBeneficio.
	 *
	 * @return qtEtapasRecadastroBeneficio
	 */
	public Integer getQtEtapasRecadastroBeneficio() {
		return qtEtapasRecadastroBeneficio;
	}

	/**
	 * Set: qtEtapasRecadastroBeneficio.
	 *
	 * @param qtEtapasRecadastroBeneficio the qt etapas recadastro beneficio
	 */
	public void setQtEtapasRecadastroBeneficio(Integer qtEtapasRecadastroBeneficio) {
		this.qtEtapasRecadastroBeneficio = qtEtapasRecadastroBeneficio;
	}

	/**
	 * Get: qtFaseRecadastroBeneficio.
	 *
	 * @return qtFaseRecadastroBeneficio
	 */
	public Integer getQtFaseRecadastroBeneficio() {
		return qtFaseRecadastroBeneficio;
	}

	/**
	 * Set: qtFaseRecadastroBeneficio.
	 *
	 * @param qtFaseRecadastroBeneficio the qt fase recadastro beneficio
	 */
	public void setQtFaseRecadastroBeneficio(Integer qtFaseRecadastroBeneficio) {
		this.qtFaseRecadastroBeneficio = qtFaseRecadastroBeneficio;
	}

	/**
	 * Get: qtLimiteLinha.
	 *
	 * @return qtLimiteLinha
	 */
	public Integer getQtLimiteLinha() {
		return qtLimiteLinha;
	}

	/**
	 * Set: qtLimiteLinha.
	 *
	 * @param qtLimiteLinha the qt limite linha
	 */
	public void setQtLimiteLinha(Integer qtLimiteLinha) {
		this.qtLimiteLinha = qtLimiteLinha;
	}

	/**
	 * Get: qtLimiteSolicitacaoCatao.
	 *
	 * @return qtLimiteSolicitacaoCatao
	 */
	public Integer getQtLimiteSolicitacaoCatao() {
		return qtLimiteSolicitacaoCatao;
	}

	/**
	 * Set: qtLimiteSolicitacaoCatao.
	 *
	 * @param qtLimiteSolicitacaoCatao the qt limite solicitacao catao
	 */
	public void setQtLimiteSolicitacaoCatao(Integer qtLimiteSolicitacaoCatao) {
		this.qtLimiteSolicitacaoCatao = qtLimiteSolicitacaoCatao;
	}

	/**
	 * Get: qtMaximaInconLote.
	 *
	 * @return qtMaximaInconLote
	 */
	public Integer getQtMaximaInconLote() {
		return qtMaximaInconLote;
	}

	/**
	 * Set: qtMaximaInconLote.
	 *
	 * @param qtMaximaInconLote the qt maxima incon lote
	 */
	public void setQtMaximaInconLote(Integer qtMaximaInconLote) {
		this.qtMaximaInconLote = qtMaximaInconLote;
	}

	/**
	 * Get: qtMaximaTituloVencido.
	 *
	 * @return qtMaximaTituloVencido
	 */
	public Integer getQtMaximaTituloVencido() {
		return qtMaximaTituloVencido;
	}

	/**
	 * Set: qtMaximaTituloVencido.
	 *
	 * @param qtMaximaTituloVencido the qt maxima titulo vencido
	 */
	public void setQtMaximaTituloVencido(Integer qtMaximaTituloVencido) {
		this.qtMaximaTituloVencido = qtMaximaTituloVencido;
	}

	/**
	 * Get: qtMesComprovante.
	 *
	 * @return qtMesComprovante
	 */
	public Integer getQtMesComprovante() {
		return qtMesComprovante;
	}

	/**
	 * Set: qtMesComprovante.
	 *
	 * @param qtMesComprovante the qt mes comprovante
	 */
	public void setQtMesComprovante(Integer qtMesComprovante) {
		this.qtMesComprovante = qtMesComprovante;
	}

	/**
	 * Get: qtMesEtapaRecadastro.
	 *
	 * @return qtMesEtapaRecadastro
	 */
	public Integer getQtMesEtapaRecadastro() {
		return qtMesEtapaRecadastro;
	}

	/**
	 * Set: qtMesEtapaRecadastro.
	 *
	 * @param qtMesEtapaRecadastro the qt mes etapa recadastro
	 */
	public void setQtMesEtapaRecadastro(Integer qtMesEtapaRecadastro) {
		this.qtMesEtapaRecadastro = qtMesEtapaRecadastro;
	}

	/**
	 * Get: qtMesFaseRecadastro.
	 *
	 * @return qtMesFaseRecadastro
	 */
	public Integer getQtMesFaseRecadastro() {
		return qtMesFaseRecadastro;
	}

	/**
	 * Set: qtMesFaseRecadastro.
	 *
	 * @param qtMesFaseRecadastro the qt mes fase recadastro
	 */
	public void setQtMesFaseRecadastro(Integer qtMesFaseRecadastro) {
		this.qtMesFaseRecadastro = qtMesFaseRecadastro;
	}

	/**
	 * Get: qtViaAviso.
	 *
	 * @return qtViaAviso
	 */
	public Integer getQtViaAviso() {
		return qtViaAviso;
	}

	/**
	 * Set: qtViaAviso.
	 *
	 * @param qtViaAviso the qt via aviso
	 */
	public void setQtViaAviso(Integer qtViaAviso) {
		this.qtViaAviso = qtViaAviso;
	}

	/**
	 * Get: qtViaCobranca.
	 *
	 * @return qtViaCobranca
	 */
	public Integer getQtViaCobranca() {
		return qtViaCobranca;
	}

	/**
	 * Set: qtViaCobranca.
	 *
	 * @param qtViaCobranca the qt via cobranca
	 */
	public void setQtViaCobranca(Integer qtViaCobranca) {
		this.qtViaCobranca = qtViaCobranca;
	}

	/**
	 * Get: qtViaComprovante.
	 *
	 * @return qtViaComprovante
	 */
	public Integer getQtViaComprovante() {
		return qtViaComprovante;
	}

	/**
	 * Set: qtViaComprovante.
	 *
	 * @param qtViaComprovante the qt via comprovante
	 */
	public void setQtViaComprovante(Integer qtViaComprovante) {
		this.qtViaComprovante = qtViaComprovante;
	}

	/**
	 * Get: quantDiasRepique.
	 *
	 * @return quantDiasRepique
	 */
	public int getQuantDiasRepique() {
		return quantDiasRepique;
	}

	/**
	 * Set: quantDiasRepique.
	 *
	 * @param quantDiasRepique the quant dias repique
	 */
	public void setQuantDiasRepique(int quantDiasRepique) {
		this.quantDiasRepique = quantDiasRepique;
	}

	/**
	 * Get: rdoAgendarTitRastProp.
	 *
	 * @return rdoAgendarTitRastProp
	 */
	public int getRdoAgendarTitRastProp() {
		return rdoAgendarTitRastProp;
	}

	/**
	 * Set: rdoAgendarTitRastProp.
	 *
	 * @param rdoAgendarTitRastProp the rdo agendar tit rast prop
	 */
	public void setRdoAgendarTitRastProp(int rdoAgendarTitRastProp) {
		this.rdoAgendarTitRastProp = rdoAgendarTitRastProp;
	}

	/**
	 * Get: rdoBloquearEmissaoPap.
	 *
	 * @return rdoBloquearEmissaoPap
	 */
	public int getRdoBloquearEmissaoPap() {
		return rdoBloquearEmissaoPap;
	}

	/**
	 * Set: rdoBloquearEmissaoPap.
	 *
	 * @param rdoBloquearEmissaoPap the rdo bloquear emissao pap
	 */
	public void setRdoBloquearEmissaoPap(int rdoBloquearEmissaoPap) {
		this.rdoBloquearEmissaoPap = rdoBloquearEmissaoPap;
	}

	/**
	 * Get: rdoCapturarTitDtaRegistro.
	 *
	 * @return rdoCapturarTitDtaRegistro
	 */
	public int getRdoCapturarTitDtaRegistro() {
		return rdoCapturarTitDtaRegistro;
	}

	/**
	 * Set: rdoCapturarTitDtaRegistro.
	 *
	 * @param rdoCapturarTitDtaRegistro the rdo capturar tit dta registro
	 */
	public void setRdoCapturarTitDtaRegistro(int rdoCapturarTitDtaRegistro) {
		this.rdoCapturarTitDtaRegistro = rdoCapturarTitDtaRegistro;
	}

	/**
	 * Get: rdoGerarLanctoFuturoCred.
	 *
	 * @return rdoGerarLanctoFuturoCred
	 */
	public int getRdoGerarLanctoFuturoCred() {
		return rdoGerarLanctoFuturoCred;
	}

	/**
	 * Set: rdoGerarLanctoFuturoCred.
	 *
	 * @param rdoGerarLanctoFuturoCred the rdo gerar lancto futuro cred
	 */
	public void setRdoGerarLanctoFuturoCred(int rdoGerarLanctoFuturoCred) {
		this.rdoGerarLanctoFuturoCred = rdoGerarLanctoFuturoCred;
	}

	/**
	 * Get: rdoGerarLanctoFuturoDeb.
	 *
	 * @return rdoGerarLanctoFuturoDeb
	 */
	public int getRdoGerarLanctoFuturoDeb() {
		return rdoGerarLanctoFuturoDeb;
	}

	/**
	 * Set: rdoGerarLanctoFuturoDeb.
	 *
	 * @param rdoGerarLanctoFuturoDeb the rdo gerar lancto futuro deb
	 */
	public void setRdoGerarLanctoFuturoDeb(int rdoGerarLanctoFuturoDeb) {
		this.rdoGerarLanctoFuturoDeb = rdoGerarLanctoFuturoDeb;
	}

	/**
	 * Get: rdoGerarLanctoProgramado.
	 *
	 * @return rdoGerarLanctoProgramado
	 */
	public int getRdoGerarLanctoProgramado() {
		return rdoGerarLanctoProgramado;
	}

	/**
	 * Set: rdoGerarLanctoProgramado.
	 *
	 * @param rdoGerarLanctoProgramado the rdo gerar lancto programado
	 */
	public void setRdoGerarLanctoProgramado(int rdoGerarLanctoProgramado) {
		this.rdoGerarLanctoProgramado = rdoGerarLanctoProgramado;
	}

	/**
	 * Get: rdoOcorrenciaDebito.
	 *
	 * @return rdoOcorrenciaDebito
	 */
	public int getRdoOcorrenciaDebito() {
		return rdoOcorrenciaDebito;
	}

	/**
	 * Set: rdoOcorrenciaDebito.
	 *
	 * @param rdoOcorrenciaDebito the rdo ocorrencia debito
	 */
	public void setRdoOcorrenciaDebito(int rdoOcorrenciaDebito) {
		this.rdoOcorrenciaDebito = rdoOcorrenciaDebito;
	}

	/**
	 * Get: rdoPermiteDebitoOnline.
	 *
	 * @return rdoPermiteDebitoOnline
	 */
	public int getRdoPermiteDebitoOnline() {
		return rdoPermiteDebitoOnline;
	}

	/**
	 * Set: rdoPermiteDebitoOnline.
	 *
	 * @param rdoPermiteDebitoOnline the rdo permite debito online
	 */
	public void setRdoPermiteDebitoOnline(int rdoPermiteDebitoOnline) {
		this.rdoPermiteDebitoOnline = rdoPermiteDebitoOnline;
	}

	/**
	 * Get: rdoPermiteFavConsultarPag.
	 *
	 * @return rdoPermiteFavConsultarPag
	 */
	public int getRdoPermiteFavConsultarPag() {
		return rdoPermiteFavConsultarPag;
	}

	/**
	 * Set: rdoPermiteFavConsultarPag.
	 *
	 * @param rdoPermiteFavConsultarPag the rdo permite fav consultar pag
	 */
	public void setRdoPermiteFavConsultarPag(int rdoPermiteFavConsultarPag) {
		this.rdoPermiteFavConsultarPag = rdoPermiteFavConsultarPag;
	}

	/**
	 * Get: rdoPermiteOutrosTiposIncricaoFav.
	 *
	 * @return rdoPermiteOutrosTiposIncricaoFav
	 */
	public int getRdoPermiteOutrosTiposIncricaoFav() {
		return rdoPermiteOutrosTiposIncricaoFav;
	}

	/**
	 * Set: rdoPermiteOutrosTiposIncricaoFav.
	 *
	 * @param rdoPermiteOutrosTiposIncricaoFav the rdo permite outros tipos incricao fav
	 */
	public void setRdoPermiteOutrosTiposIncricaoFav(
			int rdoPermiteOutrosTiposIncricaoFav) {
		this.rdoPermiteOutrosTiposIncricaoFav = rdoPermiteOutrosTiposIncricaoFav;
	}

	/**
	 * Get: rdoPermitePagarMenor.
	 *
	 * @return rdoPermitePagarMenor
	 */
	public int getRdoPermitePagarMenor() {
		return rdoPermitePagarMenor;
	}

	/**
	 * Set: rdoPermitePagarMenor.
	 *
	 * @param rdoPermitePagarMenor the rdo permite pagar menor
	 */
	public void setRdoPermitePagarMenor(int rdoPermitePagarMenor) {
		this.rdoPermitePagarMenor = rdoPermitePagarMenor;
	}

	/**
	 * Get: rdoPermitePagarVencido.
	 *
	 * @return rdoPermitePagarVencido
	 */
	public int getRdoPermitePagarVencido() {
		return rdoPermitePagarVencido;
	}

	/**
	 * Set: rdoPermitePagarVencido.
	 *
	 * @param rdoPermitePagarVencido the rdo permite pagar vencido
	 */
	public void setRdoPermitePagarVencido(int rdoPermitePagarVencido) {
		this.rdoPermitePagarVencido = rdoPermitePagarVencido;
	}

	/**
	 * Get: rdoRastrearNotasFiscais.
	 *
	 * @return rdoRastrearNotasFiscais
	 */
	public int getRdoRastrearNotasFiscais() {
		return rdoRastrearNotasFiscais;
	}

	/**
	 * Set: rdoRastrearNotasFiscais.
	 *
	 * @param rdoRastrearNotasFiscais the rdo rastrear notas fiscais
	 */
	public void setRdoRastrearNotasFiscais(int rdoRastrearNotasFiscais) {
		this.rdoRastrearNotasFiscais = rdoRastrearNotasFiscais;
	}

	/**
	 * Get: rdoRastrearTitRastreadoFilial.
	 *
	 * @return rdoRastrearTitRastreadoFilial
	 */
	public int getRdoRastrearTitRastreadoFilial() {
		return rdoRastrearTitRastreadoFilial;
	}

	/**
	 * Set: rdoRastrearTitRastreadoFilial.
	 *
	 * @param rdoRastrearTitRastreadoFilial the rdo rastrear tit rastreado filial
	 */
	public void setRdoRastrearTitRastreadoFilial(int rdoRastrearTitRastreadoFilial) {
		this.rdoRastrearTitRastreadoFilial = rdoRastrearTitRastreadoFilial;
	}

	/**
	 * Get: rdoRastrearTitTerceiros.
	 *
	 * @return rdoRastrearTitTerceiros
	 */
	public int getRdoRastrearTitTerceiros() {
		return rdoRastrearTitTerceiros;
	}

	/**
	 * Set: rdoRastrearTitTerceiros.
	 *
	 * @param rdoRastrearTitTerceiros the rdo rastrear tit terceiros
	 */
	public void setRdoRastrearTitTerceiros(int rdoRastrearTitTerceiros) {
		this.rdoRastrearTitTerceiros = rdoRastrearTitTerceiros;
	}

	/**
	 * Get: rdoUtilizaCadFavControlePag.
	 *
	 * @return rdoUtilizaCadFavControlePag
	 */
	public int getRdoUtilizaCadFavControlePag() {
		return rdoUtilizaCadFavControlePag;
	}

	/**
	 * Set: rdoUtilizaCadFavControlePag.
	 *
	 * @param rdoUtilizaCadFavControlePag the rdo utiliza cad fav controle pag
	 */
	public void setRdoUtilizaCadFavControlePag(int rdoUtilizaCadFavControlePag) {
		this.rdoUtilizaCadFavControlePag = rdoUtilizaCadFavControlePag;
	}

	/**
	 * Get: valorLimiteDiario.
	 *
	 * @return valorLimiteDiario
	 */
	public BigDecimal getValorLimiteDiario() {
		return valorLimiteDiario;
	}

	/**
	 * Set: valorLimiteDiario.
	 *
	 * @param valorLimiteDiario the valor limite diario
	 */
	public void setValorLimiteDiario(BigDecimal valorLimiteDiario) {
		this.valorLimiteDiario = valorLimiteDiario;
	}

	/**
	 * Get: valorLimiteIndividual.
	 *
	 * @return valorLimiteIndividual
	 */
	public BigDecimal getValorLimiteIndividual() {
		return valorLimiteIndividual;
	}

	/**
	 * Set: valorLimiteIndividual.
	 *
	 * @param valorLimiteIndividual the valor limite individual
	 */
	public void setValorLimiteIndividual(BigDecimal valorLimiteIndividual) {
		this.valorLimiteIndividual = valorLimiteIndividual;
	}

	/**
	 * Get: valorMaximoPagamentoFavorecidoNaoCadastrado.
	 *
	 * @return valorMaximoPagamentoFavorecidoNaoCadastrado
	 */
	public BigDecimal getValorMaximoPagamentoFavorecidoNaoCadastrado() {
		return valorMaximoPagamentoFavorecidoNaoCadastrado;
	}

	/**
	 * Set: valorMaximoPagamentoFavorecidoNaoCadastrado.
	 *
	 * @param valorMaximoPagamentoFavorecidoNaoCadastrado the valor maximo pagamento favorecido nao cadastrado
	 */
	public void setValorMaximoPagamentoFavorecidoNaoCadastrado(
			BigDecimal valorMaximoPagamentoFavorecidoNaoCadastrado) {
		this.valorMaximoPagamentoFavorecidoNaoCadastrado = valorMaximoPagamentoFavorecidoNaoCadastrado;
	}

	/**
	 * Get: valorMaxPagFavNaoCadastrado.
	 *
	 * @return valorMaxPagFavNaoCadastrado
	 */
	public BigDecimal getValorMaxPagFavNaoCadastrado() {
		return valorMaxPagFavNaoCadastrado;
	}

	/**
	 * Set: valorMaxPagFavNaoCadastrado.
	 *
	 * @param valorMaxPagFavNaoCadastrado the valor max pag fav nao cadastrado
	 */
	public void setValorMaxPagFavNaoCadastrado(
			BigDecimal valorMaxPagFavNaoCadastrado) {
		this.valorMaxPagFavNaoCadastrado = valorMaxPagFavNaoCadastrado;
	}

	/**
	 * Get: vlFavorecidoNaoCadastro.
	 *
	 * @return vlFavorecidoNaoCadastro
	 */
	public BigDecimal getVlFavorecidoNaoCadastro() {
		return vlFavorecidoNaoCadastro;
	}

	/**
	 * Set: vlFavorecidoNaoCadastro.
	 *
	 * @param vlFavorecidoNaoCadastro the vl favorecido nao cadastro
	 */
	public void setVlFavorecidoNaoCadastro(BigDecimal vlFavorecidoNaoCadastro) {
		this.vlFavorecidoNaoCadastro = vlFavorecidoNaoCadastro;
	}

	/**
	 * Get: vlLimiteDiaPagamento.
	 *
	 * @return vlLimiteDiaPagamento
	 */
	public BigDecimal getVlLimiteDiaPagamento() {
		return vlLimiteDiaPagamento;
	}

	/**
	 * Get: cdAbertContaBancoPostBradSeg.
	 *
	 * @return cdAbertContaBancoPostBradSeg
	 */
	public int getCdAbertContaBancoPostBradSeg() {
		return cdAbertContaBancoPostBradSeg;
	}

	/**
	 * Set: cdAbertContaBancoPostBradSeg.
	 *
	 * @param cdAbertContaBancoPostBradSeg the cd abert conta banco post brad seg
	 */
	public void setCdAbertContaBancoPostBradSeg(int cdAbertContaBancoPostBradSeg) {
		this.cdAbertContaBancoPostBradSeg = cdAbertContaBancoPostBradSeg;
	}

	/**
	 * Get: cdEmissaoAntCartaoContSal.
	 *
	 * @return cdEmissaoAntCartaoContSal
	 */
	public int getCdEmissaoAntCartaoContSal() {
		return cdEmissaoAntCartaoContSal;
	}

	/**
	 * Set: cdEmissaoAntCartaoContSal.
	 *
	 * @param cdEmissaoAntCartaoContSal the cd emissao ant cartao cont sal
	 */
	public void setCdEmissaoAntCartaoContSal(int cdEmissaoAntCartaoContSal) {
		this.cdEmissaoAntCartaoContSal = cdEmissaoAntCartaoContSal;
	}

	/**
	 * Get: cdGerarRetornoOperRealizadaInternet.
	 *
	 * @return cdGerarRetornoOperRealizadaInternet
	 */
	public int getCdGerarRetornoOperRealizadaInternet() {
		return cdGerarRetornoOperRealizadaInternet;
	}

	/**
	 * Set: cdGerarRetornoOperRealizadaInternet.
	 *
	 * @param cdGerarRetornoOperRealizadaInternet the cd gerar retorno oper realizada internet
	 */
	public void setCdGerarRetornoOperRealizadaInternet(
			int cdGerarRetornoOperRealizadaInternet) {
		this.cdGerarRetornoOperRealizadaInternet = cdGerarRetornoOperRealizadaInternet;
	}

	/**
	 * Get: cdTipoCartaoContaSalario.
	 *
	 * @return cdTipoCartaoContaSalario
	 */
	public int getCdTipoCartaoContaSalario() {
		return cdTipoCartaoContaSalario;
	}

	/**
	 * Set: cdTipoCartaoContaSalario.
	 *
	 * @param cdTipoCartaoContaSalario the cd tipo cartao conta salario
	 */
	public void setCdTipoCartaoContaSalario(int cdTipoCartaoContaSalario) {
		this.cdTipoCartaoContaSalario = cdTipoCartaoContaSalario;
	}

	/**
	 * Get: cdTipoConsolidacaoPagamentosComprovante.
	 *
	 * @return cdTipoConsolidacaoPagamentosComprovante
	 */
	public int getCdTipoConsolidacaoPagamentosComprovante() {
		return cdTipoConsolidacaoPagamentosComprovante;
	}

	/**
	 * Set: cdTipoConsolidacaoPagamentosComprovante.
	 *
	 * @param cdTipoConsolidacaoPagamentosComprovante the cd tipo consolidacao pagamentos comprovante
	 */
	public void setCdTipoConsolidacaoPagamentosComprovante(
			int cdTipoConsolidacaoPagamentosComprovante) {
		this.cdTipoConsolidacaoPagamentosComprovante = cdTipoConsolidacaoPagamentosComprovante;
	}

	/**
	 * Get: limiteEnqConvContaSal.
	 *
	 * @return limiteEnqConvContaSal
	 */
	public int getLimiteEnqConvContaSal() {
		return limiteEnqConvContaSal;
	}

	/**
	 * Set: limiteEnqConvContaSal.
	 *
	 * @param limiteEnqConvContaSal the limite enq conv conta sal
	 */
	public void setLimiteEnqConvContaSal(int limiteEnqConvContaSal) {
		this.limiteEnqConvContaSal = limiteEnqConvContaSal;
	}

	/**
	 * Get: qtddLimiteCartaoContaSalarioSolicitacao.
	 *
	 * @return qtddLimiteCartaoContaSalarioSolicitacao
	 */
	public int getQtddLimiteCartaoContaSalarioSolicitacao() {
		return qtddLimiteCartaoContaSalarioSolicitacao;
	}

	/**
	 * Set: qtddLimiteCartaoContaSalarioSolicitacao.
	 *
	 * @param qtddLimiteCartaoContaSalarioSolicitacao the qtdd limite cartao conta salario solicitacao
	 */
	public void setQtddLimiteCartaoContaSalarioSolicitacao(
			int qtddLimiteCartaoContaSalarioSolicitacao) {
		this.qtddLimiteCartaoContaSalarioSolicitacao = qtddLimiteCartaoContaSalarioSolicitacao;
	}

	/**
	 * Set: vlLimiteDiaPagamento.
	 *
	 * @param vlLimiteDiaPagamento the vl limite dia pagamento
	 */
	public void setVlLimiteDiaPagamento(BigDecimal vlLimiteDiaPagamento) {
		this.vlLimiteDiaPagamento = vlLimiteDiaPagamento;
	}

	/**
	 * Get: vlLimiteIndividualPagamento.
	 *
	 * @return vlLimiteIndividualPagamento
	 */
	public BigDecimal getVlLimiteIndividualPagamento() {
		return vlLimiteIndividualPagamento;
	}

	/**
	 * Set: vlLimiteIndividualPagamento.
	 *
	 * @param vlLimiteIndividualPagamento the vl limite individual pagamento
	 */
	public void setVlLimiteIndividualPagamento(
			BigDecimal vlLimiteIndividualPagamento) {
		this.vlLimiteIndividualPagamento = vlLimiteIndividualPagamento;
	}
	/**
	 * Set: rdoPermiteAcertosDados.
	 *
	 * @param rdoPermiteAcertosDados the rdo permite acertos dados
	 */
	public void setRdoPermiteAcertosDados(int rdoPermiteAcertosDados) {
		this.rdoPermiteAcertosDados = rdoPermiteAcertosDados;
	}

	/**
	 * Get: rdoPermiteAcertosDados.
	 *
	 * @return rdoPermiteAcertosDados
	 */
	public Integer getRdoPermiteAcertosDados() {
		return rdoPermiteAcertosDados;
	}

	/**
	 * Set: rdoPermiteAcertosDados.
	 *
	 * @param rdoPermiteAcertosDados the rdo permite acertos dados
	 */
	public void setRdoPermiteAcertosDados(Integer rdoPermiteAcertosDados) {
		this.rdoPermiteAcertosDados = rdoPermiteAcertosDados;
	}

	/**
	 * Get: cdIndicadorManutencaoProcurador.
	 *
	 * @return cdIndicadorManutencaoProcurador
	 */
	public Integer getCdIndicadorManutencaoProcurador() {
		return cdIndicadorManutencaoProcurador;
	}

	/**
	 * Set: cdIndicadorManutencaoProcurador.
	 *
	 * @param cdIndicadorManutencaoProcurador the cd indicador manutencao procurador
	 */
	public void setCdIndicadorManutencaoProcurador(Integer cdIndicadorManutencaoProcurador) {
		this.cdIndicadorManutencaoProcurador = cdIndicadorManutencaoProcurador;
	}

	/**
	 * Get: cdIndicadorSeparaCanal.
	 *
	 * @return cdIndicadorSeparaCanal
	 */
	public Integer getCdIndicadorSeparaCanal() {
		return cdIndicadorSeparaCanal;
	}

	/**
	 * Set: cdIndicadorSeparaCanal.
	 *
	 * @param cdIndicadorSeparaCanal the cd indicador separa canal
	 */
	public void setCdIndicadorSeparaCanal(Integer cdIndicadorSeparaCanal) {
		this.cdIndicadorSeparaCanal = cdIndicadorSeparaCanal;
	}

	/**
	 * Get: cdLocalEmissao.
	 *
	 * @return cdLocalEmissao
	 */
	public Integer getCdLocalEmissao() {
		return cdLocalEmissao;
	}

	/**
	 * Set: cdLocalEmissao.
	 *
	 * @param cdLocalEmissao the cd local emissao
	 */
	public void setCdLocalEmissao(Integer cdLocalEmissao) {
		this.cdLocalEmissao = cdLocalEmissao;
	}

	/**
	 * Get: cdConsultaSaldoValorSuperior.
	 *
	 * @return cdConsultaSaldoValorSuperior
	 */
	public Integer getCdConsultaSaldoValorSuperior() {
		return cdConsultaSaldoValorSuperior;
	}

	/**
	 * Set: cdConsultaSaldoValorSuperior.
	 *
	 * @param cdConsultaSaldoValorSuperior the cd consulta saldo valor superior
	 */
	public void setCdConsultaSaldoValorSuperior(Integer cdConsultaSaldoValorSuperior) {
		this.cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
	}

	/**
	 * Get: cdIndicadorFeriadoLocal.
	 *
	 * @return cdIndicadorFeriadoLocal
	 */
	public Integer getCdIndicadorFeriadoLocal() {
		return cdIndicadorFeriadoLocal;
	}

	/**
	 * Set: cdIndicadorFeriadoLocal.
	 *
	 * @param cdIndicadorFeriadoLocal the cd indicador feriado local
	 */
	public void setCdIndicadorFeriadoLocal(Integer cdIndicadorFeriadoLocal) {
		this.cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
	}

	/**
	 * Set: cdIndicadorSegundaLinha.
	 *
	 * @param cdIndicadorSegundaLinha the cd indicador segunda linha
	 */
	public void setCdIndicadorSegundaLinha(Integer cdIndicadorSegundaLinha) {
		this.cdIndicadorSegundaLinha = cdIndicadorSegundaLinha;
	}

	/**
	 * Get: cdIndicadorSegundaLinha.
	 *
	 * @return cdIndicadorSegundaLinha
	 */
	public Integer getCdIndicadorSegundaLinha() {
		return cdIndicadorSegundaLinha;
	}

	public Integer getCdFloatServicoContrato() {
		return cdFloatServicoContrato;
	}

	public void setCdFloatServicoContrato(Integer cdFloatServicoContrato) {
		this.cdFloatServicoContrato = cdFloatServicoContrato;
	}

	/**
	 * @param cdExigeAutFilial the cdExigeAutFilial to set
	 */
	public void setCdExigeAutFilial(Integer cdExigeAutFilial) {
		this.cdExigeAutFilial = cdExigeAutFilial;
	}

	/**
	 * @return the cdExigeAutFilial
	 */
	public Integer getCdExigeAutFilial() {
		return cdExigeAutFilial;
	}

    /**
     * Nome: getCdPreenchimentoLancamentoPersonalizado
     *
     * @return cdPreenchimentoLancamentoPersonalizado
     */
    public Integer getCdPreenchimentoLancamentoPersonalizado() {
        return cdPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: setCdPreenchimentoLancamentoPersonalizado
     *
     * @param cdPreenchimentoLancamentoPersonalizado
     */
    public void setCdPreenchimentoLancamentoPersonalizado(Integer cdPreenchimentoLancamentoPersonalizado) {
        this.cdPreenchimentoLancamentoPersonalizado = cdPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: getCdIndicadorAgendaGrade
     *
     * @return cdIndicadorAgendaGrade
     */
    public Integer getCdIndicadorAgendaGrade() {
        return cdIndicadorAgendaGrade;
    }

    /**
     * Nome: setCdIndicadorAgendaGrade
     *
     * @param cdIndicadorAgendaGrade
     */
    public void setCdIndicadorAgendaGrade(Integer cdIndicadorAgendaGrade) {
        this.cdIndicadorAgendaGrade = cdIndicadorAgendaGrade;
    }

    /**
     * Nome: getCdTituloDdaRetorno
     *
     * @return cdTituloDdaRetorno
     */
    public Integer getCdTituloDdaRetorno() {
        return cdTituloDdaRetorno;
    }

    /**
     * Nome: setCdTituloDdaRetorno
     *
     * @param cdTituloDdaRetorno
     */
    public void setCdTituloDdaRetorno(Integer cdTituloDdaRetorno) {
        this.cdTituloDdaRetorno = cdTituloDdaRetorno;
    }

	/**
	 * @param qtDiaUtilPgto the qtDiaUtilPgto to set
	 */
	public void setQtDiaUtilPgto(Integer qtDiaUtilPgto) {
		this.qtDiaUtilPgto = qtDiaUtilPgto;
	}

	/**
	 * @return the qtDiaUtilPgto
	 */
	public Integer getQtDiaUtilPgto() {
		return qtDiaUtilPgto;
	}

	/**
     * Get: cdIndicadorUtilizaMora.
     * 
     * @return cdIndicadorUtilizaMora
     */
	public Integer getCdIndicadorUtilizaMora() {
		return cdIndicadorUtilizaMora;
	}

	/**
     * Set: cdIndicadorUtilizaMora.
     * 
     * @param cdIndicadorUtilizaMora
     *            the cd indicador utiliza mora
     */
	public void setCdIndicadorUtilizaMora(Integer cdIndicadorUtilizaMora) {
		this.cdIndicadorUtilizaMora = cdIndicadorUtilizaMora;
	}

    /**
     * Nome: getVlPercentualDiferencaTolerada
     *
     * @return vlPercentualDiferencaTolerada
     */
    public BigDecimal getVlPercentualDiferencaTolerada() {
        return vlPercentualDiferencaTolerada;
    }

    /**
     * Nome: setVlPercentualDiferencaTolerada
     *
     * @param vlPercentualDiferencaTolerada
     */
    public void setVlPercentualDiferencaTolerada(BigDecimal vlPercentualDiferencaTolerada) {
        this.vlPercentualDiferencaTolerada = vlPercentualDiferencaTolerada;
    }

	public Integer getCdConsistenciaCpfCnpjBenefAvalNpc() {
		return cdConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setCdConsistenciaCpfCnpjBenefAvalNpc(
			Integer cdConsistenciaCpfCnpjBenefAvalNpc) {
		this.cdConsistenciaCpfCnpjBenefAvalNpc = cdConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setCindcdFantsRepas(int cindcdFantsRepas) {
		this.cindcdFantsRepas = cindcdFantsRepas;
	}

	public int getCindcdFantsRepas() {
		return cindcdFantsRepas;
	}

	public Integer getCdIndicadorTipoRetornoInternet() {
		return cdIndicadorTipoRetornoInternet;
	}

	public void setCdIndicadorTipoRetornoInternet(
			Integer cdIndicadorTipoRetornoInternet) {
		this.cdIndicadorTipoRetornoInternet = cdIndicadorTipoRetornoInternet;
	}		
    
    
}