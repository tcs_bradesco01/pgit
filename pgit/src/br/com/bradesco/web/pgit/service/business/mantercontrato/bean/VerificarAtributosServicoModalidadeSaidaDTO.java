/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.response.VerificarAtributosServicoModalidadeResponse;

/**
 * Nome: VerificarAtributosServicoModalidadeSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class VerificarAtributosServicoModalidadeSaidaDTO {
	
	/** Atributo cdIndicadorAcaoNaoVida. */
	private String cdIndicadorAcaoNaoVida = null;

	/** Atributo cdIndicadorAcertoDadoRecadastro. */
	private String cdIndicadorAcertoDadoRecadastro = null;

	/** Atributo cdIndicadorAgendaDebitoVeiculo. */
	private String cdIndicadorAgendaDebitoVeiculo = null;

	/** Atributo cdIndicadorAgendaPagamentoVencido. */
	private String cdIndicadorAgendaPagamentoVencido = null;

	/** Atributo cdIndicadorAgendaValorMenor. */
	private String cdIndicadorAgendaValorMenor = null;

	/** Atributo cdIndicadorAgrupamentoAviso. */
	private String cdIndicadorAgrupamentoAviso = null;

	/** Atributo cdIndicadorAgrupamentoComprovado. */
	private String cdIndicadorAgrupamentoComprovado = null;

	/** Atributo cdIndicadorAgrupamentoFormularioRecadastro. */
	private String cdIndicadorAgrupamentoFormularioRecadastro = null;

	/** Atributo cdIndicadorAntecRecadastroBeneficio. */
	private String cdIndicadorAntecRecadastroBeneficio = null;

	/** Atributo cdIndicadorAreaReservada. */
	private String cdIndicadorAreaReservada = null;

	/** Atributo cdIndicadorBaseRecadastroBeneficio. */
	private String cdIndicadorBaseRecadastroBeneficio = null;

	/** Atributo cdIndicadorBloqueioEmissaoPplta. */
	private String cdIndicadorBloqueioEmissaoPplta = null;

	/** Atributo cdIndicadorCapituloTituloRegistro. */
	private String cdIndicadorCapituloTituloRegistro = null;

	/** Atributo cdIndicadorCobrancaTarifa. */
	private String cdIndicadorCobrancaTarifa = null;

	/** Atributo cdIndicadorConsDebitoVeiculo. */
	private String cdIndicadorConsDebitoVeiculo = null;

	/** Atributo cdIndicadorConsEndereco. */
	private String cdIndicadorConsEndereco = null;

	/** Atributo cdIndicadorConsSaldoPagamento. */
	private String cdIndicadorConsSaldoPagamento = null;

	/** Atributo cdIndicadorContagemConsSaudo. */
	private String cdIndicadorContagemConsSaudo = null;

	/** Atributo cdIndicadorCreditoNaoUtilizado. */
	private String cdIndicadorCreditoNaoUtilizado = null;

	/** Atributo cdIndicadorCriterioEnquaBeneficio. */
	private String cdIndicadorCriterioEnquaBeneficio = null;

	/** Atributo cdIndicadorCriterioEnqua. */
	private String cdIndicadorCriterioEnqua = null;

	/** Atributo cdIndicadorCriterioRastreabilidadeTitulo. */
	private String cdIndicadorCriterioRastreabilidadeTitulo = null;

	/** Atributo cdIndicadorCtciaEspecieBeneficio. */
	private String cdIndicadorCtciaEspecieBeneficio = null;

	/** Atributo cdIndicadorCtciaIdentificacaoBeneficio. */
	private String cdIndicadorCtciaIdentificacaoBeneficio = null;

	/** Atributo cdIndicadorCtciaInscricaoFavorecido. */
	private String cdIndicadorCtciaInscricaoFavorecido = null;

	/** Atributo cdIndicadorCtciaProprietarioVeiculo. */
	private String cdIndicadorCtciaProprietarioVeiculo = null;

	/** Atributo cdIndicadorDispzContaCredito. */
	private String cdIndicadorDispzContaCredito = null;

	/** Atributo cdIndicadorDispzDiversar. */
	private String cdIndicadorDispzDiversar = null;

	/** Atributo cdIndicadorDispzDiversasNao. */
	private String cdIndicadorDispzDiversasNao = null;

	/** Atributo cdIndicadorDispzSalario. */
	private String cdIndicadorDispzSalario = null;

	/** Atributo cdIndicadorDispzSalarioNao. */
	private String cdIndicadorDispzSalarioNao = null;

	/** Atributo cdIndicadorDestinoAviso. */
	private String cdIndicadorDestinoAviso = null;

	/** Atributo cdIndicadorDestinoComprovante. */
	private String cdIndicadorDestinoComprovante = null;

	/** Atributo cdIndicadorDestinoFormularioRecadastro. */
	private String cdIndicadorDestinoFormularioRecadastro = null;

	/** Atributo cdIndicadorEnvelopeAberto. */
	private String cdIndicadorEnvelopeAberto = null;

	/** Atributo cdIndicadorFavorecidoConsPagamento. */
	private String cdIndicadorFavorecidoConsPagamento = null;

	/** Atributo cdIndicadorFormaAutorizacaoPagamento. */
	private String cdIndicadorFormaAutorizacaoPagamento = null;

	/** Atributo cdIndicadorFormaEnvioPagamento. */
	private String cdIndicadorFormaEnvioPagamento = null;

	/** Atributo cdIndicadorFormaEstornoPagamento. */
	private String cdIndicadorFormaEstornoPagamento = null;

	/** Atributo cdIndicadorFormaExpiracaoCredito. */
	private String cdIndicadorFormaExpiracaoCredito = null;

	/** Atributo cdIndicadorFormaManutencao. */
	private String cdIndicadorFormaManutencao = null;

	/** Atributo cdIndicadorFrasePreCadastro. */
	private String cdIndicadorFrasePreCadastro = null;

	/** Atributo cdIndicadorAgendaTitulo. */
	private String cdIndicadorAgendaTitulo = null;

	/** Atributo cdIndicadorAutorizacaoCliente. */
	private String cdIndicadorAutorizacaoCliente = null;

	/** Atributo cdIndicadorAutorizacaoComplemento. */
	private String cdIndicadorAutorizacaoComplemento = null;

	/** Atributo cdIndicadorCadastroOrg. */
	private String cdIndicadorCadastroOrg = null;

	/** Atributo cdIndicadorCadastroProcd. */
	private String cdIndicadorCadastroProcd = null;

	/** Atributo cdIndicadorCataoSalario. */
	private String cdIndicadorCataoSalario = null;

	/** Atributo cdIndicadorEconomicoReajuste. */
	private String cdIndicadorEconomicoReajuste = null;

	/** Atributo cdIndicadorExpiracaoCredito. */
	private String cdIndicadorExpiracaoCredito = null;

	/** Atributo cdIndicadorLancamentoPagamento. */
	private String cdIndicadorLancamentoPagamento = null;

	/** Atributo cdIndicadorMensagemPerso. */
	private String cdIndicadorMensagemPerso = null;

	/** Atributo cdIndicadorLancamentoFuturoCredito. */
	private String cdIndicadorLancamentoFuturoCredito = null;

	/** Atributo cdIndicadorLancamentoFuturoDebito. */
	private String cdIndicadorLancamentoFuturoDebito = null;

	/** Atributo cdIndicadorLiberacaoLoteProcesso. */
	private String cdIndicadorLiberacaoLoteProcesso = null;

	/** Atributo cdIndicadorManutencaoBaseRecadastro. */
	private String cdIndicadorManutencaoBaseRecadastro = null;

	/** Atributo cdIndicadorMidiaDisponivel. */
	private String cdIndicadorMidiaDisponivel = null;

	/** Atributo cdIndicadorMidiaMensagemRecadastro. */
	private String cdIndicadorMidiaMensagemRecadastro = null;

	/** Atributo cdIndicadorMomentoAvisoRacadastro. */
	private String cdIndicadorMomentoAvisoRacadastro = null;

	/** Atributo cdIndicadorMomentoCreditoEfetivacao. */
	private String cdIndicadorMomentoCreditoEfetivacao = null;

	/** Atributo cdIndicadorMomentoDebitoPagamento. */
	private String cdIndicadorMomentoDebitoPagamento = null;

	/** Atributo cdIndicadorMomentoFormularioRecadastro. */
	private String cdIndicadorMomentoFormularioRecadastro = null;

	/** Atributo cdIndicadorMomentoProcessamentoPagamento. */
	private String cdIndicadorMomentoProcessamentoPagamento = null;

	/** Atributo cdIndicadorMensagemRecadastroMidia. */
	private String cdIndicadorMensagemRecadastroMidia = null;

	/** Atributo cdIndicadorPermissaoDebitoOnline. */
	private String cdIndicadorPermissaoDebitoOnline = null;

	/** Atributo cdIndicadorNaturezaOperacaoPagamento. */
	private String cdIndicadorNaturezaOperacaoPagamento = null;

	/** Atributo cdIndicadorPeriodicidadeAviso. */
	private String cdIndicadorPeriodicidadeAviso = null;

	/** Atributo cdIndicadorPerdcCobrancaTarifa. */
	private String cdIndicadorPerdcCobrancaTarifa = null;

	/** Atributo cdIndicadorPerdcComprovante. */
	private String cdIndicadorPerdcComprovante = null;

	/** Atributo cdIndicadorPerdcConsultaVeiculo. */
	private String cdIndicadorPerdcConsultaVeiculo = null;

	/** Atributo cdIndicadorPerdcEnvioRemessa. */
	private String cdIndicadorPerdcEnvioRemessa = null;

	/** Atributo cdIndicadorPerdcManutencaoProcd. */
	private String cdIndicadorPerdcManutencaoProcd = null;

	/** Atributo cdIndicadorPagamentoNaoUtilizado. */
	private String cdIndicadorPagamentoNaoUtilizado = null;

	/** Atributo cdIndicadorPrincipalEnquaRecadastro. */
	private String cdIndicadorPrincipalEnquaRecadastro = null;

	/** Atributo cdIndicadorPrioridadeEfetivacaoPagamento. */
	private String cdIndicadorPrioridadeEfetivacaoPagamento = null;

	/** Atributo cdIndicadorRejeicaoAgendaLote. */
	private String cdIndicadorRejeicaoAgendaLote = null;

	/** Atributo cdIndicadorRejeicaoEfetivacaoLote. */
	private String cdIndicadorRejeicaoEfetivacaoLote = null;

	/** Atributo cdIndicadorRejeicaoLote. */
	private String cdIndicadorRejeicaoLote = null;

	/** Atributo cdIndicadorRastreabilidadeNotaFiscal. */
	private String cdIndicadorRastreabilidadeNotaFiscal = null;

	/** Atributo cdIndicadorRastreabilidadeTituloTerceiro. */
	private String cdIndicadorRastreabilidadeTituloTerceiro = null;

	/** Atributo cdIndicadorTipoCargaRecadastro. */
	private String cdIndicadorTipoCargaRecadastro = null;

	/** Atributo cdIndicadorTipoCataoSalario. */
	private String cdIndicadorTipoCataoSalario = null;

	/** Atributo cdIndicadorTipoDataFloat. */
	private String cdIndicadorTipoDataFloat = null;

	/** Atributo cdIndicadorTipoDivergenciaVeiculo. */
	private String cdIndicadorTipoDivergenciaVeiculo = null;

	/** Atributo cdIndicadorTipoIdBeneficio. */
	private String cdIndicadorTipoIdBeneficio = null;

	/** Atributo cdIndicadorTipoLayoutArquivo. */
	private String cdIndicadorTipoLayoutArquivo = null;

	/** Atributo cdIndicadorTipoReajusteTarifa. */
	private String cdIndicadorTipoReajusteTarifa = null;

	/** Atributo cdIndicadorContratoContaTransferencia. */
	private String cdIndicadorContratoContaTransferencia = null;

	/** Atributo cdIndicadorUtilizacaoFavorecidoControle. */
	private String cdIndicadorUtilizacaoFavorecidoControle = null;

	/** Atributo dtIndicadorEnquaContaSalario. */
	private String dtIndicadorEnquaContaSalario = null;

	/** Atributo dtIndicadorInicioBloqueioPplta. */
	private String dtIndicadorInicioBloqueioPplta = null;

	/** Atributo dtIndicadorInicioRastreabilidadeTitulo. */
	private String dtIndicadorInicioRastreabilidadeTitulo = null;

	/** Atributo dtIndicadorFimAcertoRecadastro. */
	private String dtIndicadorFimAcertoRecadastro = null;

	/** Atributo dtIndicadorFimRecadastroBeneficio. */
	private String dtIndicadorFimRecadastroBeneficio = null;

	/** Atributo dtIndicadorinicioAcertoRecadastro. */
	private String dtIndicadorinicioAcertoRecadastro = null;

	/** Atributo dtIndicadorInicioRecadastroBeneficio. */
	private String dtIndicadorInicioRecadastroBeneficio = null;

	/** Atributo dtIndicadorLimiteVinculoCarga. */
	private String dtIndicadorLimiteVinculoCarga = null;

	/** Atributo nrIndicadorFechamentoApuracaoTarifa. */
	private String nrIndicadorFechamentoApuracaoTarifa = null;

	/** Atributo cdIndicadorPercentualIndicadorReajusteTarifa. */
	private String cdIndicadorPercentualIndicadorReajusteTarifa = null;

	/** Atributo cdIndicadorPercentualMaximoInconLote. */
	private String cdIndicadorPercentualMaximoInconLote = null;

	/** Atributo cdIndicadorPercentualReducaoTarifaCatalogo. */
	private String cdIndicadorPercentualReducaoTarifaCatalogo = null;

	/** Atributo qtIndicadorAntecedencia. */
	private String qtIndicadorAntecedencia = null;

	/** Atributo qtIndicadorAnteriorVencimentoComprovado. */
	private String qtIndicadorAnteriorVencimentoComprovado = null;

	/** Atributo qtIndicadorDiaCobrancaTarifa. */
	private String qtIndicadorDiaCobrancaTarifa = null;

	/** Atributo qtIndicadorDiaExpiracao. */
	private String qtIndicadorDiaExpiracao = null;

	/** Atributo cdIndicadorDiaFloatPagamento. */
	private String cdIndicadorDiaFloatPagamento = null;

	/** Atributo qtIndicadorDiaInatividadeFavorecido. */
	private String qtIndicadorDiaInatividadeFavorecido = null;

	/** Atributo qtIndicadorDiaRepiqConsulta. */
	private String qtIndicadorDiaRepiqConsulta = null;

	/** Atributo qtIndicadorEtapasRecadastroBeneficio. */
	private String qtIndicadorEtapasRecadastroBeneficio = null;

	/** Atributo qtIndicadorFaseRecadastroBeneficio. */
	private String qtIndicadorFaseRecadastroBeneficio = null;

	/** Atributo qtIndicadorLimiteLinha. */
	private String qtIndicadorLimiteLinha = null;

	/** Atributo qtIndicadorLimiteSolicitacaoCatao. */
	private String qtIndicadorLimiteSolicitacaoCatao = null;

	/** Atributo qtIndicadorMaximaInconLote. */
	private String qtIndicadorMaximaInconLote = null;

	/** Atributo qtIndicadorMaximaTituloVencido. */
	private String qtIndicadorMaximaTituloVencido = null;

	/** Atributo qtIndicadorMesComprovante. */
	private String qtIndicadorMesComprovante = null;

	/** Atributo qtIndicadorMesEtapaRecadastro. */
	private String qtIndicadorMesEtapaRecadastro = null;

	/** Atributo qtIndicadorMesFaseRecadastro. */
	private String qtIndicadorMesFaseRecadastro = null;

	/** Atributo qtIndicadorMesReajusteTarifa. */
	private String qtIndicadorMesReajusteTarifa = null;

	/** Atributo qtIndicadorViaAviso. */
	private String qtIndicadorViaAviso = null;

	/** Atributo qtIndicadorViaCobranca. */
	private String qtIndicadorViaCobranca = null;

	/** Atributo qtIndicadorViaComprovante. */
	private String qtIndicadorViaComprovante = null;

	/** Atributo vlIndicadorFavorecidoNaoCadastro. */
	private String vlIndicadorFavorecidoNaoCadastro = null;

	/** Atributo vlIndicadorLimiteDiaPagamento. */
	private String vlIndicadorLimiteDiaPagamento = null;

	/** Atributo vlIndicadorLimiteIndividualPagamento. */
	private String vlIndicadorLimiteIndividualPagamento = null;

	/** Atributo cdIndicadorEmissaoAviso. */
	private String cdIndicadorEmissaoAviso = null;

	/** Atributo cdIndicadorRetornoInternet. */
	private String cdIndicadorRetornoInternet = null;

	/** Atributo cdIndicadorListaDebito. */
	private String cdIndicadorListaDebito = null;

	/** Atributo cdIndicadorTipoFormacaoLista. */
	private String cdIndicadorTipoFormacaoLista = null;

	/** Atributo cdIndicadorTipoConsistenciaLista. */
	private String cdIndicadorTipoConsistenciaLista = null;

	/** Atributo cdIndicadorTipoConsultaComprovante. */
	private String cdIndicadorTipoConsultaComprovante = null;

	/** Atributo cdIndicadorValidacaoNomeFavorecido. */
	private String cdIndicadorValidacaoNomeFavorecido = null;

	/** Atributo cdIndicadorTipoContaFavorecidoT. */
	private String cdIndicadorTipoContaFavorecidoT = null;

	/** Atributo cdIndLancamentoPersonalizado. */
	private String cdIndLancamentoPersonalizado = null;

	/** Atributo cdIndicadorAgendaRastreabilidadeFilial. */
	private String cdIndicadorAgendaRastreabilidadeFilial = null;

	/** Atributo cdIndicadorAdesaoSacador. */
	private String cdIndicadorAdesaoSacador = null;

	/** Atributo cdIndicadorMeioPagamentoCredito. */
	private String cdIndicadorMeioPagamentoCredito = null;

	/** Atributo cdIndicadorTipoIsncricaoFavorecido. */
	private String cdIndicadorTipoIsncricaoFavorecido = null;

	/** Atributo cdIndicadorBancoPostal. */
	private String cdIndicadorBancoPostal = null;

	/** Atributo cdIndicadorFormularioContratoCliente. */
	private String cdIndicadorFormularioContratoCliente = null;

	/** Atributo dsIndicadorAreaResrd. */
	private String dsIndicadorAreaResrd = null;
	
	/** Atributo dsIndicadorAreaResrd. */
	private Integer cdDependencia = null;
	
	/** Atributo cdIndicadorConsultaSaldoSuperior. */
	private String cdIndicadorConsultaSaldoSuperior = null;
	
	/**
	 * Verificar atributos servico modalidade saida dto.
	 */
	public VerificarAtributosServicoModalidadeSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verificar atributos servico modalidade saida dto.
	 *
	 * @param response the response
	 */
	public VerificarAtributosServicoModalidadeSaidaDTO(VerificarAtributosServicoModalidadeResponse response) {
		super();
		this.cdIndicadorAcaoNaoVida = response.getCdIndicadorAcaoNaoVida();
		this.cdIndicadorAcertoDadoRecadastro = response.getCdIndicadorAcertoDadoRecadastro();
		this.cdIndicadorAgendaDebitoVeiculo = response.getCdIndicadorAgendaDebitoVeiculo();
		this.cdIndicadorAgendaPagamentoVencido = response.getCdIndicadorAgendaPagamentoVencido();
		this.cdIndicadorAgendaValorMenor = response.getCdIndicadorAgendaValorMenor();
		this.cdIndicadorAgrupamentoAviso = response.getCdIndicadorAgrupamentoAviso();
		this.cdIndicadorAgrupamentoComprovado = response.getCdIndicadorAgrupamentoComprovado();
		this.cdIndicadorAgrupamentoFormularioRecadastro = 
			response.getCdIndicadorAgrupamentoFormularioRecadastro();
		this.cdIndicadorAntecRecadastroBeneficio = response.getCdIndicadorAntecRecadastroBeneficio();
		this.cdIndicadorAreaReservada = response.getCdIndicadorAreaReservada();
		this.cdIndicadorBaseRecadastroBeneficio = response.getCdIndicadorBaseRecadastroBeneficio();
		this.cdIndicadorBloqueioEmissaoPplta = response.getCdIndicadorBloqueioEmissaoPplta();
		this.cdIndicadorCapituloTituloRegistro = response.getCdIndicadorCapituloTituloRegistro();
		this.cdIndicadorCobrancaTarifa = response.getCdIndicadorCobrancaTarifa();
		this.cdIndicadorConsDebitoVeiculo = response.getCdIndicadorConsDebitoVeiculo();
		this.cdIndicadorConsEndereco = response.getCdIndicadorConsEndereco();
		this.cdIndicadorConsSaldoPagamento = response.getCdIndicadorConsSaldoPagamento();
		this.cdIndicadorContagemConsSaudo = response.getCdIndicadorContagemConsSaudo();
		this.cdIndicadorCreditoNaoUtilizado = response.getCdIndicadorCreditoNaoUtilizado();
		this.cdIndicadorCriterioEnquaBeneficio = response.getCdIndicadorCriterioEnquaBeneficio();
		this.cdIndicadorCriterioEnqua = response.getCdIndicadorCriterioEnqua();
		this.cdIndicadorCriterioRastreabilidadeTitulo = response.getCdIndicadorCriterioRastreabilidadeTitulo();
		this.cdIndicadorCtciaEspecieBeneficio = response.getCdIndicadorCtciaEspecieBeneficio();
		this.cdIndicadorCtciaIdentificacaoBeneficio = response.getCdIndicadorCtciaIdentificacaoBeneficio();
		this.cdIndicadorCtciaInscricaoFavorecido = response.getCdIndicadorCtciaInscricaoFavorecido();
		this.cdIndicadorCtciaProprietarioVeiculo = response.getCdIndicadorCtciaProprietarioVeiculo();
		this.cdIndicadorDispzContaCredito = response.getCdIndicadorDispzContaCredito();
		this.cdIndicadorDispzDiversar = response.getCdIndicadorDispzDiversar();
		this.cdIndicadorDispzDiversasNao = response.getCdIndicadorDispzDiversasNao();
		this.cdIndicadorDispzSalario = response.getCdIndicadorDispzSalario();
		this.cdIndicadorDispzSalarioNao = response.getCdIndicadorDispzSalarioNao();
		this.cdIndicadorDestinoAviso = response.getCdIndicadorDestinoAviso();
		this.cdIndicadorDestinoComprovante = response.getCdIndicadorDestinoComprovante();
		this.cdIndicadorDestinoFormularioRecadastro = response.getCdIndicadorDestinoFormularioRecadastro();
		this.cdIndicadorEnvelopeAberto = response.getCdIndicadorEnvelopeAberto();
		this.cdIndicadorFavorecidoConsPagamento = response.getCdIndicadorFavorecidoConsPagamento();
		this.cdIndicadorFormaAutorizacaoPagamento = response.getCdIndicadorFormaAutorizacaoPagamento();
		this.cdIndicadorFormaEnvioPagamento = response.getCdIndicadorFormaEnvioPagamento();
		this.cdIndicadorFormaEstornoPagamento = response.getCdIndicadorFormaEstornoPagamento();
		this.cdIndicadorFormaExpiracaoCredito = response.getCdIndicadorFormaExpiracaoCredito();
		this.cdIndicadorFormaManutencao = response.getCdIndicadorFormaManutencao();
		this.cdIndicadorFrasePreCadastro = response.getCdIndicadorFrasePreCadastro();
		this.cdIndicadorAgendaTitulo = response.getCdIndicadorAgendaTitulo();
		this.cdIndicadorAutorizacaoCliente = response.getCdIndicadorAutorizacaoCliente();
		this.cdIndicadorAutorizacaoComplemento = response.getCdIndicadorAutorizacaoComplemento();
		this.cdIndicadorCadastroOrg = response.getCdIndicadorCadastroOrg();
		this.cdIndicadorCadastroProcd = response.getCdIndicadorCadastroProcd();
		this.cdIndicadorCataoSalario = response.getCdIndicadorCataoSalario();
		this.cdIndicadorEconomicoReajuste = response.getCdIndicadorEconomicoReajuste();
		this.cdIndicadorExpiracaoCredito = response.getCdIndicadorExpiracaoCredito();
		this.cdIndicadorLancamentoPagamento = response.getCdIndicadorLancamentoPagamento();
		this.cdIndicadorMensagemPerso = response.getCdIndicadorMensagemPerso();
		this.cdIndicadorLancamentoFuturoCredito = response.getCdIndicadorLancamentoFuturoCredito();
		this.cdIndicadorLancamentoFuturoDebito = response.getCdIndicadorLancamentoFuturoDebito();
		this.cdIndicadorLiberacaoLoteProcesso = response.getCdIndicadorLiberacaoLoteProcesso();
		this.cdIndicadorManutencaoBaseRecadastro = response.getCdIndicadorManutencaoBaseRecadastro();
		this.cdIndicadorMidiaDisponivel = response.getCdIndicadorMidiaDisponivel();
		this.cdIndicadorMidiaMensagemRecadastro = response.getCdIndicadorMidiaMensagemRecadastro();
		this.cdIndicadorMomentoAvisoRacadastro = response.getCdIndicadorMomentoAvisoRacadastro();
		this.cdIndicadorMomentoCreditoEfetivacao = response.getCdIndicadorMomentoCreditoEfetivacao();
		this.cdIndicadorMomentoDebitoPagamento = response.getCdIndicadorMomentoDebitoPagamento();
		this.cdIndicadorMomentoFormularioRecadastro = response.getCdIndicadorMomentoFormularioRecadastro();
		this.cdIndicadorMomentoProcessamentoPagamento = 
			response.getCdIndicadorMomentoProcessamentoPagamento();
		this.cdIndicadorMensagemRecadastroMidia = response.getCdIndicadorMensagemRecadastroMidia();
		this.cdIndicadorPermissaoDebitoOnline = response.getCdIndicadorPermissaoDebitoOnline();
		this.cdIndicadorNaturezaOperacaoPagamento = response.getCdIndicadorNaturezaOperacaoPagamento();
		this.cdIndicadorPeriodicidadeAviso = response.getCdIndicadorPeriodicidadeAviso();
		this.cdIndicadorPerdcCobrancaTarifa = response.getCdIndicadorPerdcCobrancaTarifa();
		this.cdIndicadorPerdcComprovante = response.getCdIndicadorPerdcComprovante();
		this.cdIndicadorPerdcConsultaVeiculo = response.getCdIndicadorPerdcConsultaVeiculo();
		this.cdIndicadorPerdcEnvioRemessa = response.getCdIndicadorPerdcEnvioRemessa();
		this.cdIndicadorPerdcManutencaoProcd = response.getCdIndicadorPerdcManutencaoProcd();
		this.cdIndicadorPagamentoNaoUtilizado = response.getCdIndicadorPagamentoNaoUtilizado();
		this.cdIndicadorPrincipalEnquaRecadastro = response.getCdIndicadorPrincipalEnquaRecadastro();
		this.cdIndicadorPrioridadeEfetivacaoPagamento = response.getCdIndicadorPrioridadeEfetivacaoPagamento();
		this.cdIndicadorRejeicaoAgendaLote = response.getCdIndicadorRejeicaoAgendaLote();
		this.cdIndicadorRejeicaoEfetivacaoLote = response.getCdIndicadorRejeicaoEfetivacaoLote();
		this.cdIndicadorRejeicaoLote = response.getCdIndicadorRejeicaoLote();
		this.cdIndicadorRastreabilidadeNotaFiscal = response.getCdIndicadorRastreabilidadeNotaFiscal();
		this.cdIndicadorRastreabilidadeTituloTerceiro = response.getCdIndicadorRastreabilidadeTituloTerceiro();
		this.cdIndicadorTipoCargaRecadastro = response.getCdIndicadorTipoCargaRecadastro();
		this.cdIndicadorTipoCataoSalario = response.getCdIndicadorTipoCataoSalario();
		this.cdIndicadorTipoDataFloat = response.getCdIndicadorTipoDataFloat();
		this.cdIndicadorTipoDivergenciaVeiculo = response.getCdIndicadorTipoDivergenciaVeiculo();
		this.cdIndicadorTipoIdBeneficio = response.getCdIndicadorTipoIdBeneficio();
		this.cdIndicadorTipoLayoutArquivo = response.getCdIndicadorTipoLayoutArquivo();
		this.cdIndicadorTipoReajusteTarifa = response.getCdIndicadorTipoReajusteTarifa();
		this.cdIndicadorContratoContaTransferencia = response.getCdIndicadorContratoContaTransferencia();
		this.cdIndicadorUtilizacaoFavorecidoControle = response.getCdIndicadorUtilizacaoFavorecidoControle();
		this.dtIndicadorEnquaContaSalario = response.getDtIndicadorEnquaContaSalario();
		this.dtIndicadorInicioBloqueioPplta = response.getDtIndicadorInicioBloqueioPplta();
		this.dtIndicadorInicioRastreabilidadeTitulo = response.getDtIndicadorInicioRastreabilidadeTitulo();
		this.dtIndicadorFimAcertoRecadastro = response.getDtIndicadorFimAcertoRecadastro();
		this.dtIndicadorFimRecadastroBeneficio = response.getDtIndicadorFimRecadastroBeneficio();
		this.dtIndicadorinicioAcertoRecadastro = response.getDtIndicadorinicioAcertoRecadastro();
		this.dtIndicadorInicioRecadastroBeneficio = response.getDtIndicadorInicioRecadastroBeneficio();
		this.dtIndicadorLimiteVinculoCarga = response.getDtIndicadorLimiteVinculoCarga();
		this.nrIndicadorFechamentoApuracaoTarifa = response.getNrIndicadorFechamentoApuracaoTarifa();
		this.cdIndicadorPercentualIndicadorReajusteTarifa = 
			response.getCdIndicadorPercentualIndicadorReajusteTarifa();
		this.cdIndicadorPercentualMaximoInconLote = response.getCdIndicadorPercentualMaximoInconLote();
		this.cdIndicadorPercentualReducaoTarifaCatalogo = 
			response.getCdIndicadorPercentualReducaoTarifaCatalogo();
		this.qtIndicadorAntecedencia = response.getQtIndicadorAntecedencia();
		this.qtIndicadorAnteriorVencimentoComprovado = response.getQtIndicadorAnteriorVencimentoComprovado();
		this.qtIndicadorDiaCobrancaTarifa = response.getQtIndicadorDiaCobrancaTarifa();
		this.qtIndicadorDiaExpiracao = response.getQtIndicadorDiaExpiracao();
		this.cdIndicadorDiaFloatPagamento = response.getCdIndicadorDiaFloatPagamento();
		this.qtIndicadorDiaInatividadeFavorecido = response.getQtIndicadorDiaInatividadeFavorecido();
		this.qtIndicadorDiaRepiqConsulta = response.getQtIndicadorDiaRepiqConsulta();
		this.qtIndicadorEtapasRecadastroBeneficio = response.getQtIndicadorEtapasRecadastroBeneficio();
		this.qtIndicadorFaseRecadastroBeneficio = response.getQtIndicadorFaseRecadastroBeneficio();
		this.qtIndicadorLimiteLinha = response.getQtIndicadorLimiteLinha();
		this.qtIndicadorLimiteSolicitacaoCatao = response.getQtIndicadorLimiteSolicitacaoCatao();
		this.qtIndicadorMaximaInconLote = response.getQtIndicadorMaximaInconLote();
		this.qtIndicadorMaximaTituloVencido = response.getQtIndicadorMaximaTituloVencido();
		this.qtIndicadorMesComprovante = response.getQtIndicadorMesComprovante();
		this.qtIndicadorMesEtapaRecadastro = response.getQtIndicadorMesEtapaRecadastro();
		this.qtIndicadorMesFaseRecadastro = response.getQtIndicadorMesFaseRecadastro();
		this.qtIndicadorMesReajusteTarifa = response.getQtIndicadorMesReajusteTarifa();
		this.qtIndicadorViaAviso = response.getQtIndicadorViaAviso();
		this.qtIndicadorViaCobranca = response.getQtIndicadorViaCobranca();
		this.qtIndicadorViaComprovante = response.getQtIndicadorViaComprovante();
		this.vlIndicadorFavorecidoNaoCadastro = response.getVlIndicadorFavorecidoNaoCadastro();
		this.vlIndicadorLimiteDiaPagamento = response.getVlIndicadorLimiteDiaPagamento();
		this.vlIndicadorLimiteIndividualPagamento = response.getVlIndicadorLimiteIndividualPagamento();
		this.cdIndicadorEmissaoAviso = response.getCdIndicadorEmissaoAviso();
		this.cdIndicadorRetornoInternet = response.getCdIndicadorRetornoInternet();
		this.cdIndicadorListaDebito = response.getCdIndicadorListaDebito();
		this.cdIndicadorTipoFormacaoLista = response.getCdIndicadorTipoFormacaoLista();
		this.cdIndicadorTipoConsistenciaLista = response.getCdIndicadorTipoConsistenciaLista();
		this.cdIndicadorTipoConsultaComprovante = response.getCdIndicadorTipoConsultaComprovante();
		this.cdIndicadorValidacaoNomeFavorecido = response.getCdIndicadorValidacaoNomeFavorecido();
		this.cdIndicadorTipoContaFavorecidoT = response.getCdIndicadorTipoContaFavorecidoT();
		this.cdIndLancamentoPersonalizado = response.getCdIndLancamentoPersonalizado();
		this.cdIndicadorAgendaRastreabilidadeFilial = response.getCdIndicadorAgendaRastreabilidadeFilial();
		this.cdIndicadorAdesaoSacador = response.getCdIndicadorAdesaoSacador();
		this.cdIndicadorMeioPagamentoCredito = response.getCdIndicadorMeioPagamentoCredito();
		this.cdIndicadorTipoIsncricaoFavorecido = response.getCdIndicadorTipoIsncricaoFavorecido();
		this.cdIndicadorBancoPostal = response.getCdIndicadorBancoPostal();
		this.cdIndicadorFormularioContratoCliente = response.getCdIndicadorFormularioContratoCliente();
		this.dsIndicadorAreaResrd = response.getDsIndicadorAreaResrd();
		this.cdDependencia = response.getCdDependencia();
		this.cdIndicadorConsultaSaldoSuperior = response.getCdIndicadorConsultaSaldoSuperior();
	}

	/**
	 * @return the cdDependencia
	 */
	public Integer getCdDependencia() {
		return cdDependencia;
	}

	/**
	 * @param cdDependencia the cdDependencia to set
	 */
	public void setCdDependencia(Integer cdDependencia) {
		this.cdDependencia = cdDependencia;
	}
	/**
	 * Get: cdIndicadorAcaoNaoVida.
	 *
	 * @return cdIndicadorAcaoNaoVida
	 */
	public String getCdIndicadorAcaoNaoVida() {
		return cdIndicadorAcaoNaoVida;
	}

	/**
	 * Set: cdIndicadorAcaoNaoVida.
	 *
	 * @param cdIndicadorAcaoNaoVida the cd indicador acao nao vida
	 */
	public void setCdIndicadorAcaoNaoVida(String cdIndicadorAcaoNaoVida) {
		this.cdIndicadorAcaoNaoVida = cdIndicadorAcaoNaoVida;
	}

	/**
	 * Get: cdIndicadorAcertoDadoRecadastro.
	 *
	 * @return cdIndicadorAcertoDadoRecadastro
	 */
	public String getCdIndicadorAcertoDadoRecadastro() {
		return cdIndicadorAcertoDadoRecadastro;
	}

	/**
	 * Set: cdIndicadorAcertoDadoRecadastro.
	 *
	 * @param cdIndicadorAcertoDadoRecadastro the cd indicador acerto dado recadastro
	 */
	public void setCdIndicadorAcertoDadoRecadastro(String cdIndicadorAcertoDadoRecadastro) {
		this.cdIndicadorAcertoDadoRecadastro = cdIndicadorAcertoDadoRecadastro;
	}

	/**
	 * Get: cdIndicadorAgendaDebitoVeiculo.
	 *
	 * @return cdIndicadorAgendaDebitoVeiculo
	 */
	public String getCdIndicadorAgendaDebitoVeiculo() {
		return cdIndicadorAgendaDebitoVeiculo;
	}

	/**
	 * Set: cdIndicadorAgendaDebitoVeiculo.
	 *
	 * @param cdIndicadorAgendaDebitoVeiculo the cd indicador agenda debito veiculo
	 */
	public void setCdIndicadorAgendaDebitoVeiculo(String cdIndicadorAgendaDebitoVeiculo) {
		this.cdIndicadorAgendaDebitoVeiculo = cdIndicadorAgendaDebitoVeiculo;
	}

	/**
	 * Get: cdIndicadorAgendaPagamentoVencido.
	 *
	 * @return cdIndicadorAgendaPagamentoVencido
	 */
	public String getCdIndicadorAgendaPagamentoVencido() {
		return cdIndicadorAgendaPagamentoVencido;
	}

	/**
	 * Set: cdIndicadorAgendaPagamentoVencido.
	 *
	 * @param cdIndicadorAgendaPagamentoVencido the cd indicador agenda pagamento vencido
	 */
	public void setCdIndicadorAgendaPagamentoVencido(String cdIndicadorAgendaPagamentoVencido) {
		this.cdIndicadorAgendaPagamentoVencido = cdIndicadorAgendaPagamentoVencido;
	}

	/**
	 * Get: cdIndicadorAgendaValorMenor.
	 *
	 * @return cdIndicadorAgendaValorMenor
	 */
	public String getCdIndicadorAgendaValorMenor() {
		return cdIndicadorAgendaValorMenor;
	}

	/**
	 * Set: cdIndicadorAgendaValorMenor.
	 *
	 * @param cdIndicadorAgendaValorMenor the cd indicador agenda valor menor
	 */
	public void setCdIndicadorAgendaValorMenor(String cdIndicadorAgendaValorMenor) {
		this.cdIndicadorAgendaValorMenor = cdIndicadorAgendaValorMenor;
	}

	/**
	 * Get: cdIndicadorAgrupamentoAviso.
	 *
	 * @return cdIndicadorAgrupamentoAviso
	 */
	public String getCdIndicadorAgrupamentoAviso() {
		return cdIndicadorAgrupamentoAviso;
	}

	/**
	 * Set: cdIndicadorAgrupamentoAviso.
	 *
	 * @param cdIndicadorAgrupamentoAviso the cd indicador agrupamento aviso
	 */
	public void setCdIndicadorAgrupamentoAviso(String cdIndicadorAgrupamentoAviso) {
		this.cdIndicadorAgrupamentoAviso = cdIndicadorAgrupamentoAviso;
	}

	/**
	 * Get: cdIndicadorAgrupamentoComprovado.
	 *
	 * @return cdIndicadorAgrupamentoComprovado
	 */
	public String getCdIndicadorAgrupamentoComprovado() {
		return cdIndicadorAgrupamentoComprovado;
	}

	/**
	 * Set: cdIndicadorAgrupamentoComprovado.
	 *
	 * @param cdIndicadorAgrupamentoComprovado the cd indicador agrupamento comprovado
	 */
	public void setCdIndicadorAgrupamentoComprovado(String cdIndicadorAgrupamentoComprovado) {
		this.cdIndicadorAgrupamentoComprovado = cdIndicadorAgrupamentoComprovado;
	}

	/**
	 * Get: cdIndicadorAgrupamentoFormularioRecadastro.
	 *
	 * @return cdIndicadorAgrupamentoFormularioRecadastro
	 */
	public String getCdIndicadorAgrupamentoFormularioRecadastro() {
		return cdIndicadorAgrupamentoFormularioRecadastro;
	}

	/**
	 * Set: cdIndicadorAgrupamentoFormularioRecadastro.
	 *
	 * @param cdIndicadorAgrupamentoFormularioRecadastro the cd indicador agrupamento formulario recadastro
	 */
	public void setCdIndicadorAgrupamentoFormularioRecadastro(
			String cdIndicadorAgrupamentoFormularioRecadastro) {
		this.cdIndicadorAgrupamentoFormularioRecadastro = cdIndicadorAgrupamentoFormularioRecadastro;
	}

	/**
	 * Get: cdIndicadorAntecRecadastroBeneficio.
	 *
	 * @return cdIndicadorAntecRecadastroBeneficio
	 */
	public String getCdIndicadorAntecRecadastroBeneficio() {
		return cdIndicadorAntecRecadastroBeneficio;
	}

	/**
	 * Set: cdIndicadorAntecRecadastroBeneficio.
	 *
	 * @param cdIndicadorAntecRecadastroBeneficio the cd indicador antec recadastro beneficio
	 */
	public void setCdIndicadorAntecRecadastroBeneficio(String cdIndicadorAntecRecadastroBeneficio) {
		this.cdIndicadorAntecRecadastroBeneficio = cdIndicadorAntecRecadastroBeneficio;
	}

	/**
	 * Get: cdIndicadorAreaReservada.
	 *
	 * @return cdIndicadorAreaReservada
	 */
	public String getCdIndicadorAreaReservada() {
		return cdIndicadorAreaReservada;
	}

	/**
	 * Set: cdIndicadorAreaReservada.
	 *
	 * @param cdIndicadorAreaReservada the cd indicador area reservada
	 */
	public void setCdIndicadorAreaReservada(String cdIndicadorAreaReservada) {
		this.cdIndicadorAreaReservada = cdIndicadorAreaReservada;
	}

	/**
	 * Get: cdIndicadorBaseRecadastroBeneficio.
	 *
	 * @return cdIndicadorBaseRecadastroBeneficio
	 */
	public String getCdIndicadorBaseRecadastroBeneficio() {
		return cdIndicadorBaseRecadastroBeneficio;
	}

	/**
	 * Set: cdIndicadorBaseRecadastroBeneficio.
	 *
	 * @param cdIndicadorBaseRecadastroBeneficio the cd indicador base recadastro beneficio
	 */
	public void setCdIndicadorBaseRecadastroBeneficio(String cdIndicadorBaseRecadastroBeneficio) {
		this.cdIndicadorBaseRecadastroBeneficio = cdIndicadorBaseRecadastroBeneficio;
	}

	/**
	 * Get: cdIndicadorBloqueioEmissaoPplta.
	 *
	 * @return cdIndicadorBloqueioEmissaoPplta
	 */
	public String getCdIndicadorBloqueioEmissaoPplta() {
		return cdIndicadorBloqueioEmissaoPplta;
	}

	/**
	 * Set: cdIndicadorBloqueioEmissaoPplta.
	 *
	 * @param cdIndicadorBloqueioEmissaoPplta the cd indicador bloqueio emissao pplta
	 */
	public void setCdIndicadorBloqueioEmissaoPplta(String cdIndicadorBloqueioEmissaoPplta) {
		this.cdIndicadorBloqueioEmissaoPplta = cdIndicadorBloqueioEmissaoPplta;
	}

	/**
	 * Get: cdIndicadorCapituloTituloRegistro.
	 *
	 * @return cdIndicadorCapituloTituloRegistro
	 */
	public String getCdIndicadorCapituloTituloRegistro() {
		return cdIndicadorCapituloTituloRegistro;
	}

	/**
	 * Set: cdIndicadorCapituloTituloRegistro.
	 *
	 * @param cdIndicadorCapituloTituloRegistro the cd indicador capitulo titulo registro
	 */
	public void setCdIndicadorCapituloTituloRegistro(String cdIndicadorCapituloTituloRegistro) {
		this.cdIndicadorCapituloTituloRegistro = cdIndicadorCapituloTituloRegistro;
	}

	/**
	 * Get: cdIndicadorCobrancaTarifa.
	 *
	 * @return cdIndicadorCobrancaTarifa
	 */
	public String getCdIndicadorCobrancaTarifa() {
		return cdIndicadorCobrancaTarifa;
	}

	/**
	 * Set: cdIndicadorCobrancaTarifa.
	 *
	 * @param cdIndicadorCobrancaTarifa the cd indicador cobranca tarifa
	 */
	public void setCdIndicadorCobrancaTarifa(String cdIndicadorCobrancaTarifa) {
		this.cdIndicadorCobrancaTarifa = cdIndicadorCobrancaTarifa;
	}

	/**
	 * Get: cdIndicadorConsDebitoVeiculo.
	 *
	 * @return cdIndicadorConsDebitoVeiculo
	 */
	public String getCdIndicadorConsDebitoVeiculo() {
		return cdIndicadorConsDebitoVeiculo;
	}

	/**
	 * Set: cdIndicadorConsDebitoVeiculo.
	 *
	 * @param cdIndicadorConsDebitoVeiculo the cd indicador cons debito veiculo
	 */
	public void setCdIndicadorConsDebitoVeiculo(String cdIndicadorConsDebitoVeiculo) {
		this.cdIndicadorConsDebitoVeiculo = cdIndicadorConsDebitoVeiculo;
	}

	/**
	 * Get: cdIndicadorConsEndereco.
	 *
	 * @return cdIndicadorConsEndereco
	 */
	public String getCdIndicadorConsEndereco() {
		return cdIndicadorConsEndereco;
	}

	/**
	 * Set: cdIndicadorConsEndereco.
	 *
	 * @param cdIndicadorConsEndereco the cd indicador cons endereco
	 */
	public void setCdIndicadorConsEndereco(String cdIndicadorConsEndereco) {
		this.cdIndicadorConsEndereco = cdIndicadorConsEndereco;
	}

	/**
	 * Get: cdIndicadorConsSaldoPagamento.
	 *
	 * @return cdIndicadorConsSaldoPagamento
	 */
	public String getCdIndicadorConsSaldoPagamento() {
		return cdIndicadorConsSaldoPagamento;
	}

	/**
	 * Set: cdIndicadorConsSaldoPagamento.
	 *
	 * @param cdIndicadorConsSaldoPagamento the cd indicador cons saldo pagamento
	 */
	public void setCdIndicadorConsSaldoPagamento(String cdIndicadorConsSaldoPagamento) {
		this.cdIndicadorConsSaldoPagamento = cdIndicadorConsSaldoPagamento;
	}

	/**
	 * Get: cdIndicadorContagemConsSaudo.
	 *
	 * @return cdIndicadorContagemConsSaudo
	 */
	public String getCdIndicadorContagemConsSaudo() {
		return cdIndicadorContagemConsSaudo;
	}

	/**
	 * Set: cdIndicadorContagemConsSaudo.
	 *
	 * @param cdIndicadorContagemConsSaudo the cd indicador contagem cons saudo
	 */
	public void setCdIndicadorContagemConsSaudo(String cdIndicadorContagemConsSaudo) {
		this.cdIndicadorContagemConsSaudo = cdIndicadorContagemConsSaudo;
	}

	/**
	 * Get: cdIndicadorCreditoNaoUtilizado.
	 *
	 * @return cdIndicadorCreditoNaoUtilizado
	 */
	public String getCdIndicadorCreditoNaoUtilizado() {
		return cdIndicadorCreditoNaoUtilizado;
	}

	/**
	 * Set: cdIndicadorCreditoNaoUtilizado.
	 *
	 * @param cdIndicadorCreditoNaoUtilizado the cd indicador credito nao utilizado
	 */
	public void setCdIndicadorCreditoNaoUtilizado(String cdIndicadorCreditoNaoUtilizado) {
		this.cdIndicadorCreditoNaoUtilizado = cdIndicadorCreditoNaoUtilizado;
	}

	/**
	 * Get: cdIndicadorCriterioEnquaBeneficio.
	 *
	 * @return cdIndicadorCriterioEnquaBeneficio
	 */
	public String getCdIndicadorCriterioEnquaBeneficio() {
		return cdIndicadorCriterioEnquaBeneficio;
	}

	/**
	 * Set: cdIndicadorCriterioEnquaBeneficio.
	 *
	 * @param cdIndicadorCriterioEnquaBeneficio the cd indicador criterio enqua beneficio
	 */
	public void setCdIndicadorCriterioEnquaBeneficio(String cdIndicadorCriterioEnquaBeneficio) {
		this.cdIndicadorCriterioEnquaBeneficio = cdIndicadorCriterioEnquaBeneficio;
	}

	/**
	 * Get: cdIndicadorCriterioEnqua.
	 *
	 * @return cdIndicadorCriterioEnqua
	 */
	public String getCdIndicadorCriterioEnqua() {
		return cdIndicadorCriterioEnqua;
	}

	/**
	 * Set: cdIndicadorCriterioEnqua.
	 *
	 * @param cdIndicadorCriterioEnqua the cd indicador criterio enqua
	 */
	public void setCdIndicadorCriterioEnqua(String cdIndicadorCriterioEnqua) {
		this.cdIndicadorCriterioEnqua = cdIndicadorCriterioEnqua;
	}

	/**
	 * Get: cdIndicadorCriterioRastreabilidadeTitulo.
	 *
	 * @return cdIndicadorCriterioRastreabilidadeTitulo
	 */
	public String getCdIndicadorCriterioRastreabilidadeTitulo() {
		return cdIndicadorCriterioRastreabilidadeTitulo;
	}

	/**
	 * Set: cdIndicadorCriterioRastreabilidadeTitulo.
	 *
	 * @param cdIndicadorCriterioRastreabilidadeTitulo the cd indicador criterio rastreabilidade titulo
	 */
	public void setCdIndicadorCriterioRastreabilidadeTitulo(String cdIndicadorCriterioRastreabilidadeTitulo) {
		this.cdIndicadorCriterioRastreabilidadeTitulo = cdIndicadorCriterioRastreabilidadeTitulo;
	}

	/**
	 * Get: cdIndicadorCtciaEspecieBeneficio.
	 *
	 * @return cdIndicadorCtciaEspecieBeneficio
	 */
	public String getCdIndicadorCtciaEspecieBeneficio() {
		return cdIndicadorCtciaEspecieBeneficio;
	}

	/**
	 * Set: cdIndicadorCtciaEspecieBeneficio.
	 *
	 * @param cdIndicadorCtciaEspecieBeneficio the cd indicador ctcia especie beneficio
	 */
	public void setCdIndicadorCtciaEspecieBeneficio(String cdIndicadorCtciaEspecieBeneficio) {
		this.cdIndicadorCtciaEspecieBeneficio = cdIndicadorCtciaEspecieBeneficio;
	}

	/**
	 * Get: cdIndicadorCtciaIdentificacaoBeneficio.
	 *
	 * @return cdIndicadorCtciaIdentificacaoBeneficio
	 */
	public String getCdIndicadorCtciaIdentificacaoBeneficio() {
		return cdIndicadorCtciaIdentificacaoBeneficio;
	}

	/**
	 * Set: cdIndicadorCtciaIdentificacaoBeneficio.
	 *
	 * @param cdIndicadorCtciaIdentificacaoBeneficio the cd indicador ctcia identificacao beneficio
	 */
	public void setCdIndicadorCtciaIdentificacaoBeneficio(String cdIndicadorCtciaIdentificacaoBeneficio) {
		this.cdIndicadorCtciaIdentificacaoBeneficio = cdIndicadorCtciaIdentificacaoBeneficio;
	}

	/**
	 * Get: cdIndicadorCtciaInscricaoFavorecido.
	 *
	 * @return cdIndicadorCtciaInscricaoFavorecido
	 */
	public String getCdIndicadorCtciaInscricaoFavorecido() {
		return cdIndicadorCtciaInscricaoFavorecido;
	}

	/**
	 * Set: cdIndicadorCtciaInscricaoFavorecido.
	 *
	 * @param cdIndicadorCtciaInscricaoFavorecido the cd indicador ctcia inscricao favorecido
	 */
	public void setCdIndicadorCtciaInscricaoFavorecido(String cdIndicadorCtciaInscricaoFavorecido) {
		this.cdIndicadorCtciaInscricaoFavorecido = cdIndicadorCtciaInscricaoFavorecido;
	}

	/**
	 * Get: cdIndicadorCtciaProprietarioVeiculo.
	 *
	 * @return cdIndicadorCtciaProprietarioVeiculo
	 */
	public String getCdIndicadorCtciaProprietarioVeiculo() {
		return cdIndicadorCtciaProprietarioVeiculo;
	}

	/**
	 * Set: cdIndicadorCtciaProprietarioVeiculo.
	 *
	 * @param cdIndicadorCtciaProprietarioVeiculo the cd indicador ctcia proprietario veiculo
	 */
	public void setCdIndicadorCtciaProprietarioVeiculo(String cdIndicadorCtciaProprietarioVeiculo) {
		this.cdIndicadorCtciaProprietarioVeiculo = cdIndicadorCtciaProprietarioVeiculo;
	}

	/**
	 * Get: cdIndicadorDispzContaCredito.
	 *
	 * @return cdIndicadorDispzContaCredito
	 */
	public String getCdIndicadorDispzContaCredito() {
		return cdIndicadorDispzContaCredito;
	}

	/**
	 * Set: cdIndicadorDispzContaCredito.
	 *
	 * @param cdIndicadorDispzContaCredito the cd indicador dispz conta credito
	 */
	public void setCdIndicadorDispzContaCredito(String cdIndicadorDispzContaCredito) {
		this.cdIndicadorDispzContaCredito = cdIndicadorDispzContaCredito;
	}

	/**
	 * Get: cdIndicadorDispzDiversar.
	 *
	 * @return cdIndicadorDispzDiversar
	 */
	public String getCdIndicadorDispzDiversar() {
		return cdIndicadorDispzDiversar;
	}

	/**
	 * Set: cdIndicadorDispzDiversar.
	 *
	 * @param cdIndicadorDispzDiversar the cd indicador dispz diversar
	 */
	public void setCdIndicadorDispzDiversar(String cdIndicadorDispzDiversar) {
		this.cdIndicadorDispzDiversar = cdIndicadorDispzDiversar;
	}

	/**
	 * Get: cdIndicadorDispzDiversasNao.
	 *
	 * @return cdIndicadorDispzDiversasNao
	 */
	public String getCdIndicadorDispzDiversasNao() {
		return cdIndicadorDispzDiversasNao;
	}

	/**
	 * Set: cdIndicadorDispzDiversasNao.
	 *
	 * @param cdIndicadorDispzDiversasNao the cd indicador dispz diversas nao
	 */
	public void setCdIndicadorDispzDiversasNao(String cdIndicadorDispzDiversasNao) {
		this.cdIndicadorDispzDiversasNao = cdIndicadorDispzDiversasNao;
	}

	/**
	 * Get: cdIndicadorDispzSalario.
	 *
	 * @return cdIndicadorDispzSalario
	 */
	public String getCdIndicadorDispzSalario() {
		return cdIndicadorDispzSalario;
	}

	/**
	 * Set: cdIndicadorDispzSalario.
	 *
	 * @param cdIndicadorDispzSalario the cd indicador dispz salario
	 */
	public void setCdIndicadorDispzSalario(String cdIndicadorDispzSalario) {
		this.cdIndicadorDispzSalario = cdIndicadorDispzSalario;
	}

	/**
	 * Get: cdIndicadorDispzSalarioNao.
	 *
	 * @return cdIndicadorDispzSalarioNao
	 */
	public String getCdIndicadorDispzSalarioNao() {
		return cdIndicadorDispzSalarioNao;
	}

	/**
	 * Set: cdIndicadorDispzSalarioNao.
	 *
	 * @param cdIndicadorDispzSalarioNao the cd indicador dispz salario nao
	 */
	public void setCdIndicadorDispzSalarioNao(String cdIndicadorDispzSalarioNao) {
		this.cdIndicadorDispzSalarioNao = cdIndicadorDispzSalarioNao;
	}

	/**
	 * Get: cdIndicadorDestinoAviso.
	 *
	 * @return cdIndicadorDestinoAviso
	 */
	public String getCdIndicadorDestinoAviso() {
		return cdIndicadorDestinoAviso;
	}

	/**
	 * Set: cdIndicadorDestinoAviso.
	 *
	 * @param cdIndicadorDestinoAviso the cd indicador destino aviso
	 */
	public void setCdIndicadorDestinoAviso(String cdIndicadorDestinoAviso) {
		this.cdIndicadorDestinoAviso = cdIndicadorDestinoAviso;
	}

	/**
	 * Get: cdIndicadorDestinoComprovante.
	 *
	 * @return cdIndicadorDestinoComprovante
	 */
	public String getCdIndicadorDestinoComprovante() {
		return cdIndicadorDestinoComprovante;
	}

	/**
	 * Set: cdIndicadorDestinoComprovante.
	 *
	 * @param cdIndicadorDestinoComprovante the cd indicador destino comprovante
	 */
	public void setCdIndicadorDestinoComprovante(String cdIndicadorDestinoComprovante) {
		this.cdIndicadorDestinoComprovante = cdIndicadorDestinoComprovante;
	}

	/**
	 * Get: cdIndicadorDestinoFormularioRecadastro.
	 *
	 * @return cdIndicadorDestinoFormularioRecadastro
	 */
	public String getCdIndicadorDestinoFormularioRecadastro() {
		return cdIndicadorDestinoFormularioRecadastro;
	}

	/**
	 * Set: cdIndicadorDestinoFormularioRecadastro.
	 *
	 * @param cdIndicadorDestinoFormularioRecadastro the cd indicador destino formulario recadastro
	 */
	public void setCdIndicadorDestinoFormularioRecadastro(String cdIndicadorDestinoFormularioRecadastro) {
		this.cdIndicadorDestinoFormularioRecadastro = cdIndicadorDestinoFormularioRecadastro;
	}

	/**
	 * Get: cdIndicadorEnvelopeAberto.
	 *
	 * @return cdIndicadorEnvelopeAberto
	 */
	public String getCdIndicadorEnvelopeAberto() {
		return cdIndicadorEnvelopeAberto;
	}

	/**
	 * Set: cdIndicadorEnvelopeAberto.
	 *
	 * @param cdIndicadorEnvelopeAberto the cd indicador envelope aberto
	 */
	public void setCdIndicadorEnvelopeAberto(String cdIndicadorEnvelopeAberto) {
		this.cdIndicadorEnvelopeAberto = cdIndicadorEnvelopeAberto;
	}

	/**
	 * Get: cdIndicadorFavorecidoConsPagamento.
	 *
	 * @return cdIndicadorFavorecidoConsPagamento
	 */
	public String getCdIndicadorFavorecidoConsPagamento() {
		return cdIndicadorFavorecidoConsPagamento;
	}

	/**
	 * Set: cdIndicadorFavorecidoConsPagamento.
	 *
	 * @param cdIndicadorFavorecidoConsPagamento the cd indicador favorecido cons pagamento
	 */
	public void setCdIndicadorFavorecidoConsPagamento(String cdIndicadorFavorecidoConsPagamento) {
		this.cdIndicadorFavorecidoConsPagamento = cdIndicadorFavorecidoConsPagamento;
	}

	/**
	 * Get: cdIndicadorFormaAutorizacaoPagamento.
	 *
	 * @return cdIndicadorFormaAutorizacaoPagamento
	 */
	public String getCdIndicadorFormaAutorizacaoPagamento() {
		return cdIndicadorFormaAutorizacaoPagamento;
	}

	/**
	 * Set: cdIndicadorFormaAutorizacaoPagamento.
	 *
	 * @param cdIndicadorFormaAutorizacaoPagamento the cd indicador forma autorizacao pagamento
	 */
	public void setCdIndicadorFormaAutorizacaoPagamento(String cdIndicadorFormaAutorizacaoPagamento) {
		this.cdIndicadorFormaAutorizacaoPagamento = cdIndicadorFormaAutorizacaoPagamento;
	}

	/**
	 * Get: cdIndicadorFormaEnvioPagamento.
	 *
	 * @return cdIndicadorFormaEnvioPagamento
	 */
	public String getCdIndicadorFormaEnvioPagamento() {
		return cdIndicadorFormaEnvioPagamento;
	}

	/**
	 * Set: cdIndicadorFormaEnvioPagamento.
	 *
	 * @param cdIndicadorFormaEnvioPagamento the cd indicador forma envio pagamento
	 */
	public void setCdIndicadorFormaEnvioPagamento(String cdIndicadorFormaEnvioPagamento) {
		this.cdIndicadorFormaEnvioPagamento = cdIndicadorFormaEnvioPagamento;
	}

	/**
	 * Get: cdIndicadorFormaEstornoPagamento.
	 *
	 * @return cdIndicadorFormaEstornoPagamento
	 */
	public String getCdIndicadorFormaEstornoPagamento() {
		return cdIndicadorFormaEstornoPagamento;
	}

	/**
	 * Set: cdIndicadorFormaEstornoPagamento.
	 *
	 * @param cdIndicadorFormaEstornoPagamento the cd indicador forma estorno pagamento
	 */
	public void setCdIndicadorFormaEstornoPagamento(String cdIndicadorFormaEstornoPagamento) {
		this.cdIndicadorFormaEstornoPagamento = cdIndicadorFormaEstornoPagamento;
	}

	/**
	 * Get: cdIndicadorFormaExpiracaoCredito.
	 *
	 * @return cdIndicadorFormaExpiracaoCredito
	 */
	public String getCdIndicadorFormaExpiracaoCredito() {
		return cdIndicadorFormaExpiracaoCredito;
	}

	/**
	 * Set: cdIndicadorFormaExpiracaoCredito.
	 *
	 * @param cdIndicadorFormaExpiracaoCredito the cd indicador forma expiracao credito
	 */
	public void setCdIndicadorFormaExpiracaoCredito(String cdIndicadorFormaExpiracaoCredito) {
		this.cdIndicadorFormaExpiracaoCredito = cdIndicadorFormaExpiracaoCredito;
	}

	/**
	 * Get: cdIndicadorFormaManutencao.
	 *
	 * @return cdIndicadorFormaManutencao
	 */
	public String getCdIndicadorFormaManutencao() {
		return cdIndicadorFormaManutencao;
	}

	/**
	 * Set: cdIndicadorFormaManutencao.
	 *
	 * @param cdIndicadorFormaManutencao the cd indicador forma manutencao
	 */
	public void setCdIndicadorFormaManutencao(String cdIndicadorFormaManutencao) {
		this.cdIndicadorFormaManutencao = cdIndicadorFormaManutencao;
	}

	/**
	 * Get: cdIndicadorFrasePreCadastro.
	 *
	 * @return cdIndicadorFrasePreCadastro
	 */
	public String getCdIndicadorFrasePreCadastro() {
		return cdIndicadorFrasePreCadastro;
	}

	/**
	 * Set: cdIndicadorFrasePreCadastro.
	 *
	 * @param cdIndicadorFrasePreCadastro the cd indicador frase pre cadastro
	 */
	public void setCdIndicadorFrasePreCadastro(String cdIndicadorFrasePreCadastro) {
		this.cdIndicadorFrasePreCadastro = cdIndicadorFrasePreCadastro;
	}

	/**
	 * Get: cdIndicadorAgendaTitulo.
	 *
	 * @return cdIndicadorAgendaTitulo
	 */
	public String getCdIndicadorAgendaTitulo() {
		return cdIndicadorAgendaTitulo;
	}

	/**
	 * Set: cdIndicadorAgendaTitulo.
	 *
	 * @param cdIndicadorAgendaTitulo the cd indicador agenda titulo
	 */
	public void setCdIndicadorAgendaTitulo(String cdIndicadorAgendaTitulo) {
		this.cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
	}

	/**
	 * Get: cdIndicadorAutorizacaoCliente.
	 *
	 * @return cdIndicadorAutorizacaoCliente
	 */
	public String getCdIndicadorAutorizacaoCliente() {
		return cdIndicadorAutorizacaoCliente;
	}

	/**
	 * Set: cdIndicadorAutorizacaoCliente.
	 *
	 * @param cdIndicadorAutorizacaoCliente the cd indicador autorizacao cliente
	 */
	public void setCdIndicadorAutorizacaoCliente(String cdIndicadorAutorizacaoCliente) {
		this.cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
	}

	/**
	 * Get: cdIndicadorAutorizacaoComplemento.
	 *
	 * @return cdIndicadorAutorizacaoComplemento
	 */
	public String getCdIndicadorAutorizacaoComplemento() {
		return cdIndicadorAutorizacaoComplemento;
	}

	/**
	 * Set: cdIndicadorAutorizacaoComplemento.
	 *
	 * @param cdIndicadorAutorizacaoComplemento the cd indicador autorizacao complemento
	 */
	public void setCdIndicadorAutorizacaoComplemento(String cdIndicadorAutorizacaoComplemento) {
		this.cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
	}

	/**
	 * Get: cdIndicadorCadastroOrg.
	 *
	 * @return cdIndicadorCadastroOrg
	 */
	public String getCdIndicadorCadastroOrg() {
		return cdIndicadorCadastroOrg;
	}

	/**
	 * Set: cdIndicadorCadastroOrg.
	 *
	 * @param cdIndicadorCadastroOrg the cd indicador cadastro org
	 */
	public void setCdIndicadorCadastroOrg(String cdIndicadorCadastroOrg) {
		this.cdIndicadorCadastroOrg = cdIndicadorCadastroOrg;
	}

	/**
	 * Get: cdIndicadorCadastroProcd.
	 *
	 * @return cdIndicadorCadastroProcd
	 */
	public String getCdIndicadorCadastroProcd() {
		return cdIndicadorCadastroProcd;
	}

	/**
	 * Set: cdIndicadorCadastroProcd.
	 *
	 * @param cdIndicadorCadastroProcd the cd indicador cadastro procd
	 */
	public void setCdIndicadorCadastroProcd(String cdIndicadorCadastroProcd) {
		this.cdIndicadorCadastroProcd = cdIndicadorCadastroProcd;
	}

	/**
	 * Get: cdIndicadorCataoSalario.
	 *
	 * @return cdIndicadorCataoSalario
	 */
	public String getCdIndicadorCataoSalario() {
		return cdIndicadorCataoSalario;
	}

	/**
	 * Set: cdIndicadorCataoSalario.
	 *
	 * @param cdIndicadorCataoSalario the cd indicador catao salario
	 */
	public void setCdIndicadorCataoSalario(String cdIndicadorCataoSalario) {
		this.cdIndicadorCataoSalario = cdIndicadorCataoSalario;
	}

	/**
	 * Get: cdIndicadorEconomicoReajuste.
	 *
	 * @return cdIndicadorEconomicoReajuste
	 */
	public String getCdIndicadorEconomicoReajuste() {
		return cdIndicadorEconomicoReajuste;
	}

	/**
	 * Set: cdIndicadorEconomicoReajuste.
	 *
	 * @param cdIndicadorEconomicoReajuste the cd indicador economico reajuste
	 */
	public void setCdIndicadorEconomicoReajuste(String cdIndicadorEconomicoReajuste) {
		this.cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
	}

	/**
	 * Get: cdIndicadorExpiracaoCredito.
	 *
	 * @return cdIndicadorExpiracaoCredito
	 */
	public String getCdIndicadorExpiracaoCredito() {
		return cdIndicadorExpiracaoCredito;
	}

	/**
	 * Set: cdIndicadorExpiracaoCredito.
	 *
	 * @param cdIndicadorExpiracaoCredito the cd indicador expiracao credito
	 */
	public void setCdIndicadorExpiracaoCredito(String cdIndicadorExpiracaoCredito) {
		this.cdIndicadorExpiracaoCredito = cdIndicadorExpiracaoCredito;
	}

	/**
	 * Get: cdIndicadorLancamentoPagamento.
	 *
	 * @return cdIndicadorLancamentoPagamento
	 */
	public String getCdIndicadorLancamentoPagamento() {
		return cdIndicadorLancamentoPagamento;
	}

	/**
	 * Set: cdIndicadorLancamentoPagamento.
	 *
	 * @param cdIndicadorLancamentoPagamento the cd indicador lancamento pagamento
	 */
	public void setCdIndicadorLancamentoPagamento(String cdIndicadorLancamentoPagamento) {
		this.cdIndicadorLancamentoPagamento = cdIndicadorLancamentoPagamento;
	}

	/**
	 * Get: cdIndicadorMensagemPerso.
	 *
	 * @return cdIndicadorMensagemPerso
	 */
	public String getCdIndicadorMensagemPerso() {
		return cdIndicadorMensagemPerso;
	}

	/**
	 * Set: cdIndicadorMensagemPerso.
	 *
	 * @param cdIndicadorMensagemPerso the cd indicador mensagem perso
	 */
	public void setCdIndicadorMensagemPerso(String cdIndicadorMensagemPerso) {
		this.cdIndicadorMensagemPerso = cdIndicadorMensagemPerso;
	}

	/**
	 * Get: cdIndicadorLancamentoFuturoCredito.
	 *
	 * @return cdIndicadorLancamentoFuturoCredito
	 */
	public String getCdIndicadorLancamentoFuturoCredito() {
		return cdIndicadorLancamentoFuturoCredito;
	}

	/**
	 * Set: cdIndicadorLancamentoFuturoCredito.
	 *
	 * @param cdIndicadorLancamentoFuturoCredito the cd indicador lancamento futuro credito
	 */
	public void setCdIndicadorLancamentoFuturoCredito(String cdIndicadorLancamentoFuturoCredito) {
		this.cdIndicadorLancamentoFuturoCredito = cdIndicadorLancamentoFuturoCredito;
	}

	/**
	 * Get: cdIndicadorLancamentoFuturoDebito.
	 *
	 * @return cdIndicadorLancamentoFuturoDebito
	 */
	public String getCdIndicadorLancamentoFuturoDebito() {
		return cdIndicadorLancamentoFuturoDebito;
	}

	/**
	 * Set: cdIndicadorLancamentoFuturoDebito.
	 *
	 * @param cdIndicadorLancamentoFuturoDebito the cd indicador lancamento futuro debito
	 */
	public void setCdIndicadorLancamentoFuturoDebito(String cdIndicadorLancamentoFuturoDebito) {
		this.cdIndicadorLancamentoFuturoDebito = cdIndicadorLancamentoFuturoDebito;
	}

	/**
	 * Get: cdIndicadorLiberacaoLoteProcesso.
	 *
	 * @return cdIndicadorLiberacaoLoteProcesso
	 */
	public String getCdIndicadorLiberacaoLoteProcesso() {
		return cdIndicadorLiberacaoLoteProcesso;
	}

	/**
	 * Set: cdIndicadorLiberacaoLoteProcesso.
	 *
	 * @param cdIndicadorLiberacaoLoteProcesso the cd indicador liberacao lote processo
	 */
	public void setCdIndicadorLiberacaoLoteProcesso(String cdIndicadorLiberacaoLoteProcesso) {
		this.cdIndicadorLiberacaoLoteProcesso = cdIndicadorLiberacaoLoteProcesso;
	}

	/**
	 * Get: cdIndicadorManutencaoBaseRecadastro.
	 *
	 * @return cdIndicadorManutencaoBaseRecadastro
	 */
	public String getCdIndicadorManutencaoBaseRecadastro() {
		return cdIndicadorManutencaoBaseRecadastro;
	}

	/**
	 * Set: cdIndicadorManutencaoBaseRecadastro.
	 *
	 * @param cdIndicadorManutencaoBaseRecadastro the cd indicador manutencao base recadastro
	 */
	public void setCdIndicadorManutencaoBaseRecadastro(String cdIndicadorManutencaoBaseRecadastro) {
		this.cdIndicadorManutencaoBaseRecadastro = cdIndicadorManutencaoBaseRecadastro;
	}

	/**
	 * Get: cdIndicadorMidiaDisponivel.
	 *
	 * @return cdIndicadorMidiaDisponivel
	 */
	public String getCdIndicadorMidiaDisponivel() {
		return cdIndicadorMidiaDisponivel;
	}

	/**
	 * Set: cdIndicadorMidiaDisponivel.
	 *
	 * @param cdIndicadorMidiaDisponivel the cd indicador midia disponivel
	 */
	public void setCdIndicadorMidiaDisponivel(String cdIndicadorMidiaDisponivel) {
		this.cdIndicadorMidiaDisponivel = cdIndicadorMidiaDisponivel;
	}

	/**
	 * Get: cdIndicadorMidiaMensagemRecadastro.
	 *
	 * @return cdIndicadorMidiaMensagemRecadastro
	 */
	public String getCdIndicadorMidiaMensagemRecadastro() {
		return cdIndicadorMidiaMensagemRecadastro;
	}

	/**
	 * Set: cdIndicadorMidiaMensagemRecadastro.
	 *
	 * @param cdIndicadorMidiaMensagemRecadastro the cd indicador midia mensagem recadastro
	 */
	public void setCdIndicadorMidiaMensagemRecadastro(String cdIndicadorMidiaMensagemRecadastro) {
		this.cdIndicadorMidiaMensagemRecadastro = cdIndicadorMidiaMensagemRecadastro;
	}

	/**
	 * Get: cdIndicadorMomentoAvisoRacadastro.
	 *
	 * @return cdIndicadorMomentoAvisoRacadastro
	 */
	public String getCdIndicadorMomentoAvisoRacadastro() {
		return cdIndicadorMomentoAvisoRacadastro;
	}

	/**
	 * Set: cdIndicadorMomentoAvisoRacadastro.
	 *
	 * @param cdIndicadorMomentoAvisoRacadastro the cd indicador momento aviso racadastro
	 */
	public void setCdIndicadorMomentoAvisoRacadastro(String cdIndicadorMomentoAvisoRacadastro) {
		this.cdIndicadorMomentoAvisoRacadastro = cdIndicadorMomentoAvisoRacadastro;
	}

	/**
	 * Get: cdIndicadorMomentoCreditoEfetivacao.
	 *
	 * @return cdIndicadorMomentoCreditoEfetivacao
	 */
	public String getCdIndicadorMomentoCreditoEfetivacao() {
		return cdIndicadorMomentoCreditoEfetivacao;
	}

	/**
	 * Set: cdIndicadorMomentoCreditoEfetivacao.
	 *
	 * @param cdIndicadorMomentoCreditoEfetivacao the cd indicador momento credito efetivacao
	 */
	public void setCdIndicadorMomentoCreditoEfetivacao(String cdIndicadorMomentoCreditoEfetivacao) {
		this.cdIndicadorMomentoCreditoEfetivacao = cdIndicadorMomentoCreditoEfetivacao;
	}

	/**
	 * Get: cdIndicadorMomentoDebitoPagamento.
	 *
	 * @return cdIndicadorMomentoDebitoPagamento
	 */
	public String getCdIndicadorMomentoDebitoPagamento() {
		return cdIndicadorMomentoDebitoPagamento;
	}

	/**
	 * Set: cdIndicadorMomentoDebitoPagamento.
	 *
	 * @param cdIndicadorMomentoDebitoPagamento the cd indicador momento debito pagamento
	 */
	public void setCdIndicadorMomentoDebitoPagamento(String cdIndicadorMomentoDebitoPagamento) {
		this.cdIndicadorMomentoDebitoPagamento = cdIndicadorMomentoDebitoPagamento;
	}

	/**
	 * Get: cdIndicadorMomentoFormularioRecadastro.
	 *
	 * @return cdIndicadorMomentoFormularioRecadastro
	 */
	public String getCdIndicadorMomentoFormularioRecadastro() {
		return cdIndicadorMomentoFormularioRecadastro;
	}

	/**
	 * Set: cdIndicadorMomentoFormularioRecadastro.
	 *
	 * @param cdIndicadorMomentoFormularioRecadastro the cd indicador momento formulario recadastro
	 */
	public void setCdIndicadorMomentoFormularioRecadastro(String cdIndicadorMomentoFormularioRecadastro) {
		this.cdIndicadorMomentoFormularioRecadastro = cdIndicadorMomentoFormularioRecadastro;
	}

	/**
	 * Get: cdIndicadorMomentoProcessamentoPagamento.
	 *
	 * @return cdIndicadorMomentoProcessamentoPagamento
	 */
	public String getCdIndicadorMomentoProcessamentoPagamento() {
		return cdIndicadorMomentoProcessamentoPagamento;
	}

	/**
	 * Set: cdIndicadorMomentoProcessamentoPagamento.
	 *
	 * @param cdIndicadorMomentoProcessamentoPagamento the cd indicador momento processamento pagamento
	 */
	public void setCdIndicadorMomentoProcessamentoPagamento(
			String cdIndicadorMomentoProcessamentoPagamento) {
		this.cdIndicadorMomentoProcessamentoPagamento = cdIndicadorMomentoProcessamentoPagamento;
	}

	/**
	 * Get: cdIndicadorMensagemRecadastroMidia.
	 *
	 * @return cdIndicadorMensagemRecadastroMidia
	 */
	public String getCdIndicadorMensagemRecadastroMidia() {
		return cdIndicadorMensagemRecadastroMidia;
	}

	/**
	 * Set: cdIndicadorMensagemRecadastroMidia.
	 *
	 * @param cdIndicadorMensagemRecadastroMidia the cd indicador mensagem recadastro midia
	 */
	public void setCdIndicadorMensagemRecadastroMidia(String cdIndicadorMensagemRecadastroMidia) {
		this.cdIndicadorMensagemRecadastroMidia = cdIndicadorMensagemRecadastroMidia;
	}

	/**
	 * Get: cdIndicadorPermissaoDebitoOnline.
	 *
	 * @return cdIndicadorPermissaoDebitoOnline
	 */
	public String getCdIndicadorPermissaoDebitoOnline() {
		return cdIndicadorPermissaoDebitoOnline;
	}

	/**
	 * Set: cdIndicadorPermissaoDebitoOnline.
	 *
	 * @param cdIndicadorPermissaoDebitoOnline the cd indicador permissao debito online
	 */
	public void setCdIndicadorPermissaoDebitoOnline(String cdIndicadorPermissaoDebitoOnline) {
		this.cdIndicadorPermissaoDebitoOnline = cdIndicadorPermissaoDebitoOnline;
	}

	/**
	 * Get: cdIndicadorNaturezaOperacaoPagamento.
	 *
	 * @return cdIndicadorNaturezaOperacaoPagamento
	 */
	public String getCdIndicadorNaturezaOperacaoPagamento() {
		return cdIndicadorNaturezaOperacaoPagamento;
	}

	/**
	 * Set: cdIndicadorNaturezaOperacaoPagamento.
	 *
	 * @param cdIndicadorNaturezaOperacaoPagamento the cd indicador natureza operacao pagamento
	 */
	public void setCdIndicadorNaturezaOperacaoPagamento(String cdIndicadorNaturezaOperacaoPagamento) {
		this.cdIndicadorNaturezaOperacaoPagamento = cdIndicadorNaturezaOperacaoPagamento;
	}

	/**
	 * Get: cdIndicadorPeriodicidadeAviso.
	 *
	 * @return cdIndicadorPeriodicidadeAviso
	 */
	public String getCdIndicadorPeriodicidadeAviso() {
		return cdIndicadorPeriodicidadeAviso;
	}

	/**
	 * Set: cdIndicadorPeriodicidadeAviso.
	 *
	 * @param cdIndicadorPeriodicidadeAviso the cd indicador periodicidade aviso
	 */
	public void setCdIndicadorPeriodicidadeAviso(String cdIndicadorPeriodicidadeAviso) {
		this.cdIndicadorPeriodicidadeAviso = cdIndicadorPeriodicidadeAviso;
	}

	/**
	 * Get: cdIndicadorPerdcCobrancaTarifa.
	 *
	 * @return cdIndicadorPerdcCobrancaTarifa
	 */
	public String getCdIndicadorPerdcCobrancaTarifa() {
		return cdIndicadorPerdcCobrancaTarifa;
	}

	/**
	 * Set: cdIndicadorPerdcCobrancaTarifa.
	 *
	 * @param cdIndicadorPerdcCobrancaTarifa the cd indicador perdc cobranca tarifa
	 */
	public void setCdIndicadorPerdcCobrancaTarifa(String cdIndicadorPerdcCobrancaTarifa) {
		this.cdIndicadorPerdcCobrancaTarifa = cdIndicadorPerdcCobrancaTarifa;
	}

	/**
	 * Get: cdIndicadorPerdcComprovante.
	 *
	 * @return cdIndicadorPerdcComprovante
	 */
	public String getCdIndicadorPerdcComprovante() {
		return cdIndicadorPerdcComprovante;
	}

	/**
	 * Set: cdIndicadorPerdcComprovante.
	 *
	 * @param cdIndicadorPerdcComprovante the cd indicador perdc comprovante
	 */
	public void setCdIndicadorPerdcComprovante(String cdIndicadorPerdcComprovante) {
		this.cdIndicadorPerdcComprovante = cdIndicadorPerdcComprovante;
	}

	/**
	 * Get: cdIndicadorPerdcConsultaVeiculo.
	 *
	 * @return cdIndicadorPerdcConsultaVeiculo
	 */
	public String getCdIndicadorPerdcConsultaVeiculo() {
		return cdIndicadorPerdcConsultaVeiculo;
	}

	/**
	 * Set: cdIndicadorPerdcConsultaVeiculo.
	 *
	 * @param cdIndicadorPerdcConsultaVeiculo the cd indicador perdc consulta veiculo
	 */
	public void setCdIndicadorPerdcConsultaVeiculo(String cdIndicadorPerdcConsultaVeiculo) {
		this.cdIndicadorPerdcConsultaVeiculo = cdIndicadorPerdcConsultaVeiculo;
	}

	/**
	 * Get: cdIndicadorPerdcEnvioRemessa.
	 *
	 * @return cdIndicadorPerdcEnvioRemessa
	 */
	public String getCdIndicadorPerdcEnvioRemessa() {
		return cdIndicadorPerdcEnvioRemessa;
	}

	/**
	 * Set: cdIndicadorPerdcEnvioRemessa.
	 *
	 * @param cdIndicadorPerdcEnvioRemessa the cd indicador perdc envio remessa
	 */
	public void setCdIndicadorPerdcEnvioRemessa(String cdIndicadorPerdcEnvioRemessa) {
		this.cdIndicadorPerdcEnvioRemessa = cdIndicadorPerdcEnvioRemessa;
	}

	/**
	 * Get: cdIndicadorPerdcManutencaoProcd.
	 *
	 * @return cdIndicadorPerdcManutencaoProcd
	 */
	public String getCdIndicadorPerdcManutencaoProcd() {
		return cdIndicadorPerdcManutencaoProcd;
	}

	/**
	 * Set: cdIndicadorPerdcManutencaoProcd.
	 *
	 * @param cdIndicadorPerdcManutencaoProcd the cd indicador perdc manutencao procd
	 */
	public void setCdIndicadorPerdcManutencaoProcd(String cdIndicadorPerdcManutencaoProcd) {
		this.cdIndicadorPerdcManutencaoProcd = cdIndicadorPerdcManutencaoProcd;
	}

	/**
	 * Get: cdIndicadorPagamentoNaoUtilizado.
	 *
	 * @return cdIndicadorPagamentoNaoUtilizado
	 */
	public String getCdIndicadorPagamentoNaoUtilizado() {
		return cdIndicadorPagamentoNaoUtilizado;
	}

	/**
	 * Set: cdIndicadorPagamentoNaoUtilizado.
	 *
	 * @param cdIndicadorPagamentoNaoUtilizado the cd indicador pagamento nao utilizado
	 */
	public void setCdIndicadorPagamentoNaoUtilizado(String cdIndicadorPagamentoNaoUtilizado) {
		this.cdIndicadorPagamentoNaoUtilizado = cdIndicadorPagamentoNaoUtilizado;
	}

	/**
	 * Get: cdIndicadorPrincipalEnquaRecadastro.
	 *
	 * @return cdIndicadorPrincipalEnquaRecadastro
	 */
	public String getCdIndicadorPrincipalEnquaRecadastro() {
		return cdIndicadorPrincipalEnquaRecadastro;
	}

	/**
	 * Set: cdIndicadorPrincipalEnquaRecadastro.
	 *
	 * @param cdIndicadorPrincipalEnquaRecadastro the cd indicador principal enqua recadastro
	 */
	public void setCdIndicadorPrincipalEnquaRecadastro(String cdIndicadorPrincipalEnquaRecadastro) {
		this.cdIndicadorPrincipalEnquaRecadastro = cdIndicadorPrincipalEnquaRecadastro;
	}

	/**
	 * Get: cdIndicadorPrioridadeEfetivacaoPagamento.
	 *
	 * @return cdIndicadorPrioridadeEfetivacaoPagamento
	 */
	public String getCdIndicadorPrioridadeEfetivacaoPagamento() {
		return cdIndicadorPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Set: cdIndicadorPrioridadeEfetivacaoPagamento.
	 *
	 * @param cdIndicadorPrioridadeEfetivacaoPagamento the cd indicador prioridade efetivacao pagamento
	 */
	public void setCdIndicadorPrioridadeEfetivacaoPagamento(String cdIndicadorPrioridadeEfetivacaoPagamento) {
		this.cdIndicadorPrioridadeEfetivacaoPagamento = cdIndicadorPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Get: cdIndicadorRejeicaoAgendaLote.
	 *
	 * @return cdIndicadorRejeicaoAgendaLote
	 */
	public String getCdIndicadorRejeicaoAgendaLote() {
		return cdIndicadorRejeicaoAgendaLote;
	}

	/**
	 * Set: cdIndicadorRejeicaoAgendaLote.
	 *
	 * @param cdIndicadorRejeicaoAgendaLote the cd indicador rejeicao agenda lote
	 */
	public void setCdIndicadorRejeicaoAgendaLote(String cdIndicadorRejeicaoAgendaLote) {
		this.cdIndicadorRejeicaoAgendaLote = cdIndicadorRejeicaoAgendaLote;
	}

	/**
	 * Get: cdIndicadorRejeicaoEfetivacaoLote.
	 *
	 * @return cdIndicadorRejeicaoEfetivacaoLote
	 */
	public String getCdIndicadorRejeicaoEfetivacaoLote() {
		return cdIndicadorRejeicaoEfetivacaoLote;
	}

	/**
	 * Set: cdIndicadorRejeicaoEfetivacaoLote.
	 *
	 * @param cdIndicadorRejeicaoEfetivacaoLote the cd indicador rejeicao efetivacao lote
	 */
	public void setCdIndicadorRejeicaoEfetivacaoLote(String cdIndicadorRejeicaoEfetivacaoLote) {
		this.cdIndicadorRejeicaoEfetivacaoLote = cdIndicadorRejeicaoEfetivacaoLote;
	}

	/**
	 * Get: cdIndicadorRejeicaoLote.
	 *
	 * @return cdIndicadorRejeicaoLote
	 */
	public String getCdIndicadorRejeicaoLote() {
		return cdIndicadorRejeicaoLote;
	}

	/**
	 * Set: cdIndicadorRejeicaoLote.
	 *
	 * @param cdIndicadorRejeicaoLote the cd indicador rejeicao lote
	 */
	public void setCdIndicadorRejeicaoLote(String cdIndicadorRejeicaoLote) {
		this.cdIndicadorRejeicaoLote = cdIndicadorRejeicaoLote;
	}

	/**
	 * Get: cdIndicadorRastreabilidadeNotaFiscal.
	 *
	 * @return cdIndicadorRastreabilidadeNotaFiscal
	 */
	public String getCdIndicadorRastreabilidadeNotaFiscal() {
		return cdIndicadorRastreabilidadeNotaFiscal;
	}

	/**
	 * Set: cdIndicadorRastreabilidadeNotaFiscal.
	 *
	 * @param cdIndicadorRastreabilidadeNotaFiscal the cd indicador rastreabilidade nota fiscal
	 */
	public void setCdIndicadorRastreabilidadeNotaFiscal(String cdIndicadorRastreabilidadeNotaFiscal) {
		this.cdIndicadorRastreabilidadeNotaFiscal = cdIndicadorRastreabilidadeNotaFiscal;
	}

	/**
	 * Get: cdIndicadorRastreabilidadeTituloTerceiro.
	 *
	 * @return cdIndicadorRastreabilidadeTituloTerceiro
	 */
	public String getCdIndicadorRastreabilidadeTituloTerceiro() {
		return cdIndicadorRastreabilidadeTituloTerceiro;
	}

	/**
	 * Set: cdIndicadorRastreabilidadeTituloTerceiro.
	 *
	 * @param cdIndicadorRastreabilidadeTituloTerceiro the cd indicador rastreabilidade titulo terceiro
	 */
	public void setCdIndicadorRastreabilidadeTituloTerceiro(String cdIndicadorRastreabilidadeTituloTerceiro) {
		this.cdIndicadorRastreabilidadeTituloTerceiro = cdIndicadorRastreabilidadeTituloTerceiro;
	}

	/**
	 * Get: cdIndicadorTipoCargaRecadastro.
	 *
	 * @return cdIndicadorTipoCargaRecadastro
	 */
	public String getCdIndicadorTipoCargaRecadastro() {
		return cdIndicadorTipoCargaRecadastro;
	}

	/**
	 * Set: cdIndicadorTipoCargaRecadastro.
	 *
	 * @param cdIndicadorTipoCargaRecadastro the cd indicador tipo carga recadastro
	 */
	public void setCdIndicadorTipoCargaRecadastro(String cdIndicadorTipoCargaRecadastro) {
		this.cdIndicadorTipoCargaRecadastro = cdIndicadorTipoCargaRecadastro;
	}

	/**
	 * Get: cdIndicadorTipoCataoSalario.
	 *
	 * @return cdIndicadorTipoCataoSalario
	 */
	public String getCdIndicadorTipoCataoSalario() {
		return cdIndicadorTipoCataoSalario;
	}

	/**
	 * Set: cdIndicadorTipoCataoSalario.
	 *
	 * @param cdIndicadorTipoCataoSalario the cd indicador tipo catao salario
	 */
	public void setCdIndicadorTipoCataoSalario(String cdIndicadorTipoCataoSalario) {
		this.cdIndicadorTipoCataoSalario = cdIndicadorTipoCataoSalario;
	}

	/**
	 * Get: cdIndicadorTipoDataFloat.
	 *
	 * @return cdIndicadorTipoDataFloat
	 */
	public String getCdIndicadorTipoDataFloat() {
		return cdIndicadorTipoDataFloat;
	}

	/**
	 * Set: cdIndicadorTipoDataFloat.
	 *
	 * @param cdIndicadorTipoDataFloat the cd indicador tipo data float
	 */
	public void setCdIndicadorTipoDataFloat(String cdIndicadorTipoDataFloat) {
		this.cdIndicadorTipoDataFloat = cdIndicadorTipoDataFloat;
	}

	/**
	 * Get: cdIndicadorTipoDivergenciaVeiculo.
	 *
	 * @return cdIndicadorTipoDivergenciaVeiculo
	 */
	public String getCdIndicadorTipoDivergenciaVeiculo() {
		return cdIndicadorTipoDivergenciaVeiculo;
	}

	/**
	 * Set: cdIndicadorTipoDivergenciaVeiculo.
	 *
	 * @param cdIndicadorTipoDivergenciaVeiculo the cd indicador tipo divergencia veiculo
	 */
	public void setCdIndicadorTipoDivergenciaVeiculo(String cdIndicadorTipoDivergenciaVeiculo) {
		this.cdIndicadorTipoDivergenciaVeiculo = cdIndicadorTipoDivergenciaVeiculo;
	}

	/**
	 * Get: cdIndicadorTipoIdBeneficio.
	 *
	 * @return cdIndicadorTipoIdBeneficio
	 */
	public String getCdIndicadorTipoIdBeneficio() {
		return cdIndicadorTipoIdBeneficio;
	}

	/**
	 * Set: cdIndicadorTipoIdBeneficio.
	 *
	 * @param cdIndicadorTipoIdBeneficio the cd indicador tipo id beneficio
	 */
	public void setCdIndicadorTipoIdBeneficio(String cdIndicadorTipoIdBeneficio) {
		this.cdIndicadorTipoIdBeneficio = cdIndicadorTipoIdBeneficio;
	}

	/**
	 * Get: cdIndicadorTipoLayoutArquivo.
	 *
	 * @return cdIndicadorTipoLayoutArquivo
	 */
	public String getCdIndicadorTipoLayoutArquivo() {
		return cdIndicadorTipoLayoutArquivo;
	}

	/**
	 * Set: cdIndicadorTipoLayoutArquivo.
	 *
	 * @param cdIndicadorTipoLayoutArquivo the cd indicador tipo layout arquivo
	 */
	public void setCdIndicadorTipoLayoutArquivo(String cdIndicadorTipoLayoutArquivo) {
		this.cdIndicadorTipoLayoutArquivo = cdIndicadorTipoLayoutArquivo;
	}

	/**
	 * Get: cdIndicadorTipoReajusteTarifa.
	 *
	 * @return cdIndicadorTipoReajusteTarifa
	 */
	public String getCdIndicadorTipoReajusteTarifa() {
		return cdIndicadorTipoReajusteTarifa;
	}

	/**
	 * Set: cdIndicadorTipoReajusteTarifa.
	 *
	 * @param cdIndicadorTipoReajusteTarifa the cd indicador tipo reajuste tarifa
	 */
	public void setCdIndicadorTipoReajusteTarifa(String cdIndicadorTipoReajusteTarifa) {
		this.cdIndicadorTipoReajusteTarifa = cdIndicadorTipoReajusteTarifa;
	}

	/**
	 * Get: cdIndicadorContratoContaTransferencia.
	 *
	 * @return cdIndicadorContratoContaTransferencia
	 */
	public String getCdIndicadorContratoContaTransferencia() {
		return cdIndicadorContratoContaTransferencia;
	}

	/**
	 * Set: cdIndicadorContratoContaTransferencia.
	 *
	 * @param cdIndicadorContratoContaTransferencia the cd indicador contrato conta transferencia
	 */
	public void setCdIndicadorContratoContaTransferencia(String cdIndicadorContratoContaTransferencia) {
		this.cdIndicadorContratoContaTransferencia = cdIndicadorContratoContaTransferencia;
	}

	/**
	 * Get: cdIndicadorUtilizacaoFavorecidoControle.
	 *
	 * @return cdIndicadorUtilizacaoFavorecidoControle
	 */
	public String getCdIndicadorUtilizacaoFavorecidoControle() {
		return cdIndicadorUtilizacaoFavorecidoControle;
	}

	/**
	 * Set: cdIndicadorUtilizacaoFavorecidoControle.
	 *
	 * @param cdIndicadorUtilizacaoFavorecidoControle the cd indicador utilizacao favorecido controle
	 */
	public void setCdIndicadorUtilizacaoFavorecidoControle(String cdIndicadorUtilizacaoFavorecidoControle) {
		this.cdIndicadorUtilizacaoFavorecidoControle = cdIndicadorUtilizacaoFavorecidoControle;
	}

	/**
	 * Get: dtIndicadorEnquaContaSalario.
	 *
	 * @return dtIndicadorEnquaContaSalario
	 */
	public String getDtIndicadorEnquaContaSalario() {
		return dtIndicadorEnquaContaSalario;
	}

	/**
	 * Set: dtIndicadorEnquaContaSalario.
	 *
	 * @param dtIndicadorEnquaContaSalario the dt indicador enqua conta salario
	 */
	public void setDtIndicadorEnquaContaSalario(String dtIndicadorEnquaContaSalario) {
		this.dtIndicadorEnquaContaSalario = dtIndicadorEnquaContaSalario;
	}

	/**
	 * Get: dtIndicadorInicioBloqueioPplta.
	 *
	 * @return dtIndicadorInicioBloqueioPplta
	 */
	public String getDtIndicadorInicioBloqueioPplta() {
		return dtIndicadorInicioBloqueioPplta;
	}

	/**
	 * Set: dtIndicadorInicioBloqueioPplta.
	 *
	 * @param dtIndicadorInicioBloqueioPplta the dt indicador inicio bloqueio pplta
	 */
	public void setDtIndicadorInicioBloqueioPplta(String dtIndicadorInicioBloqueioPplta) {
		this.dtIndicadorInicioBloqueioPplta = dtIndicadorInicioBloqueioPplta;
	}

	/**
	 * Get: dtIndicadorInicioRastreabilidadeTitulo.
	 *
	 * @return dtIndicadorInicioRastreabilidadeTitulo
	 */
	public String getDtIndicadorInicioRastreabilidadeTitulo() {
		return dtIndicadorInicioRastreabilidadeTitulo;
	}

	/**
	 * Set: dtIndicadorInicioRastreabilidadeTitulo.
	 *
	 * @param dtIndicadorInicioRastreabilidadeTitulo the dt indicador inicio rastreabilidade titulo
	 */
	public void setDtIndicadorInicioRastreabilidadeTitulo(String dtIndicadorInicioRastreabilidadeTitulo) {
		this.dtIndicadorInicioRastreabilidadeTitulo = dtIndicadorInicioRastreabilidadeTitulo;
	}

	/**
	 * Get: dtIndicadorFimAcertoRecadastro.
	 *
	 * @return dtIndicadorFimAcertoRecadastro
	 */
	public String getDtIndicadorFimAcertoRecadastro() {
		return dtIndicadorFimAcertoRecadastro;
	}

	/**
	 * Set: dtIndicadorFimAcertoRecadastro.
	 *
	 * @param dtIndicadorFimAcertoRecadastro the dt indicador fim acerto recadastro
	 */
	public void setDtIndicadorFimAcertoRecadastro(String dtIndicadorFimAcertoRecadastro) {
		this.dtIndicadorFimAcertoRecadastro = dtIndicadorFimAcertoRecadastro;
	}

	/**
	 * Get: dtIndicadorFimRecadastroBeneficio.
	 *
	 * @return dtIndicadorFimRecadastroBeneficio
	 */
	public String getDtIndicadorFimRecadastroBeneficio() {
		return dtIndicadorFimRecadastroBeneficio;
	}

	/**
	 * Set: dtIndicadorFimRecadastroBeneficio.
	 *
	 * @param dtIndicadorFimRecadastroBeneficio the dt indicador fim recadastro beneficio
	 */
	public void setDtIndicadorFimRecadastroBeneficio(String dtIndicadorFimRecadastroBeneficio) {
		this.dtIndicadorFimRecadastroBeneficio = dtIndicadorFimRecadastroBeneficio;
	}

	/**
	 * Get: dtIndicadorinicioAcertoRecadastro.
	 *
	 * @return dtIndicadorinicioAcertoRecadastro
	 */
	public String getDtIndicadorinicioAcertoRecadastro() {
		return dtIndicadorinicioAcertoRecadastro;
	}

	/**
	 * Set: dtIndicadorinicioAcertoRecadastro.
	 *
	 * @param dtIndicadorinicioAcertoRecadastro the dt indicadorinicio acerto recadastro
	 */
	public void setDtIndicadorinicioAcertoRecadastro(String dtIndicadorinicioAcertoRecadastro) {
		this.dtIndicadorinicioAcertoRecadastro = dtIndicadorinicioAcertoRecadastro;
	}

	/**
	 * Get: dtIndicadorInicioRecadastroBeneficio.
	 *
	 * @return dtIndicadorInicioRecadastroBeneficio
	 */
	public String getDtIndicadorInicioRecadastroBeneficio() {
		return dtIndicadorInicioRecadastroBeneficio;
	}

	/**
	 * Set: dtIndicadorInicioRecadastroBeneficio.
	 *
	 * @param dtIndicadorInicioRecadastroBeneficio the dt indicador inicio recadastro beneficio
	 */
	public void setDtIndicadorInicioRecadastroBeneficio(String dtIndicadorInicioRecadastroBeneficio) {
		this.dtIndicadorInicioRecadastroBeneficio = dtIndicadorInicioRecadastroBeneficio;
	}

	/**
	 * Get: dtIndicadorLimiteVinculoCarga.
	 *
	 * @return dtIndicadorLimiteVinculoCarga
	 */
	public String getDtIndicadorLimiteVinculoCarga() {
		return dtIndicadorLimiteVinculoCarga;
	}

	/**
	 * Set: dtIndicadorLimiteVinculoCarga.
	 *
	 * @param dtIndicadorLimiteVinculoCarga the dt indicador limite vinculo carga
	 */
	public void setDtIndicadorLimiteVinculoCarga(String dtIndicadorLimiteVinculoCarga) {
		this.dtIndicadorLimiteVinculoCarga = dtIndicadorLimiteVinculoCarga;
	}

	/**
	 * Get: nrIndicadorFechamentoApuracaoTarifa.
	 *
	 * @return nrIndicadorFechamentoApuracaoTarifa
	 */
	public String getNrIndicadorFechamentoApuracaoTarifa() {
		return nrIndicadorFechamentoApuracaoTarifa;
	}

	/**
	 * Set: nrIndicadorFechamentoApuracaoTarifa.
	 *
	 * @param nrIndicadorFechamentoApuracaoTarifa the nr indicador fechamento apuracao tarifa
	 */
	public void setNrIndicadorFechamentoApuracaoTarifa(String nrIndicadorFechamentoApuracaoTarifa) {
		this.nrIndicadorFechamentoApuracaoTarifa = nrIndicadorFechamentoApuracaoTarifa;
	}

	/**
	 * Get: cdIndicadorPercentualIndicadorReajusteTarifa.
	 *
	 * @return cdIndicadorPercentualIndicadorReajusteTarifa
	 */
	public String getCdIndicadorPercentualIndicadorReajusteTarifa() {
		return cdIndicadorPercentualIndicadorReajusteTarifa;
	}

	/**
	 * Set: cdIndicadorPercentualIndicadorReajusteTarifa.
	 *
	 * @param cdIndicadorPercentualIndicadorReajusteTarifa the cd indicador percentual indicador reajuste tarifa
	 */
	public void setCdIndicadorPercentualIndicadorReajusteTarifa(
			String cdIndicadorPercentualIndicadorReajusteTarifa) {
		this.cdIndicadorPercentualIndicadorReajusteTarifa = cdIndicadorPercentualIndicadorReajusteTarifa;
	}

	/**
	 * Get: cdIndicadorPercentualMaximoInconLote.
	 *
	 * @return cdIndicadorPercentualMaximoInconLote
	 */
	public String getCdIndicadorPercentualMaximoInconLote() {
		return cdIndicadorPercentualMaximoInconLote;
	}

	/**
	 * Set: cdIndicadorPercentualMaximoInconLote.
	 *
	 * @param cdIndicadorPercentualMaximoInconLote the cd indicador percentual maximo incon lote
	 */
	public void setCdIndicadorPercentualMaximoInconLote(String cdIndicadorPercentualMaximoInconLote) {
		this.cdIndicadorPercentualMaximoInconLote = cdIndicadorPercentualMaximoInconLote;
	}

	/**
	 * Get: cdIndicadorPercentualReducaoTarifaCatalogo.
	 *
	 * @return cdIndicadorPercentualReducaoTarifaCatalogo
	 */
	public String getCdIndicadorPercentualReducaoTarifaCatalogo() {
		return cdIndicadorPercentualReducaoTarifaCatalogo;
	}

	/**
	 * Set: cdIndicadorPercentualReducaoTarifaCatalogo.
	 *
	 * @param cdIndicadorPercentualReducaoTarifaCatalogo the cd indicador percentual reducao tarifa catalogo
	 */
	public void setCdIndicadorPercentualReducaoTarifaCatalogo(
			String cdIndicadorPercentualReducaoTarifaCatalogo) {
		this.cdIndicadorPercentualReducaoTarifaCatalogo = cdIndicadorPercentualReducaoTarifaCatalogo;
	}

	/**
	 * Get: qtIndicadorAntecedencia.
	 *
	 * @return qtIndicadorAntecedencia
	 */
	public String getQtIndicadorAntecedencia() {
		return qtIndicadorAntecedencia;
	}

	/**
	 * Set: qtIndicadorAntecedencia.
	 *
	 * @param qtIndicadorAntecedencia the qt indicador antecedencia
	 */
	public void setQtIndicadorAntecedencia(String qtIndicadorAntecedencia) {
		this.qtIndicadorAntecedencia = qtIndicadorAntecedencia;
	}

	/**
	 * Get: qtIndicadorAnteriorVencimentoComprovado.
	 *
	 * @return qtIndicadorAnteriorVencimentoComprovado
	 */
	public String getQtIndicadorAnteriorVencimentoComprovado() {
		return qtIndicadorAnteriorVencimentoComprovado;
	}

	/**
	 * Set: qtIndicadorAnteriorVencimentoComprovado.
	 *
	 * @param qtIndicadorAnteriorVencimentoComprovado the qt indicador anterior vencimento comprovado
	 */
	public void setQtIndicadorAnteriorVencimentoComprovado(String qtIndicadorAnteriorVencimentoComprovado) {
		this.qtIndicadorAnteriorVencimentoComprovado = qtIndicadorAnteriorVencimentoComprovado;
	}

	/**
	 * Get: qtIndicadorDiaCobrancaTarifa.
	 *
	 * @return qtIndicadorDiaCobrancaTarifa
	 */
	public String getQtIndicadorDiaCobrancaTarifa() {
		return qtIndicadorDiaCobrancaTarifa;
	}

	/**
	 * Set: qtIndicadorDiaCobrancaTarifa.
	 *
	 * @param qtIndicadorDiaCobrancaTarifa the qt indicador dia cobranca tarifa
	 */
	public void setQtIndicadorDiaCobrancaTarifa(String qtIndicadorDiaCobrancaTarifa) {
		this.qtIndicadorDiaCobrancaTarifa = qtIndicadorDiaCobrancaTarifa;
	}

	/**
	 * Get: qtIndicadorDiaExpiracao.
	 *
	 * @return qtIndicadorDiaExpiracao
	 */
	public String getQtIndicadorDiaExpiracao() {
		return qtIndicadorDiaExpiracao;
	}

	/**
	 * Set: qtIndicadorDiaExpiracao.
	 *
	 * @param qtIndicadorDiaExpiracao the qt indicador dia expiracao
	 */
	public void setQtIndicadorDiaExpiracao(String qtIndicadorDiaExpiracao) {
		this.qtIndicadorDiaExpiracao = qtIndicadorDiaExpiracao;
	}

	/**
	 * Get: cdIndicadorDiaFloatPagamento.
	 *
	 * @return cdIndicadorDiaFloatPagamento
	 */
	public String getCdIndicadorDiaFloatPagamento() {
		return cdIndicadorDiaFloatPagamento;
	}

	/**
	 * Set: cdIndicadorDiaFloatPagamento.
	 *
	 * @param cdIndicadorDiaFloatPagamento the cd indicador dia float pagamento
	 */
	public void setCdIndicadorDiaFloatPagamento(String cdIndicadorDiaFloatPagamento) {
		this.cdIndicadorDiaFloatPagamento = cdIndicadorDiaFloatPagamento;
	}

	/**
	 * Get: qtIndicadorDiaInatividadeFavorecido.
	 *
	 * @return qtIndicadorDiaInatividadeFavorecido
	 */
	public String getQtIndicadorDiaInatividadeFavorecido() {
		return qtIndicadorDiaInatividadeFavorecido;
	}

	/**
	 * Set: qtIndicadorDiaInatividadeFavorecido.
	 *
	 * @param qtIndicadorDiaInatividadeFavorecido the qt indicador dia inatividade favorecido
	 */
	public void setQtIndicadorDiaInatividadeFavorecido(String qtIndicadorDiaInatividadeFavorecido) {
		this.qtIndicadorDiaInatividadeFavorecido = qtIndicadorDiaInatividadeFavorecido;
	}

	/**
	 * Get: qtIndicadorDiaRepiqConsulta.
	 *
	 * @return qtIndicadorDiaRepiqConsulta
	 */
	public String getQtIndicadorDiaRepiqConsulta() {
		return qtIndicadorDiaRepiqConsulta;
	}

	/**
	 * Set: qtIndicadorDiaRepiqConsulta.
	 *
	 * @param qtIndicadorDiaRepiqConsulta the qt indicador dia repiq consulta
	 */
	public void setQtIndicadorDiaRepiqConsulta(String qtIndicadorDiaRepiqConsulta) {
		this.qtIndicadorDiaRepiqConsulta = qtIndicadorDiaRepiqConsulta;
	}

	/**
	 * Get: qtIndicadorEtapasRecadastroBeneficio.
	 *
	 * @return qtIndicadorEtapasRecadastroBeneficio
	 */
	public String getQtIndicadorEtapasRecadastroBeneficio() {
		return qtIndicadorEtapasRecadastroBeneficio;
	}

	/**
	 * Set: qtIndicadorEtapasRecadastroBeneficio.
	 *
	 * @param qtIndicadorEtapasRecadastroBeneficio the qt indicador etapas recadastro beneficio
	 */
	public void setQtIndicadorEtapasRecadastroBeneficio(String qtIndicadorEtapasRecadastroBeneficio) {
		this.qtIndicadorEtapasRecadastroBeneficio = qtIndicadorEtapasRecadastroBeneficio;
	}

	/**
	 * Get: qtIndicadorFaseRecadastroBeneficio.
	 *
	 * @return qtIndicadorFaseRecadastroBeneficio
	 */
	public String getQtIndicadorFaseRecadastroBeneficio() {
		return qtIndicadorFaseRecadastroBeneficio;
	}

	/**
	 * Set: qtIndicadorFaseRecadastroBeneficio.
	 *
	 * @param qtIndicadorFaseRecadastroBeneficio the qt indicador fase recadastro beneficio
	 */
	public void setQtIndicadorFaseRecadastroBeneficio(String qtIndicadorFaseRecadastroBeneficio) {
		this.qtIndicadorFaseRecadastroBeneficio = qtIndicadorFaseRecadastroBeneficio;
	}

	/**
	 * Get: qtIndicadorLimiteLinha.
	 *
	 * @return qtIndicadorLimiteLinha
	 */
	public String getQtIndicadorLimiteLinha() {
		return qtIndicadorLimiteLinha;
	}

	/**
	 * Set: qtIndicadorLimiteLinha.
	 *
	 * @param qtIndicadorLimiteLinha the qt indicador limite linha
	 */
	public void setQtIndicadorLimiteLinha(String qtIndicadorLimiteLinha) {
		this.qtIndicadorLimiteLinha = qtIndicadorLimiteLinha;
	}

	/**
	 * Get: qtIndicadorLimiteSolicitacaoCatao.
	 *
	 * @return qtIndicadorLimiteSolicitacaoCatao
	 */
	public String getQtIndicadorLimiteSolicitacaoCatao() {
		return qtIndicadorLimiteSolicitacaoCatao;
	}

	/**
	 * Set: qtIndicadorLimiteSolicitacaoCatao.
	 *
	 * @param qtIndicadorLimiteSolicitacaoCatao the qt indicador limite solicitacao catao
	 */
	public void setQtIndicadorLimiteSolicitacaoCatao(String qtIndicadorLimiteSolicitacaoCatao) {
		this.qtIndicadorLimiteSolicitacaoCatao = qtIndicadorLimiteSolicitacaoCatao;
	}

	/**
	 * Get: qtIndicadorMaximaInconLote.
	 *
	 * @return qtIndicadorMaximaInconLote
	 */
	public String getQtIndicadorMaximaInconLote() {
		return qtIndicadorMaximaInconLote;
	}

	/**
	 * Set: qtIndicadorMaximaInconLote.
	 *
	 * @param qtIndicadorMaximaInconLote the qt indicador maxima incon lote
	 */
	public void setQtIndicadorMaximaInconLote(String qtIndicadorMaximaInconLote) {
		this.qtIndicadorMaximaInconLote = qtIndicadorMaximaInconLote;
	}

	/**
	 * Get: qtIndicadorMaximaTituloVencido.
	 *
	 * @return qtIndicadorMaximaTituloVencido
	 */
	public String getQtIndicadorMaximaTituloVencido() {
		return qtIndicadorMaximaTituloVencido;
	}

	/**
	 * Set: qtIndicadorMaximaTituloVencido.
	 *
	 * @param qtIndicadorMaximaTituloVencido the qt indicador maxima titulo vencido
	 */
	public void setQtIndicadorMaximaTituloVencido(String qtIndicadorMaximaTituloVencido) {
		this.qtIndicadorMaximaTituloVencido = qtIndicadorMaximaTituloVencido;
	}

	/**
	 * Get: qtIndicadorMesComprovante.
	 *
	 * @return qtIndicadorMesComprovante
	 */
	public String getQtIndicadorMesComprovante() {
		return qtIndicadorMesComprovante;
	}

	/**
	 * Set: qtIndicadorMesComprovante.
	 *
	 * @param qtIndicadorMesComprovante the qt indicador mes comprovante
	 */
	public void setQtIndicadorMesComprovante(String qtIndicadorMesComprovante) {
		this.qtIndicadorMesComprovante = qtIndicadorMesComprovante;
	}

	/**
	 * Get: qtIndicadorMesEtapaRecadastro.
	 *
	 * @return qtIndicadorMesEtapaRecadastro
	 */
	public String getQtIndicadorMesEtapaRecadastro() {
		return qtIndicadorMesEtapaRecadastro;
	}

	/**
	 * Set: qtIndicadorMesEtapaRecadastro.
	 *
	 * @param qtIndicadorMesEtapaRecadastro the qt indicador mes etapa recadastro
	 */
	public void setQtIndicadorMesEtapaRecadastro(String qtIndicadorMesEtapaRecadastro) {
		this.qtIndicadorMesEtapaRecadastro = qtIndicadorMesEtapaRecadastro;
	}

	/**
	 * Get: qtIndicadorMesFaseRecadastro.
	 *
	 * @return qtIndicadorMesFaseRecadastro
	 */
	public String getQtIndicadorMesFaseRecadastro() {
		return qtIndicadorMesFaseRecadastro;
	}

	/**
	 * Set: qtIndicadorMesFaseRecadastro.
	 *
	 * @param qtIndicadorMesFaseRecadastro the qt indicador mes fase recadastro
	 */
	public void setQtIndicadorMesFaseRecadastro(String qtIndicadorMesFaseRecadastro) {
		this.qtIndicadorMesFaseRecadastro = qtIndicadorMesFaseRecadastro;
	}

	/**
	 * Get: qtIndicadorMesReajusteTarifa.
	 *
	 * @return qtIndicadorMesReajusteTarifa
	 */
	public String getQtIndicadorMesReajusteTarifa() {
		return qtIndicadorMesReajusteTarifa;
	}

	/**
	 * Set: qtIndicadorMesReajusteTarifa.
	 *
	 * @param qtIndicadorMesReajusteTarifa the qt indicador mes reajuste tarifa
	 */
	public void setQtIndicadorMesReajusteTarifa(String qtIndicadorMesReajusteTarifa) {
		this.qtIndicadorMesReajusteTarifa = qtIndicadorMesReajusteTarifa;
	}

	/**
	 * Get: qtIndicadorViaAviso.
	 *
	 * @return qtIndicadorViaAviso
	 */
	public String getQtIndicadorViaAviso() {
		return qtIndicadorViaAviso;
	}

	/**
	 * Set: qtIndicadorViaAviso.
	 *
	 * @param qtIndicadorViaAviso the qt indicador via aviso
	 */
	public void setQtIndicadorViaAviso(String qtIndicadorViaAviso) {
		this.qtIndicadorViaAviso = qtIndicadorViaAviso;
	}

	/**
	 * Get: qtIndicadorViaCobranca.
	 *
	 * @return qtIndicadorViaCobranca
	 */
	public String getQtIndicadorViaCobranca() {
		return qtIndicadorViaCobranca;
	}

	/**
	 * Set: qtIndicadorViaCobranca.
	 *
	 * @param qtIndicadorViaCobranca the qt indicador via cobranca
	 */
	public void setQtIndicadorViaCobranca(String qtIndicadorViaCobranca) {
		this.qtIndicadorViaCobranca = qtIndicadorViaCobranca;
	}

	/**
	 * Get: qtIndicadorViaComprovante.
	 *
	 * @return qtIndicadorViaComprovante
	 */
	public String getQtIndicadorViaComprovante() {
		return qtIndicadorViaComprovante;
	}

	/**
	 * Set: qtIndicadorViaComprovante.
	 *
	 * @param qtIndicadorViaComprovante the qt indicador via comprovante
	 */
	public void setQtIndicadorViaComprovante(String qtIndicadorViaComprovante) {
		this.qtIndicadorViaComprovante = qtIndicadorViaComprovante;
	}

	/**
	 * Get: vlIndicadorFavorecidoNaoCadastro.
	 *
	 * @return vlIndicadorFavorecidoNaoCadastro
	 */
	public String getVlIndicadorFavorecidoNaoCadastro() {
		return vlIndicadorFavorecidoNaoCadastro;
	}

	/**
	 * Set: vlIndicadorFavorecidoNaoCadastro.
	 *
	 * @param vlIndicadorFavorecidoNaoCadastro the vl indicador favorecido nao cadastro
	 */
	public void setVlIndicadorFavorecidoNaoCadastro(String vlIndicadorFavorecidoNaoCadastro) {
		this.vlIndicadorFavorecidoNaoCadastro = vlIndicadorFavorecidoNaoCadastro;
	}

	/**
	 * Get: vlIndicadorLimiteDiaPagamento.
	 *
	 * @return vlIndicadorLimiteDiaPagamento
	 */
	public String getVlIndicadorLimiteDiaPagamento() {
		return vlIndicadorLimiteDiaPagamento;
	}

	/**
	 * Set: vlIndicadorLimiteDiaPagamento.
	 *
	 * @param vlIndicadorLimiteDiaPagamento the vl indicador limite dia pagamento
	 */
	public void setVlIndicadorLimiteDiaPagamento(String vlIndicadorLimiteDiaPagamento) {
		this.vlIndicadorLimiteDiaPagamento = vlIndicadorLimiteDiaPagamento;
	}

	/**
	 * Get: vlIndicadorLimiteIndividualPagamento.
	 *
	 * @return vlIndicadorLimiteIndividualPagamento
	 */
	public String getVlIndicadorLimiteIndividualPagamento() {
		return vlIndicadorLimiteIndividualPagamento;
	}

	/**
	 * Set: vlIndicadorLimiteIndividualPagamento.
	 *
	 * @param vlIndicadorLimiteIndividualPagamento the vl indicador limite individual pagamento
	 */
	public void setVlIndicadorLimiteIndividualPagamento(String vlIndicadorLimiteIndividualPagamento) {
		this.vlIndicadorLimiteIndividualPagamento = vlIndicadorLimiteIndividualPagamento;
	}

	/**
	 * Get: cdIndicadorEmissaoAviso.
	 *
	 * @return cdIndicadorEmissaoAviso
	 */
	public String getCdIndicadorEmissaoAviso() {
		return cdIndicadorEmissaoAviso;
	}

	/**
	 * Set: cdIndicadorEmissaoAviso.
	 *
	 * @param cdIndicadorEmissaoAviso the cd indicador emissao aviso
	 */
	public void setCdIndicadorEmissaoAviso(String cdIndicadorEmissaoAviso) {
		this.cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
	}

	/**
	 * Get: cdIndicadorRetornoInternet.
	 *
	 * @return cdIndicadorRetornoInternet
	 */
	public String getCdIndicadorRetornoInternet() {
		return cdIndicadorRetornoInternet;
	}

	/**
	 * Set: cdIndicadorRetornoInternet.
	 *
	 * @param cdIndicadorRetornoInternet the cd indicador retorno internet
	 */
	public void setCdIndicadorRetornoInternet(String cdIndicadorRetornoInternet) {
		this.cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
	}

	/**
	 * Get: cdIndicadorListaDebito.
	 *
	 * @return cdIndicadorListaDebito
	 */
	public String getCdIndicadorListaDebito() {
		return cdIndicadorListaDebito;
	}

	/**
	 * Set: cdIndicadorListaDebito.
	 *
	 * @param cdIndicadorListaDebito the cd indicador lista debito
	 */
	public void setCdIndicadorListaDebito(String cdIndicadorListaDebito) {
		this.cdIndicadorListaDebito = cdIndicadorListaDebito;
	}

	/**
	 * Get: cdIndicadorTipoFormacaoLista.
	 *
	 * @return cdIndicadorTipoFormacaoLista
	 */
	public String getCdIndicadorTipoFormacaoLista() {
		return cdIndicadorTipoFormacaoLista;
	}

	/**
	 * Set: cdIndicadorTipoFormacaoLista.
	 *
	 * @param cdIndicadorTipoFormacaoLista the cd indicador tipo formacao lista
	 */
	public void setCdIndicadorTipoFormacaoLista(String cdIndicadorTipoFormacaoLista) {
		this.cdIndicadorTipoFormacaoLista = cdIndicadorTipoFormacaoLista;
	}

	/**
	 * Get: cdIndicadorTipoConsistenciaLista.
	 *
	 * @return cdIndicadorTipoConsistenciaLista
	 */
	public String getCdIndicadorTipoConsistenciaLista() {
		return cdIndicadorTipoConsistenciaLista;
	}

	/**
	 * Set: cdIndicadorTipoConsistenciaLista.
	 *
	 * @param cdIndicadorTipoConsistenciaLista the cd indicador tipo consistencia lista
	 */
	public void setCdIndicadorTipoConsistenciaLista(String cdIndicadorTipoConsistenciaLista) {
		this.cdIndicadorTipoConsistenciaLista = cdIndicadorTipoConsistenciaLista;
	}

	/**
	 * Get: cdIndicadorTipoConsultaComprovante.
	 *
	 * @return cdIndicadorTipoConsultaComprovante
	 */
	public String getCdIndicadorTipoConsultaComprovante() {
		return cdIndicadorTipoConsultaComprovante;
	}

	/**
	 * Set: cdIndicadorTipoConsultaComprovante.
	 *
	 * @param cdIndicadorTipoConsultaComprovante the cd indicador tipo consulta comprovante
	 */
	public void setCdIndicadorTipoConsultaComprovante(String cdIndicadorTipoConsultaComprovante) {
		this.cdIndicadorTipoConsultaComprovante = cdIndicadorTipoConsultaComprovante;
	}

	/**
	 * Get: cdIndicadorValidacaoNomeFavorecido.
	 *
	 * @return cdIndicadorValidacaoNomeFavorecido
	 */
	public String getCdIndicadorValidacaoNomeFavorecido() {
		return cdIndicadorValidacaoNomeFavorecido;
	}

	/**
	 * Set: cdIndicadorValidacaoNomeFavorecido.
	 *
	 * @param cdIndicadorValidacaoNomeFavorecido the cd indicador validacao nome favorecido
	 */
	public void setCdIndicadorValidacaoNomeFavorecido(String cdIndicadorValidacaoNomeFavorecido) {
		this.cdIndicadorValidacaoNomeFavorecido = cdIndicadorValidacaoNomeFavorecido;
	}

	/**
	 * Get: cdIndicadorTipoContaFavorecidoT.
	 *
	 * @return cdIndicadorTipoContaFavorecidoT
	 */
	public String getCdIndicadorTipoContaFavorecidoT() {
		return cdIndicadorTipoContaFavorecidoT;
	}

	/**
	 * Set: cdIndicadorTipoContaFavorecidoT.
	 *
	 * @param cdIndicadorTipoContaFavorecidoT the cd indicador tipo conta favorecido t
	 */
	public void setCdIndicadorTipoContaFavorecidoT(String cdIndicadorTipoContaFavorecidoT) {
		this.cdIndicadorTipoContaFavorecidoT = cdIndicadorTipoContaFavorecidoT;
	}

	/**
	 * Get: cdIndLancamentoPersonalizado.
	 *
	 * @return cdIndLancamentoPersonalizado
	 */
	public String getCdIndLancamentoPersonalizado() {
		return cdIndLancamentoPersonalizado;
	}

	/**
	 * Set: cdIndLancamentoPersonalizado.
	 *
	 * @param cdIndLancamentoPersonalizado the cd ind lancamento personalizado
	 */
	public void setCdIndLancamentoPersonalizado(String cdIndLancamentoPersonalizado) {
		this.cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
	}

	/**
	 * Get: cdIndicadorAgendaRastreabilidadeFilial.
	 *
	 * @return cdIndicadorAgendaRastreabilidadeFilial
	 */
	public String getCdIndicadorAgendaRastreabilidadeFilial() {
		return cdIndicadorAgendaRastreabilidadeFilial;
	}

	/**
	 * Set: cdIndicadorAgendaRastreabilidadeFilial.
	 *
	 * @param cdIndicadorAgendaRastreabilidadeFilial the cd indicador agenda rastreabilidade filial
	 */
	public void setCdIndicadorAgendaRastreabilidadeFilial(String cdIndicadorAgendaRastreabilidadeFilial) {
		this.cdIndicadorAgendaRastreabilidadeFilial = cdIndicadorAgendaRastreabilidadeFilial;
	}

	/**
	 * Get: cdIndicadorAdesaoSacador.
	 *
	 * @return cdIndicadorAdesaoSacador
	 */
	public String getCdIndicadorAdesaoSacador() {
		return cdIndicadorAdesaoSacador;
	}

	/**
	 * Set: cdIndicadorAdesaoSacador.
	 *
	 * @param cdIndicadorAdesaoSacador the cd indicador adesao sacador
	 */
	public void setCdIndicadorAdesaoSacador(String cdIndicadorAdesaoSacador) {
		this.cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
	}

	/**
	 * Get: cdIndicadorMeioPagamentoCredito.
	 *
	 * @return cdIndicadorMeioPagamentoCredito
	 */
	public String getCdIndicadorMeioPagamentoCredito() {
		return cdIndicadorMeioPagamentoCredito;
	}

	/**
	 * Set: cdIndicadorMeioPagamentoCredito.
	 *
	 * @param cdIndicadorMeioPagamentoCredito the cd indicador meio pagamento credito
	 */
	public void setCdIndicadorMeioPagamentoCredito(String cdIndicadorMeioPagamentoCredito) {
		this.cdIndicadorMeioPagamentoCredito = cdIndicadorMeioPagamentoCredito;
	}

	/**
	 * Get: cdIndicadorTipoIsncricaoFavorecido.
	 *
	 * @return cdIndicadorTipoIsncricaoFavorecido
	 */
	public String getCdIndicadorTipoIsncricaoFavorecido() {
		return cdIndicadorTipoIsncricaoFavorecido;
	}

	/**
	 * Set: cdIndicadorTipoIsncricaoFavorecido.
	 *
	 * @param cdIndicadorTipoIsncricaoFavorecido the cd indicador tipo isncricao favorecido
	 */
	public void setCdIndicadorTipoIsncricaoFavorecido(String cdIndicadorTipoIsncricaoFavorecido) {
		this.cdIndicadorTipoIsncricaoFavorecido = cdIndicadorTipoIsncricaoFavorecido;
	}

	/**
	 * Get: cdIndicadorBancoPostal.
	 *
	 * @return cdIndicadorBancoPostal
	 */
	public String getCdIndicadorBancoPostal() {
		return cdIndicadorBancoPostal;
	}

	/**
	 * Set: cdIndicadorBancoPostal.
	 *
	 * @param cdIndicadorBancoPostal the cd indicador banco postal
	 */
	public void setCdIndicadorBancoPostal(String cdIndicadorBancoPostal) {
		this.cdIndicadorBancoPostal = cdIndicadorBancoPostal;
	}

	/**
	 * Get: cdIndicadorFormularioContratoCliente.
	 *
	 * @return cdIndicadorFormularioContratoCliente
	 */
	public String getCdIndicadorFormularioContratoCliente() {
		return cdIndicadorFormularioContratoCliente;
	}

	/**
	 * Set: cdIndicadorFormularioContratoCliente.
	 *
	 * @param cdIndicadorFormularioContratoCliente the cd indicador formulario contrato cliente
	 */
	public void setCdIndicadorFormularioContratoCliente(String cdIndicadorFormularioContratoCliente) {
		this.cdIndicadorFormularioContratoCliente = cdIndicadorFormularioContratoCliente;
	}

	/**
	 * Get: dsIndicadorAreaResrd.
	 *
	 * @return dsIndicadorAreaResrd
	 */
	public String getDsIndicadorAreaResrd() {
		return dsIndicadorAreaResrd;
	}

	/**
	 * Set: dsIndicadorAreaResrd.
	 *
	 * @param dsIndicadorAreaResrd the ds indicador area resrd
	 */
	public void setDsIndicadorAreaResrd(String dsIndicadorAreaResrd) {
		this.dsIndicadorAreaResrd = dsIndicadorAreaResrd;
	}

	/**
	 * @param cdIndicadorConsultaSaldoSuperior the cdIndicadorConsultaSaldoSuperior to set
	 */
	public void setCdIndicadorConsultaSaldoSuperior(
			String cdIndicadorConsultaSaldoSuperior) {
		this.cdIndicadorConsultaSaldoSuperior = cdIndicadorConsultaSaldoSuperior;
	}

	/**
	 * @return the cdIndicadorConsultaSaldoSuperior
	 */
	public String getCdIndicadorConsultaSaldoSuperior() {
		return cdIndicadorConsultaSaldoSuperior;
	}
}
