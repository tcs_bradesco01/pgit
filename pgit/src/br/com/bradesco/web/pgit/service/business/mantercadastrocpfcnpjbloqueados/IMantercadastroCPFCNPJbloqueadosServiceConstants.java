/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados;

/**
 * Nome: IMantercadastroCPFCNPJbloqueadosServiceConstants
 * <p>
 * Prop�sito: Interface de constantes do adaptador MantercadastroCPFCNPJbloqueados
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IMantercadastroCPFCNPJbloqueadosServiceConstants {
	
	/** Atributo QTDE_CONSULTAS_LISTAR. */
	int QTDE_CONSULTAS_LISTAR = 100;
}