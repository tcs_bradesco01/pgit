/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.util.List;


/**
 * Nome: CentroCustoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarNumeroVersaoLayoutOcorrSaidaDTO {

	/** Atributo codMensagem. */
	private Integer numeroVersaoLayoutArquivo;

	
	/**
	 * Nome: setNumeroVersaoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @param numeroVersaoLayoutArquivo
	 */
	public void setNumeroVersaoLayoutArquivo(Integer numeroVersaoLayoutArquivo) {
		this.numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
	}

	/**
	 * Nome: getNumeroVersaoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @return numeroVersaoLayoutArquivo
	 */
	public Integer getNumeroVersaoLayoutArquivo() {
		return numeroVersaoLayoutArquivo;
	}
    
}
