/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Nome: ConRelacaoConManutencoesSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConRelacaoConManutencoesSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo listaParticipantes. */
	private List<ParticipantesRelacaoConManutencoesDTO> listaParticipantes = new ArrayList<ParticipantesRelacaoConManutencoesDTO>();
	
	/** Atributo listaServicos. */
	private List<ServicosDTO> listaServicos = new ArrayList<ServicosDTO>();
	
	/** Atributo listaVinculos. */
	private List<VinculoRelacaoConManutencoesDTO> listaVinculos = new ArrayList<VinculoRelacaoConManutencoesDTO>();
	
	/** Atributo listaPerfis. */
	private List<PerfilDTO> listaPerfis= new ArrayList<PerfilDTO>();
	
	//Estes atributos s�o utilizados para a montagem das listas
    /** Atributo numeroOcorrenciasParticipante. */
	private Integer numeroOcorrenciasParticipante;
    
    /** Atributo numeroOcorrenciasVinculacao. */
    private Integer numeroOcorrenciasVinculacao;
    
    /** Atributo numeroOcorrenciasServicos. */
    private Integer numeroOcorrenciasServicos;	
    
    /** Atributo nrOcorrenciasPerfil. */
    private Integer nrOcorrenciasPerfil;
    
    /** Atributo cdFormaAutorizacaoPagamento. */
    private Integer cdFormaAutorizacaoPagamento;
    
    /** Atributo dsFormaAutorizacaoPagamento. */
    private String dsFormaAutorizacaoPagamento;
    
    /** Atributo cdUnidadeOrganizacional. */
    private Integer cdUnidadeOrganizacional;
    
    /** Atributo dsUnidadeOrganizacional. */
    private String dsUnidadeOrganizacional;
    
    /** Atributo cdFuncionarioGerenteContrato. */
    private Long cdFuncionarioGerenteContrato;
    
    /** Atributo dsFuncionarioGerenteContrato. */
    private String dsFuncionarioGerenteContrato;
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: listaServicos.
	 *
	 * @return listaServicos
	 */
	public List<ServicosDTO> getListaServicos() {
		return listaServicos;
	}
	
	/**
	 * Set: listaServicos.
	 *
	 * @param listaServicos the lista servicos
	 */
	public void setListaServicos(List<ServicosDTO> listaServicos) {
		this.listaServicos = listaServicos;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: listaVinculos.
	 *
	 * @return listaVinculos
	 */
	public List<VinculoRelacaoConManutencoesDTO> getListaVinculos() {
		return listaVinculos;
	}
	
	/**
	 * Set: listaVinculos.
	 *
	 * @param listaVinculos the lista vinculos
	 */
	public void setListaVinculos(List<VinculoRelacaoConManutencoesDTO> listaVinculos) {
		this.listaVinculos = listaVinculos;
	}
	
	/**
	 * Get: listaParticipantes.
	 *
	 * @return listaParticipantes
	 */
	public List<ParticipantesRelacaoConManutencoesDTO> getListaParticipantes() {
		return listaParticipantes;
	}
	
	/**
	 * Set: listaParticipantes.
	 *
	 * @param listaParticipantes the lista participantes
	 */
	public void setListaParticipantes(
			List<ParticipantesRelacaoConManutencoesDTO> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}
	
	/**
	 * Get: numeroOcorrenciasParticipante.
	 *
	 * @return numeroOcorrenciasParticipante
	 */
	public Integer getNumeroOcorrenciasParticipante() {
		return numeroOcorrenciasParticipante;
	}
	
	/**
	 * Set: numeroOcorrenciasParticipante.
	 *
	 * @param numeroOcorrenciasParticipante the numero ocorrencias participante
	 */
	public void setNumeroOcorrenciasParticipante(
			Integer numeroOcorrenciasParticipante) {
		this.numeroOcorrenciasParticipante = numeroOcorrenciasParticipante;
	}
	
	/**
	 * Get: numeroOcorrenciasServicos.
	 *
	 * @return numeroOcorrenciasServicos
	 */
	public Integer getNumeroOcorrenciasServicos() {
		return numeroOcorrenciasServicos;
	}
	
	/**
	 * Set: numeroOcorrenciasServicos.
	 *
	 * @param numeroOcorrenciasServicos the numero ocorrencias servicos
	 */
	public void setNumeroOcorrenciasServicos(Integer numeroOcorrenciasServicos) {
		this.numeroOcorrenciasServicos = numeroOcorrenciasServicos;
	}
	
	/**
	 * Get: numeroOcorrenciasVinculacao.
	 *
	 * @return numeroOcorrenciasVinculacao
	 */
	public Integer getNumeroOcorrenciasVinculacao() {
		return numeroOcorrenciasVinculacao;
	}
	
	/**
	 * Set: numeroOcorrenciasVinculacao.
	 *
	 * @param numeroOcorrenciasVinculacao the numero ocorrencias vinculacao
	 */
	public void setNumeroOcorrenciasVinculacao(Integer numeroOcorrenciasVinculacao) {
		this.numeroOcorrenciasVinculacao = numeroOcorrenciasVinculacao;
	}
	
	/**
	 * Get: nrOcorrenciasPerfil.
	 *
	 * @return nrOcorrenciasPerfil
	 */
	public Integer getNrOcorrenciasPerfil() {
		return nrOcorrenciasPerfil;
	}
	
	/**
	 * Set: nrOcorrenciasPerfil.
	 *
	 * @param nrOcorrenciasPerfil the nr ocorrencias perfil
	 */
	public void setNrOcorrenciasPerfil(Integer nrOcorrenciasPerfil) {
		this.nrOcorrenciasPerfil = nrOcorrenciasPerfil;
	}
	
	/**
	 * Get: listaPerfis.
	 *
	 * @return listaPerfis
	 */
	public List<PerfilDTO> getListaPerfis() {
		return listaPerfis;
	}
	
	/**
	 * Set: listaPerfis.
	 *
	 * @param listaPerfis the lista perfis
	 */
	public void setListaPerfis(List<PerfilDTO> listaPerfis) {
		this.listaPerfis = listaPerfis;
	}

    /**
     * Get: cdFormaAutorizacaoPagamento.
     *
     * @return cdFormaAutorizacaoPagamento
     */
    public Integer getCdFormaAutorizacaoPagamento() {
        return cdFormaAutorizacaoPagamento;
    }

    /**
     * Set: cdFormaAutorizacaoPagamento.
     *
     * @param cdFormaAutorizacaoPagamento the cd forma autorizacao pagamento
     */
    public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
        this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
    }

    /**
     * Get: dsFormaAutorizacaoPagamento.
     *
     * @return dsFormaAutorizacaoPagamento
     */
    public String getDsFormaAutorizacaoPagamento() {
        return dsFormaAutorizacaoPagamento;
    }

    /**
     * Set: dsFormaAutorizacaoPagamento.
     *
     * @param dsFormaAutorizacaoPagamento the ds forma autorizacao pagamento
     */
    public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
        this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    }

    /**
     * Get: cdUnidadeOrganizacional.
     *
     * @return cdUnidadeOrganizacional
     */
    public Integer getCdUnidadeOrganizacional() {
        return cdUnidadeOrganizacional;
    }

    /**
     * Set: cdUnidadeOrganizacional.
     *
     * @param cdUnidadeOrganizacional the cd unidade organizacional
     */
    public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
        this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
    }

    /**
     * Get: dsUnidadeOrganizacional.
     *
     * @return dsUnidadeOrganizacional
     */
    public String getDsUnidadeOrganizacional() {
        return dsUnidadeOrganizacional;
    }

    /**
     * Set: dsUnidadeOrganizacional.
     *
     * @param dsUnidadeOrganizacional the ds unidade organizacional
     */
    public void setDsUnidadeOrganizacional(String dsUnidadeOrganizacional) {
        this.dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    }

    /**
     * Get: cdFuncionarioGerenteContrato.
     *
     * @return cdFuncionarioGerenteContrato
     */
    public Long getCdFuncionarioGerenteContrato() {
        return cdFuncionarioGerenteContrato;
    }

    /**
     * Set: cdFuncionarioGerenteContrato.
     *
     * @param cdFuncionarioGerenteContrato the cd funcionario gerente contrato
     */
    public void setCdFuncionarioGerenteContrato(Long cdFuncionarioGerenteContrato) {
        this.cdFuncionarioGerenteContrato = cdFuncionarioGerenteContrato;
    }

    /**
     * Get: dsFuncionarioGerenteContrato.
     *
     * @return dsFuncionarioGerenteContrato
     */
    public String getDsFuncionarioGerenteContrato() {
        return dsFuncionarioGerenteContrato;
    }

    /**
     * Set: dsFuncionarioGerenteContrato.
     *
     * @param dsFuncionarioGerenteContrato the ds funcionario gerente contrato
     */
    public void setDsFuncionarioGerenteContrato(String dsFuncionarioGerenteContrato) {
        this.dsFuncionarioGerenteContrato = dsFuncionarioGerenteContrato;
    }
	
    /**
     * Get: unidadeOrganizacionalFormatada.
     *
     * @return unidadeOrganizacionalFormatada
     */
    public String getUnidadeOrganizacionalFormatada(){
        if(getCdUnidadeOrganizacional() == null || getCdUnidadeOrganizacional() == 0 || 
             getDsUnidadeOrganizacional() == null || "".equals(getDsUnidadeOrganizacional())){
            return getCdUnidadeOrganizacional() + getDsUnidadeOrganizacional();
        }
        return getCdUnidadeOrganizacional() + " - " + getDsUnidadeOrganizacional();
    }
}
