/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: ServicosModalidadesDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ServicosModalidadesDTO {	

	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo imprimeLinhaServico. */
	private String imprimeLinhaServico = "N";	
	
	
	public ServicosModalidadesDTO() {
		super();
	}

	public ServicosModalidadesDTO(String dsServico, String dsModalidade,
			String imprimeLinhaServico) {
		super();
		this.dsServico = dsServico;
		this.dsModalidade = dsModalidade;
		this.imprimeLinhaServico = imprimeLinhaServico;
	}

	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}
	
	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}
	
	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}
	
	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}
	
	/**
	 * Get: imprimeLinhaServico.
	 *
	 * @return imprimeLinhaServico
	 */
	public String getImprimeLinhaServico() {
		return imprimeLinhaServico;
	}
	
	/**
	 * Set: imprimeLinhaServico.
	 *
	 * @param imprimeLinhaServico the imprime linha servico
	 */
	public void setImprimeLinhaServico(String imprimeLinhaServico) {
		this.imprimeLinhaServico = imprimeLinhaServico;
	}	
}
