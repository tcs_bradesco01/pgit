/*
 * Nome: br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean;

/**
 * Nome: RejeitarSolicitacaoConsistPreviaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RejeitarSolicitacaoConsistPreviaEntradaDTO {
	
	/** Atributo cdConsistenciaPreviaPagamento. */
	private Integer cdConsistenciaPreviaPagamento ;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long  cdPessoaJuridicaContrato ;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer  cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio ;
	
	/**
	 * Get: cdConsistenciaPreviaPagamento.
	 *
	 * @return cdConsistenciaPreviaPagamento
	 */
	public Integer getCdConsistenciaPreviaPagamento() {
		return cdConsistenciaPreviaPagamento;
	}
	
	/**
	 * Set: cdConsistenciaPreviaPagamento.
	 *
	 * @param cdConsistenciaPreviaPagamento the cd consistencia previa pagamento
	 */
	public void setCdConsistenciaPreviaPagamento(
			Integer cdConsistenciaPreviaPagamento) {
		this.cdConsistenciaPreviaPagamento = cdConsistenciaPreviaPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	
	
	
	


}
