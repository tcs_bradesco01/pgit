/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarClientesRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarClientesRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdFilialCnpj
     */
    private int _cdFilialCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCnpj
     */
    private boolean _has_cdFilialCnpj;

    /**
     * Field _cdControleCnpj
     */
    private int _cdControleCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCnpj
     */
    private boolean _has_cdControleCnpj;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgenciaBancaria
     */
    private int _cdAgenciaBancaria = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancaria
     */
    private boolean _has_cdAgenciaBancaria;

    /**
     * Field _cdContaBancaria
     */
    private long _cdContaBancaria = 0;

    /**
     * keeps track of state for field: _cdContaBancaria
     */
    private boolean _has_cdContaBancaria;

    /**
     * Field _dtNascimentoFund
     */
    private java.lang.String _dtNascimentoFund;

    /**
     * Field _dsNomeRazao
     */
    private java.lang.String _dsNomeRazao;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarClientesRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.request.ListarClientesRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaBancaria
     * 
     */
    public void deleteCdAgenciaBancaria()
    {
        this._has_cdAgenciaBancaria= false;
    } //-- void deleteCdAgenciaBancaria() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdContaBancaria
     * 
     */
    public void deleteCdContaBancaria()
    {
        this._has_cdContaBancaria= false;
    } //-- void deleteCdContaBancaria() 

    /**
     * Method deleteCdControleCnpj
     * 
     */
    public void deleteCdControleCnpj()
    {
        this._has_cdControleCnpj= false;
    } //-- void deleteCdControleCnpj() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdFilialCnpj
     * 
     */
    public void deleteCdFilialCnpj()
    {
        this._has_cdFilialCnpj= false;
    } //-- void deleteCdFilialCnpj() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdAgenciaBancaria'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancaria'.
     */
    public int getCdAgenciaBancaria()
    {
        return this._cdAgenciaBancaria;
    } //-- int getCdAgenciaBancaria() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdContaBancaria'.
     * 
     * @return long
     * @return the value of field 'cdContaBancaria'.
     */
    public long getCdContaBancaria()
    {
        return this._cdContaBancaria;
    } //-- long getCdContaBancaria() 

    /**
     * Returns the value of field 'cdControleCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpj'.
     */
    public int getCdControleCnpj()
    {
        return this._cdControleCnpj;
    } //-- int getCdControleCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpj'.
     */
    public int getCdFilialCnpj()
    {
        return this._cdFilialCnpj;
    } //-- int getCdFilialCnpj() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'dsNomeRazao'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazao'.
     */
    public java.lang.String getDsNomeRazao()
    {
        return this._dsNomeRazao;
    } //-- java.lang.String getDsNomeRazao() 

    /**
     * Returns the value of field 'dtNascimentoFund'.
     * 
     * @return String
     * @return the value of field 'dtNascimentoFund'.
     */
    public java.lang.String getDtNascimentoFund()
    {
        return this._dtNascimentoFund;
    } //-- java.lang.String getDtNascimentoFund() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdAgenciaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancaria()
    {
        return this._has_cdAgenciaBancaria;
    } //-- boolean hasCdAgenciaBancaria() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdContaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancaria()
    {
        return this._has_cdContaBancaria;
    } //-- boolean hasCdContaBancaria() 

    /**
     * Method hasCdControleCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpj()
    {
        return this._has_cdControleCnpj;
    } //-- boolean hasCdControleCnpj() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpj()
    {
        return this._has_cdFilialCnpj;
    } //-- boolean hasCdFilialCnpj() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaBancaria'.
     * 
     * @param cdAgenciaBancaria the value of field
     * 'cdAgenciaBancaria'.
     */
    public void setCdAgenciaBancaria(int cdAgenciaBancaria)
    {
        this._cdAgenciaBancaria = cdAgenciaBancaria;
        this._has_cdAgenciaBancaria = true;
    } //-- void setCdAgenciaBancaria(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdContaBancaria'.
     * 
     * @param cdContaBancaria the value of field 'cdContaBancaria'.
     */
    public void setCdContaBancaria(long cdContaBancaria)
    {
        this._cdContaBancaria = cdContaBancaria;
        this._has_cdContaBancaria = true;
    } //-- void setCdContaBancaria(long) 

    /**
     * Sets the value of field 'cdControleCnpj'.
     * 
     * @param cdControleCnpj the value of field 'cdControleCnpj'.
     */
    public void setCdControleCnpj(int cdControleCnpj)
    {
        this._cdControleCnpj = cdControleCnpj;
        this._has_cdControleCnpj = true;
    } //-- void setCdControleCnpj(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdFilialCnpj'.
     * 
     * @param cdFilialCnpj the value of field 'cdFilialCnpj'.
     */
    public void setCdFilialCnpj(int cdFilialCnpj)
    {
        this._cdFilialCnpj = cdFilialCnpj;
        this._has_cdFilialCnpj = true;
    } //-- void setCdFilialCnpj(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'dsNomeRazao'.
     * 
     * @param dsNomeRazao the value of field 'dsNomeRazao'.
     */
    public void setDsNomeRazao(java.lang.String dsNomeRazao)
    {
        this._dsNomeRazao = dsNomeRazao;
    } //-- void setDsNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'dtNascimentoFund'.
     * 
     * @param dtNascimentoFund the value of field 'dtNascimentoFund'
     */
    public void setDtNascimentoFund(java.lang.String dtNascimentoFund)
    {
        this._dtNascimentoFund = dtNascimentoFund;
    } //-- void setDtNascimentoFund(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarClientesRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.request.ListarClientesRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.request.ListarClientesRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.request.ListarClientesRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarclientesmarq.request.ListarClientesRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
