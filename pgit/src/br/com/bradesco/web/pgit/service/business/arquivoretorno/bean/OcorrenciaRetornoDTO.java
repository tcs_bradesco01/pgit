/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoretorno.bean;

/**
 * Nome: OcorrenciaRetornoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciaRetornoDTO {
	/*Request*/
	/** Atributo controleCnpj. */
	private String controleCnpj;
	
	/** Atributo sistemaLegado. */
	private String sistemaLegado;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo filalCnpj. */
	private String filalCnpj;	
	
	/** Atributo maximoOcorrencia. */
	private String maximoOcorrencia;
	
	/** Atributo numeroArquivoRetorno. */
	private Long numeroArquivoRetorno;
	
	/** Atributo numeroLoteRetorno. */
	private Long numeroLoteRetorno;
	
	/** Atributo numeroSequenciaRegistroRetorno. */
	private String numeroSequenciaRegistroRetorno;	
	
	/** Atributo codigoPerfil. */
	private String codigoPerfil;
	
	/** Atributo horaInclusao. */
	private String horaInclusao;
	  
	/*Response*/
	/** Atributo textoMensagem. */
	private String textoMensagem;
	
	/** Atributo conteudoEnviado. */
	private String conteudoEnviado;
	
	
	/**
	 * Get: codigoPerfil.
	 *
	 * @return codigoPerfil
	 */
	public String getCodigoPerfil() {
		return codigoPerfil;
	}
	
	/**
	 * Set: codigoPerfil.
	 *
	 * @param codigoPerfil the codigo perfil
	 */
	public void setCodigoPerfil(String codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}
	
	/**
	 * Get: conteudoEnviado.
	 *
	 * @return conteudoEnviado
	 */
	public String getConteudoEnviado() {
		return conteudoEnviado;
	}
	
	/**
	 * Set: conteudoEnviado.
	 *
	 * @param conteudoEnviado the conteudo enviado
	 */
	public void setConteudoEnviado(String conteudoEnviado) {
		this.conteudoEnviado = conteudoEnviado;
	}
	
	/**
	 * Get: controleCnpj.
	 *
	 * @return controleCnpj
	 */
	public String getControleCnpj() {
		return controleCnpj;
	}
	
	/**
	 * Set: controleCnpj.
	 *
	 * @param controleCnpj the controle cnpj
	 */
	public void setControleCnpj(String controleCnpj) {
		this.controleCnpj = controleCnpj;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: filalCnpj.
	 *
	 * @return filalCnpj
	 */
	public String getFilalCnpj() {
		return filalCnpj;
	}
	
	/**
	 * Set: filalCnpj.
	 *
	 * @param filalCnpj the filal cnpj
	 */
	public void setFilalCnpj(String filalCnpj) {
		this.filalCnpj = filalCnpj;
	}
	
	/**
	 * Get: maximoOcorrencia.
	 *
	 * @return maximoOcorrencia
	 */
	public String getMaximoOcorrencia() {
		return maximoOcorrencia;
	}
	
	/**
	 * Set: maximoOcorrencia.
	 *
	 * @param maximoOcorrencia the maximo ocorrencia
	 */
	public void setMaximoOcorrencia(String maximoOcorrencia) {
		this.maximoOcorrencia = maximoOcorrencia;
	}
	
	/**
	 * Get: numeroArquivoRetorno.
	 *
	 * @return numeroArquivoRetorno
	 */
	public Long getNumeroArquivoRetorno() {
		return numeroArquivoRetorno;
	}
	
	/**
	 * Set: numeroArquivoRetorno.
	 *
	 * @param numeroArquivoRetorno the numero arquivo retorno
	 */
	public void setNumeroArquivoRetorno(Long numeroArquivoRetorno) {
		this.numeroArquivoRetorno = numeroArquivoRetorno;
	}
	
	/**
	 * Get: numeroLoteRetorno.
	 *
	 * @return numeroLoteRetorno
	 */
	public Long getNumeroLoteRetorno() {
		return numeroLoteRetorno;
	}
	
	/**
	 * Set: numeroLoteRetorno.
	 *
	 * @param numeroLoteRetorno the numero lote retorno
	 */
	public void setNumeroLoteRetorno(Long numeroLoteRetorno) {
		this.numeroLoteRetorno = numeroLoteRetorno;
	}
	
	/**
	 * Get: numeroSequenciaRegistroRetorno.
	 *
	 * @return numeroSequenciaRegistroRetorno
	 */
	public String getNumeroSequenciaRegistroRetorno() {
		return numeroSequenciaRegistroRetorno;
	}
	
	/**
	 * Set: numeroSequenciaRegistroRetorno.
	 *
	 * @param numeroSequenciaRegistroRetorno the numero sequencia registro retorno
	 */
	public void setNumeroSequenciaRegistroRetorno(
			String numeroSequenciaRegistroRetorno) {
		this.numeroSequenciaRegistroRetorno = numeroSequenciaRegistroRetorno;
	}
	
	/**
	 * Get: sistemaLegado.
	 *
	 * @return sistemaLegado
	 */
	public String getSistemaLegado() {
		return sistemaLegado;
	}
	
	/**
	 * Set: sistemaLegado.
	 *
	 * @param sistemaLegado the sistema legado
	 */
	public void setSistemaLegado(String sistemaLegado) {
		this.sistemaLegado = sistemaLegado;
	}
	
	/**
	 * Get: textoMensagem.
	 *
	 * @return textoMensagem
	 */
	public String getTextoMensagem() {
		return textoMensagem;
	}
	
	/**
	 * Set: textoMensagem.
	 *
	 * @param textoMensagem the texto mensagem
	 */
	public void setTextoMensagem(String textoMensagem) {
		this.textoMensagem = textoMensagem;
	}
	
	/**
	 * Get: horaInclusao.
	 *
	 * @return horaInclusao
	 */
	public String getHoraInclusao() {
		return horaInclusao;
	}
	
	/**
	 * Set: horaInclusao.
	 *
	 * @param horaInclusao the hora inclusao
	 */
	public void setHoraInclusao(String horaInclusao) {
		this.horaInclusao = horaInclusao;
	}
	
	
}
