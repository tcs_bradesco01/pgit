/**
 * Nome: br.com.bradesco.web.pgit.service.business.conveniosalarial.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.contrato.impl;

import static br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils.getI18nMessage;
import static br.com.bradesco.web.pgit.utils.DateUtils.formataData;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.bradesco.pdc.common.exception.PDCTechnicalException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.pgit.service.business.contrato.IContratoService;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoOcorrencias1SaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoOcorrencias2SaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoOcorrencias3SaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoOcorrencias4SaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoOcorrencias5SaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoOcorrencias6SaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoOcorrencias7SaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ListarComprovanteSalarialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ListarComprovanteSalarialOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ListarComprovanteSalarialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ListarParticipantesOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.NumerosEmissoesPagasEnum;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.request.ImprimirContratoIntranetRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.ImprimirContratoIntranetResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.request.ListarComprovanteSalarialIntranetRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcomprovantesalarialintranet.response.ListarComprovanteSalarialIntranetResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.request.ListarParticipantesIntranetRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.response.ListarParticipantesIntranetResponse;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.ImpressaoContrato;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.Anexo;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.AnexoComprovanteSalarial;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.AnexoFornecedores;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.AnexoSalarios;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.AnexoTributosContas;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.Assinatura;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.Conta;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.ContratoPrestacaoServico;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.DadosIdentificacao;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.EmpresaParticipante;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.IdentificacaoContrato;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.ModalidadeCondicao;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.Participante;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.ProdutoServico;

/**
 * The Class ContratoServiceImpl.
 * 
 * @author : todo!
 * @version :
 */
public class ContratoServiceImpl implements IContratoService {

    /** The factory adapter. */
    private FactoryAdapter factoryAdapter = null;

    /** The impressao contrato. */
    private ImpressaoContrato impressaoContrato = null;

    /**
     * Instancia um novo ContratoServiceImpl
     * 
     * @see
     */
    public ContratoServiceImpl() {
        super();
    }

    /**
     * Nome: setFactoryAdapter.
     * 
     * @param factory
     *            the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factory) {
        this.factoryAdapter = factory;
    }

    /**
     * Nome: getFactoryAdapter.
     * 
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }

    /**
     * Get: impressaoContrato.
     * 
     * @return impressaoContrato
     */
    public ImpressaoContrato getImpressaoContrato() {
        return impressaoContrato;
    }

    /**
     * Set: impressaoContrato.
     * 
     * @param impressaoContrato
     *            the impressao contrato
     */
    public void setImpressaoContrato(ImpressaoContrato impressaoContrato) {
        this.impressaoContrato = impressaoContrato;
    }

    /**
     * 
     * @param entrada
     *            the entrada
     * @return the imprimir contrato saida dto
     * @see br.com.bradesco.web.pgit.service.business.contrato.IContratoService#imprimirContrato
     *      (br.com.bradesco.web.pgit.service.business.contrato.bean.ImprimirContratoEntradaDTO)
     */
    public ImprimirContratoSaidaDTO imprimirContrato(ImprimirContratoEntradaDTO entrada) {
        ImprimirContratoIntranetResponse response = null;
        ImprimirContratoSaidaDTO saida = new ImprimirContratoSaidaDTO();
        int numeroOcorrenciasFornecedor = 0;
        int numeroOcorrenciasTributo = 0;
        int numeroOcorrenciasSalario = 0;
        int numeroOcorrenciasFuncionario = 0;
        int numeroOcorrenciasCaixa = 0;
        int numeroOcorrenciasAtendimento = 0;
        int numeroOcorrenciasFoneFacil = 0;
        int numeroOcorrenciasInternet = 0;
        ArrayList<ImprimirContratoOcorrenciasSaidaDTO> listaOcorrenciasFornecedor =
            new ArrayList<ImprimirContratoOcorrenciasSaidaDTO>();
        ArrayList<ImprimirContratoOcorrencias1SaidaDTO> listaOcorrenciasTributo =
            new ArrayList<ImprimirContratoOcorrencias1SaidaDTO>();
        ArrayList<ImprimirContratoOcorrencias2SaidaDTO> listaOcorrenciasSalario =
            new ArrayList<ImprimirContratoOcorrencias2SaidaDTO>();
        ArrayList<ImprimirContratoOcorrencias3SaidaDTO> listaOcorrenciasFuncionario =
            new ArrayList<ImprimirContratoOcorrencias3SaidaDTO>();
        ArrayList<ImprimirContratoOcorrencias4SaidaDTO> listaOcorrenciasCaixa =
            new ArrayList<ImprimirContratoOcorrencias4SaidaDTO>();
        ArrayList<ImprimirContratoOcorrencias5SaidaDTO> listaOcorrenciasAtendimento =
            new ArrayList<ImprimirContratoOcorrencias5SaidaDTO>();
        ArrayList<ImprimirContratoOcorrencias6SaidaDTO> listaOcorrenciasFoneFacil =
            new ArrayList<ImprimirContratoOcorrencias6SaidaDTO>();
        ArrayList<ImprimirContratoOcorrencias7SaidaDTO> listaOcorrenciasInternet =
            new ArrayList<ImprimirContratoOcorrencias7SaidaDTO>();
        ImprimirContratoIntranetRequest request = new ImprimirContratoIntranetRequest();

        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrAditivoContratoNegocio(verificaLongNulo(entrada.getNrAditivoContratoNegocio()));

        response = getFactoryAdapter().getImprimirContratoIntranetPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setCdBanco(response.getCdBanco());
        saida.setDsBanco(response.getDsBanco());
        saida.setCdCpfCnpj(response.getCdCpfCnpj());
        saida.setDsLogradouroPagador(response.getDsLogradouroPagador());
        saida.setDsNumeroLogradouroPagador(response.getDsNumeroLogradouroPagador());
        saida.setDsComplementoLogradouroPagador(response.getDsComplementoLogradouroPagador());
        saida.setDsBairroClientePagador(response.getDsBairroClientePagador());
        saida.setDsMunicipioClientePagador(response.getDsMunicipioClientePagador());
        saida.setCdSiglaUfPagador(response.getCdSiglaUfPagador());
        saida.setCdCepPagador(response.getCdCepPagador());
        saida.setCdCepComplementoPagador(response.getCdCepComplementoPagador());
        saida.setCdPessoaJuridicaContrato(response.getCdpessoaJuridicaContrato());
        saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
        saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
        saida.setCdAgenciaGestora(response.getCdAgenciaGestora());
        saida.setDsAgenciaGestora(response.getDsAgenciaGestora());
        saida.setCdFormaAutorizacaoPagamento(response.getCdFormaAutorizacaoPagamento());
        saida.setNrAditivoContratoNegocio(response.getNrAditivoContratoNegocio());
        saida.setDtGeracaoContratoNegocio(response.getDtGeracaoContratoNegocio());
        saida.setCdTransferenciaContaSalarial(response.getCdTransferenciaContaSalarial());
        saida.setHrLimiteTransmissaoArquivo(response.getHrLimiteTransmissaoArquivo());
        saida.setHrLimitePagamentoRemessa(response.getHrLimitePagamentoRemessa());
        saida.setHrLimiteSuperiorBoleto(response.getHrLimiteSuperiorBoleto());
        saida.sethrLimiteInferiorBoleto(response.getHrLimiteInferiorBoleto());
        saida.setHrLimitePagamentoSuperior(response.getHrLimitePagamentoSuperior());
        saida.setHrLimitePagamentoInferior(response.getHrLimitePagamentoInferior());
        saida.setHrLimiteEfetivacaoPagamento(response.getHrLimiteEfetivacaoPagamento());
        saida.setHrLimiteConsultaPagamento(response.getHrLimiteConsultaPagamento());
        saida.setHrCicloEfetivacaoPagamento(response.getHrCicloEfetivacaoPagamento());
        saida.setNrQuantidadeLimiteCobrancaImpressao(response.getNrQuantidadeLimiteCobrancaImpressao());
        saida.setDsDisponibilizacaoComprovanteBanco(response.getDsDisponibilizacaoComprovanteBanco());
        saida.setHrLimiteApresentaCarta(response.getHrLimiteApresentaCarta());
        saida.setNrMontanteBoleto(response.getNrMontanteBoleto());
        saida.setDsFrasePadraoCaput(response.getDsFrasePadraoCaput());
        saida.setDsDetalheModalidadeDoc(response.getDsDetalheModalidadeDoc());
        saida.setDsDetalheModalidadeTed(response.getDsDetalheModalidadeTed());
        saida.setDsDetalheModalidadeOp(response.getDsDetalheModalidadeOp());
        saida.setCdIndicadorServicoFornecedor(response.getCdIndicadorServicoFornecedor());
        saida.setCdProdutoServicoFornecedor(response.getCdProdutoServicoFornecedor());
        saida.setDsProdutoServicoFornecedor(response.getDsProdutoServicoFornecedor());
        saida.setCdIndicadorServicoTributos(response.getCdIndicadorServicoTributos());
        saida.setCdProdutoServicoTributos(response.getCdProdutoServicoTributos());
        saida.setDsProdutoServicoTributos(response.getDsProdutoServicoTributos());
        saida.setCdIndicadorServicoSalario(response.getCdIndicadorServicoSalario());
        saida.setCdProdutoServicoSalario(response.getCdProdutoServicoSalario());
        saida.setDsProdutoServicoSalario(response.getDsProdutoServicoSalario());
        saida.setCdFloatingServicoAplicacaoSalario(response.getCdFloatingServicoAplicacaoFloatSalario());
        saida.setDsFloatingServicoAplicacaoSalario(response.getDsFloatingServicoAplicacaoFloatSalario());
        saida.setCdIndicadorComprovanteSalarial(response.getCdIndicadorComprovanteSalarial());
        saida.setDsMunicipioAgenciaGestora(response.getDsMunicipioAgenciaGestora());
        saida.setDsSiglaUfParticipante(response.getDsSiglaUfParticipante());
        saida.setCdFloatServicoAplicacaoFornecedor(response.getCdFloatServicoAplicacaoFloatingFornecedor());
        saida.setDsFloatServicoAplicacaoFornecedor(response.getDsFloatServicoAplicacaoFloatingFornecedor());

        numeroOcorrenciasFornecedor = response.getOcorrenciasCount();
        for (int i = 0; i < numeroOcorrenciasFornecedor; i++) {
            ImprimirContratoOcorrenciasSaidaDTO itemLista = new ImprimirContratoOcorrenciasSaidaDTO();

            itemLista.setCdIndicadorModalidadeFornecedor(response.getOcorrencias(i)
                .getCdIndicadorModalidadeFornecedor());
            itemLista.setCdProdutoRelacionadoFornecedor(response.getOcorrencias(i).getCdProdutoRelacionadoFornecedor());
            itemLista.setDsProdutoRelacionadoFornecedor(response.getOcorrencias(i).getDsProdutoRelacionadoFornecedor());
            itemLista.setCdMomentoProcessamentoFornecedor(response.getOcorrencias(i)
                .getCdMomentoProcessamentoFornecedor());
            itemLista.setDsMomentoProcessamentoFornecedor(response.getOcorrencias(i)
                .getDsMomentoProcessamentoFornecedor());
            itemLista.setCdPagamentoUtilFornecedor(response.getOcorrencias(i).getCdPagamentoUtilFornecedor());
            itemLista.setDsPagamentoUtilFornecedor(response.getOcorrencias(i).getDsPagamentoUtilFornecedor());
            itemLista.setNrQuantidadeDiasFloatFornecedor(response.getOcorrencias(i)
                .getNrQuantidadeDiasFloatFornecedor());
            itemLista.setCdIndicadorFeriadoFornecedor(response.getOcorrencias(i).getCdIndicadorFeriadoFornecedor());
            itemLista.setDsIndicadorFeriadoFornecedor(response.getOcorrencias(i).getDsIndicadorFeriadoFornecedor());
            itemLista.setCdIndicadorFornecedorContrato(response.getOcorrencias(i).getCdIndicadorFornecedorContrato());

            listaOcorrenciasFornecedor.add(itemLista);
        }

        saida.setOcorrenciasFornecedor(listaOcorrenciasFornecedor);

        numeroOcorrenciasTributo = response.getOcorrencias1Count();
        for (int i = 0; i < numeroOcorrenciasTributo; i++) {
            ImprimirContratoOcorrencias1SaidaDTO itemLista = new ImprimirContratoOcorrencias1SaidaDTO();

            itemLista.setCdIndicadorModalidadeTributos(response.getOcorrencias1(i).getCdIndicadorModalidadeTributos());
            itemLista.setCdProdutoRelacionadoTributo(response.getOcorrencias1(i).getCdProdutoRelacionadoTributo());
            itemLista.setDsProdutoRelacionadoTributo(response.getOcorrencias1(i).getDsProdutoRelacionadoTributo());
            itemLista
                .setCdMomentoProcessamentoTributos(response.getOcorrencias1(i).getCdMomentoProcessamentoTributos());
            itemLista
                .setDsMomentoProcessamentoTributos(response.getOcorrencias1(i).getDsMomentoProcessamentoTributos());
            itemLista.setCdIndicadorFeriadoTributos(response.getOcorrencias1(i).getCdIndicadorFeriadoTributos());
            itemLista.setDsIndicadorFeriadoTributos(response.getOcorrencias1(i).getDsIndicadorFeriadoTributos());
            itemLista.setCdIndicadorTributoContrato(response.getOcorrencias1(i).getCdIndicadorTributoContrato());

            listaOcorrenciasTributo.add(itemLista);
        }

        saida.setOcorrenciasTributo(listaOcorrenciasTributo);

        numeroOcorrenciasSalario = response.getOcorrencias2Count();
        for (int i = 0; i < numeroOcorrenciasSalario; i++) {
            ImprimirContratoOcorrencias2SaidaDTO itemLista = new ImprimirContratoOcorrencias2SaidaDTO();

            itemLista.setCdIndicadorModalidadeSalario(response.getOcorrencias2(i).getCdIndicadorModalidadeSalario());
            itemLista.setCdProdutoRelacionadoSalario(response.getOcorrencias2(i).getCdProdutoRelacionadoSalario());
            itemLista.setDsProdutoRelacionadoSalario(response.getOcorrencias2(i).getDsProdutoRelacionadoSalario());
            itemLista.setCdMomeProcessamentoSalario(response.getOcorrencias2(i).getCdMomeProcessamentoSalario());
            itemLista.setDsMomeProcessamentoSalario(response.getOcorrencias2(i).getDsMomeProcessamentoSalario());
            itemLista.setCdPagamentoUtilSalario(response.getOcorrencias2(i).getCdPagamentoUtilSalario());
            itemLista.setDsPagamentoUtilSalario(response.getOcorrencias2(i).getDsPagamentoUtilSalario());
            itemLista.setNrQuantidadeDiaFLoatSalario(response.getOcorrencias2(i).getNrQuantidadeDiaFLoatSalario());
            itemLista.setCdIndicadorFeriadoSalario(response.getOcorrencias2(i).getCdIndicadorFeriadoSalario());
            itemLista.setDsIndicadorFeriadoSalario(response.getOcorrencias2(i).getDsIndicadorFeriadoSalario());
            itemLista.setCdIndicadorRepiqueSalario(response.getOcorrencias2(i).getCdIndicadorRepiqueSalario());
            itemLista.setNrQuantidadeRepiqueSalario(response.getOcorrencias2(i).getNrQuantidadeRepiqueSalario());
            itemLista.setCdIndicadorSalarioContrato(response.getOcorrencias2(i).getCdIndicadorSalarioContrato());

            listaOcorrenciasSalario.add(itemLista);
        }

        saida.setOcorrenciasSalario(listaOcorrenciasSalario);

        numeroOcorrenciasFuncionario = response.getOcorrencias3Count();
        for (int i = 0; i < numeroOcorrenciasFuncionario; i++) {
            ImprimirContratoOcorrencias3SaidaDTO itemLista = new ImprimirContratoOcorrencias3SaidaDTO();

            itemLista.setDsProdutoSalarioFuncionarios(response.getOcorrencias3(i).getDsProdutoSalarioFuncionarios());
            itemLista.setDsServicoSalarioFuncional(response.getOcorrencias3(i).getDsServicoSalarioFuncional());

            listaOcorrenciasFuncionario.add(itemLista);
        }

        saida.setOcorrenciasFuncionario(listaOcorrenciasFuncionario);

        numeroOcorrenciasCaixa = response.getOcorrencias4Count();
        for (int i = 0; i < numeroOcorrenciasCaixa; i++) {
            ImprimirContratoOcorrencias4SaidaDTO itemLista = new ImprimirContratoOcorrencias4SaidaDTO();

            itemLista.setDsProdutoSalarioGuicheCaixa(response.getOcorrencias4(i).getDsProdutoSalarioGuicheCaixa());
            itemLista.setDsServicoSalarioGuicheCaixa(response.getOcorrencias4(i).getDsServicoSalarioGuicheCaixa());

            listaOcorrenciasCaixa.add(itemLista);
        }

        saida.setOcorrenciasGuicheCaixa(listaOcorrenciasCaixa);

        numeroOcorrenciasAtendimento = response.getOcorrencias5Count();
        for (int i = 0; i < numeroOcorrenciasAtendimento; i++) {
            ImprimirContratoOcorrencias5SaidaDTO itemLista = new ImprimirContratoOcorrencias5SaidaDTO();

            itemLista.setDsProdutoSalarioAutoAtendimento(response.getOcorrencias5(i)
                .getDsProdutoSalarioAutoAtendimento());
            itemLista.setDsServicoSalarioAutoAtendimento(response.getOcorrencias5(i)
                .getDsServicoSalarioAutoAtendimento());

            listaOcorrenciasAtendimento.add(itemLista);
        }

        saida.setOcorrenciasAutoAtendimento(listaOcorrenciasAtendimento);

        numeroOcorrenciasFoneFacil = response.getOcorrencias6Count();
        for (int i = 0; i < numeroOcorrenciasFoneFacil; i++) {
            ImprimirContratoOcorrencias6SaidaDTO itemLista = new ImprimirContratoOcorrencias6SaidaDTO();

            itemLista.setDsProdutoSalarioFoneFacil(response.getOcorrencias6(i).getDsProdutoSalarioFoneFacil());
            itemLista.setDsServicoSalarioFoneFacil(response.getOcorrencias6(i).getDsServicoSalarioFoneFacil());

            listaOcorrenciasFoneFacil.add(itemLista);
        }

        saida.setOcorrenciasFoneFacil(listaOcorrenciasFoneFacil);

        numeroOcorrenciasInternet = response.getOcorrencias7Count();
        for (int i = 0; i < numeroOcorrenciasInternet; i++) {
            ImprimirContratoOcorrencias7SaidaDTO itemLista = new ImprimirContratoOcorrencias7SaidaDTO();

            itemLista.setDsProdutoSalarioInternetBanking(response.getOcorrencias7(i)
                .getDsProdutoSalarioInternetBanking());
            itemLista.setDsServicoSalarioInternetBanking(response.getOcorrencias7(i)
                .getDsServicoSalarioInternetBanking());

            listaOcorrenciasInternet.add(itemLista);
        }

        saida.setOcorrenciasInternet(listaOcorrenciasInternet);

        return saida;
    }

    /**
     * 
     * @param entrada
     *            the entrada
     * @return the listar comprovante salarial saida dto
     * @see br.com.bradesco.web.pgit.service.business.contrato.IContratoService#listarComprovanteSalarial
     *      (br.com.bradesco.web.pgit.service.business.contrato.bean.ListarComprovanteSalarialEntradaDTO)
     */
    public ListarComprovanteSalarialSaidaDTO listarComprovanteSalarial(ListarComprovanteSalarialEntradaDTO entrada) {
        ListarComprovanteSalarialIntranetResponse response = null;
        ListarComprovanteSalarialSaidaDTO saida = new ListarComprovanteSalarialSaidaDTO();
        int numeroOcorrenciasFornecedor = 0;
        ArrayList<ListarComprovanteSalarialOcorrenciasSaidaDTO> listaOcorrencias =
            new ArrayList<ListarComprovanteSalarialOcorrenciasSaidaDTO>();
        ListarComprovanteSalarialIntranetRequest request = new ListarComprovanteSalarialIntranetRequest();

        request.setMaxOcorrencias(verificaIntegerNulo(entrada.getMaxOcorrencias()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrAditivoContratoNegocio(verificaLongNulo(entrada.getNrAditivoContratoNegocio()));

        try{
	        response = getFactoryAdapter().getListarComprovanteSalarialIntranetPDCAdapter().invokeProcess(request);
	
	        saida.setCodMensagem(response.getCodMensagem());
	        saida.setMensagem(response.getMensagem());
	        saida.setNrComprovantes(response.getNrComprovantes());
	        saida.setQtdeMaximaDetalheComprovante(response.getQtdeMaximaDetalheComprovante());
	        saida.setQtdeEmissaoComprovantePagamento(response.getQtdeEmissaoComprovantePagamento());
	        saida.setCdMidiaComprovanteSalarial(response.getCdMidiaComprovanteSalarial());
	
	        numeroOcorrenciasFornecedor = response.getOcorrenciasCount();
	        for (int i = 0; i < numeroOcorrenciasFornecedor; i++) {
	            ListarComprovanteSalarialOcorrenciasSaidaDTO itemLista = new ListarComprovanteSalarialOcorrenciasSaidaDTO();
	            itemLista.setCdBanco(response.getOcorrencias(i).getCdBanco());
	            itemLista.setDsBanco(response.getOcorrencias(i).getDsBanco());
	            itemLista.setCdAgenciaBancaria(response.getOcorrencias(i).getCdAgenciaBancaria());
	            itemLista.setCdContaCorrente(response.getOcorrencias(i).getCdContaCorrente());
	            itemLista.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	            itemLista.setCdCpfCnpj(response.getOcorrencias(i).getCdCpfCnpj());
	            itemLista.setCdEmpresaComprovanteSalarial(response.getOcorrencias(i).getCdEmpresaComprovanteSalarial());
	            listaOcorrencias.add(itemLista);
	        }
	
	        saida.setOcorrencias(listaOcorrencias);
        }catch(PdcAdapterFunctionalException e){
        	saida = new ListarComprovanteSalarialSaidaDTO();
        }
        
        return saida;
    }

    /**
     * 
     * @param entrada
     *            the entrada
     * @return the listar participantes saida dto
     * @see br.com.bradesco.web.pgit.service.business.contrato.IContratoService#listarParticipantes
     *      (br.com.bradesco.web.pgit.service.business.contrato.bean.ListarParticipantesEntradaDTO)
     */
	private ListarParticipantesSaidaDTO listarParticipantes(
			ListarParticipantesEntradaDTO entrada) {
		ListarParticipantesIntranetResponse response = null;
		ArrayList<ListarParticipantesOcorrenciasSaidaDTO> listaOcorrencias = new ArrayList<ListarParticipantesOcorrenciasSaidaDTO>();
		ListarParticipantesSaidaDTO saida = new ListarParticipantesSaidaDTO();
		ListarParticipantesIntranetRequest request = new ListarParticipantesIntranetRequest();

		request.setMaxOcorrencias(30);
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setNrAditivoContratoNegocio(verificaLongNulo(entrada
				.getNrAditivoContratoNegocio()));
		request.setCdTipoEntrada(verificaIntegerNulo(entrada.getCdTipoEntrada()));

        response = getFactoryAdapter().getListarParticipantesIntranetPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNrParticipantes(response.getNrParticipantes());

        for (br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesintranet.response.Ocorrencias ocorr : response
            .getOcorrencias()) {

            ListarParticipantesOcorrenciasSaidaDTO itemLista = new ListarParticipantesOcorrenciasSaidaDTO();
            itemLista.setCdIndicadorParticipante(ocorr.getCdIndicadorParticipante());
            itemLista.setCdCpfCnpj(ocorr.getCdCpfCnpj());
            itemLista.setCdParticipante(ocorr.getCdParticipante());
            itemLista.setDsNomeParticipante(ocorr.getDsNomeParticipante());
            itemLista.setCdAgencia(ocorr.getCdAgencia());
            itemLista.setCdDigitoAgencia(ocorr.getCdDigitoAgencia());
            itemLista.setDsAgencia(ocorr.getDsAgencia());
            itemLista.setCdConta(ocorr.getCdConta());
            itemLista.setCdDigitoConta(ocorr.getCdDigitoConta());
            itemLista.setDsLogradouroParticipante(ocorr.getDsLogradouroParticipante());
            itemLista.setNrLogradouroParticipante(ocorr.getNrLogradouroParticipante());
            itemLista.setDsBairroClienteParticipante(ocorr.getDsBairroClienteParticipante());
            itemLista.setDsMunicipioClienteParticipante(ocorr.getDsMunicipioClienteParticipante());
            itemLista.setDsSiglaUfParticipante(ocorr.getDsSiglaUfParticipante());
            itemLista.setCdCep(ocorr.getCdCep());
            itemLista.setCdCepComplemento(ocorr.getCdCepComplemento());
            itemLista.setCdIndicadorQuebra(ocorr.getCdIndicadorQuebra());
            listaOcorrencias.add(itemLista);
        }

		saida.setOcorrencias(listaOcorrencias);
		return saida;
	}

    /**
     * 
     * @param cdTipoContratoNegocio
     *            the cd tipo contrato negocio
     * @param cdPessoaJuridicaContrato
     *            the cd pessoa juridica contrato
     * @param nrSequenciaContratoNegocio
     *            the nr sequencia contrato negocio
     * @param responseOutput
     *            the response output
     * @param pathRel
     *            the path rel
     * 
     * @see br.com.bradesco.web.pgit.service.business.contrato.IContratoService
     *      #imprimirContratoPrestacaoServico(java.lang.Integer, java.lang.Long, java.lang.Long, java.io.OutputStream,
     *      java.lang.String)
     */
    public void imprimirContratoPrestacaoServico(Integer cdTipoContratoNegocio, Long cdPessoaJuridicaContrato,
        Long nrSequenciaContratoNegocio, Long nrAditivoContratoNegocio, OutputStream responseOutput, String pathRel) {

        ListarComprovanteSalarialEntradaDTO entradaComprovanteSalarial = new ListarComprovanteSalarialEntradaDTO();
        ImprimirContratoSaidaDTO saidaImprimirContrato = null;
        ListarParticipantesEntradaDTO entradaParticipante = new ListarParticipantesEntradaDTO();
        ListarParticipantesSaidaDTO saidaListarParticipantes = null;
        ListarParticipantesSaidaDTO saidaListarParticipantesContas = null;
        ListarComprovanteSalarialSaidaDTO saidaListarComprovanteSalarial = new ListarComprovanteSalarialSaidaDTO();
        ContratoPrestacaoServico contrato = null;
        ImprimirContratoEntradaDTO entradaImprimirContrato = new ImprimirContratoEntradaDTO();
        entradaImprimirContrato.setCdPessoaJuridicaContrato(cdPessoaJuridicaContrato);
        entradaImprimirContrato.setCdTipoContratoNegocio(cdTipoContratoNegocio);
        entradaImprimirContrato.setNrSequenciaContratoNegocio(nrSequenciaContratoNegocio);
        entradaImprimirContrato.setNrAditivoContratoNegocio(nrAditivoContratoNegocio);

        saidaImprimirContrato = imprimirContrato(entradaImprimirContrato);

        entradaParticipante.setCdPessoaJuridicaContrato(cdPessoaJuridicaContrato);
        entradaParticipante.setCdTipoContratoNegocio(cdTipoContratoNegocio);
        entradaParticipante.setNrSequenciaContratoNegocio(nrSequenciaContratoNegocio);
        entradaParticipante.setNrAditivoContratoNegocio(nrAditivoContratoNegocio);
        entradaParticipante.setCdTipoEntrada(1);
        saidaListarParticipantes = listarParticipantes(entradaParticipante);
        
        entradaParticipante.setCdTipoEntrada(2);
        saidaListarParticipantesContas = listarParticipantes(entradaParticipante);

        if ("S".equalsIgnoreCase(saidaImprimirContrato.getCdIndicadorComprovanteSalarial())) {
            entradaComprovanteSalarial.setMaxOcorrencias(150);
            entradaComprovanteSalarial.setCdPessoaJuridicaContrato(cdPessoaJuridicaContrato);
            entradaComprovanteSalarial.setCdTipoContratoNegocio(cdTipoContratoNegocio);
            entradaComprovanteSalarial.setNrSequenciaContratoNegocio(nrSequenciaContratoNegocio);
            entradaComprovanteSalarial.setNrAditivoContratoNegocio(nrAditivoContratoNegocio);

            saidaListarComprovanteSalarial = listarComprovanteSalarial(entradaComprovanteSalarial);
        }

        contrato =
            preencherContrato(saidaImprimirContrato, saidaListarParticipantes, saidaListarParticipantesContas,
                saidaListarComprovanteSalarial);

        impressaoContrato.imprimirContratoPrestacaoServico(contrato, pathRel, responseOutput);
    }

    /**
     * Preencher contrato.
     * 
     * @param saidaImprimirContrato
     *            the saida imprimir contrato
     * @param saidaListarParticipantes
     *            the saida listar participantes
     * @param saidaListarParticipantesContas
     *            the saida listar participantes contas
     * @param saidaListarComprovanteSalarial
     *            the saida listar comprovante salarial
     * @return the contrato prestacao servico
     */
    private ContratoPrestacaoServico preencherContrato(ImprimirContratoSaidaDTO saidaImprimirContrato,
        ListarParticipantesSaidaDTO saidaListarParticipantes,
        ListarParticipantesSaidaDTO saidaListarParticipantesContas,
        ListarComprovanteSalarialSaidaDTO saidaListarComprovanteSalarial) {

        ContratoPrestacaoServico contrato = new ContratoPrestacaoServico();
        contrato.setIdentificacao(preencherIdentificacao(saidaImprimirContrato, saidaListarParticipantes,
            saidaListarParticipantesContas));
        contrato.setAnexo(preencherAnexo(saidaImprimirContrato, saidaListarComprovanteSalarial));
        contrato.setVersao(saidaImprimirContrato.getNrAditivoContratoNegocio().toString());
        contrato.setData(saidaImprimirContrato.getDtGeracaoContratoNegocio());

        return contrato;
    }

    /**
     * Preencher identificacao.
     * 
     * @param saidaImprimirContrato
     *            the saida imprimir contrato
     * @param saidaListarParticipantes
     *            the saida listar participantes
     * @param saidaListarParticipantesContas
     *            the saida listar participantes contas
     * @return the identificacao contrato
     */
    private IdentificacaoContrato preencherIdentificacao(ImprimirContratoSaidaDTO saidaImprimirContrato,
        ListarParticipantesSaidaDTO saidaListarParticipantes, ListarParticipantesSaidaDTO saidaListarParticipantesContas) {
        String data = null;
        IdentificacaoContrato identificacao = new IdentificacaoContrato();
        DadosIdentificacao identificacaoMatriz = new DadosIdentificacao();
        DadosIdentificacao identificacaoParticipante = new DadosIdentificacao();
        List<DadosIdentificacao> listaIdentificacaoParticipantes = new ArrayList<DadosIdentificacao>();
        List<Participante> participantes = new ArrayList<Participante>();
        Integer nrParticipantes = saidaListarParticipantes.getOcorrencias().size();
        List<Conta> contas = new ArrayList<Conta>();
        Assinatura assinatura = new Assinatura();
        Conta conta = new Conta();
        ListarParticipantesOcorrenciasSaidaDTO ocorrencia = null;
        String msgAutorizacao = null;

        // existem participantes
        if (nrParticipantes != null && nrParticipantes > 0) {

            // percorre participantes
            for (int i = 0; i < nrParticipantes; i++) {
                ocorrencia = saidaListarParticipantes.getOcorrencias().get(i);

                if (ocorrencia.getCdIndicadorParticipante() == 1) {
                    // nao tem quebra
                    if (ocorrencia.getCdIndicadorQuebra().equals("N") || i == 0) {
                        identificacaoMatriz.setRazaoSocialNome(ocorrencia.getDsNomeParticipante());
                        identificacaoMatriz.setCnpjCpf(ocorrencia.getCdCpfCnpj());
                        identificacaoMatriz.setEndereco(ocorrencia.getDsLogradouroParticipante() + " "
                            + ocorrencia.getNrLogradouroParticipante());
                        identificacaoMatriz.setBairro(ocorrencia.getDsBairroClienteParticipante());
                        identificacaoMatriz.setCidade(ocorrencia.getDsMunicipioClienteParticipante());
                        identificacaoMatriz.setCep(String.format("%05d", ocorrencia.getCdCep()) + "-"
                            + String.format("%03d", ocorrencia.getCdCepComplemento()));
                        identificacaoMatriz.setUf(ocorrencia.getDsSiglaUfParticipante());

                        assinatura.setCliente(ocorrencia.getDsNomeParticipante());
                        assinatura.setClienteCnpjCpf(ocorrencia.getCdCpfCnpj());
                    }
                } else if (ocorrencia.getCdIndicadorParticipante() == 7) {

                    if (!ocorrencia.getCdIndicadorQuebra().equals("N")) {
                        identificacaoParticipante = new DadosIdentificacao();

                        identificacaoParticipante.setRazaoSocialNome(ocorrencia.getDsNomeParticipante());
                        identificacaoParticipante.setCnpjCpf(ocorrencia.getCdCpfCnpj());
                        identificacaoParticipante.setEndereco(ocorrencia.getDsLogradouroParticipante() + " "
                            + ocorrencia.getNrLogradouroParticipante());
                        identificacaoParticipante.setBairro(ocorrencia.getDsBairroClienteParticipante());
                        identificacaoParticipante.setCidade(ocorrencia.getDsMunicipioClienteParticipante());
                        identificacaoParticipante.setCep(String.format("%05d", ocorrencia.getCdCep()) + "-"
                            + String.format("%03d", ocorrencia.getCdCepComplemento()));
                        identificacaoParticipante.setUf(ocorrencia.getDsSiglaUfParticipante());

                        listaIdentificacaoParticipantes.add(identificacaoParticipante);

                        participantes.add(new Participante(ocorrencia.getDsNomeParticipante(), ocorrencia
                            .getCdCpfCnpj()));
                    }
                }
            }
        }

        if (saidaListarParticipantesContas.getOcorrencias().size() > 0) {
            for (ListarParticipantesOcorrenciasSaidaDTO ocorrConta : saidaListarParticipantesContas.getOcorrencias()) {
                conta = new Conta();

                conta.setAgencia(ocorrConta.getDsAgencia());
                conta.setCodigo(ocorrConta.getCdAgencia().toString() + "-" + ocorrConta.getCdDigitoAgencia());
                conta.setContaDebito(ocorrConta.getCdConta().toString());
                conta.setDigito(ocorrConta.getCdDigitoConta());
                conta.setRazaoSocialNome(ocorrConta.getDsNomeParticipante());
                conta.setCnpjCpf(ocorrConta.getCdCpfCnpj());

                contas.add(conta);
            }
        }

        data =
            dataAssinaturaFormatada(saidaImprimirContrato.getDsMunicipioAgenciaGestora(), saidaImprimirContrato
                .getDsSiglaUfParticipante());
        assinatura.setData(data);
        assinatura.setParticipantes(participantes);

        identificacao.setNome(saidaImprimirContrato.getDsBanco());
        identificacao.setCnpjCpf(saidaImprimirContrato.getCdCpfCnpj());
        identificacao.setNumConvenio(saidaImprimirContrato.getNrSequenciaContratoNegocio().toString());

        identificacao.setEndereco(enderecoIdentificacaoFormatado(saidaImprimirContrato.getDsLogradouroPagador(),
            saidaImprimirContrato.getDsNumeroLogradouroPagador(), saidaImprimirContrato
                .getDsComplementoLogradouroPagador(), saidaImprimirContrato.getDsBairroClientePagador(),
            saidaImprimirContrato.getDsMunicipioClientePagador(), saidaImprimirContrato.getCdSiglaUfPagador(),
            saidaImprimirContrato.getCdCepPagador(), saidaImprimirContrato.getCdCepComplementoPagador()));

        identificacao.setAgGestoraConvenio(saidaImprimirContrato.getCdAgenciaGestora() + " - "
            + saidaImprimirContrato.getDsAgenciaGestora());

        // verifica forma pagamento
        if (saidaImprimirContrato.getCdFormaAutorizacaoPagamento() != null
            && saidaImprimirContrato.getCdFormaAutorizacaoPagamento() == 1) {

            identificacao.setFormaAutorizacaoSelecionada(getI18nMessage("contrato_net_empresa_formaAutorizacao"));
            identificacao
                .setFormaAutorizacaoParagrafo(getI18nMessage("contrato_net_empresa_autorizacaoCanalBradescoPrimeiroParagrafo"));
        } else if (saidaImprimirContrato.getCdFormaAutorizacaoPagamento() != null
            && saidaImprimirContrato.getCdFormaAutorizacaoPagamento() == 3) {

            identificacao
                .setFormaAutorizacaoSelecionada(getI18nMessage("contrato_net_empresa_arquivo_formaAutorizacao"));

            msgAutorizacao = getI18nMessage("contrato_net_empresa_arquivo_autorizacaoCanalBradescoPrimeiroParagrafo");
            identificacao.setFormaAutorizacaoParagrafo(msgAutorizacao);
        }

        identificacao.setIdentificacaoMatriz(identificacaoMatriz);
        identificacao.setIdentificacaoParticipantes(listaIdentificacaoParticipantes);
        identificacao.setContas(contas);
        identificacao.setAssinatura(assinatura);
        identificacao.setHrLimiteTransmissaoArquivo(saidaImprimirContrato.getHrLimiteTransmissaoArquivo());
        identificacao.setHrLimiteEfetivacaoPagamento(saidaImprimirContrato.getHrLimiteEfetivacaoPagamento());
        identificacao.setHrCicloEfetivacaoPagamento(saidaImprimirContrato.getHrCicloEfetivacaoPagamento());
        identificacao.setHrLimiteConsultaPagamento(saidaImprimirContrato.getHrLimiteConsultaPagamento());

        return identificacao;
    }

    /**
     * Preencher anexo.
     * 
     * @param saidaImprimirContrato
     *            the saida imprimir contrato
     * @param saidaListarComprovanteSalarial
     *            the saida listar comprovante salarial
     * @return the anexo
     * @throws
     * @see
     */
    private Anexo preencherAnexo(ImprimirContratoSaidaDTO saidaImprimirContrato,
        ListarComprovanteSalarialSaidaDTO saidaListarComprovanteSalarial) {

        Anexo anexo = new Anexo();

        anexo.setCdIndicadorServicoFornecedor(saidaImprimirContrato.getCdIndicadorServicoFornecedor());
        anexo.setCdIndicadorServicoTributos(saidaImprimirContrato.getCdIndicadorServicoTributos());
        anexo.setCdIndicadorServicoSalario(saidaImprimirContrato.getCdIndicadorServicoSalario());
        anexo.setCdIndicadorServicoComprovanteSalarial(saidaImprimirContrato.getCdIndicadorComprovanteSalarial());

        if ("S".equalsIgnoreCase(saidaImprimirContrato.getCdIndicadorServicoFornecedor())) {
            anexo.setAnexoFornecedores(preencherFornecedores(saidaImprimirContrato));
        } else {
            anexo.setAnexoFornecedores(new AnexoFornecedores());
        }

        if ("S".equalsIgnoreCase(saidaImprimirContrato.getCdIndicadorServicoTributos())) {
            anexo.setAnexoTributosContas(preencherTributoContas(saidaImprimirContrato));
        } else {
            anexo.setAnexoTributosContas(new AnexoTributosContas());
        }

        if ("S".equalsIgnoreCase(saidaImprimirContrato.getCdIndicadorServicoSalario())) {
            anexo.setAnexoSalarios(preencherSalarios(saidaImprimirContrato));
        } else {
            anexo.setAnexoSalarios(new AnexoSalarios());
        }

        if ("S".equalsIgnoreCase(saidaImprimirContrato.getCdIndicadorComprovanteSalarial())) {
            anexo.setAnexoComprovanteSalarial(preencherComprovanteSalarial(saidaImprimirContrato,
                saidaListarComprovanteSalarial));
            if(anexo.getAnexoComprovanteSalarial().getDadosEmpresaParticipantes() == null){
            	anexo.setCdIndicadorServicoComprovanteSalarial("X");
            }
        } else {
            anexo.setAnexoComprovanteSalarial(new AnexoComprovanteSalarial());
        }
        return anexo;
    }

    /**
     * Preencher comprovante salarial.
     * 
     * @param saidaImprimirContrato
     *            the saida imprimir contrato
     * @param saidaListarComprovanteSalarial
     *            the saida listar comprovante salarial
     * @return the anexo comprovante salarial
     * @throws
     * @see
     */
    private AnexoComprovanteSalarial preencherComprovanteSalarial(ImprimirContratoSaidaDTO saidaImprimirContrato,
        ListarComprovanteSalarialSaidaDTO saidaListarComprovanteSalarial) {

        int numeroOcorrencias = 0;
        List<EmpresaParticipante> dadosEmpresaParticipantes = new ArrayList<EmpresaParticipante>();
        AnexoComprovanteSalarial anexoComprovanteSalarial = new AnexoComprovanteSalarial();
        if (saidaListarComprovanteSalarial.getCdMidiaComprovanteSalarial() != null) {
            numeroOcorrencias = saidaListarComprovanteSalarial.getOcorrencias().size();
            // percorre comprovante salarial
            for (int i = 0; i < numeroOcorrencias; i++) {
                ListarComprovanteSalarialOcorrenciasSaidaDTO ocorrencia =
                    saidaListarComprovanteSalarial.getOcorrencias().get(i);
                EmpresaParticipante item = new EmpresaParticipante();
                item.setCnpj(ocorrencia.getCdCpfCnpj());
                item.setEmpresa(ocorrencia.getDsBanco());
                item.setPerfilTransmissao(ocorrencia.getCdEmpresaComprovanteSalarial().toString());
                item.setAgencia(ocorrencia.getCdAgenciaBancaria().toString());
                item.setConta(ocorrencia.getCdContaCorrente() + " - " + ocorrencia.getCdDigitoConta());

                dadosEmpresaParticipantes.add(item);
            }
            anexoComprovanteSalarial.setDadosEmpresaParticipantes(dadosEmpresaParticipantes);

            if ("B".equalsIgnoreCase(saidaListarComprovanteSalarial.getCdMidiaComprovanteSalarial())) {
                anexoComprovanteSalarial.setMidiaAutoAtendimento("X");
            } else if ("I".equalsIgnoreCase(saidaListarComprovanteSalarial.getCdMidiaComprovanteSalarial())) {
                anexoComprovanteSalarial.setMidiaInternet("X");
            } else if ("A".equalsIgnoreCase(saidaListarComprovanteSalarial.getCdMidiaComprovanteSalarial())) {
                anexoComprovanteSalarial.setMidiaAutoAtendimentoInternet("X");
            }

            anexoComprovanteSalarial.setQtdeComprovantesEmissao(saidaListarComprovanteSalarial
                .getQtdeMaximaDetalheComprovante().toString());

            NumerosEmissoesPagasEnum numerosEnum =
                NumerosEmissoesPagasEnum.getEnumByNumero(saidaListarComprovanteSalarial
                    .getQtdeEmissaoComprovantePagamento());
            String numeroExtenso = "";
            if (numerosEnum != null) {
                numeroExtenso = numerosEnum.getDescricao();
            }
            anexoComprovanteSalarial.setQtdeEmissoesPaga(numeroExtenso);
            anexoComprovanteSalarial.setNrQuantidadeLimiteCobrancaImpressao(saidaImprimirContrato
                .getNrQuantidadeLimiteCobrancaImpressao().toString());
            anexoComprovanteSalarial.setDsDisponibilizacaoComprovanteBanco(saidaImprimirContrato
                .getDsDisponibilizacaoComprovanteBanco());
            anexoComprovanteSalarial.setHrLimiteApresentaCarta(saidaImprimirContrato.getHrLimiteApresentaCarta());
            anexoComprovanteSalarial.setDsFrasePadraoCaput(saidaImprimirContrato.getDsFrasePadraoCaput());
        }
        return anexoComprovanteSalarial;
    }

    /**
     * Preencher salarios.
     * 
     * @param saidaImprimirContrato
     *            the saida imprimir contrato
     * @return the anexo salarios
     * @throws
     * @see
     */
    private AnexoSalarios preencherSalarios(ImprimirContratoSaidaDTO saidaImprimirContrato) {
        ModalidadeCondicao modalidadeCreditoConta = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeTed = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeDoc = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeOrdemPagamento = new ModalidadeCondicao();
        int numeroOcorrenciasSalario = 0;
        int numeroOcorrenciasFuncionario = 0;
        int numeroOcorrenciasCaixa = 0;
        int numeroOcorrenciasAtendimento = 0;
        int numeroOcorrenciasFoneFacil = 0;
        int numeroOcorrenciasInternet = 0;
        List<ProdutoServico> isentoTarifas = new ArrayList<ProdutoServico>();
        List<ProdutoServico> caixa = new ArrayList<ProdutoServico>();
        List<ProdutoServico> autoAtendimento = new ArrayList<ProdutoServico>();
        List<ProdutoServico> foneFacil = new ArrayList<ProdutoServico>();
        List<ProdutoServico> internet = new ArrayList<ProdutoServico>();

        numeroOcorrenciasSalario = saidaImprimirContrato.getOcorrenciasSalario().size();
        for (int i = 0; i < numeroOcorrenciasSalario; i++) {
            ImprimirContratoOcorrencias2SaidaDTO ocorrencia = saidaImprimirContrato.getOcorrenciasSalario().get(i);
            ModalidadeCondicao modalidade = new ModalidadeCondicao();

            if ("S".equalsIgnoreCase(ocorrencia.getCdIndicadorSalarioContrato())) {
                modalidade.setFlagModalidade("X");

                if (ocorrencia.getCdMomeProcessamentoSalario() == 1) {
                    modalidade.setFlagProcessamentoCiclico("X");
                } else if (ocorrencia.getCdMomeProcessamentoSalario() == 2) {
                    modalidade.setFlagProcessamentoDiario("X");
                }

                if (ocorrencia.getCdPagamentoUtilSalario() == 1) {
                    modalidade.setFlagPagDiaNaoUtilAntecipa("X");
                } else if (ocorrencia.getCdPagamentoUtilSalario() == 2) {
                    modalidade.setFlagPagDiaNaoUtilPosterga("X");
                }

                if (ocorrencia.getCdIndicadorFeriadoSalario() == 2) {
                    modalidade.setFlagPagFeriadoAcata("X");
                } else if (ocorrencia.getCdIndicadorFeriadoSalario() == 3) {
                    modalidade.setFlagPagFeriadoAcataReagendamento("X");
                } else if (ocorrencia.getCdIndicadorFeriadoSalario() == 4) {
                    modalidade.setFlagPagFeriadoAntecipa("X");
                } else if (ocorrencia.getCdIndicadorFeriadoSalario() == 5) {
                    modalidade.setFlagPagFeriadoPosterga("X");

                }

                if ("S".equalsIgnoreCase(ocorrencia.getCdIndicadorRepiqueSalario())) {
                    modalidade.setDiasRepiqueSim("X");
                } else if ("N".equalsIgnoreCase(ocorrencia.getCdIndicadorRepiqueSalario())) {
                    modalidade.setDiasRepiqueNao("X");
                }

                modalidade.setQtdeDiasFloating(String.format("%02d", ocorrencia.getNrQuantidadeDiaFLoatSalario()));
                modalidade.setQtdeDiasRepique(String.format("%02d", ocorrencia.getNrQuantidadeRepiqueSalario()));
            }

            // verifica modalidade
            if (ocorrencia.getCdIndicadorModalidadeSalario() == 1) {
                modalidadeCreditoConta = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeSalario() == 2) {
                modalidadeDoc = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeSalario() == 3) {
                modalidadeTed = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeSalario() == 4) {
                modalidadeOrdemPagamento = modalidade;
            }
        }

        numeroOcorrenciasFuncionario = saidaImprimirContrato.getOcorrenciasFuncionario().size();
        for (int i = 0; i < numeroOcorrenciasFuncionario; i++) {
            ImprimirContratoOcorrencias3SaidaDTO ocorrencia = saidaImprimirContrato.getOcorrenciasFuncionario().get(i);

            // produto nao vazio
            if (ocorrencia.getDsProdutoSalarioFuncionarios() != null
                && !"".equals(ocorrencia.getDsProdutoSalarioFuncionarios())) {
                ProdutoServico item = new ProdutoServico();
                item.setProduto(ocorrencia.getDsProdutoSalarioFuncionarios());
                item.setServico(ocorrencia.getDsServicoSalarioFuncional());
                isentoTarifas.add(item);
            }
        }

        numeroOcorrenciasCaixa = saidaImprimirContrato.getOcorrenciasGuicheCaixa().size();
        for (int i = 0; i < numeroOcorrenciasCaixa; i++) {
            ImprimirContratoOcorrencias4SaidaDTO ocorrencia = saidaImprimirContrato.getOcorrenciasGuicheCaixa().get(i);

            // produto nao vazio
            if (ocorrencia.getDsProdutoSalarioGuicheCaixa() != null
                && !"".equals(ocorrencia.getDsProdutoSalarioGuicheCaixa())) {
                ProdutoServico item = new ProdutoServico();
                item.setProduto(ocorrencia.getDsProdutoSalarioGuicheCaixa());
                item.setServico(ocorrencia.getDsServicoSalarioGuicheCaixa());
                caixa.add(item);
            }
        }

        numeroOcorrenciasAtendimento = saidaImprimirContrato.getOcorrenciasAutoAtendimento().size();
        for (int i = 0; i < numeroOcorrenciasAtendimento; i++) {
            ImprimirContratoOcorrencias5SaidaDTO ocorrencia =
                saidaImprimirContrato.getOcorrenciasAutoAtendimento().get(i);

            // produto nao vazio
            if (ocorrencia.getDsProdutoSalarioAutoAtendimento() != null
                && !"".equals(ocorrencia.getDsProdutoSalarioAutoAtendimento())) {
                ProdutoServico item = new ProdutoServico();
                item.setProduto(ocorrencia.getDsProdutoSalarioAutoAtendimento());
                item.setServico(ocorrencia.getDsServicoSalarioAutoAtendimento());
                autoAtendimento.add(item);
            }
        }

        numeroOcorrenciasFoneFacil = saidaImprimirContrato.getOcorrenciasFoneFacil().size();
        for (int i = 0; i < numeroOcorrenciasFoneFacil; i++) {
            ImprimirContratoOcorrencias6SaidaDTO ocorrencia = saidaImprimirContrato.getOcorrenciasFoneFacil().get(i);

            // produto nao vazio
            if (ocorrencia.getDsProdutoSalarioFoneFacil() != null
                && !"".equals(ocorrencia.getDsProdutoSalarioFoneFacil())) {
                ProdutoServico item = new ProdutoServico();
                item.setProduto(ocorrencia.getDsProdutoSalarioFoneFacil());
                item.setServico(ocorrencia.getDsServicoSalarioFoneFacil());
                foneFacil.add(item);
            }
        }

        numeroOcorrenciasInternet = saidaImprimirContrato.getOcorrenciasInternet().size();
        for (int i = 0; i < numeroOcorrenciasInternet; i++) {
            ImprimirContratoOcorrencias7SaidaDTO ocorrencia = saidaImprimirContrato.getOcorrenciasInternet().get(i);

            // produto nao vazio
            if (ocorrencia.getDsProdutoSalarioInternetBanking() != null
                && !"".equals(ocorrencia.getDsProdutoSalarioInternetBanking())) {
                ProdutoServico item = new ProdutoServico();
                item.setProduto(ocorrencia.getDsProdutoSalarioInternetBanking());
                item.setServico(ocorrencia.getDsServicoSalarioInternetBanking());
                internet.add(item);
            }
        }

        AnexoSalarios anexoSalarios = new AnexoSalarios();
        anexoSalarios.setModalidadeCreditoConta(modalidadeCreditoConta);
        anexoSalarios.setModalidadeTed(modalidadeTed);
        anexoSalarios.setModalidadeDoc(modalidadeDoc);
        anexoSalarios.setModalidadeOrdemPagamento(modalidadeOrdemPagamento);
        anexoSalarios.setIsentoTarifas(isentoTarifas);
        anexoSalarios.setGuicheCaixa(caixa);
        anexoSalarios.setAutoAtendimento(autoAtendimento);
        anexoSalarios.setFoneFacil(foneFacil);
        anexoSalarios.setInternetBanking(internet);
        anexoSalarios.setCdTransferenciaContaSalarial(saidaImprimirContrato.getCdTransferenciaContaSalarial()
            .toString());

        anexoSalarios.setTipoAplicacao(saidaImprimirContrato.getDsFloatingServicoAplicacaoSalario());

        Integer cdFloatServicoAplicacao = saidaImprimirContrato.getCdFloatingServicoAplicacaoSalario();
        // verifica codigo float
        if (cdFloatServicoAplicacao != null && cdFloatServicoAplicacao == 1) {
            anexoSalarios.setAplicacaoFloatingSim("X");
        } else if (cdFloatServicoAplicacao == 2) {
            anexoSalarios.setAplicacaoFloatingNao("X");
        }
        return anexoSalarios;
    }

    /**
     * Preencher fornecedores.
     * 
     * @param saidaImprimirContrato
     *            the saida imprimir contrato
     * @return the anexo fornecedores
     * @throws
     * @see
     */
    private AnexoFornecedores preencherFornecedores(ImprimirContratoSaidaDTO saidaImprimirContrato) {
        ModalidadeCondicao modalidadeCreditoConta = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeTed = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeDoc = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeBoletosBradesco = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeBoletosOutros = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeOrdemPagamento = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeRastreamentoBoletos = new ModalidadeCondicao();
        int numeroOcorrenciasFornecedor = saidaImprimirContrato.getOcorrenciasFornecedor().size();

        for (int i = 0; i < numeroOcorrenciasFornecedor; i++) {
            ImprimirContratoOcorrenciasSaidaDTO ocorrencia = saidaImprimirContrato.getOcorrenciasFornecedor().get(i);
            ModalidadeCondicao modalidade = new ModalidadeCondicao();

            if ("S".equalsIgnoreCase(ocorrencia.getCdIndicadorFornecedorContrato())) {
                modalidade.setFlagModalidade("X");

                if (ocorrencia.getCdMomentoProcessamentoFornecedor() == 1) {
                    modalidade.setFlagProcessamentoCiclico("X");
                } else if (ocorrencia.getCdMomentoProcessamentoFornecedor() == 2) {
                    modalidade.setFlagProcessamentoDiario("X");
                }

                if (ocorrencia.getCdPagamentoUtilFornecedor() == 1) {
                    modalidade.setFlagPagDiaNaoUtilAntecipa("X");
                } else if (ocorrencia.getCdPagamentoUtilFornecedor() == 2) {
                    modalidade.setFlagPagDiaNaoUtilPosterga("X");
                }

                if (ocorrencia.getCdIndicadorFeriadoFornecedor() == 2) {
                    modalidade.setFlagPagFeriadoAcata("X");
                } else if (ocorrencia.getCdIndicadorFeriadoFornecedor() == 3) {
                    modalidade.setFlagPagFeriadoAcataReagendamento("X");
                } else if (ocorrencia.getCdIndicadorFeriadoFornecedor() == 4) {
                    modalidade.setFlagPagFeriadoAntecipa("X");
                } else if (ocorrencia.getCdIndicadorFeriadoFornecedor() == 5) {
                    modalidade.setFlagPagFeriadoPosterga("X");

                }
                modalidade.setQtdeDiasFloating(String.format("%02d", ocorrencia.getNrQuantidadeDiasFloatFornecedor()));
            }

            // verifica modalidade
            if (ocorrencia.getCdIndicadorModalidadeFornecedor() == 1) {
                modalidadeCreditoConta = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeFornecedor() == 2) {
                modalidadeBoletosBradesco = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeFornecedor() == 3) {
                modalidadeBoletosOutros = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeFornecedor() == 4) {
                modalidadeDoc = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeFornecedor() == 5) {
                modalidadeTed = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeFornecedor() == 6) {
                modalidadeOrdemPagamento = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeFornecedor() == 7) {
                modalidadeRastreamentoBoletos = modalidade;
            }
        }

        AnexoFornecedores anexoFornecedores = new AnexoFornecedores();
        anexoFornecedores.setModalidadeCreditoConta(modalidadeCreditoConta);
        anexoFornecedores.setModalidadeTed(modalidadeTed);
        anexoFornecedores.setModalidadeDoc(modalidadeDoc);
        anexoFornecedores.setModalidadeBoletosBradesco(modalidadeBoletosBradesco);
        anexoFornecedores.setModalidadeBoletosOutros(modalidadeBoletosOutros);
        anexoFornecedores.setModalidadeOrdemPagamento(modalidadeOrdemPagamento);
        anexoFornecedores.setModalidadeRastreamentoBoletos(modalidadeRastreamentoBoletos);
        anexoFornecedores.setTipoAplicacao(saidaImprimirContrato.getDsFloatServicoAplicacaoFornecedor());
        anexoFornecedores.setHrLimiteSuperiorBoleto(saidaImprimirContrato.getHrLimiteSuperiorBoleto());
        anexoFornecedores.sethrLimiteInferiorBoleto(saidaImprimirContrato.gethrLimiteInferiorBoleto());
        anexoFornecedores.setHrLimitePagamentoSuperior(saidaImprimirContrato.getHrLimitePagamentoSuperior());
        anexoFornecedores.setHrLimitePagamentoInferior(saidaImprimirContrato.getHrLimitePagamentoInferior());
        anexoFornecedores.setNrMontanteBoleto(saidaImprimirContrato.getNrMontanteBoleto().toString());

        Integer cdFloatServicoAplicacao = saidaImprimirContrato.getCdFloatServicoAplicacaoFornecedor();
        // verifica codigo float
        if (cdFloatServicoAplicacao != null && cdFloatServicoAplicacao == 1) {
            anexoFornecedores.setAplicacaoFloatingSim("X");
        } else if (cdFloatServicoAplicacao == 2) {
            anexoFornecedores.setAplicacaoFloatingNao("X");
        }

        return anexoFornecedores;
    }

    /**
     * Preencher tributo contas.
     * 
     * @param saidaImprimirContrato
     *            the saida imprimir contrato
     * @return the anexo tributos contas
     * @throws
     * @see
     */
    private AnexoTributosContas preencherTributoContas(ImprimirContratoSaidaDTO saidaImprimirContrato) {
        ModalidadeCondicao modalidadeDarf = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeGare = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeGps = new ModalidadeCondicao();
        ModalidadeCondicao modalidadeCodigoBarra = new ModalidadeCondicao();
        int numeroOcorrenciasTributo = saidaImprimirContrato.getOcorrenciasTributo().size();

        for (int i = 0; i < numeroOcorrenciasTributo; i++) {
            ImprimirContratoOcorrencias1SaidaDTO ocorrencia = saidaImprimirContrato.getOcorrenciasTributo().get(i);
            ModalidadeCondicao modalidade = new ModalidadeCondicao();

            if ("S".equalsIgnoreCase(ocorrencia.getCdIndicadorTributoContrato())) {
                modalidade.setFlagModalidade("X");

                if (ocorrencia.getCdMomentoProcessamentoTributos() == 1) {
                    modalidade.setFlagProcessamentoCiclico("X");
                } else if (ocorrencia.getCdMomentoProcessamentoTributos() == 2) {
                    modalidade.setFlagProcessamentoDiario("X");
                }

                if (ocorrencia.getCdIndicadorFeriadoTributos() == 1) {
                    modalidade.setFlagPagFeriadoRejeita("X");
                } else if (ocorrencia.getCdIndicadorFeriadoTributos() == 2) {
                    modalidade.setFlagPagFeriadoAcata("X");

                }
            }

            // verifica modalidade
            if (ocorrencia.getCdIndicadorModalidadeTributos() == 1) {
                modalidadeGare = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeTributos() == 2) {
                modalidadeDarf = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeTributos() == 3) {
                modalidadeGps = modalidade;
            } else if (ocorrencia.getCdIndicadorModalidadeTributos() == 4) {
                modalidadeCodigoBarra = modalidade;
            }
        }

        AnexoTributosContas anexoTributosContas = new AnexoTributosContas();
        anexoTributosContas.setModalidadeDarf(modalidadeDarf);
        anexoTributosContas.setModalidadeGare(modalidadeGare);
        anexoTributosContas.setModalidadeGps(modalidadeGps);
        anexoTributosContas.setModalidadeCodigoBarra(modalidadeCodigoBarra);
        anexoTributosContas.setHrLimitePagamentoRemessa(saidaImprimirContrato.getHrLimitePagamentoRemessa());
        return anexoTributosContas;
    }

    /**
     * Data assinatura formatada.
     * 
     * @param cidade
     *            the cidade
     * @param uf
     *            the uf
     * @return the string
     * @throws
     * @see
     */
    private String dataAssinaturaFormatada(String cidade, String uf) {
        Date dataCorrente = new Date();
        String dia = formataData(dataCorrente, "dd");
        String mes = formataData(dataCorrente, "MMMMM");
        String ano = formataData(dataCorrente, "yyyy");

        return String.format("%s (%s), %s de %s de %s.", cidade, uf, dia, mes, ano);
    }

    /**
     * Endereco identificacao formatado.
     * 
     * @param dsLogradouroPagador
     *            the ds logradouro pagador
     * @param dsNumeroLogradouroPagador
     *            the ds numero logradouro pagador
     * @param dsComplementoLogradouroPagador
     *            the ds complemento logradouro pagador
     * @param dsBairroClientePagador
     *            the ds bairro cliente pagador
     * @param dsMunicipioClientePagador
     *            the ds municipio cliente pagador
     * @param cdSiglaUfPagador
     *            the cd sigla uf pagador
     * @param cdCepPagador
     *            the cd cep pagador
     * @param cdCepComplementoPagador
     *            the cd cep complemento pagador
     * @return the string
     * @throws
     * @see
     */
    private String enderecoIdentificacaoFormatado(String dsLogradouroPagador, String dsNumeroLogradouroPagador,
        String dsComplementoLogradouroPagador, String dsBairroClientePagador, String dsMunicipioClientePagador,
        String cdSiglaUfPagador, Integer cdCepPagador, Integer cdCepComplementoPagador) {

        StringBuilder endereco = new StringBuilder();
        endereco.append(dsLogradouroPagador);
        endereco.append(" ");
        endereco.append(dsNumeroLogradouroPagador);
        // complemento vazio
        if (dsComplementoLogradouroPagador != null && !"".equals(dsComplementoLogradouroPagador)) {
            endereco.append(" ");
            endereco.append(dsComplementoLogradouroPagador);
        }
        endereco.append(" - ");
        endereco.append(dsBairroClientePagador);
        endereco.append(" - ");
        endereco.append(dsMunicipioClientePagador);
        endereco.append(" - ");
        endereco.append(cdSiglaUfPagador);
        endereco.append(" - CEP: ");
        endereco.append(String.format("%05d", cdCepPagador));
        endereco.append("-");
        endereco.append(String.format("%03d", cdCepComplementoPagador));
        return endereco.toString();
    }

}