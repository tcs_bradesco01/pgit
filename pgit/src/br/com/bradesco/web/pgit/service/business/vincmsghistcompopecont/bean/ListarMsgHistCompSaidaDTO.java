/*
 * Nome: br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean;

/**
 * Nome: ListarMsgHistCompSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMsgHistCompSaidaDTO {
	
	/** Atributo cdMensagemLinhaExtrato. */
	private int cdMensagemLinhaExtrato;
	
	/** Atributo cdIndicadorRestricaoContrato. */
	private int cdIndicadorRestricaoContrato;
	
	/** Atributo cdTipoMensagemExtrato. */
	private int cdTipoMensagemExtrato;
	
	/** Atributo cdProdutoServicoOperacao. */
	private int cdProdutoServicoOperacao;
	
	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private int cdProdutoOperacaoRelacionado;
	
	/** Atributo dsProdutoOperacaoRelacionado. */
	private String dsProdutoOperacaoRelacionado;
	
	/** Atributo cdRelacionadoProduto. */
	private int cdRelacionadoProduto;
	
	/** Atributo cdMensagem. */
	private String cdMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo restricaoDesc. */
	private String restricaoDesc;
	
	/**
	 * Get: cdIndicadorRestricaoContrato.
	 *
	 * @return cdIndicadorRestricaoContrato
	 */
	public int getCdIndicadorRestricaoContrato() {
		return cdIndicadorRestricaoContrato;
	}
	
	/**
	 * Set: cdIndicadorRestricaoContrato.
	 *
	 * @param cdIndicadorRestricaoContrato the cd indicador restricao contrato
	 */
	public void setCdIndicadorRestricaoContrato(int cdIndicadorRestricaoContrato) {
		this.cdIndicadorRestricaoContrato = cdIndicadorRestricaoContrato;
	}
	
	/**
	 * Get: cdMensagemLinhaExtrato.
	 *
	 * @return cdMensagemLinhaExtrato
	 */
	public int getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}
	
	/**
	 * Set: cdMensagemLinhaExtrato.
	 *
	 * @param cdMensagemLinhaExtrato the cd mensagem linha extrato
	 */
	public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public int getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public int getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRelacionadoProduto.
	 *
	 * @return cdRelacionadoProduto
	 */
	public int getCdRelacionadoProduto() {
		return cdRelacionadoProduto;
	}
	
	/**
	 * Set: cdRelacionadoProduto.
	 *
	 * @param cdRelacionadoProduto the cd relacionado produto
	 */
	public void setCdRelacionadoProduto(int cdRelacionadoProduto) {
		this.cdRelacionadoProduto = cdRelacionadoProduto;
	}
	
	/**
	 * Get: cdTipoMensagemExtrato.
	 *
	 * @return cdTipoMensagemExtrato
	 */
	public int getCdTipoMensagemExtrato() {
		return cdTipoMensagemExtrato;
	}
	
	/**
	 * Set: cdTipoMensagemExtrato.
	 *
	 * @param cdTipoMensagemExtrato the cd tipo mensagem extrato
	 */
	public void setCdTipoMensagemExtrato(int cdTipoMensagemExtrato) {
		this.cdTipoMensagemExtrato = cdTipoMensagemExtrato;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdMensagem.
	 *
	 * @return cdMensagem
	 */
	public String getCdMensagem() {
		return cdMensagem;
	}
	
	/**
	 * Set: cdMensagem.
	 *
	 * @param cdMensagem the cd mensagem
	 */
	public void setCdMensagem(String cdMensagem) {
		this.cdMensagem = cdMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: restricaoDesc.
	 *
	 * @return restricaoDesc
	 */
	public String getRestricaoDesc() {
		return restricaoDesc;
	}
	
	/**
	 * Set: restricaoDesc.
	 *
	 * @param restricaoDesc the restricao desc
	 */
	public void setRestricaoDesc(String restricaoDesc) {
		this.restricaoDesc = restricaoDesc;
	}
	
}
