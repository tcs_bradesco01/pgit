/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarListaContratosManutencaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarListaContratosManutencaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridicaNegocio
     */
    private long _cdPessoaJuridicaNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaNegocio
     */
    private boolean _has_cdPessoaJuridicaNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;

    /**
     * Field _dtManutencaoInicial
     */
    private java.lang.String _dtManutencaoInicial;

    /**
     * Field _dtManutencaoFinal
     */
    private java.lang.String _dtManutencaoFinal;

    /**
     * Field _cdTipoManutencao
     */
    private int _cdTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoManutencao
     */
    private boolean _has_cdTipoManutencao;

    /**
     * Field _cdAcaoManutencao
     */
    private int _cdAcaoManutencao = 0;

    /**
     * keeps track of state for field: _cdAcaoManutencao
     */
    private boolean _has_cdAcaoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarListaContratosManutencaoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.request.ConsultarListaContratosManutencaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoManutencao
     * 
     */
    public void deleteCdAcaoManutencao()
    {
        this._has_cdAcaoManutencao= false;
    } //-- void deleteCdAcaoManutencao() 

    /**
     * Method deleteCdPessoaJuridicaNegocio
     * 
     */
    public void deleteCdPessoaJuridicaNegocio()
    {
        this._has_cdPessoaJuridicaNegocio= false;
    } //-- void deleteCdPessoaJuridicaNegocio() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoManutencao
     * 
     */
    public void deleteCdTipoManutencao()
    {
        this._has_cdTipoManutencao= false;
    } //-- void deleteCdTipoManutencao() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'cdAcaoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdAcaoManutencao'.
     */
    public int getCdAcaoManutencao()
    {
        return this._cdAcaoManutencao;
    } //-- int getCdAcaoManutencao() 

    /**
     * Returns the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaNegocio'.
     */
    public long getCdPessoaJuridicaNegocio()
    {
        return this._cdPessoaJuridicaNegocio;
    } //-- long getCdPessoaJuridicaNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencao'.
     */
    public int getCdTipoManutencao()
    {
        return this._cdTipoManutencao;
    } //-- int getCdTipoManutencao() 

    /**
     * Returns the value of field 'dtManutencaoFinal'.
     * 
     * @return String
     * @return the value of field 'dtManutencaoFinal'.
     */
    public java.lang.String getDtManutencaoFinal()
    {
        return this._dtManutencaoFinal;
    } //-- java.lang.String getDtManutencaoFinal() 

    /**
     * Returns the value of field 'dtManutencaoInicial'.
     * 
     * @return String
     * @return the value of field 'dtManutencaoInicial'.
     */
    public java.lang.String getDtManutencaoInicial()
    {
        return this._dtManutencaoInicial;
    } //-- java.lang.String getDtManutencaoInicial() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Method hasCdAcaoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoManutencao()
    {
        return this._has_cdAcaoManutencao;
    } //-- boolean hasCdAcaoManutencao() 

    /**
     * Method hasCdPessoaJuridicaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaNegocio()
    {
        return this._has_cdPessoaJuridicaNegocio;
    } //-- boolean hasCdPessoaJuridicaNegocio() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencao()
    {
        return this._has_cdTipoManutencao;
    } //-- boolean hasCdTipoManutencao() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoManutencao'.
     * 
     * @param cdAcaoManutencao the value of field 'cdAcaoManutencao'
     */
    public void setCdAcaoManutencao(int cdAcaoManutencao)
    {
        this._cdAcaoManutencao = cdAcaoManutencao;
        this._has_cdAcaoManutencao = true;
    } //-- void setCdAcaoManutencao(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaNegocio'.
     * 
     * @param cdPessoaJuridicaNegocio the value of field
     * 'cdPessoaJuridicaNegocio'.
     */
    public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio)
    {
        this._cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
        this._has_cdPessoaJuridicaNegocio = true;
    } //-- void setCdPessoaJuridicaNegocio(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoManutencao'.
     * 
     * @param cdTipoManutencao the value of field 'cdTipoManutencao'
     */
    public void setCdTipoManutencao(int cdTipoManutencao)
    {
        this._cdTipoManutencao = cdTipoManutencao;
        this._has_cdTipoManutencao = true;
    } //-- void setCdTipoManutencao(int) 

    /**
     * Sets the value of field 'dtManutencaoFinal'.
     * 
     * @param dtManutencaoFinal the value of field
     * 'dtManutencaoFinal'.
     */
    public void setDtManutencaoFinal(java.lang.String dtManutencaoFinal)
    {
        this._dtManutencaoFinal = dtManutencaoFinal;
    } //-- void setDtManutencaoFinal(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencaoInicial'.
     * 
     * @param dtManutencaoInicial the value of field
     * 'dtManutencaoInicial'.
     */
    public void setDtManutencaoInicial(java.lang.String dtManutencaoInicial)
    {
        this._dtManutencaoInicial = dtManutencaoInicial;
    } //-- void setDtManutencaoInicial(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarListaContratosManutencaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.request.ConsultarListaContratosManutencaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.request.ConsultarListaContratosManutencaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.request.ConsultarListaContratosManutencaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.request.ConsultarListaContratosManutencaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
