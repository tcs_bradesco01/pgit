/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolBasePagtoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolBasePagtoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _nrSolicitacaoPagamento
     */
    private int _nrSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _nrSolicitacaoPagamento
     */
    private boolean _has_nrSolicitacaoPagamento;

    /**
     * Field _hrSolicitacaoPagamento
     */
    private java.lang.String _hrSolicitacaoPagamento;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;

    /**
     * Field _resultadoProcessamento
     */
    private java.lang.String _resultadoProcessamento;

    /**
     * Field _hrAtendimentoSolicitacao
     */
    private java.lang.String _hrAtendimentoSolicitacao;

    /**
     * Field _qtdeRegistrosSolicitacao
     */
    private long _qtdeRegistrosSolicitacao = 0;

    /**
     * keeps track of state for field: _qtdeRegistrosSolicitacao
     */
    private boolean _has_qtdeRegistrosSolicitacao;

    /**
     * Field _dtInicioPagamento
     */
    private java.lang.String _dtInicioPagamento;

    /**
     * Field _dtFimPagamento
     */
    private java.lang.String _dtFimPagamento;

    /**
     * Field _cdServico
     */
    private java.lang.String _cdServico;

    /**
     * Field _cdServicoRelacionado
     */
    private java.lang.String _cdServicoRelacionado;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _digitoAgenciaDebito
     */
    private int _digitoAgenciaDebito = 0;

    /**
     * keeps track of state for field: _digitoAgenciaDebito
     */
    private boolean _has_digitoAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _digitoContaDebito
     */
    private java.lang.String _digitoContaDebito;

    /**
     * Field _indicadorPagos
     */
    private java.lang.String _indicadorPagos;

    /**
     * Field _indicadorNaoPagos
     */
    private java.lang.String _indicadorNaoPagos;

    /**
     * Field _indicadorAgendados
     */
    private java.lang.String _indicadorAgendados;

    /**
     * Field _cdOrigem
     */
    private int _cdOrigem = 0;

    /**
     * keeps track of state for field: _cdOrigem
     */
    private boolean _has_cdOrigem;

    /**
     * Field _cdDestino
     */
    private int _cdDestino = 0;

    /**
     * keeps track of state for field: _cdDestino
     */
    private boolean _has_cdDestino;

    /**
     * Field _dsFavorecido
     */
    private java.lang.String _dsFavorecido;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _cdTipoInscricaoFavorecido
     */
    private java.lang.String _cdTipoInscricaoFavorecido;

    /**
     * Field _nmMinemonicoFavorecido
     */
    private java.lang.String _nmMinemonicoFavorecido;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _numPercentualDescTarifa
     */
    private java.math.BigDecimal _numPercentualDescTarifa = new java.math.BigDecimal("0");

    /**
     * Field _vlrTarifaAtual
     */
    private java.math.BigDecimal _vlrTarifaAtual = new java.math.BigDecimal("0");

    /**
     * Field _cdCananInclusao
     */
    private int _cdCananInclusao = 0;

    /**
     * keeps track of state for field: _cdCananInclusao
     */
    private boolean _has_cdCananInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolBasePagtoResponse() 
     {
        super();
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
        setNumPercentualDescTarifa(new java.math.BigDecimal("0"));
        setVlrTarifaAtual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.response.DetalharSolBasePagtoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdCananInclusao
     * 
     */
    public void deleteCdCananInclusao()
    {
        this._has_cdCananInclusao= false;
    } //-- void deleteCdCananInclusao() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdDestino
     * 
     */
    public void deleteCdDestino()
    {
        this._has_cdDestino= false;
    } //-- void deleteCdDestino() 

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdOrigem
     * 
     */
    public void deleteCdOrigem()
    {
        this._has_cdOrigem= false;
    } //-- void deleteCdOrigem() 

    /**
     * Method deleteDigitoAgenciaDebito
     * 
     */
    public void deleteDigitoAgenciaDebito()
    {
        this._has_digitoAgenciaDebito= false;
    } //-- void deleteDigitoAgenciaDebito() 

    /**
     * Method deleteNrSolicitacaoPagamento
     * 
     */
    public void deleteNrSolicitacaoPagamento()
    {
        this._has_nrSolicitacaoPagamento= false;
    } //-- void deleteNrSolicitacaoPagamento() 

    /**
     * Method deleteQtdeRegistrosSolicitacao
     * 
     */
    public void deleteQtdeRegistrosSolicitacao()
    {
        this._has_qtdeRegistrosSolicitacao= false;
    } //-- void deleteQtdeRegistrosSolicitacao() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdCananInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCananInclusao'.
     */
    public int getCdCananInclusao()
    {
        return this._cdCananInclusao;
    } //-- int getCdCananInclusao() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdDestino'.
     * 
     * @return int
     * @return the value of field 'cdDestino'.
     */
    public int getCdDestino()
    {
        return this._cdDestino;
    } //-- int getCdDestino() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdOrigem'.
     * 
     * @return int
     * @return the value of field 'cdOrigem'.
     */
    public int getCdOrigem()
    {
        return this._cdOrigem;
    } //-- int getCdOrigem() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return String
     * @return the value of field 'cdServico'.
     */
    public java.lang.String getCdServico()
    {
        return this._cdServico;
    } //-- java.lang.String getCdServico() 

    /**
     * Returns the value of field 'cdServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'cdServicoRelacionado'.
     */
    public java.lang.String getCdServicoRelacionado()
    {
        return this._cdServicoRelacionado;
    } //-- java.lang.String getCdServicoRelacionado() 

    /**
     * Returns the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdTipoInscricaoFavorecido'.
     */
    public java.lang.String getCdTipoInscricaoFavorecido()
    {
        return this._cdTipoInscricaoFavorecido;
    } //-- java.lang.String getCdTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'digitoAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'digitoAgenciaDebito'.
     */
    public int getDigitoAgenciaDebito()
    {
        return this._digitoAgenciaDebito;
    } //-- int getDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'digitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'digitoContaDebito'.
     */
    public java.lang.String getDigitoContaDebito()
    {
        return this._digitoContaDebito;
    } //-- java.lang.String getDigitoContaDebito() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsFavorecido'.
     */
    public java.lang.String getDsFavorecido()
    {
        return this._dsFavorecido;
    } //-- java.lang.String getDsFavorecido() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dtFimPagamento'.
     * 
     * @return String
     * @return the value of field 'dtFimPagamento'.
     */
    public java.lang.String getDtFimPagamento()
    {
        return this._dtFimPagamento;
    } //-- java.lang.String getDtFimPagamento() 

    /**
     * Returns the value of field 'dtInicioPagamento'.
     * 
     * @return String
     * @return the value of field 'dtInicioPagamento'.
     */
    public java.lang.String getDtInicioPagamento()
    {
        return this._dtInicioPagamento;
    } //-- java.lang.String getDtInicioPagamento() 

    /**
     * Returns the value of field 'hrAtendimentoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'hrAtendimentoSolicitacao'.
     */
    public java.lang.String getHrAtendimentoSolicitacao()
    {
        return this._hrAtendimentoSolicitacao;
    } //-- java.lang.String getHrAtendimentoSolicitacao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'hrSolicitacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacaoPagamento'.
     */
    public java.lang.String getHrSolicitacaoPagamento()
    {
        return this._hrSolicitacaoPagamento;
    } //-- java.lang.String getHrSolicitacaoPagamento() 

    /**
     * Returns the value of field 'indicadorAgendados'.
     * 
     * @return String
     * @return the value of field 'indicadorAgendados'.
     */
    public java.lang.String getIndicadorAgendados()
    {
        return this._indicadorAgendados;
    } //-- java.lang.String getIndicadorAgendados() 

    /**
     * Returns the value of field 'indicadorNaoPagos'.
     * 
     * @return String
     * @return the value of field 'indicadorNaoPagos'.
     */
    public java.lang.String getIndicadorNaoPagos()
    {
        return this._indicadorNaoPagos;
    } //-- java.lang.String getIndicadorNaoPagos() 

    /**
     * Returns the value of field 'indicadorPagos'.
     * 
     * @return String
     * @return the value of field 'indicadorPagos'.
     */
    public java.lang.String getIndicadorPagos()
    {
        return this._indicadorPagos;
    } //-- java.lang.String getIndicadorPagos() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmMinemonicoFavorecido'.
     * 
     * @return String
     * @return the value of field 'nmMinemonicoFavorecido'.
     */
    public java.lang.String getNmMinemonicoFavorecido()
    {
        return this._nmMinemonicoFavorecido;
    } //-- java.lang.String getNmMinemonicoFavorecido() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamento'.
     */
    public int getNrSolicitacaoPagamento()
    {
        return this._nrSolicitacaoPagamento;
    } //-- int getNrSolicitacaoPagamento() 

    /**
     * Returns the value of field 'numPercentualDescTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'numPercentualDescTarifa'.
     */
    public java.math.BigDecimal getNumPercentualDescTarifa()
    {
        return this._numPercentualDescTarifa;
    } //-- java.math.BigDecimal getNumPercentualDescTarifa() 

    /**
     * Returns the value of field 'qtdeRegistrosSolicitacao'.
     * 
     * @return long
     * @return the value of field 'qtdeRegistrosSolicitacao'.
     */
    public long getQtdeRegistrosSolicitacao()
    {
        return this._qtdeRegistrosSolicitacao;
    } //-- long getQtdeRegistrosSolicitacao() 

    /**
     * Returns the value of field 'resultadoProcessamento'.
     * 
     * @return String
     * @return the value of field 'resultadoProcessamento'.
     */
    public java.lang.String getResultadoProcessamento()
    {
        return this._resultadoProcessamento;
    } //-- java.lang.String getResultadoProcessamento() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Returns the value of field 'vlrTarifaAtual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaAtual'.
     */
    public java.math.BigDecimal getVlrTarifaAtual()
    {
        return this._vlrTarifaAtual;
    } //-- java.math.BigDecimal getVlrTarifaAtual() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdCananInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCananInclusao()
    {
        return this._has_cdCananInclusao;
    } //-- boolean hasCdCananInclusao() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestino()
    {
        return this._has_cdDestino;
    } //-- boolean hasCdDestino() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdOrigem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrigem()
    {
        return this._has_cdOrigem;
    } //-- boolean hasCdOrigem() 

    /**
     * Method hasDigitoAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDigitoAgenciaDebito()
    {
        return this._has_digitoAgenciaDebito;
    } //-- boolean hasDigitoAgenciaDebito() 

    /**
     * Method hasNrSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamento()
    {
        return this._has_nrSolicitacaoPagamento;
    } //-- boolean hasNrSolicitacaoPagamento() 

    /**
     * Method hasQtdeRegistrosSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeRegistrosSolicitacao()
    {
        return this._has_qtdeRegistrosSolicitacao;
    } //-- boolean hasQtdeRegistrosSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdCananInclusao'.
     * 
     * @param cdCananInclusao the value of field 'cdCananInclusao'.
     */
    public void setCdCananInclusao(int cdCananInclusao)
    {
        this._cdCananInclusao = cdCananInclusao;
        this._has_cdCananInclusao = true;
    } //-- void setCdCananInclusao(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdDestino'.
     * 
     * @param cdDestino the value of field 'cdDestino'.
     */
    public void setCdDestino(int cdDestino)
    {
        this._cdDestino = cdDestino;
        this._has_cdDestino = true;
    } //-- void setCdDestino(int) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdOrigem'.
     * 
     * @param cdOrigem the value of field 'cdOrigem'.
     */
    public void setCdOrigem(int cdOrigem)
    {
        this._cdOrigem = cdOrigem;
        this._has_cdOrigem = true;
    } //-- void setCdOrigem(int) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(java.lang.String cdServico)
    {
        this._cdServico = cdServico;
    } //-- void setCdServico(java.lang.String) 

    /**
     * Sets the value of field 'cdServicoRelacionado'.
     * 
     * @param cdServicoRelacionado the value of field
     * 'cdServicoRelacionado'.
     */
    public void setCdServicoRelacionado(java.lang.String cdServicoRelacionado)
    {
        this._cdServicoRelacionado = cdServicoRelacionado;
    } //-- void setCdServicoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @param cdTipoInscricaoFavorecido the value of field
     * 'cdTipoInscricaoFavorecido'.
     */
    public void setCdTipoInscricaoFavorecido(java.lang.String cdTipoInscricaoFavorecido)
    {
        this._cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
    } //-- void setCdTipoInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'digitoAgenciaDebito'.
     * 
     * @param digitoAgenciaDebito the value of field
     * 'digitoAgenciaDebito'.
     */
    public void setDigitoAgenciaDebito(int digitoAgenciaDebito)
    {
        this._digitoAgenciaDebito = digitoAgenciaDebito;
        this._has_digitoAgenciaDebito = true;
    } //-- void setDigitoAgenciaDebito(int) 

    /**
     * Sets the value of field 'digitoContaDebito'.
     * 
     * @param digitoContaDebito the value of field
     * 'digitoContaDebito'.
     */
    public void setDigitoContaDebito(java.lang.String digitoContaDebito)
    {
        this._digitoContaDebito = digitoContaDebito;
    } //-- void setDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsFavorecido'.
     * 
     * @param dsFavorecido the value of field 'dsFavorecido'.
     */
    public void setDsFavorecido(java.lang.String dsFavorecido)
    {
        this._dsFavorecido = dsFavorecido;
    } //-- void setDsFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtFimPagamento'.
     * 
     * @param dtFimPagamento the value of field 'dtFimPagamento'.
     */
    public void setDtFimPagamento(java.lang.String dtFimPagamento)
    {
        this._dtFimPagamento = dtFimPagamento;
    } //-- void setDtFimPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioPagamento'.
     * 
     * @param dtInicioPagamento the value of field
     * 'dtInicioPagamento'.
     */
    public void setDtInicioPagamento(java.lang.String dtInicioPagamento)
    {
        this._dtInicioPagamento = dtInicioPagamento;
    } //-- void setDtInicioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrAtendimentoSolicitacao'.
     * 
     * @param hrAtendimentoSolicitacao the value of field
     * 'hrAtendimentoSolicitacao'.
     */
    public void setHrAtendimentoSolicitacao(java.lang.String hrAtendimentoSolicitacao)
    {
        this._hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
    } //-- void setHrAtendimentoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacaoPagamento'.
     * 
     * @param hrSolicitacaoPagamento the value of field
     * 'hrSolicitacaoPagamento'.
     */
    public void setHrSolicitacaoPagamento(java.lang.String hrSolicitacaoPagamento)
    {
        this._hrSolicitacaoPagamento = hrSolicitacaoPagamento;
    } //-- void setHrSolicitacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'indicadorAgendados'.
     * 
     * @param indicadorAgendados the value of field
     * 'indicadorAgendados'.
     */
    public void setIndicadorAgendados(java.lang.String indicadorAgendados)
    {
        this._indicadorAgendados = indicadorAgendados;
    } //-- void setIndicadorAgendados(java.lang.String) 

    /**
     * Sets the value of field 'indicadorNaoPagos'.
     * 
     * @param indicadorNaoPagos the value of field
     * 'indicadorNaoPagos'.
     */
    public void setIndicadorNaoPagos(java.lang.String indicadorNaoPagos)
    {
        this._indicadorNaoPagos = indicadorNaoPagos;
    } //-- void setIndicadorNaoPagos(java.lang.String) 

    /**
     * Sets the value of field 'indicadorPagos'.
     * 
     * @param indicadorPagos the value of field 'indicadorPagos'.
     */
    public void setIndicadorPagos(java.lang.String indicadorPagos)
    {
        this._indicadorPagos = indicadorPagos;
    } //-- void setIndicadorPagos(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmMinemonicoFavorecido'.
     * 
     * @param nmMinemonicoFavorecido the value of field
     * 'nmMinemonicoFavorecido'.
     */
    public void setNmMinemonicoFavorecido(java.lang.String nmMinemonicoFavorecido)
    {
        this._nmMinemonicoFavorecido = nmMinemonicoFavorecido;
    } //-- void setNmMinemonicoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamento'.
     * 
     * @param nrSolicitacaoPagamento the value of field
     * 'nrSolicitacaoPagamento'.
     */
    public void setNrSolicitacaoPagamento(int nrSolicitacaoPagamento)
    {
        this._nrSolicitacaoPagamento = nrSolicitacaoPagamento;
        this._has_nrSolicitacaoPagamento = true;
    } //-- void setNrSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'numPercentualDescTarifa'.
     * 
     * @param numPercentualDescTarifa the value of field
     * 'numPercentualDescTarifa'.
     */
    public void setNumPercentualDescTarifa(java.math.BigDecimal numPercentualDescTarifa)
    {
        this._numPercentualDescTarifa = numPercentualDescTarifa;
    } //-- void setNumPercentualDescTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'qtdeRegistrosSolicitacao'.
     * 
     * @param qtdeRegistrosSolicitacao the value of field
     * 'qtdeRegistrosSolicitacao'.
     */
    public void setQtdeRegistrosSolicitacao(long qtdeRegistrosSolicitacao)
    {
        this._qtdeRegistrosSolicitacao = qtdeRegistrosSolicitacao;
        this._has_qtdeRegistrosSolicitacao = true;
    } //-- void setQtdeRegistrosSolicitacao(long) 

    /**
     * Sets the value of field 'resultadoProcessamento'.
     * 
     * @param resultadoProcessamento the value of field
     * 'resultadoProcessamento'.
     */
    public void setResultadoProcessamento(java.lang.String resultadoProcessamento)
    {
        this._resultadoProcessamento = resultadoProcessamento;
    } //-- void setResultadoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTarifaAtual'.
     * 
     * @param vlrTarifaAtual the value of field 'vlrTarifaAtual'.
     */
    public void setVlrTarifaAtual(java.math.BigDecimal vlrTarifaAtual)
    {
        this._vlrTarifaAtual = vlrTarifaAtual;
    } //-- void setVlrTarifaAtual(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolBasePagtoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.response.DetalharSolBasePagtoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.response.DetalharSolBasePagtoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.response.DetalharSolBasePagtoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.response.DetalharSolBasePagtoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
