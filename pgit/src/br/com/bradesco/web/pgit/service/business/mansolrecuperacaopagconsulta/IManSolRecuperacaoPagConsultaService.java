/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarSolRecuperacaoPagtosBackupEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarSolRecuperacaoPagtosBackupSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ExcluirSolRecPagtosVencidoNaoPagoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ExcluirSolRecPagtosVencidoNaoPagoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManSolRecuperacaoPagConsulta
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManSolRecuperacaoPagConsultaService {
	
	
	/**
	 * Consultar sol recuperacao pagtos backup.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar sol recuperacao pagtos backup saida dt o>
	 */
	List<ConsultarSolRecuperacaoPagtosBackupSaidaDTO> consultarSolRecuperacaoPagtosBackup(ConsultarSolRecuperacaoPagtosBackupEntradaDTO entradaDTO);
	
	/**
	 * Consultar pagtos solicitacao recuperacao.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the consultar pagtos solicitacao recuperacao saida dto
	 */
	ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO consultarPagtosSolicitacaoRecuperacao(ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO entradaDTO);
	
	/**
	 * Excluir sol rec pagtos vencido nao pago.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir sol rec pagtos vencido nao pago saida dto
	 */
	ExcluirSolRecPagtosVencidoNaoPagoSaidaDTO excluirSolRecPagtosVencidoNaoPago(ExcluirSolRecPagtosVencidoNaoPagoEntradaDTO entradaDTO);

}

