/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaridioma.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdIdioma
     */
    private int _cdIdioma = 0;

    /**
     * keeps track of state for field: _cdIdioma
     */
    private boolean _has_cdIdioma;

    /**
     * Field _dsIdioma
     */
    private java.lang.String _dsIdioma;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaridioma.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdioma
     * 
     */
    public void deleteCdIdioma()
    {
        this._has_cdIdioma= false;
    } //-- void deleteCdIdioma() 

    /**
     * Returns the value of field 'cdIdioma'.
     * 
     * @return int
     * @return the value of field 'cdIdioma'.
     */
    public int getCdIdioma()
    {
        return this._cdIdioma;
    } //-- int getCdIdioma() 

    /**
     * Returns the value of field 'dsIdioma'.
     * 
     * @return String
     * @return the value of field 'dsIdioma'.
     */
    public java.lang.String getDsIdioma()
    {
        return this._dsIdioma;
    } //-- java.lang.String getDsIdioma() 

    /**
     * Method hasCdIdioma
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdioma()
    {
        return this._has_cdIdioma;
    } //-- boolean hasCdIdioma() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIdioma'.
     * 
     * @param cdIdioma the value of field 'cdIdioma'.
     */
    public void setCdIdioma(int cdIdioma)
    {
        this._cdIdioma = cdIdioma;
        this._has_cdIdioma = true;
    } //-- void setCdIdioma(int) 

    /**
     * Sets the value of field 'dsIdioma'.
     * 
     * @param dsIdioma the value of field 'dsIdioma'.
     */
    public void setDsIdioma(java.lang.String dsIdioma)
    {
        this._dsIdioma = dsIdioma;
    } //-- void setDsIdioma(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaridioma.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaridioma.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaridioma.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaridioma.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
