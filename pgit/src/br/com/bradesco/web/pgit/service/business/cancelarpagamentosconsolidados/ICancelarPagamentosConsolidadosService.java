/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.CancelarPagtosParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.ConsultarPagtosConsParaCancelamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.ConsultarPagtosConsParaCancelamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.DetalharPagtosConsParaCancelamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean.DetalharPagtosConsParaCancelamentoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: CancelarPagamentosConsolidados
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ICancelarPagamentosConsolidadosService {

    /**
     * Consultar pagtos cons para cancelamento.
     *
     * @param entrada the entrada
     * @return the list< consultar pagtos cons para cancelamento saida dt o>
     */
    List<ConsultarPagtosConsParaCancelamentoSaidaDTO> consultarPagtosConsParaCancelamento (ConsultarPagtosConsParaCancelamentoEntradaDTO entrada);
    
    /**
     * Detalhar pagtos cons para cancelamento.
     *
     * @param entradaDTO the entrada dto
     * @return the detalhar pagtos cons para cancelamento saida dto
     */
    DetalharPagtosConsParaCancelamentoSaidaDTO detalharPagtosConsParaCancelamento(DetalharPagtosConsParaCancelamentoEntradaDTO entradaDTO);
    
    /**
     * Cancelar pagtos parcial.
     *
     * @param listaEntrada the lista entrada
     * @return the cancelar pagtos parcial saida dto
     */
    CancelarPagtosParcialSaidaDTO cancelarPagtosParcial(List<CancelarPagtosParcialEntradaDTO> listaEntrada);
    
    /**
     * Cancelar pagtos integral.
     *
     * @param cancelarPagtosIntegralEntradaDTO the cancelar pagtos integral entrada dto
     * @return the cancelar pagtos integral saida dto
     */
    CancelarPagtosIntegralSaidaDTO cancelarPagtosIntegral(CancelarPagtosIntegralEntradaDTO cancelarPagtosIntegralEntradaDTO);
    

}

