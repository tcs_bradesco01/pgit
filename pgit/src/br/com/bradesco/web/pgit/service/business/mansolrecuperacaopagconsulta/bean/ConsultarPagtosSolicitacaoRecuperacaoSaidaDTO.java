/*
 * Nome: br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO {
	    
    	/** Atributo cdPessoaJuridicaContrato. */
    	private Long cdPessoaJuridicaContrato;
	    
    	/** Atributo cdTipoContratoNegocio. */
    	private Integer cdTipoContratoNegocio;
	    
    	/** Atributo nrSequenciaContratoNegocio. */
    	private Long nrSequenciaContratoNegocio;
	    
    	/** Atributo cdBancoDebito. */
    	private Integer cdBancoDebito;
	    
    	/** Atributo dsBancoDebito. */
    	private String dsBancoDebito;
	    
    	/** Atributo cdAgenciaDebito. */
    	private Integer cdAgenciaDebito;
	    
    	/** Atributo dsAgenciaDebito. */
    	private String dsAgenciaDebito;
	    
    	/** Atributo cdContaDebito. */
    	private Long cdContaDebito;
	    
    	/** Atributo cdDigitoContaDebito. */
    	private String cdDigitoContaDebito;
	    
    	/** Atributo cdTipoContaDebito. */
    	private Integer cdTipoContaDebito;
	    
    	/** Atributo dsContaDebito. */
    	private String dsContaDebito;
	    
    	/** Atributo dtInicioPeriodoPesquisa. */
    	private String dtInicioPeriodoPesquisa;
	    
    	/** Atributo dtFimPeriodoPesquisa. */
    	private String dtFimPeriodoPesquisa;
	    
    	/** Atributo cdProduto. */
    	private Integer cdProduto;
	    
    	/** Atributo cdProdutoRelacionado. */
    	private Integer cdProdutoRelacionado;
	    
    	/** Atributo nrSequenciaArquivoRemessa. */
    	private Long nrSequenciaArquivoRemessa;
	    
    	/** Atributo cdPessoa. */
    	private Long cdPessoa;
	    
    	/** Atributo dsPessoas. */
    	private String dsPessoas;
	    
    	/** Atributo cdControlePagamentoInicial. */
    	private String cdControlePagamentoInicial;
	    
    	/** Atributo cdControlePagamentoFinal. */
    	private String cdControlePagamentoFinal;
	    
    	/** Atributo vlrInicio. */
    	private BigDecimal vlrInicio;
	    
    	/** Atributo vlFim. */
    	private BigDecimal vlFim;
	    
    	/** Atributo cdSituacaoPagamento. */
    	private Integer cdSituacaoPagamento;
	    
    	/** Atributo cdMotivoSituacaoPagamento. */
    	private Integer cdMotivoSituacaoPagamento;
	    
    	/** Atributo linhaDigitavel. */
    	private String linhaDigitavel;
	    
    	/** Atributo cdFavorecidoCliente. */
    	private String cdFavorecidoCliente;
	    
    	/** Atributo cdTipoInscricaoRecebedor. */
    	private Integer cdTipoInscricaoRecebedor;
	    
    	/** Atributo cdInscricaoRecebedor. */
    	private Long cdInscricaoRecebedor;
	    
    	/** Atributo cdCpfCnpjRecebedor. */
    	private Long cdCpfCnpjRecebedor;
	    
    	/** Atributo cdFilialCnpjRecebedor. */
    	private Integer cdFilialCnpjRecebedor;
	    
    	/** Atributo cdControleCpfRecebedor. */
    	private Integer cdControleCpfRecebedor;
	    
    	/** Atributo cdInscricaoFavorecido. */
    	private Long cdInscricaoFavorecido;
	    
    	/** Atributo cdBancoCredito. */
    	private Integer cdBancoCredito;
	    
    	/** Atributo dsBancoCredito. */
    	private String dsBancoCredito;
	    
    	/** Atributo cdAgenciaCredito. */
    	private Integer cdAgenciaCredito;
	    
    	/** Atributo dsAgenciaCredito. */
    	private String dsAgenciaCredito;
	    
    	/** Atributo cdContaCredito. */
    	private Long cdContaCredito;
	    
    	/** Atributo digitoContaCredito. */
    	private String digitoContaCredito;
	    
    	/** Atributo cdTipoContaCredito. */
    	private Integer cdTipoContaCredito;
	    
    	/** Atributo dsContaCredito. */
    	private String dsContaCredito;
	    
    	/** Atributo dtRecuperacaoPagamento. */
    	private String dtRecuperacaoPagamento;
	    
    	/** Atributo dtNovoVencimentoPagamento. */
    	private String dtNovoVencimentoPagamento;
	    
    	/** Atributo vlTarifaPadrao. */
    	private BigDecimal vlTarifaPadrao;
	    
    	/** Atributo vlTarifaNegociacao. */
    	private BigDecimal vlTarifaNegociacao;
	    
    	/** Atributo percentualBonificacaoTarifa. */
    	private BigDecimal percentualBonificacaoTarifa;
	    
    	/** Atributo cdUsuarioInclusao. */
    	private String cdUsuarioInclusao;
	    
    	/** Atributo dtInclusao. */
    	private String dtInclusao;
	    
    	/** Atributo hrInclusao. */
    	private String hrInclusao;
	    
    	/** Atributo cdOperacaoCanalInclusao. */
    	private String cdOperacaoCanalInclusao;
	    
    	/** Atributo cdTipoCanalInclusao. */
    	private Integer cdTipoCanalInclusao;
	    
    	/** Atributo dsCanalInclusao. */
    	private String dsCanalInclusao;
	    
    	/** Atributo cdUsuarioManutencao. */
    	private String cdUsuarioManutencao;
	    
    	/** Atributo dtManutencao. */
    	private String dtManutencao;
	    
    	/** Atributo hrManutencao. */
    	private String hrManutencao;
	    
    	/** Atributo cdOperacaoCanalManutencao. */
    	private String cdOperacaoCanalManutencao;
	    
    	/** Atributo cdTipoCanalManutencao. */
    	private Integer cdTipoCanalManutencao;
	    
    	/** Atributo dsCanalManutencao. */
    	private String dsCanalManutencao;
	    
    	/** Atributo cdAcaoManutencao. */
    	private Integer cdAcaoManutencao;
	    
    	/** Atributo dsMotivoSituacaoPagamento. */
    	private String dsMotivoSituacaoPagamento;
	    
    	/** Atributo dsNomeFavorecido. */
    	private String dsNomeFavorecido;
	    
    	/** Atributo dsSituacaoPagamento. */
    	private String dsSituacaoPagamento;
	    
    	/** Atributo cdDigitoAgenciaDebito. */
    	private String cdDigitoAgenciaDebito;
	    
    	/** Atributo cdDigitoAgenciaCredito. */
    	private String cdDigitoAgenciaCredito;
	    
	    /** Atributo cpfCnpjFormatado. */
    	private String cpfCnpjFormatado;
    	
    	
    	private Long contaSalario;
    	private Integer bancoSalario;
    	private Integer agenciaSalario;
    	private String digitoSalario;
    	private Integer destinoPagamento;
    	private Integer bancoPgtoDestino;
    	private String ispbPgtoDestino;
    	private Long contaPgtoDestino;
    	private Long loteInterno;
    	
		public Long getCdPessoaJuridicaContrato() {
			return cdPessoaJuridicaContrato;
		}
		public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
			this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
		}
		public Integer getCdTipoContratoNegocio() {
			return cdTipoContratoNegocio;
		}
		public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
			this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		}
		public Long getNrSequenciaContratoNegocio() {
			return nrSequenciaContratoNegocio;
		}
		public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
			this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
		}
		public Integer getCdBancoDebito() {
			return cdBancoDebito;
		}
		public void setCdBancoDebito(Integer cdBancoDebito) {
			this.cdBancoDebito = cdBancoDebito;
		}
		public String getDsBancoDebito() {
			return dsBancoDebito;
		}
		public void setDsBancoDebito(String dsBancoDebito) {
			this.dsBancoDebito = dsBancoDebito;
		}
		public Integer getCdAgenciaDebito() {
			return cdAgenciaDebito;
		}
		public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
			this.cdAgenciaDebito = cdAgenciaDebito;
		}
		public String getDsAgenciaDebito() {
			return dsAgenciaDebito;
		}
		public void setDsAgenciaDebito(String dsAgenciaDebito) {
			this.dsAgenciaDebito = dsAgenciaDebito;
		}
		public Long getCdContaDebito() {
			return cdContaDebito;
		}
		public void setCdContaDebito(Long cdContaDebito) {
			this.cdContaDebito = cdContaDebito;
		}
		public String getCdDigitoContaDebito() {
			return cdDigitoContaDebito;
		}
		public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
			this.cdDigitoContaDebito = cdDigitoContaDebito;
		}
		public Integer getCdTipoContaDebito() {
			return cdTipoContaDebito;
		}
		public void setCdTipoContaDebito(Integer cdTipoContaDebito) {
			this.cdTipoContaDebito = cdTipoContaDebito;
		}
		public String getDsContaDebito() {
			return dsContaDebito;
		}
		public void setDsContaDebito(String dsContaDebito) {
			this.dsContaDebito = dsContaDebito;
		}
		public String getDtInicioPeriodoPesquisa() {
			return dtInicioPeriodoPesquisa;
		}
		public void setDtInicioPeriodoPesquisa(String dtInicioPeriodoPesquisa) {
			this.dtInicioPeriodoPesquisa = dtInicioPeriodoPesquisa;
		}
		public String getDtFimPeriodoPesquisa() {
			return dtFimPeriodoPesquisa;
		}
		public void setDtFimPeriodoPesquisa(String dtFimPeriodoPesquisa) {
			this.dtFimPeriodoPesquisa = dtFimPeriodoPesquisa;
		}
		public Integer getCdProduto() {
			return cdProduto;
		}
		public void setCdProduto(Integer cdProduto) {
			this.cdProduto = cdProduto;
		}
		public Integer getCdProdutoRelacionado() {
			return cdProdutoRelacionado;
		}
		public void setCdProdutoRelacionado(Integer cdProdutoRelacionado) {
			this.cdProdutoRelacionado = cdProdutoRelacionado;
		}
		public Long getNrSequenciaArquivoRemessa() {
			return nrSequenciaArquivoRemessa;
		}
		public void setNrSequenciaArquivoRemessa(Long nrSequenciaArquivoRemessa) {
			this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
		}
		public Long getCdPessoa() {
			return cdPessoa;
		}
		public void setCdPessoa(Long cdPessoa) {
			this.cdPessoa = cdPessoa;
		}
		public String getDsPessoas() {
			return dsPessoas;
		}
		public void setDsPessoas(String dsPessoas) {
			this.dsPessoas = dsPessoas;
		}
		public String getCdControlePagamentoInicial() {
			return cdControlePagamentoInicial;
		}
		public void setCdControlePagamentoInicial(String cdControlePagamentoInicial) {
			this.cdControlePagamentoInicial = cdControlePagamentoInicial;
		}
		public String getCdControlePagamentoFinal() {
			return cdControlePagamentoFinal;
		}
		public void setCdControlePagamentoFinal(String cdControlePagamentoFinal) {
			this.cdControlePagamentoFinal = cdControlePagamentoFinal;
		}
		public BigDecimal getVlrInicio() {
			return vlrInicio;
		}
		public void setVlrInicio(BigDecimal vlrInicio) {
			this.vlrInicio = vlrInicio;
		}
		public BigDecimal getVlFim() {
			return vlFim;
		}
		public void setVlFim(BigDecimal vlFim) {
			this.vlFim = vlFim;
		}
		public Integer getCdSituacaoPagamento() {
			return cdSituacaoPagamento;
		}
		public void setCdSituacaoPagamento(Integer cdSituacaoPagamento) {
			this.cdSituacaoPagamento = cdSituacaoPagamento;
		}
		public Integer getCdMotivoSituacaoPagamento() {
			return cdMotivoSituacaoPagamento;
		}
		public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
			this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
		}
		public String getLinhaDigitavel() {
			return linhaDigitavel;
		}
		public void setLinhaDigitavel(String linhaDigitavel) {
			this.linhaDigitavel = linhaDigitavel;
		}
		public String getCdFavorecidoCliente() {
			return cdFavorecidoCliente;
		}
		public void setCdFavorecidoCliente(String cdFavorecidoCliente) {
			this.cdFavorecidoCliente = cdFavorecidoCliente;
		}
		public Integer getCdTipoInscricaoRecebedor() {
			return cdTipoInscricaoRecebedor;
		}
		public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
			this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
		}
		public Long getCdInscricaoRecebedor() {
			return cdInscricaoRecebedor;
		}
		public void setCdInscricaoRecebedor(Long cdInscricaoRecebedor) {
			this.cdInscricaoRecebedor = cdInscricaoRecebedor;
		}
		public Long getCdCpfCnpjRecebedor() {
			return cdCpfCnpjRecebedor;
		}
		public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
			this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
		}
		public Integer getCdFilialCnpjRecebedor() {
			return cdFilialCnpjRecebedor;
		}
		public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
			this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
		}
		public Integer getCdControleCpfRecebedor() {
			return cdControleCpfRecebedor;
		}
		public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
			this.cdControleCpfRecebedor = cdControleCpfRecebedor;
		}
		public Long getCdInscricaoFavorecido() {
			return cdInscricaoFavorecido;
		}
		public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
			this.cdInscricaoFavorecido = cdInscricaoFavorecido;
		}
		public Integer getCdBancoCredito() {
			return cdBancoCredito;
		}
		public void setCdBancoCredito(Integer cdBancoCredito) {
			this.cdBancoCredito = cdBancoCredito;
		}
		public String getDsBancoCredito() {
			return dsBancoCredito;
		}
		public void setDsBancoCredito(String dsBancoCredito) {
			this.dsBancoCredito = dsBancoCredito;
		}
		public Integer getCdAgenciaCredito() {
			return cdAgenciaCredito;
		}
		public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
			this.cdAgenciaCredito = cdAgenciaCredito;
		}
		public String getDsAgenciaCredito() {
			return dsAgenciaCredito;
		}
		public void setDsAgenciaCredito(String dsAgenciaCredito) {
			this.dsAgenciaCredito = dsAgenciaCredito;
		}
		public Long getCdContaCredito() {
			return cdContaCredito;
		}
		public void setCdContaCredito(Long cdContaCredito) {
			this.cdContaCredito = cdContaCredito;
		}
		public String getDigitoContaCredito() {
			return digitoContaCredito;
		}
		public void setDigitoContaCredito(String digitoContaCredito) {
			this.digitoContaCredito = digitoContaCredito;
		}
		public Integer getCdTipoContaCredito() {
			return cdTipoContaCredito;
		}
		public void setCdTipoContaCredito(Integer cdTipoContaCredito) {
			this.cdTipoContaCredito = cdTipoContaCredito;
		}
		public String getDsContaCredito() {
			return dsContaCredito;
		}
		public void setDsContaCredito(String dsContaCredito) {
			this.dsContaCredito = dsContaCredito;
		}
		public String getDtRecuperacaoPagamento() {
			return dtRecuperacaoPagamento;
		}
		public void setDtRecuperacaoPagamento(String dtRecuperacaoPagamento) {
			this.dtRecuperacaoPagamento = dtRecuperacaoPagamento;
		}
		public String getDtNovoVencimentoPagamento() {
			return dtNovoVencimentoPagamento;
		}
		public void setDtNovoVencimentoPagamento(String dtNovoVencimentoPagamento) {
			this.dtNovoVencimentoPagamento = dtNovoVencimentoPagamento;
		}
		public BigDecimal getVlTarifaPadrao() {
			return vlTarifaPadrao;
		}
		public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
			this.vlTarifaPadrao = vlTarifaPadrao;
		}
		public BigDecimal getVlTarifaNegociacao() {
			return vlTarifaNegociacao;
		}
		public void setVlTarifaNegociacao(BigDecimal vlTarifaNegociacao) {
			this.vlTarifaNegociacao = vlTarifaNegociacao;
		}
		public BigDecimal getPercentualBonificacaoTarifa() {
			return percentualBonificacaoTarifa;
		}
		public void setPercentualBonificacaoTarifa(
				BigDecimal percentualBonificacaoTarifa) {
			this.percentualBonificacaoTarifa = percentualBonificacaoTarifa;
		}
		public String getCdUsuarioInclusao() {
			return cdUsuarioInclusao;
		}
		public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
			this.cdUsuarioInclusao = cdUsuarioInclusao;
		}
		public String getDtInclusao() {
			return dtInclusao;
		}
		public void setDtInclusao(String dtInclusao) {
			this.dtInclusao = dtInclusao;
		}
		public String getHrInclusao() {
			return hrInclusao;
		}
		public void setHrInclusao(String hrInclusao) {
			this.hrInclusao = hrInclusao;
		}
		public String getCdOperacaoCanalInclusao() {
			return cdOperacaoCanalInclusao;
		}
		public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
			this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
		}
		public Integer getCdTipoCanalInclusao() {
			return cdTipoCanalInclusao;
		}
		public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
			this.cdTipoCanalInclusao = cdTipoCanalInclusao;
		}
		public String getDsCanalInclusao() {
			return dsCanalInclusao;
		}
		public void setDsCanalInclusao(String dsCanalInclusao) {
			this.dsCanalInclusao = dsCanalInclusao;
		}
		public String getCdUsuarioManutencao() {
			return cdUsuarioManutencao;
		}
		public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
			this.cdUsuarioManutencao = cdUsuarioManutencao;
		}
		public String getDtManutencao() {
			return dtManutencao;
		}
		public void setDtManutencao(String dtManutencao) {
			this.dtManutencao = dtManutencao;
		}
		public String getHrManutencao() {
			return hrManutencao;
		}
		public void setHrManutencao(String hrManutencao) {
			this.hrManutencao = hrManutencao;
		}
		public String getCdOperacaoCanalManutencao() {
			return cdOperacaoCanalManutencao;
		}
		public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
			this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
		}
		public Integer getCdTipoCanalManutencao() {
			return cdTipoCanalManutencao;
		}
		public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
			this.cdTipoCanalManutencao = cdTipoCanalManutencao;
		}
		public String getDsCanalManutencao() {
			return dsCanalManutencao;
		}
		public void setDsCanalManutencao(String dsCanalManutencao) {
			this.dsCanalManutencao = dsCanalManutencao;
		}
		public Integer getCdAcaoManutencao() {
			return cdAcaoManutencao;
		}
		public void setCdAcaoManutencao(Integer cdAcaoManutencao) {
			this.cdAcaoManutencao = cdAcaoManutencao;
		}
		public String getDsMotivoSituacaoPagamento() {
			return dsMotivoSituacaoPagamento;
		}
		public void setDsMotivoSituacaoPagamento(String dsMotivoSituacaoPagamento) {
			this.dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
		}
		public String getDsNomeFavorecido() {
			return dsNomeFavorecido;
		}
		public void setDsNomeFavorecido(String dsNomeFavorecido) {
			this.dsNomeFavorecido = dsNomeFavorecido;
		}
		public String getDsSituacaoPagamento() {
			return dsSituacaoPagamento;
		}
		public void setDsSituacaoPagamento(String dsSituacaoPagamento) {
			this.dsSituacaoPagamento = dsSituacaoPagamento;
		}
		public String getCdDigitoAgenciaDebito() {
			return cdDigitoAgenciaDebito;
		}
		public void setCdDigitoAgenciaDebito(String cdDigitoAgenciaDebito) {
			this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
		}
		public String getCdDigitoAgenciaCredito() {
			return cdDigitoAgenciaCredito;
		}
		public void setCdDigitoAgenciaCredito(String cdDigitoAgenciaCredito) {
			this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
		}
		public String getCpfCnpjFormatado() {
			return cpfCnpjFormatado;
		}
		public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
			this.cpfCnpjFormatado = cpfCnpjFormatado;
		}
		public Long getContaSalario() {
			return contaSalario;
		}
		public void setContaSalario(Long contaSalario) {
			this.contaSalario = contaSalario;
		}
		public Integer getBancoSalario() {
			return bancoSalario;
		}
		public void setBancoSalario(Integer bancoSalario) {
			this.bancoSalario = bancoSalario;
		}
		public Integer getAgenciaSalario() {
			return agenciaSalario;
		}
		public void setAgenciaSalario(Integer agenciaSalario) {
			this.agenciaSalario = agenciaSalario;
		}
		public String getDigitoSalario() {
			return digitoSalario;
		}
		public void setDigitoSalario(String digitoSalario) {
			this.digitoSalario = digitoSalario;
		}
		public Integer getDestinoPagamento() {
			return destinoPagamento;
		}
		public void setDestinoPagamento(Integer destinoPagamento) {
			this.destinoPagamento = destinoPagamento;
		}
		public Integer getBancoPgtoDestino() {
			return bancoPgtoDestino;
		}
		public void setBancoPgtoDestino(Integer bancoPgtoDestino) {
			this.bancoPgtoDestino = bancoPgtoDestino;
		}
		public Long getContaPgtoDestino() {
			return contaPgtoDestino;
		}
		public void setContaPgtoDestino(Long contaPgtoDestino) {
			this.contaPgtoDestino = contaPgtoDestino;
		}
		public String getIspbPgtoDestino() {
			return ispbPgtoDestino;
		}
		public void setIspbPgtoDestino(String ispbPgtoDestino) {
			this.ispbPgtoDestino = ispbPgtoDestino;
		}
		public Long getLoteInterno() {
			return loteInterno;
		}
		public void setLoteInterno(Long loteInterno) {
			this.loteInterno = loteInterno;
		}
}
