/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarTipoMensagemSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoMensagemSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
    
    /** Atributo numeroLinhas. */
    private Integer numeroLinhas;
    
    /** Atributo cdControleMensagemSalarial. */
    private Integer cdControleMensagemSalarial;
    
    /** Atributo cdTipoComprovanteSalarial. */
    private Integer cdTipoComprovanteSalarial;
    
    /** Atributo dtTipoComprovanteSalarial. */
    private String dtTipoComprovanteSalarial;
    
    /** Atributo cdTipoMensagemSalarial. */
    private Integer cdTipoMensagemSalarial;
    
    /** Atributo dsControleMensagemSalarial. */
    private String dsControleMensagemSalarial;
    
    /** Atributo cdRestMensagemSalarial. */
    private Integer cdRestMensagemSalarial;
	
    /**
     * Listar tipo mensagem saida dto.
     */
    public ListarTipoMensagemSaidaDTO() {
		super();
	}

	/**
	 * Listar tipo mensagem saida dto.
	 *
	 * @param cdControleMensagemSalarial the cd controle mensagem salarial
	 * @param cdTipoComprovanteSalarial the cd tipo comprovante salarial
	 * @param dtTipoComprovanteSalarial the dt tipo comprovante salarial
	 * @param cdTipoMensagemSalarial the cd tipo mensagem salarial
	 * @param dsControleMensagemSalarial the ds controle mensagem salarial
	 * @param cdRestMensagemSalarial the cd rest mensagem salarial
	 */
	public ListarTipoMensagemSaidaDTO(Integer cdControleMensagemSalarial, Integer cdTipoComprovanteSalarial, String dtTipoComprovanteSalarial, Integer cdTipoMensagemSalarial, String dsControleMensagemSalarial, Integer cdRestMensagemSalarial) {
		super();
		this.cdControleMensagemSalarial = cdControleMensagemSalarial;
		this.cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
		this.dtTipoComprovanteSalarial = dtTipoComprovanteSalarial;
		this.cdTipoMensagemSalarial = cdTipoMensagemSalarial;
		this.dsControleMensagemSalarial = dsControleMensagemSalarial;
		this.cdRestMensagemSalarial = cdRestMensagemSalarial;
	}

	/**
	 * Get: cdControleMensagemSalarial.
	 *
	 * @return cdControleMensagemSalarial
	 */
	public Integer getCdControleMensagemSalarial() {
		return cdControleMensagemSalarial;
	}

	/**
	 * Set: cdControleMensagemSalarial.
	 *
	 * @param cdControleMensagemSalarial the cd controle mensagem salarial
	 */
	public void setCdControleMensagemSalarial(Integer cdControleMensagemSalarial) {
		this.cdControleMensagemSalarial = cdControleMensagemSalarial;
	}

	/**
	 * Get: cdRestMensagemSalarial.
	 *
	 * @return cdRestMensagemSalarial
	 */
	public Integer getCdRestMensagemSalarial() {
		return cdRestMensagemSalarial;
	}

	/**
	 * Set: cdRestMensagemSalarial.
	 *
	 * @param cdRestMensagemSalarial the cd rest mensagem salarial
	 */
	public void setCdRestMensagemSalarial(Integer cdRestMensagemSalarial) {
		this.cdRestMensagemSalarial = cdRestMensagemSalarial;
	}

	/**
	 * Get: cdTipoComprovanteSalarial.
	 *
	 * @return cdTipoComprovanteSalarial
	 */
	public Integer getCdTipoComprovanteSalarial() {
		return cdTipoComprovanteSalarial;
	}

	/**
	 * Set: cdTipoComprovanteSalarial.
	 *
	 * @param cdTipoComprovanteSalarial the cd tipo comprovante salarial
	 */
	public void setCdTipoComprovanteSalarial(Integer cdTipoComprovanteSalarial) {
		this.cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
	}

	/**
	 * Get: cdTipoMensagemSalarial.
	 *
	 * @return cdTipoMensagemSalarial
	 */
	public Integer getCdTipoMensagemSalarial() {
		return cdTipoMensagemSalarial;
	}

	/**
	 * Set: cdTipoMensagemSalarial.
	 *
	 * @param cdTipoMensagemSalarial the cd tipo mensagem salarial
	 */
	public void setCdTipoMensagemSalarial(Integer cdTipoMensagemSalarial) {
		this.cdTipoMensagemSalarial = cdTipoMensagemSalarial;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsControleMensagemSalarial.
	 *
	 * @return dsControleMensagemSalarial
	 */
	public String getDsControleMensagemSalarial() {
		return dsControleMensagemSalarial;
	}

	/**
	 * Set: dsControleMensagemSalarial.
	 *
	 * @param dsControleMensagemSalarial the ds controle mensagem salarial
	 */
	public void setDsControleMensagemSalarial(String dsControleMensagemSalarial) {
		this.dsControleMensagemSalarial = dsControleMensagemSalarial;
	}

	/**
	 * Get: dtTipoComprovanteSalarial.
	 *
	 * @return dtTipoComprovanteSalarial
	 */
	public String getDtTipoComprovanteSalarial() {
		return dtTipoComprovanteSalarial;
	}

	/**
	 * Set: dtTipoComprovanteSalarial.
	 *
	 * @param dtTipoComprovanteSalarial the dt tipo comprovante salarial
	 */
	public void setDtTipoComprovanteSalarial(String dtTipoComprovanteSalarial) {
		this.dtTipoComprovanteSalarial = dtTipoComprovanteSalarial;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
    
    
}
