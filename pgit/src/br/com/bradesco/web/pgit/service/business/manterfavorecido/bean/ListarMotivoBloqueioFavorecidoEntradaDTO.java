/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ListarMotivoBloqueioFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoBloqueioFavorecidoEntradaDTO {
	
	/** Atributo numeroOcorrencia. */
	private Integer numeroOcorrencia;
    
    /** Atributo cdSituacaoVinculacaoConta. */
    private Integer cdSituacaoVinculacaoConta;
    
	/**
	 * Listar motivo bloqueio favorecido entrada dto.
	 */
	public ListarMotivoBloqueioFavorecidoEntradaDTO() {
		super();
	}
	
	/**
	 * Get: cdSituacaoVinculacaoConta.
	 *
	 * @return cdSituacaoVinculacaoConta
	 */
	public Integer getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Set: cdSituacaoVinculacaoConta.
	 *
	 * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(Integer cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Get: numeroOcorrencia.
	 *
	 * @return numeroOcorrencia
	 */
	public Integer getNumeroOcorrencia() {
		return numeroOcorrencia;
	}
	
	/**
	 * Set: numeroOcorrencia.
	 *
	 * @param numeroOcorrencia the numero ocorrencia
	 */
	public void setNumeroOcorrencia(Integer numeroOcorrencia) {
		this.numeroOcorrencia = numeroOcorrencia;
	}

}
