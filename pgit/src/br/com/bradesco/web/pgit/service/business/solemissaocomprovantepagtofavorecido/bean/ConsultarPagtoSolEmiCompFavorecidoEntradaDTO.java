/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagtoSolEmiCompFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtoSolEmiCompFavorecidoEntradaDTO {

    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo dtCreditoPagamentoInicio. */
    private String dtCreditoPagamentoInicio;

    /** Atributo dtCreditoPagamentoFim. */
    private String dtCreditoPagamentoFim;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;

    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;

    /** Atributo cdControlePagamentoDe. */
    private String cdControlePagamentoDe;

    /** Atributo cdControlePagamentoAte. */
    private String cdControlePagamentoAte;

    /** Atributo vlPagamentoDe. */
    private BigDecimal vlPagamentoDe;

    /** Atributo vlPagamentoAte. */
    private BigDecimal vlPagamentoAte;

    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;

    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;

    /** Atributo cdIdentificacaoInscricaoFavorecido. */
    private Integer cdIdentificacaoInscricaoFavorecido;

    /**
     * Get: cdControlePagamentoAte.
     *
     * @return cdControlePagamentoAte
     */
    public String getCdControlePagamentoAte() {
	return cdControlePagamentoAte;
    }

    /**
     * Set: cdControlePagamentoAte.
     *
     * @param cdControlePagamentoAte the cd controle pagamento ate
     */
    public void setCdControlePagamentoAte(String cdControlePagamentoAte) {
	this.cdControlePagamentoAte = cdControlePagamentoAte;
    }

    /**
     * Get: cdControlePagamentoDe.
     *
     * @return cdControlePagamentoDe
     */
    public String getCdControlePagamentoDe() {
	return cdControlePagamentoDe;
    }

    /**
     * Set: cdControlePagamentoDe.
     *
     * @param cdControlePagamentoDe the cd controle pagamento de
     */
    public void setCdControlePagamentoDe(String cdControlePagamentoDe) {
	this.cdControlePagamentoDe = cdControlePagamentoDe;
    }

    /**
     * Get: cdFavorecidoClientePagador.
     *
     * @return cdFavorecidoClientePagador
     */
    public Long getCdFavorecidoClientePagador() {
	return cdFavorecidoClientePagador;
    }

    /**
     * Set: cdFavorecidoClientePagador.
     *
     * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
     */
    public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
	this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
    }

    /**
     * Get: cdIdentificacaoInscricaoFavorecido.
     *
     * @return cdIdentificacaoInscricaoFavorecido
     */
    public Integer getCdIdentificacaoInscricaoFavorecido() {
	return cdIdentificacaoInscricaoFavorecido;
    }

    /**
     * Set: cdIdentificacaoInscricaoFavorecido.
     *
     * @param cdIdentificacaoInscricaoFavorecido the cd identificacao inscricao favorecido
     */
    public void setCdIdentificacaoInscricaoFavorecido(Integer cdIdentificacaoInscricaoFavorecido) {
	this.cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
    }

    /**
     * Get: cdInscricaoFavorecido.
     *
     * @return cdInscricaoFavorecido
     */
    public Long getCdInscricaoFavorecido() {
	return cdInscricaoFavorecido;
    }

    /**
     * Set: cdInscricaoFavorecido.
     *
     * @param cdInscricaoFavorecido the cd inscricao favorecido
     */
    public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
	this.cdInscricaoFavorecido = cdInscricaoFavorecido;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
	return cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
	this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoRelacionado.
     *
     * @return cdProdutoServicoRelacionado
     */
    public Integer getCdProdutoServicoRelacionado() {
	return cdProdutoServicoRelacionado;
    }

    /**
     * Set: cdProdutoServicoRelacionado.
     *
     * @param cdProdutoServicoRelacionado the cd produto servico relacionado
     */
    public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
	this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: dtCreditoPagamentoFim.
     *
     * @return dtCreditoPagamentoFim
     */
    public String getDtCreditoPagamentoFim() {
	return dtCreditoPagamentoFim;
    }

    /**
     * Set: dtCreditoPagamentoFim.
     *
     * @param dtCreditoPagamentoFim the dt credito pagamento fim
     */
    public void setDtCreditoPagamentoFim(String dtCreditoPagamentoFim) {
	this.dtCreditoPagamentoFim = dtCreditoPagamentoFim;
    }

    /**
     * Get: dtCreditoPagamentoInicio.
     *
     * @return dtCreditoPagamentoInicio
     */
    public String getDtCreditoPagamentoInicio() {
	return dtCreditoPagamentoInicio;
    }

    /**
     * Set: dtCreditoPagamentoInicio.
     *
     * @param dtCreditoPagamentoInicio the dt credito pagamento inicio
     */
    public void setDtCreditoPagamentoInicio(String dtCreditoPagamentoInicio) {
	this.dtCreditoPagamentoInicio = dtCreditoPagamentoInicio;
    }

    /**
     * Get: nrOcorrencias.
     *
     * @return nrOcorrencias
     */
    public Integer getNrOcorrencias() {
	return nrOcorrencias;
    }

    /**
     * Set: nrOcorrencias.
     *
     * @param nrOcorrencias the nr ocorrencias
     */
    public void setNrOcorrencias(Integer nrOcorrencias) {
	this.nrOcorrencias = nrOcorrencias;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
	return nrSequenciaContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
	this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: vlPagamentoAte.
     *
     * @return vlPagamentoAte
     */
    public BigDecimal getVlPagamentoAte() {
	return vlPagamentoAte;
    }

    /**
     * Set: vlPagamentoAte.
     *
     * @param vlPagamentoAte the vl pagamento ate
     */
    public void setVlPagamentoAte(BigDecimal vlPagamentoAte) {
	this.vlPagamentoAte = vlPagamentoAte;
    }

    /**
     * Get: vlPagamentoDe.
     *
     * @return vlPagamentoDe
     */
    public BigDecimal getVlPagamentoDe() {
	return vlPagamentoDe;
    }

    /**
     * Set: vlPagamentoDe.
     *
     * @param vlPagamentoDe the vl pagamento de
     */
    public void setVlPagamentoDe(BigDecimal vlPagamentoDe) {
	this.vlPagamentoDe = vlPagamentoDe;
    }

}