/*
 * Nome: br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean;

/**
 * Nome: ListarFuncionarioSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarFuncionarioSaidaDTO {
	
	/** Atributo cdFuncionario. */
	private Long cdFuncionario;
	
	/** Atributo dsFuncionario. */
	private String dsFuncionario;
	
	/** Atributo cdJuncao. */
	private Integer cdJuncao;
	
	/**
	 * Get: cdFuncionario.
	 *
	 * @return cdFuncionario
	 */
	public Long getCdFuncionario() {
		return cdFuncionario;
	}
	
	/**
	 * Set: cdFuncionario.
	 *
	 * @param cdFuncionario the cd funcionario
	 */
	public void setCdFuncionario(Long cdFuncionario) {
		this.cdFuncionario = cdFuncionario;
	}
	
	/**
	 * Get: cdJuncao.
	 *
	 * @return cdJuncao
	 */
	public Integer getCdJuncao() {
		return cdJuncao;
	}
	
	/**
	 * Set: cdJuncao.
	 *
	 * @param cdJuncao the cd juncao
	 */
	public void setCdJuncao(Integer cdJuncao) {
		this.cdJuncao = cdJuncao;
	}
	
	/**
	 * Get: dsFuncionario.
	 *
	 * @return dsFuncionario
	 */
	public String getDsFuncionario() {
		return dsFuncionario;
	}
	
	/**
	 * Set: dsFuncionario.
	 *
	 * @param dsFuncionario the ds funcionario
	 */
	public void setDsFuncionario(String dsFuncionario) {
		this.dsFuncionario = dsFuncionario;
	}

}
