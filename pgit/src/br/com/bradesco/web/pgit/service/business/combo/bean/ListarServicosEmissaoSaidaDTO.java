/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response.Ocorrencias;

/**
 * Nome: ListarServicosEmissaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicosEmissaoSaidaDTO {

	/** Atributo cdProdutoOperacao. */
	private Integer cdProdutoOperacao;
	
	/** Atributo cdProdutoRelacionado. */
	private Integer cdProdutoRelacionado;
	
	/** Atributo cdRelacionamento. */
	private Integer cdRelacionamento;
	
	/** Atributo dsServico. */
	private String dsServico;

	/**
	 * Listar servicos emissao saida dto.
	 *
	 * @param occurs the occurs
	 */
	public ListarServicosEmissaoSaidaDTO(Ocorrencias occurs) {
		super();
		this.cdProdutoOperacao = occurs.getCdProdutoOperacao();
		this.cdProdutoRelacionado = occurs.getCdProdutoRelacionado();
		this.cdRelacionamento = occurs.getCdRelacionamento();
		this.dsServico = occurs.getDsServico();
	}

	/**
	 * Listar servicos emissao saida dto.
	 */
	public ListarServicosEmissaoSaidaDTO() {
		super();
	}
	
	/**
	 * Get: cdProdutoOperacao.
	 *
	 * @return cdProdutoOperacao
	 */
	public Integer getCdProdutoOperacao() {
		return cdProdutoOperacao;
	}
	
	/**
	 * Set: cdProdutoOperacao.
	 *
	 * @param cdProdutoOperacao the cd produto operacao
	 */
	public void setCdProdutoOperacao(Integer cdProdutoOperacao) {
		this.cdProdutoOperacao = cdProdutoOperacao;
	}
	
	/**
	 * Get: cdProdutoRelacionado.
	 *
	 * @return cdProdutoRelacionado
	 */
	public Integer getCdProdutoRelacionado() {
		return cdProdutoRelacionado;
	}
	
	/**
	 * Set: cdProdutoRelacionado.
	 *
	 * @param cdProdutoRelacionado the cd produto relacionado
	 */
	public void setCdProdutoRelacionado(Integer cdProdutoRelacionado) {
		this.cdProdutoRelacionado = cdProdutoRelacionado;
	}
	
	/**
	 * Get: cdRelacionamento.
	 *
	 * @return cdRelacionamento
	 */
	public Integer getCdRelacionamento() {
		return cdRelacionamento;
	}
	
	/**
	 * Set: cdRelacionamento.
	 *
	 * @param cdRelacionamento the cd relacionamento
	 */
	public void setCdRelacionamento(Integer cdRelacionamento) {
		this.cdRelacionamento = cdRelacionamento;
	}
	
	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}
	
	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}
}