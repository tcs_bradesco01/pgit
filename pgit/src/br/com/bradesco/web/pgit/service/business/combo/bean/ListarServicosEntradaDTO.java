/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarServicosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicosEntradaDTO {
	
	 /** Atributo numeroOcorrencias. */
 	private Integer numeroOcorrencias;
     
     /** Atributo cdNaturezaServico. */
     private Integer cdNaturezaServico;
     
	/**
	 * Get: cdNaturezaServico.
	 *
	 * @return cdNaturezaServico
	 */
	public Integer getCdNaturezaServico() {
		return cdNaturezaServico;
	}
	
	/**
	 * Set: cdNaturezaServico.
	 *
	 * @param cdNaturezaServico the cd natureza servico
	 */
	public void setCdNaturezaServico(Integer cdNaturezaServico) {
		this.cdNaturezaServico = cdNaturezaServico;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

	/**
	 * Listar servicos entrada dto.
	 */
	public ListarServicosEntradaDTO() {
		super();
	}

}
