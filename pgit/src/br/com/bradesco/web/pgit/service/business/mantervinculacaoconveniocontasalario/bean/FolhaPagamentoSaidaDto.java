package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.math.BigDecimal;

/**
 * The Class FolhaPagamentoSaidaDto.
 */
public class FolhaPagamentoSaidaDto {

	/** The cod mensagem. */
	private String codMensagem = null;

	/** The mensagem. */
	private String mensagem = null;

	/** The vl folha pagamento empresa. */
	private BigDecimal vlFolhaPagamentoEmpresa = null;

	/** The qt funcionario empresa pagadora. */
	private Long qtFuncionarioEmpresaPagadora = null;

	/** The vl media salarial empresa. */
	private BigDecimal vlMediaSalarialEmpresa = null;

	/** The dt mes ano correspondente. */
	private Integer dtMesAnoCorrespondente = null;

	/** The cd indicador pagamento mes. */
	private Integer cdIndicadorPagamentoMes = null;

	/** The cd indicador pagamento quinzena. */
	private Integer cdIndicadorPagamentoQuinzena = null;

	/** The cd abertura conta salario. */
	private Integer cdAberturaContaSalario = null;

	/**
	 * Gets the cod mensagem.
	 * 
	 * @return the codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Sets the cod mensagem.
	 * 
	 * @param codMensagem
	 *            the codMensagem to set
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Gets the mensagem.
	 * 
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Sets the mensagem.
	 * 
	 * @param mensagem
	 *            the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Gets the vl folha pagamento empresa.
	 * 
	 * @return the vlFolhaPagamentoEmpresa
	 */
	public BigDecimal getVlFolhaPagamentoEmpresa() {
		return vlFolhaPagamentoEmpresa;
	}

	/**
	 * Sets the vl folha pagamento empresa.
	 * 
	 * @param vlFolhaPagamentoEmpresa
	 *            the vlFolhaPagamentoEmpresa to set
	 */
	public void setVlFolhaPagamentoEmpresa(BigDecimal vlFolhaPagamentoEmpresa) {
		this.vlFolhaPagamentoEmpresa = vlFolhaPagamentoEmpresa;
	}

	/**
	 * Gets the qt funcionario empresa pagadora.
	 * 
	 * @return the qtFuncionarioEmpresaPagadora
	 */
	public Long getQtFuncionarioEmpresaPagadora() {
		return qtFuncionarioEmpresaPagadora;
	}

	/**
	 * Sets the qt funcionario empresa pagadora.
	 * 
	 * @param qtFuncionarioEmpresaPagadora
	 *            the qtFuncionarioEmpresaPagadora to set
	 */
	public void setQtFuncionarioEmpresaPagadora(
			Long qtFuncionarioEmpresaPagadora) {
		this.qtFuncionarioEmpresaPagadora = qtFuncionarioEmpresaPagadora;
	}

	/**
	 * Gets the vl media salarial empresa.
	 * 
	 * @return the vlMediaSalarialEmpresa
	 */
	public BigDecimal getVlMediaSalarialEmpresa() {
		return vlMediaSalarialEmpresa;
	}

	/**
	 * Sets the vl media salarial empresa.
	 * 
	 * @param vlMediaSalarialEmpresa
	 *            the vlMediaSalarialEmpresa to set
	 */
	public void setVlMediaSalarialEmpresa(BigDecimal vlMediaSalarialEmpresa) {
		this.vlMediaSalarialEmpresa = vlMediaSalarialEmpresa;
	}

	/**
	 * Gets the dt mes ano correspondente.
	 * 
	 * @return the dtMesAnoCorrespondente
	 */
	public Integer getDtMesAnoCorrespondente() {
		return dtMesAnoCorrespondente;
	}

	/**
	 * Sets the dt mes ano correspondente.
	 * 
	 * @param dtMesAnoCorrespondente
	 *            the dtMesAnoCorrespondente to set
	 */
	public void setDtMesAnoCorrespondente(Integer dtMesAnoCorrespondente) {
		this.dtMesAnoCorrespondente = dtMesAnoCorrespondente;
	}

	/**
	 * Gets the cd indicador pagamento mes.
	 * 
	 * @return the cdIndicadorPagamentoMes
	 */
	public Integer getCdIndicadorPagamentoMes() {
		return cdIndicadorPagamentoMes;
	}

	/**
	 * Sets the cd indicador pagamento mes.
	 * 
	 * @param cdIndicadorPagamentoMes
	 *            the cdIndicadorPagamentoMes to set
	 */
	public void setCdIndicadorPagamentoMes(Integer cdIndicadorPagamentoMes) {
		this.cdIndicadorPagamentoMes = cdIndicadorPagamentoMes;
	}

	/**
	 * Gets the cd indicador pagamento quinzena.
	 * 
	 * @return the cdIndicadorPagamentoQuinzena
	 */
	public Integer getCdIndicadorPagamentoQuinzena() {
		return cdIndicadorPagamentoQuinzena;
	}

	/**
	 * Sets the cd indicador pagamento quinzena.
	 * 
	 * @param cdIndicadorPagamentoQuinzena
	 *            the cdIndicadorPagamentoQuinzena to set
	 */
	public void setCdIndicadorPagamentoQuinzena(
			Integer cdIndicadorPagamentoQuinzena) {
		this.cdIndicadorPagamentoQuinzena = cdIndicadorPagamentoQuinzena;
	}

	/**
	 * Gets the cd abertura conta salario.
	 * 
	 * @return the cdAberturaContaSalario
	 */
	public Integer getCdAberturaContaSalario() {
		return cdAberturaContaSalario;
	}

	/**
	 * Sets the cd abertura conta salario.
	 * 
	 * @param cdAberturaContaSalario
	 *            the cdAberturaContaSalario to set
	 */
	public void setCdAberturaContaSalario(Integer cdAberturaContaSalario) {
		this.cdAberturaContaSalario = cdAberturaContaSalario;
	}

}
