/*
 * Nome: br.com.bradesco.web.pgit.service.business.histcomplementar.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.histcomplementar.bean;

/**
 * Nome: IncluirMantHistoricoComplementarEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirMantHistoricoComplementarEntradaDTO {
	
	/** Atributo codigoHistoricoComplementar. */
	private int codigoHistoricoComplementar;
	
	/** Atributo prefixoDebito. */
	private String prefixoDebito;
	
	/** Atributo eventoMensagemDebito. */
	private int eventoMensagemDebito;
	
	/** Atributo recursoDebito. */
	private int recursoDebito;
	
	/** Atributo idiomaDebito. */
	private int idiomaDebito;
	
	/** Atributo prefixoCredito. */
	private String prefixoCredito;
	
	/** Atributo eventoMensagemCredito. */
	private int eventoMensagemCredito;
	
	/** Atributo recursoCredito. */
	private int recursoCredito;
	
	/** Atributo idiomaCredito. */
	private int idiomaCredito;
	
	/** Atributo restricao. */
	private int restricao;
	
	/** Atributo tipoLancamento. */
	private int tipoLancamento;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo dsSegundaLinhaExtratoCredito. */ 
	private String dsSegundaLinhaExtratoCredito;
	
	/** Atributo dsSegundaLinhaExtratoDebito. */
	private String dsSegundaLinhaExtratoDebito;
	
	/**
	 * Get: codigoHistoricoComplementar.
	 *
	 * @return codigoHistoricoComplementar
	 */
	public int getCodigoHistoricoComplementar() {
		return codigoHistoricoComplementar;
	}
	
	/**
	 * Set: codigoHistoricoComplementar.
	 *
	 * @param codigoHistoricoComplementar the codigo historico complementar
	 */
	public void setCodigoHistoricoComplementar(int codigoHistoricoComplementar) {
		this.codigoHistoricoComplementar = codigoHistoricoComplementar;
	}
	
	/**
	 * Get: eventoMensagemCredito.
	 *
	 * @return eventoMensagemCredito
	 */
	public int getEventoMensagemCredito() {
		return eventoMensagemCredito;
	}
	
	/**
	 * Set: eventoMensagemCredito.
	 *
	 * @param eventoMensagemCredito the evento mensagem credito
	 */
	public void setEventoMensagemCredito(int eventoMensagemCredito) {
		this.eventoMensagemCredito = eventoMensagemCredito;
	}
	
	/**
	 * Get: eventoMensagemDebito.
	 *
	 * @return eventoMensagemDebito
	 */
	public int getEventoMensagemDebito() {
		return eventoMensagemDebito;
	}
	
	/**
	 * Set: eventoMensagemDebito.
	 *
	 * @param eventoMensagemDebito the evento mensagem debito
	 */
	public void setEventoMensagemDebito(int eventoMensagemDebito) {
		this.eventoMensagemDebito = eventoMensagemDebito;
	}
	
	/**
	 * Get: idiomaCredito.
	 *
	 * @return idiomaCredito
	 */
	public int getIdiomaCredito() {
		return idiomaCredito;
	}
	
	/**
	 * Set: idiomaCredito.
	 *
	 * @param idiomaCredito the idioma credito
	 */
	public void setIdiomaCredito(int idiomaCredito) {
		this.idiomaCredito = idiomaCredito;
	}
	
	/**
	 * Get: idiomaDebito.
	 *
	 * @return idiomaDebito
	 */
	public int getIdiomaDebito() {
		return idiomaDebito;
	}
	
	/**
	 * Set: idiomaDebito.
	 *
	 * @param idiomaDebito the idioma debito
	 */
	public void setIdiomaDebito(int idiomaDebito) {
		this.idiomaDebito = idiomaDebito;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: prefixoCredito.
	 *
	 * @return prefixoCredito
	 */
	public String getPrefixoCredito() {
		return prefixoCredito;
	}
	
	/**
	 * Set: prefixoCredito.
	 *
	 * @param prefixoCredito the prefixo credito
	 */
	public void setPrefixoCredito(String prefixoCredito) {
		this.prefixoCredito = prefixoCredito;
	}
	
	/**
	 * Get: prefixoDebito.
	 *
	 * @return prefixoDebito
	 */
	public String getPrefixoDebito() {
		return prefixoDebito;
	}
	
	/**
	 * Set: prefixoDebito.
	 *
	 * @param prefixoDebito the prefixo debito
	 */
	public void setPrefixoDebito(String prefixoDebito) {
		this.prefixoDebito = prefixoDebito;
	}
	
	/**
	 * Get: recursoCredito.
	 *
	 * @return recursoCredito
	 */
	public int getRecursoCredito() {
		return recursoCredito;
	}
	
	/**
	 * Set: recursoCredito.
	 *
	 * @param recursoCredito the recurso credito
	 */
	public void setRecursoCredito(int recursoCredito) {
		this.recursoCredito = recursoCredito;
	}
	
	/**
	 * Get: recursoDebito.
	 *
	 * @return recursoDebito
	 */
	public int getRecursoDebito() {
		return recursoDebito;
	}
	
	/**
	 * Set: recursoDebito.
	 *
	 * @param recursoDebito the recurso debito
	 */
	public void setRecursoDebito(int recursoDebito) {
		this.recursoDebito = recursoDebito;
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public int getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(int restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: tipoLancamento.
	 *
	 * @return tipoLancamento
	 */
	public int getTipoLancamento() {
		return tipoLancamento;
	}
	
	/**
	 * Set: tipoLancamento.
	 *
	 * @param tipoLancamento the tipo lancamento
	 */
	public void setTipoLancamento(int tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * @return the dsSegundaLinhaExtratoCredito
	 */
	public String getDsSegundaLinhaExtratoCredito() {
		return dsSegundaLinhaExtratoCredito;
	}

	/**
	 * @param dsSegundaLinhaExtratoCredito the dsSegundaLinhaExtratoCredito to set
	 */
	public void setDsSegundaLinhaExtratoCredito(String dsSegundaLinhaExtratoCredito) {
		this.dsSegundaLinhaExtratoCredito = dsSegundaLinhaExtratoCredito;
	}

	/**
	 * @return the dsSegundaLinhaExtratoDebito
	 */
	public String getDsSegundaLinhaExtratoDebito() {
		return dsSegundaLinhaExtratoDebito;
	}

	/**
	 * @param dsSegundaLinhaExtratoDebito the dsSegundaLinhaExtratoDebito to set
	 */
	public void setDsSegundaLinhaExtratoDebito(String dsSegundaLinhaExtratoDebito) {
		this.dsSegundaLinhaExtratoDebito = dsSegundaLinhaExtratoDebito;
	}
	
	
	
	

}


