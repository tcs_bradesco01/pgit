/*
 * Nome: br.com.bradesco.web.pgit.service.business.duplarqretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.duplarqretorno.bean;

/**
 * Nome: ExcluirDuplicacaoArqRetornoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirDuplicacaoArqRetornoEntradaDTO {

	/** Atributo cdPessoaJuridVinc. */
	private Long cdPessoaJuridVinc;
	
	/** Atributo cdTipoContratoVinc. */
	private Integer cdTipoContratoVinc;
	
	/** Atributo nrSeqContratoVinc. */
	private Long nrSeqContratoVinc;
	
	/** Atributo cdPessoaJuridContrato. */
	private Long cdPessoaJuridContrato; 
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio; 
	
	/** Atributo nrSeqContratoNegocio. */
	private Long nrSeqContratoNegocio; 
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdTipoArquivoRetorno. */
	private Integer cdTipoArquivoRetorno;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdInstrucaoEnvioRetorno. */
	private Integer cdInstrucaoEnvioRetorno;
	
	/** Atributo cdIndicadorDuplicidadeRetorno. */
	private Integer cdIndicadorDuplicidadeRetorno;
	
	/** Atributo cdProdutoServicoOper. */
	private Integer cdProdutoServicoOper;
	
	/**
	 * Get: cdIndicadorDuplicidadeRetorno.
	 *
	 * @return cdIndicadorDuplicidadeRetorno
	 */
	public Integer getCdIndicadorDuplicidadeRetorno() {
		return cdIndicadorDuplicidadeRetorno;
	}
	
	/**
	 * Set: cdIndicadorDuplicidadeRetorno.
	 *
	 * @param cdIndicadorDuplicidadeRetorno the cd indicador duplicidade retorno
	 */
	public void setCdIndicadorDuplicidadeRetorno(
			Integer cdIndicadorDuplicidadeRetorno) {
		this.cdIndicadorDuplicidadeRetorno = cdIndicadorDuplicidadeRetorno;
	}
	
	/**
	 * Get: cdInstrucaoEnvioRetorno.
	 *
	 * @return cdInstrucaoEnvioRetorno
	 */
	public Integer getCdInstrucaoEnvioRetorno() {
		return cdInstrucaoEnvioRetorno;
	}
	
	/**
	 * Set: cdInstrucaoEnvioRetorno.
	 *
	 * @param cdInstrucaoEnvioRetorno the cd instrucao envio retorno
	 */
	public void setCdInstrucaoEnvioRetorno(Integer cdInstrucaoEnvioRetorno) {
		this.cdInstrucaoEnvioRetorno = cdInstrucaoEnvioRetorno;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridContrato.
	 *
	 * @return cdPessoaJuridContrato
	 */
	public Long getCdPessoaJuridContrato() {
		return cdPessoaJuridContrato;
	}
	
	/**
	 * Set: cdPessoaJuridContrato.
	 *
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 */
	public void setCdPessoaJuridContrato(Long cdPessoaJuridContrato) {
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
	}
	
	/**
	 * Get: cdPessoaJuridVinc.
	 *
	 * @return cdPessoaJuridVinc
	 */
	public Long getCdPessoaJuridVinc() {
		return cdPessoaJuridVinc;
	}
	
	/**
	 * Set: cdPessoaJuridVinc.
	 *
	 * @param cdPessoaJuridVinc the cd pessoa jurid vinc
	 */
	public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
		this.cdPessoaJuridVinc = cdPessoaJuridVinc;
	}
	
	/**
	 * Get: cdProdutoServicoOper.
	 *
	 * @return cdProdutoServicoOper
	 */
	public Integer getCdProdutoServicoOper() {
		return cdProdutoServicoOper;
	}
	
	/**
	 * Set: cdProdutoServicoOper.
	 *
	 * @param cdProdutoServicoOper the cd produto servico oper
	 */
	public void setCdProdutoServicoOper(Integer cdProdutoServicoOper) {
		this.cdProdutoServicoOper = cdProdutoServicoOper;
	}
	
	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return cdTipoArquivoRetorno
	 */
	public Integer getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}
	
	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdTipoArquivoRetorno the cd tipo arquivo retorno
	 */
	public void setCdTipoArquivoRetorno(Integer cdTipoArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}
	
	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}
	
	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}
	
	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}
	
	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}
	

	
}
