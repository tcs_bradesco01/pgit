/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadproccontrole.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadproccontrole.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Nome: HistoricoBloqueioEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class HistoricoBloqueioEntradaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -6515863732649455754L;

	/** Atributo cdSistema. */
	private String cdSistema;

    /** Atributo cdProcessoSistema. */
    private Integer cdProcessoSistema;

    /** Atributo dtDe. */
    private Date dtDe;

    /** Atributo dtAte. */
    private Date dtAte;

    /** Atributo dtConsulta. */
    private String dtConsulta;

	/**
	 * Get: cdProcessoSistema.
	 *
	 * @return cdProcessoSistema
	 */
	public Integer getCdProcessoSistema() {
		return cdProcessoSistema;
	}

	/**
	 * Set: cdProcessoSistema.
	 *
	 * @param cdProcessoSistema the cd processo sistema
	 */
	public void setCdProcessoSistema(Integer cdProcessoSistema) {
		this.cdProcessoSistema = cdProcessoSistema;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: dtAte.
	 *
	 * @return dtAte
	 */
	public Date getDtAte() {
		return dtAte;
	}

	/**
	 * Set: dtAte.
	 *
	 * @param dtAte the dt ate
	 */
	public void setDtAte(Date dtAte) {
		this.dtAte = dtAte;
	}

	/**
	 * Get: dtDe.
	 *
	 * @return dtDe
	 */
	public Date getDtDe() {
		return dtDe;
	}

	/**
	 * Set: dtDe.
	 *
	 * @param dtDe the dt de
	 */
	public void setDtDe(Date dtDe) {
		this.dtDe = dtDe;
	}

	/**
	 * Get: dtConsulta.
	 *
	 * @return dtConsulta
	 */
	public String getDtConsulta() {
		return dtConsulta;
	}

	/**
	 * Set: dtConsulta.
	 *
	 * @param dtConsulta the dt consulta
	 */
	public void setDtConsulta(String dtConsulta) {
		this.dtConsulta = dtConsulta;
	}

}
