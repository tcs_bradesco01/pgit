/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdServicoCompostoPagamento
     */
    private long _cdServicoCompostoPagamento = 0;

    /**
     * keeps track of state for field: _cdServicoCompostoPagamento
     */
    private boolean _has_cdServicoCompostoPagamento;

    /**
     * Field _dsServicoCompostoPagamento
     */
    private java.lang.String _dsServicoCompostoPagamento;

    /**
     * Field _cdTipoServicoCnab
     */
    private int _cdTipoServicoCnab = 0;

    /**
     * keeps track of state for field: _cdTipoServicoCnab
     */
    private boolean _has_cdTipoServicoCnab;

    /**
     * Field _dsTipoServicoCnab
     */
    private java.lang.String _dsTipoServicoCnab;

    /**
     * Field _cdFormaLancamentoCnab
     */
    private int _cdFormaLancamentoCnab = 0;

    /**
     * keeps track of state for field: _cdFormaLancamentoCnab
     */
    private boolean _has_cdFormaLancamentoCnab;

    /**
     * Field _dsFormaLancamentoCnab
     */
    private java.lang.String _dsFormaLancamentoCnab;

    /**
     * Field _cdFormaLiquidacao
     */
    private int _cdFormaLiquidacao = 0;

    /**
     * keeps track of state for field: _cdFormaLiquidacao
     */
    private boolean _has_cdFormaLiquidacao;

    /**
     * Field _dsFormaLiquidacao
     */
    private java.lang.String _dsFormaLiquidacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaLancamentoCnab
     * 
     */
    public void deleteCdFormaLancamentoCnab()
    {
        this._has_cdFormaLancamentoCnab= false;
    } //-- void deleteCdFormaLancamentoCnab() 

    /**
     * Method deleteCdFormaLiquidacao
     * 
     */
    public void deleteCdFormaLiquidacao()
    {
        this._has_cdFormaLiquidacao= false;
    } //-- void deleteCdFormaLiquidacao() 

    /**
     * Method deleteCdServicoCompostoPagamento
     * 
     */
    public void deleteCdServicoCompostoPagamento()
    {
        this._has_cdServicoCompostoPagamento= false;
    } //-- void deleteCdServicoCompostoPagamento() 

    /**
     * Method deleteCdTipoServicoCnab
     * 
     */
    public void deleteCdTipoServicoCnab()
    {
        this._has_cdTipoServicoCnab= false;
    } //-- void deleteCdTipoServicoCnab() 

    /**
     * Returns the value of field 'cdFormaLancamentoCnab'.
     * 
     * @return int
     * @return the value of field 'cdFormaLancamentoCnab'.
     */
    public int getCdFormaLancamentoCnab()
    {
        return this._cdFormaLancamentoCnab;
    } //-- int getCdFormaLancamentoCnab() 

    /**
     * Returns the value of field 'cdFormaLiquidacao'.
     * 
     * @return int
     * @return the value of field 'cdFormaLiquidacao'.
     */
    public int getCdFormaLiquidacao()
    {
        return this._cdFormaLiquidacao;
    } //-- int getCdFormaLiquidacao() 

    /**
     * Returns the value of field 'cdServicoCompostoPagamento'.
     * 
     * @return long
     * @return the value of field 'cdServicoCompostoPagamento'.
     */
    public long getCdServicoCompostoPagamento()
    {
        return this._cdServicoCompostoPagamento;
    } //-- long getCdServicoCompostoPagamento() 

    /**
     * Returns the value of field 'cdTipoServicoCnab'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoCnab'.
     */
    public int getCdTipoServicoCnab()
    {
        return this._cdTipoServicoCnab;
    } //-- int getCdTipoServicoCnab() 

    /**
     * Returns the value of field 'dsFormaLancamentoCnab'.
     * 
     * @return String
     * @return the value of field 'dsFormaLancamentoCnab'.
     */
    public java.lang.String getDsFormaLancamentoCnab()
    {
        return this._dsFormaLancamentoCnab;
    } //-- java.lang.String getDsFormaLancamentoCnab() 

    /**
     * Returns the value of field 'dsFormaLiquidacao'.
     * 
     * @return String
     * @return the value of field 'dsFormaLiquidacao'.
     */
    public java.lang.String getDsFormaLiquidacao()
    {
        return this._dsFormaLiquidacao;
    } //-- java.lang.String getDsFormaLiquidacao() 

    /**
     * Returns the value of field 'dsServicoCompostoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsServicoCompostoPagamento'.
     */
    public java.lang.String getDsServicoCompostoPagamento()
    {
        return this._dsServicoCompostoPagamento;
    } //-- java.lang.String getDsServicoCompostoPagamento() 

    /**
     * Returns the value of field 'dsTipoServicoCnab'.
     * 
     * @return String
     * @return the value of field 'dsTipoServicoCnab'.
     */
    public java.lang.String getDsTipoServicoCnab()
    {
        return this._dsTipoServicoCnab;
    } //-- java.lang.String getDsTipoServicoCnab() 

    /**
     * Method hasCdFormaLancamentoCnab
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLancamentoCnab()
    {
        return this._has_cdFormaLancamentoCnab;
    } //-- boolean hasCdFormaLancamentoCnab() 

    /**
     * Method hasCdFormaLiquidacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaLiquidacao()
    {
        return this._has_cdFormaLiquidacao;
    } //-- boolean hasCdFormaLiquidacao() 

    /**
     * Method hasCdServicoCompostoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServicoCompostoPagamento()
    {
        return this._has_cdServicoCompostoPagamento;
    } //-- boolean hasCdServicoCompostoPagamento() 

    /**
     * Method hasCdTipoServicoCnab
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoCnab()
    {
        return this._has_cdTipoServicoCnab;
    } //-- boolean hasCdTipoServicoCnab() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaLancamentoCnab'.
     * 
     * @param cdFormaLancamentoCnab the value of field
     * 'cdFormaLancamentoCnab'.
     */
    public void setCdFormaLancamentoCnab(int cdFormaLancamentoCnab)
    {
        this._cdFormaLancamentoCnab = cdFormaLancamentoCnab;
        this._has_cdFormaLancamentoCnab = true;
    } //-- void setCdFormaLancamentoCnab(int) 

    /**
     * Sets the value of field 'cdFormaLiquidacao'.
     * 
     * @param cdFormaLiquidacao the value of field
     * 'cdFormaLiquidacao'.
     */
    public void setCdFormaLiquidacao(int cdFormaLiquidacao)
    {
        this._cdFormaLiquidacao = cdFormaLiquidacao;
        this._has_cdFormaLiquidacao = true;
    } //-- void setCdFormaLiquidacao(int) 

    /**
     * Sets the value of field 'cdServicoCompostoPagamento'.
     * 
     * @param cdServicoCompostoPagamento the value of field
     * 'cdServicoCompostoPagamento'.
     */
    public void setCdServicoCompostoPagamento(long cdServicoCompostoPagamento)
    {
        this._cdServicoCompostoPagamento = cdServicoCompostoPagamento;
        this._has_cdServicoCompostoPagamento = true;
    } //-- void setCdServicoCompostoPagamento(long) 

    /**
     * Sets the value of field 'cdTipoServicoCnab'.
     * 
     * @param cdTipoServicoCnab the value of field
     * 'cdTipoServicoCnab'.
     */
    public void setCdTipoServicoCnab(int cdTipoServicoCnab)
    {
        this._cdTipoServicoCnab = cdTipoServicoCnab;
        this._has_cdTipoServicoCnab = true;
    } //-- void setCdTipoServicoCnab(int) 

    /**
     * Sets the value of field 'dsFormaLancamentoCnab'.
     * 
     * @param dsFormaLancamentoCnab the value of field
     * 'dsFormaLancamentoCnab'.
     */
    public void setDsFormaLancamentoCnab(java.lang.String dsFormaLancamentoCnab)
    {
        this._dsFormaLancamentoCnab = dsFormaLancamentoCnab;
    } //-- void setDsFormaLancamentoCnab(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaLiquidacao'.
     * 
     * @param dsFormaLiquidacao the value of field
     * 'dsFormaLiquidacao'.
     */
    public void setDsFormaLiquidacao(java.lang.String dsFormaLiquidacao)
    {
        this._dsFormaLiquidacao = dsFormaLiquidacao;
    } //-- void setDsFormaLiquidacao(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoCompostoPagamento'.
     * 
     * @param dsServicoCompostoPagamento the value of field
     * 'dsServicoCompostoPagamento'.
     */
    public void setDsServicoCompostoPagamento(java.lang.String dsServicoCompostoPagamento)
    {
        this._dsServicoCompostoPagamento = dsServicoCompostoPagamento;
    } //-- void setDsServicoCompostoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoServicoCnab'.
     * 
     * @param dsTipoServicoCnab the value of field
     * 'dsTipoServicoCnab'.
     */
    public void setDsTipoServicoCnab(java.lang.String dsTipoServicoCnab)
    {
        this._dsTipoServicoCnab = dsTipoServicoCnab;
    } //-- void setDsTipoServicoCnab(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarservicocomposto.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
