/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrSolicitacaoRecuperacao
     */
    private int _nrSolicitacaoRecuperacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacaoRecuperacao
     */
    private boolean _has_nrSolicitacaoRecuperacao;

    /**
     * Field _hrSolicitacaoRecuperacao
     */
    private java.lang.String _hrSolicitacaoRecuperacao;

    /**
     * Field _cdUsuarioSolicitacao
     */
    private java.lang.String _cdUsuarioSolicitacao;

    /**
     * Field _dsSituacaoSolicitacaoRecuperacao
     */
    private java.lang.String _dsSituacaoSolicitacaoRecuperacao;

    /**
     * Field _dsDestinoRecuperacao
     */
    private java.lang.String _dsDestinoRecuperacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNrSolicitacaoRecuperacao
     * 
     */
    public void deleteNrSolicitacaoRecuperacao()
    {
        this._has_nrSolicitacaoRecuperacao= false;
    } //-- void deleteNrSolicitacaoRecuperacao() 

    /**
     * Returns the value of field 'cdUsuarioSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioSolicitacao'.
     */
    public java.lang.String getCdUsuarioSolicitacao()
    {
        return this._cdUsuarioSolicitacao;
    } //-- java.lang.String getCdUsuarioSolicitacao() 

    /**
     * Returns the value of field 'dsDestinoRecuperacao'.
     * 
     * @return String
     * @return the value of field 'dsDestinoRecuperacao'.
     */
    public java.lang.String getDsDestinoRecuperacao()
    {
        return this._dsDestinoRecuperacao;
    } //-- java.lang.String getDsDestinoRecuperacao() 

    /**
     * Returns the value of field
     * 'dsSituacaoSolicitacaoRecuperacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacaoRecuperacao'
     */
    public java.lang.String getDsSituacaoSolicitacaoRecuperacao()
    {
        return this._dsSituacaoSolicitacaoRecuperacao;
    } //-- java.lang.String getDsSituacaoSolicitacaoRecuperacao() 

    /**
     * Returns the value of field 'hrSolicitacaoRecuperacao'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacaoRecuperacao'.
     */
    public java.lang.String getHrSolicitacaoRecuperacao()
    {
        return this._hrSolicitacaoRecuperacao;
    } //-- java.lang.String getHrSolicitacaoRecuperacao() 

    /**
     * Returns the value of field 'nrSolicitacaoRecuperacao'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoRecuperacao'.
     */
    public int getNrSolicitacaoRecuperacao()
    {
        return this._nrSolicitacaoRecuperacao;
    } //-- int getNrSolicitacaoRecuperacao() 

    /**
     * Method hasNrSolicitacaoRecuperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoRecuperacao()
    {
        return this._has_nrSolicitacaoRecuperacao;
    } //-- boolean hasNrSolicitacaoRecuperacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdUsuarioSolicitacao'.
     * 
     * @param cdUsuarioSolicitacao the value of field
     * 'cdUsuarioSolicitacao'.
     */
    public void setCdUsuarioSolicitacao(java.lang.String cdUsuarioSolicitacao)
    {
        this._cdUsuarioSolicitacao = cdUsuarioSolicitacao;
    } //-- void setCdUsuarioSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoRecuperacao'.
     * 
     * @param dsDestinoRecuperacao the value of field
     * 'dsDestinoRecuperacao'.
     */
    public void setDsDestinoRecuperacao(java.lang.String dsDestinoRecuperacao)
    {
        this._dsDestinoRecuperacao = dsDestinoRecuperacao;
    } //-- void setDsDestinoRecuperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacaoRecuperacao'.
     * 
     * @param dsSituacaoSolicitacaoRecuperacao the value of field
     * 'dsSituacaoSolicitacaoRecuperacao'.
     */
    public void setDsSituacaoSolicitacaoRecuperacao(java.lang.String dsSituacaoSolicitacaoRecuperacao)
    {
        this._dsSituacaoSolicitacaoRecuperacao = dsSituacaoSolicitacaoRecuperacao;
    } //-- void setDsSituacaoSolicitacaoRecuperacao(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacaoRecuperacao'.
     * 
     * @param hrSolicitacaoRecuperacao the value of field
     * 'hrSolicitacaoRecuperacao'.
     */
    public void setHrSolicitacaoRecuperacao(java.lang.String hrSolicitacaoRecuperacao)
    {
        this._hrSolicitacaoRecuperacao = hrSolicitacaoRecuperacao;
    } //-- void setHrSolicitacaoRecuperacao(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacaoRecuperacao'.
     * 
     * @param nrSolicitacaoRecuperacao the value of field
     * 'nrSolicitacaoRecuperacao'.
     */
    public void setNrSolicitacaoRecuperacao(int nrSolicitacaoRecuperacao)
    {
        this._nrSolicitacaoRecuperacao = nrSolicitacaoRecuperacao;
        this._has_nrSolicitacaoRecuperacao = true;
    } //-- void setNrSolicitacaoRecuperacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicrecuperacao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
