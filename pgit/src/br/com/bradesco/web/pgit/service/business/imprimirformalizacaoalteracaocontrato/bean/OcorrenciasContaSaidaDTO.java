/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean;

import br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta;

/**
 * Nome: OcorrenciasContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasContaSaidaDTO {
    
    /** Atributo cdAcaoConta. */
    private Integer cdAcaoConta;
    
    /** Atributo cdBancoConta. */
    private Integer cdBancoConta;
    
    /** Atributo dsBancoConta. */
    private String dsBancoConta;
    
    /** Atributo cdAgenciaConta. */
    private Integer cdAgenciaConta;
    
    /** Atributo digitoAgenciaConta. */
    private Integer digitoAgenciaConta;
    
    /** Atributo dsAgenciaConta. */
    private String dsAgenciaConta;
    
    /** Atributo cdContaConta. */
    private Long cdContaConta;
    
    /** Atributo digitoContaConta. */
    private String digitoContaConta;
    
	/**
	 * Ocorrencias conta saida dto.
	 */
	public OcorrenciasContaSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Ocorrencias conta saida dto.
	 *
	 * @param ocorrenciasConta the ocorrencias conta
	 */
	public OcorrenciasContaSaidaDTO(OcorrenciasConta ocorrenciasConta) {
		super();
		this.cdAcaoConta = ocorrenciasConta.getCdAcaoConta();
		this.cdBancoConta = ocorrenciasConta.getCdBancoConta();
		this.dsBancoConta = ocorrenciasConta.getDsBancoConta();
		this.cdAgenciaConta = ocorrenciasConta.getCdAgenciaConta();
		this.digitoAgenciaConta = ocorrenciasConta.getDigitoAgenciaConta();
		this.dsAgenciaConta = ocorrenciasConta.getDsAgenciaConta();
		this.cdContaConta = ocorrenciasConta.getCdContaConta();
		this.digitoContaConta = ocorrenciasConta.getDigitoContaConta();
	}

	/**
	 * Get: cdAcaoConta.
	 *
	 * @return cdAcaoConta
	 */
	public Integer getCdAcaoConta() {
		return cdAcaoConta;
	}

	/**
	 * Set: cdAcaoConta.
	 *
	 * @param cdAcaoConta the cd acao conta
	 */
	public void setCdAcaoConta(Integer cdAcaoConta) {
		this.cdAcaoConta = cdAcaoConta;
	}

	/**
	 * Get: cdAgenciaConta.
	 *
	 * @return cdAgenciaConta
	 */
	public Integer getCdAgenciaConta() {
		return cdAgenciaConta;
	}

	/**
	 * Set: cdAgenciaConta.
	 *
	 * @param cdAgenciaConta the cd agencia conta
	 */
	public void setCdAgenciaConta(Integer cdAgenciaConta) {
		this.cdAgenciaConta = cdAgenciaConta;
	}

	/**
	 * Get: cdBancoConta.
	 *
	 * @return cdBancoConta
	 */
	public Integer getCdBancoConta() {
		return cdBancoConta;
	}

	/**
	 * Set: cdBancoConta.
	 *
	 * @param cdBancoConta the cd banco conta
	 */
	public void setCdBancoConta(Integer cdBancoConta) {
		this.cdBancoConta = cdBancoConta;
	}

	/**
	 * Get: cdContaConta.
	 *
	 * @return cdContaConta
	 */
	public Long getCdContaConta() {
		return cdContaConta;
	}

	/**
	 * Set: cdContaConta.
	 *
	 * @param cdContaConta the cd conta conta
	 */
	public void setCdContaConta(Long cdContaConta) {
		this.cdContaConta = cdContaConta;
	}

	/**
	 * Get: digitoAgenciaConta.
	 *
	 * @return digitoAgenciaConta
	 */
	public Integer getDigitoAgenciaConta() {
		return digitoAgenciaConta;
	}

	/**
	 * Set: digitoAgenciaConta.
	 *
	 * @param digitoAgenciaConta the digito agencia conta
	 */
	public void setDigitoAgenciaConta(Integer digitoAgenciaConta) {
		this.digitoAgenciaConta = digitoAgenciaConta;
	}

	/**
	 * Get: digitoContaConta.
	 *
	 * @return digitoContaConta
	 */
	public String getDigitoContaConta() {
		return digitoContaConta;
	}

	/**
	 * Set: digitoContaConta.
	 *
	 * @param digitoContaConta the digito conta conta
	 */
	public void setDigitoContaConta(String digitoContaConta) {
		this.digitoContaConta = digitoContaConta;
	}

	/**
	 * Get: dsAgenciaConta.
	 *
	 * @return dsAgenciaConta
	 */
	public String getDsAgenciaConta() {
		return dsAgenciaConta;
	}

	/**
	 * Set: dsAgenciaConta.
	 *
	 * @param dsAgenciaConta the ds agencia conta
	 */
	public void setDsAgenciaConta(String dsAgenciaConta) {
		this.dsAgenciaConta = dsAgenciaConta;
	}

	/**
	 * Get: dsBancoConta.
	 *
	 * @return dsBancoConta
	 */
	public String getDsBancoConta() {
		return dsBancoConta;
	}

	/**
	 * Set: dsBancoConta.
	 *
	 * @param dsBancoConta the ds banco conta
	 */
	public void setDsBancoConta(String dsBancoConta) {
		this.dsBancoConta = dsBancoConta;
	}
}
