/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMotivoSituacaoOperacao
     */
    private int _cdMotivoSituacaoOperacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoOperacao
     */
    private boolean _has_cdMotivoSituacaoOperacao;

    /**
     * Field _dsMotivoSituacaoOperacao
     */
    private java.lang.String _dsMotivoSituacaoOperacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoSituacaoOperacao
     * 
     */
    public void deleteCdMotivoSituacaoOperacao()
    {
        this._has_cdMotivoSituacaoOperacao= false;
    } //-- void deleteCdMotivoSituacaoOperacao() 

    /**
     * Returns the value of field 'cdMotivoSituacaoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoOperacao'.
     */
    public int getCdMotivoSituacaoOperacao()
    {
        return this._cdMotivoSituacaoOperacao;
    } //-- int getCdMotivoSituacaoOperacao() 

    /**
     * Returns the value of field 'dsMotivoSituacaoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoOperacao'.
     */
    public java.lang.String getDsMotivoSituacaoOperacao()
    {
        return this._dsMotivoSituacaoOperacao;
    } //-- java.lang.String getDsMotivoSituacaoOperacao() 

    /**
     * Method hasCdMotivoSituacaoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoOperacao()
    {
        return this._has_cdMotivoSituacaoOperacao;
    } //-- boolean hasCdMotivoSituacaoOperacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoSituacaoOperacao'.
     * 
     * @param cdMotivoSituacaoOperacao the value of field
     * 'cdMotivoSituacaoOperacao'.
     */
    public void setCdMotivoSituacaoOperacao(int cdMotivoSituacaoOperacao)
    {
        this._cdMotivoSituacaoOperacao = cdMotivoSituacaoOperacao;
        this._has_cdMotivoSituacaoOperacao = true;
    } //-- void setCdMotivoSituacaoOperacao(int) 

    /**
     * Sets the value of field 'dsMotivoSituacaoOperacao'.
     * 
     * @param dsMotivoSituacaoOperacao the value of field
     * 'dsMotivoSituacaoOperacao'.
     */
    public void setDsMotivoSituacaoOperacao(java.lang.String dsMotivoSituacaoOperacao)
    {
        this._dsMotivoSituacaoOperacao = dsMotivoSituacaoOperacao;
    } //-- void setDsMotivoSituacaoOperacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaoaditivocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
