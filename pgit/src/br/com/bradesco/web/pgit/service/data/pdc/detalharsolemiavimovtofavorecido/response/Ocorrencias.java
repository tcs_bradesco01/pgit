/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoCanal
     */
    private int _cdTipoCanal = 0;

    /**
     * keeps track of state for field: _cdTipoCanal
     */
    private boolean _has_cdTipoCanal;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _dtPagamento
     */
    private java.lang.String _dtPagamento;

    /**
     * Field _vlPagamento
     */
    private java.math.BigDecimal _vlPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _dsFavorecido
     */
    private java.lang.String _dsFavorecido;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdDigitoContaDebito
     */
    private int _cdDigitoContaDebito = 0;

    /**
     * keeps track of state for field: _cdDigitoContaDebito
     */
    private boolean _has_cdDigitoContaDebito;

    /**
     * Field _cdProdutoServico
     */
    private int _cdProdutoServico = 0;

    /**
     * keeps track of state for field: _cdProdutoServico
     */
    private boolean _has_cdProdutoServico;

    /**
     * Field _cdSituacaoPagamento
     */
    private int _cdSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoPagamento
     */
    private boolean _has_cdSituacaoPagamento;

    /**
     * Field _dsSituacaoPagamento
     */
    private java.lang.String _dsSituacaoPagamento;

    /**
     * Field _dsCodigoProdutoServicoOperacao
     */
    private java.lang.String _dsCodigoProdutoServicoOperacao;

    /**
     * Field _cdProdutoRelacionado
     */
    private int _cdProdutoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoRelacionado
     */
    private boolean _has_cdProdutoRelacionado;

    /**
     * Field _dsCodigoProdutoServicoRelacionado
     */
    private java.lang.String _dsCodigoProdutoServicoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdDigitoContaDebito
     * 
     */
    public void deleteCdDigitoContaDebito()
    {
        this._has_cdDigitoContaDebito= false;
    } //-- void deleteCdDigitoContaDebito() 

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdProdutoRelacionado
     * 
     */
    public void deleteCdProdutoRelacionado()
    {
        this._has_cdProdutoRelacionado= false;
    } //-- void deleteCdProdutoRelacionado() 

    /**
     * Method deleteCdProdutoServico
     * 
     */
    public void deleteCdProdutoServico()
    {
        this._has_cdProdutoServico= false;
    } //-- void deleteCdProdutoServico() 

    /**
     * Method deleteCdSituacaoPagamento
     * 
     */
    public void deleteCdSituacaoPagamento()
    {
        this._has_cdSituacaoPagamento= false;
    } //-- void deleteCdSituacaoPagamento() 

    /**
     * Method deleteCdTipoCanal
     * 
     */
    public void deleteCdTipoCanal()
    {
        this._has_cdTipoCanal= false;
    } //-- void deleteCdTipoCanal() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdDigitoContaDebito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoContaDebito'.
     */
    public int getCdDigitoContaDebito()
    {
        return this._cdDigitoContaDebito;
    } //-- int getCdDigitoContaDebito() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'cdProdutoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoRelacionado'.
     */
    public int getCdProdutoRelacionado()
    {
        return this._cdProdutoRelacionado;
    } //-- int getCdProdutoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServico'.
     */
    public int getCdProdutoServico()
    {
        return this._cdProdutoServico;
    } //-- int getCdProdutoServico() 

    /**
     * Returns the value of field 'cdSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPagamento'.
     */
    public int getCdSituacaoPagamento()
    {
        return this._cdSituacaoPagamento;
    } //-- int getCdSituacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoCanal'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanal'.
     */
    public int getCdTipoCanal()
    {
        return this._cdTipoCanal;
    } //-- int getCdTipoCanal() 

    /**
     * Returns the value of field 'dsCodigoProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsCodigoProdutoServicoOperacao'.
     */
    public java.lang.String getDsCodigoProdutoServicoOperacao()
    {
        return this._dsCodigoProdutoServicoOperacao;
    } //-- java.lang.String getDsCodigoProdutoServicoOperacao() 

    /**
     * Returns the value of field
     * 'dsCodigoProdutoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field
     * 'dsCodigoProdutoServicoRelacionado'.
     */
    public java.lang.String getDsCodigoProdutoServicoRelacionado()
    {
        return this._dsCodigoProdutoServicoRelacionado;
    } //-- java.lang.String getDsCodigoProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'dsFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsFavorecido'.
     */
    public java.lang.String getDsFavorecido()
    {
        return this._dsFavorecido;
    } //-- java.lang.String getDsFavorecido() 

    /**
     * Returns the value of field 'dsSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPagamento'.
     */
    public java.lang.String getDsSituacaoPagamento()
    {
        return this._dsSituacaoPagamento;
    } //-- java.lang.String getDsSituacaoPagamento() 

    /**
     * Returns the value of field 'dtPagamento'.
     * 
     * @return String
     * @return the value of field 'dtPagamento'.
     */
    public java.lang.String getDtPagamento()
    {
        return this._dtPagamento;
    } //-- java.lang.String getDtPagamento() 

    /**
     * Returns the value of field 'vlPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamento'.
     */
    public java.math.BigDecimal getVlPagamento()
    {
        return this._vlPagamento;
    } //-- java.math.BigDecimal getVlPagamento() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdDigitoContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoContaDebito()
    {
        return this._has_cdDigitoContaDebito;
    } //-- boolean hasCdDigitoContaDebito() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdProdutoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoRelacionado()
    {
        return this._has_cdProdutoRelacionado;
    } //-- boolean hasCdProdutoRelacionado() 

    /**
     * Method hasCdProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServico()
    {
        return this._has_cdProdutoServico;
    } //-- boolean hasCdProdutoServico() 

    /**
     * Method hasCdSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPagamento()
    {
        return this._has_cdSituacaoPagamento;
    } //-- boolean hasCdSituacaoPagamento() 

    /**
     * Method hasCdTipoCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanal()
    {
        return this._has_cdTipoCanal;
    } //-- boolean hasCdTipoCanal() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaDebito'.
     * 
     * @param cdDigitoContaDebito the value of field
     * 'cdDigitoContaDebito'.
     */
    public void setCdDigitoContaDebito(int cdDigitoContaDebito)
    {
        this._cdDigitoContaDebito = cdDigitoContaDebito;
        this._has_cdDigitoContaDebito = true;
    } //-- void setCdDigitoContaDebito(int) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'cdProdutoRelacionado'.
     * 
     * @param cdProdutoRelacionado the value of field
     * 'cdProdutoRelacionado'.
     */
    public void setCdProdutoRelacionado(int cdProdutoRelacionado)
    {
        this._cdProdutoRelacionado = cdProdutoRelacionado;
        this._has_cdProdutoRelacionado = true;
    } //-- void setCdProdutoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServico'.
     * 
     * @param cdProdutoServico the value of field 'cdProdutoServico'
     */
    public void setCdProdutoServico(int cdProdutoServico)
    {
        this._cdProdutoServico = cdProdutoServico;
        this._has_cdProdutoServico = true;
    } //-- void setCdProdutoServico(int) 

    /**
     * Sets the value of field 'cdSituacaoPagamento'.
     * 
     * @param cdSituacaoPagamento the value of field
     * 'cdSituacaoPagamento'.
     */
    public void setCdSituacaoPagamento(int cdSituacaoPagamento)
    {
        this._cdSituacaoPagamento = cdSituacaoPagamento;
        this._has_cdSituacaoPagamento = true;
    } //-- void setCdSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoCanal'.
     * 
     * @param cdTipoCanal the value of field 'cdTipoCanal'.
     */
    public void setCdTipoCanal(int cdTipoCanal)
    {
        this._cdTipoCanal = cdTipoCanal;
        this._has_cdTipoCanal = true;
    } //-- void setCdTipoCanal(int) 

    /**
     * Sets the value of field 'dsCodigoProdutoServicoOperacao'.
     * 
     * @param dsCodigoProdutoServicoOperacao the value of field
     * 'dsCodigoProdutoServicoOperacao'.
     */
    public void setDsCodigoProdutoServicoOperacao(java.lang.String dsCodigoProdutoServicoOperacao)
    {
        this._dsCodigoProdutoServicoOperacao = dsCodigoProdutoServicoOperacao;
    } //-- void setDsCodigoProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoProdutoServicoRelacionado'.
     * 
     * @param dsCodigoProdutoServicoRelacionado the value of field
     * 'dsCodigoProdutoServicoRelacionado'.
     */
    public void setDsCodigoProdutoServicoRelacionado(java.lang.String dsCodigoProdutoServicoRelacionado)
    {
        this._dsCodigoProdutoServicoRelacionado = dsCodigoProdutoServicoRelacionado;
    } //-- void setDsCodigoProdutoServicoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsFavorecido'.
     * 
     * @param dsFavorecido the value of field 'dsFavorecido'.
     */
    public void setDsFavorecido(java.lang.String dsFavorecido)
    {
        this._dsFavorecido = dsFavorecido;
    } //-- void setDsFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPagamento'.
     * 
     * @param dsSituacaoPagamento the value of field
     * 'dsSituacaoPagamento'.
     */
    public void setDsSituacaoPagamento(java.lang.String dsSituacaoPagamento)
    {
        this._dsSituacaoPagamento = dsSituacaoPagamento;
    } //-- void setDsSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtPagamento'.
     * 
     * @param dtPagamento the value of field 'dtPagamento'.
     */
    public void setDtPagamento(java.lang.String dtPagamento)
    {
        this._dtPagamento = dtPagamento;
    } //-- void setDtPagamento(java.lang.String) 

    /**
     * Sets the value of field 'vlPagamento'.
     * 
     * @param vlPagamento the value of field 'vlPagamento'.
     */
    public void setVlPagamento(java.math.BigDecimal vlPagamento)
    {
        this._vlPagamento = vlPagamento;
    } //-- void setVlPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolemiavimovtofavorecido.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
