/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class EstornarPagamentosOPRequest.
 * 
 * @version $Revision$ $Date$
 */
public class EstornarPagamentosOPRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPrimeiraSolicitacao
     */
    private java.lang.String _cdPrimeiraSolicitacao;

    /**
     * Field _cdSolicitacaoPagamento
     */
    private int _cdSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSolicitacaoPagamento
     */
    private boolean _has_cdSolicitacaoPagamento;

    /**
     * Field _nrSolicitacaoPagamento
     */
    private int _nrSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _nrSolicitacaoPagamento
     */
    private boolean _has_nrSolicitacaoPagamento;

    /**
     * Field _vlrPrevistoEstornoPagamento
     */
    private java.math.BigDecimal _vlrPrevistoEstornoPagamento = new java.math.BigDecimal("0");

    /**
     * Field _dsObservacaoAutorizacaoEstorno
     */
    private java.lang.String _dsObservacaoAutorizacaoEstorno;

    /**
     * Field _cdUsuarioAutorizacaoEstorno
     */
    private java.lang.String _cdUsuarioAutorizacaoEstorno;

    /**
     * Field _nrPagamento
     */
    private int _nrPagamento = 0;

    /**
     * keeps track of state for field: _nrPagamento
     */
    private boolean _has_nrPagamento;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public EstornarPagamentosOPRequest() 
     {
        super();
        setVlrPrevistoEstornoPagamento(new java.math.BigDecimal("0"));
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.EstornarPagamentosOPRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias) 

    /**
     * Method deleteCdSolicitacaoPagamento
     * 
     */
    public void deleteCdSolicitacaoPagamento()
    {
        this._has_cdSolicitacaoPagamento= false;
    } //-- void deleteCdSolicitacaoPagamento() 

    /**
     * Method deleteNrPagamento
     * 
     */
    public void deleteNrPagamento()
    {
        this._has_nrPagamento= false;
    } //-- void deleteNrPagamento() 

    /**
     * Method deleteNrSolicitacaoPagamento
     * 
     */
    public void deleteNrSolicitacaoPagamento()
    {
        this._has_nrSolicitacaoPagamento= false;
    } //-- void deleteNrSolicitacaoPagamento() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdPrimeiraSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdPrimeiraSolicitacao'.
     */
    public java.lang.String getCdPrimeiraSolicitacao()
    {
        return this._cdPrimeiraSolicitacao;
    } //-- java.lang.String getCdPrimeiraSolicitacao() 

    /**
     * Returns the value of field 'cdSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamento'.
     */
    public int getCdSolicitacaoPagamento()
    {
        return this._cdSolicitacaoPagamento;
    } //-- int getCdSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdUsuarioAutorizacaoEstorno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioAutorizacaoEstorno'.
     */
    public java.lang.String getCdUsuarioAutorizacaoEstorno()
    {
        return this._cdUsuarioAutorizacaoEstorno;
    } //-- java.lang.String getCdUsuarioAutorizacaoEstorno() 

    /**
     * Returns the value of field 'dsObservacaoAutorizacaoEstorno'.
     * 
     * @return String
     * @return the value of field 'dsObservacaoAutorizacaoEstorno'.
     */
    public java.lang.String getDsObservacaoAutorizacaoEstorno()
    {
        return this._dsObservacaoAutorizacaoEstorno;
    } //-- java.lang.String getDsObservacaoAutorizacaoEstorno() 

    /**
     * Returns the value of field 'nrPagamento'.
     * 
     * @return int
     * @return the value of field 'nrPagamento'.
     */
    public int getNrPagamento()
    {
        return this._nrPagamento;
    } //-- int getNrPagamento() 

    /**
     * Returns the value of field 'nrSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamento'.
     */
    public int getNrSolicitacaoPagamento()
    {
        return this._nrSolicitacaoPagamento;
    } //-- int getNrSolicitacaoPagamento() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'vlrPrevistoEstornoPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrPrevistoEstornoPagamento'.
     */
    public java.math.BigDecimal getVlrPrevistoEstornoPagamento()
    {
        return this._vlrPrevistoEstornoPagamento;
    } //-- java.math.BigDecimal getVlrPrevistoEstornoPagamento() 

    /**
     * Method hasCdSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamento()
    {
        return this._has_cdSolicitacaoPagamento;
    } //-- boolean hasCdSolicitacaoPagamento() 

    /**
     * Method hasNrPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPagamento()
    {
        return this._has_nrPagamento;
    } //-- boolean hasNrPagamento() 

    /**
     * Method hasNrSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamento()
    {
        return this._has_nrSolicitacaoPagamento;
    } //-- boolean hasNrSolicitacaoPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdPrimeiraSolicitacao'.
     * 
     * @param cdPrimeiraSolicitacao the value of field
     * 'cdPrimeiraSolicitacao'.
     */
    public void setCdPrimeiraSolicitacao(java.lang.String cdPrimeiraSolicitacao)
    {
        this._cdPrimeiraSolicitacao = cdPrimeiraSolicitacao;
    } //-- void setCdPrimeiraSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamento'.
     * 
     * @param cdSolicitacaoPagamento the value of field
     * 'cdSolicitacaoPagamento'.
     */
    public void setCdSolicitacaoPagamento(int cdSolicitacaoPagamento)
    {
        this._cdSolicitacaoPagamento = cdSolicitacaoPagamento;
        this._has_cdSolicitacaoPagamento = true;
    } //-- void setCdSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdUsuarioAutorizacaoEstorno'.
     * 
     * @param cdUsuarioAutorizacaoEstorno the value of field
     * 'cdUsuarioAutorizacaoEstorno'.
     */
    public void setCdUsuarioAutorizacaoEstorno(java.lang.String cdUsuarioAutorizacaoEstorno)
    {
        this._cdUsuarioAutorizacaoEstorno = cdUsuarioAutorizacaoEstorno;
    } //-- void setCdUsuarioAutorizacaoEstorno(java.lang.String) 

    /**
     * Sets the value of field 'dsObservacaoAutorizacaoEstorno'.
     * 
     * @param dsObservacaoAutorizacaoEstorno the value of field
     * 'dsObservacaoAutorizacaoEstorno'.
     */
    public void setDsObservacaoAutorizacaoEstorno(java.lang.String dsObservacaoAutorizacaoEstorno)
    {
        this._dsObservacaoAutorizacaoEstorno = dsObservacaoAutorizacaoEstorno;
    } //-- void setDsObservacaoAutorizacaoEstorno(java.lang.String) 

    /**
     * Sets the value of field 'nrPagamento'.
     * 
     * @param nrPagamento the value of field 'nrPagamento'.
     */
    public void setNrPagamento(int nrPagamento)
    {
        this._nrPagamento = nrPagamento;
        this._has_nrPagamento = true;
    } //-- void setNrPagamento(int) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamento'.
     * 
     * @param nrSolicitacaoPagamento the value of field
     * 'nrSolicitacaoPagamento'.
     */
    public void setNrSolicitacaoPagamento(int nrSolicitacaoPagamento)
    {
        this._nrSolicitacaoPagamento = nrSolicitacaoPagamento;
        this._has_nrSolicitacaoPagamento = true;
    } //-- void setNrSolicitacaoPagamento(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.Ocorrencias) 

    /**
     * Sets the value of field 'vlrPrevistoEstornoPagamento'.
     * 
     * @param vlrPrevistoEstornoPagamento the value of field
     * 'vlrPrevistoEstornoPagamento'.
     */
    public void setVlrPrevistoEstornoPagamento(java.math.BigDecimal vlrPrevistoEstornoPagamento)
    {
        this._vlrPrevistoEstornoPagamento = vlrPrevistoEstornoPagamento;
    } //-- void setVlrPrevistoEstornoPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return EstornarPagamentosOPRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.EstornarPagamentosOPRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.EstornarPagamentosOPRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.EstornarPagamentosOPRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.estornarpagamentosop.request.EstornarPagamentosOPRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
