/*
 * Nome: br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean;

/**
 * Nome: ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO {
	
	/** Atributo dsConveCtaSalrl. */
	private String dsConveCtaSalrl;
	
	/** Atributo cdBancoEmpresaConvn. */
	private Integer cdBancoEmpresaConvn;
	
	/** Atributo cdAgeEmpresaConvn. */
	private Integer cdAgeEmpresaConvn;
	
	/** Atributo cdDigitoAgencia. */
	private String cdDigitoAgencia;
	
	/** Atributo cdCtaEmpresaConvn. */
	private Long cdCtaEmpresaConvn;
	
	/** Atributo cdDigEmpresaConvn. */
	private String cdDigEmpresaConvn;
	
	/** Atributo cdConveNovo. */
	private Integer cdConveNovo;
	
	/** Atributo cdConveCtaSalarial. */
	private Long cdConveCtaSalarial;
	
	/** Atributo cdCpfCnpjConve. */
	private Long cdCpfCnpjConve;
	
	/** Atributo cdFilialCnpjConve. */
	private Integer cdFilialCnpjConve;
	
	/** Atributo cdCtrlCnpjEmp. */
	private Integer cdCtrlCnpjEmp;
	
	/** Atributo dsSitConve. */
	private String dsSitConve;

	/**
	 * Get: bcoAgContaFormatado.
	 *
	 * @return bcoAgContaFormatado
	 */
	public String getBcoAgContaFormatado(){
		return cdBancoEmpresaConvn + " / " + cdAgeEmpresaConvn + " / " + cdCtaEmpresaConvn + "-" + cdDigEmpresaConvn;
	}
	
	/**
	 * Get: dsConveCtaSalrl.
	 *
	 * @return dsConveCtaSalrl
	 */
	public String getDsConveCtaSalrl() {
		return dsConveCtaSalrl;
	}

	/**
	 * Set: dsConveCtaSalrl.
	 *
	 * @param dsConveCtaSalrl the ds conve cta salrl
	 */
	public void setDsConveCtaSalrl(String dsConveCtaSalrl) {
		this.dsConveCtaSalrl = dsConveCtaSalrl;
	}

	/**
	 * Get: cdBancoEmpresaConvn.
	 *
	 * @return cdBancoEmpresaConvn
	 */
	public Integer getCdBancoEmpresaConvn() {
		return cdBancoEmpresaConvn;
	}

	/**
	 * Set: cdBancoEmpresaConvn.
	 *
	 * @param cdBancoEmpresaConvn the cd banco empresa convn
	 */
	public void setCdBancoEmpresaConvn(Integer cdBancoEmpresaConvn) {
		this.cdBancoEmpresaConvn = cdBancoEmpresaConvn;
	}

	/**
	 * Get: cdAgeEmpresaConvn.
	 *
	 * @return cdAgeEmpresaConvn
	 */
	public Integer getCdAgeEmpresaConvn() {
		return cdAgeEmpresaConvn;
	}

	/**
	 * Set: cdAgeEmpresaConvn.
	 *
	 * @param cdAgeEmpresaConvn the cd age empresa convn
	 */
	public void setCdAgeEmpresaConvn(Integer cdAgeEmpresaConvn) {
		this.cdAgeEmpresaConvn = cdAgeEmpresaConvn;
	}

	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public String getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}

	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(String cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	/**
	 * Get: cdCtaEmpresaConvn.
	 *
	 * @return cdCtaEmpresaConvn
	 */
	public Long getCdCtaEmpresaConvn() {
		return cdCtaEmpresaConvn;
	}

	/**
	 * Set: cdCtaEmpresaConvn.
	 *
	 * @param cdCtaEmpresaConvn the cd cta empresa convn
	 */
	public void setCdCtaEmpresaConvn(Long cdCtaEmpresaConvn) {
		this.cdCtaEmpresaConvn = cdCtaEmpresaConvn;
	}

	/**
	 * Get: cdDigEmpresaConvn.
	 *
	 * @return cdDigEmpresaConvn
	 */
	public String getCdDigEmpresaConvn() {
		return cdDigEmpresaConvn;
	}

	/**
	 * Set: cdDigEmpresaConvn.
	 *
	 * @param cdDigEmpresaConvn the cd dig empresa convn
	 */
	public void setCdDigEmpresaConvn(String cdDigEmpresaConvn) {
		this.cdDigEmpresaConvn = cdDigEmpresaConvn;
	}

	/**
	 * Get: cdConveNovo.
	 *
	 * @return cdConveNovo
	 */
	public Integer getCdConveNovo() {
		return cdConveNovo;
	}

	/**
	 * Set: cdConveNovo.
	 *
	 * @param cdConveNovo the cd conve novo
	 */
	public void setCdConveNovo(Integer cdConveNovo) {
		this.cdConveNovo = cdConveNovo;
	}

	/**
	 * Get: cdConveCtaSalarial.
	 *
	 * @return cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}

	/**
	 * Set: cdConveCtaSalarial.
	 *
	 * @param cdConveCtaSalarial the cd conve cta salarial
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}

	/**
	 * Get: cdCpfCnpjConve.
	 *
	 * @return cdCpfCnpjConve
	 */
	public Long getCdCpfCnpjConve() {
		return cdCpfCnpjConve;
	}

	/**
	 * Set: cdCpfCnpjConve.
	 *
	 * @param cdCpfCnpjConve the cd cpf cnpj conve
	 */
	public void setCdCpfCnpjConve(Long cdCpfCnpjConve) {
		this.cdCpfCnpjConve = cdCpfCnpjConve;
	}

	/**
	 * Get: cdFilialCnpjConve.
	 *
	 * @return cdFilialCnpjConve
	 */
	public Integer getCdFilialCnpjConve() {
		return cdFilialCnpjConve;
	}

	/**
	 * Set: cdFilialCnpjConve.
	 *
	 * @param cdFilialCnpjConve the cd filial cnpj conve
	 */
	public void setCdFilialCnpjConve(Integer cdFilialCnpjConve) {
		this.cdFilialCnpjConve = cdFilialCnpjConve;
	}

	/**
	 * Get: cdCtrlCnpjEmp.
	 *
	 * @return cdCtrlCnpjEmp
	 */
	public Integer getCdCtrlCnpjEmp() {
		return cdCtrlCnpjEmp;
	}

	/**
	 * Set: cdCtrlCnpjEmp.
	 *
	 * @param cdCtrlCnpjEmp the cd ctrl cnpj emp
	 */
	public void setCdCtrlCnpjEmp(Integer cdCtrlCnpjEmp) {
		this.cdCtrlCnpjEmp = cdCtrlCnpjEmp;
	}

	/**
	 * Get: dsSitConve.
	 *
	 * @return dsSitConve
	 */
	public String getDsSitConve() {
		return dsSitConve;
	}

	/**
	 * Set: dsSitConve.
	 *
	 * @param dsSitConve the ds sit conve
	 */
	public void setDsSitConve(String dsSitConve) {
		this.dsSitConve = dsSitConve;
	}
}
