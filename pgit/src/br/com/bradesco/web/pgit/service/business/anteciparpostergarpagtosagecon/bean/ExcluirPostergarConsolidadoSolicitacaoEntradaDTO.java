package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 23/05/17.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirPostergarConsolidadoSolicitacaoEntradaDTO {

    /** The cd solicitacao pagamento integrado. */
    private Integer cdSolicitacaoPagamentoIntegrado;

    /** The nr solicitacao pagamento integrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;

    /**
     * Set cd solicitacao pagamento integrado.
     *
     * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
     */
    public void setCdSolicitacaoPagamentoIntegrado(Integer cdSolicitacaoPagamentoIntegrado) {
        this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get cd solicitacao pagamento integrado.
     *
     * @return the cd solicitacao pagamento integrado
     */
    public Integer getCdSolicitacaoPagamentoIntegrado() {
        return this.cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set nr solicitacao pagamento integrado.
     *
     * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
     */
    public void setNrSolicitacaoPagamentoIntegrado(Integer nrSolicitacaoPagamentoIntegrado) {
        this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get nr solicitacao pagamento integrado.
     *
     * @return the nr solicitacao pagamento integrado
     */
    public Integer getNrSolicitacaoPagamentoIntegrado() {
        return this.nrSolicitacaoPagamentoIntegrado;
    }
}