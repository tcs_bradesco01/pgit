/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nmControleRastreabilidadeTitulo
     */
    private int _nmControleRastreabilidadeTitulo = 0;

    /**
     * keeps track of state for field:
     * _nmControleRastreabilidadeTitulo
     */
    private boolean _has_nmControleRastreabilidadeTitulo;

    /**
     * Field _cdCpfCnpjBloqueio
     */
    private long _cdCpfCnpjBloqueio = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjBloqueio
     */
    private boolean _has_cdCpfCnpjBloqueio;

    /**
     * Field _cdFilialCnpjBloqueio
     */
    private int _cdFilialCnpjBloqueio = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjBloqueio
     */
    private boolean _has_cdFilialCnpjBloqueio;

    /**
     * Field _cdControleCnpjCpf
     */
    private int _cdControleCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdControleCnpjCpf
     */
    private boolean _has_cdControleCnpjCpf;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCnpjCpf
     * 
     */
    public void deleteCdControleCnpjCpf()
    {
        this._has_cdControleCnpjCpf= false;
    } //-- void deleteCdControleCnpjCpf() 

    /**
     * Method deleteCdCpfCnpjBloqueio
     * 
     */
    public void deleteCdCpfCnpjBloqueio()
    {
        this._has_cdCpfCnpjBloqueio= false;
    } //-- void deleteCdCpfCnpjBloqueio() 

    /**
     * Method deleteCdFilialCnpjBloqueio
     * 
     */
    public void deleteCdFilialCnpjBloqueio()
    {
        this._has_cdFilialCnpjBloqueio= false;
    } //-- void deleteCdFilialCnpjBloqueio() 

    /**
     * Method deleteNmControleRastreabilidadeTitulo
     * 
     */
    public void deleteNmControleRastreabilidadeTitulo()
    {
        this._has_nmControleRastreabilidadeTitulo= false;
    } //-- void deleteNmControleRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'cdControleCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjCpf'.
     */
    public int getCdControleCnpjCpf()
    {
        return this._cdControleCnpjCpf;
    } //-- int getCdControleCnpjCpf() 

    /**
     * Returns the value of field 'cdCpfCnpjBloqueio'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjBloqueio'.
     */
    public long getCdCpfCnpjBloqueio()
    {
        return this._cdCpfCnpjBloqueio;
    } //-- long getCdCpfCnpjBloqueio() 

    /**
     * Returns the value of field 'cdFilialCnpjBloqueio'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjBloqueio'.
     */
    public int getCdFilialCnpjBloqueio()
    {
        return this._cdFilialCnpjBloqueio;
    } //-- int getCdFilialCnpjBloqueio() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field
     * 'nmControleRastreabilidadeTitulo'.
     * 
     * @return int
     * @return the value of field 'nmControleRastreabilidadeTitulo'.
     */
    public int getNmControleRastreabilidadeTitulo()
    {
        return this._nmControleRastreabilidadeTitulo;
    } //-- int getNmControleRastreabilidadeTitulo() 

    /**
     * Method hasCdControleCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjCpf()
    {
        return this._has_cdControleCnpjCpf;
    } //-- boolean hasCdControleCnpjCpf() 

    /**
     * Method hasCdCpfCnpjBloqueio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjBloqueio()
    {
        return this._has_cdCpfCnpjBloqueio;
    } //-- boolean hasCdCpfCnpjBloqueio() 

    /**
     * Method hasCdFilialCnpjBloqueio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjBloqueio()
    {
        return this._has_cdFilialCnpjBloqueio;
    } //-- boolean hasCdFilialCnpjBloqueio() 

    /**
     * Method hasNmControleRastreabilidadeTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNmControleRastreabilidadeTitulo()
    {
        return this._has_nmControleRastreabilidadeTitulo;
    } //-- boolean hasNmControleRastreabilidadeTitulo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCnpjCpf'.
     * 
     * @param cdControleCnpjCpf the value of field
     * 'cdControleCnpjCpf'.
     */
    public void setCdControleCnpjCpf(int cdControleCnpjCpf)
    {
        this._cdControleCnpjCpf = cdControleCnpjCpf;
        this._has_cdControleCnpjCpf = true;
    } //-- void setCdControleCnpjCpf(int) 

    /**
     * Sets the value of field 'cdCpfCnpjBloqueio'.
     * 
     * @param cdCpfCnpjBloqueio the value of field
     * 'cdCpfCnpjBloqueio'.
     */
    public void setCdCpfCnpjBloqueio(long cdCpfCnpjBloqueio)
    {
        this._cdCpfCnpjBloqueio = cdCpfCnpjBloqueio;
        this._has_cdCpfCnpjBloqueio = true;
    } //-- void setCdCpfCnpjBloqueio(long) 

    /**
     * Sets the value of field 'cdFilialCnpjBloqueio'.
     * 
     * @param cdFilialCnpjBloqueio the value of field
     * 'cdFilialCnpjBloqueio'.
     */
    public void setCdFilialCnpjBloqueio(int cdFilialCnpjBloqueio)
    {
        this._cdFilialCnpjBloqueio = cdFilialCnpjBloqueio;
        this._has_cdFilialCnpjBloqueio = true;
    } //-- void setCdFilialCnpjBloqueio(int) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'nmControleRastreabilidadeTitulo'.
     * 
     * @param nmControleRastreabilidadeTitulo the value of field
     * 'nmControleRastreabilidadeTitulo'.
     */
    public void setNmControleRastreabilidadeTitulo(int nmControleRastreabilidadeTitulo)
    {
        this._nmControleRastreabilidadeTitulo = nmControleRastreabilidadeTitulo;
        this._has_nmControleRastreabilidadeTitulo = true;
    } //-- void setNmControleRastreabilidadeTitulo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarbloqueiorastreamento.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
