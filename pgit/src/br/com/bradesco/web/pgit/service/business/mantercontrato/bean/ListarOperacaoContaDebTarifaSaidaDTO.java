package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.List;

public class ListarOperacaoContaDebTarifaSaidaDTO {

    private String codMensagem;
    private String mensagem;
    private String cdLayout;
    private Integer tamanhoLayout;
    private Integer numeroLinhas;
    private List<ListarOperacaoContaDebTarifaOcorrencias> ocorrencias;
    
	public String getCodMensagem() {
		return codMensagem;
	}
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getCdLayout() {
		return cdLayout;
	}
	public void setCdLayout(String cdLayout) {
		this.cdLayout = cdLayout;
	}
	public Integer getTamanhoLayout() {
		return tamanhoLayout;
	}
	public void setTamanhoLayout(Integer tamanhoLayout) {
		this.tamanhoLayout = tamanhoLayout;
	}
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
	public List<ListarOperacaoContaDebTarifaOcorrencias> getOcorrencias() {
		return ocorrencias;
	}
	public void setOcorrencias(
			List<ListarOperacaoContaDebTarifaOcorrencias> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
}
