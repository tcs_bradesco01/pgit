/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarservicoscontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoServico
     */
    private int _cdProdutoServico = 0;

    /**
     * keeps track of state for field: _cdProdutoServico
     */
    private boolean _has_cdProdutoServico;

    /**
     * Field _dsProdutoServico
     */
    private java.lang.String _dsProdutoServico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarservicoscontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProdutoServico
     * 
     */
    public void deleteCdProdutoServico()
    {
        this._has_cdProdutoServico= false;
    } //-- void deleteCdProdutoServico() 

    /**
     * Returns the value of field 'cdProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServico'.
     */
    public int getCdProdutoServico()
    {
        return this._cdProdutoServico;
    } //-- int getCdProdutoServico() 

    /**
     * Returns the value of field 'dsProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServico'.
     */
    public java.lang.String getDsProdutoServico()
    {
        return this._dsProdutoServico;
    } //-- java.lang.String getDsProdutoServico() 

    /**
     * Method hasCdProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServico()
    {
        return this._has_cdProdutoServico;
    } //-- boolean hasCdProdutoServico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProdutoServico'.
     * 
     * @param cdProdutoServico the value of field 'cdProdutoServico'
     */
    public void setCdProdutoServico(int cdProdutoServico)
    {
        this._cdProdutoServico = cdProdutoServico;
        this._has_cdProdutoServico = true;
    } //-- void setCdProdutoServico(int) 

    /**
     * Sets the value of field 'dsProdutoServico'.
     * 
     * @param dsProdutoServico the value of field 'dsProdutoServico'
     */
    public void setDsProdutoServico(java.lang.String dsProdutoServico)
    {
        this._dsProdutoServico = dsProdutoServico;
    } //-- void setDsProdutoServico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarservicoscontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarservicoscontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarservicoscontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarservicoscontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
