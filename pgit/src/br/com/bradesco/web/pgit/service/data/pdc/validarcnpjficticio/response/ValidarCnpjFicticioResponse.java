/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ValidarCnpjFicticioResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ValidarCnpjFicticioResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdContratoPerfil
     */
    private long _cdContratoPerfil = 0;

    /**
     * keeps track of state for field: _cdContratoPerfil
     */
    private boolean _has_cdContratoPerfil;

    /**
     * Field _cdCnpjCpf
     */
    private long _cdCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCnpjCpf
     */
    private boolean _has_cdCnpjCpf;

    /**
     * Field _cdFilialCpfCnpj
     */
    private int _cdFilialCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpj
     */
    private boolean _has_cdFilialCpfCnpj;

    /**
     * Field _cdCtrlCpfCnpj
     */
    private int _cdCtrlCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCtrlCpfCnpj
     */
    private boolean _has_cdCtrlCpfCnpj;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;


      //----------------/
     //- Constructors -/
    //----------------/

    public ValidarCnpjFicticioResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.response.ValidarCnpjFicticioResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjCpf
     * 
     */
    public void deleteCdCnpjCpf()
    {
        this._has_cdCnpjCpf= false;
    } //-- void deleteCdCnpjCpf() 

    /**
     * Method deleteCdContratoPerfil
     * 
     */
    public void deleteCdContratoPerfil()
    {
        this._has_cdContratoPerfil= false;
    } //-- void deleteCdContratoPerfil() 

    /**
     * Method deleteCdCtrlCpfCnpj
     * 
     */
    public void deleteCdCtrlCpfCnpj()
    {
        this._has_cdCtrlCpfCnpj= false;
    } //-- void deleteCdCtrlCpfCnpj() 

    /**
     * Method deleteCdFilialCpfCnpj
     * 
     */
    public void deleteCdFilialCpfCnpj()
    {
        this._has_cdFilialCpfCnpj= false;
    } //-- void deleteCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'cdCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCnpjCpf'.
     */
    public long getCdCnpjCpf()
    {
        return this._cdCnpjCpf;
    } //-- long getCdCnpjCpf() 

    /**
     * Returns the value of field 'cdContratoPerfil'.
     * 
     * @return long
     * @return the value of field 'cdContratoPerfil'.
     */
    public long getCdContratoPerfil()
    {
        return this._cdContratoPerfil;
    } //-- long getCdContratoPerfil() 

    /**
     * Returns the value of field 'cdCtrlCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCpfCnpj'.
     */
    public int getCdCtrlCpfCnpj()
    {
        return this._cdCtrlCpfCnpj;
    } //-- int getCdCtrlCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpj'.
     */
    public int getCdFilialCpfCnpj()
    {
        return this._cdFilialCpfCnpj;
    } //-- int getCdFilialCpfCnpj() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpf()
    {
        return this._has_cdCnpjCpf;
    } //-- boolean hasCdCnpjCpf() 

    /**
     * Method hasCdContratoPerfil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContratoPerfil()
    {
        return this._has_cdContratoPerfil;
    } //-- boolean hasCdContratoPerfil() 

    /**
     * Method hasCdCtrlCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCpfCnpj()
    {
        return this._has_cdCtrlCpfCnpj;
    } //-- boolean hasCdCtrlCpfCnpj() 

    /**
     * Method hasCdFilialCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpj()
    {
        return this._has_cdFilialCpfCnpj;
    } //-- boolean hasCdFilialCpfCnpj() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjCpf'.
     * 
     * @param cdCnpjCpf the value of field 'cdCnpjCpf'.
     */
    public void setCdCnpjCpf(long cdCnpjCpf)
    {
        this._cdCnpjCpf = cdCnpjCpf;
        this._has_cdCnpjCpf = true;
    } //-- void setCdCnpjCpf(long) 

    /**
     * Sets the value of field 'cdContratoPerfil'.
     * 
     * @param cdContratoPerfil the value of field 'cdContratoPerfil'
     */
    public void setCdContratoPerfil(long cdContratoPerfil)
    {
        this._cdContratoPerfil = cdContratoPerfil;
        this._has_cdContratoPerfil = true;
    } //-- void setCdContratoPerfil(long) 

    /**
     * Sets the value of field 'cdCtrlCpfCnpj'.
     * 
     * @param cdCtrlCpfCnpj the value of field 'cdCtrlCpfCnpj'.
     */
    public void setCdCtrlCpfCnpj(int cdCtrlCpfCnpj)
    {
        this._cdCtrlCpfCnpj = cdCtrlCpfCnpj;
        this._has_cdCtrlCpfCnpj = true;
    } //-- void setCdCtrlCpfCnpj(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpj'.
     * 
     * @param cdFilialCpfCnpj the value of field 'cdFilialCpfCnpj'.
     */
    public void setCdFilialCpfCnpj(int cdFilialCpfCnpj)
    {
        this._cdFilialCpfCnpj = cdFilialCpfCnpj;
        this._has_cdFilialCpfCnpj = true;
    } //-- void setCdFilialCpfCnpj(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ValidarCnpjFicticioResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.response.ValidarCnpjFicticioResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.response.ValidarCnpjFicticioResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.response.ValidarCnpjFicticioResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.validarcnpjficticio.response.ValidarCnpjFicticioResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
