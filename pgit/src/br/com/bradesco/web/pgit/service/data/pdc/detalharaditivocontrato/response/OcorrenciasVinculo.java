/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class OcorrenciasVinculo.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasVinculo implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaContratoOrigem
     */
    private long _cdPessoaContratoOrigem = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoOrigem
     */
    private boolean _has_cdPessoaContratoOrigem;

    /**
     * Field _cdTipoContratoOrigem
     */
    private int _cdTipoContratoOrigem = 0;

    /**
     * keeps track of state for field: _cdTipoContratoOrigem
     */
    private boolean _has_cdTipoContratoOrigem;

    /**
     * Field _nrSequenciaContratoOrigem
     */
    private long _nrSequenciaContratoOrigem = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoOrigem
     */
    private boolean _has_nrSequenciaContratoOrigem;

    /**
     * Field _cdPessoaJuridicaVinculo
     */
    private long _cdPessoaJuridicaVinculo = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaVinculo
     */
    private boolean _has_cdPessoaJuridicaVinculo;

    /**
     * Field _cdTipoContratoVinculo
     */
    private int _cdTipoContratoVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinculo
     */
    private boolean _has_cdTipoContratoVinculo;

    /**
     * Field _nrSequenciaContratoVinculo
     */
    private long _nrSequenciaContratoVinculo = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoVinculo
     */
    private boolean _has_nrSequenciaContratoVinculo;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _dsAcaoManutencao
     */
    private java.lang.String _dsAcaoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasVinculo() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculo()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaContratoOrigem
     * 
     */
    public void deleteCdPessoaContratoOrigem()
    {
        this._has_cdPessoaContratoOrigem= false;
    } //-- void deleteCdPessoaContratoOrigem() 

    /**
     * Method deleteCdPessoaJuridicaVinculo
     * 
     */
    public void deleteCdPessoaJuridicaVinculo()
    {
        this._has_cdPessoaJuridicaVinculo= false;
    } //-- void deleteCdPessoaJuridicaVinculo() 

    /**
     * Method deleteCdTipoContratoOrigem
     * 
     */
    public void deleteCdTipoContratoOrigem()
    {
        this._has_cdTipoContratoOrigem= false;
    } //-- void deleteCdTipoContratoOrigem() 

    /**
     * Method deleteCdTipoContratoVinculo
     * 
     */
    public void deleteCdTipoContratoVinculo()
    {
        this._has_cdTipoContratoVinculo= false;
    } //-- void deleteCdTipoContratoVinculo() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSequenciaContratoOrigem
     * 
     */
    public void deleteNrSequenciaContratoOrigem()
    {
        this._has_nrSequenciaContratoOrigem= false;
    } //-- void deleteNrSequenciaContratoOrigem() 

    /**
     * Method deleteNrSequenciaContratoVinculo
     * 
     */
    public void deleteNrSequenciaContratoVinculo()
    {
        this._has_nrSequenciaContratoVinculo= false;
    } //-- void deleteNrSequenciaContratoVinculo() 

    /**
     * Returns the value of field 'cdPessoaContratoOrigem'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoOrigem'.
     */
    public long getCdPessoaContratoOrigem()
    {
        return this._cdPessoaContratoOrigem;
    } //-- long getCdPessoaContratoOrigem() 

    /**
     * Returns the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaVinculo'.
     */
    public long getCdPessoaJuridicaVinculo()
    {
        return this._cdPessoaJuridicaVinculo;
    } //-- long getCdPessoaJuridicaVinculo() 

    /**
     * Returns the value of field 'cdTipoContratoOrigem'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoOrigem'.
     */
    public int getCdTipoContratoOrigem()
    {
        return this._cdTipoContratoOrigem;
    } //-- int getCdTipoContratoOrigem() 

    /**
     * Returns the value of field 'cdTipoContratoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinculo'.
     */
    public int getCdTipoContratoVinculo()
    {
        return this._cdTipoContratoVinculo;
    } //-- int getCdTipoContratoVinculo() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'dsAcaoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsAcaoManutencao'.
     */
    public java.lang.String getDsAcaoManutencao()
    {
        return this._dsAcaoManutencao;
    } //-- java.lang.String getDsAcaoManutencao() 

    /**
     * Returns the value of field 'nrSequenciaContratoOrigem'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoOrigem'.
     */
    public long getNrSequenciaContratoOrigem()
    {
        return this._nrSequenciaContratoOrigem;
    } //-- long getNrSequenciaContratoOrigem() 

    /**
     * Returns the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoVinculo'.
     */
    public long getNrSequenciaContratoVinculo()
    {
        return this._nrSequenciaContratoVinculo;
    } //-- long getNrSequenciaContratoVinculo() 

    /**
     * Method hasCdPessoaContratoOrigem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoOrigem()
    {
        return this._has_cdPessoaContratoOrigem;
    } //-- boolean hasCdPessoaContratoOrigem() 

    /**
     * Method hasCdPessoaJuridicaVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaVinculo()
    {
        return this._has_cdPessoaJuridicaVinculo;
    } //-- boolean hasCdPessoaJuridicaVinculo() 

    /**
     * Method hasCdTipoContratoOrigem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoOrigem()
    {
        return this._has_cdTipoContratoOrigem;
    } //-- boolean hasCdTipoContratoOrigem() 

    /**
     * Method hasCdTipoContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinculo()
    {
        return this._has_cdTipoContratoVinculo;
    } //-- boolean hasCdTipoContratoVinculo() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSequenciaContratoOrigem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoOrigem()
    {
        return this._has_nrSequenciaContratoOrigem;
    } //-- boolean hasNrSequenciaContratoOrigem() 

    /**
     * Method hasNrSequenciaContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoVinculo()
    {
        return this._has_nrSequenciaContratoVinculo;
    } //-- boolean hasNrSequenciaContratoVinculo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaContratoOrigem'.
     * 
     * @param cdPessoaContratoOrigem the value of field
     * 'cdPessoaContratoOrigem'.
     */
    public void setCdPessoaContratoOrigem(long cdPessoaContratoOrigem)
    {
        this._cdPessoaContratoOrigem = cdPessoaContratoOrigem;
        this._has_cdPessoaContratoOrigem = true;
    } //-- void setCdPessoaContratoOrigem(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @param cdPessoaJuridicaVinculo the value of field
     * 'cdPessoaJuridicaVinculo'.
     */
    public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo)
    {
        this._cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
        this._has_cdPessoaJuridicaVinculo = true;
    } //-- void setCdPessoaJuridicaVinculo(long) 

    /**
     * Sets the value of field 'cdTipoContratoOrigem'.
     * 
     * @param cdTipoContratoOrigem the value of field
     * 'cdTipoContratoOrigem'.
     */
    public void setCdTipoContratoOrigem(int cdTipoContratoOrigem)
    {
        this._cdTipoContratoOrigem = cdTipoContratoOrigem;
        this._has_cdTipoContratoOrigem = true;
    } //-- void setCdTipoContratoOrigem(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinculo'.
     * 
     * @param cdTipoContratoVinculo the value of field
     * 'cdTipoContratoVinculo'.
     */
    public void setCdTipoContratoVinculo(int cdTipoContratoVinculo)
    {
        this._cdTipoContratoVinculo = cdTipoContratoVinculo;
        this._has_cdTipoContratoVinculo = true;
    } //-- void setCdTipoContratoVinculo(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'dsAcaoManutencao'.
     * 
     * @param dsAcaoManutencao the value of field 'dsAcaoManutencao'
     */
    public void setDsAcaoManutencao(java.lang.String dsAcaoManutencao)
    {
        this._dsAcaoManutencao = dsAcaoManutencao;
    } //-- void setDsAcaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoOrigem'.
     * 
     * @param nrSequenciaContratoOrigem the value of field
     * 'nrSequenciaContratoOrigem'.
     */
    public void setNrSequenciaContratoOrigem(long nrSequenciaContratoOrigem)
    {
        this._nrSequenciaContratoOrigem = nrSequenciaContratoOrigem;
        this._has_nrSequenciaContratoOrigem = true;
    } //-- void setNrSequenciaContratoOrigem(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @param nrSequenciaContratoVinculo the value of field
     * 'nrSequenciaContratoVinculo'.
     */
    public void setNrSequenciaContratoVinculo(long nrSequenciaContratoVinculo)
    {
        this._nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
        this._has_nrSequenciaContratoVinculo = true;
    } //-- void setNrSequenciaContratoVinculo(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasVinculo
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculo unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculo) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculo.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharaditivocontrato.response.OcorrenciasVinculo unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
