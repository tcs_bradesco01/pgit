/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.impl;

import br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.IRegistrarAssinaturaContratoService;
import br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.bean.RegistrarAssinaturaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.bean.RegistrarAssinaturaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.registrarassinaturacontrato.request.RegistrarAssinaturaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.registrarassinaturacontrato.response.RegistrarAssinaturaContratoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: RegistrarAssinaturaContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class RegistrarAssinaturaContratoServiceImpl implements IRegistrarAssinaturaContratoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.IRegistrarAssinaturaContratoService#registrarAssinaturaContratoSaidaDTO(br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.bean.RegistrarAssinaturaContratoEntradaDTO)
	 */
	public RegistrarAssinaturaContratoSaidaDTO registrarAssinaturaContratoSaidaDTO(RegistrarAssinaturaContratoEntradaDTO registrarAssinaturaContratoEntradaDTO) {
		
		RegistrarAssinaturaContratoSaidaDTO registrarAssinaturaContratoSaidaDTO = new RegistrarAssinaturaContratoSaidaDTO();
		
		RegistrarAssinaturaContratoRequest request = new RegistrarAssinaturaContratoRequest();
		RegistrarAssinaturaContratoResponse response = new RegistrarAssinaturaContratoResponse();
		
		
		request.setCdPessoaJuridicaContrato(registrarAssinaturaContratoEntradaDTO.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(registrarAssinaturaContratoEntradaDTO.getCdTipoContratoNegocio());
		request.setDtAssinaturaContrato(registrarAssinaturaContratoEntradaDTO.getDtAssinaturaAditivo());
		request.setNrSequenciaContratoNegocio(Long.parseLong(registrarAssinaturaContratoEntradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdContratoNegocioPagamento(registrarAssinaturaContratoEntradaDTO.getNumContratoFisico());
		
	
		response = getFactoryAdapter().getRegistrarAssinaturaContratoPDCAdapter().invokeProcess(request);
		
		registrarAssinaturaContratoSaidaDTO.setCodMensagem(response.getCodMensagem());
		registrarAssinaturaContratoSaidaDTO.setMensagem(response.getMensagem());
		
		
		return registrarAssinaturaContratoSaidaDTO;
	}
    
}

