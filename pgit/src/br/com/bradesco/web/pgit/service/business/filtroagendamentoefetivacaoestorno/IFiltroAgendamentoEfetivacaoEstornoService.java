/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarClientesMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarClientesMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratoMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratoMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratosClienteMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratosClienteMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarPerfilTrocaArquivoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: FiltroAgendamentoEfetivacaoEstorno
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IFiltroAgendamentoEfetivacaoEstornoService {
	
	/**
	 * Listar clientes marq.
	 *
	 * @param entrada the entrada
	 * @return the list< listar clientes marq saida dt o>
	 */
	List<ListarClientesMarqSaidaDTO> listarClientesMarq(ListarClientesMarqEntradaDTO entrada);	
	
	/**
	 * Listar contratos cliente marq.
	 *
	 * @param entrada the entrada
	 * @return the list< listar contratos cliente marq saida dt o>
	 */
	List<ListarContratosClienteMarqSaidaDTO> listarContratosClienteMarq(ListarContratosClienteMarqEntradaDTO entrada);	
	
	/**
	 * Listar contrato marq.
	 *
	 * @param entrada the entrada
	 * @return the list< listar contrato marq saida dt o>
	 */
	List<ListarContratoMarqSaidaDTO> listarContratoMarq(ListarContratoMarqEntradaDTO entrada);
	
	/**
	 * Listar perfil troca arquivo.
	 *
	 * @param entrada the entrada
	 * @return the list< listar perfil troca arquivo saida dt o>
	 */
	List<ListarPerfilTrocaArquivoSaidaDTO> listarPerfilTrocaArquivo(ListarPerfilTrocaArquivoEntradaDTO entrada);
	
	/**
	 * Consultar contrato cliente filtro.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar contrato cliente filtro saida dt o>
	 */
	List<ConsultarContratoClienteFiltroSaidaDTO> consultarContratoClienteFiltro(ConsultarContratoClienteFiltroEntradaDTO entrada);	
}

