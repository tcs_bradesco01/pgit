/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: AlterarConfiguracaoLayoutArqContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConfiguracaoLayoutArquivoContratoEntradaDto {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato = null;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio = null;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio = null;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo = null;
	
	/** Atributo cdSituacaoLayoutContrato. */
	private Integer cdSituacaoLayoutContrato = null;
	
	/** Atributo cdResponsavelCustoEmpresa. */
	private Integer cdResponsavelCustoEmpresa = null;
	
	/** Atributo percentualCustoOrganizacaoTransmissao. */
	private BigDecimal percentualCustoOrganizacaoTransmissao = null;
	
	/** Atributo cdMensagemLinhaExtrato. */
	private Integer cdMensagemLinhaExtrato = null;

	/** Atributo numeroVersaoLayoutArquivo. */
	private Integer numeroVersaoLayoutArquivo = null;
	
	/** Atributo cdTipoControlePagamento. */
	private Integer cdTipoControlePagamento = null;
	
	/**
	 * 
	 */
	public ConfiguracaoLayoutArquivoContratoEntradaDto() {
		super();
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdResponsavelCustoEmpresa.
	 *
	 * @return cdResponsavelCustoEmpresa
	 */
	public Integer getCdResponsavelCustoEmpresa() {
		return cdResponsavelCustoEmpresa;
	}
	
	/**
	 * Set: cdResponsavelCustoEmpresa.
	 *
	 * @param cdResponsavelCustoEmpresa the cd responsavel custo empresa
	 */
	public void setCdResponsavelCustoEmpresa(Integer cdResponsavelCustoEmpresa) {
		this.cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
	}
	
	/**
	 * Get: cdSituacaoLayoutContrato.
	 *
	 * @return cdSituacaoLayoutContrato
	 */
	public Integer getCdSituacaoLayoutContrato() {
		return cdSituacaoLayoutContrato;
	}
	
	/**
	 * Set: cdSituacaoLayoutContrato.
	 *
	 * @param cdSituacaoLayoutContrato the cd situacao layout contrato
	 */
	public void setCdSituacaoLayoutContrato(Integer cdSituacaoLayoutContrato) {
		this.cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
	}
	
	/**
	 * Get: percentualCustoOrganizacaoTransmissao.
	 *
	 * @return percentualCustoOrganizacaoTransmissao
	 */
	public BigDecimal getPercentualCustoOrganizacaoTransmissao() {
		return percentualCustoOrganizacaoTransmissao;
	}
	
	/**
	 * Set: percentualCustoOrganizacaoTransmissao.
	 *
	 * @param percentualCustoOrganizacaoTransmissao the percentual custo organizacao transmissao
	 */
	public void setPercentualCustoOrganizacaoTransmissao(
			BigDecimal percentualCustoOrganizacaoTransmissao) {
		this.percentualCustoOrganizacaoTransmissao = percentualCustoOrganizacaoTransmissao;
	}
	
	/**
	 * Get: cdMensagemLinhaExtrato.
	 *
	 * @return cdMensagemLinhaExtrato
	 */
	public Integer getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}
	
	/**
	 * Set: cdMensagemLinhaExtrato.
	 *
	 * @param cdMensagemLinhaExtrato the cd mensagem linha extrato
	 */
	public void setCdMensagemLinhaExtrato(Integer cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}

	/**
	 * Nome: setNumeroVersaoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @param numeroVersaoLayoutArquivo
	 */
	public void setNumeroVersaoLayoutArquivo(Integer numeroVersaoLayoutArquivo) {
		this.numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
	}

	/**
	 * Nome: getNumeroVersaoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @return numeroVersaoLayoutArquivo
	 */
	public Integer getNumeroVersaoLayoutArquivo() {
		return numeroVersaoLayoutArquivo;
	}

	public Integer getCdTipoControlePagamento() {
		return cdTipoControlePagamento;
	}

	public void setCdTipoControlePagamento(Integer cdTipoControlePagamento) {
		this.cdTipoControlePagamento = cdTipoControlePagamento;
	}
}
