/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarPagtoSolEmiCompFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarPagtoSolEmiCompFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ExcluirSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ExcluirSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.IncluirSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.IncluirSolEmiComprovanteFavorecidoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: SolEmissaoComprovantePagtoFavorecido
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolEmissaoComprovantePagtoFavorecidoService {
	
	/**
	 * Consultar sol emi comprovante favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar sol emi comprovante favorecido saida dt o>
	 */
	List<ConsultarSolEmiComprovanteFavorecidoSaidaDTO> consultarSolEmiComprovanteFavorecido(ConsultarSolEmiComprovanteFavorecidoEntradaDTO entradaDTO);
	
	/**
	 * Excluir sol emi comprovante favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir sol emi comprovante favorecido saida dto
	 */
	ExcluirSolEmiComprovanteFavorecidoSaidaDTO excluirSolEmiComprovanteFavorecido(ExcluirSolEmiComprovanteFavorecidoEntradaDTO entradaDTO);
	
	/**
	 * Incluir sol emi comprovante favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the incluir sol emi comprovante favorecido saida dto
	 */
	IncluirSolEmiComprovanteFavorecidoSaidaDTO incluirSolEmiComprovanteFavorecido(IncluirSolEmiComprovanteFavorecidoEntradaDTO entradaDTO);
	
	/**
	 * Detalhar sol emi comprovante favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar sol emi comprovante favorecido saida dto
	 */
	DetalharSolEmiComprovanteFavorecidoSaidaDTO detalharSolEmiComprovanteFavorecido(DetalharSolEmiComprovanteFavorecidoEntradaDTO entradaDTO);
	
	/**
	 * Consultar pagto sol emi comp favorecido.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar pagto sol emi comp favorecido saida dt o>
	 */
	List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> consultarPagtoSolEmiCompFavorecido(ConsultarPagtoSolEmiCompFavorecidoEntradaDTO entradaDTO);
}

