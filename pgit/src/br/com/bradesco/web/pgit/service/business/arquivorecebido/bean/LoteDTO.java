/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.bean;

/**
 * Nome: LoteDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LoteDTO {

	/** Atributo seqLote. */
	private String seqLote;
	
	/** Atributo bcoAgeCtaDebito. */
	private String bcoAgeCtaDebito;
	
	/** Atributo modalidadePagto. */
	private String modalidadePagto;
	
	/** Atributo descricaoModalidadePagto. */
	private String descricaoModalidadePagto;
	
	/** Atributo qtdeRegistros. */
	private String qtdeRegistros;
	
	/** Atributo valorTotal. */
	private String valorTotal;
	
	/** Atributo situacaoProcessamento. */
	private String situacaoProcessamento;
	
	/** Atributo descricaoSituacaoProcessamento. */
	private String descricaoSituacaoProcessamento;
	
	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;
	
	/** Atributo descricaoResultadoProcessamento. */
	private String descricaoResultadoProcessamento;	
	
	/** Atributo labelDescrSitProcessamento. */
	private String labelDescrSitProcessamento;
	
	/** Atributo labelDescrResultadoProcessamento. */
	private String labelDescrResultadoProcessamento;	
	
	/** Atributo sistemaLegado. */
	private String sistemaLegado;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo filialCnpj. */
	private String filialCnpj;
	
	/** Atributo controleCpfCnpj. */
	private String controleCpfCnpj;
	
	/** Atributo perfil. */
	private String perfil;
	
	/** Atributo numeroArquivoRemessa. */
	private String numeroArquivoRemessa;
	
	/** Atributo numeroLote. */
	private String numeroLote;
	
	/** Atributo sequenciaRemessa. */
	private String sequenciaRemessa;
	
	/** Atributo tipoPesquisa. */
	private String tipoPesquisa;
	
	/** Atributo recurso. */
	private String recurso;
	
	/** Atributo filler. */
	private String filler;
	
	/** Atributo sequencia. */
	private int sequencia;
	
	/** Atributo dataInclusaoRemessa. */
	private String dataInclusaoRemessa;
	
	/**
	 * Get: bcoAgeCtaDebito.
	 *
	 * @return bcoAgeCtaDebito
	 */
	public String getBcoAgeCtaDebito() {
		return bcoAgeCtaDebito;
	}
	
	/**
	 * Set: bcoAgeCtaDebito.
	 *
	 * @param bcoAgeCtaDebito the bco age cta debito
	 */
	public void setBcoAgeCtaDebito(String bcoAgeCtaDebito) {
		this.bcoAgeCtaDebito = bcoAgeCtaDebito;
	}
	
	/**
	 * Get: modalidadePagto.
	 *
	 * @return modalidadePagto
	 */
	public String getModalidadePagto() {
		return modalidadePagto;
	}
	
	/**
	 * Set: modalidadePagto.
	 *
	 * @param modalidadePagto the modalidade pagto
	 */
	public void setModalidadePagto(String modalidadePagto) {
		this.modalidadePagto = modalidadePagto;
	}
	
	/**
	 * Get: qtdeRegistros.
	 *
	 * @return qtdeRegistros
	 */
	public String getQtdeRegistros() {
		return qtdeRegistros;
	}
	
	/**
	 * Set: qtdeRegistros.
	 *
	 * @param qtdeRegistros the qtde registros
	 */
	public void setQtdeRegistros(String qtdeRegistros) {
		this.qtdeRegistros = qtdeRegistros;
	}
	
	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}
	
	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}
	
	/**
	 * Get: seqLote.
	 *
	 * @return seqLote
	 */
	public String getSeqLote() {
		return seqLote;
	}
	
	/**
	 * Set: seqLote.
	 *
	 * @param seqLote the seq lote
	 */
	public void setSeqLote(String seqLote) {
		this.seqLote = seqLote;
	}
	
	/**
	 * Get: situacaoProcessamento.
	 *
	 * @return situacaoProcessamento
	 */
	public String getSituacaoProcessamento() {
		return situacaoProcessamento;
	}
	
	/**
	 * Set: situacaoProcessamento.
	 *
	 * @param situacaoProcessamento the situacao processamento
	 */
	public void setSituacaoProcessamento(String situacaoProcessamento) {
		this.situacaoProcessamento = situacaoProcessamento;
	}
	
	/**
	 * Get: valorTotal.
	 *
	 * @return valorTotal
	 */
	public String getValorTotal() {
		return valorTotal;
	}
	
	/**
	 * Set: valorTotal.
	 *
	 * @param valorTotal the valor total
	 */
	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	/**
	 * Get: controleCpfCnpj.
	 *
	 * @return controleCpfCnpj
	 */
	public String getControleCpfCnpj() {
		return controleCpfCnpj;
	}
	
	/**
	 * Set: controleCpfCnpj.
	 *
	 * @param controleCpfCnpj the controle cpf cnpj
	 */
	public void setControleCpfCnpj(String controleCpfCnpj) {
		this.controleCpfCnpj = controleCpfCnpj;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: filialCnpj.
	 *
	 * @return filialCnpj
	 */
	public String getFilialCnpj() {
		return filialCnpj;
	}
	
	/**
	 * Set: filialCnpj.
	 *
	 * @param filialCnpj the filial cnpj
	 */
	public void setFilialCnpj(String filialCnpj) {
		this.filialCnpj = filialCnpj;
	}
	
	/**
	 * Get: filler.
	 *
	 * @return filler
	 */
	public String getFiller() {
		return filler;
	}
	
	/**
	 * Set: filler.
	 *
	 * @param filler the filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
	/**
	 * Get: numeroArquivoRemessa.
	 *
	 * @return numeroArquivoRemessa
	 */
	public String getNumeroArquivoRemessa() {
		return numeroArquivoRemessa;
	}
	
	/**
	 * Set: numeroArquivoRemessa.
	 *
	 * @param numeroArquivoRemessa the numero arquivo remessa
	 */
	public void setNumeroArquivoRemessa(String numeroArquivoRemessa) {
		this.numeroArquivoRemessa = numeroArquivoRemessa;
	}
	
	/**
	 * Get: numeroLote.
	 *
	 * @return numeroLote
	 */
	public String getNumeroLote() {
		return numeroLote;
	}
	
	/**
	 * Set: numeroLote.
	 *
	 * @param numeroLote the numero lote
	 */
	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}
	
	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public String getPerfil() {
		return perfil;
	}
	
	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public String getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}
	
	/**
	 * Get: sequenciaRemessa.
	 *
	 * @return sequenciaRemessa
	 */
	public String getSequenciaRemessa() {
		return sequenciaRemessa;
	}
	
	/**
	 * Set: sequenciaRemessa.
	 *
	 * @param sequenciaRemessa the sequencia remessa
	 */
	public void setSequenciaRemessa(String sequenciaRemessa) {
		this.sequenciaRemessa = sequenciaRemessa;
	}
	
	/**
	 * Get: sistemaLegado.
	 *
	 * @return sistemaLegado
	 */
	public String getSistemaLegado() {
		return sistemaLegado;
	}
	
	/**
	 * Set: sistemaLegado.
	 *
	 * @param sistemaLegado the sistema legado
	 */
	public void setSistemaLegado(String sistemaLegado) {
		this.sistemaLegado = sistemaLegado;
	}
	
	/**
	 * Get: tipoPesquisa.
	 *
	 * @return tipoPesquisa
	 */
	public String getTipoPesquisa() {
		return tipoPesquisa;
	}
	
	/**
	 * Set: tipoPesquisa.
	 *
	 * @param tipoPesquisa the tipo pesquisa
	 */
	public void setTipoPesquisa(String tipoPesquisa) {
		this.tipoPesquisa = tipoPesquisa;
	}
	
	/**
	 * Get: sequencia.
	 *
	 * @return sequencia
	 */
	public int getSequencia() {
		return sequencia;
	}
	
	/**
	 * Set: sequencia.
	 *
	 * @param sequencia the sequencia
	 */
	public void setSequencia(int sequencia) {
		this.sequencia = sequencia;
	}
	
	/**
	 * Get: dataInclusaoRemessa.
	 *
	 * @return dataInclusaoRemessa
	 */
	public String getDataInclusaoRemessa() {
		return dataInclusaoRemessa;
	}
	
	/**
	 * Set: dataInclusaoRemessa.
	 *
	 * @param dataInclusaoRemessa the data inclusao remessa
	 */
	public void setDataInclusaoRemessa(String dataInclusaoRemessa) {
		this.dataInclusaoRemessa = dataInclusaoRemessa;
	}
	
	/**
	 * Get: descricaoModalidadePagto.
	 *
	 * @return descricaoModalidadePagto
	 */
	public String getDescricaoModalidadePagto() {
		return descricaoModalidadePagto;
	}
	
	/**
	 * Set: descricaoModalidadePagto.
	 *
	 * @param descricaoModalidadePagto the descricao modalidade pagto
	 */
	public void setDescricaoModalidadePagto(String descricaoModalidadePagto) {
		this.descricaoModalidadePagto = descricaoModalidadePagto;
	}
	
	/**
	 * Get: descricaoSituacaoProcessamento.
	 *
	 * @return descricaoSituacaoProcessamento
	 */
	public String getDescricaoSituacaoProcessamento() {
		return descricaoSituacaoProcessamento;
	}
	
	/**
	 * Set: descricaoSituacaoProcessamento.
	 *
	 * @param descricaoSituacaoProcessamento the descricao situacao processamento
	 */
	public void setDescricaoSituacaoProcessamento(
			String descricaoSituacaoProcessamento) {
		this.descricaoSituacaoProcessamento = descricaoSituacaoProcessamento;
	}
	
	/**
	 * Get: descricaoResultadoProcessamento.
	 *
	 * @return descricaoResultadoProcessamento
	 */
	public String getDescricaoResultadoProcessamento() {
		return descricaoResultadoProcessamento;
	}
	
	/**
	 * Set: descricaoResultadoProcessamento.
	 *
	 * @param descricaoResultadoProcessamento the descricao resultado processamento
	 */
	public void setDescricaoResultadoProcessamento(
			String descricaoResultadoProcessamento) {
		this.descricaoResultadoProcessamento = descricaoResultadoProcessamento;
	}
	
	/**
	 * Get: labelDescrResultadoProcessamento.
	 *
	 * @return labelDescrResultadoProcessamento
	 */
	public String getLabelDescrResultadoProcessamento() {
		return labelDescrResultadoProcessamento;
	}
	
	/**
	 * Set: labelDescrResultadoProcessamento.
	 *
	 * @param labelDescrResultadoProcessamento the label descr resultado processamento
	 */
	public void setLabelDescrResultadoProcessamento(String labelDescrResultadoProcessamento) {
		this.labelDescrResultadoProcessamento = labelDescrResultadoProcessamento;
	}
	
	/**
	 * Get: labelDescrSitProcessamento.
	 *
	 * @return labelDescrSitProcessamento
	 */
	public String getLabelDescrSitProcessamento() {
		return labelDescrSitProcessamento;
	}
	
	/**
	 * Set: labelDescrSitProcessamento.
	 *
	 * @param labelDescrSitProcessamento the label descr sit processamento
	 */
	public void setLabelDescrSitProcessamento(String labelDescrSitProcessamento) {
		this.labelDescrSitProcessamento = labelDescrSitProcessamento;
	}
	
}
