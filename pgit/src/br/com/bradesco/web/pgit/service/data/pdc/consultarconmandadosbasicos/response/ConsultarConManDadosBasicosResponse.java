/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarConManDadosBasicosResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarConManDadosBasicosResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _hrInclusaoRegistroHistorico
     */
    private java.lang.String _hrInclusaoRegistroHistorico;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _cdMotivoSituacaoContrato
     */
    private int _cdMotivoSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoContrato
     */
    private boolean _has_cdMotivoSituacaoContrato;

    /**
     * Field _cdAcaoRelacionamento
     */
    private long _cdAcaoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdAcaoRelacionamento
     */
    private boolean _has_cdAcaoRelacionamento;

    /**
     * Field _cdContratoNegocio
     */
    private java.lang.String _cdContratoNegocio;

    /**
     * Field _cdOrigemContrato
     */
    private int _cdOrigemContrato = 0;

    /**
     * keeps track of state for field: _cdOrigemContrato
     */
    private boolean _has_cdOrigemContrato;

    /**
     * Field _hrAssinaturaContrato
     */
    private java.lang.String _hrAssinaturaContrato;

    /**
     * Field _hrSituacaoContrato
     */
    private java.lang.String _hrSituacaoContrato;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _dsContratoNegocio
     */
    private java.lang.String _dsContratoNegocio;

    /**
     * Field _cdIndicadorMoeda
     */
    private int _cdIndicadorMoeda = 0;

    /**
     * keeps track of state for field: _cdIndicadorMoeda
     */
    private boolean _has_cdIndicadorMoeda;

    /**
     * Field _dsMoeda
     */
    private java.lang.String _dsMoeda;

    /**
     * Field _cdIdentificadorIdioma
     */
    private int _cdIdentificadorIdioma = 0;

    /**
     * keeps track of state for field: _cdIdentificadorIdioma
     */
    private boolean _has_cdIdentificadorIdioma;

    /**
     * Field _dsIdioma
     */
    private java.lang.String _dsIdioma;

    /**
     * Field _dtInicioVigencia
     */
    private java.lang.String _dtInicioVigencia;

    /**
     * Field _dtFimVigencia
     */
    private java.lang.String _dtFimVigencia;

    /**
     * Field _dsSituacaoContrato
     */
    private java.lang.String _dsSituacaoContrato;

    /**
     * Field _dsMotivoSituacaoContrato
     */
    private java.lang.String _dsMotivoSituacaoContrato;

    /**
     * Field _dsOrigemContrato
     */
    private java.lang.String _dsOrigemContrato;

    /**
     * Field _dsIndicadorParticipante
     */
    private java.lang.String _dsIndicadorParticipante;

    /**
     * Field _dsIndicadorAditivo
     */
    private java.lang.String _dsIndicadorAditivo;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _dsTipoContratoNegocio
     */
    private java.lang.String _dsTipoContratoNegocio;

    /**
     * Field _nmPessoaJuridica
     */
    private java.lang.String _nmPessoaJuridica;

    /**
     * Field _cdDeptoGestor
     */
    private int _cdDeptoGestor = 0;

    /**
     * keeps track of state for field: _cdDeptoGestor
     */
    private boolean _has_cdDeptoGestor;

    /**
     * Field _nmDepto
     */
    private java.lang.String _nmDepto;

    /**
     * Field _cdFuncionarioBradesco
     */
    private long _cdFuncionarioBradesco = 0;

    /**
     * keeps track of state for field: _cdFuncionarioBradesco
     */
    private boolean _has_cdFuncionarioBradesco;

    /**
     * Field _nmFuncionarioBradesco
     */
    private java.lang.String _nmFuncionarioBradesco;

    /**
     * Field _cdSetorContratoPagamento
     */
    private int _cdSetorContratoPagamento = 0;

    /**
     * keeps track of state for field: _cdSetorContratoPagamento
     */
    private boolean _has_cdSetorContratoPagamento;

    /**
     * Field _dsSetorContratoPagamento
     */
    private java.lang.String _dsSetorContratoPagamento;

    /**
     * Field _vlSaldoVirtualTeste
     */
    private java.math.BigDecimal _vlSaldoVirtualTeste = new java.math.BigDecimal("0");

    /**
     * Field _cdPessoaJuridicaProposta
     */
    private long _cdPessoaJuridicaProposta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaProposta
     */
    private boolean _has_cdPessoaJuridicaProposta;

    /**
     * Field _cdTipoContratoProposta
     */
    private int _cdTipoContratoProposta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoProposta
     */
    private boolean _has_cdTipoContratoProposta;

    /**
     * Field _nrContratoProposta
     */
    private long _nrContratoProposta = 0;

    /**
     * keeps track of state for field: _nrContratoProposta
     */
    private boolean _has_nrContratoProposta;

    /**
     * Field _dsPessoaJuridicaProposta
     */
    private java.lang.String _dsPessoaJuridicaProposta;

    /**
     * Field _dsTipoContratoProposta
     */
    private java.lang.String _dsTipoContratoProposta;

    /**
     * Field _cdPropostaJuridicaPagamentoSalario
     */
    private long _cdPropostaJuridicaPagamentoSalario = 0;

    /**
     * keeps track of state for field:
     * _cdPropostaJuridicaPagamentoSalario
     */
    private boolean _has_cdPropostaJuridicaPagamentoSalario;

    /**
     * Field _cdTipoContratoPagamentoSalario
     */
    private int _cdTipoContratoPagamentoSalario = 0;

    /**
     * keeps track of state for field:
     * _cdTipoContratoPagamentoSalario
     */
    private boolean _has_cdTipoContratoPagamentoSalario;

    /**
     * Field _nrSequenciaContratoPagamentoSalario
     */
    private long _nrSequenciaContratoPagamentoSalario = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaContratoPagamentoSalario
     */
    private boolean _has_nrSequenciaContratoPagamentoSalario;

    /**
     * Field _cdPessoaJuridicaOutros
     */
    private long _cdPessoaJuridicaOutros = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaOutros
     */
    private boolean _has_cdPessoaJuridicaOutros;

    /**
     * Field _cdTipoContratoOutros
     */
    private int _cdTipoContratoOutros = 0;

    /**
     * keeps track of state for field: _cdTipoContratoOutros
     */
    private boolean _has_cdTipoContratoOutros;

    /**
     * Field _nrSequenciaContratoOutros
     */
    private long _nrSequenciaContratoOutros = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoOutros
     */
    private boolean _has_nrSequenciaContratoOutros;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _dsFormaAutorizacaoPagamento
     */
    private java.lang.String _dsFormaAutorizacaoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarConManDadosBasicosResponse() 
     {
        super();
        setVlSaldoVirtualTeste(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.response.ConsultarConManDadosBasicosResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoRelacionamento
     * 
     */
    public void deleteCdAcaoRelacionamento()
    {
        this._has_cdAcaoRelacionamento= false;
    } //-- void deleteCdAcaoRelacionamento() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdDeptoGestor
     * 
     */
    public void deleteCdDeptoGestor()
    {
        this._has_cdDeptoGestor= false;
    } //-- void deleteCdDeptoGestor() 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFuncionarioBradesco
     * 
     */
    public void deleteCdFuncionarioBradesco()
    {
        this._has_cdFuncionarioBradesco= false;
    } //-- void deleteCdFuncionarioBradesco() 

    /**
     * Method deleteCdIdentificadorIdioma
     * 
     */
    public void deleteCdIdentificadorIdioma()
    {
        this._has_cdIdentificadorIdioma= false;
    } //-- void deleteCdIdentificadorIdioma() 

    /**
     * Method deleteCdIndicadorMoeda
     * 
     */
    public void deleteCdIndicadorMoeda()
    {
        this._has_cdIndicadorMoeda= false;
    } //-- void deleteCdIndicadorMoeda() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdMotivoSituacaoContrato
     * 
     */
    public void deleteCdMotivoSituacaoContrato()
    {
        this._has_cdMotivoSituacaoContrato= false;
    } //-- void deleteCdMotivoSituacaoContrato() 

    /**
     * Method deleteCdOrigemContrato
     * 
     */
    public void deleteCdOrigemContrato()
    {
        this._has_cdOrigemContrato= false;
    } //-- void deleteCdOrigemContrato() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaOutros
     * 
     */
    public void deleteCdPessoaJuridicaOutros()
    {
        this._has_cdPessoaJuridicaOutros= false;
    } //-- void deleteCdPessoaJuridicaOutros() 

    /**
     * Method deleteCdPessoaJuridicaProposta
     * 
     */
    public void deleteCdPessoaJuridicaProposta()
    {
        this._has_cdPessoaJuridicaProposta= false;
    } //-- void deleteCdPessoaJuridicaProposta() 

    /**
     * Method deleteCdPropostaJuridicaPagamentoSalario
     * 
     */
    public void deleteCdPropostaJuridicaPagamentoSalario()
    {
        this._has_cdPropostaJuridicaPagamentoSalario= false;
    } //-- void deleteCdPropostaJuridicaPagamentoSalario() 

    /**
     * Method deleteCdSetorContratoPagamento
     * 
     */
    public void deleteCdSetorContratoPagamento()
    {
        this._has_cdSetorContratoPagamento= false;
    } //-- void deleteCdSetorContratoPagamento() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoOutros
     * 
     */
    public void deleteCdTipoContratoOutros()
    {
        this._has_cdTipoContratoOutros= false;
    } //-- void deleteCdTipoContratoOutros() 

    /**
     * Method deleteCdTipoContratoPagamentoSalario
     * 
     */
    public void deleteCdTipoContratoPagamentoSalario()
    {
        this._has_cdTipoContratoPagamentoSalario= false;
    } //-- void deleteCdTipoContratoPagamentoSalario() 

    /**
     * Method deleteCdTipoContratoProposta
     * 
     */
    public void deleteCdTipoContratoProposta()
    {
        this._has_cdTipoContratoProposta= false;
    } //-- void deleteCdTipoContratoProposta() 

    /**
     * Method deleteNrContratoProposta
     * 
     */
    public void deleteNrContratoProposta()
    {
        this._has_nrContratoProposta= false;
    } //-- void deleteNrContratoProposta() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoOutros
     * 
     */
    public void deleteNrSequenciaContratoOutros()
    {
        this._has_nrSequenciaContratoOutros= false;
    } //-- void deleteNrSequenciaContratoOutros() 

    /**
     * Method deleteNrSequenciaContratoPagamentoSalario
     * 
     */
    public void deleteNrSequenciaContratoPagamentoSalario()
    {
        this._has_nrSequenciaContratoPagamentoSalario= false;
    } //-- void deleteNrSequenciaContratoPagamentoSalario() 

    /**
     * Returns the value of field 'cdAcaoRelacionamento'.
     * 
     * @return long
     * @return the value of field 'cdAcaoRelacionamento'.
     */
    public long getCdAcaoRelacionamento()
    {
        return this._cdAcaoRelacionamento;
    } //-- long getCdAcaoRelacionamento() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'cdContratoNegocio'.
     */
    public java.lang.String getCdContratoNegocio()
    {
        return this._cdContratoNegocio;
    } //-- java.lang.String getCdContratoNegocio() 

    /**
     * Returns the value of field 'cdDeptoGestor'.
     * 
     * @return int
     * @return the value of field 'cdDeptoGestor'.
     */
    public int getCdDeptoGestor()
    {
        return this._cdDeptoGestor;
    } //-- int getCdDeptoGestor() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFuncionarioBradesco'.
     * 
     * @return long
     * @return the value of field 'cdFuncionarioBradesco'.
     */
    public long getCdFuncionarioBradesco()
    {
        return this._cdFuncionarioBradesco;
    } //-- long getCdFuncionarioBradesco() 

    /**
     * Returns the value of field 'cdIdentificadorIdioma'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorIdioma'.
     */
    public int getCdIdentificadorIdioma()
    {
        return this._cdIdentificadorIdioma;
    } //-- int getCdIdentificadorIdioma() 

    /**
     * Returns the value of field 'cdIndicadorMoeda'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorMoeda'.
     */
    public int getCdIndicadorMoeda()
    {
        return this._cdIndicadorMoeda;
    } //-- int getCdIndicadorMoeda() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdMotivoSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoContrato'.
     */
    public int getCdMotivoSituacaoContrato()
    {
        return this._cdMotivoSituacaoContrato;
    } //-- int getCdMotivoSituacaoContrato() 

    /**
     * Returns the value of field 'cdOrigemContrato'.
     * 
     * @return int
     * @return the value of field 'cdOrigemContrato'.
     */
    public int getCdOrigemContrato()
    {
        return this._cdOrigemContrato;
    } //-- int getCdOrigemContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaOutros'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaOutros'.
     */
    public long getCdPessoaJuridicaOutros()
    {
        return this._cdPessoaJuridicaOutros;
    } //-- long getCdPessoaJuridicaOutros() 

    /**
     * Returns the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaProposta'.
     */
    public long getCdPessoaJuridicaProposta()
    {
        return this._cdPessoaJuridicaProposta;
    } //-- long getCdPessoaJuridicaProposta() 

    /**
     * Returns the value of field
     * 'cdPropostaJuridicaPagamentoSalario'.
     * 
     * @return long
     * @return the value of field
     * 'cdPropostaJuridicaPagamentoSalario'.
     */
    public long getCdPropostaJuridicaPagamentoSalario()
    {
        return this._cdPropostaJuridicaPagamentoSalario;
    } //-- long getCdPropostaJuridicaPagamentoSalario() 

    /**
     * Returns the value of field 'cdSetorContratoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSetorContratoPagamento'.
     */
    public int getCdSetorContratoPagamento()
    {
        return this._cdSetorContratoPagamento;
    } //-- int getCdSetorContratoPagamento() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoOutros'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoOutros'.
     */
    public int getCdTipoContratoOutros()
    {
        return this._cdTipoContratoOutros;
    } //-- int getCdTipoContratoOutros() 

    /**
     * Returns the value of field 'cdTipoContratoPagamentoSalario'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoPagamentoSalario'.
     */
    public int getCdTipoContratoPagamentoSalario()
    {
        return this._cdTipoContratoPagamentoSalario;
    } //-- int getCdTipoContratoPagamentoSalario() 

    /**
     * Returns the value of field 'cdTipoContratoProposta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoProposta'.
     */
    public int getCdTipoContratoProposta()
    {
        return this._cdTipoContratoProposta;
    } //-- int getCdTipoContratoProposta() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsContratoNegocio'.
     */
    public java.lang.String getDsContratoNegocio()
    {
        return this._dsContratoNegocio;
    } //-- java.lang.String getDsContratoNegocio() 

    /**
     * Returns the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsFormaAutorizacaoPagamento()
    {
        return this._dsFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'dsIdioma'.
     * 
     * @return String
     * @return the value of field 'dsIdioma'.
     */
    public java.lang.String getDsIdioma()
    {
        return this._dsIdioma;
    } //-- java.lang.String getDsIdioma() 

    /**
     * Returns the value of field 'dsIndicadorAditivo'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAditivo'.
     */
    public java.lang.String getDsIndicadorAditivo()
    {
        return this._dsIndicadorAditivo;
    } //-- java.lang.String getDsIndicadorAditivo() 

    /**
     * Returns the value of field 'dsIndicadorParticipante'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorParticipante'.
     */
    public java.lang.String getDsIndicadorParticipante()
    {
        return this._dsIndicadorParticipante;
    } //-- java.lang.String getDsIndicadorParticipante() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsMoeda'.
     * 
     * @return String
     * @return the value of field 'dsMoeda'.
     */
    public java.lang.String getDsMoeda()
    {
        return this._dsMoeda;
    } //-- java.lang.String getDsMoeda() 

    /**
     * Returns the value of field 'dsMotivoSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoContrato'.
     */
    public java.lang.String getDsMotivoSituacaoContrato()
    {
        return this._dsMotivoSituacaoContrato;
    } //-- java.lang.String getDsMotivoSituacaoContrato() 

    /**
     * Returns the value of field 'dsOrigemContrato'.
     * 
     * @return String
     * @return the value of field 'dsOrigemContrato'.
     */
    public java.lang.String getDsOrigemContrato()
    {
        return this._dsOrigemContrato;
    } //-- java.lang.String getDsOrigemContrato() 

    /**
     * Returns the value of field 'dsPessoaJuridicaProposta'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaProposta'.
     */
    public java.lang.String getDsPessoaJuridicaProposta()
    {
        return this._dsPessoaJuridicaProposta;
    } //-- java.lang.String getDsPessoaJuridicaProposta() 

    /**
     * Returns the value of field 'dsSetorContratoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSetorContratoPagamento'.
     */
    public java.lang.String getDsSetorContratoPagamento()
    {
        return this._dsSetorContratoPagamento;
    } //-- java.lang.String getDsSetorContratoPagamento() 

    /**
     * Returns the value of field 'dsSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContrato'.
     */
    public java.lang.String getDsSituacaoContrato()
    {
        return this._dsSituacaoContrato;
    } //-- java.lang.String getDsSituacaoContrato() 

    /**
     * Returns the value of field 'dsTipoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoNegocio'.
     */
    public java.lang.String getDsTipoContratoNegocio()
    {
        return this._dsTipoContratoNegocio;
    } //-- java.lang.String getDsTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsTipoContratoProposta'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoProposta'.
     */
    public java.lang.String getDsTipoContratoProposta()
    {
        return this._dsTipoContratoProposta;
    } //-- java.lang.String getDsTipoContratoProposta() 

    /**
     * Returns the value of field 'dtFimVigencia'.
     * 
     * @return String
     * @return the value of field 'dtFimVigencia'.
     */
    public java.lang.String getDtFimVigencia()
    {
        return this._dtFimVigencia;
    } //-- java.lang.String getDtFimVigencia() 

    /**
     * Returns the value of field 'dtInicioVigencia'.
     * 
     * @return String
     * @return the value of field 'dtInicioVigencia'.
     */
    public java.lang.String getDtInicioVigencia()
    {
        return this._dtInicioVigencia;
    } //-- java.lang.String getDtInicioVigencia() 

    /**
     * Returns the value of field 'hrAssinaturaContrato'.
     * 
     * @return String
     * @return the value of field 'hrAssinaturaContrato'.
     */
    public java.lang.String getHrAssinaturaContrato()
    {
        return this._hrAssinaturaContrato;
    } //-- java.lang.String getHrAssinaturaContrato() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHistorico'.
     */
    public java.lang.String getHrInclusaoRegistroHistorico()
    {
        return this._hrInclusaoRegistroHistorico;
    } //-- java.lang.String getHrInclusaoRegistroHistorico() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'hrSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'hrSituacaoContrato'.
     */
    public java.lang.String getHrSituacaoContrato()
    {
        return this._hrSituacaoContrato;
    } //-- java.lang.String getHrSituacaoContrato() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmDepto'.
     * 
     * @return String
     * @return the value of field 'nmDepto'.
     */
    public java.lang.String getNmDepto()
    {
        return this._nmDepto;
    } //-- java.lang.String getNmDepto() 

    /**
     * Returns the value of field 'nmFuncionarioBradesco'.
     * 
     * @return String
     * @return the value of field 'nmFuncionarioBradesco'.
     */
    public java.lang.String getNmFuncionarioBradesco()
    {
        return this._nmFuncionarioBradesco;
    } //-- java.lang.String getNmFuncionarioBradesco() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nmPessoaJuridica'.
     * 
     * @return String
     * @return the value of field 'nmPessoaJuridica'.
     */
    public java.lang.String getNmPessoaJuridica()
    {
        return this._nmPessoaJuridica;
    } //-- java.lang.String getNmPessoaJuridica() 

    /**
     * Returns the value of field 'nrContratoProposta'.
     * 
     * @return long
     * @return the value of field 'nrContratoProposta'.
     */
    public long getNrContratoProposta()
    {
        return this._nrContratoProposta;
    } //-- long getNrContratoProposta() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoOutros'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoOutros'.
     */
    public long getNrSequenciaContratoOutros()
    {
        return this._nrSequenciaContratoOutros;
    } //-- long getNrSequenciaContratoOutros() 

    /**
     * Returns the value of field
     * 'nrSequenciaContratoPagamentoSalario'.
     * 
     * @return long
     * @return the value of field
     * 'nrSequenciaContratoPagamentoSalario'.
     */
    public long getNrSequenciaContratoPagamentoSalario()
    {
        return this._nrSequenciaContratoPagamentoSalario;
    } //-- long getNrSequenciaContratoPagamentoSalario() 

    /**
     * Returns the value of field 'vlSaldoVirtualTeste'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoVirtualTeste'.
     */
    public java.math.BigDecimal getVlSaldoVirtualTeste()
    {
        return this._vlSaldoVirtualTeste;
    } //-- java.math.BigDecimal getVlSaldoVirtualTeste() 

    /**
     * Method hasCdAcaoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoRelacionamento()
    {
        return this._has_cdAcaoRelacionamento;
    } //-- boolean hasCdAcaoRelacionamento() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdDeptoGestor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDeptoGestor()
    {
        return this._has_cdDeptoGestor;
    } //-- boolean hasCdDeptoGestor() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFuncionarioBradesco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionarioBradesco()
    {
        return this._has_cdFuncionarioBradesco;
    } //-- boolean hasCdFuncionarioBradesco() 

    /**
     * Method hasCdIdentificadorIdioma
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorIdioma()
    {
        return this._has_cdIdentificadorIdioma;
    } //-- boolean hasCdIdentificadorIdioma() 

    /**
     * Method hasCdIndicadorMoeda
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorMoeda()
    {
        return this._has_cdIndicadorMoeda;
    } //-- boolean hasCdIndicadorMoeda() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdMotivoSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoContrato()
    {
        return this._has_cdMotivoSituacaoContrato;
    } //-- boolean hasCdMotivoSituacaoContrato() 

    /**
     * Method hasCdOrigemContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrigemContrato()
    {
        return this._has_cdOrigemContrato;
    } //-- boolean hasCdOrigemContrato() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaOutros()
    {
        return this._has_cdPessoaJuridicaOutros;
    } //-- boolean hasCdPessoaJuridicaOutros() 

    /**
     * Method hasCdPessoaJuridicaProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaProposta()
    {
        return this._has_cdPessoaJuridicaProposta;
    } //-- boolean hasCdPessoaJuridicaProposta() 

    /**
     * Method hasCdPropostaJuridicaPagamentoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPropostaJuridicaPagamentoSalario()
    {
        return this._has_cdPropostaJuridicaPagamentoSalario;
    } //-- boolean hasCdPropostaJuridicaPagamentoSalario() 

    /**
     * Method hasCdSetorContratoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSetorContratoPagamento()
    {
        return this._has_cdSetorContratoPagamento;
    } //-- boolean hasCdSetorContratoPagamento() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoOutros()
    {
        return this._has_cdTipoContratoOutros;
    } //-- boolean hasCdTipoContratoOutros() 

    /**
     * Method hasCdTipoContratoPagamentoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoPagamentoSalario()
    {
        return this._has_cdTipoContratoPagamentoSalario;
    } //-- boolean hasCdTipoContratoPagamentoSalario() 

    /**
     * Method hasCdTipoContratoProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoProposta()
    {
        return this._has_cdTipoContratoProposta;
    } //-- boolean hasCdTipoContratoProposta() 

    /**
     * Method hasNrContratoProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContratoProposta()
    {
        return this._has_nrContratoProposta;
    } //-- boolean hasNrContratoProposta() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoOutros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoOutros()
    {
        return this._has_nrSequenciaContratoOutros;
    } //-- boolean hasNrSequenciaContratoOutros() 

    /**
     * Method hasNrSequenciaContratoPagamentoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoPagamentoSalario()
    {
        return this._has_nrSequenciaContratoPagamentoSalario;
    } //-- boolean hasNrSequenciaContratoPagamentoSalario() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoRelacionamento'.
     * 
     * @param cdAcaoRelacionamento the value of field
     * 'cdAcaoRelacionamento'.
     */
    public void setCdAcaoRelacionamento(long cdAcaoRelacionamento)
    {
        this._cdAcaoRelacionamento = cdAcaoRelacionamento;
        this._has_cdAcaoRelacionamento = true;
    } //-- void setCdAcaoRelacionamento(long) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdContratoNegocio'.
     * 
     * @param cdContratoNegocio the value of field
     * 'cdContratoNegocio'.
     */
    public void setCdContratoNegocio(java.lang.String cdContratoNegocio)
    {
        this._cdContratoNegocio = cdContratoNegocio;
    } //-- void setCdContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'cdDeptoGestor'.
     * 
     * @param cdDeptoGestor the value of field 'cdDeptoGestor'.
     */
    public void setCdDeptoGestor(int cdDeptoGestor)
    {
        this._cdDeptoGestor = cdDeptoGestor;
        this._has_cdDeptoGestor = true;
    } //-- void setCdDeptoGestor(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFuncionarioBradesco'.
     * 
     * @param cdFuncionarioBradesco the value of field
     * 'cdFuncionarioBradesco'.
     */
    public void setCdFuncionarioBradesco(long cdFuncionarioBradesco)
    {
        this._cdFuncionarioBradesco = cdFuncionarioBradesco;
        this._has_cdFuncionarioBradesco = true;
    } //-- void setCdFuncionarioBradesco(long) 

    /**
     * Sets the value of field 'cdIdentificadorIdioma'.
     * 
     * @param cdIdentificadorIdioma the value of field
     * 'cdIdentificadorIdioma'.
     */
    public void setCdIdentificadorIdioma(int cdIdentificadorIdioma)
    {
        this._cdIdentificadorIdioma = cdIdentificadorIdioma;
        this._has_cdIdentificadorIdioma = true;
    } //-- void setCdIdentificadorIdioma(int) 

    /**
     * Sets the value of field 'cdIndicadorMoeda'.
     * 
     * @param cdIndicadorMoeda the value of field 'cdIndicadorMoeda'
     */
    public void setCdIndicadorMoeda(int cdIndicadorMoeda)
    {
        this._cdIndicadorMoeda = cdIndicadorMoeda;
        this._has_cdIndicadorMoeda = true;
    } //-- void setCdIndicadorMoeda(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoContrato'.
     * 
     * @param cdMotivoSituacaoContrato the value of field
     * 'cdMotivoSituacaoContrato'.
     */
    public void setCdMotivoSituacaoContrato(int cdMotivoSituacaoContrato)
    {
        this._cdMotivoSituacaoContrato = cdMotivoSituacaoContrato;
        this._has_cdMotivoSituacaoContrato = true;
    } //-- void setCdMotivoSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdOrigemContrato'.
     * 
     * @param cdOrigemContrato the value of field 'cdOrigemContrato'
     */
    public void setCdOrigemContrato(int cdOrigemContrato)
    {
        this._cdOrigemContrato = cdOrigemContrato;
        this._has_cdOrigemContrato = true;
    } //-- void setCdOrigemContrato(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaOutros'.
     * 
     * @param cdPessoaJuridicaOutros the value of field
     * 'cdPessoaJuridicaOutros'.
     */
    public void setCdPessoaJuridicaOutros(long cdPessoaJuridicaOutros)
    {
        this._cdPessoaJuridicaOutros = cdPessoaJuridicaOutros;
        this._has_cdPessoaJuridicaOutros = true;
    } //-- void setCdPessoaJuridicaOutros(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @param cdPessoaJuridicaProposta the value of field
     * 'cdPessoaJuridicaProposta'.
     */
    public void setCdPessoaJuridicaProposta(long cdPessoaJuridicaProposta)
    {
        this._cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
        this._has_cdPessoaJuridicaProposta = true;
    } //-- void setCdPessoaJuridicaProposta(long) 

    /**
     * Sets the value of field
     * 'cdPropostaJuridicaPagamentoSalario'.
     * 
     * @param cdPropostaJuridicaPagamentoSalario the value of field
     * 'cdPropostaJuridicaPagamentoSalario'.
     */
    public void setCdPropostaJuridicaPagamentoSalario(long cdPropostaJuridicaPagamentoSalario)
    {
        this._cdPropostaJuridicaPagamentoSalario = cdPropostaJuridicaPagamentoSalario;
        this._has_cdPropostaJuridicaPagamentoSalario = true;
    } //-- void setCdPropostaJuridicaPagamentoSalario(long) 

    /**
     * Sets the value of field 'cdSetorContratoPagamento'.
     * 
     * @param cdSetorContratoPagamento the value of field
     * 'cdSetorContratoPagamento'.
     */
    public void setCdSetorContratoPagamento(int cdSetorContratoPagamento)
    {
        this._cdSetorContratoPagamento = cdSetorContratoPagamento;
        this._has_cdSetorContratoPagamento = true;
    } //-- void setCdSetorContratoPagamento(int) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoOutros'.
     * 
     * @param cdTipoContratoOutros the value of field
     * 'cdTipoContratoOutros'.
     */
    public void setCdTipoContratoOutros(int cdTipoContratoOutros)
    {
        this._cdTipoContratoOutros = cdTipoContratoOutros;
        this._has_cdTipoContratoOutros = true;
    } //-- void setCdTipoContratoOutros(int) 

    /**
     * Sets the value of field 'cdTipoContratoPagamentoSalario'.
     * 
     * @param cdTipoContratoPagamentoSalario the value of field
     * 'cdTipoContratoPagamentoSalario'.
     */
    public void setCdTipoContratoPagamentoSalario(int cdTipoContratoPagamentoSalario)
    {
        this._cdTipoContratoPagamentoSalario = cdTipoContratoPagamentoSalario;
        this._has_cdTipoContratoPagamentoSalario = true;
    } //-- void setCdTipoContratoPagamentoSalario(int) 

    /**
     * Sets the value of field 'cdTipoContratoProposta'.
     * 
     * @param cdTipoContratoProposta the value of field
     * 'cdTipoContratoProposta'.
     */
    public void setCdTipoContratoProposta(int cdTipoContratoProposta)
    {
        this._cdTipoContratoProposta = cdTipoContratoProposta;
        this._has_cdTipoContratoProposta = true;
    } //-- void setCdTipoContratoProposta(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsContratoNegocio'.
     * 
     * @param dsContratoNegocio the value of field
     * 'dsContratoNegocio'.
     */
    public void setDsContratoNegocio(java.lang.String dsContratoNegocio)
    {
        this._dsContratoNegocio = dsContratoNegocio;
    } //-- void setDsContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @param dsFormaAutorizacaoPagamento the value of field
     * 'dsFormaAutorizacaoPagamento'.
     */
    public void setDsFormaAutorizacaoPagamento(java.lang.String dsFormaAutorizacaoPagamento)
    {
        this._dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    } //-- void setDsFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsIdioma'.
     * 
     * @param dsIdioma the value of field 'dsIdioma'.
     */
    public void setDsIdioma(java.lang.String dsIdioma)
    {
        this._dsIdioma = dsIdioma;
    } //-- void setDsIdioma(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAditivo'.
     * 
     * @param dsIndicadorAditivo the value of field
     * 'dsIndicadorAditivo'.
     */
    public void setDsIndicadorAditivo(java.lang.String dsIndicadorAditivo)
    {
        this._dsIndicadorAditivo = dsIndicadorAditivo;
    } //-- void setDsIndicadorAditivo(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorParticipante'.
     * 
     * @param dsIndicadorParticipante the value of field
     * 'dsIndicadorParticipante'.
     */
    public void setDsIndicadorParticipante(java.lang.String dsIndicadorParticipante)
    {
        this._dsIndicadorParticipante = dsIndicadorParticipante;
    } //-- void setDsIndicadorParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsMoeda'.
     * 
     * @param dsMoeda the value of field 'dsMoeda'.
     */
    public void setDsMoeda(java.lang.String dsMoeda)
    {
        this._dsMoeda = dsMoeda;
    } //-- void setDsMoeda(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoContrato'.
     * 
     * @param dsMotivoSituacaoContrato the value of field
     * 'dsMotivoSituacaoContrato'.
     */
    public void setDsMotivoSituacaoContrato(java.lang.String dsMotivoSituacaoContrato)
    {
        this._dsMotivoSituacaoContrato = dsMotivoSituacaoContrato;
    } //-- void setDsMotivoSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsOrigemContrato'.
     * 
     * @param dsOrigemContrato the value of field 'dsOrigemContrato'
     */
    public void setDsOrigemContrato(java.lang.String dsOrigemContrato)
    {
        this._dsOrigemContrato = dsOrigemContrato;
    } //-- void setDsOrigemContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaProposta'.
     * 
     * @param dsPessoaJuridicaProposta the value of field
     * 'dsPessoaJuridicaProposta'.
     */
    public void setDsPessoaJuridicaProposta(java.lang.String dsPessoaJuridicaProposta)
    {
        this._dsPessoaJuridicaProposta = dsPessoaJuridicaProposta;
    } //-- void setDsPessoaJuridicaProposta(java.lang.String) 

    /**
     * Sets the value of field 'dsSetorContratoPagamento'.
     * 
     * @param dsSetorContratoPagamento the value of field
     * 'dsSetorContratoPagamento'.
     */
    public void setDsSetorContratoPagamento(java.lang.String dsSetorContratoPagamento)
    {
        this._dsSetorContratoPagamento = dsSetorContratoPagamento;
    } //-- void setDsSetorContratoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContrato'.
     * 
     * @param dsSituacaoContrato the value of field
     * 'dsSituacaoContrato'.
     */
    public void setDsSituacaoContrato(java.lang.String dsSituacaoContrato)
    {
        this._dsSituacaoContrato = dsSituacaoContrato;
    } //-- void setDsSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoNegocio'.
     * 
     * @param dsTipoContratoNegocio the value of field
     * 'dsTipoContratoNegocio'.
     */
    public void setDsTipoContratoNegocio(java.lang.String dsTipoContratoNegocio)
    {
        this._dsTipoContratoNegocio = dsTipoContratoNegocio;
    } //-- void setDsTipoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoProposta'.
     * 
     * @param dsTipoContratoProposta the value of field
     * 'dsTipoContratoProposta'.
     */
    public void setDsTipoContratoProposta(java.lang.String dsTipoContratoProposta)
    {
        this._dsTipoContratoProposta = dsTipoContratoProposta;
    } //-- void setDsTipoContratoProposta(java.lang.String) 

    /**
     * Sets the value of field 'dtFimVigencia'.
     * 
     * @param dtFimVigencia the value of field 'dtFimVigencia'.
     */
    public void setDtFimVigencia(java.lang.String dtFimVigencia)
    {
        this._dtFimVigencia = dtFimVigencia;
    } //-- void setDtFimVigencia(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioVigencia'.
     * 
     * @param dtInicioVigencia the value of field 'dtInicioVigencia'
     */
    public void setDtInicioVigencia(java.lang.String dtInicioVigencia)
    {
        this._dtInicioVigencia = dtInicioVigencia;
    } //-- void setDtInicioVigencia(java.lang.String) 

    /**
     * Sets the value of field 'hrAssinaturaContrato'.
     * 
     * @param hrAssinaturaContrato the value of field
     * 'hrAssinaturaContrato'.
     */
    public void setHrAssinaturaContrato(java.lang.String hrAssinaturaContrato)
    {
        this._hrAssinaturaContrato = hrAssinaturaContrato;
    } //-- void setHrAssinaturaContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @param hrInclusaoRegistroHistorico the value of field
     * 'hrInclusaoRegistroHistorico'.
     */
    public void setHrInclusaoRegistroHistorico(java.lang.String hrInclusaoRegistroHistorico)
    {
        this._hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
    } //-- void setHrInclusaoRegistroHistorico(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrSituacaoContrato'.
     * 
     * @param hrSituacaoContrato the value of field
     * 'hrSituacaoContrato'.
     */
    public void setHrSituacaoContrato(java.lang.String hrSituacaoContrato)
    {
        this._hrSituacaoContrato = hrSituacaoContrato;
    } //-- void setHrSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmDepto'.
     * 
     * @param nmDepto the value of field 'nmDepto'.
     */
    public void setNmDepto(java.lang.String nmDepto)
    {
        this._nmDepto = nmDepto;
    } //-- void setNmDepto(java.lang.String) 

    /**
     * Sets the value of field 'nmFuncionarioBradesco'.
     * 
     * @param nmFuncionarioBradesco the value of field
     * 'nmFuncionarioBradesco'.
     */
    public void setNmFuncionarioBradesco(java.lang.String nmFuncionarioBradesco)
    {
        this._nmFuncionarioBradesco = nmFuncionarioBradesco;
    } //-- void setNmFuncionarioBradesco(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nmPessoaJuridica'.
     * 
     * @param nmPessoaJuridica the value of field 'nmPessoaJuridica'
     */
    public void setNmPessoaJuridica(java.lang.String nmPessoaJuridica)
    {
        this._nmPessoaJuridica = nmPessoaJuridica;
    } //-- void setNmPessoaJuridica(java.lang.String) 

    /**
     * Sets the value of field 'nrContratoProposta'.
     * 
     * @param nrContratoProposta the value of field
     * 'nrContratoProposta'.
     */
    public void setNrContratoProposta(long nrContratoProposta)
    {
        this._nrContratoProposta = nrContratoProposta;
        this._has_nrContratoProposta = true;
    } //-- void setNrContratoProposta(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoOutros'.
     * 
     * @param nrSequenciaContratoOutros the value of field
     * 'nrSequenciaContratoOutros'.
     */
    public void setNrSequenciaContratoOutros(long nrSequenciaContratoOutros)
    {
        this._nrSequenciaContratoOutros = nrSequenciaContratoOutros;
        this._has_nrSequenciaContratoOutros = true;
    } //-- void setNrSequenciaContratoOutros(long) 

    /**
     * Sets the value of field
     * 'nrSequenciaContratoPagamentoSalario'.
     * 
     * @param nrSequenciaContratoPagamentoSalario the value of
     * field 'nrSequenciaContratoPagamentoSalario'.
     */
    public void setNrSequenciaContratoPagamentoSalario(long nrSequenciaContratoPagamentoSalario)
    {
        this._nrSequenciaContratoPagamentoSalario = nrSequenciaContratoPagamentoSalario;
        this._has_nrSequenciaContratoPagamentoSalario = true;
    } //-- void setNrSequenciaContratoPagamentoSalario(long) 

    /**
     * Sets the value of field 'vlSaldoVirtualTeste'.
     * 
     * @param vlSaldoVirtualTeste the value of field
     * 'vlSaldoVirtualTeste'.
     */
    public void setVlSaldoVirtualTeste(java.math.BigDecimal vlSaldoVirtualTeste)
    {
        this._vlSaldoVirtualTeste = vlSaldoVirtualTeste;
    } //-- void setVlSaldoVirtualTeste(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarConManDadosBasicosResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.response.ConsultarConManDadosBasicosResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.response.ConsultarConManDadosBasicosResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.response.ConsultarConManDadosBasicosResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmandadosbasicos.response.ConsultarConManDadosBasicosResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
