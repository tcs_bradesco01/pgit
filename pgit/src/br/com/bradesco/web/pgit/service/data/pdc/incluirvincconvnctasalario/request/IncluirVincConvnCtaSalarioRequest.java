/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirVincConvnCtaSalarioRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirVincConvnCtaSalarioRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _cdConvenNovo
     */
    private int _cdConvenNovo = 0;

    /**
     * keeps track of state for field: _cdConvenNovo
     */
    private boolean _has_cdConvenNovo;

    /**
     * Field _cdCnpjEmp
     */
    private long _cdCnpjEmp = 0;

    /**
     * keeps track of state for field: _cdCnpjEmp
     */
    private boolean _has_cdCnpjEmp;

    /**
     * Field _cdFilialEmp
     */
    private int _cdFilialEmp = 0;

    /**
     * keeps track of state for field: _cdFilialEmp
     */
    private boolean _has_cdFilialEmp;

    /**
     * Field _cdCtrlCnpjEmp
     */
    private int _cdCtrlCnpjEmp = 0;

    /**
     * keeps track of state for field: _cdCtrlCnpjEmp
     */
    private boolean _has_cdCtrlCnpjEmp;

    /**
     * Field _dsConvCtaEmp
     */
    private java.lang.String _dsConvCtaEmp;

    /**
     * Field _cdBcoCtaEmp
     */
    private int _cdBcoCtaEmp = 0;

    /**
     * keeps track of state for field: _cdBcoCtaEmp
     */
    private boolean _has_cdBcoCtaEmp;

    /**
     * Field _cdAgencCtaEmp
     */
    private int _cdAgencCtaEmp = 0;

    /**
     * keeps track of state for field: _cdAgencCtaEmp
     */
    private boolean _has_cdAgencCtaEmp;

    /**
     * Field _cdNumeroCtaDest
     */
    private long _cdNumeroCtaDest = 0;

    /**
     * keeps track of state for field: _cdNumeroCtaDest
     */
    private boolean _has_cdNumeroCtaDest;

    /**
     * Field _cdDigitoCtaEmp
     */
    private java.lang.String _cdDigitoCtaEmp;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirVincConvnCtaSalarioRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.request.IncluirVincConvnCtaSalarioRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencCtaEmp
     * 
     */
    public void deleteCdAgencCtaEmp()
    {
        this._has_cdAgencCtaEmp= false;
    } //-- void deleteCdAgencCtaEmp() 

    /**
     * Method deleteCdBcoCtaEmp
     * 
     */
    public void deleteCdBcoCtaEmp()
    {
        this._has_cdBcoCtaEmp= false;
    } //-- void deleteCdBcoCtaEmp() 

    /**
     * Method deleteCdCnpjEmp
     * 
     */
    public void deleteCdCnpjEmp()
    {
        this._has_cdCnpjEmp= false;
    } //-- void deleteCdCnpjEmp() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdConvenNovo
     * 
     */
    public void deleteCdConvenNovo()
    {
        this._has_cdConvenNovo= false;
    } //-- void deleteCdConvenNovo() 

    /**
     * Method deleteCdCtrlCnpjEmp
     * 
     */
    public void deleteCdCtrlCnpjEmp()
    {
        this._has_cdCtrlCnpjEmp= false;
    } //-- void deleteCdCtrlCnpjEmp() 

    /**
     * Method deleteCdFilialEmp
     * 
     */
    public void deleteCdFilialEmp()
    {
        this._has_cdFilialEmp= false;
    } //-- void deleteCdFilialEmp() 

    /**
     * Method deleteCdNumeroCtaDest
     * 
     */
    public void deleteCdNumeroCtaDest()
    {
        this._has_cdNumeroCtaDest= false;
    } //-- void deleteCdNumeroCtaDest() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Returns the value of field 'cdAgencCtaEmp'.
     * 
     * @return int
     * @return the value of field 'cdAgencCtaEmp'.
     */
    public int getCdAgencCtaEmp()
    {
        return this._cdAgencCtaEmp;
    } //-- int getCdAgencCtaEmp() 

    /**
     * Returns the value of field 'cdBcoCtaEmp'.
     * 
     * @return int
     * @return the value of field 'cdBcoCtaEmp'.
     */
    public int getCdBcoCtaEmp()
    {
        return this._cdBcoCtaEmp;
    } //-- int getCdBcoCtaEmp() 

    /**
     * Returns the value of field 'cdCnpjEmp'.
     * 
     * @return long
     * @return the value of field 'cdCnpjEmp'.
     */
    public long getCdCnpjEmp()
    {
        return this._cdCnpjEmp;
    } //-- long getCdCnpjEmp() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdConvenNovo'.
     * 
     * @return int
     * @return the value of field 'cdConvenNovo'.
     */
    public int getCdConvenNovo()
    {
        return this._cdConvenNovo;
    } //-- int getCdConvenNovo() 

    /**
     * Returns the value of field 'cdCtrlCnpjEmp'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCnpjEmp'.
     */
    public int getCdCtrlCnpjEmp()
    {
        return this._cdCtrlCnpjEmp;
    } //-- int getCdCtrlCnpjEmp() 

    /**
     * Returns the value of field 'cdDigitoCtaEmp'.
     * 
     * @return String
     * @return the value of field 'cdDigitoCtaEmp'.
     */
    public java.lang.String getCdDigitoCtaEmp()
    {
        return this._cdDigitoCtaEmp;
    } //-- java.lang.String getCdDigitoCtaEmp() 

    /**
     * Returns the value of field 'cdFilialEmp'.
     * 
     * @return int
     * @return the value of field 'cdFilialEmp'.
     */
    public int getCdFilialEmp()
    {
        return this._cdFilialEmp;
    } //-- int getCdFilialEmp() 

    /**
     * Returns the value of field 'cdNumeroCtaDest'.
     * 
     * @return long
     * @return the value of field 'cdNumeroCtaDest'.
     */
    public long getCdNumeroCtaDest()
    {
        return this._cdNumeroCtaDest;
    } //-- long getCdNumeroCtaDest() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'dsConvCtaEmp'.
     * 
     * @return String
     * @return the value of field 'dsConvCtaEmp'.
     */
    public java.lang.String getDsConvCtaEmp()
    {
        return this._dsConvCtaEmp;
    } //-- java.lang.String getDsConvCtaEmp() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Method hasCdAgencCtaEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencCtaEmp()
    {
        return this._has_cdAgencCtaEmp;
    } //-- boolean hasCdAgencCtaEmp() 

    /**
     * Method hasCdBcoCtaEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBcoCtaEmp()
    {
        return this._has_cdBcoCtaEmp;
    } //-- boolean hasCdBcoCtaEmp() 

    /**
     * Method hasCdCnpjEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjEmp()
    {
        return this._has_cdCnpjEmp;
    } //-- boolean hasCdCnpjEmp() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdConvenNovo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConvenNovo()
    {
        return this._has_cdConvenNovo;
    } //-- boolean hasCdConvenNovo() 

    /**
     * Method hasCdCtrlCnpjEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCnpjEmp()
    {
        return this._has_cdCtrlCnpjEmp;
    } //-- boolean hasCdCtrlCnpjEmp() 

    /**
     * Method hasCdFilialEmp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialEmp()
    {
        return this._has_cdFilialEmp;
    } //-- boolean hasCdFilialEmp() 

    /**
     * Method hasCdNumeroCtaDest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNumeroCtaDest()
    {
        return this._has_cdNumeroCtaDest;
    } //-- boolean hasCdNumeroCtaDest() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencCtaEmp'.
     * 
     * @param cdAgencCtaEmp the value of field 'cdAgencCtaEmp'.
     */
    public void setCdAgencCtaEmp(int cdAgencCtaEmp)
    {
        this._cdAgencCtaEmp = cdAgencCtaEmp;
        this._has_cdAgencCtaEmp = true;
    } //-- void setCdAgencCtaEmp(int) 

    /**
     * Sets the value of field 'cdBcoCtaEmp'.
     * 
     * @param cdBcoCtaEmp the value of field 'cdBcoCtaEmp'.
     */
    public void setCdBcoCtaEmp(int cdBcoCtaEmp)
    {
        this._cdBcoCtaEmp = cdBcoCtaEmp;
        this._has_cdBcoCtaEmp = true;
    } //-- void setCdBcoCtaEmp(int) 

    /**
     * Sets the value of field 'cdCnpjEmp'.
     * 
     * @param cdCnpjEmp the value of field 'cdCnpjEmp'.
     */
    public void setCdCnpjEmp(long cdCnpjEmp)
    {
        this._cdCnpjEmp = cdCnpjEmp;
        this._has_cdCnpjEmp = true;
    } //-- void setCdCnpjEmp(long) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdConvenNovo'.
     * 
     * @param cdConvenNovo the value of field 'cdConvenNovo'.
     */
    public void setCdConvenNovo(int cdConvenNovo)
    {
        this._cdConvenNovo = cdConvenNovo;
        this._has_cdConvenNovo = true;
    } //-- void setCdConvenNovo(int) 

    /**
     * Sets the value of field 'cdCtrlCnpjEmp'.
     * 
     * @param cdCtrlCnpjEmp the value of field 'cdCtrlCnpjEmp'.
     */
    public void setCdCtrlCnpjEmp(int cdCtrlCnpjEmp)
    {
        this._cdCtrlCnpjEmp = cdCtrlCnpjEmp;
        this._has_cdCtrlCnpjEmp = true;
    } //-- void setCdCtrlCnpjEmp(int) 

    /**
     * Sets the value of field 'cdDigitoCtaEmp'.
     * 
     * @param cdDigitoCtaEmp the value of field 'cdDigitoCtaEmp'.
     */
    public void setCdDigitoCtaEmp(java.lang.String cdDigitoCtaEmp)
    {
        this._cdDigitoCtaEmp = cdDigitoCtaEmp;
    } //-- void setCdDigitoCtaEmp(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialEmp'.
     * 
     * @param cdFilialEmp the value of field 'cdFilialEmp'.
     */
    public void setCdFilialEmp(int cdFilialEmp)
    {
        this._cdFilialEmp = cdFilialEmp;
        this._has_cdFilialEmp = true;
    } //-- void setCdFilialEmp(int) 

    /**
     * Sets the value of field 'cdNumeroCtaDest'.
     * 
     * @param cdNumeroCtaDest the value of field 'cdNumeroCtaDest'.
     */
    public void setCdNumeroCtaDest(long cdNumeroCtaDest)
    {
        this._cdNumeroCtaDest = cdNumeroCtaDest;
        this._has_cdNumeroCtaDest = true;
    } //-- void setCdNumeroCtaDest(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'dsConvCtaEmp'.
     * 
     * @param dsConvCtaEmp the value of field 'dsConvCtaEmp'.
     */
    public void setDsConvCtaEmp(java.lang.String dsConvCtaEmp)
    {
        this._dsConvCtaEmp = dsConvCtaEmp;
    } //-- void setDsConvCtaEmp(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirVincConvnCtaSalarioRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.request.IncluirVincConvnCtaSalarioRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.request.IncluirVincConvnCtaSalarioRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.request.IncluirVincConvnCtaSalarioRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirvincconvnctasalario.request.IncluirVincConvnCtaSalarioRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
