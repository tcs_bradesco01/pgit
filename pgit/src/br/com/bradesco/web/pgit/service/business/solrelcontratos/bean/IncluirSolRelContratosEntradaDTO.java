/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelcontratos.bean;

/**
 * Nome: IncluirSolRelContratosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSolRelContratosEntradaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/** Atributo cdSolicitacaoPagamentoIntegrado. */
	private int cdSolicitacaoPagamentoIntegrado;
	
	/** Atributo nrSolicitacaoPagamentoIntegrado. */
	private int nrSolicitacaoPagamentoIntegrado;	
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo cdModalidadeServico. */
	private int cdModalidadeServico;
	
	/** Atributo cdTipoRelacionamento. */
	private int cdTipoRelacionamento;
	
	/** Atributo cdSituacao. */
	private int cdSituacao;	
	
	/** Atributo cdEmpresaConglomeradoDiretoriaRegional. */
	private long cdEmpresaConglomeradoDiretoriaRegional;
	
	/** Atributo cdDiretoriaRegional. */
	private int cdDiretoriaRegional;
	
	/** Atributo cdEmpresaConglomeradoGerenciaRegional. */
	private long cdEmpresaConglomeradoGerenciaRegional;
	
	/** Atributo cdGerenciaRegional. */
	private int cdGerenciaRegional;
	
	/** Atributo cdEmpresaConglomeradoAgenciaOperadora. */
	private long cdEmpresaConglomeradoAgenciaOperadora;
	
	/** Atributo cdAgenciaOperadora. */
	private int cdAgenciaOperadora;
	
	/** Atributo tipoRelatorio. */
	private int tipoRelatorio;
	
	/** Atributo cdSegmento. */
	private int cdSegmento;
	
	/** Atributo cdParticipanteGrupoEconomico. */
	private long cdParticipanteGrupoEconomico;
	
	/** Atributo cdClassAtividadeEconomica. */
	private String cdClassAtividadeEconomica;
	
	/** Atributo cdRamoAtividadeEconomica. */
	private int cdRamoAtividadeEconomica;
	
	/** Atributo cdSubRamoAtividadeEconomica. */
	private int cdSubRamoAtividadeEconomica;
	
	/** Atributo cdAtividadeEconomica. */
	private int cdAtividadeEconomica;
	
	

	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public int getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			int cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public int getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			int nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: cdModalidadeServico.
	 *
	 * @return cdModalidadeServico
	 */
	public int getCdModalidadeServico() {
		return cdModalidadeServico;
	}
	
	/**
	 * Set: cdModalidadeServico.
	 *
	 * @param cdModalidadeServico the cd modalidade servico
	 */
	public void setCdModalidadeServico(int cdModalidadeServico) {
		this.cdModalidadeServico = cdModalidadeServico;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public int getCdSituacao() {
		return cdSituacao;
	}
	
	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(int cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
	/**
	 * Get: cdTipoRelacionamento.
	 *
	 * @return cdTipoRelacionamento
	 */
	public int getCdTipoRelacionamento() {
		return cdTipoRelacionamento;
	}
	
	/**
	 * Set: cdTipoRelacionamento.
	 *
	 * @param cdTipoRelacionamento the cd tipo relacionamento
	 */
	public void setCdTipoRelacionamento(int cdTipoRelacionamento) {
		this.cdTipoRelacionamento = cdTipoRelacionamento;
	}

	/**
	 * Get: cdEmpresaConglomeradoDiretoriaRegional.
	 *
	 * @return cdEmpresaConglomeradoDiretoriaRegional
	 */
	public long getCdEmpresaConglomeradoDiretoriaRegional() {
		return cdEmpresaConglomeradoDiretoriaRegional;
	}
	
	/**
	 * Set: cdEmpresaConglomeradoDiretoriaRegional.
	 *
	 * @param cdEmpresaConglomeradoDiretoriaRegional the cd empresa conglomerado diretoria regional
	 */
	public void setCdEmpresaConglomeradoDiretoriaRegional(
			long cdEmpresaConglomeradoDiretoriaRegional) {
		this.cdEmpresaConglomeradoDiretoriaRegional = cdEmpresaConglomeradoDiretoriaRegional;
	}

	/**
	 * Get: cdDiretoriaRegional.
	 *
	 * @return cdDiretoriaRegional
	 */
	public int getCdDiretoriaRegional() {
		return cdDiretoriaRegional;
	}
	
	/**
	 * Set: cdDiretoriaRegional.
	 *
	 * @param cdDiretoriaRegional the cd diretoria regional
	 */
	public void setCdDiretoriaRegional(int cdDiretoriaRegional) {
		this.cdDiretoriaRegional = cdDiretoriaRegional;
	}
	
	/**
	 * Get: cdEmpresaConglomeradoGerenciaRegional.
	 *
	 * @return cdEmpresaConglomeradoGerenciaRegional
	 */
	public long getCdEmpresaConglomeradoGerenciaRegional() {
		return cdEmpresaConglomeradoGerenciaRegional;
	}
	
	/**
	 * Set: cdEmpresaConglomeradoGerenciaRegional.
	 *
	 * @param cdEmpresaConglomeradoGerenciaRegional the cd empresa conglomerado gerencia regional
	 */
	public void setCdEmpresaConglomeradoGerenciaRegional(
			long cdEmpresaConglomeradoGerenciaRegional) {
		this.cdEmpresaConglomeradoGerenciaRegional = cdEmpresaConglomeradoGerenciaRegional;
	}
	
	/**
	 * Get: cdGerenciaRegional.
	 *
	 * @return cdGerenciaRegional
	 */
	public int getCdGerenciaRegional() {
		return cdGerenciaRegional;
	}
	
	/**
	 * Set: cdGerenciaRegional.
	 *
	 * @param cdGerenciaRegional the cd gerencia regional
	 */
	public void setCdGerenciaRegional(int cdGerenciaRegional) {
		this.cdGerenciaRegional = cdGerenciaRegional;
	}
	
	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public int getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}
	
	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(int cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}
	
	/**
	 * Get: cdEmpresaConglomeradoAgenciaOperadora.
	 *
	 * @return cdEmpresaConglomeradoAgenciaOperadora
	 */
	public long getCdEmpresaConglomeradoAgenciaOperadora() {
		return cdEmpresaConglomeradoAgenciaOperadora;
	}
	
	/**
	 * Set: cdEmpresaConglomeradoAgenciaOperadora.
	 *
	 * @param cdEmpresaConglomeradoAgenciaOperadora the cd empresa conglomerado agencia operadora
	 */
	public void setCdEmpresaConglomeradoAgenciaOperadora(
			long cdEmpresaConglomeradoAgenciaOperadora) {
		this.cdEmpresaConglomeradoAgenciaOperadora = cdEmpresaConglomeradoAgenciaOperadora;
	}
	
	/**
	 * Get: tipoRelatorio.
	 *
	 * @return tipoRelatorio
	 */
	public int getTipoRelatorio() {
		return tipoRelatorio;
	}
	
	/**
	 * Set: tipoRelatorio.
	 *
	 * @param tipoRelatorio the tipo relatorio
	 */
	public void setTipoRelatorio(int tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}
	
	/**
	 * Get: cdSegmento.
	 *
	 * @return cdSegmento
	 */
	public int getCdSegmento() {
		return cdSegmento;
	}
	
	/**
	 * Set: cdSegmento.
	 *
	 * @param cdSegmento the cd segmento
	 */
	public void setCdSegmento(int cdSegmento) {
		this.cdSegmento = cdSegmento;
	}
	
	/**
	 * Get: cdParticipanteGrupoEconomico.
	 *
	 * @return cdParticipanteGrupoEconomico
	 */
	public long getCdParticipanteGrupoEconomico() {
		return cdParticipanteGrupoEconomico;
	}
	
	/**
	 * Set: cdParticipanteGrupoEconomico.
	 *
	 * @param cdParticipanteGrupoEconomico the cd participante grupo economico
	 */
	public void setCdParticipanteGrupoEconomico(long cdParticipanteGrupoEconomico) {
		this.cdParticipanteGrupoEconomico = cdParticipanteGrupoEconomico;
	}
	
	/**
	 * Get: cdSubRamoAtividadeEconomica.
	 *
	 * @return cdSubRamoAtividadeEconomica
	 */
	public int getCdSubRamoAtividadeEconomica() {
		return cdSubRamoAtividadeEconomica;
	}
	
	/**
	 * Set: cdSubRamoAtividadeEconomica.
	 *
	 * @param cdSubRamoAtividadeEconomica the cd sub ramo atividade economica
	 */
	public void setCdSubRamoAtividadeEconomica(int cdSubRamoAtividadeEconomica) {
		this.cdSubRamoAtividadeEconomica = cdSubRamoAtividadeEconomica;
	}
	
	/**
	 * Get: cdRamoAtividadeEconomica.
	 *
	 * @return cdRamoAtividadeEconomica
	 */
	public int getCdRamoAtividadeEconomica() {
		return cdRamoAtividadeEconomica;
	}
	
	/**
	 * Set: cdRamoAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd ramo atividade economica
	 */
	public void setCdRamoAtividadeEconomica(int cdAtividadeEconomica) {
		this.cdRamoAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: cdClassAtividadeEconomica.
	 *
	 * @return cdClassAtividadeEconomica
	 */
	public String getCdClassAtividadeEconomica() {
		return cdClassAtividadeEconomica;
	}
	
	/**
	 * Set: cdClassAtividadeEconomica.
	 *
	 * @param cdClassAtividadeEconomica the cd class atividade economica
	 */
	public void setCdClassAtividadeEconomica(String cdClassAtividadeEconomica) {
		this.cdClassAtividadeEconomica = cdClassAtividadeEconomica;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public int getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(int cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
}
