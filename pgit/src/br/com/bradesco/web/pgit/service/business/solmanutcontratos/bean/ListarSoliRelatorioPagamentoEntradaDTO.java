/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

/**
 * Nome: ListarSoliRelatorioPagamentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSoliRelatorioPagamentoEntradaDTO {

	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
	
	/** Atributo cdTipoSolicitacao. */
	private Integer cdTipoSolicitacao;
	
	/** Atributo dtInicioSolicitacao. */
	private String dtInicioSolicitacao;
	
	/** Atributo dtFimSolicitacao. */
	private String dtFimSolicitacao;
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	/**
	 * Get: cdTipoSolicitacao.
	 *
	 * @return cdTipoSolicitacao
	 */
	public Integer getCdTipoSolicitacao() {
		return cdTipoSolicitacao;
	}
	
	/**
	 * Set: cdTipoSolicitacao.
	 *
	 * @param cdTipoSolicitacao the cd tipo solicitacao
	 */
	public void setCdTipoSolicitacao(Integer cdTipoSolicitacao) {
		this.cdTipoSolicitacao = cdTipoSolicitacao;
	}
	
	/**
	 * Get: dtInicioSolicitacao.
	 *
	 * @return dtInicioSolicitacao
	 */
	public String getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}
	
	/**
	 * Set: dtInicioSolicitacao.
	 *
	 * @param dtInicioSolicitacao the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(String dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}
	
	/**
	 * Get: dtFimSolicitacao.
	 *
	 * @return dtFimSolicitacao
	 */
	public String getDtFimSolicitacao() {
		return dtFimSolicitacao;
	}
	
	/**
	 * Set: dtFimSolicitacao.
	 *
	 * @param dtFimSolicitacao the dt fim solicitacao
	 */
	public void setDtFimSolicitacao(String dtFimSolicitacao) {
		this.dtFimSolicitacao = dtFimSolicitacao;
	}
}
