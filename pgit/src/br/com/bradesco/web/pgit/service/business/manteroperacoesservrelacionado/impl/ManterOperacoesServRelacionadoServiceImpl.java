/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaBigDecimalNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoServiceConstants;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.AlterarOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.AlterarOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ConsultarTarifaCatalogoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ConsultarTarifaCatalogoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ExcluirOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ExcluirOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.IncluirOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.IncluirOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarHistoricoOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.IManterServicoRelacionadosServiceConstants;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.request.AlterarOperacaoServicoPagtoIntegradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alteraroperacaoservicopagtointegrado.response.AlterarOperacaoServicoPagtoIntegradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.request.ConsultarTarifaCatalogoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultartarifacatalogo.response.ConsultarTarifaCatalogoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.request.DetalharHistoricoOperacaoServicoPagtoIntegradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.response.DetalharHistoricoOperacaoServicoPagtoIntegradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharoperacaoservicopagtointegrado.request.DetalharOperacaoServicoPagtoIntegradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharoperacaoservicopagtointegrado.response.DetalharOperacaoServicoPagtoIntegradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluiroperacaoservicopagtointegrado.request.ExcluirOperacaoServicoPagtoIntegradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluiroperacaoservicopagtointegrado.response.ExcluirOperacaoServicoPagtoIntegradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.request.IncluirOperacaoServicoPagtoIntegradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluiroperacaoservicopagtointegrado.response.IncluirOperacaoServicoPagtoIntegradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.request.ListarHistoricoOperacaoServicoPagtoIntegradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricooperacaoservicopagtointegrado.response.ListarHistoricoOperacaoServicoPagtoIntegradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.request.ListarOperacaoServicoPagtoIntegradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.response.ListarOperacaoServicoPagtoIntegradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionado.request.ListarServicoRelacionadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionado.response.ListarServicoRelacionadoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


// TODO: Auto-generated Javadoc
/**
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterOperacoesServRelacionado
 * </p>.
 *
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterOperacoesServRelacionadoServiceImpl implements IManterOperacoesServRelacionadoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc).
	 *
	 * @param entrada the entrada
	 * @return the list< listar operacao servico pagto integrado saida dt o>
	 * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#listarOperacaoServicoPagtoIntegrado(br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarOperacaoServicoPagtoIntegradoEntradaDTO)
	 */
	public List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> listarOperacaoServicoPagtoIntegrado(ListarOperacaoServicoPagtoIntegradoEntradaDTO entrada){
		List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> listaSaida = new ArrayList<ListarOperacaoServicoPagtoIntegradoSaidaDTO>();
		ListarOperacaoServicoPagtoIntegradoRequest request = new ListarOperacaoServicoPagtoIntegradoRequest();
		ListarOperacaoServicoPagtoIntegradoResponse response = new ListarOperacaoServicoPagtoIntegradoResponse();

		request.setMaxOcorrencias(IManterOperacoesServRelacionadoServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);
		request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdprodutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));

		response = getFactoryAdapter().getListarOperacaoServicoPagtoIntegradoPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ListarOperacaoServicoPagtoIntegradoSaidaDTO saida = new ListarOperacaoServicoPagtoIntegradoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdprodutoServicoOperacao(response.getOcorrencias(i).getCdprodutoServicoOperacao());
			saida.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			saida.setCdOperacaoProdutoServico(response.getOcorrencias(i).getCdOperacaoProdutoServico());
			saida.setDsTipoServico(response.getOcorrencias(i).getDsTipoServico());
			saida.setDsModalidadeServico(response.getOcorrencias(i).getDsModalidadeServico());
			saida.setDsOperacaoCatalogo(response.getOcorrencias(i).getDsOperacaoCatalogo());
			saida.setCdOperacaoServicoIntegrado(response.getOcorrencias(i).getCdOperacaoServicoIntegrado() == 0 ? "" : String.valueOf(response.getOcorrencias(i).getCdOperacaoServicoIntegrado()));
			saida.setTipoServicoFormatado(PgitUtil.concatenarCampos(saida.getCdprodutoServicoOperacao(), saida.getDsTipoServico()));
			saida.setModalidadeServicoFormatado(PgitUtil.concatenarCampos(saida.getCdProdutoOperacaoRelacionado(), saida.getDsModalidadeServico()));
			saida.setOperacaoCatalogoFormatado(PgitUtil.concatenarCampos(saida.getCdOperacaoProdutoServico(), saida.getDsOperacaoCatalogo()));
			saida.setDsNatureza(response.getOcorrencias(i).getDsNatureza());

			listaSaida.add(saida);
		}

		return listaSaida;
	}
	
	/**
	 * (non-Javadoc).
	 *
	 * @param entrada the entrada
	 * @return the detalhar operacao servico pagto integrado saida dto
	 * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#detalharOperacaoServicoPagtoIntegrado(br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharOperacaoServicoPagtoIntegradoEntradaDTO)
	 */
	public DetalharOperacaoServicoPagtoIntegradoSaidaDTO detalharOperacaoServicoPagtoIntegrado(DetalharOperacaoServicoPagtoIntegradoEntradaDTO entrada){
		DetalharOperacaoServicoPagtoIntegradoSaidaDTO saida = new DetalharOperacaoServicoPagtoIntegradoSaidaDTO();
		DetalharOperacaoServicoPagtoIntegradoRequest request = new DetalharOperacaoServicoPagtoIntegradoRequest();
		DetalharOperacaoServicoPagtoIntegradoResponse response = new DetalharOperacaoServicoPagtoIntegradoResponse();

		request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdprodutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));
		
		response = getFactoryAdapter().getDetalharOperacaoServicoPagtoIntegradoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdOperacaoServicoIntegrado(response.getCdOperacaoServicoIntegrado());
		
		saida.setCdOperacaoCanalIncusao(response.getCdOperacaoCanalIncusao());
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao() == 0 ? "" : String.valueOf(response.getCdTipoCanalInclusao()));
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
		saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao() == 0 ? "" : String.valueOf(response.getCdTipoCanalManutencao()));
		saida.setCdUsuarioManutencao(PgitUtil.verificaStringNula(response.getCdUsuarioManutencao()));
		saida.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
		saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		
		saida.setCdNatureza(response.getCdNatureza());
		
		saida.setUsuarioInclusaoFormatado(PgitUtil.verificaUsuarioInternoExterno(saida.getCdUsuarioInclusao(), saida.getCdUsuarioInclusaoExterno()));		
		saida.setComplementoInclusaoFormatado(saida.getCdOperacaoCanalIncusao() == null || saida.getCdOperacaoCanalIncusao().equals("0")? "" : saida.getCdOperacaoCanalIncusao());
		saida.setUsuarioManutencaoFormatado(PgitUtil.verificaUsuarioInternoExterno(String.valueOf(saida.getCdUsuarioManutencao()), saida.getCdUsuarioManutencaoExterno()));
		saida.setComplementoManutencaoFormatado(saida.getCdOperacaoCanalManutencao() == null || saida.getCdOperacaoCanalManutencao().equals("0") ? "" : saida.getCdOperacaoCanalManutencao());
		
		saida.setCdNaturezaOperacaoPagamento(response.getCdNaturezaOperacaoPagamento());
		saida.setCdTipoAlcadaTarifa(response.getCdTipoAlcadaTarifa());
		saida.setDsTipoAlcadaTarifa(response.getDsTipoAlcadaTarifa());
		saida.setVrAlcadaTarifaAgencia(response.getVrAlcadaTarifaAgencia());
		saida.setPcAlcadaTarifaAgencia(response.getPcAlcadaTarifaAgencia());
		saida.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saida.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());

		return saida;
	}
	
	/**
	 * (non-Javadoc).
	 *
	 * @param entrada the entrada
	 * @return the incluir operacao servico pagto integrado saida dto
	 * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#incluirOperacaoServicoPagtoIntegrado(br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.IncluirOperacaoServicoPagtoIntegradoEntradaDTO)
	 */
	public IncluirOperacaoServicoPagtoIntegradoSaidaDTO incluirOperacaoServicoPagtoIntegrado(IncluirOperacaoServicoPagtoIntegradoEntradaDTO entrada){
		IncluirOperacaoServicoPagtoIntegradoSaidaDTO saida = new IncluirOperacaoServicoPagtoIntegradoSaidaDTO();
		IncluirOperacaoServicoPagtoIntegradoRequest request = new IncluirOperacaoServicoPagtoIntegradoRequest();
		IncluirOperacaoServicoPagtoIntegradoResponse response = new IncluirOperacaoServicoPagtoIntegradoResponse();

		request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdprodutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));
		request.setCdOperacaoServicoIntegrado(PgitUtil.verificaIntegerNulo(entrada.getCdOperacaoServicoIntegrado()));
		request.setCdNaturezaOperacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdNaturezaOperacaoPagamento()));
		request.setCdTipoAlcadaTarifa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoAlcadaTarifa()));
		request.setVrAlcadaTarifaAgencia(PgitUtil.verificaBigDecimalNulo(entrada.getVrAlcadaTarifaAgencia()));
		request.setPcAlcadaTarifaAgencia(PgitUtil.verificaBigDecimalNulo(entrada.getPcAlcadaTarifaAgencia()));

		response = getFactoryAdapter().getIncluirOperacaoServicoPagtoIntegradoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
	
	/**
	 * (non-Javadoc).
	 *
	 * @param entrada the entrada
	 * @return the excluir operacao servico pagto integrado saida dto
	 * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#excluirOperacaoServicoPagtoIntegrado(br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ExcluirOperacaoServicoPagtoIntegradoEntradaDTO)
	 */
	public ExcluirOperacaoServicoPagtoIntegradoSaidaDTO excluirOperacaoServicoPagtoIntegrado(ExcluirOperacaoServicoPagtoIntegradoEntradaDTO entrada){
		ExcluirOperacaoServicoPagtoIntegradoSaidaDTO saida = new ExcluirOperacaoServicoPagtoIntegradoSaidaDTO();
		ExcluirOperacaoServicoPagtoIntegradoRequest request = new ExcluirOperacaoServicoPagtoIntegradoRequest();
		ExcluirOperacaoServicoPagtoIntegradoResponse response = new ExcluirOperacaoServicoPagtoIntegradoResponse();

		request.setCdprodutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdprodutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacaoRelacionado()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));

		response = getFactoryAdapter().getExcluirOperacaoServicoPagtoIntegradoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

    /**
     * (non-Javadoc).
     *
     * @param entrada the entrada
     * @return the consultar tarifa catalogo saida dto
     * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#consultarTarifaCatalogo(br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ConsultarTarifaCatalogoEntradaDTO)
     */
    public ConsultarTarifaCatalogoSaidaDTO consultarTarifaCatalogo(ConsultarTarifaCatalogoEntradaDTO entrada) {
        ConsultarTarifaCatalogoSaidaDTO saida = new ConsultarTarifaCatalogoSaidaDTO();
        ConsultarTarifaCatalogoRequest request = new ConsultarTarifaCatalogoRequest();
        ConsultarTarifaCatalogoResponse response = null;
        
        request.setCdProdutoServicoOperacional(verificaIntegerNulo(entrada.getCdProdutoServicoOperacional()));
        request.setCdProdutoOperacionalRelacionado(verificaIntegerNulo(entrada.getCdProdutoOperacionalRelacionado()));
        request.setCdOperacaoProdutoServico(verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));
        
        response = getFactoryAdapter().getConsultarTarifaCatalogoPDCAdapter().invokeProcess(request);
        
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setCdCondicaoEconomica(response.getCdCondicaoEconomica());
        saida.setVrTarifaMaximaNegociada(response.getVrTarifaMaximaNegociada());
        saida.setVrTarifaMinimaNegociada(response.getVrTarifaMinimaNegociada());
        
        return saida;
    }

    /**
     * (non-Javadoc).
     *
     * @param entrada the entrada
     * @return the detalhar historico operacao servico pagto integrado saida dto
     * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#detalharHistoricoOperacaoServicoPagtoIntegrado(br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO)
     */
    public DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO detalharHistoricoOperacaoServicoPagtoIntegrado(
        DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO entrada) {
        
        DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO saida = new DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO();
        DetalharHistoricoOperacaoServicoPagtoIntegradoRequest request = new DetalharHistoricoOperacaoServicoPagtoIntegradoRequest();
        DetalharHistoricoOperacaoServicoPagtoIntegradoResponse response = null;
        
        request.setCdProdutoServicoOperacional(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacional()));
        request.setCdProdutoOperacionalRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacionalRelacionado()));
        request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));
        request.setHrInclusaoRegistroHistorico(PgitUtil.verificaStringNula(entrada.getHrInclusaoRegistroHistorico()));
        
        response = getFactoryAdapter().getDetalharHistoricoOperacaoServicoPagtoIntegradoPDCAdapter().invokeProcess(request);
        
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setCdOperacaoServicoPagamentoIntegrado(response.getCdOperacaoServicoPagamentoIntegrado());
        saida.setCdNaturezaOperacaoPagamento(response.getCdNaturezaOperacaoPagamento());
        saida.setDsNaturezaOperacaoPagamento(response.getDsNaturezaOperacaoPagamento());
        saida.setCdTipoCondcTarifa(response.getCdTipoCondcTarifa());
        saida.setDsTipoCondcTarifa(response.getDsTipoCondcTarifa());
        saida.setVrAlcadaTarifaAgencia(response.getVrAlcadaTarifaAgencia());
        saida.setPcAlcadaTarifaAgencia(response.getPcAlcadaTarifaAgencia());
        saida.setCdOperCanalInclusao(response.getCdOperCanalInclusao());
        saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
        saida.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
        saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
        saida.setCdUsuarioInclusaoExterno(response.getCdUsuarioInclusaoExterno());
        saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
        saida.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
        saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
        saida.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
        saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
        saida.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
        saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
        
        return saida;
    }

    /**
     * (non-Javadoc).
     *
     * @param entrada the entrada
     * @return the listar historico operacao servico pagto integrado saida dto
     * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#listarHistoricoOperacaoServicoPagtoIntegrado(br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO)
     */
    public ListarHistoricoOperacaoServicoPagtoIntegradoSaidaDTO listarHistoricoOperacaoServicoPagtoIntegrado(
        ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO entrada) {
        
        ListarHistoricoOperacaoServicoPagtoIntegradoSaidaDTO saida = new ListarHistoricoOperacaoServicoPagtoIntegradoSaidaDTO();
        ListarHistoricoOperacaoServicoPagtoIntegradoRequest request = new ListarHistoricoOperacaoServicoPagtoIntegradoRequest();
        ListarHistoricoOperacaoServicoPagtoIntegradoResponse response = null;
        
        request.setQtMaximaRegistro(50);
        request.setCdProdutoServicoOperacional(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacional()));
        request.setCdProdutoOperacionalRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoOperacionalRelacionado()));
        request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));
        request.setDtInicialPesquisaHistorico(FormatarData.formataDiaMesAnoToPdc(entrada.getDtInicialPesquisaHistorico()));
        request.setDtFinalPesquisaHistorico(FormatarData.formataDiaMesAnoToPdc(entrada.getDtFinalPesquisaHistorico()));
        
        response = getFactoryAdapter().getListarHistoricoOperacaoServicoPagtoIntegradoPDCAdapter().invokeProcess(request);
        
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setQtRegistroSaida(response.getQtRegistroSaida());
        saida.setOcorrencias(new ArrayList<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO>());
        
        for (int i = 0; i < response.getQtRegistroSaida(); i++) {
            ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO ocorrencia = new ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO();
            
            ocorrencia.setCdProdutoServicoOperacional(response.getOcorrencias(i).getCdProdutoServicoOperacional());
            ocorrencia.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
            ocorrencia.setCdProdutoOperacionalRelacionado(response.getOcorrencias(i).getCdProdutoOperacionalRelacionado());
            ocorrencia.setDsProdutoOperacaoRelacionado(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
            ocorrencia.setCdOperacaoProdutoServico(response.getOcorrencias(i).getCdOperacaoProdutoServico());
            ocorrencia.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
            ocorrencia.setHrInclusaoRegistroHistorico(response.getOcorrencias(i).getHrInclusaoRegistroHistorico());
            ocorrencia.setCdUsuarioManutencao(response.getOcorrencias(i).getCdUsuarioManutencao());
            ocorrencia.setCdUsuarioManutencaoExterno(response.getOcorrencias(i).getCdUsuarioManutencaoExterno());
            ocorrencia.setCdIndicadorTipoManutencao(response.getOcorrencias(i).getCdIndicadorTipoManutencao());
            ocorrencia.setDsIndicadorTipoManutencao(response.getOcorrencias(i).getDsIndicadorTipoManutencao());
            
            saida.getOcorrencias().add(ocorrencia);
        }
        
        return saida;
    }

    /**
     * (non-Javadoc).
     *
     * @param entrada the entrada
     * @return the alterar operacao servico pagto integrado saida dto
     * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#alterarOperacaoServicoPagtoIntegrado(br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.AlterarOperacaoServicoPagtoIntegradoEntradaDTO)
     */
    public AlterarOperacaoServicoPagtoIntegradoSaidaDTO alterarOperacaoServicoPagtoIntegrado(
        AlterarOperacaoServicoPagtoIntegradoEntradaDTO entrada) {
        
        AlterarOperacaoServicoPagtoIntegradoSaidaDTO saida = new AlterarOperacaoServicoPagtoIntegradoSaidaDTO();
        AlterarOperacaoServicoPagtoIntegradoRequest request = new AlterarOperacaoServicoPagtoIntegradoRequest();
        AlterarOperacaoServicoPagtoIntegradoResponse response = null;
        
        request.setCdProdutoServicoOperacional(verificaIntegerNulo(entrada.getCdProdutoServicoOperacional()));
        request.setCdProdutoOperacionalRelacionado(verificaIntegerNulo(entrada.getCdProdutoOperacionalRelacionado()));
        request.setCdOperacaoProdutoServico(verificaIntegerNulo(entrada.getCdOperacaoProdutoServico()));
        request.setCdOperacaoServicoPagamentoIntegrado(verificaIntegerNulo(entrada.getCdOperacaoServicoPagamentoIntegrado()));
        request.setCdNaturezaOperacaoPagamento(verificaIntegerNulo(entrada.getCdNaturezaOperacaoPagamento()));
        request.setCdTipoAlcadaTarifa(verificaIntegerNulo(entrada.getCdTipoAlcadaTarifa()));
        request.setVrAlcadaTarifaAgencia(verificaBigDecimalNulo(entrada.getVrAlcadaTarifaAgencia()));
        request.setPcAlcadaTarifaAgencia(verificaBigDecimalNulo(entrada.getPcAlcadaTarifaAgencia()));
        
        response = getFactoryAdapter().getAlterarOperacaoServicoPagtoIntegradoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());

        return saida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService#listarServicoRelacionadoDTO(br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoEntradaDTO)
     */
    public ListarServicoRelacionadoSaidaDTO listarServicoRelacionadoDTO(
        ListarServicoRelacionadoEntradaDTO entradaDto) {

        ListarServicoRelacionadoRequest request = new ListarServicoRelacionadoRequest();
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entradaDto.getTipoServico()));
        request.setCdRelacionamentoProduto(verificaIntegerNulo(entradaDto.getTipoRelacionamento()));
        request.setCdProdutoOperacaoRelacionado(verificaIntegerNulo(entradaDto.getModalidadeServico()));
        request.setQtOcorrencias(verificaIntegerNulo(entradaDto.getQtOcorrencias()));
    
        ListarServicoRelacionadoResponse response = 
            getFactoryAdapter().getListarServicoRelacionadoPDCAdapter().invokeProcess(request);
    
        ListarServicoRelacionadoSaidaDTO saidaDTO = new ListarServicoRelacionadoSaidaDTO();
    
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());
    
        if(response.getOcorrenciasCount() >= 1){ 
        
            saidaDTO.setCodModalidadeServico(response.getOcorrencias(0).getCdProdutoOperacaoRelacionado());
            saidaDTO.setCodNaturezaServico(response.getOcorrencias(0).getCdNaturezaOperacaoPagamento());
            saidaDTO.setCodRelacionamentoProd(response.getOcorrencias(0).getCdRelacionamentoProduto());
            saidaDTO.setCodServicoComposto(response.getOcorrencias(0).getCdServicoCompostoPagamento());
            saidaDTO.setCodTipoServico(response.getOcorrencias(0).getCdProdutoServicoOperacao());
            saidaDTO.setDsModalidadeServico(response.getOcorrencias(0).getDsProdutoOperacaoRelacionado());
            saidaDTO.setDsServicoComposto(response.getOcorrencias(0).getDsServicoCompostoPagamento());
            saidaDTO.setDsTipoServico(response.getOcorrencias(0).getDsProdutoServicoOperacao());
        
            saidaDTO.setTipoServicoFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(0)
                .getCdProdutoServicoOperacao(), response.getOcorrencias(0).getDsProdutoServicoOperacao()));
            saidaDTO.setModalidadeFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(0)
                .getCdProdutoOperacaoRelacionado(), response.getOcorrencias(0).getDsProdutoOperacaoRelacionado()));
            saidaDTO.setServicoCompostoFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(0)
                .getCdServicoCompostoPagamento(), response.getOcorrencias(0).getDsServicoCompostoPagamento()));
            
        }
    
        return saidaDTO;
    }
}

