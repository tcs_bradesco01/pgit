/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.IRegistrarAssinaturaFormAltContratoService;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.ConsultarAditivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.ConsultarAditivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarAssinaturaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarAssinaturaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.request.ConsultarAditivoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.response.ConsultarAditivoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.request.RegistrarFormalizacaoManContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.registrarformalizacaomancontrato.response.RegistrarFormalizacaoManContratoResponse;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: RegistrarAssinaturaFormAltContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class RegistrarAssinaturaFormAltContratoServiceImpl implements IRegistrarAssinaturaFormAltContratoService {

    /**
     * Construtor.
     */
    public RegistrarAssinaturaFormAltContratoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.IRegistrarAssinaturaFormAltContratoService#registrarAssinaturaContrato(br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarAssinaturaContratoEntradaDTO)
	 */
	public RegistrarAssinaturaContratoSaidaDTO registrarAssinaturaContrato(RegistrarAssinaturaContratoEntradaDTO entrada){
		RegistrarAssinaturaContratoSaidaDTO saida = new RegistrarAssinaturaContratoSaidaDTO();

		/* RegistrarAssinaturaContratoRequest request = new RegistrarAssinaturaContratoRequest();
		RegistrarAssinaturaContratoResponse response = new RegistrarAssinaturaContratoResponse();
		
		request.setCdMotivoSituacaoContrato(entrada.getCdMotivoSituacaoAditivo());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setDtAssinaturaContrato(entrada.getDtAssinaturaAditivo());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
		
		response = getFactoryAdapter().getRegistrarAssinaturaContratoPDCAdapter().invokeProcess(request);
		
		/*saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setCdMotivoSituacaoContrato(response.getCdMotivoSituacaoContrato());
		saida.setCdPessoaJuridicaContrato(response.getCdPessoaJuridicaContrato());
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saida.setCdUsuarioManutencaoExterno(response.getCdUsuarioManutencaoExterno());
		saida.setCodMensagem(response.getCodMensagem());
		saida.setDtAssinaturaContrato(response.getDtAssinaturaContrato());
		saida.setMensagem(response.getMensagem());
		saida.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
		saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio()); */
		
		return saida;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.IRegistrarAssinaturaFormAltContratoService#consultarAditivoContrato(br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.ConsultarAditivoContratoEntradaDTO)
	 */
	public List<ConsultarAditivoContratoSaidaDTO> consultarAditivoContrato(ConsultarAditivoContratoEntradaDTO  entradaDTO){
		List<ConsultarAditivoContratoSaidaDTO> listaRetorno = new ArrayList<ConsultarAditivoContratoSaidaDTO>();
		ConsultarAditivoContratoRequest request = new 	ConsultarAditivoContratoRequest();
		ConsultarAditivoContratoResponse response = new ConsultarAditivoContratoResponse();
		
		request.setCdAcesso(entradaDTO.getCdAcesso());
		request.setCdPessoaJuridicaContrato(entradaDTO.getCdPessoaJuridicaContrato());
		request.setCdSituacaoAditivoContrato(entradaDTO.getCdSituacaoAditivoContrato());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setDtFimInclusaoContrato(entradaDTO.getDtFimInclusaoContrato());
		request.setDtInicioInclusaoContrato(entradaDTO.getDtInicioInclusaoContrato());
		request.setNrAditivoContratoNegocio(entradaDTO.getNrAditivoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entradaDTO.getNrSequenciaContratoNegocio());
		request.setNumeroOcorrencias(30);
		
		response = getFactoryAdapter().getConsultarAditivoContratoPDCAdapter().invokeProcess(request);
		
		ConsultarAditivoContratoSaidaDTO saida;
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formato = new SimpleDateFormat("dd.mm.yyyy");

		for(int i=0;i<response.getOcorrenciasCount();i++){
			saida = new ConsultarAditivoContratoSaidaDTO();
			saida.setCdMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdSituacaoAditivoContrato(response.getOcorrencias(i).getCdSituacaoAditivoContrato());
			saida.setDsSituacaoAditivoContrato(response.getOcorrencias(i).getDsSitucaoAditivoContrato());
			//saida.setDtInclusaoAditivoContrato(response.getOcorrencias(i).getDtInclusaoAditivoContrato());
			saida.setHrInclusaoAditivoContrato(response.getOcorrencias(i).getHrInclusaoAditivoContrato());
			saida.setNrAditivoContratoNegocio(response.getOcorrencias(i).getNrAditivoContratoNegocio());
			saida.setDtInclusaoAditivoContrato(FormatarData.formatarData(response.getOcorrencias(i).getDtInclusaoAditivoContrato()));			
			saida.setDataHoraInclusaoFormatada(saida.getDtInclusaoAditivoContrato() + " - " + saida.getHrInclusaoAditivoContrato());

			listaRetorno.add(saida);
		}
		
		return listaRetorno;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.IRegistrarAssinaturaFormAltContratoService#registrarFormalizacaoManContrato(br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoEntradaDTO)
	 */
	public RegistrarFormalizacaoManContratoSaidaDTO registrarFormalizacaoManContrato (RegistrarFormalizacaoManContratoEntradaDTO entrada){
		
		RegistrarFormalizacaoManContratoSaidaDTO saida = new RegistrarFormalizacaoManContratoSaidaDTO();
		RegistrarFormalizacaoManContratoRequest request = new RegistrarFormalizacaoManContratoRequest();
		RegistrarFormalizacaoManContratoResponse response = new RegistrarFormalizacaoManContratoResponse();
				
		
		
		request.setCdContratoNegocioPagamento(entrada.getCdContratoNegocioPagamento());
		request.setCdPessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setDtAssinaturaAditivo(entrada.getDtAssinaturaAditivo());
		request.setNrAditivoContratoNegocio(entrada.getNrAditivoContratoNegocio());
		request.setNrSequenciaContratoNEgocio(entrada.getNrSequenciaContratoNegocio());
		
		response = getFactoryAdapter().getRegistrarFormalizacaoManContratoPDCAdapter().invokeProcess(request);
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());	
		
		return saida;
		
	}
    
}

