/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarAditivoContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarAditivoContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdAcesso
     */
    private int _cdAcesso = 0;

    /**
     * keeps track of state for field: _cdAcesso
     */
    private boolean _has_cdAcesso;

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _cdSituacaoAditivoContrato
     */
    private int _cdSituacaoAditivoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoAditivoContrato
     */
    private boolean _has_cdSituacaoAditivoContrato;

    /**
     * Field _dtInicioInclusaoContrato
     */
    private java.lang.String _dtInicioInclusaoContrato;

    /**
     * Field _dtFimInclusaoContrato
     */
    private java.lang.String _dtFimInclusaoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarAditivoContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.request.ConsultarAditivoContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcesso
     * 
     */
    public void deleteCdAcesso()
    {
        this._has_cdAcesso= false;
    } //-- void deleteCdAcesso() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoAditivoContrato
     * 
     */
    public void deleteCdSituacaoAditivoContrato()
    {
        this._has_cdSituacaoAditivoContrato= false;
    } //-- void deleteCdSituacaoAditivoContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdAcesso'.
     * 
     * @return int
     * @return the value of field 'cdAcesso'.
     */
    public int getCdAcesso()
    {
        return this._cdAcesso;
    } //-- int getCdAcesso() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoAditivoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoAditivoContrato'.
     */
    public int getCdSituacaoAditivoContrato()
    {
        return this._cdSituacaoAditivoContrato;
    } //-- int getCdSituacaoAditivoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtFimInclusaoContrato'.
     * 
     * @return String
     * @return the value of field 'dtFimInclusaoContrato'.
     */
    public java.lang.String getDtFimInclusaoContrato()
    {
        return this._dtFimInclusaoContrato;
    } //-- java.lang.String getDtFimInclusaoContrato() 

    /**
     * Returns the value of field 'dtInicioInclusaoContrato'.
     * 
     * @return String
     * @return the value of field 'dtInicioInclusaoContrato'.
     */
    public java.lang.String getDtInicioInclusaoContrato()
    {
        return this._dtInicioInclusaoContrato;
    } //-- java.lang.String getDtInicioInclusaoContrato() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcesso()
    {
        return this._has_cdAcesso;
    } //-- boolean hasCdAcesso() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoAditivoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoAditivoContrato()
    {
        return this._has_cdSituacaoAditivoContrato;
    } //-- boolean hasCdSituacaoAditivoContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcesso'.
     * 
     * @param cdAcesso the value of field 'cdAcesso'.
     */
    public void setCdAcesso(int cdAcesso)
    {
        this._cdAcesso = cdAcesso;
        this._has_cdAcesso = true;
    } //-- void setCdAcesso(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoAditivoContrato'.
     * 
     * @param cdSituacaoAditivoContrato the value of field
     * 'cdSituacaoAditivoContrato'.
     */
    public void setCdSituacaoAditivoContrato(int cdSituacaoAditivoContrato)
    {
        this._cdSituacaoAditivoContrato = cdSituacaoAditivoContrato;
        this._has_cdSituacaoAditivoContrato = true;
    } //-- void setCdSituacaoAditivoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dtFimInclusaoContrato'.
     * 
     * @param dtFimInclusaoContrato the value of field
     * 'dtFimInclusaoContrato'.
     */
    public void setDtFimInclusaoContrato(java.lang.String dtFimInclusaoContrato)
    {
        this._dtFimInclusaoContrato = dtFimInclusaoContrato;
    } //-- void setDtFimInclusaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioInclusaoContrato'.
     * 
     * @param dtInicioInclusaoContrato the value of field
     * 'dtInicioInclusaoContrato'.
     */
    public void setDtInicioInclusaoContrato(java.lang.String dtInicioInclusaoContrato)
    {
        this._dtInicioInclusaoContrato = dtInicioInclusaoContrato;
    } //-- void setDtInicioInclusaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarAditivoContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.request.ConsultarAditivoContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.request.ConsultarAditivoContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.request.ConsultarAditivoContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.request.ConsultarAditivoContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
