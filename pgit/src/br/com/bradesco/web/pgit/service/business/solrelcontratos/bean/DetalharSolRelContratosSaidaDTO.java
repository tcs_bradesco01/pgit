/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelcontratos.bean;

/**
 * Nome: DetalharSolRelContratosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolRelContratosSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdSolicitacaoPagamentoIntegrado. */
	private int cdSolicitacaoPagamentoIntegrado;
	
	/** Atributo nrSolicitacaoPagamentoIntegrado. */
	private int nrSolicitacaoPagamentoIntegrado;
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdModalidadeServico. */
	private int cdModalidadeServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo cdTipoRelacionamento. */
	private int cdTipoRelacionamento;
	
	/** Atributo dataHoraSolicitacao. */
	private String dataHoraSolicitacao;
	
	/** Atributo cdSituacao. */
	private int cdSituacao;
	
	/** Atributo cdEmpresaConglomeradoDiretoriaRegional. */
	private long cdEmpresaConglomeradoDiretoriaRegional;
	
	/** Atributo dsEmpresaConglomeradoDiretoriaRegional. */
	private String dsEmpresaConglomeradoDiretoriaRegional;
	
	/** Atributo cdDiretoriaRegional. */
	private int cdDiretoriaRegional;
	
	/** Atributo dsDiretoriaRegional. */
	private String dsDiretoriaRegional;
	
	/** Atributo cdEmpresaConglomeradoGerenciaRegional. */
	private long cdEmpresaConglomeradoGerenciaRegional;
	
	/** Atributo dsEmpresaConglomeradoGerenciaRegional. */
	private String dsEmpresaConglomeradoGerenciaRegional;
	
	/** Atributo cdGerenciaRegional. */
	private int cdGerenciaRegional;
	
	/** Atributo dsGerenciaRegional. */
	private String dsGerenciaRegional;
	
	/** Atributo cdEmpresaConglomeradoAgenciaOperadora. */
	private long cdEmpresaConglomeradoAgenciaOperadora;
	
	/** Atributo dsEmpresaConglomeradoAgenciaOperadora. */
	private String dsEmpresaConglomeradoAgenciaOperadora;
	
	/** Atributo cdAgenciaOperadora. */
	private int cdAgenciaOperadora;
	
	/** Atributo dsAgenciaOperadora. */
	private String dsAgenciaOperadora;
	
	/** Atributo tipoRelatorio. */
	private int tipoRelatorio;
	
	/** Atributo dsTipoRelatorio. */
	private String dsTipoRelatorio;
	
	/** Atributo cdSegmento. */
	private int cdSegmento;
	
	/** Atributo dsSegmento. */
	private String dsSegmento;
	
	/** Atributo cdParticipanteGrupoEconomico. */
	private long cdParticipanteGrupoEconomico;
	
	/** Atributo dsParticipanteGrupoEconomico. */
	private String dsParticipanteGrupoEconomico;
	
	/** Atributo cdClassAtividadeEconomica. */
	private String cdClassAtividadeEconomica;
	
	/** Atributo cdRamoAtividadeEconomica. */
	private int cdRamoAtividadeEconomica;
	
	/** Atributo cdSubRamoAtividadeEconomica. */
	private int cdSubRamoAtividadeEconomica;
	
	/** Atributo cdAtividadeEconomica. */
	private int cdAtividadeEconomica;
	
	/** Atributo dsAtividadeEconomica. */
	private String dsAtividadeEconomica;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo dsRamo. */
	private String dsRamo;
	
	/** Atributo dsSubRamo. */
	private String dsSubRamo;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public int getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			int cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public int getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			int nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: cdModalidadeServico.
	 *
	 * @return cdModalidadeServico
	 */
	public int getCdModalidadeServico() {
		return cdModalidadeServico;
	}
	
	/**
	 * Set: cdModalidadeServico.
	 *
	 * @param cdModalidadeServico the cd modalidade servico
	 */
	public void setCdModalidadeServico(int cdModalidadeServico) {
		this.cdModalidadeServico = cdModalidadeServico;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}
	
	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public int getCdSituacao() {
		return cdSituacao;
	}
	
	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(int cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
	/**
	 * Get: cdTipoRelacionamento.
	 *
	 * @return cdTipoRelacionamento
	 */
	public int getCdTipoRelacionamento() {
		return cdTipoRelacionamento;
	}
	
	/**
	 * Set: cdTipoRelacionamento.
	 *
	 * @param cdTipoRelacionamento the cd tipo relacionamento
	 */
	public void setCdTipoRelacionamento(int cdTipoRelacionamento) {
		this.cdTipoRelacionamento = cdTipoRelacionamento;
	}
	
	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public String getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}
	
	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(String dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}
	
	/**
	 * Get: cdEmpresaConglomeradoDiretoriaRegional.
	 *
	 * @return cdEmpresaConglomeradoDiretoriaRegional
	 */
	public long getCdEmpresaConglomeradoDiretoriaRegional() {
		return cdEmpresaConglomeradoDiretoriaRegional;
	}
	
	/**
	 * Set: cdEmpresaConglomeradoDiretoriaRegional.
	 *
	 * @param cdEmpresaConglomeradoDiretoriaRegional the cd empresa conglomerado diretoria regional
	 */
	public void setCdEmpresaConglomeradoDiretoriaRegional(
			long cdEmpresaConglomeradoDiretoriaRegional) {
		this.cdEmpresaConglomeradoDiretoriaRegional = cdEmpresaConglomeradoDiretoriaRegional;
	}
	
	/**
	 * Get: dsEmpresaConglomeradoDiretoriaRegional.
	 *
	 * @return dsEmpresaConglomeradoDiretoriaRegional
	 */
	public String getDsEmpresaConglomeradoDiretoriaRegional() {
		return dsEmpresaConglomeradoDiretoriaRegional;
	}
	
	/**
	 * Set: dsEmpresaConglomeradoDiretoriaRegional.
	 *
	 * @param dsEmpresaConglomeradoDiretoriaRegional the ds empresa conglomerado diretoria regional
	 */
	public void setDsEmpresaConglomeradoDiretoriaRegional(
			String dsEmpresaConglomeradoDiretoriaRegional) {
		this.dsEmpresaConglomeradoDiretoriaRegional = dsEmpresaConglomeradoDiretoriaRegional;
	}
	
	/**
	 * Get: cdDiretoriaRegional.
	 *
	 * @return cdDiretoriaRegional
	 */
	public int getCdDiretoriaRegional() {
		return cdDiretoriaRegional;
	}
	
	/**
	 * Set: cdDiretoriaRegional.
	 *
	 * @param cdDiretoriaRegional the cd diretoria regional
	 */
	public void setCdDiretoriaRegional(int cdDiretoriaRegional) {
		this.cdDiretoriaRegional = cdDiretoriaRegional;
	}
	
	/**
	 * Get: dsDiretoriaRegional.
	 *
	 * @return dsDiretoriaRegional
	 */
	public String getDsDiretoriaRegional() {
		return dsDiretoriaRegional;
	}
	
	/**
	 * Set: dsDiretoriaRegional.
	 *
	 * @param dsDiretoriaRegional the ds diretoria regional
	 */
	public void setDsDiretoriaRegional(String dsDiretoriaRegional) {
		this.dsDiretoriaRegional = dsDiretoriaRegional;
	}
	
	/**
	 * Get: cdEmpresaConglomeradoGerenciaRegional.
	 *
	 * @return cdEmpresaConglomeradoGerenciaRegional
	 */
	public long getCdEmpresaConglomeradoGerenciaRegional() {
		return cdEmpresaConglomeradoGerenciaRegional;
	}
	
	/**
	 * Set: cdEmpresaConglomeradoGerenciaRegional.
	 *
	 * @param cdEmpresaConglomeradoGerenciaRegional the cd empresa conglomerado gerencia regional
	 */
	public void setCdEmpresaConglomeradoGerenciaRegional(
			long cdEmpresaConglomeradoGerenciaRegional) {
		this.cdEmpresaConglomeradoGerenciaRegional = cdEmpresaConglomeradoGerenciaRegional;
	}
	
	/**
	 * Get: dsEmpresaConglomeradoGerenciaRegional.
	 *
	 * @return dsEmpresaConglomeradoGerenciaRegional
	 */
	public String getDsEmpresaConglomeradoGerenciaRegional() {
		return dsEmpresaConglomeradoGerenciaRegional;
	}
	
	/**
	 * Set: dsEmpresaConglomeradoGerenciaRegional.
	 *
	 * @param dsEmpresaConglomeradoGerenciaRegional the ds empresa conglomerado gerencia regional
	 */
	public void setDsEmpresaConglomeradoGerenciaRegional(
			String dsEmpresaConglomeradoGerenciaRegional) {
		this.dsEmpresaConglomeradoGerenciaRegional = dsEmpresaConglomeradoGerenciaRegional;
	}
	
	/**
	 * Get: cdGerenciaRegional.
	 *
	 * @return cdGerenciaRegional
	 */
	public int getCdGerenciaRegional() {
		return cdGerenciaRegional;
	}
	
	/**
	 * Set: cdGerenciaRegional.
	 *
	 * @param cdGerenciaRegional the cd gerencia regional
	 */
	public void setCdGerenciaRegional(int cdGerenciaRegional) {
		this.cdGerenciaRegional = cdGerenciaRegional;
	}
	
	/**
	 * Get: dsGerenciaRegional.
	 *
	 * @return dsGerenciaRegional
	 */
	public String getDsGerenciaRegional() {
		return dsGerenciaRegional;
	}
	
	/**
	 * Set: dsGerenciaRegional.
	 *
	 * @param dsGerenciaRegional the ds gerencia regional
	 */
	public void setDsGerenciaRegional(String dsGerenciaRegional) {
		this.dsGerenciaRegional = dsGerenciaRegional;
	}
	
	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public int getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}
	
	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(int cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}
	
	/**
	 * Get: cdEmpresaConglomeradoAgenciaOperadora.
	 *
	 * @return cdEmpresaConglomeradoAgenciaOperadora
	 */
	public long getCdEmpresaConglomeradoAgenciaOperadora() {
		return cdEmpresaConglomeradoAgenciaOperadora;
	}
	
	/**
	 * Set: cdEmpresaConglomeradoAgenciaOperadora.
	 *
	 * @param cdEmpresaConglomeradoAgenciaOperadora the cd empresa conglomerado agencia operadora
	 */
	public void setCdEmpresaConglomeradoAgenciaOperadora(
			long cdEmpresaConglomeradoAgenciaOperadora) {
		this.cdEmpresaConglomeradoAgenciaOperadora = cdEmpresaConglomeradoAgenciaOperadora;
	}
	
	/**
	 * Get: dsAgenciaOperadora.
	 *
	 * @return dsAgenciaOperadora
	 */
	public String getDsAgenciaOperadora() {
		return dsAgenciaOperadora;
	}
	
	/**
	 * Set: dsAgenciaOperadora.
	 *
	 * @param dsAgenciaOperadora the ds agencia operadora
	 */
	public void setDsAgenciaOperadora(String dsAgenciaOperadora) {
		this.dsAgenciaOperadora = dsAgenciaOperadora;
	}
	
	/**
	 * Get: dsEmpresaConglomeradoAgenciaOperadora.
	 *
	 * @return dsEmpresaConglomeradoAgenciaOperadora
	 */
	public String getDsEmpresaConglomeradoAgenciaOperadora() {
		return dsEmpresaConglomeradoAgenciaOperadora;
	}
	
	/**
	 * Set: dsEmpresaConglomeradoAgenciaOperadora.
	 *
	 * @param dsEmpresaConglomeradoAgenciaOperadora the ds empresa conglomerado agencia operadora
	 */
	public void setDsEmpresaConglomeradoAgenciaOperadora(
			String dsEmpresaConglomeradoAgenciaOperadora) {
		this.dsEmpresaConglomeradoAgenciaOperadora = dsEmpresaConglomeradoAgenciaOperadora;
	}
	
	/**
	 * Get: tipoRelatorio.
	 *
	 * @return tipoRelatorio
	 */
	public int getTipoRelatorio() {
		return tipoRelatorio;
	}
	
	/**
	 * Set: tipoRelatorio.
	 *
	 * @param tipoRelatorio the tipo relatorio
	 */
	public void setTipoRelatorio(int tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}
	
	/**
	 * Get: cdSegmento.
	 *
	 * @return cdSegmento
	 */
	public int getCdSegmento() {
		return cdSegmento;
	}
	
	/**
	 * Set: cdSegmento.
	 *
	 * @param cdSegmento the cd segmento
	 */
	public void setCdSegmento(int cdSegmento) {
		this.cdSegmento = cdSegmento;
	}
	
	/**
	 * Get: dsSegmento.
	 *
	 * @return dsSegmento
	 */
	public String getDsSegmento() {
		return dsSegmento;
	}
	
	/**
	 * Set: dsSegmento.
	 *
	 * @param dsSegmento the ds segmento
	 */
	public void setDsSegmento(String dsSegmento) {
		this.dsSegmento = dsSegmento;
	}
	
	/**
	 * Get: cdParticipanteGrupoEconomico.
	 *
	 * @return cdParticipanteGrupoEconomico
	 */
	public long getCdParticipanteGrupoEconomico() {
		return cdParticipanteGrupoEconomico;
	}
	
	/**
	 * Set: cdParticipanteGrupoEconomico.
	 *
	 * @param cdParticipanteGrupoEconomico the cd participante grupo economico
	 */
	public void setCdParticipanteGrupoEconomico(long cdParticipanteGrupoEconomico) {
		this.cdParticipanteGrupoEconomico = cdParticipanteGrupoEconomico;
	}
	
	/**
	 * Get: dsParticipanteGrupoEconomico.
	 *
	 * @return dsParticipanteGrupoEconomico
	 */
	public String getDsParticipanteGrupoEconomico() {
		return dsParticipanteGrupoEconomico;
	}
	
	/**
	 * Set: dsParticipanteGrupoEconomico.
	 *
	 * @param dsParticipanteGrupoEconomico the ds participante grupo economico
	 */
	public void setDsParticipanteGrupoEconomico(String dsParticipanteGrupoEconomico) {
		this.dsParticipanteGrupoEconomico = dsParticipanteGrupoEconomico;
	}

	/**
	 * Get: cdSubRamoAtividadeEconomica.
	 *
	 * @return cdSubRamoAtividadeEconomica
	 */
	public int getCdSubRamoAtividadeEconomica() {
		return cdSubRamoAtividadeEconomica;
	}
	
	/**
	 * Set: cdSubRamoAtividadeEconomica.
	 *
	 * @param cdSubRamoAtividadeEconomica the cd sub ramo atividade economica
	 */
	public void setCdSubRamoAtividadeEconomica(int cdSubRamoAtividadeEconomica) {
		this.cdSubRamoAtividadeEconomica = cdSubRamoAtividadeEconomica;
	}
	
	/**
	 * Get: cdRamoAtividadeEconomica.
	 *
	 * @return cdRamoAtividadeEconomica
	 */
	public int getCdRamoAtividadeEconomica() {
		return cdRamoAtividadeEconomica;
	}
	
	/**
	 * Set: cdRamoAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd ramo atividade economica
	 */
	public void setCdRamoAtividadeEconomica(int cdAtividadeEconomica) {
		this.cdRamoAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: cdClassAtividadeEconomica.
	 *
	 * @return cdClassAtividadeEconomica
	 */
	public String getCdClassAtividadeEconomica() {
		return cdClassAtividadeEconomica;
	}
	
	/**
	 * Set: cdClassAtividadeEconomica.
	 *
	 * @param cdClassAtividadeEconomica the cd class atividade economica
	 */
	public void setCdClassAtividadeEconomica(String cdClassAtividadeEconomica) {
		this.cdClassAtividadeEconomica = cdClassAtividadeEconomica;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public int getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(int cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}
	
	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsTipoRelatorio.
	 *
	 * @return dsTipoRelatorio
	 */
	public String getDsTipoRelatorio() {
		return dsTipoRelatorio;
	}
	
	/**
	 * Set: dsTipoRelatorio.
	 *
	 * @param dsTipoRelatorio the ds tipo relatorio
	 */
	public void setDsTipoRelatorio(String dsTipoRelatorio) {
		this.dsTipoRelatorio = dsTipoRelatorio;
	}
	
	/**
	 * Get: dsRamo.
	 *
	 * @return dsRamo
	 */
	public String getDsRamo() {
		return dsRamo;
	}
	
	/**
	 * Set: dsRamo.
	 *
	 * @param dsRamo the ds ramo
	 */
	public void setDsRamo(String dsRamo) {
		this.dsRamo = dsRamo;
	}
	
	/**
	 * Get: dsSubRamo.
	 *
	 * @return dsSubRamo
	 */
	public String getDsSubRamo() {
		return dsSubRamo;
	}
	
	/**
	 * Set: dsSubRamo.
	 *
	 * @param dsSubRamo the ds sub ramo
	 */
	public void setDsSubRamo(String dsSubRamo) {
		this.dsSubRamo = dsSubRamo;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	
	 
	
}
