/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolicRecuperacaoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolicRecuperacaoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _hrSolicitacaoRecuperacao
     */
    private java.lang.String _hrSolicitacaoRecuperacao;

    /**
     * Field _cdUsuarioSolicitacao
     */
    private java.lang.String _cdUsuarioSolicitacao;

    /**
     * Field _dsSituacaoRecuperacao
     */
    private java.lang.String _dsSituacaoRecuperacao;

    /**
     * Field _dsMotvtoSituacaoRecuperacao
     */
    private java.lang.String _dsMotvtoSituacaoRecuperacao;

    /**
     * Field _dsDestinoRelatorio
     */
    private java.lang.String _dsDestinoRelatorio;

    /**
     * Field _dsDestinoArqIsd
     */
    private java.lang.String _dsDestinoArqIsd;

    /**
     * Field _dsDestinoArquivoRetorno
     */
    private java.lang.String _dsDestinoArquivoRetorno;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegregacaoInclusao
     */
    private java.lang.String _cdAutenticacaoSegregacaoInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegregacaoManutencao
     */
    private java.lang.String _cdAutenticacaoSegregacaoManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _nrSolicitacao
     */
    private int _nrSolicitacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacao
     */
    private boolean _has_nrSolicitacao;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolicRecuperacaoResponse() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.DetalharSolicRecuperacaoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias) 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteNrSolicitacao
     * 
     */
    public void deleteNrSolicitacao()
    {
        this._has_nrSolicitacao= false;
    } //-- void deleteNrSolicitacao() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegregacaoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegregacaoInclusao'
     */
    public java.lang.String getCdAutenticacaoSegregacaoInclusao()
    {
        return this._cdAutenticacaoSegregacaoInclusao;
    } //-- java.lang.String getCdAutenticacaoSegregacaoInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegregacaoManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegregacaoManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegregacaoManutencao()
    {
        return this._cdAutenticacaoSegregacaoManutencao;
    } //-- java.lang.String getCdAutenticacaoSegregacaoManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdUsuarioSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioSolicitacao'.
     */
    public java.lang.String getCdUsuarioSolicitacao()
    {
        return this._cdUsuarioSolicitacao;
    } //-- java.lang.String getCdUsuarioSolicitacao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsDestinoArqIsd'.
     * 
     * @return String
     * @return the value of field 'dsDestinoArqIsd'.
     */
    public java.lang.String getDsDestinoArqIsd()
    {
        return this._dsDestinoArqIsd;
    } //-- java.lang.String getDsDestinoArqIsd() 

    /**
     * Returns the value of field 'dsDestinoArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsDestinoArquivoRetorno'.
     */
    public java.lang.String getDsDestinoArquivoRetorno()
    {
        return this._dsDestinoArquivoRetorno;
    } //-- java.lang.String getDsDestinoArquivoRetorno() 

    /**
     * Returns the value of field 'dsDestinoRelatorio'.
     * 
     * @return String
     * @return the value of field 'dsDestinoRelatorio'.
     */
    public java.lang.String getDsDestinoRelatorio()
    {
        return this._dsDestinoRelatorio;
    } //-- java.lang.String getDsDestinoRelatorio() 

    /**
     * Returns the value of field 'dsMotvtoSituacaoRecuperacao'.
     * 
     * @return String
     * @return the value of field 'dsMotvtoSituacaoRecuperacao'.
     */
    public java.lang.String getDsMotvtoSituacaoRecuperacao()
    {
        return this._dsMotvtoSituacaoRecuperacao;
    } //-- java.lang.String getDsMotvtoSituacaoRecuperacao() 

    /**
     * Returns the value of field 'dsSituacaoRecuperacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoRecuperacao'.
     */
    public java.lang.String getDsSituacaoRecuperacao()
    {
        return this._dsSituacaoRecuperacao;
    } //-- java.lang.String getDsSituacaoRecuperacao() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'hrSolicitacaoRecuperacao'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacaoRecuperacao'.
     */
    public java.lang.String getHrSolicitacaoRecuperacao()
    {
        return this._hrSolicitacaoRecuperacao;
    } //-- java.lang.String getHrSolicitacaoRecuperacao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrSolicitacao'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacao'.
     */
    public int getNrSolicitacao()
    {
        return this._nrSolicitacao;
    } //-- int getNrSolicitacao() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasNrSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacao()
    {
        return this._has_nrSolicitacao;
    } //-- boolean hasNrSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdAutenticacaoSegregacaoInclusao'.
     * 
     * @param cdAutenticacaoSegregacaoInclusao the value of field
     * 'cdAutenticacaoSegregacaoInclusao'.
     */
    public void setCdAutenticacaoSegregacaoInclusao(java.lang.String cdAutenticacaoSegregacaoInclusao)
    {
        this._cdAutenticacaoSegregacaoInclusao = cdAutenticacaoSegregacaoInclusao;
    } //-- void setCdAutenticacaoSegregacaoInclusao(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdAutenticacaoSegregacaoManutencao'.
     * 
     * @param cdAutenticacaoSegregacaoManutencao the value of field
     * 'cdAutenticacaoSegregacaoManutencao'.
     */
    public void setCdAutenticacaoSegregacaoManutencao(java.lang.String cdAutenticacaoSegregacaoManutencao)
    {
        this._cdAutenticacaoSegregacaoManutencao = cdAutenticacaoSegregacaoManutencao;
    } //-- void setCdAutenticacaoSegregacaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdUsuarioSolicitacao'.
     * 
     * @param cdUsuarioSolicitacao the value of field
     * 'cdUsuarioSolicitacao'.
     */
    public void setCdUsuarioSolicitacao(java.lang.String cdUsuarioSolicitacao)
    {
        this._cdUsuarioSolicitacao = cdUsuarioSolicitacao;
    } //-- void setCdUsuarioSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoArqIsd'.
     * 
     * @param dsDestinoArqIsd the value of field 'dsDestinoArqIsd'.
     */
    public void setDsDestinoArqIsd(java.lang.String dsDestinoArqIsd)
    {
        this._dsDestinoArqIsd = dsDestinoArqIsd;
    } //-- void setDsDestinoArqIsd(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoArquivoRetorno'.
     * 
     * @param dsDestinoArquivoRetorno the value of field
     * 'dsDestinoArquivoRetorno'.
     */
    public void setDsDestinoArquivoRetorno(java.lang.String dsDestinoArquivoRetorno)
    {
        this._dsDestinoArquivoRetorno = dsDestinoArquivoRetorno;
    } //-- void setDsDestinoArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoRelatorio'.
     * 
     * @param dsDestinoRelatorio the value of field
     * 'dsDestinoRelatorio'.
     */
    public void setDsDestinoRelatorio(java.lang.String dsDestinoRelatorio)
    {
        this._dsDestinoRelatorio = dsDestinoRelatorio;
    } //-- void setDsDestinoRelatorio(java.lang.String) 

    /**
     * Sets the value of field 'dsMotvtoSituacaoRecuperacao'.
     * 
     * @param dsMotvtoSituacaoRecuperacao the value of field
     * 'dsMotvtoSituacaoRecuperacao'.
     */
    public void setDsMotvtoSituacaoRecuperacao(java.lang.String dsMotvtoSituacaoRecuperacao)
    {
        this._dsMotvtoSituacaoRecuperacao = dsMotvtoSituacaoRecuperacao;
    } //-- void setDsMotvtoSituacaoRecuperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoRecuperacao'.
     * 
     * @param dsSituacaoRecuperacao the value of field
     * 'dsSituacaoRecuperacao'.
     */
    public void setDsSituacaoRecuperacao(java.lang.String dsSituacaoRecuperacao)
    {
        this._dsSituacaoRecuperacao = dsSituacaoRecuperacao;
    } //-- void setDsSituacaoRecuperacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacaoRecuperacao'.
     * 
     * @param hrSolicitacaoRecuperacao the value of field
     * 'hrSolicitacaoRecuperacao'.
     */
    public void setHrSolicitacaoRecuperacao(java.lang.String hrSolicitacaoRecuperacao)
    {
        this._hrSolicitacaoRecuperacao = hrSolicitacaoRecuperacao;
    } //-- void setHrSolicitacaoRecuperacao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacao'.
     * 
     * @param nrSolicitacao the value of field 'nrSolicitacao'.
     */
    public void setNrSolicitacao(int nrSolicitacao)
    {
        this._nrSolicitacao = nrSolicitacao;
        this._has_nrSolicitacao = true;
    } //-- void setNrSolicitacao(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.Ocorrencias) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolicRecuperacaoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.DetalharSolicRecuperacaoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.DetalharSolicRecuperacaoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.DetalharSolicRecuperacaoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicrecuperacao.response.DetalharSolicRecuperacaoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
