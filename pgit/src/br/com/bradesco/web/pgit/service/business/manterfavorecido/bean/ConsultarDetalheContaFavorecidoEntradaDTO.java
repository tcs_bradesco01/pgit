/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarDetalheContaFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetalheContaFavorecidoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;
    
    /** Atributo nrOcorrenciaContaFavorecido. */
    private Integer nrOcorrenciaContaFavorecido;
    
    /** Atributo cdTipoContaFavorecido. */
    private Integer cdTipoContaFavorecido;
    
    /**
     * Consultar detalhe conta favorecido entrada dto.
     */
    public ConsultarDetalheContaFavorecidoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
    
	/**
	 * Get: cdFavorecidoClientePagador.
	 *
	 * @return cdFavorecidoClientePagador
	 */
	public Long getCdFavorecidoClientePagador() {
		return cdFavorecidoClientePagador;
	}
	
	/**
	 * Set: cdFavorecidoClientePagador.
	 *
	 * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
	 */
	public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
		this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContaFavorecido.
	 *
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}
	
	/**
	 * Set: cdTipoContaFavorecido.
	 *
	 * @param cdTipoContaFavorecido the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrOcorrenciaContaFavorecido.
	 *
	 * @return nrOcorrenciaContaFavorecido
	 */
	public Integer getNrOcorrenciaContaFavorecido() {
		return nrOcorrenciaContaFavorecido;
	}
	
	/**
	 * Set: nrOcorrenciaContaFavorecido.
	 *
	 * @param nrOcorrenciaContaFavorecido the nr ocorrencia conta favorecido
	 */
	public void setNrOcorrenciaContaFavorecido(Integer nrOcorrenciaContaFavorecido) {
		this.nrOcorrenciaContaFavorecido = nrOcorrenciaContaFavorecido;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

}
