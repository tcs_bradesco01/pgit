package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

import java.util.List;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Arquivo criado em 30/12/15.
 */
public class ListarContratoMaeSaidaDTO {

    private String codMensagem;

    private String mensagem;

    private Long cdpessoaJuridicaContrato;

    private Integer cdTipoContratoNegocio;

    private Integer nrLinhas;

    private List<ListarContratoMaeOcorrenciasSaidaDTO> ocorrencias;

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    public void setNrLinhas(Integer nrLinhas) {
        this.nrLinhas = nrLinhas;
    }

    public Integer getNrLinhas() {
        return this.nrLinhas;
    }

    public void setOcorrencias(List<ListarContratoMaeOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    public List<ListarContratoMaeOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}