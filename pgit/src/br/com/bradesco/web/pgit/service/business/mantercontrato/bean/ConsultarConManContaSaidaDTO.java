/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarConManContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConManContaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdBanco. */
	private Integer cdBanco;

	/** Atributo dsBanco. */
	private String dsBanco;

	/** Atributo cdComercialAgenciaContabil. */
	private Integer cdComercialAgenciaContabil;

	/** Atributo dsComercialAgenciaContabil. */
	private String dsComercialAgenciaContabil;

	/** Atributo cdNumeroContaBancaria. */
	private Long cdNumeroContaBancaria;

	/** Atributo cdDigitoContaBancaria. */
	private Integer cdDigitoContaBancaria;

	/** Atributo cdTipoConta. */
	private Integer cdTipoConta;

	/** Atributo dsTipoConta. */
	private String dsTipoConta;

	/** Atributo cdSituacaoVinculo. */
	private Integer cdSituacaoVinculo;

	/** Atributo dsSituacaoVinculo. */
	private String dsSituacaoVinculo;

	/** Atributo dtVinculoInicial. */
	private String dtVinculoInicial;

	/** Atributo dtVinculoFinal. */
	private String dtVinculoFinal;

	/** Atributo cdMunicipioFeri. */
	private Integer cdMunicipioFeri;

	/** Atributo dsMunicipioFeri. */
	private String dsMunicipioFeri;

	/** Atributo cdMunicipio. */
	private Integer cdMunicipio;

	/** Atributo dsMunicipio. */
	private String dsMunicipio;

	/** Atributo cdUnidadeFederativa. */
	private Integer cdUnidadeFederativa;

	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;

	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;

	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;

	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;

	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;

	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;

	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;

	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;

	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;

	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;

	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;

	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;

	/** Atributo dsUnidadeFederativa. */
	private String dsUnidadeFederativa;

	/** Atributo cdMotivoSituacaoConta. */
	private Integer cdMotivoSituacaoConta;

	/** Atributo dsMotivoSituacaoConta. */
	private String dsMotivoSituacaoConta;

	/** Atributo valorAdicionadoContratoConta. */
	private BigDecimal valorAdicionadoContratoConta;

	/** Atributo dtInicialValorAdicionado. */
	private String dtInicialValorAdicionado;

	/** Atributo dtFinalValorAdicionado. */
	private String dtFinalValorAdicionado;

	/** Atributo cdIndicadorTipoManutencao. */
	private Integer cdIndicadorTipoManutencao;

	/** Atributo dsIndicadorTipoManutencao. */
	private String dsIndicadorTipoManutencao;

	/** Atributo vlAdicionalTed. */
	private BigDecimal vlAdicionalTed;

	/** Atributo dtValidadeInicialTed. */
	private String dtValidadeInicialTed;

	/** Atributo dtValidadeFinalTed. */
	private String dtValidadeFinalTed;
	
	private String dsEmpresaSegLinha;

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdComercialAgenciaContabil.
	 *
	 * @return cdComercialAgenciaContabil
	 */
	public Integer getCdComercialAgenciaContabil() {
		return cdComercialAgenciaContabil;
	}

	/**
	 * Set: cdComercialAgenciaContabil.
	 *
	 * @param cdComercialAgenciaContabil the cd comercial agencia contabil
	 */
	public void setCdComercialAgenciaContabil(Integer cdComercialAgenciaContabil) {
		this.cdComercialAgenciaContabil = cdComercialAgenciaContabil;
	}

	/**
	 * Get: cdDigitoContaBancaria.
	 *
	 * @return cdDigitoContaBancaria
	 */
	public Integer getCdDigitoContaBancaria() {
		return cdDigitoContaBancaria;
	}

	/**
	 * Set: cdDigitoContaBancaria.
	 *
	 * @param cdDigitoContaBancaria the cd digito conta bancaria
	 */
	public void setCdDigitoContaBancaria(Integer cdDigitoContaBancaria) {
		this.cdDigitoContaBancaria = cdDigitoContaBancaria;
	}

	/**
	 * Get: cdIndicadorTipoManutencao.
	 *
	 * @return cdIndicadorTipoManutencao
	 */
	public Integer getCdIndicadorTipoManutencao() {
		return cdIndicadorTipoManutencao;
	}

	/**
	 * Set: cdIndicadorTipoManutencao.
	 *
	 * @param cdIndicadorTipoManutencao the cd indicador tipo manutencao
	 */
	public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
		this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
	}

	/**
	 * Get: cdMotivoSituacaoConta.
	 *
	 * @return cdMotivoSituacaoConta
	 */
	public Integer getCdMotivoSituacaoConta() {
		return cdMotivoSituacaoConta;
	}

	/**
	 * Set: cdMotivoSituacaoConta.
	 *
	 * @param cdMotivoSituacaoConta the cd motivo situacao conta
	 */
	public void setCdMotivoSituacaoConta(Integer cdMotivoSituacaoConta) {
		this.cdMotivoSituacaoConta = cdMotivoSituacaoConta;
	}

	/**
	 * Get: cdMunicipio.
	 *
	 * @return cdMunicipio
	 */
	public Integer getCdMunicipio() {
		return cdMunicipio;
	}

	/**
	 * Set: cdMunicipio.
	 *
	 * @param cdMunicipio the cd municipio
	 */
	public void setCdMunicipio(Integer cdMunicipio) {
		this.cdMunicipio = cdMunicipio;
	}

	/**
	 * Get: cdMunicipioFeri.
	 *
	 * @return cdMunicipioFeri
	 */
	public Integer getCdMunicipioFeri() {
		return cdMunicipioFeri;
	}

	/**
	 * Set: cdMunicipioFeri.
	 *
	 * @param cdMunicipioFeri the cd municipio feri
	 */
	public void setCdMunicipioFeri(Integer cdMunicipioFeri) {
		this.cdMunicipioFeri = cdMunicipioFeri;
	}

	/**
	 * Get: cdNumeroContaBancaria.
	 *
	 * @return cdNumeroContaBancaria
	 */
	public Long getCdNumeroContaBancaria() {
		return cdNumeroContaBancaria;
	}

	/**
	 * Set: cdNumeroContaBancaria.
	 *
	 * @param cdNumeroContaBancaria the cd numero conta bancaria
	 */
	public void setCdNumeroContaBancaria(Long cdNumeroContaBancaria) {
		this.cdNumeroContaBancaria = cdNumeroContaBancaria;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdSituacaoVinculo.
	 *
	 * @return cdSituacaoVinculo
	 */
	public Integer getCdSituacaoVinculo() {
		return cdSituacaoVinculo;
	}

	/**
	 * Set: cdSituacaoVinculo.
	 *
	 * @param cdSituacaoVinculo the cd situacao vinculo
	 */
	public void setCdSituacaoVinculo(Integer cdSituacaoVinculo) {
		this.cdSituacaoVinculo = cdSituacaoVinculo;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}

	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}

	/**
	 * Get: cdUnidadeFederativa.
	 *
	 * @return cdUnidadeFederativa
	 */
	public Integer getCdUnidadeFederativa() {
		return cdUnidadeFederativa;
	}

	/**
	 * Set: cdUnidadeFederativa.
	 *
	 * @param cdUnidadeFederativa the cd unidade federativa
	 */
	public void setCdUnidadeFederativa(Integer cdUnidadeFederativa) {
		this.cdUnidadeFederativa = cdUnidadeFederativa;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExter.
	 *
	 * @return cdUsuarioInclusaoExter
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExter.
	 *
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExter.
	 *
	 * @return cdUsuarioManutencaoExter
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}

	/**
	 * Set: cdUsuarioManutencaoExter.
	 *
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	/**
	 * Get: cdValorAdicionadoContratoConta.
	 *
	 * @return cdValorAdicionadoContratoConta
	 */
	public BigDecimal getValorAdicionadoContratoConta() {
		return valorAdicionadoContratoConta;
	}

	/**
	 * Set: cdValorAdicionadoContratoConta.
	 *
	 * @param cdValorAdicionadoContratoConta the cd valor adicionado contrato conta
	 */
	public void setValorAdicionadoContratoConta(BigDecimal valorAdicionadoContratoConta) {
		this.valorAdicionadoContratoConta = valorAdicionadoContratoConta;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}

	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsComercialAgenciaContabil.
	 *
	 * @return dsComercialAgenciaContabil
	 */
	public String getDsComercialAgenciaContabil() {
		return dsComercialAgenciaContabil;
	}

	/**
	 * Set: dsComercialAgenciaContabil.
	 *
	 * @param dsComercialAgenciaContabil the ds comercial agencia contabil
	 */
	public void setDsComercialAgenciaContabil(String dsComercialAgenciaContabil) {
		this.dsComercialAgenciaContabil = dsComercialAgenciaContabil;
	}

	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao() {
		return dsIndicadorTipoManutencao;
	}

	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}

	/**
	 * Get: dsInicialValorAdicionado.
	 *
	 * @return dsInicialValorAdicionado
	 */
	public String getDtInicialValorAdicionado() {
		return dtInicialValorAdicionado;
	}

	/**
	 * Set: dsInicialValorAdicionado.
	 *
	 * @param dsInicialValorAdicionado the ds inicial valor adicionado
	 */
	public void setDtInicialValorAdicionado(String dtInicialValorAdicionado) {
		this.dtInicialValorAdicionado = dtInicialValorAdicionado;
	}

	/**
	 * Get: dsMotivoSituacaoConta.
	 *
	 * @return dsMotivoSituacaoConta
	 */
	public String getDsMotivoSituacaoConta() {
		return dsMotivoSituacaoConta;
	}

	/**
	 * Set: dsMotivoSituacaoConta.
	 *
	 * @param dsMotivoSituacaoConta the ds motivo situacao conta
	 */
	public void setDsMotivoSituacaoConta(String dsMotivoSituacaoConta) {
		this.dsMotivoSituacaoConta = dsMotivoSituacaoConta;
	}

	/**
	 * Get: dsMunicipio.
	 *
	 * @return dsMunicipio
	 */
	public String getDsMunicipio() {
		return dsMunicipio;
	}

	/**
	 * Set: dsMunicipio.
	 *
	 * @param dsMunicipio the ds municipio
	 */
	public void setDsMunicipio(String dsMunicipio) {
		this.dsMunicipio = dsMunicipio;
	}

	/**
	 * Get: dsMunicipioFeri.
	 *
	 * @return dsMunicipioFeri
	 */
	public String getDsMunicipioFeri() {
		return dsMunicipioFeri;
	}

	/**
	 * Set: dsMunicipioFeri.
	 *
	 * @param dsMunicipioFeri the ds municipio feri
	 */
	public void setDsMunicipioFeri(String dsMunicipioFeri) {
		this.dsMunicipioFeri = dsMunicipioFeri;
	}

	/**
	 * Get: dsSituacaoVinculo.
	 *
	 * @return dsSituacaoVinculo
	 */
	public String getDsSituacaoVinculo() {
		return dsSituacaoVinculo;
	}

	/**
	 * Set: dsSituacaoVinculo.
	 *
	 * @param dsSituacaoVinculo the ds situacao vinculo
	 */
	public void setDsSituacaoVinculo(String dsSituacaoVinculo) {
		this.dsSituacaoVinculo = dsSituacaoVinculo;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

	/**
	 * Get: dtFinalValorAdicionado.
	 *
	 * @return dtFinalValorAdicionado
	 */
	public String getDtFinalValorAdicionado() {
		return dtFinalValorAdicionado;
	}

	/**
	 * Set: dtFinalValorAdicionado.
	 *
	 * @param dtFinalValorAdicionado the dt final valor adicionado
	 */
	public void setDtFinalValorAdicionado(String dtFinalValorAdicionado) {
		this.dtFinalValorAdicionado = dtFinalValorAdicionado;
	}

	/**
	 * Get: dtVinculoFinal.
	 *
	 * @return dtVinculoFinal
	 */
	public String getDtVinculoFinal() {
		return dtVinculoFinal;
	}

	/**
	 * Set: dtVinculoFinal.
	 *
	 * @param dtVinculoFinal the dt vinculo final
	 */
	public void setDtVinculoFinal(String dtVinculoFinal) {
		this.dtVinculoFinal = dtVinculoFinal;
	}

	/**
	 * Get: dtVinculoInicial.
	 *
	 * @return dtVinculoInicial
	 */
	public String getDtVinculoInicial() {
		return dtVinculoInicial;
	}

	/**
	 * Set: dtVinculoInicial.
	 *
	 * @param dtVinculoInicial the dt vinculo inicial
	 */
	public void setDtVinculoInicial(String dtVinculoInicial) {
		this.dtVinculoInicial = dtVinculoInicial;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: dsUnidFede.
	 *
	 * @return dsUnidFede
	 */
	public String getDsUnidadeFederativa() {
		return dsUnidadeFederativa;
	}

	/**
	 * Set: dsUnidFede.
	 *
	 * @param dsUnidFede the ds unid fede
	 */
	public void setDsUnidadeFederativa(String dsUnidadeFederativa) {
		this.dsUnidadeFederativa = dsUnidadeFederativa;
	}

	/**
	 * Get: vlAdicionalTed.
	 *
	 * @return vlAdicionalTed
	 */
	public BigDecimal getVlAdicionalTed() {
		return vlAdicionalTed;
	}

	/**
	 * Set: vlAdicionalTed.
	 *
	 * @param vlAdicionalTed the vl adicional ted
	 */
	public void setVlAdicionalTed(BigDecimal vlAdicionalTed) {
		this.vlAdicionalTed = vlAdicionalTed;
	}

	/**
	 * Get: dtValidadeInicialTed.
	 *
	 * @return dtValidadeInicialTed
	 */
	public String getDtValidadeInicialTed() {
		return dtValidadeInicialTed;
	}

	/**
	 * Set: dtValidadeInicialTed.
	 *
	 * @param dtValidadeInicialTed the dt validade inicial ted
	 */
	public void setDtValidadeInicialTed(String dtValidadeInicialTed) {
		this.dtValidadeInicialTed = dtValidadeInicialTed;
	}

	/**
	 * Get: dtValidadeFinalTed.
	 *
	 * @return dtValidadeFinalTed
	 */
	public String getDtValidadeFinalTed() {
		return dtValidadeFinalTed;
	}

	/**
	 * Set: dtValidadeFinalTed.
	 *
	 * @param dtValidadeFinalTed the dt validade final ted
	 */
	public void setDtValidadeFinalTed(String dtValidadeFinalTed) {
		this.dtValidadeFinalTed = dtValidadeFinalTed;
	}

	public String getDsEmpresaSegLinha() {
		return dsEmpresaSegLinha;
	}

	public void setDsEmpresaSegLinha(String dsEmpresaSegLinha) {
		this.dsEmpresaSegLinha = dsEmpresaSegLinha;
	}
	

}
