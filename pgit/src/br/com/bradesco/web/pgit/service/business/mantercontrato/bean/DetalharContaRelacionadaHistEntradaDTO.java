/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: DetalharContaRelacionadaHistEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharContaRelacionadaHistEntradaDTO{
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdPessoaJuridicaNegocio. */
    private Long cdPessoaJuridicaNegocio;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdPessoVinc. */
    private Long cdPessoVinc;
    
    /** Atributo cdTipoContratoVinc. */
    private Integer cdTipoContratoVinc;
    
    /** Atributo nrSeqContratoVinc. */
    private Long nrSeqContratoVinc;
    
    /** Atributo cdTipoVincContrato. */
    private Integer cdTipoVincContrato;
    
    /** Atributo cdTipoRelacionamentoConta. */
    private Integer cdTipoRelacionamentoConta;
    
    /** Atributo cdBancoRelacionamento. */
    private Integer cdBancoRelacionamento;
    
    /** Atributo cdAgenciaRelacionamento. */
    private Integer cdAgenciaRelacionamento;
    
    /** Atributo cdContaRelacionamento. */
    private Long cdContaRelacionamento;
    
    /** Atributo digitoContaRelacionamento. */
    private String digitoContaRelacionamento;
    
    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;
    
    /** Atributo cdCorpoCpfCnpjParticipante. */
    private Long cdCorpoCpfCnpjParticipante;
    
    /** Atributo cdFilialCpfCnpjParticipante. */
    private Integer cdFilialCpfCnpjParticipante;
    
    /** Atributo cdDigitoCpfCnpjParticipante. */
    private Integer cdDigitoCpfCnpjParticipante;
    
    private long cdPssoa;
    
	/**
	 * Get: cdAgenciaRelacionamento.
	 *
	 * @return cdAgenciaRelacionamento
	 */
	public Integer getCdAgenciaRelacionamento() {
		return cdAgenciaRelacionamento;
	}
	
	/**
	 * Set: cdAgenciaRelacionamento.
	 *
	 * @param cdAgenciaRelacionamento the cd agencia relacionamento
	 */
	public void setCdAgenciaRelacionamento(Integer cdAgenciaRelacionamento) {
		this.cdAgenciaRelacionamento = cdAgenciaRelacionamento;
	}
	
	/**
	 * Get: cdBancoRelacionamento.
	 *
	 * @return cdBancoRelacionamento
	 */
	public Integer getCdBancoRelacionamento() {
		return cdBancoRelacionamento;
	}
	
	/**
	 * Set: cdBancoRelacionamento.
	 *
	 * @param cdBancoRelacionamento the cd banco relacionamento
	 */
	public void setCdBancoRelacionamento(Integer cdBancoRelacionamento) {
		this.cdBancoRelacionamento = cdBancoRelacionamento;
	}
	
	/**
	 * Get: cdContaRelacionamento.
	 *
	 * @return cdContaRelacionamento
	 */
	public Long getCdContaRelacionamento() {
		return cdContaRelacionamento;
	}
	
	/**
	 * Set: cdContaRelacionamento.
	 *
	 * @param cdContaRelacionamento the cd conta relacionamento
	 */
	public void setCdContaRelacionamento(Long cdContaRelacionamento) {
		this.cdContaRelacionamento = cdContaRelacionamento;
	}
	
	/**
	 * Get: cdCorpoCpfCnpjParticipante.
	 *
	 * @return cdCorpoCpfCnpjParticipante
	 */
	public Long getCdCorpoCpfCnpjParticipante() {
		return cdCorpoCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdCorpoCpfCnpjParticipante.
	 *
	 * @param cdCorpoCpfCnpjParticipante the cd corpo cpf cnpj participante
	 */
	public void setCdCorpoCpfCnpjParticipante(Long cdCorpoCpfCnpjParticipante) {
		this.cdCorpoCpfCnpjParticipante = cdCorpoCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdDigitoCpfCnpjParticipante.
	 *
	 * @return cdDigitoCpfCnpjParticipante
	 */
	public Integer getCdDigitoCpfCnpjParticipante() {
		return cdDigitoCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdDigitoCpfCnpjParticipante.
	 *
	 * @param cdDigitoCpfCnpjParticipante the cd digito cpf cnpj participante
	 */
	public void setCdDigitoCpfCnpjParticipante(Integer cdDigitoCpfCnpjParticipante) {
		this.cdDigitoCpfCnpjParticipante = cdDigitoCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdFilialCpfCnpjParticipante.
	 *
	 * @return cdFilialCpfCnpjParticipante
	 */
	public Integer getCdFilialCpfCnpjParticipante() {
		return cdFilialCpfCnpjParticipante;
	}
	
	/**
	 * Set: cdFilialCpfCnpjParticipante.
	 *
	 * @param cdFilialCpfCnpjParticipante the cd filial cpf cnpj participante
	 */
	public void setCdFilialCpfCnpjParticipante(Integer cdFilialCpfCnpjParticipante) {
		this.cdFilialCpfCnpjParticipante = cdFilialCpfCnpjParticipante;
	}
	
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdPessoVinc.
	 *
	 * @return cdPessoVinc
	 */
	public Long getCdPessoVinc() {
		return cdPessoVinc;
	}
	
	/**
	 * Set: cdPessoVinc.
	 *
	 * @param cdPessoVinc the cd pesso vinc
	 */
	public void setCdPessoVinc(Long cdPessoVinc) {
		this.cdPessoVinc = cdPessoVinc;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}
	
	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}
	
	/**
	 * Get: cdTipoRelacionamentoConta.
	 *
	 * @return cdTipoRelacionamentoConta
	 */
	public Integer getCdTipoRelacionamentoConta() {
		return cdTipoRelacionamentoConta;
	}
	
	/**
	 * Set: cdTipoRelacionamentoConta.
	 *
	 * @param cdTipoRelacionamentoConta the cd tipo relacionamento conta
	 */
	public void setCdTipoRelacionamentoConta(Integer cdTipoRelacionamentoConta) {
		this.cdTipoRelacionamentoConta = cdTipoRelacionamentoConta;
	}
	
	/**
	 * Get: cdTipoVincContrato.
	 *
	 * @return cdTipoVincContrato
	 */
	public Integer getCdTipoVincContrato() {
		return cdTipoVincContrato;
	}
	
	/**
	 * Set: cdTipoVincContrato.
	 *
	 * @param cdTipoVincContrato the cd tipo vinc contrato
	 */
	public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
		this.cdTipoVincContrato = cdTipoVincContrato;
	}
	
	/**
	 * Get: digitoContaRelacionamento.
	 *
	 * @return digitoContaRelacionamento
	 */
	public String getDigitoContaRelacionamento() {
		return digitoContaRelacionamento;
	}
	
	/**
	 * Set: digitoContaRelacionamento.
	 *
	 * @param digitoContaRelacionamento the digito conta relacionamento
	 */
	public void setDigitoContaRelacionamento(String digitoContaRelacionamento) {
		this.digitoContaRelacionamento = digitoContaRelacionamento;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}
	
	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public long getCdPssoa() {
		return cdPssoa;
	}

	public void setCdPssoa(long cdPssoa) {
		this.cdPssoa = cdPssoa;
	}
}