/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoMensagemEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoMensagemEntradaDTO {

	/** Atributo cdSituacaoVinculacaoConta. */
	private int cdSituacaoVinculacaoConta;
	
	/** Atributo numeroOcorrencias. */
	private int numeroOcorrencias;
	
	
	/**
	 * Get: cdSituacaoVinculacaoConta.
	 *
	 * @return cdSituacaoVinculacaoConta
	 */
	public int getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Set: cdSituacaoVinculacaoConta.
	 *
	 * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public int getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(int numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	
	
	
	
	 

}
