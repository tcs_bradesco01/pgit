/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean;

/**
 * Nome: ConsultarSolicitacaoEstornoPagtosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoEstornoPagtosEntradaDTO {
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo dtSolicitacaoInicio. */
	private String dtSolicitacaoInicio;
	
	/** Atributo dtSolicitacaoFinal. */
	private String dtSolicitacaoFinal;
	
	/** Atributo cdSituacaoSolicitacaoPagamento. */
	private Integer cdSituacaoSolicitacaoPagamento;
	
	/** Atributo cdMotivoSituacaoSolicitacao. */
	private Integer cdMotivoSituacaoSolicitacao;    
    
    /** Atributo cdModalidadePagamentoCliente. */
    private Integer cdModalidadePagamentoCliente;
	
	/**
	 * Get: cdModalidadePagamentoCliente.
	 *
	 * @return cdModalidadePagamentoCliente
	 */
	public Integer getCdModalidadePagamentoCliente() {
		return cdModalidadePagamentoCliente;
	}

	/**
	 * Set: cdModalidadePagamentoCliente.
	 *
	 * @param cdModalidadePagamentoCliente the cd modalidade pagamento cliente
	 */
	public void setCdModalidadePagamentoCliente(Integer cdModalidadePagamentoCliente) {
		this.cdModalidadePagamentoCliente = cdModalidadePagamentoCliente;
	}

	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	/**
	 * Get: cdMotivoSituacaoSolicitacao.
	 *
	 * @return cdMotivoSituacaoSolicitacao
	 */
	public Integer getCdMotivoSituacaoSolicitacao() {
		return cdMotivoSituacaoSolicitacao;
	}
	
	/**
	 * Set: cdMotivoSituacaoSolicitacao.
	 *
	 * @param cdMotivoSituacaoSolicitacao the cd motivo situacao solicitacao
	 */
	public void setCdMotivoSituacaoSolicitacao(Integer cdMotivoSituacaoSolicitacao) {
		this.cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoSolicitacaoPagamento.
	 *
	 * @return cdSituacaoSolicitacaoPagamento
	 */
	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoSolicitacaoPagamento.
	 *
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 */
	public void setCdSituacaoSolicitacaoPagamento(
			Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtSolicitacaoInicio.
	 *
	 * @return dtSolicitacaoInicio
	 */
	public String getDtSolicitacaoInicio() {
		return dtSolicitacaoInicio;
	}
	
	/**
	 * Set: dtSolicitacaoInicio.
	 *
	 * @param dtSolicitacaoInicio the dt solicitacao inicio
	 */
	public void setDtSolicitacaoInicio(String dtSolicitacaoInicio) {
		this.dtSolicitacaoInicio = dtSolicitacaoInicio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: dtSolicitacaoFinal.
	 *
	 * @return dtSolicitacaoFinal
	 */
	public String getDtSolicitacaoFinal() {
		return dtSolicitacaoFinal;
	}
	
	/**
	 * Set: dtSolicitacaoFinal.
	 *
	 * @param dtSolicitacaoFinal the dt solicitacao final
	 */
	public void setDtSolicitacaoFinal(String dtSolicitacaoFinal) {
		this.dtSolicitacaoFinal = dtSolicitacaoFinal;
	}	
}
