/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.assoclinhatipomsg;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.AlterarLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.AlterarLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.DetalharLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.DetalharLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ExcluirLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ExcluirLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.IncluirLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.IncluirLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ListarLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ListarLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ConsultarDescricaoMensagemEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ConsultarDescricaoMensagemSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterAssociacaoLinhaTipoMensagem
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAssocLinhaTipoMsgService {
	
	
	/**
	 * Listar linha msg comprovante.
	 *
	 * @param listarLinhaMsgComprovanteEntradaDTO the listar linha msg comprovante entrada dto
	 * @return the list< listar linha msg comprovante saida dt o>
	 */
	List<ListarLinhaMsgComprovanteSaidaDTO> listarLinhaMsgComprovante (ListarLinhaMsgComprovanteEntradaDTO listarLinhaMsgComprovanteEntradaDTO);
	
	/**
	 * Incluir linha msg comprovante.
	 *
	 * @param incluirLinhaMsgComprovanteEntradaDTO the incluir linha msg comprovante entrada dto
	 * @return the incluir linha msg comprovante saida dto
	 */
	IncluirLinhaMsgComprovanteSaidaDTO incluirLinhaMsgComprovante (IncluirLinhaMsgComprovanteEntradaDTO incluirLinhaMsgComprovanteEntradaDTO);
	
	/**
	 * Alterar linha msg comprovante.
	 *
	 * @param alterarLinhaMsgComprovanteEntradaDTO the alterar linha msg comprovante entrada dto
	 * @return the alterar linha msg comprovante saida dto
	 */
	AlterarLinhaMsgComprovanteSaidaDTO alterarLinhaMsgComprovante (AlterarLinhaMsgComprovanteEntradaDTO alterarLinhaMsgComprovanteEntradaDTO);
	
	/**
	 * Excluir linha msg comprovante.
	 *
	 * @param excluirLinhaMsgComprovanteEntradaDTO the excluir linha msg comprovante entrada dto
	 * @return the excluir linha msg comprovante saida dto
	 */
	ExcluirLinhaMsgComprovanteSaidaDTO excluirLinhaMsgComprovante (ExcluirLinhaMsgComprovanteEntradaDTO excluirLinhaMsgComprovanteEntradaDTO);
	
	/**
	 * Detalhar linha msg comprovante.
	 *
	 * @param detalharLinhaMsgComprovanteEntradaDTO the detalhar linha msg comprovante entrada dto
	 * @return the detalhar linha msg comprovante saida dto
	 */
	DetalharLinhaMsgComprovanteSaidaDTO detalharLinhaMsgComprovante (DetalharLinhaMsgComprovanteEntradaDTO detalharLinhaMsgComprovanteEntradaDTO);
	
	/**
	 * Consultar descricao mensagem.
	 *
	 * @param consultarDescricaoMensagemEntradaDTO the consultar descricao mensagem entrada dto
	 * @return the consultar descricao mensagem saida dto
	 */
	ConsultarDescricaoMensagemSaidaDTO consultarDescricaoMensagem (ConsultarDescricaoMensagemEntradaDTO consultarDescricaoMensagemEntradaDTO);
	
    /**
     * M�todo de exemplo.
     */
    void sampleManterAssociacaoLinhaTipoMensagem();

}

