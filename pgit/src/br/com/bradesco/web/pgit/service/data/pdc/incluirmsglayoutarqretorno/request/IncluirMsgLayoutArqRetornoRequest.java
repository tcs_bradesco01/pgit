/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirMsgLayoutArqRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirMsgLayoutArqRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdMensagemArquivoRetorno
     */
    private java.lang.String _cdMensagemArquivoRetorno;

    /**
     * Field _cdTipoMensagemRetorno
     */
    private int _cdTipoMensagemRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemRetorno
     */
    private boolean _has_cdTipoMensagemRetorno;

    /**
     * Field _nrPosicaoinicialmensagem
     */
    private int _nrPosicaoinicialmensagem = 0;

    /**
     * keeps track of state for field: _nrPosicaoinicialmensagem
     */
    private boolean _has_nrPosicaoinicialmensagem;

    /**
     * Field _nrPosicaoFinalMensagem
     */
    private int _nrPosicaoFinalMensagem = 0;

    /**
     * keeps track of state for field: _nrPosicaoFinalMensagem
     */
    private boolean _has_nrPosicaoFinalMensagem;

    /**
     * Field _cdNivelMensagemLayout
     */
    private int _cdNivelMensagemLayout = 0;

    /**
     * keeps track of state for field: _cdNivelMensagemLayout
     */
    private boolean _has_cdNivelMensagemLayout;

    /**
     * Field _dsMensagemLayoutRetorno
     */
    private java.lang.String _dsMensagemLayoutRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirMsgLayoutArqRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.request.IncluirMsgLayoutArqRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdNivelMensagemLayout
     * 
     */
    public void deleteCdNivelMensagemLayout()
    {
        this._has_cdNivelMensagemLayout= false;
    } //-- void deleteCdNivelMensagemLayout() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoMensagemRetorno
     * 
     */
    public void deleteCdTipoMensagemRetorno()
    {
        this._has_cdTipoMensagemRetorno= false;
    } //-- void deleteCdTipoMensagemRetorno() 

    /**
     * Method deleteNrPosicaoFinalMensagem
     * 
     */
    public void deleteNrPosicaoFinalMensagem()
    {
        this._has_nrPosicaoFinalMensagem= false;
    } //-- void deleteNrPosicaoFinalMensagem() 

    /**
     * Method deleteNrPosicaoinicialmensagem
     * 
     */
    public void deleteNrPosicaoinicialmensagem()
    {
        this._has_nrPosicaoinicialmensagem= false;
    } //-- void deleteNrPosicaoinicialmensagem() 

    /**
     * Returns the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'cdMensagemArquivoRetorno'.
     */
    public java.lang.String getCdMensagemArquivoRetorno()
    {
        return this._cdMensagemArquivoRetorno;
    } //-- java.lang.String getCdMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'cdNivelMensagemLayout'.
     * 
     * @return int
     * @return the value of field 'cdNivelMensagemLayout'.
     */
    public int getCdNivelMensagemLayout()
    {
        return this._cdNivelMensagemLayout;
    } //-- int getCdNivelMensagemLayout() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoMensagemRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemRetorno'.
     */
    public int getCdTipoMensagemRetorno()
    {
        return this._cdTipoMensagemRetorno;
    } //-- int getCdTipoMensagemRetorno() 

    /**
     * Returns the value of field 'dsMensagemLayoutRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMensagemLayoutRetorno'.
     */
    public java.lang.String getDsMensagemLayoutRetorno()
    {
        return this._dsMensagemLayoutRetorno;
    } //-- java.lang.String getDsMensagemLayoutRetorno() 

    /**
     * Returns the value of field 'nrPosicaoFinalMensagem'.
     * 
     * @return int
     * @return the value of field 'nrPosicaoFinalMensagem'.
     */
    public int getNrPosicaoFinalMensagem()
    {
        return this._nrPosicaoFinalMensagem;
    } //-- int getNrPosicaoFinalMensagem() 

    /**
     * Returns the value of field 'nrPosicaoinicialmensagem'.
     * 
     * @return int
     * @return the value of field 'nrPosicaoinicialmensagem'.
     */
    public int getNrPosicaoinicialmensagem()
    {
        return this._nrPosicaoinicialmensagem;
    } //-- int getNrPosicaoinicialmensagem() 

    /**
     * Method hasCdNivelMensagemLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelMensagemLayout()
    {
        return this._has_cdNivelMensagemLayout;
    } //-- boolean hasCdNivelMensagemLayout() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoMensagemRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemRetorno()
    {
        return this._has_cdTipoMensagemRetorno;
    } //-- boolean hasCdTipoMensagemRetorno() 

    /**
     * Method hasNrPosicaoFinalMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPosicaoFinalMensagem()
    {
        return this._has_nrPosicaoFinalMensagem;
    } //-- boolean hasNrPosicaoFinalMensagem() 

    /**
     * Method hasNrPosicaoinicialmensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPosicaoinicialmensagem()
    {
        return this._has_nrPosicaoinicialmensagem;
    } //-- boolean hasNrPosicaoinicialmensagem() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @param cdMensagemArquivoRetorno the value of field
     * 'cdMensagemArquivoRetorno'.
     */
    public void setCdMensagemArquivoRetorno(java.lang.String cdMensagemArquivoRetorno)
    {
        this._cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
    } //-- void setCdMensagemArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdNivelMensagemLayout'.
     * 
     * @param cdNivelMensagemLayout the value of field
     * 'cdNivelMensagemLayout'.
     */
    public void setCdNivelMensagemLayout(int cdNivelMensagemLayout)
    {
        this._cdNivelMensagemLayout = cdNivelMensagemLayout;
        this._has_cdNivelMensagemLayout = true;
    } //-- void setCdNivelMensagemLayout(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoMensagemRetorno'.
     * 
     * @param cdTipoMensagemRetorno the value of field
     * 'cdTipoMensagemRetorno'.
     */
    public void setCdTipoMensagemRetorno(int cdTipoMensagemRetorno)
    {
        this._cdTipoMensagemRetorno = cdTipoMensagemRetorno;
        this._has_cdTipoMensagemRetorno = true;
    } //-- void setCdTipoMensagemRetorno(int) 

    /**
     * Sets the value of field 'dsMensagemLayoutRetorno'.
     * 
     * @param dsMensagemLayoutRetorno the value of field
     * 'dsMensagemLayoutRetorno'.
     */
    public void setDsMensagemLayoutRetorno(java.lang.String dsMensagemLayoutRetorno)
    {
        this._dsMensagemLayoutRetorno = dsMensagemLayoutRetorno;
    } //-- void setDsMensagemLayoutRetorno(java.lang.String) 

    /**
     * Sets the value of field 'nrPosicaoFinalMensagem'.
     * 
     * @param nrPosicaoFinalMensagem the value of field
     * 'nrPosicaoFinalMensagem'.
     */
    public void setNrPosicaoFinalMensagem(int nrPosicaoFinalMensagem)
    {
        this._nrPosicaoFinalMensagem = nrPosicaoFinalMensagem;
        this._has_nrPosicaoFinalMensagem = true;
    } //-- void setNrPosicaoFinalMensagem(int) 

    /**
     * Sets the value of field 'nrPosicaoinicialmensagem'.
     * 
     * @param nrPosicaoinicialmensagem the value of field
     * 'nrPosicaoinicialmensagem'.
     */
    public void setNrPosicaoinicialmensagem(int nrPosicaoinicialmensagem)
    {
        this._nrPosicaoinicialmensagem = nrPosicaoinicialmensagem;
        this._has_nrPosicaoinicialmensagem = true;
    } //-- void setNrPosicaoinicialmensagem(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirMsgLayoutArqRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.request.IncluirMsgLayoutArqRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.request.IncluirMsgLayoutArqRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.request.IncluirMsgLayoutArqRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.request.IncluirMsgLayoutArqRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
