/*
 * Nome: br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean;

/**
 * Nome: ExcluirFinalidadeEndOpeEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirFinalidadeEndOpeEntradaDTO {
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private Integer tipoRelacionamento;
	
	/** Atributo finalidadeEndereco. */
	private Integer finalidadeEndereco;
	
	/** Atributo cdOperacaoProdutoServico. */
	private Integer cdOperacaoProdutoServico;
	
	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public Integer getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}
	
	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}
	
	/**
	 * Get: finalidadeEndereco.
	 *
	 * @return finalidadeEndereco
	 */
	public Integer getFinalidadeEndereco() {
		return finalidadeEndereco;
	}
	
	/**
	 * Set: finalidadeEndereco.
	 *
	 * @param finalidadeEndereco the finalidade endereco
	 */
	public void setFinalidadeEndereco(Integer finalidadeEndereco) {
		this.finalidadeEndereco = finalidadeEndereco;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(Integer modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public Integer getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(Integer tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
	

}
