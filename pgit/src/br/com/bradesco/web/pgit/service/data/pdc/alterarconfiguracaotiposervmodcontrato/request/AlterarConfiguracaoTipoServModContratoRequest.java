/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AlterarConfiguracaoTipoServModContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarConfiguracaoTipoServModContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
	 * 
	 */
	private static final long serialVersionUID = 8680508113729518654L;

	/**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaCotratoNegocio
     */
    private long _nrSequenciaCotratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaCotratoNegocio
     */
    private boolean _has_nrSequenciaCotratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdParametro
     */
    private int _cdParametro = 0;

    /**
     * keeps track of state for field: _cdParametro
     */
    private boolean _has_cdParametro;

    /**
     * Field _cdProdutoOperacaroRelacionado
     */
    private int _cdProdutoOperacaroRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaroRelacionad
     */
    private boolean _has_cdProdutoOperacaroRelacionado;

    /**
     * Field _cdAcaoNaoVida
     */
    private int _cdAcaoNaoVida = 0;

    /**
     * keeps track of state for field: _cdAcaoNaoVida
     */
    private boolean _has_cdAcaoNaoVida;

    /**
     * Field _cdAcertoDadoRecadastro
     */
    private int _cdAcertoDadoRecadastro = 0;

    /**
     * keeps track of state for field: _cdAcertoDadoRecadastro
     */
    private boolean _has_cdAcertoDadoRecadastro;

    /**
     * Field _cdAgendaDebitoVeiculo
     */
    private int _cdAgendaDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdAgendaDebitoVeiculo
     */
    private boolean _has_cdAgendaDebitoVeiculo;

    /**
     * Field _cdAgendaPagamentoVencido
     */
    private int _cdAgendaPagamentoVencido = 0;

    /**
     * keeps track of state for field: _cdAgendaPagamentoVencido
     */
    private boolean _has_cdAgendaPagamentoVencido;

    /**
     * Field _cdAgendaValorMenor
     */
    private int _cdAgendaValorMenor = 0;

    /**
     * keeps track of state for field: _cdAgendaValorMenor
     */
    private boolean _has_cdAgendaValorMenor;

    /**
     * Field _cdAgrupamentoAviso
     */
    private int _cdAgrupamentoAviso = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoAviso
     */
    private boolean _has_cdAgrupamentoAviso;

    /**
     * Field _cdAgrupamentoComprovado
     */
    private int _cdAgrupamentoComprovado = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoComprovado
     */
    private boolean _has_cdAgrupamentoComprovado;

    /**
     * Field _cdAgrupamentoFormularioRecadastro
     */
    private int _cdAgrupamentoFormularioRecadastro = 0;

    /**
     * keeps track of state for field:
     * _cdAgrupamentoFormularioRecadastro
     */
    private boolean _has_cdAgrupamentoFormularioRecadastro;

    /**
     * Field _cdAntecRecadastroBeneficio
     */
    private int _cdAntecRecadastroBeneficio = 0;

    /**
     * keeps track of state for field: _cdAntecRecadastroBeneficio
     */
    private boolean _has_cdAntecRecadastroBeneficio;

    /**
     * Field _cdAreaReservada
     */
    private int _cdAreaReservada = 0;

    /**
     * keeps track of state for field: _cdAreaReservada
     */
    private boolean _has_cdAreaReservada;

    /**
     * Field _cdBaseRecadastroBeneficio
     */
    private int _cdBaseRecadastroBeneficio = 0;

    /**
     * keeps track of state for field: _cdBaseRecadastroBeneficio
     */
    private boolean _has_cdBaseRecadastroBeneficio;

    /**
     * Field _cdBloqueioEmissaoPplta
     */
    private int _cdBloqueioEmissaoPplta = 0;

    /**
     * keeps track of state for field: _cdBloqueioEmissaoPplta
     */
    private boolean _has_cdBloqueioEmissaoPplta;

    /**
     * Field _cdCapituloTituloRegistro
     */
    private int _cdCapituloTituloRegistro = 0;

    /**
     * keeps track of state for field: _cdCapituloTituloRegistro
     */
    private boolean _has_cdCapituloTituloRegistro;

    /**
     * Field _cdCobrancaTarifa
     */
    private int _cdCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdCobrancaTarifa
     */
    private boolean _has_cdCobrancaTarifa;

    /**
     * Field _cdConsDebitoVeiculo
     */
    private int _cdConsDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdConsDebitoVeiculo
     */
    private boolean _has_cdConsDebitoVeiculo;

    /**
     * Field _cdConsEndereco
     */
    private int _cdConsEndereco = 0;

    /**
     * keeps track of state for field: _cdConsEndereco
     */
    private boolean _has_cdConsEndereco;

    /**
     * Field _cdConsSaldoPagamento
     */
    private int _cdConsSaldoPagamento = 0;

    /**
     * keeps track of state for field: _cdConsSaldoPagamento
     */
    private boolean _has_cdConsSaldoPagamento;

    /**
     * Field _cdContagemConsSaudo
     */
    private int _cdContagemConsSaudo = 0;

    /**
     * keeps track of state for field: _cdContagemConsSaudo
     */
    private boolean _has_cdContagemConsSaudo;

    /**
     * Field _cdCreditoNaoUtilizado
     */
    private int _cdCreditoNaoUtilizado = 0;

    /**
     * keeps track of state for field: _cdCreditoNaoUtilizado
     */
    private boolean _has_cdCreditoNaoUtilizado;

    /**
     * Field _cdCriterioEnquaBeneficio
     */
    private int _cdCriterioEnquaBeneficio = 0;

    /**
     * keeps track of state for field: _cdCriterioEnquaBeneficio
     */
    private boolean _has_cdCriterioEnquaBeneficio;

    /**
     * Field _cdCriterioEnquaRecadastro
     */
    private int _cdCriterioEnquaRecadastro = 0;

    /**
     * keeps track of state for field: _cdCriterioEnquaRecadastro
     */
    private boolean _has_cdCriterioEnquaRecadastro;

    /**
     * Field _cdCriterioRastreabilidadeTitulo
     */
    private int _cdCriterioRastreabilidadeTitulo = 0;

    /**
     * keeps track of state for field:
     * _cdCriterioRastreabilidadeTitulo
     */
    private boolean _has_cdCriterioRastreabilidadeTitulo;

    /**
     * Field _cdCtciaEspecieBeneficio
     */
    private int _cdCtciaEspecieBeneficio = 0;

    /**
     * keeps track of state for field: _cdCtciaEspecieBeneficio
     */
    private boolean _has_cdCtciaEspecieBeneficio;

    /**
     * Field _cdCtciaIdentificacaoBeneficio
     */
    private int _cdCtciaIdentificacaoBeneficio = 0;

    /**
     * keeps track of state for field: _cdCtciaIdentificacaoBenefici
     */
    private boolean _has_cdCtciaIdentificacaoBeneficio;

    /**
     * Field _cdCtciaInscricaoFavorecido
     */
    private int _cdCtciaInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdCtciaInscricaoFavorecido
     */
    private boolean _has_cdCtciaInscricaoFavorecido;

    /**
     * Field _cdCtciaProprietarioVeiculo
     */
    private int _cdCtciaProprietarioVeiculo = 0;

    /**
     * keeps track of state for field: _cdCtciaProprietarioVeiculo
     */
    private boolean _has_cdCtciaProprietarioVeiculo;

    /**
     * Field _cdDispzContaCredito
     */
    private int _cdDispzContaCredito = 0;

    /**
     * keeps track of state for field: _cdDispzContaCredito
     */
    private boolean _has_cdDispzContaCredito;

    /**
     * Field _cdDispzDiversarCrrtt
     */
    private int _cdDispzDiversarCrrtt = 0;

    /**
     * keeps track of state for field: _cdDispzDiversarCrrtt
     */
    private boolean _has_cdDispzDiversarCrrtt;

    /**
     * Field _cdDispzDiversasNao
     */
    private int _cdDispzDiversasNao = 0;

    /**
     * keeps track of state for field: _cdDispzDiversasNao
     */
    private boolean _has_cdDispzDiversasNao;

    /**
     * Field _cdDIspzSalarioCrrtt
     */
    private int _cdDIspzSalarioCrrtt = 0;

    /**
     * keeps track of state for field: _cdDIspzSalarioCrrtt
     */
    private boolean _has_cdDIspzSalarioCrrtt;

    /**
     * Field _cdDispzSalarioNao
     */
    private int _cdDispzSalarioNao = 0;

    /**
     * keeps track of state for field: _cdDispzSalarioNao
     */
    private boolean _has_cdDispzSalarioNao;

    /**
     * Field _cdDestinoAviso
     */
    private int _cdDestinoAviso = 0;

    /**
     * keeps track of state for field: _cdDestinoAviso
     */
    private boolean _has_cdDestinoAviso;

    /**
     * Field _cdDestinoComprovante
     */
    private int _cdDestinoComprovante = 0;

    /**
     * keeps track of state for field: _cdDestinoComprovante
     */
    private boolean _has_cdDestinoComprovante;

    /**
     * Field _cdDestinoFormularioRecadastro
     */
    private int _cdDestinoFormularioRecadastro = 0;

    /**
     * keeps track of state for field: _cdDestinoFormularioRecadastr
     */
    private boolean _has_cdDestinoFormularioRecadastro;

    /**
     * Field _cdEnvelopeAberto
     */
    private int _cdEnvelopeAberto = 0;

    /**
     * keeps track of state for field: _cdEnvelopeAberto
     */
    private boolean _has_cdEnvelopeAberto;

    /**
     * Field _cdFavorecidoConsPagamento
     */
    private int _cdFavorecidoConsPagamento = 0;

    /**
     * keeps track of state for field: _cdFavorecidoConsPagamento
     */
    private boolean _has_cdFavorecidoConsPagamento;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _cdFormaEnvioPagamento
     */
    private int _cdFormaEnvioPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaEnvioPagamento
     */
    private boolean _has_cdFormaEnvioPagamento;

    /**
     * Field _cdFormaEstornoCredito
     */
    private int _cdFormaEstornoCredito = 0;

    /**
     * keeps track of state for field: _cdFormaEstornoCredito
     */
    private boolean _has_cdFormaEstornoCredito;

    /**
     * Field _cdFormaExpiracaoCredito
     */
    private int _cdFormaExpiracaoCredito = 0;

    /**
     * keeps track of state for field: _cdFormaExpiracaoCredito
     */
    private boolean _has_cdFormaExpiracaoCredito;

    /**
     * Field _cdFormaManutencao
     */
    private int _cdFormaManutencao = 0;

    /**
     * keeps track of state for field: _cdFormaManutencao
     */
    private boolean _has_cdFormaManutencao;

    /**
     * Field _cdFrasePreCadastro
     */
    private int _cdFrasePreCadastro = 0;

    /**
     * keeps track of state for field: _cdFrasePreCadastro
     */
    private boolean _has_cdFrasePreCadastro;

    /**
     * Field _cdIndicadorAgendaTitulo
     */
    private int _cdIndicadorAgendaTitulo = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaTitulo
     */
    private boolean _has_cdIndicadorAgendaTitulo;

    /**
     * Field _cdIndicadorAutorizacaoCliente
     */
    private int _cdIndicadorAutorizacaoCliente = 0;

    /**
     * keeps track of state for field: _cdIndicadorAutorizacaoClient
     */
    private boolean _has_cdIndicadorAutorizacaoCliente;

    /**
     * Field _cdIndicadorAutorizacaoComplemento
     */
    private int _cdIndicadorAutorizacaoComplemento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorAutorizacaoComplemento
     */
    private boolean _has_cdIndicadorAutorizacaoComplemento;

    /**
     * Field _cdIndicadorCadastroOrg
     */
    private int _cdIndicadorCadastroOrg = 0;

    /**
     * keeps track of state for field: _cdIndicadorCadastroOrg
     */
    private boolean _has_cdIndicadorCadastroOrg;

    /**
     * Field _cdIndicadorCadastroProcd
     */
    private int _cdIndicadorCadastroProcd = 0;

    /**
     * keeps track of state for field: _cdIndicadorCadastroProcd
     */
    private boolean _has_cdIndicadorCadastroProcd;

    /**
     * Field _cdIndicadorCataoSalario
     */
    private int _cdIndicadorCataoSalario = 0;

    /**
     * keeps track of state for field: _cdIndicadorCataoSalario
     */
    private boolean _has_cdIndicadorCataoSalario;

    /**
     * Field _cdIndicadorExpiracaoCredito
     */
    private int _cdIndicadorExpiracaoCredito = 0;

    /**
     * keeps track of state for field: _cdIndicadorExpiracaoCredito
     */
    private boolean _has_cdIndicadorExpiracaoCredito;

    /**
     * Field _cdIndicadorLancamentoPagamento
     */
    private int _cdIndicadorLancamentoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorLancamentoPagamento
     */
    private boolean _has_cdIndicadorLancamentoPagamento;

    /**
     * Field _cdIndicadorMensagemPerso
     */
    private int _cdIndicadorMensagemPerso = 0;

    /**
     * keeps track of state for field: _cdIndicadorMensagemPerso
     */
    private boolean _has_cdIndicadorMensagemPerso;

    /**
     * Field _cdLancamentoFuturoCredito
     */
    private int _cdLancamentoFuturoCredito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoCredito
     */
    private boolean _has_cdLancamentoFuturoCredito;

    /**
     * Field _cdLancamentoFuturoDebito
     */
    private int _cdLancamentoFuturoDebito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoDebito
     */
    private boolean _has_cdLancamentoFuturoDebito;

    /**
     * Field _cdLiberacaoLoteProcesso
     */
    private int _cdLiberacaoLoteProcesso = 0;

    /**
     * keeps track of state for field: _cdLiberacaoLoteProcesso
     */
    private boolean _has_cdLiberacaoLoteProcesso;

    /**
     * Field _cdManutencaoBaseRecadastro
     */
    private int _cdManutencaoBaseRecadastro = 0;

    /**
     * keeps track of state for field: _cdManutencaoBaseRecadastro
     */
    private boolean _has_cdManutencaoBaseRecadastro;

    /**
     * Field _cdMeioPagamentoDebito
     */
    private int _cdMeioPagamentoDebito = 0;

    /**
     * keeps track of state for field: _cdMeioPagamentoDebito
     */
    private boolean _has_cdMeioPagamentoDebito;

    /**
     * Field _cdMidiaDisponivel
     */
    private int _cdMidiaDisponivel = 0;

    /**
     * keeps track of state for field: _cdMidiaDisponivel
     */
    private boolean _has_cdMidiaDisponivel;

    /**
     * Field _cdMidiaMensagemRecadastro
     */
    private int _cdMidiaMensagemRecadastro = 0;

    /**
     * keeps track of state for field: _cdMidiaMensagemRecadastro
     */
    private boolean _has_cdMidiaMensagemRecadastro;

    /**
     * Field _cdMomentoAvisoRacadastro
     */
    private int _cdMomentoAvisoRacadastro = 0;

    /**
     * keeps track of state for field: _cdMomentoAvisoRacadastro
     */
    private boolean _has_cdMomentoAvisoRacadastro;

    /**
     * Field _cdMomentoCreditoEfetivacao
     */
    private int _cdMomentoCreditoEfetivacao = 0;

    /**
     * keeps track of state for field: _cdMomentoCreditoEfetivacao
     */
    private boolean _has_cdMomentoCreditoEfetivacao;

    /**
     * Field _cdMomentoDebitoPagamento
     */
    private int _cdMomentoDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdMomentoDebitoPagamento
     */
    private boolean _has_cdMomentoDebitoPagamento;

    /**
     * Field _cdMomentoFormularioRecadastro
     */
    private int _cdMomentoFormularioRecadastro = 0;

    /**
     * keeps track of state for field: _cdMomentoFormularioRecadastr
     */
    private boolean _has_cdMomentoFormularioRecadastro;

    /**
     * Field _cdMomentoProcessamentoPagamento
     */
    private int _cdMomentoProcessamentoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoProcessamentoPagamento
     */
    private boolean _has_cdMomentoProcessamentoPagamento;

    /**
     * Field _cdMensagemRecadastroMidia
     */
    private int _cdMensagemRecadastroMidia = 0;

    /**
     * keeps track of state for field: _cdMensagemRecadastroMidia
     */
    private boolean _has_cdMensagemRecadastroMidia;

    /**
     * Field _cdPermissaoDebitoOnline
     */
    private int _cdPermissaoDebitoOnline = 0;

    /**
     * keeps track of state for field: _cdPermissaoDebitoOnline
     */
    private boolean _has_cdPermissaoDebitoOnline;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _cdPeriodicidadeAviso
     */
    private int _cdPeriodicidadeAviso = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeAviso
     */
    private boolean _has_cdPeriodicidadeAviso;

    /**
     * Field _cdPerdcComprovante
     */
    private int _cdPerdcComprovante = 0;

    /**
     * keeps track of state for field: _cdPerdcComprovante
     */
    private boolean _has_cdPerdcComprovante;

    /**
     * Field _cdPerdcConsultaVeiculo
     */
    private int _cdPerdcConsultaVeiculo = 0;

    /**
     * keeps track of state for field: _cdPerdcConsultaVeiculo
     */
    private boolean _has_cdPerdcConsultaVeiculo;

    /**
     * Field _cdPerdcEnvioRemessa
     */
    private int _cdPerdcEnvioRemessa = 0;

    /**
     * keeps track of state for field: _cdPerdcEnvioRemessa
     */
    private boolean _has_cdPerdcEnvioRemessa;

    /**
     * Field _cdPerdcManutencaoProcd
     */
    private int _cdPerdcManutencaoProcd = 0;

    /**
     * keeps track of state for field: _cdPerdcManutencaoProcd
     */
    private boolean _has_cdPerdcManutencaoProcd;

    /**
     * Field _cdPagamentoNaoUtilizado
     */
    private int _cdPagamentoNaoUtilizado = 0;

    /**
     * keeps track of state for field: _cdPagamentoNaoUtilizado
     */
    private boolean _has_cdPagamentoNaoUtilizado;

    /**
     * Field _cdPrincipalEnquaRecadastro
     */
    private int _cdPrincipalEnquaRecadastro = 0;

    /**
     * keeps track of state for field: _cdPrincipalEnquaRecadastro
     */
    private boolean _has_cdPrincipalEnquaRecadastro;

    /**
     * Field _cdPrioridadeEfetivacaoPagamento
     */
    private int _cdPrioridadeEfetivacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdPrioridadeEfetivacaoPagamento
     */
    private boolean _has_cdPrioridadeEfetivacaoPagamento;

    /**
     * Field _cdRejeicaoAgendaLote
     */
    private int _cdRejeicaoAgendaLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAgendaLote
     */
    private boolean _has_cdRejeicaoAgendaLote;

    /**
     * Field _cdRejeicaoEfetivacaoLote
     */
    private int _cdRejeicaoEfetivacaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoEfetivacaoLote
     */
    private boolean _has_cdRejeicaoEfetivacaoLote;

    /**
     * Field _cdRejeicaoLote
     */
    private int _cdRejeicaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoLote
     */
    private boolean _has_cdRejeicaoLote;

    /**
     * Field _cdRastreabilidadeNotaFiscal
     */
    private int _cdRastreabilidadeNotaFiscal = 0;

    /**
     * keeps track of state for field: _cdRastreabilidadeNotaFiscal
     */
    private boolean _has_cdRastreabilidadeNotaFiscal;

    /**
     * Field _cdRastreabilidadeTituloTerceiro
     */
    private int _cdRastreabilidadeTituloTerceiro = 0;

    /**
     * keeps track of state for field:
     * _cdRastreabilidadeTituloTerceiro
     */
    private boolean _has_cdRastreabilidadeTituloTerceiro;

    /**
     * Field _cdTipoCargaRecadastro
     */
    private int _cdTipoCargaRecadastro = 0;

    /**
     * keeps track of state for field: _cdTipoCargaRecadastro
     */
    private boolean _has_cdTipoCargaRecadastro;

    /**
     * Field _cdTipoCataoSalario
     */
    private int _cdTipoCataoSalario = 0;

    /**
     * keeps track of state for field: _cdTipoCataoSalario
     */
    private boolean _has_cdTipoCataoSalario;

    /**
     * Field _cdTipoDataFloat
     */
    private int _cdTipoDataFloat = 0;

    /**
     * keeps track of state for field: _cdTipoDataFloat
     */
    private boolean _has_cdTipoDataFloat;

    /**
     * Field _cdTipoDivergenciaVeiculo
     */
    private int _cdTipoDivergenciaVeiculo = 0;

    /**
     * keeps track of state for field: _cdTipoDivergenciaVeiculo
     */
    private boolean _has_cdTipoDivergenciaVeiculo;

    /**
     * Field _cdTipoIdBeneficio
     */
    private int _cdTipoIdBeneficio = 0;

    /**
     * keeps track of state for field: _cdTipoIdBeneficio
     */
    private boolean _has_cdTipoIdBeneficio;

    /**
     * Field _cdContratoContaTransferencia
     */
    private int _cdContratoContaTransferencia = 0;

    /**
     * keeps track of state for field: _cdContratoContaTransferencia
     */
    private boolean _has_cdContratoContaTransferencia;

    /**
     * Field _cdUtilizacaoFavorecidoControle
     */
    private int _cdUtilizacaoFavorecidoControle = 0;

    /**
     * keeps track of state for field:
     * _cdUtilizacaoFavorecidoControle
     */
    private boolean _has_cdUtilizacaoFavorecidoControle;

    /**
     * Field _dtEnquaContaSalario
     */
    private java.lang.String _dtEnquaContaSalario;

    /**
     * Field _dtInicioBloqueioPapeleta
     */
    private java.lang.String _dtInicioBloqueioPapeleta;

    /**
     * Field _dtInicioRastreabilidadeTitulo
     */
    private java.lang.String _dtInicioRastreabilidadeTitulo;

    /**
     * Field _dtFimAcertoRecadastro
     */
    private java.lang.String _dtFimAcertoRecadastro;

    /**
     * Field _dtFimRecadastroBeneficio
     */
    private java.lang.String _dtFimRecadastroBeneficio;

    /**
     * Field _dtInicioAcertoRecadastro
     */
    private java.lang.String _dtInicioAcertoRecadastro;

    /**
     * Field _dtInicioRecadastroBeneficio
     */
    private java.lang.String _dtInicioRecadastroBeneficio;

    /**
     * Field _dtLimiteVinculoCarga
     */
    private java.lang.String _dtLimiteVinculoCarga;

    /**
     * Field _cdPercentualMaximoInconLote
     */
    private int _cdPercentualMaximoInconLote = 0;

    /**
     * keeps track of state for field: _cdPercentualMaximoInconLote
     */
    private boolean _has_cdPercentualMaximoInconLote;

    /**
     * Field _qtAntecedencia
     */
    private int _qtAntecedencia = 0;

    /**
     * keeps track of state for field: _qtAntecedencia
     */
    private boolean _has_qtAntecedencia;

    /**
     * Field _qtAnteriorVencimentoComprovado
     */
    private int _qtAnteriorVencimentoComprovado = 0;

    /**
     * keeps track of state for field:
     * _qtAnteriorVencimentoComprovado
     */
    private boolean _has_qtAnteriorVencimentoComprovado;

    /**
     * Field _qtDiaExpiracao
     */
    private int _qtDiaExpiracao = 0;

    /**
     * keeps track of state for field: _qtDiaExpiracao
     */
    private boolean _has_qtDiaExpiracao;

    /**
     * Field _cdDiaFloatPagamento
     */
    private int _cdDiaFloatPagamento = 0;

    /**
     * keeps track of state for field: _cdDiaFloatPagamento
     */
    private boolean _has_cdDiaFloatPagamento;

    /**
     * Field _qtDiaInatividadeFavorecido
     */
    private int _qtDiaInatividadeFavorecido = 0;

    /**
     * keeps track of state for field: _qtDiaInatividadeFavorecido
     */
    private boolean _has_qtDiaInatividadeFavorecido;

    /**
     * Field _qtDiaRepiqConsulta
     */
    private int _qtDiaRepiqConsulta = 0;

    /**
     * keeps track of state for field: _qtDiaRepiqConsulta
     */
    private boolean _has_qtDiaRepiqConsulta;

    /**
     * Field _qtEtapasRecadastroBeneficio
     */
    private int _qtEtapasRecadastroBeneficio = 0;

    /**
     * keeps track of state for field: _qtEtapasRecadastroBeneficio
     */
    private boolean _has_qtEtapasRecadastroBeneficio;

    /**
     * Field _qtFaseRecadastroBeneficio
     */
    private int _qtFaseRecadastroBeneficio = 0;

    /**
     * keeps track of state for field: _qtFaseRecadastroBeneficio
     */
    private boolean _has_qtFaseRecadastroBeneficio;

    /**
     * Field _qtLimiteLinha
     */
    private int _qtLimiteLinha = 0;

    /**
     * keeps track of state for field: _qtLimiteLinha
     */
    private boolean _has_qtLimiteLinha;

    /**
     * Field _qtLimiteSolicitacaoCatao
     */
    private int _qtLimiteSolicitacaoCatao = 0;

    /**
     * keeps track of state for field: _qtLimiteSolicitacaoCatao
     */
    private boolean _has_qtLimiteSolicitacaoCatao;

    /**
     * Field _qtMaximaInconLote
     */
    private int _qtMaximaInconLote = 0;

    /**
     * keeps track of state for field: _qtMaximaInconLote
     */
    private boolean _has_qtMaximaInconLote;

    /**
     * Field _qtMaximaTituloVencido
     */
    private int _qtMaximaTituloVencido = 0;

    /**
     * keeps track of state for field: _qtMaximaTituloVencido
     */
    private boolean _has_qtMaximaTituloVencido;

    /**
     * Field _qtMesComprovante
     */
    private int _qtMesComprovante = 0;

    /**
     * keeps track of state for field: _qtMesComprovante
     */
    private boolean _has_qtMesComprovante;

    /**
     * Field _qtMesEtapaRecadastro
     */
    private int _qtMesEtapaRecadastro = 0;

    /**
     * keeps track of state for field: _qtMesEtapaRecadastro
     */
    private boolean _has_qtMesEtapaRecadastro;

    /**
     * Field _qtMesFaseRecadastro
     */
    private int _qtMesFaseRecadastro = 0;

    /**
     * keeps track of state for field: _qtMesFaseRecadastro
     */
    private boolean _has_qtMesFaseRecadastro;

    /**
     * Field _qtViaAviso
     */
    private int _qtViaAviso = 0;

    /**
     * keeps track of state for field: _qtViaAviso
     */
    private boolean _has_qtViaAviso;

    /**
     * Field _qtViaCobranca
     */
    private int _qtViaCobranca = 0;

    /**
     * keeps track of state for field: _qtViaCobranca
     */
    private boolean _has_qtViaCobranca;

    /**
     * Field _qtViaComprovante
     */
    private int _qtViaComprovante = 0;

    /**
     * keeps track of state for field: _qtViaComprovante
     */
    private boolean _has_qtViaComprovante;

    /**
     * Field _vlFavorecidoNaoCadastro
     */
    private double _vlFavorecidoNaoCadastro = 0;

    /**
     * keeps track of state for field: _vlFavorecidoNaoCadastro
     */
    private boolean _has_vlFavorecidoNaoCadastro;

    /**
     * Field _vlLimiteDiaPagamento
     */
    private double _vlLimiteDiaPagamento = 0;

    /**
     * keeps track of state for field: _vlLimiteDiaPagamento
     */
    private boolean _has_vlLimiteDiaPagamento;

    /**
     * Field _vlLimiteIndividualPagamento
     */
    private double _vlLimiteIndividualPagamento = 0;

    /**
     * keeps track of state for field: _vlLimiteIndividualPagamento
     */
    private boolean _has_vlLimiteIndividualPagamento;

    /**
     * Field _dsAreaReservada
     */
    private java.lang.String _dsAreaReservada;

    /**
     * Field _cdIndicadorRetornoInternet
     */
    private int _cdIndicadorRetornoInternet = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoInternet
     */
    private boolean _has_cdIndicadorRetornoInternet;

    /**
     * Field _cdIndicadorListaDebito
     */
    private int _cdIndicadorListaDebito = 0;

    /**
     * keeps track of state for field: _cdIndicadorListaDebito
     */
    private boolean _has_cdIndicadorListaDebito;

    /**
     * Field _cdTipoFormacaoLista
     */
    private int _cdTipoFormacaoLista = 0;

    /**
     * keeps track of state for field: _cdTipoFormacaoLista
     */
    private boolean _has_cdTipoFormacaoLista;

    /**
     * Field _cdTipoConsistenciaLista
     */
    private int _cdTipoConsistenciaLista = 0;

    /**
     * keeps track of state for field: _cdTipoConsistenciaLista
     */
    private boolean _has_cdTipoConsistenciaLista;

    /**
     * Field _cdTipoConsultaComprovante
     */
    private int _cdTipoConsultaComprovante = 0;

    /**
     * keeps track of state for field: _cdTipoConsultaComprovante
     */
    private boolean _has_cdTipoConsultaComprovante;

    /**
     * Field _cdIndicadorBancoPostal
     */
    private int _cdIndicadorBancoPostal = 0;

    /**
     * keeps track of state for field: _cdIndicadorBancoPostal
     */
    private boolean _has_cdIndicadorBancoPostal;

    /**
     * Field _cdValidacaoNomeFavorecido
     */
    private int _cdValidacaoNomeFavorecido = 0;

    /**
     * keeps track of state for field: _cdValidacaoNomeFavorecido
     */
    private boolean _has_cdValidacaoNomeFavorecido;

    /**
     * Field _cdTipoContaFavorecido
     */
    private int _cdTipoContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoContaFavorecido
     */
    private boolean _has_cdTipoContaFavorecido;

    /**
     * Field _cdIndLancamentoPersonalizado
     */
    private int _cdIndLancamentoPersonalizado = 0;

    /**
     * keeps track of state for field: _cdIndLancamentoPersonalizado
     */
    private boolean _has_cdIndLancamentoPersonalizado;

    /**
     * Field _cdTipoIsncricaoFavorecido
     */
    private int _cdTipoIsncricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoIsncricaoFavorecido
     */
    private boolean _has_cdTipoIsncricaoFavorecido;

    /**
     * Field _cdMeioPagamentoCredito
     */
    private int _cdMeioPagamentoCredito = 0;

    /**
     * keeps track of state for field: _cdMeioPagamentoCredito
     */
    private boolean _has_cdMeioPagamentoCredito;

    /**
     * Field _cdAgendaRastreabilidadeFilial
     */
    private int _cdAgendaRastreabilidadeFilial = 0;

    /**
     * keeps track of state for field: _cdAgendaRastreabilidadeFilia
     */
    private boolean _has_cdAgendaRastreabilidadeFilial;

    /**
     * Field _cdIndicadorAdesaoSacador
     */
    private int _cdIndicadorAdesaoSacador = 0;

    /**
     * keeps track of state for field: _cdIndicadorAdesaoSacador
     */
    private boolean _has_cdIndicadorAdesaoSacador;

    /**
     * Field _cdFormularioContratoCliente
     */
    private int _cdFormularioContratoCliente = 0;

    /**
     * keeps track of state for field: _cdFormularioContratoCliente
     */
    private boolean _has_cdFormularioContratoCliente;

    /**
     * Field _cdIndicadorSeparaCanal
     */
    private int _cdIndicadorSeparaCanal = 0;

    /**
     * keeps track of state for field: _cdIndicadorSeparaCanal
     */
    private boolean _has_cdIndicadorSeparaCanal;

    /**
     * Field _cdIndicadorManutencaoProcurador
     */
    private int _cdIndicadorManutencaoProcurador = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorManutencaoProcurador
     */
    private boolean _has_cdIndicadorManutencaoProcurador;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdSequenciaEnderecoPessoa
     */
    private int _cdSequenciaEnderecoPessoa = 0;

    /**
     * keeps track of state for field: _cdSequenciaEnderecoPessoa
     */
    private boolean _has_cdSequenciaEnderecoPessoa;

    /**
     * Field _cdTipoEnderecoPessoa
     */
    private int _cdTipoEnderecoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoEnderecoPessoa
     */
    private boolean _has_cdTipoEnderecoPessoa;

    /**
     * Field _cdEspecieEnderecoPessoa
     */
    private int _cdEspecieEnderecoPessoa = 0;

    /**
     * keeps track of state for field: _cdEspecieEnderecoPessoa
     */
    private boolean _has_cdEspecieEnderecoPessoa;

    /**
     * Field _cdIndicadorEmissaoAviso
     */
    private int _cdIndicadorEmissaoAviso = 0;

    /**
     * keeps track of state for field: _cdIndicadorEmissaoAviso
     */
    private boolean _has_cdIndicadorEmissaoAviso;

    /**
     * Field _cdLocalEmissao
     */
    private int _cdLocalEmissao = 0;

    /**
     * keeps track of state for field: _cdLocalEmissao
     */
    private boolean _has_cdLocalEmissao;

    /**
     * Field _cdConsultaSaldoValorSuperior
     */
    private int _cdConsultaSaldoValorSuperior = 0;

    /**
     * keeps track of state for field: _cdConsultaSaldoValorSuperior
     */
    private boolean _has_cdConsultaSaldoValorSuperior;

    /**
     * Field _cdIndicadorFeriadoLocal
     */
    private int _cdIndicadorFeriadoLocal = 0;

    /**
     * keeps track of state for field: _cdIndicadorFeriadoLocal
     */
    private boolean _has_cdIndicadorFeriadoLocal;

    /**
     * Field _cdIndicadorSegundaLinha
     */
    private int _cdIndicadorSegundaLinha = 0;

    /**
     * keeps track of state for field: _cdIndicadorSegundaLinha
     */
    private boolean _has_cdIndicadorSegundaLinha;

    /**
     * Field _cdFloatServicoContrato
     */
    private int _cdFloatServicoContrato = 0;

    /**
     * keeps track of state for field: _cdFloatServicoContrato
     */
    private boolean _has_cdFloatServicoContrato;

    /**
     * Field _qtDiaUtilPgto
     */
    private int _qtDiaUtilPgto = 0;

    /**
     * keeps track of state for field: _qtDiaUtilPgto
     */
    private boolean _has_qtDiaUtilPgto;

    /**
     * Field _cdExigeAutFilial
     */
    private int _cdExigeAutFilial = 0;

    /**
     * keeps track of state for field: _cdExigeAutFilial
     */
    private boolean _has_cdExigeAutFilial;

    /**
     * Field _cdPreenchimentoLancamentoPersonalizado
     */
    private int _cdPreenchimentoLancamentoPersonalizado = 0;

    /**
     * keeps track of state for field:
     * _cdPreenchimentoLancamentoPersonalizado
     */
    private boolean _has_cdPreenchimentoLancamentoPersonalizado;

    /**
     * Field _cdIndicadorAgendaGrade
     */
    private int _cdIndicadorAgendaGrade = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaGrade
     */
    private boolean _has_cdIndicadorAgendaGrade;

    /**
     * Field _cdTituloDdaRetorno
     */
    private int _cdTituloDdaRetorno = 0;

    /**
     * keeps track of state for field: _cdTituloDdaRetorno
     */
    private boolean _has_cdTituloDdaRetorno;

    /**
     * Field _cdIndicadorUtilizaMora
     */
    private int _cdIndicadorUtilizaMora = 0;

    /**
     * keeps track of state for field: _cdIndicadorUtilizaMora
     */
    private boolean _has_cdIndicadorUtilizaMora;

    /**
     * Field _vlPercentualDiferencaTolerada
     */
    private double _vlPercentualDiferencaTolerada = 0;

    /**
     * keeps track of state for field: _vlPercentualDiferencaTolerad
     */
    private boolean _has_vlPercentualDiferencaTolerada;

    /**
     * Field _cdConsistenciaCpfCnpjBenefAvalNpc
     */
    private int _cdConsistenciaCpfCnpjBenefAvalNpc = 0;

    /**
     * keeps track of state for field:
     * _cdConsistenciaCpfCnpjBenefAvalNpc
     */
    private boolean _has_cdConsistenciaCpfCnpjBenefAvalNpc;

    /**
     * Field _cindcdFantsRepas
     */
    private int _cindcdFantsRepas = 0;

    /**
     * keeps track of state for field: _cindcdFantsRepas
     */
    private boolean _has_cindcdFantsRepas;

    /**
     * Field _cdIndicadorTipoRetornoInternet
     */
    private int _cdIndicadorTipoRetornoInternet = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorTipoRetornoInternet
     */
    private boolean _has_cdIndicadorTipoRetornoInternet;

    /**
     * Field _cdusuario
     */
    private java.lang.String _cdusuario;

    /**
     * Field _cdUsuarioExterno
     */
    private java.lang.String _cdUsuarioExterno;

    /**
     * Field _cdCanal
     */
    private int _cdCanal = 0;

    /**
     * keeps track of state for field: _cdCanal
     */
    private boolean _has_cdCanal;

    /**
     * Field _nmOperacaoFluxo
     */
    private java.lang.String _nmOperacaoFluxo;

    /**
     * Field _cdEmpresaOperante
     */
    private long _cdEmpresaOperante = 0;

    /**
     * keeps track of state for field: _cdEmpresaOperante
     */
    private boolean _has_cdEmpresaOperante;

    /**
     * Field _cdDependenteOperante
     */
    private int _cdDependenteOperante = 0;

    /**
     * keeps track of state for field: _cdDependenteOperante
     */
    private boolean _has_cdDependenteOperante;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarConfiguracaoTipoServModContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.request.AlterarConfiguracaoTipoServModContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoNaoVida
     * 
     */
    public void deleteCdAcaoNaoVida()
    {
        this._has_cdAcaoNaoVida= false;
    } //-- void deleteCdAcaoNaoVida() 

    /**
     * Method deleteCdAcertoDadoRecadastro
     * 
     */
    public void deleteCdAcertoDadoRecadastro()
    {
        this._has_cdAcertoDadoRecadastro= false;
    } //-- void deleteCdAcertoDadoRecadastro() 

    /**
     * Method deleteCdAgendaDebitoVeiculo
     * 
     */
    public void deleteCdAgendaDebitoVeiculo()
    {
        this._has_cdAgendaDebitoVeiculo= false;
    } //-- void deleteCdAgendaDebitoVeiculo() 

    /**
     * Method deleteCdAgendaPagamentoVencido
     * 
     */
    public void deleteCdAgendaPagamentoVencido()
    {
        this._has_cdAgendaPagamentoVencido= false;
    } //-- void deleteCdAgendaPagamentoVencido() 

    /**
     * Method deleteCdAgendaRastreabilidadeFilial
     * 
     */
    public void deleteCdAgendaRastreabilidadeFilial()
    {
        this._has_cdAgendaRastreabilidadeFilial= false;
    } //-- void deleteCdAgendaRastreabilidadeFilial() 

    /**
     * Method deleteCdAgendaValorMenor
     * 
     */
    public void deleteCdAgendaValorMenor()
    {
        this._has_cdAgendaValorMenor= false;
    } //-- void deleteCdAgendaValorMenor() 

    /**
     * Method deleteCdAgrupamentoAviso
     * 
     */
    public void deleteCdAgrupamentoAviso()
    {
        this._has_cdAgrupamentoAviso= false;
    } //-- void deleteCdAgrupamentoAviso() 

    /**
     * Method deleteCdAgrupamentoComprovado
     * 
     */
    public void deleteCdAgrupamentoComprovado()
    {
        this._has_cdAgrupamentoComprovado= false;
    } //-- void deleteCdAgrupamentoComprovado() 

    /**
     * Method deleteCdAgrupamentoFormularioRecadastro
     * 
     */
    public void deleteCdAgrupamentoFormularioRecadastro()
    {
        this._has_cdAgrupamentoFormularioRecadastro= false;
    } //-- void deleteCdAgrupamentoFormularioRecadastro() 

    /**
     * Method deleteCdAntecRecadastroBeneficio
     * 
     */
    public void deleteCdAntecRecadastroBeneficio()
    {
        this._has_cdAntecRecadastroBeneficio= false;
    } //-- void deleteCdAntecRecadastroBeneficio() 

    /**
     * Method deleteCdAreaReservada
     * 
     */
    public void deleteCdAreaReservada()
    {
        this._has_cdAreaReservada= false;
    } //-- void deleteCdAreaReservada() 

    /**
     * Method deleteCdBaseRecadastroBeneficio
     * 
     */
    public void deleteCdBaseRecadastroBeneficio()
    {
        this._has_cdBaseRecadastroBeneficio= false;
    } //-- void deleteCdBaseRecadastroBeneficio() 

    /**
     * Method deleteCdBloqueioEmissaoPplta
     * 
     */
    public void deleteCdBloqueioEmissaoPplta()
    {
        this._has_cdBloqueioEmissaoPplta= false;
    } //-- void deleteCdBloqueioEmissaoPplta() 

    /**
     * Method deleteCdCanal
     * 
     */
    public void deleteCdCanal()
    {
        this._has_cdCanal= false;
    } //-- void deleteCdCanal() 

    /**
     * Method deleteCdCapituloTituloRegistro
     * 
     */
    public void deleteCdCapituloTituloRegistro()
    {
        this._has_cdCapituloTituloRegistro= false;
    } //-- void deleteCdCapituloTituloRegistro() 

    /**
     * Method deleteCdCobrancaTarifa
     * 
     */
    public void deleteCdCobrancaTarifa()
    {
        this._has_cdCobrancaTarifa= false;
    } //-- void deleteCdCobrancaTarifa() 

    /**
     * Method deleteCdConsDebitoVeiculo
     * 
     */
    public void deleteCdConsDebitoVeiculo()
    {
        this._has_cdConsDebitoVeiculo= false;
    } //-- void deleteCdConsDebitoVeiculo() 

    /**
     * Method deleteCdConsEndereco
     * 
     */
    public void deleteCdConsEndereco()
    {
        this._has_cdConsEndereco= false;
    } //-- void deleteCdConsEndereco() 

    /**
     * Method deleteCdConsSaldoPagamento
     * 
     */
    public void deleteCdConsSaldoPagamento()
    {
        this._has_cdConsSaldoPagamento= false;
    } //-- void deleteCdConsSaldoPagamento() 

    /**
     * Method deleteCdConsistenciaCpfCnpjBenefAvalNpc
     * 
     */
    public void deleteCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        this._has_cdConsistenciaCpfCnpjBenefAvalNpc= false;
    } //-- void deleteCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Method deleteCdConsultaSaldoValorSuperior
     * 
     */
    public void deleteCdConsultaSaldoValorSuperior()
    {
        this._has_cdConsultaSaldoValorSuperior= false;
    } //-- void deleteCdConsultaSaldoValorSuperior() 

    /**
     * Method deleteCdContagemConsSaudo
     * 
     */
    public void deleteCdContagemConsSaudo()
    {
        this._has_cdContagemConsSaudo= false;
    } //-- void deleteCdContagemConsSaudo() 

    /**
     * Method deleteCdContratoContaTransferencia
     * 
     */
    public void deleteCdContratoContaTransferencia()
    {
        this._has_cdContratoContaTransferencia= false;
    } //-- void deleteCdContratoContaTransferencia() 

    /**
     * Method deleteCdCreditoNaoUtilizado
     * 
     */
    public void deleteCdCreditoNaoUtilizado()
    {
        this._has_cdCreditoNaoUtilizado= false;
    } //-- void deleteCdCreditoNaoUtilizado() 

    /**
     * Method deleteCdCriterioEnquaBeneficio
     * 
     */
    public void deleteCdCriterioEnquaBeneficio()
    {
        this._has_cdCriterioEnquaBeneficio= false;
    } //-- void deleteCdCriterioEnquaBeneficio() 

    /**
     * Method deleteCdCriterioEnquaRecadastro
     * 
     */
    public void deleteCdCriterioEnquaRecadastro()
    {
        this._has_cdCriterioEnquaRecadastro= false;
    } //-- void deleteCdCriterioEnquaRecadastro() 

    /**
     * Method deleteCdCriterioRastreabilidadeTitulo
     * 
     */
    public void deleteCdCriterioRastreabilidadeTitulo()
    {
        this._has_cdCriterioRastreabilidadeTitulo= false;
    } //-- void deleteCdCriterioRastreabilidadeTitulo() 

    /**
     * Method deleteCdCtciaEspecieBeneficio
     * 
     */
    public void deleteCdCtciaEspecieBeneficio()
    {
        this._has_cdCtciaEspecieBeneficio= false;
    } //-- void deleteCdCtciaEspecieBeneficio() 

    /**
     * Method deleteCdCtciaIdentificacaoBeneficio
     * 
     */
    public void deleteCdCtciaIdentificacaoBeneficio()
    {
        this._has_cdCtciaIdentificacaoBeneficio= false;
    } //-- void deleteCdCtciaIdentificacaoBeneficio() 

    /**
     * Method deleteCdCtciaInscricaoFavorecido
     * 
     */
    public void deleteCdCtciaInscricaoFavorecido()
    {
        this._has_cdCtciaInscricaoFavorecido= false;
    } //-- void deleteCdCtciaInscricaoFavorecido() 

    /**
     * Method deleteCdCtciaProprietarioVeiculo
     * 
     */
    public void deleteCdCtciaProprietarioVeiculo()
    {
        this._has_cdCtciaProprietarioVeiculo= false;
    } //-- void deleteCdCtciaProprietarioVeiculo() 

    /**
     * Method deleteCdDIspzSalarioCrrtt
     * 
     */
    public void deleteCdDIspzSalarioCrrtt()
    {
        this._has_cdDIspzSalarioCrrtt= false;
    } //-- void deleteCdDIspzSalarioCrrtt() 

    /**
     * Method deleteCdDependenteOperante
     * 
     */
    public void deleteCdDependenteOperante()
    {
        this._has_cdDependenteOperante= false;
    } //-- void deleteCdDependenteOperante() 

    /**
     * Method deleteCdDestinoAviso
     * 
     */
    public void deleteCdDestinoAviso()
    {
        this._has_cdDestinoAviso= false;
    } //-- void deleteCdDestinoAviso() 

    /**
     * Method deleteCdDestinoComprovante
     * 
     */
    public void deleteCdDestinoComprovante()
    {
        this._has_cdDestinoComprovante= false;
    } //-- void deleteCdDestinoComprovante() 

    /**
     * Method deleteCdDestinoFormularioRecadastro
     * 
     */
    public void deleteCdDestinoFormularioRecadastro()
    {
        this._has_cdDestinoFormularioRecadastro= false;
    } //-- void deleteCdDestinoFormularioRecadastro() 

    /**
     * Method deleteCdDiaFloatPagamento
     * 
     */
    public void deleteCdDiaFloatPagamento()
    {
        this._has_cdDiaFloatPagamento= false;
    } //-- void deleteCdDiaFloatPagamento() 

    /**
     * Method deleteCdDispzContaCredito
     * 
     */
    public void deleteCdDispzContaCredito()
    {
        this._has_cdDispzContaCredito= false;
    } //-- void deleteCdDispzContaCredito() 

    /**
     * Method deleteCdDispzDiversarCrrtt
     * 
     */
    public void deleteCdDispzDiversarCrrtt()
    {
        this._has_cdDispzDiversarCrrtt= false;
    } //-- void deleteCdDispzDiversarCrrtt() 

    /**
     * Method deleteCdDispzDiversasNao
     * 
     */
    public void deleteCdDispzDiversasNao()
    {
        this._has_cdDispzDiversasNao= false;
    } //-- void deleteCdDispzDiversasNao() 

    /**
     * Method deleteCdDispzSalarioNao
     * 
     */
    public void deleteCdDispzSalarioNao()
    {
        this._has_cdDispzSalarioNao= false;
    } //-- void deleteCdDispzSalarioNao() 

    /**
     * Method deleteCdEmpresaOperante
     * 
     */
    public void deleteCdEmpresaOperante()
    {
        this._has_cdEmpresaOperante= false;
    } //-- void deleteCdEmpresaOperante() 

    /**
     * Method deleteCdEnvelopeAberto
     * 
     */
    public void deleteCdEnvelopeAberto()
    {
        this._has_cdEnvelopeAberto= false;
    } //-- void deleteCdEnvelopeAberto() 

    /**
     * Method deleteCdEspecieEnderecoPessoa
     * 
     */
    public void deleteCdEspecieEnderecoPessoa()
    {
        this._has_cdEspecieEnderecoPessoa= false;
    } //-- void deleteCdEspecieEnderecoPessoa() 

    /**
     * Method deleteCdExigeAutFilial
     * 
     */
    public void deleteCdExigeAutFilial()
    {
        this._has_cdExigeAutFilial= false;
    } //-- void deleteCdExigeAutFilial() 

    /**
     * Method deleteCdFavorecidoConsPagamento
     * 
     */
    public void deleteCdFavorecidoConsPagamento()
    {
        this._has_cdFavorecidoConsPagamento= false;
    } //-- void deleteCdFavorecidoConsPagamento() 

    /**
     * Method deleteCdFloatServicoContrato
     * 
     */
    public void deleteCdFloatServicoContrato()
    {
        this._has_cdFloatServicoContrato= false;
    } //-- void deleteCdFloatServicoContrato() 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFormaEnvioPagamento
     * 
     */
    public void deleteCdFormaEnvioPagamento()
    {
        this._has_cdFormaEnvioPagamento= false;
    } //-- void deleteCdFormaEnvioPagamento() 

    /**
     * Method deleteCdFormaEstornoCredito
     * 
     */
    public void deleteCdFormaEstornoCredito()
    {
        this._has_cdFormaEstornoCredito= false;
    } //-- void deleteCdFormaEstornoCredito() 

    /**
     * Method deleteCdFormaExpiracaoCredito
     * 
     */
    public void deleteCdFormaExpiracaoCredito()
    {
        this._has_cdFormaExpiracaoCredito= false;
    } //-- void deleteCdFormaExpiracaoCredito() 

    /**
     * Method deleteCdFormaManutencao
     * 
     */
    public void deleteCdFormaManutencao()
    {
        this._has_cdFormaManutencao= false;
    } //-- void deleteCdFormaManutencao() 

    /**
     * Method deleteCdFormularioContratoCliente
     * 
     */
    public void deleteCdFormularioContratoCliente()
    {
        this._has_cdFormularioContratoCliente= false;
    } //-- void deleteCdFormularioContratoCliente() 

    /**
     * Method deleteCdFrasePreCadastro
     * 
     */
    public void deleteCdFrasePreCadastro()
    {
        this._has_cdFrasePreCadastro= false;
    } //-- void deleteCdFrasePreCadastro() 

    /**
     * Method deleteCdIndLancamentoPersonalizado
     * 
     */
    public void deleteCdIndLancamentoPersonalizado()
    {
        this._has_cdIndLancamentoPersonalizado= false;
    } //-- void deleteCdIndLancamentoPersonalizado() 

    /**
     * Method deleteCdIndicadorAdesaoSacador
     * 
     */
    public void deleteCdIndicadorAdesaoSacador()
    {
        this._has_cdIndicadorAdesaoSacador= false;
    } //-- void deleteCdIndicadorAdesaoSacador() 

    /**
     * Method deleteCdIndicadorAgendaGrade
     * 
     */
    public void deleteCdIndicadorAgendaGrade()
    {
        this._has_cdIndicadorAgendaGrade= false;
    } //-- void deleteCdIndicadorAgendaGrade() 

    /**
     * Method deleteCdIndicadorAgendaTitulo
     * 
     */
    public void deleteCdIndicadorAgendaTitulo()
    {
        this._has_cdIndicadorAgendaTitulo= false;
    } //-- void deleteCdIndicadorAgendaTitulo() 

    /**
     * Method deleteCdIndicadorAutorizacaoCliente
     * 
     */
    public void deleteCdIndicadorAutorizacaoCliente()
    {
        this._has_cdIndicadorAutorizacaoCliente= false;
    } //-- void deleteCdIndicadorAutorizacaoCliente() 

    /**
     * Method deleteCdIndicadorAutorizacaoComplemento
     * 
     */
    public void deleteCdIndicadorAutorizacaoComplemento()
    {
        this._has_cdIndicadorAutorizacaoComplemento= false;
    } //-- void deleteCdIndicadorAutorizacaoComplemento() 

    /**
     * Method deleteCdIndicadorBancoPostal
     * 
     */
    public void deleteCdIndicadorBancoPostal()
    {
        this._has_cdIndicadorBancoPostal= false;
    } //-- void deleteCdIndicadorBancoPostal() 

    /**
     * Method deleteCdIndicadorCadastroOrg
     * 
     */
    public void deleteCdIndicadorCadastroOrg()
    {
        this._has_cdIndicadorCadastroOrg= false;
    } //-- void deleteCdIndicadorCadastroOrg() 

    /**
     * Method deleteCdIndicadorCadastroProcd
     * 
     */
    public void deleteCdIndicadorCadastroProcd()
    {
        this._has_cdIndicadorCadastroProcd= false;
    } //-- void deleteCdIndicadorCadastroProcd() 

    /**
     * Method deleteCdIndicadorCataoSalario
     * 
     */
    public void deleteCdIndicadorCataoSalario()
    {
        this._has_cdIndicadorCataoSalario= false;
    } //-- void deleteCdIndicadorCataoSalario() 

    /**
     * Method deleteCdIndicadorEmissaoAviso
     * 
     */
    public void deleteCdIndicadorEmissaoAviso()
    {
        this._has_cdIndicadorEmissaoAviso= false;
    } //-- void deleteCdIndicadorEmissaoAviso() 

    /**
     * Method deleteCdIndicadorExpiracaoCredito
     * 
     */
    public void deleteCdIndicadorExpiracaoCredito()
    {
        this._has_cdIndicadorExpiracaoCredito= false;
    } //-- void deleteCdIndicadorExpiracaoCredito() 

    /**
     * Method deleteCdIndicadorFeriadoLocal
     * 
     */
    public void deleteCdIndicadorFeriadoLocal()
    {
        this._has_cdIndicadorFeriadoLocal= false;
    } //-- void deleteCdIndicadorFeriadoLocal() 

    /**
     * Method deleteCdIndicadorLancamentoPagamento
     * 
     */
    public void deleteCdIndicadorLancamentoPagamento()
    {
        this._has_cdIndicadorLancamentoPagamento= false;
    } //-- void deleteCdIndicadorLancamentoPagamento() 

    /**
     * Method deleteCdIndicadorListaDebito
     * 
     */
    public void deleteCdIndicadorListaDebito()
    {
        this._has_cdIndicadorListaDebito= false;
    } //-- void deleteCdIndicadorListaDebito() 

    /**
     * Method deleteCdIndicadorManutencaoProcurador
     * 
     */
    public void deleteCdIndicadorManutencaoProcurador()
    {
        this._has_cdIndicadorManutencaoProcurador= false;
    } //-- void deleteCdIndicadorManutencaoProcurador() 

    /**
     * Method deleteCdIndicadorMensagemPerso
     * 
     */
    public void deleteCdIndicadorMensagemPerso()
    {
        this._has_cdIndicadorMensagemPerso= false;
    } //-- void deleteCdIndicadorMensagemPerso() 

    /**
     * Method deleteCdIndicadorRetornoInternet
     * 
     */
    public void deleteCdIndicadorRetornoInternet()
    {
        this._has_cdIndicadorRetornoInternet= false;
    } //-- void deleteCdIndicadorRetornoInternet() 

    /**
     * Method deleteCdIndicadorSegundaLinha
     * 
     */
    public void deleteCdIndicadorSegundaLinha()
    {
        this._has_cdIndicadorSegundaLinha= false;
    } //-- void deleteCdIndicadorSegundaLinha() 

    /**
     * Method deleteCdIndicadorSeparaCanal
     * 
     */
    public void deleteCdIndicadorSeparaCanal()
    {
        this._has_cdIndicadorSeparaCanal= false;
    } //-- void deleteCdIndicadorSeparaCanal() 

    /**
     * Method deleteCdIndicadorTipoRetornoInternet
     * 
     */
    public void deleteCdIndicadorTipoRetornoInternet()
    {
        this._has_cdIndicadorTipoRetornoInternet= false;
    } //-- void deleteCdIndicadorTipoRetornoInternet() 

    /**
     * Method deleteCdIndicadorUtilizaMora
     * 
     */
    public void deleteCdIndicadorUtilizaMora()
    {
        this._has_cdIndicadorUtilizaMora= false;
    } //-- void deleteCdIndicadorUtilizaMora() 

    /**
     * Method deleteCdLancamentoFuturoCredito
     * 
     */
    public void deleteCdLancamentoFuturoCredito()
    {
        this._has_cdLancamentoFuturoCredito= false;
    } //-- void deleteCdLancamentoFuturoCredito() 

    /**
     * Method deleteCdLancamentoFuturoDebito
     * 
     */
    public void deleteCdLancamentoFuturoDebito()
    {
        this._has_cdLancamentoFuturoDebito= false;
    } //-- void deleteCdLancamentoFuturoDebito() 

    /**
     * Method deleteCdLiberacaoLoteProcesso
     * 
     */
    public void deleteCdLiberacaoLoteProcesso()
    {
        this._has_cdLiberacaoLoteProcesso= false;
    } //-- void deleteCdLiberacaoLoteProcesso() 

    /**
     * Method deleteCdLocalEmissao
     * 
     */
    public void deleteCdLocalEmissao()
    {
        this._has_cdLocalEmissao= false;
    } //-- void deleteCdLocalEmissao() 

    /**
     * Method deleteCdManutencaoBaseRecadastro
     * 
     */
    public void deleteCdManutencaoBaseRecadastro()
    {
        this._has_cdManutencaoBaseRecadastro= false;
    } //-- void deleteCdManutencaoBaseRecadastro() 

    /**
     * Method deleteCdMeioPagamentoCredito
     * 
     */
    public void deleteCdMeioPagamentoCredito()
    {
        this._has_cdMeioPagamentoCredito= false;
    } //-- void deleteCdMeioPagamentoCredito() 

    /**
     * Method deleteCdMeioPagamentoDebito
     * 
     */
    public void deleteCdMeioPagamentoDebito()
    {
        this._has_cdMeioPagamentoDebito= false;
    } //-- void deleteCdMeioPagamentoDebito() 

    /**
     * Method deleteCdMensagemRecadastroMidia
     * 
     */
    public void deleteCdMensagemRecadastroMidia()
    {
        this._has_cdMensagemRecadastroMidia= false;
    } //-- void deleteCdMensagemRecadastroMidia() 

    /**
     * Method deleteCdMidiaDisponivel
     * 
     */
    public void deleteCdMidiaDisponivel()
    {
        this._has_cdMidiaDisponivel= false;
    } //-- void deleteCdMidiaDisponivel() 

    /**
     * Method deleteCdMidiaMensagemRecadastro
     * 
     */
    public void deleteCdMidiaMensagemRecadastro()
    {
        this._has_cdMidiaMensagemRecadastro= false;
    } //-- void deleteCdMidiaMensagemRecadastro() 

    /**
     * Method deleteCdMomentoAvisoRacadastro
     * 
     */
    public void deleteCdMomentoAvisoRacadastro()
    {
        this._has_cdMomentoAvisoRacadastro= false;
    } //-- void deleteCdMomentoAvisoRacadastro() 

    /**
     * Method deleteCdMomentoCreditoEfetivacao
     * 
     */
    public void deleteCdMomentoCreditoEfetivacao()
    {
        this._has_cdMomentoCreditoEfetivacao= false;
    } //-- void deleteCdMomentoCreditoEfetivacao() 

    /**
     * Method deleteCdMomentoDebitoPagamento
     * 
     */
    public void deleteCdMomentoDebitoPagamento()
    {
        this._has_cdMomentoDebitoPagamento= false;
    } //-- void deleteCdMomentoDebitoPagamento() 

    /**
     * Method deleteCdMomentoFormularioRecadastro
     * 
     */
    public void deleteCdMomentoFormularioRecadastro()
    {
        this._has_cdMomentoFormularioRecadastro= false;
    } //-- void deleteCdMomentoFormularioRecadastro() 

    /**
     * Method deleteCdMomentoProcessamentoPagamento
     * 
     */
    public void deleteCdMomentoProcessamentoPagamento()
    {
        this._has_cdMomentoProcessamentoPagamento= false;
    } //-- void deleteCdMomentoProcessamentoPagamento() 

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdPagamentoNaoUtilizado
     * 
     */
    public void deleteCdPagamentoNaoUtilizado()
    {
        this._has_cdPagamentoNaoUtilizado= false;
    } //-- void deleteCdPagamentoNaoUtilizado() 

    /**
     * Method deleteCdParametro
     * 
     */
    public void deleteCdParametro()
    {
        this._has_cdParametro= false;
    } //-- void deleteCdParametro() 

    /**
     * Method deleteCdPercentualMaximoInconLote
     * 
     */
    public void deleteCdPercentualMaximoInconLote()
    {
        this._has_cdPercentualMaximoInconLote= false;
    } //-- void deleteCdPercentualMaximoInconLote() 

    /**
     * Method deleteCdPerdcComprovante
     * 
     */
    public void deleteCdPerdcComprovante()
    {
        this._has_cdPerdcComprovante= false;
    } //-- void deleteCdPerdcComprovante() 

    /**
     * Method deleteCdPerdcConsultaVeiculo
     * 
     */
    public void deleteCdPerdcConsultaVeiculo()
    {
        this._has_cdPerdcConsultaVeiculo= false;
    } //-- void deleteCdPerdcConsultaVeiculo() 

    /**
     * Method deleteCdPerdcEnvioRemessa
     * 
     */
    public void deleteCdPerdcEnvioRemessa()
    {
        this._has_cdPerdcEnvioRemessa= false;
    } //-- void deleteCdPerdcEnvioRemessa() 

    /**
     * Method deleteCdPerdcManutencaoProcd
     * 
     */
    public void deleteCdPerdcManutencaoProcd()
    {
        this._has_cdPerdcManutencaoProcd= false;
    } //-- void deleteCdPerdcManutencaoProcd() 

    /**
     * Method deleteCdPeriodicidadeAviso
     * 
     */
    public void deleteCdPeriodicidadeAviso()
    {
        this._has_cdPeriodicidadeAviso= false;
    } //-- void deleteCdPeriodicidadeAviso() 

    /**
     * Method deleteCdPermissaoDebitoOnline
     * 
     */
    public void deleteCdPermissaoDebitoOnline()
    {
        this._has_cdPermissaoDebitoOnline= false;
    } //-- void deleteCdPermissaoDebitoOnline() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPreenchimentoLancamentoPersonalizado
     * 
     */
    public void deleteCdPreenchimentoLancamentoPersonalizado()
    {
        this._has_cdPreenchimentoLancamentoPersonalizado= false;
    } //-- void deleteCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Method deleteCdPrincipalEnquaRecadastro
     * 
     */
    public void deleteCdPrincipalEnquaRecadastro()
    {
        this._has_cdPrincipalEnquaRecadastro= false;
    } //-- void deleteCdPrincipalEnquaRecadastro() 

    /**
     * Method deleteCdPrioridadeEfetivacaoPagamento
     * 
     */
    public void deleteCdPrioridadeEfetivacaoPagamento()
    {
        this._has_cdPrioridadeEfetivacaoPagamento= false;
    } //-- void deleteCdPrioridadeEfetivacaoPagamento() 

    /**
     * Method deleteCdProdutoOperacaroRelacionado
     * 
     */
    public void deleteCdProdutoOperacaroRelacionado()
    {
        this._has_cdProdutoOperacaroRelacionado= false;
    } //-- void deleteCdProdutoOperacaroRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRastreabilidadeNotaFiscal
     * 
     */
    public void deleteCdRastreabilidadeNotaFiscal()
    {
        this._has_cdRastreabilidadeNotaFiscal= false;
    } //-- void deleteCdRastreabilidadeNotaFiscal() 

    /**
     * Method deleteCdRastreabilidadeTituloTerceiro
     * 
     */
    public void deleteCdRastreabilidadeTituloTerceiro()
    {
        this._has_cdRastreabilidadeTituloTerceiro= false;
    } //-- void deleteCdRastreabilidadeTituloTerceiro() 

    /**
     * Method deleteCdRejeicaoAgendaLote
     * 
     */
    public void deleteCdRejeicaoAgendaLote()
    {
        this._has_cdRejeicaoAgendaLote= false;
    } //-- void deleteCdRejeicaoAgendaLote() 

    /**
     * Method deleteCdRejeicaoEfetivacaoLote
     * 
     */
    public void deleteCdRejeicaoEfetivacaoLote()
    {
        this._has_cdRejeicaoEfetivacaoLote= false;
    } //-- void deleteCdRejeicaoEfetivacaoLote() 

    /**
     * Method deleteCdRejeicaoLote
     * 
     */
    public void deleteCdRejeicaoLote()
    {
        this._has_cdRejeicaoLote= false;
    } //-- void deleteCdRejeicaoLote() 

    /**
     * Method deleteCdSequenciaEnderecoPessoa
     * 
     */
    public void deleteCdSequenciaEnderecoPessoa()
    {
        this._has_cdSequenciaEnderecoPessoa= false;
    } //-- void deleteCdSequenciaEnderecoPessoa() 

    /**
     * Method deleteCdTipoCargaRecadastro
     * 
     */
    public void deleteCdTipoCargaRecadastro()
    {
        this._has_cdTipoCargaRecadastro= false;
    } //-- void deleteCdTipoCargaRecadastro() 

    /**
     * Method deleteCdTipoCataoSalario
     * 
     */
    public void deleteCdTipoCataoSalario()
    {
        this._has_cdTipoCataoSalario= false;
    } //-- void deleteCdTipoCataoSalario() 

    /**
     * Method deleteCdTipoConsistenciaLista
     * 
     */
    public void deleteCdTipoConsistenciaLista()
    {
        this._has_cdTipoConsistenciaLista= false;
    } //-- void deleteCdTipoConsistenciaLista() 

    /**
     * Method deleteCdTipoConsultaComprovante
     * 
     */
    public void deleteCdTipoConsultaComprovante()
    {
        this._has_cdTipoConsultaComprovante= false;
    } //-- void deleteCdTipoConsultaComprovante() 

    /**
     * Method deleteCdTipoContaFavorecido
     * 
     */
    public void deleteCdTipoContaFavorecido()
    {
        this._has_cdTipoContaFavorecido= false;
    } //-- void deleteCdTipoContaFavorecido() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoDataFloat
     * 
     */
    public void deleteCdTipoDataFloat()
    {
        this._has_cdTipoDataFloat= false;
    } //-- void deleteCdTipoDataFloat() 

    /**
     * Method deleteCdTipoDivergenciaVeiculo
     * 
     */
    public void deleteCdTipoDivergenciaVeiculo()
    {
        this._has_cdTipoDivergenciaVeiculo= false;
    } //-- void deleteCdTipoDivergenciaVeiculo() 

    /**
     * Method deleteCdTipoEnderecoPessoa
     * 
     */
    public void deleteCdTipoEnderecoPessoa()
    {
        this._has_cdTipoEnderecoPessoa= false;
    } //-- void deleteCdTipoEnderecoPessoa() 

    /**
     * Method deleteCdTipoFormacaoLista
     * 
     */
    public void deleteCdTipoFormacaoLista()
    {
        this._has_cdTipoFormacaoLista= false;
    } //-- void deleteCdTipoFormacaoLista() 

    /**
     * Method deleteCdTipoIdBeneficio
     * 
     */
    public void deleteCdTipoIdBeneficio()
    {
        this._has_cdTipoIdBeneficio= false;
    } //-- void deleteCdTipoIdBeneficio() 

    /**
     * Method deleteCdTipoIsncricaoFavorecido
     * 
     */
    public void deleteCdTipoIsncricaoFavorecido()
    {
        this._has_cdTipoIsncricaoFavorecido= false;
    } //-- void deleteCdTipoIsncricaoFavorecido() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdTituloDdaRetorno
     * 
     */
    public void deleteCdTituloDdaRetorno()
    {
        this._has_cdTituloDdaRetorno= false;
    } //-- void deleteCdTituloDdaRetorno() 

    /**
     * Method deleteCdUtilizacaoFavorecidoControle
     * 
     */
    public void deleteCdUtilizacaoFavorecidoControle()
    {
        this._has_cdUtilizacaoFavorecidoControle= false;
    } //-- void deleteCdUtilizacaoFavorecidoControle() 

    /**
     * Method deleteCdValidacaoNomeFavorecido
     * 
     */
    public void deleteCdValidacaoNomeFavorecido()
    {
        this._has_cdValidacaoNomeFavorecido= false;
    } //-- void deleteCdValidacaoNomeFavorecido() 

    /**
     * Method deleteCindcdFantsRepas
     * 
     */
    public void deleteCindcdFantsRepas()
    {
        this._has_cindcdFantsRepas= false;
    } //-- void deleteCindcdFantsRepas() 

    /**
     * Method deleteNrSequenciaCotratoNegocio
     * 
     */
    public void deleteNrSequenciaCotratoNegocio()
    {
        this._has_nrSequenciaCotratoNegocio= false;
    } //-- void deleteNrSequenciaCotratoNegocio() 

    /**
     * Method deleteQtAntecedencia
     * 
     */
    public void deleteQtAntecedencia()
    {
        this._has_qtAntecedencia= false;
    } //-- void deleteQtAntecedencia() 

    /**
     * Method deleteQtAnteriorVencimentoComprovado
     * 
     */
    public void deleteQtAnteriorVencimentoComprovado()
    {
        this._has_qtAnteriorVencimentoComprovado= false;
    } //-- void deleteQtAnteriorVencimentoComprovado() 

    /**
     * Method deleteQtDiaExpiracao
     * 
     */
    public void deleteQtDiaExpiracao()
    {
        this._has_qtDiaExpiracao= false;
    } //-- void deleteQtDiaExpiracao() 

    /**
     * Method deleteQtDiaInatividadeFavorecido
     * 
     */
    public void deleteQtDiaInatividadeFavorecido()
    {
        this._has_qtDiaInatividadeFavorecido= false;
    } //-- void deleteQtDiaInatividadeFavorecido() 

    /**
     * Method deleteQtDiaRepiqConsulta
     * 
     */
    public void deleteQtDiaRepiqConsulta()
    {
        this._has_qtDiaRepiqConsulta= false;
    } //-- void deleteQtDiaRepiqConsulta() 

    /**
     * Method deleteQtDiaUtilPgto
     * 
     */
    public void deleteQtDiaUtilPgto()
    {
        this._has_qtDiaUtilPgto= false;
    } //-- void deleteQtDiaUtilPgto() 

    /**
     * Method deleteQtEtapasRecadastroBeneficio
     * 
     */
    public void deleteQtEtapasRecadastroBeneficio()
    {
        this._has_qtEtapasRecadastroBeneficio= false;
    } //-- void deleteQtEtapasRecadastroBeneficio() 

    /**
     * Method deleteQtFaseRecadastroBeneficio
     * 
     */
    public void deleteQtFaseRecadastroBeneficio()
    {
        this._has_qtFaseRecadastroBeneficio= false;
    } //-- void deleteQtFaseRecadastroBeneficio() 

    /**
     * Method deleteQtLimiteLinha
     * 
     */
    public void deleteQtLimiteLinha()
    {
        this._has_qtLimiteLinha= false;
    } //-- void deleteQtLimiteLinha() 

    /**
     * Method deleteQtLimiteSolicitacaoCatao
     * 
     */
    public void deleteQtLimiteSolicitacaoCatao()
    {
        this._has_qtLimiteSolicitacaoCatao= false;
    } //-- void deleteQtLimiteSolicitacaoCatao() 

    /**
     * Method deleteQtMaximaInconLote
     * 
     */
    public void deleteQtMaximaInconLote()
    {
        this._has_qtMaximaInconLote= false;
    } //-- void deleteQtMaximaInconLote() 

    /**
     * Method deleteQtMaximaTituloVencido
     * 
     */
    public void deleteQtMaximaTituloVencido()
    {
        this._has_qtMaximaTituloVencido= false;
    } //-- void deleteQtMaximaTituloVencido() 

    /**
     * Method deleteQtMesComprovante
     * 
     */
    public void deleteQtMesComprovante()
    {
        this._has_qtMesComprovante= false;
    } //-- void deleteQtMesComprovante() 

    /**
     * Method deleteQtMesEtapaRecadastro
     * 
     */
    public void deleteQtMesEtapaRecadastro()
    {
        this._has_qtMesEtapaRecadastro= false;
    } //-- void deleteQtMesEtapaRecadastro() 

    /**
     * Method deleteQtMesFaseRecadastro
     * 
     */
    public void deleteQtMesFaseRecadastro()
    {
        this._has_qtMesFaseRecadastro= false;
    } //-- void deleteQtMesFaseRecadastro() 

    /**
     * Method deleteQtViaAviso
     * 
     */
    public void deleteQtViaAviso()
    {
        this._has_qtViaAviso= false;
    } //-- void deleteQtViaAviso() 

    /**
     * Method deleteQtViaCobranca
     * 
     */
    public void deleteQtViaCobranca()
    {
        this._has_qtViaCobranca= false;
    } //-- void deleteQtViaCobranca() 

    /**
     * Method deleteQtViaComprovante
     * 
     */
    public void deleteQtViaComprovante()
    {
        this._has_qtViaComprovante= false;
    } //-- void deleteQtViaComprovante() 

    /**
     * Method deleteVlFavorecidoNaoCadastro
     * 
     */
    public void deleteVlFavorecidoNaoCadastro()
    {
        this._has_vlFavorecidoNaoCadastro= false;
    } //-- void deleteVlFavorecidoNaoCadastro() 

    /**
     * Method deleteVlLimiteDiaPagamento
     * 
     */
    public void deleteVlLimiteDiaPagamento()
    {
        this._has_vlLimiteDiaPagamento= false;
    } //-- void deleteVlLimiteDiaPagamento() 

    /**
     * Method deleteVlLimiteIndividualPagamento
     * 
     */
    public void deleteVlLimiteIndividualPagamento()
    {
        this._has_vlLimiteIndividualPagamento= false;
    } //-- void deleteVlLimiteIndividualPagamento() 

    /**
     * Method deleteVlPercentualDiferencaTolerada
     * 
     */
    public void deleteVlPercentualDiferencaTolerada()
    {
        this._has_vlPercentualDiferencaTolerada= false;
    } //-- void deleteVlPercentualDiferencaTolerada() 

    /**
     * Returns the value of field 'cdAcaoNaoVida'.
     * 
     * @return int
     * @return the value of field 'cdAcaoNaoVida'.
     */
    public int getCdAcaoNaoVida()
    {
        return this._cdAcaoNaoVida;
    } //-- int getCdAcaoNaoVida() 

    /**
     * Returns the value of field 'cdAcertoDadoRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdAcertoDadoRecadastro'.
     */
    public int getCdAcertoDadoRecadastro()
    {
        return this._cdAcertoDadoRecadastro;
    } //-- int getCdAcertoDadoRecadastro() 

    /**
     * Returns the value of field 'cdAgendaDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdAgendaDebitoVeiculo'.
     */
    public int getCdAgendaDebitoVeiculo()
    {
        return this._cdAgendaDebitoVeiculo;
    } //-- int getCdAgendaDebitoVeiculo() 

    /**
     * Returns the value of field 'cdAgendaPagamentoVencido'.
     * 
     * @return int
     * @return the value of field 'cdAgendaPagamentoVencido'.
     */
    public int getCdAgendaPagamentoVencido()
    {
        return this._cdAgendaPagamentoVencido;
    } //-- int getCdAgendaPagamentoVencido() 

    /**
     * Returns the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @return int
     * @return the value of field 'cdAgendaRastreabilidadeFilial'.
     */
    public int getCdAgendaRastreabilidadeFilial()
    {
        return this._cdAgendaRastreabilidadeFilial;
    } //-- int getCdAgendaRastreabilidadeFilial() 

    /**
     * Returns the value of field 'cdAgendaValorMenor'.
     * 
     * @return int
     * @return the value of field 'cdAgendaValorMenor'.
     */
    public int getCdAgendaValorMenor()
    {
        return this._cdAgendaValorMenor;
    } //-- int getCdAgendaValorMenor() 

    /**
     * Returns the value of field 'cdAgrupamentoAviso'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoAviso'.
     */
    public int getCdAgrupamentoAviso()
    {
        return this._cdAgrupamentoAviso;
    } //-- int getCdAgrupamentoAviso() 

    /**
     * Returns the value of field 'cdAgrupamentoComprovado'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoComprovado'.
     */
    public int getCdAgrupamentoComprovado()
    {
        return this._cdAgrupamentoComprovado;
    } //-- int getCdAgrupamentoComprovado() 

    /**
     * Returns the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     */
    public int getCdAgrupamentoFormularioRecadastro()
    {
        return this._cdAgrupamentoFormularioRecadastro;
    } //-- int getCdAgrupamentoFormularioRecadastro() 

    /**
     * Returns the value of field 'cdAntecRecadastroBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdAntecRecadastroBeneficio'.
     */
    public int getCdAntecRecadastroBeneficio()
    {
        return this._cdAntecRecadastroBeneficio;
    } //-- int getCdAntecRecadastroBeneficio() 

    /**
     * Returns the value of field 'cdAreaReservada'.
     * 
     * @return int
     * @return the value of field 'cdAreaReservada'.
     */
    public int getCdAreaReservada()
    {
        return this._cdAreaReservada;
    } //-- int getCdAreaReservada() 

    /**
     * Returns the value of field 'cdBaseRecadastroBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdBaseRecadastroBeneficio'.
     */
    public int getCdBaseRecadastroBeneficio()
    {
        return this._cdBaseRecadastroBeneficio;
    } //-- int getCdBaseRecadastroBeneficio() 

    /**
     * Returns the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @return int
     * @return the value of field 'cdBloqueioEmissaoPplta'.
     */
    public int getCdBloqueioEmissaoPplta()
    {
        return this._cdBloqueioEmissaoPplta;
    } //-- int getCdBloqueioEmissaoPplta() 

    /**
     * Returns the value of field 'cdCanal'.
     * 
     * @return int
     * @return the value of field 'cdCanal'.
     */
    public int getCdCanal()
    {
        return this._cdCanal;
    } //-- int getCdCanal() 

    /**
     * Returns the value of field 'cdCapituloTituloRegistro'.
     * 
     * @return int
     * @return the value of field 'cdCapituloTituloRegistro'.
     */
    public int getCdCapituloTituloRegistro()
    {
        return this._cdCapituloTituloRegistro;
    } //-- int getCdCapituloTituloRegistro() 

    /**
     * Returns the value of field 'cdCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdCobrancaTarifa'.
     */
    public int getCdCobrancaTarifa()
    {
        return this._cdCobrancaTarifa;
    } //-- int getCdCobrancaTarifa() 

    /**
     * Returns the value of field 'cdConsDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdConsDebitoVeiculo'.
     */
    public int getCdConsDebitoVeiculo()
    {
        return this._cdConsDebitoVeiculo;
    } //-- int getCdConsDebitoVeiculo() 

    /**
     * Returns the value of field 'cdConsEndereco'.
     * 
     * @return int
     * @return the value of field 'cdConsEndereco'.
     */
    public int getCdConsEndereco()
    {
        return this._cdConsEndereco;
    } //-- int getCdConsEndereco() 

    /**
     * Returns the value of field 'cdConsSaldoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdConsSaldoPagamento'.
     */
    public int getCdConsSaldoPagamento()
    {
        return this._cdConsSaldoPagamento;
    } //-- int getCdConsSaldoPagamento() 

    /**
     * Returns the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @return int
     * @return the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public int getCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        return this._cdConsistenciaCpfCnpjBenefAvalNpc;
    } //-- int getCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Returns the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @return int
     * @return the value of field 'cdConsultaSaldoValorSuperior'.
     */
    public int getCdConsultaSaldoValorSuperior()
    {
        return this._cdConsultaSaldoValorSuperior;
    } //-- int getCdConsultaSaldoValorSuperior() 

    /**
     * Returns the value of field 'cdContagemConsSaudo'.
     * 
     * @return int
     * @return the value of field 'cdContagemConsSaudo'.
     */
    public int getCdContagemConsSaudo()
    {
        return this._cdContagemConsSaudo;
    } //-- int getCdContagemConsSaudo() 

    /**
     * Returns the value of field 'cdContratoContaTransferencia'.
     * 
     * @return int
     * @return the value of field 'cdContratoContaTransferencia'.
     */
    public int getCdContratoContaTransferencia()
    {
        return this._cdContratoContaTransferencia;
    } //-- int getCdContratoContaTransferencia() 

    /**
     * Returns the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @return int
     * @return the value of field 'cdCreditoNaoUtilizado'.
     */
    public int getCdCreditoNaoUtilizado()
    {
        return this._cdCreditoNaoUtilizado;
    } //-- int getCdCreditoNaoUtilizado() 

    /**
     * Returns the value of field 'cdCriterioEnquaBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCriterioEnquaBeneficio'.
     */
    public int getCdCriterioEnquaBeneficio()
    {
        return this._cdCriterioEnquaBeneficio;
    } //-- int getCdCriterioEnquaBeneficio() 

    /**
     * Returns the value of field 'cdCriterioEnquaRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdCriterioEnquaRecadastro'.
     */
    public int getCdCriterioEnquaRecadastro()
    {
        return this._cdCriterioEnquaRecadastro;
    } //-- int getCdCriterioEnquaRecadastro() 

    /**
     * Returns the value of field
     * 'cdCriterioRastreabilidadeTitulo'.
     * 
     * @return int
     * @return the value of field 'cdCriterioRastreabilidadeTitulo'.
     */
    public int getCdCriterioRastreabilidadeTitulo()
    {
        return this._cdCriterioRastreabilidadeTitulo;
    } //-- int getCdCriterioRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'cdCtciaEspecieBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCtciaEspecieBeneficio'.
     */
    public int getCdCtciaEspecieBeneficio()
    {
        return this._cdCtciaEspecieBeneficio;
    } //-- int getCdCtciaEspecieBeneficio() 

    /**
     * Returns the value of field 'cdCtciaIdentificacaoBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCtciaIdentificacaoBeneficio'.
     */
    public int getCdCtciaIdentificacaoBeneficio()
    {
        return this._cdCtciaIdentificacaoBeneficio;
    } //-- int getCdCtciaIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'cdCtciaInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdCtciaInscricaoFavorecido'.
     */
    public int getCdCtciaInscricaoFavorecido()
    {
        return this._cdCtciaInscricaoFavorecido;
    } //-- int getCdCtciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdCtciaProprietarioVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdCtciaProprietarioVeiculo'.
     */
    public int getCdCtciaProprietarioVeiculo()
    {
        return this._cdCtciaProprietarioVeiculo;
    } //-- int getCdCtciaProprietarioVeiculo() 

    /**
     * Returns the value of field 'cdDIspzSalarioCrrtt'.
     * 
     * @return int
     * @return the value of field 'cdDIspzSalarioCrrtt'.
     */
    public int getCdDIspzSalarioCrrtt()
    {
        return this._cdDIspzSalarioCrrtt;
    } //-- int getCdDIspzSalarioCrrtt() 

    /**
     * Returns the value of field 'cdDependenteOperante'.
     * 
     * @return int
     * @return the value of field 'cdDependenteOperante'.
     */
    public int getCdDependenteOperante()
    {
        return this._cdDependenteOperante;
    } //-- int getCdDependenteOperante() 

    /**
     * Returns the value of field 'cdDestinoAviso'.
     * 
     * @return int
     * @return the value of field 'cdDestinoAviso'.
     */
    public int getCdDestinoAviso()
    {
        return this._cdDestinoAviso;
    } //-- int getCdDestinoAviso() 

    /**
     * Returns the value of field 'cdDestinoComprovante'.
     * 
     * @return int
     * @return the value of field 'cdDestinoComprovante'.
     */
    public int getCdDestinoComprovante()
    {
        return this._cdDestinoComprovante;
    } //-- int getCdDestinoComprovante() 

    /**
     * Returns the value of field 'cdDestinoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdDestinoFormularioRecadastro'.
     */
    public int getCdDestinoFormularioRecadastro()
    {
        return this._cdDestinoFormularioRecadastro;
    } //-- int getCdDestinoFormularioRecadastro() 

    /**
     * Returns the value of field 'cdDiaFloatPagamento'.
     * 
     * @return int
     * @return the value of field 'cdDiaFloatPagamento'.
     */
    public int getCdDiaFloatPagamento()
    {
        return this._cdDiaFloatPagamento;
    } //-- int getCdDiaFloatPagamento() 

    /**
     * Returns the value of field 'cdDispzContaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDispzContaCredito'.
     */
    public int getCdDispzContaCredito()
    {
        return this._cdDispzContaCredito;
    } //-- int getCdDispzContaCredito() 

    /**
     * Returns the value of field 'cdDispzDiversarCrrtt'.
     * 
     * @return int
     * @return the value of field 'cdDispzDiversarCrrtt'.
     */
    public int getCdDispzDiversarCrrtt()
    {
        return this._cdDispzDiversarCrrtt;
    } //-- int getCdDispzDiversarCrrtt() 

    /**
     * Returns the value of field 'cdDispzDiversasNao'.
     * 
     * @return int
     * @return the value of field 'cdDispzDiversasNao'.
     */
    public int getCdDispzDiversasNao()
    {
        return this._cdDispzDiversasNao;
    } //-- int getCdDispzDiversasNao() 

    /**
     * Returns the value of field 'cdDispzSalarioNao'.
     * 
     * @return int
     * @return the value of field 'cdDispzSalarioNao'.
     */
    public int getCdDispzSalarioNao()
    {
        return this._cdDispzSalarioNao;
    } //-- int getCdDispzSalarioNao() 

    /**
     * Returns the value of field 'cdEmpresaOperante'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaOperante'.
     */
    public long getCdEmpresaOperante()
    {
        return this._cdEmpresaOperante;
    } //-- long getCdEmpresaOperante() 

    /**
     * Returns the value of field 'cdEnvelopeAberto'.
     * 
     * @return int
     * @return the value of field 'cdEnvelopeAberto'.
     */
    public int getCdEnvelopeAberto()
    {
        return this._cdEnvelopeAberto;
    } //-- int getCdEnvelopeAberto() 

    /**
     * Returns the value of field 'cdEspecieEnderecoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdEspecieEnderecoPessoa'.
     */
    public int getCdEspecieEnderecoPessoa()
    {
        return this._cdEspecieEnderecoPessoa;
    } //-- int getCdEspecieEnderecoPessoa() 

    /**
     * Returns the value of field 'cdExigeAutFilial'.
     * 
     * @return int
     * @return the value of field 'cdExigeAutFilial'.
     */
    public int getCdExigeAutFilial()
    {
        return this._cdExigeAutFilial;
    } //-- int getCdExigeAutFilial() 

    /**
     * Returns the value of field 'cdFavorecidoConsPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFavorecidoConsPagamento'.
     */
    public int getCdFavorecidoConsPagamento()
    {
        return this._cdFavorecidoConsPagamento;
    } //-- int getCdFavorecidoConsPagamento() 

    /**
     * Returns the value of field 'cdFloatServicoContrato'.
     * 
     * @return int
     * @return the value of field 'cdFloatServicoContrato'.
     */
    public int getCdFloatServicoContrato()
    {
        return this._cdFloatServicoContrato;
    } //-- int getCdFloatServicoContrato() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFormaEnvioPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaEnvioPagamento'.
     */
    public int getCdFormaEnvioPagamento()
    {
        return this._cdFormaEnvioPagamento;
    } //-- int getCdFormaEnvioPagamento() 

    /**
     * Returns the value of field 'cdFormaEstornoCredito'.
     * 
     * @return int
     * @return the value of field 'cdFormaEstornoCredito'.
     */
    public int getCdFormaEstornoCredito()
    {
        return this._cdFormaEstornoCredito;
    } //-- int getCdFormaEstornoCredito() 

    /**
     * Returns the value of field 'cdFormaExpiracaoCredito'.
     * 
     * @return int
     * @return the value of field 'cdFormaExpiracaoCredito'.
     */
    public int getCdFormaExpiracaoCredito()
    {
        return this._cdFormaExpiracaoCredito;
    } //-- int getCdFormaExpiracaoCredito() 

    /**
     * Returns the value of field 'cdFormaManutencao'.
     * 
     * @return int
     * @return the value of field 'cdFormaManutencao'.
     */
    public int getCdFormaManutencao()
    {
        return this._cdFormaManutencao;
    } //-- int getCdFormaManutencao() 

    /**
     * Returns the value of field 'cdFormularioContratoCliente'.
     * 
     * @return int
     * @return the value of field 'cdFormularioContratoCliente'.
     */
    public int getCdFormularioContratoCliente()
    {
        return this._cdFormularioContratoCliente;
    } //-- int getCdFormularioContratoCliente() 

    /**
     * Returns the value of field 'cdFrasePreCadastro'.
     * 
     * @return int
     * @return the value of field 'cdFrasePreCadastro'.
     */
    public int getCdFrasePreCadastro()
    {
        return this._cdFrasePreCadastro;
    } //-- int getCdFrasePreCadastro() 

    /**
     * Returns the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @return int
     * @return the value of field 'cdIndLancamentoPersonalizado'.
     */
    public int getCdIndLancamentoPersonalizado()
    {
        return this._cdIndLancamentoPersonalizado;
    } //-- int getCdIndLancamentoPersonalizado() 

    /**
     * Returns the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAdesaoSacador'.
     */
    public int getCdIndicadorAdesaoSacador()
    {
        return this._cdIndicadorAdesaoSacador;
    } //-- int getCdIndicadorAdesaoSacador() 

    /**
     * Returns the value of field 'cdIndicadorAgendaGrade'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaGrade'.
     */
    public int getCdIndicadorAgendaGrade()
    {
        return this._cdIndicadorAgendaGrade;
    } //-- int getCdIndicadorAgendaGrade() 

    /**
     * Returns the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaTitulo'.
     */
    public int getCdIndicadorAgendaTitulo()
    {
        return this._cdIndicadorAgendaTitulo;
    } //-- int getCdIndicadorAgendaTitulo() 

    /**
     * Returns the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAutorizacaoCliente'.
     */
    public int getCdIndicadorAutorizacaoCliente()
    {
        return this._cdIndicadorAutorizacaoCliente;
    } //-- int getCdIndicadorAutorizacaoCliente() 

    /**
     * Returns the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @return int
     * @return the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public int getCdIndicadorAutorizacaoComplemento()
    {
        return this._cdIndicadorAutorizacaoComplemento;
    } //-- int getCdIndicadorAutorizacaoComplemento() 

    /**
     * Returns the value of field 'cdIndicadorBancoPostal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorBancoPostal'.
     */
    public int getCdIndicadorBancoPostal()
    {
        return this._cdIndicadorBancoPostal;
    } //-- int getCdIndicadorBancoPostal() 

    /**
     * Returns the value of field 'cdIndicadorCadastroOrg'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadastroOrg'.
     */
    public int getCdIndicadorCadastroOrg()
    {
        return this._cdIndicadorCadastroOrg;
    } //-- int getCdIndicadorCadastroOrg() 

    /**
     * Returns the value of field 'cdIndicadorCadastroProcd'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadastroProcd'.
     */
    public int getCdIndicadorCadastroProcd()
    {
        return this._cdIndicadorCadastroProcd;
    } //-- int getCdIndicadorCadastroProcd() 

    /**
     * Returns the value of field 'cdIndicadorCataoSalario'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCataoSalario'.
     */
    public int getCdIndicadorCataoSalario()
    {
        return this._cdIndicadorCataoSalario;
    } //-- int getCdIndicadorCataoSalario() 

    /**
     * Returns the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEmissaoAviso'.
     */
    public int getCdIndicadorEmissaoAviso()
    {
        return this._cdIndicadorEmissaoAviso;
    } //-- int getCdIndicadorEmissaoAviso() 

    /**
     * Returns the value of field 'cdIndicadorExpiracaoCredito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorExpiracaoCredito'.
     */
    public int getCdIndicadorExpiracaoCredito()
    {
        return this._cdIndicadorExpiracaoCredito;
    } //-- int getCdIndicadorExpiracaoCredito() 

    /**
     * Returns the value of field 'cdIndicadorFeriadoLocal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFeriadoLocal'.
     */
    public int getCdIndicadorFeriadoLocal()
    {
        return this._cdIndicadorFeriadoLocal;
    } //-- int getCdIndicadorFeriadoLocal() 

    /**
     * Returns the value of field 'cdIndicadorLancamentoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorLancamentoPagamento'.
     */
    public int getCdIndicadorLancamentoPagamento()
    {
        return this._cdIndicadorLancamentoPagamento;
    } //-- int getCdIndicadorLancamentoPagamento() 

    /**
     * Returns the value of field 'cdIndicadorListaDebito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorListaDebito'.
     */
    public int getCdIndicadorListaDebito()
    {
        return this._cdIndicadorListaDebito;
    } //-- int getCdIndicadorListaDebito() 

    /**
     * Returns the value of field
     * 'cdIndicadorManutencaoProcurador'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorManutencaoProcurador'.
     */
    public int getCdIndicadorManutencaoProcurador()
    {
        return this._cdIndicadorManutencaoProcurador;
    } //-- int getCdIndicadorManutencaoProcurador() 

    /**
     * Returns the value of field 'cdIndicadorMensagemPerso'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorMensagemPerso'.
     */
    public int getCdIndicadorMensagemPerso()
    {
        return this._cdIndicadorMensagemPerso;
    } //-- int getCdIndicadorMensagemPerso() 

    /**
     * Returns the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoInternet'.
     */
    public int getCdIndicadorRetornoInternet()
    {
        return this._cdIndicadorRetornoInternet;
    } //-- int getCdIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'cdIndicadorSegundaLinha'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorSegundaLinha'.
     */
    public int getCdIndicadorSegundaLinha()
    {
        return this._cdIndicadorSegundaLinha;
    } //-- int getCdIndicadorSegundaLinha() 

    /**
     * Returns the value of field 'cdIndicadorSeparaCanal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorSeparaCanal'.
     */
    public int getCdIndicadorSeparaCanal()
    {
        return this._cdIndicadorSeparaCanal;
    } //-- int getCdIndicadorSeparaCanal() 

    /**
     * Returns the value of field 'cdIndicadorTipoRetornoInternet'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoRetornoInternet'.
     */
    public int getCdIndicadorTipoRetornoInternet()
    {
        return this._cdIndicadorTipoRetornoInternet;
    } //-- int getCdIndicadorTipoRetornoInternet() 

    /**
     * Returns the value of field 'cdIndicadorUtilizaMora'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorUtilizaMora'.
     */
    public int getCdIndicadorUtilizaMora()
    {
        return this._cdIndicadorUtilizaMora;
    } //-- int getCdIndicadorUtilizaMora() 

    /**
     * Returns the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoCredito'.
     */
    public int getCdLancamentoFuturoCredito()
    {
        return this._cdLancamentoFuturoCredito;
    } //-- int getCdLancamentoFuturoCredito() 

    /**
     * Returns the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoDebito'.
     */
    public int getCdLancamentoFuturoDebito()
    {
        return this._cdLancamentoFuturoDebito;
    } //-- int getCdLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'cdLiberacaoLoteProcesso'.
     * 
     * @return int
     * @return the value of field 'cdLiberacaoLoteProcesso'.
     */
    public int getCdLiberacaoLoteProcesso()
    {
        return this._cdLiberacaoLoteProcesso;
    } //-- int getCdLiberacaoLoteProcesso() 

    /**
     * Returns the value of field 'cdLocalEmissao'.
     * 
     * @return int
     * @return the value of field 'cdLocalEmissao'.
     */
    public int getCdLocalEmissao()
    {
        return this._cdLocalEmissao;
    } //-- int getCdLocalEmissao() 

    /**
     * Returns the value of field 'cdManutencaoBaseRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdManutencaoBaseRecadastro'.
     */
    public int getCdManutencaoBaseRecadastro()
    {
        return this._cdManutencaoBaseRecadastro;
    } //-- int getCdManutencaoBaseRecadastro() 

    /**
     * Returns the value of field 'cdMeioPagamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdMeioPagamentoCredito'.
     */
    public int getCdMeioPagamentoCredito()
    {
        return this._cdMeioPagamentoCredito;
    } //-- int getCdMeioPagamentoCredito() 

    /**
     * Returns the value of field 'cdMeioPagamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdMeioPagamentoDebito'.
     */
    public int getCdMeioPagamentoDebito()
    {
        return this._cdMeioPagamentoDebito;
    } //-- int getCdMeioPagamentoDebito() 

    /**
     * Returns the value of field 'cdMensagemRecadastroMidia'.
     * 
     * @return int
     * @return the value of field 'cdMensagemRecadastroMidia'.
     */
    public int getCdMensagemRecadastroMidia()
    {
        return this._cdMensagemRecadastroMidia;
    } //-- int getCdMensagemRecadastroMidia() 

    /**
     * Returns the value of field 'cdMidiaDisponivel'.
     * 
     * @return int
     * @return the value of field 'cdMidiaDisponivel'.
     */
    public int getCdMidiaDisponivel()
    {
        return this._cdMidiaDisponivel;
    } //-- int getCdMidiaDisponivel() 

    /**
     * Returns the value of field 'cdMidiaMensagemRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdMidiaMensagemRecadastro'.
     */
    public int getCdMidiaMensagemRecadastro()
    {
        return this._cdMidiaMensagemRecadastro;
    } //-- int getCdMidiaMensagemRecadastro() 

    /**
     * Returns the value of field 'cdMomentoAvisoRacadastro'.
     * 
     * @return int
     * @return the value of field 'cdMomentoAvisoRacadastro'.
     */
    public int getCdMomentoAvisoRacadastro()
    {
        return this._cdMomentoAvisoRacadastro;
    } //-- int getCdMomentoAvisoRacadastro() 

    /**
     * Returns the value of field 'cdMomentoCreditoEfetivacao'.
     * 
     * @return int
     * @return the value of field 'cdMomentoCreditoEfetivacao'.
     */
    public int getCdMomentoCreditoEfetivacao()
    {
        return this._cdMomentoCreditoEfetivacao;
    } //-- int getCdMomentoCreditoEfetivacao() 

    /**
     * Returns the value of field 'cdMomentoDebitoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoDebitoPagamento'.
     */
    public int getCdMomentoDebitoPagamento()
    {
        return this._cdMomentoDebitoPagamento;
    } //-- int getCdMomentoDebitoPagamento() 

    /**
     * Returns the value of field 'cdMomentoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdMomentoFormularioRecadastro'.
     */
    public int getCdMomentoFormularioRecadastro()
    {
        return this._cdMomentoFormularioRecadastro;
    } //-- int getCdMomentoFormularioRecadastro() 

    /**
     * Returns the value of field
     * 'cdMomentoProcessamentoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoProcessamentoPagamento'.
     */
    public int getCdMomentoProcessamentoPagamento()
    {
        return this._cdMomentoProcessamentoPagamento;
    } //-- int getCdMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdPagamentoNaoUtilizado'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoNaoUtilizado'.
     */
    public int getCdPagamentoNaoUtilizado()
    {
        return this._cdPagamentoNaoUtilizado;
    } //-- int getCdPagamentoNaoUtilizado() 

    /**
     * Returns the value of field 'cdParametro'.
     * 
     * @return int
     * @return the value of field 'cdParametro'.
     */
    public int getCdParametro()
    {
        return this._cdParametro;
    } //-- int getCdParametro() 

    /**
     * Returns the value of field 'cdPercentualMaximoInconLote'.
     * 
     * @return int
     * @return the value of field 'cdPercentualMaximoInconLote'.
     */
    public int getCdPercentualMaximoInconLote()
    {
        return this._cdPercentualMaximoInconLote;
    } //-- int getCdPercentualMaximoInconLote() 

    /**
     * Returns the value of field 'cdPerdcComprovante'.
     * 
     * @return int
     * @return the value of field 'cdPerdcComprovante'.
     */
    public int getCdPerdcComprovante()
    {
        return this._cdPerdcComprovante;
    } //-- int getCdPerdcComprovante() 

    /**
     * Returns the value of field 'cdPerdcConsultaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdPerdcConsultaVeiculo'.
     */
    public int getCdPerdcConsultaVeiculo()
    {
        return this._cdPerdcConsultaVeiculo;
    } //-- int getCdPerdcConsultaVeiculo() 

    /**
     * Returns the value of field 'cdPerdcEnvioRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPerdcEnvioRemessa'.
     */
    public int getCdPerdcEnvioRemessa()
    {
        return this._cdPerdcEnvioRemessa;
    } //-- int getCdPerdcEnvioRemessa() 

    /**
     * Returns the value of field 'cdPerdcManutencaoProcd'.
     * 
     * @return int
     * @return the value of field 'cdPerdcManutencaoProcd'.
     */
    public int getCdPerdcManutencaoProcd()
    {
        return this._cdPerdcManutencaoProcd;
    } //-- int getCdPerdcManutencaoProcd() 

    /**
     * Returns the value of field 'cdPeriodicidadeAviso'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeAviso'.
     */
    public int getCdPeriodicidadeAviso()
    {
        return this._cdPeriodicidadeAviso;
    } //-- int getCdPeriodicidadeAviso() 

    /**
     * Returns the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoDebitoOnline'.
     */
    public int getCdPermissaoDebitoOnline()
    {
        return this._cdPermissaoDebitoOnline;
    } //-- int getCdPermissaoDebitoOnline() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     * 
     * @return int
     * @return the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     */
    public int getCdPreenchimentoLancamentoPersonalizado()
    {
        return this._cdPreenchimentoLancamentoPersonalizado;
    } //-- int getCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Returns the value of field 'cdPrincipalEnquaRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdPrincipalEnquaRecadastro'.
     */
    public int getCdPrincipalEnquaRecadastro()
    {
        return this._cdPrincipalEnquaRecadastro;
    } //-- int getCdPrincipalEnquaRecadastro() 

    /**
     * Returns the value of field
     * 'cdPrioridadeEfetivacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeEfetivacaoPagamento'.
     */
    public int getCdPrioridadeEfetivacaoPagamento()
    {
        return this._cdPrioridadeEfetivacaoPagamento;
    } //-- int getCdPrioridadeEfetivacaoPagamento() 

    /**
     * Returns the value of field 'cdProdutoOperacaroRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaroRelacionado'.
     */
    public int getCdProdutoOperacaroRelacionado()
    {
        return this._cdProdutoOperacaroRelacionado;
    } //-- int getCdProdutoOperacaroRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRastreabilidadeNotaFiscal'.
     * 
     * @return int
     * @return the value of field 'cdRastreabilidadeNotaFiscal'.
     */
    public int getCdRastreabilidadeNotaFiscal()
    {
        return this._cdRastreabilidadeNotaFiscal;
    } //-- int getCdRastreabilidadeNotaFiscal() 

    /**
     * Returns the value of field
     * 'cdRastreabilidadeTituloTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdRastreabilidadeTituloTerceiro'.
     */
    public int getCdRastreabilidadeTituloTerceiro()
    {
        return this._cdRastreabilidadeTituloTerceiro;
    } //-- int getCdRastreabilidadeTituloTerceiro() 

    /**
     * Returns the value of field 'cdRejeicaoAgendaLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAgendaLote'.
     */
    public int getCdRejeicaoAgendaLote()
    {
        return this._cdRejeicaoAgendaLote;
    } //-- int getCdRejeicaoAgendaLote() 

    /**
     * Returns the value of field 'cdRejeicaoEfetivacaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoEfetivacaoLote'.
     */
    public int getCdRejeicaoEfetivacaoLote()
    {
        return this._cdRejeicaoEfetivacaoLote;
    } //-- int getCdRejeicaoEfetivacaoLote() 

    /**
     * Returns the value of field 'cdRejeicaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoLote'.
     */
    public int getCdRejeicaoLote()
    {
        return this._cdRejeicaoLote;
    } //-- int getCdRejeicaoLote() 

    /**
     * Returns the value of field 'cdSequenciaEnderecoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdSequenciaEnderecoPessoa'.
     */
    public int getCdSequenciaEnderecoPessoa()
    {
        return this._cdSequenciaEnderecoPessoa;
    } //-- int getCdSequenciaEnderecoPessoa() 

    /**
     * Returns the value of field 'cdTipoCargaRecadastro'.
     * 
     * @return int
     * @return the value of field 'cdTipoCargaRecadastro'.
     */
    public int getCdTipoCargaRecadastro()
    {
        return this._cdTipoCargaRecadastro;
    } //-- int getCdTipoCargaRecadastro() 

    /**
     * Returns the value of field 'cdTipoCataoSalario'.
     * 
     * @return int
     * @return the value of field 'cdTipoCataoSalario'.
     */
    public int getCdTipoCataoSalario()
    {
        return this._cdTipoCataoSalario;
    } //-- int getCdTipoCataoSalario() 

    /**
     * Returns the value of field 'cdTipoConsistenciaLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsistenciaLista'.
     */
    public int getCdTipoConsistenciaLista()
    {
        return this._cdTipoConsistenciaLista;
    } //-- int getCdTipoConsistenciaLista() 

    /**
     * Returns the value of field 'cdTipoConsultaComprovante'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsultaComprovante'.
     */
    public int getCdTipoConsultaComprovante()
    {
        return this._cdTipoConsultaComprovante;
    } //-- int getCdTipoConsultaComprovante() 

    /**
     * Returns the value of field 'cdTipoContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaFavorecido'.
     */
    public int getCdTipoContaFavorecido()
    {
        return this._cdTipoContaFavorecido;
    } //-- int getCdTipoContaFavorecido() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoDataFloat'.
     * 
     * @return int
     * @return the value of field 'cdTipoDataFloat'.
     */
    public int getCdTipoDataFloat()
    {
        return this._cdTipoDataFloat;
    } //-- int getCdTipoDataFloat() 

    /**
     * Returns the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoDivergenciaVeiculo'.
     */
    public int getCdTipoDivergenciaVeiculo()
    {
        return this._cdTipoDivergenciaVeiculo;
    } //-- int getCdTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'cdTipoEnderecoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoEnderecoPessoa'.
     */
    public int getCdTipoEnderecoPessoa()
    {
        return this._cdTipoEnderecoPessoa;
    } //-- int getCdTipoEnderecoPessoa() 

    /**
     * Returns the value of field 'cdTipoFormacaoLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoFormacaoLista'.
     */
    public int getCdTipoFormacaoLista()
    {
        return this._cdTipoFormacaoLista;
    } //-- int getCdTipoFormacaoLista() 

    /**
     * Returns the value of field 'cdTipoIdBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdTipoIdBeneficio'.
     */
    public int getCdTipoIdBeneficio()
    {
        return this._cdTipoIdBeneficio;
    } //-- int getCdTipoIdBeneficio() 

    /**
     * Returns the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoIsncricaoFavorecido'.
     */
    public int getCdTipoIsncricaoFavorecido()
    {
        return this._cdTipoIsncricaoFavorecido;
    } //-- int getCdTipoIsncricaoFavorecido() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdTituloDdaRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTituloDdaRetorno'.
     */
    public int getCdTituloDdaRetorno()
    {
        return this._cdTituloDdaRetorno;
    } //-- int getCdTituloDdaRetorno() 

    /**
     * Returns the value of field 'cdUsuarioExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExterno'.
     */
    public java.lang.String getCdUsuarioExterno()
    {
        return this._cdUsuarioExterno;
    } //-- java.lang.String getCdUsuarioExterno() 

    /**
     * Returns the value of field 'cdUtilizacaoFavorecidoControle'.
     * 
     * @return int
     * @return the value of field 'cdUtilizacaoFavorecidoControle'.
     */
    public int getCdUtilizacaoFavorecidoControle()
    {
        return this._cdUtilizacaoFavorecidoControle;
    } //-- int getCdUtilizacaoFavorecidoControle() 

    /**
     * Returns the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdValidacaoNomeFavorecido'.
     */
    public int getCdValidacaoNomeFavorecido()
    {
        return this._cdValidacaoNomeFavorecido;
    } //-- int getCdValidacaoNomeFavorecido() 

    /**
     * Returns the value of field 'cdusuario'.
     * 
     * @return String
     * @return the value of field 'cdusuario'.
     */
    public java.lang.String getCdusuario()
    {
        return this._cdusuario;
    } //-- java.lang.String getCdusuario() 

    /**
     * Returns the value of field 'cindcdFantsRepas'.
     * 
     * @return int
     * @return the value of field 'cindcdFantsRepas'.
     */
    public int getCindcdFantsRepas()
    {
        return this._cindcdFantsRepas;
    } //-- int getCindcdFantsRepas() 

    /**
     * Returns the value of field 'dsAreaReservada'.
     * 
     * @return String
     * @return the value of field 'dsAreaReservada'.
     */
    public java.lang.String getDsAreaReservada()
    {
        return this._dsAreaReservada;
    } //-- java.lang.String getDsAreaReservada() 

    /**
     * Returns the value of field 'dtEnquaContaSalario'.
     * 
     * @return String
     * @return the value of field 'dtEnquaContaSalario'.
     */
    public java.lang.String getDtEnquaContaSalario()
    {
        return this._dtEnquaContaSalario;
    } //-- java.lang.String getDtEnquaContaSalario() 

    /**
     * Returns the value of field 'dtFimAcertoRecadastro'.
     * 
     * @return String
     * @return the value of field 'dtFimAcertoRecadastro'.
     */
    public java.lang.String getDtFimAcertoRecadastro()
    {
        return this._dtFimAcertoRecadastro;
    } //-- java.lang.String getDtFimAcertoRecadastro() 

    /**
     * Returns the value of field 'dtFimRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field 'dtFimRecadastroBeneficio'.
     */
    public java.lang.String getDtFimRecadastroBeneficio()
    {
        return this._dtFimRecadastroBeneficio;
    } //-- java.lang.String getDtFimRecadastroBeneficio() 

    /**
     * Returns the value of field 'dtInicioAcertoRecadastro'.
     * 
     * @return String
     * @return the value of field 'dtInicioAcertoRecadastro'.
     */
    public java.lang.String getDtInicioAcertoRecadastro()
    {
        return this._dtInicioAcertoRecadastro;
    } //-- java.lang.String getDtInicioAcertoRecadastro() 

    /**
     * Returns the value of field 'dtInicioBloqueioPapeleta'.
     * 
     * @return String
     * @return the value of field 'dtInicioBloqueioPapeleta'.
     */
    public java.lang.String getDtInicioBloqueioPapeleta()
    {
        return this._dtInicioBloqueioPapeleta;
    } //-- java.lang.String getDtInicioBloqueioPapeleta() 

    /**
     * Returns the value of field 'dtInicioRastreabilidadeTitulo'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreabilidadeTitulo'.
     */
    public java.lang.String getDtInicioRastreabilidadeTitulo()
    {
        return this._dtInicioRastreabilidadeTitulo;
    } //-- java.lang.String getDtInicioRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'dtInicioRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field 'dtInicioRecadastroBeneficio'.
     */
    public java.lang.String getDtInicioRecadastroBeneficio()
    {
        return this._dtInicioRecadastroBeneficio;
    } //-- java.lang.String getDtInicioRecadastroBeneficio() 

    /**
     * Returns the value of field 'dtLimiteVinculoCarga'.
     * 
     * @return String
     * @return the value of field 'dtLimiteVinculoCarga'.
     */
    public java.lang.String getDtLimiteVinculoCarga()
    {
        return this._dtLimiteVinculoCarga;
    } //-- java.lang.String getDtLimiteVinculoCarga() 

    /**
     * Returns the value of field 'nmOperacaoFluxo'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxo'.
     */
    public java.lang.String getNmOperacaoFluxo()
    {
        return this._nmOperacaoFluxo;
    } //-- java.lang.String getNmOperacaoFluxo() 

    /**
     * Returns the value of field 'nrSequenciaCotratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaCotratoNegocio'.
     */
    public long getNrSequenciaCotratoNegocio()
    {
        return this._nrSequenciaCotratoNegocio;
    } //-- long getNrSequenciaCotratoNegocio() 

    /**
     * Returns the value of field 'qtAntecedencia'.
     * 
     * @return int
     * @return the value of field 'qtAntecedencia'.
     */
    public int getQtAntecedencia()
    {
        return this._qtAntecedencia;
    } //-- int getQtAntecedencia() 

    /**
     * Returns the value of field 'qtAnteriorVencimentoComprovado'.
     * 
     * @return int
     * @return the value of field 'qtAnteriorVencimentoComprovado'.
     */
    public int getQtAnteriorVencimentoComprovado()
    {
        return this._qtAnteriorVencimentoComprovado;
    } //-- int getQtAnteriorVencimentoComprovado() 

    /**
     * Returns the value of field 'qtDiaExpiracao'.
     * 
     * @return int
     * @return the value of field 'qtDiaExpiracao'.
     */
    public int getQtDiaExpiracao()
    {
        return this._qtDiaExpiracao;
    } //-- int getQtDiaExpiracao() 

    /**
     * Returns the value of field 'qtDiaInatividadeFavorecido'.
     * 
     * @return int
     * @return the value of field 'qtDiaInatividadeFavorecido'.
     */
    public int getQtDiaInatividadeFavorecido()
    {
        return this._qtDiaInatividadeFavorecido;
    } //-- int getQtDiaInatividadeFavorecido() 

    /**
     * Returns the value of field 'qtDiaRepiqConsulta'.
     * 
     * @return int
     * @return the value of field 'qtDiaRepiqConsulta'.
     */
    public int getQtDiaRepiqConsulta()
    {
        return this._qtDiaRepiqConsulta;
    } //-- int getQtDiaRepiqConsulta() 

    /**
     * Returns the value of field 'qtDiaUtilPgto'.
     * 
     * @return int
     * @return the value of field 'qtDiaUtilPgto'.
     */
    public int getQtDiaUtilPgto()
    {
        return this._qtDiaUtilPgto;
    } //-- int getQtDiaUtilPgto() 

    /**
     * Returns the value of field 'qtEtapasRecadastroBeneficio'.
     * 
     * @return int
     * @return the value of field 'qtEtapasRecadastroBeneficio'.
     */
    public int getQtEtapasRecadastroBeneficio()
    {
        return this._qtEtapasRecadastroBeneficio;
    } //-- int getQtEtapasRecadastroBeneficio() 

    /**
     * Returns the value of field 'qtFaseRecadastroBeneficio'.
     * 
     * @return int
     * @return the value of field 'qtFaseRecadastroBeneficio'.
     */
    public int getQtFaseRecadastroBeneficio()
    {
        return this._qtFaseRecadastroBeneficio;
    } //-- int getQtFaseRecadastroBeneficio() 

    /**
     * Returns the value of field 'qtLimiteLinha'.
     * 
     * @return int
     * @return the value of field 'qtLimiteLinha'.
     */
    public int getQtLimiteLinha()
    {
        return this._qtLimiteLinha;
    } //-- int getQtLimiteLinha() 

    /**
     * Returns the value of field 'qtLimiteSolicitacaoCatao'.
     * 
     * @return int
     * @return the value of field 'qtLimiteSolicitacaoCatao'.
     */
    public int getQtLimiteSolicitacaoCatao()
    {
        return this._qtLimiteSolicitacaoCatao;
    } //-- int getQtLimiteSolicitacaoCatao() 

    /**
     * Returns the value of field 'qtMaximaInconLote'.
     * 
     * @return int
     * @return the value of field 'qtMaximaInconLote'.
     */
    public int getQtMaximaInconLote()
    {
        return this._qtMaximaInconLote;
    } //-- int getQtMaximaInconLote() 

    /**
     * Returns the value of field 'qtMaximaTituloVencido'.
     * 
     * @return int
     * @return the value of field 'qtMaximaTituloVencido'.
     */
    public int getQtMaximaTituloVencido()
    {
        return this._qtMaximaTituloVencido;
    } //-- int getQtMaximaTituloVencido() 

    /**
     * Returns the value of field 'qtMesComprovante'.
     * 
     * @return int
     * @return the value of field 'qtMesComprovante'.
     */
    public int getQtMesComprovante()
    {
        return this._qtMesComprovante;
    } //-- int getQtMesComprovante() 

    /**
     * Returns the value of field 'qtMesEtapaRecadastro'.
     * 
     * @return int
     * @return the value of field 'qtMesEtapaRecadastro'.
     */
    public int getQtMesEtapaRecadastro()
    {
        return this._qtMesEtapaRecadastro;
    } //-- int getQtMesEtapaRecadastro() 

    /**
     * Returns the value of field 'qtMesFaseRecadastro'.
     * 
     * @return int
     * @return the value of field 'qtMesFaseRecadastro'.
     */
    public int getQtMesFaseRecadastro()
    {
        return this._qtMesFaseRecadastro;
    } //-- int getQtMesFaseRecadastro() 

    /**
     * Returns the value of field 'qtViaAviso'.
     * 
     * @return int
     * @return the value of field 'qtViaAviso'.
     */
    public int getQtViaAviso()
    {
        return this._qtViaAviso;
    } //-- int getQtViaAviso() 

    /**
     * Returns the value of field 'qtViaCobranca'.
     * 
     * @return int
     * @return the value of field 'qtViaCobranca'.
     */
    public int getQtViaCobranca()
    {
        return this._qtViaCobranca;
    } //-- int getQtViaCobranca() 

    /**
     * Returns the value of field 'qtViaComprovante'.
     * 
     * @return int
     * @return the value of field 'qtViaComprovante'.
     */
    public int getQtViaComprovante()
    {
        return this._qtViaComprovante;
    } //-- int getQtViaComprovante() 

    /**
     * Returns the value of field 'vlFavorecidoNaoCadastro'.
     * 
     * @return double
     * @return the value of field 'vlFavorecidoNaoCadastro'.
     */
    public double getVlFavorecidoNaoCadastro()
    {
        return this._vlFavorecidoNaoCadastro;
    } //-- double getVlFavorecidoNaoCadastro() 

    /**
     * Returns the value of field 'vlLimiteDiaPagamento'.
     * 
     * @return double
     * @return the value of field 'vlLimiteDiaPagamento'.
     */
    public double getVlLimiteDiaPagamento()
    {
        return this._vlLimiteDiaPagamento;
    } //-- double getVlLimiteDiaPagamento() 

    /**
     * Returns the value of field 'vlLimiteIndividualPagamento'.
     * 
     * @return double
     * @return the value of field 'vlLimiteIndividualPagamento'.
     */
    public double getVlLimiteIndividualPagamento()
    {
        return this._vlLimiteIndividualPagamento;
    } //-- double getVlLimiteIndividualPagamento() 

    /**
     * Returns the value of field 'vlPercentualDiferencaTolerada'.
     * 
     * @return double
     * @return the value of field 'vlPercentualDiferencaTolerada'.
     */
    public double getVlPercentualDiferencaTolerada()
    {
        return this._vlPercentualDiferencaTolerada;
    } //-- double getVlPercentualDiferencaTolerada() 

    /**
     * Method hasCdAcaoNaoVida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoNaoVida()
    {
        return this._has_cdAcaoNaoVida;
    } //-- boolean hasCdAcaoNaoVida() 

    /**
     * Method hasCdAcertoDadoRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcertoDadoRecadastro()
    {
        return this._has_cdAcertoDadoRecadastro;
    } //-- boolean hasCdAcertoDadoRecadastro() 

    /**
     * Method hasCdAgendaDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaDebitoVeiculo()
    {
        return this._has_cdAgendaDebitoVeiculo;
    } //-- boolean hasCdAgendaDebitoVeiculo() 

    /**
     * Method hasCdAgendaPagamentoVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaPagamentoVencido()
    {
        return this._has_cdAgendaPagamentoVencido;
    } //-- boolean hasCdAgendaPagamentoVencido() 

    /**
     * Method hasCdAgendaRastreabilidadeFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaRastreabilidadeFilial()
    {
        return this._has_cdAgendaRastreabilidadeFilial;
    } //-- boolean hasCdAgendaRastreabilidadeFilial() 

    /**
     * Method hasCdAgendaValorMenor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendaValorMenor()
    {
        return this._has_cdAgendaValorMenor;
    } //-- boolean hasCdAgendaValorMenor() 

    /**
     * Method hasCdAgrupamentoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoAviso()
    {
        return this._has_cdAgrupamentoAviso;
    } //-- boolean hasCdAgrupamentoAviso() 

    /**
     * Method hasCdAgrupamentoComprovado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoComprovado()
    {
        return this._has_cdAgrupamentoComprovado;
    } //-- boolean hasCdAgrupamentoComprovado() 

    /**
     * Method hasCdAgrupamentoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoFormularioRecadastro()
    {
        return this._has_cdAgrupamentoFormularioRecadastro;
    } //-- boolean hasCdAgrupamentoFormularioRecadastro() 

    /**
     * Method hasCdAntecRecadastroBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAntecRecadastroBeneficio()
    {
        return this._has_cdAntecRecadastroBeneficio;
    } //-- boolean hasCdAntecRecadastroBeneficio() 

    /**
     * Method hasCdAreaReservada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaReservada()
    {
        return this._has_cdAreaReservada;
    } //-- boolean hasCdAreaReservada() 

    /**
     * Method hasCdBaseRecadastroBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBaseRecadastroBeneficio()
    {
        return this._has_cdBaseRecadastroBeneficio;
    } //-- boolean hasCdBaseRecadastroBeneficio() 

    /**
     * Method hasCdBloqueioEmissaoPplta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBloqueioEmissaoPplta()
    {
        return this._has_cdBloqueioEmissaoPplta;
    } //-- boolean hasCdBloqueioEmissaoPplta() 

    /**
     * Method hasCdCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanal()
    {
        return this._has_cdCanal;
    } //-- boolean hasCdCanal() 

    /**
     * Method hasCdCapituloTituloRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCapituloTituloRegistro()
    {
        return this._has_cdCapituloTituloRegistro;
    } //-- boolean hasCdCapituloTituloRegistro() 

    /**
     * Method hasCdCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCobrancaTarifa()
    {
        return this._has_cdCobrancaTarifa;
    } //-- boolean hasCdCobrancaTarifa() 

    /**
     * Method hasCdConsDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsDebitoVeiculo()
    {
        return this._has_cdConsDebitoVeiculo;
    } //-- boolean hasCdConsDebitoVeiculo() 

    /**
     * Method hasCdConsEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsEndereco()
    {
        return this._has_cdConsEndereco;
    } //-- boolean hasCdConsEndereco() 

    /**
     * Method hasCdConsSaldoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsSaldoPagamento()
    {
        return this._has_cdConsSaldoPagamento;
    } //-- boolean hasCdConsSaldoPagamento() 

    /**
     * Method hasCdConsistenciaCpfCnpjBenefAvalNpc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        return this._has_cdConsistenciaCpfCnpjBenefAvalNpc;
    } //-- boolean hasCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Method hasCdConsultaSaldoValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaSaldoValorSuperior()
    {
        return this._has_cdConsultaSaldoValorSuperior;
    } //-- boolean hasCdConsultaSaldoValorSuperior() 

    /**
     * Method hasCdContagemConsSaudo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContagemConsSaudo()
    {
        return this._has_cdContagemConsSaudo;
    } //-- boolean hasCdContagemConsSaudo() 

    /**
     * Method hasCdContratoContaTransferencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContratoContaTransferencia()
    {
        return this._has_cdContratoContaTransferencia;
    } //-- boolean hasCdContratoContaTransferencia() 

    /**
     * Method hasCdCreditoNaoUtilizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCreditoNaoUtilizado()
    {
        return this._has_cdCreditoNaoUtilizado;
    } //-- boolean hasCdCreditoNaoUtilizado() 

    /**
     * Method hasCdCriterioEnquaBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquaBeneficio()
    {
        return this._has_cdCriterioEnquaBeneficio;
    } //-- boolean hasCdCriterioEnquaBeneficio() 

    /**
     * Method hasCdCriterioEnquaRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquaRecadastro()
    {
        return this._has_cdCriterioEnquaRecadastro;
    } //-- boolean hasCdCriterioEnquaRecadastro() 

    /**
     * Method hasCdCriterioRastreabilidadeTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioRastreabilidadeTitulo()
    {
        return this._has_cdCriterioRastreabilidadeTitulo;
    } //-- boolean hasCdCriterioRastreabilidadeTitulo() 

    /**
     * Method hasCdCtciaEspecieBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaEspecieBeneficio()
    {
        return this._has_cdCtciaEspecieBeneficio;
    } //-- boolean hasCdCtciaEspecieBeneficio() 

    /**
     * Method hasCdCtciaIdentificacaoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaIdentificacaoBeneficio()
    {
        return this._has_cdCtciaIdentificacaoBeneficio;
    } //-- boolean hasCdCtciaIdentificacaoBeneficio() 

    /**
     * Method hasCdCtciaInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaInscricaoFavorecido()
    {
        return this._has_cdCtciaInscricaoFavorecido;
    } //-- boolean hasCdCtciaInscricaoFavorecido() 

    /**
     * Method hasCdCtciaProprietarioVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtciaProprietarioVeiculo()
    {
        return this._has_cdCtciaProprietarioVeiculo;
    } //-- boolean hasCdCtciaProprietarioVeiculo() 

    /**
     * Method hasCdDIspzSalarioCrrtt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDIspzSalarioCrrtt()
    {
        return this._has_cdDIspzSalarioCrrtt;
    } //-- boolean hasCdDIspzSalarioCrrtt() 

    /**
     * Method hasCdDependenteOperante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDependenteOperante()
    {
        return this._has_cdDependenteOperante;
    } //-- boolean hasCdDependenteOperante() 

    /**
     * Method hasCdDestinoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoAviso()
    {
        return this._has_cdDestinoAviso;
    } //-- boolean hasCdDestinoAviso() 

    /**
     * Method hasCdDestinoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoComprovante()
    {
        return this._has_cdDestinoComprovante;
    } //-- boolean hasCdDestinoComprovante() 

    /**
     * Method hasCdDestinoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoFormularioRecadastro()
    {
        return this._has_cdDestinoFormularioRecadastro;
    } //-- boolean hasCdDestinoFormularioRecadastro() 

    /**
     * Method hasCdDiaFloatPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDiaFloatPagamento()
    {
        return this._has_cdDiaFloatPagamento;
    } //-- boolean hasCdDiaFloatPagamento() 

    /**
     * Method hasCdDispzContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzContaCredito()
    {
        return this._has_cdDispzContaCredito;
    } //-- boolean hasCdDispzContaCredito() 

    /**
     * Method hasCdDispzDiversarCrrtt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzDiversarCrrtt()
    {
        return this._has_cdDispzDiversarCrrtt;
    } //-- boolean hasCdDispzDiversarCrrtt() 

    /**
     * Method hasCdDispzDiversasNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzDiversasNao()
    {
        return this._has_cdDispzDiversasNao;
    } //-- boolean hasCdDispzDiversasNao() 

    /**
     * Method hasCdDispzSalarioNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDispzSalarioNao()
    {
        return this._has_cdDispzSalarioNao;
    } //-- boolean hasCdDispzSalarioNao() 

    /**
     * Method hasCdEmpresaOperante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaOperante()
    {
        return this._has_cdEmpresaOperante;
    } //-- boolean hasCdEmpresaOperante() 

    /**
     * Method hasCdEnvelopeAberto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEnvelopeAberto()
    {
        return this._has_cdEnvelopeAberto;
    } //-- boolean hasCdEnvelopeAberto() 

    /**
     * Method hasCdEspecieEnderecoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEspecieEnderecoPessoa()
    {
        return this._has_cdEspecieEnderecoPessoa;
    } //-- boolean hasCdEspecieEnderecoPessoa() 

    /**
     * Method hasCdExigeAutFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdExigeAutFilial()
    {
        return this._has_cdExigeAutFilial;
    } //-- boolean hasCdExigeAutFilial() 

    /**
     * Method hasCdFavorecidoConsPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoConsPagamento()
    {
        return this._has_cdFavorecidoConsPagamento;
    } //-- boolean hasCdFavorecidoConsPagamento() 

    /**
     * Method hasCdFloatServicoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFloatServicoContrato()
    {
        return this._has_cdFloatServicoContrato;
    } //-- boolean hasCdFloatServicoContrato() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFormaEnvioPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEnvioPagamento()
    {
        return this._has_cdFormaEnvioPagamento;
    } //-- boolean hasCdFormaEnvioPagamento() 

    /**
     * Method hasCdFormaEstornoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEstornoCredito()
    {
        return this._has_cdFormaEstornoCredito;
    } //-- boolean hasCdFormaEstornoCredito() 

    /**
     * Method hasCdFormaExpiracaoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaExpiracaoCredito()
    {
        return this._has_cdFormaExpiracaoCredito;
    } //-- boolean hasCdFormaExpiracaoCredito() 

    /**
     * Method hasCdFormaManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaManutencao()
    {
        return this._has_cdFormaManutencao;
    } //-- boolean hasCdFormaManutencao() 

    /**
     * Method hasCdFormularioContratoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormularioContratoCliente()
    {
        return this._has_cdFormularioContratoCliente;
    } //-- boolean hasCdFormularioContratoCliente() 

    /**
     * Method hasCdFrasePreCadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFrasePreCadastro()
    {
        return this._has_cdFrasePreCadastro;
    } //-- boolean hasCdFrasePreCadastro() 

    /**
     * Method hasCdIndLancamentoPersonalizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndLancamentoPersonalizado()
    {
        return this._has_cdIndLancamentoPersonalizado;
    } //-- boolean hasCdIndLancamentoPersonalizado() 

    /**
     * Method hasCdIndicadorAdesaoSacador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAdesaoSacador()
    {
        return this._has_cdIndicadorAdesaoSacador;
    } //-- boolean hasCdIndicadorAdesaoSacador() 

    /**
     * Method hasCdIndicadorAgendaGrade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaGrade()
    {
        return this._has_cdIndicadorAgendaGrade;
    } //-- boolean hasCdIndicadorAgendaGrade() 

    /**
     * Method hasCdIndicadorAgendaTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaTitulo()
    {
        return this._has_cdIndicadorAgendaTitulo;
    } //-- boolean hasCdIndicadorAgendaTitulo() 

    /**
     * Method hasCdIndicadorAutorizacaoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoCliente()
    {
        return this._has_cdIndicadorAutorizacaoCliente;
    } //-- boolean hasCdIndicadorAutorizacaoCliente() 

    /**
     * Method hasCdIndicadorAutorizacaoComplemento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoComplemento()
    {
        return this._has_cdIndicadorAutorizacaoComplemento;
    } //-- boolean hasCdIndicadorAutorizacaoComplemento() 

    /**
     * Method hasCdIndicadorBancoPostal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorBancoPostal()
    {
        return this._has_cdIndicadorBancoPostal;
    } //-- boolean hasCdIndicadorBancoPostal() 

    /**
     * Method hasCdIndicadorCadastroOrg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadastroOrg()
    {
        return this._has_cdIndicadorCadastroOrg;
    } //-- boolean hasCdIndicadorCadastroOrg() 

    /**
     * Method hasCdIndicadorCadastroProcd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadastroProcd()
    {
        return this._has_cdIndicadorCadastroProcd;
    } //-- boolean hasCdIndicadorCadastroProcd() 

    /**
     * Method hasCdIndicadorCataoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCataoSalario()
    {
        return this._has_cdIndicadorCataoSalario;
    } //-- boolean hasCdIndicadorCataoSalario() 

    /**
     * Method hasCdIndicadorEmissaoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEmissaoAviso()
    {
        return this._has_cdIndicadorEmissaoAviso;
    } //-- boolean hasCdIndicadorEmissaoAviso() 

    /**
     * Method hasCdIndicadorExpiracaoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorExpiracaoCredito()
    {
        return this._has_cdIndicadorExpiracaoCredito;
    } //-- boolean hasCdIndicadorExpiracaoCredito() 

    /**
     * Method hasCdIndicadorFeriadoLocal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFeriadoLocal()
    {
        return this._has_cdIndicadorFeriadoLocal;
    } //-- boolean hasCdIndicadorFeriadoLocal() 

    /**
     * Method hasCdIndicadorLancamentoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorLancamentoPagamento()
    {
        return this._has_cdIndicadorLancamentoPagamento;
    } //-- boolean hasCdIndicadorLancamentoPagamento() 

    /**
     * Method hasCdIndicadorListaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorListaDebito()
    {
        return this._has_cdIndicadorListaDebito;
    } //-- boolean hasCdIndicadorListaDebito() 

    /**
     * Method hasCdIndicadorManutencaoProcurador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorManutencaoProcurador()
    {
        return this._has_cdIndicadorManutencaoProcurador;
    } //-- boolean hasCdIndicadorManutencaoProcurador() 

    /**
     * Method hasCdIndicadorMensagemPerso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorMensagemPerso()
    {
        return this._has_cdIndicadorMensagemPerso;
    } //-- boolean hasCdIndicadorMensagemPerso() 

    /**
     * Method hasCdIndicadorRetornoInternet
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoInternet()
    {
        return this._has_cdIndicadorRetornoInternet;
    } //-- boolean hasCdIndicadorRetornoInternet() 

    /**
     * Method hasCdIndicadorSegundaLinha
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorSegundaLinha()
    {
        return this._has_cdIndicadorSegundaLinha;
    } //-- boolean hasCdIndicadorSegundaLinha() 

    /**
     * Method hasCdIndicadorSeparaCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorSeparaCanal()
    {
        return this._has_cdIndicadorSeparaCanal;
    } //-- boolean hasCdIndicadorSeparaCanal() 

    /**
     * Method hasCdIndicadorTipoRetornoInternet
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoRetornoInternet()
    {
        return this._has_cdIndicadorTipoRetornoInternet;
    } //-- boolean hasCdIndicadorTipoRetornoInternet() 

    /**
     * Method hasCdIndicadorUtilizaMora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorUtilizaMora()
    {
        return this._has_cdIndicadorUtilizaMora;
    } //-- boolean hasCdIndicadorUtilizaMora() 

    /**
     * Method hasCdLancamentoFuturoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoCredito()
    {
        return this._has_cdLancamentoFuturoCredito;
    } //-- boolean hasCdLancamentoFuturoCredito() 

    /**
     * Method hasCdLancamentoFuturoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoDebito()
    {
        return this._has_cdLancamentoFuturoDebito;
    } //-- boolean hasCdLancamentoFuturoDebito() 

    /**
     * Method hasCdLiberacaoLoteProcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLiberacaoLoteProcesso()
    {
        return this._has_cdLiberacaoLoteProcesso;
    } //-- boolean hasCdLiberacaoLoteProcesso() 

    /**
     * Method hasCdLocalEmissao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLocalEmissao()
    {
        return this._has_cdLocalEmissao;
    } //-- boolean hasCdLocalEmissao() 

    /**
     * Method hasCdManutencaoBaseRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdManutencaoBaseRecadastro()
    {
        return this._has_cdManutencaoBaseRecadastro;
    } //-- boolean hasCdManutencaoBaseRecadastro() 

    /**
     * Method hasCdMeioPagamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPagamentoCredito()
    {
        return this._has_cdMeioPagamentoCredito;
    } //-- boolean hasCdMeioPagamentoCredito() 

    /**
     * Method hasCdMeioPagamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPagamentoDebito()
    {
        return this._has_cdMeioPagamentoDebito;
    } //-- boolean hasCdMeioPagamentoDebito() 

    /**
     * Method hasCdMensagemRecadastroMidia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemRecadastroMidia()
    {
        return this._has_cdMensagemRecadastroMidia;
    } //-- boolean hasCdMensagemRecadastroMidia() 

    /**
     * Method hasCdMidiaDisponivel
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaDisponivel()
    {
        return this._has_cdMidiaDisponivel;
    } //-- boolean hasCdMidiaDisponivel() 

    /**
     * Method hasCdMidiaMensagemRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaMensagemRecadastro()
    {
        return this._has_cdMidiaMensagemRecadastro;
    } //-- boolean hasCdMidiaMensagemRecadastro() 

    /**
     * Method hasCdMomentoAvisoRacadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoAvisoRacadastro()
    {
        return this._has_cdMomentoAvisoRacadastro;
    } //-- boolean hasCdMomentoAvisoRacadastro() 

    /**
     * Method hasCdMomentoCreditoEfetivacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoCreditoEfetivacao()
    {
        return this._has_cdMomentoCreditoEfetivacao;
    } //-- boolean hasCdMomentoCreditoEfetivacao() 

    /**
     * Method hasCdMomentoDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoDebitoPagamento()
    {
        return this._has_cdMomentoDebitoPagamento;
    } //-- boolean hasCdMomentoDebitoPagamento() 

    /**
     * Method hasCdMomentoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoFormularioRecadastro()
    {
        return this._has_cdMomentoFormularioRecadastro;
    } //-- boolean hasCdMomentoFormularioRecadastro() 

    /**
     * Method hasCdMomentoProcessamentoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoProcessamentoPagamento()
    {
        return this._has_cdMomentoProcessamentoPagamento;
    } //-- boolean hasCdMomentoProcessamentoPagamento() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdPagamentoNaoUtilizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoNaoUtilizado()
    {
        return this._has_cdPagamentoNaoUtilizado;
    } //-- boolean hasCdPagamentoNaoUtilizado() 

    /**
     * Method hasCdParametro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParametro()
    {
        return this._has_cdParametro;
    } //-- boolean hasCdParametro() 

    /**
     * Method hasCdPercentualMaximoInconLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPercentualMaximoInconLote()
    {
        return this._has_cdPercentualMaximoInconLote;
    } //-- boolean hasCdPercentualMaximoInconLote() 

    /**
     * Method hasCdPerdcComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcComprovante()
    {
        return this._has_cdPerdcComprovante;
    } //-- boolean hasCdPerdcComprovante() 

    /**
     * Method hasCdPerdcConsultaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcConsultaVeiculo()
    {
        return this._has_cdPerdcConsultaVeiculo;
    } //-- boolean hasCdPerdcConsultaVeiculo() 

    /**
     * Method hasCdPerdcEnvioRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcEnvioRemessa()
    {
        return this._has_cdPerdcEnvioRemessa;
    } //-- boolean hasCdPerdcEnvioRemessa() 

    /**
     * Method hasCdPerdcManutencaoProcd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcManutencaoProcd()
    {
        return this._has_cdPerdcManutencaoProcd;
    } //-- boolean hasCdPerdcManutencaoProcd() 

    /**
     * Method hasCdPeriodicidadeAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeAviso()
    {
        return this._has_cdPeriodicidadeAviso;
    } //-- boolean hasCdPeriodicidadeAviso() 

    /**
     * Method hasCdPermissaoDebitoOnline
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoDebitoOnline()
    {
        return this._has_cdPermissaoDebitoOnline;
    } //-- boolean hasCdPermissaoDebitoOnline() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPreenchimentoLancamentoPersonalizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPreenchimentoLancamentoPersonalizado()
    {
        return this._has_cdPreenchimentoLancamentoPersonalizado;
    } //-- boolean hasCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Method hasCdPrincipalEnquaRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrincipalEnquaRecadastro()
    {
        return this._has_cdPrincipalEnquaRecadastro;
    } //-- boolean hasCdPrincipalEnquaRecadastro() 

    /**
     * Method hasCdPrioridadeEfetivacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeEfetivacaoPagamento()
    {
        return this._has_cdPrioridadeEfetivacaoPagamento;
    } //-- boolean hasCdPrioridadeEfetivacaoPagamento() 

    /**
     * Method hasCdProdutoOperacaroRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaroRelacionado()
    {
        return this._has_cdProdutoOperacaroRelacionado;
    } //-- boolean hasCdProdutoOperacaroRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRastreabilidadeNotaFiscal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabilidadeNotaFiscal()
    {
        return this._has_cdRastreabilidadeNotaFiscal;
    } //-- boolean hasCdRastreabilidadeNotaFiscal() 

    /**
     * Method hasCdRastreabilidadeTituloTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabilidadeTituloTerceiro()
    {
        return this._has_cdRastreabilidadeTituloTerceiro;
    } //-- boolean hasCdRastreabilidadeTituloTerceiro() 

    /**
     * Method hasCdRejeicaoAgendaLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAgendaLote()
    {
        return this._has_cdRejeicaoAgendaLote;
    } //-- boolean hasCdRejeicaoAgendaLote() 

    /**
     * Method hasCdRejeicaoEfetivacaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoEfetivacaoLote()
    {
        return this._has_cdRejeicaoEfetivacaoLote;
    } //-- boolean hasCdRejeicaoEfetivacaoLote() 

    /**
     * Method hasCdRejeicaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoLote()
    {
        return this._has_cdRejeicaoLote;
    } //-- boolean hasCdRejeicaoLote() 

    /**
     * Method hasCdSequenciaEnderecoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSequenciaEnderecoPessoa()
    {
        return this._has_cdSequenciaEnderecoPessoa;
    } //-- boolean hasCdSequenciaEnderecoPessoa() 

    /**
     * Method hasCdTipoCargaRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCargaRecadastro()
    {
        return this._has_cdTipoCargaRecadastro;
    } //-- boolean hasCdTipoCargaRecadastro() 

    /**
     * Method hasCdTipoCataoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCataoSalario()
    {
        return this._has_cdTipoCataoSalario;
    } //-- boolean hasCdTipoCataoSalario() 

    /**
     * Method hasCdTipoConsistenciaLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsistenciaLista()
    {
        return this._has_cdTipoConsistenciaLista;
    } //-- boolean hasCdTipoConsistenciaLista() 

    /**
     * Method hasCdTipoConsultaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsultaComprovante()
    {
        return this._has_cdTipoConsultaComprovante;
    } //-- boolean hasCdTipoConsultaComprovante() 

    /**
     * Method hasCdTipoContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaFavorecido()
    {
        return this._has_cdTipoContaFavorecido;
    } //-- boolean hasCdTipoContaFavorecido() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoDataFloat
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDataFloat()
    {
        return this._has_cdTipoDataFloat;
    } //-- boolean hasCdTipoDataFloat() 

    /**
     * Method hasCdTipoDivergenciaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDivergenciaVeiculo()
    {
        return this._has_cdTipoDivergenciaVeiculo;
    } //-- boolean hasCdTipoDivergenciaVeiculo() 

    /**
     * Method hasCdTipoEnderecoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoEnderecoPessoa()
    {
        return this._has_cdTipoEnderecoPessoa;
    } //-- boolean hasCdTipoEnderecoPessoa() 

    /**
     * Method hasCdTipoFormacaoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFormacaoLista()
    {
        return this._has_cdTipoFormacaoLista;
    } //-- boolean hasCdTipoFormacaoLista() 

    /**
     * Method hasCdTipoIdBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIdBeneficio()
    {
        return this._has_cdTipoIdBeneficio;
    } //-- boolean hasCdTipoIdBeneficio() 

    /**
     * Method hasCdTipoIsncricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIsncricaoFavorecido()
    {
        return this._has_cdTipoIsncricaoFavorecido;
    } //-- boolean hasCdTipoIsncricaoFavorecido() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdTituloDdaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTituloDdaRetorno()
    {
        return this._has_cdTituloDdaRetorno;
    } //-- boolean hasCdTituloDdaRetorno() 

    /**
     * Method hasCdUtilizacaoFavorecidoControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUtilizacaoFavorecidoControle()
    {
        return this._has_cdUtilizacaoFavorecidoControle;
    } //-- boolean hasCdUtilizacaoFavorecidoControle() 

    /**
     * Method hasCdValidacaoNomeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdValidacaoNomeFavorecido()
    {
        return this._has_cdValidacaoNomeFavorecido;
    } //-- boolean hasCdValidacaoNomeFavorecido() 

    /**
     * Method hasCindcdFantsRepas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCindcdFantsRepas()
    {
        return this._has_cindcdFantsRepas;
    } //-- boolean hasCindcdFantsRepas() 

    /**
     * Method hasNrSequenciaCotratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaCotratoNegocio()
    {
        return this._has_nrSequenciaCotratoNegocio;
    } //-- boolean hasNrSequenciaCotratoNegocio() 

    /**
     * Method hasQtAntecedencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAntecedencia()
    {
        return this._has_qtAntecedencia;
    } //-- boolean hasQtAntecedencia() 

    /**
     * Method hasQtAnteriorVencimentoComprovado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtAnteriorVencimentoComprovado()
    {
        return this._has_qtAnteriorVencimentoComprovado;
    } //-- boolean hasQtAnteriorVencimentoComprovado() 

    /**
     * Method hasQtDiaExpiracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaExpiracao()
    {
        return this._has_qtDiaExpiracao;
    } //-- boolean hasQtDiaExpiracao() 

    /**
     * Method hasQtDiaInatividadeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaInatividadeFavorecido()
    {
        return this._has_qtDiaInatividadeFavorecido;
    } //-- boolean hasQtDiaInatividadeFavorecido() 

    /**
     * Method hasQtDiaRepiqConsulta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaRepiqConsulta()
    {
        return this._has_qtDiaRepiqConsulta;
    } //-- boolean hasQtDiaRepiqConsulta() 

    /**
     * Method hasQtDiaUtilPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaUtilPgto()
    {
        return this._has_qtDiaUtilPgto;
    } //-- boolean hasQtDiaUtilPgto() 

    /**
     * Method hasQtEtapasRecadastroBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtEtapasRecadastroBeneficio()
    {
        return this._has_qtEtapasRecadastroBeneficio;
    } //-- boolean hasQtEtapasRecadastroBeneficio() 

    /**
     * Method hasQtFaseRecadastroBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtFaseRecadastroBeneficio()
    {
        return this._has_qtFaseRecadastroBeneficio;
    } //-- boolean hasQtFaseRecadastroBeneficio() 

    /**
     * Method hasQtLimiteLinha
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtLimiteLinha()
    {
        return this._has_qtLimiteLinha;
    } //-- boolean hasQtLimiteLinha() 

    /**
     * Method hasQtLimiteSolicitacaoCatao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtLimiteSolicitacaoCatao()
    {
        return this._has_qtLimiteSolicitacaoCatao;
    } //-- boolean hasQtLimiteSolicitacaoCatao() 

    /**
     * Method hasQtMaximaInconLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMaximaInconLote()
    {
        return this._has_qtMaximaInconLote;
    } //-- boolean hasQtMaximaInconLote() 

    /**
     * Method hasQtMaximaTituloVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMaximaTituloVencido()
    {
        return this._has_qtMaximaTituloVencido;
    } //-- boolean hasQtMaximaTituloVencido() 

    /**
     * Method hasQtMesComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesComprovante()
    {
        return this._has_qtMesComprovante;
    } //-- boolean hasQtMesComprovante() 

    /**
     * Method hasQtMesEtapaRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesEtapaRecadastro()
    {
        return this._has_qtMesEtapaRecadastro;
    } //-- boolean hasQtMesEtapaRecadastro() 

    /**
     * Method hasQtMesFaseRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesFaseRecadastro()
    {
        return this._has_qtMesFaseRecadastro;
    } //-- boolean hasQtMesFaseRecadastro() 

    /**
     * Method hasQtViaAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaAviso()
    {
        return this._has_qtViaAviso;
    } //-- boolean hasQtViaAviso() 

    /**
     * Method hasQtViaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaCobranca()
    {
        return this._has_qtViaCobranca;
    } //-- boolean hasQtViaCobranca() 

    /**
     * Method hasQtViaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaComprovante()
    {
        return this._has_qtViaComprovante;
    } //-- boolean hasQtViaComprovante() 

    /**
     * Method hasVlFavorecidoNaoCadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasVlFavorecidoNaoCadastro()
    {
        return this._has_vlFavorecidoNaoCadastro;
    } //-- boolean hasVlFavorecidoNaoCadastro() 

    /**
     * Method hasVlLimiteDiaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasVlLimiteDiaPagamento()
    {
        return this._has_vlLimiteDiaPagamento;
    } //-- boolean hasVlLimiteDiaPagamento() 

    /**
     * Method hasVlLimiteIndividualPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasVlLimiteIndividualPagamento()
    {
        return this._has_vlLimiteIndividualPagamento;
    } //-- boolean hasVlLimiteIndividualPagamento() 

    /**
     * Method hasVlPercentualDiferencaTolerada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasVlPercentualDiferencaTolerada()
    {
        return this._has_vlPercentualDiferencaTolerada;
    } //-- boolean hasVlPercentualDiferencaTolerada() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoNaoVida'.
     * 
     * @param cdAcaoNaoVida the value of field 'cdAcaoNaoVida'.
     */
    public void setCdAcaoNaoVida(int cdAcaoNaoVida)
    {
        this._cdAcaoNaoVida = cdAcaoNaoVida;
        this._has_cdAcaoNaoVida = true;
    } //-- void setCdAcaoNaoVida(int) 

    /**
     * Sets the value of field 'cdAcertoDadoRecadastro'.
     * 
     * @param cdAcertoDadoRecadastro the value of field
     * 'cdAcertoDadoRecadastro'.
     */
    public void setCdAcertoDadoRecadastro(int cdAcertoDadoRecadastro)
    {
        this._cdAcertoDadoRecadastro = cdAcertoDadoRecadastro;
        this._has_cdAcertoDadoRecadastro = true;
    } //-- void setCdAcertoDadoRecadastro(int) 

    /**
     * Sets the value of field 'cdAgendaDebitoVeiculo'.
     * 
     * @param cdAgendaDebitoVeiculo the value of field
     * 'cdAgendaDebitoVeiculo'.
     */
    public void setCdAgendaDebitoVeiculo(int cdAgendaDebitoVeiculo)
    {
        this._cdAgendaDebitoVeiculo = cdAgendaDebitoVeiculo;
        this._has_cdAgendaDebitoVeiculo = true;
    } //-- void setCdAgendaDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdAgendaPagamentoVencido'.
     * 
     * @param cdAgendaPagamentoVencido the value of field
     * 'cdAgendaPagamentoVencido'.
     */
    public void setCdAgendaPagamentoVencido(int cdAgendaPagamentoVencido)
    {
        this._cdAgendaPagamentoVencido = cdAgendaPagamentoVencido;
        this._has_cdAgendaPagamentoVencido = true;
    } //-- void setCdAgendaPagamentoVencido(int) 

    /**
     * Sets the value of field 'cdAgendaRastreabilidadeFilial'.
     * 
     * @param cdAgendaRastreabilidadeFilial the value of field
     * 'cdAgendaRastreabilidadeFilial'.
     */
    public void setCdAgendaRastreabilidadeFilial(int cdAgendaRastreabilidadeFilial)
    {
        this._cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
        this._has_cdAgendaRastreabilidadeFilial = true;
    } //-- void setCdAgendaRastreabilidadeFilial(int) 

    /**
     * Sets the value of field 'cdAgendaValorMenor'.
     * 
     * @param cdAgendaValorMenor the value of field
     * 'cdAgendaValorMenor'.
     */
    public void setCdAgendaValorMenor(int cdAgendaValorMenor)
    {
        this._cdAgendaValorMenor = cdAgendaValorMenor;
        this._has_cdAgendaValorMenor = true;
    } //-- void setCdAgendaValorMenor(int) 

    /**
     * Sets the value of field 'cdAgrupamentoAviso'.
     * 
     * @param cdAgrupamentoAviso the value of field
     * 'cdAgrupamentoAviso'.
     */
    public void setCdAgrupamentoAviso(int cdAgrupamentoAviso)
    {
        this._cdAgrupamentoAviso = cdAgrupamentoAviso;
        this._has_cdAgrupamentoAviso = true;
    } //-- void setCdAgrupamentoAviso(int) 

    /**
     * Sets the value of field 'cdAgrupamentoComprovado'.
     * 
     * @param cdAgrupamentoComprovado the value of field
     * 'cdAgrupamentoComprovado'.
     */
    public void setCdAgrupamentoComprovado(int cdAgrupamentoComprovado)
    {
        this._cdAgrupamentoComprovado = cdAgrupamentoComprovado;
        this._has_cdAgrupamentoComprovado = true;
    } //-- void setCdAgrupamentoComprovado(int) 

    /**
     * Sets the value of field 'cdAgrupamentoFormularioRecadastro'.
     * 
     * @param cdAgrupamentoFormularioRecadastro the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     */
    public void setCdAgrupamentoFormularioRecadastro(int cdAgrupamentoFormularioRecadastro)
    {
        this._cdAgrupamentoFormularioRecadastro = cdAgrupamentoFormularioRecadastro;
        this._has_cdAgrupamentoFormularioRecadastro = true;
    } //-- void setCdAgrupamentoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdAntecRecadastroBeneficio'.
     * 
     * @param cdAntecRecadastroBeneficio the value of field
     * 'cdAntecRecadastroBeneficio'.
     */
    public void setCdAntecRecadastroBeneficio(int cdAntecRecadastroBeneficio)
    {
        this._cdAntecRecadastroBeneficio = cdAntecRecadastroBeneficio;
        this._has_cdAntecRecadastroBeneficio = true;
    } //-- void setCdAntecRecadastroBeneficio(int) 

    /**
     * Sets the value of field 'cdAreaReservada'.
     * 
     * @param cdAreaReservada the value of field 'cdAreaReservada'.
     */
    public void setCdAreaReservada(int cdAreaReservada)
    {
        this._cdAreaReservada = cdAreaReservada;
        this._has_cdAreaReservada = true;
    } //-- void setCdAreaReservada(int) 

    /**
     * Sets the value of field 'cdBaseRecadastroBeneficio'.
     * 
     * @param cdBaseRecadastroBeneficio the value of field
     * 'cdBaseRecadastroBeneficio'.
     */
    public void setCdBaseRecadastroBeneficio(int cdBaseRecadastroBeneficio)
    {
        this._cdBaseRecadastroBeneficio = cdBaseRecadastroBeneficio;
        this._has_cdBaseRecadastroBeneficio = true;
    } //-- void setCdBaseRecadastroBeneficio(int) 

    /**
     * Sets the value of field 'cdBloqueioEmissaoPplta'.
     * 
     * @param cdBloqueioEmissaoPplta the value of field
     * 'cdBloqueioEmissaoPplta'.
     */
    public void setCdBloqueioEmissaoPplta(int cdBloqueioEmissaoPplta)
    {
        this._cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
        this._has_cdBloqueioEmissaoPplta = true;
    } //-- void setCdBloqueioEmissaoPplta(int) 

    /**
     * Sets the value of field 'cdCanal'.
     * 
     * @param cdCanal the value of field 'cdCanal'.
     */
    public void setCdCanal(int cdCanal)
    {
        this._cdCanal = cdCanal;
        this._has_cdCanal = true;
    } //-- void setCdCanal(int) 

    /**
     * Sets the value of field 'cdCapituloTituloRegistro'.
     * 
     * @param cdCapituloTituloRegistro the value of field
     * 'cdCapituloTituloRegistro'.
     */
    public void setCdCapituloTituloRegistro(int cdCapituloTituloRegistro)
    {
        this._cdCapituloTituloRegistro = cdCapituloTituloRegistro;
        this._has_cdCapituloTituloRegistro = true;
    } //-- void setCdCapituloTituloRegistro(int) 

    /**
     * Sets the value of field 'cdCobrancaTarifa'.
     * 
     * @param cdCobrancaTarifa the value of field 'cdCobrancaTarifa'
     */
    public void setCdCobrancaTarifa(int cdCobrancaTarifa)
    {
        this._cdCobrancaTarifa = cdCobrancaTarifa;
        this._has_cdCobrancaTarifa = true;
    } //-- void setCdCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdConsDebitoVeiculo'.
     * 
     * @param cdConsDebitoVeiculo the value of field
     * 'cdConsDebitoVeiculo'.
     */
    public void setCdConsDebitoVeiculo(int cdConsDebitoVeiculo)
    {
        this._cdConsDebitoVeiculo = cdConsDebitoVeiculo;
        this._has_cdConsDebitoVeiculo = true;
    } //-- void setCdConsDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdConsEndereco'.
     * 
     * @param cdConsEndereco the value of field 'cdConsEndereco'.
     */
    public void setCdConsEndereco(int cdConsEndereco)
    {
        this._cdConsEndereco = cdConsEndereco;
        this._has_cdConsEndereco = true;
    } //-- void setCdConsEndereco(int) 

    /**
     * Sets the value of field 'cdConsSaldoPagamento'.
     * 
     * @param cdConsSaldoPagamento the value of field
     * 'cdConsSaldoPagamento'.
     */
    public void setCdConsSaldoPagamento(int cdConsSaldoPagamento)
    {
        this._cdConsSaldoPagamento = cdConsSaldoPagamento;
        this._has_cdConsSaldoPagamento = true;
    } //-- void setCdConsSaldoPagamento(int) 

    /**
     * Sets the value of field 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @param cdConsistenciaCpfCnpjBenefAvalNpc the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public void setCdConsistenciaCpfCnpjBenefAvalNpc(int cdConsistenciaCpfCnpjBenefAvalNpc)
    {
        this._cdConsistenciaCpfCnpjBenefAvalNpc = cdConsistenciaCpfCnpjBenefAvalNpc;
        this._has_cdConsistenciaCpfCnpjBenefAvalNpc = true;
    } //-- void setCdConsistenciaCpfCnpjBenefAvalNpc(int) 

    /**
     * Sets the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @param cdConsultaSaldoValorSuperior the value of field
     * 'cdConsultaSaldoValorSuperior'.
     */
    public void setCdConsultaSaldoValorSuperior(int cdConsultaSaldoValorSuperior)
    {
        this._cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
        this._has_cdConsultaSaldoValorSuperior = true;
    } //-- void setCdConsultaSaldoValorSuperior(int) 

    /**
     * Sets the value of field 'cdContagemConsSaudo'.
     * 
     * @param cdContagemConsSaudo the value of field
     * 'cdContagemConsSaudo'.
     */
    public void setCdContagemConsSaudo(int cdContagemConsSaudo)
    {
        this._cdContagemConsSaudo = cdContagemConsSaudo;
        this._has_cdContagemConsSaudo = true;
    } //-- void setCdContagemConsSaudo(int) 

    /**
     * Sets the value of field 'cdContratoContaTransferencia'.
     * 
     * @param cdContratoContaTransferencia the value of field
     * 'cdContratoContaTransferencia'.
     */
    public void setCdContratoContaTransferencia(int cdContratoContaTransferencia)
    {
        this._cdContratoContaTransferencia = cdContratoContaTransferencia;
        this._has_cdContratoContaTransferencia = true;
    } //-- void setCdContratoContaTransferencia(int) 

    /**
     * Sets the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @param cdCreditoNaoUtilizado the value of field
     * 'cdCreditoNaoUtilizado'.
     */
    public void setCdCreditoNaoUtilizado(int cdCreditoNaoUtilizado)
    {
        this._cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
        this._has_cdCreditoNaoUtilizado = true;
    } //-- void setCdCreditoNaoUtilizado(int) 

    /**
     * Sets the value of field 'cdCriterioEnquaBeneficio'.
     * 
     * @param cdCriterioEnquaBeneficio the value of field
     * 'cdCriterioEnquaBeneficio'.
     */
    public void setCdCriterioEnquaBeneficio(int cdCriterioEnquaBeneficio)
    {
        this._cdCriterioEnquaBeneficio = cdCriterioEnquaBeneficio;
        this._has_cdCriterioEnquaBeneficio = true;
    } //-- void setCdCriterioEnquaBeneficio(int) 

    /**
     * Sets the value of field 'cdCriterioEnquaRecadastro'.
     * 
     * @param cdCriterioEnquaRecadastro the value of field
     * 'cdCriterioEnquaRecadastro'.
     */
    public void setCdCriterioEnquaRecadastro(int cdCriterioEnquaRecadastro)
    {
        this._cdCriterioEnquaRecadastro = cdCriterioEnquaRecadastro;
        this._has_cdCriterioEnquaRecadastro = true;
    } //-- void setCdCriterioEnquaRecadastro(int) 

    /**
     * Sets the value of field 'cdCriterioRastreabilidadeTitulo'.
     * 
     * @param cdCriterioRastreabilidadeTitulo the value of field
     * 'cdCriterioRastreabilidadeTitulo'.
     */
    public void setCdCriterioRastreabilidadeTitulo(int cdCriterioRastreabilidadeTitulo)
    {
        this._cdCriterioRastreabilidadeTitulo = cdCriterioRastreabilidadeTitulo;
        this._has_cdCriterioRastreabilidadeTitulo = true;
    } //-- void setCdCriterioRastreabilidadeTitulo(int) 

    /**
     * Sets the value of field 'cdCtciaEspecieBeneficio'.
     * 
     * @param cdCtciaEspecieBeneficio the value of field
     * 'cdCtciaEspecieBeneficio'.
     */
    public void setCdCtciaEspecieBeneficio(int cdCtciaEspecieBeneficio)
    {
        this._cdCtciaEspecieBeneficio = cdCtciaEspecieBeneficio;
        this._has_cdCtciaEspecieBeneficio = true;
    } //-- void setCdCtciaEspecieBeneficio(int) 

    /**
     * Sets the value of field 'cdCtciaIdentificacaoBeneficio'.
     * 
     * @param cdCtciaIdentificacaoBeneficio the value of field
     * 'cdCtciaIdentificacaoBeneficio'.
     */
    public void setCdCtciaIdentificacaoBeneficio(int cdCtciaIdentificacaoBeneficio)
    {
        this._cdCtciaIdentificacaoBeneficio = cdCtciaIdentificacaoBeneficio;
        this._has_cdCtciaIdentificacaoBeneficio = true;
    } //-- void setCdCtciaIdentificacaoBeneficio(int) 

    /**
     * Sets the value of field 'cdCtciaInscricaoFavorecido'.
     * 
     * @param cdCtciaInscricaoFavorecido the value of field
     * 'cdCtciaInscricaoFavorecido'.
     */
    public void setCdCtciaInscricaoFavorecido(int cdCtciaInscricaoFavorecido)
    {
        this._cdCtciaInscricaoFavorecido = cdCtciaInscricaoFavorecido;
        this._has_cdCtciaInscricaoFavorecido = true;
    } //-- void setCdCtciaInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdCtciaProprietarioVeiculo'.
     * 
     * @param cdCtciaProprietarioVeiculo the value of field
     * 'cdCtciaProprietarioVeiculo'.
     */
    public void setCdCtciaProprietarioVeiculo(int cdCtciaProprietarioVeiculo)
    {
        this._cdCtciaProprietarioVeiculo = cdCtciaProprietarioVeiculo;
        this._has_cdCtciaProprietarioVeiculo = true;
    } //-- void setCdCtciaProprietarioVeiculo(int) 

    /**
     * Sets the value of field 'cdDIspzSalarioCrrtt'.
     * 
     * @param cdDIspzSalarioCrrtt the value of field
     * 'cdDIspzSalarioCrrtt'.
     */
    public void setCdDIspzSalarioCrrtt(int cdDIspzSalarioCrrtt)
    {
        this._cdDIspzSalarioCrrtt = cdDIspzSalarioCrrtt;
        this._has_cdDIspzSalarioCrrtt = true;
    } //-- void setCdDIspzSalarioCrrtt(int) 

    /**
     * Sets the value of field 'cdDependenteOperante'.
     * 
     * @param cdDependenteOperante the value of field
     * 'cdDependenteOperante'.
     */
    public void setCdDependenteOperante(int cdDependenteOperante)
    {
        this._cdDependenteOperante = cdDependenteOperante;
        this._has_cdDependenteOperante = true;
    } //-- void setCdDependenteOperante(int) 

    /**
     * Sets the value of field 'cdDestinoAviso'.
     * 
     * @param cdDestinoAviso the value of field 'cdDestinoAviso'.
     */
    public void setCdDestinoAviso(int cdDestinoAviso)
    {
        this._cdDestinoAviso = cdDestinoAviso;
        this._has_cdDestinoAviso = true;
    } //-- void setCdDestinoAviso(int) 

    /**
     * Sets the value of field 'cdDestinoComprovante'.
     * 
     * @param cdDestinoComprovante the value of field
     * 'cdDestinoComprovante'.
     */
    public void setCdDestinoComprovante(int cdDestinoComprovante)
    {
        this._cdDestinoComprovante = cdDestinoComprovante;
        this._has_cdDestinoComprovante = true;
    } //-- void setCdDestinoComprovante(int) 

    /**
     * Sets the value of field 'cdDestinoFormularioRecadastro'.
     * 
     * @param cdDestinoFormularioRecadastro the value of field
     * 'cdDestinoFormularioRecadastro'.
     */
    public void setCdDestinoFormularioRecadastro(int cdDestinoFormularioRecadastro)
    {
        this._cdDestinoFormularioRecadastro = cdDestinoFormularioRecadastro;
        this._has_cdDestinoFormularioRecadastro = true;
    } //-- void setCdDestinoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdDiaFloatPagamento'.
     * 
     * @param cdDiaFloatPagamento the value of field
     * 'cdDiaFloatPagamento'.
     */
    public void setCdDiaFloatPagamento(int cdDiaFloatPagamento)
    {
        this._cdDiaFloatPagamento = cdDiaFloatPagamento;
        this._has_cdDiaFloatPagamento = true;
    } //-- void setCdDiaFloatPagamento(int) 

    /**
     * Sets the value of field 'cdDispzContaCredito'.
     * 
     * @param cdDispzContaCredito the value of field
     * 'cdDispzContaCredito'.
     */
    public void setCdDispzContaCredito(int cdDispzContaCredito)
    {
        this._cdDispzContaCredito = cdDispzContaCredito;
        this._has_cdDispzContaCredito = true;
    } //-- void setCdDispzContaCredito(int) 

    /**
     * Sets the value of field 'cdDispzDiversarCrrtt'.
     * 
     * @param cdDispzDiversarCrrtt the value of field
     * 'cdDispzDiversarCrrtt'.
     */
    public void setCdDispzDiversarCrrtt(int cdDispzDiversarCrrtt)
    {
        this._cdDispzDiversarCrrtt = cdDispzDiversarCrrtt;
        this._has_cdDispzDiversarCrrtt = true;
    } //-- void setCdDispzDiversarCrrtt(int) 

    /**
     * Sets the value of field 'cdDispzDiversasNao'.
     * 
     * @param cdDispzDiversasNao the value of field
     * 'cdDispzDiversasNao'.
     */
    public void setCdDispzDiversasNao(int cdDispzDiversasNao)
    {
        this._cdDispzDiversasNao = cdDispzDiversasNao;
        this._has_cdDispzDiversasNao = true;
    } //-- void setCdDispzDiversasNao(int) 

    /**
     * Sets the value of field 'cdDispzSalarioNao'.
     * 
     * @param cdDispzSalarioNao the value of field
     * 'cdDispzSalarioNao'.
     */
    public void setCdDispzSalarioNao(int cdDispzSalarioNao)
    {
        this._cdDispzSalarioNao = cdDispzSalarioNao;
        this._has_cdDispzSalarioNao = true;
    } //-- void setCdDispzSalarioNao(int) 

    /**
     * Sets the value of field 'cdEmpresaOperante'.
     * 
     * @param cdEmpresaOperante the value of field
     * 'cdEmpresaOperante'.
     */
    public void setCdEmpresaOperante(long cdEmpresaOperante)
    {
        this._cdEmpresaOperante = cdEmpresaOperante;
        this._has_cdEmpresaOperante = true;
    } //-- void setCdEmpresaOperante(long) 

    /**
     * Sets the value of field 'cdEnvelopeAberto'.
     * 
     * @param cdEnvelopeAberto the value of field 'cdEnvelopeAberto'
     */
    public void setCdEnvelopeAberto(int cdEnvelopeAberto)
    {
        this._cdEnvelopeAberto = cdEnvelopeAberto;
        this._has_cdEnvelopeAberto = true;
    } //-- void setCdEnvelopeAberto(int) 

    /**
     * Sets the value of field 'cdEspecieEnderecoPessoa'.
     * 
     * @param cdEspecieEnderecoPessoa the value of field
     * 'cdEspecieEnderecoPessoa'.
     */
    public void setCdEspecieEnderecoPessoa(int cdEspecieEnderecoPessoa)
    {
        this._cdEspecieEnderecoPessoa = cdEspecieEnderecoPessoa;
        this._has_cdEspecieEnderecoPessoa = true;
    } //-- void setCdEspecieEnderecoPessoa(int) 

    /**
     * Sets the value of field 'cdExigeAutFilial'.
     * 
     * @param cdExigeAutFilial the value of field 'cdExigeAutFilial'
     */
    public void setCdExigeAutFilial(int cdExigeAutFilial)
    {
        this._cdExigeAutFilial = cdExigeAutFilial;
        this._has_cdExigeAutFilial = true;
    } //-- void setCdExigeAutFilial(int) 

    /**
     * Sets the value of field 'cdFavorecidoConsPagamento'.
     * 
     * @param cdFavorecidoConsPagamento the value of field
     * 'cdFavorecidoConsPagamento'.
     */
    public void setCdFavorecidoConsPagamento(int cdFavorecidoConsPagamento)
    {
        this._cdFavorecidoConsPagamento = cdFavorecidoConsPagamento;
        this._has_cdFavorecidoConsPagamento = true;
    } //-- void setCdFavorecidoConsPagamento(int) 

    /**
     * Sets the value of field 'cdFloatServicoContrato'.
     * 
     * @param cdFloatServicoContrato the value of field
     * 'cdFloatServicoContrato'.
     */
    public void setCdFloatServicoContrato(int cdFloatServicoContrato)
    {
        this._cdFloatServicoContrato = cdFloatServicoContrato;
        this._has_cdFloatServicoContrato = true;
    } //-- void setCdFloatServicoContrato(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFormaEnvioPagamento'.
     * 
     * @param cdFormaEnvioPagamento the value of field
     * 'cdFormaEnvioPagamento'.
     */
    public void setCdFormaEnvioPagamento(int cdFormaEnvioPagamento)
    {
        this._cdFormaEnvioPagamento = cdFormaEnvioPagamento;
        this._has_cdFormaEnvioPagamento = true;
    } //-- void setCdFormaEnvioPagamento(int) 

    /**
     * Sets the value of field 'cdFormaEstornoCredito'.
     * 
     * @param cdFormaEstornoCredito the value of field
     * 'cdFormaEstornoCredito'.
     */
    public void setCdFormaEstornoCredito(int cdFormaEstornoCredito)
    {
        this._cdFormaEstornoCredito = cdFormaEstornoCredito;
        this._has_cdFormaEstornoCredito = true;
    } //-- void setCdFormaEstornoCredito(int) 

    /**
     * Sets the value of field 'cdFormaExpiracaoCredito'.
     * 
     * @param cdFormaExpiracaoCredito the value of field
     * 'cdFormaExpiracaoCredito'.
     */
    public void setCdFormaExpiracaoCredito(int cdFormaExpiracaoCredito)
    {
        this._cdFormaExpiracaoCredito = cdFormaExpiracaoCredito;
        this._has_cdFormaExpiracaoCredito = true;
    } //-- void setCdFormaExpiracaoCredito(int) 

    /**
     * Sets the value of field 'cdFormaManutencao'.
     * 
     * @param cdFormaManutencao the value of field
     * 'cdFormaManutencao'.
     */
    public void setCdFormaManutencao(int cdFormaManutencao)
    {
        this._cdFormaManutencao = cdFormaManutencao;
        this._has_cdFormaManutencao = true;
    } //-- void setCdFormaManutencao(int) 

    /**
     * Sets the value of field 'cdFormularioContratoCliente'.
     * 
     * @param cdFormularioContratoCliente the value of field
     * 'cdFormularioContratoCliente'.
     */
    public void setCdFormularioContratoCliente(int cdFormularioContratoCliente)
    {
        this._cdFormularioContratoCliente = cdFormularioContratoCliente;
        this._has_cdFormularioContratoCliente = true;
    } //-- void setCdFormularioContratoCliente(int) 

    /**
     * Sets the value of field 'cdFrasePreCadastro'.
     * 
     * @param cdFrasePreCadastro the value of field
     * 'cdFrasePreCadastro'.
     */
    public void setCdFrasePreCadastro(int cdFrasePreCadastro)
    {
        this._cdFrasePreCadastro = cdFrasePreCadastro;
        this._has_cdFrasePreCadastro = true;
    } //-- void setCdFrasePreCadastro(int) 

    /**
     * Sets the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @param cdIndLancamentoPersonalizado the value of field
     * 'cdIndLancamentoPersonalizado'.
     */
    public void setCdIndLancamentoPersonalizado(int cdIndLancamentoPersonalizado)
    {
        this._cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
        this._has_cdIndLancamentoPersonalizado = true;
    } //-- void setCdIndLancamentoPersonalizado(int) 

    /**
     * Sets the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @param cdIndicadorAdesaoSacador the value of field
     * 'cdIndicadorAdesaoSacador'.
     */
    public void setCdIndicadorAdesaoSacador(int cdIndicadorAdesaoSacador)
    {
        this._cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
        this._has_cdIndicadorAdesaoSacador = true;
    } //-- void setCdIndicadorAdesaoSacador(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaGrade'.
     * 
     * @param cdIndicadorAgendaGrade the value of field
     * 'cdIndicadorAgendaGrade'.
     */
    public void setCdIndicadorAgendaGrade(int cdIndicadorAgendaGrade)
    {
        this._cdIndicadorAgendaGrade = cdIndicadorAgendaGrade;
        this._has_cdIndicadorAgendaGrade = true;
    } //-- void setCdIndicadorAgendaGrade(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @param cdIndicadorAgendaTitulo the value of field
     * 'cdIndicadorAgendaTitulo'.
     */
    public void setCdIndicadorAgendaTitulo(int cdIndicadorAgendaTitulo)
    {
        this._cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
        this._has_cdIndicadorAgendaTitulo = true;
    } //-- void setCdIndicadorAgendaTitulo(int) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @param cdIndicadorAutorizacaoCliente the value of field
     * 'cdIndicadorAutorizacaoCliente'.
     */
    public void setCdIndicadorAutorizacaoCliente(int cdIndicadorAutorizacaoCliente)
    {
        this._cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
        this._has_cdIndicadorAutorizacaoCliente = true;
    } //-- void setCdIndicadorAutorizacaoCliente(int) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @param cdIndicadorAutorizacaoComplemento the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public void setCdIndicadorAutorizacaoComplemento(int cdIndicadorAutorizacaoComplemento)
    {
        this._cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
        this._has_cdIndicadorAutorizacaoComplemento = true;
    } //-- void setCdIndicadorAutorizacaoComplemento(int) 

    /**
     * Sets the value of field 'cdIndicadorBancoPostal'.
     * 
     * @param cdIndicadorBancoPostal the value of field
     * 'cdIndicadorBancoPostal'.
     */
    public void setCdIndicadorBancoPostal(int cdIndicadorBancoPostal)
    {
        this._cdIndicadorBancoPostal = cdIndicadorBancoPostal;
        this._has_cdIndicadorBancoPostal = true;
    } //-- void setCdIndicadorBancoPostal(int) 

    /**
     * Sets the value of field 'cdIndicadorCadastroOrg'.
     * 
     * @param cdIndicadorCadastroOrg the value of field
     * 'cdIndicadorCadastroOrg'.
     */
    public void setCdIndicadorCadastroOrg(int cdIndicadorCadastroOrg)
    {
        this._cdIndicadorCadastroOrg = cdIndicadorCadastroOrg;
        this._has_cdIndicadorCadastroOrg = true;
    } //-- void setCdIndicadorCadastroOrg(int) 

    /**
     * Sets the value of field 'cdIndicadorCadastroProcd'.
     * 
     * @param cdIndicadorCadastroProcd the value of field
     * 'cdIndicadorCadastroProcd'.
     */
    public void setCdIndicadorCadastroProcd(int cdIndicadorCadastroProcd)
    {
        this._cdIndicadorCadastroProcd = cdIndicadorCadastroProcd;
        this._has_cdIndicadorCadastroProcd = true;
    } //-- void setCdIndicadorCadastroProcd(int) 

    /**
     * Sets the value of field 'cdIndicadorCataoSalario'.
     * 
     * @param cdIndicadorCataoSalario the value of field
     * 'cdIndicadorCataoSalario'.
     */
    public void setCdIndicadorCataoSalario(int cdIndicadorCataoSalario)
    {
        this._cdIndicadorCataoSalario = cdIndicadorCataoSalario;
        this._has_cdIndicadorCataoSalario = true;
    } //-- void setCdIndicadorCataoSalario(int) 

    /**
     * Sets the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @param cdIndicadorEmissaoAviso the value of field
     * 'cdIndicadorEmissaoAviso'.
     */
    public void setCdIndicadorEmissaoAviso(int cdIndicadorEmissaoAviso)
    {
        this._cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
        this._has_cdIndicadorEmissaoAviso = true;
    } //-- void setCdIndicadorEmissaoAviso(int) 

    /**
     * Sets the value of field 'cdIndicadorExpiracaoCredito'.
     * 
     * @param cdIndicadorExpiracaoCredito the value of field
     * 'cdIndicadorExpiracaoCredito'.
     */
    public void setCdIndicadorExpiracaoCredito(int cdIndicadorExpiracaoCredito)
    {
        this._cdIndicadorExpiracaoCredito = cdIndicadorExpiracaoCredito;
        this._has_cdIndicadorExpiracaoCredito = true;
    } //-- void setCdIndicadorExpiracaoCredito(int) 

    /**
     * Sets the value of field 'cdIndicadorFeriadoLocal'.
     * 
     * @param cdIndicadorFeriadoLocal the value of field
     * 'cdIndicadorFeriadoLocal'.
     */
    public void setCdIndicadorFeriadoLocal(int cdIndicadorFeriadoLocal)
    {
        this._cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
        this._has_cdIndicadorFeriadoLocal = true;
    } //-- void setCdIndicadorFeriadoLocal(int) 

    /**
     * Sets the value of field 'cdIndicadorLancamentoPagamento'.
     * 
     * @param cdIndicadorLancamentoPagamento the value of field
     * 'cdIndicadorLancamentoPagamento'.
     */
    public void setCdIndicadorLancamentoPagamento(int cdIndicadorLancamentoPagamento)
    {
        this._cdIndicadorLancamentoPagamento = cdIndicadorLancamentoPagamento;
        this._has_cdIndicadorLancamentoPagamento = true;
    } //-- void setCdIndicadorLancamentoPagamento(int) 

    /**
     * Sets the value of field 'cdIndicadorListaDebito'.
     * 
     * @param cdIndicadorListaDebito the value of field
     * 'cdIndicadorListaDebito'.
     */
    public void setCdIndicadorListaDebito(int cdIndicadorListaDebito)
    {
        this._cdIndicadorListaDebito = cdIndicadorListaDebito;
        this._has_cdIndicadorListaDebito = true;
    } //-- void setCdIndicadorListaDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorManutencaoProcurador'.
     * 
     * @param cdIndicadorManutencaoProcurador the value of field
     * 'cdIndicadorManutencaoProcurador'.
     */
    public void setCdIndicadorManutencaoProcurador(int cdIndicadorManutencaoProcurador)
    {
        this._cdIndicadorManutencaoProcurador = cdIndicadorManutencaoProcurador;
        this._has_cdIndicadorManutencaoProcurador = true;
    } //-- void setCdIndicadorManutencaoProcurador(int) 

    /**
     * Sets the value of field 'cdIndicadorMensagemPerso'.
     * 
     * @param cdIndicadorMensagemPerso the value of field
     * 'cdIndicadorMensagemPerso'.
     */
    public void setCdIndicadorMensagemPerso(int cdIndicadorMensagemPerso)
    {
        this._cdIndicadorMensagemPerso = cdIndicadorMensagemPerso;
        this._has_cdIndicadorMensagemPerso = true;
    } //-- void setCdIndicadorMensagemPerso(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @param cdIndicadorRetornoInternet the value of field
     * 'cdIndicadorRetornoInternet'.
     */
    public void setCdIndicadorRetornoInternet(int cdIndicadorRetornoInternet)
    {
        this._cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
        this._has_cdIndicadorRetornoInternet = true;
    } //-- void setCdIndicadorRetornoInternet(int) 

    /**
     * Sets the value of field 'cdIndicadorSegundaLinha'.
     * 
     * @param cdIndicadorSegundaLinha the value of field
     * 'cdIndicadorSegundaLinha'.
     */
    public void setCdIndicadorSegundaLinha(int cdIndicadorSegundaLinha)
    {
        this._cdIndicadorSegundaLinha = cdIndicadorSegundaLinha;
        this._has_cdIndicadorSegundaLinha = true;
    } //-- void setCdIndicadorSegundaLinha(int) 

    /**
     * Sets the value of field 'cdIndicadorSeparaCanal'.
     * 
     * @param cdIndicadorSeparaCanal the value of field
     * 'cdIndicadorSeparaCanal'.
     */
    public void setCdIndicadorSeparaCanal(int cdIndicadorSeparaCanal)
    {
        this._cdIndicadorSeparaCanal = cdIndicadorSeparaCanal;
        this._has_cdIndicadorSeparaCanal = true;
    } //-- void setCdIndicadorSeparaCanal(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoRetornoInternet'.
     * 
     * @param cdIndicadorTipoRetornoInternet the value of field
     * 'cdIndicadorTipoRetornoInternet'.
     */
    public void setCdIndicadorTipoRetornoInternet(int cdIndicadorTipoRetornoInternet)
    {
        this._cdIndicadorTipoRetornoInternet = cdIndicadorTipoRetornoInternet;
        this._has_cdIndicadorTipoRetornoInternet = true;
    } //-- void setCdIndicadorTipoRetornoInternet(int) 

    /**
     * Sets the value of field 'cdIndicadorUtilizaMora'.
     * 
     * @param cdIndicadorUtilizaMora the value of field
     * 'cdIndicadorUtilizaMora'.
     */
    public void setCdIndicadorUtilizaMora(int cdIndicadorUtilizaMora)
    {
        this._cdIndicadorUtilizaMora = cdIndicadorUtilizaMora;
        this._has_cdIndicadorUtilizaMora = true;
    } //-- void setCdIndicadorUtilizaMora(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @param cdLancamentoFuturoCredito the value of field
     * 'cdLancamentoFuturoCredito'.
     */
    public void setCdLancamentoFuturoCredito(int cdLancamentoFuturoCredito)
    {
        this._cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
        this._has_cdLancamentoFuturoCredito = true;
    } //-- void setCdLancamentoFuturoCredito(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @param cdLancamentoFuturoDebito the value of field
     * 'cdLancamentoFuturoDebito'.
     */
    public void setCdLancamentoFuturoDebito(int cdLancamentoFuturoDebito)
    {
        this._cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
        this._has_cdLancamentoFuturoDebito = true;
    } //-- void setCdLancamentoFuturoDebito(int) 

    /**
     * Sets the value of field 'cdLiberacaoLoteProcesso'.
     * 
     * @param cdLiberacaoLoteProcesso the value of field
     * 'cdLiberacaoLoteProcesso'.
     */
    public void setCdLiberacaoLoteProcesso(int cdLiberacaoLoteProcesso)
    {
        this._cdLiberacaoLoteProcesso = cdLiberacaoLoteProcesso;
        this._has_cdLiberacaoLoteProcesso = true;
    } //-- void setCdLiberacaoLoteProcesso(int) 

    /**
     * Sets the value of field 'cdLocalEmissao'.
     * 
     * @param cdLocalEmissao the value of field 'cdLocalEmissao'.
     */
    public void setCdLocalEmissao(int cdLocalEmissao)
    {
        this._cdLocalEmissao = cdLocalEmissao;
        this._has_cdLocalEmissao = true;
    } //-- void setCdLocalEmissao(int) 

    /**
     * Sets the value of field 'cdManutencaoBaseRecadastro'.
     * 
     * @param cdManutencaoBaseRecadastro the value of field
     * 'cdManutencaoBaseRecadastro'.
     */
    public void setCdManutencaoBaseRecadastro(int cdManutencaoBaseRecadastro)
    {
        this._cdManutencaoBaseRecadastro = cdManutencaoBaseRecadastro;
        this._has_cdManutencaoBaseRecadastro = true;
    } //-- void setCdManutencaoBaseRecadastro(int) 

    /**
     * Sets the value of field 'cdMeioPagamentoCredito'.
     * 
     * @param cdMeioPagamentoCredito the value of field
     * 'cdMeioPagamentoCredito'.
     */
    public void setCdMeioPagamentoCredito(int cdMeioPagamentoCredito)
    {
        this._cdMeioPagamentoCredito = cdMeioPagamentoCredito;
        this._has_cdMeioPagamentoCredito = true;
    } //-- void setCdMeioPagamentoCredito(int) 

    /**
     * Sets the value of field 'cdMeioPagamentoDebito'.
     * 
     * @param cdMeioPagamentoDebito the value of field
     * 'cdMeioPagamentoDebito'.
     */
    public void setCdMeioPagamentoDebito(int cdMeioPagamentoDebito)
    {
        this._cdMeioPagamentoDebito = cdMeioPagamentoDebito;
        this._has_cdMeioPagamentoDebito = true;
    } //-- void setCdMeioPagamentoDebito(int) 

    /**
     * Sets the value of field 'cdMensagemRecadastroMidia'.
     * 
     * @param cdMensagemRecadastroMidia the value of field
     * 'cdMensagemRecadastroMidia'.
     */
    public void setCdMensagemRecadastroMidia(int cdMensagemRecadastroMidia)
    {
        this._cdMensagemRecadastroMidia = cdMensagemRecadastroMidia;
        this._has_cdMensagemRecadastroMidia = true;
    } //-- void setCdMensagemRecadastroMidia(int) 

    /**
     * Sets the value of field 'cdMidiaDisponivel'.
     * 
     * @param cdMidiaDisponivel the value of field
     * 'cdMidiaDisponivel'.
     */
    public void setCdMidiaDisponivel(int cdMidiaDisponivel)
    {
        this._cdMidiaDisponivel = cdMidiaDisponivel;
        this._has_cdMidiaDisponivel = true;
    } //-- void setCdMidiaDisponivel(int) 

    /**
     * Sets the value of field 'cdMidiaMensagemRecadastro'.
     * 
     * @param cdMidiaMensagemRecadastro the value of field
     * 'cdMidiaMensagemRecadastro'.
     */
    public void setCdMidiaMensagemRecadastro(int cdMidiaMensagemRecadastro)
    {
        this._cdMidiaMensagemRecadastro = cdMidiaMensagemRecadastro;
        this._has_cdMidiaMensagemRecadastro = true;
    } //-- void setCdMidiaMensagemRecadastro(int) 

    /**
     * Sets the value of field 'cdMomentoAvisoRacadastro'.
     * 
     * @param cdMomentoAvisoRacadastro the value of field
     * 'cdMomentoAvisoRacadastro'.
     */
    public void setCdMomentoAvisoRacadastro(int cdMomentoAvisoRacadastro)
    {
        this._cdMomentoAvisoRacadastro = cdMomentoAvisoRacadastro;
        this._has_cdMomentoAvisoRacadastro = true;
    } //-- void setCdMomentoAvisoRacadastro(int) 

    /**
     * Sets the value of field 'cdMomentoCreditoEfetivacao'.
     * 
     * @param cdMomentoCreditoEfetivacao the value of field
     * 'cdMomentoCreditoEfetivacao'.
     */
    public void setCdMomentoCreditoEfetivacao(int cdMomentoCreditoEfetivacao)
    {
        this._cdMomentoCreditoEfetivacao = cdMomentoCreditoEfetivacao;
        this._has_cdMomentoCreditoEfetivacao = true;
    } //-- void setCdMomentoCreditoEfetivacao(int) 

    /**
     * Sets the value of field 'cdMomentoDebitoPagamento'.
     * 
     * @param cdMomentoDebitoPagamento the value of field
     * 'cdMomentoDebitoPagamento'.
     */
    public void setCdMomentoDebitoPagamento(int cdMomentoDebitoPagamento)
    {
        this._cdMomentoDebitoPagamento = cdMomentoDebitoPagamento;
        this._has_cdMomentoDebitoPagamento = true;
    } //-- void setCdMomentoDebitoPagamento(int) 

    /**
     * Sets the value of field 'cdMomentoFormularioRecadastro'.
     * 
     * @param cdMomentoFormularioRecadastro the value of field
     * 'cdMomentoFormularioRecadastro'.
     */
    public void setCdMomentoFormularioRecadastro(int cdMomentoFormularioRecadastro)
    {
        this._cdMomentoFormularioRecadastro = cdMomentoFormularioRecadastro;
        this._has_cdMomentoFormularioRecadastro = true;
    } //-- void setCdMomentoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdMomentoProcessamentoPagamento'.
     * 
     * @param cdMomentoProcessamentoPagamento the value of field
     * 'cdMomentoProcessamentoPagamento'.
     */
    public void setCdMomentoProcessamentoPagamento(int cdMomentoProcessamentoPagamento)
    {
        this._cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
        this._has_cdMomentoProcessamentoPagamento = true;
    } //-- void setCdMomentoProcessamentoPagamento(int) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdPagamentoNaoUtilizado'.
     * 
     * @param cdPagamentoNaoUtilizado the value of field
     * 'cdPagamentoNaoUtilizado'.
     */
    public void setCdPagamentoNaoUtilizado(int cdPagamentoNaoUtilizado)
    {
        this._cdPagamentoNaoUtilizado = cdPagamentoNaoUtilizado;
        this._has_cdPagamentoNaoUtilizado = true;
    } //-- void setCdPagamentoNaoUtilizado(int) 

    /**
     * Sets the value of field 'cdParametro'.
     * 
     * @param cdParametro the value of field 'cdParametro'.
     */
    public void setCdParametro(int cdParametro)
    {
        this._cdParametro = cdParametro;
        this._has_cdParametro = true;
    } //-- void setCdParametro(int) 

    /**
     * Sets the value of field 'cdPercentualMaximoInconLote'.
     * 
     * @param cdPercentualMaximoInconLote the value of field
     * 'cdPercentualMaximoInconLote'.
     */
    public void setCdPercentualMaximoInconLote(int cdPercentualMaximoInconLote)
    {
        this._cdPercentualMaximoInconLote = cdPercentualMaximoInconLote;
        this._has_cdPercentualMaximoInconLote = true;
    } //-- void setCdPercentualMaximoInconLote(int) 

    /**
     * Sets the value of field 'cdPerdcComprovante'.
     * 
     * @param cdPerdcComprovante the value of field
     * 'cdPerdcComprovante'.
     */
    public void setCdPerdcComprovante(int cdPerdcComprovante)
    {
        this._cdPerdcComprovante = cdPerdcComprovante;
        this._has_cdPerdcComprovante = true;
    } //-- void setCdPerdcComprovante(int) 

    /**
     * Sets the value of field 'cdPerdcConsultaVeiculo'.
     * 
     * @param cdPerdcConsultaVeiculo the value of field
     * 'cdPerdcConsultaVeiculo'.
     */
    public void setCdPerdcConsultaVeiculo(int cdPerdcConsultaVeiculo)
    {
        this._cdPerdcConsultaVeiculo = cdPerdcConsultaVeiculo;
        this._has_cdPerdcConsultaVeiculo = true;
    } //-- void setCdPerdcConsultaVeiculo(int) 

    /**
     * Sets the value of field 'cdPerdcEnvioRemessa'.
     * 
     * @param cdPerdcEnvioRemessa the value of field
     * 'cdPerdcEnvioRemessa'.
     */
    public void setCdPerdcEnvioRemessa(int cdPerdcEnvioRemessa)
    {
        this._cdPerdcEnvioRemessa = cdPerdcEnvioRemessa;
        this._has_cdPerdcEnvioRemessa = true;
    } //-- void setCdPerdcEnvioRemessa(int) 

    /**
     * Sets the value of field 'cdPerdcManutencaoProcd'.
     * 
     * @param cdPerdcManutencaoProcd the value of field
     * 'cdPerdcManutencaoProcd'.
     */
    public void setCdPerdcManutencaoProcd(int cdPerdcManutencaoProcd)
    {
        this._cdPerdcManutencaoProcd = cdPerdcManutencaoProcd;
        this._has_cdPerdcManutencaoProcd = true;
    } //-- void setCdPerdcManutencaoProcd(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeAviso'.
     * 
     * @param cdPeriodicidadeAviso the value of field
     * 'cdPeriodicidadeAviso'.
     */
    public void setCdPeriodicidadeAviso(int cdPeriodicidadeAviso)
    {
        this._cdPeriodicidadeAviso = cdPeriodicidadeAviso;
        this._has_cdPeriodicidadeAviso = true;
    } //-- void setCdPeriodicidadeAviso(int) 

    /**
     * Sets the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @param cdPermissaoDebitoOnline the value of field
     * 'cdPermissaoDebitoOnline'.
     */
    public void setCdPermissaoDebitoOnline(int cdPermissaoDebitoOnline)
    {
        this._cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
        this._has_cdPermissaoDebitoOnline = true;
    } //-- void setCdPermissaoDebitoOnline(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     * 
     * @param cdPreenchimentoLancamentoPersonalizado the value of
     * field 'cdPreenchimentoLancamentoPersonalizado'.
     */
    public void setCdPreenchimentoLancamentoPersonalizado(int cdPreenchimentoLancamentoPersonalizado)
    {
        this._cdPreenchimentoLancamentoPersonalizado = cdPreenchimentoLancamentoPersonalizado;
        this._has_cdPreenchimentoLancamentoPersonalizado = true;
    } //-- void setCdPreenchimentoLancamentoPersonalizado(int) 

    /**
     * Sets the value of field 'cdPrincipalEnquaRecadastro'.
     * 
     * @param cdPrincipalEnquaRecadastro the value of field
     * 'cdPrincipalEnquaRecadastro'.
     */
    public void setCdPrincipalEnquaRecadastro(int cdPrincipalEnquaRecadastro)
    {
        this._cdPrincipalEnquaRecadastro = cdPrincipalEnquaRecadastro;
        this._has_cdPrincipalEnquaRecadastro = true;
    } //-- void setCdPrincipalEnquaRecadastro(int) 

    /**
     * Sets the value of field 'cdPrioridadeEfetivacaoPagamento'.
     * 
     * @param cdPrioridadeEfetivacaoPagamento the value of field
     * 'cdPrioridadeEfetivacaoPagamento'.
     */
    public void setCdPrioridadeEfetivacaoPagamento(int cdPrioridadeEfetivacaoPagamento)
    {
        this._cdPrioridadeEfetivacaoPagamento = cdPrioridadeEfetivacaoPagamento;
        this._has_cdPrioridadeEfetivacaoPagamento = true;
    } //-- void setCdPrioridadeEfetivacaoPagamento(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaroRelacionado'.
     * 
     * @param cdProdutoOperacaroRelacionado the value of field
     * 'cdProdutoOperacaroRelacionado'.
     */
    public void setCdProdutoOperacaroRelacionado(int cdProdutoOperacaroRelacionado)
    {
        this._cdProdutoOperacaroRelacionado = cdProdutoOperacaroRelacionado;
        this._has_cdProdutoOperacaroRelacionado = true;
    } //-- void setCdProdutoOperacaroRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRastreabilidadeNotaFiscal'.
     * 
     * @param cdRastreabilidadeNotaFiscal the value of field
     * 'cdRastreabilidadeNotaFiscal'.
     */
    public void setCdRastreabilidadeNotaFiscal(int cdRastreabilidadeNotaFiscal)
    {
        this._cdRastreabilidadeNotaFiscal = cdRastreabilidadeNotaFiscal;
        this._has_cdRastreabilidadeNotaFiscal = true;
    } //-- void setCdRastreabilidadeNotaFiscal(int) 

    /**
     * Sets the value of field 'cdRastreabilidadeTituloTerceiro'.
     * 
     * @param cdRastreabilidadeTituloTerceiro the value of field
     * 'cdRastreabilidadeTituloTerceiro'.
     */
    public void setCdRastreabilidadeTituloTerceiro(int cdRastreabilidadeTituloTerceiro)
    {
        this._cdRastreabilidadeTituloTerceiro = cdRastreabilidadeTituloTerceiro;
        this._has_cdRastreabilidadeTituloTerceiro = true;
    } //-- void setCdRastreabilidadeTituloTerceiro(int) 

    /**
     * Sets the value of field 'cdRejeicaoAgendaLote'.
     * 
     * @param cdRejeicaoAgendaLote the value of field
     * 'cdRejeicaoAgendaLote'.
     */
    public void setCdRejeicaoAgendaLote(int cdRejeicaoAgendaLote)
    {
        this._cdRejeicaoAgendaLote = cdRejeicaoAgendaLote;
        this._has_cdRejeicaoAgendaLote = true;
    } //-- void setCdRejeicaoAgendaLote(int) 

    /**
     * Sets the value of field 'cdRejeicaoEfetivacaoLote'.
     * 
     * @param cdRejeicaoEfetivacaoLote the value of field
     * 'cdRejeicaoEfetivacaoLote'.
     */
    public void setCdRejeicaoEfetivacaoLote(int cdRejeicaoEfetivacaoLote)
    {
        this._cdRejeicaoEfetivacaoLote = cdRejeicaoEfetivacaoLote;
        this._has_cdRejeicaoEfetivacaoLote = true;
    } //-- void setCdRejeicaoEfetivacaoLote(int) 

    /**
     * Sets the value of field 'cdRejeicaoLote'.
     * 
     * @param cdRejeicaoLote the value of field 'cdRejeicaoLote'.
     */
    public void setCdRejeicaoLote(int cdRejeicaoLote)
    {
        this._cdRejeicaoLote = cdRejeicaoLote;
        this._has_cdRejeicaoLote = true;
    } //-- void setCdRejeicaoLote(int) 

    /**
     * Sets the value of field 'cdSequenciaEnderecoPessoa'.
     * 
     * @param cdSequenciaEnderecoPessoa the value of field
     * 'cdSequenciaEnderecoPessoa'.
     */
    public void setCdSequenciaEnderecoPessoa(int cdSequenciaEnderecoPessoa)
    {
        this._cdSequenciaEnderecoPessoa = cdSequenciaEnderecoPessoa;
        this._has_cdSequenciaEnderecoPessoa = true;
    } //-- void setCdSequenciaEnderecoPessoa(int) 

    /**
     * Sets the value of field 'cdTipoCargaRecadastro'.
     * 
     * @param cdTipoCargaRecadastro the value of field
     * 'cdTipoCargaRecadastro'.
     */
    public void setCdTipoCargaRecadastro(int cdTipoCargaRecadastro)
    {
        this._cdTipoCargaRecadastro = cdTipoCargaRecadastro;
        this._has_cdTipoCargaRecadastro = true;
    } //-- void setCdTipoCargaRecadastro(int) 

    /**
     * Sets the value of field 'cdTipoCataoSalario'.
     * 
     * @param cdTipoCataoSalario the value of field
     * 'cdTipoCataoSalario'.
     */
    public void setCdTipoCataoSalario(int cdTipoCataoSalario)
    {
        this._cdTipoCataoSalario = cdTipoCataoSalario;
        this._has_cdTipoCataoSalario = true;
    } //-- void setCdTipoCataoSalario(int) 

    /**
     * Sets the value of field 'cdTipoConsistenciaLista'.
     * 
     * @param cdTipoConsistenciaLista the value of field
     * 'cdTipoConsistenciaLista'.
     */
    public void setCdTipoConsistenciaLista(int cdTipoConsistenciaLista)
    {
        this._cdTipoConsistenciaLista = cdTipoConsistenciaLista;
        this._has_cdTipoConsistenciaLista = true;
    } //-- void setCdTipoConsistenciaLista(int) 

    /**
     * Sets the value of field 'cdTipoConsultaComprovante'.
     * 
     * @param cdTipoConsultaComprovante the value of field
     * 'cdTipoConsultaComprovante'.
     */
    public void setCdTipoConsultaComprovante(int cdTipoConsultaComprovante)
    {
        this._cdTipoConsultaComprovante = cdTipoConsultaComprovante;
        this._has_cdTipoConsultaComprovante = true;
    } //-- void setCdTipoConsultaComprovante(int) 

    /**
     * Sets the value of field 'cdTipoContaFavorecido'.
     * 
     * @param cdTipoContaFavorecido the value of field
     * 'cdTipoContaFavorecido'.
     */
    public void setCdTipoContaFavorecido(int cdTipoContaFavorecido)
    {
        this._cdTipoContaFavorecido = cdTipoContaFavorecido;
        this._has_cdTipoContaFavorecido = true;
    } //-- void setCdTipoContaFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoDataFloat'.
     * 
     * @param cdTipoDataFloat the value of field 'cdTipoDataFloat'.
     */
    public void setCdTipoDataFloat(int cdTipoDataFloat)
    {
        this._cdTipoDataFloat = cdTipoDataFloat;
        this._has_cdTipoDataFloat = true;
    } //-- void setCdTipoDataFloat(int) 

    /**
     * Sets the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @param cdTipoDivergenciaVeiculo the value of field
     * 'cdTipoDivergenciaVeiculo'.
     */
    public void setCdTipoDivergenciaVeiculo(int cdTipoDivergenciaVeiculo)
    {
        this._cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
        this._has_cdTipoDivergenciaVeiculo = true;
    } //-- void setCdTipoDivergenciaVeiculo(int) 

    /**
     * Sets the value of field 'cdTipoEnderecoPessoa'.
     * 
     * @param cdTipoEnderecoPessoa the value of field
     * 'cdTipoEnderecoPessoa'.
     */
    public void setCdTipoEnderecoPessoa(int cdTipoEnderecoPessoa)
    {
        this._cdTipoEnderecoPessoa = cdTipoEnderecoPessoa;
        this._has_cdTipoEnderecoPessoa = true;
    } //-- void setCdTipoEnderecoPessoa(int) 

    /**
     * Sets the value of field 'cdTipoFormacaoLista'.
     * 
     * @param cdTipoFormacaoLista the value of field
     * 'cdTipoFormacaoLista'.
     */
    public void setCdTipoFormacaoLista(int cdTipoFormacaoLista)
    {
        this._cdTipoFormacaoLista = cdTipoFormacaoLista;
        this._has_cdTipoFormacaoLista = true;
    } //-- void setCdTipoFormacaoLista(int) 

    /**
     * Sets the value of field 'cdTipoIdBeneficio'.
     * 
     * @param cdTipoIdBeneficio the value of field
     * 'cdTipoIdBeneficio'.
     */
    public void setCdTipoIdBeneficio(int cdTipoIdBeneficio)
    {
        this._cdTipoIdBeneficio = cdTipoIdBeneficio;
        this._has_cdTipoIdBeneficio = true;
    } //-- void setCdTipoIdBeneficio(int) 

    /**
     * Sets the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @param cdTipoIsncricaoFavorecido the value of field
     * 'cdTipoIsncricaoFavorecido'.
     */
    public void setCdTipoIsncricaoFavorecido(int cdTipoIsncricaoFavorecido)
    {
        this._cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
        this._has_cdTipoIsncricaoFavorecido = true;
    } //-- void setCdTipoIsncricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdTituloDdaRetorno'.
     * 
     * @param cdTituloDdaRetorno the value of field
     * 'cdTituloDdaRetorno'.
     */
    public void setCdTituloDdaRetorno(int cdTituloDdaRetorno)
    {
        this._cdTituloDdaRetorno = cdTituloDdaRetorno;
        this._has_cdTituloDdaRetorno = true;
    } //-- void setCdTituloDdaRetorno(int) 

    /**
     * Sets the value of field 'cdUsuarioExterno'.
     * 
     * @param cdUsuarioExterno the value of field 'cdUsuarioExterno'
     */
    public void setCdUsuarioExterno(java.lang.String cdUsuarioExterno)
    {
        this._cdUsuarioExterno = cdUsuarioExterno;
    } //-- void setCdUsuarioExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUtilizacaoFavorecidoControle'.
     * 
     * @param cdUtilizacaoFavorecidoControle the value of field
     * 'cdUtilizacaoFavorecidoControle'.
     */
    public void setCdUtilizacaoFavorecidoControle(int cdUtilizacaoFavorecidoControle)
    {
        this._cdUtilizacaoFavorecidoControle = cdUtilizacaoFavorecidoControle;
        this._has_cdUtilizacaoFavorecidoControle = true;
    } //-- void setCdUtilizacaoFavorecidoControle(int) 

    /**
     * Sets the value of field 'cdValidacaoNomeFavorecido'.
     * 
     * @param cdValidacaoNomeFavorecido the value of field
     * 'cdValidacaoNomeFavorecido'.
     */
    public void setCdValidacaoNomeFavorecido(int cdValidacaoNomeFavorecido)
    {
        this._cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
        this._has_cdValidacaoNomeFavorecido = true;
    } //-- void setCdValidacaoNomeFavorecido(int) 

    /**
     * Sets the value of field 'cdusuario'.
     * 
     * @param cdusuario the value of field 'cdusuario'.
     */
    public void setCdusuario(java.lang.String cdusuario)
    {
        this._cdusuario = cdusuario;
    } //-- void setCdusuario(java.lang.String) 

    /**
     * Sets the value of field 'cindcdFantsRepas'.
     * 
     * @param cindcdFantsRepas the value of field 'cindcdFantsRepas'
     */
    public void setCindcdFantsRepas(int cindcdFantsRepas)
    {
        this._cindcdFantsRepas = cindcdFantsRepas;
        this._has_cindcdFantsRepas = true;
    } //-- void setCindcdFantsRepas(int) 

    /**
     * Sets the value of field 'dsAreaReservada'.
     * 
     * @param dsAreaReservada the value of field 'dsAreaReservada'.
     */
    public void setDsAreaReservada(java.lang.String dsAreaReservada)
    {
        this._dsAreaReservada = dsAreaReservada;
    } //-- void setDsAreaReservada(java.lang.String) 

    /**
     * Sets the value of field 'dtEnquaContaSalario'.
     * 
     * @param dtEnquaContaSalario the value of field
     * 'dtEnquaContaSalario'.
     */
    public void setDtEnquaContaSalario(java.lang.String dtEnquaContaSalario)
    {
        this._dtEnquaContaSalario = dtEnquaContaSalario;
    } //-- void setDtEnquaContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'dtFimAcertoRecadastro'.
     * 
     * @param dtFimAcertoRecadastro the value of field
     * 'dtFimAcertoRecadastro'.
     */
    public void setDtFimAcertoRecadastro(java.lang.String dtFimAcertoRecadastro)
    {
        this._dtFimAcertoRecadastro = dtFimAcertoRecadastro;
    } //-- void setDtFimAcertoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dtFimRecadastroBeneficio'.
     * 
     * @param dtFimRecadastroBeneficio the value of field
     * 'dtFimRecadastroBeneficio'.
     */
    public void setDtFimRecadastroBeneficio(java.lang.String dtFimRecadastroBeneficio)
    {
        this._dtFimRecadastroBeneficio = dtFimRecadastroBeneficio;
    } //-- void setDtFimRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioAcertoRecadastro'.
     * 
     * @param dtInicioAcertoRecadastro the value of field
     * 'dtInicioAcertoRecadastro'.
     */
    public void setDtInicioAcertoRecadastro(java.lang.String dtInicioAcertoRecadastro)
    {
        this._dtInicioAcertoRecadastro = dtInicioAcertoRecadastro;
    } //-- void setDtInicioAcertoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioBloqueioPapeleta'.
     * 
     * @param dtInicioBloqueioPapeleta the value of field
     * 'dtInicioBloqueioPapeleta'.
     */
    public void setDtInicioBloqueioPapeleta(java.lang.String dtInicioBloqueioPapeleta)
    {
        this._dtInicioBloqueioPapeleta = dtInicioBloqueioPapeleta;
    } //-- void setDtInicioBloqueioPapeleta(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreabilidadeTitulo'.
     * 
     * @param dtInicioRastreabilidadeTitulo the value of field
     * 'dtInicioRastreabilidadeTitulo'.
     */
    public void setDtInicioRastreabilidadeTitulo(java.lang.String dtInicioRastreabilidadeTitulo)
    {
        this._dtInicioRastreabilidadeTitulo = dtInicioRastreabilidadeTitulo;
    } //-- void setDtInicioRastreabilidadeTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRecadastroBeneficio'.
     * 
     * @param dtInicioRecadastroBeneficio the value of field
     * 'dtInicioRecadastroBeneficio'.
     */
    public void setDtInicioRecadastroBeneficio(java.lang.String dtInicioRecadastroBeneficio)
    {
        this._dtInicioRecadastroBeneficio = dtInicioRecadastroBeneficio;
    } //-- void setDtInicioRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtLimiteVinculoCarga'.
     * 
     * @param dtLimiteVinculoCarga the value of field
     * 'dtLimiteVinculoCarga'.
     */
    public void setDtLimiteVinculoCarga(java.lang.String dtLimiteVinculoCarga)
    {
        this._dtLimiteVinculoCarga = dtLimiteVinculoCarga;
    } //-- void setDtLimiteVinculoCarga(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxo'.
     * 
     * @param nmOperacaoFluxo the value of field 'nmOperacaoFluxo'.
     */
    public void setNmOperacaoFluxo(java.lang.String nmOperacaoFluxo)
    {
        this._nmOperacaoFluxo = nmOperacaoFluxo;
    } //-- void setNmOperacaoFluxo(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaCotratoNegocio'.
     * 
     * @param nrSequenciaCotratoNegocio the value of field
     * 'nrSequenciaCotratoNegocio'.
     */
    public void setNrSequenciaCotratoNegocio(long nrSequenciaCotratoNegocio)
    {
        this._nrSequenciaCotratoNegocio = nrSequenciaCotratoNegocio;
        this._has_nrSequenciaCotratoNegocio = true;
    } //-- void setNrSequenciaCotratoNegocio(long) 

    /**
     * Sets the value of field 'qtAntecedencia'.
     * 
     * @param qtAntecedencia the value of field 'qtAntecedencia'.
     */
    public void setQtAntecedencia(int qtAntecedencia)
    {
        this._qtAntecedencia = qtAntecedencia;
        this._has_qtAntecedencia = true;
    } //-- void setQtAntecedencia(int) 

    /**
     * Sets the value of field 'qtAnteriorVencimentoComprovado'.
     * 
     * @param qtAnteriorVencimentoComprovado the value of field
     * 'qtAnteriorVencimentoComprovado'.
     */
    public void setQtAnteriorVencimentoComprovado(int qtAnteriorVencimentoComprovado)
    {
        this._qtAnteriorVencimentoComprovado = qtAnteriorVencimentoComprovado;
        this._has_qtAnteriorVencimentoComprovado = true;
    } //-- void setQtAnteriorVencimentoComprovado(int) 

    /**
     * Sets the value of field 'qtDiaExpiracao'.
     * 
     * @param qtDiaExpiracao the value of field 'qtDiaExpiracao'.
     */
    public void setQtDiaExpiracao(int qtDiaExpiracao)
    {
        this._qtDiaExpiracao = qtDiaExpiracao;
        this._has_qtDiaExpiracao = true;
    } //-- void setQtDiaExpiracao(int) 

    /**
     * Sets the value of field 'qtDiaInatividadeFavorecido'.
     * 
     * @param qtDiaInatividadeFavorecido the value of field
     * 'qtDiaInatividadeFavorecido'.
     */
    public void setQtDiaInatividadeFavorecido(int qtDiaInatividadeFavorecido)
    {
        this._qtDiaInatividadeFavorecido = qtDiaInatividadeFavorecido;
        this._has_qtDiaInatividadeFavorecido = true;
    } //-- void setQtDiaInatividadeFavorecido(int) 

    /**
     * Sets the value of field 'qtDiaRepiqConsulta'.
     * 
     * @param qtDiaRepiqConsulta the value of field
     * 'qtDiaRepiqConsulta'.
     */
    public void setQtDiaRepiqConsulta(int qtDiaRepiqConsulta)
    {
        this._qtDiaRepiqConsulta = qtDiaRepiqConsulta;
        this._has_qtDiaRepiqConsulta = true;
    } //-- void setQtDiaRepiqConsulta(int) 

    /**
     * Sets the value of field 'qtDiaUtilPgto'.
     * 
     * @param qtDiaUtilPgto the value of field 'qtDiaUtilPgto'.
     */
    public void setQtDiaUtilPgto(int qtDiaUtilPgto)
    {
        this._qtDiaUtilPgto = qtDiaUtilPgto;
        this._has_qtDiaUtilPgto = true;
    } //-- void setQtDiaUtilPgto(int) 

    /**
     * Sets the value of field 'qtEtapasRecadastroBeneficio'.
     * 
     * @param qtEtapasRecadastroBeneficio the value of field
     * 'qtEtapasRecadastroBeneficio'.
     */
    public void setQtEtapasRecadastroBeneficio(int qtEtapasRecadastroBeneficio)
    {
        this._qtEtapasRecadastroBeneficio = qtEtapasRecadastroBeneficio;
        this._has_qtEtapasRecadastroBeneficio = true;
    } //-- void setQtEtapasRecadastroBeneficio(int) 

    /**
     * Sets the value of field 'qtFaseRecadastroBeneficio'.
     * 
     * @param qtFaseRecadastroBeneficio the value of field
     * 'qtFaseRecadastroBeneficio'.
     */
    public void setQtFaseRecadastroBeneficio(int qtFaseRecadastroBeneficio)
    {
        this._qtFaseRecadastroBeneficio = qtFaseRecadastroBeneficio;
        this._has_qtFaseRecadastroBeneficio = true;
    } //-- void setQtFaseRecadastroBeneficio(int) 

    /**
     * Sets the value of field 'qtLimiteLinha'.
     * 
     * @param qtLimiteLinha the value of field 'qtLimiteLinha'.
     */
    public void setQtLimiteLinha(int qtLimiteLinha)
    {
        this._qtLimiteLinha = qtLimiteLinha;
        this._has_qtLimiteLinha = true;
    } //-- void setQtLimiteLinha(int) 

    /**
     * Sets the value of field 'qtLimiteSolicitacaoCatao'.
     * 
     * @param qtLimiteSolicitacaoCatao the value of field
     * 'qtLimiteSolicitacaoCatao'.
     */
    public void setQtLimiteSolicitacaoCatao(int qtLimiteSolicitacaoCatao)
    {
        this._qtLimiteSolicitacaoCatao = qtLimiteSolicitacaoCatao;
        this._has_qtLimiteSolicitacaoCatao = true;
    } //-- void setQtLimiteSolicitacaoCatao(int) 

    /**
     * Sets the value of field 'qtMaximaInconLote'.
     * 
     * @param qtMaximaInconLote the value of field
     * 'qtMaximaInconLote'.
     */
    public void setQtMaximaInconLote(int qtMaximaInconLote)
    {
        this._qtMaximaInconLote = qtMaximaInconLote;
        this._has_qtMaximaInconLote = true;
    } //-- void setQtMaximaInconLote(int) 

    /**
     * Sets the value of field 'qtMaximaTituloVencido'.
     * 
     * @param qtMaximaTituloVencido the value of field
     * 'qtMaximaTituloVencido'.
     */
    public void setQtMaximaTituloVencido(int qtMaximaTituloVencido)
    {
        this._qtMaximaTituloVencido = qtMaximaTituloVencido;
        this._has_qtMaximaTituloVencido = true;
    } //-- void setQtMaximaTituloVencido(int) 

    /**
     * Sets the value of field 'qtMesComprovante'.
     * 
     * @param qtMesComprovante the value of field 'qtMesComprovante'
     */
    public void setQtMesComprovante(int qtMesComprovante)
    {
        this._qtMesComprovante = qtMesComprovante;
        this._has_qtMesComprovante = true;
    } //-- void setQtMesComprovante(int) 

    /**
     * Sets the value of field 'qtMesEtapaRecadastro'.
     * 
     * @param qtMesEtapaRecadastro the value of field
     * 'qtMesEtapaRecadastro'.
     */
    public void setQtMesEtapaRecadastro(int qtMesEtapaRecadastro)
    {
        this._qtMesEtapaRecadastro = qtMesEtapaRecadastro;
        this._has_qtMesEtapaRecadastro = true;
    } //-- void setQtMesEtapaRecadastro(int) 

    /**
     * Sets the value of field 'qtMesFaseRecadastro'.
     * 
     * @param qtMesFaseRecadastro the value of field
     * 'qtMesFaseRecadastro'.
     */
    public void setQtMesFaseRecadastro(int qtMesFaseRecadastro)
    {
        this._qtMesFaseRecadastro = qtMesFaseRecadastro;
        this._has_qtMesFaseRecadastro = true;
    } //-- void setQtMesFaseRecadastro(int) 

    /**
     * Sets the value of field 'qtViaAviso'.
     * 
     * @param qtViaAviso the value of field 'qtViaAviso'.
     */
    public void setQtViaAviso(int qtViaAviso)
    {
        this._qtViaAviso = qtViaAviso;
        this._has_qtViaAviso = true;
    } //-- void setQtViaAviso(int) 

    /**
     * Sets the value of field 'qtViaCobranca'.
     * 
     * @param qtViaCobranca the value of field 'qtViaCobranca'.
     */
    public void setQtViaCobranca(int qtViaCobranca)
    {
        this._qtViaCobranca = qtViaCobranca;
        this._has_qtViaCobranca = true;
    } //-- void setQtViaCobranca(int) 

    /**
     * Sets the value of field 'qtViaComprovante'.
     * 
     * @param qtViaComprovante the value of field 'qtViaComprovante'
     */
    public void setQtViaComprovante(int qtViaComprovante)
    {
        this._qtViaComprovante = qtViaComprovante;
        this._has_qtViaComprovante = true;
    } //-- void setQtViaComprovante(int) 

    /**
     * Sets the value of field 'vlFavorecidoNaoCadastro'.
     * 
     * @param vlFavorecidoNaoCadastro the value of field
     * 'vlFavorecidoNaoCadastro'.
     */
    public void setVlFavorecidoNaoCadastro(double vlFavorecidoNaoCadastro)
    {
        this._vlFavorecidoNaoCadastro = vlFavorecidoNaoCadastro;
        this._has_vlFavorecidoNaoCadastro = true;
    } //-- void setVlFavorecidoNaoCadastro(double) 

    /**
     * Sets the value of field 'vlLimiteDiaPagamento'.
     * 
     * @param vlLimiteDiaPagamento the value of field
     * 'vlLimiteDiaPagamento'.
     */
    public void setVlLimiteDiaPagamento(double vlLimiteDiaPagamento)
    {
        this._vlLimiteDiaPagamento = vlLimiteDiaPagamento;
        this._has_vlLimiteDiaPagamento = true;
    } //-- void setVlLimiteDiaPagamento(double) 

    /**
     * Sets the value of field 'vlLimiteIndividualPagamento'.
     * 
     * @param vlLimiteIndividualPagamento the value of field
     * 'vlLimiteIndividualPagamento'.
     */
    public void setVlLimiteIndividualPagamento(double vlLimiteIndividualPagamento)
    {
        this._vlLimiteIndividualPagamento = vlLimiteIndividualPagamento;
        this._has_vlLimiteIndividualPagamento = true;
    } //-- void setVlLimiteIndividualPagamento(double) 

    /**
     * Sets the value of field 'vlPercentualDiferencaTolerada'.
     * 
     * @param vlPercentualDiferencaTolerada the value of field
     * 'vlPercentualDiferencaTolerada'.
     */
    public void setVlPercentualDiferencaTolerada(double vlPercentualDiferencaTolerada)
    {
        this._vlPercentualDiferencaTolerada = vlPercentualDiferencaTolerada;
        this._has_vlPercentualDiferencaTolerada = true;
    } //-- void setVlPercentualDiferencaTolerada(double) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarConfiguracaoTipoServModContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.request.AlterarConfiguracaoTipoServModContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.request.AlterarConfiguracaoTipoServModContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.request.AlterarConfiguracaoTipoServModContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfiguracaotiposervmodcontrato.request.AlterarConfiguracaoTipoServModContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
