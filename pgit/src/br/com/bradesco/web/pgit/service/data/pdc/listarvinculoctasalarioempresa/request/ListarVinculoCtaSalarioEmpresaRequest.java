/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarVinculoCtaSalarioEmpresaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarVinculoCtaSalarioEmpresaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridNegocio
     */
    private long _cdPessoaJuridNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridNegocio
     */
    private boolean _has_cdPessoaJuridNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;

    /**
     * Field _cdBancoSalario
     */
    private int _cdBancoSalario = 0;

    /**
     * keeps track of state for field: _cdBancoSalario
     */
    private boolean _has_cdBancoSalario;

    /**
     * Field _cdAgenciaSalario
     */
    private int _cdAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdAgenciaSalario
     */
    private boolean _has_cdAgenciaSalario;

    /**
     * Field _cdDigitoAgenciaSalario
     */
    private int _cdDigitoAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaSalario
     */
    private boolean _has_cdDigitoAgenciaSalario;

    /**
     * Field _cdContaSalario
     */
    private long _cdContaSalario = 0;

    /**
     * keeps track of state for field: _cdContaSalario
     */
    private boolean _has_cdContaSalario;

    /**
     * Field _cdDigitoContaSalario
     */
    private java.lang.String _cdDigitoContaSalario;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarVinculoCtaSalarioEmpresaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.request.ListarVinculoCtaSalarioEmpresaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaSalario
     * 
     */
    public void deleteCdAgenciaSalario()
    {
        this._has_cdAgenciaSalario= false;
    } //-- void deleteCdAgenciaSalario() 

    /**
     * Method deleteCdBancoSalario
     * 
     */
    public void deleteCdBancoSalario()
    {
        this._has_cdBancoSalario= false;
    } //-- void deleteCdBancoSalario() 

    /**
     * Method deleteCdContaSalario
     * 
     */
    public void deleteCdContaSalario()
    {
        this._has_cdContaSalario= false;
    } //-- void deleteCdContaSalario() 

    /**
     * Method deleteCdDigitoAgenciaSalario
     * 
     */
    public void deleteCdDigitoAgenciaSalario()
    {
        this._has_cdDigitoAgenciaSalario= false;
    } //-- void deleteCdDigitoAgenciaSalario() 

    /**
     * Method deleteCdPessoaJuridNegocio
     * 
     */
    public void deleteCdPessoaJuridNegocio()
    {
        this._has_cdPessoaJuridNegocio= false;
    } //-- void deleteCdPessoaJuridNegocio() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaSalario'.
     */
    public int getCdAgenciaSalario()
    {
        return this._cdAgenciaSalario;
    } //-- int getCdAgenciaSalario() 

    /**
     * Returns the value of field 'cdBancoSalario'.
     * 
     * @return int
     * @return the value of field 'cdBancoSalario'.
     */
    public int getCdBancoSalario()
    {
        return this._cdBancoSalario;
    } //-- int getCdBancoSalario() 

    /**
     * Returns the value of field 'cdContaSalario'.
     * 
     * @return long
     * @return the value of field 'cdContaSalario'.
     */
    public long getCdContaSalario()
    {
        return this._cdContaSalario;
    } //-- long getCdContaSalario() 

    /**
     * Returns the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaSalario'.
     */
    public int getCdDigitoAgenciaSalario()
    {
        return this._cdDigitoAgenciaSalario;
    } //-- int getCdDigitoAgenciaSalario() 

    /**
     * Returns the value of field 'cdDigitoContaSalario'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaSalario'.
     */
    public java.lang.String getCdDigitoContaSalario()
    {
        return this._cdDigitoContaSalario;
    } //-- java.lang.String getCdDigitoContaSalario() 

    /**
     * Returns the value of field 'cdPessoaJuridNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridNegocio'.
     */
    public long getCdPessoaJuridNegocio()
    {
        return this._cdPessoaJuridNegocio;
    } //-- long getCdPessoaJuridNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Method hasCdAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaSalario()
    {
        return this._has_cdAgenciaSalario;
    } //-- boolean hasCdAgenciaSalario() 

    /**
     * Method hasCdBancoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoSalario()
    {
        return this._has_cdBancoSalario;
    } //-- boolean hasCdBancoSalario() 

    /**
     * Method hasCdContaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaSalario()
    {
        return this._has_cdContaSalario;
    } //-- boolean hasCdContaSalario() 

    /**
     * Method hasCdDigitoAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaSalario()
    {
        return this._has_cdDigitoAgenciaSalario;
    } //-- boolean hasCdDigitoAgenciaSalario() 

    /**
     * Method hasCdPessoaJuridNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridNegocio()
    {
        return this._has_cdPessoaJuridNegocio;
    } //-- boolean hasCdPessoaJuridNegocio() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaSalario'.
     * 
     * @param cdAgenciaSalario the value of field 'cdAgenciaSalario'
     */
    public void setCdAgenciaSalario(int cdAgenciaSalario)
    {
        this._cdAgenciaSalario = cdAgenciaSalario;
        this._has_cdAgenciaSalario = true;
    } //-- void setCdAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdBancoSalario'.
     * 
     * @param cdBancoSalario the value of field 'cdBancoSalario'.
     */
    public void setCdBancoSalario(int cdBancoSalario)
    {
        this._cdBancoSalario = cdBancoSalario;
        this._has_cdBancoSalario = true;
    } //-- void setCdBancoSalario(int) 

    /**
     * Sets the value of field 'cdContaSalario'.
     * 
     * @param cdContaSalario the value of field 'cdContaSalario'.
     */
    public void setCdContaSalario(long cdContaSalario)
    {
        this._cdContaSalario = cdContaSalario;
        this._has_cdContaSalario = true;
    } //-- void setCdContaSalario(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @param cdDigitoAgenciaSalario the value of field
     * 'cdDigitoAgenciaSalario'.
     */
    public void setCdDigitoAgenciaSalario(int cdDigitoAgenciaSalario)
    {
        this._cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
        this._has_cdDigitoAgenciaSalario = true;
    } //-- void setCdDigitoAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdDigitoContaSalario'.
     * 
     * @param cdDigitoContaSalario the value of field
     * 'cdDigitoContaSalario'.
     */
    public void setCdDigitoContaSalario(java.lang.String cdDigitoContaSalario)
    {
        this._cdDigitoContaSalario = cdDigitoContaSalario;
    } //-- void setCdDigitoContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridNegocio'.
     * 
     * @param cdPessoaJuridNegocio the value of field
     * 'cdPessoaJuridNegocio'.
     */
    public void setCdPessoaJuridNegocio(long cdPessoaJuridNegocio)
    {
        this._cdPessoaJuridNegocio = cdPessoaJuridNegocio;
        this._has_cdPessoaJuridNegocio = true;
    } //-- void setCdPessoaJuridNegocio(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarVinculoCtaSalarioEmpresaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.request.ListarVinculoCtaSalarioEmpresaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.request.ListarVinculoCtaSalarioEmpresaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.request.ListarVinculoCtaSalarioEmpresaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvinculoctasalarioempresa.request.ListarVinculoCtaSalarioEmpresaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
