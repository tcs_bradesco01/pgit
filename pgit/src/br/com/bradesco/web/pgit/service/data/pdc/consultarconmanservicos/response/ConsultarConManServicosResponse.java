/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ConsultarConManServicosResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarConManServicosResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdAcaoNaoVida
     */
    private int _cdAcaoNaoVida = 0;

    /**
     * keeps track of state for field: _cdAcaoNaoVida
     */
    private boolean _has_cdAcaoNaoVida;

    /**
     * Field _dsAcaoNaoVida
     */
    private java.lang.String _dsAcaoNaoVida;

    /**
     * Field _cdAcertoDadosRecadastramento
     */
    private int _cdAcertoDadosRecadastramento = 0;

    /**
     * keeps track of state for field: _cdAcertoDadosRecadastramento
     */
    private boolean _has_cdAcertoDadosRecadastramento;

    /**
     * Field _dsAcertoDadosRecadastramento
     */
    private java.lang.String _dsAcertoDadosRecadastramento;

    /**
     * Field _cdAgendamentoDebitoVeiculo
     */
    private int _cdAgendamentoDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdAgendamentoDebitoVeiculo
     */
    private boolean _has_cdAgendamentoDebitoVeiculo;

    /**
     * Field _dsAgendamentoDebitoVeiculo
     */
    private java.lang.String _dsAgendamentoDebitoVeiculo;

    /**
     * Field _cdAgendamentoPagamentoVencido
     */
    private int _cdAgendamentoPagamentoVencido = 0;

    /**
     * keeps track of state for field: _cdAgendamentoPagamentoVencid
     */
    private boolean _has_cdAgendamentoPagamentoVencido;

    /**
     * Field _dsAgendamentoPagamentoVencido
     */
    private java.lang.String _dsAgendamentoPagamentoVencido;

    /**
     * Field _cdAgendamentoRastreabilidadeFinal
     */
    private int _cdAgendamentoRastreabilidadeFinal = 0;

    /**
     * keeps track of state for field:
     * _cdAgendamentoRastreabilidadeFinal
     */
    private boolean _has_cdAgendamentoRastreabilidadeFinal;

    /**
     * Field _dsAgendamentoRastreabilidadeFinal
     */
    private java.lang.String _dsAgendamentoRastreabilidadeFinal;

    /**
     * Field _cdAgendamentoValorMenor
     */
    private int _cdAgendamentoValorMenor = 0;

    /**
     * keeps track of state for field: _cdAgendamentoValorMenor
     */
    private boolean _has_cdAgendamentoValorMenor;

    /**
     * Field _dsAgendamentoValorMenor
     */
    private java.lang.String _dsAgendamentoValorMenor;

    /**
     * Field _cdAgrupamentoAviso
     */
    private int _cdAgrupamentoAviso = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoAviso
     */
    private boolean _has_cdAgrupamentoAviso;

    /**
     * Field _dsAgrupamentoAviso
     */
    private java.lang.String _dsAgrupamentoAviso;

    /**
     * Field _cdAgrupamentoComprovante
     */
    private int _cdAgrupamentoComprovante = 0;

    /**
     * keeps track of state for field: _cdAgrupamentoComprovante
     */
    private boolean _has_cdAgrupamentoComprovante;

    /**
     * Field _dsAgrupamentoComprovante
     */
    private java.lang.String _dsAgrupamentoComprovante;

    /**
     * Field _cdAgrupamentoFormularioRecadastro
     */
    private int _cdAgrupamentoFormularioRecadastro = 0;

    /**
     * keeps track of state for field:
     * _cdAgrupamentoFormularioRecadastro
     */
    private boolean _has_cdAgrupamentoFormularioRecadastro;

    /**
     * Field _dsAgrupamentoFormularioRecadastro
     */
    private java.lang.String _dsAgrupamentoFormularioRecadastro;

    /**
     * Field _cdAntecipacaoRecadastramentoBeneficiario
     */
    private int _cdAntecipacaoRecadastramentoBeneficiario = 0;

    /**
     * keeps track of state for field:
     * _cdAntecipacaoRecadastramentoBeneficiario
     */
    private boolean _has_cdAntecipacaoRecadastramentoBeneficiario;

    /**
     * Field _dsAntecipacaoRecadastramentoBeneficiario
     */
    private java.lang.String _dsAntecipacaoRecadastramentoBeneficiario;

    /**
     * Field _cdAreaReservada
     */
    private int _cdAreaReservada = 0;

    /**
     * keeps track of state for field: _cdAreaReservada
     */
    private boolean _has_cdAreaReservada;

    /**
     * Field _dsAreaReservada
     */
    private java.lang.String _dsAreaReservada;

    /**
     * Field _cdBaseRecadastramentoBeneficio
     */
    private int _cdBaseRecadastramentoBeneficio = 0;

    /**
     * keeps track of state for field:
     * _cdBaseRecadastramentoBeneficio
     */
    private boolean _has_cdBaseRecadastramentoBeneficio;

    /**
     * Field _dsBaseRecadastramentoBeneficio
     */
    private java.lang.String _dsBaseRecadastramentoBeneficio;

    /**
     * Field _cdBloqueioEmissaoPapeleta
     */
    private int _cdBloqueioEmissaoPapeleta = 0;

    /**
     * keeps track of state for field: _cdBloqueioEmissaoPapeleta
     */
    private boolean _has_cdBloqueioEmissaoPapeleta;

    /**
     * Field _dsBloqueioEmissaoPapeleta
     */
    private java.lang.String _dsBloqueioEmissaoPapeleta;

    /**
     * Field _cdCapturaTituloRegistrado
     */
    private int _cdCapturaTituloRegistrado = 0;

    /**
     * keeps track of state for field: _cdCapturaTituloRegistrado
     */
    private boolean _has_cdCapturaTituloRegistrado;

    /**
     * Field _dsCapturaTituloRegistrado
     */
    private java.lang.String _dsCapturaTituloRegistrado;

    /**
     * Field _cdCobrancaTarifa
     */
    private int _cdCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdCobrancaTarifa
     */
    private boolean _has_cdCobrancaTarifa;

    /**
     * Field _dsCobrancaTarifa
     */
    private java.lang.String _dsCobrancaTarifa;

    /**
     * Field _cdConsultaDebitoVeiculo
     */
    private int _cdConsultaDebitoVeiculo = 0;

    /**
     * keeps track of state for field: _cdConsultaDebitoVeiculo
     */
    private boolean _has_cdConsultaDebitoVeiculo;

    /**
     * Field _dsConsultaDebitoVeiculo
     */
    private java.lang.String _dsConsultaDebitoVeiculo;

    /**
     * Field _cdConsultaEndereco
     */
    private int _cdConsultaEndereco = 0;

    /**
     * keeps track of state for field: _cdConsultaEndereco
     */
    private boolean _has_cdConsultaEndereco;

    /**
     * Field _dsConsultaEndereco
     */
    private java.lang.String _dsConsultaEndereco;

    /**
     * Field _cdConsultaSaldoPagamento
     */
    private int _cdConsultaSaldoPagamento = 0;

    /**
     * keeps track of state for field: _cdConsultaSaldoPagamento
     */
    private boolean _has_cdConsultaSaldoPagamento;

    /**
     * Field _dsConsultaSaldoPagamento
     */
    private java.lang.String _dsConsultaSaldoPagamento;

    /**
     * Field _cdContagemConsultaSaldo
     */
    private int _cdContagemConsultaSaldo = 0;

    /**
     * keeps track of state for field: _cdContagemConsultaSaldo
     */
    private boolean _has_cdContagemConsultaSaldo;

    /**
     * Field _dsContagemConsultaSaldo
     */
    private java.lang.String _dsContagemConsultaSaldo;

    /**
     * Field _cdCreditoNaoUtilizado
     */
    private int _cdCreditoNaoUtilizado = 0;

    /**
     * keeps track of state for field: _cdCreditoNaoUtilizado
     */
    private boolean _has_cdCreditoNaoUtilizado;

    /**
     * Field _dsCreditoNaoUtilizado
     */
    private java.lang.String _dsCreditoNaoUtilizado;

    /**
     * Field _cdCriterioEnquandraBeneficio
     */
    private int _cdCriterioEnquandraBeneficio = 0;

    /**
     * keeps track of state for field: _cdCriterioEnquandraBeneficio
     */
    private boolean _has_cdCriterioEnquandraBeneficio;

    /**
     * Field _dsCriterioEnquandraBeneficio
     */
    private java.lang.String _dsCriterioEnquandraBeneficio;

    /**
     * Field _cdCriterioEnquadraRecadastramento
     */
    private int _cdCriterioEnquadraRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdCriterioEnquadraRecadastramento
     */
    private boolean _has_cdCriterioEnquadraRecadastramento;

    /**
     * Field _dsCriterioEnquadraRecadastramento
     */
    private java.lang.String _dsCriterioEnquadraRecadastramento;

    /**
     * Field _cdCriterioRastreabilidadeTitulo
     */
    private int _cdCriterioRastreabilidadeTitulo = 0;

    /**
     * keeps track of state for field:
     * _cdCriterioRastreabilidadeTitulo
     */
    private boolean _has_cdCriterioRastreabilidadeTitulo;

    /**
     * Field _dsCriterioRastreabilidadeTitulo
     */
    private java.lang.String _dsCriterioRastreabilidadeTitulo;

    /**
     * Field _cdCctciaEspeBeneficio
     */
    private int _cdCctciaEspeBeneficio = 0;

    /**
     * keeps track of state for field: _cdCctciaEspeBeneficio
     */
    private boolean _has_cdCctciaEspeBeneficio;

    /**
     * Field _dsCctciaEspeBeneficio
     */
    private java.lang.String _dsCctciaEspeBeneficio;

    /**
     * Field _cdCctciaIdentificacaoBeneficio
     */
    private int _cdCctciaIdentificacaoBeneficio = 0;

    /**
     * keeps track of state for field:
     * _cdCctciaIdentificacaoBeneficio
     */
    private boolean _has_cdCctciaIdentificacaoBeneficio;

    /**
     * Field _dsCctciaIdentificacaoBeneficio
     */
    private java.lang.String _dsCctciaIdentificacaoBeneficio;

    /**
     * Field _cdCctciaInscricaoFavorecido
     */
    private int _cdCctciaInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdCctciaInscricaoFavorecido
     */
    private boolean _has_cdCctciaInscricaoFavorecido;

    /**
     * Field _dsCctciaInscricaoFavorecido
     */
    private java.lang.String _dsCctciaInscricaoFavorecido;

    /**
     * Field _cdCctciaProprietarioVeculo
     */
    private int _cdCctciaProprietarioVeculo = 0;

    /**
     * keeps track of state for field: _cdCctciaProprietarioVeculo
     */
    private boolean _has_cdCctciaProprietarioVeculo;

    /**
     * Field _dsCctciaProprietarioVeculo
     */
    private java.lang.String _dsCctciaProprietarioVeculo;

    /**
     * Field _cdDisponibilizacaoContaCredito
     */
    private int _cdDisponibilizacaoContaCredito = 0;

    /**
     * keeps track of state for field:
     * _cdDisponibilizacaoContaCredito
     */
    private boolean _has_cdDisponibilizacaoContaCredito;

    /**
     * Field _dsDisponibilizacaoContaCredito
     */
    private java.lang.String _dsDisponibilizacaoContaCredito;

    /**
     * Field _cdDisponibilizacaoDiversoCriterio
     */
    private int _cdDisponibilizacaoDiversoCriterio = 0;

    /**
     * keeps track of state for field:
     * _cdDisponibilizacaoDiversoCriterio
     */
    private boolean _has_cdDisponibilizacaoDiversoCriterio;

    /**
     * Field _dsDisponibilizacaoDiversoCriterio
     */
    private java.lang.String _dsDisponibilizacaoDiversoCriterio;

    /**
     * Field _cdDisponibilizacaoDiversoNao
     */
    private int _cdDisponibilizacaoDiversoNao = 0;

    /**
     * keeps track of state for field: _cdDisponibilizacaoDiversoNao
     */
    private boolean _has_cdDisponibilizacaoDiversoNao;

    /**
     * Field _dsDisponibilizacaoDiversoNao
     */
    private java.lang.String _dsDisponibilizacaoDiversoNao;

    /**
     * Field _cdDisponibilizacaoSalarioCriterio
     */
    private int _cdDisponibilizacaoSalarioCriterio = 0;

    /**
     * keeps track of state for field:
     * _cdDisponibilizacaoSalarioCriterio
     */
    private boolean _has_cdDisponibilizacaoSalarioCriterio;

    /**
     * Field _dsDisponibilizacaoSalarioCriterio
     */
    private java.lang.String _dsDisponibilizacaoSalarioCriterio;

    /**
     * Field _cdDisponibilizacaoSalarioNao
     */
    private int _cdDisponibilizacaoSalarioNao = 0;

    /**
     * keeps track of state for field: _cdDisponibilizacaoSalarioNao
     */
    private boolean _has_cdDisponibilizacaoSalarioNao;

    /**
     * Field _dsDisponibilizacaoSalarioNao
     */
    private java.lang.String _dsDisponibilizacaoSalarioNao;

    /**
     * Field _cdDestinoAviso
     */
    private int _cdDestinoAviso = 0;

    /**
     * keeps track of state for field: _cdDestinoAviso
     */
    private boolean _has_cdDestinoAviso;

    /**
     * Field _dsDestinoAviso
     */
    private java.lang.String _dsDestinoAviso;

    /**
     * Field _cdDestinoComprovante
     */
    private int _cdDestinoComprovante = 0;

    /**
     * keeps track of state for field: _cdDestinoComprovante
     */
    private boolean _has_cdDestinoComprovante;

    /**
     * Field _dsDestinoComprovante
     */
    private java.lang.String _dsDestinoComprovante;

    /**
     * Field _cdDestinoFormularioRecadastramento
     */
    private int _cdDestinoFormularioRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdDestinoFormularioRecadastramento
     */
    private boolean _has_cdDestinoFormularioRecadastramento;

    /**
     * Field _dsDestinoFormularioRecadastramento
     */
    private java.lang.String _dsDestinoFormularioRecadastramento;

    /**
     * Field _cdEnvelopeAberto
     */
    private int _cdEnvelopeAberto = 0;

    /**
     * keeps track of state for field: _cdEnvelopeAberto
     */
    private boolean _has_cdEnvelopeAberto;

    /**
     * Field _dsEnvelopeAberto
     */
    private java.lang.String _dsEnvelopeAberto;

    /**
     * Field _cdFavorecidoConsultaPagamento
     */
    private int _cdFavorecidoConsultaPagamento = 0;

    /**
     * keeps track of state for field: _cdFavorecidoConsultaPagament
     */
    private boolean _has_cdFavorecidoConsultaPagamento;

    /**
     * Field _dsFavorecidoConsultaPagamento
     */
    private java.lang.String _dsFavorecidoConsultaPagamento;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _dsFormaAutorizacaoPagamento
     */
    private java.lang.String _dsFormaAutorizacaoPagamento;

    /**
     * Field _cdFormaEnvioPagamento
     */
    private int _cdFormaEnvioPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaEnvioPagamento
     */
    private boolean _has_cdFormaEnvioPagamento;

    /**
     * Field _dsFormaEnvioPagamento
     */
    private java.lang.String _dsFormaEnvioPagamento;

    /**
     * Field _cdFormaEstornoCredito
     */
    private int _cdFormaEstornoCredito = 0;

    /**
     * keeps track of state for field: _cdFormaEstornoCredito
     */
    private boolean _has_cdFormaEstornoCredito;

    /**
     * Field _dsFormaEstornoCredito
     */
    private java.lang.String _dsFormaEstornoCredito;

    /**
     * Field _cdFormaExpiracaoCredito
     */
    private int _cdFormaExpiracaoCredito = 0;

    /**
     * keeps track of state for field: _cdFormaExpiracaoCredito
     */
    private boolean _has_cdFormaExpiracaoCredito;

    /**
     * Field _dsFormaExpiracaoCredito
     */
    private java.lang.String _dsFormaExpiracaoCredito;

    /**
     * Field _cdFormaManutencao
     */
    private int _cdFormaManutencao = 0;

    /**
     * keeps track of state for field: _cdFormaManutencao
     */
    private boolean _has_cdFormaManutencao;

    /**
     * Field _dsFormaManutencao
     */
    private java.lang.String _dsFormaManutencao;

    /**
     * Field _cdFrasePrecadastrada
     */
    private int _cdFrasePrecadastrada = 0;

    /**
     * keeps track of state for field: _cdFrasePrecadastrada
     */
    private boolean _has_cdFrasePrecadastrada;

    /**
     * Field _dsFrasePrecadastrada
     */
    private java.lang.String _dsFrasePrecadastrada;

    /**
     * Field _cdIndicadorAgendamentoTitulo
     */
    private int _cdIndicadorAgendamentoTitulo = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendamentoTitulo
     */
    private boolean _has_cdIndicadorAgendamentoTitulo;

    /**
     * Field _dsIndicadorAgendamentoTitulo
     */
    private java.lang.String _dsIndicadorAgendamentoTitulo;

    /**
     * Field _cdIndicadorAutorizacaoCliente
     */
    private int _cdIndicadorAutorizacaoCliente = 0;

    /**
     * keeps track of state for field: _cdIndicadorAutorizacaoClient
     */
    private boolean _has_cdIndicadorAutorizacaoCliente;

    /**
     * Field _dsIndicadorAutorizacaoCliente
     */
    private java.lang.String _dsIndicadorAutorizacaoCliente;

    /**
     * Field _cdIndicadorAutorizacaoComplemento
     */
    private int _cdIndicadorAutorizacaoComplemento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorAutorizacaoComplemento
     */
    private boolean _has_cdIndicadorAutorizacaoComplemento;

    /**
     * Field _dsIndicadorAutorizacaoComplemento
     */
    private java.lang.String _dsIndicadorAutorizacaoComplemento;

    /**
     * Field _cdIndicadorCadastroorganizacao
     */
    private int _cdIndicadorCadastroorganizacao = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorCadastroorganizacao
     */
    private boolean _has_cdIndicadorCadastroorganizacao;

    /**
     * Field _dsIndicadorCadastroorganizacao
     */
    private java.lang.String _dsIndicadorCadastroorganizacao;

    /**
     * Field _cdIndicadorCadastroProcurador
     */
    private int _cdIndicadorCadastroProcurador = 0;

    /**
     * keeps track of state for field: _cdIndicadorCadastroProcurado
     */
    private boolean _has_cdIndicadorCadastroProcurador;

    /**
     * Field _dsIndicadorCadastroProcurador
     */
    private java.lang.String _dsIndicadorCadastroProcurador;

    /**
     * Field _cdIndicadorCartaoSalario
     */
    private int _cdIndicadorCartaoSalario = 0;

    /**
     * keeps track of state for field: _cdIndicadorCartaoSalario
     */
    private boolean _has_cdIndicadorCartaoSalario;

    /**
     * Field _dsIndicadorCartaoSalario
     */
    private java.lang.String _dsIndicadorCartaoSalario;

    /**
     * Field _cdIndicadorEconomicoReajuste
     */
    private int _cdIndicadorEconomicoReajuste = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoReajuste
     */
    private boolean _has_cdIndicadorEconomicoReajuste;

    /**
     * Field _dsIndicadorEconomicoReajuste
     */
    private java.lang.String _dsIndicadorEconomicoReajuste;

    /**
     * Field _cdindicadorExpiraCredito
     */
    private int _cdindicadorExpiraCredito = 0;

    /**
     * keeps track of state for field: _cdindicadorExpiraCredito
     */
    private boolean _has_cdindicadorExpiraCredito;

    /**
     * Field _dsindicadorExpiraCredito
     */
    private java.lang.String _dsindicadorExpiraCredito;

    /**
     * Field _cdindicadorLancamentoProgramado
     */
    private int _cdindicadorLancamentoProgramado = 0;

    /**
     * keeps track of state for field:
     * _cdindicadorLancamentoProgramado
     */
    private boolean _has_cdindicadorLancamentoProgramado;

    /**
     * Field _dsindicadorLancamentoProgramado
     */
    private java.lang.String _dsindicadorLancamentoProgramado;

    /**
     * Field _cdIndicadorMensagemPersonalizada
     */
    private int _cdIndicadorMensagemPersonalizada = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorMensagemPersonalizada
     */
    private boolean _has_cdIndicadorMensagemPersonalizada;

    /**
     * Field _dsIndicadorMensagemPersonalizada
     */
    private java.lang.String _dsIndicadorMensagemPersonalizada;

    /**
     * Field _cdLancamentoFuturoCredito
     */
    private int _cdLancamentoFuturoCredito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoCredito
     */
    private boolean _has_cdLancamentoFuturoCredito;

    /**
     * Field _dsLancamentoFuturoCredito
     */
    private java.lang.String _dsLancamentoFuturoCredito;

    /**
     * Field _cdLancamentoFuturoDebito
     */
    private int _cdLancamentoFuturoDebito = 0;

    /**
     * keeps track of state for field: _cdLancamentoFuturoDebito
     */
    private boolean _has_cdLancamentoFuturoDebito;

    /**
     * Field _dsLancamentoFuturoDebito
     */
    private java.lang.String _dsLancamentoFuturoDebito;

    /**
     * Field _cdLiberacaoLoteProcessado
     */
    private int _cdLiberacaoLoteProcessado = 0;

    /**
     * keeps track of state for field: _cdLiberacaoLoteProcessado
     */
    private boolean _has_cdLiberacaoLoteProcessado;

    /**
     * Field _dsLiberacaoLoteProcessado
     */
    private java.lang.String _dsLiberacaoLoteProcessado;

    /**
     * Field _cdManutencaoBaseRecadastramento
     */
    private int _cdManutencaoBaseRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdManutencaoBaseRecadastramento
     */
    private boolean _has_cdManutencaoBaseRecadastramento;

    /**
     * Field _dsManutencaoBaseRecadastramento
     */
    private java.lang.String _dsManutencaoBaseRecadastramento;

    /**
     * Field _cdMeioPagamentoDebito
     */
    private int _cdMeioPagamentoDebito = 0;

    /**
     * keeps track of state for field: _cdMeioPagamentoDebito
     */
    private boolean _has_cdMeioPagamentoDebito;

    /**
     * Field _dsMeioPagamentoDebito
     */
    private java.lang.String _dsMeioPagamentoDebito;

    /**
     * Field _cdMidiaDisponivel
     */
    private int _cdMidiaDisponivel = 0;

    /**
     * keeps track of state for field: _cdMidiaDisponivel
     */
    private boolean _has_cdMidiaDisponivel;

    /**
     * Field _dsMidiaDisponivel
     */
    private java.lang.String _dsMidiaDisponivel;

    /**
     * Field _cdMidiaMensagemRecadastramento
     */
    private int _cdMidiaMensagemRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdMidiaMensagemRecadastramento
     */
    private boolean _has_cdMidiaMensagemRecadastramento;

    /**
     * Field _dsMidiaMensagemRecadastramento
     */
    private java.lang.String _dsMidiaMensagemRecadastramento;

    /**
     * Field _cdMomentoAvisoRecadastramento
     */
    private int _cdMomentoAvisoRecadastramento = 0;

    /**
     * keeps track of state for field: _cdMomentoAvisoRecadastrament
     */
    private boolean _has_cdMomentoAvisoRecadastramento;

    /**
     * Field _dsMomentoAvisoRecadastramento
     */
    private java.lang.String _dsMomentoAvisoRecadastramento;

    /**
     * Field _cdMomentoCreditoEfetivacao
     */
    private int _cdMomentoCreditoEfetivacao = 0;

    /**
     * keeps track of state for field: _cdMomentoCreditoEfetivacao
     */
    private boolean _has_cdMomentoCreditoEfetivacao;

    /**
     * Field _dsMomentoCreditoEfetivacao
     */
    private java.lang.String _dsMomentoCreditoEfetivacao;

    /**
     * Field _cdMomentoDebitoPagamento
     */
    private int _cdMomentoDebitoPagamento = 0;

    /**
     * keeps track of state for field: _cdMomentoDebitoPagamento
     */
    private boolean _has_cdMomentoDebitoPagamento;

    /**
     * Field _dsMomentoDebitoPagamento
     */
    private java.lang.String _dsMomentoDebitoPagamento;

    /**
     * Field _cdMomentoFormularioRecadastramento
     */
    private int _cdMomentoFormularioRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoFormularioRecadastramento
     */
    private boolean _has_cdMomentoFormularioRecadastramento;

    /**
     * Field _dsMomentoFormularioRecadastramento
     */
    private java.lang.String _dsMomentoFormularioRecadastramento;

    /**
     * Field _cdMomentoProcessamentoPagamento
     */
    private int _cdMomentoProcessamentoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdMomentoProcessamentoPagamento
     */
    private boolean _has_cdMomentoProcessamentoPagamento;

    /**
     * Field _dsMomentoProcessamentoPagamento
     */
    private java.lang.String _dsMomentoProcessamentoPagamento;

    /**
     * Field _cdMensagemRecadastramentoMidia
     */
    private int _cdMensagemRecadastramentoMidia = 0;

    /**
     * keeps track of state for field:
     * _cdMensagemRecadastramentoMidia
     */
    private boolean _has_cdMensagemRecadastramentoMidia;

    /**
     * Field _dsMensagemRecadastramentoMidia
     */
    private java.lang.String _dsMensagemRecadastramentoMidia;

    /**
     * Field _cdPermissaoDebitoOnline
     */
    private int _cdPermissaoDebitoOnline = 0;

    /**
     * keeps track of state for field: _cdPermissaoDebitoOnline
     */
    private boolean _has_cdPermissaoDebitoOnline;

    /**
     * Field _dsPermissaoDebitoOnline
     */
    private java.lang.String _dsPermissaoDebitoOnline;

    /**
     * Field _cdNaturezaOperacaoPagamento
     */
    private int _cdNaturezaOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdNaturezaOperacaoPagamento
     */
    private boolean _has_cdNaturezaOperacaoPagamento;

    /**
     * Field _dsNaturezaOperacaoPagamento
     */
    private java.lang.String _dsNaturezaOperacaoPagamento;

    /**
     * Field _cdOutraidentificacaoFavorecido
     */
    private int _cdOutraidentificacaoFavorecido = 0;

    /**
     * keeps track of state for field:
     * _cdOutraidentificacaoFavorecido
     */
    private boolean _has_cdOutraidentificacaoFavorecido;

    /**
     * Field _dsOutraidentificacaoFavorecido
     */
    private java.lang.String _dsOutraidentificacaoFavorecido;

    /**
     * Field _cdPeriodicidadeAviso
     */
    private int _cdPeriodicidadeAviso = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeAviso
     */
    private boolean _has_cdPeriodicidadeAviso;

    /**
     * Field _dsPeriodicidadeAviso
     */
    private java.lang.String _dsPeriodicidadeAviso;

    /**
     * Field _cdPeriodicidadeCobrancaTarifa
     */
    private int _cdPeriodicidadeCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeCobrancaTarif
     */
    private boolean _has_cdPeriodicidadeCobrancaTarifa;

    /**
     * Field _dsPeriodicidadeCobrancaTarifa
     */
    private java.lang.String _dsPeriodicidadeCobrancaTarifa;

    /**
     * Field _cdPeriodicidadeComprovante
     */
    private int _cdPeriodicidadeComprovante = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeComprovante
     */
    private boolean _has_cdPeriodicidadeComprovante;

    /**
     * Field _dsPeriodicidadeComprovante
     */
    private java.lang.String _dsPeriodicidadeComprovante;

    /**
     * Field _cdPeriodicidadeConsultaVeiculo
     */
    private int _cdPeriodicidadeConsultaVeiculo = 0;

    /**
     * keeps track of state for field:
     * _cdPeriodicidadeConsultaVeiculo
     */
    private boolean _has_cdPeriodicidadeConsultaVeiculo;

    /**
     * Field _dsPeriodicidadeConsultaVeiculo
     */
    private java.lang.String _dsPeriodicidadeConsultaVeiculo;

    /**
     * Field _cdPeriodicidadeEnvioRemessa
     */
    private int _cdPeriodicidadeEnvioRemessa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeEnvioRemessa
     */
    private boolean _has_cdPeriodicidadeEnvioRemessa;

    /**
     * Field _dsPeriodicidadeEnvioRemessa
     */
    private java.lang.String _dsPeriodicidadeEnvioRemessa;

    /**
     * Field _cdPeriodicidadeManutencaoProcd
     */
    private int _cdPeriodicidadeManutencaoProcd = 0;

    /**
     * keeps track of state for field:
     * _cdPeriodicidadeManutencaoProcd
     */
    private boolean _has_cdPeriodicidadeManutencaoProcd;

    /**
     * Field _dsPeriodicidadeManutencaoProcd
     */
    private java.lang.String _dsPeriodicidadeManutencaoProcd;

    /**
     * Field _cdPeriodicidadeReajusteTarifa
     */
    private int _cdPeriodicidadeReajusteTarifa = 0;

    /**
     * keeps track of state for field: _cdPeriodicidadeReajusteTarif
     */
    private boolean _has_cdPeriodicidadeReajusteTarifa;

    /**
     * Field _dsPeriodicidadeReajusteTarifa
     */
    private java.lang.String _dsPeriodicidadeReajusteTarifa;

    /**
     * Field _cdPagamentoNaoUtil
     */
    private int _cdPagamentoNaoUtil = 0;

    /**
     * keeps track of state for field: _cdPagamentoNaoUtil
     */
    private boolean _has_cdPagamentoNaoUtil;

    /**
     * Field _dsPagamentoNaoUtil
     */
    private java.lang.String _dsPagamentoNaoUtil;

    /**
     * Field _cdPrincipalEnquaRecadastramento
     */
    private int _cdPrincipalEnquaRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _cdPrincipalEnquaRecadastramento
     */
    private boolean _has_cdPrincipalEnquaRecadastramento;

    /**
     * Field _dsPrincipalEnquaRecadastramento
     */
    private java.lang.String _dsPrincipalEnquaRecadastramento;

    /**
     * Field _cdPrioridadeEfetivacaoPagamento
     */
    private int _cdPrioridadeEfetivacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdPrioridadeEfetivacaoPagamento
     */
    private boolean _has_cdPrioridadeEfetivacaoPagamento;

    /**
     * Field _dsPrioridadeEfetivacaoPagamento
     */
    private java.lang.String _dsPrioridadeEfetivacaoPagamento;

    /**
     * Field _cdRejeicaoAgendamentoLote
     */
    private int _cdRejeicaoAgendamentoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAgendamentoLote
     */
    private boolean _has_cdRejeicaoAgendamentoLote;

    /**
     * Field _dsRejeicaoAgendamentoLote
     */
    private java.lang.String _dsRejeicaoAgendamentoLote;

    /**
     * Field _cdRejeicaoEfetivacaoLote
     */
    private int _cdRejeicaoEfetivacaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoEfetivacaoLote
     */
    private boolean _has_cdRejeicaoEfetivacaoLote;

    /**
     * Field _dsRejeicaoEfetivacaoLote
     */
    private java.lang.String _dsRejeicaoEfetivacaoLote;

    /**
     * Field _cdRejeicaoLote
     */
    private int _cdRejeicaoLote = 0;

    /**
     * keeps track of state for field: _cdRejeicaoLote
     */
    private boolean _has_cdRejeicaoLote;

    /**
     * Field _dsRejeicaoLote
     */
    private java.lang.String _dsRejeicaoLote;

    /**
     * Field _cdRastreabilidadeNotaFiscal
     */
    private int _cdRastreabilidadeNotaFiscal = 0;

    /**
     * keeps track of state for field: _cdRastreabilidadeNotaFiscal
     */
    private boolean _has_cdRastreabilidadeNotaFiscal;

    /**
     * Field _dsRastreabilidadeNotaFiscal
     */
    private java.lang.String _dsRastreabilidadeNotaFiscal;

    /**
     * Field _cdRastreabilidadeTituloTerceiro
     */
    private int _cdRastreabilidadeTituloTerceiro = 0;

    /**
     * keeps track of state for field:
     * _cdRastreabilidadeTituloTerceiro
     */
    private boolean _has_cdRastreabilidadeTituloTerceiro;

    /**
     * Field _dsRastreabilidadeTituloTerceiro
     */
    private java.lang.String _dsRastreabilidadeTituloTerceiro;

    /**
     * Field _cdTipoCargaRecadastramento
     */
    private int _cdTipoCargaRecadastramento = 0;

    /**
     * keeps track of state for field: _cdTipoCargaRecadastramento
     */
    private boolean _has_cdTipoCargaRecadastramento;

    /**
     * Field _dsTipoCargaRecadastramento
     */
    private java.lang.String _dsTipoCargaRecadastramento;

    /**
     * Field _cdTipoCartaoSalario
     */
    private int _cdTipoCartaoSalario = 0;

    /**
     * keeps track of state for field: _cdTipoCartaoSalario
     */
    private boolean _has_cdTipoCartaoSalario;

    /**
     * Field _dsTipoCartaoSalario
     */
    private java.lang.String _dsTipoCartaoSalario;

    /**
     * Field _cdTipoDataFloating
     */
    private int _cdTipoDataFloating = 0;

    /**
     * keeps track of state for field: _cdTipoDataFloating
     */
    private boolean _has_cdTipoDataFloating;

    /**
     * Field _dsTipoDataFloating
     */
    private java.lang.String _dsTipoDataFloating;

    /**
     * Field _cdTipoDivergenciaVeiculo
     */
    private int _cdTipoDivergenciaVeiculo = 0;

    /**
     * keeps track of state for field: _cdTipoDivergenciaVeiculo
     */
    private boolean _has_cdTipoDivergenciaVeiculo;

    /**
     * Field _dsTipoDivergenciaVeiculo
     */
    private java.lang.String _dsTipoDivergenciaVeiculo;

    /**
     * Field _cdTipoEfetivacaoPagamento
     */
    private int _cdTipoEfetivacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoEfetivacaoPagamento
     */
    private boolean _has_cdTipoEfetivacaoPagamento;

    /**
     * Field _dsTipoEfetivacaoPagamento
     */
    private java.lang.String _dsTipoEfetivacaoPagamento;

    /**
     * Field _cdTipoIdentificacaoBeneficio
     */
    private int _cdTipoIdentificacaoBeneficio = 0;

    /**
     * keeps track of state for field: _cdTipoIdentificacaoBeneficio
     */
    private boolean _has_cdTipoIdentificacaoBeneficio;

    /**
     * Field _dsTipoIdentificacaoBeneficio
     */
    private java.lang.String _dsTipoIdentificacaoBeneficio;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdTipoReajusteTarifa
     */
    private int _cdTipoReajusteTarifa = 0;

    /**
     * keeps track of state for field: _cdTipoReajusteTarifa
     */
    private boolean _has_cdTipoReajusteTarifa;

    /**
     * Field _dsTipoReajusteTarifa
     */
    private java.lang.String _dsTipoReajusteTarifa;

    /**
     * Field _cdTratamentoContaTransferida
     */
    private int _cdTratamentoContaTransferida = 0;

    /**
     * keeps track of state for field: _cdTratamentoContaTransferida
     */
    private boolean _has_cdTratamentoContaTransferida;

    /**
     * Field _dsTratamentoContaTransferida
     */
    private java.lang.String _dsTratamentoContaTransferida;

    /**
     * Field _cdUtilizacaoFavorecidoControle
     */
    private int _cdUtilizacaoFavorecidoControle = 0;

    /**
     * keeps track of state for field:
     * _cdUtilizacaoFavorecidoControle
     */
    private boolean _has_cdUtilizacaoFavorecidoControle;

    /**
     * Field _dsUtilizacaoFavorecidoControle
     */
    private java.lang.String _dsUtilizacaoFavorecidoControle;

    /**
     * Field _dtEnquaContaSalario
     */
    private java.lang.String _dtEnquaContaSalario;

    /**
     * Field _dtInicioBloqueioPapeleta
     */
    private java.lang.String _dtInicioBloqueioPapeleta;

    /**
     * Field _dtInicioRastreabilidadeTitulo
     */
    private java.lang.String _dtInicioRastreabilidadeTitulo;

    /**
     * Field _dtFimAcertoRecadastramento
     */
    private java.lang.String _dtFimAcertoRecadastramento;

    /**
     * Field _dtFimRecadastramentoBeneficio
     */
    private java.lang.String _dtFimRecadastramentoBeneficio;

    /**
     * Field _dtInicioAcertoRecadastramento
     */
    private java.lang.String _dtInicioAcertoRecadastramento;

    /**
     * Field _dtInicioRecadastramentoBeneficio
     */
    private java.lang.String _dtInicioRecadastramentoBeneficio;

    /**
     * Field _dtLimiteVinculoCarga
     */
    private java.lang.String _dtLimiteVinculoCarga;

    /**
     * Field _dtRegistroTitulo
     */
    private java.lang.String _dtRegistroTitulo;

    /**
     * Field _nrFechamentoApuracaoTarifa
     */
    private int _nrFechamentoApuracaoTarifa = 0;

    /**
     * keeps track of state for field: _nrFechamentoApuracaoTarifa
     */
    private boolean _has_nrFechamentoApuracaoTarifa;

    /**
     * Field _percentualIndiceReajusteTarifa
     */
    private java.math.BigDecimal _percentualIndiceReajusteTarifa = new java.math.BigDecimal("0");

    /**
     * Field _percentualMaximoInconsistenteLote
     */
    private int _percentualMaximoInconsistenteLote = 0;

    /**
     * keeps track of state for field:
     * _percentualMaximoInconsistenteLote
     */
    private boolean _has_percentualMaximoInconsistenteLote;

    /**
     * Field _periodicidadeTarifaCatalogo
     */
    private java.math.BigDecimal _periodicidadeTarifaCatalogo = new java.math.BigDecimal("0");

    /**
     * Field _quantidadeAntecedencia
     */
    private int _quantidadeAntecedencia = 0;

    /**
     * keeps track of state for field: _quantidadeAntecedencia
     */
    private boolean _has_quantidadeAntecedencia;

    /**
     * Field _quantidadeAnteriorVencimentoComprovante
     */
    private int _quantidadeAnteriorVencimentoComprovante = 0;

    /**
     * keeps track of state for field:
     * _quantidadeAnteriorVencimentoComprovante
     */
    private boolean _has_quantidadeAnteriorVencimentoComprovante;

    /**
     * Field _quantidadeDiaCobrancaTarifa
     */
    private int _quantidadeDiaCobrancaTarifa = 0;

    /**
     * keeps track of state for field: _quantidadeDiaCobrancaTarifa
     */
    private boolean _has_quantidadeDiaCobrancaTarifa;

    /**
     * Field _quantidadeDiaExpiracao
     */
    private int _quantidadeDiaExpiracao = 0;

    /**
     * keeps track of state for field: _quantidadeDiaExpiracao
     */
    private boolean _has_quantidadeDiaExpiracao;

    /**
     * Field _quantidadeDiaFloatingPagamento
     */
    private int _quantidadeDiaFloatingPagamento = 0;

    /**
     * keeps track of state for field:
     * _quantidadeDiaFloatingPagamento
     */
    private boolean _has_quantidadeDiaFloatingPagamento;

    /**
     * Field _quantidadeDiaInatividadeFavorecido
     */
    private int _quantidadeDiaInatividadeFavorecido = 0;

    /**
     * keeps track of state for field:
     * _quantidadeDiaInatividadeFavorecido
     */
    private boolean _has_quantidadeDiaInatividadeFavorecido;

    /**
     * Field _quantidadeDiaRepiqueConsulta
     */
    private int _quantidadeDiaRepiqueConsulta = 0;

    /**
     * keeps track of state for field: _quantidadeDiaRepiqueConsulta
     */
    private boolean _has_quantidadeDiaRepiqueConsulta;

    /**
     * Field _quantidadeEtapaRecadastramentoBeneficio
     */
    private int _quantidadeEtapaRecadastramentoBeneficio = 0;

    /**
     * keeps track of state for field:
     * _quantidadeEtapaRecadastramentoBeneficio
     */
    private boolean _has_quantidadeEtapaRecadastramentoBeneficio;

    /**
     * Field _quantidadeFaseRecadastramentoBeneficio
     */
    private int _quantidadeFaseRecadastramentoBeneficio = 0;

    /**
     * keeps track of state for field:
     * _quantidadeFaseRecadastramentoBeneficio
     */
    private boolean _has_quantidadeFaseRecadastramentoBeneficio;

    /**
     * Field _quantidadeLimiteLinha
     */
    private int _quantidadeLimiteLinha = 0;

    /**
     * keeps track of state for field: _quantidadeLimiteLinha
     */
    private boolean _has_quantidadeLimiteLinha;

    /**
     * Field _quantidadeSolicitacaoCartao
     */
    private int _quantidadeSolicitacaoCartao = 0;

    /**
     * keeps track of state for field: _quantidadeSolicitacaoCartao
     */
    private boolean _has_quantidadeSolicitacaoCartao;

    /**
     * Field _quantidadeMaximaInconsistenteLote
     */
    private int _quantidadeMaximaInconsistenteLote = 0;

    /**
     * keeps track of state for field:
     * _quantidadeMaximaInconsistenteLote
     */
    private boolean _has_quantidadeMaximaInconsistenteLote;

    /**
     * Field _quantidadeMaximaTituloVencido
     */
    private int _quantidadeMaximaTituloVencido = 0;

    /**
     * keeps track of state for field: _quantidadeMaximaTituloVencid
     */
    private boolean _has_quantidadeMaximaTituloVencido;

    /**
     * Field _quantidadeMesComprovante
     */
    private int _quantidadeMesComprovante = 0;

    /**
     * keeps track of state for field: _quantidadeMesComprovante
     */
    private boolean _has_quantidadeMesComprovante;

    /**
     * Field _quantidadeMesEtapaRecadastramento
     */
    private int _quantidadeMesEtapaRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _quantidadeMesEtapaRecadastramento
     */
    private boolean _has_quantidadeMesEtapaRecadastramento;

    /**
     * Field _quantidadeMesFaseRecadastramento
     */
    private int _quantidadeMesFaseRecadastramento = 0;

    /**
     * keeps track of state for field:
     * _quantidadeMesFaseRecadastramento
     */
    private boolean _has_quantidadeMesFaseRecadastramento;

    /**
     * Field _quantidadeMesReajusteTarifa
     */
    private int _quantidadeMesReajusteTarifa = 0;

    /**
     * keeps track of state for field: _quantidadeMesReajusteTarifa
     */
    private boolean _has_quantidadeMesReajusteTarifa;

    /**
     * Field _quantidadeViaAviso
     */
    private int _quantidadeViaAviso = 0;

    /**
     * keeps track of state for field: _quantidadeViaAviso
     */
    private boolean _has_quantidadeViaAviso;

    /**
     * Field _quantidadeViaCobranca
     */
    private int _quantidadeViaCobranca = 0;

    /**
     * keeps track of state for field: _quantidadeViaCobranca
     */
    private boolean _has_quantidadeViaCobranca;

    /**
     * Field _quantidadeViaComprovante
     */
    private int _quantidadeViaComprovante = 0;

    /**
     * keeps track of state for field: _quantidadeViaComprovante
     */
    private boolean _has_quantidadeViaComprovante;

    /**
     * Field _valorFavorecidoNaoCadastrado
     */
    private java.math.BigDecimal _valorFavorecidoNaoCadastrado = new java.math.BigDecimal("0");

    /**
     * Field _valorLimiteDiarioPagamento
     */
    private java.math.BigDecimal _valorLimiteDiarioPagamento = new java.math.BigDecimal("0");

    /**
     * Field _valorLimiteIndividualPagamento
     */
    private java.math.BigDecimal _valorLimiteIndividualPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdIndicadorListaDebito
     */
    private int _cdIndicadorListaDebito = 0;

    /**
     * keeps track of state for field: _cdIndicadorListaDebito
     */
    private boolean _has_cdIndicadorListaDebito;

    /**
     * Field _cdTipoConsistenciaLista
     */
    private int _cdTipoConsistenciaLista = 0;

    /**
     * keeps track of state for field: _cdTipoConsistenciaLista
     */
    private boolean _has_cdTipoConsistenciaLista;

    /**
     * Field _dsIndicadorListaDebito
     */
    private java.lang.String _dsIndicadorListaDebito;

    /**
     * Field _dsIndicadorRetornoInternet
     */
    private java.lang.String _dsIndicadorRetornoInternet;

    /**
     * Field _cdTipoFormacaoLista
     */
    private int _cdTipoFormacaoLista = 0;

    /**
     * keeps track of state for field: _cdTipoFormacaoLista
     */
    private boolean _has_cdTipoFormacaoLista;

    /**
     * Field _dsTipoFormacaoLista
     */
    private java.lang.String _dsTipoFormacaoLista;

    /**
     * Field _dsTipoConsistenciaLista
     */
    private java.lang.String _dsTipoConsistenciaLista;

    /**
     * Field _cdIndicadorRetornoInternet
     */
    private int _cdIndicadorRetornoInternet = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoInternet
     */
    private boolean _has_cdIndicadorRetornoInternet;

    /**
     * Field _dsTipoConsolidacaoComprovante
     */
    private java.lang.String _dsTipoConsolidacaoComprovante;

    /**
     * Field _cdTipoConsultaComprovante
     */
    private int _cdTipoConsultaComprovante = 0;

    /**
     * keeps track of state for field: _cdTipoConsultaComprovante
     */
    private boolean _has_cdTipoConsultaComprovante;

    /**
     * Field _cdIndicadorBancoPostal
     */
    private int _cdIndicadorBancoPostal = 0;

    /**
     * keeps track of state for field: _cdIndicadorBancoPostal
     */
    private boolean _has_cdIndicadorBancoPostal;

    /**
     * Field _dsIndicadorBancoPostal
     */
    private java.lang.String _dsIndicadorBancoPostal;

    /**
     * Field _cdAmbienteServicoContrato
     */
    private java.lang.String _cdAmbienteServicoContrato;

    /**
     * Field _dsAreaReservada2
     */
    private java.lang.String _dsAreaReservada2;

    /**
     * Field _cdLocalEmissao
     */
    private java.lang.String _cdLocalEmissao;

    /**
     * Field _dsSituacaoServicoRelacionado
     */
    private java.lang.String _dsSituacaoServicoRelacionado;

    /**
     * Field _cdConsultaSaldoValorSuperior
     */
    private int _cdConsultaSaldoValorSuperior = 0;

    /**
     * keeps track of state for field: _cdConsultaSaldoValorSuperior
     */
    private boolean _has_cdConsultaSaldoValorSuperior;

    /**
     * Field _dsConsultaSaldoValorSuperior
     */
    private java.lang.String _dsConsultaSaldoValorSuperior;

    /**
     * Field _cdIndicadorFeriadoLocal
     */
    private int _cdIndicadorFeriadoLocal = 0;

    /**
     * keeps track of state for field: _cdIndicadorFeriadoLocal
     */
    private boolean _has_cdIndicadorFeriadoLocal;

    /**
     * Field _dsCodigoIndFeriadoLocal
     */
    private java.lang.String _dsCodigoIndFeriadoLocal;

    /**
     * Field _cdIndicadorSegundaLinha
     */
    private int _cdIndicadorSegundaLinha = 0;

    /**
     * keeps track of state for field: _cdIndicadorSegundaLinha
     */
    private boolean _has_cdIndicadorSegundaLinha;

    /**
     * Field _dsIndicadorSegundaLinha
     */
    private java.lang.String _dsIndicadorSegundaLinha;

    /**
     * Field _cdFloatServicoContrato
     */
    private int _cdFloatServicoContrato = 0;

    /**
     * keeps track of state for field: _cdFloatServicoContrato
     */
    private boolean _has_cdFloatServicoContrato;

    /**
     * Field _dsFloatServicoContrato
     */
    private java.lang.String _dsFloatServicoContrato;

    /**
     * Field _qtDiaUtilPgto
     */
    private int _qtDiaUtilPgto = 0;

    /**
     * keeps track of state for field: _qtDiaUtilPgto
     */
    private boolean _has_qtDiaUtilPgto;

    /**
     * Field _cdExigeAutFilial
     */
    private int _cdExigeAutFilial = 0;

    /**
     * keeps track of state for field: _cdExigeAutFilial
     */
    private boolean _has_cdExigeAutFilial;

    /**
     * Field _dsExigeFilial
     */
    private java.lang.String _dsExigeFilial;

    /**
     * Field _cdPreenchimentoLancamentoPersonalizado
     */
    private int _cdPreenchimentoLancamentoPersonalizado = 0;

    /**
     * keeps track of state for field:
     * _cdPreenchimentoLancamentoPersonalizado
     */
    private boolean _has_cdPreenchimentoLancamentoPersonalizado;

    /**
     * Field _dsPreenchimentoLancamentoPersonalizado
     */
    private java.lang.String _dsPreenchimentoLancamentoPersonalizado;

    /**
     * Field _cdIndicadorAgendaGrade
     */
    private int _cdIndicadorAgendaGrade = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgendaGrade
     */
    private boolean _has_cdIndicadorAgendaGrade;

    /**
     * Field _dsIndicadorAgendaGrade
     */
    private java.lang.String _dsIndicadorAgendaGrade;

    /**
     * Field _cdTituloDdaRetorno
     */
    private int _cdTituloDdaRetorno = 0;

    /**
     * keeps track of state for field: _cdTituloDdaRetorno
     */
    private boolean _has_cdTituloDdaRetorno;

    /**
     * Field _dsTituloDdaRetorno
     */
    private java.lang.String _dsTituloDdaRetorno;

    /**
     * Field _cdIndicadorUtilizaMora
     */
    private int _cdIndicadorUtilizaMora = 0;

    /**
     * keeps track of state for field: _cdIndicadorUtilizaMora
     */
    private boolean _has_cdIndicadorUtilizaMora;

    /**
     * Field _dsIndicadorUtilizaMora
     */
    private java.lang.String _dsIndicadorUtilizaMora;

    /**
     * Field _vlPercentualDiferencaTolerada
     */
    private java.math.BigDecimal _vlPercentualDiferencaTolerada = new java.math.BigDecimal("0");

    /**
     * Field _cdConsistenciaCpfCnpjBenefAvalNpc
     */
    private int _cdConsistenciaCpfCnpjBenefAvalNpc = 0;

    /**
     * keeps track of state for field:
     * _cdConsistenciaCpfCnpjBenefAvalNpc
     */
    private boolean _has_cdConsistenciaCpfCnpjBenefAvalNpc;

    /**
     * Field _dsConsistenciaCpfCnpjBenefAvalNpc
     */
    private java.lang.String _dsConsistenciaCpfCnpjBenefAvalNpc;

    /**
     * Field _dsOrigemIndicador
     */
    private int _dsOrigemIndicador = 0;

    /**
     * keeps track of state for field: _dsOrigemIndicador
     */
    private boolean _has_dsOrigemIndicador;

    /**
     * Field _dsIndicador
     */
    private java.lang.String _dsIndicador;

    /**
     * Field _cdIdentificadorTipoRetornoInternet
     */
    private int _cdIdentificadorTipoRetornoInternet = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificadorTipoRetornoInternet
     */
    private boolean _has_cdIdentificadorTipoRetornoInternet;

    /**
     * Field _dsCodIdentificadortipoRetornoInternet
     */
    private java.lang.String _dsCodIdentificadorTipoRetornoInternet;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioExternoInclusao
     */
    private java.lang.String _cdUsuarioExternoInclusao;

    /**
     * Field _hrManutencaoRegistroInclusao
     */
    private java.lang.String _hrManutencaoRegistroInclusao;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _nrOperacaoFluxoInclusao
     */
    private java.lang.String _nrOperacaoFluxoInclusao;

    /**
     * Field _cdUsuarioAlteracao
     */
    private java.lang.String _cdUsuarioAlteracao;

    /**
     * Field _cdUsuarioExternoAlteracao
     */
    private java.lang.String _cdUsuarioExternoAlteracao;

    /**
     * Field _hrManutencaoRegistroAlteracao
     */
    private java.lang.String _hrManutencaoRegistroAlteracao;

    /**
     * Field _cdCanalAlteracao
     */
    private int _cdCanalAlteracao = 0;

    /**
     * keeps track of state for field: _cdCanalAlteracao
     */
    private boolean _has_cdCanalAlteracao;

    /**
     * Field _dsCanalAlteracao
     */
    private java.lang.String _dsCanalAlteracao;

    /**
     * Field _nrOperacaoFluxoAlteracao
     */
    private java.lang.String _nrOperacaoFluxoAlteracao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarConManServicosResponse() 
     {
        super();
        setPercentualIndiceReajusteTarifa(new java.math.BigDecimal("0"));
        setPeriodicidadeTarifaCatalogo(new java.math.BigDecimal("0"));
        setValorFavorecidoNaoCadastrado(new java.math.BigDecimal("0"));
        setValorLimiteDiarioPagamento(new java.math.BigDecimal("0"));
        setValorLimiteIndividualPagamento(new java.math.BigDecimal("0"));
        setVlPercentualDiferencaTolerada(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.ConsultarConManServicosResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoNaoVida
     * 
     */
    public void deleteCdAcaoNaoVida()
    {
        this._has_cdAcaoNaoVida= false;
    } //-- void deleteCdAcaoNaoVida() 

    /**
     * Method deleteCdAcertoDadosRecadastramento
     * 
     */
    public void deleteCdAcertoDadosRecadastramento()
    {
        this._has_cdAcertoDadosRecadastramento= false;
    } //-- void deleteCdAcertoDadosRecadastramento() 

    /**
     * Method deleteCdAgendamentoDebitoVeiculo
     * 
     */
    public void deleteCdAgendamentoDebitoVeiculo()
    {
        this._has_cdAgendamentoDebitoVeiculo= false;
    } //-- void deleteCdAgendamentoDebitoVeiculo() 

    /**
     * Method deleteCdAgendamentoPagamentoVencido
     * 
     */
    public void deleteCdAgendamentoPagamentoVencido()
    {
        this._has_cdAgendamentoPagamentoVencido= false;
    } //-- void deleteCdAgendamentoPagamentoVencido() 

    /**
     * Method deleteCdAgendamentoRastreabilidadeFinal
     * 
     */
    public void deleteCdAgendamentoRastreabilidadeFinal()
    {
        this._has_cdAgendamentoRastreabilidadeFinal= false;
    } //-- void deleteCdAgendamentoRastreabilidadeFinal() 

    /**
     * Method deleteCdAgendamentoValorMenor
     * 
     */
    public void deleteCdAgendamentoValorMenor()
    {
        this._has_cdAgendamentoValorMenor= false;
    } //-- void deleteCdAgendamentoValorMenor() 

    /**
     * Method deleteCdAgrupamentoAviso
     * 
     */
    public void deleteCdAgrupamentoAviso()
    {
        this._has_cdAgrupamentoAviso= false;
    } //-- void deleteCdAgrupamentoAviso() 

    /**
     * Method deleteCdAgrupamentoComprovante
     * 
     */
    public void deleteCdAgrupamentoComprovante()
    {
        this._has_cdAgrupamentoComprovante= false;
    } //-- void deleteCdAgrupamentoComprovante() 

    /**
     * Method deleteCdAgrupamentoFormularioRecadastro
     * 
     */
    public void deleteCdAgrupamentoFormularioRecadastro()
    {
        this._has_cdAgrupamentoFormularioRecadastro= false;
    } //-- void deleteCdAgrupamentoFormularioRecadastro() 

    /**
     * Method deleteCdAntecipacaoRecadastramentoBeneficiario
     * 
     */
    public void deleteCdAntecipacaoRecadastramentoBeneficiario()
    {
        this._has_cdAntecipacaoRecadastramentoBeneficiario= false;
    } //-- void deleteCdAntecipacaoRecadastramentoBeneficiario() 

    /**
     * Method deleteCdAreaReservada
     * 
     */
    public void deleteCdAreaReservada()
    {
        this._has_cdAreaReservada= false;
    } //-- void deleteCdAreaReservada() 

    /**
     * Method deleteCdBaseRecadastramentoBeneficio
     * 
     */
    public void deleteCdBaseRecadastramentoBeneficio()
    {
        this._has_cdBaseRecadastramentoBeneficio= false;
    } //-- void deleteCdBaseRecadastramentoBeneficio() 

    /**
     * Method deleteCdBloqueioEmissaoPapeleta
     * 
     */
    public void deleteCdBloqueioEmissaoPapeleta()
    {
        this._has_cdBloqueioEmissaoPapeleta= false;
    } //-- void deleteCdBloqueioEmissaoPapeleta() 

    /**
     * Method deleteCdCanalAlteracao
     * 
     */
    public void deleteCdCanalAlteracao()
    {
        this._has_cdCanalAlteracao= false;
    } //-- void deleteCdCanalAlteracao() 

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCapturaTituloRegistrado
     * 
     */
    public void deleteCdCapturaTituloRegistrado()
    {
        this._has_cdCapturaTituloRegistrado= false;
    } //-- void deleteCdCapturaTituloRegistrado() 

    /**
     * Method deleteCdCctciaEspeBeneficio
     * 
     */
    public void deleteCdCctciaEspeBeneficio()
    {
        this._has_cdCctciaEspeBeneficio= false;
    } //-- void deleteCdCctciaEspeBeneficio() 

    /**
     * Method deleteCdCctciaIdentificacaoBeneficio
     * 
     */
    public void deleteCdCctciaIdentificacaoBeneficio()
    {
        this._has_cdCctciaIdentificacaoBeneficio= false;
    } //-- void deleteCdCctciaIdentificacaoBeneficio() 

    /**
     * Method deleteCdCctciaInscricaoFavorecido
     * 
     */
    public void deleteCdCctciaInscricaoFavorecido()
    {
        this._has_cdCctciaInscricaoFavorecido= false;
    } //-- void deleteCdCctciaInscricaoFavorecido() 

    /**
     * Method deleteCdCctciaProprietarioVeculo
     * 
     */
    public void deleteCdCctciaProprietarioVeculo()
    {
        this._has_cdCctciaProprietarioVeculo= false;
    } //-- void deleteCdCctciaProprietarioVeculo() 

    /**
     * Method deleteCdCobrancaTarifa
     * 
     */
    public void deleteCdCobrancaTarifa()
    {
        this._has_cdCobrancaTarifa= false;
    } //-- void deleteCdCobrancaTarifa() 

    /**
     * Method deleteCdConsistenciaCpfCnpjBenefAvalNpc
     * 
     */
    public void deleteCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        this._has_cdConsistenciaCpfCnpjBenefAvalNpc= false;
    } //-- void deleteCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Method deleteCdConsultaDebitoVeiculo
     * 
     */
    public void deleteCdConsultaDebitoVeiculo()
    {
        this._has_cdConsultaDebitoVeiculo= false;
    } //-- void deleteCdConsultaDebitoVeiculo() 

    /**
     * Method deleteCdConsultaEndereco
     * 
     */
    public void deleteCdConsultaEndereco()
    {
        this._has_cdConsultaEndereco= false;
    } //-- void deleteCdConsultaEndereco() 

    /**
     * Method deleteCdConsultaSaldoPagamento
     * 
     */
    public void deleteCdConsultaSaldoPagamento()
    {
        this._has_cdConsultaSaldoPagamento= false;
    } //-- void deleteCdConsultaSaldoPagamento() 

    /**
     * Method deleteCdConsultaSaldoValorSuperior
     * 
     */
    public void deleteCdConsultaSaldoValorSuperior()
    {
        this._has_cdConsultaSaldoValorSuperior= false;
    } //-- void deleteCdConsultaSaldoValorSuperior() 

    /**
     * Method deleteCdContagemConsultaSaldo
     * 
     */
    public void deleteCdContagemConsultaSaldo()
    {
        this._has_cdContagemConsultaSaldo= false;
    } //-- void deleteCdContagemConsultaSaldo() 

    /**
     * Method deleteCdCreditoNaoUtilizado
     * 
     */
    public void deleteCdCreditoNaoUtilizado()
    {
        this._has_cdCreditoNaoUtilizado= false;
    } //-- void deleteCdCreditoNaoUtilizado() 

    /**
     * Method deleteCdCriterioEnquadraRecadastramento
     * 
     */
    public void deleteCdCriterioEnquadraRecadastramento()
    {
        this._has_cdCriterioEnquadraRecadastramento= false;
    } //-- void deleteCdCriterioEnquadraRecadastramento() 

    /**
     * Method deleteCdCriterioEnquandraBeneficio
     * 
     */
    public void deleteCdCriterioEnquandraBeneficio()
    {
        this._has_cdCriterioEnquandraBeneficio= false;
    } //-- void deleteCdCriterioEnquandraBeneficio() 

    /**
     * Method deleteCdCriterioRastreabilidadeTitulo
     * 
     */
    public void deleteCdCriterioRastreabilidadeTitulo()
    {
        this._has_cdCriterioRastreabilidadeTitulo= false;
    } //-- void deleteCdCriterioRastreabilidadeTitulo() 

    /**
     * Method deleteCdDestinoAviso
     * 
     */
    public void deleteCdDestinoAviso()
    {
        this._has_cdDestinoAviso= false;
    } //-- void deleteCdDestinoAviso() 

    /**
     * Method deleteCdDestinoComprovante
     * 
     */
    public void deleteCdDestinoComprovante()
    {
        this._has_cdDestinoComprovante= false;
    } //-- void deleteCdDestinoComprovante() 

    /**
     * Method deleteCdDestinoFormularioRecadastramento
     * 
     */
    public void deleteCdDestinoFormularioRecadastramento()
    {
        this._has_cdDestinoFormularioRecadastramento= false;
    } //-- void deleteCdDestinoFormularioRecadastramento() 

    /**
     * Method deleteCdDisponibilizacaoContaCredito
     * 
     */
    public void deleteCdDisponibilizacaoContaCredito()
    {
        this._has_cdDisponibilizacaoContaCredito= false;
    } //-- void deleteCdDisponibilizacaoContaCredito() 

    /**
     * Method deleteCdDisponibilizacaoDiversoCriterio
     * 
     */
    public void deleteCdDisponibilizacaoDiversoCriterio()
    {
        this._has_cdDisponibilizacaoDiversoCriterio= false;
    } //-- void deleteCdDisponibilizacaoDiversoCriterio() 

    /**
     * Method deleteCdDisponibilizacaoDiversoNao
     * 
     */
    public void deleteCdDisponibilizacaoDiversoNao()
    {
        this._has_cdDisponibilizacaoDiversoNao= false;
    } //-- void deleteCdDisponibilizacaoDiversoNao() 

    /**
     * Method deleteCdDisponibilizacaoSalarioCriterio
     * 
     */
    public void deleteCdDisponibilizacaoSalarioCriterio()
    {
        this._has_cdDisponibilizacaoSalarioCriterio= false;
    } //-- void deleteCdDisponibilizacaoSalarioCriterio() 

    /**
     * Method deleteCdDisponibilizacaoSalarioNao
     * 
     */
    public void deleteCdDisponibilizacaoSalarioNao()
    {
        this._has_cdDisponibilizacaoSalarioNao= false;
    } //-- void deleteCdDisponibilizacaoSalarioNao() 

    /**
     * Method deleteCdEnvelopeAberto
     * 
     */
    public void deleteCdEnvelopeAberto()
    {
        this._has_cdEnvelopeAberto= false;
    } //-- void deleteCdEnvelopeAberto() 

    /**
     * Method deleteCdExigeAutFilial
     * 
     */
    public void deleteCdExigeAutFilial()
    {
        this._has_cdExigeAutFilial= false;
    } //-- void deleteCdExigeAutFilial() 

    /**
     * Method deleteCdFavorecidoConsultaPagamento
     * 
     */
    public void deleteCdFavorecidoConsultaPagamento()
    {
        this._has_cdFavorecidoConsultaPagamento= false;
    } //-- void deleteCdFavorecidoConsultaPagamento() 

    /**
     * Method deleteCdFloatServicoContrato
     * 
     */
    public void deleteCdFloatServicoContrato()
    {
        this._has_cdFloatServicoContrato= false;
    } //-- void deleteCdFloatServicoContrato() 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFormaEnvioPagamento
     * 
     */
    public void deleteCdFormaEnvioPagamento()
    {
        this._has_cdFormaEnvioPagamento= false;
    } //-- void deleteCdFormaEnvioPagamento() 

    /**
     * Method deleteCdFormaEstornoCredito
     * 
     */
    public void deleteCdFormaEstornoCredito()
    {
        this._has_cdFormaEstornoCredito= false;
    } //-- void deleteCdFormaEstornoCredito() 

    /**
     * Method deleteCdFormaExpiracaoCredito
     * 
     */
    public void deleteCdFormaExpiracaoCredito()
    {
        this._has_cdFormaExpiracaoCredito= false;
    } //-- void deleteCdFormaExpiracaoCredito() 

    /**
     * Method deleteCdFormaManutencao
     * 
     */
    public void deleteCdFormaManutencao()
    {
        this._has_cdFormaManutencao= false;
    } //-- void deleteCdFormaManutencao() 

    /**
     * Method deleteCdFrasePrecadastrada
     * 
     */
    public void deleteCdFrasePrecadastrada()
    {
        this._has_cdFrasePrecadastrada= false;
    } //-- void deleteCdFrasePrecadastrada() 

    /**
     * Method deleteCdIdentificadorTipoRetornoInternet
     * 
     */
    public void deleteCdIdentificadorTipoRetornoInternet()
    {
        this._has_cdIdentificadorTipoRetornoInternet= false;
    } //-- void deleteCdIdentificadorTipoRetornoInternet() 

    /**
     * Method deleteCdIndicadorAgendaGrade
     * 
     */
    public void deleteCdIndicadorAgendaGrade()
    {
        this._has_cdIndicadorAgendaGrade= false;
    } //-- void deleteCdIndicadorAgendaGrade() 

    /**
     * Method deleteCdIndicadorAgendamentoTitulo
     * 
     */
    public void deleteCdIndicadorAgendamentoTitulo()
    {
        this._has_cdIndicadorAgendamentoTitulo= false;
    } //-- void deleteCdIndicadorAgendamentoTitulo() 

    /**
     * Method deleteCdIndicadorAutorizacaoCliente
     * 
     */
    public void deleteCdIndicadorAutorizacaoCliente()
    {
        this._has_cdIndicadorAutorizacaoCliente= false;
    } //-- void deleteCdIndicadorAutorizacaoCliente() 

    /**
     * Method deleteCdIndicadorAutorizacaoComplemento
     * 
     */
    public void deleteCdIndicadorAutorizacaoComplemento()
    {
        this._has_cdIndicadorAutorizacaoComplemento= false;
    } //-- void deleteCdIndicadorAutorizacaoComplemento() 

    /**
     * Method deleteCdIndicadorBancoPostal
     * 
     */
    public void deleteCdIndicadorBancoPostal()
    {
        this._has_cdIndicadorBancoPostal= false;
    } //-- void deleteCdIndicadorBancoPostal() 

    /**
     * Method deleteCdIndicadorCadastroProcurador
     * 
     */
    public void deleteCdIndicadorCadastroProcurador()
    {
        this._has_cdIndicadorCadastroProcurador= false;
    } //-- void deleteCdIndicadorCadastroProcurador() 

    /**
     * Method deleteCdIndicadorCadastroorganizacao
     * 
     */
    public void deleteCdIndicadorCadastroorganizacao()
    {
        this._has_cdIndicadorCadastroorganizacao= false;
    } //-- void deleteCdIndicadorCadastroorganizacao() 

    /**
     * Method deleteCdIndicadorCartaoSalario
     * 
     */
    public void deleteCdIndicadorCartaoSalario()
    {
        this._has_cdIndicadorCartaoSalario= false;
    } //-- void deleteCdIndicadorCartaoSalario() 

    /**
     * Method deleteCdIndicadorEconomicoReajuste
     * 
     */
    public void deleteCdIndicadorEconomicoReajuste()
    {
        this._has_cdIndicadorEconomicoReajuste= false;
    } //-- void deleteCdIndicadorEconomicoReajuste() 

    /**
     * Method deleteCdIndicadorFeriadoLocal
     * 
     */
    public void deleteCdIndicadorFeriadoLocal()
    {
        this._has_cdIndicadorFeriadoLocal= false;
    } //-- void deleteCdIndicadorFeriadoLocal() 

    /**
     * Method deleteCdIndicadorListaDebito
     * 
     */
    public void deleteCdIndicadorListaDebito()
    {
        this._has_cdIndicadorListaDebito= false;
    } //-- void deleteCdIndicadorListaDebito() 

    /**
     * Method deleteCdIndicadorMensagemPersonalizada
     * 
     */
    public void deleteCdIndicadorMensagemPersonalizada()
    {
        this._has_cdIndicadorMensagemPersonalizada= false;
    } //-- void deleteCdIndicadorMensagemPersonalizada() 

    /**
     * Method deleteCdIndicadorRetornoInternet
     * 
     */
    public void deleteCdIndicadorRetornoInternet()
    {
        this._has_cdIndicadorRetornoInternet= false;
    } //-- void deleteCdIndicadorRetornoInternet() 

    /**
     * Method deleteCdIndicadorSegundaLinha
     * 
     */
    public void deleteCdIndicadorSegundaLinha()
    {
        this._has_cdIndicadorSegundaLinha= false;
    } //-- void deleteCdIndicadorSegundaLinha() 

    /**
     * Method deleteCdIndicadorUtilizaMora
     * 
     */
    public void deleteCdIndicadorUtilizaMora()
    {
        this._has_cdIndicadorUtilizaMora= false;
    } //-- void deleteCdIndicadorUtilizaMora() 

    /**
     * Method deleteCdLancamentoFuturoCredito
     * 
     */
    public void deleteCdLancamentoFuturoCredito()
    {
        this._has_cdLancamentoFuturoCredito= false;
    } //-- void deleteCdLancamentoFuturoCredito() 

    /**
     * Method deleteCdLancamentoFuturoDebito
     * 
     */
    public void deleteCdLancamentoFuturoDebito()
    {
        this._has_cdLancamentoFuturoDebito= false;
    } //-- void deleteCdLancamentoFuturoDebito() 

    /**
     * Method deleteCdLiberacaoLoteProcessado
     * 
     */
    public void deleteCdLiberacaoLoteProcessado()
    {
        this._has_cdLiberacaoLoteProcessado= false;
    } //-- void deleteCdLiberacaoLoteProcessado() 

    /**
     * Method deleteCdManutencaoBaseRecadastramento
     * 
     */
    public void deleteCdManutencaoBaseRecadastramento()
    {
        this._has_cdManutencaoBaseRecadastramento= false;
    } //-- void deleteCdManutencaoBaseRecadastramento() 

    /**
     * Method deleteCdMeioPagamentoDebito
     * 
     */
    public void deleteCdMeioPagamentoDebito()
    {
        this._has_cdMeioPagamentoDebito= false;
    } //-- void deleteCdMeioPagamentoDebito() 

    /**
     * Method deleteCdMensagemRecadastramentoMidia
     * 
     */
    public void deleteCdMensagemRecadastramentoMidia()
    {
        this._has_cdMensagemRecadastramentoMidia= false;
    } //-- void deleteCdMensagemRecadastramentoMidia() 

    /**
     * Method deleteCdMidiaDisponivel
     * 
     */
    public void deleteCdMidiaDisponivel()
    {
        this._has_cdMidiaDisponivel= false;
    } //-- void deleteCdMidiaDisponivel() 

    /**
     * Method deleteCdMidiaMensagemRecadastramento
     * 
     */
    public void deleteCdMidiaMensagemRecadastramento()
    {
        this._has_cdMidiaMensagemRecadastramento= false;
    } //-- void deleteCdMidiaMensagemRecadastramento() 

    /**
     * Method deleteCdMomentoAvisoRecadastramento
     * 
     */
    public void deleteCdMomentoAvisoRecadastramento()
    {
        this._has_cdMomentoAvisoRecadastramento= false;
    } //-- void deleteCdMomentoAvisoRecadastramento() 

    /**
     * Method deleteCdMomentoCreditoEfetivacao
     * 
     */
    public void deleteCdMomentoCreditoEfetivacao()
    {
        this._has_cdMomentoCreditoEfetivacao= false;
    } //-- void deleteCdMomentoCreditoEfetivacao() 

    /**
     * Method deleteCdMomentoDebitoPagamento
     * 
     */
    public void deleteCdMomentoDebitoPagamento()
    {
        this._has_cdMomentoDebitoPagamento= false;
    } //-- void deleteCdMomentoDebitoPagamento() 

    /**
     * Method deleteCdMomentoFormularioRecadastramento
     * 
     */
    public void deleteCdMomentoFormularioRecadastramento()
    {
        this._has_cdMomentoFormularioRecadastramento= false;
    } //-- void deleteCdMomentoFormularioRecadastramento() 

    /**
     * Method deleteCdMomentoProcessamentoPagamento
     * 
     */
    public void deleteCdMomentoProcessamentoPagamento()
    {
        this._has_cdMomentoProcessamentoPagamento= false;
    } //-- void deleteCdMomentoProcessamentoPagamento() 

    /**
     * Method deleteCdNaturezaOperacaoPagamento
     * 
     */
    public void deleteCdNaturezaOperacaoPagamento()
    {
        this._has_cdNaturezaOperacaoPagamento= false;
    } //-- void deleteCdNaturezaOperacaoPagamento() 

    /**
     * Method deleteCdOutraidentificacaoFavorecido
     * 
     */
    public void deleteCdOutraidentificacaoFavorecido()
    {
        this._has_cdOutraidentificacaoFavorecido= false;
    } //-- void deleteCdOutraidentificacaoFavorecido() 

    /**
     * Method deleteCdPagamentoNaoUtil
     * 
     */
    public void deleteCdPagamentoNaoUtil()
    {
        this._has_cdPagamentoNaoUtil= false;
    } //-- void deleteCdPagamentoNaoUtil() 

    /**
     * Method deleteCdPeriodicidadeAviso
     * 
     */
    public void deleteCdPeriodicidadeAviso()
    {
        this._has_cdPeriodicidadeAviso= false;
    } //-- void deleteCdPeriodicidadeAviso() 

    /**
     * Method deleteCdPeriodicidadeCobrancaTarifa
     * 
     */
    public void deleteCdPeriodicidadeCobrancaTarifa()
    {
        this._has_cdPeriodicidadeCobrancaTarifa= false;
    } //-- void deleteCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method deleteCdPeriodicidadeComprovante
     * 
     */
    public void deleteCdPeriodicidadeComprovante()
    {
        this._has_cdPeriodicidadeComprovante= false;
    } //-- void deleteCdPeriodicidadeComprovante() 

    /**
     * Method deleteCdPeriodicidadeConsultaVeiculo
     * 
     */
    public void deleteCdPeriodicidadeConsultaVeiculo()
    {
        this._has_cdPeriodicidadeConsultaVeiculo= false;
    } //-- void deleteCdPeriodicidadeConsultaVeiculo() 

    /**
     * Method deleteCdPeriodicidadeEnvioRemessa
     * 
     */
    public void deleteCdPeriodicidadeEnvioRemessa()
    {
        this._has_cdPeriodicidadeEnvioRemessa= false;
    } //-- void deleteCdPeriodicidadeEnvioRemessa() 

    /**
     * Method deleteCdPeriodicidadeManutencaoProcd
     * 
     */
    public void deleteCdPeriodicidadeManutencaoProcd()
    {
        this._has_cdPeriodicidadeManutencaoProcd= false;
    } //-- void deleteCdPeriodicidadeManutencaoProcd() 

    /**
     * Method deleteCdPeriodicidadeReajusteTarifa
     * 
     */
    public void deleteCdPeriodicidadeReajusteTarifa()
    {
        this._has_cdPeriodicidadeReajusteTarifa= false;
    } //-- void deleteCdPeriodicidadeReajusteTarifa() 

    /**
     * Method deleteCdPermissaoDebitoOnline
     * 
     */
    public void deleteCdPermissaoDebitoOnline()
    {
        this._has_cdPermissaoDebitoOnline= false;
    } //-- void deleteCdPermissaoDebitoOnline() 

    /**
     * Method deleteCdPreenchimentoLancamentoPersonalizado
     * 
     */
    public void deleteCdPreenchimentoLancamentoPersonalizado()
    {
        this._has_cdPreenchimentoLancamentoPersonalizado= false;
    } //-- void deleteCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Method deleteCdPrincipalEnquaRecadastramento
     * 
     */
    public void deleteCdPrincipalEnquaRecadastramento()
    {
        this._has_cdPrincipalEnquaRecadastramento= false;
    } //-- void deleteCdPrincipalEnquaRecadastramento() 

    /**
     * Method deleteCdPrioridadeEfetivacaoPagamento
     * 
     */
    public void deleteCdPrioridadeEfetivacaoPagamento()
    {
        this._has_cdPrioridadeEfetivacaoPagamento= false;
    } //-- void deleteCdPrioridadeEfetivacaoPagamento() 

    /**
     * Method deleteCdRastreabilidadeNotaFiscal
     * 
     */
    public void deleteCdRastreabilidadeNotaFiscal()
    {
        this._has_cdRastreabilidadeNotaFiscal= false;
    } //-- void deleteCdRastreabilidadeNotaFiscal() 

    /**
     * Method deleteCdRastreabilidadeTituloTerceiro
     * 
     */
    public void deleteCdRastreabilidadeTituloTerceiro()
    {
        this._has_cdRastreabilidadeTituloTerceiro= false;
    } //-- void deleteCdRastreabilidadeTituloTerceiro() 

    /**
     * Method deleteCdRejeicaoAgendamentoLote
     * 
     */
    public void deleteCdRejeicaoAgendamentoLote()
    {
        this._has_cdRejeicaoAgendamentoLote= false;
    } //-- void deleteCdRejeicaoAgendamentoLote() 

    /**
     * Method deleteCdRejeicaoEfetivacaoLote
     * 
     */
    public void deleteCdRejeicaoEfetivacaoLote()
    {
        this._has_cdRejeicaoEfetivacaoLote= false;
    } //-- void deleteCdRejeicaoEfetivacaoLote() 

    /**
     * Method deleteCdRejeicaoLote
     * 
     */
    public void deleteCdRejeicaoLote()
    {
        this._has_cdRejeicaoLote= false;
    } //-- void deleteCdRejeicaoLote() 

    /**
     * Method deleteCdTipoCargaRecadastramento
     * 
     */
    public void deleteCdTipoCargaRecadastramento()
    {
        this._has_cdTipoCargaRecadastramento= false;
    } //-- void deleteCdTipoCargaRecadastramento() 

    /**
     * Method deleteCdTipoCartaoSalario
     * 
     */
    public void deleteCdTipoCartaoSalario()
    {
        this._has_cdTipoCartaoSalario= false;
    } //-- void deleteCdTipoCartaoSalario() 

    /**
     * Method deleteCdTipoConsistenciaLista
     * 
     */
    public void deleteCdTipoConsistenciaLista()
    {
        this._has_cdTipoConsistenciaLista= false;
    } //-- void deleteCdTipoConsistenciaLista() 

    /**
     * Method deleteCdTipoConsultaComprovante
     * 
     */
    public void deleteCdTipoConsultaComprovante()
    {
        this._has_cdTipoConsultaComprovante= false;
    } //-- void deleteCdTipoConsultaComprovante() 

    /**
     * Method deleteCdTipoDataFloating
     * 
     */
    public void deleteCdTipoDataFloating()
    {
        this._has_cdTipoDataFloating= false;
    } //-- void deleteCdTipoDataFloating() 

    /**
     * Method deleteCdTipoDivergenciaVeiculo
     * 
     */
    public void deleteCdTipoDivergenciaVeiculo()
    {
        this._has_cdTipoDivergenciaVeiculo= false;
    } //-- void deleteCdTipoDivergenciaVeiculo() 

    /**
     * Method deleteCdTipoEfetivacaoPagamento
     * 
     */
    public void deleteCdTipoEfetivacaoPagamento()
    {
        this._has_cdTipoEfetivacaoPagamento= false;
    } //-- void deleteCdTipoEfetivacaoPagamento() 

    /**
     * Method deleteCdTipoFormacaoLista
     * 
     */
    public void deleteCdTipoFormacaoLista()
    {
        this._has_cdTipoFormacaoLista= false;
    } //-- void deleteCdTipoFormacaoLista() 

    /**
     * Method deleteCdTipoIdentificacaoBeneficio
     * 
     */
    public void deleteCdTipoIdentificacaoBeneficio()
    {
        this._has_cdTipoIdentificacaoBeneficio= false;
    } //-- void deleteCdTipoIdentificacaoBeneficio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoReajusteTarifa
     * 
     */
    public void deleteCdTipoReajusteTarifa()
    {
        this._has_cdTipoReajusteTarifa= false;
    } //-- void deleteCdTipoReajusteTarifa() 

    /**
     * Method deleteCdTituloDdaRetorno
     * 
     */
    public void deleteCdTituloDdaRetorno()
    {
        this._has_cdTituloDdaRetorno= false;
    } //-- void deleteCdTituloDdaRetorno() 

    /**
     * Method deleteCdTratamentoContaTransferida
     * 
     */
    public void deleteCdTratamentoContaTransferida()
    {
        this._has_cdTratamentoContaTransferida= false;
    } //-- void deleteCdTratamentoContaTransferida() 

    /**
     * Method deleteCdUtilizacaoFavorecidoControle
     * 
     */
    public void deleteCdUtilizacaoFavorecidoControle()
    {
        this._has_cdUtilizacaoFavorecidoControle= false;
    } //-- void deleteCdUtilizacaoFavorecidoControle() 

    /**
     * Method deleteCdindicadorExpiraCredito
     * 
     */
    public void deleteCdindicadorExpiraCredito()
    {
        this._has_cdindicadorExpiraCredito= false;
    } //-- void deleteCdindicadorExpiraCredito() 

    /**
     * Method deleteCdindicadorLancamentoProgramado
     * 
     */
    public void deleteCdindicadorLancamentoProgramado()
    {
        this._has_cdindicadorLancamentoProgramado= false;
    } //-- void deleteCdindicadorLancamentoProgramado() 

    /**
     * Method deleteDsOrigemIndicador
     * 
     */
    public void deleteDsOrigemIndicador()
    {
        this._has_dsOrigemIndicador= false;
    } //-- void deleteDsOrigemIndicador() 

    /**
     * Method deleteNrFechamentoApuracaoTarifa
     * 
     */
    public void deleteNrFechamentoApuracaoTarifa()
    {
        this._has_nrFechamentoApuracaoTarifa= false;
    } //-- void deleteNrFechamentoApuracaoTarifa() 

    /**
     * Method deletePercentualMaximoInconsistenteLote
     * 
     */
    public void deletePercentualMaximoInconsistenteLote()
    {
        this._has_percentualMaximoInconsistenteLote= false;
    } //-- void deletePercentualMaximoInconsistenteLote() 

    /**
     * Method deleteQtDiaUtilPgto
     * 
     */
    public void deleteQtDiaUtilPgto()
    {
        this._has_qtDiaUtilPgto= false;
    } //-- void deleteQtDiaUtilPgto() 

    /**
     * Method deleteQuantidadeAntecedencia
     * 
     */
    public void deleteQuantidadeAntecedencia()
    {
        this._has_quantidadeAntecedencia= false;
    } //-- void deleteQuantidadeAntecedencia() 

    /**
     * Method deleteQuantidadeAnteriorVencimentoComprovante
     * 
     */
    public void deleteQuantidadeAnteriorVencimentoComprovante()
    {
        this._has_quantidadeAnteriorVencimentoComprovante= false;
    } //-- void deleteQuantidadeAnteriorVencimentoComprovante() 

    /**
     * Method deleteQuantidadeDiaCobrancaTarifa
     * 
     */
    public void deleteQuantidadeDiaCobrancaTarifa()
    {
        this._has_quantidadeDiaCobrancaTarifa= false;
    } //-- void deleteQuantidadeDiaCobrancaTarifa() 

    /**
     * Method deleteQuantidadeDiaExpiracao
     * 
     */
    public void deleteQuantidadeDiaExpiracao()
    {
        this._has_quantidadeDiaExpiracao= false;
    } //-- void deleteQuantidadeDiaExpiracao() 

    /**
     * Method deleteQuantidadeDiaFloatingPagamento
     * 
     */
    public void deleteQuantidadeDiaFloatingPagamento()
    {
        this._has_quantidadeDiaFloatingPagamento= false;
    } //-- void deleteQuantidadeDiaFloatingPagamento() 

    /**
     * Method deleteQuantidadeDiaInatividadeFavorecido
     * 
     */
    public void deleteQuantidadeDiaInatividadeFavorecido()
    {
        this._has_quantidadeDiaInatividadeFavorecido= false;
    } //-- void deleteQuantidadeDiaInatividadeFavorecido() 

    /**
     * Method deleteQuantidadeDiaRepiqueConsulta
     * 
     */
    public void deleteQuantidadeDiaRepiqueConsulta()
    {
        this._has_quantidadeDiaRepiqueConsulta= false;
    } //-- void deleteQuantidadeDiaRepiqueConsulta() 

    /**
     * Method deleteQuantidadeEtapaRecadastramentoBeneficio
     * 
     */
    public void deleteQuantidadeEtapaRecadastramentoBeneficio()
    {
        this._has_quantidadeEtapaRecadastramentoBeneficio= false;
    } //-- void deleteQuantidadeEtapaRecadastramentoBeneficio() 

    /**
     * Method deleteQuantidadeFaseRecadastramentoBeneficio
     * 
     */
    public void deleteQuantidadeFaseRecadastramentoBeneficio()
    {
        this._has_quantidadeFaseRecadastramentoBeneficio= false;
    } //-- void deleteQuantidadeFaseRecadastramentoBeneficio() 

    /**
     * Method deleteQuantidadeLimiteLinha
     * 
     */
    public void deleteQuantidadeLimiteLinha()
    {
        this._has_quantidadeLimiteLinha= false;
    } //-- void deleteQuantidadeLimiteLinha() 

    /**
     * Method deleteQuantidadeMaximaInconsistenteLote
     * 
     */
    public void deleteQuantidadeMaximaInconsistenteLote()
    {
        this._has_quantidadeMaximaInconsistenteLote= false;
    } //-- void deleteQuantidadeMaximaInconsistenteLote() 

    /**
     * Method deleteQuantidadeMaximaTituloVencido
     * 
     */
    public void deleteQuantidadeMaximaTituloVencido()
    {
        this._has_quantidadeMaximaTituloVencido= false;
    } //-- void deleteQuantidadeMaximaTituloVencido() 

    /**
     * Method deleteQuantidadeMesComprovante
     * 
     */
    public void deleteQuantidadeMesComprovante()
    {
        this._has_quantidadeMesComprovante= false;
    } //-- void deleteQuantidadeMesComprovante() 

    /**
     * Method deleteQuantidadeMesEtapaRecadastramento
     * 
     */
    public void deleteQuantidadeMesEtapaRecadastramento()
    {
        this._has_quantidadeMesEtapaRecadastramento= false;
    } //-- void deleteQuantidadeMesEtapaRecadastramento() 

    /**
     * Method deleteQuantidadeMesFaseRecadastramento
     * 
     */
    public void deleteQuantidadeMesFaseRecadastramento()
    {
        this._has_quantidadeMesFaseRecadastramento= false;
    } //-- void deleteQuantidadeMesFaseRecadastramento() 

    /**
     * Method deleteQuantidadeMesReajusteTarifa
     * 
     */
    public void deleteQuantidadeMesReajusteTarifa()
    {
        this._has_quantidadeMesReajusteTarifa= false;
    } //-- void deleteQuantidadeMesReajusteTarifa() 

    /**
     * Method deleteQuantidadeSolicitacaoCartao
     * 
     */
    public void deleteQuantidadeSolicitacaoCartao()
    {
        this._has_quantidadeSolicitacaoCartao= false;
    } //-- void deleteQuantidadeSolicitacaoCartao() 

    /**
     * Method deleteQuantidadeViaAviso
     * 
     */
    public void deleteQuantidadeViaAviso()
    {
        this._has_quantidadeViaAviso= false;
    } //-- void deleteQuantidadeViaAviso() 

    /**
     * Method deleteQuantidadeViaCobranca
     * 
     */
    public void deleteQuantidadeViaCobranca()
    {
        this._has_quantidadeViaCobranca= false;
    } //-- void deleteQuantidadeViaCobranca() 

    /**
     * Method deleteQuantidadeViaComprovante
     * 
     */
    public void deleteQuantidadeViaComprovante()
    {
        this._has_quantidadeViaComprovante= false;
    } //-- void deleteQuantidadeViaComprovante() 

    /**
     * Returns the value of field 'cdAcaoNaoVida'.
     * 
     * @return int
     * @return the value of field 'cdAcaoNaoVida'.
     */
    public int getCdAcaoNaoVida()
    {
        return this._cdAcaoNaoVida;
    } //-- int getCdAcaoNaoVida() 

    /**
     * Returns the value of field 'cdAcertoDadosRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdAcertoDadosRecadastramento'.
     */
    public int getCdAcertoDadosRecadastramento()
    {
        return this._cdAcertoDadosRecadastramento;
    } //-- int getCdAcertoDadosRecadastramento() 

    /**
     * Returns the value of field 'cdAgendamentoDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdAgendamentoDebitoVeiculo'.
     */
    public int getCdAgendamentoDebitoVeiculo()
    {
        return this._cdAgendamentoDebitoVeiculo;
    } //-- int getCdAgendamentoDebitoVeiculo() 

    /**
     * Returns the value of field 'cdAgendamentoPagamentoVencido'.
     * 
     * @return int
     * @return the value of field 'cdAgendamentoPagamentoVencido'.
     */
    public int getCdAgendamentoPagamentoVencido()
    {
        return this._cdAgendamentoPagamentoVencido;
    } //-- int getCdAgendamentoPagamentoVencido() 

    /**
     * Returns the value of field
     * 'cdAgendamentoRastreabilidadeFinal'.
     * 
     * @return int
     * @return the value of field
     * 'cdAgendamentoRastreabilidadeFinal'.
     */
    public int getCdAgendamentoRastreabilidadeFinal()
    {
        return this._cdAgendamentoRastreabilidadeFinal;
    } //-- int getCdAgendamentoRastreabilidadeFinal() 

    /**
     * Returns the value of field 'cdAgendamentoValorMenor'.
     * 
     * @return int
     * @return the value of field 'cdAgendamentoValorMenor'.
     */
    public int getCdAgendamentoValorMenor()
    {
        return this._cdAgendamentoValorMenor;
    } //-- int getCdAgendamentoValorMenor() 

    /**
     * Returns the value of field 'cdAgrupamentoAviso'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoAviso'.
     */
    public int getCdAgrupamentoAviso()
    {
        return this._cdAgrupamentoAviso;
    } //-- int getCdAgrupamentoAviso() 

    /**
     * Returns the value of field 'cdAgrupamentoComprovante'.
     * 
     * @return int
     * @return the value of field 'cdAgrupamentoComprovante'.
     */
    public int getCdAgrupamentoComprovante()
    {
        return this._cdAgrupamentoComprovante;
    } //-- int getCdAgrupamentoComprovante() 

    /**
     * Returns the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     * 
     * @return int
     * @return the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     */
    public int getCdAgrupamentoFormularioRecadastro()
    {
        return this._cdAgrupamentoFormularioRecadastro;
    } //-- int getCdAgrupamentoFormularioRecadastro() 

    /**
     * Returns the value of field 'cdAmbienteServicoContrato'.
     * 
     * @return String
     * @return the value of field 'cdAmbienteServicoContrato'.
     */
    public java.lang.String getCdAmbienteServicoContrato()
    {
        return this._cdAmbienteServicoContrato;
    } //-- java.lang.String getCdAmbienteServicoContrato() 

    /**
     * Returns the value of field
     * 'cdAntecipacaoRecadastramentoBeneficiario'.
     * 
     * @return int
     * @return the value of field
     * 'cdAntecipacaoRecadastramentoBeneficiario'.
     */
    public int getCdAntecipacaoRecadastramentoBeneficiario()
    {
        return this._cdAntecipacaoRecadastramentoBeneficiario;
    } //-- int getCdAntecipacaoRecadastramentoBeneficiario() 

    /**
     * Returns the value of field 'cdAreaReservada'.
     * 
     * @return int
     * @return the value of field 'cdAreaReservada'.
     */
    public int getCdAreaReservada()
    {
        return this._cdAreaReservada;
    } //-- int getCdAreaReservada() 

    /**
     * Returns the value of field 'cdBaseRecadastramentoBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdBaseRecadastramentoBeneficio'.
     */
    public int getCdBaseRecadastramentoBeneficio()
    {
        return this._cdBaseRecadastramentoBeneficio;
    } //-- int getCdBaseRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'cdBloqueioEmissaoPapeleta'.
     * 
     * @return int
     * @return the value of field 'cdBloqueioEmissaoPapeleta'.
     */
    public int getCdBloqueioEmissaoPapeleta()
    {
        return this._cdBloqueioEmissaoPapeleta;
    } //-- int getCdBloqueioEmissaoPapeleta() 

    /**
     * Returns the value of field 'cdCanalAlteracao'.
     * 
     * @return int
     * @return the value of field 'cdCanalAlteracao'.
     */
    public int getCdCanalAlteracao()
    {
        return this._cdCanalAlteracao;
    } //-- int getCdCanalAlteracao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCapturaTituloRegistrado'.
     * 
     * @return int
     * @return the value of field 'cdCapturaTituloRegistrado'.
     */
    public int getCdCapturaTituloRegistrado()
    {
        return this._cdCapturaTituloRegistrado;
    } //-- int getCdCapturaTituloRegistrado() 

    /**
     * Returns the value of field 'cdCctciaEspeBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCctciaEspeBeneficio'.
     */
    public int getCdCctciaEspeBeneficio()
    {
        return this._cdCctciaEspeBeneficio;
    } //-- int getCdCctciaEspeBeneficio() 

    /**
     * Returns the value of field 'cdCctciaIdentificacaoBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCctciaIdentificacaoBeneficio'.
     */
    public int getCdCctciaIdentificacaoBeneficio()
    {
        return this._cdCctciaIdentificacaoBeneficio;
    } //-- int getCdCctciaIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'cdCctciaInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdCctciaInscricaoFavorecido'.
     */
    public int getCdCctciaInscricaoFavorecido()
    {
        return this._cdCctciaInscricaoFavorecido;
    } //-- int getCdCctciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdCctciaProprietarioVeculo'.
     * 
     * @return int
     * @return the value of field 'cdCctciaProprietarioVeculo'.
     */
    public int getCdCctciaProprietarioVeculo()
    {
        return this._cdCctciaProprietarioVeculo;
    } //-- int getCdCctciaProprietarioVeculo() 

    /**
     * Returns the value of field 'cdCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdCobrancaTarifa'.
     */
    public int getCdCobrancaTarifa()
    {
        return this._cdCobrancaTarifa;
    } //-- int getCdCobrancaTarifa() 

    /**
     * Returns the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @return int
     * @return the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public int getCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        return this._cdConsistenciaCpfCnpjBenefAvalNpc;
    } //-- int getCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Returns the value of field 'cdConsultaDebitoVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdConsultaDebitoVeiculo'.
     */
    public int getCdConsultaDebitoVeiculo()
    {
        return this._cdConsultaDebitoVeiculo;
    } //-- int getCdConsultaDebitoVeiculo() 

    /**
     * Returns the value of field 'cdConsultaEndereco'.
     * 
     * @return int
     * @return the value of field 'cdConsultaEndereco'.
     */
    public int getCdConsultaEndereco()
    {
        return this._cdConsultaEndereco;
    } //-- int getCdConsultaEndereco() 

    /**
     * Returns the value of field 'cdConsultaSaldoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdConsultaSaldoPagamento'.
     */
    public int getCdConsultaSaldoPagamento()
    {
        return this._cdConsultaSaldoPagamento;
    } //-- int getCdConsultaSaldoPagamento() 

    /**
     * Returns the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @return int
     * @return the value of field 'cdConsultaSaldoValorSuperior'.
     */
    public int getCdConsultaSaldoValorSuperior()
    {
        return this._cdConsultaSaldoValorSuperior;
    } //-- int getCdConsultaSaldoValorSuperior() 

    /**
     * Returns the value of field 'cdContagemConsultaSaldo'.
     * 
     * @return int
     * @return the value of field 'cdContagemConsultaSaldo'.
     */
    public int getCdContagemConsultaSaldo()
    {
        return this._cdContagemConsultaSaldo;
    } //-- int getCdContagemConsultaSaldo() 

    /**
     * Returns the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @return int
     * @return the value of field 'cdCreditoNaoUtilizado'.
     */
    public int getCdCreditoNaoUtilizado()
    {
        return this._cdCreditoNaoUtilizado;
    } //-- int getCdCreditoNaoUtilizado() 

    /**
     * Returns the value of field
     * 'cdCriterioEnquadraRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'cdCriterioEnquadraRecadastramento'.
     */
    public int getCdCriterioEnquadraRecadastramento()
    {
        return this._cdCriterioEnquadraRecadastramento;
    } //-- int getCdCriterioEnquadraRecadastramento() 

    /**
     * Returns the value of field 'cdCriterioEnquandraBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdCriterioEnquandraBeneficio'.
     */
    public int getCdCriterioEnquandraBeneficio()
    {
        return this._cdCriterioEnquandraBeneficio;
    } //-- int getCdCriterioEnquandraBeneficio() 

    /**
     * Returns the value of field
     * 'cdCriterioRastreabilidadeTitulo'.
     * 
     * @return int
     * @return the value of field 'cdCriterioRastreabilidadeTitulo'.
     */
    public int getCdCriterioRastreabilidadeTitulo()
    {
        return this._cdCriterioRastreabilidadeTitulo;
    } //-- int getCdCriterioRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'cdDestinoAviso'.
     * 
     * @return int
     * @return the value of field 'cdDestinoAviso'.
     */
    public int getCdDestinoAviso()
    {
        return this._cdDestinoAviso;
    } //-- int getCdDestinoAviso() 

    /**
     * Returns the value of field 'cdDestinoComprovante'.
     * 
     * @return int
     * @return the value of field 'cdDestinoComprovante'.
     */
    public int getCdDestinoComprovante()
    {
        return this._cdDestinoComprovante;
    } //-- int getCdDestinoComprovante() 

    /**
     * Returns the value of field
     * 'cdDestinoFormularioRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'cdDestinoFormularioRecadastramento'.
     */
    public int getCdDestinoFormularioRecadastramento()
    {
        return this._cdDestinoFormularioRecadastramento;
    } //-- int getCdDestinoFormularioRecadastramento() 

    /**
     * Returns the value of field 'cdDisponibilizacaoContaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDisponibilizacaoContaCredito'.
     */
    public int getCdDisponibilizacaoContaCredito()
    {
        return this._cdDisponibilizacaoContaCredito;
    } //-- int getCdDisponibilizacaoContaCredito() 

    /**
     * Returns the value of field
     * 'cdDisponibilizacaoDiversoCriterio'.
     * 
     * @return int
     * @return the value of field
     * 'cdDisponibilizacaoDiversoCriterio'.
     */
    public int getCdDisponibilizacaoDiversoCriterio()
    {
        return this._cdDisponibilizacaoDiversoCriterio;
    } //-- int getCdDisponibilizacaoDiversoCriterio() 

    /**
     * Returns the value of field 'cdDisponibilizacaoDiversoNao'.
     * 
     * @return int
     * @return the value of field 'cdDisponibilizacaoDiversoNao'.
     */
    public int getCdDisponibilizacaoDiversoNao()
    {
        return this._cdDisponibilizacaoDiversoNao;
    } //-- int getCdDisponibilizacaoDiversoNao() 

    /**
     * Returns the value of field
     * 'cdDisponibilizacaoSalarioCriterio'.
     * 
     * @return int
     * @return the value of field
     * 'cdDisponibilizacaoSalarioCriterio'.
     */
    public int getCdDisponibilizacaoSalarioCriterio()
    {
        return this._cdDisponibilizacaoSalarioCriterio;
    } //-- int getCdDisponibilizacaoSalarioCriterio() 

    /**
     * Returns the value of field 'cdDisponibilizacaoSalarioNao'.
     * 
     * @return int
     * @return the value of field 'cdDisponibilizacaoSalarioNao'.
     */
    public int getCdDisponibilizacaoSalarioNao()
    {
        return this._cdDisponibilizacaoSalarioNao;
    } //-- int getCdDisponibilizacaoSalarioNao() 

    /**
     * Returns the value of field 'cdEnvelopeAberto'.
     * 
     * @return int
     * @return the value of field 'cdEnvelopeAberto'.
     */
    public int getCdEnvelopeAberto()
    {
        return this._cdEnvelopeAberto;
    } //-- int getCdEnvelopeAberto() 

    /**
     * Returns the value of field 'cdExigeAutFilial'.
     * 
     * @return int
     * @return the value of field 'cdExigeAutFilial'.
     */
    public int getCdExigeAutFilial()
    {
        return this._cdExigeAutFilial;
    } //-- int getCdExigeAutFilial() 

    /**
     * Returns the value of field 'cdFavorecidoConsultaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFavorecidoConsultaPagamento'.
     */
    public int getCdFavorecidoConsultaPagamento()
    {
        return this._cdFavorecidoConsultaPagamento;
    } //-- int getCdFavorecidoConsultaPagamento() 

    /**
     * Returns the value of field 'cdFloatServicoContrato'.
     * 
     * @return int
     * @return the value of field 'cdFloatServicoContrato'.
     */
    public int getCdFloatServicoContrato()
    {
        return this._cdFloatServicoContrato;
    } //-- int getCdFloatServicoContrato() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFormaEnvioPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaEnvioPagamento'.
     */
    public int getCdFormaEnvioPagamento()
    {
        return this._cdFormaEnvioPagamento;
    } //-- int getCdFormaEnvioPagamento() 

    /**
     * Returns the value of field 'cdFormaEstornoCredito'.
     * 
     * @return int
     * @return the value of field 'cdFormaEstornoCredito'.
     */
    public int getCdFormaEstornoCredito()
    {
        return this._cdFormaEstornoCredito;
    } //-- int getCdFormaEstornoCredito() 

    /**
     * Returns the value of field 'cdFormaExpiracaoCredito'.
     * 
     * @return int
     * @return the value of field 'cdFormaExpiracaoCredito'.
     */
    public int getCdFormaExpiracaoCredito()
    {
        return this._cdFormaExpiracaoCredito;
    } //-- int getCdFormaExpiracaoCredito() 

    /**
     * Returns the value of field 'cdFormaManutencao'.
     * 
     * @return int
     * @return the value of field 'cdFormaManutencao'.
     */
    public int getCdFormaManutencao()
    {
        return this._cdFormaManutencao;
    } //-- int getCdFormaManutencao() 

    /**
     * Returns the value of field 'cdFrasePrecadastrada'.
     * 
     * @return int
     * @return the value of field 'cdFrasePrecadastrada'.
     */
    public int getCdFrasePrecadastrada()
    {
        return this._cdFrasePrecadastrada;
    } //-- int getCdFrasePrecadastrada() 

    /**
     * Returns the value of field
     * 'cdIdentificadorTipoRetornoInternet'.
     * 
     * @return int
     * @return the value of field
     * 'cdIdentificadorTipoRetornoInternet'.
     */
    public int getCdIdentificadorTipoRetornoInternet()
    {
        return this._cdIdentificadorTipoRetornoInternet;
    } //-- int getCdIdentificadortipoRetornoInternet() 

    /**
     * Returns the value of field 'cdIndicadorAgendaGrade'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendaGrade'.
     */
    public int getCdIndicadorAgendaGrade()
    {
        return this._cdIndicadorAgendaGrade;
    } //-- int getCdIndicadorAgendaGrade() 

    /**
     * Returns the value of field 'cdIndicadorAgendamentoTitulo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgendamentoTitulo'.
     */
    public int getCdIndicadorAgendamentoTitulo()
    {
        return this._cdIndicadorAgendamentoTitulo;
    } //-- int getCdIndicadorAgendamentoTitulo() 

    /**
     * Returns the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAutorizacaoCliente'.
     */
    public int getCdIndicadorAutorizacaoCliente()
    {
        return this._cdIndicadorAutorizacaoCliente;
    } //-- int getCdIndicadorAutorizacaoCliente() 

    /**
     * Returns the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @return int
     * @return the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public int getCdIndicadorAutorizacaoComplemento()
    {
        return this._cdIndicadorAutorizacaoComplemento;
    } //-- int getCdIndicadorAutorizacaoComplemento() 

    /**
     * Returns the value of field 'cdIndicadorBancoPostal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorBancoPostal'.
     */
    public int getCdIndicadorBancoPostal()
    {
        return this._cdIndicadorBancoPostal;
    } //-- int getCdIndicadorBancoPostal() 

    /**
     * Returns the value of field 'cdIndicadorCadastroProcurador'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadastroProcurador'.
     */
    public int getCdIndicadorCadastroProcurador()
    {
        return this._cdIndicadorCadastroProcurador;
    } //-- int getCdIndicadorCadastroProcurador() 

    /**
     * Returns the value of field 'cdIndicadorCadastroorganizacao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCadastroorganizacao'.
     */
    public int getCdIndicadorCadastroorganizacao()
    {
        return this._cdIndicadorCadastroorganizacao;
    } //-- int getCdIndicadorCadastroorganizacao() 

    /**
     * Returns the value of field 'cdIndicadorCartaoSalario'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCartaoSalario'.
     */
    public int getCdIndicadorCartaoSalario()
    {
        return this._cdIndicadorCartaoSalario;
    } //-- int getCdIndicadorCartaoSalario() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoReajuste'.
     */
    public int getCdIndicadorEconomicoReajuste()
    {
        return this._cdIndicadorEconomicoReajuste;
    } //-- int getCdIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'cdIndicadorFeriadoLocal'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFeriadoLocal'.
     */
    public int getCdIndicadorFeriadoLocal()
    {
        return this._cdIndicadorFeriadoLocal;
    } //-- int getCdIndicadorFeriadoLocal() 

    /**
     * Returns the value of field 'cdIndicadorListaDebito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorListaDebito'.
     */
    public int getCdIndicadorListaDebito()
    {
        return this._cdIndicadorListaDebito;
    } //-- int getCdIndicadorListaDebito() 

    /**
     * Returns the value of field
     * 'cdIndicadorMensagemPersonalizada'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorMensagemPersonalizada'
     */
    public int getCdIndicadorMensagemPersonalizada()
    {
        return this._cdIndicadorMensagemPersonalizada;
    } //-- int getCdIndicadorMensagemPersonalizada() 

    /**
     * Returns the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoInternet'.
     */
    public int getCdIndicadorRetornoInternet()
    {
        return this._cdIndicadorRetornoInternet;
    } //-- int getCdIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'cdIndicadorSegundaLinha'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorSegundaLinha'.
     */
    public int getCdIndicadorSegundaLinha()
    {
        return this._cdIndicadorSegundaLinha;
    } //-- int getCdIndicadorSegundaLinha() 

    /**
     * Returns the value of field 'cdIndicadorUtilizaMora'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorUtilizaMora'.
     */
    public int getCdIndicadorUtilizaMora()
    {
        return this._cdIndicadorUtilizaMora;
    } //-- int getCdIndicadorUtilizaMora() 

    /**
     * Returns the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoCredito'.
     */
    public int getCdLancamentoFuturoCredito()
    {
        return this._cdLancamentoFuturoCredito;
    } //-- int getCdLancamentoFuturoCredito() 

    /**
     * Returns the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @return int
     * @return the value of field 'cdLancamentoFuturoDebito'.
     */
    public int getCdLancamentoFuturoDebito()
    {
        return this._cdLancamentoFuturoDebito;
    } //-- int getCdLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'cdLiberacaoLoteProcessado'.
     * 
     * @return int
     * @return the value of field 'cdLiberacaoLoteProcessado'.
     */
    public int getCdLiberacaoLoteProcessado()
    {
        return this._cdLiberacaoLoteProcessado;
    } //-- int getCdLiberacaoLoteProcessado() 

    /**
     * Returns the value of field 'cdLocalEmissao'.
     * 
     * @return String
     * @return the value of field 'cdLocalEmissao'.
     */
    public java.lang.String getCdLocalEmissao()
    {
        return this._cdLocalEmissao;
    } //-- java.lang.String getCdLocalEmissao() 

    /**
     * Returns the value of field
     * 'cdManutencaoBaseRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdManutencaoBaseRecadastramento'.
     */
    public int getCdManutencaoBaseRecadastramento()
    {
        return this._cdManutencaoBaseRecadastramento;
    } //-- int getCdManutencaoBaseRecadastramento() 

    /**
     * Returns the value of field 'cdMeioPagamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdMeioPagamentoDebito'.
     */
    public int getCdMeioPagamentoDebito()
    {
        return this._cdMeioPagamentoDebito;
    } //-- int getCdMeioPagamentoDebito() 

    /**
     * Returns the value of field 'cdMensagemRecadastramentoMidia'.
     * 
     * @return int
     * @return the value of field 'cdMensagemRecadastramentoMidia'.
     */
    public int getCdMensagemRecadastramentoMidia()
    {
        return this._cdMensagemRecadastramentoMidia;
    } //-- int getCdMensagemRecadastramentoMidia() 

    /**
     * Returns the value of field 'cdMidiaDisponivel'.
     * 
     * @return int
     * @return the value of field 'cdMidiaDisponivel'.
     */
    public int getCdMidiaDisponivel()
    {
        return this._cdMidiaDisponivel;
    } //-- int getCdMidiaDisponivel() 

    /**
     * Returns the value of field 'cdMidiaMensagemRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdMidiaMensagemRecadastramento'.
     */
    public int getCdMidiaMensagemRecadastramento()
    {
        return this._cdMidiaMensagemRecadastramento;
    } //-- int getCdMidiaMensagemRecadastramento() 

    /**
     * Returns the value of field 'cdMomentoAvisoRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoAvisoRecadastramento'.
     */
    public int getCdMomentoAvisoRecadastramento()
    {
        return this._cdMomentoAvisoRecadastramento;
    } //-- int getCdMomentoAvisoRecadastramento() 

    /**
     * Returns the value of field 'cdMomentoCreditoEfetivacao'.
     * 
     * @return int
     * @return the value of field 'cdMomentoCreditoEfetivacao'.
     */
    public int getCdMomentoCreditoEfetivacao()
    {
        return this._cdMomentoCreditoEfetivacao;
    } //-- int getCdMomentoCreditoEfetivacao() 

    /**
     * Returns the value of field 'cdMomentoDebitoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoDebitoPagamento'.
     */
    public int getCdMomentoDebitoPagamento()
    {
        return this._cdMomentoDebitoPagamento;
    } //-- int getCdMomentoDebitoPagamento() 

    /**
     * Returns the value of field
     * 'cdMomentoFormularioRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'cdMomentoFormularioRecadastramento'.
     */
    public int getCdMomentoFormularioRecadastramento()
    {
        return this._cdMomentoFormularioRecadastramento;
    } //-- int getCdMomentoFormularioRecadastramento() 

    /**
     * Returns the value of field
     * 'cdMomentoProcessamentoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMomentoProcessamentoPagamento'.
     */
    public int getCdMomentoProcessamentoPagamento()
    {
        return this._cdMomentoProcessamentoPagamento;
    } //-- int getCdMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdNaturezaOperacaoPagamento'.
     */
    public int getCdNaturezaOperacaoPagamento()
    {
        return this._cdNaturezaOperacaoPagamento;
    } //-- int getCdNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'cdOutraidentificacaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdOutraidentificacaoFavorecido'.
     */
    public int getCdOutraidentificacaoFavorecido()
    {
        return this._cdOutraidentificacaoFavorecido;
    } //-- int getCdOutraidentificacaoFavorecido() 

    /**
     * Returns the value of field 'cdPagamentoNaoUtil'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoNaoUtil'.
     */
    public int getCdPagamentoNaoUtil()
    {
        return this._cdPagamentoNaoUtil;
    } //-- int getCdPagamentoNaoUtil() 

    /**
     * Returns the value of field 'cdPeriodicidadeAviso'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeAviso'.
     */
    public int getCdPeriodicidadeAviso()
    {
        return this._cdPeriodicidadeAviso;
    } //-- int getCdPeriodicidadeAviso() 

    /**
     * Returns the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeCobrancaTarifa'.
     */
    public int getCdPeriodicidadeCobrancaTarifa()
    {
        return this._cdPeriodicidadeCobrancaTarifa;
    } //-- int getCdPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'cdPeriodicidadeComprovante'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeComprovante'.
     */
    public int getCdPeriodicidadeComprovante()
    {
        return this._cdPeriodicidadeComprovante;
    } //-- int getCdPeriodicidadeComprovante() 

    /**
     * Returns the value of field 'cdPeriodicidadeConsultaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeConsultaVeiculo'.
     */
    public int getCdPeriodicidadeConsultaVeiculo()
    {
        return this._cdPeriodicidadeConsultaVeiculo;
    } //-- int getCdPeriodicidadeConsultaVeiculo() 

    /**
     * Returns the value of field 'cdPeriodicidadeEnvioRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeEnvioRemessa'.
     */
    public int getCdPeriodicidadeEnvioRemessa()
    {
        return this._cdPeriodicidadeEnvioRemessa;
    } //-- int getCdPeriodicidadeEnvioRemessa() 

    /**
     * Returns the value of field 'cdPeriodicidadeManutencaoProcd'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeManutencaoProcd'.
     */
    public int getCdPeriodicidadeManutencaoProcd()
    {
        return this._cdPeriodicidadeManutencaoProcd;
    } //-- int getCdPeriodicidadeManutencaoProcd() 

    /**
     * Returns the value of field 'cdPeriodicidadeReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeReajusteTarifa'.
     */
    public int getCdPeriodicidadeReajusteTarifa()
    {
        return this._cdPeriodicidadeReajusteTarifa;
    } //-- int getCdPeriodicidadeReajusteTarifa() 

    /**
     * Returns the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @return int
     * @return the value of field 'cdPermissaoDebitoOnline'.
     */
    public int getCdPermissaoDebitoOnline()
    {
        return this._cdPermissaoDebitoOnline;
    } //-- int getCdPermissaoDebitoOnline() 

    /**
     * Returns the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     * 
     * @return int
     * @return the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     */
    public int getCdPreenchimentoLancamentoPersonalizado()
    {
        return this._cdPreenchimentoLancamentoPersonalizado;
    } //-- int getCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Returns the value of field
     * 'cdPrincipalEnquaRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdPrincipalEnquaRecadastramento'.
     */
    public int getCdPrincipalEnquaRecadastramento()
    {
        return this._cdPrincipalEnquaRecadastramento;
    } //-- int getCdPrincipalEnquaRecadastramento() 

    /**
     * Returns the value of field
     * 'cdPrioridadeEfetivacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdPrioridadeEfetivacaoPagamento'.
     */
    public int getCdPrioridadeEfetivacaoPagamento()
    {
        return this._cdPrioridadeEfetivacaoPagamento;
    } //-- int getCdPrioridadeEfetivacaoPagamento() 

    /**
     * Returns the value of field 'cdRastreabilidadeNotaFiscal'.
     * 
     * @return int
     * @return the value of field 'cdRastreabilidadeNotaFiscal'.
     */
    public int getCdRastreabilidadeNotaFiscal()
    {
        return this._cdRastreabilidadeNotaFiscal;
    } //-- int getCdRastreabilidadeNotaFiscal() 

    /**
     * Returns the value of field
     * 'cdRastreabilidadeTituloTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdRastreabilidadeTituloTerceiro'.
     */
    public int getCdRastreabilidadeTituloTerceiro()
    {
        return this._cdRastreabilidadeTituloTerceiro;
    } //-- int getCdRastreabilidadeTituloTerceiro() 

    /**
     * Returns the value of field 'cdRejeicaoAgendamentoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAgendamentoLote'.
     */
    public int getCdRejeicaoAgendamentoLote()
    {
        return this._cdRejeicaoAgendamentoLote;
    } //-- int getCdRejeicaoAgendamentoLote() 

    /**
     * Returns the value of field 'cdRejeicaoEfetivacaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoEfetivacaoLote'.
     */
    public int getCdRejeicaoEfetivacaoLote()
    {
        return this._cdRejeicaoEfetivacaoLote;
    } //-- int getCdRejeicaoEfetivacaoLote() 

    /**
     * Returns the value of field 'cdRejeicaoLote'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoLote'.
     */
    public int getCdRejeicaoLote()
    {
        return this._cdRejeicaoLote;
    } //-- int getCdRejeicaoLote() 

    /**
     * Returns the value of field 'cdTipoCargaRecadastramento'.
     * 
     * @return int
     * @return the value of field 'cdTipoCargaRecadastramento'.
     */
    public int getCdTipoCargaRecadastramento()
    {
        return this._cdTipoCargaRecadastramento;
    } //-- int getCdTipoCargaRecadastramento() 

    /**
     * Returns the value of field 'cdTipoCartaoSalario'.
     * 
     * @return int
     * @return the value of field 'cdTipoCartaoSalario'.
     */
    public int getCdTipoCartaoSalario()
    {
        return this._cdTipoCartaoSalario;
    } //-- int getCdTipoCartaoSalario() 

    /**
     * Returns the value of field 'cdTipoConsistenciaLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsistenciaLista'.
     */
    public int getCdTipoConsistenciaLista()
    {
        return this._cdTipoConsistenciaLista;
    } //-- int getCdTipoConsistenciaLista() 

    /**
     * Returns the value of field 'cdTipoConsultaComprovante'.
     * 
     * @return int
     * @return the value of field 'cdTipoConsultaComprovante'.
     */
    public int getCdTipoConsultaComprovante()
    {
        return this._cdTipoConsultaComprovante;
    } //-- int getCdTipoConsultaComprovante() 

    /**
     * Returns the value of field 'cdTipoDataFloating'.
     * 
     * @return int
     * @return the value of field 'cdTipoDataFloating'.
     */
    public int getCdTipoDataFloating()
    {
        return this._cdTipoDataFloating;
    } //-- int getCdTipoDataFloating() 

    /**
     * Returns the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoDivergenciaVeiculo'.
     */
    public int getCdTipoDivergenciaVeiculo()
    {
        return this._cdTipoDivergenciaVeiculo;
    } //-- int getCdTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'cdTipoEfetivacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoEfetivacaoPagamento'.
     */
    public int getCdTipoEfetivacaoPagamento()
    {
        return this._cdTipoEfetivacaoPagamento;
    } //-- int getCdTipoEfetivacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoFormacaoLista'.
     * 
     * @return int
     * @return the value of field 'cdTipoFormacaoLista'.
     */
    public int getCdTipoFormacaoLista()
    {
        return this._cdTipoFormacaoLista;
    } //-- int getCdTipoFormacaoLista() 

    /**
     * Returns the value of field 'cdTipoIdentificacaoBeneficio'.
     * 
     * @return int
     * @return the value of field 'cdTipoIdentificacaoBeneficio'.
     */
    public int getCdTipoIdentificacaoBeneficio()
    {
        return this._cdTipoIdentificacaoBeneficio;
    } //-- int getCdTipoIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'cdTipoReajusteTarifa'.
     */
    public int getCdTipoReajusteTarifa()
    {
        return this._cdTipoReajusteTarifa;
    } //-- int getCdTipoReajusteTarifa() 

    /**
     * Returns the value of field 'cdTituloDdaRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTituloDdaRetorno'.
     */
    public int getCdTituloDdaRetorno()
    {
        return this._cdTituloDdaRetorno;
    } //-- int getCdTituloDdaRetorno() 

    /**
     * Returns the value of field 'cdTratamentoContaTransferida'.
     * 
     * @return int
     * @return the value of field 'cdTratamentoContaTransferida'.
     */
    public int getCdTratamentoContaTransferida()
    {
        return this._cdTratamentoContaTransferida;
    } //-- int getCdTratamentoContaTransferida() 

    /**
     * Returns the value of field 'cdUsuarioAlteracao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioAlteracao'.
     */
    public java.lang.String getCdUsuarioAlteracao()
    {
        return this._cdUsuarioAlteracao;
    } //-- java.lang.String getCdUsuarioAlteracao() 

    /**
     * Returns the value of field 'cdUsuarioExternoAlteracao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExternoAlteracao'.
     */
    public java.lang.String getCdUsuarioExternoAlteracao()
    {
        return this._cdUsuarioExternoAlteracao;
    } //-- java.lang.String getCdUsuarioExternoAlteracao() 

    /**
     * Returns the value of field 'cdUsuarioExternoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExternoInclusao'.
     */
    public java.lang.String getCdUsuarioExternoInclusao()
    {
        return this._cdUsuarioExternoInclusao;
    } //-- java.lang.String getCdUsuarioExternoInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUtilizacaoFavorecidoControle'.
     * 
     * @return int
     * @return the value of field 'cdUtilizacaoFavorecidoControle'.
     */
    public int getCdUtilizacaoFavorecidoControle()
    {
        return this._cdUtilizacaoFavorecidoControle;
    } //-- int getCdUtilizacaoFavorecidoControle() 

    /**
     * Returns the value of field 'cdindicadorExpiraCredito'.
     * 
     * @return int
     * @return the value of field 'cdindicadorExpiraCredito'.
     */
    public int getCdindicadorExpiraCredito()
    {
        return this._cdindicadorExpiraCredito;
    } //-- int getCdindicadorExpiraCredito() 

    /**
     * Returns the value of field
     * 'cdindicadorLancamentoProgramado'.
     * 
     * @return int
     * @return the value of field 'cdindicadorLancamentoProgramado'.
     */
    public int getCdindicadorLancamentoProgramado()
    {
        return this._cdindicadorLancamentoProgramado;
    } //-- int getCdindicadorLancamentoProgramado() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAcaoNaoVida'.
     * 
     * @return String
     * @return the value of field 'dsAcaoNaoVida'.
     */
    public java.lang.String getDsAcaoNaoVida()
    {
        return this._dsAcaoNaoVida;
    } //-- java.lang.String getDsAcaoNaoVida() 

    /**
     * Returns the value of field 'dsAcertoDadosRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsAcertoDadosRecadastramento'.
     */
    public java.lang.String getDsAcertoDadosRecadastramento()
    {
        return this._dsAcertoDadosRecadastramento;
    } //-- java.lang.String getDsAcertoDadosRecadastramento() 

    /**
     * Returns the value of field 'dsAgendamentoDebitoVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsAgendamentoDebitoVeiculo'.
     */
    public java.lang.String getDsAgendamentoDebitoVeiculo()
    {
        return this._dsAgendamentoDebitoVeiculo;
    } //-- java.lang.String getDsAgendamentoDebitoVeiculo() 

    /**
     * Returns the value of field 'dsAgendamentoPagamentoVencido'.
     * 
     * @return String
     * @return the value of field 'dsAgendamentoPagamentoVencido'.
     */
    public java.lang.String getDsAgendamentoPagamentoVencido()
    {
        return this._dsAgendamentoPagamentoVencido;
    } //-- java.lang.String getDsAgendamentoPagamentoVencido() 

    /**
     * Returns the value of field
     * 'dsAgendamentoRastreabilidadeFinal'.
     * 
     * @return String
     * @return the value of field
     * 'dsAgendamentoRastreabilidadeFinal'.
     */
    public java.lang.String getDsAgendamentoRastreabilidadeFinal()
    {
        return this._dsAgendamentoRastreabilidadeFinal;
    } //-- java.lang.String getDsAgendamentoRastreabilidadeFinal() 

    /**
     * Returns the value of field 'dsAgendamentoValorMenor'.
     * 
     * @return String
     * @return the value of field 'dsAgendamentoValorMenor'.
     */
    public java.lang.String getDsAgendamentoValorMenor()
    {
        return this._dsAgendamentoValorMenor;
    } //-- java.lang.String getDsAgendamentoValorMenor() 

    /**
     * Returns the value of field 'dsAgrupamentoAviso'.
     * 
     * @return String
     * @return the value of field 'dsAgrupamentoAviso'.
     */
    public java.lang.String getDsAgrupamentoAviso()
    {
        return this._dsAgrupamentoAviso;
    } //-- java.lang.String getDsAgrupamentoAviso() 

    /**
     * Returns the value of field 'dsAgrupamentoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsAgrupamentoComprovante'.
     */
    public java.lang.String getDsAgrupamentoComprovante()
    {
        return this._dsAgrupamentoComprovante;
    } //-- java.lang.String getDsAgrupamentoComprovante() 

    /**
     * Returns the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     */
    public java.lang.String getDsAgrupamentoFormularioRecadastro()
    {
        return this._dsAgrupamentoFormularioRecadastro;
    } //-- java.lang.String getDsAgrupamentoFormularioRecadastro() 

    /**
     * Returns the value of field
     * 'dsAntecipacaoRecadastramentoBeneficiario'.
     * 
     * @return String
     * @return the value of field
     * 'dsAntecipacaoRecadastramentoBeneficiario'.
     */
    public java.lang.String getDsAntecipacaoRecadastramentoBeneficiario()
    {
        return this._dsAntecipacaoRecadastramentoBeneficiario;
    } //-- java.lang.String getDsAntecipacaoRecadastramentoBeneficiario() 

    /**
     * Returns the value of field 'dsAreaReservada'.
     * 
     * @return String
     * @return the value of field 'dsAreaReservada'.
     */
    public java.lang.String getDsAreaReservada()
    {
        return this._dsAreaReservada;
    } //-- java.lang.String getDsAreaReservada() 

    /**
     * Returns the value of field 'dsAreaReservada2'.
     * 
     * @return String
     * @return the value of field 'dsAreaReservada2'.
     */
    public java.lang.String getDsAreaReservada2()
    {
        return this._dsAreaReservada2;
    } //-- java.lang.String getDsAreaReservada2() 

    /**
     * Returns the value of field 'dsBaseRecadastramentoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsBaseRecadastramentoBeneficio'.
     */
    public java.lang.String getDsBaseRecadastramentoBeneficio()
    {
        return this._dsBaseRecadastramentoBeneficio;
    } //-- java.lang.String getDsBaseRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'dsBloqueioEmissaoPapeleta'.
     * 
     * @return String
     * @return the value of field 'dsBloqueioEmissaoPapeleta'.
     */
    public java.lang.String getDsBloqueioEmissaoPapeleta()
    {
        return this._dsBloqueioEmissaoPapeleta;
    } //-- java.lang.String getDsBloqueioEmissaoPapeleta() 

    /**
     * Returns the value of field 'dsCanalAlteracao'.
     * 
     * @return String
     * @return the value of field 'dsCanalAlteracao'.
     */
    public java.lang.String getDsCanalAlteracao()
    {
        return this._dsCanalAlteracao;
    } //-- java.lang.String getDsCanalAlteracao() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCapturaTituloRegistrado'.
     * 
     * @return String
     * @return the value of field 'dsCapturaTituloRegistrado'.
     */
    public java.lang.String getDsCapturaTituloRegistrado()
    {
        return this._dsCapturaTituloRegistrado;
    } //-- java.lang.String getDsCapturaTituloRegistrado() 

    /**
     * Returns the value of field 'dsCctciaEspeBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCctciaEspeBeneficio'.
     */
    public java.lang.String getDsCctciaEspeBeneficio()
    {
        return this._dsCctciaEspeBeneficio;
    } //-- java.lang.String getDsCctciaEspeBeneficio() 

    /**
     * Returns the value of field 'dsCctciaIdentificacaoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCctciaIdentificacaoBeneficio'.
     */
    public java.lang.String getDsCctciaIdentificacaoBeneficio()
    {
        return this._dsCctciaIdentificacaoBeneficio;
    } //-- java.lang.String getDsCctciaIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'dsCctciaInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsCctciaInscricaoFavorecido'.
     */
    public java.lang.String getDsCctciaInscricaoFavorecido()
    {
        return this._dsCctciaInscricaoFavorecido;
    } //-- java.lang.String getDsCctciaInscricaoFavorecido() 

    /**
     * Returns the value of field 'dsCctciaProprietarioVeculo'.
     * 
     * @return String
     * @return the value of field 'dsCctciaProprietarioVeculo'.
     */
    public java.lang.String getDsCctciaProprietarioVeculo()
    {
        return this._dsCctciaProprietarioVeculo;
    } //-- java.lang.String getDsCctciaProprietarioVeculo() 

    /**
     * Returns the value of field 'dsCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'dsCobrancaTarifa'.
     */
    public java.lang.String getDsCobrancaTarifa()
    {
        return this._dsCobrancaTarifa;
    } //-- java.lang.String getDsCobrancaTarifa() 

    /**
     * Returns the value of field
     * 'dsCodIdentificadortipoRetornoInternet'.
     * 
     * @return String
     * @return the value of field
     * 'dsCodIdentificadortipoRetornoInternet'.
     */
    public java.lang.String getDsCodIdentificadortipoRetornoInternet()
    {
        return this._dsCodIdentificadorTipoRetornoInternet;
    } //-- java.lang.String getDsCodIdentificadortipoRetornoInternet() 

    /**
     * Returns the value of field 'dsCodigoIndFeriadoLocal'.
     * 
     * @return String
     * @return the value of field 'dsCodigoIndFeriadoLocal'.
     */
    public java.lang.String getDsCodigoIndFeriadoLocal()
    {
        return this._dsCodigoIndFeriadoLocal;
    } //-- java.lang.String getDsCodigoIndFeriadoLocal() 

    /**
     * Returns the value of field
     * 'dsConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @return String
     * @return the value of field
     * 'dsConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public java.lang.String getDsConsistenciaCpfCnpjBenefAvalNpc()
    {
        return this._dsConsistenciaCpfCnpjBenefAvalNpc;
    } //-- java.lang.String getDsConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Returns the value of field 'dsConsultaDebitoVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsConsultaDebitoVeiculo'.
     */
    public java.lang.String getDsConsultaDebitoVeiculo()
    {
        return this._dsConsultaDebitoVeiculo;
    } //-- java.lang.String getDsConsultaDebitoVeiculo() 

    /**
     * Returns the value of field 'dsConsultaEndereco'.
     * 
     * @return String
     * @return the value of field 'dsConsultaEndereco'.
     */
    public java.lang.String getDsConsultaEndereco()
    {
        return this._dsConsultaEndereco;
    } //-- java.lang.String getDsConsultaEndereco() 

    /**
     * Returns the value of field 'dsConsultaSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsConsultaSaldoPagamento'.
     */
    public java.lang.String getDsConsultaSaldoPagamento()
    {
        return this._dsConsultaSaldoPagamento;
    } //-- java.lang.String getDsConsultaSaldoPagamento() 

    /**
     * Returns the value of field 'dsConsultaSaldoValorSuperior'.
     * 
     * @return String
     * @return the value of field 'dsConsultaSaldoValorSuperior'.
     */
    public java.lang.String getDsConsultaSaldoValorSuperior()
    {
        return this._dsConsultaSaldoValorSuperior;
    } //-- java.lang.String getDsConsultaSaldoValorSuperior() 

    /**
     * Returns the value of field 'dsContagemConsultaSaldo'.
     * 
     * @return String
     * @return the value of field 'dsContagemConsultaSaldo'.
     */
    public java.lang.String getDsContagemConsultaSaldo()
    {
        return this._dsContagemConsultaSaldo;
    } //-- java.lang.String getDsContagemConsultaSaldo() 

    /**
     * Returns the value of field 'dsCreditoNaoUtilizado'.
     * 
     * @return String
     * @return the value of field 'dsCreditoNaoUtilizado'.
     */
    public java.lang.String getDsCreditoNaoUtilizado()
    {
        return this._dsCreditoNaoUtilizado;
    } //-- java.lang.String getDsCreditoNaoUtilizado() 

    /**
     * Returns the value of field
     * 'dsCriterioEnquadraRecadastramento'.
     * 
     * @return String
     * @return the value of field
     * 'dsCriterioEnquadraRecadastramento'.
     */
    public java.lang.String getDsCriterioEnquadraRecadastramento()
    {
        return this._dsCriterioEnquadraRecadastramento;
    } //-- java.lang.String getDsCriterioEnquadraRecadastramento() 

    /**
     * Returns the value of field 'dsCriterioEnquandraBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsCriterioEnquandraBeneficio'.
     */
    public java.lang.String getDsCriterioEnquandraBeneficio()
    {
        return this._dsCriterioEnquandraBeneficio;
    } //-- java.lang.String getDsCriterioEnquandraBeneficio() 

    /**
     * Returns the value of field
     * 'dsCriterioRastreabilidadeTitulo'.
     * 
     * @return String
     * @return the value of field 'dsCriterioRastreabilidadeTitulo'.
     */
    public java.lang.String getDsCriterioRastreabilidadeTitulo()
    {
        return this._dsCriterioRastreabilidadeTitulo;
    } //-- java.lang.String getDsCriterioRastreabilidadeTitulo() 

    /**
     * Returns the value of field 'dsDestinoAviso'.
     * 
     * @return String
     * @return the value of field 'dsDestinoAviso'.
     */
    public java.lang.String getDsDestinoAviso()
    {
        return this._dsDestinoAviso;
    } //-- java.lang.String getDsDestinoAviso() 

    /**
     * Returns the value of field 'dsDestinoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsDestinoComprovante'.
     */
    public java.lang.String getDsDestinoComprovante()
    {
        return this._dsDestinoComprovante;
    } //-- java.lang.String getDsDestinoComprovante() 

    /**
     * Returns the value of field
     * 'dsDestinoFormularioRecadastramento'.
     * 
     * @return String
     * @return the value of field
     * 'dsDestinoFormularioRecadastramento'.
     */
    public java.lang.String getDsDestinoFormularioRecadastramento()
    {
        return this._dsDestinoFormularioRecadastramento;
    } //-- java.lang.String getDsDestinoFormularioRecadastramento() 

    /**
     * Returns the value of field 'dsDisponibilizacaoContaCredito'.
     * 
     * @return String
     * @return the value of field 'dsDisponibilizacaoContaCredito'.
     */
    public java.lang.String getDsDisponibilizacaoContaCredito()
    {
        return this._dsDisponibilizacaoContaCredito;
    } //-- java.lang.String getDsDisponibilizacaoContaCredito() 

    /**
     * Returns the value of field
     * 'dsDisponibilizacaoDiversoCriterio'.
     * 
     * @return String
     * @return the value of field
     * 'dsDisponibilizacaoDiversoCriterio'.
     */
    public java.lang.String getDsDisponibilizacaoDiversoCriterio()
    {
        return this._dsDisponibilizacaoDiversoCriterio;
    } //-- java.lang.String getDsDisponibilizacaoDiversoCriterio() 

    /**
     * Returns the value of field 'dsDisponibilizacaoDiversoNao'.
     * 
     * @return String
     * @return the value of field 'dsDisponibilizacaoDiversoNao'.
     */
    public java.lang.String getDsDisponibilizacaoDiversoNao()
    {
        return this._dsDisponibilizacaoDiversoNao;
    } //-- java.lang.String getDsDisponibilizacaoDiversoNao() 

    /**
     * Returns the value of field
     * 'dsDisponibilizacaoSalarioCriterio'.
     * 
     * @return String
     * @return the value of field
     * 'dsDisponibilizacaoSalarioCriterio'.
     */
    public java.lang.String getDsDisponibilizacaoSalarioCriterio()
    {
        return this._dsDisponibilizacaoSalarioCriterio;
    } //-- java.lang.String getDsDisponibilizacaoSalarioCriterio() 

    /**
     * Returns the value of field 'dsDisponibilizacaoSalarioNao'.
     * 
     * @return String
     * @return the value of field 'dsDisponibilizacaoSalarioNao'.
     */
    public java.lang.String getDsDisponibilizacaoSalarioNao()
    {
        return this._dsDisponibilizacaoSalarioNao;
    } //-- java.lang.String getDsDisponibilizacaoSalarioNao() 

    /**
     * Returns the value of field 'dsEnvelopeAberto'.
     * 
     * @return String
     * @return the value of field 'dsEnvelopeAberto'.
     */
    public java.lang.String getDsEnvelopeAberto()
    {
        return this._dsEnvelopeAberto;
    } //-- java.lang.String getDsEnvelopeAberto() 

    /**
     * Returns the value of field 'dsExigeFilial'.
     * 
     * @return String
     * @return the value of field 'dsExigeFilial'.
     */
    public java.lang.String getDsExigeFilial()
    {
        return this._dsExigeFilial;
    } //-- java.lang.String getDsExigeFilial() 

    /**
     * Returns the value of field 'dsFavorecidoConsultaPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFavorecidoConsultaPagamento'.
     */
    public java.lang.String getDsFavorecidoConsultaPagamento()
    {
        return this._dsFavorecidoConsultaPagamento;
    } //-- java.lang.String getDsFavorecidoConsultaPagamento() 

    /**
     * Returns the value of field 'dsFloatServicoContrato'.
     * 
     * @return String
     * @return the value of field 'dsFloatServicoContrato'.
     */
    public java.lang.String getDsFloatServicoContrato()
    {
        return this._dsFloatServicoContrato;
    } //-- java.lang.String getDsFloatServicoContrato() 

    /**
     * Returns the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsFormaAutorizacaoPagamento()
    {
        return this._dsFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'dsFormaEnvioPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaEnvioPagamento'.
     */
    public java.lang.String getDsFormaEnvioPagamento()
    {
        return this._dsFormaEnvioPagamento;
    } //-- java.lang.String getDsFormaEnvioPagamento() 

    /**
     * Returns the value of field 'dsFormaEstornoCredito'.
     * 
     * @return String
     * @return the value of field 'dsFormaEstornoCredito'.
     */
    public java.lang.String getDsFormaEstornoCredito()
    {
        return this._dsFormaEstornoCredito;
    } //-- java.lang.String getDsFormaEstornoCredito() 

    /**
     * Returns the value of field 'dsFormaExpiracaoCredito'.
     * 
     * @return String
     * @return the value of field 'dsFormaExpiracaoCredito'.
     */
    public java.lang.String getDsFormaExpiracaoCredito()
    {
        return this._dsFormaExpiracaoCredito;
    } //-- java.lang.String getDsFormaExpiracaoCredito() 

    /**
     * Returns the value of field 'dsFormaManutencao'.
     * 
     * @return String
     * @return the value of field 'dsFormaManutencao'.
     */
    public java.lang.String getDsFormaManutencao()
    {
        return this._dsFormaManutencao;
    } //-- java.lang.String getDsFormaManutencao() 

    /**
     * Returns the value of field 'dsFrasePrecadastrada'.
     * 
     * @return String
     * @return the value of field 'dsFrasePrecadastrada'.
     */
    public java.lang.String getDsFrasePrecadastrada()
    {
        return this._dsFrasePrecadastrada;
    } //-- java.lang.String getDsFrasePrecadastrada() 

    /**
     * Returns the value of field 'dsIndicador'.
     * 
     * @return String
     * @return the value of field 'dsIndicador'.
     */
    public java.lang.String getDsIndicador()
    {
        return this._dsIndicador;
    } //-- java.lang.String getDsIndicador() 

    /**
     * Returns the value of field 'dsIndicadorAgendaGrade'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAgendaGrade'.
     */
    public java.lang.String getDsIndicadorAgendaGrade()
    {
        return this._dsIndicadorAgendaGrade;
    } //-- java.lang.String getDsIndicadorAgendaGrade() 

    /**
     * Returns the value of field 'dsIndicadorAgendamentoTitulo'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAgendamentoTitulo'.
     */
    public java.lang.String getDsIndicadorAgendamentoTitulo()
    {
        return this._dsIndicadorAgendamentoTitulo;
    } //-- java.lang.String getDsIndicadorAgendamentoTitulo() 

    /**
     * Returns the value of field 'dsIndicadorAutorizacaoCliente'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAutorizacaoCliente'.
     */
    public java.lang.String getDsIndicadorAutorizacaoCliente()
    {
        return this._dsIndicadorAutorizacaoCliente;
    } //-- java.lang.String getDsIndicadorAutorizacaoCliente() 

    /**
     * Returns the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     * 
     * @return String
     * @return the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     */
    public java.lang.String getDsIndicadorAutorizacaoComplemento()
    {
        return this._dsIndicadorAutorizacaoComplemento;
    } //-- java.lang.String getDsIndicadorAutorizacaoComplemento() 

    /**
     * Returns the value of field 'dsIndicadorBancoPostal'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorBancoPostal'.
     */
    public java.lang.String getDsIndicadorBancoPostal()
    {
        return this._dsIndicadorBancoPostal;
    } //-- java.lang.String getDsIndicadorBancoPostal() 

    /**
     * Returns the value of field 'dsIndicadorCadastroProcurador'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCadastroProcurador'.
     */
    public java.lang.String getDsIndicadorCadastroProcurador()
    {
        return this._dsIndicadorCadastroProcurador;
    } //-- java.lang.String getDsIndicadorCadastroProcurador() 

    /**
     * Returns the value of field 'dsIndicadorCadastroorganizacao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCadastroorganizacao'.
     */
    public java.lang.String getDsIndicadorCadastroorganizacao()
    {
        return this._dsIndicadorCadastroorganizacao;
    } //-- java.lang.String getDsIndicadorCadastroorganizacao() 

    /**
     * Returns the value of field 'dsIndicadorCartaoSalario'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCartaoSalario'.
     */
    public java.lang.String getDsIndicadorCartaoSalario()
    {
        return this._dsIndicadorCartaoSalario;
    } //-- java.lang.String getDsIndicadorCartaoSalario() 

    /**
     * Returns the value of field 'dsIndicadorEconomicoReajuste'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorEconomicoReajuste'.
     */
    public java.lang.String getDsIndicadorEconomicoReajuste()
    {
        return this._dsIndicadorEconomicoReajuste;
    } //-- java.lang.String getDsIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'dsIndicadorListaDebito'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorListaDebito'.
     */
    public java.lang.String getDsIndicadorListaDebito()
    {
        return this._dsIndicadorListaDebito;
    } //-- java.lang.String getDsIndicadorListaDebito() 

    /**
     * Returns the value of field
     * 'dsIndicadorMensagemPersonalizada'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorMensagemPersonalizada'
     */
    public java.lang.String getDsIndicadorMensagemPersonalizada()
    {
        return this._dsIndicadorMensagemPersonalizada;
    } //-- java.lang.String getDsIndicadorMensagemPersonalizada() 

    /**
     * Returns the value of field 'dsIndicadorRetornoInternet'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorRetornoInternet'.
     */
    public java.lang.String getDsIndicadorRetornoInternet()
    {
        return this._dsIndicadorRetornoInternet;
    } //-- java.lang.String getDsIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'dsIndicadorSegundaLinha'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorSegundaLinha'.
     */
    public java.lang.String getDsIndicadorSegundaLinha()
    {
        return this._dsIndicadorSegundaLinha;
    } //-- java.lang.String getDsIndicadorSegundaLinha() 

    /**
     * Returns the value of field 'dsIndicadorUtilizaMora'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorUtilizaMora'.
     */
    public java.lang.String getDsIndicadorUtilizaMora()
    {
        return this._dsIndicadorUtilizaMora;
    } //-- java.lang.String getDsIndicadorUtilizaMora() 

    /**
     * Returns the value of field 'dsLancamentoFuturoCredito'.
     * 
     * @return String
     * @return the value of field 'dsLancamentoFuturoCredito'.
     */
    public java.lang.String getDsLancamentoFuturoCredito()
    {
        return this._dsLancamentoFuturoCredito;
    } //-- java.lang.String getDsLancamentoFuturoCredito() 

    /**
     * Returns the value of field 'dsLancamentoFuturoDebito'.
     * 
     * @return String
     * @return the value of field 'dsLancamentoFuturoDebito'.
     */
    public java.lang.String getDsLancamentoFuturoDebito()
    {
        return this._dsLancamentoFuturoDebito;
    } //-- java.lang.String getDsLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'dsLiberacaoLoteProcessado'.
     * 
     * @return String
     * @return the value of field 'dsLiberacaoLoteProcessado'.
     */
    public java.lang.String getDsLiberacaoLoteProcessado()
    {
        return this._dsLiberacaoLoteProcessado;
    } //-- java.lang.String getDsLiberacaoLoteProcessado() 

    /**
     * Returns the value of field
     * 'dsManutencaoBaseRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsManutencaoBaseRecadastramento'.
     */
    public java.lang.String getDsManutencaoBaseRecadastramento()
    {
        return this._dsManutencaoBaseRecadastramento;
    } //-- java.lang.String getDsManutencaoBaseRecadastramento() 

    /**
     * Returns the value of field 'dsMeioPagamentoDebito'.
     * 
     * @return String
     * @return the value of field 'dsMeioPagamentoDebito'.
     */
    public java.lang.String getDsMeioPagamentoDebito()
    {
        return this._dsMeioPagamentoDebito;
    } //-- java.lang.String getDsMeioPagamentoDebito() 

    /**
     * Returns the value of field 'dsMensagemRecadastramentoMidia'.
     * 
     * @return String
     * @return the value of field 'dsMensagemRecadastramentoMidia'.
     */
    public java.lang.String getDsMensagemRecadastramentoMidia()
    {
        return this._dsMensagemRecadastramentoMidia;
    } //-- java.lang.String getDsMensagemRecadastramentoMidia() 

    /**
     * Returns the value of field 'dsMidiaDisponivel'.
     * 
     * @return String
     * @return the value of field 'dsMidiaDisponivel'.
     */
    public java.lang.String getDsMidiaDisponivel()
    {
        return this._dsMidiaDisponivel;
    } //-- java.lang.String getDsMidiaDisponivel() 

    /**
     * Returns the value of field 'dsMidiaMensagemRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsMidiaMensagemRecadastramento'.
     */
    public java.lang.String getDsMidiaMensagemRecadastramento()
    {
        return this._dsMidiaMensagemRecadastramento;
    } //-- java.lang.String getDsMidiaMensagemRecadastramento() 

    /**
     * Returns the value of field 'dsMomentoAvisoRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoAvisoRecadastramento'.
     */
    public java.lang.String getDsMomentoAvisoRecadastramento()
    {
        return this._dsMomentoAvisoRecadastramento;
    } //-- java.lang.String getDsMomentoAvisoRecadastramento() 

    /**
     * Returns the value of field 'dsMomentoCreditoEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dsMomentoCreditoEfetivacao'.
     */
    public java.lang.String getDsMomentoCreditoEfetivacao()
    {
        return this._dsMomentoCreditoEfetivacao;
    } //-- java.lang.String getDsMomentoCreditoEfetivacao() 

    /**
     * Returns the value of field 'dsMomentoDebitoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoDebitoPagamento'.
     */
    public java.lang.String getDsMomentoDebitoPagamento()
    {
        return this._dsMomentoDebitoPagamento;
    } //-- java.lang.String getDsMomentoDebitoPagamento() 

    /**
     * Returns the value of field
     * 'dsMomentoFormularioRecadastramento'.
     * 
     * @return String
     * @return the value of field
     * 'dsMomentoFormularioRecadastramento'.
     */
    public java.lang.String getDsMomentoFormularioRecadastramento()
    {
        return this._dsMomentoFormularioRecadastramento;
    } //-- java.lang.String getDsMomentoFormularioRecadastramento() 

    /**
     * Returns the value of field
     * 'dsMomentoProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMomentoProcessamentoPagamento'.
     */
    public java.lang.String getDsMomentoProcessamentoPagamento()
    {
        return this._dsMomentoProcessamentoPagamento;
    } //-- java.lang.String getDsMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field 'dsNaturezaOperacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsNaturezaOperacaoPagamento'.
     */
    public java.lang.String getDsNaturezaOperacaoPagamento()
    {
        return this._dsNaturezaOperacaoPagamento;
    } //-- java.lang.String getDsNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field 'dsOrigemIndicador'.
     * 
     * @return int
     * @return the value of field 'dsOrigemIndicador'.
     */
    public int getDsOrigemIndicador()
    {
        return this._dsOrigemIndicador;
    } //-- int getDsOrigemIndicador() 

    /**
     * Returns the value of field 'dsOutraidentificacaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsOutraidentificacaoFavorecido'.
     */
    public java.lang.String getDsOutraidentificacaoFavorecido()
    {
        return this._dsOutraidentificacaoFavorecido;
    } //-- java.lang.String getDsOutraidentificacaoFavorecido() 

    /**
     * Returns the value of field 'dsPagamentoNaoUtil'.
     * 
     * @return String
     * @return the value of field 'dsPagamentoNaoUtil'.
     */
    public java.lang.String getDsPagamentoNaoUtil()
    {
        return this._dsPagamentoNaoUtil;
    } //-- java.lang.String getDsPagamentoNaoUtil() 

    /**
     * Returns the value of field 'dsPeriodicidadeAviso'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeAviso'.
     */
    public java.lang.String getDsPeriodicidadeAviso()
    {
        return this._dsPeriodicidadeAviso;
    } //-- java.lang.String getDsPeriodicidadeAviso() 

    /**
     * Returns the value of field 'dsPeriodicidadeCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeCobrancaTarifa'.
     */
    public java.lang.String getDsPeriodicidadeCobrancaTarifa()
    {
        return this._dsPeriodicidadeCobrancaTarifa;
    } //-- java.lang.String getDsPeriodicidadeCobrancaTarifa() 

    /**
     * Returns the value of field 'dsPeriodicidadeComprovante'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeComprovante'.
     */
    public java.lang.String getDsPeriodicidadeComprovante()
    {
        return this._dsPeriodicidadeComprovante;
    } //-- java.lang.String getDsPeriodicidadeComprovante() 

    /**
     * Returns the value of field 'dsPeriodicidadeConsultaVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeConsultaVeiculo'.
     */
    public java.lang.String getDsPeriodicidadeConsultaVeiculo()
    {
        return this._dsPeriodicidadeConsultaVeiculo;
    } //-- java.lang.String getDsPeriodicidadeConsultaVeiculo() 

    /**
     * Returns the value of field 'dsPeriodicidadeEnvioRemessa'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeEnvioRemessa'.
     */
    public java.lang.String getDsPeriodicidadeEnvioRemessa()
    {
        return this._dsPeriodicidadeEnvioRemessa;
    } //-- java.lang.String getDsPeriodicidadeEnvioRemessa() 

    /**
     * Returns the value of field 'dsPeriodicidadeManutencaoProcd'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeManutencaoProcd'.
     */
    public java.lang.String getDsPeriodicidadeManutencaoProcd()
    {
        return this._dsPeriodicidadeManutencaoProcd;
    } //-- java.lang.String getDsPeriodicidadeManutencaoProcd() 

    /**
     * Returns the value of field 'dsPeriodicidadeReajusteTarifa'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeReajusteTarifa'.
     */
    public java.lang.String getDsPeriodicidadeReajusteTarifa()
    {
        return this._dsPeriodicidadeReajusteTarifa;
    } //-- java.lang.String getDsPeriodicidadeReajusteTarifa() 

    /**
     * Returns the value of field 'dsPermissaoDebitoOnline'.
     * 
     * @return String
     * @return the value of field 'dsPermissaoDebitoOnline'.
     */
    public java.lang.String getDsPermissaoDebitoOnline()
    {
        return this._dsPermissaoDebitoOnline;
    } //-- java.lang.String getDsPermissaoDebitoOnline() 

    /**
     * Returns the value of field
     * 'dsPreenchimentoLancamentoPersonalizado'.
     * 
     * @return String
     * @return the value of field
     * 'dsPreenchimentoLancamentoPersonalizado'.
     */
    public java.lang.String getDsPreenchimentoLancamentoPersonalizado()
    {
        return this._dsPreenchimentoLancamentoPersonalizado;
    } //-- java.lang.String getDsPreenchimentoLancamentoPersonalizado() 

    /**
     * Returns the value of field
     * 'dsPrincipalEnquaRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsPrincipalEnquaRecadastramento'.
     */
    public java.lang.String getDsPrincipalEnquaRecadastramento()
    {
        return this._dsPrincipalEnquaRecadastramento;
    } //-- java.lang.String getDsPrincipalEnquaRecadastramento() 

    /**
     * Returns the value of field
     * 'dsPrioridadeEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsPrioridadeEfetivacaoPagamento'.
     */
    public java.lang.String getDsPrioridadeEfetivacaoPagamento()
    {
        return this._dsPrioridadeEfetivacaoPagamento;
    } //-- java.lang.String getDsPrioridadeEfetivacaoPagamento() 

    /**
     * Returns the value of field 'dsRastreabilidadeNotaFiscal'.
     * 
     * @return String
     * @return the value of field 'dsRastreabilidadeNotaFiscal'.
     */
    public java.lang.String getDsRastreabilidadeNotaFiscal()
    {
        return this._dsRastreabilidadeNotaFiscal;
    } //-- java.lang.String getDsRastreabilidadeNotaFiscal() 

    /**
     * Returns the value of field
     * 'dsRastreabilidadeTituloTerceiro'.
     * 
     * @return String
     * @return the value of field 'dsRastreabilidadeTituloTerceiro'.
     */
    public java.lang.String getDsRastreabilidadeTituloTerceiro()
    {
        return this._dsRastreabilidadeTituloTerceiro;
    } //-- java.lang.String getDsRastreabilidadeTituloTerceiro() 

    /**
     * Returns the value of field 'dsRejeicaoAgendamentoLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoAgendamentoLote'.
     */
    public java.lang.String getDsRejeicaoAgendamentoLote()
    {
        return this._dsRejeicaoAgendamentoLote;
    } //-- java.lang.String getDsRejeicaoAgendamentoLote() 

    /**
     * Returns the value of field 'dsRejeicaoEfetivacaoLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoEfetivacaoLote'.
     */
    public java.lang.String getDsRejeicaoEfetivacaoLote()
    {
        return this._dsRejeicaoEfetivacaoLote;
    } //-- java.lang.String getDsRejeicaoEfetivacaoLote() 

    /**
     * Returns the value of field 'dsRejeicaoLote'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoLote'.
     */
    public java.lang.String getDsRejeicaoLote()
    {
        return this._dsRejeicaoLote;
    } //-- java.lang.String getDsRejeicaoLote() 

    /**
     * Returns the value of field 'dsSituacaoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoServicoRelacionado'.
     */
    public java.lang.String getDsSituacaoServicoRelacionado()
    {
        return this._dsSituacaoServicoRelacionado;
    } //-- java.lang.String getDsSituacaoServicoRelacionado() 

    /**
     * Returns the value of field 'dsTipoCargaRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dsTipoCargaRecadastramento'.
     */
    public java.lang.String getDsTipoCargaRecadastramento()
    {
        return this._dsTipoCargaRecadastramento;
    } //-- java.lang.String getDsTipoCargaRecadastramento() 

    /**
     * Returns the value of field 'dsTipoCartaoSalario'.
     * 
     * @return String
     * @return the value of field 'dsTipoCartaoSalario'.
     */
    public java.lang.String getDsTipoCartaoSalario()
    {
        return this._dsTipoCartaoSalario;
    } //-- java.lang.String getDsTipoCartaoSalario() 

    /**
     * Returns the value of field 'dsTipoConsistenciaLista'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsistenciaLista'.
     */
    public java.lang.String getDsTipoConsistenciaLista()
    {
        return this._dsTipoConsistenciaLista;
    } //-- java.lang.String getDsTipoConsistenciaLista() 

    /**
     * Returns the value of field 'dsTipoConsolidacaoComprovante'.
     * 
     * @return String
     * @return the value of field 'dsTipoConsolidacaoComprovante'.
     */
    public java.lang.String getDsTipoConsolidacaoComprovante()
    {
        return this._dsTipoConsolidacaoComprovante;
    } //-- java.lang.String getDsTipoConsolidacaoComprovante() 

    /**
     * Returns the value of field 'dsTipoDataFloating'.
     * 
     * @return String
     * @return the value of field 'dsTipoDataFloating'.
     */
    public java.lang.String getDsTipoDataFloating()
    {
        return this._dsTipoDataFloating;
    } //-- java.lang.String getDsTipoDataFloating() 

    /**
     * Returns the value of field 'dsTipoDivergenciaVeiculo'.
     * 
     * @return String
     * @return the value of field 'dsTipoDivergenciaVeiculo'.
     */
    public java.lang.String getDsTipoDivergenciaVeiculo()
    {
        return this._dsTipoDivergenciaVeiculo;
    } //-- java.lang.String getDsTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'dsTipoEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsTipoEfetivacaoPagamento'.
     */
    public java.lang.String getDsTipoEfetivacaoPagamento()
    {
        return this._dsTipoEfetivacaoPagamento;
    } //-- java.lang.String getDsTipoEfetivacaoPagamento() 

    /**
     * Returns the value of field 'dsTipoFormacaoLista'.
     * 
     * @return String
     * @return the value of field 'dsTipoFormacaoLista'.
     */
    public java.lang.String getDsTipoFormacaoLista()
    {
        return this._dsTipoFormacaoLista;
    } //-- java.lang.String getDsTipoFormacaoLista() 

    /**
     * Returns the value of field 'dsTipoIdentificacaoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dsTipoIdentificacaoBeneficio'.
     */
    public java.lang.String getDsTipoIdentificacaoBeneficio()
    {
        return this._dsTipoIdentificacaoBeneficio;
    } //-- java.lang.String getDsTipoIdentificacaoBeneficio() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoReajusteTarifa'.
     * 
     * @return String
     * @return the value of field 'dsTipoReajusteTarifa'.
     */
    public java.lang.String getDsTipoReajusteTarifa()
    {
        return this._dsTipoReajusteTarifa;
    } //-- java.lang.String getDsTipoReajusteTarifa() 

    /**
     * Returns the value of field 'dsTituloDdaRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTituloDdaRetorno'.
     */
    public java.lang.String getDsTituloDdaRetorno()
    {
        return this._dsTituloDdaRetorno;
    } //-- java.lang.String getDsTituloDdaRetorno() 

    /**
     * Returns the value of field 'dsTratamentoContaTransferida'.
     * 
     * @return String
     * @return the value of field 'dsTratamentoContaTransferida'.
     */
    public java.lang.String getDsTratamentoContaTransferida()
    {
        return this._dsTratamentoContaTransferida;
    } //-- java.lang.String getDsTratamentoContaTransferida() 

    /**
     * Returns the value of field 'dsUtilizacaoFavorecidoControle'.
     * 
     * @return String
     * @return the value of field 'dsUtilizacaoFavorecidoControle'.
     */
    public java.lang.String getDsUtilizacaoFavorecidoControle()
    {
        return this._dsUtilizacaoFavorecidoControle;
    } //-- java.lang.String getDsUtilizacaoFavorecidoControle() 

    /**
     * Returns the value of field 'dsindicadorExpiraCredito'.
     * 
     * @return String
     * @return the value of field 'dsindicadorExpiraCredito'.
     */
    public java.lang.String getDsindicadorExpiraCredito()
    {
        return this._dsindicadorExpiraCredito;
    } //-- java.lang.String getDsindicadorExpiraCredito() 

    /**
     * Returns the value of field
     * 'dsindicadorLancamentoProgramado'.
     * 
     * @return String
     * @return the value of field 'dsindicadorLancamentoProgramado'.
     */
    public java.lang.String getDsindicadorLancamentoProgramado()
    {
        return this._dsindicadorLancamentoProgramado;
    } //-- java.lang.String getDsindicadorLancamentoProgramado() 

    /**
     * Returns the value of field 'dtEnquaContaSalario'.
     * 
     * @return String
     * @return the value of field 'dtEnquaContaSalario'.
     */
    public java.lang.String getDtEnquaContaSalario()
    {
        return this._dtEnquaContaSalario;
    } //-- java.lang.String getDtEnquaContaSalario() 

    /**
     * Returns the value of field 'dtFimAcertoRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dtFimAcertoRecadastramento'.
     */
    public java.lang.String getDtFimAcertoRecadastramento()
    {
        return this._dtFimAcertoRecadastramento;
    } //-- java.lang.String getDtFimAcertoRecadastramento() 

    /**
     * Returns the value of field 'dtFimRecadastramentoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dtFimRecadastramentoBeneficio'.
     */
    public java.lang.String getDtFimRecadastramentoBeneficio()
    {
        return this._dtFimRecadastramentoBeneficio;
    } //-- java.lang.String getDtFimRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'dtInicioAcertoRecadastramento'.
     * 
     * @return String
     * @return the value of field 'dtInicioAcertoRecadastramento'.
     */
    public java.lang.String getDtInicioAcertoRecadastramento()
    {
        return this._dtInicioAcertoRecadastramento;
    } //-- java.lang.String getDtInicioAcertoRecadastramento() 

    /**
     * Returns the value of field 'dtInicioBloqueioPapeleta'.
     * 
     * @return String
     * @return the value of field 'dtInicioBloqueioPapeleta'.
     */
    public java.lang.String getDtInicioBloqueioPapeleta()
    {
        return this._dtInicioBloqueioPapeleta;
    } //-- java.lang.String getDtInicioBloqueioPapeleta() 

    /**
     * Returns the value of field 'dtInicioRastreabilidadeTitulo'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreabilidadeTitulo'.
     */
    public java.lang.String getDtInicioRastreabilidadeTitulo()
    {
        return this._dtInicioRastreabilidadeTitulo;
    } //-- java.lang.String getDtInicioRastreabilidadeTitulo() 

    /**
     * Returns the value of field
     * 'dtInicioRecadastramentoBeneficio'.
     * 
     * @return String
     * @return the value of field 'dtInicioRecadastramentoBeneficio'
     */
    public java.lang.String getDtInicioRecadastramentoBeneficio()
    {
        return this._dtInicioRecadastramentoBeneficio;
    } //-- java.lang.String getDtInicioRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'dtLimiteVinculoCarga'.
     * 
     * @return String
     * @return the value of field 'dtLimiteVinculoCarga'.
     */
    public java.lang.String getDtLimiteVinculoCarga()
    {
        return this._dtLimiteVinculoCarga;
    } //-- java.lang.String getDtLimiteVinculoCarga() 

    /**
     * Returns the value of field 'dtRegistroTitulo'.
     * 
     * @return String
     * @return the value of field 'dtRegistroTitulo'.
     */
    public java.lang.String getDtRegistroTitulo()
    {
        return this._dtRegistroTitulo;
    } //-- java.lang.String getDtRegistroTitulo() 

    /**
     * Returns the value of field 'hrManutencaoRegistroAlteracao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroAlteracao'.
     */
    public java.lang.String getHrManutencaoRegistroAlteracao()
    {
        return this._hrManutencaoRegistroAlteracao;
    } //-- java.lang.String getHrManutencaoRegistroAlteracao() 

    /**
     * Returns the value of field 'hrManutencaoRegistroInclusao'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistroInclusao'.
     */
    public java.lang.String getHrManutencaoRegistroInclusao()
    {
        return this._hrManutencaoRegistroInclusao;
    } //-- java.lang.String getHrManutencaoRegistroInclusao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @return int
     * @return the value of field 'nrFechamentoApuracaoTarifa'.
     */
    public int getNrFechamentoApuracaoTarifa()
    {
        return this._nrFechamentoApuracaoTarifa;
    } //-- int getNrFechamentoApuracaoTarifa() 

    /**
     * Returns the value of field 'nrOperacaoFluxoAlteracao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoAlteracao'.
     */
    public java.lang.String getNrOperacaoFluxoAlteracao()
    {
        return this._nrOperacaoFluxoAlteracao;
    } //-- java.lang.String getNrOperacaoFluxoAlteracao() 

    /**
     * Returns the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoInclusao'.
     */
    public java.lang.String getNrOperacaoFluxoInclusao()
    {
        return this._nrOperacaoFluxoInclusao;
    } //-- java.lang.String getNrOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'percentualIndiceReajusteTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'percentualIndiceReajusteTarifa'.
     */
    public java.math.BigDecimal getPercentualIndiceReajusteTarifa()
    {
        return this._percentualIndiceReajusteTarifa;
    } //-- java.math.BigDecimal getPercentualIndiceReajusteTarifa() 

    /**
     * Returns the value of field
     * 'percentualMaximoInconsistenteLote'.
     * 
     * @return int
     * @return the value of field
     * 'percentualMaximoInconsistenteLote'.
     */
    public int getPercentualMaximoInconsistenteLote()
    {
        return this._percentualMaximoInconsistenteLote;
    } //-- int getPercentualMaximoInconsistenteLote() 

    /**
     * Returns the value of field 'periodicidadeTarifaCatalogo'.
     * 
     * @return BigDecimal
     * @return the value of field 'periodicidadeTarifaCatalogo'.
     */
    public java.math.BigDecimal getPeriodicidadeTarifaCatalogo()
    {
        return this._periodicidadeTarifaCatalogo;
    } //-- java.math.BigDecimal getPeriodicidadeTarifaCatalogo() 

    /**
     * Returns the value of field 'qtDiaUtilPgto'.
     * 
     * @return int
     * @return the value of field 'qtDiaUtilPgto'.
     */
    public int getQtDiaUtilPgto()
    {
        return this._qtDiaUtilPgto;
    } //-- int getQtDiaUtilPgto() 

    /**
     * Returns the value of field 'quantidadeAntecedencia'.
     * 
     * @return int
     * @return the value of field 'quantidadeAntecedencia'.
     */
    public int getQuantidadeAntecedencia()
    {
        return this._quantidadeAntecedencia;
    } //-- int getQuantidadeAntecedencia() 

    /**
     * Returns the value of field
     * 'quantidadeAnteriorVencimentoComprovante'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeAnteriorVencimentoComprovante'.
     */
    public int getQuantidadeAnteriorVencimentoComprovante()
    {
        return this._quantidadeAnteriorVencimentoComprovante;
    } //-- int getQuantidadeAnteriorVencimentoComprovante() 

    /**
     * Returns the value of field 'quantidadeDiaCobrancaTarifa'.
     * 
     * @return int
     * @return the value of field 'quantidadeDiaCobrancaTarifa'.
     */
    public int getQuantidadeDiaCobrancaTarifa()
    {
        return this._quantidadeDiaCobrancaTarifa;
    } //-- int getQuantidadeDiaCobrancaTarifa() 

    /**
     * Returns the value of field 'quantidadeDiaExpiracao'.
     * 
     * @return int
     * @return the value of field 'quantidadeDiaExpiracao'.
     */
    public int getQuantidadeDiaExpiracao()
    {
        return this._quantidadeDiaExpiracao;
    } //-- int getQuantidadeDiaExpiracao() 

    /**
     * Returns the value of field 'quantidadeDiaFloatingPagamento'.
     * 
     * @return int
     * @return the value of field 'quantidadeDiaFloatingPagamento'.
     */
    public int getQuantidadeDiaFloatingPagamento()
    {
        return this._quantidadeDiaFloatingPagamento;
    } //-- int getQuantidadeDiaFloatingPagamento() 

    /**
     * Returns the value of field
     * 'quantidadeDiaInatividadeFavorecido'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeDiaInatividadeFavorecido'.
     */
    public int getQuantidadeDiaInatividadeFavorecido()
    {
        return this._quantidadeDiaInatividadeFavorecido;
    } //-- int getQuantidadeDiaInatividadeFavorecido() 

    /**
     * Returns the value of field 'quantidadeDiaRepiqueConsulta'.
     * 
     * @return int
     * @return the value of field 'quantidadeDiaRepiqueConsulta'.
     */
    public int getQuantidadeDiaRepiqueConsulta()
    {
        return this._quantidadeDiaRepiqueConsulta;
    } //-- int getQuantidadeDiaRepiqueConsulta() 

    /**
     * Returns the value of field
     * 'quantidadeEtapaRecadastramentoBeneficio'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeEtapaRecadastramentoBeneficio'.
     */
    public int getQuantidadeEtapaRecadastramentoBeneficio()
    {
        return this._quantidadeEtapaRecadastramentoBeneficio;
    } //-- int getQuantidadeEtapaRecadastramentoBeneficio() 

    /**
     * Returns the value of field
     * 'quantidadeFaseRecadastramentoBeneficio'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeFaseRecadastramentoBeneficio'.
     */
    public int getQuantidadeFaseRecadastramentoBeneficio()
    {
        return this._quantidadeFaseRecadastramentoBeneficio;
    } //-- int getQuantidadeFaseRecadastramentoBeneficio() 

    /**
     * Returns the value of field 'quantidadeLimiteLinha'.
     * 
     * @return int
     * @return the value of field 'quantidadeLimiteLinha'.
     */
    public int getQuantidadeLimiteLinha()
    {
        return this._quantidadeLimiteLinha;
    } //-- int getQuantidadeLimiteLinha() 

    /**
     * Returns the value of field
     * 'quantidadeMaximaInconsistenteLote'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeMaximaInconsistenteLote'.
     */
    public int getQuantidadeMaximaInconsistenteLote()
    {
        return this._quantidadeMaximaInconsistenteLote;
    } //-- int getQuantidadeMaximaInconsistenteLote() 

    /**
     * Returns the value of field 'quantidadeMaximaTituloVencido'.
     * 
     * @return int
     * @return the value of field 'quantidadeMaximaTituloVencido'.
     */
    public int getQuantidadeMaximaTituloVencido()
    {
        return this._quantidadeMaximaTituloVencido;
    } //-- int getQuantidadeMaximaTituloVencido() 

    /**
     * Returns the value of field 'quantidadeMesComprovante'.
     * 
     * @return int
     * @return the value of field 'quantidadeMesComprovante'.
     */
    public int getQuantidadeMesComprovante()
    {
        return this._quantidadeMesComprovante;
    } //-- int getQuantidadeMesComprovante() 

    /**
     * Returns the value of field
     * 'quantidadeMesEtapaRecadastramento'.
     * 
     * @return int
     * @return the value of field
     * 'quantidadeMesEtapaRecadastramento'.
     */
    public int getQuantidadeMesEtapaRecadastramento()
    {
        return this._quantidadeMesEtapaRecadastramento;
    } //-- int getQuantidadeMesEtapaRecadastramento() 

    /**
     * Returns the value of field
     * 'quantidadeMesFaseRecadastramento'.
     * 
     * @return int
     * @return the value of field 'quantidadeMesFaseRecadastramento'
     */
    public int getQuantidadeMesFaseRecadastramento()
    {
        return this._quantidadeMesFaseRecadastramento;
    } //-- int getQuantidadeMesFaseRecadastramento() 

    /**
     * Returns the value of field 'quantidadeMesReajusteTarifa'.
     * 
     * @return int
     * @return the value of field 'quantidadeMesReajusteTarifa'.
     */
    public int getQuantidadeMesReajusteTarifa()
    {
        return this._quantidadeMesReajusteTarifa;
    } //-- int getQuantidadeMesReajusteTarifa() 

    /**
     * Returns the value of field 'quantidadeSolicitacaoCartao'.
     * 
     * @return int
     * @return the value of field 'quantidadeSolicitacaoCartao'.
     */
    public int getQuantidadeSolicitacaoCartao()
    {
        return this._quantidadeSolicitacaoCartao;
    } //-- int getQuantidadeSolicitacaoCartao() 

    /**
     * Returns the value of field 'quantidadeViaAviso'.
     * 
     * @return int
     * @return the value of field 'quantidadeViaAviso'.
     */
    public int getQuantidadeViaAviso()
    {
        return this._quantidadeViaAviso;
    } //-- int getQuantidadeViaAviso() 

    /**
     * Returns the value of field 'quantidadeViaCobranca'.
     * 
     * @return int
     * @return the value of field 'quantidadeViaCobranca'.
     */
    public int getQuantidadeViaCobranca()
    {
        return this._quantidadeViaCobranca;
    } //-- int getQuantidadeViaCobranca() 

    /**
     * Returns the value of field 'quantidadeViaComprovante'.
     * 
     * @return int
     * @return the value of field 'quantidadeViaComprovante'.
     */
    public int getQuantidadeViaComprovante()
    {
        return this._quantidadeViaComprovante;
    } //-- int getQuantidadeViaComprovante() 

    /**
     * Returns the value of field 'valorFavorecidoNaoCadastrado'.
     * 
     * @return BigDecimal
     * @return the value of field 'valorFavorecidoNaoCadastrado'.
     */
    public java.math.BigDecimal getValorFavorecidoNaoCadastrado()
    {
        return this._valorFavorecidoNaoCadastrado;
    } //-- java.math.BigDecimal getValorFavorecidoNaoCadastrado() 

    /**
     * Returns the value of field 'valorLimiteDiarioPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'valorLimiteDiarioPagamento'.
     */
    public java.math.BigDecimal getValorLimiteDiarioPagamento()
    {
        return this._valorLimiteDiarioPagamento;
    } //-- java.math.BigDecimal getValorLimiteDiarioPagamento() 

    /**
     * Returns the value of field 'valorLimiteIndividualPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'valorLimiteIndividualPagamento'.
     */
    public java.math.BigDecimal getValorLimiteIndividualPagamento()
    {
        return this._valorLimiteIndividualPagamento;
    } //-- java.math.BigDecimal getValorLimiteIndividualPagamento() 

    /**
     * Returns the value of field 'vlPercentualDiferencaTolerada'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPercentualDiferencaTolerada'.
     */
    public java.math.BigDecimal getVlPercentualDiferencaTolerada()
    {
        return this._vlPercentualDiferencaTolerada;
    } //-- java.math.BigDecimal getVlPercentualDiferencaTolerada() 

    /**
     * Method hasCdAcaoNaoVida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoNaoVida()
    {
        return this._has_cdAcaoNaoVida;
    } //-- boolean hasCdAcaoNaoVida() 

    /**
     * Method hasCdAcertoDadosRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcertoDadosRecadastramento()
    {
        return this._has_cdAcertoDadosRecadastramento;
    } //-- boolean hasCdAcertoDadosRecadastramento() 

    /**
     * Method hasCdAgendamentoDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendamentoDebitoVeiculo()
    {
        return this._has_cdAgendamentoDebitoVeiculo;
    } //-- boolean hasCdAgendamentoDebitoVeiculo() 

    /**
     * Method hasCdAgendamentoPagamentoVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendamentoPagamentoVencido()
    {
        return this._has_cdAgendamentoPagamentoVencido;
    } //-- boolean hasCdAgendamentoPagamentoVencido() 

    /**
     * Method hasCdAgendamentoRastreabilidadeFinal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendamentoRastreabilidadeFinal()
    {
        return this._has_cdAgendamentoRastreabilidadeFinal;
    } //-- boolean hasCdAgendamentoRastreabilidadeFinal() 

    /**
     * Method hasCdAgendamentoValorMenor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendamentoValorMenor()
    {
        return this._has_cdAgendamentoValorMenor;
    } //-- boolean hasCdAgendamentoValorMenor() 

    /**
     * Method hasCdAgrupamentoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoAviso()
    {
        return this._has_cdAgrupamentoAviso;
    } //-- boolean hasCdAgrupamentoAviso() 

    /**
     * Method hasCdAgrupamentoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoComprovante()
    {
        return this._has_cdAgrupamentoComprovante;
    } //-- boolean hasCdAgrupamentoComprovante() 

    /**
     * Method hasCdAgrupamentoFormularioRecadastro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgrupamentoFormularioRecadastro()
    {
        return this._has_cdAgrupamentoFormularioRecadastro;
    } //-- boolean hasCdAgrupamentoFormularioRecadastro() 

    /**
     * Method hasCdAntecipacaoRecadastramentoBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAntecipacaoRecadastramentoBeneficiario()
    {
        return this._has_cdAntecipacaoRecadastramentoBeneficiario;
    } //-- boolean hasCdAntecipacaoRecadastramentoBeneficiario() 

    /**
     * Method hasCdAreaReservada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaReservada()
    {
        return this._has_cdAreaReservada;
    } //-- boolean hasCdAreaReservada() 

    /**
     * Method hasCdBaseRecadastramentoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBaseRecadastramentoBeneficio()
    {
        return this._has_cdBaseRecadastramentoBeneficio;
    } //-- boolean hasCdBaseRecadastramentoBeneficio() 

    /**
     * Method hasCdBloqueioEmissaoPapeleta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBloqueioEmissaoPapeleta()
    {
        return this._has_cdBloqueioEmissaoPapeleta;
    } //-- boolean hasCdBloqueioEmissaoPapeleta() 

    /**
     * Method hasCdCanalAlteracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalAlteracao()
    {
        return this._has_cdCanalAlteracao;
    } //-- boolean hasCdCanalAlteracao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCapturaTituloRegistrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCapturaTituloRegistrado()
    {
        return this._has_cdCapturaTituloRegistrado;
    } //-- boolean hasCdCapturaTituloRegistrado() 

    /**
     * Method hasCdCctciaEspeBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCctciaEspeBeneficio()
    {
        return this._has_cdCctciaEspeBeneficio;
    } //-- boolean hasCdCctciaEspeBeneficio() 

    /**
     * Method hasCdCctciaIdentificacaoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCctciaIdentificacaoBeneficio()
    {
        return this._has_cdCctciaIdentificacaoBeneficio;
    } //-- boolean hasCdCctciaIdentificacaoBeneficio() 

    /**
     * Method hasCdCctciaInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCctciaInscricaoFavorecido()
    {
        return this._has_cdCctciaInscricaoFavorecido;
    } //-- boolean hasCdCctciaInscricaoFavorecido() 

    /**
     * Method hasCdCctciaProprietarioVeculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCctciaProprietarioVeculo()
    {
        return this._has_cdCctciaProprietarioVeculo;
    } //-- boolean hasCdCctciaProprietarioVeculo() 

    /**
     * Method hasCdCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCobrancaTarifa()
    {
        return this._has_cdCobrancaTarifa;
    } //-- boolean hasCdCobrancaTarifa() 

    /**
     * Method hasCdConsistenciaCpfCnpjBenefAvalNpc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsistenciaCpfCnpjBenefAvalNpc()
    {
        return this._has_cdConsistenciaCpfCnpjBenefAvalNpc;
    } //-- boolean hasCdConsistenciaCpfCnpjBenefAvalNpc() 

    /**
     * Method hasCdConsultaDebitoVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaDebitoVeiculo()
    {
        return this._has_cdConsultaDebitoVeiculo;
    } //-- boolean hasCdConsultaDebitoVeiculo() 

    /**
     * Method hasCdConsultaEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaEndereco()
    {
        return this._has_cdConsultaEndereco;
    } //-- boolean hasCdConsultaEndereco() 

    /**
     * Method hasCdConsultaSaldoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaSaldoPagamento()
    {
        return this._has_cdConsultaSaldoPagamento;
    } //-- boolean hasCdConsultaSaldoPagamento() 

    /**
     * Method hasCdConsultaSaldoValorSuperior
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsultaSaldoValorSuperior()
    {
        return this._has_cdConsultaSaldoValorSuperior;
    } //-- boolean hasCdConsultaSaldoValorSuperior() 

    /**
     * Method hasCdContagemConsultaSaldo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContagemConsultaSaldo()
    {
        return this._has_cdContagemConsultaSaldo;
    } //-- boolean hasCdContagemConsultaSaldo() 

    /**
     * Method hasCdCreditoNaoUtilizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCreditoNaoUtilizado()
    {
        return this._has_cdCreditoNaoUtilizado;
    } //-- boolean hasCdCreditoNaoUtilizado() 

    /**
     * Method hasCdCriterioEnquadraRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquadraRecadastramento()
    {
        return this._has_cdCriterioEnquadraRecadastramento;
    } //-- boolean hasCdCriterioEnquadraRecadastramento() 

    /**
     * Method hasCdCriterioEnquandraBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioEnquandraBeneficio()
    {
        return this._has_cdCriterioEnquandraBeneficio;
    } //-- boolean hasCdCriterioEnquandraBeneficio() 

    /**
     * Method hasCdCriterioRastreabilidadeTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriterioRastreabilidadeTitulo()
    {
        return this._has_cdCriterioRastreabilidadeTitulo;
    } //-- boolean hasCdCriterioRastreabilidadeTitulo() 

    /**
     * Method hasCdDestinoAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoAviso()
    {
        return this._has_cdDestinoAviso;
    } //-- boolean hasCdDestinoAviso() 

    /**
     * Method hasCdDestinoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoComprovante()
    {
        return this._has_cdDestinoComprovante;
    } //-- boolean hasCdDestinoComprovante() 

    /**
     * Method hasCdDestinoFormularioRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDestinoFormularioRecadastramento()
    {
        return this._has_cdDestinoFormularioRecadastramento;
    } //-- boolean hasCdDestinoFormularioRecadastramento() 

    /**
     * Method hasCdDisponibilizacaoContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoContaCredito()
    {
        return this._has_cdDisponibilizacaoContaCredito;
    } //-- boolean hasCdDisponibilizacaoContaCredito() 

    /**
     * Method hasCdDisponibilizacaoDiversoCriterio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoDiversoCriterio()
    {
        return this._has_cdDisponibilizacaoDiversoCriterio;
    } //-- boolean hasCdDisponibilizacaoDiversoCriterio() 

    /**
     * Method hasCdDisponibilizacaoDiversoNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoDiversoNao()
    {
        return this._has_cdDisponibilizacaoDiversoNao;
    } //-- boolean hasCdDisponibilizacaoDiversoNao() 

    /**
     * Method hasCdDisponibilizacaoSalarioCriterio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoSalarioCriterio()
    {
        return this._has_cdDisponibilizacaoSalarioCriterio;
    } //-- boolean hasCdDisponibilizacaoSalarioCriterio() 

    /**
     * Method hasCdDisponibilizacaoSalarioNao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDisponibilizacaoSalarioNao()
    {
        return this._has_cdDisponibilizacaoSalarioNao;
    } //-- boolean hasCdDisponibilizacaoSalarioNao() 

    /**
     * Method hasCdEnvelopeAberto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEnvelopeAberto()
    {
        return this._has_cdEnvelopeAberto;
    } //-- boolean hasCdEnvelopeAberto() 

    /**
     * Method hasCdExigeAutFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdExigeAutFilial()
    {
        return this._has_cdExigeAutFilial;
    } //-- boolean hasCdExigeAutFilial() 

    /**
     * Method hasCdFavorecidoConsultaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoConsultaPagamento()
    {
        return this._has_cdFavorecidoConsultaPagamento;
    } //-- boolean hasCdFavorecidoConsultaPagamento() 

    /**
     * Method hasCdFloatServicoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFloatServicoContrato()
    {
        return this._has_cdFloatServicoContrato;
    } //-- boolean hasCdFloatServicoContrato() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFormaEnvioPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEnvioPagamento()
    {
        return this._has_cdFormaEnvioPagamento;
    } //-- boolean hasCdFormaEnvioPagamento() 

    /**
     * Method hasCdFormaEstornoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEstornoCredito()
    {
        return this._has_cdFormaEstornoCredito;
    } //-- boolean hasCdFormaEstornoCredito() 

    /**
     * Method hasCdFormaExpiracaoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaExpiracaoCredito()
    {
        return this._has_cdFormaExpiracaoCredito;
    } //-- boolean hasCdFormaExpiracaoCredito() 

    /**
     * Method hasCdFormaManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaManutencao()
    {
        return this._has_cdFormaManutencao;
    } //-- boolean hasCdFormaManutencao() 

    /**
     * Method hasCdFrasePrecadastrada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFrasePrecadastrada()
    {
        return this._has_cdFrasePrecadastrada;
    } //-- boolean hasCdFrasePrecadastrada() 

    /**
     * Method hasCdIdentificadortipoRetornoInternet
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadortipoRetornoInternet()
    {
        return this._has_cdIdentificadorTipoRetornoInternet;
    } //-- boolean hasCdIdentificadortipoRetornoInternet() 

    /**
     * Method hasCdIndicadorAgendaGrade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendaGrade()
    {
        return this._has_cdIndicadorAgendaGrade;
    } //-- boolean hasCdIndicadorAgendaGrade() 

    /**
     * Method hasCdIndicadorAgendamentoTitulo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgendamentoTitulo()
    {
        return this._has_cdIndicadorAgendamentoTitulo;
    } //-- boolean hasCdIndicadorAgendamentoTitulo() 

    /**
     * Method hasCdIndicadorAutorizacaoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoCliente()
    {
        return this._has_cdIndicadorAutorizacaoCliente;
    } //-- boolean hasCdIndicadorAutorizacaoCliente() 

    /**
     * Method hasCdIndicadorAutorizacaoComplemento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoComplemento()
    {
        return this._has_cdIndicadorAutorizacaoComplemento;
    } //-- boolean hasCdIndicadorAutorizacaoComplemento() 

    /**
     * Method hasCdIndicadorBancoPostal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorBancoPostal()
    {
        return this._has_cdIndicadorBancoPostal;
    } //-- boolean hasCdIndicadorBancoPostal() 

    /**
     * Method hasCdIndicadorCadastroProcurador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadastroProcurador()
    {
        return this._has_cdIndicadorCadastroProcurador;
    } //-- boolean hasCdIndicadorCadastroProcurador() 

    /**
     * Method hasCdIndicadorCadastroorganizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCadastroorganizacao()
    {
        return this._has_cdIndicadorCadastroorganizacao;
    } //-- boolean hasCdIndicadorCadastroorganizacao() 

    /**
     * Method hasCdIndicadorCartaoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCartaoSalario()
    {
        return this._has_cdIndicadorCartaoSalario;
    } //-- boolean hasCdIndicadorCartaoSalario() 

    /**
     * Method hasCdIndicadorEconomicoReajuste
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoReajuste()
    {
        return this._has_cdIndicadorEconomicoReajuste;
    } //-- boolean hasCdIndicadorEconomicoReajuste() 

    /**
     * Method hasCdIndicadorFeriadoLocal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFeriadoLocal()
    {
        return this._has_cdIndicadorFeriadoLocal;
    } //-- boolean hasCdIndicadorFeriadoLocal() 

    /**
     * Method hasCdIndicadorListaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorListaDebito()
    {
        return this._has_cdIndicadorListaDebito;
    } //-- boolean hasCdIndicadorListaDebito() 

    /**
     * Method hasCdIndicadorMensagemPersonalizada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorMensagemPersonalizada()
    {
        return this._has_cdIndicadorMensagemPersonalizada;
    } //-- boolean hasCdIndicadorMensagemPersonalizada() 

    /**
     * Method hasCdIndicadorRetornoInternet
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoInternet()
    {
        return this._has_cdIndicadorRetornoInternet;
    } //-- boolean hasCdIndicadorRetornoInternet() 

    /**
     * Method hasCdIndicadorSegundaLinha
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorSegundaLinha()
    {
        return this._has_cdIndicadorSegundaLinha;
    } //-- boolean hasCdIndicadorSegundaLinha() 

    /**
     * Method hasCdIndicadorUtilizaMora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorUtilizaMora()
    {
        return this._has_cdIndicadorUtilizaMora;
    } //-- boolean hasCdIndicadorUtilizaMora() 

    /**
     * Method hasCdLancamentoFuturoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoCredito()
    {
        return this._has_cdLancamentoFuturoCredito;
    } //-- boolean hasCdLancamentoFuturoCredito() 

    /**
     * Method hasCdLancamentoFuturoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamentoFuturoDebito()
    {
        return this._has_cdLancamentoFuturoDebito;
    } //-- boolean hasCdLancamentoFuturoDebito() 

    /**
     * Method hasCdLiberacaoLoteProcessado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLiberacaoLoteProcessado()
    {
        return this._has_cdLiberacaoLoteProcessado;
    } //-- boolean hasCdLiberacaoLoteProcessado() 

    /**
     * Method hasCdManutencaoBaseRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdManutencaoBaseRecadastramento()
    {
        return this._has_cdManutencaoBaseRecadastramento;
    } //-- boolean hasCdManutencaoBaseRecadastramento() 

    /**
     * Method hasCdMeioPagamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPagamentoDebito()
    {
        return this._has_cdMeioPagamentoDebito;
    } //-- boolean hasCdMeioPagamentoDebito() 

    /**
     * Method hasCdMensagemRecadastramentoMidia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemRecadastramentoMidia()
    {
        return this._has_cdMensagemRecadastramentoMidia;
    } //-- boolean hasCdMensagemRecadastramentoMidia() 

    /**
     * Method hasCdMidiaDisponivel
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaDisponivel()
    {
        return this._has_cdMidiaDisponivel;
    } //-- boolean hasCdMidiaDisponivel() 

    /**
     * Method hasCdMidiaMensagemRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaMensagemRecadastramento()
    {
        return this._has_cdMidiaMensagemRecadastramento;
    } //-- boolean hasCdMidiaMensagemRecadastramento() 

    /**
     * Method hasCdMomentoAvisoRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoAvisoRecadastramento()
    {
        return this._has_cdMomentoAvisoRecadastramento;
    } //-- boolean hasCdMomentoAvisoRecadastramento() 

    /**
     * Method hasCdMomentoCreditoEfetivacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoCreditoEfetivacao()
    {
        return this._has_cdMomentoCreditoEfetivacao;
    } //-- boolean hasCdMomentoCreditoEfetivacao() 

    /**
     * Method hasCdMomentoDebitoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoDebitoPagamento()
    {
        return this._has_cdMomentoDebitoPagamento;
    } //-- boolean hasCdMomentoDebitoPagamento() 

    /**
     * Method hasCdMomentoFormularioRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoFormularioRecadastramento()
    {
        return this._has_cdMomentoFormularioRecadastramento;
    } //-- boolean hasCdMomentoFormularioRecadastramento() 

    /**
     * Method hasCdMomentoProcessamentoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMomentoProcessamentoPagamento()
    {
        return this._has_cdMomentoProcessamentoPagamento;
    } //-- boolean hasCdMomentoProcessamentoPagamento() 

    /**
     * Method hasCdNaturezaOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNaturezaOperacaoPagamento()
    {
        return this._has_cdNaturezaOperacaoPagamento;
    } //-- boolean hasCdNaturezaOperacaoPagamento() 

    /**
     * Method hasCdOutraidentificacaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOutraidentificacaoFavorecido()
    {
        return this._has_cdOutraidentificacaoFavorecido;
    } //-- boolean hasCdOutraidentificacaoFavorecido() 

    /**
     * Method hasCdPagamentoNaoUtil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoNaoUtil()
    {
        return this._has_cdPagamentoNaoUtil;
    } //-- boolean hasCdPagamentoNaoUtil() 

    /**
     * Method hasCdPeriodicidadeAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeAviso()
    {
        return this._has_cdPeriodicidadeAviso;
    } //-- boolean hasCdPeriodicidadeAviso() 

    /**
     * Method hasCdPeriodicidadeCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeCobrancaTarifa()
    {
        return this._has_cdPeriodicidadeCobrancaTarifa;
    } //-- boolean hasCdPeriodicidadeCobrancaTarifa() 

    /**
     * Method hasCdPeriodicidadeComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeComprovante()
    {
        return this._has_cdPeriodicidadeComprovante;
    } //-- boolean hasCdPeriodicidadeComprovante() 

    /**
     * Method hasCdPeriodicidadeConsultaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeConsultaVeiculo()
    {
        return this._has_cdPeriodicidadeConsultaVeiculo;
    } //-- boolean hasCdPeriodicidadeConsultaVeiculo() 

    /**
     * Method hasCdPeriodicidadeEnvioRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeEnvioRemessa()
    {
        return this._has_cdPeriodicidadeEnvioRemessa;
    } //-- boolean hasCdPeriodicidadeEnvioRemessa() 

    /**
     * Method hasCdPeriodicidadeManutencaoProcd
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeManutencaoProcd()
    {
        return this._has_cdPeriodicidadeManutencaoProcd;
    } //-- boolean hasCdPeriodicidadeManutencaoProcd() 

    /**
     * Method hasCdPeriodicidadeReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeReajusteTarifa()
    {
        return this._has_cdPeriodicidadeReajusteTarifa;
    } //-- boolean hasCdPeriodicidadeReajusteTarifa() 

    /**
     * Method hasCdPermissaoDebitoOnline
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPermissaoDebitoOnline()
    {
        return this._has_cdPermissaoDebitoOnline;
    } //-- boolean hasCdPermissaoDebitoOnline() 

    /**
     * Method hasCdPreenchimentoLancamentoPersonalizado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPreenchimentoLancamentoPersonalizado()
    {
        return this._has_cdPreenchimentoLancamentoPersonalizado;
    } //-- boolean hasCdPreenchimentoLancamentoPersonalizado() 

    /**
     * Method hasCdPrincipalEnquaRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrincipalEnquaRecadastramento()
    {
        return this._has_cdPrincipalEnquaRecadastramento;
    } //-- boolean hasCdPrincipalEnquaRecadastramento() 

    /**
     * Method hasCdPrioridadeEfetivacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPrioridadeEfetivacaoPagamento()
    {
        return this._has_cdPrioridadeEfetivacaoPagamento;
    } //-- boolean hasCdPrioridadeEfetivacaoPagamento() 

    /**
     * Method hasCdRastreabilidadeNotaFiscal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabilidadeNotaFiscal()
    {
        return this._has_cdRastreabilidadeNotaFiscal;
    } //-- boolean hasCdRastreabilidadeNotaFiscal() 

    /**
     * Method hasCdRastreabilidadeTituloTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRastreabilidadeTituloTerceiro()
    {
        return this._has_cdRastreabilidadeTituloTerceiro;
    } //-- boolean hasCdRastreabilidadeTituloTerceiro() 

    /**
     * Method hasCdRejeicaoAgendamentoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAgendamentoLote()
    {
        return this._has_cdRejeicaoAgendamentoLote;
    } //-- boolean hasCdRejeicaoAgendamentoLote() 

    /**
     * Method hasCdRejeicaoEfetivacaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoEfetivacaoLote()
    {
        return this._has_cdRejeicaoEfetivacaoLote;
    } //-- boolean hasCdRejeicaoEfetivacaoLote() 

    /**
     * Method hasCdRejeicaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoLote()
    {
        return this._has_cdRejeicaoLote;
    } //-- boolean hasCdRejeicaoLote() 

    /**
     * Method hasCdTipoCargaRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCargaRecadastramento()
    {
        return this._has_cdTipoCargaRecadastramento;
    } //-- boolean hasCdTipoCargaRecadastramento() 

    /**
     * Method hasCdTipoCartaoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCartaoSalario()
    {
        return this._has_cdTipoCartaoSalario;
    } //-- boolean hasCdTipoCartaoSalario() 

    /**
     * Method hasCdTipoConsistenciaLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsistenciaLista()
    {
        return this._has_cdTipoConsistenciaLista;
    } //-- boolean hasCdTipoConsistenciaLista() 

    /**
     * Method hasCdTipoConsultaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConsultaComprovante()
    {
        return this._has_cdTipoConsultaComprovante;
    } //-- boolean hasCdTipoConsultaComprovante() 

    /**
     * Method hasCdTipoDataFloating
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDataFloating()
    {
        return this._has_cdTipoDataFloating;
    } //-- boolean hasCdTipoDataFloating() 

    /**
     * Method hasCdTipoDivergenciaVeiculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoDivergenciaVeiculo()
    {
        return this._has_cdTipoDivergenciaVeiculo;
    } //-- boolean hasCdTipoDivergenciaVeiculo() 

    /**
     * Method hasCdTipoEfetivacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoEfetivacaoPagamento()
    {
        return this._has_cdTipoEfetivacaoPagamento;
    } //-- boolean hasCdTipoEfetivacaoPagamento() 

    /**
     * Method hasCdTipoFormacaoLista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFormacaoLista()
    {
        return this._has_cdTipoFormacaoLista;
    } //-- boolean hasCdTipoFormacaoLista() 

    /**
     * Method hasCdTipoIdentificacaoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIdentificacaoBeneficio()
    {
        return this._has_cdTipoIdentificacaoBeneficio;
    } //-- boolean hasCdTipoIdentificacaoBeneficio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoReajusteTarifa()
    {
        return this._has_cdTipoReajusteTarifa;
    } //-- boolean hasCdTipoReajusteTarifa() 

    /**
     * Method hasCdTituloDdaRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTituloDdaRetorno()
    {
        return this._has_cdTituloDdaRetorno;
    } //-- boolean hasCdTituloDdaRetorno() 

    /**
     * Method hasCdTratamentoContaTransferida
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTratamentoContaTransferida()
    {
        return this._has_cdTratamentoContaTransferida;
    } //-- boolean hasCdTratamentoContaTransferida() 

    /**
     * Method hasCdUtilizacaoFavorecidoControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUtilizacaoFavorecidoControle()
    {
        return this._has_cdUtilizacaoFavorecidoControle;
    } //-- boolean hasCdUtilizacaoFavorecidoControle() 

    /**
     * Method hasCdindicadorExpiraCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdindicadorExpiraCredito()
    {
        return this._has_cdindicadorExpiraCredito;
    } //-- boolean hasCdindicadorExpiraCredito() 

    /**
     * Method hasCdindicadorLancamentoProgramado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdindicadorLancamentoProgramado()
    {
        return this._has_cdindicadorLancamentoProgramado;
    } //-- boolean hasCdindicadorLancamentoProgramado() 

    /**
     * Method hasDsOrigemIndicador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDsOrigemIndicador()
    {
        return this._has_dsOrigemIndicador;
    } //-- boolean hasDsOrigemIndicador() 

    /**
     * Method hasNrFechamentoApuracaoTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFechamentoApuracaoTarifa()
    {
        return this._has_nrFechamentoApuracaoTarifa;
    } //-- boolean hasNrFechamentoApuracaoTarifa() 

    /**
     * Method hasPercentualMaximoInconsistenteLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPercentualMaximoInconsistenteLote()
    {
        return this._has_percentualMaximoInconsistenteLote;
    } //-- boolean hasPercentualMaximoInconsistenteLote() 

    /**
     * Method hasQtDiaUtilPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtDiaUtilPgto()
    {
        return this._has_qtDiaUtilPgto;
    } //-- boolean hasQtDiaUtilPgto() 

    /**
     * Method hasQuantidadeAntecedencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeAntecedencia()
    {
        return this._has_quantidadeAntecedencia;
    } //-- boolean hasQuantidadeAntecedencia() 

    /**
     * Method hasQuantidadeAnteriorVencimentoComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeAnteriorVencimentoComprovante()
    {
        return this._has_quantidadeAnteriorVencimentoComprovante;
    } //-- boolean hasQuantidadeAnteriorVencimentoComprovante() 

    /**
     * Method hasQuantidadeDiaCobrancaTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaCobrancaTarifa()
    {
        return this._has_quantidadeDiaCobrancaTarifa;
    } //-- boolean hasQuantidadeDiaCobrancaTarifa() 

    /**
     * Method hasQuantidadeDiaExpiracao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaExpiracao()
    {
        return this._has_quantidadeDiaExpiracao;
    } //-- boolean hasQuantidadeDiaExpiracao() 

    /**
     * Method hasQuantidadeDiaFloatingPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaFloatingPagamento()
    {
        return this._has_quantidadeDiaFloatingPagamento;
    } //-- boolean hasQuantidadeDiaFloatingPagamento() 

    /**
     * Method hasQuantidadeDiaInatividadeFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaInatividadeFavorecido()
    {
        return this._has_quantidadeDiaInatividadeFavorecido;
    } //-- boolean hasQuantidadeDiaInatividadeFavorecido() 

    /**
     * Method hasQuantidadeDiaRepiqueConsulta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeDiaRepiqueConsulta()
    {
        return this._has_quantidadeDiaRepiqueConsulta;
    } //-- boolean hasQuantidadeDiaRepiqueConsulta() 

    /**
     * Method hasQuantidadeEtapaRecadastramentoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeEtapaRecadastramentoBeneficio()
    {
        return this._has_quantidadeEtapaRecadastramentoBeneficio;
    } //-- boolean hasQuantidadeEtapaRecadastramentoBeneficio() 

    /**
     * Method hasQuantidadeFaseRecadastramentoBeneficio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeFaseRecadastramentoBeneficio()
    {
        return this._has_quantidadeFaseRecadastramentoBeneficio;
    } //-- boolean hasQuantidadeFaseRecadastramentoBeneficio() 

    /**
     * Method hasQuantidadeLimiteLinha
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeLimiteLinha()
    {
        return this._has_quantidadeLimiteLinha;
    } //-- boolean hasQuantidadeLimiteLinha() 

    /**
     * Method hasQuantidadeMaximaInconsistenteLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMaximaInconsistenteLote()
    {
        return this._has_quantidadeMaximaInconsistenteLote;
    } //-- boolean hasQuantidadeMaximaInconsistenteLote() 

    /**
     * Method hasQuantidadeMaximaTituloVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMaximaTituloVencido()
    {
        return this._has_quantidadeMaximaTituloVencido;
    } //-- boolean hasQuantidadeMaximaTituloVencido() 

    /**
     * Method hasQuantidadeMesComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMesComprovante()
    {
        return this._has_quantidadeMesComprovante;
    } //-- boolean hasQuantidadeMesComprovante() 

    /**
     * Method hasQuantidadeMesEtapaRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMesEtapaRecadastramento()
    {
        return this._has_quantidadeMesEtapaRecadastramento;
    } //-- boolean hasQuantidadeMesEtapaRecadastramento() 

    /**
     * Method hasQuantidadeMesFaseRecadastramento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMesFaseRecadastramento()
    {
        return this._has_quantidadeMesFaseRecadastramento;
    } //-- boolean hasQuantidadeMesFaseRecadastramento() 

    /**
     * Method hasQuantidadeMesReajusteTarifa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeMesReajusteTarifa()
    {
        return this._has_quantidadeMesReajusteTarifa;
    } //-- boolean hasQuantidadeMesReajusteTarifa() 

    /**
     * Method hasQuantidadeSolicitacaoCartao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeSolicitacaoCartao()
    {
        return this._has_quantidadeSolicitacaoCartao;
    } //-- boolean hasQuantidadeSolicitacaoCartao() 

    /**
     * Method hasQuantidadeViaAviso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeViaAviso()
    {
        return this._has_quantidadeViaAviso;
    } //-- boolean hasQuantidadeViaAviso() 

    /**
     * Method hasQuantidadeViaCobranca
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeViaCobranca()
    {
        return this._has_quantidadeViaCobranca;
    } //-- boolean hasQuantidadeViaCobranca() 

    /**
     * Method hasQuantidadeViaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeViaComprovante()
    {
        return this._has_quantidadeViaComprovante;
    } //-- boolean hasQuantidadeViaComprovante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoNaoVida'.
     * 
     * @param cdAcaoNaoVida the value of field 'cdAcaoNaoVida'.
     */
    public void setCdAcaoNaoVida(int cdAcaoNaoVida)
    {
        this._cdAcaoNaoVida = cdAcaoNaoVida;
        this._has_cdAcaoNaoVida = true;
    } //-- void setCdAcaoNaoVida(int) 

    /**
     * Sets the value of field 'cdAcertoDadosRecadastramento'.
     * 
     * @param cdAcertoDadosRecadastramento the value of field
     * 'cdAcertoDadosRecadastramento'.
     */
    public void setCdAcertoDadosRecadastramento(int cdAcertoDadosRecadastramento)
    {
        this._cdAcertoDadosRecadastramento = cdAcertoDadosRecadastramento;
        this._has_cdAcertoDadosRecadastramento = true;
    } //-- void setCdAcertoDadosRecadastramento(int) 

    /**
     * Sets the value of field 'cdAgendamentoDebitoVeiculo'.
     * 
     * @param cdAgendamentoDebitoVeiculo the value of field
     * 'cdAgendamentoDebitoVeiculo'.
     */
    public void setCdAgendamentoDebitoVeiculo(int cdAgendamentoDebitoVeiculo)
    {
        this._cdAgendamentoDebitoVeiculo = cdAgendamentoDebitoVeiculo;
        this._has_cdAgendamentoDebitoVeiculo = true;
    } //-- void setCdAgendamentoDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdAgendamentoPagamentoVencido'.
     * 
     * @param cdAgendamentoPagamentoVencido the value of field
     * 'cdAgendamentoPagamentoVencido'.
     */
    public void setCdAgendamentoPagamentoVencido(int cdAgendamentoPagamentoVencido)
    {
        this._cdAgendamentoPagamentoVencido = cdAgendamentoPagamentoVencido;
        this._has_cdAgendamentoPagamentoVencido = true;
    } //-- void setCdAgendamentoPagamentoVencido(int) 

    /**
     * Sets the value of field 'cdAgendamentoRastreabilidadeFinal'.
     * 
     * @param cdAgendamentoRastreabilidadeFinal the value of field
     * 'cdAgendamentoRastreabilidadeFinal'.
     */
    public void setCdAgendamentoRastreabilidadeFinal(int cdAgendamentoRastreabilidadeFinal)
    {
        this._cdAgendamentoRastreabilidadeFinal = cdAgendamentoRastreabilidadeFinal;
        this._has_cdAgendamentoRastreabilidadeFinal = true;
    } //-- void setCdAgendamentoRastreabilidadeFinal(int) 

    /**
     * Sets the value of field 'cdAgendamentoValorMenor'.
     * 
     * @param cdAgendamentoValorMenor the value of field
     * 'cdAgendamentoValorMenor'.
     */
    public void setCdAgendamentoValorMenor(int cdAgendamentoValorMenor)
    {
        this._cdAgendamentoValorMenor = cdAgendamentoValorMenor;
        this._has_cdAgendamentoValorMenor = true;
    } //-- void setCdAgendamentoValorMenor(int) 

    /**
     * Sets the value of field 'cdAgrupamentoAviso'.
     * 
     * @param cdAgrupamentoAviso the value of field
     * 'cdAgrupamentoAviso'.
     */
    public void setCdAgrupamentoAviso(int cdAgrupamentoAviso)
    {
        this._cdAgrupamentoAviso = cdAgrupamentoAviso;
        this._has_cdAgrupamentoAviso = true;
    } //-- void setCdAgrupamentoAviso(int) 

    /**
     * Sets the value of field 'cdAgrupamentoComprovante'.
     * 
     * @param cdAgrupamentoComprovante the value of field
     * 'cdAgrupamentoComprovante'.
     */
    public void setCdAgrupamentoComprovante(int cdAgrupamentoComprovante)
    {
        this._cdAgrupamentoComprovante = cdAgrupamentoComprovante;
        this._has_cdAgrupamentoComprovante = true;
    } //-- void setCdAgrupamentoComprovante(int) 

    /**
     * Sets the value of field 'cdAgrupamentoFormularioRecadastro'.
     * 
     * @param cdAgrupamentoFormularioRecadastro the value of field
     * 'cdAgrupamentoFormularioRecadastro'.
     */
    public void setCdAgrupamentoFormularioRecadastro(int cdAgrupamentoFormularioRecadastro)
    {
        this._cdAgrupamentoFormularioRecadastro = cdAgrupamentoFormularioRecadastro;
        this._has_cdAgrupamentoFormularioRecadastro = true;
    } //-- void setCdAgrupamentoFormularioRecadastro(int) 

    /**
     * Sets the value of field 'cdAmbienteServicoContrato'.
     * 
     * @param cdAmbienteServicoContrato the value of field
     * 'cdAmbienteServicoContrato'.
     */
    public void setCdAmbienteServicoContrato(java.lang.String cdAmbienteServicoContrato)
    {
        this._cdAmbienteServicoContrato = cdAmbienteServicoContrato;
    } //-- void setCdAmbienteServicoContrato(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdAntecipacaoRecadastramentoBeneficiario'.
     * 
     * @param cdAntecipacaoRecadastramentoBeneficiario the value of
     * field 'cdAntecipacaoRecadastramentoBeneficiario'.
     */
    public void setCdAntecipacaoRecadastramentoBeneficiario(int cdAntecipacaoRecadastramentoBeneficiario)
    {
        this._cdAntecipacaoRecadastramentoBeneficiario = cdAntecipacaoRecadastramentoBeneficiario;
        this._has_cdAntecipacaoRecadastramentoBeneficiario = true;
    } //-- void setCdAntecipacaoRecadastramentoBeneficiario(int) 

    /**
     * Sets the value of field 'cdAreaReservada'.
     * 
     * @param cdAreaReservada the value of field 'cdAreaReservada'.
     */
    public void setCdAreaReservada(int cdAreaReservada)
    {
        this._cdAreaReservada = cdAreaReservada;
        this._has_cdAreaReservada = true;
    } //-- void setCdAreaReservada(int) 

    /**
     * Sets the value of field 'cdBaseRecadastramentoBeneficio'.
     * 
     * @param cdBaseRecadastramentoBeneficio the value of field
     * 'cdBaseRecadastramentoBeneficio'.
     */
    public void setCdBaseRecadastramentoBeneficio(int cdBaseRecadastramentoBeneficio)
    {
        this._cdBaseRecadastramentoBeneficio = cdBaseRecadastramentoBeneficio;
        this._has_cdBaseRecadastramentoBeneficio = true;
    } //-- void setCdBaseRecadastramentoBeneficio(int) 

    /**
     * Sets the value of field 'cdBloqueioEmissaoPapeleta'.
     * 
     * @param cdBloqueioEmissaoPapeleta the value of field
     * 'cdBloqueioEmissaoPapeleta'.
     */
    public void setCdBloqueioEmissaoPapeleta(int cdBloqueioEmissaoPapeleta)
    {
        this._cdBloqueioEmissaoPapeleta = cdBloqueioEmissaoPapeleta;
        this._has_cdBloqueioEmissaoPapeleta = true;
    } //-- void setCdBloqueioEmissaoPapeleta(int) 

    /**
     * Sets the value of field 'cdCanalAlteracao'.
     * 
     * @param cdCanalAlteracao the value of field 'cdCanalAlteracao'
     */
    public void setCdCanalAlteracao(int cdCanalAlteracao)
    {
        this._cdCanalAlteracao = cdCanalAlteracao;
        this._has_cdCanalAlteracao = true;
    } //-- void setCdCanalAlteracao(int) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCapturaTituloRegistrado'.
     * 
     * @param cdCapturaTituloRegistrado the value of field
     * 'cdCapturaTituloRegistrado'.
     */
    public void setCdCapturaTituloRegistrado(int cdCapturaTituloRegistrado)
    {
        this._cdCapturaTituloRegistrado = cdCapturaTituloRegistrado;
        this._has_cdCapturaTituloRegistrado = true;
    } //-- void setCdCapturaTituloRegistrado(int) 

    /**
     * Sets the value of field 'cdCctciaEspeBeneficio'.
     * 
     * @param cdCctciaEspeBeneficio the value of field
     * 'cdCctciaEspeBeneficio'.
     */
    public void setCdCctciaEspeBeneficio(int cdCctciaEspeBeneficio)
    {
        this._cdCctciaEspeBeneficio = cdCctciaEspeBeneficio;
        this._has_cdCctciaEspeBeneficio = true;
    } //-- void setCdCctciaEspeBeneficio(int) 

    /**
     * Sets the value of field 'cdCctciaIdentificacaoBeneficio'.
     * 
     * @param cdCctciaIdentificacaoBeneficio the value of field
     * 'cdCctciaIdentificacaoBeneficio'.
     */
    public void setCdCctciaIdentificacaoBeneficio(int cdCctciaIdentificacaoBeneficio)
    {
        this._cdCctciaIdentificacaoBeneficio = cdCctciaIdentificacaoBeneficio;
        this._has_cdCctciaIdentificacaoBeneficio = true;
    } //-- void setCdCctciaIdentificacaoBeneficio(int) 

    /**
     * Sets the value of field 'cdCctciaInscricaoFavorecido'.
     * 
     * @param cdCctciaInscricaoFavorecido the value of field
     * 'cdCctciaInscricaoFavorecido'.
     */
    public void setCdCctciaInscricaoFavorecido(int cdCctciaInscricaoFavorecido)
    {
        this._cdCctciaInscricaoFavorecido = cdCctciaInscricaoFavorecido;
        this._has_cdCctciaInscricaoFavorecido = true;
    } //-- void setCdCctciaInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdCctciaProprietarioVeculo'.
     * 
     * @param cdCctciaProprietarioVeculo the value of field
     * 'cdCctciaProprietarioVeculo'.
     */
    public void setCdCctciaProprietarioVeculo(int cdCctciaProprietarioVeculo)
    {
        this._cdCctciaProprietarioVeculo = cdCctciaProprietarioVeculo;
        this._has_cdCctciaProprietarioVeculo = true;
    } //-- void setCdCctciaProprietarioVeculo(int) 

    /**
     * Sets the value of field 'cdCobrancaTarifa'.
     * 
     * @param cdCobrancaTarifa the value of field 'cdCobrancaTarifa'
     */
    public void setCdCobrancaTarifa(int cdCobrancaTarifa)
    {
        this._cdCobrancaTarifa = cdCobrancaTarifa;
        this._has_cdCobrancaTarifa = true;
    } //-- void setCdCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @param cdConsistenciaCpfCnpjBenefAvalNpc the value of field
     * 'cdConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public void setCdConsistenciaCpfCnpjBenefAvalNpc(int cdConsistenciaCpfCnpjBenefAvalNpc)
    {
        this._cdConsistenciaCpfCnpjBenefAvalNpc = cdConsistenciaCpfCnpjBenefAvalNpc;
        this._has_cdConsistenciaCpfCnpjBenefAvalNpc = true;
    } //-- void setCdConsistenciaCpfCnpjBenefAvalNpc(int) 

    /**
     * Sets the value of field 'cdConsultaDebitoVeiculo'.
     * 
     * @param cdConsultaDebitoVeiculo the value of field
     * 'cdConsultaDebitoVeiculo'.
     */
    public void setCdConsultaDebitoVeiculo(int cdConsultaDebitoVeiculo)
    {
        this._cdConsultaDebitoVeiculo = cdConsultaDebitoVeiculo;
        this._has_cdConsultaDebitoVeiculo = true;
    } //-- void setCdConsultaDebitoVeiculo(int) 

    /**
     * Sets the value of field 'cdConsultaEndereco'.
     * 
     * @param cdConsultaEndereco the value of field
     * 'cdConsultaEndereco'.
     */
    public void setCdConsultaEndereco(int cdConsultaEndereco)
    {
        this._cdConsultaEndereco = cdConsultaEndereco;
        this._has_cdConsultaEndereco = true;
    } //-- void setCdConsultaEndereco(int) 

    /**
     * Sets the value of field 'cdConsultaSaldoPagamento'.
     * 
     * @param cdConsultaSaldoPagamento the value of field
     * 'cdConsultaSaldoPagamento'.
     */
    public void setCdConsultaSaldoPagamento(int cdConsultaSaldoPagamento)
    {
        this._cdConsultaSaldoPagamento = cdConsultaSaldoPagamento;
        this._has_cdConsultaSaldoPagamento = true;
    } //-- void setCdConsultaSaldoPagamento(int) 

    /**
     * Sets the value of field 'cdConsultaSaldoValorSuperior'.
     * 
     * @param cdConsultaSaldoValorSuperior the value of field
     * 'cdConsultaSaldoValorSuperior'.
     */
    public void setCdConsultaSaldoValorSuperior(int cdConsultaSaldoValorSuperior)
    {
        this._cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
        this._has_cdConsultaSaldoValorSuperior = true;
    } //-- void setCdConsultaSaldoValorSuperior(int) 

    /**
     * Sets the value of field 'cdContagemConsultaSaldo'.
     * 
     * @param cdContagemConsultaSaldo the value of field
     * 'cdContagemConsultaSaldo'.
     */
    public void setCdContagemConsultaSaldo(int cdContagemConsultaSaldo)
    {
        this._cdContagemConsultaSaldo = cdContagemConsultaSaldo;
        this._has_cdContagemConsultaSaldo = true;
    } //-- void setCdContagemConsultaSaldo(int) 

    /**
     * Sets the value of field 'cdCreditoNaoUtilizado'.
     * 
     * @param cdCreditoNaoUtilizado the value of field
     * 'cdCreditoNaoUtilizado'.
     */
    public void setCdCreditoNaoUtilizado(int cdCreditoNaoUtilizado)
    {
        this._cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
        this._has_cdCreditoNaoUtilizado = true;
    } //-- void setCdCreditoNaoUtilizado(int) 

    /**
     * Sets the value of field 'cdCriterioEnquadraRecadastramento'.
     * 
     * @param cdCriterioEnquadraRecadastramento the value of field
     * 'cdCriterioEnquadraRecadastramento'.
     */
    public void setCdCriterioEnquadraRecadastramento(int cdCriterioEnquadraRecadastramento)
    {
        this._cdCriterioEnquadraRecadastramento = cdCriterioEnquadraRecadastramento;
        this._has_cdCriterioEnquadraRecadastramento = true;
    } //-- void setCdCriterioEnquadraRecadastramento(int) 

    /**
     * Sets the value of field 'cdCriterioEnquandraBeneficio'.
     * 
     * @param cdCriterioEnquandraBeneficio the value of field
     * 'cdCriterioEnquandraBeneficio'.
     */
    public void setCdCriterioEnquandraBeneficio(int cdCriterioEnquandraBeneficio)
    {
        this._cdCriterioEnquandraBeneficio = cdCriterioEnquandraBeneficio;
        this._has_cdCriterioEnquandraBeneficio = true;
    } //-- void setCdCriterioEnquandraBeneficio(int) 

    /**
     * Sets the value of field 'cdCriterioRastreabilidadeTitulo'.
     * 
     * @param cdCriterioRastreabilidadeTitulo the value of field
     * 'cdCriterioRastreabilidadeTitulo'.
     */
    public void setCdCriterioRastreabilidadeTitulo(int cdCriterioRastreabilidadeTitulo)
    {
        this._cdCriterioRastreabilidadeTitulo = cdCriterioRastreabilidadeTitulo;
        this._has_cdCriterioRastreabilidadeTitulo = true;
    } //-- void setCdCriterioRastreabilidadeTitulo(int) 

    /**
     * Sets the value of field 'cdDestinoAviso'.
     * 
     * @param cdDestinoAviso the value of field 'cdDestinoAviso'.
     */
    public void setCdDestinoAviso(int cdDestinoAviso)
    {
        this._cdDestinoAviso = cdDestinoAviso;
        this._has_cdDestinoAviso = true;
    } //-- void setCdDestinoAviso(int) 

    /**
     * Sets the value of field 'cdDestinoComprovante'.
     * 
     * @param cdDestinoComprovante the value of field
     * 'cdDestinoComprovante'.
     */
    public void setCdDestinoComprovante(int cdDestinoComprovante)
    {
        this._cdDestinoComprovante = cdDestinoComprovante;
        this._has_cdDestinoComprovante = true;
    } //-- void setCdDestinoComprovante(int) 

    /**
     * Sets the value of field
     * 'cdDestinoFormularioRecadastramento'.
     * 
     * @param cdDestinoFormularioRecadastramento the value of field
     * 'cdDestinoFormularioRecadastramento'.
     */
    public void setCdDestinoFormularioRecadastramento(int cdDestinoFormularioRecadastramento)
    {
        this._cdDestinoFormularioRecadastramento = cdDestinoFormularioRecadastramento;
        this._has_cdDestinoFormularioRecadastramento = true;
    } //-- void setCdDestinoFormularioRecadastramento(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoContaCredito'.
     * 
     * @param cdDisponibilizacaoContaCredito the value of field
     * 'cdDisponibilizacaoContaCredito'.
     */
    public void setCdDisponibilizacaoContaCredito(int cdDisponibilizacaoContaCredito)
    {
        this._cdDisponibilizacaoContaCredito = cdDisponibilizacaoContaCredito;
        this._has_cdDisponibilizacaoContaCredito = true;
    } //-- void setCdDisponibilizacaoContaCredito(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoDiversoCriterio'.
     * 
     * @param cdDisponibilizacaoDiversoCriterio the value of field
     * 'cdDisponibilizacaoDiversoCriterio'.
     */
    public void setCdDisponibilizacaoDiversoCriterio(int cdDisponibilizacaoDiversoCriterio)
    {
        this._cdDisponibilizacaoDiversoCriterio = cdDisponibilizacaoDiversoCriterio;
        this._has_cdDisponibilizacaoDiversoCriterio = true;
    } //-- void setCdDisponibilizacaoDiversoCriterio(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoDiversoNao'.
     * 
     * @param cdDisponibilizacaoDiversoNao the value of field
     * 'cdDisponibilizacaoDiversoNao'.
     */
    public void setCdDisponibilizacaoDiversoNao(int cdDisponibilizacaoDiversoNao)
    {
        this._cdDisponibilizacaoDiversoNao = cdDisponibilizacaoDiversoNao;
        this._has_cdDisponibilizacaoDiversoNao = true;
    } //-- void setCdDisponibilizacaoDiversoNao(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoSalarioCriterio'.
     * 
     * @param cdDisponibilizacaoSalarioCriterio the value of field
     * 'cdDisponibilizacaoSalarioCriterio'.
     */
    public void setCdDisponibilizacaoSalarioCriterio(int cdDisponibilizacaoSalarioCriterio)
    {
        this._cdDisponibilizacaoSalarioCriterio = cdDisponibilizacaoSalarioCriterio;
        this._has_cdDisponibilizacaoSalarioCriterio = true;
    } //-- void setCdDisponibilizacaoSalarioCriterio(int) 

    /**
     * Sets the value of field 'cdDisponibilizacaoSalarioNao'.
     * 
     * @param cdDisponibilizacaoSalarioNao the value of field
     * 'cdDisponibilizacaoSalarioNao'.
     */
    public void setCdDisponibilizacaoSalarioNao(int cdDisponibilizacaoSalarioNao)
    {
        this._cdDisponibilizacaoSalarioNao = cdDisponibilizacaoSalarioNao;
        this._has_cdDisponibilizacaoSalarioNao = true;
    } //-- void setCdDisponibilizacaoSalarioNao(int) 

    /**
     * Sets the value of field 'cdEnvelopeAberto'.
     * 
     * @param cdEnvelopeAberto the value of field 'cdEnvelopeAberto'
     */
    public void setCdEnvelopeAberto(int cdEnvelopeAberto)
    {
        this._cdEnvelopeAberto = cdEnvelopeAberto;
        this._has_cdEnvelopeAberto = true;
    } //-- void setCdEnvelopeAberto(int) 

    /**
     * Sets the value of field 'cdExigeAutFilial'.
     * 
     * @param cdExigeAutFilial the value of field 'cdExigeAutFilial'
     */
    public void setCdExigeAutFilial(int cdExigeAutFilial)
    {
        this._cdExigeAutFilial = cdExigeAutFilial;
        this._has_cdExigeAutFilial = true;
    } //-- void setCdExigeAutFilial(int) 

    /**
     * Sets the value of field 'cdFavorecidoConsultaPagamento'.
     * 
     * @param cdFavorecidoConsultaPagamento the value of field
     * 'cdFavorecidoConsultaPagamento'.
     */
    public void setCdFavorecidoConsultaPagamento(int cdFavorecidoConsultaPagamento)
    {
        this._cdFavorecidoConsultaPagamento = cdFavorecidoConsultaPagamento;
        this._has_cdFavorecidoConsultaPagamento = true;
    } //-- void setCdFavorecidoConsultaPagamento(int) 

    /**
     * Sets the value of field 'cdFloatServicoContrato'.
     * 
     * @param cdFloatServicoContrato the value of field
     * 'cdFloatServicoContrato'.
     */
    public void setCdFloatServicoContrato(int cdFloatServicoContrato)
    {
        this._cdFloatServicoContrato = cdFloatServicoContrato;
        this._has_cdFloatServicoContrato = true;
    } //-- void setCdFloatServicoContrato(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFormaEnvioPagamento'.
     * 
     * @param cdFormaEnvioPagamento the value of field
     * 'cdFormaEnvioPagamento'.
     */
    public void setCdFormaEnvioPagamento(int cdFormaEnvioPagamento)
    {
        this._cdFormaEnvioPagamento = cdFormaEnvioPagamento;
        this._has_cdFormaEnvioPagamento = true;
    } //-- void setCdFormaEnvioPagamento(int) 

    /**
     * Sets the value of field 'cdFormaEstornoCredito'.
     * 
     * @param cdFormaEstornoCredito the value of field
     * 'cdFormaEstornoCredito'.
     */
    public void setCdFormaEstornoCredito(int cdFormaEstornoCredito)
    {
        this._cdFormaEstornoCredito = cdFormaEstornoCredito;
        this._has_cdFormaEstornoCredito = true;
    } //-- void setCdFormaEstornoCredito(int) 

    /**
     * Sets the value of field 'cdFormaExpiracaoCredito'.
     * 
     * @param cdFormaExpiracaoCredito the value of field
     * 'cdFormaExpiracaoCredito'.
     */
    public void setCdFormaExpiracaoCredito(int cdFormaExpiracaoCredito)
    {
        this._cdFormaExpiracaoCredito = cdFormaExpiracaoCredito;
        this._has_cdFormaExpiracaoCredito = true;
    } //-- void setCdFormaExpiracaoCredito(int) 

    /**
     * Sets the value of field 'cdFormaManutencao'.
     * 
     * @param cdFormaManutencao the value of field
     * 'cdFormaManutencao'.
     */
    public void setCdFormaManutencao(int cdFormaManutencao)
    {
        this._cdFormaManutencao = cdFormaManutencao;
        this._has_cdFormaManutencao = true;
    } //-- void setCdFormaManutencao(int) 

    /**
     * Sets the value of field 'cdFrasePrecadastrada'.
     * 
     * @param cdFrasePrecadastrada the value of field
     * 'cdFrasePrecadastrada'.
     */
    public void setCdFrasePrecadastrada(int cdFrasePrecadastrada)
    {
        this._cdFrasePrecadastrada = cdFrasePrecadastrada;
        this._has_cdFrasePrecadastrada = true;
    } //-- void setCdFrasePrecadastrada(int) 

    /**
     * Sets the value of field
     * 'cdIdentificadortipoRetornoInternet'.
     * 
     * @param cdIdentificadortipoRetornoInternet the value of field
     * 'cdIdentificadortipoRetornoInternet'.
     */
    public void setCdIdentificadorTipoRetornoInternet(int cdIdentificadorTipoRetornoInternet)
    {
        this._cdIdentificadorTipoRetornoInternet = cdIdentificadorTipoRetornoInternet;
        this._has_cdIdentificadorTipoRetornoInternet = true;
    } //-- void setCdIdentificadortipoRetornoInternet(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendaGrade'.
     * 
     * @param cdIndicadorAgendaGrade the value of field
     * 'cdIndicadorAgendaGrade'.
     */
    public void setCdIndicadorAgendaGrade(int cdIndicadorAgendaGrade)
    {
        this._cdIndicadorAgendaGrade = cdIndicadorAgendaGrade;
        this._has_cdIndicadorAgendaGrade = true;
    } //-- void setCdIndicadorAgendaGrade(int) 

    /**
     * Sets the value of field 'cdIndicadorAgendamentoTitulo'.
     * 
     * @param cdIndicadorAgendamentoTitulo the value of field
     * 'cdIndicadorAgendamentoTitulo'.
     */
    public void setCdIndicadorAgendamentoTitulo(int cdIndicadorAgendamentoTitulo)
    {
        this._cdIndicadorAgendamentoTitulo = cdIndicadorAgendamentoTitulo;
        this._has_cdIndicadorAgendamentoTitulo = true;
    } //-- void setCdIndicadorAgendamentoTitulo(int) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @param cdIndicadorAutorizacaoCliente the value of field
     * 'cdIndicadorAutorizacaoCliente'.
     */
    public void setCdIndicadorAutorizacaoCliente(int cdIndicadorAutorizacaoCliente)
    {
        this._cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
        this._has_cdIndicadorAutorizacaoCliente = true;
    } //-- void setCdIndicadorAutorizacaoCliente(int) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @param cdIndicadorAutorizacaoComplemento the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public void setCdIndicadorAutorizacaoComplemento(int cdIndicadorAutorizacaoComplemento)
    {
        this._cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
        this._has_cdIndicadorAutorizacaoComplemento = true;
    } //-- void setCdIndicadorAutorizacaoComplemento(int) 

    /**
     * Sets the value of field 'cdIndicadorBancoPostal'.
     * 
     * @param cdIndicadorBancoPostal the value of field
     * 'cdIndicadorBancoPostal'.
     */
    public void setCdIndicadorBancoPostal(int cdIndicadorBancoPostal)
    {
        this._cdIndicadorBancoPostal = cdIndicadorBancoPostal;
        this._has_cdIndicadorBancoPostal = true;
    } //-- void setCdIndicadorBancoPostal(int) 

    /**
     * Sets the value of field 'cdIndicadorCadastroProcurador'.
     * 
     * @param cdIndicadorCadastroProcurador the value of field
     * 'cdIndicadorCadastroProcurador'.
     */
    public void setCdIndicadorCadastroProcurador(int cdIndicadorCadastroProcurador)
    {
        this._cdIndicadorCadastroProcurador = cdIndicadorCadastroProcurador;
        this._has_cdIndicadorCadastroProcurador = true;
    } //-- void setCdIndicadorCadastroProcurador(int) 

    /**
     * Sets the value of field 'cdIndicadorCadastroorganizacao'.
     * 
     * @param cdIndicadorCadastroorganizacao the value of field
     * 'cdIndicadorCadastroorganizacao'.
     */
    public void setCdIndicadorCadastroorganizacao(int cdIndicadorCadastroorganizacao)
    {
        this._cdIndicadorCadastroorganizacao = cdIndicadorCadastroorganizacao;
        this._has_cdIndicadorCadastroorganizacao = true;
    } //-- void setCdIndicadorCadastroorganizacao(int) 

    /**
     * Sets the value of field 'cdIndicadorCartaoSalario'.
     * 
     * @param cdIndicadorCartaoSalario the value of field
     * 'cdIndicadorCartaoSalario'.
     */
    public void setCdIndicadorCartaoSalario(int cdIndicadorCartaoSalario)
    {
        this._cdIndicadorCartaoSalario = cdIndicadorCartaoSalario;
        this._has_cdIndicadorCartaoSalario = true;
    } //-- void setCdIndicadorCartaoSalario(int) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @param cdIndicadorEconomicoReajuste the value of field
     * 'cdIndicadorEconomicoReajuste'.
     */
    public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste)
    {
        this._cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
        this._has_cdIndicadorEconomicoReajuste = true;
    } //-- void setCdIndicadorEconomicoReajuste(int) 

    /**
     * Sets the value of field 'cdIndicadorFeriadoLocal'.
     * 
     * @param cdIndicadorFeriadoLocal the value of field
     * 'cdIndicadorFeriadoLocal'.
     */
    public void setCdIndicadorFeriadoLocal(int cdIndicadorFeriadoLocal)
    {
        this._cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
        this._has_cdIndicadorFeriadoLocal = true;
    } //-- void setCdIndicadorFeriadoLocal(int) 

    /**
     * Sets the value of field 'cdIndicadorListaDebito'.
     * 
     * @param cdIndicadorListaDebito the value of field
     * 'cdIndicadorListaDebito'.
     */
    public void setCdIndicadorListaDebito(int cdIndicadorListaDebito)
    {
        this._cdIndicadorListaDebito = cdIndicadorListaDebito;
        this._has_cdIndicadorListaDebito = true;
    } //-- void setCdIndicadorListaDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorMensagemPersonalizada'.
     * 
     * @param cdIndicadorMensagemPersonalizada the value of field
     * 'cdIndicadorMensagemPersonalizada'.
     */
    public void setCdIndicadorMensagemPersonalizada(int cdIndicadorMensagemPersonalizada)
    {
        this._cdIndicadorMensagemPersonalizada = cdIndicadorMensagemPersonalizada;
        this._has_cdIndicadorMensagemPersonalizada = true;
    } //-- void setCdIndicadorMensagemPersonalizada(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @param cdIndicadorRetornoInternet the value of field
     * 'cdIndicadorRetornoInternet'.
     */
    public void setCdIndicadorRetornoInternet(int cdIndicadorRetornoInternet)
    {
        this._cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
        this._has_cdIndicadorRetornoInternet = true;
    } //-- void setCdIndicadorRetornoInternet(int) 

    /**
     * Sets the value of field 'cdIndicadorSegundaLinha'.
     * 
     * @param cdIndicadorSegundaLinha the value of field
     * 'cdIndicadorSegundaLinha'.
     */
    public void setCdIndicadorSegundaLinha(int cdIndicadorSegundaLinha)
    {
        this._cdIndicadorSegundaLinha = cdIndicadorSegundaLinha;
        this._has_cdIndicadorSegundaLinha = true;
    } //-- void setCdIndicadorSegundaLinha(int) 

    /**
     * Sets the value of field 'cdIndicadorUtilizaMora'.
     * 
     * @param cdIndicadorUtilizaMora the value of field
     * 'cdIndicadorUtilizaMora'.
     */
    public void setCdIndicadorUtilizaMora(int cdIndicadorUtilizaMora)
    {
        this._cdIndicadorUtilizaMora = cdIndicadorUtilizaMora;
        this._has_cdIndicadorUtilizaMora = true;
    } //-- void setCdIndicadorUtilizaMora(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoCredito'.
     * 
     * @param cdLancamentoFuturoCredito the value of field
     * 'cdLancamentoFuturoCredito'.
     */
    public void setCdLancamentoFuturoCredito(int cdLancamentoFuturoCredito)
    {
        this._cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
        this._has_cdLancamentoFuturoCredito = true;
    } //-- void setCdLancamentoFuturoCredito(int) 

    /**
     * Sets the value of field 'cdLancamentoFuturoDebito'.
     * 
     * @param cdLancamentoFuturoDebito the value of field
     * 'cdLancamentoFuturoDebito'.
     */
    public void setCdLancamentoFuturoDebito(int cdLancamentoFuturoDebito)
    {
        this._cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
        this._has_cdLancamentoFuturoDebito = true;
    } //-- void setCdLancamentoFuturoDebito(int) 

    /**
     * Sets the value of field 'cdLiberacaoLoteProcessado'.
     * 
     * @param cdLiberacaoLoteProcessado the value of field
     * 'cdLiberacaoLoteProcessado'.
     */
    public void setCdLiberacaoLoteProcessado(int cdLiberacaoLoteProcessado)
    {
        this._cdLiberacaoLoteProcessado = cdLiberacaoLoteProcessado;
        this._has_cdLiberacaoLoteProcessado = true;
    } //-- void setCdLiberacaoLoteProcessado(int) 

    /**
     * Sets the value of field 'cdLocalEmissao'.
     * 
     * @param cdLocalEmissao the value of field 'cdLocalEmissao'.
     */
    public void setCdLocalEmissao(java.lang.String cdLocalEmissao)
    {
        this._cdLocalEmissao = cdLocalEmissao;
    } //-- void setCdLocalEmissao(java.lang.String) 

    /**
     * Sets the value of field 'cdManutencaoBaseRecadastramento'.
     * 
     * @param cdManutencaoBaseRecadastramento the value of field
     * 'cdManutencaoBaseRecadastramento'.
     */
    public void setCdManutencaoBaseRecadastramento(int cdManutencaoBaseRecadastramento)
    {
        this._cdManutencaoBaseRecadastramento = cdManutencaoBaseRecadastramento;
        this._has_cdManutencaoBaseRecadastramento = true;
    } //-- void setCdManutencaoBaseRecadastramento(int) 

    /**
     * Sets the value of field 'cdMeioPagamentoDebito'.
     * 
     * @param cdMeioPagamentoDebito the value of field
     * 'cdMeioPagamentoDebito'.
     */
    public void setCdMeioPagamentoDebito(int cdMeioPagamentoDebito)
    {
        this._cdMeioPagamentoDebito = cdMeioPagamentoDebito;
        this._has_cdMeioPagamentoDebito = true;
    } //-- void setCdMeioPagamentoDebito(int) 

    /**
     * Sets the value of field 'cdMensagemRecadastramentoMidia'.
     * 
     * @param cdMensagemRecadastramentoMidia the value of field
     * 'cdMensagemRecadastramentoMidia'.
     */
    public void setCdMensagemRecadastramentoMidia(int cdMensagemRecadastramentoMidia)
    {
        this._cdMensagemRecadastramentoMidia = cdMensagemRecadastramentoMidia;
        this._has_cdMensagemRecadastramentoMidia = true;
    } //-- void setCdMensagemRecadastramentoMidia(int) 

    /**
     * Sets the value of field 'cdMidiaDisponivel'.
     * 
     * @param cdMidiaDisponivel the value of field
     * 'cdMidiaDisponivel'.
     */
    public void setCdMidiaDisponivel(int cdMidiaDisponivel)
    {
        this._cdMidiaDisponivel = cdMidiaDisponivel;
        this._has_cdMidiaDisponivel = true;
    } //-- void setCdMidiaDisponivel(int) 

    /**
     * Sets the value of field 'cdMidiaMensagemRecadastramento'.
     * 
     * @param cdMidiaMensagemRecadastramento the value of field
     * 'cdMidiaMensagemRecadastramento'.
     */
    public void setCdMidiaMensagemRecadastramento(int cdMidiaMensagemRecadastramento)
    {
        this._cdMidiaMensagemRecadastramento = cdMidiaMensagemRecadastramento;
        this._has_cdMidiaMensagemRecadastramento = true;
    } //-- void setCdMidiaMensagemRecadastramento(int) 

    /**
     * Sets the value of field 'cdMomentoAvisoRecadastramento'.
     * 
     * @param cdMomentoAvisoRecadastramento the value of field
     * 'cdMomentoAvisoRecadastramento'.
     */
    public void setCdMomentoAvisoRecadastramento(int cdMomentoAvisoRecadastramento)
    {
        this._cdMomentoAvisoRecadastramento = cdMomentoAvisoRecadastramento;
        this._has_cdMomentoAvisoRecadastramento = true;
    } //-- void setCdMomentoAvisoRecadastramento(int) 

    /**
     * Sets the value of field 'cdMomentoCreditoEfetivacao'.
     * 
     * @param cdMomentoCreditoEfetivacao the value of field
     * 'cdMomentoCreditoEfetivacao'.
     */
    public void setCdMomentoCreditoEfetivacao(int cdMomentoCreditoEfetivacao)
    {
        this._cdMomentoCreditoEfetivacao = cdMomentoCreditoEfetivacao;
        this._has_cdMomentoCreditoEfetivacao = true;
    } //-- void setCdMomentoCreditoEfetivacao(int) 

    /**
     * Sets the value of field 'cdMomentoDebitoPagamento'.
     * 
     * @param cdMomentoDebitoPagamento the value of field
     * 'cdMomentoDebitoPagamento'.
     */
    public void setCdMomentoDebitoPagamento(int cdMomentoDebitoPagamento)
    {
        this._cdMomentoDebitoPagamento = cdMomentoDebitoPagamento;
        this._has_cdMomentoDebitoPagamento = true;
    } //-- void setCdMomentoDebitoPagamento(int) 

    /**
     * Sets the value of field
     * 'cdMomentoFormularioRecadastramento'.
     * 
     * @param cdMomentoFormularioRecadastramento the value of field
     * 'cdMomentoFormularioRecadastramento'.
     */
    public void setCdMomentoFormularioRecadastramento(int cdMomentoFormularioRecadastramento)
    {
        this._cdMomentoFormularioRecadastramento = cdMomentoFormularioRecadastramento;
        this._has_cdMomentoFormularioRecadastramento = true;
    } //-- void setCdMomentoFormularioRecadastramento(int) 

    /**
     * Sets the value of field 'cdMomentoProcessamentoPagamento'.
     * 
     * @param cdMomentoProcessamentoPagamento the value of field
     * 'cdMomentoProcessamentoPagamento'.
     */
    public void setCdMomentoProcessamentoPagamento(int cdMomentoProcessamentoPagamento)
    {
        this._cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
        this._has_cdMomentoProcessamentoPagamento = true;
    } //-- void setCdMomentoProcessamentoPagamento(int) 

    /**
     * Sets the value of field 'cdNaturezaOperacaoPagamento'.
     * 
     * @param cdNaturezaOperacaoPagamento the value of field
     * 'cdNaturezaOperacaoPagamento'.
     */
    public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento)
    {
        this._cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
        this._has_cdNaturezaOperacaoPagamento = true;
    } //-- void setCdNaturezaOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdOutraidentificacaoFavorecido'.
     * 
     * @param cdOutraidentificacaoFavorecido the value of field
     * 'cdOutraidentificacaoFavorecido'.
     */
    public void setCdOutraidentificacaoFavorecido(int cdOutraidentificacaoFavorecido)
    {
        this._cdOutraidentificacaoFavorecido = cdOutraidentificacaoFavorecido;
        this._has_cdOutraidentificacaoFavorecido = true;
    } //-- void setCdOutraidentificacaoFavorecido(int) 

    /**
     * Sets the value of field 'cdPagamentoNaoUtil'.
     * 
     * @param cdPagamentoNaoUtil the value of field
     * 'cdPagamentoNaoUtil'.
     */
    public void setCdPagamentoNaoUtil(int cdPagamentoNaoUtil)
    {
        this._cdPagamentoNaoUtil = cdPagamentoNaoUtil;
        this._has_cdPagamentoNaoUtil = true;
    } //-- void setCdPagamentoNaoUtil(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeAviso'.
     * 
     * @param cdPeriodicidadeAviso the value of field
     * 'cdPeriodicidadeAviso'.
     */
    public void setCdPeriodicidadeAviso(int cdPeriodicidadeAviso)
    {
        this._cdPeriodicidadeAviso = cdPeriodicidadeAviso;
        this._has_cdPeriodicidadeAviso = true;
    } //-- void setCdPeriodicidadeAviso(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeCobrancaTarifa'.
     * 
     * @param cdPeriodicidadeCobrancaTarifa the value of field
     * 'cdPeriodicidadeCobrancaTarifa'.
     */
    public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa)
    {
        this._cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
        this._has_cdPeriodicidadeCobrancaTarifa = true;
    } //-- void setCdPeriodicidadeCobrancaTarifa(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeComprovante'.
     * 
     * @param cdPeriodicidadeComprovante the value of field
     * 'cdPeriodicidadeComprovante'.
     */
    public void setCdPeriodicidadeComprovante(int cdPeriodicidadeComprovante)
    {
        this._cdPeriodicidadeComprovante = cdPeriodicidadeComprovante;
        this._has_cdPeriodicidadeComprovante = true;
    } //-- void setCdPeriodicidadeComprovante(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeConsultaVeiculo'.
     * 
     * @param cdPeriodicidadeConsultaVeiculo the value of field
     * 'cdPeriodicidadeConsultaVeiculo'.
     */
    public void setCdPeriodicidadeConsultaVeiculo(int cdPeriodicidadeConsultaVeiculo)
    {
        this._cdPeriodicidadeConsultaVeiculo = cdPeriodicidadeConsultaVeiculo;
        this._has_cdPeriodicidadeConsultaVeiculo = true;
    } //-- void setCdPeriodicidadeConsultaVeiculo(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeEnvioRemessa'.
     * 
     * @param cdPeriodicidadeEnvioRemessa the value of field
     * 'cdPeriodicidadeEnvioRemessa'.
     */
    public void setCdPeriodicidadeEnvioRemessa(int cdPeriodicidadeEnvioRemessa)
    {
        this._cdPeriodicidadeEnvioRemessa = cdPeriodicidadeEnvioRemessa;
        this._has_cdPeriodicidadeEnvioRemessa = true;
    } //-- void setCdPeriodicidadeEnvioRemessa(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeManutencaoProcd'.
     * 
     * @param cdPeriodicidadeManutencaoProcd the value of field
     * 'cdPeriodicidadeManutencaoProcd'.
     */
    public void setCdPeriodicidadeManutencaoProcd(int cdPeriodicidadeManutencaoProcd)
    {
        this._cdPeriodicidadeManutencaoProcd = cdPeriodicidadeManutencaoProcd;
        this._has_cdPeriodicidadeManutencaoProcd = true;
    } //-- void setCdPeriodicidadeManutencaoProcd(int) 

    /**
     * Sets the value of field 'cdPeriodicidadeReajusteTarifa'.
     * 
     * @param cdPeriodicidadeReajusteTarifa the value of field
     * 'cdPeriodicidadeReajusteTarifa'.
     */
    public void setCdPeriodicidadeReajusteTarifa(int cdPeriodicidadeReajusteTarifa)
    {
        this._cdPeriodicidadeReajusteTarifa = cdPeriodicidadeReajusteTarifa;
        this._has_cdPeriodicidadeReajusteTarifa = true;
    } //-- void setCdPeriodicidadeReajusteTarifa(int) 

    /**
     * Sets the value of field 'cdPermissaoDebitoOnline'.
     * 
     * @param cdPermissaoDebitoOnline the value of field
     * 'cdPermissaoDebitoOnline'.
     */
    public void setCdPermissaoDebitoOnline(int cdPermissaoDebitoOnline)
    {
        this._cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
        this._has_cdPermissaoDebitoOnline = true;
    } //-- void setCdPermissaoDebitoOnline(int) 

    /**
     * Sets the value of field
     * 'cdPreenchimentoLancamentoPersonalizado'.
     * 
     * @param cdPreenchimentoLancamentoPersonalizado the value of
     * field 'cdPreenchimentoLancamentoPersonalizado'.
     */
    public void setCdPreenchimentoLancamentoPersonalizado(int cdPreenchimentoLancamentoPersonalizado)
    {
        this._cdPreenchimentoLancamentoPersonalizado = cdPreenchimentoLancamentoPersonalizado;
        this._has_cdPreenchimentoLancamentoPersonalizado = true;
    } //-- void setCdPreenchimentoLancamentoPersonalizado(int) 

    /**
     * Sets the value of field 'cdPrincipalEnquaRecadastramento'.
     * 
     * @param cdPrincipalEnquaRecadastramento the value of field
     * 'cdPrincipalEnquaRecadastramento'.
     */
    public void setCdPrincipalEnquaRecadastramento(int cdPrincipalEnquaRecadastramento)
    {
        this._cdPrincipalEnquaRecadastramento = cdPrincipalEnquaRecadastramento;
        this._has_cdPrincipalEnquaRecadastramento = true;
    } //-- void setCdPrincipalEnquaRecadastramento(int) 

    /**
     * Sets the value of field 'cdPrioridadeEfetivacaoPagamento'.
     * 
     * @param cdPrioridadeEfetivacaoPagamento the value of field
     * 'cdPrioridadeEfetivacaoPagamento'.
     */
    public void setCdPrioridadeEfetivacaoPagamento(int cdPrioridadeEfetivacaoPagamento)
    {
        this._cdPrioridadeEfetivacaoPagamento = cdPrioridadeEfetivacaoPagamento;
        this._has_cdPrioridadeEfetivacaoPagamento = true;
    } //-- void setCdPrioridadeEfetivacaoPagamento(int) 

    /**
     * Sets the value of field 'cdRastreabilidadeNotaFiscal'.
     * 
     * @param cdRastreabilidadeNotaFiscal the value of field
     * 'cdRastreabilidadeNotaFiscal'.
     */
    public void setCdRastreabilidadeNotaFiscal(int cdRastreabilidadeNotaFiscal)
    {
        this._cdRastreabilidadeNotaFiscal = cdRastreabilidadeNotaFiscal;
        this._has_cdRastreabilidadeNotaFiscal = true;
    } //-- void setCdRastreabilidadeNotaFiscal(int) 

    /**
     * Sets the value of field 'cdRastreabilidadeTituloTerceiro'.
     * 
     * @param cdRastreabilidadeTituloTerceiro the value of field
     * 'cdRastreabilidadeTituloTerceiro'.
     */
    public void setCdRastreabilidadeTituloTerceiro(int cdRastreabilidadeTituloTerceiro)
    {
        this._cdRastreabilidadeTituloTerceiro = cdRastreabilidadeTituloTerceiro;
        this._has_cdRastreabilidadeTituloTerceiro = true;
    } //-- void setCdRastreabilidadeTituloTerceiro(int) 

    /**
     * Sets the value of field 'cdRejeicaoAgendamentoLote'.
     * 
     * @param cdRejeicaoAgendamentoLote the value of field
     * 'cdRejeicaoAgendamentoLote'.
     */
    public void setCdRejeicaoAgendamentoLote(int cdRejeicaoAgendamentoLote)
    {
        this._cdRejeicaoAgendamentoLote = cdRejeicaoAgendamentoLote;
        this._has_cdRejeicaoAgendamentoLote = true;
    } //-- void setCdRejeicaoAgendamentoLote(int) 

    /**
     * Sets the value of field 'cdRejeicaoEfetivacaoLote'.
     * 
     * @param cdRejeicaoEfetivacaoLote the value of field
     * 'cdRejeicaoEfetivacaoLote'.
     */
    public void setCdRejeicaoEfetivacaoLote(int cdRejeicaoEfetivacaoLote)
    {
        this._cdRejeicaoEfetivacaoLote = cdRejeicaoEfetivacaoLote;
        this._has_cdRejeicaoEfetivacaoLote = true;
    } //-- void setCdRejeicaoEfetivacaoLote(int) 

    /**
     * Sets the value of field 'cdRejeicaoLote'.
     * 
     * @param cdRejeicaoLote the value of field 'cdRejeicaoLote'.
     */
    public void setCdRejeicaoLote(int cdRejeicaoLote)
    {
        this._cdRejeicaoLote = cdRejeicaoLote;
        this._has_cdRejeicaoLote = true;
    } //-- void setCdRejeicaoLote(int) 

    /**
     * Sets the value of field 'cdTipoCargaRecadastramento'.
     * 
     * @param cdTipoCargaRecadastramento the value of field
     * 'cdTipoCargaRecadastramento'.
     */
    public void setCdTipoCargaRecadastramento(int cdTipoCargaRecadastramento)
    {
        this._cdTipoCargaRecadastramento = cdTipoCargaRecadastramento;
        this._has_cdTipoCargaRecadastramento = true;
    } //-- void setCdTipoCargaRecadastramento(int) 

    /**
     * Sets the value of field 'cdTipoCartaoSalario'.
     * 
     * @param cdTipoCartaoSalario the value of field
     * 'cdTipoCartaoSalario'.
     */
    public void setCdTipoCartaoSalario(int cdTipoCartaoSalario)
    {
        this._cdTipoCartaoSalario = cdTipoCartaoSalario;
        this._has_cdTipoCartaoSalario = true;
    } //-- void setCdTipoCartaoSalario(int) 

    /**
     * Sets the value of field 'cdTipoConsistenciaLista'.
     * 
     * @param cdTipoConsistenciaLista the value of field
     * 'cdTipoConsistenciaLista'.
     */
    public void setCdTipoConsistenciaLista(int cdTipoConsistenciaLista)
    {
        this._cdTipoConsistenciaLista = cdTipoConsistenciaLista;
        this._has_cdTipoConsistenciaLista = true;
    } //-- void setCdTipoConsistenciaLista(int) 

    /**
     * Sets the value of field 'cdTipoConsultaComprovante'.
     * 
     * @param cdTipoConsultaComprovante the value of field
     * 'cdTipoConsultaComprovante'.
     */
    public void setCdTipoConsultaComprovante(int cdTipoConsultaComprovante)
    {
        this._cdTipoConsultaComprovante = cdTipoConsultaComprovante;
        this._has_cdTipoConsultaComprovante = true;
    } //-- void setCdTipoConsultaComprovante(int) 

    /**
     * Sets the value of field 'cdTipoDataFloating'.
     * 
     * @param cdTipoDataFloating the value of field
     * 'cdTipoDataFloating'.
     */
    public void setCdTipoDataFloating(int cdTipoDataFloating)
    {
        this._cdTipoDataFloating = cdTipoDataFloating;
        this._has_cdTipoDataFloating = true;
    } //-- void setCdTipoDataFloating(int) 

    /**
     * Sets the value of field 'cdTipoDivergenciaVeiculo'.
     * 
     * @param cdTipoDivergenciaVeiculo the value of field
     * 'cdTipoDivergenciaVeiculo'.
     */
    public void setCdTipoDivergenciaVeiculo(int cdTipoDivergenciaVeiculo)
    {
        this._cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
        this._has_cdTipoDivergenciaVeiculo = true;
    } //-- void setCdTipoDivergenciaVeiculo(int) 

    /**
     * Sets the value of field 'cdTipoEfetivacaoPagamento'.
     * 
     * @param cdTipoEfetivacaoPagamento the value of field
     * 'cdTipoEfetivacaoPagamento'.
     */
    public void setCdTipoEfetivacaoPagamento(int cdTipoEfetivacaoPagamento)
    {
        this._cdTipoEfetivacaoPagamento = cdTipoEfetivacaoPagamento;
        this._has_cdTipoEfetivacaoPagamento = true;
    } //-- void setCdTipoEfetivacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoFormacaoLista'.
     * 
     * @param cdTipoFormacaoLista the value of field
     * 'cdTipoFormacaoLista'.
     */
    public void setCdTipoFormacaoLista(int cdTipoFormacaoLista)
    {
        this._cdTipoFormacaoLista = cdTipoFormacaoLista;
        this._has_cdTipoFormacaoLista = true;
    } //-- void setCdTipoFormacaoLista(int) 

    /**
     * Sets the value of field 'cdTipoIdentificacaoBeneficio'.
     * 
     * @param cdTipoIdentificacaoBeneficio the value of field
     * 'cdTipoIdentificacaoBeneficio'.
     */
    public void setCdTipoIdentificacaoBeneficio(int cdTipoIdentificacaoBeneficio)
    {
        this._cdTipoIdentificacaoBeneficio = cdTipoIdentificacaoBeneficio;
        this._has_cdTipoIdentificacaoBeneficio = true;
    } //-- void setCdTipoIdentificacaoBeneficio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoReajusteTarifa'.
     * 
     * @param cdTipoReajusteTarifa the value of field
     * 'cdTipoReajusteTarifa'.
     */
    public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa)
    {
        this._cdTipoReajusteTarifa = cdTipoReajusteTarifa;
        this._has_cdTipoReajusteTarifa = true;
    } //-- void setCdTipoReajusteTarifa(int) 

    /**
     * Sets the value of field 'cdTituloDdaRetorno'.
     * 
     * @param cdTituloDdaRetorno the value of field
     * 'cdTituloDdaRetorno'.
     */
    public void setCdTituloDdaRetorno(int cdTituloDdaRetorno)
    {
        this._cdTituloDdaRetorno = cdTituloDdaRetorno;
        this._has_cdTituloDdaRetorno = true;
    } //-- void setCdTituloDdaRetorno(int) 

    /**
     * Sets the value of field 'cdTratamentoContaTransferida'.
     * 
     * @param cdTratamentoContaTransferida the value of field
     * 'cdTratamentoContaTransferida'.
     */
    public void setCdTratamentoContaTransferida(int cdTratamentoContaTransferida)
    {
        this._cdTratamentoContaTransferida = cdTratamentoContaTransferida;
        this._has_cdTratamentoContaTransferida = true;
    } //-- void setCdTratamentoContaTransferida(int) 

    /**
     * Sets the value of field 'cdUsuarioAlteracao'.
     * 
     * @param cdUsuarioAlteracao the value of field
     * 'cdUsuarioAlteracao'.
     */
    public void setCdUsuarioAlteracao(java.lang.String cdUsuarioAlteracao)
    {
        this._cdUsuarioAlteracao = cdUsuarioAlteracao;
    } //-- void setCdUsuarioAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioExternoAlteracao'.
     * 
     * @param cdUsuarioExternoAlteracao the value of field
     * 'cdUsuarioExternoAlteracao'.
     */
    public void setCdUsuarioExternoAlteracao(java.lang.String cdUsuarioExternoAlteracao)
    {
        this._cdUsuarioExternoAlteracao = cdUsuarioExternoAlteracao;
    } //-- void setCdUsuarioExternoAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioExternoInclusao'.
     * 
     * @param cdUsuarioExternoInclusao the value of field
     * 'cdUsuarioExternoInclusao'.
     */
    public void setCdUsuarioExternoInclusao(java.lang.String cdUsuarioExternoInclusao)
    {
        this._cdUsuarioExternoInclusao = cdUsuarioExternoInclusao;
    } //-- void setCdUsuarioExternoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUtilizacaoFavorecidoControle'.
     * 
     * @param cdUtilizacaoFavorecidoControle the value of field
     * 'cdUtilizacaoFavorecidoControle'.
     */
    public void setCdUtilizacaoFavorecidoControle(int cdUtilizacaoFavorecidoControle)
    {
        this._cdUtilizacaoFavorecidoControle = cdUtilizacaoFavorecidoControle;
        this._has_cdUtilizacaoFavorecidoControle = true;
    } //-- void setCdUtilizacaoFavorecidoControle(int) 

    /**
     * Sets the value of field 'cdindicadorExpiraCredito'.
     * 
     * @param cdindicadorExpiraCredito the value of field
     * 'cdindicadorExpiraCredito'.
     */
    public void setCdindicadorExpiraCredito(int cdindicadorExpiraCredito)
    {
        this._cdindicadorExpiraCredito = cdindicadorExpiraCredito;
        this._has_cdindicadorExpiraCredito = true;
    } //-- void setCdindicadorExpiraCredito(int) 

    /**
     * Sets the value of field 'cdindicadorLancamentoProgramado'.
     * 
     * @param cdindicadorLancamentoProgramado the value of field
     * 'cdindicadorLancamentoProgramado'.
     */
    public void setCdindicadorLancamentoProgramado(int cdindicadorLancamentoProgramado)
    {
        this._cdindicadorLancamentoProgramado = cdindicadorLancamentoProgramado;
        this._has_cdindicadorLancamentoProgramado = true;
    } //-- void setCdindicadorLancamentoProgramado(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAcaoNaoVida'.
     * 
     * @param dsAcaoNaoVida the value of field 'dsAcaoNaoVida'.
     */
    public void setDsAcaoNaoVida(java.lang.String dsAcaoNaoVida)
    {
        this._dsAcaoNaoVida = dsAcaoNaoVida;
    } //-- void setDsAcaoNaoVida(java.lang.String) 

    /**
     * Sets the value of field 'dsAcertoDadosRecadastramento'.
     * 
     * @param dsAcertoDadosRecadastramento the value of field
     * 'dsAcertoDadosRecadastramento'.
     */
    public void setDsAcertoDadosRecadastramento(java.lang.String dsAcertoDadosRecadastramento)
    {
        this._dsAcertoDadosRecadastramento = dsAcertoDadosRecadastramento;
    } //-- void setDsAcertoDadosRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoDebitoVeiculo'.
     * 
     * @param dsAgendamentoDebitoVeiculo the value of field
     * 'dsAgendamentoDebitoVeiculo'.
     */
    public void setDsAgendamentoDebitoVeiculo(java.lang.String dsAgendamentoDebitoVeiculo)
    {
        this._dsAgendamentoDebitoVeiculo = dsAgendamentoDebitoVeiculo;
    } //-- void setDsAgendamentoDebitoVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoPagamentoVencido'.
     * 
     * @param dsAgendamentoPagamentoVencido the value of field
     * 'dsAgendamentoPagamentoVencido'.
     */
    public void setDsAgendamentoPagamentoVencido(java.lang.String dsAgendamentoPagamentoVencido)
    {
        this._dsAgendamentoPagamentoVencido = dsAgendamentoPagamentoVencido;
    } //-- void setDsAgendamentoPagamentoVencido(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoRastreabilidadeFinal'.
     * 
     * @param dsAgendamentoRastreabilidadeFinal the value of field
     * 'dsAgendamentoRastreabilidadeFinal'.
     */
    public void setDsAgendamentoRastreabilidadeFinal(java.lang.String dsAgendamentoRastreabilidadeFinal)
    {
        this._dsAgendamentoRastreabilidadeFinal = dsAgendamentoRastreabilidadeFinal;
    } //-- void setDsAgendamentoRastreabilidadeFinal(java.lang.String) 

    /**
     * Sets the value of field 'dsAgendamentoValorMenor'.
     * 
     * @param dsAgendamentoValorMenor the value of field
     * 'dsAgendamentoValorMenor'.
     */
    public void setDsAgendamentoValorMenor(java.lang.String dsAgendamentoValorMenor)
    {
        this._dsAgendamentoValorMenor = dsAgendamentoValorMenor;
    } //-- void setDsAgendamentoValorMenor(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoAviso'.
     * 
     * @param dsAgrupamentoAviso the value of field
     * 'dsAgrupamentoAviso'.
     */
    public void setDsAgrupamentoAviso(java.lang.String dsAgrupamentoAviso)
    {
        this._dsAgrupamentoAviso = dsAgrupamentoAviso;
    } //-- void setDsAgrupamentoAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoComprovante'.
     * 
     * @param dsAgrupamentoComprovante the value of field
     * 'dsAgrupamentoComprovante'.
     */
    public void setDsAgrupamentoComprovante(java.lang.String dsAgrupamentoComprovante)
    {
        this._dsAgrupamentoComprovante = dsAgrupamentoComprovante;
    } //-- void setDsAgrupamentoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoFormularioRecadastro'.
     * 
     * @param dsAgrupamentoFormularioRecadastro the value of field
     * 'dsAgrupamentoFormularioRecadastro'.
     */
    public void setDsAgrupamentoFormularioRecadastro(java.lang.String dsAgrupamentoFormularioRecadastro)
    {
        this._dsAgrupamentoFormularioRecadastro = dsAgrupamentoFormularioRecadastro;
    } //-- void setDsAgrupamentoFormularioRecadastro(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsAntecipacaoRecadastramentoBeneficiario'.
     * 
     * @param dsAntecipacaoRecadastramentoBeneficiario the value of
     * field 'dsAntecipacaoRecadastramentoBeneficiario'.
     */
    public void setDsAntecipacaoRecadastramentoBeneficiario(java.lang.String dsAntecipacaoRecadastramentoBeneficiario)
    {
        this._dsAntecipacaoRecadastramentoBeneficiario = dsAntecipacaoRecadastramentoBeneficiario;
    } //-- void setDsAntecipacaoRecadastramentoBeneficiario(java.lang.String) 

    /**
     * Sets the value of field 'dsAreaReservada'.
     * 
     * @param dsAreaReservada the value of field 'dsAreaReservada'.
     */
    public void setDsAreaReservada(java.lang.String dsAreaReservada)
    {
        this._dsAreaReservada = dsAreaReservada;
    } //-- void setDsAreaReservada(java.lang.String) 

    /**
     * Sets the value of field 'dsAreaReservada2'.
     * 
     * @param dsAreaReservada2 the value of field 'dsAreaReservada2'
     */
    public void setDsAreaReservada2(java.lang.String dsAreaReservada2)
    {
        this._dsAreaReservada2 = dsAreaReservada2;
    } //-- void setDsAreaReservada2(java.lang.String) 

    /**
     * Sets the value of field 'dsBaseRecadastramentoBeneficio'.
     * 
     * @param dsBaseRecadastramentoBeneficio the value of field
     * 'dsBaseRecadastramentoBeneficio'.
     */
    public void setDsBaseRecadastramentoBeneficio(java.lang.String dsBaseRecadastramentoBeneficio)
    {
        this._dsBaseRecadastramentoBeneficio = dsBaseRecadastramentoBeneficio;
    } //-- void setDsBaseRecadastramentoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsBloqueioEmissaoPapeleta'.
     * 
     * @param dsBloqueioEmissaoPapeleta the value of field
     * 'dsBloqueioEmissaoPapeleta'.
     */
    public void setDsBloqueioEmissaoPapeleta(java.lang.String dsBloqueioEmissaoPapeleta)
    {
        this._dsBloqueioEmissaoPapeleta = dsBloqueioEmissaoPapeleta;
    } //-- void setDsBloqueioEmissaoPapeleta(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalAlteracao'.
     * 
     * @param dsCanalAlteracao the value of field 'dsCanalAlteracao'
     */
    public void setDsCanalAlteracao(java.lang.String dsCanalAlteracao)
    {
        this._dsCanalAlteracao = dsCanalAlteracao;
    } //-- void setDsCanalAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCapturaTituloRegistrado'.
     * 
     * @param dsCapturaTituloRegistrado the value of field
     * 'dsCapturaTituloRegistrado'.
     */
    public void setDsCapturaTituloRegistrado(java.lang.String dsCapturaTituloRegistrado)
    {
        this._dsCapturaTituloRegistrado = dsCapturaTituloRegistrado;
    } //-- void setDsCapturaTituloRegistrado(java.lang.String) 

    /**
     * Sets the value of field 'dsCctciaEspeBeneficio'.
     * 
     * @param dsCctciaEspeBeneficio the value of field
     * 'dsCctciaEspeBeneficio'.
     */
    public void setDsCctciaEspeBeneficio(java.lang.String dsCctciaEspeBeneficio)
    {
        this._dsCctciaEspeBeneficio = dsCctciaEspeBeneficio;
    } //-- void setDsCctciaEspeBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCctciaIdentificacaoBeneficio'.
     * 
     * @param dsCctciaIdentificacaoBeneficio the value of field
     * 'dsCctciaIdentificacaoBeneficio'.
     */
    public void setDsCctciaIdentificacaoBeneficio(java.lang.String dsCctciaIdentificacaoBeneficio)
    {
        this._dsCctciaIdentificacaoBeneficio = dsCctciaIdentificacaoBeneficio;
    } //-- void setDsCctciaIdentificacaoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCctciaInscricaoFavorecido'.
     * 
     * @param dsCctciaInscricaoFavorecido the value of field
     * 'dsCctciaInscricaoFavorecido'.
     */
    public void setDsCctciaInscricaoFavorecido(java.lang.String dsCctciaInscricaoFavorecido)
    {
        this._dsCctciaInscricaoFavorecido = dsCctciaInscricaoFavorecido;
    } //-- void setDsCctciaInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsCctciaProprietarioVeculo'.
     * 
     * @param dsCctciaProprietarioVeculo the value of field
     * 'dsCctciaProprietarioVeculo'.
     */
    public void setDsCctciaProprietarioVeculo(java.lang.String dsCctciaProprietarioVeculo)
    {
        this._dsCctciaProprietarioVeculo = dsCctciaProprietarioVeculo;
    } //-- void setDsCctciaProprietarioVeculo(java.lang.String) 

    /**
     * Sets the value of field 'dsCobrancaTarifa'.
     * 
     * @param dsCobrancaTarifa the value of field 'dsCobrancaTarifa'
     */
    public void setDsCobrancaTarifa(java.lang.String dsCobrancaTarifa)
    {
        this._dsCobrancaTarifa = dsCobrancaTarifa;
    } //-- void setDsCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsCodIdentificadortipoRetornoInternet'.
     * 
     * @param dsCodIdentificadortipoRetornoInternet the value of
     * field 'dsCodIdentificadortipoRetornoInternet'.
     */
    public void setDsCodIdentificadorTipoRetornoInternet(java.lang.String dsCodIdentificadorTipoRetornoInternet)
    {
        this._dsCodIdentificadorTipoRetornoInternet = dsCodIdentificadorTipoRetornoInternet;
    } //-- void setDsCodIdentificadortipoRetornoInternet(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoIndFeriadoLocal'.
     * 
     * @param dsCodigoIndFeriadoLocal the value of field
     * 'dsCodigoIndFeriadoLocal'.
     */
    public void setDsCodigoIndFeriadoLocal(java.lang.String dsCodigoIndFeriadoLocal)
    {
        this._dsCodigoIndFeriadoLocal = dsCodigoIndFeriadoLocal;
    } //-- void setDsCodigoIndFeriadoLocal(java.lang.String) 

    /**
     * Sets the value of field 'dsConsistenciaCpfCnpjBenefAvalNpc'.
     * 
     * @param dsConsistenciaCpfCnpjBenefAvalNpc the value of field
     * 'dsConsistenciaCpfCnpjBenefAvalNpc'.
     */
    public void setDsConsistenciaCpfCnpjBenefAvalNpc(java.lang.String dsConsistenciaCpfCnpjBenefAvalNpc)
    {
        this._dsConsistenciaCpfCnpjBenefAvalNpc = dsConsistenciaCpfCnpjBenefAvalNpc;
    } //-- void setDsConsistenciaCpfCnpjBenefAvalNpc(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaDebitoVeiculo'.
     * 
     * @param dsConsultaDebitoVeiculo the value of field
     * 'dsConsultaDebitoVeiculo'.
     */
    public void setDsConsultaDebitoVeiculo(java.lang.String dsConsultaDebitoVeiculo)
    {
        this._dsConsultaDebitoVeiculo = dsConsultaDebitoVeiculo;
    } //-- void setDsConsultaDebitoVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaEndereco'.
     * 
     * @param dsConsultaEndereco the value of field
     * 'dsConsultaEndereco'.
     */
    public void setDsConsultaEndereco(java.lang.String dsConsultaEndereco)
    {
        this._dsConsultaEndereco = dsConsultaEndereco;
    } //-- void setDsConsultaEndereco(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaSaldoPagamento'.
     * 
     * @param dsConsultaSaldoPagamento the value of field
     * 'dsConsultaSaldoPagamento'.
     */
    public void setDsConsultaSaldoPagamento(java.lang.String dsConsultaSaldoPagamento)
    {
        this._dsConsultaSaldoPagamento = dsConsultaSaldoPagamento;
    } //-- void setDsConsultaSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsConsultaSaldoValorSuperior'.
     * 
     * @param dsConsultaSaldoValorSuperior the value of field
     * 'dsConsultaSaldoValorSuperior'.
     */
    public void setDsConsultaSaldoValorSuperior(java.lang.String dsConsultaSaldoValorSuperior)
    {
        this._dsConsultaSaldoValorSuperior = dsConsultaSaldoValorSuperior;
    } //-- void setDsConsultaSaldoValorSuperior(java.lang.String) 

    /**
     * Sets the value of field 'dsContagemConsultaSaldo'.
     * 
     * @param dsContagemConsultaSaldo the value of field
     * 'dsContagemConsultaSaldo'.
     */
    public void setDsContagemConsultaSaldo(java.lang.String dsContagemConsultaSaldo)
    {
        this._dsContagemConsultaSaldo = dsContagemConsultaSaldo;
    } //-- void setDsContagemConsultaSaldo(java.lang.String) 

    /**
     * Sets the value of field 'dsCreditoNaoUtilizado'.
     * 
     * @param dsCreditoNaoUtilizado the value of field
     * 'dsCreditoNaoUtilizado'.
     */
    public void setDsCreditoNaoUtilizado(java.lang.String dsCreditoNaoUtilizado)
    {
        this._dsCreditoNaoUtilizado = dsCreditoNaoUtilizado;
    } //-- void setDsCreditoNaoUtilizado(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioEnquadraRecadastramento'.
     * 
     * @param dsCriterioEnquadraRecadastramento the value of field
     * 'dsCriterioEnquadraRecadastramento'.
     */
    public void setDsCriterioEnquadraRecadastramento(java.lang.String dsCriterioEnquadraRecadastramento)
    {
        this._dsCriterioEnquadraRecadastramento = dsCriterioEnquadraRecadastramento;
    } //-- void setDsCriterioEnquadraRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioEnquandraBeneficio'.
     * 
     * @param dsCriterioEnquandraBeneficio the value of field
     * 'dsCriterioEnquandraBeneficio'.
     */
    public void setDsCriterioEnquandraBeneficio(java.lang.String dsCriterioEnquandraBeneficio)
    {
        this._dsCriterioEnquandraBeneficio = dsCriterioEnquandraBeneficio;
    } //-- void setDsCriterioEnquandraBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsCriterioRastreabilidadeTitulo'.
     * 
     * @param dsCriterioRastreabilidadeTitulo the value of field
     * 'dsCriterioRastreabilidadeTitulo'.
     */
    public void setDsCriterioRastreabilidadeTitulo(java.lang.String dsCriterioRastreabilidadeTitulo)
    {
        this._dsCriterioRastreabilidadeTitulo = dsCriterioRastreabilidadeTitulo;
    } //-- void setDsCriterioRastreabilidadeTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoAviso'.
     * 
     * @param dsDestinoAviso the value of field 'dsDestinoAviso'.
     */
    public void setDsDestinoAviso(java.lang.String dsDestinoAviso)
    {
        this._dsDestinoAviso = dsDestinoAviso;
    } //-- void setDsDestinoAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsDestinoComprovante'.
     * 
     * @param dsDestinoComprovante the value of field
     * 'dsDestinoComprovante'.
     */
    public void setDsDestinoComprovante(java.lang.String dsDestinoComprovante)
    {
        this._dsDestinoComprovante = dsDestinoComprovante;
    } //-- void setDsDestinoComprovante(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsDestinoFormularioRecadastramento'.
     * 
     * @param dsDestinoFormularioRecadastramento the value of field
     * 'dsDestinoFormularioRecadastramento'.
     */
    public void setDsDestinoFormularioRecadastramento(java.lang.String dsDestinoFormularioRecadastramento)
    {
        this._dsDestinoFormularioRecadastramento = dsDestinoFormularioRecadastramento;
    } //-- void setDsDestinoFormularioRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoContaCredito'.
     * 
     * @param dsDisponibilizacaoContaCredito the value of field
     * 'dsDisponibilizacaoContaCredito'.
     */
    public void setDsDisponibilizacaoContaCredito(java.lang.String dsDisponibilizacaoContaCredito)
    {
        this._dsDisponibilizacaoContaCredito = dsDisponibilizacaoContaCredito;
    } //-- void setDsDisponibilizacaoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoDiversoCriterio'.
     * 
     * @param dsDisponibilizacaoDiversoCriterio the value of field
     * 'dsDisponibilizacaoDiversoCriterio'.
     */
    public void setDsDisponibilizacaoDiversoCriterio(java.lang.String dsDisponibilizacaoDiversoCriterio)
    {
        this._dsDisponibilizacaoDiversoCriterio = dsDisponibilizacaoDiversoCriterio;
    } //-- void setDsDisponibilizacaoDiversoCriterio(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoDiversoNao'.
     * 
     * @param dsDisponibilizacaoDiversoNao the value of field
     * 'dsDisponibilizacaoDiversoNao'.
     */
    public void setDsDisponibilizacaoDiversoNao(java.lang.String dsDisponibilizacaoDiversoNao)
    {
        this._dsDisponibilizacaoDiversoNao = dsDisponibilizacaoDiversoNao;
    } //-- void setDsDisponibilizacaoDiversoNao(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoSalarioCriterio'.
     * 
     * @param dsDisponibilizacaoSalarioCriterio the value of field
     * 'dsDisponibilizacaoSalarioCriterio'.
     */
    public void setDsDisponibilizacaoSalarioCriterio(java.lang.String dsDisponibilizacaoSalarioCriterio)
    {
        this._dsDisponibilizacaoSalarioCriterio = dsDisponibilizacaoSalarioCriterio;
    } //-- void setDsDisponibilizacaoSalarioCriterio(java.lang.String) 

    /**
     * Sets the value of field 'dsDisponibilizacaoSalarioNao'.
     * 
     * @param dsDisponibilizacaoSalarioNao the value of field
     * 'dsDisponibilizacaoSalarioNao'.
     */
    public void setDsDisponibilizacaoSalarioNao(java.lang.String dsDisponibilizacaoSalarioNao)
    {
        this._dsDisponibilizacaoSalarioNao = dsDisponibilizacaoSalarioNao;
    } //-- void setDsDisponibilizacaoSalarioNao(java.lang.String) 

    /**
     * Sets the value of field 'dsEnvelopeAberto'.
     * 
     * @param dsEnvelopeAberto the value of field 'dsEnvelopeAberto'
     */
    public void setDsEnvelopeAberto(java.lang.String dsEnvelopeAberto)
    {
        this._dsEnvelopeAberto = dsEnvelopeAberto;
    } //-- void setDsEnvelopeAberto(java.lang.String) 

    /**
     * Sets the value of field 'dsExigeFilial'.
     * 
     * @param dsExigeFilial the value of field 'dsExigeFilial'.
     */
    public void setDsExigeFilial(java.lang.String dsExigeFilial)
    {
        this._dsExigeFilial = dsExigeFilial;
    } //-- void setDsExigeFilial(java.lang.String) 

    /**
     * Sets the value of field 'dsFavorecidoConsultaPagamento'.
     * 
     * @param dsFavorecidoConsultaPagamento the value of field
     * 'dsFavorecidoConsultaPagamento'.
     */
    public void setDsFavorecidoConsultaPagamento(java.lang.String dsFavorecidoConsultaPagamento)
    {
        this._dsFavorecidoConsultaPagamento = dsFavorecidoConsultaPagamento;
    } //-- void setDsFavorecidoConsultaPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFloatServicoContrato'.
     * 
     * @param dsFloatServicoContrato the value of field
     * 'dsFloatServicoContrato'.
     */
    public void setDsFloatServicoContrato(java.lang.String dsFloatServicoContrato)
    {
        this._dsFloatServicoContrato = dsFloatServicoContrato;
    } //-- void setDsFloatServicoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @param dsFormaAutorizacaoPagamento the value of field
     * 'dsFormaAutorizacaoPagamento'.
     */
    public void setDsFormaAutorizacaoPagamento(java.lang.String dsFormaAutorizacaoPagamento)
    {
        this._dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    } //-- void setDsFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaEnvioPagamento'.
     * 
     * @param dsFormaEnvioPagamento the value of field
     * 'dsFormaEnvioPagamento'.
     */
    public void setDsFormaEnvioPagamento(java.lang.String dsFormaEnvioPagamento)
    {
        this._dsFormaEnvioPagamento = dsFormaEnvioPagamento;
    } //-- void setDsFormaEnvioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaEstornoCredito'.
     * 
     * @param dsFormaEstornoCredito the value of field
     * 'dsFormaEstornoCredito'.
     */
    public void setDsFormaEstornoCredito(java.lang.String dsFormaEstornoCredito)
    {
        this._dsFormaEstornoCredito = dsFormaEstornoCredito;
    } //-- void setDsFormaEstornoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaExpiracaoCredito'.
     * 
     * @param dsFormaExpiracaoCredito the value of field
     * 'dsFormaExpiracaoCredito'.
     */
    public void setDsFormaExpiracaoCredito(java.lang.String dsFormaExpiracaoCredito)
    {
        this._dsFormaExpiracaoCredito = dsFormaExpiracaoCredito;
    } //-- void setDsFormaExpiracaoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaManutencao'.
     * 
     * @param dsFormaManutencao the value of field
     * 'dsFormaManutencao'.
     */
    public void setDsFormaManutencao(java.lang.String dsFormaManutencao)
    {
        this._dsFormaManutencao = dsFormaManutencao;
    } //-- void setDsFormaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsFrasePrecadastrada'.
     * 
     * @param dsFrasePrecadastrada the value of field
     * 'dsFrasePrecadastrada'.
     */
    public void setDsFrasePrecadastrada(java.lang.String dsFrasePrecadastrada)
    {
        this._dsFrasePrecadastrada = dsFrasePrecadastrada;
    } //-- void setDsFrasePrecadastrada(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicador'.
     * 
     * @param dsIndicador the value of field 'dsIndicador'.
     */
    public void setDsIndicador(java.lang.String dsIndicador)
    {
        this._dsIndicador = dsIndicador;
    } //-- void setDsIndicador(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAgendaGrade'.
     * 
     * @param dsIndicadorAgendaGrade the value of field
     * 'dsIndicadorAgendaGrade'.
     */
    public void setDsIndicadorAgendaGrade(java.lang.String dsIndicadorAgendaGrade)
    {
        this._dsIndicadorAgendaGrade = dsIndicadorAgendaGrade;
    } //-- void setDsIndicadorAgendaGrade(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAgendamentoTitulo'.
     * 
     * @param dsIndicadorAgendamentoTitulo the value of field
     * 'dsIndicadorAgendamentoTitulo'.
     */
    public void setDsIndicadorAgendamentoTitulo(java.lang.String dsIndicadorAgendamentoTitulo)
    {
        this._dsIndicadorAgendamentoTitulo = dsIndicadorAgendamentoTitulo;
    } //-- void setDsIndicadorAgendamentoTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAutorizacaoCliente'.
     * 
     * @param dsIndicadorAutorizacaoCliente the value of field
     * 'dsIndicadorAutorizacaoCliente'.
     */
    public void setDsIndicadorAutorizacaoCliente(java.lang.String dsIndicadorAutorizacaoCliente)
    {
        this._dsIndicadorAutorizacaoCliente = dsIndicadorAutorizacaoCliente;
    } //-- void setDsIndicadorAutorizacaoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAutorizacaoComplemento'.
     * 
     * @param dsIndicadorAutorizacaoComplemento the value of field
     * 'dsIndicadorAutorizacaoComplemento'.
     */
    public void setDsIndicadorAutorizacaoComplemento(java.lang.String dsIndicadorAutorizacaoComplemento)
    {
        this._dsIndicadorAutorizacaoComplemento = dsIndicadorAutorizacaoComplemento;
    } //-- void setDsIndicadorAutorizacaoComplemento(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorBancoPostal'.
     * 
     * @param dsIndicadorBancoPostal the value of field
     * 'dsIndicadorBancoPostal'.
     */
    public void setDsIndicadorBancoPostal(java.lang.String dsIndicadorBancoPostal)
    {
        this._dsIndicadorBancoPostal = dsIndicadorBancoPostal;
    } //-- void setDsIndicadorBancoPostal(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCadastroProcurador'.
     * 
     * @param dsIndicadorCadastroProcurador the value of field
     * 'dsIndicadorCadastroProcurador'.
     */
    public void setDsIndicadorCadastroProcurador(java.lang.String dsIndicadorCadastroProcurador)
    {
        this._dsIndicadorCadastroProcurador = dsIndicadorCadastroProcurador;
    } //-- void setDsIndicadorCadastroProcurador(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCadastroorganizacao'.
     * 
     * @param dsIndicadorCadastroorganizacao the value of field
     * 'dsIndicadorCadastroorganizacao'.
     */
    public void setDsIndicadorCadastroorganizacao(java.lang.String dsIndicadorCadastroorganizacao)
    {
        this._dsIndicadorCadastroorganizacao = dsIndicadorCadastroorganizacao;
    } //-- void setDsIndicadorCadastroorganizacao(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCartaoSalario'.
     * 
     * @param dsIndicadorCartaoSalario the value of field
     * 'dsIndicadorCartaoSalario'.
     */
    public void setDsIndicadorCartaoSalario(java.lang.String dsIndicadorCartaoSalario)
    {
        this._dsIndicadorCartaoSalario = dsIndicadorCartaoSalario;
    } //-- void setDsIndicadorCartaoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorEconomicoReajuste'.
     * 
     * @param dsIndicadorEconomicoReajuste the value of field
     * 'dsIndicadorEconomicoReajuste'.
     */
    public void setDsIndicadorEconomicoReajuste(java.lang.String dsIndicadorEconomicoReajuste)
    {
        this._dsIndicadorEconomicoReajuste = dsIndicadorEconomicoReajuste;
    } //-- void setDsIndicadorEconomicoReajuste(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorListaDebito'.
     * 
     * @param dsIndicadorListaDebito the value of field
     * 'dsIndicadorListaDebito'.
     */
    public void setDsIndicadorListaDebito(java.lang.String dsIndicadorListaDebito)
    {
        this._dsIndicadorListaDebito = dsIndicadorListaDebito;
    } //-- void setDsIndicadorListaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorMensagemPersonalizada'.
     * 
     * @param dsIndicadorMensagemPersonalizada the value of field
     * 'dsIndicadorMensagemPersonalizada'.
     */
    public void setDsIndicadorMensagemPersonalizada(java.lang.String dsIndicadorMensagemPersonalizada)
    {
        this._dsIndicadorMensagemPersonalizada = dsIndicadorMensagemPersonalizada;
    } //-- void setDsIndicadorMensagemPersonalizada(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorRetornoInternet'.
     * 
     * @param dsIndicadorRetornoInternet the value of field
     * 'dsIndicadorRetornoInternet'.
     */
    public void setDsIndicadorRetornoInternet(java.lang.String dsIndicadorRetornoInternet)
    {
        this._dsIndicadorRetornoInternet = dsIndicadorRetornoInternet;
    } //-- void setDsIndicadorRetornoInternet(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorSegundaLinha'.
     * 
     * @param dsIndicadorSegundaLinha the value of field
     * 'dsIndicadorSegundaLinha'.
     */
    public void setDsIndicadorSegundaLinha(java.lang.String dsIndicadorSegundaLinha)
    {
        this._dsIndicadorSegundaLinha = dsIndicadorSegundaLinha;
    } //-- void setDsIndicadorSegundaLinha(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorUtilizaMora'.
     * 
     * @param dsIndicadorUtilizaMora the value of field
     * 'dsIndicadorUtilizaMora'.
     */
    public void setDsIndicadorUtilizaMora(java.lang.String dsIndicadorUtilizaMora)
    {
        this._dsIndicadorUtilizaMora = dsIndicadorUtilizaMora;
    } //-- void setDsIndicadorUtilizaMora(java.lang.String) 

    /**
     * Sets the value of field 'dsLancamentoFuturoCredito'.
     * 
     * @param dsLancamentoFuturoCredito the value of field
     * 'dsLancamentoFuturoCredito'.
     */
    public void setDsLancamentoFuturoCredito(java.lang.String dsLancamentoFuturoCredito)
    {
        this._dsLancamentoFuturoCredito = dsLancamentoFuturoCredito;
    } //-- void setDsLancamentoFuturoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsLancamentoFuturoDebito'.
     * 
     * @param dsLancamentoFuturoDebito the value of field
     * 'dsLancamentoFuturoDebito'.
     */
    public void setDsLancamentoFuturoDebito(java.lang.String dsLancamentoFuturoDebito)
    {
        this._dsLancamentoFuturoDebito = dsLancamentoFuturoDebito;
    } //-- void setDsLancamentoFuturoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsLiberacaoLoteProcessado'.
     * 
     * @param dsLiberacaoLoteProcessado the value of field
     * 'dsLiberacaoLoteProcessado'.
     */
    public void setDsLiberacaoLoteProcessado(java.lang.String dsLiberacaoLoteProcessado)
    {
        this._dsLiberacaoLoteProcessado = dsLiberacaoLoteProcessado;
    } //-- void setDsLiberacaoLoteProcessado(java.lang.String) 

    /**
     * Sets the value of field 'dsManutencaoBaseRecadastramento'.
     * 
     * @param dsManutencaoBaseRecadastramento the value of field
     * 'dsManutencaoBaseRecadastramento'.
     */
    public void setDsManutencaoBaseRecadastramento(java.lang.String dsManutencaoBaseRecadastramento)
    {
        this._dsManutencaoBaseRecadastramento = dsManutencaoBaseRecadastramento;
    } //-- void setDsManutencaoBaseRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPagamentoDebito'.
     * 
     * @param dsMeioPagamentoDebito the value of field
     * 'dsMeioPagamentoDebito'.
     */
    public void setDsMeioPagamentoDebito(java.lang.String dsMeioPagamentoDebito)
    {
        this._dsMeioPagamentoDebito = dsMeioPagamentoDebito;
    } //-- void setDsMeioPagamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemRecadastramentoMidia'.
     * 
     * @param dsMensagemRecadastramentoMidia the value of field
     * 'dsMensagemRecadastramentoMidia'.
     */
    public void setDsMensagemRecadastramentoMidia(java.lang.String dsMensagemRecadastramentoMidia)
    {
        this._dsMensagemRecadastramentoMidia = dsMensagemRecadastramentoMidia;
    } //-- void setDsMensagemRecadastramentoMidia(java.lang.String) 

    /**
     * Sets the value of field 'dsMidiaDisponivel'.
     * 
     * @param dsMidiaDisponivel the value of field
     * 'dsMidiaDisponivel'.
     */
    public void setDsMidiaDisponivel(java.lang.String dsMidiaDisponivel)
    {
        this._dsMidiaDisponivel = dsMidiaDisponivel;
    } //-- void setDsMidiaDisponivel(java.lang.String) 

    /**
     * Sets the value of field 'dsMidiaMensagemRecadastramento'.
     * 
     * @param dsMidiaMensagemRecadastramento the value of field
     * 'dsMidiaMensagemRecadastramento'.
     */
    public void setDsMidiaMensagemRecadastramento(java.lang.String dsMidiaMensagemRecadastramento)
    {
        this._dsMidiaMensagemRecadastramento = dsMidiaMensagemRecadastramento;
    } //-- void setDsMidiaMensagemRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoAvisoRecadastramento'.
     * 
     * @param dsMomentoAvisoRecadastramento the value of field
     * 'dsMomentoAvisoRecadastramento'.
     */
    public void setDsMomentoAvisoRecadastramento(java.lang.String dsMomentoAvisoRecadastramento)
    {
        this._dsMomentoAvisoRecadastramento = dsMomentoAvisoRecadastramento;
    } //-- void setDsMomentoAvisoRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoCreditoEfetivacao'.
     * 
     * @param dsMomentoCreditoEfetivacao the value of field
     * 'dsMomentoCreditoEfetivacao'.
     */
    public void setDsMomentoCreditoEfetivacao(java.lang.String dsMomentoCreditoEfetivacao)
    {
        this._dsMomentoCreditoEfetivacao = dsMomentoCreditoEfetivacao;
    } //-- void setDsMomentoCreditoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoDebitoPagamento'.
     * 
     * @param dsMomentoDebitoPagamento the value of field
     * 'dsMomentoDebitoPagamento'.
     */
    public void setDsMomentoDebitoPagamento(java.lang.String dsMomentoDebitoPagamento)
    {
        this._dsMomentoDebitoPagamento = dsMomentoDebitoPagamento;
    } //-- void setDsMomentoDebitoPagamento(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsMomentoFormularioRecadastramento'.
     * 
     * @param dsMomentoFormularioRecadastramento the value of field
     * 'dsMomentoFormularioRecadastramento'.
     */
    public void setDsMomentoFormularioRecadastramento(java.lang.String dsMomentoFormularioRecadastramento)
    {
        this._dsMomentoFormularioRecadastramento = dsMomentoFormularioRecadastramento;
    } //-- void setDsMomentoFormularioRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsMomentoProcessamentoPagamento'.
     * 
     * @param dsMomentoProcessamentoPagamento the value of field
     * 'dsMomentoProcessamentoPagamento'.
     */
    public void setDsMomentoProcessamentoPagamento(java.lang.String dsMomentoProcessamentoPagamento)
    {
        this._dsMomentoProcessamentoPagamento = dsMomentoProcessamentoPagamento;
    } //-- void setDsMomentoProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsNaturezaOperacaoPagamento'.
     * 
     * @param dsNaturezaOperacaoPagamento the value of field
     * 'dsNaturezaOperacaoPagamento'.
     */
    public void setDsNaturezaOperacaoPagamento(java.lang.String dsNaturezaOperacaoPagamento)
    {
        this._dsNaturezaOperacaoPagamento = dsNaturezaOperacaoPagamento;
    } //-- void setDsNaturezaOperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsOrigemIndicador'.
     * 
     * @param dsOrigemIndicador the value of field
     * 'dsOrigemIndicador'.
     */
    public void setDsOrigemIndicador(int dsOrigemIndicador)
    {
        this._dsOrigemIndicador = dsOrigemIndicador;
        this._has_dsOrigemIndicador = true;
    } //-- void setDsOrigemIndicador(int) 

    /**
     * Sets the value of field 'dsOutraidentificacaoFavorecido'.
     * 
     * @param dsOutraidentificacaoFavorecido the value of field
     * 'dsOutraidentificacaoFavorecido'.
     */
    public void setDsOutraidentificacaoFavorecido(java.lang.String dsOutraidentificacaoFavorecido)
    {
        this._dsOutraidentificacaoFavorecido = dsOutraidentificacaoFavorecido;
    } //-- void setDsOutraidentificacaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsPagamentoNaoUtil'.
     * 
     * @param dsPagamentoNaoUtil the value of field
     * 'dsPagamentoNaoUtil'.
     */
    public void setDsPagamentoNaoUtil(java.lang.String dsPagamentoNaoUtil)
    {
        this._dsPagamentoNaoUtil = dsPagamentoNaoUtil;
    } //-- void setDsPagamentoNaoUtil(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeAviso'.
     * 
     * @param dsPeriodicidadeAviso the value of field
     * 'dsPeriodicidadeAviso'.
     */
    public void setDsPeriodicidadeAviso(java.lang.String dsPeriodicidadeAviso)
    {
        this._dsPeriodicidadeAviso = dsPeriodicidadeAviso;
    } //-- void setDsPeriodicidadeAviso(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeCobrancaTarifa'.
     * 
     * @param dsPeriodicidadeCobrancaTarifa the value of field
     * 'dsPeriodicidadeCobrancaTarifa'.
     */
    public void setDsPeriodicidadeCobrancaTarifa(java.lang.String dsPeriodicidadeCobrancaTarifa)
    {
        this._dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
    } //-- void setDsPeriodicidadeCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeComprovante'.
     * 
     * @param dsPeriodicidadeComprovante the value of field
     * 'dsPeriodicidadeComprovante'.
     */
    public void setDsPeriodicidadeComprovante(java.lang.String dsPeriodicidadeComprovante)
    {
        this._dsPeriodicidadeComprovante = dsPeriodicidadeComprovante;
    } //-- void setDsPeriodicidadeComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeConsultaVeiculo'.
     * 
     * @param dsPeriodicidadeConsultaVeiculo the value of field
     * 'dsPeriodicidadeConsultaVeiculo'.
     */
    public void setDsPeriodicidadeConsultaVeiculo(java.lang.String dsPeriodicidadeConsultaVeiculo)
    {
        this._dsPeriodicidadeConsultaVeiculo = dsPeriodicidadeConsultaVeiculo;
    } //-- void setDsPeriodicidadeConsultaVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeEnvioRemessa'.
     * 
     * @param dsPeriodicidadeEnvioRemessa the value of field
     * 'dsPeriodicidadeEnvioRemessa'.
     */
    public void setDsPeriodicidadeEnvioRemessa(java.lang.String dsPeriodicidadeEnvioRemessa)
    {
        this._dsPeriodicidadeEnvioRemessa = dsPeriodicidadeEnvioRemessa;
    } //-- void setDsPeriodicidadeEnvioRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeManutencaoProcd'.
     * 
     * @param dsPeriodicidadeManutencaoProcd the value of field
     * 'dsPeriodicidadeManutencaoProcd'.
     */
    public void setDsPeriodicidadeManutencaoProcd(java.lang.String dsPeriodicidadeManutencaoProcd)
    {
        this._dsPeriodicidadeManutencaoProcd = dsPeriodicidadeManutencaoProcd;
    } //-- void setDsPeriodicidadeManutencaoProcd(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeReajusteTarifa'.
     * 
     * @param dsPeriodicidadeReajusteTarifa the value of field
     * 'dsPeriodicidadeReajusteTarifa'.
     */
    public void setDsPeriodicidadeReajusteTarifa(java.lang.String dsPeriodicidadeReajusteTarifa)
    {
        this._dsPeriodicidadeReajusteTarifa = dsPeriodicidadeReajusteTarifa;
    } //-- void setDsPeriodicidadeReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsPermissaoDebitoOnline'.
     * 
     * @param dsPermissaoDebitoOnline the value of field
     * 'dsPermissaoDebitoOnline'.
     */
    public void setDsPermissaoDebitoOnline(java.lang.String dsPermissaoDebitoOnline)
    {
        this._dsPermissaoDebitoOnline = dsPermissaoDebitoOnline;
    } //-- void setDsPermissaoDebitoOnline(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsPreenchimentoLancamentoPersonalizado'.
     * 
     * @param dsPreenchimentoLancamentoPersonalizado the value of
     * field 'dsPreenchimentoLancamentoPersonalizado'.
     */
    public void setDsPreenchimentoLancamentoPersonalizado(java.lang.String dsPreenchimentoLancamentoPersonalizado)
    {
        this._dsPreenchimentoLancamentoPersonalizado = dsPreenchimentoLancamentoPersonalizado;
    } //-- void setDsPreenchimentoLancamentoPersonalizado(java.lang.String) 

    /**
     * Sets the value of field 'dsPrincipalEnquaRecadastramento'.
     * 
     * @param dsPrincipalEnquaRecadastramento the value of field
     * 'dsPrincipalEnquaRecadastramento'.
     */
    public void setDsPrincipalEnquaRecadastramento(java.lang.String dsPrincipalEnquaRecadastramento)
    {
        this._dsPrincipalEnquaRecadastramento = dsPrincipalEnquaRecadastramento;
    } //-- void setDsPrincipalEnquaRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsPrioridadeEfetivacaoPagamento'.
     * 
     * @param dsPrioridadeEfetivacaoPagamento the value of field
     * 'dsPrioridadeEfetivacaoPagamento'.
     */
    public void setDsPrioridadeEfetivacaoPagamento(java.lang.String dsPrioridadeEfetivacaoPagamento)
    {
        this._dsPrioridadeEfetivacaoPagamento = dsPrioridadeEfetivacaoPagamento;
    } //-- void setDsPrioridadeEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsRastreabilidadeNotaFiscal'.
     * 
     * @param dsRastreabilidadeNotaFiscal the value of field
     * 'dsRastreabilidadeNotaFiscal'.
     */
    public void setDsRastreabilidadeNotaFiscal(java.lang.String dsRastreabilidadeNotaFiscal)
    {
        this._dsRastreabilidadeNotaFiscal = dsRastreabilidadeNotaFiscal;
    } //-- void setDsRastreabilidadeNotaFiscal(java.lang.String) 

    /**
     * Sets the value of field 'dsRastreabilidadeTituloTerceiro'.
     * 
     * @param dsRastreabilidadeTituloTerceiro the value of field
     * 'dsRastreabilidadeTituloTerceiro'.
     */
    public void setDsRastreabilidadeTituloTerceiro(java.lang.String dsRastreabilidadeTituloTerceiro)
    {
        this._dsRastreabilidadeTituloTerceiro = dsRastreabilidadeTituloTerceiro;
    } //-- void setDsRastreabilidadeTituloTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoAgendamentoLote'.
     * 
     * @param dsRejeicaoAgendamentoLote the value of field
     * 'dsRejeicaoAgendamentoLote'.
     */
    public void setDsRejeicaoAgendamentoLote(java.lang.String dsRejeicaoAgendamentoLote)
    {
        this._dsRejeicaoAgendamentoLote = dsRejeicaoAgendamentoLote;
    } //-- void setDsRejeicaoAgendamentoLote(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoEfetivacaoLote'.
     * 
     * @param dsRejeicaoEfetivacaoLote the value of field
     * 'dsRejeicaoEfetivacaoLote'.
     */
    public void setDsRejeicaoEfetivacaoLote(java.lang.String dsRejeicaoEfetivacaoLote)
    {
        this._dsRejeicaoEfetivacaoLote = dsRejeicaoEfetivacaoLote;
    } //-- void setDsRejeicaoEfetivacaoLote(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoLote'.
     * 
     * @param dsRejeicaoLote the value of field 'dsRejeicaoLote'.
     */
    public void setDsRejeicaoLote(java.lang.String dsRejeicaoLote)
    {
        this._dsRejeicaoLote = dsRejeicaoLote;
    } //-- void setDsRejeicaoLote(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoServicoRelacionado'.
     * 
     * @param dsSituacaoServicoRelacionado the value of field
     * 'dsSituacaoServicoRelacionado'.
     */
    public void setDsSituacaoServicoRelacionado(java.lang.String dsSituacaoServicoRelacionado)
    {
        this._dsSituacaoServicoRelacionado = dsSituacaoServicoRelacionado;
    } //-- void setDsSituacaoServicoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCargaRecadastramento'.
     * 
     * @param dsTipoCargaRecadastramento the value of field
     * 'dsTipoCargaRecadastramento'.
     */
    public void setDsTipoCargaRecadastramento(java.lang.String dsTipoCargaRecadastramento)
    {
        this._dsTipoCargaRecadastramento = dsTipoCargaRecadastramento;
    } //-- void setDsTipoCargaRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCartaoSalario'.
     * 
     * @param dsTipoCartaoSalario the value of field
     * 'dsTipoCartaoSalario'.
     */
    public void setDsTipoCartaoSalario(java.lang.String dsTipoCartaoSalario)
    {
        this._dsTipoCartaoSalario = dsTipoCartaoSalario;
    } //-- void setDsTipoCartaoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsistenciaLista'.
     * 
     * @param dsTipoConsistenciaLista the value of field
     * 'dsTipoConsistenciaLista'.
     */
    public void setDsTipoConsistenciaLista(java.lang.String dsTipoConsistenciaLista)
    {
        this._dsTipoConsistenciaLista = dsTipoConsistenciaLista;
    } //-- void setDsTipoConsistenciaLista(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConsolidacaoComprovante'.
     * 
     * @param dsTipoConsolidacaoComprovante the value of field
     * 'dsTipoConsolidacaoComprovante'.
     */
    public void setDsTipoConsolidacaoComprovante(java.lang.String dsTipoConsolidacaoComprovante)
    {
        this._dsTipoConsolidacaoComprovante = dsTipoConsolidacaoComprovante;
    } //-- void setDsTipoConsolidacaoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDataFloating'.
     * 
     * @param dsTipoDataFloating the value of field
     * 'dsTipoDataFloating'.
     */
    public void setDsTipoDataFloating(java.lang.String dsTipoDataFloating)
    {
        this._dsTipoDataFloating = dsTipoDataFloating;
    } //-- void setDsTipoDataFloating(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoDivergenciaVeiculo'.
     * 
     * @param dsTipoDivergenciaVeiculo the value of field
     * 'dsTipoDivergenciaVeiculo'.
     */
    public void setDsTipoDivergenciaVeiculo(java.lang.String dsTipoDivergenciaVeiculo)
    {
        this._dsTipoDivergenciaVeiculo = dsTipoDivergenciaVeiculo;
    } //-- void setDsTipoDivergenciaVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoEfetivacaoPagamento'.
     * 
     * @param dsTipoEfetivacaoPagamento the value of field
     * 'dsTipoEfetivacaoPagamento'.
     */
    public void setDsTipoEfetivacaoPagamento(java.lang.String dsTipoEfetivacaoPagamento)
    {
        this._dsTipoEfetivacaoPagamento = dsTipoEfetivacaoPagamento;
    } //-- void setDsTipoEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoFormacaoLista'.
     * 
     * @param dsTipoFormacaoLista the value of field
     * 'dsTipoFormacaoLista'.
     */
    public void setDsTipoFormacaoLista(java.lang.String dsTipoFormacaoLista)
    {
        this._dsTipoFormacaoLista = dsTipoFormacaoLista;
    } //-- void setDsTipoFormacaoLista(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoIdentificacaoBeneficio'.
     * 
     * @param dsTipoIdentificacaoBeneficio the value of field
     * 'dsTipoIdentificacaoBeneficio'.
     */
    public void setDsTipoIdentificacaoBeneficio(java.lang.String dsTipoIdentificacaoBeneficio)
    {
        this._dsTipoIdentificacaoBeneficio = dsTipoIdentificacaoBeneficio;
    } //-- void setDsTipoIdentificacaoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoReajusteTarifa'.
     * 
     * @param dsTipoReajusteTarifa the value of field
     * 'dsTipoReajusteTarifa'.
     */
    public void setDsTipoReajusteTarifa(java.lang.String dsTipoReajusteTarifa)
    {
        this._dsTipoReajusteTarifa = dsTipoReajusteTarifa;
    } //-- void setDsTipoReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field 'dsTituloDdaRetorno'.
     * 
     * @param dsTituloDdaRetorno the value of field
     * 'dsTituloDdaRetorno'.
     */
    public void setDsTituloDdaRetorno(java.lang.String dsTituloDdaRetorno)
    {
        this._dsTituloDdaRetorno = dsTituloDdaRetorno;
    } //-- void setDsTituloDdaRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTratamentoContaTransferida'.
     * 
     * @param dsTratamentoContaTransferida the value of field
     * 'dsTratamentoContaTransferida'.
     */
    public void setDsTratamentoContaTransferida(java.lang.String dsTratamentoContaTransferida)
    {
        this._dsTratamentoContaTransferida = dsTratamentoContaTransferida;
    } //-- void setDsTratamentoContaTransferida(java.lang.String) 

    /**
     * Sets the value of field 'dsUtilizacaoFavorecidoControle'.
     * 
     * @param dsUtilizacaoFavorecidoControle the value of field
     * 'dsUtilizacaoFavorecidoControle'.
     */
    public void setDsUtilizacaoFavorecidoControle(java.lang.String dsUtilizacaoFavorecidoControle)
    {
        this._dsUtilizacaoFavorecidoControle = dsUtilizacaoFavorecidoControle;
    } //-- void setDsUtilizacaoFavorecidoControle(java.lang.String) 

    /**
     * Sets the value of field 'dsindicadorExpiraCredito'.
     * 
     * @param dsindicadorExpiraCredito the value of field
     * 'dsindicadorExpiraCredito'.
     */
    public void setDsindicadorExpiraCredito(java.lang.String dsindicadorExpiraCredito)
    {
        this._dsindicadorExpiraCredito = dsindicadorExpiraCredito;
    } //-- void setDsindicadorExpiraCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsindicadorLancamentoProgramado'.
     * 
     * @param dsindicadorLancamentoProgramado the value of field
     * 'dsindicadorLancamentoProgramado'.
     */
    public void setDsindicadorLancamentoProgramado(java.lang.String dsindicadorLancamentoProgramado)
    {
        this._dsindicadorLancamentoProgramado = dsindicadorLancamentoProgramado;
    } //-- void setDsindicadorLancamentoProgramado(java.lang.String) 

    /**
     * Sets the value of field 'dtEnquaContaSalario'.
     * 
     * @param dtEnquaContaSalario the value of field
     * 'dtEnquaContaSalario'.
     */
    public void setDtEnquaContaSalario(java.lang.String dtEnquaContaSalario)
    {
        this._dtEnquaContaSalario = dtEnquaContaSalario;
    } //-- void setDtEnquaContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'dtFimAcertoRecadastramento'.
     * 
     * @param dtFimAcertoRecadastramento the value of field
     * 'dtFimAcertoRecadastramento'.
     */
    public void setDtFimAcertoRecadastramento(java.lang.String dtFimAcertoRecadastramento)
    {
        this._dtFimAcertoRecadastramento = dtFimAcertoRecadastramento;
    } //-- void setDtFimAcertoRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dtFimRecadastramentoBeneficio'.
     * 
     * @param dtFimRecadastramentoBeneficio the value of field
     * 'dtFimRecadastramentoBeneficio'.
     */
    public void setDtFimRecadastramentoBeneficio(java.lang.String dtFimRecadastramentoBeneficio)
    {
        this._dtFimRecadastramentoBeneficio = dtFimRecadastramentoBeneficio;
    } //-- void setDtFimRecadastramentoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioAcertoRecadastramento'.
     * 
     * @param dtInicioAcertoRecadastramento the value of field
     * 'dtInicioAcertoRecadastramento'.
     */
    public void setDtInicioAcertoRecadastramento(java.lang.String dtInicioAcertoRecadastramento)
    {
        this._dtInicioAcertoRecadastramento = dtInicioAcertoRecadastramento;
    } //-- void setDtInicioAcertoRecadastramento(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioBloqueioPapeleta'.
     * 
     * @param dtInicioBloqueioPapeleta the value of field
     * 'dtInicioBloqueioPapeleta'.
     */
    public void setDtInicioBloqueioPapeleta(java.lang.String dtInicioBloqueioPapeleta)
    {
        this._dtInicioBloqueioPapeleta = dtInicioBloqueioPapeleta;
    } //-- void setDtInicioBloqueioPapeleta(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreabilidadeTitulo'.
     * 
     * @param dtInicioRastreabilidadeTitulo the value of field
     * 'dtInicioRastreabilidadeTitulo'.
     */
    public void setDtInicioRastreabilidadeTitulo(java.lang.String dtInicioRastreabilidadeTitulo)
    {
        this._dtInicioRastreabilidadeTitulo = dtInicioRastreabilidadeTitulo;
    } //-- void setDtInicioRastreabilidadeTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRecadastramentoBeneficio'.
     * 
     * @param dtInicioRecadastramentoBeneficio the value of field
     * 'dtInicioRecadastramentoBeneficio'.
     */
    public void setDtInicioRecadastramentoBeneficio(java.lang.String dtInicioRecadastramentoBeneficio)
    {
        this._dtInicioRecadastramentoBeneficio = dtInicioRecadastramentoBeneficio;
    } //-- void setDtInicioRecadastramentoBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtLimiteVinculoCarga'.
     * 
     * @param dtLimiteVinculoCarga the value of field
     * 'dtLimiteVinculoCarga'.
     */
    public void setDtLimiteVinculoCarga(java.lang.String dtLimiteVinculoCarga)
    {
        this._dtLimiteVinculoCarga = dtLimiteVinculoCarga;
    } //-- void setDtLimiteVinculoCarga(java.lang.String) 

    /**
     * Sets the value of field 'dtRegistroTitulo'.
     * 
     * @param dtRegistroTitulo the value of field 'dtRegistroTitulo'
     */
    public void setDtRegistroTitulo(java.lang.String dtRegistroTitulo)
    {
        this._dtRegistroTitulo = dtRegistroTitulo;
    } //-- void setDtRegistroTitulo(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroAlteracao'.
     * 
     * @param hrManutencaoRegistroAlteracao the value of field
     * 'hrManutencaoRegistroAlteracao'.
     */
    public void setHrManutencaoRegistroAlteracao(java.lang.String hrManutencaoRegistroAlteracao)
    {
        this._hrManutencaoRegistroAlteracao = hrManutencaoRegistroAlteracao;
    } //-- void setHrManutencaoRegistroAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistroInclusao'.
     * 
     * @param hrManutencaoRegistroInclusao the value of field
     * 'hrManutencaoRegistroInclusao'.
     */
    public void setHrManutencaoRegistroInclusao(java.lang.String hrManutencaoRegistroInclusao)
    {
        this._hrManutencaoRegistroInclusao = hrManutencaoRegistroInclusao;
    } //-- void setHrManutencaoRegistroInclusao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrFechamentoApuracaoTarifa'.
     * 
     * @param nrFechamentoApuracaoTarifa the value of field
     * 'nrFechamentoApuracaoTarifa'.
     */
    public void setNrFechamentoApuracaoTarifa(int nrFechamentoApuracaoTarifa)
    {
        this._nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
        this._has_nrFechamentoApuracaoTarifa = true;
    } //-- void setNrFechamentoApuracaoTarifa(int) 

    /**
     * Sets the value of field 'nrOperacaoFluxoAlteracao'.
     * 
     * @param nrOperacaoFluxoAlteracao the value of field
     * 'nrOperacaoFluxoAlteracao'.
     */
    public void setNrOperacaoFluxoAlteracao(java.lang.String nrOperacaoFluxoAlteracao)
    {
        this._nrOperacaoFluxoAlteracao = nrOperacaoFluxoAlteracao;
    } //-- void setNrOperacaoFluxoAlteracao(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @param nrOperacaoFluxoInclusao the value of field
     * 'nrOperacaoFluxoInclusao'.
     */
    public void setNrOperacaoFluxoInclusao(java.lang.String nrOperacaoFluxoInclusao)
    {
        this._nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    } //-- void setNrOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'percentualIndiceReajusteTarifa'.
     * 
     * @param percentualIndiceReajusteTarifa the value of field
     * 'percentualIndiceReajusteTarifa'.
     */
    public void setPercentualIndiceReajusteTarifa(java.math.BigDecimal percentualIndiceReajusteTarifa)
    {
        this._percentualIndiceReajusteTarifa = percentualIndiceReajusteTarifa;
    } //-- void setPercentualIndiceReajusteTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'percentualMaximoInconsistenteLote'.
     * 
     * @param percentualMaximoInconsistenteLote the value of field
     * 'percentualMaximoInconsistenteLote'.
     */
    public void setPercentualMaximoInconsistenteLote(int percentualMaximoInconsistenteLote)
    {
        this._percentualMaximoInconsistenteLote = percentualMaximoInconsistenteLote;
        this._has_percentualMaximoInconsistenteLote = true;
    } //-- void setPercentualMaximoInconsistenteLote(int) 

    /**
     * Sets the value of field 'periodicidadeTarifaCatalogo'.
     * 
     * @param periodicidadeTarifaCatalogo the value of field
     * 'periodicidadeTarifaCatalogo'.
     */
    public void setPeriodicidadeTarifaCatalogo(java.math.BigDecimal periodicidadeTarifaCatalogo)
    {
        this._periodicidadeTarifaCatalogo = periodicidadeTarifaCatalogo;
    } //-- void setPeriodicidadeTarifaCatalogo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'qtDiaUtilPgto'.
     * 
     * @param qtDiaUtilPgto the value of field 'qtDiaUtilPgto'.
     */
    public void setQtDiaUtilPgto(int qtDiaUtilPgto)
    {
        this._qtDiaUtilPgto = qtDiaUtilPgto;
        this._has_qtDiaUtilPgto = true;
    } //-- void setQtDiaUtilPgto(int) 

    /**
     * Sets the value of field 'quantidadeAntecedencia'.
     * 
     * @param quantidadeAntecedencia the value of field
     * 'quantidadeAntecedencia'.
     */
    public void setQuantidadeAntecedencia(int quantidadeAntecedencia)
    {
        this._quantidadeAntecedencia = quantidadeAntecedencia;
        this._has_quantidadeAntecedencia = true;
    } //-- void setQuantidadeAntecedencia(int) 

    /**
     * Sets the value of field
     * 'quantidadeAnteriorVencimentoComprovante'.
     * 
     * @param quantidadeAnteriorVencimentoComprovante the value of
     * field 'quantidadeAnteriorVencimentoComprovante'.
     */
    public void setQuantidadeAnteriorVencimentoComprovante(int quantidadeAnteriorVencimentoComprovante)
    {
        this._quantidadeAnteriorVencimentoComprovante = quantidadeAnteriorVencimentoComprovante;
        this._has_quantidadeAnteriorVencimentoComprovante = true;
    } //-- void setQuantidadeAnteriorVencimentoComprovante(int) 

    /**
     * Sets the value of field 'quantidadeDiaCobrancaTarifa'.
     * 
     * @param quantidadeDiaCobrancaTarifa the value of field
     * 'quantidadeDiaCobrancaTarifa'.
     */
    public void setQuantidadeDiaCobrancaTarifa(int quantidadeDiaCobrancaTarifa)
    {
        this._quantidadeDiaCobrancaTarifa = quantidadeDiaCobrancaTarifa;
        this._has_quantidadeDiaCobrancaTarifa = true;
    } //-- void setQuantidadeDiaCobrancaTarifa(int) 

    /**
     * Sets the value of field 'quantidadeDiaExpiracao'.
     * 
     * @param quantidadeDiaExpiracao the value of field
     * 'quantidadeDiaExpiracao'.
     */
    public void setQuantidadeDiaExpiracao(int quantidadeDiaExpiracao)
    {
        this._quantidadeDiaExpiracao = quantidadeDiaExpiracao;
        this._has_quantidadeDiaExpiracao = true;
    } //-- void setQuantidadeDiaExpiracao(int) 

    /**
     * Sets the value of field 'quantidadeDiaFloatingPagamento'.
     * 
     * @param quantidadeDiaFloatingPagamento the value of field
     * 'quantidadeDiaFloatingPagamento'.
     */
    public void setQuantidadeDiaFloatingPagamento(int quantidadeDiaFloatingPagamento)
    {
        this._quantidadeDiaFloatingPagamento = quantidadeDiaFloatingPagamento;
        this._has_quantidadeDiaFloatingPagamento = true;
    } //-- void setQuantidadeDiaFloatingPagamento(int) 

    /**
     * Sets the value of field
     * 'quantidadeDiaInatividadeFavorecido'.
     * 
     * @param quantidadeDiaInatividadeFavorecido the value of field
     * 'quantidadeDiaInatividadeFavorecido'.
     */
    public void setQuantidadeDiaInatividadeFavorecido(int quantidadeDiaInatividadeFavorecido)
    {
        this._quantidadeDiaInatividadeFavorecido = quantidadeDiaInatividadeFavorecido;
        this._has_quantidadeDiaInatividadeFavorecido = true;
    } //-- void setQuantidadeDiaInatividadeFavorecido(int) 

    /**
     * Sets the value of field 'quantidadeDiaRepiqueConsulta'.
     * 
     * @param quantidadeDiaRepiqueConsulta the value of field
     * 'quantidadeDiaRepiqueConsulta'.
     */
    public void setQuantidadeDiaRepiqueConsulta(int quantidadeDiaRepiqueConsulta)
    {
        this._quantidadeDiaRepiqueConsulta = quantidadeDiaRepiqueConsulta;
        this._has_quantidadeDiaRepiqueConsulta = true;
    } //-- void setQuantidadeDiaRepiqueConsulta(int) 

    /**
     * Sets the value of field
     * 'quantidadeEtapaRecadastramentoBeneficio'.
     * 
     * @param quantidadeEtapaRecadastramentoBeneficio the value of
     * field 'quantidadeEtapaRecadastramentoBeneficio'.
     */
    public void setQuantidadeEtapaRecadastramentoBeneficio(int quantidadeEtapaRecadastramentoBeneficio)
    {
        this._quantidadeEtapaRecadastramentoBeneficio = quantidadeEtapaRecadastramentoBeneficio;
        this._has_quantidadeEtapaRecadastramentoBeneficio = true;
    } //-- void setQuantidadeEtapaRecadastramentoBeneficio(int) 

    /**
     * Sets the value of field
     * 'quantidadeFaseRecadastramentoBeneficio'.
     * 
     * @param quantidadeFaseRecadastramentoBeneficio the value of
     * field 'quantidadeFaseRecadastramentoBeneficio'.
     */
    public void setQuantidadeFaseRecadastramentoBeneficio(int quantidadeFaseRecadastramentoBeneficio)
    {
        this._quantidadeFaseRecadastramentoBeneficio = quantidadeFaseRecadastramentoBeneficio;
        this._has_quantidadeFaseRecadastramentoBeneficio = true;
    } //-- void setQuantidadeFaseRecadastramentoBeneficio(int) 

    /**
     * Sets the value of field 'quantidadeLimiteLinha'.
     * 
     * @param quantidadeLimiteLinha the value of field
     * 'quantidadeLimiteLinha'.
     */
    public void setQuantidadeLimiteLinha(int quantidadeLimiteLinha)
    {
        this._quantidadeLimiteLinha = quantidadeLimiteLinha;
        this._has_quantidadeLimiteLinha = true;
    } //-- void setQuantidadeLimiteLinha(int) 

    /**
     * Sets the value of field 'quantidadeMaximaInconsistenteLote'.
     * 
     * @param quantidadeMaximaInconsistenteLote the value of field
     * 'quantidadeMaximaInconsistenteLote'.
     */
    public void setQuantidadeMaximaInconsistenteLote(int quantidadeMaximaInconsistenteLote)
    {
        this._quantidadeMaximaInconsistenteLote = quantidadeMaximaInconsistenteLote;
        this._has_quantidadeMaximaInconsistenteLote = true;
    } //-- void setQuantidadeMaximaInconsistenteLote(int) 

    /**
     * Sets the value of field 'quantidadeMaximaTituloVencido'.
     * 
     * @param quantidadeMaximaTituloVencido the value of field
     * 'quantidadeMaximaTituloVencido'.
     */
    public void setQuantidadeMaximaTituloVencido(int quantidadeMaximaTituloVencido)
    {
        this._quantidadeMaximaTituloVencido = quantidadeMaximaTituloVencido;
        this._has_quantidadeMaximaTituloVencido = true;
    } //-- void setQuantidadeMaximaTituloVencido(int) 

    /**
     * Sets the value of field 'quantidadeMesComprovante'.
     * 
     * @param quantidadeMesComprovante the value of field
     * 'quantidadeMesComprovante'.
     */
    public void setQuantidadeMesComprovante(int quantidadeMesComprovante)
    {
        this._quantidadeMesComprovante = quantidadeMesComprovante;
        this._has_quantidadeMesComprovante = true;
    } //-- void setQuantidadeMesComprovante(int) 

    /**
     * Sets the value of field 'quantidadeMesEtapaRecadastramento'.
     * 
     * @param quantidadeMesEtapaRecadastramento the value of field
     * 'quantidadeMesEtapaRecadastramento'.
     */
    public void setQuantidadeMesEtapaRecadastramento(int quantidadeMesEtapaRecadastramento)
    {
        this._quantidadeMesEtapaRecadastramento = quantidadeMesEtapaRecadastramento;
        this._has_quantidadeMesEtapaRecadastramento = true;
    } //-- void setQuantidadeMesEtapaRecadastramento(int) 

    /**
     * Sets the value of field 'quantidadeMesFaseRecadastramento'.
     * 
     * @param quantidadeMesFaseRecadastramento the value of field
     * 'quantidadeMesFaseRecadastramento'.
     */
    public void setQuantidadeMesFaseRecadastramento(int quantidadeMesFaseRecadastramento)
    {
        this._quantidadeMesFaseRecadastramento = quantidadeMesFaseRecadastramento;
        this._has_quantidadeMesFaseRecadastramento = true;
    } //-- void setQuantidadeMesFaseRecadastramento(int) 

    /**
     * Sets the value of field 'quantidadeMesReajusteTarifa'.
     * 
     * @param quantidadeMesReajusteTarifa the value of field
     * 'quantidadeMesReajusteTarifa'.
     */
    public void setQuantidadeMesReajusteTarifa(int quantidadeMesReajusteTarifa)
    {
        this._quantidadeMesReajusteTarifa = quantidadeMesReajusteTarifa;
        this._has_quantidadeMesReajusteTarifa = true;
    } //-- void setQuantidadeMesReajusteTarifa(int) 

    /**
     * Sets the value of field 'quantidadeSolicitacaoCartao'.
     * 
     * @param quantidadeSolicitacaoCartao the value of field
     * 'quantidadeSolicitacaoCartao'.
     */
    public void setQuantidadeSolicitacaoCartao(int quantidadeSolicitacaoCartao)
    {
        this._quantidadeSolicitacaoCartao = quantidadeSolicitacaoCartao;
        this._has_quantidadeSolicitacaoCartao = true;
    } //-- void setQuantidadeSolicitacaoCartao(int) 

    /**
     * Sets the value of field 'quantidadeViaAviso'.
     * 
     * @param quantidadeViaAviso the value of field
     * 'quantidadeViaAviso'.
     */
    public void setQuantidadeViaAviso(int quantidadeViaAviso)
    {
        this._quantidadeViaAviso = quantidadeViaAviso;
        this._has_quantidadeViaAviso = true;
    } //-- void setQuantidadeViaAviso(int) 

    /**
     * Sets the value of field 'quantidadeViaCobranca'.
     * 
     * @param quantidadeViaCobranca the value of field
     * 'quantidadeViaCobranca'.
     */
    public void setQuantidadeViaCobranca(int quantidadeViaCobranca)
    {
        this._quantidadeViaCobranca = quantidadeViaCobranca;
        this._has_quantidadeViaCobranca = true;
    } //-- void setQuantidadeViaCobranca(int) 

    /**
     * Sets the value of field 'quantidadeViaComprovante'.
     * 
     * @param quantidadeViaComprovante the value of field
     * 'quantidadeViaComprovante'.
     */
    public void setQuantidadeViaComprovante(int quantidadeViaComprovante)
    {
        this._quantidadeViaComprovante = quantidadeViaComprovante;
        this._has_quantidadeViaComprovante = true;
    } //-- void setQuantidadeViaComprovante(int) 

    /**
     * Sets the value of field 'valorFavorecidoNaoCadastrado'.
     * 
     * @param valorFavorecidoNaoCadastrado the value of field
     * 'valorFavorecidoNaoCadastrado'.
     */
    public void setValorFavorecidoNaoCadastrado(java.math.BigDecimal valorFavorecidoNaoCadastrado)
    {
        this._valorFavorecidoNaoCadastrado = valorFavorecidoNaoCadastrado;
    } //-- void setValorFavorecidoNaoCadastrado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'valorLimiteDiarioPagamento'.
     * 
     * @param valorLimiteDiarioPagamento the value of field
     * 'valorLimiteDiarioPagamento'.
     */
    public void setValorLimiteDiarioPagamento(java.math.BigDecimal valorLimiteDiarioPagamento)
    {
        this._valorLimiteDiarioPagamento = valorLimiteDiarioPagamento;
    } //-- void setValorLimiteDiarioPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'valorLimiteIndividualPagamento'.
     * 
     * @param valorLimiteIndividualPagamento the value of field
     * 'valorLimiteIndividualPagamento'.
     */
    public void setValorLimiteIndividualPagamento(java.math.BigDecimal valorLimiteIndividualPagamento)
    {
        this._valorLimiteIndividualPagamento = valorLimiteIndividualPagamento;
    } //-- void setValorLimiteIndividualPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPercentualDiferencaTolerada'.
     * 
     * @param vlPercentualDiferencaTolerada the value of field
     * 'vlPercentualDiferencaTolerada'.
     */
    public void setVlPercentualDiferencaTolerada(java.math.BigDecimal vlPercentualDiferencaTolerada)
    {
        this._vlPercentualDiferencaTolerada = vlPercentualDiferencaTolerada;
    } //-- void setVlPercentualDiferencaTolerada(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarConManServicosResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.ConsultarConManServicosResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.ConsultarConManServicosResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.ConsultarConManServicosResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanservicos.response.ConsultarConManServicosResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
