/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _nrCorpoCpfCnpj
     */
    private long _nrCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _nrCorpoCpfCnpj
     */
    private boolean _has_nrCorpoCpfCnpj;

    /**
     * Field _nrFilialCnpjParticipante
     */
    private int _nrFilialCnpjParticipante = 0;

    /**
     * keeps track of state for field: _nrFilialCnpjParticipante
     */
    private boolean _has_nrFilialCnpjParticipante;

    /**
     * Field _nrControleCnpjParticipante
     */
    private int _nrControleCnpjParticipante = 0;

    /**
     * keeps track of state for field: _nrControleCnpjParticipante
     */
    private boolean _has_nrControleCnpjParticipante;

    /**
     * Field _dsRazaoSocialParticipante
     */
    private java.lang.String _dsRazaoSocialParticipante;

    /**
     * Field _dsRazaoSocialReduzido
     */
    private java.lang.String _dsRazaoSocialReduzido;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _nrConta
     */
    private int _nrConta = 0;

    /**
     * keeps track of state for field: _nrConta
     */
    private boolean _has_nrConta;

    /**
     * Field _nrContaDigito
     */
    private int _nrContaDigito = 0;

    /**
     * keeps track of state for field: _nrContaDigito
     */
    private boolean _has_nrContaDigito;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNrConta
     * 
     */
    public void deleteNrConta()
    {
        this._has_nrConta= false;
    } //-- void deleteNrConta() 

    /**
     * Method deleteNrContaDigito
     * 
     */
    public void deleteNrContaDigito()
    {
        this._has_nrContaDigito= false;
    } //-- void deleteNrContaDigito() 

    /**
     * Method deleteNrControleCnpjParticipante
     * 
     */
    public void deleteNrControleCnpjParticipante()
    {
        this._has_nrControleCnpjParticipante= false;
    } //-- void deleteNrControleCnpjParticipante() 

    /**
     * Method deleteNrCorpoCpfCnpj
     * 
     */
    public void deleteNrCorpoCpfCnpj()
    {
        this._has_nrCorpoCpfCnpj= false;
    } //-- void deleteNrCorpoCpfCnpj() 

    /**
     * Method deleteNrFilialCnpjParticipante
     * 
     */
    public void deleteNrFilialCnpjParticipante()
    {
        this._has_nrFilialCnpjParticipante= false;
    } //-- void deleteNrFilialCnpjParticipante() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsRazaoSocialParticipante'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocialParticipante'.
     */
    public java.lang.String getDsRazaoSocialParticipante()
    {
        return this._dsRazaoSocialParticipante;
    } //-- java.lang.String getDsRazaoSocialParticipante() 

    /**
     * Returns the value of field 'dsRazaoSocialReduzido'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocialReduzido'.
     */
    public java.lang.String getDsRazaoSocialReduzido()
    {
        return this._dsRazaoSocialReduzido;
    } //-- java.lang.String getDsRazaoSocialReduzido() 

    /**
     * Returns the value of field 'nrConta'.
     * 
     * @return int
     * @return the value of field 'nrConta'.
     */
    public int getNrConta()
    {
        return this._nrConta;
    } //-- int getNrConta() 

    /**
     * Returns the value of field 'nrContaDigito'.
     * 
     * @return int
     * @return the value of field 'nrContaDigito'.
     */
    public int getNrContaDigito()
    {
        return this._nrContaDigito;
    } //-- int getNrContaDigito() 

    /**
     * Returns the value of field 'nrControleCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'nrControleCnpjParticipante'.
     */
    public int getNrControleCnpjParticipante()
    {
        return this._nrControleCnpjParticipante;
    } //-- int getNrControleCnpjParticipante() 

    /**
     * Returns the value of field 'nrCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'nrCorpoCpfCnpj'.
     */
    public long getNrCorpoCpfCnpj()
    {
        return this._nrCorpoCpfCnpj;
    } //-- long getNrCorpoCpfCnpj() 

    /**
     * Returns the value of field 'nrFilialCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'nrFilialCnpjParticipante'.
     */
    public int getNrFilialCnpjParticipante()
    {
        return this._nrFilialCnpjParticipante;
    } //-- int getNrFilialCnpjParticipante() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNrConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrConta()
    {
        return this._has_nrConta;
    } //-- boolean hasNrConta() 

    /**
     * Method hasNrContaDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContaDigito()
    {
        return this._has_nrContaDigito;
    } //-- boolean hasNrContaDigito() 

    /**
     * Method hasNrControleCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrControleCnpjParticipante()
    {
        return this._has_nrControleCnpjParticipante;
    } //-- boolean hasNrControleCnpjParticipante() 

    /**
     * Method hasNrCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrCorpoCpfCnpj()
    {
        return this._has_nrCorpoCpfCnpj;
    } //-- boolean hasNrCorpoCpfCnpj() 

    /**
     * Method hasNrFilialCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFilialCnpjParticipante()
    {
        return this._has_nrFilialCnpjParticipante;
    } //-- boolean hasNrFilialCnpjParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'dsRazaoSocialParticipante'.
     * 
     * @param dsRazaoSocialParticipante the value of field
     * 'dsRazaoSocialParticipante'.
     */
    public void setDsRazaoSocialParticipante(java.lang.String dsRazaoSocialParticipante)
    {
        this._dsRazaoSocialParticipante = dsRazaoSocialParticipante;
    } //-- void setDsRazaoSocialParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocialReduzido'.
     * 
     * @param dsRazaoSocialReduzido the value of field
     * 'dsRazaoSocialReduzido'.
     */
    public void setDsRazaoSocialReduzido(java.lang.String dsRazaoSocialReduzido)
    {
        this._dsRazaoSocialReduzido = dsRazaoSocialReduzido;
    } //-- void setDsRazaoSocialReduzido(java.lang.String) 

    /**
     * Sets the value of field 'nrConta'.
     * 
     * @param nrConta the value of field 'nrConta'.
     */
    public void setNrConta(int nrConta)
    {
        this._nrConta = nrConta;
        this._has_nrConta = true;
    } //-- void setNrConta(int) 

    /**
     * Sets the value of field 'nrContaDigito'.
     * 
     * @param nrContaDigito the value of field 'nrContaDigito'.
     */
    public void setNrContaDigito(int nrContaDigito)
    {
        this._nrContaDigito = nrContaDigito;
        this._has_nrContaDigito = true;
    } //-- void setNrContaDigito(int) 

    /**
     * Sets the value of field 'nrControleCnpjParticipante'.
     * 
     * @param nrControleCnpjParticipante the value of field
     * 'nrControleCnpjParticipante'.
     */
    public void setNrControleCnpjParticipante(int nrControleCnpjParticipante)
    {
        this._nrControleCnpjParticipante = nrControleCnpjParticipante;
        this._has_nrControleCnpjParticipante = true;
    } //-- void setNrControleCnpjParticipante(int) 

    /**
     * Sets the value of field 'nrCorpoCpfCnpj'.
     * 
     * @param nrCorpoCpfCnpj the value of field 'nrCorpoCpfCnpj'.
     */
    public void setNrCorpoCpfCnpj(long nrCorpoCpfCnpj)
    {
        this._nrCorpoCpfCnpj = nrCorpoCpfCnpj;
        this._has_nrCorpoCpfCnpj = true;
    } //-- void setNrCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'nrFilialCnpjParticipante'.
     * 
     * @param nrFilialCnpjParticipante the value of field
     * 'nrFilialCnpjParticipante'.
     */
    public void setNrFilialCnpjParticipante(int nrFilialCnpjParticipante)
    {
        this._nrFilialCnpjParticipante = nrFilialCnpjParticipante;
        this._has_nrFilialCnpjParticipante = true;
    } //-- void setNrFilialCnpjParticipante(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
