/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarParticipantesEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipantesEntradaDTO {
	

	/** Atributo maxOcorrencias. */
	private int maxOcorrencias;
	
	/** Atributo codPessoaJuridica. */
	private long codPessoaJuridica;
	
	/** Atributo codTipoContrato. */
	private int codTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private long nrSequenciaContrato;

	
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: codPessoaJuridica.
	 *
	 * @return codPessoaJuridica
	 */
	public long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}
	
	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}
	
	/**
	 * Get: codTipoContrato.
	 *
	 * @return codTipoContrato
	 */
	public int getCodTipoContrato() {
		return codTipoContrato;
	}
	
	/**
	 * Set: codTipoContrato.
	 *
	 * @param codTipoContrato the cod tipo contrato
	 */
	public void setCodTipoContrato(int codTipoContrato) {
		this.codTipoContrato = codTipoContrato;
	}
	
	/**
	 * Get: maxOcorrencias.
	 *
	 * @return maxOcorrencias
	 */
	public int getMaxOcorrencias() {
		return maxOcorrencias;
	}
	
	/**
	 * Set: maxOcorrencias.
	 *
	 * @param maxOcorrencias the max ocorrencias
	 */
	public void setMaxOcorrencias(int maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}


}
