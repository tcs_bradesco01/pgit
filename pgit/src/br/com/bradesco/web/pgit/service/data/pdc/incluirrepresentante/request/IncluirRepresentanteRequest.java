/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirRepresentanteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirRepresentanteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _nrManutencaoContratoNegocio
     */
    private long _nrManutencaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrManutencaoContratoNegocio
     */
    private boolean _has_nrManutencaoContratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProdutoProduto
     */
    private int _cdRelacionamentoProdutoProduto = 0;

    /**
     * keeps track of state for field:
     * _cdRelacionamentoProdutoProduto
     */
    private boolean _has_cdRelacionamentoProdutoProduto;

    /**
     * Field _cdFinalidadeItemAditivo
     */
    private int _cdFinalidadeItemAditivo = 0;

    /**
     * keeps track of state for field: _cdFinalidadeItemAditivo
     */
    private boolean _has_cdFinalidadeItemAditivo;

    /**
     * Field _cdEmpresaOperante
     */
    private long _cdEmpresaOperante = 0;

    /**
     * keeps track of state for field: _cdEmpresaOperante
     */
    private boolean _has_cdEmpresaOperante;

    /**
     * Field _cdDependenciaOperante
     */
    private int _cdDependenciaOperante = 0;

    /**
     * keeps track of state for field: _cdDependenciaOperante
     */
    private boolean _has_cdDependenciaOperante;

    /**
     * Field _cdUsuario
     */
    private java.lang.String _cdUsuario;

    /**
     * Field _nmOperacaoFluxo
     */
    private java.lang.String _nmOperacaoFluxo;

    /**
     * Field _cdCanal
     */
    private int _cdCanal = 0;

    /**
     * keeps track of state for field: _cdCanal
     */
    private boolean _has_cdCanal;

    /**
     * Field _cdMidiaComprovanteCorrentista
     */
    private int _cdMidiaComprovanteCorrentista = 0;

    /**
     * keeps track of state for field: _cdMidiaComprovanteCorrentist
     */
    private boolean _has_cdMidiaComprovanteCorrentista;

    /**
     * Field _dsMidiaComprovanteCorrentista
     */
    private java.lang.String _dsMidiaComprovanteCorrentista;

    /**
     * Field _qtFuncionarioEmpresaPagadora
     */
    private long _qtFuncionarioEmpresaPagadora = 0;

    /**
     * keeps track of state for field: _qtFuncionarioEmpresaPagadora
     */
    private boolean _has_qtFuncionarioEmpresaPagadora;

    /**
     * Field _qtViaComprovante
     */
    private int _qtViaComprovante = 0;

    /**
     * keeps track of state for field: _qtViaComprovante
     */
    private boolean _has_qtViaComprovante;

    /**
     * Field _qtViaCobradaComprovante
     */
    private int _qtViaCobradaComprovante = 0;

    /**
     * keeps track of state for field: _qtViaCobradaComprovante
     */
    private boolean _has_qtViaCobradaComprovante;

    /**
     * Field _dsViaCobradaComprovante
     */
    private java.lang.String _dsViaCobradaComprovante;

    /**
     * Field _cdIndicadorFormaContratacao
     */
    private int _cdIndicadorFormaContratacao = 0;

    /**
     * keeps track of state for field: _cdIndicadorFormaContratacao
     */
    private boolean _has_cdIndicadorFormaContratacao;

    /**
     * Field _qtdeTotalItens
     */
    private int _qtdeTotalItens = 0;

    /**
     * keeps track of state for field: _qtdeTotalItens
     */
    private boolean _has_qtdeTotalItens;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirRepresentanteRequest() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.IncluirRepresentanteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 100)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 100");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 100)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 100");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias) 

    /**
     * Method deleteCdCanal
     * 
     */
    public void deleteCdCanal()
    {
        this._has_cdCanal= false;
    } //-- void deleteCdCanal() 

    /**
     * Method deleteCdDependenciaOperante
     * 
     */
    public void deleteCdDependenciaOperante()
    {
        this._has_cdDependenciaOperante= false;
    } //-- void deleteCdDependenciaOperante() 

    /**
     * Method deleteCdEmpresaOperante
     * 
     */
    public void deleteCdEmpresaOperante()
    {
        this._has_cdEmpresaOperante= false;
    } //-- void deleteCdEmpresaOperante() 

    /**
     * Method deleteCdFinalidadeItemAditivo
     * 
     */
    public void deleteCdFinalidadeItemAditivo()
    {
        this._has_cdFinalidadeItemAditivo= false;
    } //-- void deleteCdFinalidadeItemAditivo() 

    /**
     * Method deleteCdIndicadorFormaContratacao
     * 
     */
    public void deleteCdIndicadorFormaContratacao()
    {
        this._has_cdIndicadorFormaContratacao= false;
    } //-- void deleteCdIndicadorFormaContratacao() 

    /**
     * Method deleteCdMidiaComprovanteCorrentista
     * 
     */
    public void deleteCdMidiaComprovanteCorrentista()
    {
        this._has_cdMidiaComprovanteCorrentista= false;
    } //-- void deleteCdMidiaComprovanteCorrentista() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProdutoProduto
     * 
     */
    public void deleteCdRelacionamentoProdutoProduto()
    {
        this._has_cdRelacionamentoProdutoProduto= false;
    } //-- void deleteCdRelacionamentoProdutoProduto() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrManutencaoContratoNegocio
     * 
     */
    public void deleteNrManutencaoContratoNegocio()
    {
        this._has_nrManutencaoContratoNegocio= false;
    } //-- void deleteNrManutencaoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtFuncionarioEmpresaPagadora
     * 
     */
    public void deleteQtFuncionarioEmpresaPagadora()
    {
        this._has_qtFuncionarioEmpresaPagadora= false;
    } //-- void deleteQtFuncionarioEmpresaPagadora() 

    /**
     * Method deleteQtViaCobradaComprovante
     * 
     */
    public void deleteQtViaCobradaComprovante()
    {
        this._has_qtViaCobradaComprovante= false;
    } //-- void deleteQtViaCobradaComprovante() 

    /**
     * Method deleteQtViaComprovante
     * 
     */
    public void deleteQtViaComprovante()
    {
        this._has_qtViaComprovante= false;
    } //-- void deleteQtViaComprovante() 

    /**
     * Method deleteQtdeTotalItens
     * 
     */
    public void deleteQtdeTotalItens()
    {
        this._has_qtdeTotalItens= false;
    } //-- void deleteQtdeTotalItens() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdCanal'.
     * 
     * @return int
     * @return the value of field 'cdCanal'.
     */
    public int getCdCanal()
    {
        return this._cdCanal;
    } //-- int getCdCanal() 

    /**
     * Returns the value of field 'cdDependenciaOperante'.
     * 
     * @return int
     * @return the value of field 'cdDependenciaOperante'.
     */
    public int getCdDependenciaOperante()
    {
        return this._cdDependenciaOperante;
    } //-- int getCdDependenciaOperante() 

    /**
     * Returns the value of field 'cdEmpresaOperante'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaOperante'.
     */
    public long getCdEmpresaOperante()
    {
        return this._cdEmpresaOperante;
    } //-- long getCdEmpresaOperante() 

    /**
     * Returns the value of field 'cdFinalidadeItemAditivo'.
     * 
     * @return int
     * @return the value of field 'cdFinalidadeItemAditivo'.
     */
    public int getCdFinalidadeItemAditivo()
    {
        return this._cdFinalidadeItemAditivo;
    } //-- int getCdFinalidadeItemAditivo() 

    /**
     * Returns the value of field 'cdIndicadorFormaContratacao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFormaContratacao'.
     */
    public int getCdIndicadorFormaContratacao()
    {
        return this._cdIndicadorFormaContratacao;
    } //-- int getCdIndicadorFormaContratacao() 

    /**
     * Returns the value of field 'cdMidiaComprovanteCorrentista'.
     * 
     * @return int
     * @return the value of field 'cdMidiaComprovanteCorrentista'.
     */
    public int getCdMidiaComprovanteCorrentista()
    {
        return this._cdMidiaComprovanteCorrentista;
    } //-- int getCdMidiaComprovanteCorrentista() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProdutoProduto'.
     */
    public int getCdRelacionamentoProdutoProduto()
    {
        return this._cdRelacionamentoProdutoProduto;
    } //-- int getCdRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUsuario'.
     * 
     * @return String
     * @return the value of field 'cdUsuario'.
     */
    public java.lang.String getCdUsuario()
    {
        return this._cdUsuario;
    } //-- java.lang.String getCdUsuario() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsMidiaComprovanteCorrentista'.
     * 
     * @return String
     * @return the value of field 'dsMidiaComprovanteCorrentista'.
     */
    public java.lang.String getDsMidiaComprovanteCorrentista()
    {
        return this._dsMidiaComprovanteCorrentista;
    } //-- java.lang.String getDsMidiaComprovanteCorrentista() 

    /**
     * Returns the value of field 'dsViaCobradaComprovante'.
     * 
     * @return String
     * @return the value of field 'dsViaCobradaComprovante'.
     */
    public java.lang.String getDsViaCobradaComprovante()
    {
        return this._dsViaCobradaComprovante;
    } //-- java.lang.String getDsViaCobradaComprovante() 

    /**
     * Returns the value of field 'nmOperacaoFluxo'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxo'.
     */
    public java.lang.String getNmOperacaoFluxo()
    {
        return this._nmOperacaoFluxo;
    } //-- java.lang.String getNmOperacaoFluxo() 

    /**
     * Returns the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrManutencaoContratoNegocio'.
     */
    public long getNrManutencaoContratoNegocio()
    {
        return this._nrManutencaoContratoNegocio;
    } //-- long getNrManutencaoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'qtFuncionarioEmpresaPagadora'.
     * 
     * @return long
     * @return the value of field 'qtFuncionarioEmpresaPagadora'.
     */
    public long getQtFuncionarioEmpresaPagadora()
    {
        return this._qtFuncionarioEmpresaPagadora;
    } //-- long getQtFuncionarioEmpresaPagadora() 

    /**
     * Returns the value of field 'qtViaCobradaComprovante'.
     * 
     * @return int
     * @return the value of field 'qtViaCobradaComprovante'.
     */
    public int getQtViaCobradaComprovante()
    {
        return this._qtViaCobradaComprovante;
    } //-- int getQtViaCobradaComprovante() 

    /**
     * Returns the value of field 'qtViaComprovante'.
     * 
     * @return int
     * @return the value of field 'qtViaComprovante'.
     */
    public int getQtViaComprovante()
    {
        return this._qtViaComprovante;
    } //-- int getQtViaComprovante() 

    /**
     * Returns the value of field 'qtdeTotalItens'.
     * 
     * @return int
     * @return the value of field 'qtdeTotalItens'.
     */
    public int getQtdeTotalItens()
    {
        return this._qtdeTotalItens;
    } //-- int getQtdeTotalItens() 

    /**
     * Method hasCdCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanal()
    {
        return this._has_cdCanal;
    } //-- boolean hasCdCanal() 

    /**
     * Method hasCdDependenciaOperante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDependenciaOperante()
    {
        return this._has_cdDependenciaOperante;
    } //-- boolean hasCdDependenciaOperante() 

    /**
     * Method hasCdEmpresaOperante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaOperante()
    {
        return this._has_cdEmpresaOperante;
    } //-- boolean hasCdEmpresaOperante() 

    /**
     * Method hasCdFinalidadeItemAditivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidadeItemAditivo()
    {
        return this._has_cdFinalidadeItemAditivo;
    } //-- boolean hasCdFinalidadeItemAditivo() 

    /**
     * Method hasCdIndicadorFormaContratacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFormaContratacao()
    {
        return this._has_cdIndicadorFormaContratacao;
    } //-- boolean hasCdIndicadorFormaContratacao() 

    /**
     * Method hasCdMidiaComprovanteCorrentista
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMidiaComprovanteCorrentista()
    {
        return this._has_cdMidiaComprovanteCorrentista;
    } //-- boolean hasCdMidiaComprovanteCorrentista() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProdutoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProdutoProduto()
    {
        return this._has_cdRelacionamentoProdutoProduto;
    } //-- boolean hasCdRelacionamentoProdutoProduto() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrManutencaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrManutencaoContratoNegocio()
    {
        return this._has_nrManutencaoContratoNegocio;
    } //-- boolean hasNrManutencaoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtFuncionarioEmpresaPagadora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtFuncionarioEmpresaPagadora()
    {
        return this._has_qtFuncionarioEmpresaPagadora;
    } //-- boolean hasQtFuncionarioEmpresaPagadora() 

    /**
     * Method hasQtViaCobradaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaCobradaComprovante()
    {
        return this._has_qtViaCobradaComprovante;
    } //-- boolean hasQtViaCobradaComprovante() 

    /**
     * Method hasQtViaComprovante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViaComprovante()
    {
        return this._has_qtViaComprovante;
    } //-- boolean hasQtViaComprovante() 

    /**
     * Method hasQtdeTotalItens
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeTotalItens()
    {
        return this._has_qtdeTotalItens;
    } //-- boolean hasQtdeTotalItens() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdCanal'.
     * 
     * @param cdCanal the value of field 'cdCanal'.
     */
    public void setCdCanal(int cdCanal)
    {
        this._cdCanal = cdCanal;
        this._has_cdCanal = true;
    } //-- void setCdCanal(int) 

    /**
     * Sets the value of field 'cdDependenciaOperante'.
     * 
     * @param cdDependenciaOperante the value of field
     * 'cdDependenciaOperante'.
     */
    public void setCdDependenciaOperante(int cdDependenciaOperante)
    {
        this._cdDependenciaOperante = cdDependenciaOperante;
        this._has_cdDependenciaOperante = true;
    } //-- void setCdDependenciaOperante(int) 

    /**
     * Sets the value of field 'cdEmpresaOperante'.
     * 
     * @param cdEmpresaOperante the value of field
     * 'cdEmpresaOperante'.
     */
    public void setCdEmpresaOperante(long cdEmpresaOperante)
    {
        this._cdEmpresaOperante = cdEmpresaOperante;
        this._has_cdEmpresaOperante = true;
    } //-- void setCdEmpresaOperante(long) 

    /**
     * Sets the value of field 'cdFinalidadeItemAditivo'.
     * 
     * @param cdFinalidadeItemAditivo the value of field
     * 'cdFinalidadeItemAditivo'.
     */
    public void setCdFinalidadeItemAditivo(int cdFinalidadeItemAditivo)
    {
        this._cdFinalidadeItemAditivo = cdFinalidadeItemAditivo;
        this._has_cdFinalidadeItemAditivo = true;
    } //-- void setCdFinalidadeItemAditivo(int) 

    /**
     * Sets the value of field 'cdIndicadorFormaContratacao'.
     * 
     * @param cdIndicadorFormaContratacao the value of field
     * 'cdIndicadorFormaContratacao'.
     */
    public void setCdIndicadorFormaContratacao(int cdIndicadorFormaContratacao)
    {
        this._cdIndicadorFormaContratacao = cdIndicadorFormaContratacao;
        this._has_cdIndicadorFormaContratacao = true;
    } //-- void setCdIndicadorFormaContratacao(int) 

    /**
     * Sets the value of field 'cdMidiaComprovanteCorrentista'.
     * 
     * @param cdMidiaComprovanteCorrentista the value of field
     * 'cdMidiaComprovanteCorrentista'.
     */
    public void setCdMidiaComprovanteCorrentista(int cdMidiaComprovanteCorrentista)
    {
        this._cdMidiaComprovanteCorrentista = cdMidiaComprovanteCorrentista;
        this._has_cdMidiaComprovanteCorrentista = true;
    } //-- void setCdMidiaComprovanteCorrentista(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @param cdRelacionamentoProdutoProduto the value of field
     * 'cdRelacionamentoProdutoProduto'.
     */
    public void setCdRelacionamentoProdutoProduto(int cdRelacionamentoProdutoProduto)
    {
        this._cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
        this._has_cdRelacionamentoProdutoProduto = true;
    } //-- void setCdRelacionamentoProdutoProduto(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUsuario'.
     * 
     * @param cdUsuario the value of field 'cdUsuario'.
     */
    public void setCdUsuario(java.lang.String cdUsuario)
    {
        this._cdUsuario = cdUsuario;
    } //-- void setCdUsuario(java.lang.String) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dsMidiaComprovanteCorrentista'.
     * 
     * @param dsMidiaComprovanteCorrentista the value of field
     * 'dsMidiaComprovanteCorrentista'.
     */
    public void setDsMidiaComprovanteCorrentista(java.lang.String dsMidiaComprovanteCorrentista)
    {
        this._dsMidiaComprovanteCorrentista = dsMidiaComprovanteCorrentista;
    } //-- void setDsMidiaComprovanteCorrentista(java.lang.String) 

    /**
     * Sets the value of field 'dsViaCobradaComprovante'.
     * 
     * @param dsViaCobradaComprovante the value of field
     * 'dsViaCobradaComprovante'.
     */
    public void setDsViaCobradaComprovante(java.lang.String dsViaCobradaComprovante)
    {
        this._dsViaCobradaComprovante = dsViaCobradaComprovante;
    } //-- void setDsViaCobradaComprovante(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxo'.
     * 
     * @param nmOperacaoFluxo the value of field 'nmOperacaoFluxo'.
     */
    public void setNmOperacaoFluxo(java.lang.String nmOperacaoFluxo)
    {
        this._nmOperacaoFluxo = nmOperacaoFluxo;
    } //-- void setNmOperacaoFluxo(java.lang.String) 

    /**
     * Sets the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @param nrManutencaoContratoNegocio the value of field
     * 'nrManutencaoContratoNegocio'.
     */
    public void setNrManutencaoContratoNegocio(long nrManutencaoContratoNegocio)
    {
        this._nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
        this._has_nrManutencaoContratoNegocio = true;
    } //-- void setNrManutencaoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 100)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 100");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.Ocorrencias) 

    /**
     * Sets the value of field 'qtFuncionarioEmpresaPagadora'.
     * 
     * @param qtFuncionarioEmpresaPagadora the value of field
     * 'qtFuncionarioEmpresaPagadora'.
     */
    public void setQtFuncionarioEmpresaPagadora(long qtFuncionarioEmpresaPagadora)
    {
        this._qtFuncionarioEmpresaPagadora = qtFuncionarioEmpresaPagadora;
        this._has_qtFuncionarioEmpresaPagadora = true;
    } //-- void setQtFuncionarioEmpresaPagadora(long) 

    /**
     * Sets the value of field 'qtViaCobradaComprovante'.
     * 
     * @param qtViaCobradaComprovante the value of field
     * 'qtViaCobradaComprovante'.
     */
    public void setQtViaCobradaComprovante(int qtViaCobradaComprovante)
    {
        this._qtViaCobradaComprovante = qtViaCobradaComprovante;
        this._has_qtViaCobradaComprovante = true;
    } //-- void setQtViaCobradaComprovante(int) 

    /**
     * Sets the value of field 'qtViaComprovante'.
     * 
     * @param qtViaComprovante the value of field 'qtViaComprovante'
     */
    public void setQtViaComprovante(int qtViaComprovante)
    {
        this._qtViaComprovante = qtViaComprovante;
        this._has_qtViaComprovante = true;
    } //-- void setQtViaComprovante(int) 

    /**
     * Sets the value of field 'qtdeTotalItens'.
     * 
     * @param qtdeTotalItens the value of field 'qtdeTotalItens'.
     */
    public void setQtdeTotalItens(int qtdeTotalItens)
    {
        this._qtdeTotalItens = qtdeTotalItens;
        this._has_qtdeTotalItens = true;
    } //-- void setQtdeTotalItens(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirRepresentanteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.IncluirRepresentanteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.IncluirRepresentanteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.IncluirRepresentanteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirrepresentante.request.IncluirRepresentanteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
