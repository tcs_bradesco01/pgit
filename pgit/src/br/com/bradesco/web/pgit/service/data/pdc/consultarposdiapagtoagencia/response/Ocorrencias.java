/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCorpoCnpjCpf
     */
    private long _cdCorpoCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCorpoCnpjCpf
     */
    private boolean _has_cdCorpoCnpjCpf;

    /**
     * Field _cdCnpjCpfFilial
     */
    private int _cdCnpjCpfFilial = 0;

    /**
     * keeps track of state for field: _cdCnpjCpfFilial
     */
    private boolean _has_cdCnpjCpfFilial;

    /**
     * Field _cdCnpjCpfDigito
     */
    private int _cdCnpjCpfDigito = 0;

    /**
     * keeps track of state for field: _cdCnpjCpfDigito
     */
    private boolean _has_cdCnpjCpfDigito;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdEmpresa
     */
    private long _cdEmpresa = 0;

    /**
     * keeps track of state for field: _cdEmpresa
     */
    private boolean _has_cdEmpresa;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;

    /**
     * Field _cdTipoContratoRec
     */
    private int _cdTipoContratoRec = 0;

    /**
     * keeps track of state for field: _cdTipoContratoRec
     */
    private boolean _has_cdTipoContratoRec;

    /**
     * Field _dsTipoContratoNegocio
     */
    private java.lang.String _dsTipoContratoNegocio;

    /**
     * Field _cdContratoOrigemRec
     */
    private long _cdContratoOrigemRec = 0;

    /**
     * keeps track of state for field: _cdContratoOrigemRec
     */
    private boolean _has_cdContratoOrigemRec;

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _dsServico
     */
    private java.lang.String _dsServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _dsModalidade
     */
    private java.lang.String _dsModalidade;

    /**
     * Field _vlPagamentoAgendado
     */
    private java.math.BigDecimal _vlPagamentoAgendado = new java.math.BigDecimal("0");

    /**
     * Field _vlPagamentoPendente
     */
    private java.math.BigDecimal _vlPagamentoPendente = new java.math.BigDecimal("0");

    /**
     * Field _vlPagamentoSemSaldo
     */
    private java.math.BigDecimal _vlPagamentoSemSaldo = new java.math.BigDecimal("0");

    /**
     * Field _vlPagamentoEfetivado
     */
    private java.math.BigDecimal _vlPagamentoEfetivado = new java.math.BigDecimal("0");

    /**
     * Field _vlTotalPagamentos
     */
    private java.math.BigDecimal _vlTotalPagamentos = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamentoAgendado(new java.math.BigDecimal("0"));
        setVlPagamentoPendente(new java.math.BigDecimal("0"));
        setVlPagamentoSemSaldo(new java.math.BigDecimal("0"));
        setVlPagamentoEfetivado(new java.math.BigDecimal("0"));
        setVlTotalPagamentos(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjCpfDigito
     * 
     */
    public void deleteCdCnpjCpfDigito()
    {
        this._has_cdCnpjCpfDigito= false;
    } //-- void deleteCdCnpjCpfDigito() 

    /**
     * Method deleteCdCnpjCpfFilial
     * 
     */
    public void deleteCdCnpjCpfFilial()
    {
        this._has_cdCnpjCpfFilial= false;
    } //-- void deleteCdCnpjCpfFilial() 

    /**
     * Method deleteCdContratoOrigemRec
     * 
     */
    public void deleteCdContratoOrigemRec()
    {
        this._has_cdContratoOrigemRec= false;
    } //-- void deleteCdContratoOrigemRec() 

    /**
     * Method deleteCdCorpoCnpjCpf
     * 
     */
    public void deleteCdCorpoCnpjCpf()
    {
        this._has_cdCorpoCnpjCpf= false;
    } //-- void deleteCdCorpoCnpjCpf() 

    /**
     * Method deleteCdEmpresa
     * 
     */
    public void deleteCdEmpresa()
    {
        this._has_cdEmpresa= false;
    } //-- void deleteCdEmpresa() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdTipoContratoRec
     * 
     */
    public void deleteCdTipoContratoRec()
    {
        this._has_cdTipoContratoRec= false;
    } //-- void deleteCdTipoContratoRec() 

    /**
     * Returns the value of field 'cdCnpjCpfDigito'.
     * 
     * @return int
     * @return the value of field 'cdCnpjCpfDigito'.
     */
    public int getCdCnpjCpfDigito()
    {
        return this._cdCnpjCpfDigito;
    } //-- int getCdCnpjCpfDigito() 

    /**
     * Returns the value of field 'cdCnpjCpfFilial'.
     * 
     * @return int
     * @return the value of field 'cdCnpjCpfFilial'.
     */
    public int getCdCnpjCpfFilial()
    {
        return this._cdCnpjCpfFilial;
    } //-- int getCdCnpjCpfFilial() 

    /**
     * Returns the value of field 'cdContratoOrigemRec'.
     * 
     * @return long
     * @return the value of field 'cdContratoOrigemRec'.
     */
    public long getCdContratoOrigemRec()
    {
        return this._cdContratoOrigemRec;
    } //-- long getCdContratoOrigemRec() 

    /**
     * Returns the value of field 'cdCorpoCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCnpjCpf'.
     */
    public long getCdCorpoCnpjCpf()
    {
        return this._cdCorpoCnpjCpf;
    } //-- long getCdCorpoCnpjCpf() 

    /**
     * Returns the value of field 'cdEmpresa'.
     * 
     * @return long
     * @return the value of field 'cdEmpresa'.
     */
    public long getCdEmpresa()
    {
        return this._cdEmpresa;
    } //-- long getCdEmpresa() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdTipoContratoRec'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoRec'.
     */
    public int getCdTipoContratoRec()
    {
        return this._cdTipoContratoRec;
    } //-- int getCdTipoContratoRec() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModalidade'.
     */
    public java.lang.String getDsModalidade()
    {
        return this._dsModalidade;
    } //-- java.lang.String getDsModalidade() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsServico'.
     * 
     * @return String
     * @return the value of field 'dsServico'.
     */
    public java.lang.String getDsServico()
    {
        return this._dsServico;
    } //-- java.lang.String getDsServico() 

    /**
     * Returns the value of field 'dsTipoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoNegocio'.
     */
    public java.lang.String getDsTipoContratoNegocio()
    {
        return this._dsTipoContratoNegocio;
    } //-- java.lang.String getDsTipoContratoNegocio() 

    /**
     * Returns the value of field 'vlPagamentoAgendado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoAgendado'.
     */
    public java.math.BigDecimal getVlPagamentoAgendado()
    {
        return this._vlPagamentoAgendado;
    } //-- java.math.BigDecimal getVlPagamentoAgendado() 

    /**
     * Returns the value of field 'vlPagamentoEfetivado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoEfetivado'.
     */
    public java.math.BigDecimal getVlPagamentoEfetivado()
    {
        return this._vlPagamentoEfetivado;
    } //-- java.math.BigDecimal getVlPagamentoEfetivado() 

    /**
     * Returns the value of field 'vlPagamentoPendente'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoPendente'.
     */
    public java.math.BigDecimal getVlPagamentoPendente()
    {
        return this._vlPagamentoPendente;
    } //-- java.math.BigDecimal getVlPagamentoPendente() 

    /**
     * Returns the value of field 'vlPagamentoSemSaldo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoSemSaldo'.
     */
    public java.math.BigDecimal getVlPagamentoSemSaldo()
    {
        return this._vlPagamentoSemSaldo;
    } //-- java.math.BigDecimal getVlPagamentoSemSaldo() 

    /**
     * Returns the value of field 'vlTotalPagamentos'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalPagamentos'.
     */
    public java.math.BigDecimal getVlTotalPagamentos()
    {
        return this._vlTotalPagamentos;
    } //-- java.math.BigDecimal getVlTotalPagamentos() 

    /**
     * Method hasCdCnpjCpfDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpfDigito()
    {
        return this._has_cdCnpjCpfDigito;
    } //-- boolean hasCdCnpjCpfDigito() 

    /**
     * Method hasCdCnpjCpfFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpfFilial()
    {
        return this._has_cdCnpjCpfFilial;
    } //-- boolean hasCdCnpjCpfFilial() 

    /**
     * Method hasCdContratoOrigemRec
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContratoOrigemRec()
    {
        return this._has_cdContratoOrigemRec;
    } //-- boolean hasCdContratoOrigemRec() 

    /**
     * Method hasCdCorpoCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCnpjCpf()
    {
        return this._has_cdCorpoCnpjCpf;
    } //-- boolean hasCdCorpoCnpjCpf() 

    /**
     * Method hasCdEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresa()
    {
        return this._has_cdEmpresa;
    } //-- boolean hasCdEmpresa() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdTipoContratoRec
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoRec()
    {
        return this._has_cdTipoContratoRec;
    } //-- boolean hasCdTipoContratoRec() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjCpfDigito'.
     * 
     * @param cdCnpjCpfDigito the value of field 'cdCnpjCpfDigito'.
     */
    public void setCdCnpjCpfDigito(int cdCnpjCpfDigito)
    {
        this._cdCnpjCpfDigito = cdCnpjCpfDigito;
        this._has_cdCnpjCpfDigito = true;
    } //-- void setCdCnpjCpfDigito(int) 

    /**
     * Sets the value of field 'cdCnpjCpfFilial'.
     * 
     * @param cdCnpjCpfFilial the value of field 'cdCnpjCpfFilial'.
     */
    public void setCdCnpjCpfFilial(int cdCnpjCpfFilial)
    {
        this._cdCnpjCpfFilial = cdCnpjCpfFilial;
        this._has_cdCnpjCpfFilial = true;
    } //-- void setCdCnpjCpfFilial(int) 

    /**
     * Sets the value of field 'cdContratoOrigemRec'.
     * 
     * @param cdContratoOrigemRec the value of field
     * 'cdContratoOrigemRec'.
     */
    public void setCdContratoOrigemRec(long cdContratoOrigemRec)
    {
        this._cdContratoOrigemRec = cdContratoOrigemRec;
        this._has_cdContratoOrigemRec = true;
    } //-- void setCdContratoOrigemRec(long) 

    /**
     * Sets the value of field 'cdCorpoCnpjCpf'.
     * 
     * @param cdCorpoCnpjCpf the value of field 'cdCorpoCnpjCpf'.
     */
    public void setCdCorpoCnpjCpf(long cdCorpoCnpjCpf)
    {
        this._cdCorpoCnpjCpf = cdCorpoCnpjCpf;
        this._has_cdCorpoCnpjCpf = true;
    } //-- void setCdCorpoCnpjCpf(long) 

    /**
     * Sets the value of field 'cdEmpresa'.
     * 
     * @param cdEmpresa the value of field 'cdEmpresa'.
     */
    public void setCdEmpresa(long cdEmpresa)
    {
        this._cdEmpresa = cdEmpresa;
        this._has_cdEmpresa = true;
    } //-- void setCdEmpresa(long) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdTipoContratoRec'.
     * 
     * @param cdTipoContratoRec the value of field
     * 'cdTipoContratoRec'.
     */
    public void setCdTipoContratoRec(int cdTipoContratoRec)
    {
        this._cdTipoContratoRec = cdTipoContratoRec;
        this._has_cdTipoContratoRec = true;
    } //-- void setCdTipoContratoRec(int) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(java.lang.String dsModalidade)
    {
        this._dsModalidade = dsModalidade;
    } //-- void setDsModalidade(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsServico'.
     * 
     * @param dsServico the value of field 'dsServico'.
     */
    public void setDsServico(java.lang.String dsServico)
    {
        this._dsServico = dsServico;
    } //-- void setDsServico(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoNegocio'.
     * 
     * @param dsTipoContratoNegocio the value of field
     * 'dsTipoContratoNegocio'.
     */
    public void setDsTipoContratoNegocio(java.lang.String dsTipoContratoNegocio)
    {
        this._dsTipoContratoNegocio = dsTipoContratoNegocio;
    } //-- void setDsTipoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'vlPagamentoAgendado'.
     * 
     * @param vlPagamentoAgendado the value of field
     * 'vlPagamentoAgendado'.
     */
    public void setVlPagamentoAgendado(java.math.BigDecimal vlPagamentoAgendado)
    {
        this._vlPagamentoAgendado = vlPagamentoAgendado;
    } //-- void setVlPagamentoAgendado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoEfetivado'.
     * 
     * @param vlPagamentoEfetivado the value of field
     * 'vlPagamentoEfetivado'.
     */
    public void setVlPagamentoEfetivado(java.math.BigDecimal vlPagamentoEfetivado)
    {
        this._vlPagamentoEfetivado = vlPagamentoEfetivado;
    } //-- void setVlPagamentoEfetivado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoPendente'.
     * 
     * @param vlPagamentoPendente the value of field
     * 'vlPagamentoPendente'.
     */
    public void setVlPagamentoPendente(java.math.BigDecimal vlPagamentoPendente)
    {
        this._vlPagamentoPendente = vlPagamentoPendente;
    } //-- void setVlPagamentoPendente(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoSemSaldo'.
     * 
     * @param vlPagamentoSemSaldo the value of field
     * 'vlPagamentoSemSaldo'.
     */
    public void setVlPagamentoSemSaldo(java.math.BigDecimal vlPagamentoSemSaldo)
    {
        this._vlPagamentoSemSaldo = vlPagamentoSemSaldo;
    } //-- void setVlPagamentoSemSaldo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTotalPagamentos'.
     * 
     * @param vlTotalPagamentos the value of field
     * 'vlTotalPagamentos'.
     */
    public void setVlTotalPagamentos(java.math.BigDecimal vlTotalPagamentos)
    {
        this._vlTotalPagamentos = vlTotalPagamentos;
    } //-- void setVlTotalPagamentos(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
