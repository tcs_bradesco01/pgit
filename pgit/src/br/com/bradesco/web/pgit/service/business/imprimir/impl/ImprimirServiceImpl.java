/**
 * Nome: br.com.bradesco.web.pgit.service.business.imprimir.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */ 
package br.com.bradesco.web.pgit.service.business.imprimir.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.beanutils.PropertyUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.pdc.session.bean.PDCPaginationBean;
import br.com.bradesco.web.aq.application.pdc.session.manager.IPDCSessionManager;
import br.com.bradesco.web.aq.application.pdc.util.PDCServiceFactory;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.pgit.exception.PgitException;
import br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService;
import br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.impdiaad.request.ImpdiaadRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecodigobarra.request.ImprimirComprovanteCodigoBarraRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantecredconta.request.ImprimirComprovanteCredContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantedarf.request.ImprimirComprovanteDARFRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantedoc.request.ImprimirComprovanteDOCRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantegare.request.ImprimirComprovanteGARERequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantegps.request.ImprimirComprovanteGPSRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovanteop.request.ImprimirComprovanteOPRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovanteted.request.ImprimirComprovanteTEDRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulobradesco.request.ImprimirComprovanteTituloBradescoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircomprovantetitulooutros.request.ImprimirComprovanteTituloOutrosRequest;
import br.com.bradesco.web.pgit.utils.PgitUtil;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Nome: ImprimirServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador Imprimir
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IImprimirService
 */
public class ImprimirServiceImpl implements IImprimirService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirArquivo(java.io.OutputStream, java.lang.Long)
	 */
	public void imprimirArquivo(OutputStream outputStream, Long protocolo) {
        try {
            List inputStreamImpd = imprimirProtocoloImpd(protocolo);

            if (inputStreamImpd != null) {
                for (Object o : inputStreamImpd) {
                    outputStream.write((byte[]) o);
                }
            }
        } catch (IOException ex) {
            throw new PgitException(ex.getMessage(), ex, "");
        }
	}

	/**
     * Imprimir protocolo impd.
     * 
     * @param protocolo
     *            the protocolo
     * @return the list inputStreamImpd
     */
	private List imprimirProtocoloImpd(Long protocolo) {
	    ImpdiaadRequest requestImpd = new ImpdiaadRequest();

        requestImpd.setIdDocumentoGerado(PgitUtil.verificaLongNulo(protocolo));
        requestImpd.setTipoImpressao("O");//TIPO DE IMPRESS�O = O (ONLINE)
        requestImpd.setPaginaDocumento(1); //PAGINA = 1 (DESDE A PAGINA 1

        getFactoryAdapter().getImpdiaadPDCAdapter().invokeProcess(requestImpd);

        IPDCSessionManager pdcSessionManager = PDCServiceFactory.getPdcSessionManager();

        PDCPaginationBean paginationBean = pdcSessionManager.getPDCPaginationBean(null);

        return paginationBean.getParamMap("inputStreams");
	}
	
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirArquivoDuasVias(
     * java.io.OutputStream, java.lang.Long, java.lang.Long)
     */
    public void imprimirArquivoDuasVias(OutputStream outputStream, Long protocoloPrimeiraVia,
        Long protocoloSegundaVia) {

        List<ByteArrayOutputStream> listBytesPdf = new ArrayList<ByteArrayOutputStream>();

        Long[] protocolos = {protocoloPrimeiraVia, protocoloSegundaVia};
        for (Long p : protocolos) {
            List inputStreamImpd = imprimirProtocoloImpd(p);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                if (inputStreamImpd != null) {
                    for (Object o : inputStreamImpd) {
                        byteArrayOutputStream.write((byte[]) o);
                    }
                }
            } catch (IOException e) {
                throw new PgitException(e.getMessage(), e, "");
            } finally {
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e) {
                    BradescoCommonServiceFactory.getLogManager().info(this.getClass(), e.getMessage());
                }
            }

            listBytesPdf.add(byteArrayOutputStream);
        }

        imprimirViasJuntasPdf(outputStream, listBytesPdf);
    }

    /**
     * Imprimir vias juntas pdf.
     * 
     * @param outputStream
     *            the output stream
     * @param listBytesPdf
     *            the list bytes pdf
     */
    private void imprimirViasJuntasPdf(OutputStream outputStream, List<ByteArrayOutputStream> listBytesPdf) {
        Document document = new Document();
        PdfWriter writer = null;
        try {
            writer = PdfWriter.getInstance(document, outputStream);
            document.open();
            PdfContentByte cb = writer.getDirectContent();
            
            for (ByteArrayOutputStream bytes : listBytesPdf) {
                PdfReader reader = new PdfReader(bytes.toByteArray());
                for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                    document.newPage();
                    PdfImportedPage page = writer.getImportedPage(reader, i);
                    cb.addTemplate(page, 0, 0);
                }
            }

            outputStream.flush();
        } catch (DocumentException e) {
            throw new PgitException(e.getMessage(), e, "");
        } catch (IOException e) {
            throw new PgitException(e.getMessage(), e, "");
        } finally {
            document.close();
        }
    }

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteCredConta(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteCredConta(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteCredContaRequest(), getFactoryAdapter().getImprimirComprovanteCredContaPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteCodigoBarras(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteCodigoBarras(
			ImprimirComprovanteEntradaDTO entrada) {
		ImprimirComprovanteCodigoBarraRequest request = new ImprimirComprovanteCodigoBarraRequest();
		request.setCdTipoVia(PgitUtil.verificaIntegerNulo(entrada.getCdTipoVia()));

		return imprimirComprovante(entrada, request, getFactoryAdapter().getImprimirComprovanteCodigoBarraPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteDARF(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteDARF(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteDARFRequest(), getFactoryAdapter().getImprimirComprovanteDARFPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteDOC(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteDOC(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteDOCRequest(), getFactoryAdapter().getImprimirComprovanteDOCPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteGARE(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteGARE(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteGARERequest(), getFactoryAdapter().getImprimirComprovanteGAREPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteGPS(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteGPS(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteGPSRequest(), getFactoryAdapter().getImprimirComprovanteGPSPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteOP(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteOP(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteOPRequest(), getFactoryAdapter().getImprimirComprovanteOPPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteTED(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteTED(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteTEDRequest(), getFactoryAdapter().getImprimirComprovanteTEDPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteTituloBradesco(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteTituloBradesco(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteTituloBradescoRequest(), getFactoryAdapter().getImprimirComprovanteTituloBradescoPDCAdapter());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService#imprimirComprovanteTituloOutros(br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO)
	 */
	public ImprimirComprovanteSaidaDTO imprimirComprovanteTituloOutros(
			ImprimirComprovanteEntradaDTO entrada) {
		return imprimirComprovante(entrada, new ImprimirComprovanteTituloOutrosRequest(), getFactoryAdapter().getImprimirComprovanteTituloOutrosPDCAdapter());
	}

	/**
	 * Imprimir comprovante.
	 *
	 * @param entrada the entrada
	 * @param request the request
	 * @param pdcAdapter the pdc adapter
	 * @return the imprimir comprovante saida dto
	 */
	private ImprimirComprovanteSaidaDTO imprimirComprovante(
			ImprimirComprovanteEntradaDTO entrada, Object request, Object pdcAdapter) {
		try {
			PropertyUtils.setProperty(request, "cdPessoaJuridicaContrato", PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
			PropertyUtils.setProperty(request, "cdTipoContratoNegocio", PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
			PropertyUtils.setProperty(request, "nrSequenciaContratoNegocio", PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
			PropertyUtils.setProperty(request, "cdControlePagamento", PgitUtil.verificaStringNula(entrada.getCdControlePagamento()));
			PropertyUtils.setProperty(request, "dtEfetivacaoPagamento", PgitUtil.verificaStringNula(entrada.getDtEfetivacaoPagamento()));
			
			Object response = MethodUtils.invokeMethod(pdcAdapter, "invokeProcess", request);

			ImprimirComprovanteSaidaDTO saida = new ImprimirComprovanteSaidaDTO();

			saida.setCodMensagem((String) PropertyUtils.getProperty(response, "codMensagem"));
			saida.setMensagem((String) PropertyUtils.getProperty(response, "mensagem"));
			if (PropertyUtils.isReadable(response, "cdWidtfd")) {
				saida.setCdWidtfd((String) PropertyUtils.getProperty(response, "cdWidtfd"));	
			}
			saida.setQuantidadeOcorrencia((Integer) PropertyUtils.getProperty(response, "quantidadeOcorrencia"));

			Long protocolo = new Long(0l);
			Integer qtdeRegistros = (Integer) PropertyUtils.getProperty(response, "ocorrenciaCount");
			for (int i = 0; i < qtdeRegistros; i++) {
				Object ocorrencia = PropertyUtils.getIndexedProperty(response, "ocorrencia", i);

				String centroCusto = (String) PropertyUtils.getProperty(ocorrencia, "centroCusto");
				if ("IMPD".equals(centroCusto)) {
					protocolo = (Long) PropertyUtils.getProperty(ocorrencia, "protocolo");
				}
			}
			saida.setProtocolo(protocolo);
			return saida;
		} catch (IllegalAccessException e) {
			throw new PgitException("Erro na gera��o do comprovante.", e, "");
		} catch (InvocationTargetException e) {
			Throwable targetException = e.getTargetException();
			if (targetException instanceof PdcAdapterFunctionalException) {
				throw (PdcAdapterFunctionalException) targetException;
			}

			throw new PgitException("Erro na gera��o do comprovante.", e, "");
		} catch (NoSuchMethodException e) {
			throw new PgitException("Erro na gera��o do comprovante.", e, "");
		}
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

}