package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean;


/**
 * Arquivo criado em 13/10/15.
 */
public class ValidarAgenciaEntradaDTO {

    private Integer maxOcorrencias;

    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }
}