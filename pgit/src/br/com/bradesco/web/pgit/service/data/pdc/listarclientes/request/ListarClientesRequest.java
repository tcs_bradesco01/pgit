/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarClientesRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarClientesRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filial
     */
    private int _filial = 0;

    /**
     * keeps track of state for field: _filial
     */
    private boolean _has_filial;

    /**
     * Field _controle
     */
    private int _controle = 0;

    /**
     * keeps track of state for field: _controle
     */
    private boolean _has_controle;

    /**
     * Field _banco
     */
    private int _banco = 0;

    /**
     * keeps track of state for field: _banco
     */
    private boolean _has_banco;

    /**
     * Field _agencia
     */
    private int _agencia = 0;

    /**
     * keeps track of state for field: _agencia
     */
    private boolean _has_agencia;

    /**
     * Field _conta
     */
    private long _conta = 0;

    /**
     * keeps track of state for field: _conta
     */
    private boolean _has_conta;

    /**
     * Field _nomeRazao
     */
    private java.lang.String _nomeRazao;

    /**
     * Field _dataNascimentoFundacao
     */
    private java.lang.String _dataNascimentoFundacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarClientesRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request.ListarClientesRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteAgencia
     * 
     */
    public void deleteAgencia()
    {
        this._has_agencia= false;
    } //-- void deleteAgencia() 

    /**
     * Method deleteBanco
     * 
     */
    public void deleteBanco()
    {
        this._has_banco= false;
    } //-- void deleteBanco() 

    /**
     * Method deleteConta
     * 
     */
    public void deleteConta()
    {
        this._has_conta= false;
    } //-- void deleteConta() 

    /**
     * Method deleteControle
     * 
     */
    public void deleteControle()
    {
        this._has_controle= false;
    } //-- void deleteControle() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilial
     * 
     */
    public void deleteFilial()
    {
        this._has_filial= false;
    } //-- void deleteFilial() 

    /**
     * Returns the value of field 'agencia'.
     * 
     * @return int
     * @return the value of field 'agencia'.
     */
    public int getAgencia()
    {
        return this._agencia;
    } //-- int getAgencia() 

    /**
     * Returns the value of field 'banco'.
     * 
     * @return int
     * @return the value of field 'banco'.
     */
    public int getBanco()
    {
        return this._banco;
    } //-- int getBanco() 

    /**
     * Returns the value of field 'conta'.
     * 
     * @return long
     * @return the value of field 'conta'.
     */
    public long getConta()
    {
        return this._conta;
    } //-- long getConta() 

    /**
     * Returns the value of field 'controle'.
     * 
     * @return int
     * @return the value of field 'controle'.
     */
    public int getControle()
    {
        return this._controle;
    } //-- int getControle() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'dataNascimentoFundacao'.
     * 
     * @return String
     * @return the value of field 'dataNascimentoFundacao'.
     */
    public java.lang.String getDataNascimentoFundacao()
    {
        return this._dataNascimentoFundacao;
    } //-- java.lang.String getDataNascimentoFundacao() 

    /**
     * Returns the value of field 'filial'.
     * 
     * @return int
     * @return the value of field 'filial'.
     */
    public int getFilial()
    {
        return this._filial;
    } //-- int getFilial() 

    /**
     * Returns the value of field 'nomeRazao'.
     * 
     * @return String
     * @return the value of field 'nomeRazao'.
     */
    public java.lang.String getNomeRazao()
    {
        return this._nomeRazao;
    } //-- java.lang.String getNomeRazao() 

    /**
     * Method hasAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasAgencia()
    {
        return this._has_agencia;
    } //-- boolean hasAgencia() 

    /**
     * Method hasBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasBanco()
    {
        return this._has_banco;
    } //-- boolean hasBanco() 

    /**
     * Method hasConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasConta()
    {
        return this._has_conta;
    } //-- boolean hasConta() 

    /**
     * Method hasControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControle()
    {
        return this._has_controle;
    } //-- boolean hasControle() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilial()
    {
        return this._has_filial;
    } //-- boolean hasFilial() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'agencia'.
     * 
     * @param agencia the value of field 'agencia'.
     */
    public void setAgencia(int agencia)
    {
        this._agencia = agencia;
        this._has_agencia = true;
    } //-- void setAgencia(int) 

    /**
     * Sets the value of field 'banco'.
     * 
     * @param banco the value of field 'banco'.
     */
    public void setBanco(int banco)
    {
        this._banco = banco;
        this._has_banco = true;
    } //-- void setBanco(int) 

    /**
     * Sets the value of field 'conta'.
     * 
     * @param conta the value of field 'conta'.
     */
    public void setConta(long conta)
    {
        this._conta = conta;
        this._has_conta = true;
    } //-- void setConta(long) 

    /**
     * Sets the value of field 'controle'.
     * 
     * @param controle the value of field 'controle'.
     */
    public void setControle(int controle)
    {
        this._controle = controle;
        this._has_controle = true;
    } //-- void setControle(int) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'dataNascimentoFundacao'.
     * 
     * @param dataNascimentoFundacao the value of field
     * 'dataNascimentoFundacao'.
     */
    public void setDataNascimentoFundacao(java.lang.String dataNascimentoFundacao)
    {
        this._dataNascimentoFundacao = dataNascimentoFundacao;
    } //-- void setDataNascimentoFundacao(java.lang.String) 

    /**
     * Sets the value of field 'filial'.
     * 
     * @param filial the value of field 'filial'.
     */
    public void setFilial(int filial)
    {
        this._filial = filial;
        this._has_filial = true;
    } //-- void setFilial(int) 

    /**
     * Sets the value of field 'nomeRazao'.
     * 
     * @param nomeRazao the value of field 'nomeRazao'.
     */
    public void setNomeRazao(java.lang.String nomeRazao)
    {
        this._nomeRazao = nomeRazao;
    } //-- void setNomeRazao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarClientesRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request.ListarClientesRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request.ListarClientesRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request.ListarClientesRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request.ListarClientesRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
