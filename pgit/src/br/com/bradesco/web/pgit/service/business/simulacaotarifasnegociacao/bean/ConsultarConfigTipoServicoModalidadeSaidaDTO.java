/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.response.ConsultarConfigTipoServicoModalidadeResponse;

/**
 * Nome: ConsultarConfigTipoServicoModalidadeSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConfigTipoServicoModalidadeSaidaDTO {

    private String codMensagem;

    private String mensagem;

    private Integer cdAcaoNaoVida;

    private String dsAcaoNaoVida;

    private Integer cdAcertoDadosRecadastramento;

    private String dsAcertoDadosRecadastramento;

    private Integer cdAgendamentoDebitoVeiculo;

    private String dsAgendamentoDebitoVeiculo;

    private Integer cdAgendamentoPagamentoVencido;

    private String dsAgendamentoPagamentoVencido;

    private Integer cdAgendamentoValorMenor;

    private String dsAgendamentoValorMenor;

    private Integer cdAgrupamentoAviso;

    private String dsAgrupamentoAviso;

    private Integer cdAgrupamentoComprovante;

    private String dsAgrupamentoComprovante;

    private Integer cdAgrupamentoFormularioRecadastro;

    private String dsAgrupamentoFormularioRecadastro;

    private Integer cdAntecipacaoRecadastramentoBeneficiario;

    private String dsAntecipacaoRecadastramentoBeneficiario;

    private Integer cdAreaReservada;

    private String dsAreaReservada;

    private Integer cdBaseRecadastramentoBeneficio;

    private String dsBaseRecadastramentoBeneficio;

    private Integer cdBloqueioEmissaoPplta;

    private String dsBloqueioEmissaoPapeleta;

    private Integer cdCaptuTituloRegistro;

    private String dsCapturaTituloRegistrado;

    private Integer cdCobrancaTarifa;

    private String dsCobrancaTarifa;

    private Integer cdConsultaDebitoVeiculo;

    private String dsConsultaDebitoVeiculo;

    private Integer cdConsultaEndereco;

    private String dsConsultaEndereco;

    private Integer cdConsultaSaldoPagamento;

    private String dsConsultaSaldoPagamento;

    private Integer cdContagemConsultaSaldo;

    private String dsContagemConsultaSaldo;

    private Integer cdCreditoNaoUtilizado;

    private String dsCreditoNaoUtilizado;

    private Integer cdCriterioEnquandraBeneficio;

    private String dsCriterioEnquandraBeneficio;

    private Integer cdCriterioEnquadraRecadastramento;

    private String dsCriterioEnquadraRecadastramento;

    private Integer cdCriterioRstrbTitulo;

    private String dsCriterioRastreabilidadeTitulo;

    private Integer cdCctciaEspeBeneficio;

    private String dsCctciaEspeBeneficio;

    private Integer cdCctciaIdentificacaoBeneficio;

    private String dsCctciaIdentificacaoBeneficio;

    private Integer cdCctciaInscricaoFavorecido;

    private String dsCctciaInscricaoFavorecido;

    private Integer cdCctciaProprietarioVeculo;

    private String dsCctciaProprietarioVeculo;

    private Integer cdDisponibilizacaoContaCredito;

    private String dsDisponibilizacaoContaCredito;

    private Integer cdDisponibilizacaoDiversoCriterio;

    private String dsDisponibilizacaoDiversoCriterio;

    private Integer cdDisponibilizacaoDiversoNao;

    private String dsDisponibilizacaoDiversoNao;

    private Integer cdDisponibilizacaoSalarioCriterio;

    private String dsDisponibilizacaoSalarioCriterio;

    private Integer cdDisponibilizacaoSalarioNao;

    private String dsDisponibilizacaoSalarioNao;

    private Integer cdDestinoAviso;

    private String dsDestinoAviso;

    private Integer cdDestinoComprovante;

    private String dsDestinoComprovante;

    private Integer cdDestinoFormularioRecadastramento;

    private String dsDestinoFormularioRecadastramento;

    private Integer cdEnvelopeAberto;

    private String dsEnvelopeAberto;

    private Integer cdFavorecidoConsultaPagamento;

    private String dsFavorecidoConsultaPagamento;

    private Integer cdFormaAutorizacaoPagamento;

    private String dsFormaAutorizacaoPagamento;

    private Integer cdFormaEnvioPagamento;

    private String dsFormaEnvioPagamento;

    private Integer cdFormaExpiracaoCredito;

    private String dsFormaExpiracaoCredito;

    private Integer cdFormaManutencao;

    private String dsFormaManutencao;

    private Integer cdFrasePrecadastrada;

    private String dsFrasePrecadastrada;

    private Integer cdIndicadorAgendaTitulo;

    private String dsIndicadorAgendamentoTitulo;

    private Integer cdIndicadorAutorizacaoCliente;

    private String dsIndicadorAutorizacaoCliente;

    private Integer cdIndicadorAutorizacaoComplemento;

    private String dsIndicadorAutorizacaoComplemento;

    private Integer cdIndicadorCadastroorganizacao;

    private String dsIndicadorCadastroorganizacao;

    private Integer cdIndicadorCadastroProcurador;

    private String dsIndicadorCadastroProcurador;

    private Integer cdIndicadorCartaoSalario;

    private String dsIndicadorCartaoSalario;

    private Integer cdIndicadorEconomicoReajuste;

    private String dsIndicadorEconomicoReajuste;

    private Integer cdindicadorExpiraCredito;

    private String dsindicadorExpiraCredito;

    private Integer cdindicadorLancamentoProgramado;

    private String dsindicadorLancamentoProgramado;

    private Integer cdIndicadorMensagemPersonalizada;

    private String dsIndicadorMensagemPersonalizada;

    private Integer cdLancamentoFuturoCredito;

    private String dsLancamentoFuturoCredito;

    private Integer cdLancamentoFuturoDebito;

    private String dsLancamentoFuturoDebito;

    private Integer cdLiberacaoLoteProcs;

    private String dsLiberacaoLoteProcessado;

    private Integer cdManutencaoBaseRecadastramento;

    private String dsManutencaoBaseRecadastramento;

    private Integer cdMidiaDisponivel;

    private String dsMidiaDisponivel;

    private Integer cdMidiaMensagemRecadastramento;

    private String dsMidiaMensagemRecadastramento;

    private Integer cdMomentoAvisoRecadastramento;

    private String dsMomentoAvisoRecadastramento;

    private Integer cdMomentoCreditoEfetivacao;

    private String dsMomentoCreditoEfetivacao;

    private Integer cdMomentoDebitoPagamento;

    private String dsMomentoDebitoPagamento;

    private Integer cdMomentoFormularioRecadastramento;

    private String dsMomentoFormularioRecadastramento;

    private Integer cdMomentoProcessamentoPagamento;

    private String dsMomentoProcessamentoPagamento;

    private Integer cdMensagemRecadastramentoMidia;

    private String dsMensagemRecadastramentoMidia;

    private Integer cdPermissaoDebitoOnline;

    private String dsPermissaoDebitoOnline;

    private Integer cdNaturezaOperacaoPagamento;

    private String dsNaturezaOperacaoPagamento;

    private Integer cdPeriodicidadeAviso;

    private String dsPeriodicidadeAviso;

    private Integer cdPeriodicidadeCobrancaTarifa;

    private String dsPeriodicidadeCobrancaTarifa;

    private Integer cdPeriodicidadeComprovante;

    private String dsPeriodicidadeComprovante;

    private Integer cdPeriodicidadeConsultaVeiculo;

    private String dsPeriodicidadeConsultaVeiculo;

    private Integer cdPeriodicidadeEnvioRemessa;

    private String dsPeriodicidadeEnvioRemessa;

    private Integer cdPeriodicidadeManutencaoProcd;

    private String dsPeriodicidadeManutencaoProcd;

    private Integer cdPagamentoNaoUtil;

    private String dsPagamentoNaoUtil;

    private Integer cdPrincipalEnquaRecadastramento;

    private String dsPrincipalEnquaRecadastramento;

    private Integer cdPrioridadeEfetivacaoPagamento;

    private String dsPrioridadeEfetivacaoPagamento;

    private Integer cdRejeicaoAgendamentoLote;

    private String dsRejeicaoAgendamentoLote;

    private Integer cdRejeicaoEfetivacaoLote;

    private String dsRejeicaoEfetivacaoLote;

    private Integer cdRejeicaoLote;

    private String dsRejeicaoLote;

    private Integer cdRastreabNotaFiscal;

    private String dsRastreabilidadeNotaFiscal;

    private Integer cdRastreabTituloTerc;

    private String dsRastreabilidadeTituloTerceiro;

    private Integer cdTipoCargaRecadastramento;

    private String dsTipoCargaRecadastramento;

    private Integer cdTipoCartaoSalario;

    private String dsTipoCartaoSalario;

    private Integer cdTipoDataFloating;

    private String dsTipoDataFloating;

    private Integer cdTipoDivergenciaVeiculo;

    private String dsTipoDivergenciaVeiculo;

    private Integer cdTipoIdentificacaoBeneficio;

    private String dsTipoIdentificacaoBeneficio;

    private Integer cdTipoLayoutArquivo;

    private String dsTipoLayoutArquivo;

    private Integer cdTipoReajusteTarifa;

    private String dsTipoReajusteTarifa;

    private Integer cdTratoContaTransf;

    private String dsTratamentoContaTransferida;

    private Integer cdUtilizacaoFavorecidoControle;

    private String dsUtilizacaoFavorecidoControle;

    private String dtEnquaContaSalario;

    private String dtInicioBloqueioPplta;

    private String dtInicioRastreabTitulo;

    private String dtFimAcertoRecadastramento;

    private String dtFimRecadastramentoBeneficio;

    private String dtInicioAcertoRecadastramento;

    private String dtInicioRecadastramentoBeneficio;

    private String dtLimiteVinculoCarga;

    private Integer nrFechamentoApuracaoTarifa;

    private BigDecimal percentualIndiceReajusteTarifa;

    private Integer percentualMaximoInconsistenteLote;

    private BigDecimal periodicidadeTarifaCatalogo;

    private Integer quantidadeAntecedencia;

    private Integer quantidadeAnteriorVencimentoComprovante;

    private Integer quantidadeDiaCobrancaTarifa;

    private Integer quantidadeDiaExpiracao;

    private Integer quantidadeDiaFloatingPagamento;

    private Integer quantidadeDiaInatividadeFavorecido;

    private Integer quantidadeDiaRepiqueConsulta;

    private Integer quantidadeEtapaRecadastramentoBeneficio;

    private Integer quantidadeFaseRecadastramentoBeneficio;

    private Integer quantidadeLimiteLinha;

    private Integer quantidadeSolicitacaoCartao;

    private Integer quantidadeMaximaInconsistenteLote;

    private Integer quantidadeMaximaTituloVencido;

    private Integer quantidadeMesComprovante;

    private Integer quantidadeMesEtapaRecadastramento;

    private Integer quantidadeMesFaseRecadastramento;

    private Integer quantidadeMesReajusteTarifa;

    private Integer quantidadeViaAviso;

    private Integer quantidadeViaCombranca;

    private Integer quantidadeViaComprovante;

    private BigDecimal valorFavorecidoNaoCadastrado;

    private BigDecimal valorLimiteDiarioPagamento;

    private BigDecimal valorLimiteIndividualPagamento;

    private Integer cdIndicadorEmissaoAviso;

    private String dsIndicadorEmissaoAviso;

    private Integer cdIndicadorRetornoInternet;

    private String dsIndicadorRetornoInternet;

    private Integer cdIndicadorRetornoSeparado;

    private String dsCodigoIndicadorRetornoSeparado;

    private Integer cdIndicadorListaDebito;

    private String dsIndicadorListaDebito;

    private Integer cdTipoFormacaoLista;

    private String dsTipoFormacaoLista;

    private Integer cdTipoConsistenciaLista;

    private String dsTipoConsistenciaLista;

    private Integer cdTipoConsultaComprovante;

    private String dsTipoConsolidacaoComprovante;

    private Integer cdValidacaoNomeFavorecido;

    private String dsValidacaoNomeFavorecido;

    private Integer cdTipoConsistenciaFavorecido;

    private String dsTipoConsistenciaFavorecido;

    private Integer cdIndLancamentoPersonalizado;

    private String dsIndLancamentoPersonalizado;

    private Integer cdAgendaRastreabilidadeFilial;

    private String dsAgendamentoRastFilial;

    private Integer cdIndicadorAdesaoSacador;

    private String dsIndicadorAdesaoSacador;

    private Integer cdMeioPagamentoCredito;

    private String dsMeioPagamentoCredito;

    private Integer cdTipoInscricaoFavorecidoAceita;

    private String dsTipoIscricaoFavorecido;

    private Integer cdIndicadorBancoPostal;

    private String dsIndicadorBancoPostal;

    private Integer cdFormularioContratoCliente;

    private String dsCodigoFormularioContratoCliente;

    private String dsLocalEmissao;

    private String cdUsuarioInclusao;

    private String cdUsuarioExternoInclusao;

    private String hrManutencaoRegistroInclusao;

    private Integer cdCanalInclusao;

    private String dsCanalInclusao;

    private String nrOperacaoFluxoInclusao;

    private String cdUsuarioAlteracao;

    private String cdUsuarioExternoAlteracao;

    private String hrManutencaoRegistroAlteracao;

    private Integer cdCanalAlteracao;

    private String dsCanalAlteracao;

    private String nrOperacaoFluxoAlteracao;

    private String cdAmbienteServicoContrato;

    private String dsAreaReservada2;

    private Integer cdConsultaSaldoValorSuperior;

    private String dsConsultaSaldoSuperior;
    
    

    public ConsultarConfigTipoServicoModalidadeSaidaDTO() {
    	super();
    }
    
    public ConsultarConfigTipoServicoModalidadeSaidaDTO(ConsultarConfigTipoServicoModalidadeResponse response) {
		super();
		this.codMensagem = response.getCodMensagem();
		this.mensagem = response.getMensagem();
		this.cdAcaoNaoVida = response.getCdAcaoNaoVida();
		this.dsAcaoNaoVida = response.getDsAcaoNaoVida();
		this.cdAcertoDadosRecadastramento = response.getCdAcertoDadosRecadastramento();
		this.dsAcertoDadosRecadastramento = response.getDsAcertoDadosRecadastramento();
		this.cdAgendamentoDebitoVeiculo = response.getCdAgendamentoDebitoVeiculo();
		this.dsAgendamentoDebitoVeiculo = response.getDsAgendamentoDebitoVeiculo();
		this.cdAgendamentoPagamentoVencido = response.getCdAgendamentoPagamentoVencido();
		this.dsAgendamentoPagamentoVencido = response.getDsAgendamentoPagamentoVencido();
		this.cdAgendamentoValorMenor = response.getCdAgendamentoValorMenor();
		this.dsAgendamentoValorMenor = response.getDsAgendamentoValorMenor();
		this.cdAgrupamentoAviso = response.getCdAgrupamentoAviso();
		this.dsAgrupamentoAviso = response.getDsAgrupamentoAviso();
		this.cdAgrupamentoComprovante = response.getCdAgrupamentoComprovante();
		this.dsAgrupamentoComprovante = response.getDsAgrupamentoComprovante();
		this.cdAgrupamentoFormularioRecadastro = response.getCdAgrupamentoFormularioRecadastro();
		this.dsAgrupamentoFormularioRecadastro = response.getDsAgrupamentoFormularioRecadastro();
		this.cdAntecipacaoRecadastramentoBeneficiario = response.getCdAntecipacaoRecadastramentoBeneficiario();
		this.dsAntecipacaoRecadastramentoBeneficiario = response.getDsAntecipacaoRecadastramentoBeneficiario();
		this.cdAreaReservada = response.getCdAreaReservada();
		this.dsAreaReservada = response.getDsAreaReservada();
		this.cdBaseRecadastramentoBeneficio = response.getCdBaseRecadastramentoBeneficio();
		this.dsBaseRecadastramentoBeneficio = response.getDsBaseRecadastramentoBeneficio();
		this.cdBloqueioEmissaoPplta = response.getCdBloqueioEmissaoPplta();
		this.dsBloqueioEmissaoPapeleta = response.getDsBloqueioEmissaoPapeleta();
		this.cdCaptuTituloRegistro = response.getCdCaptuTituloRegistro();
		this.dsCapturaTituloRegistrado = response.getDsCapturaTituloRegistrado();
		this.cdCobrancaTarifa = response.getCdCobrancaTarifa();
		this.dsCobrancaTarifa = response.getDsCobrancaTarifa();
		this.cdConsultaDebitoVeiculo = response.getCdConsultaDebitoVeiculo();
		this.dsConsultaDebitoVeiculo = response.getDsConsultaDebitoVeiculo();
		this.cdConsultaEndereco = response.getCdConsultaEndereco();
		this.dsConsultaEndereco = response.getDsConsultaEndereco();
		this.cdConsultaSaldoPagamento = response.getCdConsultaSaldoPagamento();
		this.dsConsultaSaldoPagamento = response.getDsConsultaSaldoPagamento();
		this.cdContagemConsultaSaldo = response.getCdContagemConsultaSaldo();
		this.dsContagemConsultaSaldo = response.getDsContagemConsultaSaldo();
		this.cdCreditoNaoUtilizado = response.getCdCreditoNaoUtilizado();
		this.dsCreditoNaoUtilizado = response.getDsCreditoNaoUtilizado();
		this.cdCriterioEnquandraBeneficio = response.getCdCriterioEnquandraBeneficio();
		this.dsCriterioEnquandraBeneficio = response.getDsCriterioEnquandraBeneficio();
		this.cdCriterioEnquadraRecadastramento = response.getCdCriterioEnquadraRecadastramento();
		this.dsCriterioEnquadraRecadastramento = response.getDsCriterioEnquadraRecadastramento();
		this.cdCriterioRstrbTitulo = response.getCdCriterioRstrbTitulo();
		this.dsCriterioRastreabilidadeTitulo = response.getDsCriterioRastreabilidadeTitulo();
		this.cdCctciaEspeBeneficio = response.getCdCctciaEspeBeneficio();
		this.dsCctciaEspeBeneficio = response.getDsCctciaEspeBeneficio();
		this.cdCctciaIdentificacaoBeneficio = response.getCdCctciaIdentificacaoBeneficio();
		this.dsCctciaIdentificacaoBeneficio = response.getDsCctciaIdentificacaoBeneficio();
		this.cdCctciaInscricaoFavorecido = response.getCdCctciaInscricaoFavorecido();
		this.dsCctciaInscricaoFavorecido = response.getDsCctciaInscricaoFavorecido();
		this.cdCctciaProprietarioVeculo = response.getCdCctciaProprietarioVeculo();
		this.dsCctciaProprietarioVeculo = response.getDsCctciaProprietarioVeculo();
		this.cdDisponibilizacaoContaCredito = response.getCdDisponibilizacaoContaCredito();
		this.dsDisponibilizacaoContaCredito = response.getDsDisponibilizacaoContaCredito();
		this.cdDisponibilizacaoDiversoCriterio = response.getCdDisponibilizacaoDiversoCriterio();
		this.dsDisponibilizacaoDiversoCriterio = response.getDsDisponibilizacaoDiversoCriterio();
		this.cdDisponibilizacaoDiversoNao = response.getCdDisponibilizacaoDiversoNao();
		this.dsDisponibilizacaoDiversoNao = response.getDsDisponibilizacaoDiversoNao();
		this.cdDisponibilizacaoSalarioCriterio = response.getCdDisponibilizacaoSalarioCriterio();
		this.dsDisponibilizacaoSalarioCriterio = response.getDsDisponibilizacaoSalarioCriterio();
		this.cdDisponibilizacaoSalarioNao = response.getCdDisponibilizacaoSalarioNao();
		this.dsDisponibilizacaoSalarioNao = response.getDsDisponibilizacaoSalarioNao();
		this.cdDestinoAviso = response.getCdDestinoAviso();
		this.dsDestinoAviso = response.getDsDestinoAviso();
		this.cdDestinoComprovante = response.getCdDestinoComprovante();
		this.dsDestinoComprovante = response.getDsDestinoComprovante();
		this.cdDestinoFormularioRecadastramento = response.getCdDestinoFormularioRecadastramento();
		this.dsDestinoFormularioRecadastramento = response.getDsDestinoFormularioRecadastramento();
		this.cdEnvelopeAberto = response.getCdEnvelopeAberto();
		this.dsEnvelopeAberto = response.getDsEnvelopeAberto();
		this.cdFavorecidoConsultaPagamento = response.getCdFavorecidoConsultaPagamento();
		this.dsFavorecidoConsultaPagamento = response.getDsFavorecidoConsultaPagamento();
		this.cdFormaAutorizacaoPagamento = response.getCdFormaAutorizacaoPagamento();
		this.dsFormaAutorizacaoPagamento = response.getDsFormaAutorizacaoPagamento();
		this.cdFormaEnvioPagamento = response.getCdFormaEnvioPagamento();
		this.dsFormaEnvioPagamento = response.getDsFormaEnvioPagamento();
		this.cdFormaExpiracaoCredito = response.getCdFormaExpiracaoCredito();
		this.dsFormaExpiracaoCredito = response.getDsFormaExpiracaoCredito();
		this.cdFormaManutencao = response.getCdFormaManutencao();
		this.dsFormaManutencao = response.getDsFormaManutencao();
		this.cdFrasePrecadastrada = response.getCdFrasePrecadastrada();
		this.dsFrasePrecadastrada = response.getDsFrasePrecadastrada();
		this.cdIndicadorAgendaTitulo = response.getCdIndicadorAgendaTitulo();
		this.dsIndicadorAgendamentoTitulo = response.getDsIndicadorAgendamentoTitulo();
		this.cdIndicadorAutorizacaoCliente = response.getCdIndicadorAutorizacaoCliente();
		this.dsIndicadorAutorizacaoCliente = response.getDsIndicadorAutorizacaoCliente();
		this.cdIndicadorAutorizacaoComplemento = response.getCdIndicadorAutorizacaoComplemento();
		this.dsIndicadorAutorizacaoComplemento = response.getDsIndicadorAutorizacaoComplemento();
		this.cdIndicadorCadastroorganizacao = response.getCdIndicadorCadastroorganizacao();
		this.dsIndicadorCadastroorganizacao = response.getDsIndicadorCadastroorganizacao();
		this.cdIndicadorCadastroProcurador = response.getCdIndicadorCadastroProcurador();
		this.dsIndicadorCadastroProcurador = response.getDsIndicadorCadastroProcurador();
		this.cdIndicadorCartaoSalario = response.getCdIndicadorCartaoSalario();
		this.dsIndicadorCartaoSalario = response.getDsIndicadorCartaoSalario();
		this.cdIndicadorEconomicoReajuste = response.getCdIndicadorEconomicoReajuste();
		this.dsIndicadorEconomicoReajuste = response.getDsIndicadorEconomicoReajuste();
		this.cdindicadorExpiraCredito = response.getCdindicadorExpiraCredito();
		this.dsindicadorExpiraCredito = response.getDsindicadorExpiraCredito();
		this.cdindicadorLancamentoProgramado = response.getCdindicadorLancamentoProgramado();
		this.dsindicadorLancamentoProgramado = response.getDsindicadorLancamentoProgramado();
		this.cdIndicadorMensagemPersonalizada = response.getCdIndicadorMensagemPersonalizada();
		this.dsIndicadorMensagemPersonalizada = response.getDsIndicadorMensagemPersonalizada();
		this.cdLancamentoFuturoCredito = response.getCdLancamentoFuturoCredito();
		this.dsLancamentoFuturoCredito = response.getDsLancamentoFuturoCredito();
		this.cdLancamentoFuturoDebito = response.getCdLancamentoFuturoDebito();
		this.dsLancamentoFuturoDebito = response.getDsLancamentoFuturoDebito();
		this.cdLiberacaoLoteProcs = response.getCdLiberacaoLoteProcs();
		this.dsLiberacaoLoteProcessado = response.getDsLiberacaoLoteProcessado();
		this.cdManutencaoBaseRecadastramento = response.getCdManutencaoBaseRecadastramento();
		this.dsManutencaoBaseRecadastramento = response.getDsManutencaoBaseRecadastramento();
		this.cdMidiaDisponivel = response.getCdMidiaDisponivel();
		this.dsMidiaDisponivel = response.getDsMidiaDisponivel();
		this.cdMidiaMensagemRecadastramento = response.getCdMidiaMensagemRecadastramento();
		this.dsMidiaMensagemRecadastramento = response.getDsMidiaMensagemRecadastramento();
		this.cdMomentoAvisoRecadastramento = response.getCdMomentoAvisoRecadastramento();
		this.dsMomentoAvisoRecadastramento = response.getDsMomentoAvisoRecadastramento();
		this.cdMomentoCreditoEfetivacao = response.getCdMomentoCreditoEfetivacao();
		this.dsMomentoCreditoEfetivacao = response.getDsMomentoCreditoEfetivacao();
		this.cdMomentoDebitoPagamento = response.getCdMomentoDebitoPagamento();
		this.dsMomentoDebitoPagamento = response.getDsMomentoDebitoPagamento();
		this.cdMomentoFormularioRecadastramento = response.getCdMomentoFormularioRecadastramento();
		this.dsMomentoFormularioRecadastramento = response.getDsMomentoFormularioRecadastramento();
		this.cdMomentoProcessamentoPagamento = response.getCdMomentoProcessamentoPagamento();
		this.dsMomentoProcessamentoPagamento = response.getDsMomentoProcessamentoPagamento();
		this.cdMensagemRecadastramentoMidia = response.getCdMensagemRecadastramentoMidia();
		this.dsMensagemRecadastramentoMidia = response.getDsMensagemRecadastramentoMidia();
		this.cdPermissaoDebitoOnline = response.getCdPermissaoDebitoOnline();
		this.dsPermissaoDebitoOnline = response.getDsPermissaoDebitoOnline();
		this.cdNaturezaOperacaoPagamento = response.getCdNaturezaOperacaoPagamento();
		this.dsNaturezaOperacaoPagamento = response.getDsNaturezaOperacaoPagamento();
		this.cdPeriodicidadeAviso = response.getCdPeriodicidadeAviso();
		this.dsPeriodicidadeAviso = response.getDsPeriodicidadeAviso();
		this.cdPeriodicidadeCobrancaTarifa = response.getCdPeriodicidadeCobrancaTarifa();
		this.dsPeriodicidadeCobrancaTarifa = response.getDsPeriodicidadeCobrancaTarifa();
		this.cdPeriodicidadeComprovante = response.getCdPeriodicidadeComprovante();
		this.dsPeriodicidadeComprovante = response.getDsPeriodicidadeComprovante();
		this.cdPeriodicidadeConsultaVeiculo = response.getCdPeriodicidadeConsultaVeiculo();
		this.dsPeriodicidadeConsultaVeiculo = response.getDsPeriodicidadeConsultaVeiculo();
		this.cdPeriodicidadeEnvioRemessa = response.getCdPeriodicidadeEnvioRemessa();
		this.dsPeriodicidadeEnvioRemessa = response.getDsPeriodicidadeEnvioRemessa();
		this.cdPeriodicidadeManutencaoProcd = response.getCdPeriodicidadeManutencaoProcd();
		this.dsPeriodicidadeManutencaoProcd = response.getDsPeriodicidadeManutencaoProcd();
		this.cdPagamentoNaoUtil = response.getCdPagamentoNaoUtil();
		this.dsPagamentoNaoUtil = response.getDsPagamentoNaoUtil();
		this.cdPrincipalEnquaRecadastramento = response.getCdPrincipalEnquaRecadastramento();
		this.dsPrincipalEnquaRecadastramento = response.getDsPrincipalEnquaRecadastramento();
		this.cdPrioridadeEfetivacaoPagamento = response.getCdPrioridadeEfetivacaoPagamento();
		this.dsPrioridadeEfetivacaoPagamento = response.getDsPrioridadeEfetivacaoPagamento();
		this.cdRejeicaoAgendamentoLote = response.getCdRejeicaoAgendamentoLote();
		this.dsRejeicaoAgendamentoLote = response.getDsRejeicaoAgendamentoLote();
		this.cdRejeicaoEfetivacaoLote = response.getCdRejeicaoEfetivacaoLote();
		this.dsRejeicaoEfetivacaoLote = response.getDsRejeicaoEfetivacaoLote();
		this.cdRejeicaoLote = response.getCdRejeicaoLote();
		this.dsRejeicaoLote = response.getDsRejeicaoLote();
		this.cdRastreabNotaFiscal = response.getCdRastreabNotaFiscal();
		this.dsRastreabilidadeNotaFiscal = response.getDsRastreabilidadeNotaFiscal();
		this.cdRastreabTituloTerc = response.getCdRastreabTituloTerc();
		this.dsRastreabilidadeTituloTerceiro = response.getDsRastreabilidadeTituloTerceiro();
		this.cdTipoCargaRecadastramento = response.getCdTipoCargaRecadastramento();
		this.dsTipoCargaRecadastramento = response.getDsTipoCargaRecadastramento();
		this.cdTipoCartaoSalario = response.getCdTipoCartaoSalario();
		this.dsTipoCartaoSalario = response.getDsTipoCartaoSalario();
		this.cdTipoDataFloating = response.getCdTipoDataFloating();
		this.dsTipoDataFloating = response.getDsTipoDataFloating();
		this.cdTipoDivergenciaVeiculo = response.getCdTipoDivergenciaVeiculo();
		this.dsTipoDivergenciaVeiculo = response.getDsTipoDivergenciaVeiculo();
		this.cdTipoIdentificacaoBeneficio = response.getCdTipoIdentificacaoBeneficio();
		this.dsTipoIdentificacaoBeneficio = response.getDsTipoIdentificacaoBeneficio();
		this.cdTipoLayoutArquivo = response.getCdTipoLayoutArquivo();
		this.dsTipoLayoutArquivo = response.getDsTipoLayoutArquivo();
		this.cdTipoReajusteTarifa = response.getCdTipoReajusteTarifa();
		this.dsTipoReajusteTarifa = response.getDsTipoReajusteTarifa();
		this.cdTratoContaTransf = response.getCdTratoContaTransf();
		this.dsTratamentoContaTransferida = response.getDsTratamentoContaTransferida();
		this.cdUtilizacaoFavorecidoControle = response.getCdUtilizacaoFavorecidoControle();
		this.dsUtilizacaoFavorecidoControle = response.getDsUtilizacaoFavorecidoControle();
		this.dtEnquaContaSalario = response.getDtEnquaContaSalario();
		this.dtInicioBloqueioPplta = response.getDtInicioBloqueioPplta();
		this.dtInicioRastreabTitulo = response.getDtInicioRastreabTitulo();
		this.dtFimAcertoRecadastramento = response.getDtFimAcertoRecadastramento();
		this.dtFimRecadastramentoBeneficio = response.getDtFimRecadastramentoBeneficio();
		this.dtInicioAcertoRecadastramento = response.getDtInicioAcertoRecadastramento();
		this.dtInicioRecadastramentoBeneficio = response.getDtInicioRecadastramentoBeneficio();
		this.dtLimiteVinculoCarga = response.getDtLimiteVinculoCarga();
		this.nrFechamentoApuracaoTarifa = response.getNrFechamentoApuracaoTarifa();
		this.percentualIndiceReajusteTarifa = response.getPercentualIndiceReajusteTarifa();
		this.percentualMaximoInconsistenteLote = response.getPercentualMaximoInconsistenteLote();
		this.periodicidadeTarifaCatalogo = response.getPeriodicidadeTarifaCatalogo();
		this.quantidadeAntecedencia = response.getQuantidadeAntecedencia();
		this.quantidadeAnteriorVencimentoComprovante = response.getQuantidadeAnteriorVencimentoComprovante();
		this.quantidadeDiaCobrancaTarifa = response.getQuantidadeDiaCobrancaTarifa();
		this.quantidadeDiaExpiracao = response.getQuantidadeDiaExpiracao();
		this.quantidadeDiaFloatingPagamento = response.getQuantidadeDiaFloatingPagamento();
		this.quantidadeDiaInatividadeFavorecido = response.getQuantidadeDiaInatividadeFavorecido();
		this.quantidadeDiaRepiqueConsulta = response.getQuantidadeDiaRepiqueConsulta();
		this.quantidadeEtapaRecadastramentoBeneficio = response.getQuantidadeEtapaRecadastramentoBeneficio();
		this.quantidadeFaseRecadastramentoBeneficio = response.getQuantidadeFaseRecadastramentoBeneficio();
		this.quantidadeLimiteLinha = response.getQuantidadeLimiteLinha();
		this.quantidadeSolicitacaoCartao = response.getQuantidadeSolicitacaoCartao();
		this.quantidadeMaximaInconsistenteLote = response.getQuantidadeMaximaInconsistenteLote();
		this.quantidadeMaximaTituloVencido = response.getQuantidadeMaximaTituloVencido();
		this.quantidadeMesComprovante = response.getQuantidadeMesComprovante();
		this.quantidadeMesEtapaRecadastramento = response.getQuantidadeMesEtapaRecadastramento();
		this.quantidadeMesFaseRecadastramento = response.getQuantidadeMesFaseRecadastramento();
		this.quantidadeMesReajusteTarifa = response.getQuantidadeMesReajusteTarifa();
		this.quantidadeViaAviso = response.getQuantidadeViaAviso();
		this.quantidadeViaCombranca = response.getQuantidadeViaCombranca();
		this.quantidadeViaComprovante = response.getQuantidadeViaComprovante();
		this.valorFavorecidoNaoCadastrado = response.getValorFavorecidoNaoCadastrado();
		this.valorLimiteDiarioPagamento = response.getValorLimiteDiarioPagamento();
		this.valorLimiteIndividualPagamento = response.getValorLimiteIndividualPagamento();
		this.cdIndicadorEmissaoAviso = response.getCdIndicadorEmissaoAviso();
		this.dsIndicadorEmissaoAviso = response.getDsIndicadorEmissaoAviso();
		this.cdIndicadorRetornoInternet = response.getCdIndicadorRetornoInternet();
		this.dsIndicadorRetornoInternet = response.getDsIndicadorRetornoInternet();
		this.cdIndicadorRetornoSeparado = response.getCdIndicadorRetornoSeparado();
		this.dsCodigoIndicadorRetornoSeparado = response.getDsCodigoIndicadorRetornoSeparado();
		this.cdIndicadorListaDebito = response.getCdIndicadorListaDebito();
		this.dsIndicadorListaDebito = response.getDsIndicadorListaDebito();
		this.cdTipoFormacaoLista = response.getCdTipoFormacaoLista();
		this.dsTipoFormacaoLista = response.getDsTipoFormacaoLista();
		this.cdTipoConsistenciaLista = response.getCdTipoConsistenciaLista();
		this.dsTipoConsistenciaLista = response.getDsTipoConsistenciaLista();
		this.cdTipoConsultaComprovante = response.getCdTipoConsultaComprovante();
		this.dsTipoConsolidacaoComprovante = response.getDsTipoConsolidacaoComprovante();
		this.cdValidacaoNomeFavorecido = response.getCdValidacaoNomeFavorecido();
		this.dsValidacaoNomeFavorecido = response.getDsValidacaoNomeFavorecido();
		this.cdTipoConsistenciaFavorecido = response.getCdTipoConsistenciaFavorecido();
		this.dsTipoConsistenciaFavorecido = response.getDsTipoConsistenciaFavorecido();
		this.cdIndLancamentoPersonalizado = response.getCdIndLancamentoPersonalizado();
		this.dsIndLancamentoPersonalizado = response.getDsIndLancamentoPersonalizado();
		this.cdAgendaRastreabilidadeFilial = response.getCdAgendaRastreabilidadeFilial();
		this.dsAgendamentoRastFilial = response.getDsAgendamentoRastFilial();
		this.cdIndicadorAdesaoSacador = response.getCdIndicadorAdesaoSacador();
		this.dsIndicadorAdesaoSacador = response.getDsIndicadorAdesaoSacador();
		this.cdMeioPagamentoCredito = response.getCdMeioPagamentoCredito();
		this.dsMeioPagamentoCredito = response.getDsMeioPagamentoCredito();
		this.cdTipoInscricaoFavorecidoAceita = response.getCdTipoInscricaoFavorecidoAceita();
		this.dsTipoIscricaoFavorecido = response.getDsTipoIscricaoFavorecido();
		this.cdIndicadorBancoPostal = response.getCdIndicadorBancoPostal();
		this.dsIndicadorBancoPostal = response.getDsIndicadorBancoPostal();
		this.cdFormularioContratoCliente = response.getCdFormularioContratoCliente();
		this.dsCodigoFormularioContratoCliente = response.getDsCodigoFormularioContratoCliente();
		this.dsLocalEmissao = response.getDsLocalEmissao();
		this.cdUsuarioInclusao = response.getCdUsuarioInclusao();
		this.cdUsuarioExternoInclusao = response.getCdUsuarioExternoInclusao();
		this.hrManutencaoRegistroInclusao = response.getHrManutencaoRegistroInclusao();
		this.cdCanalInclusao = response.getCdCanalInclusao();
		this.dsCanalInclusao = response.getDsCanalInclusao();
		this.nrOperacaoFluxoInclusao = response.getNrOperacaoFluxoInclusao();
		this.cdUsuarioAlteracao = response.getCdUsuarioAlteracao();
		this.cdUsuarioExternoAlteracao = response.getCdUsuarioExternoAlteracao();
		this.hrManutencaoRegistroAlteracao = response.getHrManutencaoRegistroAlteracao();
		this.cdCanalAlteracao = response.getCdCanalAlteracao();
		this.dsCanalAlteracao = response.getDsCanalAlteracao();
		this.nrOperacaoFluxoAlteracao = response.getNrOperacaoFluxoAlteracao();
		this.cdAmbienteServicoContrato = response.getCdAmbienteServicoContrato();
		this.dsAreaReservada2 = response.getDsAreaReservada2();
		this.cdConsultaSaldoValorSuperior = response.getCdConsultaSaldoValorSuperior();
		this.dsConsultaSaldoSuperior = response.getDsConsultaSaldoSuperior();
	}

	/**
	 * Nome: getCodMensagem
	 *
	 * @exception
	 * @throws
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Nome: setCodMensagem
	 *
	 * @exception
	 * @throws
	 * @param codMensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Nome: getMensagem
	 *
	 * @exception
	 * @throws
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Nome: setMensagem
	 *
	 * @exception
	 * @throws
	 * @param mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Nome: getCdAcaoNaoVida
	 *
	 * @exception
	 * @throws
	 * @return cdAcaoNaoVida
	 */
	public Integer getCdAcaoNaoVida() {
		return cdAcaoNaoVida;
	}

	/**
	 * Nome: setCdAcaoNaoVida
	 *
	 * @exception
	 * @throws
	 * @param cdAcaoNaoVida
	 */
	public void setCdAcaoNaoVida(Integer cdAcaoNaoVida) {
		this.cdAcaoNaoVida = cdAcaoNaoVida;
	}

	/**
	 * Nome: getDsAcaoNaoVida
	 *
	 * @exception
	 * @throws
	 * @return dsAcaoNaoVida
	 */
	public String getDsAcaoNaoVida() {
		return dsAcaoNaoVida;
	}

	/**
	 * Nome: setDsAcaoNaoVida
	 *
	 * @exception
	 * @throws
	 * @param dsAcaoNaoVida
	 */
	public void setDsAcaoNaoVida(String dsAcaoNaoVida) {
		this.dsAcaoNaoVida = dsAcaoNaoVida;
	}

	/**
	 * Nome: getCdAcertoDadosRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdAcertoDadosRecadastramento
	 */
	public Integer getCdAcertoDadosRecadastramento() {
		return cdAcertoDadosRecadastramento;
	}

	/**
	 * Nome: setCdAcertoDadosRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdAcertoDadosRecadastramento
	 */
	public void setCdAcertoDadosRecadastramento(Integer cdAcertoDadosRecadastramento) {
		this.cdAcertoDadosRecadastramento = cdAcertoDadosRecadastramento;
	}

	/**
	 * Nome: getDsAcertoDadosRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsAcertoDadosRecadastramento
	 */
	public String getDsAcertoDadosRecadastramento() {
		return dsAcertoDadosRecadastramento;
	}

	/**
	 * Nome: setDsAcertoDadosRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsAcertoDadosRecadastramento
	 */
	public void setDsAcertoDadosRecadastramento(String dsAcertoDadosRecadastramento) {
		this.dsAcertoDadosRecadastramento = dsAcertoDadosRecadastramento;
	}

	/**
	 * Nome: getCdAgendamentoDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdAgendamentoDebitoVeiculo
	 */
	public Integer getCdAgendamentoDebitoVeiculo() {
		return cdAgendamentoDebitoVeiculo;
	}

	/**
	 * Nome: setCdAgendamentoDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdAgendamentoDebitoVeiculo
	 */
	public void setCdAgendamentoDebitoVeiculo(Integer cdAgendamentoDebitoVeiculo) {
		this.cdAgendamentoDebitoVeiculo = cdAgendamentoDebitoVeiculo;
	}

	/**
	 * Nome: getDsAgendamentoDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @return dsAgendamentoDebitoVeiculo
	 */
	public String getDsAgendamentoDebitoVeiculo() {
		return dsAgendamentoDebitoVeiculo;
	}

	/**
	 * Nome: setDsAgendamentoDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @param dsAgendamentoDebitoVeiculo
	 */
	public void setDsAgendamentoDebitoVeiculo(String dsAgendamentoDebitoVeiculo) {
		this.dsAgendamentoDebitoVeiculo = dsAgendamentoDebitoVeiculo;
	}

	/**
	 * Nome: getCdAgendamentoPagamentoVencido
	 *
	 * @exception
	 * @throws
	 * @return cdAgendamentoPagamentoVencido
	 */
	public Integer getCdAgendamentoPagamentoVencido() {
		return cdAgendamentoPagamentoVencido;
	}

	/**
	 * Nome: setCdAgendamentoPagamentoVencido
	 *
	 * @exception
	 * @throws
	 * @param cdAgendamentoPagamentoVencido
	 */
	public void setCdAgendamentoPagamentoVencido(
			Integer cdAgendamentoPagamentoVencido) {
		this.cdAgendamentoPagamentoVencido = cdAgendamentoPagamentoVencido;
	}

	/**
	 * Nome: getDsAgendamentoPagamentoVencido
	 *
	 * @exception
	 * @throws
	 * @return dsAgendamentoPagamentoVencido
	 */
	public String getDsAgendamentoPagamentoVencido() {
		return dsAgendamentoPagamentoVencido;
	}

	/**
	 * Nome: setDsAgendamentoPagamentoVencido
	 *
	 * @exception
	 * @throws
	 * @param dsAgendamentoPagamentoVencido
	 */
	public void setDsAgendamentoPagamentoVencido(
			String dsAgendamentoPagamentoVencido) {
		this.dsAgendamentoPagamentoVencido = dsAgendamentoPagamentoVencido;
	}

	/**
	 * Nome: getCdAgendamentoValorMenor
	 *
	 * @exception
	 * @throws
	 * @return cdAgendamentoValorMenor
	 */
	public Integer getCdAgendamentoValorMenor() {
		return cdAgendamentoValorMenor;
	}

	/**
	 * Nome: setCdAgendamentoValorMenor
	 *
	 * @exception
	 * @throws
	 * @param cdAgendamentoValorMenor
	 */
	public void setCdAgendamentoValorMenor(Integer cdAgendamentoValorMenor) {
		this.cdAgendamentoValorMenor = cdAgendamentoValorMenor;
	}

	/**
	 * Nome: getDsAgendamentoValorMenor
	 *
	 * @exception
	 * @throws
	 * @return dsAgendamentoValorMenor
	 */
	public String getDsAgendamentoValorMenor() {
		return dsAgendamentoValorMenor;
	}

	/**
	 * Nome: setDsAgendamentoValorMenor
	 *
	 * @exception
	 * @throws
	 * @param dsAgendamentoValorMenor
	 */
	public void setDsAgendamentoValorMenor(String dsAgendamentoValorMenor) {
		this.dsAgendamentoValorMenor = dsAgendamentoValorMenor;
	}

	/**
	 * Nome: getCdAgrupamentoAviso
	 *
	 * @exception
	 * @throws
	 * @return cdAgrupamentoAviso
	 */
	public Integer getCdAgrupamentoAviso() {
		return cdAgrupamentoAviso;
	}

	/**
	 * Nome: setCdAgrupamentoAviso
	 *
	 * @exception
	 * @throws
	 * @param cdAgrupamentoAviso
	 */
	public void setCdAgrupamentoAviso(Integer cdAgrupamentoAviso) {
		this.cdAgrupamentoAviso = cdAgrupamentoAviso;
	}

	/**
	 * Nome: getDsAgrupamentoAviso
	 *
	 * @exception
	 * @throws
	 * @return dsAgrupamentoAviso
	 */
	public String getDsAgrupamentoAviso() {
		return dsAgrupamentoAviso;
	}

	/**
	 * Nome: setDsAgrupamentoAviso
	 *
	 * @exception
	 * @throws
	 * @param dsAgrupamentoAviso
	 */
	public void setDsAgrupamentoAviso(String dsAgrupamentoAviso) {
		this.dsAgrupamentoAviso = dsAgrupamentoAviso;
	}

	/**
	 * Nome: getCdAgrupamentoComprovante
	 *
	 * @exception
	 * @throws
	 * @return cdAgrupamentoComprovante
	 */
	public Integer getCdAgrupamentoComprovante() {
		return cdAgrupamentoComprovante;
	}

	/**
	 * Nome: setCdAgrupamentoComprovante
	 *
	 * @exception
	 * @throws
	 * @param cdAgrupamentoComprovante
	 */
	public void setCdAgrupamentoComprovante(Integer cdAgrupamentoComprovante) {
		this.cdAgrupamentoComprovante = cdAgrupamentoComprovante;
	}

	/**
	 * Nome: getDsAgrupamentoComprovante
	 *
	 * @exception
	 * @throws
	 * @return dsAgrupamentoComprovante
	 */
	public String getDsAgrupamentoComprovante() {
		return dsAgrupamentoComprovante;
	}

	/**
	 * Nome: setDsAgrupamentoComprovante
	 *
	 * @exception
	 * @throws
	 * @param dsAgrupamentoComprovante
	 */
	public void setDsAgrupamentoComprovante(String dsAgrupamentoComprovante) {
		this.dsAgrupamentoComprovante = dsAgrupamentoComprovante;
	}

	/**
	 * Nome: getCdAgrupamentoFormularioRecadastro
	 *
	 * @exception
	 * @throws
	 * @return cdAgrupamentoFormularioRecadastro
	 */
	public Integer getCdAgrupamentoFormularioRecadastro() {
		return cdAgrupamentoFormularioRecadastro;
	}

	/**
	 * Nome: setCdAgrupamentoFormularioRecadastro
	 *
	 * @exception
	 * @throws
	 * @param cdAgrupamentoFormularioRecadastro
	 */
	public void setCdAgrupamentoFormularioRecadastro(
			Integer cdAgrupamentoFormularioRecadastro) {
		this.cdAgrupamentoFormularioRecadastro = cdAgrupamentoFormularioRecadastro;
	}

	/**
	 * Nome: getDsAgrupamentoFormularioRecadastro
	 *
	 * @exception
	 * @throws
	 * @return dsAgrupamentoFormularioRecadastro
	 */
	public String getDsAgrupamentoFormularioRecadastro() {
		return dsAgrupamentoFormularioRecadastro;
	}

	/**
	 * Nome: setDsAgrupamentoFormularioRecadastro
	 *
	 * @exception
	 * @throws
	 * @param dsAgrupamentoFormularioRecadastro
	 */
	public void setDsAgrupamentoFormularioRecadastro(
			String dsAgrupamentoFormularioRecadastro) {
		this.dsAgrupamentoFormularioRecadastro = dsAgrupamentoFormularioRecadastro;
	}

	/**
	 * Nome: getCdAntecipacaoRecadastramentoBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return cdAntecipacaoRecadastramentoBeneficiario
	 */
	public Integer getCdAntecipacaoRecadastramentoBeneficiario() {
		return cdAntecipacaoRecadastramentoBeneficiario;
	}

	/**
	 * Nome: setCdAntecipacaoRecadastramentoBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param cdAntecipacaoRecadastramentoBeneficiario
	 */
	public void setCdAntecipacaoRecadastramentoBeneficiario(
			Integer cdAntecipacaoRecadastramentoBeneficiario) {
		this.cdAntecipacaoRecadastramentoBeneficiario = cdAntecipacaoRecadastramentoBeneficiario;
	}

	/**
	 * Nome: getDsAntecipacaoRecadastramentoBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return dsAntecipacaoRecadastramentoBeneficiario
	 */
	public String getDsAntecipacaoRecadastramentoBeneficiario() {
		return dsAntecipacaoRecadastramentoBeneficiario;
	}

	/**
	 * Nome: setDsAntecipacaoRecadastramentoBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param dsAntecipacaoRecadastramentoBeneficiario
	 */
	public void setDsAntecipacaoRecadastramentoBeneficiario(
			String dsAntecipacaoRecadastramentoBeneficiario) {
		this.dsAntecipacaoRecadastramentoBeneficiario = dsAntecipacaoRecadastramentoBeneficiario;
	}

	/**
	 * Nome: getCdAreaReservada
	 *
	 * @exception
	 * @throws
	 * @return cdAreaReservada
	 */
	public Integer getCdAreaReservada() {
		return cdAreaReservada;
	}

	/**
	 * Nome: setCdAreaReservada
	 *
	 * @exception
	 * @throws
	 * @param cdAreaReservada
	 */
	public void setCdAreaReservada(Integer cdAreaReservada) {
		this.cdAreaReservada = cdAreaReservada;
	}

	/**
	 * Nome: getDsAreaReservada
	 *
	 * @exception
	 * @throws
	 * @return dsAreaReservada
	 */
	public String getDsAreaReservada() {
		return dsAreaReservada;
	}

	/**
	 * Nome: setDsAreaReservada
	 *
	 * @exception
	 * @throws
	 * @param dsAreaReservada
	 */
	public void setDsAreaReservada(String dsAreaReservada) {
		this.dsAreaReservada = dsAreaReservada;
	}

	/**
	 * Nome: getCdBaseRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return cdBaseRecadastramentoBeneficio
	 */
	public Integer getCdBaseRecadastramentoBeneficio() {
		return cdBaseRecadastramentoBeneficio;
	}

	/**
	 * Nome: setCdBaseRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param cdBaseRecadastramentoBeneficio
	 */
	public void setCdBaseRecadastramentoBeneficio(
			Integer cdBaseRecadastramentoBeneficio) {
		this.cdBaseRecadastramentoBeneficio = cdBaseRecadastramentoBeneficio;
	}

	/**
	 * Nome: getDsBaseRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return dsBaseRecadastramentoBeneficio
	 */
	public String getDsBaseRecadastramentoBeneficio() {
		return dsBaseRecadastramentoBeneficio;
	}

	/**
	 * Nome: setDsBaseRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param dsBaseRecadastramentoBeneficio
	 */
	public void setDsBaseRecadastramentoBeneficio(
			String dsBaseRecadastramentoBeneficio) {
		this.dsBaseRecadastramentoBeneficio = dsBaseRecadastramentoBeneficio;
	}

	/**
	 * Nome: getCdBloqueioEmissaoPplta
	 *
	 * @exception
	 * @throws
	 * @return cdBloqueioEmissaoPplta
	 */
	public Integer getCdBloqueioEmissaoPplta() {
		return cdBloqueioEmissaoPplta;
	}

	/**
	 * Nome: setCdBloqueioEmissaoPplta
	 *
	 * @exception
	 * @throws
	 * @param cdBloqueioEmissaoPplta
	 */
	public void setCdBloqueioEmissaoPplta(Integer cdBloqueioEmissaoPplta) {
		this.cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
	}

	/**
	 * Nome: getDsBloqueioEmissaoPapeleta
	 *
	 * @exception
	 * @throws
	 * @return dsBloqueioEmissaoPapeleta
	 */
	public String getDsBloqueioEmissaoPapeleta() {
		return dsBloqueioEmissaoPapeleta;
	}

	/**
	 * Nome: setDsBloqueioEmissaoPapeleta
	 *
	 * @exception
	 * @throws
	 * @param dsBloqueioEmissaoPapeleta
	 */
	public void setDsBloqueioEmissaoPapeleta(String dsBloqueioEmissaoPapeleta) {
		this.dsBloqueioEmissaoPapeleta = dsBloqueioEmissaoPapeleta;
	}

	/**
	 * Nome: getCdCaptuTituloRegistro
	 *
	 * @exception
	 * @throws
	 * @return cdCaptuTituloRegistro
	 */
	public Integer getCdCaptuTituloRegistro() {
		return cdCaptuTituloRegistro;
	}

	/**
	 * Nome: setCdCaptuTituloRegistro
	 *
	 * @exception
	 * @throws
	 * @param cdCaptuTituloRegistro
	 */
	public void setCdCaptuTituloRegistro(Integer cdCaptuTituloRegistro) {
		this.cdCaptuTituloRegistro = cdCaptuTituloRegistro;
	}

	/**
	 * Nome: getDsCapturaTituloRegistrado
	 *
	 * @exception
	 * @throws
	 * @return dsCapturaTituloRegistrado
	 */
	public String getDsCapturaTituloRegistrado() {
		return dsCapturaTituloRegistrado;
	}

	/**
	 * Nome: setDsCapturaTituloRegistrado
	 *
	 * @exception
	 * @throws
	 * @param dsCapturaTituloRegistrado
	 */
	public void setDsCapturaTituloRegistrado(String dsCapturaTituloRegistrado) {
		this.dsCapturaTituloRegistrado = dsCapturaTituloRegistrado;
	}

	/**
	 * Nome: getCdCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @return cdCobrancaTarifa
	 */
	public Integer getCdCobrancaTarifa() {
		return cdCobrancaTarifa;
	}

	/**
	 * Nome: setCdCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @param cdCobrancaTarifa
	 */
	public void setCdCobrancaTarifa(Integer cdCobrancaTarifa) {
		this.cdCobrancaTarifa = cdCobrancaTarifa;
	}

	/**
	 * Nome: getDsCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @return dsCobrancaTarifa
	 */
	public String getDsCobrancaTarifa() {
		return dsCobrancaTarifa;
	}

	/**
	 * Nome: setDsCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @param dsCobrancaTarifa
	 */
	public void setDsCobrancaTarifa(String dsCobrancaTarifa) {
		this.dsCobrancaTarifa = dsCobrancaTarifa;
	}

	/**
	 * Nome: getCdConsultaDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdConsultaDebitoVeiculo
	 */
	public Integer getCdConsultaDebitoVeiculo() {
		return cdConsultaDebitoVeiculo;
	}

	/**
	 * Nome: setCdConsultaDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdConsultaDebitoVeiculo
	 */
	public void setCdConsultaDebitoVeiculo(Integer cdConsultaDebitoVeiculo) {
		this.cdConsultaDebitoVeiculo = cdConsultaDebitoVeiculo;
	}

	/**
	 * Nome: getDsConsultaDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @return dsConsultaDebitoVeiculo
	 */
	public String getDsConsultaDebitoVeiculo() {
		return dsConsultaDebitoVeiculo;
	}

	/**
	 * Nome: setDsConsultaDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @param dsConsultaDebitoVeiculo
	 */
	public void setDsConsultaDebitoVeiculo(String dsConsultaDebitoVeiculo) {
		this.dsConsultaDebitoVeiculo = dsConsultaDebitoVeiculo;
	}

	/**
	 * Nome: getCdConsultaEndereco
	 *
	 * @exception
	 * @throws
	 * @return cdConsultaEndereco
	 */
	public Integer getCdConsultaEndereco() {
		return cdConsultaEndereco;
	}

	/**
	 * Nome: setCdConsultaEndereco
	 *
	 * @exception
	 * @throws
	 * @param cdConsultaEndereco
	 */
	public void setCdConsultaEndereco(Integer cdConsultaEndereco) {
		this.cdConsultaEndereco = cdConsultaEndereco;
	}

	/**
	 * Nome: getDsConsultaEndereco
	 *
	 * @exception
	 * @throws
	 * @return dsConsultaEndereco
	 */
	public String getDsConsultaEndereco() {
		return dsConsultaEndereco;
	}

	/**
	 * Nome: setDsConsultaEndereco
	 *
	 * @exception
	 * @throws
	 * @param dsConsultaEndereco
	 */
	public void setDsConsultaEndereco(String dsConsultaEndereco) {
		this.dsConsultaEndereco = dsConsultaEndereco;
	}

	/**
	 * Nome: getCdConsultaSaldoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdConsultaSaldoPagamento
	 */
	public Integer getCdConsultaSaldoPagamento() {
		return cdConsultaSaldoPagamento;
	}

	/**
	 * Nome: setCdConsultaSaldoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdConsultaSaldoPagamento
	 */
	public void setCdConsultaSaldoPagamento(Integer cdConsultaSaldoPagamento) {
		this.cdConsultaSaldoPagamento = cdConsultaSaldoPagamento;
	}

	/**
	 * Nome: getDsConsultaSaldoPagamento
	 *
	 * @exception
	 * @throws
	 * @return dsConsultaSaldoPagamento
	 */
	public String getDsConsultaSaldoPagamento() {
		return dsConsultaSaldoPagamento;
	}

	/**
	 * Nome: setDsConsultaSaldoPagamento
	 *
	 * @exception
	 * @throws
	 * @param dsConsultaSaldoPagamento
	 */
	public void setDsConsultaSaldoPagamento(String dsConsultaSaldoPagamento) {
		this.dsConsultaSaldoPagamento = dsConsultaSaldoPagamento;
	}

	/**
	 * Nome: getCdContagemConsultaSaldo
	 *
	 * @exception
	 * @throws
	 * @return cdContagemConsultaSaldo
	 */
	public Integer getCdContagemConsultaSaldo() {
		return cdContagemConsultaSaldo;
	}

	/**
	 * Nome: setCdContagemConsultaSaldo
	 *
	 * @exception
	 * @throws
	 * @param cdContagemConsultaSaldo
	 */
	public void setCdContagemConsultaSaldo(Integer cdContagemConsultaSaldo) {
		this.cdContagemConsultaSaldo = cdContagemConsultaSaldo;
	}

	/**
	 * Nome: getDsContagemConsultaSaldo
	 *
	 * @exception
	 * @throws
	 * @return dsContagemConsultaSaldo
	 */
	public String getDsContagemConsultaSaldo() {
		return dsContagemConsultaSaldo;
	}

	/**
	 * Nome: setDsContagemConsultaSaldo
	 *
	 * @exception
	 * @throws
	 * @param dsContagemConsultaSaldo
	 */
	public void setDsContagemConsultaSaldo(String dsContagemConsultaSaldo) {
		this.dsContagemConsultaSaldo = dsContagemConsultaSaldo;
	}

	/**
	 * Nome: getCdCreditoNaoUtilizado
	 *
	 * @exception
	 * @throws
	 * @return cdCreditoNaoUtilizado
	 */
	public Integer getCdCreditoNaoUtilizado() {
		return cdCreditoNaoUtilizado;
	}

	/**
	 * Nome: setCdCreditoNaoUtilizado
	 *
	 * @exception
	 * @throws
	 * @param cdCreditoNaoUtilizado
	 */
	public void setCdCreditoNaoUtilizado(Integer cdCreditoNaoUtilizado) {
		this.cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
	}

	/**
	 * Nome: getDsCreditoNaoUtilizado
	 *
	 * @exception
	 * @throws
	 * @return dsCreditoNaoUtilizado
	 */
	public String getDsCreditoNaoUtilizado() {
		return dsCreditoNaoUtilizado;
	}

	/**
	 * Nome: setDsCreditoNaoUtilizado
	 *
	 * @exception
	 * @throws
	 * @param dsCreditoNaoUtilizado
	 */
	public void setDsCreditoNaoUtilizado(String dsCreditoNaoUtilizado) {
		this.dsCreditoNaoUtilizado = dsCreditoNaoUtilizado;
	}

	/**
	 * Nome: getCdCriterioEnquandraBeneficio
	 *
	 * @exception
	 * @throws
	 * @return cdCriterioEnquandraBeneficio
	 */
	public Integer getCdCriterioEnquandraBeneficio() {
		return cdCriterioEnquandraBeneficio;
	}

	/**
	 * Nome: setCdCriterioEnquandraBeneficio
	 *
	 * @exception
	 * @throws
	 * @param cdCriterioEnquandraBeneficio
	 */
	public void setCdCriterioEnquandraBeneficio(Integer cdCriterioEnquandraBeneficio) {
		this.cdCriterioEnquandraBeneficio = cdCriterioEnquandraBeneficio;
	}

	/**
	 * Nome: getDsCriterioEnquandraBeneficio
	 *
	 * @exception
	 * @throws
	 * @return dsCriterioEnquandraBeneficio
	 */
	public String getDsCriterioEnquandraBeneficio() {
		return dsCriterioEnquandraBeneficio;
	}

	/**
	 * Nome: setDsCriterioEnquandraBeneficio
	 *
	 * @exception
	 * @throws
	 * @param dsCriterioEnquandraBeneficio
	 */
	public void setDsCriterioEnquandraBeneficio(String dsCriterioEnquandraBeneficio) {
		this.dsCriterioEnquandraBeneficio = dsCriterioEnquandraBeneficio;
	}

	/**
	 * Nome: getCdCriterioEnquadraRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdCriterioEnquadraRecadastramento
	 */
	public Integer getCdCriterioEnquadraRecadastramento() {
		return cdCriterioEnquadraRecadastramento;
	}

	/**
	 * Nome: setCdCriterioEnquadraRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdCriterioEnquadraRecadastramento
	 */
	public void setCdCriterioEnquadraRecadastramento(
			Integer cdCriterioEnquadraRecadastramento) {
		this.cdCriterioEnquadraRecadastramento = cdCriterioEnquadraRecadastramento;
	}

	/**
	 * Nome: getDsCriterioEnquadraRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsCriterioEnquadraRecadastramento
	 */
	public String getDsCriterioEnquadraRecadastramento() {
		return dsCriterioEnquadraRecadastramento;
	}

	/**
	 * Nome: setDsCriterioEnquadraRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsCriterioEnquadraRecadastramento
	 */
	public void setDsCriterioEnquadraRecadastramento(
			String dsCriterioEnquadraRecadastramento) {
		this.dsCriterioEnquadraRecadastramento = dsCriterioEnquadraRecadastramento;
	}

	/**
	 * Nome: getCdCriterioRstrbTitulo
	 *
	 * @exception
	 * @throws
	 * @return cdCriterioRstrbTitulo
	 */
	public Integer getCdCriterioRstrbTitulo() {
		return cdCriterioRstrbTitulo;
	}

	/**
	 * Nome: setCdCriterioRstrbTitulo
	 *
	 * @exception
	 * @throws
	 * @param cdCriterioRstrbTitulo
	 */
	public void setCdCriterioRstrbTitulo(Integer cdCriterioRstrbTitulo) {
		this.cdCriterioRstrbTitulo = cdCriterioRstrbTitulo;
	}

	/**
	 * Nome: getDsCriterioRastreabilidadeTitulo
	 *
	 * @exception
	 * @throws
	 * @return dsCriterioRastreabilidadeTitulo
	 */
	public String getDsCriterioRastreabilidadeTitulo() {
		return dsCriterioRastreabilidadeTitulo;
	}

	/**
	 * Nome: setDsCriterioRastreabilidadeTitulo
	 *
	 * @exception
	 * @throws
	 * @param dsCriterioRastreabilidadeTitulo
	 */
	public void setDsCriterioRastreabilidadeTitulo(
			String dsCriterioRastreabilidadeTitulo) {
		this.dsCriterioRastreabilidadeTitulo = dsCriterioRastreabilidadeTitulo;
	}

	/**
	 * Nome: getCdCctciaEspeBeneficio
	 *
	 * @exception
	 * @throws
	 * @return cdCctciaEspeBeneficio
	 */
	public Integer getCdCctciaEspeBeneficio() {
		return cdCctciaEspeBeneficio;
	}

	/**
	 * Nome: setCdCctciaEspeBeneficio
	 *
	 * @exception
	 * @throws
	 * @param cdCctciaEspeBeneficio
	 */
	public void setCdCctciaEspeBeneficio(Integer cdCctciaEspeBeneficio) {
		this.cdCctciaEspeBeneficio = cdCctciaEspeBeneficio;
	}

	/**
	 * Nome: getDsCctciaEspeBeneficio
	 *
	 * @exception
	 * @throws
	 * @return dsCctciaEspeBeneficio
	 */
	public String getDsCctciaEspeBeneficio() {
		return dsCctciaEspeBeneficio;
	}

	/**
	 * Nome: setDsCctciaEspeBeneficio
	 *
	 * @exception
	 * @throws
	 * @param dsCctciaEspeBeneficio
	 */
	public void setDsCctciaEspeBeneficio(String dsCctciaEspeBeneficio) {
		this.dsCctciaEspeBeneficio = dsCctciaEspeBeneficio;
	}

	/**
	 * Nome: getCdCctciaIdentificacaoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return cdCctciaIdentificacaoBeneficio
	 */
	public Integer getCdCctciaIdentificacaoBeneficio() {
		return cdCctciaIdentificacaoBeneficio;
	}

	/**
	 * Nome: setCdCctciaIdentificacaoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param cdCctciaIdentificacaoBeneficio
	 */
	public void setCdCctciaIdentificacaoBeneficio(
			Integer cdCctciaIdentificacaoBeneficio) {
		this.cdCctciaIdentificacaoBeneficio = cdCctciaIdentificacaoBeneficio;
	}

	/**
	 * Nome: getDsCctciaIdentificacaoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return dsCctciaIdentificacaoBeneficio
	 */
	public String getDsCctciaIdentificacaoBeneficio() {
		return dsCctciaIdentificacaoBeneficio;
	}

	/**
	 * Nome: setDsCctciaIdentificacaoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param dsCctciaIdentificacaoBeneficio
	 */
	public void setDsCctciaIdentificacaoBeneficio(
			String dsCctciaIdentificacaoBeneficio) {
		this.dsCctciaIdentificacaoBeneficio = dsCctciaIdentificacaoBeneficio;
	}

	/**
	 * Nome: getCdCctciaInscricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdCctciaInscricaoFavorecido
	 */
	public Integer getCdCctciaInscricaoFavorecido() {
		return cdCctciaInscricaoFavorecido;
	}

	/**
	 * Nome: setCdCctciaInscricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdCctciaInscricaoFavorecido
	 */
	public void setCdCctciaInscricaoFavorecido(Integer cdCctciaInscricaoFavorecido) {
		this.cdCctciaInscricaoFavorecido = cdCctciaInscricaoFavorecido;
	}

	/**
	 * Nome: getDsCctciaInscricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @return dsCctciaInscricaoFavorecido
	 */
	public String getDsCctciaInscricaoFavorecido() {
		return dsCctciaInscricaoFavorecido;
	}

	/**
	 * Nome: setDsCctciaInscricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @param dsCctciaInscricaoFavorecido
	 */
	public void setDsCctciaInscricaoFavorecido(String dsCctciaInscricaoFavorecido) {
		this.dsCctciaInscricaoFavorecido = dsCctciaInscricaoFavorecido;
	}

	/**
	 * Nome: getCdCctciaProprietarioVeculo
	 *
	 * @exception
	 * @throws
	 * @return cdCctciaProprietarioVeculo
	 */
	public Integer getCdCctciaProprietarioVeculo() {
		return cdCctciaProprietarioVeculo;
	}

	/**
	 * Nome: setCdCctciaProprietarioVeculo
	 *
	 * @exception
	 * @throws
	 * @param cdCctciaProprietarioVeculo
	 */
	public void setCdCctciaProprietarioVeculo(Integer cdCctciaProprietarioVeculo) {
		this.cdCctciaProprietarioVeculo = cdCctciaProprietarioVeculo;
	}

	/**
	 * Nome: getDsCctciaProprietarioVeculo
	 *
	 * @exception
	 * @throws
	 * @return dsCctciaProprietarioVeculo
	 */
	public String getDsCctciaProprietarioVeculo() {
		return dsCctciaProprietarioVeculo;
	}

	/**
	 * Nome: setDsCctciaProprietarioVeculo
	 *
	 * @exception
	 * @throws
	 * @param dsCctciaProprietarioVeculo
	 */
	public void setDsCctciaProprietarioVeculo(String dsCctciaProprietarioVeculo) {
		this.dsCctciaProprietarioVeculo = dsCctciaProprietarioVeculo;
	}

	/**
	 * Nome: getCdDisponibilizacaoContaCredito
	 *
	 * @exception
	 * @throws
	 * @return cdDisponibilizacaoContaCredito
	 */
	public Integer getCdDisponibilizacaoContaCredito() {
		return cdDisponibilizacaoContaCredito;
	}

	/**
	 * Nome: setCdDisponibilizacaoContaCredito
	 *
	 * @exception
	 * @throws
	 * @param cdDisponibilizacaoContaCredito
	 */
	public void setCdDisponibilizacaoContaCredito(
			Integer cdDisponibilizacaoContaCredito) {
		this.cdDisponibilizacaoContaCredito = cdDisponibilizacaoContaCredito;
	}

	/**
	 * Nome: getDsDisponibilizacaoContaCredito
	 *
	 * @exception
	 * @throws
	 * @return dsDisponibilizacaoContaCredito
	 */
	public String getDsDisponibilizacaoContaCredito() {
		return dsDisponibilizacaoContaCredito;
	}

	/**
	 * Nome: setDsDisponibilizacaoContaCredito
	 *
	 * @exception
	 * @throws
	 * @param dsDisponibilizacaoContaCredito
	 */
	public void setDsDisponibilizacaoContaCredito(
			String dsDisponibilizacaoContaCredito) {
		this.dsDisponibilizacaoContaCredito = dsDisponibilizacaoContaCredito;
	}

	/**
	 * Nome: getCdDisponibilizacaoDiversoCriterio
	 *
	 * @exception
	 * @throws
	 * @return cdDisponibilizacaoDiversoCriterio
	 */
	public Integer getCdDisponibilizacaoDiversoCriterio() {
		return cdDisponibilizacaoDiversoCriterio;
	}

	/**
	 * Nome: setCdDisponibilizacaoDiversoCriterio
	 *
	 * @exception
	 * @throws
	 * @param cdDisponibilizacaoDiversoCriterio
	 */
	public void setCdDisponibilizacaoDiversoCriterio(
			Integer cdDisponibilizacaoDiversoCriterio) {
		this.cdDisponibilizacaoDiversoCriterio = cdDisponibilizacaoDiversoCriterio;
	}

	/**
	 * Nome: getDsDisponibilizacaoDiversoCriterio
	 *
	 * @exception
	 * @throws
	 * @return dsDisponibilizacaoDiversoCriterio
	 */
	public String getDsDisponibilizacaoDiversoCriterio() {
		return dsDisponibilizacaoDiversoCriterio;
	}

	/**
	 * Nome: setDsDisponibilizacaoDiversoCriterio
	 *
	 * @exception
	 * @throws
	 * @param dsDisponibilizacaoDiversoCriterio
	 */
	public void setDsDisponibilizacaoDiversoCriterio(
			String dsDisponibilizacaoDiversoCriterio) {
		this.dsDisponibilizacaoDiversoCriterio = dsDisponibilizacaoDiversoCriterio;
	}

	/**
	 * Nome: getCdDisponibilizacaoDiversoNao
	 *
	 * @exception
	 * @throws
	 * @return cdDisponibilizacaoDiversoNao
	 */
	public Integer getCdDisponibilizacaoDiversoNao() {
		return cdDisponibilizacaoDiversoNao;
	}

	/**
	 * Nome: setCdDisponibilizacaoDiversoNao
	 *
	 * @exception
	 * @throws
	 * @param cdDisponibilizacaoDiversoNao
	 */
	public void setCdDisponibilizacaoDiversoNao(Integer cdDisponibilizacaoDiversoNao) {
		this.cdDisponibilizacaoDiversoNao = cdDisponibilizacaoDiversoNao;
	}

	/**
	 * Nome: getDsDisponibilizacaoDiversoNao
	 *
	 * @exception
	 * @throws
	 * @return dsDisponibilizacaoDiversoNao
	 */
	public String getDsDisponibilizacaoDiversoNao() {
		return dsDisponibilizacaoDiversoNao;
	}

	/**
	 * Nome: setDsDisponibilizacaoDiversoNao
	 *
	 * @exception
	 * @throws
	 * @param dsDisponibilizacaoDiversoNao
	 */
	public void setDsDisponibilizacaoDiversoNao(String dsDisponibilizacaoDiversoNao) {
		this.dsDisponibilizacaoDiversoNao = dsDisponibilizacaoDiversoNao;
	}

	/**
	 * Nome: getCdDisponibilizacaoSalarioCriterio
	 *
	 * @exception
	 * @throws
	 * @return cdDisponibilizacaoSalarioCriterio
	 */
	public Integer getCdDisponibilizacaoSalarioCriterio() {
		return cdDisponibilizacaoSalarioCriterio;
	}

	/**
	 * Nome: setCdDisponibilizacaoSalarioCriterio
	 *
	 * @exception
	 * @throws
	 * @param cdDisponibilizacaoSalarioCriterio
	 */
	public void setCdDisponibilizacaoSalarioCriterio(
			Integer cdDisponibilizacaoSalarioCriterio) {
		this.cdDisponibilizacaoSalarioCriterio = cdDisponibilizacaoSalarioCriterio;
	}

	/**
	 * Nome: getDsDisponibilizacaoSalarioCriterio
	 *
	 * @exception
	 * @throws
	 * @return dsDisponibilizacaoSalarioCriterio
	 */
	public String getDsDisponibilizacaoSalarioCriterio() {
		return dsDisponibilizacaoSalarioCriterio;
	}

	/**
	 * Nome: setDsDisponibilizacaoSalarioCriterio
	 *
	 * @exception
	 * @throws
	 * @param dsDisponibilizacaoSalarioCriterio
	 */
	public void setDsDisponibilizacaoSalarioCriterio(
			String dsDisponibilizacaoSalarioCriterio) {
		this.dsDisponibilizacaoSalarioCriterio = dsDisponibilizacaoSalarioCriterio;
	}

	/**
	 * Nome: getCdDisponibilizacaoSalarioNao
	 *
	 * @exception
	 * @throws
	 * @return cdDisponibilizacaoSalarioNao
	 */
	public Integer getCdDisponibilizacaoSalarioNao() {
		return cdDisponibilizacaoSalarioNao;
	}

	/**
	 * Nome: setCdDisponibilizacaoSalarioNao
	 *
	 * @exception
	 * @throws
	 * @param cdDisponibilizacaoSalarioNao
	 */
	public void setCdDisponibilizacaoSalarioNao(Integer cdDisponibilizacaoSalarioNao) {
		this.cdDisponibilizacaoSalarioNao = cdDisponibilizacaoSalarioNao;
	}

	/**
	 * Nome: getDsDisponibilizacaoSalarioNao
	 *
	 * @exception
	 * @throws
	 * @return dsDisponibilizacaoSalarioNao
	 */
	public String getDsDisponibilizacaoSalarioNao() {
		return dsDisponibilizacaoSalarioNao;
	}

	/**
	 * Nome: setDsDisponibilizacaoSalarioNao
	 *
	 * @exception
	 * @throws
	 * @param dsDisponibilizacaoSalarioNao
	 */
	public void setDsDisponibilizacaoSalarioNao(String dsDisponibilizacaoSalarioNao) {
		this.dsDisponibilizacaoSalarioNao = dsDisponibilizacaoSalarioNao;
	}

	/**
	 * Nome: getCdDestinoAviso
	 *
	 * @exception
	 * @throws
	 * @return cdDestinoAviso
	 */
	public Integer getCdDestinoAviso() {
		return cdDestinoAviso;
	}

	/**
	 * Nome: setCdDestinoAviso
	 *
	 * @exception
	 * @throws
	 * @param cdDestinoAviso
	 */
	public void setCdDestinoAviso(Integer cdDestinoAviso) {
		this.cdDestinoAviso = cdDestinoAviso;
	}

	/**
	 * Nome: getDsDestinoAviso
	 *
	 * @exception
	 * @throws
	 * @return dsDestinoAviso
	 */
	public String getDsDestinoAviso() {
		return dsDestinoAviso;
	}

	/**
	 * Nome: setDsDestinoAviso
	 *
	 * @exception
	 * @throws
	 * @param dsDestinoAviso
	 */
	public void setDsDestinoAviso(String dsDestinoAviso) {
		this.dsDestinoAviso = dsDestinoAviso;
	}

	/**
	 * Nome: getCdDestinoComprovante
	 *
	 * @exception
	 * @throws
	 * @return cdDestinoComprovante
	 */
	public Integer getCdDestinoComprovante() {
		return cdDestinoComprovante;
	}

	/**
	 * Nome: setCdDestinoComprovante
	 *
	 * @exception
	 * @throws
	 * @param cdDestinoComprovante
	 */
	public void setCdDestinoComprovante(Integer cdDestinoComprovante) {
		this.cdDestinoComprovante = cdDestinoComprovante;
	}

	/**
	 * Nome: getDsDestinoComprovante
	 *
	 * @exception
	 * @throws
	 * @return dsDestinoComprovante
	 */
	public String getDsDestinoComprovante() {
		return dsDestinoComprovante;
	}

	/**
	 * Nome: setDsDestinoComprovante
	 *
	 * @exception
	 * @throws
	 * @param dsDestinoComprovante
	 */
	public void setDsDestinoComprovante(String dsDestinoComprovante) {
		this.dsDestinoComprovante = dsDestinoComprovante;
	}

	/**
	 * Nome: getCdDestinoFormularioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdDestinoFormularioRecadastramento
	 */
	public Integer getCdDestinoFormularioRecadastramento() {
		return cdDestinoFormularioRecadastramento;
	}

	/**
	 * Nome: setCdDestinoFormularioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdDestinoFormularioRecadastramento
	 */
	public void setCdDestinoFormularioRecadastramento(
			Integer cdDestinoFormularioRecadastramento) {
		this.cdDestinoFormularioRecadastramento = cdDestinoFormularioRecadastramento;
	}

	/**
	 * Nome: getDsDestinoFormularioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsDestinoFormularioRecadastramento
	 */
	public String getDsDestinoFormularioRecadastramento() {
		return dsDestinoFormularioRecadastramento;
	}

	/**
	 * Nome: setDsDestinoFormularioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsDestinoFormularioRecadastramento
	 */
	public void setDsDestinoFormularioRecadastramento(
			String dsDestinoFormularioRecadastramento) {
		this.dsDestinoFormularioRecadastramento = dsDestinoFormularioRecadastramento;
	}

	/**
	 * Nome: getCdEnvelopeAberto
	 *
	 * @exception
	 * @throws
	 * @return cdEnvelopeAberto
	 */
	public Integer getCdEnvelopeAberto() {
		return cdEnvelopeAberto;
	}

	/**
	 * Nome: setCdEnvelopeAberto
	 *
	 * @exception
	 * @throws
	 * @param cdEnvelopeAberto
	 */
	public void setCdEnvelopeAberto(Integer cdEnvelopeAberto) {
		this.cdEnvelopeAberto = cdEnvelopeAberto;
	}

	/**
	 * Nome: getDsEnvelopeAberto
	 *
	 * @exception
	 * @throws
	 * @return dsEnvelopeAberto
	 */
	public String getDsEnvelopeAberto() {
		return dsEnvelopeAberto;
	}

	/**
	 * Nome: setDsEnvelopeAberto
	 *
	 * @exception
	 * @throws
	 * @param dsEnvelopeAberto
	 */
	public void setDsEnvelopeAberto(String dsEnvelopeAberto) {
		this.dsEnvelopeAberto = dsEnvelopeAberto;
	}

	/**
	 * Nome: getCdFavorecidoConsultaPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdFavorecidoConsultaPagamento
	 */
	public Integer getCdFavorecidoConsultaPagamento() {
		return cdFavorecidoConsultaPagamento;
	}

	/**
	 * Nome: setCdFavorecidoConsultaPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdFavorecidoConsultaPagamento
	 */
	public void setCdFavorecidoConsultaPagamento(
			Integer cdFavorecidoConsultaPagamento) {
		this.cdFavorecidoConsultaPagamento = cdFavorecidoConsultaPagamento;
	}

	/**
	 * Nome: getDsFavorecidoConsultaPagamento
	 *
	 * @exception
	 * @throws
	 * @return dsFavorecidoConsultaPagamento
	 */
	public String getDsFavorecidoConsultaPagamento() {
		return dsFavorecidoConsultaPagamento;
	}

	/**
	 * Nome: setDsFavorecidoConsultaPagamento
	 *
	 * @exception
	 * @throws
	 * @param dsFavorecidoConsultaPagamento
	 */
	public void setDsFavorecidoConsultaPagamento(
			String dsFavorecidoConsultaPagamento) {
		this.dsFavorecidoConsultaPagamento = dsFavorecidoConsultaPagamento;
	}

	/**
	 * Nome: getCdFormaAutorizacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdFormaAutorizacaoPagamento
	 */
	public Integer getCdFormaAutorizacaoPagamento() {
		return cdFormaAutorizacaoPagamento;
	}

	/**
	 * Nome: setCdFormaAutorizacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdFormaAutorizacaoPagamento
	 */
	public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
		this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
	}

	/**
	 * Nome: getDsFormaAutorizacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @return dsFormaAutorizacaoPagamento
	 */
	public String getDsFormaAutorizacaoPagamento() {
		return dsFormaAutorizacaoPagamento;
	}

	/**
	 * Nome: setDsFormaAutorizacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @param dsFormaAutorizacaoPagamento
	 */
	public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
		this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
	}

	/**
	 * Nome: getCdFormaEnvioPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdFormaEnvioPagamento
	 */
	public Integer getCdFormaEnvioPagamento() {
		return cdFormaEnvioPagamento;
	}

	/**
	 * Nome: setCdFormaEnvioPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdFormaEnvioPagamento
	 */
	public void setCdFormaEnvioPagamento(Integer cdFormaEnvioPagamento) {
		this.cdFormaEnvioPagamento = cdFormaEnvioPagamento;
	}

	/**
	 * Nome: getDsFormaEnvioPagamento
	 *
	 * @exception
	 * @throws
	 * @return dsFormaEnvioPagamento
	 */
	public String getDsFormaEnvioPagamento() {
		return dsFormaEnvioPagamento;
	}

	/**
	 * Nome: setDsFormaEnvioPagamento
	 *
	 * @exception
	 * @throws
	 * @param dsFormaEnvioPagamento
	 */
	public void setDsFormaEnvioPagamento(String dsFormaEnvioPagamento) {
		this.dsFormaEnvioPagamento = dsFormaEnvioPagamento;
	}

	/**
	 * Nome: getCdFormaExpiracaoCredito
	 *
	 * @exception
	 * @throws
	 * @return cdFormaExpiracaoCredito
	 */
	public Integer getCdFormaExpiracaoCredito() {
		return cdFormaExpiracaoCredito;
	}

	/**
	 * Nome: setCdFormaExpiracaoCredito
	 *
	 * @exception
	 * @throws
	 * @param cdFormaExpiracaoCredito
	 */
	public void setCdFormaExpiracaoCredito(Integer cdFormaExpiracaoCredito) {
		this.cdFormaExpiracaoCredito = cdFormaExpiracaoCredito;
	}

	/**
	 * Nome: getDsFormaExpiracaoCredito
	 *
	 * @exception
	 * @throws
	 * @return dsFormaExpiracaoCredito
	 */
	public String getDsFormaExpiracaoCredito() {
		return dsFormaExpiracaoCredito;
	}

	/**
	 * Nome: setDsFormaExpiracaoCredito
	 *
	 * @exception
	 * @throws
	 * @param dsFormaExpiracaoCredito
	 */
	public void setDsFormaExpiracaoCredito(String dsFormaExpiracaoCredito) {
		this.dsFormaExpiracaoCredito = dsFormaExpiracaoCredito;
	}

	/**
	 * Nome: getCdFormaManutencao
	 *
	 * @exception
	 * @throws
	 * @return cdFormaManutencao
	 */
	public Integer getCdFormaManutencao() {
		return cdFormaManutencao;
	}

	/**
	 * Nome: setCdFormaManutencao
	 *
	 * @exception
	 * @throws
	 * @param cdFormaManutencao
	 */
	public void setCdFormaManutencao(Integer cdFormaManutencao) {
		this.cdFormaManutencao = cdFormaManutencao;
	}

	/**
	 * Nome: getDsFormaManutencao
	 *
	 * @exception
	 * @throws
	 * @return dsFormaManutencao
	 */
	public String getDsFormaManutencao() {
		return dsFormaManutencao;
	}

	/**
	 * Nome: setDsFormaManutencao
	 *
	 * @exception
	 * @throws
	 * @param dsFormaManutencao
	 */
	public void setDsFormaManutencao(String dsFormaManutencao) {
		this.dsFormaManutencao = dsFormaManutencao;
	}

	/**
	 * Nome: getCdFrasePrecadastrada
	 *
	 * @exception
	 * @throws
	 * @return cdFrasePrecadastrada
	 */
	public Integer getCdFrasePrecadastrada() {
		return cdFrasePrecadastrada;
	}

	/**
	 * Nome: setCdFrasePrecadastrada
	 *
	 * @exception
	 * @throws
	 * @param cdFrasePrecadastrada
	 */
	public void setCdFrasePrecadastrada(Integer cdFrasePrecadastrada) {
		this.cdFrasePrecadastrada = cdFrasePrecadastrada;
	}

	/**
	 * Nome: getDsFrasePrecadastrada
	 *
	 * @exception
	 * @throws
	 * @return dsFrasePrecadastrada
	 */
	public String getDsFrasePrecadastrada() {
		return dsFrasePrecadastrada;
	}

	/**
	 * Nome: setDsFrasePrecadastrada
	 *
	 * @exception
	 * @throws
	 * @param dsFrasePrecadastrada
	 */
	public void setDsFrasePrecadastrada(String dsFrasePrecadastrada) {
		this.dsFrasePrecadastrada = dsFrasePrecadastrada;
	}

	/**
	 * Nome: getCdIndicadorAgendaTitulo
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAgendaTitulo
	 */
	public Integer getCdIndicadorAgendaTitulo() {
		return cdIndicadorAgendaTitulo;
	}

	/**
	 * Nome: setCdIndicadorAgendaTitulo
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAgendaTitulo
	 */
	public void setCdIndicadorAgendaTitulo(Integer cdIndicadorAgendaTitulo) {
		this.cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
	}

	/**
	 * Nome: getDsIndicadorAgendamentoTitulo
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorAgendamentoTitulo
	 */
	public String getDsIndicadorAgendamentoTitulo() {
		return dsIndicadorAgendamentoTitulo;
	}

	/**
	 * Nome: setDsIndicadorAgendamentoTitulo
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorAgendamentoTitulo
	 */
	public void setDsIndicadorAgendamentoTitulo(String dsIndicadorAgendamentoTitulo) {
		this.dsIndicadorAgendamentoTitulo = dsIndicadorAgendamentoTitulo;
	}

	/**
	 * Nome: getCdIndicadorAutorizacaoCliente
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAutorizacaoCliente
	 */
	public Integer getCdIndicadorAutorizacaoCliente() {
		return cdIndicadorAutorizacaoCliente;
	}

	/**
	 * Nome: setCdIndicadorAutorizacaoCliente
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAutorizacaoCliente
	 */
	public void setCdIndicadorAutorizacaoCliente(
			Integer cdIndicadorAutorizacaoCliente) {
		this.cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
	}

	/**
	 * Nome: getDsIndicadorAutorizacaoCliente
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorAutorizacaoCliente
	 */
	public String getDsIndicadorAutorizacaoCliente() {
		return dsIndicadorAutorizacaoCliente;
	}

	/**
	 * Nome: setDsIndicadorAutorizacaoCliente
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorAutorizacaoCliente
	 */
	public void setDsIndicadorAutorizacaoCliente(
			String dsIndicadorAutorizacaoCliente) {
		this.dsIndicadorAutorizacaoCliente = dsIndicadorAutorizacaoCliente;
	}

	/**
	 * Nome: getCdIndicadorAutorizacaoComplemento
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAutorizacaoComplemento
	 */
	public Integer getCdIndicadorAutorizacaoComplemento() {
		return cdIndicadorAutorizacaoComplemento;
	}

	/**
	 * Nome: setCdIndicadorAutorizacaoComplemento
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAutorizacaoComplemento
	 */
	public void setCdIndicadorAutorizacaoComplemento(
			Integer cdIndicadorAutorizacaoComplemento) {
		this.cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
	}

	/**
	 * Nome: getDsIndicadorAutorizacaoComplemento
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorAutorizacaoComplemento
	 */
	public String getDsIndicadorAutorizacaoComplemento() {
		return dsIndicadorAutorizacaoComplemento;
	}

	/**
	 * Nome: setDsIndicadorAutorizacaoComplemento
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorAutorizacaoComplemento
	 */
	public void setDsIndicadorAutorizacaoComplemento(
			String dsIndicadorAutorizacaoComplemento) {
		this.dsIndicadorAutorizacaoComplemento = dsIndicadorAutorizacaoComplemento;
	}

	/**
	 * Nome: getCdIndicadorCadastroorganizacao
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorCadastroorganizacao
	 */
	public Integer getCdIndicadorCadastroorganizacao() {
		return cdIndicadorCadastroorganizacao;
	}

	/**
	 * Nome: setCdIndicadorCadastroorganizacao
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorCadastroorganizacao
	 */
	public void setCdIndicadorCadastroorganizacao(
			Integer cdIndicadorCadastroorganizacao) {
		this.cdIndicadorCadastroorganizacao = cdIndicadorCadastroorganizacao;
	}

	/**
	 * Nome: getDsIndicadorCadastroorganizacao
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorCadastroorganizacao
	 */
	public String getDsIndicadorCadastroorganizacao() {
		return dsIndicadorCadastroorganizacao;
	}

	/**
	 * Nome: setDsIndicadorCadastroorganizacao
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorCadastroorganizacao
	 */
	public void setDsIndicadorCadastroorganizacao(
			String dsIndicadorCadastroorganizacao) {
		this.dsIndicadorCadastroorganizacao = dsIndicadorCadastroorganizacao;
	}

	/**
	 * Nome: getCdIndicadorCadastroProcurador
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorCadastroProcurador
	 */
	public Integer getCdIndicadorCadastroProcurador() {
		return cdIndicadorCadastroProcurador;
	}

	/**
	 * Nome: setCdIndicadorCadastroProcurador
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorCadastroProcurador
	 */
	public void setCdIndicadorCadastroProcurador(
			Integer cdIndicadorCadastroProcurador) {
		this.cdIndicadorCadastroProcurador = cdIndicadorCadastroProcurador;
	}

	/**
	 * Nome: getDsIndicadorCadastroProcurador
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorCadastroProcurador
	 */
	public String getDsIndicadorCadastroProcurador() {
		return dsIndicadorCadastroProcurador;
	}

	/**
	 * Nome: setDsIndicadorCadastroProcurador
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorCadastroProcurador
	 */
	public void setDsIndicadorCadastroProcurador(
			String dsIndicadorCadastroProcurador) {
		this.dsIndicadorCadastroProcurador = dsIndicadorCadastroProcurador;
	}

	/**
	 * Nome: getCdIndicadorCartaoSalario
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorCartaoSalario
	 */
	public Integer getCdIndicadorCartaoSalario() {
		return cdIndicadorCartaoSalario;
	}

	/**
	 * Nome: setCdIndicadorCartaoSalario
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorCartaoSalario
	 */
	public void setCdIndicadorCartaoSalario(Integer cdIndicadorCartaoSalario) {
		this.cdIndicadorCartaoSalario = cdIndicadorCartaoSalario;
	}

	/**
	 * Nome: getDsIndicadorCartaoSalario
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorCartaoSalario
	 */
	public String getDsIndicadorCartaoSalario() {
		return dsIndicadorCartaoSalario;
	}

	/**
	 * Nome: setDsIndicadorCartaoSalario
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorCartaoSalario
	 */
	public void setDsIndicadorCartaoSalario(String dsIndicadorCartaoSalario) {
		this.dsIndicadorCartaoSalario = dsIndicadorCartaoSalario;
	}

	/**
	 * Nome: getCdIndicadorEconomicoReajuste
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorEconomicoReajuste
	 */
	public Integer getCdIndicadorEconomicoReajuste() {
		return cdIndicadorEconomicoReajuste;
	}

	/**
	 * Nome: setCdIndicadorEconomicoReajuste
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorEconomicoReajuste
	 */
	public void setCdIndicadorEconomicoReajuste(Integer cdIndicadorEconomicoReajuste) {
		this.cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
	}

	/**
	 * Nome: getDsIndicadorEconomicoReajuste
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorEconomicoReajuste
	 */
	public String getDsIndicadorEconomicoReajuste() {
		return dsIndicadorEconomicoReajuste;
	}

	/**
	 * Nome: setDsIndicadorEconomicoReajuste
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorEconomicoReajuste
	 */
	public void setDsIndicadorEconomicoReajuste(String dsIndicadorEconomicoReajuste) {
		this.dsIndicadorEconomicoReajuste = dsIndicadorEconomicoReajuste;
	}

	/**
	 * Nome: getCdindicadorExpiraCredito
	 *
	 * @exception
	 * @throws
	 * @return cdindicadorExpiraCredito
	 */
	public Integer getCdindicadorExpiraCredito() {
		return cdindicadorExpiraCredito;
	}

	/**
	 * Nome: setCdindicadorExpiraCredito
	 *
	 * @exception
	 * @throws
	 * @param cdindicadorExpiraCredito
	 */
	public void setCdindicadorExpiraCredito(Integer cdindicadorExpiraCredito) {
		this.cdindicadorExpiraCredito = cdindicadorExpiraCredito;
	}

	/**
	 * Nome: getDsindicadorExpiraCredito
	 *
	 * @exception
	 * @throws
	 * @return dsindicadorExpiraCredito
	 */
	public String getDsindicadorExpiraCredito() {
		return dsindicadorExpiraCredito;
	}

	/**
	 * Nome: setDsindicadorExpiraCredito
	 *
	 * @exception
	 * @throws
	 * @param dsindicadorExpiraCredito
	 */
	public void setDsindicadorExpiraCredito(String dsindicadorExpiraCredito) {
		this.dsindicadorExpiraCredito = dsindicadorExpiraCredito;
	}

	/**
	 * Nome: getCdindicadorLancamentoProgramado
	 *
	 * @exception
	 * @throws
	 * @return cdindicadorLancamentoProgramado
	 */
	public Integer getCdindicadorLancamentoProgramado() {
		return cdindicadorLancamentoProgramado;
	}

	/**
	 * Nome: setCdindicadorLancamentoProgramado
	 *
	 * @exception
	 * @throws
	 * @param cdindicadorLancamentoProgramado
	 */
	public void setCdindicadorLancamentoProgramado(
			Integer cdindicadorLancamentoProgramado) {
		this.cdindicadorLancamentoProgramado = cdindicadorLancamentoProgramado;
	}

	/**
	 * Nome: getDsindicadorLancamentoProgramado
	 *
	 * @exception
	 * @throws
	 * @return dsindicadorLancamentoProgramado
	 */
	public String getDsindicadorLancamentoProgramado() {
		return dsindicadorLancamentoProgramado;
	}

	/**
	 * Nome: setDsindicadorLancamentoProgramado
	 *
	 * @exception
	 * @throws
	 * @param dsindicadorLancamentoProgramado
	 */
	public void setDsindicadorLancamentoProgramado(
			String dsindicadorLancamentoProgramado) {
		this.dsindicadorLancamentoProgramado = dsindicadorLancamentoProgramado;
	}

	/**
	 * Nome: getCdIndicadorMensagemPersonalizada
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorMensagemPersonalizada
	 */
	public Integer getCdIndicadorMensagemPersonalizada() {
		return cdIndicadorMensagemPersonalizada;
	}

	/**
	 * Nome: setCdIndicadorMensagemPersonalizada
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorMensagemPersonalizada
	 */
	public void setCdIndicadorMensagemPersonalizada(
			Integer cdIndicadorMensagemPersonalizada) {
		this.cdIndicadorMensagemPersonalizada = cdIndicadorMensagemPersonalizada;
	}

	/**
	 * Nome: getDsIndicadorMensagemPersonalizada
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorMensagemPersonalizada
	 */
	public String getDsIndicadorMensagemPersonalizada() {
		return dsIndicadorMensagemPersonalizada;
	}

	/**
	 * Nome: setDsIndicadorMensagemPersonalizada
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorMensagemPersonalizada
	 */
	public void setDsIndicadorMensagemPersonalizada(
			String dsIndicadorMensagemPersonalizada) {
		this.dsIndicadorMensagemPersonalizada = dsIndicadorMensagemPersonalizada;
	}

	/**
	 * Nome: getCdLancamentoFuturoCredito
	 *
	 * @exception
	 * @throws
	 * @return cdLancamentoFuturoCredito
	 */
	public Integer getCdLancamentoFuturoCredito() {
		return cdLancamentoFuturoCredito;
	}

	/**
	 * Nome: setCdLancamentoFuturoCredito
	 *
	 * @exception
	 * @throws
	 * @param cdLancamentoFuturoCredito
	 */
	public void setCdLancamentoFuturoCredito(Integer cdLancamentoFuturoCredito) {
		this.cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
	}

	/**
	 * Nome: getDsLancamentoFuturoCredito
	 *
	 * @exception
	 * @throws
	 * @return dsLancamentoFuturoCredito
	 */
	public String getDsLancamentoFuturoCredito() {
		return dsLancamentoFuturoCredito;
	}

	/**
	 * Nome: setDsLancamentoFuturoCredito
	 *
	 * @exception
	 * @throws
	 * @param dsLancamentoFuturoCredito
	 */
	public void setDsLancamentoFuturoCredito(String dsLancamentoFuturoCredito) {
		this.dsLancamentoFuturoCredito = dsLancamentoFuturoCredito;
	}

	/**
	 * Nome: getCdLancamentoFuturoDebito
	 *
	 * @exception
	 * @throws
	 * @return cdLancamentoFuturoDebito
	 */
	public Integer getCdLancamentoFuturoDebito() {
		return cdLancamentoFuturoDebito;
	}

	/**
	 * Nome: setCdLancamentoFuturoDebito
	 *
	 * @exception
	 * @throws
	 * @param cdLancamentoFuturoDebito
	 */
	public void setCdLancamentoFuturoDebito(Integer cdLancamentoFuturoDebito) {
		this.cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
	}

	/**
	 * Nome: getDsLancamentoFuturoDebito
	 *
	 * @exception
	 * @throws
	 * @return dsLancamentoFuturoDebito
	 */
	public String getDsLancamentoFuturoDebito() {
		return dsLancamentoFuturoDebito;
	}

	/**
	 * Nome: setDsLancamentoFuturoDebito
	 *
	 * @exception
	 * @throws
	 * @param dsLancamentoFuturoDebito
	 */
	public void setDsLancamentoFuturoDebito(String dsLancamentoFuturoDebito) {
		this.dsLancamentoFuturoDebito = dsLancamentoFuturoDebito;
	}

	/**
	 * Nome: getCdLiberacaoLoteProcs
	 *
	 * @exception
	 * @throws
	 * @return cdLiberacaoLoteProcs
	 */
	public Integer getCdLiberacaoLoteProcs() {
		return cdLiberacaoLoteProcs;
	}

	/**
	 * Nome: setCdLiberacaoLoteProcs
	 *
	 * @exception
	 * @throws
	 * @param cdLiberacaoLoteProcs
	 */
	public void setCdLiberacaoLoteProcs(Integer cdLiberacaoLoteProcs) {
		this.cdLiberacaoLoteProcs = cdLiberacaoLoteProcs;
	}

	/**
	 * Nome: getDsLiberacaoLoteProcessado
	 *
	 * @exception
	 * @throws
	 * @return dsLiberacaoLoteProcessado
	 */
	public String getDsLiberacaoLoteProcessado() {
		return dsLiberacaoLoteProcessado;
	}

	/**
	 * Nome: setDsLiberacaoLoteProcessado
	 *
	 * @exception
	 * @throws
	 * @param dsLiberacaoLoteProcessado
	 */
	public void setDsLiberacaoLoteProcessado(String dsLiberacaoLoteProcessado) {
		this.dsLiberacaoLoteProcessado = dsLiberacaoLoteProcessado;
	}

	/**
	 * Nome: getCdManutencaoBaseRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdManutencaoBaseRecadastramento
	 */
	public Integer getCdManutencaoBaseRecadastramento() {
		return cdManutencaoBaseRecadastramento;
	}

	/**
	 * Nome: setCdManutencaoBaseRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdManutencaoBaseRecadastramento
	 */
	public void setCdManutencaoBaseRecadastramento(
			Integer cdManutencaoBaseRecadastramento) {
		this.cdManutencaoBaseRecadastramento = cdManutencaoBaseRecadastramento;
	}

	/**
	 * Nome: getDsManutencaoBaseRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsManutencaoBaseRecadastramento
	 */
	public String getDsManutencaoBaseRecadastramento() {
		return dsManutencaoBaseRecadastramento;
	}

	/**
	 * Nome: setDsManutencaoBaseRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsManutencaoBaseRecadastramento
	 */
	public void setDsManutencaoBaseRecadastramento(
			String dsManutencaoBaseRecadastramento) {
		this.dsManutencaoBaseRecadastramento = dsManutencaoBaseRecadastramento;
	}

	/**
	 * Nome: getCdMidiaDisponivel
	 *
	 * @exception
	 * @throws
	 * @return cdMidiaDisponivel
	 */
	public Integer getCdMidiaDisponivel() {
		return cdMidiaDisponivel;
	}

	/**
	 * Nome: setCdMidiaDisponivel
	 *
	 * @exception
	 * @throws
	 * @param cdMidiaDisponivel
	 */
	public void setCdMidiaDisponivel(Integer cdMidiaDisponivel) {
		this.cdMidiaDisponivel = cdMidiaDisponivel;
	}

	/**
	 * Nome: getDsMidiaDisponivel
	 *
	 * @exception
	 * @throws
	 * @return dsMidiaDisponivel
	 */
	public String getDsMidiaDisponivel() {
		return dsMidiaDisponivel;
	}

	/**
	 * Nome: setDsMidiaDisponivel
	 *
	 * @exception
	 * @throws
	 * @param dsMidiaDisponivel
	 */
	public void setDsMidiaDisponivel(String dsMidiaDisponivel) {
		this.dsMidiaDisponivel = dsMidiaDisponivel;
	}

	/**
	 * Nome: getCdMidiaMensagemRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdMidiaMensagemRecadastramento
	 */
	public Integer getCdMidiaMensagemRecadastramento() {
		return cdMidiaMensagemRecadastramento;
	}

	/**
	 * Nome: setCdMidiaMensagemRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdMidiaMensagemRecadastramento
	 */
	public void setCdMidiaMensagemRecadastramento(
			Integer cdMidiaMensagemRecadastramento) {
		this.cdMidiaMensagemRecadastramento = cdMidiaMensagemRecadastramento;
	}

	/**
	 * Nome: getDsMidiaMensagemRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsMidiaMensagemRecadastramento
	 */
	public String getDsMidiaMensagemRecadastramento() {
		return dsMidiaMensagemRecadastramento;
	}

	/**
	 * Nome: setDsMidiaMensagemRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsMidiaMensagemRecadastramento
	 */
	public void setDsMidiaMensagemRecadastramento(
			String dsMidiaMensagemRecadastramento) {
		this.dsMidiaMensagemRecadastramento = dsMidiaMensagemRecadastramento;
	}

	/**
	 * Nome: getCdMomentoAvisoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdMomentoAvisoRecadastramento
	 */
	public Integer getCdMomentoAvisoRecadastramento() {
		return cdMomentoAvisoRecadastramento;
	}

	/**
	 * Nome: setCdMomentoAvisoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdMomentoAvisoRecadastramento
	 */
	public void setCdMomentoAvisoRecadastramento(
			Integer cdMomentoAvisoRecadastramento) {
		this.cdMomentoAvisoRecadastramento = cdMomentoAvisoRecadastramento;
	}

	/**
	 * Nome: getDsMomentoAvisoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsMomentoAvisoRecadastramento
	 */
	public String getDsMomentoAvisoRecadastramento() {
		return dsMomentoAvisoRecadastramento;
	}

	/**
	 * Nome: setDsMomentoAvisoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsMomentoAvisoRecadastramento
	 */
	public void setDsMomentoAvisoRecadastramento(
			String dsMomentoAvisoRecadastramento) {
		this.dsMomentoAvisoRecadastramento = dsMomentoAvisoRecadastramento;
	}

	/**
	 * Nome: getCdMomentoCreditoEfetivacao
	 *
	 * @exception
	 * @throws
	 * @return cdMomentoCreditoEfetivacao
	 */
	public Integer getCdMomentoCreditoEfetivacao() {
		return cdMomentoCreditoEfetivacao;
	}

	/**
	 * Nome: setCdMomentoCreditoEfetivacao
	 *
	 * @exception
	 * @throws
	 * @param cdMomentoCreditoEfetivacao
	 */
	public void setCdMomentoCreditoEfetivacao(Integer cdMomentoCreditoEfetivacao) {
		this.cdMomentoCreditoEfetivacao = cdMomentoCreditoEfetivacao;
	}

	/**
	 * Nome: getDsMomentoCreditoEfetivacao
	 *
	 * @exception
	 * @throws
	 * @return dsMomentoCreditoEfetivacao
	 */
	public String getDsMomentoCreditoEfetivacao() {
		return dsMomentoCreditoEfetivacao;
	}

	/**
	 * Nome: setDsMomentoCreditoEfetivacao
	 *
	 * @exception
	 * @throws
	 * @param dsMomentoCreditoEfetivacao
	 */
	public void setDsMomentoCreditoEfetivacao(String dsMomentoCreditoEfetivacao) {
		this.dsMomentoCreditoEfetivacao = dsMomentoCreditoEfetivacao;
	}

	/**
	 * Nome: getCdMomentoDebitoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdMomentoDebitoPagamento
	 */
	public Integer getCdMomentoDebitoPagamento() {
		return cdMomentoDebitoPagamento;
	}

	/**
	 * Nome: setCdMomentoDebitoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdMomentoDebitoPagamento
	 */
	public void setCdMomentoDebitoPagamento(Integer cdMomentoDebitoPagamento) {
		this.cdMomentoDebitoPagamento = cdMomentoDebitoPagamento;
	}

	/**
	 * Nome: getDsMomentoDebitoPagamento
	 *
	 * @exception
	 * @throws
	 * @return dsMomentoDebitoPagamento
	 */
	public String getDsMomentoDebitoPagamento() {
		return dsMomentoDebitoPagamento;
	}

	/**
	 * Nome: setDsMomentoDebitoPagamento
	 *
	 * @exception
	 * @throws
	 * @param dsMomentoDebitoPagamento
	 */
	public void setDsMomentoDebitoPagamento(String dsMomentoDebitoPagamento) {
		this.dsMomentoDebitoPagamento = dsMomentoDebitoPagamento;
	}

	/**
	 * Nome: getCdMomentoFormularioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdMomentoFormularioRecadastramento
	 */
	public Integer getCdMomentoFormularioRecadastramento() {
		return cdMomentoFormularioRecadastramento;
	}

	/**
	 * Nome: setCdMomentoFormularioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdMomentoFormularioRecadastramento
	 */
	public void setCdMomentoFormularioRecadastramento(
			Integer cdMomentoFormularioRecadastramento) {
		this.cdMomentoFormularioRecadastramento = cdMomentoFormularioRecadastramento;
	}

	/**
	 * Nome: getDsMomentoFormularioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsMomentoFormularioRecadastramento
	 */
	public String getDsMomentoFormularioRecadastramento() {
		return dsMomentoFormularioRecadastramento;
	}

	/**
	 * Nome: setDsMomentoFormularioRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsMomentoFormularioRecadastramento
	 */
	public void setDsMomentoFormularioRecadastramento(
			String dsMomentoFormularioRecadastramento) {
		this.dsMomentoFormularioRecadastramento = dsMomentoFormularioRecadastramento;
	}

	/**
	 * Nome: getCdMomentoProcessamentoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdMomentoProcessamentoPagamento
	 */
	public Integer getCdMomentoProcessamentoPagamento() {
		return cdMomentoProcessamentoPagamento;
	}

	/**
	 * Nome: setCdMomentoProcessamentoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdMomentoProcessamentoPagamento
	 */
	public void setCdMomentoProcessamentoPagamento(
			Integer cdMomentoProcessamentoPagamento) {
		this.cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
	}

	/**
	 * Nome: getDsMomentoProcessamentoPagamento
	 *
	 * @exception
	 * @throws
	 * @return dsMomentoProcessamentoPagamento
	 */
	public String getDsMomentoProcessamentoPagamento() {
		return dsMomentoProcessamentoPagamento;
	}

	/**
	 * Nome: setDsMomentoProcessamentoPagamento
	 *
	 * @exception
	 * @throws
	 * @param dsMomentoProcessamentoPagamento
	 */
	public void setDsMomentoProcessamentoPagamento(
			String dsMomentoProcessamentoPagamento) {
		this.dsMomentoProcessamentoPagamento = dsMomentoProcessamentoPagamento;
	}

	/**
	 * Nome: getCdMensagemRecadastramentoMidia
	 *
	 * @exception
	 * @throws
	 * @return cdMensagemRecadastramentoMidia
	 */
	public Integer getCdMensagemRecadastramentoMidia() {
		return cdMensagemRecadastramentoMidia;
	}

	/**
	 * Nome: setCdMensagemRecadastramentoMidia
	 *
	 * @exception
	 * @throws
	 * @param cdMensagemRecadastramentoMidia
	 */
	public void setCdMensagemRecadastramentoMidia(
			Integer cdMensagemRecadastramentoMidia) {
		this.cdMensagemRecadastramentoMidia = cdMensagemRecadastramentoMidia;
	}

	/**
	 * Nome: getDsMensagemRecadastramentoMidia
	 *
	 * @exception
	 * @throws
	 * @return dsMensagemRecadastramentoMidia
	 */
	public String getDsMensagemRecadastramentoMidia() {
		return dsMensagemRecadastramentoMidia;
	}

	/**
	 * Nome: setDsMensagemRecadastramentoMidia
	 *
	 * @exception
	 * @throws
	 * @param dsMensagemRecadastramentoMidia
	 */
	public void setDsMensagemRecadastramentoMidia(
			String dsMensagemRecadastramentoMidia) {
		this.dsMensagemRecadastramentoMidia = dsMensagemRecadastramentoMidia;
	}

	/**
	 * Nome: getCdPermissaoDebitoOnline
	 *
	 * @exception
	 * @throws
	 * @return cdPermissaoDebitoOnline
	 */
	public Integer getCdPermissaoDebitoOnline() {
		return cdPermissaoDebitoOnline;
	}

	/**
	 * Nome: setCdPermissaoDebitoOnline
	 *
	 * @exception
	 * @throws
	 * @param cdPermissaoDebitoOnline
	 */
	public void setCdPermissaoDebitoOnline(Integer cdPermissaoDebitoOnline) {
		this.cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
	}

	/**
	 * Nome: getDsPermissaoDebitoOnline
	 *
	 * @exception
	 * @throws
	 * @return dsPermissaoDebitoOnline
	 */
	public String getDsPermissaoDebitoOnline() {
		return dsPermissaoDebitoOnline;
	}

	/**
	 * Nome: setDsPermissaoDebitoOnline
	 *
	 * @exception
	 * @throws
	 * @param dsPermissaoDebitoOnline
	 */
	public void setDsPermissaoDebitoOnline(String dsPermissaoDebitoOnline) {
		this.dsPermissaoDebitoOnline = dsPermissaoDebitoOnline;
	}

	/**
	 * Nome: getCdNaturezaOperacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdNaturezaOperacaoPagamento
	 */
	public Integer getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}

	/**
	 * Nome: setCdNaturezaOperacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdNaturezaOperacaoPagamento
	 */
	public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}

	/**
	 * Nome: getDsNaturezaOperacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @return dsNaturezaOperacaoPagamento
	 */
	public String getDsNaturezaOperacaoPagamento() {
		return dsNaturezaOperacaoPagamento;
	}

	/**
	 * Nome: setDsNaturezaOperacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @param dsNaturezaOperacaoPagamento
	 */
	public void setDsNaturezaOperacaoPagamento(String dsNaturezaOperacaoPagamento) {
		this.dsNaturezaOperacaoPagamento = dsNaturezaOperacaoPagamento;
	}

	/**
	 * Nome: getCdPeriodicidadeAviso
	 *
	 * @exception
	 * @throws
	 * @return cdPeriodicidadeAviso
	 */
	public Integer getCdPeriodicidadeAviso() {
		return cdPeriodicidadeAviso;
	}

	/**
	 * Nome: setCdPeriodicidadeAviso
	 *
	 * @exception
	 * @throws
	 * @param cdPeriodicidadeAviso
	 */
	public void setCdPeriodicidadeAviso(Integer cdPeriodicidadeAviso) {
		this.cdPeriodicidadeAviso = cdPeriodicidadeAviso;
	}

	/**
	 * Nome: getDsPeriodicidadeAviso
	 *
	 * @exception
	 * @throws
	 * @return dsPeriodicidadeAviso
	 */
	public String getDsPeriodicidadeAviso() {
		return dsPeriodicidadeAviso;
	}

	/**
	 * Nome: setDsPeriodicidadeAviso
	 *
	 * @exception
	 * @throws
	 * @param dsPeriodicidadeAviso
	 */
	public void setDsPeriodicidadeAviso(String dsPeriodicidadeAviso) {
		this.dsPeriodicidadeAviso = dsPeriodicidadeAviso;
	}

	/**
	 * Nome: getCdPeriodicidadeCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @return cdPeriodicidadeCobrancaTarifa
	 */
	public Integer getCdPeriodicidadeCobrancaTarifa() {
		return cdPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Nome: setCdPeriodicidadeCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @param cdPeriodicidadeCobrancaTarifa
	 */
	public void setCdPeriodicidadeCobrancaTarifa(
			Integer cdPeriodicidadeCobrancaTarifa) {
		this.cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Nome: getDsPeriodicidadeCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @return dsPeriodicidadeCobrancaTarifa
	 */
	public String getDsPeriodicidadeCobrancaTarifa() {
		return dsPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Nome: setDsPeriodicidadeCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @param dsPeriodicidadeCobrancaTarifa
	 */
	public void setDsPeriodicidadeCobrancaTarifa(
			String dsPeriodicidadeCobrancaTarifa) {
		this.dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Nome: getCdPeriodicidadeComprovante
	 *
	 * @exception
	 * @throws
	 * @return cdPeriodicidadeComprovante
	 */
	public Integer getCdPeriodicidadeComprovante() {
		return cdPeriodicidadeComprovante;
	}

	/**
	 * Nome: setCdPeriodicidadeComprovante
	 *
	 * @exception
	 * @throws
	 * @param cdPeriodicidadeComprovante
	 */
	public void setCdPeriodicidadeComprovante(Integer cdPeriodicidadeComprovante) {
		this.cdPeriodicidadeComprovante = cdPeriodicidadeComprovante;
	}

	/**
	 * Nome: getDsPeriodicidadeComprovante
	 *
	 * @exception
	 * @throws
	 * @return dsPeriodicidadeComprovante
	 */
	public String getDsPeriodicidadeComprovante() {
		return dsPeriodicidadeComprovante;
	}

	/**
	 * Nome: setDsPeriodicidadeComprovante
	 *
	 * @exception
	 * @throws
	 * @param dsPeriodicidadeComprovante
	 */
	public void setDsPeriodicidadeComprovante(String dsPeriodicidadeComprovante) {
		this.dsPeriodicidadeComprovante = dsPeriodicidadeComprovante;
	}

	/**
	 * Nome: getCdPeriodicidadeConsultaVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdPeriodicidadeConsultaVeiculo
	 */
	public Integer getCdPeriodicidadeConsultaVeiculo() {
		return cdPeriodicidadeConsultaVeiculo;
	}

	/**
	 * Nome: setCdPeriodicidadeConsultaVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdPeriodicidadeConsultaVeiculo
	 */
	public void setCdPeriodicidadeConsultaVeiculo(
			Integer cdPeriodicidadeConsultaVeiculo) {
		this.cdPeriodicidadeConsultaVeiculo = cdPeriodicidadeConsultaVeiculo;
	}

	/**
	 * Nome: getDsPeriodicidadeConsultaVeiculo
	 *
	 * @exception
	 * @throws
	 * @return dsPeriodicidadeConsultaVeiculo
	 */
	public String getDsPeriodicidadeConsultaVeiculo() {
		return dsPeriodicidadeConsultaVeiculo;
	}

	/**
	 * Nome: setDsPeriodicidadeConsultaVeiculo
	 *
	 * @exception
	 * @throws
	 * @param dsPeriodicidadeConsultaVeiculo
	 */
	public void setDsPeriodicidadeConsultaVeiculo(
			String dsPeriodicidadeConsultaVeiculo) {
		this.dsPeriodicidadeConsultaVeiculo = dsPeriodicidadeConsultaVeiculo;
	}

	/**
	 * Nome: getCdPeriodicidadeEnvioRemessa
	 *
	 * @exception
	 * @throws
	 * @return cdPeriodicidadeEnvioRemessa
	 */
	public Integer getCdPeriodicidadeEnvioRemessa() {
		return cdPeriodicidadeEnvioRemessa;
	}

	/**
	 * Nome: setCdPeriodicidadeEnvioRemessa
	 *
	 * @exception
	 * @throws
	 * @param cdPeriodicidadeEnvioRemessa
	 */
	public void setCdPeriodicidadeEnvioRemessa(Integer cdPeriodicidadeEnvioRemessa) {
		this.cdPeriodicidadeEnvioRemessa = cdPeriodicidadeEnvioRemessa;
	}

	/**
	 * Nome: getDsPeriodicidadeEnvioRemessa
	 *
	 * @exception
	 * @throws
	 * @return dsPeriodicidadeEnvioRemessa
	 */
	public String getDsPeriodicidadeEnvioRemessa() {
		return dsPeriodicidadeEnvioRemessa;
	}

	/**
	 * Nome: setDsPeriodicidadeEnvioRemessa
	 *
	 * @exception
	 * @throws
	 * @param dsPeriodicidadeEnvioRemessa
	 */
	public void setDsPeriodicidadeEnvioRemessa(String dsPeriodicidadeEnvioRemessa) {
		this.dsPeriodicidadeEnvioRemessa = dsPeriodicidadeEnvioRemessa;
	}

	/**
	 * Nome: getCdPeriodicidadeManutencaoProcd
	 *
	 * @exception
	 * @throws
	 * @return cdPeriodicidadeManutencaoProcd
	 */
	public Integer getCdPeriodicidadeManutencaoProcd() {
		return cdPeriodicidadeManutencaoProcd;
	}

	/**
	 * Nome: setCdPeriodicidadeManutencaoProcd
	 *
	 * @exception
	 * @throws
	 * @param cdPeriodicidadeManutencaoProcd
	 */
	public void setCdPeriodicidadeManutencaoProcd(
			Integer cdPeriodicidadeManutencaoProcd) {
		this.cdPeriodicidadeManutencaoProcd = cdPeriodicidadeManutencaoProcd;
	}

	/**
	 * Nome: getDsPeriodicidadeManutencaoProcd
	 *
	 * @exception
	 * @throws
	 * @return dsPeriodicidadeManutencaoProcd
	 */
	public String getDsPeriodicidadeManutencaoProcd() {
		return dsPeriodicidadeManutencaoProcd;
	}

	/**
	 * Nome: setDsPeriodicidadeManutencaoProcd
	 *
	 * @exception
	 * @throws
	 * @param dsPeriodicidadeManutencaoProcd
	 */
	public void setDsPeriodicidadeManutencaoProcd(
			String dsPeriodicidadeManutencaoProcd) {
		this.dsPeriodicidadeManutencaoProcd = dsPeriodicidadeManutencaoProcd;
	}

	/**
	 * Nome: getCdPagamentoNaoUtil
	 *
	 * @exception
	 * @throws
	 * @return cdPagamentoNaoUtil
	 */
	public Integer getCdPagamentoNaoUtil() {
		return cdPagamentoNaoUtil;
	}

	/**
	 * Nome: setCdPagamentoNaoUtil
	 *
	 * @exception
	 * @throws
	 * @param cdPagamentoNaoUtil
	 */
	public void setCdPagamentoNaoUtil(Integer cdPagamentoNaoUtil) {
		this.cdPagamentoNaoUtil = cdPagamentoNaoUtil;
	}

	/**
	 * Nome: getDsPagamentoNaoUtil
	 *
	 * @exception
	 * @throws
	 * @return dsPagamentoNaoUtil
	 */
	public String getDsPagamentoNaoUtil() {
		return dsPagamentoNaoUtil;
	}

	/**
	 * Nome: setDsPagamentoNaoUtil
	 *
	 * @exception
	 * @throws
	 * @param dsPagamentoNaoUtil
	 */
	public void setDsPagamentoNaoUtil(String dsPagamentoNaoUtil) {
		this.dsPagamentoNaoUtil = dsPagamentoNaoUtil;
	}

	/**
	 * Nome: getCdPrincipalEnquaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdPrincipalEnquaRecadastramento
	 */
	public Integer getCdPrincipalEnquaRecadastramento() {
		return cdPrincipalEnquaRecadastramento;
	}

	/**
	 * Nome: setCdPrincipalEnquaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdPrincipalEnquaRecadastramento
	 */
	public void setCdPrincipalEnquaRecadastramento(
			Integer cdPrincipalEnquaRecadastramento) {
		this.cdPrincipalEnquaRecadastramento = cdPrincipalEnquaRecadastramento;
	}

	/**
	 * Nome: getDsPrincipalEnquaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsPrincipalEnquaRecadastramento
	 */
	public String getDsPrincipalEnquaRecadastramento() {
		return dsPrincipalEnquaRecadastramento;
	}

	/**
	 * Nome: setDsPrincipalEnquaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsPrincipalEnquaRecadastramento
	 */
	public void setDsPrincipalEnquaRecadastramento(
			String dsPrincipalEnquaRecadastramento) {
		this.dsPrincipalEnquaRecadastramento = dsPrincipalEnquaRecadastramento;
	}

	/**
	 * Nome: getCdPrioridadeEfetivacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdPrioridadeEfetivacaoPagamento
	 */
	public Integer getCdPrioridadeEfetivacaoPagamento() {
		return cdPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Nome: setCdPrioridadeEfetivacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdPrioridadeEfetivacaoPagamento
	 */
	public void setCdPrioridadeEfetivacaoPagamento(
			Integer cdPrioridadeEfetivacaoPagamento) {
		this.cdPrioridadeEfetivacaoPagamento = cdPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Nome: getDsPrioridadeEfetivacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @return dsPrioridadeEfetivacaoPagamento
	 */
	public String getDsPrioridadeEfetivacaoPagamento() {
		return dsPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Nome: setDsPrioridadeEfetivacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @param dsPrioridadeEfetivacaoPagamento
	 */
	public void setDsPrioridadeEfetivacaoPagamento(
			String dsPrioridadeEfetivacaoPagamento) {
		this.dsPrioridadeEfetivacaoPagamento = dsPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Nome: getCdRejeicaoAgendamentoLote
	 *
	 * @exception
	 * @throws
	 * @return cdRejeicaoAgendamentoLote
	 */
	public Integer getCdRejeicaoAgendamentoLote() {
		return cdRejeicaoAgendamentoLote;
	}

	/**
	 * Nome: setCdRejeicaoAgendamentoLote
	 *
	 * @exception
	 * @throws
	 * @param cdRejeicaoAgendamentoLote
	 */
	public void setCdRejeicaoAgendamentoLote(Integer cdRejeicaoAgendamentoLote) {
		this.cdRejeicaoAgendamentoLote = cdRejeicaoAgendamentoLote;
	}

	/**
	 * Nome: getDsRejeicaoAgendamentoLote
	 *
	 * @exception
	 * @throws
	 * @return dsRejeicaoAgendamentoLote
	 */
	public String getDsRejeicaoAgendamentoLote() {
		return dsRejeicaoAgendamentoLote;
	}

	/**
	 * Nome: setDsRejeicaoAgendamentoLote
	 *
	 * @exception
	 * @throws
	 * @param dsRejeicaoAgendamentoLote
	 */
	public void setDsRejeicaoAgendamentoLote(String dsRejeicaoAgendamentoLote) {
		this.dsRejeicaoAgendamentoLote = dsRejeicaoAgendamentoLote;
	}

	/**
	 * Nome: getCdRejeicaoEfetivacaoLote
	 *
	 * @exception
	 * @throws
	 * @return cdRejeicaoEfetivacaoLote
	 */
	public Integer getCdRejeicaoEfetivacaoLote() {
		return cdRejeicaoEfetivacaoLote;
	}

	/**
	 * Nome: setCdRejeicaoEfetivacaoLote
	 *
	 * @exception
	 * @throws
	 * @param cdRejeicaoEfetivacaoLote
	 */
	public void setCdRejeicaoEfetivacaoLote(Integer cdRejeicaoEfetivacaoLote) {
		this.cdRejeicaoEfetivacaoLote = cdRejeicaoEfetivacaoLote;
	}

	/**
	 * Nome: getDsRejeicaoEfetivacaoLote
	 *
	 * @exception
	 * @throws
	 * @return dsRejeicaoEfetivacaoLote
	 */
	public String getDsRejeicaoEfetivacaoLote() {
		return dsRejeicaoEfetivacaoLote;
	}

	/**
	 * Nome: setDsRejeicaoEfetivacaoLote
	 *
	 * @exception
	 * @throws
	 * @param dsRejeicaoEfetivacaoLote
	 */
	public void setDsRejeicaoEfetivacaoLote(String dsRejeicaoEfetivacaoLote) {
		this.dsRejeicaoEfetivacaoLote = dsRejeicaoEfetivacaoLote;
	}

	/**
	 * Nome: getCdRejeicaoLote
	 *
	 * @exception
	 * @throws
	 * @return cdRejeicaoLote
	 */
	public Integer getCdRejeicaoLote() {
		return cdRejeicaoLote;
	}

	/**
	 * Nome: setCdRejeicaoLote
	 *
	 * @exception
	 * @throws
	 * @param cdRejeicaoLote
	 */
	public void setCdRejeicaoLote(Integer cdRejeicaoLote) {
		this.cdRejeicaoLote = cdRejeicaoLote;
	}

	/**
	 * Nome: getDsRejeicaoLote
	 *
	 * @exception
	 * @throws
	 * @return dsRejeicaoLote
	 */
	public String getDsRejeicaoLote() {
		return dsRejeicaoLote;
	}

	/**
	 * Nome: setDsRejeicaoLote
	 *
	 * @exception
	 * @throws
	 * @param dsRejeicaoLote
	 */
	public void setDsRejeicaoLote(String dsRejeicaoLote) {
		this.dsRejeicaoLote = dsRejeicaoLote;
	}

	/**
	 * Nome: getCdRastreabNotaFiscal
	 *
	 * @exception
	 * @throws
	 * @return cdRastreabNotaFiscal
	 */
	public Integer getCdRastreabNotaFiscal() {
		return cdRastreabNotaFiscal;
	}

	/**
	 * Nome: setCdRastreabNotaFiscal
	 *
	 * @exception
	 * @throws
	 * @param cdRastreabNotaFiscal
	 */
	public void setCdRastreabNotaFiscal(Integer cdRastreabNotaFiscal) {
		this.cdRastreabNotaFiscal = cdRastreabNotaFiscal;
	}

	/**
	 * Nome: getDsRastreabilidadeNotaFiscal
	 *
	 * @exception
	 * @throws
	 * @return dsRastreabilidadeNotaFiscal
	 */
	public String getDsRastreabilidadeNotaFiscal() {
		return dsRastreabilidadeNotaFiscal;
	}

	/**
	 * Nome: setDsRastreabilidadeNotaFiscal
	 *
	 * @exception
	 * @throws
	 * @param dsRastreabilidadeNotaFiscal
	 */
	public void setDsRastreabilidadeNotaFiscal(String dsRastreabilidadeNotaFiscal) {
		this.dsRastreabilidadeNotaFiscal = dsRastreabilidadeNotaFiscal;
	}

	/**
	 * Nome: getCdRastreabTituloTerc
	 *
	 * @exception
	 * @throws
	 * @return cdRastreabTituloTerc
	 */
	public Integer getCdRastreabTituloTerc() {
		return cdRastreabTituloTerc;
	}

	/**
	 * Nome: setCdRastreabTituloTerc
	 *
	 * @exception
	 * @throws
	 * @param cdRastreabTituloTerc
	 */
	public void setCdRastreabTituloTerc(Integer cdRastreabTituloTerc) {
		this.cdRastreabTituloTerc = cdRastreabTituloTerc;
	}

	/**
	 * Nome: getDsRastreabilidadeTituloTerceiro
	 *
	 * @exception
	 * @throws
	 * @return dsRastreabilidadeTituloTerceiro
	 */
	public String getDsRastreabilidadeTituloTerceiro() {
		return dsRastreabilidadeTituloTerceiro;
	}

	/**
	 * Nome: setDsRastreabilidadeTituloTerceiro
	 *
	 * @exception
	 * @throws
	 * @param dsRastreabilidadeTituloTerceiro
	 */
	public void setDsRastreabilidadeTituloTerceiro(
			String dsRastreabilidadeTituloTerceiro) {
		this.dsRastreabilidadeTituloTerceiro = dsRastreabilidadeTituloTerceiro;
	}

	/**
	 * Nome: getCdTipoCargaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return cdTipoCargaRecadastramento
	 */
	public Integer getCdTipoCargaRecadastramento() {
		return cdTipoCargaRecadastramento;
	}

	/**
	 * Nome: setCdTipoCargaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param cdTipoCargaRecadastramento
	 */
	public void setCdTipoCargaRecadastramento(Integer cdTipoCargaRecadastramento) {
		this.cdTipoCargaRecadastramento = cdTipoCargaRecadastramento;
	}

	/**
	 * Nome: getDsTipoCargaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dsTipoCargaRecadastramento
	 */
	public String getDsTipoCargaRecadastramento() {
		return dsTipoCargaRecadastramento;
	}

	/**
	 * Nome: setDsTipoCargaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dsTipoCargaRecadastramento
	 */
	public void setDsTipoCargaRecadastramento(String dsTipoCargaRecadastramento) {
		this.dsTipoCargaRecadastramento = dsTipoCargaRecadastramento;
	}

	/**
	 * Nome: getCdTipoCartaoSalario
	 *
	 * @exception
	 * @throws
	 * @return cdTipoCartaoSalario
	 */
	public Integer getCdTipoCartaoSalario() {
		return cdTipoCartaoSalario;
	}

	/**
	 * Nome: setCdTipoCartaoSalario
	 *
	 * @exception
	 * @throws
	 * @param cdTipoCartaoSalario
	 */
	public void setCdTipoCartaoSalario(Integer cdTipoCartaoSalario) {
		this.cdTipoCartaoSalario = cdTipoCartaoSalario;
	}

	/**
	 * Nome: getDsTipoCartaoSalario
	 *
	 * @exception
	 * @throws
	 * @return dsTipoCartaoSalario
	 */
	public String getDsTipoCartaoSalario() {
		return dsTipoCartaoSalario;
	}

	/**
	 * Nome: setDsTipoCartaoSalario
	 *
	 * @exception
	 * @throws
	 * @param dsTipoCartaoSalario
	 */
	public void setDsTipoCartaoSalario(String dsTipoCartaoSalario) {
		this.dsTipoCartaoSalario = dsTipoCartaoSalario;
	}

	/**
	 * Nome: getCdTipoDataFloating
	 *
	 * @exception
	 * @throws
	 * @return cdTipoDataFloating
	 */
	public Integer getCdTipoDataFloating() {
		return cdTipoDataFloating;
	}

	/**
	 * Nome: setCdTipoDataFloating
	 *
	 * @exception
	 * @throws
	 * @param cdTipoDataFloating
	 */
	public void setCdTipoDataFloating(Integer cdTipoDataFloating) {
		this.cdTipoDataFloating = cdTipoDataFloating;
	}

	/**
	 * Nome: getDsTipoDataFloating
	 *
	 * @exception
	 * @throws
	 * @return dsTipoDataFloating
	 */
	public String getDsTipoDataFloating() {
		return dsTipoDataFloating;
	}

	/**
	 * Nome: setDsTipoDataFloating
	 *
	 * @exception
	 * @throws
	 * @param dsTipoDataFloating
	 */
	public void setDsTipoDataFloating(String dsTipoDataFloating) {
		this.dsTipoDataFloating = dsTipoDataFloating;
	}

	/**
	 * Nome: getCdTipoDivergenciaVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdTipoDivergenciaVeiculo
	 */
	public Integer getCdTipoDivergenciaVeiculo() {
		return cdTipoDivergenciaVeiculo;
	}

	/**
	 * Nome: setCdTipoDivergenciaVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdTipoDivergenciaVeiculo
	 */
	public void setCdTipoDivergenciaVeiculo(Integer cdTipoDivergenciaVeiculo) {
		this.cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
	}

	/**
	 * Nome: getDsTipoDivergenciaVeiculo
	 *
	 * @exception
	 * @throws
	 * @return dsTipoDivergenciaVeiculo
	 */
	public String getDsTipoDivergenciaVeiculo() {
		return dsTipoDivergenciaVeiculo;
	}

	/**
	 * Nome: setDsTipoDivergenciaVeiculo
	 *
	 * @exception
	 * @throws
	 * @param dsTipoDivergenciaVeiculo
	 */
	public void setDsTipoDivergenciaVeiculo(String dsTipoDivergenciaVeiculo) {
		this.dsTipoDivergenciaVeiculo = dsTipoDivergenciaVeiculo;
	}

	/**
	 * Nome: getCdTipoIdentificacaoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return cdTipoIdentificacaoBeneficio
	 */
	public Integer getCdTipoIdentificacaoBeneficio() {
		return cdTipoIdentificacaoBeneficio;
	}

	/**
	 * Nome: setCdTipoIdentificacaoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param cdTipoIdentificacaoBeneficio
	 */
	public void setCdTipoIdentificacaoBeneficio(Integer cdTipoIdentificacaoBeneficio) {
		this.cdTipoIdentificacaoBeneficio = cdTipoIdentificacaoBeneficio;
	}

	/**
	 * Nome: getDsTipoIdentificacaoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return dsTipoIdentificacaoBeneficio
	 */
	public String getDsTipoIdentificacaoBeneficio() {
		return dsTipoIdentificacaoBeneficio;
	}

	/**
	 * Nome: setDsTipoIdentificacaoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param dsTipoIdentificacaoBeneficio
	 */
	public void setDsTipoIdentificacaoBeneficio(String dsTipoIdentificacaoBeneficio) {
		this.dsTipoIdentificacaoBeneficio = dsTipoIdentificacaoBeneficio;
	}

	/**
	 * Nome: getCdTipoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Nome: setCdTipoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @param cdTipoLayoutArquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Nome: getDsTipoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Nome: setDsTipoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @param dsTipoLayoutArquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Nome: getCdTipoReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @return cdTipoReajusteTarifa
	 */
	public Integer getCdTipoReajusteTarifa() {
		return cdTipoReajusteTarifa;
	}

	/**
	 * Nome: setCdTipoReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @param cdTipoReajusteTarifa
	 */
	public void setCdTipoReajusteTarifa(Integer cdTipoReajusteTarifa) {
		this.cdTipoReajusteTarifa = cdTipoReajusteTarifa;
	}

	/**
	 * Nome: getDsTipoReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @return dsTipoReajusteTarifa
	 */
	public String getDsTipoReajusteTarifa() {
		return dsTipoReajusteTarifa;
	}

	/**
	 * Nome: setDsTipoReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @param dsTipoReajusteTarifa
	 */
	public void setDsTipoReajusteTarifa(String dsTipoReajusteTarifa) {
		this.dsTipoReajusteTarifa = dsTipoReajusteTarifa;
	}

	/**
	 * Nome: getCdTratoContaTransf
	 *
	 * @exception
	 * @throws
	 * @return cdTratoContaTransf
	 */
	public Integer getCdTratoContaTransf() {
		return cdTratoContaTransf;
	}

	/**
	 * Nome: setCdTratoContaTransf
	 *
	 * @exception
	 * @throws
	 * @param cdTratoContaTransf
	 */
	public void setCdTratoContaTransf(Integer cdTratoContaTransf) {
		this.cdTratoContaTransf = cdTratoContaTransf;
	}

	/**
	 * Nome: getDsTratamentoContaTransferida
	 *
	 * @exception
	 * @throws
	 * @return dsTratamentoContaTransferida
	 */
	public String getDsTratamentoContaTransferida() {
		return dsTratamentoContaTransferida;
	}

	/**
	 * Nome: setDsTratamentoContaTransferida
	 *
	 * @exception
	 * @throws
	 * @param dsTratamentoContaTransferida
	 */
	public void setDsTratamentoContaTransferida(String dsTratamentoContaTransferida) {
		this.dsTratamentoContaTransferida = dsTratamentoContaTransferida;
	}

	/**
	 * Nome: getCdUtilizacaoFavorecidoControle
	 *
	 * @exception
	 * @throws
	 * @return cdUtilizacaoFavorecidoControle
	 */
	public Integer getCdUtilizacaoFavorecidoControle() {
		return cdUtilizacaoFavorecidoControle;
	}

	/**
	 * Nome: setCdUtilizacaoFavorecidoControle
	 *
	 * @exception
	 * @throws
	 * @param cdUtilizacaoFavorecidoControle
	 */
	public void setCdUtilizacaoFavorecidoControle(
			Integer cdUtilizacaoFavorecidoControle) {
		this.cdUtilizacaoFavorecidoControle = cdUtilizacaoFavorecidoControle;
	}

	/**
	 * Nome: getDsUtilizacaoFavorecidoControle
	 *
	 * @exception
	 * @throws
	 * @return dsUtilizacaoFavorecidoControle
	 */
	public String getDsUtilizacaoFavorecidoControle() {
		return dsUtilizacaoFavorecidoControle;
	}

	/**
	 * Nome: setDsUtilizacaoFavorecidoControle
	 *
	 * @exception
	 * @throws
	 * @param dsUtilizacaoFavorecidoControle
	 */
	public void setDsUtilizacaoFavorecidoControle(
			String dsUtilizacaoFavorecidoControle) {
		this.dsUtilizacaoFavorecidoControle = dsUtilizacaoFavorecidoControle;
	}

	/**
	 * Nome: getDtEnquaContaSalario
	 *
	 * @exception
	 * @throws
	 * @return dtEnquaContaSalario
	 */
	public String getDtEnquaContaSalario() {
		return dtEnquaContaSalario;
	}

	/**
	 * Nome: setDtEnquaContaSalario
	 *
	 * @exception
	 * @throws
	 * @param dtEnquaContaSalario
	 */
	public void setDtEnquaContaSalario(String dtEnquaContaSalario) {
		this.dtEnquaContaSalario = dtEnquaContaSalario;
	}

	/**
	 * Nome: getDtInicioBloqueioPplta
	 *
	 * @exception
	 * @throws
	 * @return dtInicioBloqueioPplta
	 */
	public String getDtInicioBloqueioPplta() {
		return dtInicioBloqueioPplta;
	}

	/**
	 * Nome: setDtInicioBloqueioPplta
	 *
	 * @exception
	 * @throws
	 * @param dtInicioBloqueioPplta
	 */
	public void setDtInicioBloqueioPplta(String dtInicioBloqueioPplta) {
		this.dtInicioBloqueioPplta = dtInicioBloqueioPplta;
	}

	/**
	 * Nome: getDtInicioRastreabTitulo
	 *
	 * @exception
	 * @throws
	 * @return dtInicioRastreabTitulo
	 */
	public String getDtInicioRastreabTitulo() {
		return dtInicioRastreabTitulo;
	}

	/**
	 * Nome: setDtInicioRastreabTitulo
	 *
	 * @exception
	 * @throws
	 * @param dtInicioRastreabTitulo
	 */
	public void setDtInicioRastreabTitulo(String dtInicioRastreabTitulo) {
		this.dtInicioRastreabTitulo = dtInicioRastreabTitulo;
	}

	/**
	 * Nome: getDtFimAcertoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dtFimAcertoRecadastramento
	 */
	public String getDtFimAcertoRecadastramento() {
		return dtFimAcertoRecadastramento;
	}

	/**
	 * Nome: setDtFimAcertoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dtFimAcertoRecadastramento
	 */
	public void setDtFimAcertoRecadastramento(String dtFimAcertoRecadastramento) {
		this.dtFimAcertoRecadastramento = dtFimAcertoRecadastramento;
	}

	/**
	 * Nome: getDtFimRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return dtFimRecadastramentoBeneficio
	 */
	public String getDtFimRecadastramentoBeneficio() {
		return dtFimRecadastramentoBeneficio;
	}

	/**
	 * Nome: setDtFimRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param dtFimRecadastramentoBeneficio
	 */
	public void setDtFimRecadastramentoBeneficio(
			String dtFimRecadastramentoBeneficio) {
		this.dtFimRecadastramentoBeneficio = dtFimRecadastramentoBeneficio;
	}

	/**
	 * Nome: getDtInicioAcertoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return dtInicioAcertoRecadastramento
	 */
	public String getDtInicioAcertoRecadastramento() {
		return dtInicioAcertoRecadastramento;
	}

	/**
	 * Nome: setDtInicioAcertoRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param dtInicioAcertoRecadastramento
	 */
	public void setDtInicioAcertoRecadastramento(
			String dtInicioAcertoRecadastramento) {
		this.dtInicioAcertoRecadastramento = dtInicioAcertoRecadastramento;
	}

	/**
	 * Nome: getDtInicioRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return dtInicioRecadastramentoBeneficio
	 */
	public String getDtInicioRecadastramentoBeneficio() {
		return dtInicioRecadastramentoBeneficio;
	}

	/**
	 * Nome: setDtInicioRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param dtInicioRecadastramentoBeneficio
	 */
	public void setDtInicioRecadastramentoBeneficio(
			String dtInicioRecadastramentoBeneficio) {
		this.dtInicioRecadastramentoBeneficio = dtInicioRecadastramentoBeneficio;
	}

	/**
	 * Nome: getDtLimiteVinculoCarga
	 *
	 * @exception
	 * @throws
	 * @return dtLimiteVinculoCarga
	 */
	public String getDtLimiteVinculoCarga() {
		return dtLimiteVinculoCarga;
	}

	/**
	 * Nome: setDtLimiteVinculoCarga
	 *
	 * @exception
	 * @throws
	 * @param dtLimiteVinculoCarga
	 */
	public void setDtLimiteVinculoCarga(String dtLimiteVinculoCarga) {
		this.dtLimiteVinculoCarga = dtLimiteVinculoCarga;
	}

	/**
	 * Nome: getNrFechamentoApuracaoTarifa
	 *
	 * @exception
	 * @throws
	 * @return nrFechamentoApuracaoTarifa
	 */
	public Integer getNrFechamentoApuracaoTarifa() {
		return nrFechamentoApuracaoTarifa;
	}

	/**
	 * Nome: setNrFechamentoApuracaoTarifa
	 *
	 * @exception
	 * @throws
	 * @param nrFechamentoApuracaoTarifa
	 */
	public void setNrFechamentoApuracaoTarifa(Integer nrFechamentoApuracaoTarifa) {
		this.nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
	}

	/**
	 * Nome: getPercentualIndiceReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @return percentualIndiceReajusteTarifa
	 */
	public BigDecimal getPercentualIndiceReajusteTarifa() {
		return percentualIndiceReajusteTarifa;
	}

	/**
	 * Nome: setPercentualIndiceReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @param percentualIndiceReajusteTarifa
	 */
	public void setPercentualIndiceReajusteTarifa(
			BigDecimal percentualIndiceReajusteTarifa) {
		this.percentualIndiceReajusteTarifa = percentualIndiceReajusteTarifa;
	}

	/**
	 * Nome: getPercentualMaximoInconsistenteLote
	 *
	 * @exception
	 * @throws
	 * @return percentualMaximoInconsistenteLote
	 */
	public Integer getPercentualMaximoInconsistenteLote() {
		return percentualMaximoInconsistenteLote;
	}

	/**
	 * Nome: setPercentualMaximoInconsistenteLote
	 *
	 * @exception
	 * @throws
	 * @param percentualMaximoInconsistenteLote
	 */
	public void setPercentualMaximoInconsistenteLote(
			Integer percentualMaximoInconsistenteLote) {
		this.percentualMaximoInconsistenteLote = percentualMaximoInconsistenteLote;
	}

	/**
	 * Nome: getPeriodicidadeTarifaCatalogo
	 *
	 * @exception
	 * @throws
	 * @return periodicidadeTarifaCatalogo
	 */
	public BigDecimal getPeriodicidadeTarifaCatalogo() {
		return periodicidadeTarifaCatalogo;
	}

	/**
	 * Nome: setPeriodicidadeTarifaCatalogo
	 *
	 * @exception
	 * @throws
	 * @param periodicidadeTarifaCatalogo
	 */
	public void setPeriodicidadeTarifaCatalogo(
			BigDecimal periodicidadeTarifaCatalogo) {
		this.periodicidadeTarifaCatalogo = periodicidadeTarifaCatalogo;
	}

	/**
	 * Nome: getQuantidadeAntecedencia
	 *
	 * @exception
	 * @throws
	 * @return quantidadeAntecedencia
	 */
	public Integer getQuantidadeAntecedencia() {
		return quantidadeAntecedencia;
	}

	/**
	 * Nome: setQuantidadeAntecedencia
	 *
	 * @exception
	 * @throws
	 * @param quantidadeAntecedencia
	 */
	public void setQuantidadeAntecedencia(Integer quantidadeAntecedencia) {
		this.quantidadeAntecedencia = quantidadeAntecedencia;
	}

	/**
	 * Nome: getQuantidadeAnteriorVencimentoComprovante
	 *
	 * @exception
	 * @throws
	 * @return quantidadeAnteriorVencimentoComprovante
	 */
	public Integer getQuantidadeAnteriorVencimentoComprovante() {
		return quantidadeAnteriorVencimentoComprovante;
	}

	/**
	 * Nome: setQuantidadeAnteriorVencimentoComprovante
	 *
	 * @exception
	 * @throws
	 * @param quantidadeAnteriorVencimentoComprovante
	 */
	public void setQuantidadeAnteriorVencimentoComprovante(
			Integer quantidadeAnteriorVencimentoComprovante) {
		this.quantidadeAnteriorVencimentoComprovante = quantidadeAnteriorVencimentoComprovante;
	}

	/**
	 * Nome: getQuantidadeDiaCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @return quantidadeDiaCobrancaTarifa
	 */
	public Integer getQuantidadeDiaCobrancaTarifa() {
		return quantidadeDiaCobrancaTarifa;
	}

	/**
	 * Nome: setQuantidadeDiaCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @param quantidadeDiaCobrancaTarifa
	 */
	public void setQuantidadeDiaCobrancaTarifa(Integer quantidadeDiaCobrancaTarifa) {
		this.quantidadeDiaCobrancaTarifa = quantidadeDiaCobrancaTarifa;
	}

	/**
	 * Nome: getQuantidadeDiaExpiracao
	 *
	 * @exception
	 * @throws
	 * @return quantidadeDiaExpiracao
	 */
	public Integer getQuantidadeDiaExpiracao() {
		return quantidadeDiaExpiracao;
	}

	/**
	 * Nome: setQuantidadeDiaExpiracao
	 *
	 * @exception
	 * @throws
	 * @param quantidadeDiaExpiracao
	 */
	public void setQuantidadeDiaExpiracao(Integer quantidadeDiaExpiracao) {
		this.quantidadeDiaExpiracao = quantidadeDiaExpiracao;
	}

	/**
	 * Nome: getQuantidadeDiaFloatingPagamento
	 *
	 * @exception
	 * @throws
	 * @return quantidadeDiaFloatingPagamento
	 */
	public Integer getQuantidadeDiaFloatingPagamento() {
		return quantidadeDiaFloatingPagamento;
	}

	/**
	 * Nome: setQuantidadeDiaFloatingPagamento
	 *
	 * @exception
	 * @throws
	 * @param quantidadeDiaFloatingPagamento
	 */
	public void setQuantidadeDiaFloatingPagamento(
			Integer quantidadeDiaFloatingPagamento) {
		this.quantidadeDiaFloatingPagamento = quantidadeDiaFloatingPagamento;
	}

	/**
	 * Nome: getQuantidadeDiaInatividadeFavorecido
	 *
	 * @exception
	 * @throws
	 * @return quantidadeDiaInatividadeFavorecido
	 */
	public Integer getQuantidadeDiaInatividadeFavorecido() {
		return quantidadeDiaInatividadeFavorecido;
	}

	/**
	 * Nome: setQuantidadeDiaInatividadeFavorecido
	 *
	 * @exception
	 * @throws
	 * @param quantidadeDiaInatividadeFavorecido
	 */
	public void setQuantidadeDiaInatividadeFavorecido(
			Integer quantidadeDiaInatividadeFavorecido) {
		this.quantidadeDiaInatividadeFavorecido = quantidadeDiaInatividadeFavorecido;
	}

	/**
	 * Nome: getQuantidadeDiaRepiqueConsulta
	 *
	 * @exception
	 * @throws
	 * @return quantidadeDiaRepiqueConsulta
	 */
	public Integer getQuantidadeDiaRepiqueConsulta() {
		return quantidadeDiaRepiqueConsulta;
	}

	/**
	 * Nome: setQuantidadeDiaRepiqueConsulta
	 *
	 * @exception
	 * @throws
	 * @param quantidadeDiaRepiqueConsulta
	 */
	public void setQuantidadeDiaRepiqueConsulta(Integer quantidadeDiaRepiqueConsulta) {
		this.quantidadeDiaRepiqueConsulta = quantidadeDiaRepiqueConsulta;
	}

	/**
	 * Nome: getQuantidadeEtapaRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return quantidadeEtapaRecadastramentoBeneficio
	 */
	public Integer getQuantidadeEtapaRecadastramentoBeneficio() {
		return quantidadeEtapaRecadastramentoBeneficio;
	}

	/**
	 * Nome: setQuantidadeEtapaRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param quantidadeEtapaRecadastramentoBeneficio
	 */
	public void setQuantidadeEtapaRecadastramentoBeneficio(
			Integer quantidadeEtapaRecadastramentoBeneficio) {
		this.quantidadeEtapaRecadastramentoBeneficio = quantidadeEtapaRecadastramentoBeneficio;
	}

	/**
	 * Nome: getQuantidadeFaseRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @return quantidadeFaseRecadastramentoBeneficio
	 */
	public Integer getQuantidadeFaseRecadastramentoBeneficio() {
		return quantidadeFaseRecadastramentoBeneficio;
	}

	/**
	 * Nome: setQuantidadeFaseRecadastramentoBeneficio
	 *
	 * @exception
	 * @throws
	 * @param quantidadeFaseRecadastramentoBeneficio
	 */
	public void setQuantidadeFaseRecadastramentoBeneficio(
			Integer quantidadeFaseRecadastramentoBeneficio) {
		this.quantidadeFaseRecadastramentoBeneficio = quantidadeFaseRecadastramentoBeneficio;
	}

	/**
	 * Nome: getQuantidadeLimiteLinha
	 *
	 * @exception
	 * @throws
	 * @return quantidadeLimiteLinha
	 */
	public Integer getQuantidadeLimiteLinha() {
		return quantidadeLimiteLinha;
	}

	/**
	 * Nome: setQuantidadeLimiteLinha
	 *
	 * @exception
	 * @throws
	 * @param quantidadeLimiteLinha
	 */
	public void setQuantidadeLimiteLinha(Integer quantidadeLimiteLinha) {
		this.quantidadeLimiteLinha = quantidadeLimiteLinha;
	}

	/**
	 * Nome: getQuantidadeSolicitacaoCartao
	 *
	 * @exception
	 * @throws
	 * @return quantidadeSolicitacaoCartao
	 */
	public Integer getQuantidadeSolicitacaoCartao() {
		return quantidadeSolicitacaoCartao;
	}

	/**
	 * Nome: setQuantidadeSolicitacaoCartao
	 *
	 * @exception
	 * @throws
	 * @param quantidadeSolicitacaoCartao
	 */
	public void setQuantidadeSolicitacaoCartao(Integer quantidadeSolicitacaoCartao) {
		this.quantidadeSolicitacaoCartao = quantidadeSolicitacaoCartao;
	}

	/**
	 * Nome: getQuantidadeMaximaInconsistenteLote
	 *
	 * @exception
	 * @throws
	 * @return quantidadeMaximaInconsistenteLote
	 */
	public Integer getQuantidadeMaximaInconsistenteLote() {
		return quantidadeMaximaInconsistenteLote;
	}

	/**
	 * Nome: setQuantidadeMaximaInconsistenteLote
	 *
	 * @exception
	 * @throws
	 * @param quantidadeMaximaInconsistenteLote
	 */
	public void setQuantidadeMaximaInconsistenteLote(
			Integer quantidadeMaximaInconsistenteLote) {
		this.quantidadeMaximaInconsistenteLote = quantidadeMaximaInconsistenteLote;
	}

	/**
	 * Nome: getQuantidadeMaximaTituloVencido
	 *
	 * @exception
	 * @throws
	 * @return quantidadeMaximaTituloVencido
	 */
	public Integer getQuantidadeMaximaTituloVencido() {
		return quantidadeMaximaTituloVencido;
	}

	/**
	 * Nome: setQuantidadeMaximaTituloVencido
	 *
	 * @exception
	 * @throws
	 * @param quantidadeMaximaTituloVencido
	 */
	public void setQuantidadeMaximaTituloVencido(
			Integer quantidadeMaximaTituloVencido) {
		this.quantidadeMaximaTituloVencido = quantidadeMaximaTituloVencido;
	}

	/**
	 * Nome: getQuantidadeMesComprovante
	 *
	 * @exception
	 * @throws
	 * @return quantidadeMesComprovante
	 */
	public Integer getQuantidadeMesComprovante() {
		return quantidadeMesComprovante;
	}

	/**
	 * Nome: setQuantidadeMesComprovante
	 *
	 * @exception
	 * @throws
	 * @param quantidadeMesComprovante
	 */
	public void setQuantidadeMesComprovante(Integer quantidadeMesComprovante) {
		this.quantidadeMesComprovante = quantidadeMesComprovante;
	}

	/**
	 * Nome: getQuantidadeMesEtapaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return quantidadeMesEtapaRecadastramento
	 */
	public Integer getQuantidadeMesEtapaRecadastramento() {
		return quantidadeMesEtapaRecadastramento;
	}

	/**
	 * Nome: setQuantidadeMesEtapaRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param quantidadeMesEtapaRecadastramento
	 */
	public void setQuantidadeMesEtapaRecadastramento(
			Integer quantidadeMesEtapaRecadastramento) {
		this.quantidadeMesEtapaRecadastramento = quantidadeMesEtapaRecadastramento;
	}

	/**
	 * Nome: getQuantidadeMesFaseRecadastramento
	 *
	 * @exception
	 * @throws
	 * @return quantidadeMesFaseRecadastramento
	 */
	public Integer getQuantidadeMesFaseRecadastramento() {
		return quantidadeMesFaseRecadastramento;
	}

	/**
	 * Nome: setQuantidadeMesFaseRecadastramento
	 *
	 * @exception
	 * @throws
	 * @param quantidadeMesFaseRecadastramento
	 */
	public void setQuantidadeMesFaseRecadastramento(
			Integer quantidadeMesFaseRecadastramento) {
		this.quantidadeMesFaseRecadastramento = quantidadeMesFaseRecadastramento;
	}

	/**
	 * Nome: getQuantidadeMesReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @return quantidadeMesReajusteTarifa
	 */
	public Integer getQuantidadeMesReajusteTarifa() {
		return quantidadeMesReajusteTarifa;
	}

	/**
	 * Nome: setQuantidadeMesReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @param quantidadeMesReajusteTarifa
	 */
	public void setQuantidadeMesReajusteTarifa(Integer quantidadeMesReajusteTarifa) {
		this.quantidadeMesReajusteTarifa = quantidadeMesReajusteTarifa;
	}

	/**
	 * Nome: getQuantidadeViaAviso
	 *
	 * @exception
	 * @throws
	 * @return quantidadeViaAviso
	 */
	public Integer getQuantidadeViaAviso() {
		return quantidadeViaAviso;
	}

	/**
	 * Nome: setQuantidadeViaAviso
	 *
	 * @exception
	 * @throws
	 * @param quantidadeViaAviso
	 */
	public void setQuantidadeViaAviso(Integer quantidadeViaAviso) {
		this.quantidadeViaAviso = quantidadeViaAviso;
	}

	/**
	 * Nome: getQuantidadeViaCombranca
	 *
	 * @exception
	 * @throws
	 * @return quantidadeViaCombranca
	 */
	public Integer getQuantidadeViaCombranca() {
		return quantidadeViaCombranca;
	}

	/**
	 * Nome: setQuantidadeViaCombranca
	 *
	 * @exception
	 * @throws
	 * @param quantidadeViaCombranca
	 */
	public void setQuantidadeViaCombranca(Integer quantidadeViaCombranca) {
		this.quantidadeViaCombranca = quantidadeViaCombranca;
	}

	/**
	 * Nome: getQuantidadeViaComprovante
	 *
	 * @exception
	 * @throws
	 * @return quantidadeViaComprovante
	 */
	public Integer getQuantidadeViaComprovante() {
		return quantidadeViaComprovante;
	}

	/**
	 * Nome: setQuantidadeViaComprovante
	 *
	 * @exception
	 * @throws
	 * @param quantidadeViaComprovante
	 */
	public void setQuantidadeViaComprovante(Integer quantidadeViaComprovante) {
		this.quantidadeViaComprovante = quantidadeViaComprovante;
	}

	/**
	 * Nome: getValorFavorecidoNaoCadastrado
	 *
	 * @exception
	 * @throws
	 * @return valorFavorecidoNaoCadastrado
	 */
	public BigDecimal getValorFavorecidoNaoCadastrado() {
		return valorFavorecidoNaoCadastrado;
	}

	/**
	 * Nome: setValorFavorecidoNaoCadastrado
	 *
	 * @exception
	 * @throws
	 * @param valorFavorecidoNaoCadastrado
	 */
	public void setValorFavorecidoNaoCadastrado(
			BigDecimal valorFavorecidoNaoCadastrado) {
		this.valorFavorecidoNaoCadastrado = valorFavorecidoNaoCadastrado;
	}

	/**
	 * Nome: getValorLimiteDiarioPagamento
	 *
	 * @exception
	 * @throws
	 * @return valorLimiteDiarioPagamento
	 */
	public BigDecimal getValorLimiteDiarioPagamento() {
		return valorLimiteDiarioPagamento;
	}

	/**
	 * Nome: setValorLimiteDiarioPagamento
	 *
	 * @exception
	 * @throws
	 * @param valorLimiteDiarioPagamento
	 */
	public void setValorLimiteDiarioPagamento(BigDecimal valorLimiteDiarioPagamento) {
		this.valorLimiteDiarioPagamento = valorLimiteDiarioPagamento;
	}

	/**
	 * Nome: getValorLimiteIndividualPagamento
	 *
	 * @exception
	 * @throws
	 * @return valorLimiteIndividualPagamento
	 */
	public BigDecimal getValorLimiteIndividualPagamento() {
		return valorLimiteIndividualPagamento;
	}

	/**
	 * Nome: setValorLimiteIndividualPagamento
	 *
	 * @exception
	 * @throws
	 * @param valorLimiteIndividualPagamento
	 */
	public void setValorLimiteIndividualPagamento(
			BigDecimal valorLimiteIndividualPagamento) {
		this.valorLimiteIndividualPagamento = valorLimiteIndividualPagamento;
	}

	/**
	 * Nome: getCdIndicadorEmissaoAviso
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorEmissaoAviso
	 */
	public Integer getCdIndicadorEmissaoAviso() {
		return cdIndicadorEmissaoAviso;
	}

	/**
	 * Nome: setCdIndicadorEmissaoAviso
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorEmissaoAviso
	 */
	public void setCdIndicadorEmissaoAviso(Integer cdIndicadorEmissaoAviso) {
		this.cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
	}

	/**
	 * Nome: getDsIndicadorEmissaoAviso
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorEmissaoAviso
	 */
	public String getDsIndicadorEmissaoAviso() {
		return dsIndicadorEmissaoAviso;
	}

	/**
	 * Nome: setDsIndicadorEmissaoAviso
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorEmissaoAviso
	 */
	public void setDsIndicadorEmissaoAviso(String dsIndicadorEmissaoAviso) {
		this.dsIndicadorEmissaoAviso = dsIndicadorEmissaoAviso;
	}

	/**
	 * Nome: getCdIndicadorRetornoInternet
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorRetornoInternet
	 */
	public Integer getCdIndicadorRetornoInternet() {
		return cdIndicadorRetornoInternet;
	}

	/**
	 * Nome: setCdIndicadorRetornoInternet
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorRetornoInternet
	 */
	public void setCdIndicadorRetornoInternet(Integer cdIndicadorRetornoInternet) {
		this.cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
	}

	/**
	 * Nome: getDsIndicadorRetornoInternet
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorRetornoInternet
	 */
	public String getDsIndicadorRetornoInternet() {
		return dsIndicadorRetornoInternet;
	}

	/**
	 * Nome: setDsIndicadorRetornoInternet
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorRetornoInternet
	 */
	public void setDsIndicadorRetornoInternet(String dsIndicadorRetornoInternet) {
		this.dsIndicadorRetornoInternet = dsIndicadorRetornoInternet;
	}

	/**
	 * Nome: getCdIndicadorRetornoSeparado
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorRetornoSeparado
	 */
	public Integer getCdIndicadorRetornoSeparado() {
		return cdIndicadorRetornoSeparado;
	}

	/**
	 * Nome: setCdIndicadorRetornoSeparado
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorRetornoSeparado
	 */
	public void setCdIndicadorRetornoSeparado(Integer cdIndicadorRetornoSeparado) {
		this.cdIndicadorRetornoSeparado = cdIndicadorRetornoSeparado;
	}

	/**
	 * Nome: getDsCodigoIndicadorRetornoSeparado
	 *
	 * @exception
	 * @throws
	 * @return dsCodigoIndicadorRetornoSeparado
	 */
	public String getDsCodigoIndicadorRetornoSeparado() {
		return dsCodigoIndicadorRetornoSeparado;
	}

	/**
	 * Nome: setDsCodigoIndicadorRetornoSeparado
	 *
	 * @exception
	 * @throws
	 * @param dsCodigoIndicadorRetornoSeparado
	 */
	public void setDsCodigoIndicadorRetornoSeparado(
			String dsCodigoIndicadorRetornoSeparado) {
		this.dsCodigoIndicadorRetornoSeparado = dsCodigoIndicadorRetornoSeparado;
	}

	/**
	 * Nome: getCdIndicadorListaDebito
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorListaDebito
	 */
	public Integer getCdIndicadorListaDebito() {
		return cdIndicadorListaDebito;
	}

	/**
	 * Nome: setCdIndicadorListaDebito
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorListaDebito
	 */
	public void setCdIndicadorListaDebito(Integer cdIndicadorListaDebito) {
		this.cdIndicadorListaDebito = cdIndicadorListaDebito;
	}

	/**
	 * Nome: getDsIndicadorListaDebito
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorListaDebito
	 */
	public String getDsIndicadorListaDebito() {
		return dsIndicadorListaDebito;
	}

	/**
	 * Nome: setDsIndicadorListaDebito
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorListaDebito
	 */
	public void setDsIndicadorListaDebito(String dsIndicadorListaDebito) {
		this.dsIndicadorListaDebito = dsIndicadorListaDebito;
	}

	/**
	 * Nome: getCdTipoFormacaoLista
	 *
	 * @exception
	 * @throws
	 * @return cdTipoFormacaoLista
	 */
	public Integer getCdTipoFormacaoLista() {
		return cdTipoFormacaoLista;
	}

	/**
	 * Nome: setCdTipoFormacaoLista
	 *
	 * @exception
	 * @throws
	 * @param cdTipoFormacaoLista
	 */
	public void setCdTipoFormacaoLista(Integer cdTipoFormacaoLista) {
		this.cdTipoFormacaoLista = cdTipoFormacaoLista;
	}

	/**
	 * Nome: getDsTipoFormacaoLista
	 *
	 * @exception
	 * @throws
	 * @return dsTipoFormacaoLista
	 */
	public String getDsTipoFormacaoLista() {
		return dsTipoFormacaoLista;
	}

	/**
	 * Nome: setDsTipoFormacaoLista
	 *
	 * @exception
	 * @throws
	 * @param dsTipoFormacaoLista
	 */
	public void setDsTipoFormacaoLista(String dsTipoFormacaoLista) {
		this.dsTipoFormacaoLista = dsTipoFormacaoLista;
	}

	/**
	 * Nome: getCdTipoConsistenciaLista
	 *
	 * @exception
	 * @throws
	 * @return cdTipoConsistenciaLista
	 */
	public Integer getCdTipoConsistenciaLista() {
		return cdTipoConsistenciaLista;
	}

	/**
	 * Nome: setCdTipoConsistenciaLista
	 *
	 * @exception
	 * @throws
	 * @param cdTipoConsistenciaLista
	 */
	public void setCdTipoConsistenciaLista(Integer cdTipoConsistenciaLista) {
		this.cdTipoConsistenciaLista = cdTipoConsistenciaLista;
	}

	/**
	 * Nome: getDsTipoConsistenciaLista
	 *
	 * @exception
	 * @throws
	 * @return dsTipoConsistenciaLista
	 */
	public String getDsTipoConsistenciaLista() {
		return dsTipoConsistenciaLista;
	}

	/**
	 * Nome: setDsTipoConsistenciaLista
	 *
	 * @exception
	 * @throws
	 * @param dsTipoConsistenciaLista
	 */
	public void setDsTipoConsistenciaLista(String dsTipoConsistenciaLista) {
		this.dsTipoConsistenciaLista = dsTipoConsistenciaLista;
	}

	/**
	 * Nome: getCdTipoConsultaComprovante
	 *
	 * @exception
	 * @throws
	 * @return cdTipoConsultaComprovante
	 */
	public Integer getCdTipoConsultaComprovante() {
		return cdTipoConsultaComprovante;
	}

	/**
	 * Nome: setCdTipoConsultaComprovante
	 *
	 * @exception
	 * @throws
	 * @param cdTipoConsultaComprovante
	 */
	public void setCdTipoConsultaComprovante(Integer cdTipoConsultaComprovante) {
		this.cdTipoConsultaComprovante = cdTipoConsultaComprovante;
	}

	/**
	 * Nome: getDsTipoConsolidacaoComprovante
	 *
	 * @exception
	 * @throws
	 * @return dsTipoConsolidacaoComprovante
	 */
	public String getDsTipoConsolidacaoComprovante() {
		return dsTipoConsolidacaoComprovante;
	}

	/**
	 * Nome: setDsTipoConsolidacaoComprovante
	 *
	 * @exception
	 * @throws
	 * @param dsTipoConsolidacaoComprovante
	 */
	public void setDsTipoConsolidacaoComprovante(
			String dsTipoConsolidacaoComprovante) {
		this.dsTipoConsolidacaoComprovante = dsTipoConsolidacaoComprovante;
	}

	/**
	 * Nome: getCdValidacaoNomeFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdValidacaoNomeFavorecido
	 */
	public Integer getCdValidacaoNomeFavorecido() {
		return cdValidacaoNomeFavorecido;
	}

	/**
	 * Nome: setCdValidacaoNomeFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdValidacaoNomeFavorecido
	 */
	public void setCdValidacaoNomeFavorecido(Integer cdValidacaoNomeFavorecido) {
		this.cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
	}

	/**
	 * Nome: getDsValidacaoNomeFavorecido
	 *
	 * @exception
	 * @throws
	 * @return dsValidacaoNomeFavorecido
	 */
	public String getDsValidacaoNomeFavorecido() {
		return dsValidacaoNomeFavorecido;
	}

	/**
	 * Nome: setDsValidacaoNomeFavorecido
	 *
	 * @exception
	 * @throws
	 * @param dsValidacaoNomeFavorecido
	 */
	public void setDsValidacaoNomeFavorecido(String dsValidacaoNomeFavorecido) {
		this.dsValidacaoNomeFavorecido = dsValidacaoNomeFavorecido;
	}

	/**
	 * Nome: getCdTipoConsistenciaFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdTipoConsistenciaFavorecido
	 */
	public Integer getCdTipoConsistenciaFavorecido() {
		return cdTipoConsistenciaFavorecido;
	}

	/**
	 * Nome: setCdTipoConsistenciaFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdTipoConsistenciaFavorecido
	 */
	public void setCdTipoConsistenciaFavorecido(Integer cdTipoConsistenciaFavorecido) {
		this.cdTipoConsistenciaFavorecido = cdTipoConsistenciaFavorecido;
	}

	/**
	 * Nome: getDsTipoConsistenciaFavorecido
	 *
	 * @exception
	 * @throws
	 * @return dsTipoConsistenciaFavorecido
	 */
	public String getDsTipoConsistenciaFavorecido() {
		return dsTipoConsistenciaFavorecido;
	}

	/**
	 * Nome: setDsTipoConsistenciaFavorecido
	 *
	 * @exception
	 * @throws
	 * @param dsTipoConsistenciaFavorecido
	 */
	public void setDsTipoConsistenciaFavorecido(String dsTipoConsistenciaFavorecido) {
		this.dsTipoConsistenciaFavorecido = dsTipoConsistenciaFavorecido;
	}

	/**
	 * Nome: getCdIndLancamentoPersonalizado
	 *
	 * @exception
	 * @throws
	 * @return cdIndLancamentoPersonalizado
	 */
	public Integer getCdIndLancamentoPersonalizado() {
		return cdIndLancamentoPersonalizado;
	}

	/**
	 * Nome: setCdIndLancamentoPersonalizado
	 *
	 * @exception
	 * @throws
	 * @param cdIndLancamentoPersonalizado
	 */
	public void setCdIndLancamentoPersonalizado(Integer cdIndLancamentoPersonalizado) {
		this.cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
	}

	/**
	 * Nome: getDsIndLancamentoPersonalizado
	 *
	 * @exception
	 * @throws
	 * @return dsIndLancamentoPersonalizado
	 */
	public String getDsIndLancamentoPersonalizado() {
		return dsIndLancamentoPersonalizado;
	}

	/**
	 * Nome: setDsIndLancamentoPersonalizado
	 *
	 * @exception
	 * @throws
	 * @param dsIndLancamentoPersonalizado
	 */
	public void setDsIndLancamentoPersonalizado(String dsIndLancamentoPersonalizado) {
		this.dsIndLancamentoPersonalizado = dsIndLancamentoPersonalizado;
	}

	/**
	 * Nome: getCdAgendaRastreabilidadeFilial
	 *
	 * @exception
	 * @throws
	 * @return cdAgendaRastreabilidadeFilial
	 */
	public Integer getCdAgendaRastreabilidadeFilial() {
		return cdAgendaRastreabilidadeFilial;
	}

	/**
	 * Nome: setCdAgendaRastreabilidadeFilial
	 *
	 * @exception
	 * @throws
	 * @param cdAgendaRastreabilidadeFilial
	 */
	public void setCdAgendaRastreabilidadeFilial(
			Integer cdAgendaRastreabilidadeFilial) {
		this.cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
	}

	/**
	 * Nome: getDsAgendamentoRastFilial
	 *
	 * @exception
	 * @throws
	 * @return dsAgendamentoRastFilial
	 */
	public String getDsAgendamentoRastFilial() {
		return dsAgendamentoRastFilial;
	}

	/**
	 * Nome: setDsAgendamentoRastFilial
	 *
	 * @exception
	 * @throws
	 * @param dsAgendamentoRastFilial
	 */
	public void setDsAgendamentoRastFilial(String dsAgendamentoRastFilial) {
		this.dsAgendamentoRastFilial = dsAgendamentoRastFilial;
	}

	/**
	 * Nome: getCdIndicadorAdesaoSacador
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAdesaoSacador
	 */
	public Integer getCdIndicadorAdesaoSacador() {
		return cdIndicadorAdesaoSacador;
	}

	/**
	 * Nome: setCdIndicadorAdesaoSacador
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAdesaoSacador
	 */
	public void setCdIndicadorAdesaoSacador(Integer cdIndicadorAdesaoSacador) {
		this.cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
	}

	/**
	 * Nome: getDsIndicadorAdesaoSacador
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorAdesaoSacador
	 */
	public String getDsIndicadorAdesaoSacador() {
		return dsIndicadorAdesaoSacador;
	}

	/**
	 * Nome: setDsIndicadorAdesaoSacador
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorAdesaoSacador
	 */
	public void setDsIndicadorAdesaoSacador(String dsIndicadorAdesaoSacador) {
		this.dsIndicadorAdesaoSacador = dsIndicadorAdesaoSacador;
	}

	/**
	 * Nome: getCdMeioPagamentoCredito
	 *
	 * @exception
	 * @throws
	 * @return cdMeioPagamentoCredito
	 */
	public Integer getCdMeioPagamentoCredito() {
		return cdMeioPagamentoCredito;
	}

	/**
	 * Nome: setCdMeioPagamentoCredito
	 *
	 * @exception
	 * @throws
	 * @param cdMeioPagamentoCredito
	 */
	public void setCdMeioPagamentoCredito(Integer cdMeioPagamentoCredito) {
		this.cdMeioPagamentoCredito = cdMeioPagamentoCredito;
	}

	/**
	 * Nome: getDsMeioPagamentoCredito
	 *
	 * @exception
	 * @throws
	 * @return dsMeioPagamentoCredito
	 */
	public String getDsMeioPagamentoCredito() {
		return dsMeioPagamentoCredito;
	}

	/**
	 * Nome: setDsMeioPagamentoCredito
	 *
	 * @exception
	 * @throws
	 * @param dsMeioPagamentoCredito
	 */
	public void setDsMeioPagamentoCredito(String dsMeioPagamentoCredito) {
		this.dsMeioPagamentoCredito = dsMeioPagamentoCredito;
	}

	/**
	 * Nome: getCdTipoInscricaoFavorecidoAceita
	 *
	 * @exception
	 * @throws
	 * @return cdTipoInscricaoFavorecidoAceita
	 */
	public Integer getCdTipoInscricaoFavorecidoAceita() {
		return cdTipoInscricaoFavorecidoAceita;
	}

	/**
	 * Nome: setCdTipoInscricaoFavorecidoAceita
	 *
	 * @exception
	 * @throws
	 * @param cdTipoInscricaoFavorecidoAceita
	 */
	public void setCdTipoInscricaoFavorecidoAceita(
			Integer cdTipoInscricaoFavorecidoAceita) {
		this.cdTipoInscricaoFavorecidoAceita = cdTipoInscricaoFavorecidoAceita;
	}

	/**
	 * Nome: getDsTipoIscricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @return dsTipoIscricaoFavorecido
	 */
	public String getDsTipoIscricaoFavorecido() {
		return dsTipoIscricaoFavorecido;
	}

	/**
	 * Nome: setDsTipoIscricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @param dsTipoIscricaoFavorecido
	 */
	public void setDsTipoIscricaoFavorecido(String dsTipoIscricaoFavorecido) {
		this.dsTipoIscricaoFavorecido = dsTipoIscricaoFavorecido;
	}

	/**
	 * Nome: getCdIndicadorBancoPostal
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorBancoPostal
	 */
	public Integer getCdIndicadorBancoPostal() {
		return cdIndicadorBancoPostal;
	}

	/**
	 * Nome: setCdIndicadorBancoPostal
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorBancoPostal
	 */
	public void setCdIndicadorBancoPostal(Integer cdIndicadorBancoPostal) {
		this.cdIndicadorBancoPostal = cdIndicadorBancoPostal;
	}

	/**
	 * Nome: getDsIndicadorBancoPostal
	 *
	 * @exception
	 * @throws
	 * @return dsIndicadorBancoPostal
	 */
	public String getDsIndicadorBancoPostal() {
		return dsIndicadorBancoPostal;
	}

	/**
	 * Nome: setDsIndicadorBancoPostal
	 *
	 * @exception
	 * @throws
	 * @param dsIndicadorBancoPostal
	 */
	public void setDsIndicadorBancoPostal(String dsIndicadorBancoPostal) {
		this.dsIndicadorBancoPostal = dsIndicadorBancoPostal;
	}

	/**
	 * Nome: getCdFormularioContratoCliente
	 *
	 * @exception
	 * @throws
	 * @return cdFormularioContratoCliente
	 */
	public Integer getCdFormularioContratoCliente() {
		return cdFormularioContratoCliente;
	}

	/**
	 * Nome: setCdFormularioContratoCliente
	 *
	 * @exception
	 * @throws
	 * @param cdFormularioContratoCliente
	 */
	public void setCdFormularioContratoCliente(Integer cdFormularioContratoCliente) {
		this.cdFormularioContratoCliente = cdFormularioContratoCliente;
	}

	/**
	 * Nome: getDsCodigoFormularioContratoCliente
	 *
	 * @exception
	 * @throws
	 * @return dsCodigoFormularioContratoCliente
	 */
	public String getDsCodigoFormularioContratoCliente() {
		return dsCodigoFormularioContratoCliente;
	}

	/**
	 * Nome: setDsCodigoFormularioContratoCliente
	 *
	 * @exception
	 * @throws
	 * @param dsCodigoFormularioContratoCliente
	 */
	public void setDsCodigoFormularioContratoCliente(
			String dsCodigoFormularioContratoCliente) {
		this.dsCodigoFormularioContratoCliente = dsCodigoFormularioContratoCliente;
	}

	/**
	 * Nome: getDsLocalEmissao
	 *
	 * @exception
	 * @throws
	 * @return dsLocalEmissao
	 */
	public String getDsLocalEmissao() {
		return dsLocalEmissao;
	}

	/**
	 * Nome: setDsLocalEmissao
	 *
	 * @exception
	 * @throws
	 * @param dsLocalEmissao
	 */
	public void setDsLocalEmissao(String dsLocalEmissao) {
		this.dsLocalEmissao = dsLocalEmissao;
	}

	/**
	 * Nome: getCdUsuarioInclusao
	 *
	 * @exception
	 * @throws
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Nome: setCdUsuarioInclusao
	 *
	 * @exception
	 * @throws
	 * @param cdUsuarioInclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Nome: getCdUsuarioExternoInclusao
	 *
	 * @exception
	 * @throws
	 * @return cdUsuarioExternoInclusao
	 */
	public String getCdUsuarioExternoInclusao() {
		return cdUsuarioExternoInclusao;
	}

	/**
	 * Nome: setCdUsuarioExternoInclusao
	 *
	 * @exception
	 * @throws
	 * @param cdUsuarioExternoInclusao
	 */
	public void setCdUsuarioExternoInclusao(String cdUsuarioExternoInclusao) {
		this.cdUsuarioExternoInclusao = cdUsuarioExternoInclusao;
	}

	/**
	 * Nome: getHrManutencaoRegistroInclusao
	 *
	 * @exception
	 * @throws
	 * @return hrManutencaoRegistroInclusao
	 */
	public String getHrManutencaoRegistroInclusao() {
		return hrManutencaoRegistroInclusao;
	}

	/**
	 * Nome: setHrManutencaoRegistroInclusao
	 *
	 * @exception
	 * @throws
	 * @param hrManutencaoRegistroInclusao
	 */
	public void setHrManutencaoRegistroInclusao(String hrManutencaoRegistroInclusao) {
		this.hrManutencaoRegistroInclusao = hrManutencaoRegistroInclusao;
	}

	/**
	 * Nome: getCdCanalInclusao
	 *
	 * @exception
	 * @throws
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Nome: setCdCanalInclusao
	 *
	 * @exception
	 * @throws
	 * @param cdCanalInclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Nome: getDsCanalInclusao
	 *
	 * @exception
	 * @throws
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Nome: setDsCanalInclusao
	 *
	 * @exception
	 * @throws
	 * @param dsCanalInclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Nome: getNrOperacaoFluxoInclusao
	 *
	 * @exception
	 * @throws
	 * @return nrOperacaoFluxoInclusao
	 */
	public String getNrOperacaoFluxoInclusao() {
		return nrOperacaoFluxoInclusao;
	}

	/**
	 * Nome: setNrOperacaoFluxoInclusao
	 *
	 * @exception
	 * @throws
	 * @param nrOperacaoFluxoInclusao
	 */
	public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
		this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
	}

	/**
	 * Nome: getCdUsuarioAlteracao
	 *
	 * @exception
	 * @throws
	 * @return cdUsuarioAlteracao
	 */
	public String getCdUsuarioAlteracao() {
		return cdUsuarioAlteracao;
	}

	/**
	 * Nome: setCdUsuarioAlteracao
	 *
	 * @exception
	 * @throws
	 * @param cdUsuarioAlteracao
	 */
	public void setCdUsuarioAlteracao(String cdUsuarioAlteracao) {
		this.cdUsuarioAlteracao = cdUsuarioAlteracao;
	}

	/**
	 * Nome: getCdUsuarioExternoAlteracao
	 *
	 * @exception
	 * @throws
	 * @return cdUsuarioExternoAlteracao
	 */
	public String getCdUsuarioExternoAlteracao() {
		return cdUsuarioExternoAlteracao;
	}

	/**
	 * Nome: setCdUsuarioExternoAlteracao
	 *
	 * @exception
	 * @throws
	 * @param cdUsuarioExternoAlteracao
	 */
	public void setCdUsuarioExternoAlteracao(String cdUsuarioExternoAlteracao) {
		this.cdUsuarioExternoAlteracao = cdUsuarioExternoAlteracao;
	}

	/**
	 * Nome: getHrManutencaoRegistroAlteracao
	 *
	 * @exception
	 * @throws
	 * @return hrManutencaoRegistroAlteracao
	 */
	public String getHrManutencaoRegistroAlteracao() {
		return hrManutencaoRegistroAlteracao;
	}

	/**
	 * Nome: setHrManutencaoRegistroAlteracao
	 *
	 * @exception
	 * @throws
	 * @param hrManutencaoRegistroAlteracao
	 */
	public void setHrManutencaoRegistroAlteracao(
			String hrManutencaoRegistroAlteracao) {
		this.hrManutencaoRegistroAlteracao = hrManutencaoRegistroAlteracao;
	}

	/**
	 * Nome: getCdCanalAlteracao
	 *
	 * @exception
	 * @throws
	 * @return cdCanalAlteracao
	 */
	public Integer getCdCanalAlteracao() {
		return cdCanalAlteracao;
	}

	/**
	 * Nome: setCdCanalAlteracao
	 *
	 * @exception
	 * @throws
	 * @param cdCanalAlteracao
	 */
	public void setCdCanalAlteracao(Integer cdCanalAlteracao) {
		this.cdCanalAlteracao = cdCanalAlteracao;
	}

	/**
	 * Nome: getDsCanalAlteracao
	 *
	 * @exception
	 * @throws
	 * @return dsCanalAlteracao
	 */
	public String getDsCanalAlteracao() {
		return dsCanalAlteracao;
	}

	/**
	 * Nome: setDsCanalAlteracao
	 *
	 * @exception
	 * @throws
	 * @param dsCanalAlteracao
	 */
	public void setDsCanalAlteracao(String dsCanalAlteracao) {
		this.dsCanalAlteracao = dsCanalAlteracao;
	}

	/**
	 * Nome: getNrOperacaoFluxoAlteracao
	 *
	 * @exception
	 * @throws
	 * @return nrOperacaoFluxoAlteracao
	 */
	public String getNrOperacaoFluxoAlteracao() {
		return nrOperacaoFluxoAlteracao;
	}

	/**
	 * Nome: setNrOperacaoFluxoAlteracao
	 *
	 * @exception
	 * @throws
	 * @param nrOperacaoFluxoAlteracao
	 */
	public void setNrOperacaoFluxoAlteracao(String nrOperacaoFluxoAlteracao) {
		this.nrOperacaoFluxoAlteracao = nrOperacaoFluxoAlteracao;
	}

	/**
	 * Nome: getCdAmbienteServicoContrato
	 *
	 * @exception
	 * @throws
	 * @return cdAmbienteServicoContrato
	 */
	public String getCdAmbienteServicoContrato() {
		return cdAmbienteServicoContrato;
	}

	/**
	 * Nome: setCdAmbienteServicoContrato
	 *
	 * @exception
	 * @throws
	 * @param cdAmbienteServicoContrato
	 */
	public void setCdAmbienteServicoContrato(String cdAmbienteServicoContrato) {
		this.cdAmbienteServicoContrato = cdAmbienteServicoContrato;
	}

	/**
	 * Nome: getDsAreaReservada2
	 *
	 * @exception
	 * @throws
	 * @return dsAreaReservada2
	 */
	public String getDsAreaReservada2() {
		return dsAreaReservada2;
	}

	/**
	 * Nome: setDsAreaReservada2
	 *
	 * @exception
	 * @throws
	 * @param dsAreaReservada2
	 */
	public void setDsAreaReservada2(String dsAreaReservada2) {
		this.dsAreaReservada2 = dsAreaReservada2;
	}

	/**
	 * Nome: getCdConsultaSaldoValorSuperior
	 *
	 * @exception
	 * @throws
	 * @return cdConsultaSaldoValorSuperior
	 */
	public Integer getCdConsultaSaldoValorSuperior() {
		return cdConsultaSaldoValorSuperior;
	}

	/**
	 * Nome: setCdConsultaSaldoValorSuperior
	 *
	 * @exception
	 * @throws
	 * @param cdConsultaSaldoValorSuperior
	 */
	public void setCdConsultaSaldoValorSuperior(Integer cdConsultaSaldoValorSuperior) {
		this.cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
	}

	/**
	 * Nome: getDsConsultaSaldoSuperior
	 *
	 * @exception
	 * @throws
	 * @return dsConsultaSaldoSuperior
	 */
	public String getDsConsultaSaldoSuperior() {
		return dsConsultaSaldoSuperior;
	}

	/**
	 * Nome: setDsConsultaSaldoSuperior
	 *
	 * @exception
	 * @throws
	 * @param dsConsultaSaldoSuperior
	 */
	public void setDsConsultaSaldoSuperior(String dsConsultaSaldoSuperior) {
		this.dsConsultaSaldoSuperior = dsConsultaSaldoSuperior;
	}

 
}