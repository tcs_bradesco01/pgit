/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasModSalarial
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasModSalarial {

	/** Atributo cdModalidadeSalarial. */
	private String cdModalidadeSalarial;

	/**
	 * Ocorrencias mod salarial.
	 */
	public OcorrenciasModSalarial() {
		super();
	}

	/**
	 * Ocorrencias mod salarial.
	 *
	 * @param cdModalidadeSalarial the cd modalidade salarial
	 */
	public OcorrenciasModSalarial(String cdModalidadeSalarial) {
		super();
		this.cdModalidadeSalarial = cdModalidadeSalarial;
	}

	/**
	 * Get: cdModalidadeSalarial.
	 *
	 * @return cdModalidadeSalarial
	 */
	public String getCdModalidadeSalarial() {
		return cdModalidadeSalarial;
	}

	/**
	 * Set: cdModalidadeSalarial.
	 *
	 * @param cdModalidadeSalarial the cd modalidade salarial
	 */
	public void setCdModalidadeSalarial(String cdModalidadeSalarial) {
		this.cdModalidadeSalarial = cdModalidadeSalarial;
	}
}