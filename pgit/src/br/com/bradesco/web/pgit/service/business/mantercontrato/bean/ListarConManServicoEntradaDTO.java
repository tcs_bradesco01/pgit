/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarConManServicoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarConManServicoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaNegocio. */
	private long cdPessoaJuridicaNegocio;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/** Atributo dtInicio. */
	private String dtInicio;
	
	/** Atributo dtFim. */
	private String dtFim;
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo cdProdutoServicoOperacao. */
	private int cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private int cdProdutoOperacaoRelacionado;
	
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public int getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public int getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: dtFim.
	 *
	 * @return dtFim
	 */
	public String getDtFim() {
		return dtFim;
	}
	
	/**
	 * Set: dtFim.
	 *
	 * @param dtFim the dt fim
	 */
	public void setDtFim(String dtFim) {
		this.dtFim = dtFim;
	}
	
	/**
	 * Get: dtInicio.
	 *
	 * @return dtInicio
	 */
	public String getDtInicio() {
		return dtInicio;
	}
	
	/**
	 * Set: dtInicio.
	 *
	 * @param dtInicio the dt inicio
	 */
	public void setDtInicio(String dtInicio) {
		this.dtInicio = dtInicio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	
}
