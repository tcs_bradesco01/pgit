/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _dsNomeUsuarioInclusao
     */
    private java.lang.String _dsNomeUsuarioInclusao;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.response.ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsNomeUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'dsNomeUsuarioInclusao'.
     */
    public java.lang.String getDsNomeUsuarioInclusao()
    {
        return this._dsNomeUsuarioInclusao;
    } //-- java.lang.String getDsNomeUsuarioInclusao() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeUsuarioInclusao'.
     * 
     * @param dsNomeUsuarioInclusao the value of field
     * 'dsNomeUsuarioInclusao'.
     */
    public void setDsNomeUsuarioInclusao(java.lang.String dsNomeUsuarioInclusao)
    {
        this._dsNomeUsuarioInclusao = dsNomeUsuarioInclusao;
    } //-- void setDsNomeUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.response.ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.response.ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.response.ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.response.ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
