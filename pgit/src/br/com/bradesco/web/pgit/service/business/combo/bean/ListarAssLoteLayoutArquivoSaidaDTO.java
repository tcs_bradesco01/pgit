/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarAssLoteLayoutArquivoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAssLoteLayoutArquivoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo cdTipoLoteLayout. */
    private Integer cdTipoLoteLayout;
    
    /** Atributo dsTipoLayoutArquivo. */
    private String dsTipoLayoutArquivo;
    
    /** Atributo dsTipoLoteLayout. */
    private String dsTipoLoteLayout;
    
    /**
     * Listar ass lote layout arquivo saida dto.
     *
     * @param cdTipoLoteLayout the cd tipo lote layout
     * @param dsTipoLoteLayout the ds tipo lote layout
     */
    public ListarAssLoteLayoutArquivoSaidaDTO(Integer cdTipoLoteLayout, String dsTipoLoteLayout){
    	this.cdTipoLoteLayout = cdTipoLoteLayout;
    	this.dsTipoLoteLayout = dsTipoLoteLayout;
    }
    
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoLoteLayout.
	 *
	 * @return cdTipoLoteLayout
	 */
	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}
	
	/**
	 * Set: cdTipoLoteLayout.
	 *
	 * @param cdTipoLoteLayout the cd tipo lote layout
	 */
	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsTipoLoteLayout.
	 *
	 * @return dsTipoLoteLayout
	 */
	public String getDsTipoLoteLayout() {
		return dsTipoLoteLayout;
	}
	
	/**
	 * Set: dsTipoLoteLayout.
	 *
	 * @param dsTipoLoteLayout the ds tipo lote layout
	 */
	public void setDsTipoLoteLayout(String dsTipoLoteLayout) {
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
    
    
}
