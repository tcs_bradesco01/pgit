/*
 * Nome: br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean;

/**
 * Nome: DetalharLanctoPersonalizadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharLanctoPersonalizadoSaidaDTO {
	
	/** Atributo codLancamento. */
	private Integer codLancamento;
	
	/** Atributo restricao. */
	private Integer restricao;
	
	/** Atributo tipoLancamento. */
	private Integer tipoLancamento;
	
	/** Atributo cdLancDebito. */
	private Integer cdLancDebito;
	
	/** Atributo dsLancDebito. */
	private String dsLancDebito;
	
	/** Atributo cdLancCredito. */
	private Integer cdLancCredito;
	
	/** Atributo dsLancCredito. */
	private String dsLancCredito;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdTipoModalidade. */
	private Integer cdTipoModalidade;
	
	/** Atributo dsTipoModalidade. */
	private String dsTipoModalidade;
	
	/** Atributo cdTipoRelacionamento. */
	private Integer cdTipoRelacionamento;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;	
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: cdLancCredito.
	 *
	 * @return cdLancCredito
	 */
	public Integer getCdLancCredito() {
		return cdLancCredito;
	}
	
	/**
	 * Set: cdLancCredito.
	 *
	 * @param cdLancCredito the cd lanc credito
	 */
	public void setCdLancCredito(Integer cdLancCredito) {
		this.cdLancCredito = cdLancCredito;
	}
	
	/**
	 * Get: cdLancDebito.
	 *
	 * @return cdLancDebito
	 */
	public Integer getCdLancDebito() {
		return cdLancDebito;
	}
	
	/**
	 * Set: cdLancDebito.
	 *
	 * @param cdLancDebito the cd lanc debito
	 */
	public void setCdLancDebito(Integer cdLancDebito) {
		this.cdLancDebito = cdLancDebito;
	}
	
	/**
	 * Get: cdTipoModalidade.
	 *
	 * @return cdTipoModalidade
	 */
	public Integer getCdTipoModalidade() {
		return cdTipoModalidade;
	}
	
	/**
	 * Set: cdTipoModalidade.
	 *
	 * @param cdTipoModalidade the cd tipo modalidade
	 */
	public void setCdTipoModalidade(Integer cdTipoModalidade) {
		this.cdTipoModalidade = cdTipoModalidade;
	}
	
	/**
	 * Get: cdTipoRelacionamento.
	 *
	 * @return cdTipoRelacionamento
	 */
	public Integer getCdTipoRelacionamento() {
		return cdTipoRelacionamento;
	}
	
	/**
	 * Set: cdTipoRelacionamento.
	 *
	 * @param cdTipoRelacionamento the cd tipo relacionamento
	 */
	public void setCdTipoRelacionamento(Integer cdTipoRelacionamento) {
		this.cdTipoRelacionamento = cdTipoRelacionamento;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codLancamento.
	 *
	 * @return codLancamento
	 */
	public Integer getCodLancamento() {
		return codLancamento;
	}
	
	/**
	 * Set: codLancamento.
	 *
	 * @param codLancamento the cod lancamento
	 */
	public void setCodLancamento(Integer codLancamento) {
		this.codLancamento = codLancamento;
	}
	
	/**
	 * Get: dsLancCredito.
	 *
	 * @return dsLancCredito
	 */
	public String getDsLancCredito() {
		return dsLancCredito;
	}
	
	/**
	 * Set: dsLancCredito.
	 *
	 * @param dsLancCredito the ds lanc credito
	 */
	public void setDsLancCredito(String dsLancCredito) {
		this.dsLancCredito = dsLancCredito;
	}
	
	/**
	 * Get: dsLancDebito.
	 *
	 * @return dsLancDebito
	 */
	public String getDsLancDebito() {
		return dsLancDebito;
	}
	
	/**
	 * Set: dsLancDebito.
	 *
	 * @param dsLancDebito the ds lanc debito
	 */
	public void setDsLancDebito(String dsLancDebito) {
		this.dsLancDebito = dsLancDebito;
	}
	
	/**
	 * Get: dsTipoModalidade.
	 *
	 * @return dsTipoModalidade
	 */
	public String getDsTipoModalidade() {
		return dsTipoModalidade;
	}
	
	/**
	 * Set: dsTipoModalidade.
	 *
	 * @param dsTipoModalidade the ds tipo modalidade
	 */
	public void setDsTipoModalidade(String dsTipoModalidade) {
		this.dsTipoModalidade = dsTipoModalidade;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public Integer getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(Integer restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: tipoLancamento.
	 *
	 * @return tipoLancamento
	 */
	public Integer getTipoLancamento() {
		return tipoLancamento;
	}
	
	/**
	 * Set: tipoLancamento.
	 *
	 * @param tipoLancamento the tipo lancamento
	 */
	public void setTipoLancamento(Integer tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

}
