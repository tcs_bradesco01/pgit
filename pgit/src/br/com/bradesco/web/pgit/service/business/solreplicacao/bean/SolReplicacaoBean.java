/*
 * Nome: br.com.bradesco.web.pgit.service.business.solreplicacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solreplicacao.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.service.business.solreplicacao.ISolReplicacaoService;

/**
 * Nome: SolReplicacaoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolReplicacaoBean {

	/** Atributo nivelEstruturaProdutoReplicadoFiltro. */
	private String nivelEstruturaProdutoReplicadoFiltro;

	/** Atributo acaoReplicacoFiltro. */
	private String acaoReplicacoFiltro;

	/** Atributo produtoFiltro. */
	private String produtoFiltro;

	/** Atributo atributoProdutoFiltro. */
	private String atributoProdutoFiltro;

	/** Atributo atributoProdutoSubstitutivoFiltro. */
	private String atributoProdutoSubstitutivoFiltro;

	/** Atributo dominioAtributoProdutoFiltro. */
	private String dominioAtributoProdutoFiltro;

	/** Atributo dominioProdutoSubstitutivoFiltro. */
	private String dominioProdutoSubstitutivoFiltro;

	/** Atributo operacaoFiltro. */
	private String operacaoFiltro;

	/** Atributo atributoOperacaoFiltro. */
	private String atributoOperacaoFiltro;

	/** Atributo atributoOperacaoSubstitutivoFiltro. */
	private String atributoOperacaoSubstitutivoFiltro;

	/** Atributo dominioAtributoOperacaoFiltro. */
	private String dominioAtributoOperacaoFiltro;

	/** Atributo dominioAtributoOperacaoSubstitutivoFiltro. */
	private String dominioAtributoOperacaoSubstitutivoFiltro;

	/** Atributo dataProgramadaReplicacaoFiltro. */
	private Date dataProgramadaReplicacaoFiltro;

	/** Atributo percReajusteFiltro. */
	private String percReajusteFiltro;

	/** Atributo contratosClientAgencOperFiltro. */
	private String contratosClientAgencOperFiltro;

	/** Atributo contratoClientDetermSegmFiltro. */
	private String contratoClientDetermSegmFiltro;

	/** Atributo contratoClientDetermRegiaoFiltro. */
	private String contratoClientDetermRegiaoFiltro;

	/** Atributo contratoClientDetermAtvEconomicaFiltro. */
	private String contratoClientDetermAtvEconomicaFiltro;

	/** Atributo filtroDataDe. */
	private Date filtroDataDe;

	/** Atributo filtroDataAte. */
	private Date filtroDataAte;
	
	/** Atributo situacaoSolicitacaoFiltro. */
	private String situacaoSolicitacaoFiltro;

	/** Atributo manterSolicitacaoReplicacaoImpl. */
	private ISolReplicacaoService manterSolicitacaoReplicacaoImpl;

	/*
	 * listas//
	 * 
	 * listaNivelEstrutProdRepl; listaAcaoReplicacao;
	 * 
	 * listaProduto; listaAtributoProduto; listaAtributoProdutoSubstitutivo;
	 * listaDominioAtributoProduto; listaDominioProdutoSubstitutivo;
	 * 
	 * listaOperacao; listaAtributoOperacao; listaAtributoOperacaoSubstitutivo;
	 * listaDominioAtributoOperacao; listaContratoClientDetermSegm;
	 * listaContratoClientDetermRegiao; listaContratoClientDetermAtvEconomica;
	 */

	/**
	 * Get: acaoReplicacoFiltro.
	 *
	 * @return acaoReplicacoFiltro
	 */
	public String getAcaoReplicacoFiltro() {
		return acaoReplicacoFiltro;
	}

	/**
	 * Set: acaoReplicacoFiltro.
	 *
	 * @param acaoReplicacoFiltro the acao replicaco filtro
	 */
	public void setAcaoReplicacoFiltro(String acaoReplicacoFiltro) {
		this.acaoReplicacoFiltro = acaoReplicacoFiltro;
	}

	/**
	 * Get: atributoProdutoFiltro.
	 *
	 * @return atributoProdutoFiltro
	 */
	public String getAtributoProdutoFiltro() {
		return atributoProdutoFiltro;
	}

	/**
	 * Set: atributoProdutoFiltro.
	 *
	 * @param atributoProdutoFiltro the atributo produto filtro
	 */
	public void setAtributoProdutoFiltro(String atributoProdutoFiltro) {
		this.atributoProdutoFiltro = atributoProdutoFiltro;
	}

	/**
	 * Get: atributoProdutoSubstitutivoFiltro.
	 *
	 * @return atributoProdutoSubstitutivoFiltro
	 */
	public String getAtributoProdutoSubstitutivoFiltro() {
		return atributoProdutoSubstitutivoFiltro;
	}

	/**
	 * Set: atributoProdutoSubstitutivoFiltro.
	 *
	 * @param atributoProdutoSubstitutivoFiltro the atributo produto substitutivo filtro
	 */
	public void setAtributoProdutoSubstitutivoFiltro(
			String atributoProdutoSubstitutivoFiltro) {
		this.atributoProdutoSubstitutivoFiltro = atributoProdutoSubstitutivoFiltro;
	}

	/**
	 * Get: contratoClientDetermAtvEconomicaFiltro.
	 *
	 * @return contratoClientDetermAtvEconomicaFiltro
	 */
	public String getContratoClientDetermAtvEconomicaFiltro() {
		return contratoClientDetermAtvEconomicaFiltro;
	}

	/**
	 * Set: contratoClientDetermAtvEconomicaFiltro.
	 *
	 * @param contratoClientDetermAtvEconomicaFiltro the contrato client determ atv economica filtro
	 */
	public void setContratoClientDetermAtvEconomicaFiltro(
			String contratoClientDetermAtvEconomicaFiltro) {
		this.contratoClientDetermAtvEconomicaFiltro = contratoClientDetermAtvEconomicaFiltro;
	}

	/**
	 * Get: contratoClientDetermRegiaoFiltro.
	 *
	 * @return contratoClientDetermRegiaoFiltro
	 */
	public String getContratoClientDetermRegiaoFiltro() {
		return contratoClientDetermRegiaoFiltro;
	}

	/**
	 * Set: contratoClientDetermRegiaoFiltro.
	 *
	 * @param contratoClientDetermRegiaoFiltro the contrato client determ regiao filtro
	 */
	public void setContratoClientDetermRegiaoFiltro(
			String contratoClientDetermRegiaoFiltro) {
		this.contratoClientDetermRegiaoFiltro = contratoClientDetermRegiaoFiltro;
	}

	/**
	 * Get: contratoClientDetermSegmFiltro.
	 *
	 * @return contratoClientDetermSegmFiltro
	 */
	public String getContratoClientDetermSegmFiltro() {
		return contratoClientDetermSegmFiltro;
	}

	/**
	 * Set: contratoClientDetermSegmFiltro.
	 *
	 * @param contratoClientDetermSegmFiltro the contrato client determ segm filtro
	 */
	public void setContratoClientDetermSegmFiltro(
			String contratoClientDetermSegmFiltro) {
		this.contratoClientDetermSegmFiltro = contratoClientDetermSegmFiltro;
	}

	/**
	 * Get: contratosClientAgencOperFiltro.
	 *
	 * @return contratosClientAgencOperFiltro
	 */
	public String getContratosClientAgencOperFiltro() {
		return contratosClientAgencOperFiltro;
	}

	/**
	 * Set: contratosClientAgencOperFiltro.
	 *
	 * @param contratosClientAgencOperFiltro the contratos client agenc oper filtro
	 */
	public void setContratosClientAgencOperFiltro(
			String contratosClientAgencOperFiltro) {
		this.contratosClientAgencOperFiltro = contratosClientAgencOperFiltro;
	}

	/**
	 * Get: dataProgramadaReplicacaoFiltro.
	 *
	 * @return dataProgramadaReplicacaoFiltro
	 */
	public Date getDataProgramadaReplicacaoFiltro() {
		if (dataProgramadaReplicacaoFiltro == null){
			dataProgramadaReplicacaoFiltro = new Date(); 
		}
		return dataProgramadaReplicacaoFiltro;
	}

	/**
	 * Set: dataProgramadaReplicacaoFiltro.
	 *
	 * @param dataProgramadaReplicacaoFiltro the data programada replicacao filtro
	 */
	public void setDataProgramadaReplicacaoFiltro(
			Date dataProgramadaReplicacaoFiltro) {
		this.dataProgramadaReplicacaoFiltro = dataProgramadaReplicacaoFiltro;
	}

	/**
	 * Get: filtroDataAte.
	 *
	 * @return filtroDataAte
	 */
	public Date getFiltroDataAte() {
		if (filtroDataAte == null){
			filtroDataAte = new Date(); 
		}
		return filtroDataAte;
	}

	/**
	 * Set: filtroDataAte.
	 *
	 * @param filtroDataAte the filtro data ate
	 */
	public void setFiltroDataAte(Date filtroDataAte) {
		this.filtroDataAte = filtroDataAte;
	}

	/**
	 * Get: filtroDataDe.
	 *
	 * @return filtroDataDe
	 */
	public Date getFiltroDataDe() {
		if (filtroDataDe == null){
			filtroDataDe = new Date(); 
		}
		return filtroDataDe;
	}

	/**
	 * Set: filtroDataDe.
	 *
	 * @param filtroDataDe the filtro data de
	 */
	public void setFiltroDataDe(Date filtroDataDe) {
		this.filtroDataDe = filtroDataDe;
	}

	/**
	 * Get: nivelEstruturaProdutoReplicadoFiltro.
	 *
	 * @return nivelEstruturaProdutoReplicadoFiltro
	 */
	public String getNivelEstruturaProdutoReplicadoFiltro() {
		return nivelEstruturaProdutoReplicadoFiltro;
	}

	/**
	 * Set: nivelEstruturaProdutoReplicadoFiltro.
	 *
	 * @param nivelEstruturaProdutoReplicadoFiltro the nivel estrutura produto replicado filtro
	 */
	public void setNivelEstruturaProdutoReplicadoFiltro(
			String nivelEstruturaProdutoReplicadoFiltro) {
		this.nivelEstruturaProdutoReplicadoFiltro = nivelEstruturaProdutoReplicadoFiltro;
	}

	/**
	 * Get: manterSolicitacaoReplicacaoImpl.
	 *
	 * @return manterSolicitacaoReplicacaoImpl
	 */
	public ISolReplicacaoService getManterSolicitacaoReplicacaoImpl() {
		return manterSolicitacaoReplicacaoImpl;
	}

	/**
	 * Set: manterSolicitacaoReplicacaoImpl.
	 *
	 * @param manterSolicitacaoReplicacaoImpl the manter solicitacao replicacao impl
	 */
	public void setManterSolicitacaoReplicacaoImpl(
			ISolReplicacaoService manterSolicitacaoReplicacaoImpl) {
		this.manterSolicitacaoReplicacaoImpl = manterSolicitacaoReplicacaoImpl;
	}

	/**
	 * Get: produtoFiltro.
	 *
	 * @return produtoFiltro
	 */
	public String getProdutoFiltro() {
		return produtoFiltro;
	}

	/**
	 * Set: produtoFiltro.
	 *
	 * @param produtoFiltro the produto filtro
	 */
	public void setProdutoFiltro(String produtoFiltro) {
		this.produtoFiltro = produtoFiltro;
	}

	/**
	 * Get: dominioAtributoProdutoFiltro.
	 *
	 * @return dominioAtributoProdutoFiltro
	 */
	public String getDominioAtributoProdutoFiltro() {
		return dominioAtributoProdutoFiltro;
	}

	/**
	 * Set: dominioAtributoProdutoFiltro.
	 *
	 * @param dominioAtributoProdutoFiltro the dominio atributo produto filtro
	 */
	public void setDominioAtributoProdutoFiltro(
			String dominioAtributoProdutoFiltro) {
		this.dominioAtributoProdutoFiltro = dominioAtributoProdutoFiltro;
	}

	/**
	 * Get: dominioProdutoSubstitutivoFiltro.
	 *
	 * @return dominioProdutoSubstitutivoFiltro
	 */
	public String getDominioProdutoSubstitutivoFiltro() {
		return dominioProdutoSubstitutivoFiltro;
	}

	/**
	 * Set: dominioProdutoSubstitutivoFiltro.
	 *
	 * @param dominioProdutoSubstitutivoFiltro the dominio produto substitutivo filtro
	 */
	public void setDominioProdutoSubstitutivoFiltro(
			String dominioProdutoSubstitutivoFiltro) {
		this.dominioProdutoSubstitutivoFiltro = dominioProdutoSubstitutivoFiltro;
	}

	/**
	 * Get: atributoOperacaoFiltro.
	 *
	 * @return atributoOperacaoFiltro
	 */
	public String getAtributoOperacaoFiltro() {
		return atributoOperacaoFiltro;
	}

	/**
	 * Set: atributoOperacaoFiltro.
	 *
	 * @param atributoOperacaoFiltro the atributo operacao filtro
	 */
	public void setAtributoOperacaoFiltro(String atributoOperacaoFiltro) {
		this.atributoOperacaoFiltro = atributoOperacaoFiltro;
	}

	/**
	 * Get: atributoOperacaoSubstitutivoFiltro.
	 *
	 * @return atributoOperacaoSubstitutivoFiltro
	 */
	public String getAtributoOperacaoSubstitutivoFiltro() {
		return atributoOperacaoSubstitutivoFiltro;
	}

	/**
	 * Set: atributoOperacaoSubstitutivoFiltro.
	 *
	 * @param atributoOperacaoSubstitutivoFiltro the atributo operacao substitutivo filtro
	 */
	public void setAtributoOperacaoSubstitutivoFiltro(
			String atributoOperacaoSubstitutivoFiltro) {
		this.atributoOperacaoSubstitutivoFiltro = atributoOperacaoSubstitutivoFiltro;
	}

	/**
	 * Get: dominioAtributoOperacaoFiltro.
	 *
	 * @return dominioAtributoOperacaoFiltro
	 */
	public String getDominioAtributoOperacaoFiltro() {
		return dominioAtributoOperacaoFiltro;
	}

	/**
	 * Set: dominioAtributoOperacaoFiltro.
	 *
	 * @param dominioAtributoOperacaoFiltro the dominio atributo operacao filtro
	 */
	public void setDominioAtributoOperacaoFiltro(
			String dominioAtributoOperacaoFiltro) {
		this.dominioAtributoOperacaoFiltro = dominioAtributoOperacaoFiltro;
	}

	/**
	 * Get: dominioAtributoOperacaoSubstitutivoFiltro.
	 *
	 * @return dominioAtributoOperacaoSubstitutivoFiltro
	 */
	public String getDominioAtributoOperacaoSubstitutivoFiltro() {
		return dominioAtributoOperacaoSubstitutivoFiltro;
	}

	/**
	 * Set: dominioAtributoOperacaoSubstitutivoFiltro.
	 *
	 * @param dominioAtributoOperacaoSubstitutivoFiltro the dominio atributo operacao substitutivo filtro
	 */
	public void setDominioAtributoOperacaoSubstitutivoFiltro(
			String dominioAtributoOperacaoSubstitutivoFiltro) {
		this.dominioAtributoOperacaoSubstitutivoFiltro = dominioAtributoOperacaoSubstitutivoFiltro;
	}

	/**
	 * Get: operacaoFiltro.
	 *
	 * @return operacaoFiltro
	 */
	public String getOperacaoFiltro() {
		return operacaoFiltro;
	}

	/**
	 * Set: operacaoFiltro.
	 *
	 * @param operacaoFiltro the operacao filtro
	 */
	public void setOperacaoFiltro(String operacaoFiltro) {
		this.operacaoFiltro = operacaoFiltro;
	}

	/**
	 * Get: percReajusteFiltro.
	 *
	 * @return percReajusteFiltro
	 */
	public String getPercReajusteFiltro() {
		return percReajusteFiltro;
	}

	/**
	 * Set: percReajusteFiltro.
	 *
	 * @param percReajusteFiltro the perc reajuste filtro
	 */
	public void setPercReajusteFiltro(String percReajusteFiltro) {
		this.percReajusteFiltro = percReajusteFiltro;
	}

	/**
	 * Get: situacaoSolicitacaoFiltro.
	 *
	 * @return situacaoSolicitacaoFiltro
	 */
	public String getSituacaoSolicitacaoFiltro() {
		return situacaoSolicitacaoFiltro;
	}

	/**
	 * Set: situacaoSolicitacaoFiltro.
	 *
	 * @param situacaoSolicitacaoFiltro the situacao solicitacao filtro
	 */
	public void setSituacaoSolicitacaoFiltro(String situacaoSolicitacaoFiltro) {
		this.situacaoSolicitacaoFiltro = situacaoSolicitacaoFiltro;
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {		
		this.setFiltroDataDe(null);
		this.setFiltroDataAte(null);	
		this.setSituacaoSolicitacaoFiltro("");
		return "ok";		
	}
	
	
	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar() {			
		return "ok";		
	}
	
	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar(){
		return "AVANCAR_DETALHAR";
	}	
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		return "AVANCAR_INCLUIR";
	}	
	
	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir(){
		return "AVANCAR_EXCLUIR";
	}		
	
	/**
	 * Avancar confirmar.
	 *
	 * @return the string
	 */
	public String avancarConfirmar(){
		return "AVANCAR_CONFIRMAR";
	}	
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR_INCLUIR";
	}	
	
	/**
	 * Confirmar inclusao.
	 *
	 * @return the string
	 */
	public String confirmarInclusao(){
		return "ok";
	}	
	
	/**
	 * Confirmar exclusao.
	 *
	 * @return the string
	 */
	public String confirmarExclusao(){
		return "ok";
	}
	
	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		return "VOLTAR_CONSULTAR";
	}



}
