/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdParametroTela
     */
    private int _cdParametroTela = 0;

    /**
     * keeps track of state for field: _cdParametroTela
     */
    private boolean _has_cdParametroTela;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _dtInclusaoServico
     */
    private java.lang.String _dtInclusaoServico;

    /**
     * Field _dsServico
     */
    private java.lang.String _dsServico;

    /**
     * Field _dsMadalidade
     */
    private java.lang.String _dsMadalidade;

    /**
     * Field _vlLimiteIndvdPagamento
     */
    private java.math.BigDecimal _vlLimiteIndvdPagamento = new java.math.BigDecimal("0");

    /**
     * Field _vlLimiteDiaPagamento
     */
    private java.math.BigDecimal _vlLimiteDiaPagamento = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlLimiteIndvdPagamento(new java.math.BigDecimal("0"));
        setVlLimiteDiaPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdParametroTela
     * 
     */
    public void deleteCdParametroTela()
    {
        this._has_cdParametroTela= false;
    } //-- void deleteCdParametroTela() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdParametroTela'.
     * 
     * @return int
     * @return the value of field 'cdParametroTela'.
     */
    public int getCdParametroTela()
    {
        return this._cdParametroTela;
    } //-- int getCdParametroTela() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'dsMadalidade'.
     * 
     * @return String
     * @return the value of field 'dsMadalidade'.
     */
    public java.lang.String getDsMadalidade()
    {
        return this._dsMadalidade;
    } //-- java.lang.String getDsMadalidade() 

    /**
     * Returns the value of field 'dsServico'.
     * 
     * @return String
     * @return the value of field 'dsServico'.
     */
    public java.lang.String getDsServico()
    {
        return this._dsServico;
    } //-- java.lang.String getDsServico() 

    /**
     * Returns the value of field 'dtInclusaoServico'.
     * 
     * @return String
     * @return the value of field 'dtInclusaoServico'.
     */
    public java.lang.String getDtInclusaoServico()
    {
        return this._dtInclusaoServico;
    } //-- java.lang.String getDtInclusaoServico() 

    /**
     * Returns the value of field 'vlLimiteDiaPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteDiaPagamento'.
     */
    public java.math.BigDecimal getVlLimiteDiaPagamento()
    {
        return this._vlLimiteDiaPagamento;
    } //-- java.math.BigDecimal getVlLimiteDiaPagamento() 

    /**
     * Returns the value of field 'vlLimiteIndvdPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteIndvdPagamento'.
     */
    public java.math.BigDecimal getVlLimiteIndvdPagamento()
    {
        return this._vlLimiteIndvdPagamento;
    } //-- java.math.BigDecimal getVlLimiteIndvdPagamento() 

    /**
     * Method hasCdParametroTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParametroTela()
    {
        return this._has_cdParametroTela;
    } //-- boolean hasCdParametroTela() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdParametroTela'.
     * 
     * @param cdParametroTela the value of field 'cdParametroTela'.
     */
    public void setCdParametroTela(int cdParametroTela)
    {
        this._cdParametroTela = cdParametroTela;
        this._has_cdParametroTela = true;
    } //-- void setCdParametroTela(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'dsMadalidade'.
     * 
     * @param dsMadalidade the value of field 'dsMadalidade'.
     */
    public void setDsMadalidade(java.lang.String dsMadalidade)
    {
        this._dsMadalidade = dsMadalidade;
    } //-- void setDsMadalidade(java.lang.String) 

    /**
     * Sets the value of field 'dsServico'.
     * 
     * @param dsServico the value of field 'dsServico'.
     */
    public void setDsServico(java.lang.String dsServico)
    {
        this._dsServico = dsServico;
    } //-- void setDsServico(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusaoServico'.
     * 
     * @param dtInclusaoServico the value of field
     * 'dtInclusaoServico'.
     */
    public void setDtInclusaoServico(java.lang.String dtInclusaoServico)
    {
        this._dtInclusaoServico = dtInclusaoServico;
    } //-- void setDtInclusaoServico(java.lang.String) 

    /**
     * Sets the value of field 'vlLimiteDiaPagamento'.
     * 
     * @param vlLimiteDiaPagamento the value of field
     * 'vlLimiteDiaPagamento'.
     */
    public void setVlLimiteDiaPagamento(java.math.BigDecimal vlLimiteDiaPagamento)
    {
        this._vlLimiteDiaPagamento = vlLimiteDiaPagamento;
    } //-- void setVlLimiteDiaPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlLimiteIndvdPagamento'.
     * 
     * @param vlLimiteIndvdPagamento the value of field
     * 'vlLimiteIndvdPagamento'.
     */
    public void setVlLimiteIndvdPagamento(java.math.BigDecimal vlLimiteIndvdPagamento)
    {
        this._vlLimiteIndvdPagamento = vlLimiteIndvdPagamento;
    } //-- void setVlLimiteIndvdPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistaservlimdiaindv.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
