/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

/**
 * Nome: ConsultarDetHistContaSalarioDestinoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetHistContaSalarioDestinoEntradaDTO {
	
	 /** Atributo cdPessoaJuridNegocio. */
 	private Long cdPessoaJuridNegocio;
     
     /** Atributo cdTipoContratoNegocio. */
     private Integer cdTipoContratoNegocio;
     
     /** Atributo nrSeqContratoNegocio. */
     private Long nrSeqContratoNegocio;
     
     /** Atributo cdPessoaJuridVinc. */
     private Long cdPessoaJuridVinc;
     
     /** Atributo cdTipoContratoVinc. */
     private Integer cdTipoContratoVinc;
     
     /** Atributo nrSeqContratoVinc. */
     private Long nrSeqContratoVinc;
     
     /** Atributo cdTipoVincContrato. */
     private Integer cdTipoVincContrato;
     
     /** Atributo hrInclusaoRegistro. */
     private String hrInclusaoRegistro;
 
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Consultar det hist conta salario destino entrada dto.
	 */
	public ConsultarDetHistContaSalarioDestinoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Get: cdPessoaJuridNegocio.
	 *
	 * @return cdPessoaJuridNegocio
	 */
	public Long getCdPessoaJuridNegocio() {
		return cdPessoaJuridNegocio;
	}

	/**
	 * Set: cdPessoaJuridNegocio.
	 *
	 * @param cdPessoaJuridNegocio the cd pessoa jurid negocio
	 */
	public void setCdPessoaJuridNegocio(Long cdPessoaJuridNegocio) {
		this.cdPessoaJuridNegocio = cdPessoaJuridNegocio;
	}

	/**
	 * Get: cdPessoaJuridVinc.
	 *
	 * @return cdPessoaJuridVinc
	 */
	public Long getCdPessoaJuridVinc() {
		return cdPessoaJuridVinc;
	}

	/**
	 * Set: cdPessoaJuridVinc.
	 *
	 * @param cdPessoaJuridVinc the cd pessoa jurid vinc
	 */
	public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
		this.cdPessoaJuridVinc = cdPessoaJuridVinc;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}

	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}

	/**
	 * Get: cdTipoVincContrato.
	 *
	 * @return cdTipoVincContrato
	 */
	public Integer getCdTipoVincContrato() {
		return cdTipoVincContrato;
	}

	/**
	 * Set: cdTipoVincContrato.
	 *
	 * @param cdTipoVincContrato the cd tipo vinc contrato
	 */
	public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
		this.cdTipoVincContrato = cdTipoVincContrato;
	}

	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}

	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}

	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}

	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}
}
	