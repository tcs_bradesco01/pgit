/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

/**
 * Nome: ConsultarInformacoesFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarInformacoesFavorecidoEntradaDTO{
    
    /** Atributo cdFavorecido. */
    private Long cdFavorecido;
    
    /** Atributo cdTipoIsncricaoFavorecido. */
    private Integer cdTipoIsncricaoFavorecido;
    
    /** Atributo nrInscricaoFavorecido. */
    private Long nrInscricaoFavorecido;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    
	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}
	
	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}
	
	/**
	 * Get: cdTipoIsncricaoFavorecido.
	 *
	 * @return cdTipoIsncricaoFavorecido
	 */
	public Integer getCdTipoIsncricaoFavorecido() {
		return cdTipoIsncricaoFavorecido;
	}
	
	/**
	 * Set: cdTipoIsncricaoFavorecido.
	 *
	 * @param cdTipoIsncricaoFavorecido the cd tipo isncricao favorecido
	 */
	public void setCdTipoIsncricaoFavorecido(Integer cdTipoIsncricaoFavorecido) {
		this.cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
	}
	
	/**
	 * Get: nrInscricaoFavorecido.
	 *
	 * @return nrInscricaoFavorecido
	 */
	public Long getNrInscricaoFavorecido() {
		return nrInscricaoFavorecido;
	}
	
	/**
	 * Set: nrInscricaoFavorecido.
	 *
	 * @param nrInscricaoFavorecido the nr inscricao favorecido
	 */
	public void setNrInscricaoFavorecido(Long nrInscricaoFavorecido) {
		this.nrInscricaoFavorecido = nrInscricaoFavorecido;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
    
    
}
