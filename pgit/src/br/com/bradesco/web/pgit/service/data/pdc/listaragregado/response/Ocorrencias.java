/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaragregado.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdFilialCnpj
     */
    private int _cdFilialCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCnpj
     */
    private boolean _has_cdFilialCnpj;

    /**
     * Field _cdControleCpfCnpj
     */
    private int _cdControleCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpj
     */
    private boolean _has_cdControleCpfCnpj;

    /**
     * Field _dsNomeAgregado
     */
    private java.lang.String _dsNomeAgregado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaragregado.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCpfCnpj
     * 
     */
    public void deleteCdControleCpfCnpj()
    {
        this._has_cdControleCpfCnpj= false;
    } //-- void deleteCdControleCpfCnpj() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdFilialCnpj
     * 
     */
    public void deleteCdFilialCnpj()
    {
        this._has_cdFilialCnpj= false;
    } //-- void deleteCdFilialCnpj() 

    /**
     * Returns the value of field 'cdControleCpfCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpj'.
     */
    public int getCdControleCpfCnpj()
    {
        return this._cdControleCpfCnpj;
    } //-- int getCdControleCpfCnpj() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpj'.
     */
    public int getCdFilialCnpj()
    {
        return this._cdFilialCnpj;
    } //-- int getCdFilialCnpj() 

    /**
     * Returns the value of field 'dsNomeAgregado'.
     * 
     * @return String
     * @return the value of field 'dsNomeAgregado'.
     */
    public java.lang.String getDsNomeAgregado()
    {
        return this._dsNomeAgregado;
    } //-- java.lang.String getDsNomeAgregado() 

    /**
     * Method hasCdControleCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpj()
    {
        return this._has_cdControleCpfCnpj;
    } //-- boolean hasCdControleCpfCnpj() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpj()
    {
        return this._has_cdFilialCnpj;
    } //-- boolean hasCdFilialCnpj() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCpfCnpj'.
     * 
     * @param cdControleCpfCnpj the value of field
     * 'cdControleCpfCnpj'.
     */
    public void setCdControleCpfCnpj(int cdControleCpfCnpj)
    {
        this._cdControleCpfCnpj = cdControleCpfCnpj;
        this._has_cdControleCpfCnpj = true;
    } //-- void setCdControleCpfCnpj(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdFilialCnpj'.
     * 
     * @param cdFilialCnpj the value of field 'cdFilialCnpj'.
     */
    public void setCdFilialCnpj(int cdFilialCnpj)
    {
        this._cdFilialCnpj = cdFilialCnpj;
        this._has_cdFilialCnpj = true;
    } //-- void setCdFilialCnpj(int) 

    /**
     * Sets the value of field 'dsNomeAgregado'.
     * 
     * @param dsNomeAgregado the value of field 'dsNomeAgregado'.
     */
    public void setDsNomeAgregado(java.lang.String dsNomeAgregado)
    {
        this._dsNomeAgregado = dsNomeAgregado;
    } //-- void setDsNomeAgregado(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaragregado.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaragregado.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaragregado.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaragregado.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
