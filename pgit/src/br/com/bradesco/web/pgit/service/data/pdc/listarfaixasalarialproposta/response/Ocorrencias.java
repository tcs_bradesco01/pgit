/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrFaixa
     */
    private int _nrFaixa = 0;

    /**
     * keeps track of state for field: _nrFaixa
     */
    private boolean _has_nrFaixa;

    /**
     * Field _vlInicialFaixaSalarial
     */
    private java.math.BigDecimal _vlInicialFaixaSalarial = new java.math.BigDecimal("0");

    /**
     * Field _vlFinalFaixaSalarial
     */
    private java.math.BigDecimal _vlFinalFaixaSalarial = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlInicialFaixaSalarial(new java.math.BigDecimal("0"));
        setVlFinalFaixaSalarial(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNrFaixa
     * 
     */
    public void deleteNrFaixa()
    {
        this._has_nrFaixa= false;
    } //-- void deleteNrFaixa() 

    /**
     * Returns the value of field 'nrFaixa'.
     * 
     * @return int
     * @return the value of field 'nrFaixa'.
     */
    public int getNrFaixa()
    {
        return this._nrFaixa;
    } //-- int getNrFaixa() 

    /**
     * Returns the value of field 'vlFinalFaixaSalarial'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFinalFaixaSalarial'.
     */
    public java.math.BigDecimal getVlFinalFaixaSalarial()
    {
        return this._vlFinalFaixaSalarial;
    } //-- java.math.BigDecimal getVlFinalFaixaSalarial() 

    /**
     * Returns the value of field 'vlInicialFaixaSalarial'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlInicialFaixaSalarial'.
     */
    public java.math.BigDecimal getVlInicialFaixaSalarial()
    {
        return this._vlInicialFaixaSalarial;
    } //-- java.math.BigDecimal getVlInicialFaixaSalarial() 

    /**
     * Method hasNrFaixa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFaixa()
    {
        return this._has_nrFaixa;
    } //-- boolean hasNrFaixa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'nrFaixa'.
     * 
     * @param nrFaixa the value of field 'nrFaixa'.
     */
    public void setNrFaixa(int nrFaixa)
    {
        this._nrFaixa = nrFaixa;
        this._has_nrFaixa = true;
    } //-- void setNrFaixa(int) 

    /**
     * Sets the value of field 'vlFinalFaixaSalarial'.
     * 
     * @param vlFinalFaixaSalarial the value of field
     * 'vlFinalFaixaSalarial'.
     */
    public void setVlFinalFaixaSalarial(java.math.BigDecimal vlFinalFaixaSalarial)
    {
        this._vlFinalFaixaSalarial = vlFinalFaixaSalarial;
    } //-- void setVlFinalFaixaSalarial(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlInicialFaixaSalarial'.
     * 
     * @param vlInicialFaixaSalarial the value of field
     * 'vlInicialFaixaSalarial'.
     */
    public void setVlInicialFaixaSalarial(java.math.BigDecimal vlInicialFaixaSalarial)
    {
        this._vlInicialFaixaSalarial = vlInicialFaixaSalarial;
    } //-- void setVlInicialFaixaSalarial(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfaixasalarialproposta.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
