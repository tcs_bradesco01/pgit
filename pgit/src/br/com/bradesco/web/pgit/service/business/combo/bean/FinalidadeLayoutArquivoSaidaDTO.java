/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: FinalidadeLayoutArquivoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FinalidadeLayoutArquivoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdFinalidadeLayoutArquivo. */
	private Integer cdFinalidadeLayoutArquivo;
	
	/** Atributo dsFinalidadeLayoutArquivo. */
	private String dsFinalidadeLayoutArquivo;

	/**
	 * Instancia um novo FinalidadeLayoutArquivoSaidaDTO
	 *
	 */
	public FinalidadeLayoutArquivoSaidaDTO() {
		super();
	}

	/**
	 * Finalidade layout arquivo saida dto.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 * @param cdFinalidadeLayoutArquivo the cd finalidade layout arquivo
	 * @param dsFinalidadeLayoutArquivo the ds finalidade layout arquivo
	 */
	public FinalidadeLayoutArquivoSaidaDTO(String codMensagem, String mensagem, Integer cdFinalidadeLayoutArquivo, String dsFinalidadeLayoutArquivo) {
		super();
		this.codMensagem = codMensagem;
		this.mensagem = mensagem;
		this.cdFinalidadeLayoutArquivo = cdFinalidadeLayoutArquivo;
		this.dsFinalidadeLayoutArquivo = dsFinalidadeLayoutArquivo;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdFinalidadeLayoutArquivo.
	 *
	 * @return cdFinalidadeLayoutArquivo
	 */
	public Integer getCdFinalidadeLayoutArquivo() {
		return cdFinalidadeLayoutArquivo;
	}
	
	/**
	 * Set: cdFinalidadeLayoutArquivo.
	 *
	 * @param cdFinalidadeLayoutArquivo the cd finalidade layout arquivo
	 */
	public void setCdFinalidadeLayoutArquivo(Integer cdFinalidadeLayoutArquivo) {
		this.cdFinalidadeLayoutArquivo = cdFinalidadeLayoutArquivo;
	}
	
	/**
	 * Get: dsFinalidadeLayoutArquivo.
	 *
	 * @return dsFinalidadeLayoutArquivo
	 */
	public String getDsFinalidadeLayoutArquivo() {
		return dsFinalidadeLayoutArquivo;
	}
	
	/**
	 * Set: dsFinalidadeLayoutArquivo.
	 *
	 * @param dsFinalidadeLayoutArquivo the ds finalidade layout arquivo
	 */
	public void setDsFinalidadeLayoutArquivo(String dsFinalidadeLayoutArquivo) {
		this.dsFinalidadeLayoutArquivo = dsFinalidadeLayoutArquivo;
	}

}