/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarAssLoteLayoutArquivoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAssLoteLayoutArquivoEntradaDTO {

    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo cdTipoLoteLayout. */
    private Integer cdTipoLoteLayout;    
    
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoLoteLayout.
	 *
	 * @return cdTipoLoteLayout
	 */
	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}
	
	/**
	 * Set: cdTipoLoteLayout.
	 *
	 * @param cdTipoLoteLayout the cd tipo lote layout
	 */
	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}
}	
