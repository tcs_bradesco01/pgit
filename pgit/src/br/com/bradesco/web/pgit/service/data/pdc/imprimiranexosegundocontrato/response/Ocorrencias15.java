/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias15.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias15 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsRecTipoModalidade
     */
    private java.lang.String _dsRecTipoModalidade;

    /**
     * Field _dsRecMomentoEnvio
     */
    private java.lang.String _dsRecMomentoEnvio;

    /**
     * Field _dsRecDestino
     */
    private java.lang.String _dsRecDestino;

    /**
     * Field _dsRecCadastroEnderecoUtilizado
     */
    private java.lang.String _dsRecCadastroEnderecoUtilizado;

    /**
     * Field _qtRecDiaAvisoAntecipado
     */
    private int _qtRecDiaAvisoAntecipado = 0;

    /**
     * keeps track of state for field: _qtRecDiaAvisoAntecipado
     */
    private boolean _has_qtRecDiaAvisoAntecipado;

    /**
     * Field _dsRecPermissaoAgrupamento
     */
    private java.lang.String _dsRecPermissaoAgrupamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias15() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteQtRecDiaAvisoAntecipado
     * 
     */
    public void deleteQtRecDiaAvisoAntecipado()
    {
        this._has_qtRecDiaAvisoAntecipado= false;
    } //-- void deleteQtRecDiaAvisoAntecipado() 

    /**
     * Returns the value of field 'dsRecCadastroEnderecoUtilizado'.
     * 
     * @return String
     * @return the value of field 'dsRecCadastroEnderecoUtilizado'.
     */
    public java.lang.String getDsRecCadastroEnderecoUtilizado()
    {
        return this._dsRecCadastroEnderecoUtilizado;
    } //-- java.lang.String getDsRecCadastroEnderecoUtilizado() 

    /**
     * Returns the value of field 'dsRecDestino'.
     * 
     * @return String
     * @return the value of field 'dsRecDestino'.
     */
    public java.lang.String getDsRecDestino()
    {
        return this._dsRecDestino;
    } //-- java.lang.String getDsRecDestino() 

    /**
     * Returns the value of field 'dsRecMomentoEnvio'.
     * 
     * @return String
     * @return the value of field 'dsRecMomentoEnvio'.
     */
    public java.lang.String getDsRecMomentoEnvio()
    {
        return this._dsRecMomentoEnvio;
    } //-- java.lang.String getDsRecMomentoEnvio() 

    /**
     * Returns the value of field 'dsRecPermissaoAgrupamento'.
     * 
     * @return String
     * @return the value of field 'dsRecPermissaoAgrupamento'.
     */
    public java.lang.String getDsRecPermissaoAgrupamento()
    {
        return this._dsRecPermissaoAgrupamento;
    } //-- java.lang.String getDsRecPermissaoAgrupamento() 

    /**
     * Returns the value of field 'dsRecTipoModalidade'.
     * 
     * @return String
     * @return the value of field 'dsRecTipoModalidade'.
     */
    public java.lang.String getDsRecTipoModalidade()
    {
        return this._dsRecTipoModalidade;
    } //-- java.lang.String getDsRecTipoModalidade() 

    /**
     * Returns the value of field 'qtRecDiaAvisoAntecipado'.
     * 
     * @return int
     * @return the value of field 'qtRecDiaAvisoAntecipado'.
     */
    public int getQtRecDiaAvisoAntecipado()
    {
        return this._qtRecDiaAvisoAntecipado;
    } //-- int getQtRecDiaAvisoAntecipado() 

    /**
     * Method hasQtRecDiaAvisoAntecipado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtRecDiaAvisoAntecipado()
    {
        return this._has_qtRecDiaAvisoAntecipado;
    } //-- boolean hasQtRecDiaAvisoAntecipado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsRecCadastroEnderecoUtilizado'.
     * 
     * @param dsRecCadastroEnderecoUtilizado the value of field
     * 'dsRecCadastroEnderecoUtilizado'.
     */
    public void setDsRecCadastroEnderecoUtilizado(java.lang.String dsRecCadastroEnderecoUtilizado)
    {
        this._dsRecCadastroEnderecoUtilizado = dsRecCadastroEnderecoUtilizado;
    } //-- void setDsRecCadastroEnderecoUtilizado(java.lang.String) 

    /**
     * Sets the value of field 'dsRecDestino'.
     * 
     * @param dsRecDestino the value of field 'dsRecDestino'.
     */
    public void setDsRecDestino(java.lang.String dsRecDestino)
    {
        this._dsRecDestino = dsRecDestino;
    } //-- void setDsRecDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsRecMomentoEnvio'.
     * 
     * @param dsRecMomentoEnvio the value of field
     * 'dsRecMomentoEnvio'.
     */
    public void setDsRecMomentoEnvio(java.lang.String dsRecMomentoEnvio)
    {
        this._dsRecMomentoEnvio = dsRecMomentoEnvio;
    } //-- void setDsRecMomentoEnvio(java.lang.String) 

    /**
     * Sets the value of field 'dsRecPermissaoAgrupamento'.
     * 
     * @param dsRecPermissaoAgrupamento the value of field
     * 'dsRecPermissaoAgrupamento'.
     */
    public void setDsRecPermissaoAgrupamento(java.lang.String dsRecPermissaoAgrupamento)
    {
        this._dsRecPermissaoAgrupamento = dsRecPermissaoAgrupamento;
    } //-- void setDsRecPermissaoAgrupamento(java.lang.String) 

    /**
     * Sets the value of field 'dsRecTipoModalidade'.
     * 
     * @param dsRecTipoModalidade the value of field
     * 'dsRecTipoModalidade'.
     */
    public void setDsRecTipoModalidade(java.lang.String dsRecTipoModalidade)
    {
        this._dsRecTipoModalidade = dsRecTipoModalidade;
    } //-- void setDsRecTipoModalidade(java.lang.String) 

    /**
     * Sets the value of field 'qtRecDiaAvisoAntecipado'.
     * 
     * @param qtRecDiaAvisoAntecipado the value of field
     * 'qtRecDiaAvisoAntecipado'.
     */
    public void setQtRecDiaAvisoAntecipado(int qtRecDiaAvisoAntecipado)
    {
        this._qtRecDiaAvisoAntecipado = qtRecDiaAvisoAntecipado;
        this._has_qtRecDiaAvisoAntecipado = true;
    } //-- void setQtRecDiaAvisoAntecipado(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias15
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias15 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
