/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Nome: ImprimirAnexoSegundoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirAnexoSegundoContratoSaidaDTO {	

	/** The cd beneficio quantidade dias expiracao credito. */
	private Integer cdBeneficioQuantidadeDiasExpiracaoCredito;
	
	/** The cd cadastro quantidade dias inativos. */
	private Integer cdCadastroQuantidadeDiasInativos;
	
	/** The cd salario quantidade limite solicitacao cartao. */
	private Integer cdSalarioQuantidadeLimiteSolicitacaoCartao;
	
	/** The cd servico quantidade servico pagamento. */
	private Integer cdServicoQuantidadeServicoPagamento;        
	
	/** The qt aviso. */
	private Integer qtAviso;
	
	/** The qt aviso formulario. */
	private Integer qtAvisoFormulario;        
	
	/** The qt bpr dia anterior aviso. */
	private Integer qtBprDiaAnteriorAviso;
	
	/** The qt bpr dia anterior vencimento. */
	private Integer qtBprDiaAnteriorVencimento;
	
	/** The qt bpr mes provisorio. */
	private Integer qtBprMesProvisorio;
	
	/** The qt comprovante. */
	private Integer qtComprovante;
	
	/** The qt comprovante salario diversos. */
	private Integer qtComprovanteSalarioDiversos;
	
	/** The qt credito. */
	private Integer qtCredito;
	
	/** The qt for titulo. */
	private Integer qtForTitulo;        
	
	/** The qt modalidade beneficio. */
	private Integer qtModalidadeBeneficio;
	
	/** The qt modalidade fornecedor. */
	private Integer qtModalidadeFornecedor;        
	
	/** The qt modalidade recadastro. */
	private Integer qtModalidadeRecadastro;        
	
	/** The qt modalidade salario. */
	private Integer qtModalidadeSalario;
	
	/** The qt modalidade tributos. */
	private Integer qtModalidadeTributos;        
	
	/** The qt modularidade. */
	private Integer qtModularidade;
	
	/** The qt operacao. */
	private Integer qtOperacao;
	
	/** The qt rec etapa recadastramento. */
	private Integer qtRecEtapaRecadastramento;
	
	/** The qt rec fase etapa recadastramento. */
	private Integer qtRecFaseEtapaRecadastramento;
	
	/** The qt tri tributo. */
	private Integer qtTriTributo;        
	
	/** The lista aviso formularios. */
	private List<OcorrenciasAvisoFormularios> listaAvisoFormularios;
	
	/** The lista avisos. */
	private List<OcorrenciasAvisos> listaAvisos;        
	
	/** The lista comprovantes. */
	private List<OcorrenciasComprovantes> listaComprovantes;        
	
	/** The lista comprovante salario diversos. */
	private List<OcorrenciasComprovanteSalariosDiversos> listaComprovanteSalarioDiversos;        
	
	/** The lista creditos. */
	private List<OcorrenciasCreditos> listaCreditos;        
	
	/** The lista mod beneficio. */
	private List<OcorrenciasModBeneficios> listaModBeneficio;        
	
	/** The list mod fornecedores. */
	private List<OcorrenciasModFornecedores> listModFornecedores;        
	
	/** The lista mod recadastro. */
	private List<OcorrenciasModRecadastro> listaModRecadastro;        
	
	/** The lista mod salario. */
	private List<OcorrenciasModSalarial> listaModSalario;        
	
	/** The lista mod tributos. */
	private List<OcorrenciasModTributos> listaModTributos;        
	
	/** The lista modularidades. */
	private List<OcorrenciasModularidades> listaModularidades;        
	
	/** The lista operacoes. */
	private List<OcorrenciasOperacoes> listaOperacoes;        
	
	/** The lista servico pagamento. */
	private List<OcorrenciasServicoPagamentos> listaServicoPagamento;        
	
	/** The lista titulos. */
	private List<OcorrenciasTitulos> listaTitulos;        
	
	/** The lista tri tributos. */
	private List<OcorrenciasTriTributo> listaTriTributos;        
	
	/** The cd bpr indicador emissao aviso. */
	private String cdBprIndicadorEmissaoAviso;
	
	/** The cd bpr utilizado mensagem personalizada. */
	private String cdBprUtilizadoMensagemPersonalizada;
	
	/** The cd indcador cadastro favorecido. */
	private String cdIndcadorCadastroFavorecido;
	
	/** The cd indicador aviso. */
	private String cdIndicadorAviso;
	
	/** The cd indicador aviso favorecido. */
	private String cdIndicadorAvisoFavorecido;
	
	/** The cd indicador aviso pagador. */
	private String cdIndicadorAvisoPagador;
	
	/** The cd indicador complemento. */
	private String cdIndicadorComplemento;
	
	/** The cd indicador complemento divergente. */
	private String cdIndicadorComplementoDivergente;
	
	/** The cd indicador complemento favorecido. */
	private String cdIndicadorComplementoFavorecido;
	
	/** The cd indicador complemento pagador. */
	private String cdIndicadorComplementoPagador;
	
	/** The cd indicador complemento salarial. */
	private String cdIndicadorComplementoSalarial;
	
	/** The cd servico beneficio. */
	private String cdServicoBeneficio;        
	
	/** The cd servico fornecedor. */
	private String cdServicoFornecedor;
	
	/** The cd servico salarial. */
	private String cdServicoSalarial;
	
	/** The cd servico tributos. */
	private String cdServicoTributos;
	
	/** The cod mensagem. */
	private String codMensagem;
	
	/** The ds beneficio informado agencia conta. */
	private String dsBeneficioInformadoAgenciaConta;
	
	/** The ds beneficio possui expiracao credito. */
	private String dsBeneficioPossuiExpiracaoCredito;
	
	/** The ds beneficio tipo identificacao beneficio. */
	private String dsBeneficioTipoIdentificacaoBeneficio;
	
	/** The ds beneficio utilizacao cadastro organizacao. */
	private String dsBeneficioUtilizacaoCadastroOrganizacao;
	
	/** The ds beneficio utilizacao cadastro procuradores. */
	private String dsBeneficioUtilizacaoCadastroProcuradores;
	
	/** The ds bpr cadastro endereco utilizado. */
	private String dsBprCadastroEnderecoUtilizado;
	
	/** The ds bpr destino. */
	private String dsBprDestino;
	
	/** The ds bpr tipo acao. */
	private String dsBprTipoAcao;
	
	/** The ds cadastro forma manutencao. */
	private String dsCadastroFormaManutencao;
	
	/** The ds cadastro tipo consistencia inscricao. */
	private String dsCadastroTipoConsistenciaInscricao;
	
	/** The ds fra adesao sacado. */
	private String dsFraAdesaoSacado;
	
	/** The ds fra agenda cliente. */
	private String dsFraAgendaCliente;
	
	/** The ds fra agenda filial. */
	private String dsFraAgendaFilial;
	
	/** The ds fra bloqueio emissao. */
	private String dsFraBloqueioEmissao;
	
	/** The ds fra captacao titulo. */
	private String dsFraCaptacaoTitulo;
	
	/** The ds fra rastreabilidade nota. */
	private String dsFraRastreabilidadeNota;
	
	/** The ds fra rastreabilidade terceiro. */
	private String dsFraRastreabilidadeTerceiro;
	
	/** The ds fra tipo rastreabilidade. */
	private String dsFraTipoRastreabilidade;
	
	/** The ds rec condicao enquadramento. */
	private String dsRecCondicaoEnquadramento;
	
	/** The ds rec criterio principal enquadramento. */
	private String dsRecCriterioPrincipalEnquadramento;
	
	/** The ds rec gerador retorno internet. */
	private String dsRecGeradorRetornoInternet;
	
	/** The ds rec tipo consistencia identificacao. */
	private String dsRecTipoConsistenciaIdentificacao;
	
	/** The ds rec tipo criterio composto enquadramento. */
	private String dsRecTipoCriterioCompostoEnquadramento;
	
	/** The ds rec tipo identificacao beneficio. */
	private String dsRecTipoIdentificacaoBeneficio;
	
	/** The ds salario abertura conta expressa. */
	private String dsSalarioAberturaContaExpressa;
	
	/** The ds salario emissao antecipado cartao. */
	private String dsSalarioEmissaoAntecipadoCartao;
	
	/** The ds servico recadastro. */
	private String dsServicoRecadastro;
	
	/** The ds tdv tipo consulta proposta. */
	private String dsTdvTipoConsultaProposta;
	
	/** The ds tdv tipo tratamento valor. */
	private String dsTdvTipoTratamentoValor;
	
	/** The ds tibuto agendado debito veicular. */
	private String dsTibutoAgendadoDebitoVeicular;
	
	/** The dt fra inicio bloqueio. */
	private String dtFraInicioBloqueio;
	
	/** The dt fra inicio rastreabilidade. */
	private String dtFraInicioRastreabilidade;
	
	/** The dt rec final recadastramento. */
	private String dtRecFinalRecadastramento;
	
	/** The dt rec inicio recadastramento. */
	private String dtRecInicioRecadastramento;
	
	/** The mensagem. */
	private String mensagem;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdServicoFornecedor.
	 *
	 * @return cdServicoFornecedor
	 */
	public String getCdServicoFornecedor() {
		return cdServicoFornecedor;
	}
	
	/**
	 * Set: cdServicoFornecedor.
	 *
	 * @param cdServicoFornecedor the cd servico fornecedor
	 */
	public void setCdServicoFornecedor(String cdServicoFornecedor) {
		this.cdServicoFornecedor = cdServicoFornecedor;
	}
	
	/**
	 * Get: qtModalidadeFornecedor.
	 *
	 * @return qtModalidadeFornecedor
	 */
	public Integer getQtModalidadeFornecedor() {
		return qtModalidadeFornecedor;
	}
	
	/**
	 * Set: qtModalidadeFornecedor.
	 *
	 * @param qtModalidadeFornecedor the qt modalidade fornecedor
	 */
	public void setQtModalidadeFornecedor(Integer qtModalidadeFornecedor) {
		this.qtModalidadeFornecedor = qtModalidadeFornecedor;
	}
	
	/**
	 * Get: listModFornecedores.
	 *
	 * @return listModFornecedores
	 */
	public List<OcorrenciasModFornecedores> getListModFornecedores() {
		return listModFornecedores;
	}
	
	/**
	 * Set: listModFornecedores.
	 *
	 * @param listModFornecedores the list mod fornecedores
	 */
	public void setListModFornecedores(
			List<OcorrenciasModFornecedores> listModFornecedores) {
		this.listModFornecedores = listModFornecedores;
	}
	
	/**
	 * Get: cdServicoTributos.
	 *
	 * @return cdServicoTributos
	 */
	public String getCdServicoTributos() {
		return cdServicoTributos;
	}
	
	/**
	 * Set: cdServicoTributos.
	 *
	 * @param cdServicoTributos the cd servico tributos
	 */
	public void setCdServicoTributos(String cdServicoTributos) {
		this.cdServicoTributos = cdServicoTributos;
	}
	
	/**
	 * Get: qtModalidadeTributos.
	 *
	 * @return qtModalidadeTributos
	 */
	public Integer getQtModalidadeTributos() {
		return qtModalidadeTributos;
	}
	
	/**
	 * Set: qtModalidadeTributos.
	 *
	 * @param qtModalidadeTributos the qt modalidade tributos
	 */
	public void setQtModalidadeTributos(Integer qtModalidadeTributos) {
		this.qtModalidadeTributos = qtModalidadeTributos;
	}
	
	/**
	 * Get: listaModTributos.
	 *
	 * @return listaModTributos
	 */
	public List<OcorrenciasModTributos> getListaModTributos() {
		return listaModTributos;
	}
	
	/**
	 * Set: listaModTributos.
	 *
	 * @param listaModTributos the lista mod tributos
	 */
	public void setListaModTributos(List<OcorrenciasModTributos> listaModTributos) {
		this.listaModTributos = listaModTributos;
	}
	
	/**
	 * Get: cdServicoSalarial.
	 *
	 * @return cdServicoSalarial
	 */
	public String getCdServicoSalarial() {
		return cdServicoSalarial;
	}
	
	/**
	 * Set: cdServicoSalarial.
	 *
	 * @param cdServicoSalarial the cd servico salarial
	 */
	public void setCdServicoSalarial(String cdServicoSalarial) {
		this.cdServicoSalarial = cdServicoSalarial;
	}
	
	/**
	 * Get: qtModalidadeSalario.
	 *
	 * @return qtModalidadeSalario
	 */
	public Integer getQtModalidadeSalario() {
		return qtModalidadeSalario;
	}
	
	/**
	 * Set: qtModalidadeSalario.
	 *
	 * @param qtModalidadeSalario the qt modalidade salario
	 */
	public void setQtModalidadeSalario(Integer qtModalidadeSalario) {
		this.qtModalidadeSalario = qtModalidadeSalario;
	}
	
	/**
	 * Get: listaModSalario.
	 *
	 * @return listaModSalario
	 */
	public List<OcorrenciasModSalarial> getListaModSalario() {
		return listaModSalario;
	}
	
	/**
	 * Set: listaModSalario.
	 *
	 * @param listaModSalario the lista mod salario
	 */
	public void setListaModSalario(List<OcorrenciasModSalarial> listaModSalario) {
		this.listaModSalario = listaModSalario;
	}
	
	/**
	 * Get: cdServicoBeneficio.
	 *
	 * @return cdServicoBeneficio
	 */
	public String getCdServicoBeneficio() {
		return cdServicoBeneficio;
	}
	
	/**
	 * Set: cdServicoBeneficio.
	 *
	 * @param cdServicoBeneficio the cd servico beneficio
	 */
	public void setCdServicoBeneficio(String cdServicoBeneficio) {
		this.cdServicoBeneficio = cdServicoBeneficio;
	}
	
	/**
	 * Get: qtModalidadeBeneficio.
	 *
	 * @return qtModalidadeBeneficio
	 */
	public Integer getQtModalidadeBeneficio() {
		return qtModalidadeBeneficio;
	}
	
	/**
	 * Set: qtModalidadeBeneficio.
	 *
	 * @param qtModalidadeBeneficio the qt modalidade beneficio
	 */
	public void setQtModalidadeBeneficio(Integer qtModalidadeBeneficio) {
		this.qtModalidadeBeneficio = qtModalidadeBeneficio;
	}
	
	/**
	 * Get: listaModBeneficio.
	 *
	 * @return listaModBeneficio
	 */
	public List<OcorrenciasModBeneficios> getListaModBeneficio() {
		return listaModBeneficio;
	}
	
	/**
	 * Set: listaModBeneficio.
	 *
	 * @param listaModBeneficio the lista mod beneficio
	 */
	public void setListaModBeneficio(
			List<OcorrenciasModBeneficios> listaModBeneficio) {
		this.listaModBeneficio = listaModBeneficio;
	}
	
	/**
	 * Get: cdIndicadorAviso.
	 *
	 * @return cdIndicadorAviso
	 */
	public String getCdIndicadorAviso() {
		return cdIndicadorAviso;
	}
	
	/**
	 * Set: cdIndicadorAviso.
	 *
	 * @param cdIndicadorAviso the cd indicador aviso
	 */
	public void setCdIndicadorAviso(String cdIndicadorAviso) {
		this.cdIndicadorAviso = cdIndicadorAviso;
	}
	
	/**
	 * Get: cdIndicadorAvisoPagador.
	 *
	 * @return cdIndicadorAvisoPagador
	 */
	public String getCdIndicadorAvisoPagador() {
		return cdIndicadorAvisoPagador;
	}
	
	/**
	 * Set: cdIndicadorAvisoPagador.
	 *
	 * @param cdIndicadorAvisoPagador the cd indicador aviso pagador
	 */
	public void setCdIndicadorAvisoPagador(String cdIndicadorAvisoPagador) {
		this.cdIndicadorAvisoPagador = cdIndicadorAvisoPagador;
	}
	
	/**
	 * Get: cdIndicadorAvisoFavorecido.
	 *
	 * @return cdIndicadorAvisoFavorecido
	 */
	public String getCdIndicadorAvisoFavorecido() {
		return cdIndicadorAvisoFavorecido;
	}
	
	/**
	 * Set: cdIndicadorAvisoFavorecido.
	 *
	 * @param cdIndicadorAvisoFavorecido the cd indicador aviso favorecido
	 */
	public void setCdIndicadorAvisoFavorecido(String cdIndicadorAvisoFavorecido) {
		this.cdIndicadorAvisoFavorecido = cdIndicadorAvisoFavorecido;
	}
	
	/**
	 * Get: cdIndicadorComplemento.
	 *
	 * @return cdIndicadorComplemento
	 */
	public String getCdIndicadorComplemento() {
		return cdIndicadorComplemento;
	}
	
	/**
	 * Set: cdIndicadorComplemento.
	 *
	 * @param cdIndicadorComplemento the cd indicador complemento
	 */
	public void setCdIndicadorComplemento(String cdIndicadorComplemento) {
		this.cdIndicadorComplemento = cdIndicadorComplemento;
	}
	
	/**
	 * Get: cdIndicadorComplementoPagador.
	 *
	 * @return cdIndicadorComplementoPagador
	 */
	public String getCdIndicadorComplementoPagador() {
		return cdIndicadorComplementoPagador;
	}
	
	/**
	 * Set: cdIndicadorComplementoPagador.
	 *
	 * @param cdIndicadorComplementoPagador the cd indicador complemento pagador
	 */
	public void setCdIndicadorComplementoPagador(
			String cdIndicadorComplementoPagador) {
		this.cdIndicadorComplementoPagador = cdIndicadorComplementoPagador;
	}
	
	/**
	 * Get: cdIndicadorComplementoFavorecido.
	 *
	 * @return cdIndicadorComplementoFavorecido
	 */
	public String getCdIndicadorComplementoFavorecido() {
		return cdIndicadorComplementoFavorecido;
	}
	
	/**
	 * Set: cdIndicadorComplementoFavorecido.
	 *
	 * @param cdIndicadorComplementoFavorecido the cd indicador complemento favorecido
	 */
	public void setCdIndicadorComplementoFavorecido(
			String cdIndicadorComplementoFavorecido) {
		this.cdIndicadorComplementoFavorecido = cdIndicadorComplementoFavorecido;
	}
	
	/**
	 * Get: cdIndicadorComplementoSalarial.
	 *
	 * @return cdIndicadorComplementoSalarial
	 */
	public String getCdIndicadorComplementoSalarial() {
		return cdIndicadorComplementoSalarial;
	}
	
	/**
	 * Set: cdIndicadorComplementoSalarial.
	 *
	 * @param cdIndicadorComplementoSalarial the cd indicador complemento salarial
	 */
	public void setCdIndicadorComplementoSalarial(
			String cdIndicadorComplementoSalarial) {
		this.cdIndicadorComplementoSalarial = cdIndicadorComplementoSalarial;
	}
	
	/**
	 * Get: cdIndicadorComplementoDivergente.
	 *
	 * @return cdIndicadorComplementoDivergente
	 */
	public String getCdIndicadorComplementoDivergente() {
		return cdIndicadorComplementoDivergente;
	}
	
	/**
	 * Set: cdIndicadorComplementoDivergente.
	 *
	 * @param cdIndicadorComplementoDivergente the cd indicador complemento divergente
	 */
	public void setCdIndicadorComplementoDivergente(
			String cdIndicadorComplementoDivergente) {
		this.cdIndicadorComplementoDivergente = cdIndicadorComplementoDivergente;
	}
	
	/**
	 * Get: cdIndcadorCadastroFavorecido.
	 *
	 * @return cdIndcadorCadastroFavorecido
	 */
	public String getCdIndcadorCadastroFavorecido() {
		return cdIndcadorCadastroFavorecido;
	}
	
	/**
	 * Set: cdIndcadorCadastroFavorecido.
	 *
	 * @param cdIndcadorCadastroFavorecido the cd indcador cadastro favorecido
	 */
	public void setCdIndcadorCadastroFavorecido(String cdIndcadorCadastroFavorecido) {
		this.cdIndcadorCadastroFavorecido = cdIndcadorCadastroFavorecido;
	}
	
	/**
	 * Get: dsServicoRecadastro.
	 *
	 * @return dsServicoRecadastro
	 */
	public String getDsServicoRecadastro() {
		return dsServicoRecadastro;
	}
	
	/**
	 * Set: dsServicoRecadastro.
	 *
	 * @param dsServicoRecadastro the ds servico recadastro
	 */
	public void setDsServicoRecadastro(String dsServicoRecadastro) {
		this.dsServicoRecadastro = dsServicoRecadastro;
	}
	
	/**
	 * Get: qtModalidadeRecadastro.
	 *
	 * @return qtModalidadeRecadastro
	 */
	public Integer getQtModalidadeRecadastro() {
		return qtModalidadeRecadastro;
	}
	
	/**
	 * Set: qtModalidadeRecadastro.
	 *
	 * @param qtModalidadeRecadastro the qt modalidade recadastro
	 */
	public void setQtModalidadeRecadastro(Integer qtModalidadeRecadastro) {
		this.qtModalidadeRecadastro = qtModalidadeRecadastro;
	}
	
	/**
	 * Get: listaModRecadastro.
	 *
	 * @return listaModRecadastro
	 */
	public List<OcorrenciasModRecadastro> getListaModRecadastro() {
		return listaModRecadastro;
	}
	
	/**
	 * Set: listaModRecadastro.
	 *
	 * @param listaModRecadastro the lista mod recadastro
	 */
	public void setListaModRecadastro(
			List<OcorrenciasModRecadastro> listaModRecadastro) {
		this.listaModRecadastro = listaModRecadastro;
	}
	
	/**
	 * Get: dsSalarioEmissaoAntecipadoCartao.
	 *
	 * @return dsSalarioEmissaoAntecipadoCartao
	 */
	public String getDsSalarioEmissaoAntecipadoCartao() {
		return dsSalarioEmissaoAntecipadoCartao;
	}
	
	/**
	 * Set: dsSalarioEmissaoAntecipadoCartao.
	 *
	 * @param dsSalarioEmissaoAntecipadoCartao the ds salario emissao antecipado cartao
	 */
	public void setDsSalarioEmissaoAntecipadoCartao(
			String dsSalarioEmissaoAntecipadoCartao) {
		this.dsSalarioEmissaoAntecipadoCartao = dsSalarioEmissaoAntecipadoCartao;
	}
	
	/**
	 * Get: cdSalarioQuantidadeLimiteSolicitacaoCartao.
	 *
	 * @return cdSalarioQuantidadeLimiteSolicitacaoCartao
	 */
	public Integer getCdSalarioQuantidadeLimiteSolicitacaoCartao() {
		return cdSalarioQuantidadeLimiteSolicitacaoCartao;
	}
	
	/**
	 * Set: cdSalarioQuantidadeLimiteSolicitacaoCartao.
	 *
	 * @param cdSalarioQuantidadeLimiteSolicitacaoCartao the cd salario quantidade limite solicitacao cartao
	 */
	public void setCdSalarioQuantidadeLimiteSolicitacaoCartao(
			Integer cdSalarioQuantidadeLimiteSolicitacaoCartao) {
		this.cdSalarioQuantidadeLimiteSolicitacaoCartao = cdSalarioQuantidadeLimiteSolicitacaoCartao;
	}
	
	/**
	 * Get: dsSalarioAberturaContaExpressa.
	 *
	 * @return dsSalarioAberturaContaExpressa
	 */
	public String getDsSalarioAberturaContaExpressa() {
		return dsSalarioAberturaContaExpressa;
	}
	
	/**
	 * Set: dsSalarioAberturaContaExpressa.
	 *
	 * @param dsSalarioAberturaContaExpressa the ds salario abertura conta expressa
	 */
	public void setDsSalarioAberturaContaExpressa(
			String dsSalarioAberturaContaExpressa) {
		this.dsSalarioAberturaContaExpressa = dsSalarioAberturaContaExpressa;
	}
	
	/**
	 * Get: dsTibutoAgendadoDebitoVeicular.
	 *
	 * @return dsTibutoAgendadoDebitoVeicular
	 */
	public String getDsTibutoAgendadoDebitoVeicular() {
		return dsTibutoAgendadoDebitoVeicular;
	}
	
	/**
	 * Set: dsTibutoAgendadoDebitoVeicular.
	 *
	 * @param dsTibutoAgendadoDebitoVeicular the ds tibuto agendado debito veicular
	 */
	public void setDsTibutoAgendadoDebitoVeicular(
			String dsTibutoAgendadoDebitoVeicular) {
		this.dsTibutoAgendadoDebitoVeicular = dsTibutoAgendadoDebitoVeicular;
	}
	
	/**
	 * Get: dsBeneficioInformadoAgenciaConta.
	 *
	 * @return dsBeneficioInformadoAgenciaConta
	 */
	public String getDsBeneficioInformadoAgenciaConta() {
		return dsBeneficioInformadoAgenciaConta;
	}
	
	/**
	 * Set: dsBeneficioInformadoAgenciaConta.
	 *
	 * @param dsBeneficioInformadoAgenciaConta the ds beneficio informado agencia conta
	 */
	public void setDsBeneficioInformadoAgenciaConta(
			String dsBeneficioInformadoAgenciaConta) {
		this.dsBeneficioInformadoAgenciaConta = dsBeneficioInformadoAgenciaConta;
	}
	
	/**
	 * Get: dsBeneficioTipoIdentificacaoBeneficio.
	 *
	 * @return dsBeneficioTipoIdentificacaoBeneficio
	 */
	public String getDsBeneficioTipoIdentificacaoBeneficio() {
		return dsBeneficioTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Set: dsBeneficioTipoIdentificacaoBeneficio.
	 *
	 * @param dsBeneficioTipoIdentificacaoBeneficio the ds beneficio tipo identificacao beneficio
	 */
	public void setDsBeneficioTipoIdentificacaoBeneficio(
			String dsBeneficioTipoIdentificacaoBeneficio) {
		this.dsBeneficioTipoIdentificacaoBeneficio = dsBeneficioTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Get: dsBeneficioUtilizacaoCadastroOrganizacao.
	 *
	 * @return dsBeneficioUtilizacaoCadastroOrganizacao
	 */
	public String getDsBeneficioUtilizacaoCadastroOrganizacao() {
		return dsBeneficioUtilizacaoCadastroOrganizacao;
	}
	
	/**
	 * Set: dsBeneficioUtilizacaoCadastroOrganizacao.
	 *
	 * @param dsBeneficioUtilizacaoCadastroOrganizacao the ds beneficio utilizacao cadastro organizacao
	 */
	public void setDsBeneficioUtilizacaoCadastroOrganizacao(
			String dsBeneficioUtilizacaoCadastroOrganizacao) {
		this.dsBeneficioUtilizacaoCadastroOrganizacao = dsBeneficioUtilizacaoCadastroOrganizacao;
	}
	
	/**
	 * Get: dsBeneficioUtilizacaoCadastroProcuradores.
	 *
	 * @return dsBeneficioUtilizacaoCadastroProcuradores
	 */
	public String getDsBeneficioUtilizacaoCadastroProcuradores() {
		return dsBeneficioUtilizacaoCadastroProcuradores;
	}
	
	/**
	 * Set: dsBeneficioUtilizacaoCadastroProcuradores.
	 *
	 * @param dsBeneficioUtilizacaoCadastroProcuradores the ds beneficio utilizacao cadastro procuradores
	 */
	public void setDsBeneficioUtilizacaoCadastroProcuradores(
			String dsBeneficioUtilizacaoCadastroProcuradores) {
		this.dsBeneficioUtilizacaoCadastroProcuradores = dsBeneficioUtilizacaoCadastroProcuradores;
	}
	
	/**
	 * Get: dsBeneficioPossuiExpiracaoCredito.
	 *
	 * @return dsBeneficioPossuiExpiracaoCredito
	 */
	public String getDsBeneficioPossuiExpiracaoCredito() {
		return dsBeneficioPossuiExpiracaoCredito;
	}
	
	/**
	 * Set: dsBeneficioPossuiExpiracaoCredito.
	 *
	 * @param dsBeneficioPossuiExpiracaoCredito the ds beneficio possui expiracao credito
	 */
	public void setDsBeneficioPossuiExpiracaoCredito(
			String dsBeneficioPossuiExpiracaoCredito) {
		this.dsBeneficioPossuiExpiracaoCredito = dsBeneficioPossuiExpiracaoCredito;
	}
	
	/**
	 * Get: cdBeneficioQuantidadeDiasExpiracaoCredito.
	 *
	 * @return cdBeneficioQuantidadeDiasExpiracaoCredito
	 */
	public Integer getCdBeneficioQuantidadeDiasExpiracaoCredito() {
		return cdBeneficioQuantidadeDiasExpiracaoCredito;
	}
	
	/**
	 * Set: cdBeneficioQuantidadeDiasExpiracaoCredito.
	 *
	 * @param cdBeneficioQuantidadeDiasExpiracaoCredito the cd beneficio quantidade dias expiracao credito
	 */
	public void setCdBeneficioQuantidadeDiasExpiracaoCredito(
			Integer cdBeneficioQuantidadeDiasExpiracaoCredito) {
		this.cdBeneficioQuantidadeDiasExpiracaoCredito = cdBeneficioQuantidadeDiasExpiracaoCredito;
	}
	
	/**
	 * Get: cdServicoQuantidadeServicoPagamento.
	 *
	 * @return cdServicoQuantidadeServicoPagamento
	 */
	public Integer getCdServicoQuantidadeServicoPagamento() {
		return cdServicoQuantidadeServicoPagamento;
	}
	
	/**
	 * Set: cdServicoQuantidadeServicoPagamento.
	 *
	 * @param cdServicoQuantidadeServicoPagamento the cd servico quantidade servico pagamento
	 */
	public void setCdServicoQuantidadeServicoPagamento(
			Integer cdServicoQuantidadeServicoPagamento) {
		this.cdServicoQuantidadeServicoPagamento = cdServicoQuantidadeServicoPagamento;
	}
	
	/**
	 * Get: listaServicoPagamento.
	 *
	 * @return listaServicoPagamento
	 */
	public List<OcorrenciasServicoPagamentos> getListaServicoPagamento() {
		return listaServicoPagamento;
	}
	
	/**
	 * Set: listaServicoPagamento.
	 *
	 * @param listaServicoPagamento the lista servico pagamento
	 */
	public void setListaServicoPagamento(
			List<OcorrenciasServicoPagamentos> listaServicoPagamento) {
		this.listaServicoPagamento = listaServicoPagamento;
	}
	
	/**
	 * Get: qtAviso.
	 *
	 * @return qtAviso
	 */
	public Integer getQtAviso() {
		return qtAviso;
	}
	
	/**
	 * Set: qtAviso.
	 *
	 * @param qtAviso the qt aviso
	 */
	public void setQtAviso(Integer qtAviso) {
		this.qtAviso = qtAviso;
	}
	
	/**
	 * Get: listaAvisos.
	 *
	 * @return listaAvisos
	 */
	public List<OcorrenciasAvisos> getListaAvisos() {
		return listaAvisos;
	}
	
	/**
	 * Set: listaAvisos.
	 *
	 * @param listaAvisos the lista avisos
	 */
	public void setListaAvisos(List<OcorrenciasAvisos> listaAvisos) {
		this.listaAvisos = listaAvisos;
	}
	
	/**
	 * Get: qtComprovante.
	 *
	 * @return qtComprovante
	 */
	public Integer getQtComprovante() {
		return qtComprovante;
	}
	
	/**
	 * Set: qtComprovante.
	 *
	 * @param qtComprovante the qt comprovante
	 */
	public void setQtComprovante(Integer qtComprovante) {
		this.qtComprovante = qtComprovante;
	}
	
	/**
	 * Get: listaComprovantes.
	 *
	 * @return listaComprovantes
	 */
	public List<OcorrenciasComprovantes> getListaComprovantes() {
		return listaComprovantes;
	}
	
	/**
	 * Set: listaComprovantes.
	 *
	 * @param listaComprovantes the lista comprovantes
	 */
	public void setListaComprovantes(List<OcorrenciasComprovantes> listaComprovantes) {
		this.listaComprovantes = listaComprovantes;
	}
	
	/**
	 * Get: qtComprovanteSalarioDiversos.
	 *
	 * @return qtComprovanteSalarioDiversos
	 */
	public Integer getQtComprovanteSalarioDiversos() {
		return qtComprovanteSalarioDiversos;
	}
	
	/**
	 * Set: qtComprovanteSalarioDiversos.
	 *
	 * @param qtComprovanteSalarioDiversos the qt comprovante salario diversos
	 */
	public void setQtComprovanteSalarioDiversos(Integer qtComprovanteSalarioDiversos) {
		this.qtComprovanteSalarioDiversos = qtComprovanteSalarioDiversos;
	}
	
	/**
	 * Get: listaComprovanteSalarioDiversos.
	 *
	 * @return listaComprovanteSalarioDiversos
	 */
	public List<OcorrenciasComprovanteSalariosDiversos> getListaComprovanteSalarioDiversos() {
		return listaComprovanteSalarioDiversos;
	}
	
	/**
	 * Set: listaComprovanteSalarioDiversos.
	 *
	 * @param listaComprovanteSalarioDiversos the lista comprovante salario diversos
	 */
	public void setListaComprovanteSalarioDiversos(
			List<OcorrenciasComprovanteSalariosDiversos> listaComprovanteSalarioDiversos) {
		this.listaComprovanteSalarioDiversos = listaComprovanteSalarioDiversos;
	}
	
	/**
	 * Get: dsCadastroFormaManutencao.
	 *
	 * @return dsCadastroFormaManutencao
	 */
	public String getDsCadastroFormaManutencao() {
		return dsCadastroFormaManutencao;
	}
	
	/**
	 * Set: dsCadastroFormaManutencao.
	 *
	 * @param dsCadastroFormaManutencao the ds cadastro forma manutencao
	 */
	public void setDsCadastroFormaManutencao(String dsCadastroFormaManutencao) {
		this.dsCadastroFormaManutencao = dsCadastroFormaManutencao;
	}
	
	/**
	 * Get: cdCadastroQuantidadeDiasInativos.
	 *
	 * @return cdCadastroQuantidadeDiasInativos
	 */
	public Integer getCdCadastroQuantidadeDiasInativos() {
		return cdCadastroQuantidadeDiasInativos;
	}
	
	/**
	 * Set: cdCadastroQuantidadeDiasInativos.
	 *
	 * @param cdCadastroQuantidadeDiasInativos the cd cadastro quantidade dias inativos
	 */
	public void setCdCadastroQuantidadeDiasInativos(
			Integer cdCadastroQuantidadeDiasInativos) {
		this.cdCadastroQuantidadeDiasInativos = cdCadastroQuantidadeDiasInativos;
	}
	
	/**
	 * Get: dsCadastroTipoConsistenciaInscricao.
	 *
	 * @return dsCadastroTipoConsistenciaInscricao
	 */
	public String getDsCadastroTipoConsistenciaInscricao() {
		return dsCadastroTipoConsistenciaInscricao;
	}
	
	/**
	 * Set: dsCadastroTipoConsistenciaInscricao.
	 *
	 * @param dsCadastroTipoConsistenciaInscricao the ds cadastro tipo consistencia inscricao
	 */
	public void setDsCadastroTipoConsistenciaInscricao(
			String dsCadastroTipoConsistenciaInscricao) {
		this.dsCadastroTipoConsistenciaInscricao = dsCadastroTipoConsistenciaInscricao;
	}
	
	/**
	 * Get: dsRecTipoIdentificacaoBeneficio.
	 *
	 * @return dsRecTipoIdentificacaoBeneficio
	 */
	public String getDsRecTipoIdentificacaoBeneficio() {
		return dsRecTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Set: dsRecTipoIdentificacaoBeneficio.
	 *
	 * @param dsRecTipoIdentificacaoBeneficio the ds rec tipo identificacao beneficio
	 */
	public void setDsRecTipoIdentificacaoBeneficio(
			String dsRecTipoIdentificacaoBeneficio) {
		this.dsRecTipoIdentificacaoBeneficio = dsRecTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Get: dsRecTipoConsistenciaIdentificacao.
	 *
	 * @return dsRecTipoConsistenciaIdentificacao
	 */
	public String getDsRecTipoConsistenciaIdentificacao() {
		return dsRecTipoConsistenciaIdentificacao;
	}
	
	/**
	 * Set: dsRecTipoConsistenciaIdentificacao.
	 *
	 * @param dsRecTipoConsistenciaIdentificacao the ds rec tipo consistencia identificacao
	 */
	public void setDsRecTipoConsistenciaIdentificacao(
			String dsRecTipoConsistenciaIdentificacao) {
		this.dsRecTipoConsistenciaIdentificacao = dsRecTipoConsistenciaIdentificacao;
	}
	
	/**
	 * Get: dsRecCondicaoEnquadramento.
	 *
	 * @return dsRecCondicaoEnquadramento
	 */
	public String getDsRecCondicaoEnquadramento() {
		return dsRecCondicaoEnquadramento;
	}
	
	/**
	 * Set: dsRecCondicaoEnquadramento.
	 *
	 * @param dsRecCondicaoEnquadramento the ds rec condicao enquadramento
	 */
	public void setDsRecCondicaoEnquadramento(String dsRecCondicaoEnquadramento) {
		this.dsRecCondicaoEnquadramento = dsRecCondicaoEnquadramento;
	}
	
	/**
	 * Get: dsRecCriterioPrincipalEnquadramento.
	 *
	 * @return dsRecCriterioPrincipalEnquadramento
	 */
	public String getDsRecCriterioPrincipalEnquadramento() {
		return dsRecCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Set: dsRecCriterioPrincipalEnquadramento.
	 *
	 * @param dsRecCriterioPrincipalEnquadramento the ds rec criterio principal enquadramento
	 */
	public void setDsRecCriterioPrincipalEnquadramento(
			String dsRecCriterioPrincipalEnquadramento) {
		this.dsRecCriterioPrincipalEnquadramento = dsRecCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Get: dsRecTipoCriterioCompostoEnquadramento.
	 *
	 * @return dsRecTipoCriterioCompostoEnquadramento
	 */
	public String getDsRecTipoCriterioCompostoEnquadramento() {
		return dsRecTipoCriterioCompostoEnquadramento;
	}
	
	/**
	 * Set: dsRecTipoCriterioCompostoEnquadramento.
	 *
	 * @param dsRecTipoCriterioCompostoEnquadramento the ds rec tipo criterio composto enquadramento
	 */
	public void setDsRecTipoCriterioCompostoEnquadramento(
			String dsRecTipoCriterioCompostoEnquadramento) {
		this.dsRecTipoCriterioCompostoEnquadramento = dsRecTipoCriterioCompostoEnquadramento;
	}
	
	/**
	 * Get: qtRecEtapaRecadastramento.
	 *
	 * @return qtRecEtapaRecadastramento
	 */
	public Integer getQtRecEtapaRecadastramento() {
		return qtRecEtapaRecadastramento;
	}
	
	/**
	 * Set: qtRecEtapaRecadastramento.
	 *
	 * @param qtRecEtapaRecadastramento the qt rec etapa recadastramento
	 */
	public void setQtRecEtapaRecadastramento(Integer qtRecEtapaRecadastramento) {
		this.qtRecEtapaRecadastramento = qtRecEtapaRecadastramento;
	}
	
	/**
	 * Get: qtRecFaseEtapaRecadastramento.
	 *
	 * @return qtRecFaseEtapaRecadastramento
	 */
	public Integer getQtRecFaseEtapaRecadastramento() {
		return qtRecFaseEtapaRecadastramento;
	}
	
	/**
	 * Set: qtRecFaseEtapaRecadastramento.
	 *
	 * @param qtRecFaseEtapaRecadastramento the qt rec fase etapa recadastramento
	 */
	public void setQtRecFaseEtapaRecadastramento(
			Integer qtRecFaseEtapaRecadastramento) {
		this.qtRecFaseEtapaRecadastramento = qtRecFaseEtapaRecadastramento;
	}
	
	/**
	 * Get: dtRecInicioRecadastramento.
	 *
	 * @return dtRecInicioRecadastramento
	 */
	public String getDtRecInicioRecadastramento() {
		return dtRecInicioRecadastramento;
	}
	
	/**
	 * Set: dtRecInicioRecadastramento.
	 *
	 * @param dtRecInicioRecadastramento the dt rec inicio recadastramento
	 */
	public void setDtRecInicioRecadastramento(String dtRecInicioRecadastramento) {
		this.dtRecInicioRecadastramento = dtRecInicioRecadastramento;
	}
	
	/**
	 * Get: dtRecFinalRecadastramento.
	 *
	 * @return dtRecFinalRecadastramento
	 */
	public String getDtRecFinalRecadastramento() {
		return dtRecFinalRecadastramento;
	}
	
	/**
	 * Set: dtRecFinalRecadastramento.
	 *
	 * @param dtRecFinalRecadastramento the dt rec final recadastramento
	 */
	public void setDtRecFinalRecadastramento(String dtRecFinalRecadastramento) {
		this.dtRecFinalRecadastramento = dtRecFinalRecadastramento;
	}
	
	/**
	 * Get: dsRecGeradorRetornoInternet.
	 *
	 * @return dsRecGeradorRetornoInternet
	 */
	public String getDsRecGeradorRetornoInternet() {
		return dsRecGeradorRetornoInternet;
	}
	
	/**
	 * Set: dsRecGeradorRetornoInternet.
	 *
	 * @param dsRecGeradorRetornoInternet the ds rec gerador retorno internet
	 */
	public void setDsRecGeradorRetornoInternet(String dsRecGeradorRetornoInternet) {
		this.dsRecGeradorRetornoInternet = dsRecGeradorRetornoInternet;
	}
	
	/**
	 * Get: qtForTitulo.
	 *
	 * @return qtForTitulo
	 */
	public Integer getQtForTitulo() {
		return qtForTitulo;
	}
	
	/**
	 * Set: qtForTitulo.
	 *
	 * @param qtForTitulo the qt for titulo
	 */
	public void setQtForTitulo(Integer qtForTitulo) {
		this.qtForTitulo = qtForTitulo;
	}
	
	/**
	 * Get: listaTitulos.
	 *
	 * @return listaTitulos
	 */
	public List<OcorrenciasTitulos> getListaTitulos() {
		return listaTitulos;
	}
	
	/**
	 * Set: listaTitulos.
	 *
	 * @param listaTitulos the lista titulos
	 */
	public void setListaTitulos(List<OcorrenciasTitulos> listaTitulos) {
		this.listaTitulos = listaTitulos;
	}
	
	/**
	 * Get: qtCredito.
	 *
	 * @return qtCredito
	 */
	public Integer getQtCredito() {
		return qtCredito;
	}
	
	/**
	 * Set: qtCredito.
	 *
	 * @param qtCredito the qt credito
	 */
	public void setQtCredito(Integer qtCredito) {
		this.qtCredito = qtCredito;
	}
	
	/**
	 * Get: listaCreditos.
	 *
	 * @return listaCreditos
	 */
	public List<OcorrenciasCreditos> getListaCreditos() {
		return listaCreditos;
	}
	
	/**
	 * Set: listaCreditos.
	 *
	 * @param listaCreditos the lista creditos
	 */
	public void setListaCreditos(List<OcorrenciasCreditos> listaCreditos) {
		this.listaCreditos = listaCreditos;
	}
	
	/**
	 * Get: qtOperacao.
	 *
	 * @return qtOperacao
	 */
	public Integer getQtOperacao() {
		return qtOperacao;
	}
	
	/**
	 * Set: qtOperacao.
	 *
	 * @param qtOperacao the qt operacao
	 */
	public void setQtOperacao(Integer qtOperacao) {
		this.qtOperacao = qtOperacao;
	}
	
	/**
	 * Get: listaOperacoes.
	 *
	 * @return listaOperacoes
	 */
	public List<OcorrenciasOperacoes> getListaOperacoes() {
		return listaOperacoes;
	}
	
	/**
	 * Set: listaOperacoes.
	 *
	 * @param listaOperacoes the lista operacoes
	 */
	public void setListaOperacoes(List<OcorrenciasOperacoes> listaOperacoes) {
		this.listaOperacoes = listaOperacoes;
	}
	
	/**
	 * Get: qtModularidade.
	 *
	 * @return qtModularidade
	 */
	public Integer getQtModularidade() {
		return qtModularidade;
	}
	
	/**
	 * Set: qtModularidade.
	 *
	 * @param qtModularidade the qt modularidade
	 */
	public void setQtModularidade(Integer qtModularidade) {
		this.qtModularidade = qtModularidade;
	}
	
	/**
	 * Get: listaModularidades.
	 *
	 * @return listaModularidades
	 */
	public List<OcorrenciasModularidades> getListaModularidades() {
		return listaModularidades;
	}
	
	/**
	 * Set: listaModularidades.
	 *
	 * @param listaModularidades the lista modularidades
	 */
	public void setListaModularidades(
			List<OcorrenciasModularidades> listaModularidades) {
		this.listaModularidades = listaModularidades;
	}
	
	/**
	 * Get: dsFraTipoRastreabilidade.
	 *
	 * @return dsFraTipoRastreabilidade
	 */
	public String getDsFraTipoRastreabilidade() {
		return dsFraTipoRastreabilidade;
	}
	
	/**
	 * Set: dsFraTipoRastreabilidade.
	 *
	 * @param dsFraTipoRastreabilidade the ds fra tipo rastreabilidade
	 */
	public void setDsFraTipoRastreabilidade(String dsFraTipoRastreabilidade) {
		this.dsFraTipoRastreabilidade = dsFraTipoRastreabilidade;
	}
	
	/**
	 * Get: dsFraRastreabilidadeTerceiro.
	 *
	 * @return dsFraRastreabilidadeTerceiro
	 */
	public String getDsFraRastreabilidadeTerceiro() {
		return dsFraRastreabilidadeTerceiro;
	}
	
	/**
	 * Set: dsFraRastreabilidadeTerceiro.
	 *
	 * @param dsFraRastreabilidadeTerceiro the ds fra rastreabilidade terceiro
	 */
	public void setDsFraRastreabilidadeTerceiro(String dsFraRastreabilidadeTerceiro) {
		this.dsFraRastreabilidadeTerceiro = dsFraRastreabilidadeTerceiro;
	}
	
	/**
	 * Get: dsFraRastreabilidadeNota.
	 *
	 * @return dsFraRastreabilidadeNota
	 */
	public String getDsFraRastreabilidadeNota() {
		return dsFraRastreabilidadeNota;
	}
	
	/**
	 * Set: dsFraRastreabilidadeNota.
	 *
	 * @param dsFraRastreabilidadeNota the ds fra rastreabilidade nota
	 */
	public void setDsFraRastreabilidadeNota(String dsFraRastreabilidadeNota) {
		this.dsFraRastreabilidadeNota = dsFraRastreabilidadeNota;
	}
	
	/**
	 * Get: dsFraCaptacaoTitulo.
	 *
	 * @return dsFraCaptacaoTitulo
	 */
	public String getDsFraCaptacaoTitulo() {
		return dsFraCaptacaoTitulo;
	}
	
	/**
	 * Set: dsFraCaptacaoTitulo.
	 *
	 * @param dsFraCaptacaoTitulo the ds fra captacao titulo
	 */
	public void setDsFraCaptacaoTitulo(String dsFraCaptacaoTitulo) {
		this.dsFraCaptacaoTitulo = dsFraCaptacaoTitulo;
	}
	
	/**
	 * Get: dsFraAgendaCliente.
	 *
	 * @return dsFraAgendaCliente
	 */
	public String getDsFraAgendaCliente() {
		return dsFraAgendaCliente;
	}
	
	/**
	 * Set: dsFraAgendaCliente.
	 *
	 * @param dsFraAgendaCliente the ds fra agenda cliente
	 */
	public void setDsFraAgendaCliente(String dsFraAgendaCliente) {
		this.dsFraAgendaCliente = dsFraAgendaCliente;
	}
	
	/**
	 * Get: dsFraAgendaFilial.
	 *
	 * @return dsFraAgendaFilial
	 */
	public String getDsFraAgendaFilial() {
		return dsFraAgendaFilial;
	}
	
	/**
	 * Set: dsFraAgendaFilial.
	 *
	 * @param dsFraAgendaFilial the ds fra agenda filial
	 */
	public void setDsFraAgendaFilial(String dsFraAgendaFilial) {
		this.dsFraAgendaFilial = dsFraAgendaFilial;
	}
	
	/**
	 * Get: dsFraBloqueioEmissao.
	 *
	 * @return dsFraBloqueioEmissao
	 */
	public String getDsFraBloqueioEmissao() {
		return dsFraBloqueioEmissao;
	}
	
	/**
	 * Set: dsFraBloqueioEmissao.
	 *
	 * @param dsFraBloqueioEmissao the ds fra bloqueio emissao
	 */
	public void setDsFraBloqueioEmissao(String dsFraBloqueioEmissao) {
		this.dsFraBloqueioEmissao = dsFraBloqueioEmissao;
	}
	
	/**
	 * Get: dtFraInicioBloqueio.
	 *
	 * @return dtFraInicioBloqueio
	 */
	public String getDtFraInicioBloqueio() {
		return dtFraInicioBloqueio;
	}
	
	/**
	 * Set: dtFraInicioBloqueio.
	 *
	 * @param dtFraInicioBloqueio the dt fra inicio bloqueio
	 */
	public void setDtFraInicioBloqueio(String dtFraInicioBloqueio) {
		this.dtFraInicioBloqueio = dtFraInicioBloqueio;
	}
	
	/**
	 * Get: dtFraInicioRastreabilidade.
	 *
	 * @return dtFraInicioRastreabilidade
	 */
	public String getDtFraInicioRastreabilidade() {
		return dtFraInicioRastreabilidade;
	}
	
	/**
	 * Set: dtFraInicioRastreabilidade.
	 *
	 * @param dtFraInicioRastreabilidade the dt fra inicio rastreabilidade
	 */
	public void setDtFraInicioRastreabilidade(String dtFraInicioRastreabilidade) {
		this.dtFraInicioRastreabilidade = dtFraInicioRastreabilidade;
	}
	
	/**
	 * Get: dsFraAdesaoSacado.
	 *
	 * @return dsFraAdesaoSacado
	 */
	public String getDsFraAdesaoSacado() {
		return dsFraAdesaoSacado;
	}
	
	/**
	 * Set: dsFraAdesaoSacado.
	 *
	 * @param dsFraAdesaoSacado the ds fra adesao sacado
	 */
	public void setDsFraAdesaoSacado(String dsFraAdesaoSacado) {
		this.dsFraAdesaoSacado = dsFraAdesaoSacado;
	}
	
	/**
	 * Get: dsTdvTipoConsultaProposta.
	 *
	 * @return dsTdvTipoConsultaProposta
	 */
	public String getDsTdvTipoConsultaProposta() {
		return dsTdvTipoConsultaProposta;
	}
	
	/**
	 * Set: dsTdvTipoConsultaProposta.
	 *
	 * @param dsTdvTipoConsultaProposta the ds tdv tipo consulta proposta
	 */
	public void setDsTdvTipoConsultaProposta(String dsTdvTipoConsultaProposta) {
		this.dsTdvTipoConsultaProposta = dsTdvTipoConsultaProposta;
	}
	
	/**
	 * Get: dsTdvTipoTratamentoValor.
	 *
	 * @return dsTdvTipoTratamentoValor
	 */
	public String getDsTdvTipoTratamentoValor() {
		return dsTdvTipoTratamentoValor;
	}
	
	/**
	 * Set: dsTdvTipoTratamentoValor.
	 *
	 * @param dsTdvTipoTratamentoValor the ds tdv tipo tratamento valor
	 */
	public void setDsTdvTipoTratamentoValor(String dsTdvTipoTratamentoValor) {
		this.dsTdvTipoTratamentoValor = dsTdvTipoTratamentoValor;
	}
	
	/**
	 * Get: qtTriTributo.
	 *
	 * @return qtTriTributo
	 */
	public Integer getQtTriTributo() {
		return qtTriTributo;
	}
	
	/**
	 * Set: qtTriTributo.
	 *
	 * @param qtTriTributo the qt tri tributo
	 */
	public void setQtTriTributo(Integer qtTriTributo) {
		this.qtTriTributo = qtTriTributo;
	}
	
	/**
	 * Get: listaTriTributos.
	 *
	 * @return listaTriTributos
	 */
	public List<OcorrenciasTriTributo> getListaTriTributos() {
		return listaTriTributos;
	}
	
	/**
	 * Set: listaTriTributos.
	 *
	 * @param listaTriTributos the lista tri tributos
	 */
	public void setListaTriTributos(List<OcorrenciasTriTributo> listaTriTributos) {
		this.listaTriTributos = listaTriTributos;
	}
	
	/**
	 * Get: dsBprTipoAcao.
	 *
	 * @return dsBprTipoAcao
	 */
	public String getDsBprTipoAcao() {
		return dsBprTipoAcao;
	}
	
	/**
	 * Set: dsBprTipoAcao.
	 *
	 * @param dsBprTipoAcao the ds bpr tipo acao
	 */
	public void setDsBprTipoAcao(String dsBprTipoAcao) {
		this.dsBprTipoAcao = dsBprTipoAcao;
	}
	
	/**
	 * Get: qtBprMesProvisorio.
	 *
	 * @return qtBprMesProvisorio
	 */
	public Integer getQtBprMesProvisorio() {
		return qtBprMesProvisorio;
	}
	
	/**
	 * Set: qtBprMesProvisorio.
	 *
	 * @param qtBprMesProvisorio the qt bpr mes provisorio
	 */
	public void setQtBprMesProvisorio(Integer qtBprMesProvisorio) {
		this.qtBprMesProvisorio = qtBprMesProvisorio;
	}
	
	/**
	 * Get: cdBprIndicadorEmissaoAviso.
	 *
	 * @return cdBprIndicadorEmissaoAviso
	 */
	public String getCdBprIndicadorEmissaoAviso() {
		return cdBprIndicadorEmissaoAviso;
	}
	
	/**
	 * Set: cdBprIndicadorEmissaoAviso.
	 *
	 * @param cdBprIndicadorEmissaoAviso the cd bpr indicador emissao aviso
	 */
	public void setCdBprIndicadorEmissaoAviso(String cdBprIndicadorEmissaoAviso) {
		this.cdBprIndicadorEmissaoAviso = cdBprIndicadorEmissaoAviso;
	}
	
	/**
	 * Get: qtBprDiaAnteriorAviso.
	 *
	 * @return qtBprDiaAnteriorAviso
	 */
	public Integer getQtBprDiaAnteriorAviso() {
		return qtBprDiaAnteriorAviso;
	}
	
	/**
	 * Set: qtBprDiaAnteriorAviso.
	 *
	 * @param qtBprDiaAnteriorAviso the qt bpr dia anterior aviso
	 */
	public void setQtBprDiaAnteriorAviso(Integer qtBprDiaAnteriorAviso) {
		this.qtBprDiaAnteriorAviso = qtBprDiaAnteriorAviso;
	}
	
	/**
	 * Get: qtBprDiaAnteriorVencimento.
	 *
	 * @return qtBprDiaAnteriorVencimento
	 */
	public Integer getQtBprDiaAnteriorVencimento() {
		return qtBprDiaAnteriorVencimento;
	}
	
	/**
	 * Set: qtBprDiaAnteriorVencimento.
	 *
	 * @param qtBprDiaAnteriorVencimento the qt bpr dia anterior vencimento
	 */
	public void setQtBprDiaAnteriorVencimento(Integer qtBprDiaAnteriorVencimento) {
		this.qtBprDiaAnteriorVencimento = qtBprDiaAnteriorVencimento;
	}
	
	/**
	 * Get: cdBprUtilizadoMensagemPersonalizada.
	 *
	 * @return cdBprUtilizadoMensagemPersonalizada
	 */
	public String getCdBprUtilizadoMensagemPersonalizada() {
		return cdBprUtilizadoMensagemPersonalizada;
	}
	
	/**
	 * Set: cdBprUtilizadoMensagemPersonalizada.
	 *
	 * @param cdBprUtilizadoMensagemPersonalizada the cd bpr utilizado mensagem personalizada
	 */
	public void setCdBprUtilizadoMensagemPersonalizada(
			String cdBprUtilizadoMensagemPersonalizada) {
		this.cdBprUtilizadoMensagemPersonalizada = cdBprUtilizadoMensagemPersonalizada;
	}
	
	/**
	 * Get: dsBprDestino.
	 *
	 * @return dsBprDestino
	 */
	public String getDsBprDestino() {
		return dsBprDestino;
	}
	
	/**
	 * Set: dsBprDestino.
	 *
	 * @param dsBprDestino the ds bpr destino
	 */
	public void setDsBprDestino(String dsBprDestino) {
		this.dsBprDestino = dsBprDestino;
	}
	
	/**
	 * Get: dsBprCadastroEnderecoUtilizado.
	 *
	 * @return dsBprCadastroEnderecoUtilizado
	 */
	public String getDsBprCadastroEnderecoUtilizado() {
		return dsBprCadastroEnderecoUtilizado;
	}
	
	/**
	 * Set: dsBprCadastroEnderecoUtilizado.
	 *
	 * @param dsBprCadastroEnderecoUtilizado the ds bpr cadastro endereco utilizado
	 */
	public void setDsBprCadastroEnderecoUtilizado(
			String dsBprCadastroEnderecoUtilizado) {
		this.dsBprCadastroEnderecoUtilizado = dsBprCadastroEnderecoUtilizado;
	}
	
	/**
	 * Get: qtAvisoFormulario.
	 *
	 * @return qtAvisoFormulario
	 */
	public Integer getQtAvisoFormulario() {
		return qtAvisoFormulario;
	}
	
	/**
	 * Set: qtAvisoFormulario.
	 *
	 * @param qtAvisoFormulario the qt aviso formulario
	 */
	public void setQtAvisoFormulario(Integer qtAvisoFormulario) {
		this.qtAvisoFormulario = qtAvisoFormulario;
	}
	
	/**
	 * Get: listaAvisoFormularios.
	 *
	 * @return listaAvisoFormularios
	 */
	public List<OcorrenciasAvisoFormularios> getListaAvisoFormularios() {
		return listaAvisoFormularios;
	}
	
	/**
	 * Set: listaAvisoFormularios.
	 *
	 * @param listaAvisoFormularios the lista aviso formularios
	 */
	public void setListaAvisoFormularios(
			List<OcorrenciasAvisoFormularios> listaAvisoFormularios) {
		this.listaAvisoFormularios = listaAvisoFormularios;
	}
	
}
