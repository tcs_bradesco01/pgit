/*
 * Nome: br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean;


/**
 * Nome: IncRelFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncRelFavorecidoEntradaDTO {
    
    /** Atributo nrSolicitacao. */
    private Integer nrSolicitacao;
    
    /** Atributo tpRelatorio. */
    private Integer tpRelatorio;
    
    /** Atributo tpServico. */
    private Integer tpServico;
    
    /** Atributo tpEmpresaConglomerada. */
    private Long tpEmpresaConglomerada;
	
	/** Atributo tpDiretoriaRegional. */
	private Integer tpDiretoriaRegional;
	
	/** Atributo tpGerenciaRegional. */
	private Integer tpGerenciaRegional;
	
	/** Atributo cdAgenciaOperadora. */
	private Integer cdAgenciaOperadora;
	
	/** Atributo cdSegmento. */
	private Integer cdSegmento;
	
	/** Atributo cdParticipanteGrupoEconomico. */
	private Long cdParticipanteGrupoEconomico;
	
	/** Atributo cdAtividadeEconomica. */
	private Integer cdAtividadeEconomica;
	
	/** Atributo cdClasse. */
	private String cdClasse;
	
	/** Atributo cdRamo. */
	private Integer cdRamo;
	
	/** Atributo cdSubramo. */
	private Integer cdSubramo;
	
	/** Atributo tipoSolicitacao. */
	private String tipoSolicitacao;
	
	/** Atributo dsUnidadeOrganizacional. */
	private String dsUnidadeOrganizacional;
	
	/** Atributo dsDependencia. */
	private String dsDependencia;
	
	/** Atributo dsEmpresaConglomerada. */
	private String dsEmpresaConglomerada;
	
	/** Atributo dsSegmento. */
	private String dsSegmento;
	
	/** Atributo dtSolicitacao. */
	private String dtSolicitacao;
	
	/** Atributo dsRelatorio. */
	private String dsRelatorio;
	
	/** Atributo dsAgencia. */
	private String dsAgencia;
	
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Inc rel favorecido entrada dto.
	 */
	public IncRelFavorecidoEntradaDTO() {
	}
	
	/**
	 * Get: dsRelatorio.
	 *
	 * @return dsRelatorio
	 */
	public String getDsRelatorio() {
		return dsRelatorio;
	}
	
	/**
	 * Set: dsRelatorio.
	 *
	 * @param dsRelatorio the ds relatorio
	 */
	public void setDsRelatorio(String dsRelatorio) {
		this.dsRelatorio = dsRelatorio;
	}
	
	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public Integer getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}
	
	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(Integer cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: cdClasse.
	 *
	 * @return cdClasse
	 */
	public String getCdClasse() {
		return cdClasse;
	}
	
	/**
	 * Set: cdClasse.
	 *
	 * @param cdClasse the cd classe
	 */
	public void setCdClasse(String cdClasse) {
		this.cdClasse = cdClasse;
	}
	
	/**
	 * Get: cdParticipanteGrupoEconomico.
	 *
	 * @return cdParticipanteGrupoEconomico
	 */
	public Long getCdParticipanteGrupoEconomico() {
		return cdParticipanteGrupoEconomico;
	}
	
	/**
	 * Set: cdParticipanteGrupoEconomico.
	 *
	 * @param cdParticipanteGrupoEconomico the cd participante grupo economico
	 */
	public void setCdParticipanteGrupoEconomico(Long cdParticipanteGrupoEconomico) {
		this.cdParticipanteGrupoEconomico = cdParticipanteGrupoEconomico;
	}
	
	/**
	 * Get: cdRamo.
	 *
	 * @return cdRamo
	 */
	public Integer getCdRamo() {
		return cdRamo;
	}
	
	/**
	 * Set: cdRamo.
	 *
	 * @param cdRamo the cd ramo
	 */
	public void setCdRamo(Integer cdRamo) {
		this.cdRamo = cdRamo;
	}
	
	/**
	 * Get: cdSegmento.
	 *
	 * @return cdSegmento
	 */
	public Integer getCdSegmento() {
		return cdSegmento;
	}
	
	/**
	 * Set: cdSegmento.
	 *
	 * @param cdSegmento the cd segmento
	 */
	public void setCdSegmento(Integer cdSegmento) {
		this.cdSegmento = cdSegmento;
	}
	
	/**
	 * Get: cdSubramo.
	 *
	 * @return cdSubramo
	 */
	public Integer getCdSubramo() {
		return cdSubramo;
	}
	
	/**
	 * Set: cdSubramo.
	 *
	 * @param cdSubramo the cd subramo
	 */
	public void setCdSubramo(Integer cdSubramo) {
		this.cdSubramo = cdSubramo;
	}
	
	/**
	 * Get: dsDependencia.
	 *
	 * @return dsDependencia
	 */
	public String getDsDependencia() {
		return dsDependencia;
	}
	
	/**
	 * Set: dsDependencia.
	 *
	 * @param dsDependencia the ds dependencia
	 */
	public void setDsDependencia(String dsDependencia) {
		this.dsDependencia = dsDependencia;
	}
	
	/**
	 * Get: dsEmpresaConglomerada.
	 *
	 * @return dsEmpresaConglomerada
	 */
	public String getDsEmpresaConglomerada() {
		return dsEmpresaConglomerada;
	}
	
	/**
	 * Set: dsEmpresaConglomerada.
	 *
	 * @param dsEmpresaConglomerada the ds empresa conglomerada
	 */
	public void setDsEmpresaConglomerada(String dsEmpresaConglomerada) {
		this.dsEmpresaConglomerada = dsEmpresaConglomerada;
	}
	
	/**
	 * Get: dsSegmento.
	 *
	 * @return dsSegmento
	 */
	public String getDsSegmento() {
		return dsSegmento;
	}
	
	/**
	 * Set: dsSegmento.
	 *
	 * @param dsSegmento the ds segmento
	 */
	public void setDsSegmento(String dsSegmento) {
		this.dsSegmento = dsSegmento;
	}
	
	/**
	 * Get: dsUnidadeOrganizacional.
	 *
	 * @return dsUnidadeOrganizacional
	 */
	public String getDsUnidadeOrganizacional() {
		return dsUnidadeOrganizacional;
	}
	
	/**
	 * Set: dsUnidadeOrganizacional.
	 *
	 * @param dsUnidadeOrganizacional the ds unidade organizacional
	 */
	public void setDsUnidadeOrganizacional(String dsUnidadeOrganizacional) {
		this.dsUnidadeOrganizacional = dsUnidadeOrganizacional;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: tipoSolicitacao.
	 *
	 * @return tipoSolicitacao
	 */
	public String getTipoSolicitacao() {
		return tipoSolicitacao;
	}
	
	/**
	 * Set: tipoSolicitacao.
	 *
	 * @param tipoSolicitacao the tipo solicitacao
	 */
	public void setTipoSolicitacao(String tipoSolicitacao) {
		this.tipoSolicitacao = tipoSolicitacao;
	}
	
	/**
	 * Get: tpDiretoriaRegional.
	 *
	 * @return tpDiretoriaRegional
	 */
	public Integer getTpDiretoriaRegional() {
		return tpDiretoriaRegional;
	}
	
	/**
	 * Set: tpDiretoriaRegional.
	 *
	 * @param tpDiretoriaRegional the tp diretoria regional
	 */
	public void setTpDiretoriaRegional(Integer tpDiretoriaRegional) {
		this.tpDiretoriaRegional = tpDiretoriaRegional;
	}
	
	/**
	 * Get: tpEmpresaConglomerada.
	 *
	 * @return tpEmpresaConglomerada
	 */
	public Long getTpEmpresaConglomerada() {
		return tpEmpresaConglomerada;
	}
	
	/**
	 * Set: tpEmpresaConglomerada.
	 *
	 * @param tpEmpresaConglomerada the tp empresa conglomerada
	 */
	public void setTpEmpresaConglomerada(Long tpEmpresaConglomerada) {
		this.tpEmpresaConglomerada = tpEmpresaConglomerada;
	}
	
	/**
	 * Get: tpGerenciaRegional.
	 *
	 * @return tpGerenciaRegional
	 */
	public Integer getTpGerenciaRegional() {
		return tpGerenciaRegional;
	}
	
	/**
	 * Set: tpGerenciaRegional.
	 *
	 * @param tpGerenciaRegional the tp gerencia regional
	 */
	public void setTpGerenciaRegional(Integer tpGerenciaRegional) {
		this.tpGerenciaRegional = tpGerenciaRegional;
	}
	
	/**
	 * Get: tpRelatorio.
	 *
	 * @return tpRelatorio
	 */
	public Integer getTpRelatorio() {
		return tpRelatorio;
	}
	
	/**
	 * Set: tpRelatorio.
	 *
	 * @param tpRelatorio the tp relatorio
	 */
	public void setTpRelatorio(Integer tpRelatorio) {
		this.tpRelatorio = tpRelatorio;
	}
	
	/**
	 * Get: tpServico.
	 *
	 * @return tpServico
	 */
	public Integer getTpServico() {
		return tpServico;
	}
	
	/**
	 * Set: tpServico.
	 *
	 * @param tpServico the tp servico
	 */
	public void setTpServico(Integer tpServico) {
		this.tpServico = tpServico;
	}
	
	/**
	 * Get: dtSolicitacao.
	 *
	 * @return dtSolicitacao
	 */
	public String getDtSolicitacao() {
		return dtSolicitacao;
	}
	
	/**
	 * Set: dtSolicitacao.
	 *
	 * @param dtSolicitacao the dt solicitacao
	 */
	public void setDtSolicitacao(String dtSolicitacao) {
		this.dtSolicitacao = dtSolicitacao;
	}
	
	/**
	 * Get: dsUnidadeOrganizacionalStr.
	 *
	 * @return dsUnidadeOrganizacionalStr
	 */
	public String getDsUnidadeOrganizacionalStr(){
		return this.dsUnidadeOrganizacional == null || this.dsUnidadeOrganizacional.equals(0) ? "" : dsUnidadeOrganizacional;
	}
	
	/**
	 * Get: dsDependenciaStr.
	 *
	 * @return dsDependenciaStr
	 */
	public String getDsDependenciaStr(){
		return this.dsDependencia == null || this.dsDependencia.equals(0) ? "" : dsDependencia;
	}
	
	/**
	 * Get: cdAgenciaOperadoraStr.
	 *
	 * @return cdAgenciaOperadoraStr
	 */
	public String getCdAgenciaOperadoraStr(){
		return this.cdAgenciaOperadora == null || this.cdAgenciaOperadora.equals(0) ? "" : cdAgenciaOperadora.toString();
	}
	
	/**
	 * Get: dsSegmentoStr.
	 *
	 * @return dsSegmentoStr
	 */
	public String getDsSegmentoStr(){
		return this.dsSegmento == null || this.dsSegmento.equals(0) ? "" : dsSegmento;
	}
	
	/**
	 * Get: cdParticipanteGrupoEconomicoStr.
	 *
	 * @return cdParticipanteGrupoEconomicoStr
	 */
	public String getCdParticipanteGrupoEconomicoStr(){
		return this.cdParticipanteGrupoEconomico == null || this.cdParticipanteGrupoEconomico.equals(0L) ? "" : cdParticipanteGrupoEconomico.toString();
	}
	
	/**
	 * Get: cdClasseStr.
	 *
	 * @return cdClasseStr
	 */
	public String getCdClasseStr(){
		return this.cdClasse == null || this.cdClasse.equals(0) ? "" : cdClasse;
	}
	
	/**
	 * Get: cdRamoStr.
	 *
	 * @return cdRamoStr
	 */
	public String getCdRamoStr(){
		return this.cdRamo == null || this.cdRamo.equals(0) ? "" : cdRamo.toString();
	}
	
	/**
	 * Get: cdSubramoStr.
	 *
	 * @return cdSubramoStr
	 */
	public String getCdSubramoStr(){
		return this.cdSubramo == null || this.cdSubramo.equals(0) ? "" : cdSubramo.toString();
	}
	
	/**
	 * Get: cdAtividadeEconomicaStr.
	 *
	 * @return cdAtividadeEconomicaStr
	 */
	public String getCdAtividadeEconomicaStr(){
		return this.cdAtividadeEconomica == null || this.cdAtividadeEconomica.equals(0) ? "" : cdAtividadeEconomica.toString();
	}
	
	
}
