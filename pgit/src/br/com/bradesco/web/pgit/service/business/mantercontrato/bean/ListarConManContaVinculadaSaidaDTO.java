/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarConManContaVinculadaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarConManContaVinculadaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdPessoaJuridicaVinculo. */
	private Long cdPessoaJuridicaVinculo;

	/** Atributo cdTipoContratoVinculo. */
	private Integer cdTipoContratoVinculo;

	/** Atributo nrSequenciaContratoVinculo. */
	private Long nrSequenciaContratoVinculo;

	/** Atributo hrInclusaoRegistroHistorico. */
	private String hrInclusaoRegistroHistorico;

	/** Atributo cdBanco. */
	private Integer cdBanco;

	/** Atributo dsBanco. */
	private String dsBanco;

	/** Atributo cdAgencia. */
	private Integer cdAgencia;

	/** Atributo cdDigitoAgencia. */
	private Integer cdDigitoAgencia;

	/** Atributo dsAgencia. */
	private String dsAgencia;

	/** Atributo cdConta. */
	private Long cdConta;

	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;

	/** Atributo dtManutencao. */
	private String dtManutencao;

	/** Atributo hrManutencao. */
	private String hrManutencao;

	/** Atributo cdUsuario. */
	private String cdUsuario;

	/** Atributo cdSituacaoConta. */
	private Integer cdSituacaoConta;

	/** Atributo dsSituacaoConta. */
	private String dsSituacaoConta;

	/** Atributo cdTipoConta. */
	private Integer cdTipoConta;

	/** Atributo dsTipoConta. */
	private String dsTipoConta;

	/** Atributo cdCpfCnpj. */
	private String cdCpfCnpj;

	/** Atributo dsNomeParticipante. */
	private String dsNomeParticipante;

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}

	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}

	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}

	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}

	/**
	 * Get: cdSituacaoConta.
	 *
	 * @return cdSituacaoConta
	 */
	public Integer getCdSituacaoConta() {
		return cdSituacaoConta;
	}

	/**
	 * Set: cdSituacaoConta.
	 *
	 * @param cdSituacaoConta the cd situacao conta
	 */
	public void setCdSituacaoConta(Integer cdSituacaoConta) {
		this.cdSituacaoConta = cdSituacaoConta;
	}

	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}

	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}

	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}

	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}

	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}

	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}

	/**
	 * Get: dsSituacaoConta.
	 *
	 * @return dsSituacaoConta
	 */
	public String getDsSituacaoConta() {
		return dsSituacaoConta;
	}

	/**
	 * Set: dsSituacaoConta.
	 *
	 * @param dsSituacaoConta the ds situacao conta
	 */
	public void setDsSituacaoConta(String dsSituacaoConta) {
		this.dsSituacaoConta = dsSituacaoConta;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao.replace(".", "/");
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrInclusaoRegistroHistorico.
	 *
	 * @return hrInclusaoRegistroHistorico
	 */
	public String getHrInclusaoRegistroHistorico() {
		return hrInclusaoRegistroHistorico;
	}

	/**
	 * Set: hrInclusaoRegistroHistorico.
	 *
	 * @param hrInclusaoRegistroHistorico the hr inclusao registro historico
	 */
	public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
		this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}

	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}

	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public String getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(String cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * Get: dsNomeParticipante.
	 *
	 * @return dsNomeParticipante
	 */
	public String getDsNomeParticipante() {
		return dsNomeParticipante;
	}

	/**
	 * Set: dsNomeParticipante.
	 *
	 * @param dsNomeParticipante the ds nome participante
	 */
	public void setDsNomeParticipante(String dsNomeParticipante) {
		this.dsNomeParticipante = dsNomeParticipante;
	}

}
