/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantermensagemlayout.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutService;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.AlterarMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.AlterarMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ConsultarMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ConsultarMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ExcluirMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ExcluirMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.IncluirMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.IncluirMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.request.AlterarMsgLayoutArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsglayoutarqretorno.response.AlterarMsgLayoutArqRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.request.ConsultarMsgLayoutArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.response.ConsultarMsgLayoutArqRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.request.DetalharMsgLayoutArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.response.DetalharMsgLayoutArqRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.request.ExcluirMsgLayoutArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsglayoutarqretorno.response.ExcluirMsgLayoutArqRetornoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.request.IncluirMsgLayoutArqRetornoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsglayoutarqretorno.response.IncluirMsgLayoutArqRetornoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterMensagemLayout
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterMensagemLayoutServiceImpl implements IManterMensagemLayoutService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutService#consultarMsgLayoutArqRetorno(br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ConsultarMsgLayoutArqRetornoEntradaDTO)
     */
    public List<ConsultarMsgLayoutArqRetornoSaidaDTO> consultarMsgLayoutArqRetorno(ConsultarMsgLayoutArqRetornoEntradaDTO entrada){
		List<ConsultarMsgLayoutArqRetornoSaidaDTO> listaSaida = new ArrayList<ConsultarMsgLayoutArqRetornoSaidaDTO>();
		ConsultarMsgLayoutArqRetornoRequest request = new ConsultarMsgLayoutArqRetornoRequest();
		ConsultarMsgLayoutArqRetornoResponse response = new ConsultarMsgLayoutArqRetornoResponse();

		request.setQtdeConsultas(IManterMensagemLayoutServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);
		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setNrMensagemArquivoRetorno(PgitUtil.verificaIntegerNulo(entrada.getNrMensagemArquivoRetorno()));
		request.setCdMensagemArquivoRetorno(PgitUtil.verificaStringNula(entrada.getCdMensagemArquivoRetorno()));

		response = getFactoryAdapter().getConsultarMsgLayoutArqRetornoPDCAdapter().invokeProcess(request);

		for(int i=0;i<response.getOcorrenciasCount();i++){
			ConsultarMsgLayoutArqRetornoSaidaDTO saida = new ConsultarMsgLayoutArqRetornoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setNumeroLinhas(response.getNumeroLinhas());
			saida.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
			saida.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
			saida.setNrMensagemArquivoRetorno(response.getOcorrencias(i).getNrMensagemArquivoRetorno());
			saida.setCdMensagemArquivoRetorno(response.getOcorrencias(i).getCdMensagemArquivoRetorno());
			saida.setCdTipoMensagemRetorno(response.getOcorrencias(i).getCdTipoMensagemRetorno());
			saida.setDsMensagemLayoutRetorno(response.getOcorrencias(i).getDsMensagemLayoutRetorno());
			saida.setDsTipoMansagemRetorno(response.getOcorrencias(i).getDsTipoMansagemRetorno());
			saida.setTipoLayoutArquivoFormatado(PgitUtil.concatenarCampos(saida.getCdTipoLayoutArquivo(), saida.getDsTipoLayoutArquivo()));
			listaSaida.add(saida);
		}

		return listaSaida;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutService#incluirMsgLayoutArqRetorno(br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.IncluirMsgLayoutArqRetornoEntradaDTO)
     */
    public IncluirMsgLayoutArqRetornoSaidaDTO incluirMsgLayoutArqRetorno(IncluirMsgLayoutArqRetornoEntradaDTO entrada){
		IncluirMsgLayoutArqRetornoSaidaDTO saida = new IncluirMsgLayoutArqRetornoSaidaDTO();
		IncluirMsgLayoutArqRetornoRequest request = new IncluirMsgLayoutArqRetornoRequest();
		IncluirMsgLayoutArqRetornoResponse response = new IncluirMsgLayoutArqRetornoResponse();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setCdMensagemArquivoRetorno(PgitUtil.verificaStringNula(entrada.getCdMensagemArquivoRetorno()));
		request.setCdTipoMensagemRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdTipoMensagemRetorno()));
		request.setNrPosicaoinicialmensagem(PgitUtil.verificaIntegerNulo(entrada.getNrPosicaoinicialmensagem()));
		request.setNrPosicaoFinalMensagem(PgitUtil.verificaIntegerNulo(entrada.getNrPosicaoFinalMensagem()));
		request.setCdNivelMensagemLayout(PgitUtil.verificaIntegerNulo(entrada.getCdNivelMensagemLayout()));
		request.setDsMensagemLayoutRetorno(PgitUtil.verificaStringNula(entrada.getDsMensagemLayoutRetorno()));

		response = getFactoryAdapter().getIncluirMsgLayoutArqRetornoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutService#excluirMsgLayoutArqRetorno(br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ExcluirMsgLayoutArqRetornoEntradaDTO)
     */
    public ExcluirMsgLayoutArqRetornoSaidaDTO excluirMsgLayoutArqRetorno(ExcluirMsgLayoutArqRetornoEntradaDTO entrada){
		ExcluirMsgLayoutArqRetornoSaidaDTO saida = new ExcluirMsgLayoutArqRetornoSaidaDTO();
		ExcluirMsgLayoutArqRetornoRequest request = new ExcluirMsgLayoutArqRetornoRequest();
		ExcluirMsgLayoutArqRetornoResponse response = new ExcluirMsgLayoutArqRetornoResponse();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setNrMensagemArquivoRetorno(PgitUtil.verificaIntegerNulo(entrada.getNrMensagemArquivoRetorno()));
		//request.setCdMensagemArquivoRetorno(PgitUtil.verificaStringNula(entrada.getCdMensagemArquivoRetorno()));

		response = getFactoryAdapter().getExcluirMsgLayoutArqRetornoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutService#alterarMsgLayoutArqRetorno(br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.AlterarMsgLayoutArqRetornoEntradaDTO)
     */
    public AlterarMsgLayoutArqRetornoSaidaDTO alterarMsgLayoutArqRetorno(AlterarMsgLayoutArqRetornoEntradaDTO entrada){
		AlterarMsgLayoutArqRetornoSaidaDTO saida = new AlterarMsgLayoutArqRetornoSaidaDTO();
		AlterarMsgLayoutArqRetornoRequest request = new AlterarMsgLayoutArqRetornoRequest();
		AlterarMsgLayoutArqRetornoResponse response = new AlterarMsgLayoutArqRetornoResponse();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setNrMensagemArquivoRetorno(PgitUtil.verificaIntegerNulo(entrada.getNrMensagemArquivoRetorno()));
		request.setCdMensagemArquivoRetorno(PgitUtil.verificaStringNula(entrada.getCdMensagemArquivoRetorno()));
		request.setCdTipoMensagemRetorno(PgitUtil.verificaIntegerNulo(entrada.getCdTipoMensagemRetorno()));
		request.setNrPosicaoInicialMensagem(PgitUtil.verificaIntegerNulo(entrada.getNrPosicaoInicialMensagem()));
		request.setNrPosicaoFinalMensagem(PgitUtil.verificaIntegerNulo(entrada.getNrPosicaoFinalMensagem()));
		request.setCdNivelMensagemLayout(PgitUtil.verificaIntegerNulo(entrada.getCdNivelMensagemLayout()));
		request.setDsMensagemLayoutRetorno(PgitUtil.verificaStringNula(entrada.getDsMensagemLayoutRetorno()));

		response = getFactoryAdapter().getAlterarMsgLayoutArqRetornoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutService#detalharMsgLayoutArqRetorno(br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoEntradaDTO)
     */
    public DetalharMsgLayoutArqRetornoSaidaDTO detalharMsgLayoutArqRetorno(DetalharMsgLayoutArqRetornoEntradaDTO entrada){
		DetalharMsgLayoutArqRetornoSaidaDTO saida = new DetalharMsgLayoutArqRetornoSaidaDTO();
		DetalharMsgLayoutArqRetornoRequest request = new DetalharMsgLayoutArqRetornoRequest();
		DetalharMsgLayoutArqRetornoResponse response = new DetalharMsgLayoutArqRetornoResponse();

		request.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
		request.setNrMensagemArquivoRetorno(PgitUtil.verificaIntegerNulo(entrada.getNrMensagemArquivoRetorno()));
		request.setCdMensagemArquivoRetorno(PgitUtil.verificaStringNula(entrada.getCdMensagemArquivoRetorno()));
		request.setCdTipoMensagemRetorno(0);
		request.setNrPosicaoInicialMensagem(0);
		request.setNrPosicaoFinalMensagem(0);
		request.setCdNivelMensagemLayout(0);
		request.setDsMensagemLayoutRetorno("");

		response = getFactoryAdapter().getDetalharMsgLayoutArqRetornoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdTipoLayoutArquivo(response.getCdTipoLayoutArquivo());
		saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
		saida.setCdMensagemArquivoRetorno(response.getCdMensagemArquivoRetorno());
		saida.setCdTipoMensagemRetorno(response.getCdTipoMensagemRetorno());
		saida.setNrPosicaoInicialMensagem(response.getNrPosicaoInicialMensagem());
		saida.setNrPosicaoFinalMensagem(response.getNrPosicaoFinalMensagem());
		saida.setCdNivelMensagemLayout(response.getCdNivelMensagemLayout());
		saida.setDsMensagemLayoutRetorno(response.getDsMensagemLayoutRetorno());
		saida.setCdCanalInclusao(response.getCdCanalInclusao());
		saida.setDsCanalInclusao(response.getDsCanalInclusao());
		saida.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
		saida.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
		saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saida.setCdCanalManutencao(response.getCdCanalManutencao());
		saida.setDsCanalManutencao(response.getDsCanalManutencao());
		saida.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
		saida.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
		saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());

		return saida;
	}
}

