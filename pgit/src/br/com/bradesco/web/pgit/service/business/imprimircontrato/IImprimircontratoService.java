/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.imprimircontrato;

import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Imprimircontrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IImprimircontratoService {

	/**
	 * Imprimir contrato saida dto.
	 *
	 * @param imprimirContratoEntradaDTO the imprimir contrato entrada dto
	 * @return the imprimir contrato saida dto
	 */
	ImprimirContratoSaidaDTO imprimirContratoSaidaDTO(ImprimirContratoEntradaDTO imprimirContratoEntradaDTO);

	/**
	 * Imprimir contrato multicanal.
	 *
	 * @param imprimirContratoEntradaDTO the imprimir contrato entrada dto
	 * @return the long
	 */
	long imprimirContratoMulticanal(ImprimirContratoEntradaDTO imprimirContratoEntradaDTO);
}

