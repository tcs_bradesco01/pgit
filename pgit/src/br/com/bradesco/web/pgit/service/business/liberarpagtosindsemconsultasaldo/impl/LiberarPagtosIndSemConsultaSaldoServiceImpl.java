/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.ILiberarPagtosIndSemConsultaSaldoService;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.ILiberarPagtosIndSemConsultaSaldoServiceConstants;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.ConsultarLibPagtosIndSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.ConsultarLibPagtosIndSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.LiberarPagtosIndSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.LiberarPagtosIndSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlibpagtosindsemconssaldo.request.ConsultarLibPagtosIndSemConsSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlibpagtosindsemconssaldo.response.ConsultarLibPagtosIndSemConsSaldoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtosindsemconsultasaldo.request.LiberarPagtosIndSemConsultaSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.liberarpagtosindsemconsultasaldo.response.LiberarPagtosIndSemConsultaSaldoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: LiberarPagtosIndSemConsultaSaldo
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class LiberarPagtosIndSemConsultaSaldoServiceImpl implements ILiberarPagtosIndSemConsultaSaldoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	    
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public LiberarPagtosIndSemConsultaSaldoServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.ILiberarPagtosIndSemConsultaSaldoService#consultarLibPagtosIndSemConsSaldo(br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.ConsultarLibPagtosIndSemConsSaldoEntradaDTO)
     */
    public List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> consultarLibPagtosIndSemConsSaldo(ConsultarLibPagtosIndSemConsSaldoEntradaDTO entradaDTO){
		
    	ConsultarLibPagtosIndSemConsSaldoRequest request = new ConsultarLibPagtosIndSemConsSaldoRequest();
    	ConsultarLibPagtosIndSemConsSaldoResponse response = new ConsultarLibPagtosIndSemConsSaldoResponse();
    	List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> listaSaida = new ArrayList<ConsultarLibPagtosIndSemConsSaldoSaidaDTO>();
    	
    	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
    	request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));    	
    	request.setNrOcorrencias(ILiberarPagtosIndSemConsultaSaldoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));	
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);    	
    	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
    	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
    	request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemssaPagamento()));    	
    	request.setCdParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdParticipante()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));    	
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
    	request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoDe()));    	
    	request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoAte()));   	
    	request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
    	request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));   	
    	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
    	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));     	   	
    	request.setCdBarraDocumento(PgitUtil.verificaStringNula(entradaDTO.getCdBarraDocumento()));
    	request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
    	request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
    	request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
    	request.setCdBancoBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
    	request.setCdAgenciaBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
    	request.setCdContaBeneficiario(PgitUtil.verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
    	request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
    	request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
    	request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoConta()));
    	request.setNrContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrContrato()));
    	request.setNrUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
    	request.setCdBeneficio(PgitUtil.verificaLongNulo(entradaDTO.getCdBeneficio()));
    	request.setCdEspecieBeneficioInss(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
    	request.setCdClassificacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdClassificacao()));
    	request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
    	request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
    	request.setCdControleCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
    	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
    	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
    	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
    	request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
    	request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
    	request.setNrSequenciaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
    	request.setCdIndiceSimulaPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
    	request.setCdOrigemRecebidoEfetivacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao())); 
    	request.setCdTituloPgtoRastreado(0);
    	request.setCdBancoSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
    	request.setCdAgencaSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
    	request.setCdContaSalarial(PgitUtil.verificaLongNulo(entradaDTO.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getCdIspbPagtoDestino(), 8));
		}
		request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getContaPagtoDestino(), 20));

    	response = getFactoryAdapter().getConsultarLibPagtosIndSemConsSaldoPDCAdapter().invokeProcess(request);
    	
    	SimpleDateFormat formato1 = new SimpleDateFormat("dd.MM.yyyy");
  	    SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
    	
  	    for(br.com.bradesco.web.pgit.service.data.pdc.consultarlibpagtosindsemconssaldo.response.Ocorrencias o : response.getOcorrencias()){
    		ConsultarLibPagtosIndSemConsSaldoSaidaDTO saidaDTO = new ConsultarLibPagtosIndSemConsSaldoSaidaDTO();
    		saidaDTO.setCodMensagem(response.getCodMensagem());
    		saidaDTO.setMensagem(response.getMensagem());
    		saidaDTO.setNumeroConsultas(response.getNumeroConsultas());	
    		saidaDTO.setNrCnpjCpf(o.getNrCnpjCpf());
    		saidaDTO.setNrFilialCnpjCpf(o.getNrFilialCnpjCpf());
    		saidaDTO.setNrDigitoCnpjCpf(o.getNrDigitoCnpjCpf());
    		saidaDTO.setCdPessoaJuridicaContrato(o.getCdPessoaJuridicaContrato());
    		saidaDTO.setDsRazaoSocial(o.getDsRazaoSocial());
    		saidaDTO.setCdEmpresa(o.getCdEmpresa());
    		saidaDTO.setDsEmpresa(o.getDsEmpresa());
    		saidaDTO.setCdTipoContratoNegocio(o.getCdTipoContratoNegocio());
    		saidaDTO.setNrContrato(o.getNrContrato());
    		saidaDTO.setCdProdutoServicoOperacao(o.getCdProdutoServicoOperacao());
    		saidaDTO.setDsResumoProdutoServico(o.getDsResumoProdutoServico());
    		saidaDTO.setCdProdutoServicoRelacionado(o.getCdProdutoServicoRelacionado());
    		saidaDTO.setDsOperacaoProdutoServico(o.getDsOperacaoProdutoServico());
    		saidaDTO.setCdControlePagamento(o.getCdControlePagamento());
    		saidaDTO.setDtCreditoPagamento(o.getDtCreditoPagamento());
    		saidaDTO.setVlEfetivoPagamento(o.getVlEfetivoPagamento());
    		saidaDTO.setCdInscricaoFavorecido(o.getCdInscricaoFavorecido() == 0L ? "" : String.valueOf(o.getCdInscricaoFavorecido()));
    		saidaDTO.setDsFavorecido(o.getDsFavorecido());
    		saidaDTO.setCdBancoDebito(o.getCdBancoDebito());
    		saidaDTO.setCdAgenciaDebito(o.getCdAgenciaDebito());
    		saidaDTO.setCdDigitoAgenciaDebito(o.getCdDigitoAgenciaDebito());
    		saidaDTO.setCdContaDebito(o.getCdContaDebito());
    		saidaDTO.setCdDigitoContaDebito(o.getCdDigitoContaDebito());
    		saidaDTO.setCdBancoCredito(o.getCdBancoCredito());
    		saidaDTO.setCdAgenciaCredito(o.getCdAgenciaCredito());
    		saidaDTO.setCdDigitoAgenciaCredito(o.getCdDigitoAgenciaCredito());
    		saidaDTO.setCdContaCredito(o.getCdContaCredito());
    		saidaDTO.setCdDigitoContaCredito(o.getCdDigitoContaCredito());
    		saidaDTO.setCdSituacaoOperacaoPagamento(o.getCdSituacaoOperacaoPagamento());
    		saidaDTO.setDsSituacaoOperacaoPagamento(o.getDsSituacaoOperacaoPagamento());
    		saidaDTO.setCdMotivoSituacaoPagamento(o.getCdMotivoSituacaoPagamento());
    		saidaDTO.setDsMotivoSituacaoPagamento(o.getDsMotivoSituacaoPagamento());
    		saidaDTO.setCdTipoCanal(o.getCdTipoCanal());
    		saidaDTO.setCdTipoTela(o.getCdTipoTela());
    		saidaDTO.setDsIndicadorPagamento(o.getDsIndicadorPagamento());
    	
    		saidaDTO.setDsEfetivacaoPagamento(o.getDsEfetivacaoPagamento());
    		
    		
    		saidaDTO.setFavBeneficiarioFormatado(saidaDTO.getDsFavorecido());
    		
    		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(o.getCdBancoCredito(), false));
    		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(o.getCdAgenciaCredito(), o.getCdDigitoAgenciaCredito(), false));
    		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(o.getCdContaCredito(), o.getCdDigitoContaCredito(), false));

    		saidaDTO.setCpfFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(o.getNrCnpjCpf(), o.getNrFilialCnpjCpf(), o.getNrDigitoCnpjCpf()));
    		saidaDTO.setCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpjCompleto(o.getNrCnpjCpf(), o.getNrFilialCnpjCpf(), o.getNrDigitoCnpjCpf()));
    		saidaDTO.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(o.getCdBancoDebito(), o.getCdAgenciaDebito(), o.getCdDigitoAgenciaDebito(), + o.getCdContaDebito(), o.getCdDigitoContaDebito(), true));
    		saidaDTO.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(o.getCdBancoCredito(), o.getCdAgenciaCredito(), o.getCdDigitoAgenciaCredito(), + o.getCdContaCredito(), o.getCdDigitoContaCredito(), true));
    		saidaDTO.setContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(o.getCdBancoDebito(), o.getCdAgenciaDebito(), o.getCdDigitoAgenciaDebito(), + o.getCdContaDebito(), o.getCdDigitoContaDebito(), true));
    		saidaDTO.setContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(o.getCdBancoCredito(), o.getCdAgenciaCredito(), o.getCdDigitoAgenciaCredito(), + o.getCdContaCredito(), o.getCdDigitoContaCredito(), true));    		
            try
            {
            	saidaDTO.setDtFormatada(formato2.format(formato1.parse(o.getDtCreditoPagamento())));
            }catch (IndexOutOfBoundsException e) {

			} catch (java.text.ParseException e) {
				saidaDTO.setDtFormatada("");
			}
            
			saidaDTO.setCdIspbPagtoDestino(o.getCdIspbPagtoDestino());
			saidaDTO.setContaPagtoDestino(o.getContaPagtoDestino());
				
    		listaSaida.add(saidaDTO);
    	}
		
		return listaSaida;
	}    


	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.ILiberarPagtosIndSemConsultaSaldoService#liberarPagtosIndSemConsultaSaldo(java.util.List)
	 */
	public LiberarPagtosIndSemConsultaSaldoSaidaDTO liberarPagtosIndSemConsultaSaldo (List<LiberarPagtosIndSemConsultaSaldoEntradaDTO> listaEntradaDTO){
	 	 
		LiberarPagtosIndSemConsultaSaldoRequest request = new LiberarPagtosIndSemConsultaSaldoRequest();
		LiberarPagtosIndSemConsultaSaldoResponse response = new LiberarPagtosIndSemConsultaSaldoResponse();
		LiberarPagtosIndSemConsultaSaldoSaidaDTO saidaDTO = new LiberarPagtosIndSemConsultaSaldoSaidaDTO();
	   	   
	  
		
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpagtosindsemconsultasaldo.request.Ocorrencias> ocorrencias = new 
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.liberarpagtosindsemconsultasaldo.request.Ocorrencias>();
		
		for(int i=0; i<listaEntradaDTO.size(); i++){
			br.com.bradesco.web.pgit.service.data.pdc.liberarpagtosindsemconsultasaldo.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.liberarpagtosindsemconsultasaldo.request.Ocorrencias();
			ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getCdPessoaJuridicaContrato()));
			ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoContratoNegocio()));
			ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getNrSequenciaContratoNegocio()));
			ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoCanal()));
			ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdProdutoServicoOperacao()));
			ocorrencia.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdOperacaoProdutoServico()));
			ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntradaDTO.get(i).getCdControlePagamento()));
			ocorrencias.add(ocorrencia);			
		}
		request.setNumeroConsultas(listaEntradaDTO.size());
		request.setOcorrencias(ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.liberarpagtosindsemconsultasaldo.request.Ocorrencias[0]));
		
		response = getFactoryAdapter().getLiberarPagtosIndSemConsultaSaldoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());		
	   
	   return saidaDTO;
	  
	}
}