/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias1.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias1 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoServicoModalidade
     */
    private int _cdTipoServicoModalidade = 0;

    /**
     * keeps track of state for field: _cdTipoServicoModalidade
     */
    private boolean _has_cdTipoServicoModalidade;

    /**
     * Field _cdModServico
     */
    private int _cdModServico = 0;

    /**
     * keeps track of state for field: _cdModServico
     */
    private boolean _has_cdModServico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias1() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModServico
     * 
     */
    public void deleteCdModServico()
    {
        this._has_cdModServico= false;
    } //-- void deleteCdModServico() 

    /**
     * Method deleteCdTipoServicoModalidade
     * 
     */
    public void deleteCdTipoServicoModalidade()
    {
        this._has_cdTipoServicoModalidade= false;
    } //-- void deleteCdTipoServicoModalidade() 

    /**
     * Returns the value of field 'cdModServico'.
     * 
     * @return int
     * @return the value of field 'cdModServico'.
     */
    public int getCdModServico()
    {
        return this._cdModServico;
    } //-- int getCdModServico() 

    /**
     * Returns the value of field 'cdTipoServicoModalidade'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoModalidade'.
     */
    public int getCdTipoServicoModalidade()
    {
        return this._cdTipoServicoModalidade;
    } //-- int getCdTipoServicoModalidade() 

    /**
     * Method hasCdModServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModServico()
    {
        return this._has_cdModServico;
    } //-- boolean hasCdModServico() 

    /**
     * Method hasCdTipoServicoModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoModalidade()
    {
        return this._has_cdTipoServicoModalidade;
    } //-- boolean hasCdTipoServicoModalidade() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModServico'.
     * 
     * @param cdModServico the value of field 'cdModServico'.
     */
    public void setCdModServico(int cdModServico)
    {
        this._cdModServico = cdModServico;
        this._has_cdModServico = true;
    } //-- void setCdModServico(int) 

    /**
     * Sets the value of field 'cdTipoServicoModalidade'.
     * 
     * @param cdTipoServicoModalidade the value of field
     * 'cdTipoServicoModalidade'.
     */
    public void setCdTipoServicoModalidade(int cdTipoServicoModalidade)
    {
        this._cdTipoServicoModalidade = cdTipoServicoModalidade;
        this._has_cdTipoServicoModalidade = true;
    } //-- void setCdTipoServicoModalidade(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias1
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirtiposervmodcontrato.request.Ocorrencias1 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
