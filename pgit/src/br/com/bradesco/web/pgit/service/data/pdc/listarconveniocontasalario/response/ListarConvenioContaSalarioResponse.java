/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.math.BigDecimal;
import java.util.Vector;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ListarConvenioContaSalarioResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ListarConvenioContaSalarioResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
	 * 
	 */
	private static final long serialVersionUID = -2284002564310889415L;

	/**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdFaiMaxOcorr
     */
    private int _cdFaiMaxOcorr = 0;

    /**
     * keeps track of state for field: _cdFaiMaxOcorr
     */
    private boolean _has_cdFaiMaxOcorr;

    /**
     * Field _cdTurMaxOcorr
     */
    private int _cdTurMaxOcorr = 0;

    /**
     * keeps track of state for field: _cdTurMaxOcorr
     */
    private boolean _has_cdTurMaxOcorr;

    /**
     * Field _cdFilMaxOcorr
     */
    private int _cdFilMaxOcorr = 0;

    /**
     * keeps track of state for field: _cdFilMaxOcorr
     */
    private boolean _has_cdFilMaxOcorr;

    /**
     * Field _cdConMaxOcorr
     */
    private int _cdConMaxOcorr = 0;

    /**
     * keeps track of state for field: _cdConMaxOcorr
     */
    private boolean _has_cdConMaxOcorr;

    /**
     * Field _cdFolMaxOcorr
     */
    private int _cdFolMaxOcorr = 0;

    /**
     * keeps track of state for field: _cdFolMaxOcorr
     */
    private boolean _has_cdFolMaxOcorr;

    /**
     * Field _cdCocMaxOcorr
     */
    private int _cdCocMaxOcorr = 0;

    /**
     * keeps track of state for field: _cdCocMaxOcorr
     */
    private boolean _has_cdCocMaxOcorr;

    /**
     * Field _cdPssoaJuridCta
     */
    private long _cdPssoaJuridCta = 0;

    /**
     * keeps track of state for field: _cdPssoaJuridCta
     */
    private boolean _has_cdPssoaJuridCta;

    /**
     * Field _cdTipoContrCta
     */
    private int _cdTipoContrCta = 0;

    /**
     * keeps track of state for field: _cdTipoContrCta
     */
    private boolean _has_cdTipoContrCta;

    /**
     * Field _numSequenciaContratoCta
     */
    private long _numSequenciaContratoCta = 0;

    /**
     * keeps track of state for field: _numSequenciaContratoCta
     */
    private boolean _has_numSequenciaContratoCta;

    /**
     * Field _cdInstituicaoBancaria
     */
    private int _cdInstituicaoBancaria = 0;

    /**
     * keeps track of state for field: _cdInstituicaoBancaria
     */
    private boolean _has_cdInstituicaoBancaria;

    /**
     * Field _cdUnidadeOrganizacionalCusto
     */
    private int _cdUnidadeOrganizacionalCusto = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacionalCusto
     */
    private boolean _has_cdUnidadeOrganizacionalCusto;

    /**
     * Field _numConta
     */
    private int _numConta = 0;

    /**
     * keeps track of state for field: _numConta
     */
    private boolean _has_numConta;

    /**
     * Field _numContaDigito
     */
    private int _numContaDigito = 0;

    /**
     * keeps track of state for field: _numContaDigito
     */
    private boolean _has_numContaDigito;

    /**
     * Field _cdIndicadorNumConve
     */
    private int _cdIndicadorNumConve = 0;

    /**
     * keeps track of state for field: _cdIndicadorNumConve
     */
    private boolean _has_cdIndicadorNumConve;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _vlPagamentoMes
     */
    private java.math.BigDecimal _vlPagamentoMes = new java.math.BigDecimal("0");

    /**
     * Field _qtdFuncionarioFlPgto
     */
    private int _qtdFuncionarioFlPgto = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFlPgto
     */
    private boolean _has_qtdFuncionarioFlPgto;

    /**
     * Field _vlMediaSalarialMes
     */
    private java.math.BigDecimal _vlMediaSalarialMes = new java.math.BigDecimal("0");

    /**
     * Field _numDiaCredtPrim
     */
    private int _numDiaCredtPrim = 0;

    /**
     * keeps track of state for field: _numDiaCredtPrim
     */
    private boolean _has_numDiaCredtPrim;

    /**
     * Field _numDiaCredtSeg
     */
    private int _numDiaCredtSeg = 0;

    /**
     * keeps track of state for field: _numDiaCredtSeg
     */
    private boolean _has_numDiaCredtSeg;

    /**
     * Field _cdIndicadorQuestSalarial
     */
    private int _cdIndicadorQuestSalarial = 0;

    /**
     * keeps track of state for field: _cdIndicadorQuestSalarial
     */
    private boolean _has_cdIndicadorQuestSalarial;

    /**
     * Field _cdPagamentoSalarialMes
     */
    private int _cdPagamentoSalarialMes = 0;

    /**
     * keeps track of state for field: _cdPagamentoSalarialMes
     */
    private boolean _has_cdPagamentoSalarialMes;

    /**
     * Field _cdPagamentoSalarialQzena
     */
    private int _cdPagamentoSalarialQzena = 0;

    /**
     * keeps track of state for field: _cdPagamentoSalarialQzena
     */
    private boolean _has_cdPagamentoSalarialQzena;

    /**
     * Field _cdLocCtaSalarial
     */
    private int _cdLocCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdLocCtaSalarial
     */
    private boolean _has_cdLocCtaSalarial;

    /**
     * Field _dsLocCtaSalarial
     */
    private java.lang.String _dsLocCtaSalarial;

    /**
     * Field _dataReftFlPgto
     */
    private int _dataReftFlPgto = 0;

    /**
     * keeps track of state for field: _dataReftFlPgto
     */
    private boolean _has_dataReftFlPgto;

    /**
     * Field _codIndicadorFedelize
     */
    private java.lang.String _codIndicadorFedelize;

    /**
     * Field _dsEmailEmpresa
     */
    private java.lang.String _dsEmailEmpresa;

    /**
     * Field _dsEmailAgencia
     */
    private java.lang.String _dsEmailAgencia;

    /**
     * Field _cdIndicadorAltoTurnover
     */
    private int _cdIndicadorAltoTurnover = 0;

    /**
     * keeps track of state for field: _cdIndicadorAltoTurnover
     */
    private boolean _has_cdIndicadorAltoTurnover;

    /**
     * Field _cdIndicadorOfertaCartaoPrePago
     */
    private int _cdIndicadorOfertaCartaoPrePago = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorOfertaCartaoPrePago
     */
    private boolean _has_cdIndicadorOfertaCartaoPrePago;

    /**
     * Field _cdIndicadorAtivacaoAPP
     */
    private int _cdIndicadorAtivacaoAPP = 0;

    /**
     * keeps track of state for field: _cdIndicadorAtivacaoAPP
     */
    private boolean _has_cdIndicadorAtivacaoAPP;

    /**
     * Field _dsTextoJustfConc
     */
    private java.lang.String _dsTextoJustfConc;

    /**
     * Field _dsTextoJustfRecip
     */
    private java.lang.String _dsTextoJustfRecip;

    /**
     * Field _dsTextoJustfCompl
     */
    private java.lang.String _dsTextoJustfCompl;

    /**
     * Field _cdIndicadorGrpQuest
     */
    private int _cdIndicadorGrpQuest = 0;

    /**
     * keeps track of state for field: _cdIndicadorGrpQuest
     */
    private boolean _has_cdIndicadorGrpQuest;

    /**
     * Field _nroGrupoEconmQuest
     */
    private long _nroGrupoEconmQuest = 0;

    /**
     * keeps track of state for field: _nroGrupoEconmQuest
     */
    private boolean _has_nroGrupoEconmQuest;

    /**
     * Field _dsGrupoEconmQuest
     */
    private java.lang.String _dsGrupoEconmQuest;

    /**
     * Field _cdUnidadeMeddAg
     */
    private int _cdUnidadeMeddAg = 0;

    /**
     * keeps track of state for field: _cdUnidadeMeddAg
     */
    private boolean _has_cdUnidadeMeddAg;

    /**
     * Field _dsAbrevMeddAg
     */
    private java.lang.String _dsAbrevMeddAg;

    /**
     * Field _cdUdistcAgEmpr
     */
    private int _cdUdistcAgEmpr = 0;

    /**
     * keeps track of state for field: _cdUdistcAgEmpr
     */
    private boolean _has_cdUdistcAgEmpr;

    /**
     * Field _dsLogradouroEmprQuest
     */
    private java.lang.String _dsLogradouroEmprQuest;

    /**
     * Field _dsBairroEmprQuest
     */
    private java.lang.String _dsBairroEmprQuest;

    /**
     * Field _cdMunEmprQuest
     */
    private long _cdMunEmprQuest = 0;

    /**
     * keeps track of state for field: _cdMunEmprQuest
     */
    private boolean _has_cdMunEmprQuest;

    /**
     * Field _dsMuncEmpreQuest
     */
    private java.lang.String _dsMuncEmpreQuest;

    /**
     * Field _cdCepEmprQuest
     */
    private int _cdCepEmprQuest = 0;

    /**
     * keeps track of state for field: _cdCepEmprQuest
     */
    private boolean _has_cdCepEmprQuest;

    /**
     * Field _cdCepComplemento
     */
    private int _cdCepComplemento = 0;

    /**
     * keeps track of state for field: _cdCepComplemento
     */
    private boolean _has_cdCepComplemento;

    /**
     * Field _cdUfEmpreQuest
     */
    private int _cdUfEmpreQuest = 0;

    /**
     * keeps track of state for field: _cdUfEmpreQuest
     */
    private boolean _has_cdUfEmpreQuest;

    /**
     * Field _dsUfEmprQuest
     */
    private java.lang.String _dsUfEmprQuest;

    /**
     * Field _cdAgptoAtividadeQuest
     */
    private long _cdAgptoAtividadeQuest = 0;

    /**
     * keeps track of state for field: _cdAgptoAtividadeQuest
     */
    private boolean _has_cdAgptoAtividadeQuest;

    /**
     * Field _dsRamoAtividadeQuest
     */
    private java.lang.String _dsRamoAtividadeQuest;

    /**
     * Field _cdDiscagemDiretaInterEmpr
     */
    private int _cdDiscagemDiretaInterEmpr = 0;

    /**
     * keeps track of state for field: _cdDiscagemDiretaInterEmpr
     */
    private boolean _has_cdDiscagemDiretaInterEmpr;

    /**
     * Field _cdDiscagemDiretaDistEmpr
     */
    private int _cdDiscagemDiretaDistEmpr = 0;

    /**
     * keeps track of state for field: _cdDiscagemDiretaDistEmpr
     */
    private boolean _has_cdDiscagemDiretaDistEmpr;

    /**
     * Field _numFoneEmprQuest
     */
    private long _numFoneEmprQuest = 0;

    /**
     * keeps track of state for field: _numFoneEmprQuest
     */
    private boolean _has_numFoneEmprQuest;

    /**
     * Field _dsSiteEmprQuest
     */
    private java.lang.String _dsSiteEmprQuest;

    /**
     * Field _cdFuncGerRelacionamento
     */
    private long _cdFuncGerRelacionamento = 0;

    /**
     * keeps track of state for field: _cdFuncGerRelacionamento
     */
    private boolean _has_cdFuncGerRelacionamento;

    /**
     * Field _dsGerRelacionamentoQuest
     */
    private java.lang.String _dsGerRelacionamentoQuest;

    /**
     * Field _cdDiscagemDiretaDistGer
     */
    private int _cdDiscagemDiretaDistGer = 0;

    /**
     * keeps track of state for field: _cdDiscagemDiretaDistGer
     */
    private boolean _has_cdDiscagemDiretaDistGer;

    /**
     * Field _cdDiscagemDiretaDistanGer
     */
    private int _cdDiscagemDiretaDistanGer = 0;

    /**
     * keeps track of state for field: _cdDiscagemDiretaDistanGer
     */
    private boolean _has_cdDiscagemDiretaDistanGer;

    /**
     * Field _numFoneGerQuest
     */
    private long _numFoneGerQuest = 0;

    /**
     * keeps track of state for field: _numFoneGerQuest
     */
    private boolean _has_numFoneGerQuest;

    /**
     * Field _cdSegmentoEmprQuest
     */
    private int _cdSegmentoEmprQuest = 0;

    /**
     * keeps track of state for field: _cdSegmentoEmprQuest
     */
    private boolean _has_cdSegmentoEmprQuest;

    /**
     * Field _dsAbrevSegmentoEmpr
     */
    private java.lang.String _dsAbrevSegmentoEmpr;

    /**
     * Field _cdIndicadorFlQuest
     */
    private int _cdIndicadorFlQuest = 0;

    /**
     * keeps track of state for field: _cdIndicadorFlQuest
     */
    private boolean _has_cdIndicadorFlQuest;

    /**
     * Field _dsIndicadorFlQuest
     */
    private java.lang.String _dsIndicadorFlQuest;

    /**
     * Field _pctgmPreNovAnoQuest
     */
    private java.math.BigDecimal _pctgmPreNovAnoQuest = new java.math.BigDecimal("0");

    /**
     * Field _cdprefeCsign
     */
    private int _cdprefeCsign = 0;

    /**
     * keeps track of state for field: _cdprefeCsign
     */
    private boolean _has_cdprefeCsign;

    /**
     * Field _cdprefePabCren
     */
    private int _cdprefePabCren = 0;

    /**
     * keeps track of state for field: _cdprefePabCren
     */
    private boolean _has_cdprefePabCren;

    /**
     * Field _cdIndicadorPaeCcren
     */
    private int _cdIndicadorPaeCcren = 0;

    /**
     * keeps track of state for field: _cdIndicadorPaeCcren
     */
    private boolean _has_cdIndicadorPaeCcren;

    /**
     * Field _cdIndicadorAgCcren
     */
    private int _cdIndicadorAgCcren = 0;

    /**
     * keeps track of state for field: _cdIndicadorAgCcren
     */
    private boolean _has_cdIndicadorAgCcren;

    /**
     * Field _cdIndicadorInstaEstrt
     */
    private int _cdIndicadorInstaEstrt = 0;

    /**
     * keeps track of state for field: _cdIndicadorInstaEstrt
     */
    private boolean _has_cdIndicadorInstaEstrt;

    /**
     * Field _cdEstrtInstaEmpr
     */
    private int _cdEstrtInstaEmpr = 0;

    /**
     * keeps track of state for field: _cdEstrtInstaEmpr
     */
    private boolean _has_cdEstrtInstaEmpr;

    /**
     * Field _dsEstruturaInstaEmpr
     */
    private java.lang.String _dsEstruturaInstaEmpr;

    /**
     * Field _cdbancoVincInsta
     */
    private int _cdbancoVincInsta = 0;

    /**
     * keeps track of state for field: _cdbancoVincInsta
     */
    private boolean _has_cdbancoVincInsta;

    /**
     * Field _cdAgenciaVincInsta
     */
    private int _cdAgenciaVincInsta = 0;

    /**
     * keeps track of state for field: _cdAgenciaVincInsta
     */
    private boolean _has_cdAgenciaVincInsta;

    /**
     * Field _dsAgenciaVincInsta
     */
    private java.lang.String _dsAgenciaVincInsta;

    /**
     * Field _cdUnidadeMeddInsta
     */
    private int _cdUnidadeMeddInsta = 0;

    /**
     * keeps track of state for field: _cdUnidadeMeddInsta
     */
    private boolean _has_cdUnidadeMeddInsta;

    /**
     * Field _dsAbrevInsta
     */
    private java.lang.String _dsAbrevInsta;

    /**
     * Field _cdDistcAgenciaInsta
     */
    private int _cdDistcAgenciaInsta = 0;

    /**
     * keeps track of state for field: _cdDistcAgenciaInsta
     */
    private boolean _has_cdDistcAgenciaInsta;

    /**
     * Field _cdIndicadorVisitaInsta
     */
    private int _cdIndicadorVisitaInsta = 0;

    /**
     * keeps track of state for field: _cdIndicadorVisitaInsta
     */
    private boolean _has_cdIndicadorVisitaInsta;

    /**
     * Field _dsIndicadorVisitaInsta
     */
    private java.lang.String _dsIndicadorVisitaInsta;

    /**
     * Field _cdFuncGerVisitante
     */
    private long _cdFuncGerVisitante = 0;

    /**
     * keeps track of state for field: _cdFuncGerVisitante
     */
    private boolean _has_cdFuncGerVisitante;

    /**
     * Field _dsGerVisitanteInsta
     */
    private java.lang.String _dsGerVisitanteInsta;

    /**
     * Field _cdUnidadeMeddEspac
     */
    private int _cdUnidadeMeddEspac = 0;

    /**
     * keeps track of state for field: _cdUnidadeMeddEspac
     */
    private boolean _has_cdUnidadeMeddEspac;

    /**
     * Field _dsAbrevEspacConcedido
     */
    private java.lang.String _dsAbrevEspacConcedido;

    /**
     * Field _cdUespacConcedidoInsta
     */
    private int _cdUespacConcedidoInsta = 0;

    /**
     * keeps track of state for field: _cdUespacConcedidoInsta
     */
    private boolean _has_cdUespacConcedidoInsta;

    /**
     * Field _cdTipoNecesInsta
     */
    private int _cdTipoNecesInsta = 0;

    /**
     * keeps track of state for field: _cdTipoNecesInsta
     */
    private boolean _has_cdTipoNecesInsta;

    /**
     * Field _dsTipoNecessInsta
     */
    private java.lang.String _dsTipoNecessInsta;

    /**
     * Field _cdIndicadorPerdaRelacionamento
     */
    private int _cdIndicadorPerdaRelacionamento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorPerdaRelacionamento
     */
    private boolean _has_cdIndicadorPerdaRelacionamento;

    /**
     * Field _dsIndicadorPerdarelacionamento
     */
    private java.lang.String _dsIndicadorPerdarelacionamento;

    /**
     * Field _cdPagamentoSalCredt
     */
    private int _cdPagamentoSalCredt = 0;

    /**
     * keeps track of state for field: _cdPagamentoSalCredt
     */
    private boolean _has_cdPagamentoSalCredt;

    /**
     * Field _cdPagamentoSalCheque
     */
    private int _cdPagamentoSalCheque = 0;

    /**
     * keeps track of state for field: _cdPagamentoSalCheque
     */
    private boolean _has_cdPagamentoSalCheque;

    /**
     * Field _cdPagamentoSalDinheiro
     */
    private int _cdPagamentoSalDinheiro = 0;

    /**
     * keeps track of state for field: _cdPagamentoSalDinheiro
     */
    private boolean _has_cdPagamentoSalDinheiro;

    /**
     * Field _cdPagamentoSalDivers
     */
    private int _cdPagamentoSalDivers = 0;

    /**
     * keeps track of state for field: _cdPagamentoSalDivers
     */
    private boolean _has_cdPagamentoSalDivers;

    /**
     * Field _dsPagamentoSalDivers
     */
    private java.lang.String _dsPagamentoSalDivers;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;

    /**
     * Field _ocorrencias2List
     */
    private java.util.Vector _ocorrencias2List;

    /**
     * Field _ocorrencias3List
     */
    private java.util.Vector _ocorrencias3List;

    /**
     * Field _ocorrencias4List
     */
    private java.util.Vector _ocorrencias4List;

    /**
     * Field _ocorrencias5List
     */
    private java.util.Vector _ocorrencias5List;

    /**
     * Field _ocorrencias6List
     */
    private java.util.Vector _ocorrencias6List;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarConvenioContaSalarioResponse() 
     {
        super();
        setVlPagamentoMes(new java.math.BigDecimal("0"));
        setVlMediaSalarialMes(new java.math.BigDecimal("0"));
        setPctgmPreNovAnoQuest(new java.math.BigDecimal("0"));
        _ocorrenciasList = new Vector();
        _ocorrencias2List = new Vector();
        _ocorrencias3List = new Vector();
        _ocorrencias4List = new Vector();
        _ocorrencias5List = new Vector();
        _ocorrencias6List = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 15)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 15");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 15)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 15");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param vOcorrencias2
     */
    public void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias2List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias2 has a maximum of 10");
        }
        _ocorrencias2List.addElement(vOcorrencias2);
    } //-- void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void addOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias2List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias2 has a maximum of 10");
        }
        _ocorrencias2List.insertElementAt(vOcorrencias2, index);
    } //-- void addOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2) 

    /**
     * Method addOcorrencias3
     * 
     * 
     * 
     * @param vOcorrencias3
     */
    public void addOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias3List.size() < 15)) {
            throw new IndexOutOfBoundsException("addOcorrencias3 has a maximum of 15");
        }
        _ocorrencias3List.addElement(vOcorrencias3);
    } //-- void addOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3) 

    /**
     * Method addOcorrencias3
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias3
     */
    public void addOcorrencias3(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias3List.size() < 15)) {
            throw new IndexOutOfBoundsException("addOcorrencias3 has a maximum of 15");
        }
        _ocorrencias3List.insertElementAt(vOcorrencias3, index);
    } //-- void addOcorrencias3(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3) 

    /**
     * Method addOcorrencias4
     * 
     * 
     * 
     * @param vOcorrencias4
     */
    public void addOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias4List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias4 has a maximum of 10");
        }
        _ocorrencias4List.addElement(vOcorrencias4);
    } //-- void addOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4) 

    /**
     * Method addOcorrencias4
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias4
     */
    public void addOcorrencias4(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias4List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias4 has a maximum of 10");
        }
        _ocorrencias4List.insertElementAt(vOcorrencias4, index);
    } //-- void addOcorrencias4(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4) 

    /**
     * Method addOcorrencias5
     * 
     * 
     * 
     * @param vOcorrencias5
     */
    public void addOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias5List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias5 has a maximum of 10");
        }
        _ocorrencias5List.addElement(vOcorrencias5);
    } //-- void addOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5) 

    /**
     * Method addOcorrencias5
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias5
     */
    public void addOcorrencias5(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias5List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias5 has a maximum of 10");
        }
        _ocorrencias5List.insertElementAt(vOcorrencias5, index);
    } //-- void addOcorrencias5(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5) 

    /**
     * Method addOcorrencias6
     * 
     * 
     * 
     * @param vOcorrencias6
     */
    public void addOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias6List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias6 has a maximum of 10");
        }
        _ocorrencias6List.addElement(vOcorrencias6);
    } //-- void addOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6) 

    /**
     * Method addOcorrencias6
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias6
     */
    public void addOcorrencias6(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrencias6List.size() < 10)) {
            throw new IndexOutOfBoundsException("addOcorrencias6 has a maximum of 10");
        }
        _ocorrencias6List.insertElementAt(vOcorrencias6, index);
    } //-- void addOcorrencias6(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6) 

    /**
     * Method deleteCdAgenciaVincInsta
     * 
     */
    public void deleteCdAgenciaVincInsta()
    {
        this._has_cdAgenciaVincInsta= false;
    } //-- void deleteCdAgenciaVincInsta() 

    /**
     * Method deleteCdAgptoAtividadeQuest
     * 
     */
    public void deleteCdAgptoAtividadeQuest()
    {
        this._has_cdAgptoAtividadeQuest= false;
    } //-- void deleteCdAgptoAtividadeQuest() 

    /**
     * Method deleteCdCepComplemento
     * 
     */
    public void deleteCdCepComplemento()
    {
        this._has_cdCepComplemento= false;
    } //-- void deleteCdCepComplemento() 

    /**
     * Method deleteCdCepEmprQuest
     * 
     */
    public void deleteCdCepEmprQuest()
    {
        this._has_cdCepEmprQuest= false;
    } //-- void deleteCdCepEmprQuest() 

    /**
     * Method deleteCdCocMaxOcorr
     * 
     */
    public void deleteCdCocMaxOcorr()
    {
        this._has_cdCocMaxOcorr= false;
    } //-- void deleteCdCocMaxOcorr() 

    /**
     * Method deleteCdConMaxOcorr
     * 
     */
    public void deleteCdConMaxOcorr()
    {
        this._has_cdConMaxOcorr= false;
    } //-- void deleteCdConMaxOcorr() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdDiscagemDiretaDistEmpr
     * 
     */
    public void deleteCdDiscagemDiretaDistEmpr()
    {
        this._has_cdDiscagemDiretaDistEmpr= false;
    } //-- void deleteCdDiscagemDiretaDistEmpr() 

    /**
     * Method deleteCdDiscagemDiretaDistGer
     * 
     */
    public void deleteCdDiscagemDiretaDistGer()
    {
        this._has_cdDiscagemDiretaDistGer= false;
    } //-- void deleteCdDiscagemDiretaDistGer() 

    /**
     * Method deleteCdDiscagemDiretaDistanGer
     * 
     */
    public void deleteCdDiscagemDiretaDistanGer()
    {
        this._has_cdDiscagemDiretaDistanGer= false;
    } //-- void deleteCdDiscagemDiretaDistanGer() 

    /**
     * Method deleteCdDiscagemDiretaInterEmpr
     * 
     */
    public void deleteCdDiscagemDiretaInterEmpr()
    {
        this._has_cdDiscagemDiretaInterEmpr= false;
    } //-- void deleteCdDiscagemDiretaInterEmpr() 

    /**
     * Method deleteCdDistcAgenciaInsta
     * 
     */
    public void deleteCdDistcAgenciaInsta()
    {
        this._has_cdDistcAgenciaInsta= false;
    } //-- void deleteCdDistcAgenciaInsta() 

    /**
     * Method deleteCdEstrtInstaEmpr
     * 
     */
    public void deleteCdEstrtInstaEmpr()
    {
        this._has_cdEstrtInstaEmpr= false;
    } //-- void deleteCdEstrtInstaEmpr() 

    /**
     * Method deleteCdFaiMaxOcorr
     * 
     */
    public void deleteCdFaiMaxOcorr()
    {
        this._has_cdFaiMaxOcorr= false;
    } //-- void deleteCdFaiMaxOcorr() 

    /**
     * Method deleteCdFilMaxOcorr
     * 
     */
    public void deleteCdFilMaxOcorr()
    {
        this._has_cdFilMaxOcorr= false;
    } //-- void deleteCdFilMaxOcorr() 

    /**
     * Method deleteCdFolMaxOcorr
     * 
     */
    public void deleteCdFolMaxOcorr()
    {
        this._has_cdFolMaxOcorr= false;
    } //-- void deleteCdFolMaxOcorr() 

    /**
     * Method deleteCdFuncGerRelacionamento
     * 
     */
    public void deleteCdFuncGerRelacionamento()
    {
        this._has_cdFuncGerRelacionamento= false;
    } //-- void deleteCdFuncGerRelacionamento() 

    /**
     * Method deleteCdFuncGerVisitante
     * 
     */
    public void deleteCdFuncGerVisitante()
    {
        this._has_cdFuncGerVisitante= false;
    } //-- void deleteCdFuncGerVisitante() 

    /**
     * Method deleteCdIndicadorAgCcren
     * 
     */
    public void deleteCdIndicadorAgCcren()
    {
        this._has_cdIndicadorAgCcren= false;
    } //-- void deleteCdIndicadorAgCcren() 

    /**
     * Method deleteCdIndicadorAltoTurnover
     * 
     */
    public void deleteCdIndicadorAltoTurnover()
    {
        this._has_cdIndicadorAltoTurnover= false;
    } //-- void deleteCdIndicadorAltoTurnover() 

    /**
     * Method deleteCdIndicadorAtivacaoAPP
     * 
     */
    public void deleteCdIndicadorAtivacaoAPP()
    {
        this._has_cdIndicadorAtivacaoAPP= false;
    } //-- void deleteCdIndicadorAtivacaoAPP() 

    /**
     * Method deleteCdIndicadorFlQuest
     * 
     */
    public void deleteCdIndicadorFlQuest()
    {
        this._has_cdIndicadorFlQuest= false;
    } //-- void deleteCdIndicadorFlQuest() 

    /**
     * Method deleteCdIndicadorGrpQuest
     * 
     */
    public void deleteCdIndicadorGrpQuest()
    {
        this._has_cdIndicadorGrpQuest= false;
    } //-- void deleteCdIndicadorGrpQuest() 

    /**
     * Method deleteCdIndicadorInstaEstrt
     * 
     */
    public void deleteCdIndicadorInstaEstrt()
    {
        this._has_cdIndicadorInstaEstrt= false;
    } //-- void deleteCdIndicadorInstaEstrt() 

    /**
     * Method deleteCdIndicadorNumConve
     * 
     */
    public void deleteCdIndicadorNumConve()
    {
        this._has_cdIndicadorNumConve= false;
    } //-- void deleteCdIndicadorNumConve() 

    /**
     * Method deleteCdIndicadorOfertaCartaoPrePago
     * 
     */
    public void deleteCdIndicadorOfertaCartaoPrePago()
    {
        this._has_cdIndicadorOfertaCartaoPrePago= false;
    } //-- void deleteCdIndicadorOfertaCartaoPrePago() 

    /**
     * Method deleteCdIndicadorPaeCcren
     * 
     */
    public void deleteCdIndicadorPaeCcren()
    {
        this._has_cdIndicadorPaeCcren= false;
    } //-- void deleteCdIndicadorPaeCcren() 

    /**
     * Method deleteCdIndicadorPerdaRelacionamento
     * 
     */
    public void deleteCdIndicadorPerdaRelacionamento()
    {
        this._has_cdIndicadorPerdaRelacionamento= false;
    } //-- void deleteCdIndicadorPerdaRelacionamento() 

    /**
     * Method deleteCdIndicadorQuestSalarial
     * 
     */
    public void deleteCdIndicadorQuestSalarial()
    {
        this._has_cdIndicadorQuestSalarial= false;
    } //-- void deleteCdIndicadorQuestSalarial() 

    /**
     * Method deleteCdIndicadorVisitaInsta
     * 
     */
    public void deleteCdIndicadorVisitaInsta()
    {
        this._has_cdIndicadorVisitaInsta= false;
    } //-- void deleteCdIndicadorVisitaInsta() 

    /**
     * Method deleteCdInstituicaoBancaria
     * 
     */
    public void deleteCdInstituicaoBancaria()
    {
        this._has_cdInstituicaoBancaria= false;
    } //-- void deleteCdInstituicaoBancaria() 

    /**
     * Method deleteCdLocCtaSalarial
     * 
     */
    public void deleteCdLocCtaSalarial()
    {
        this._has_cdLocCtaSalarial= false;
    } //-- void deleteCdLocCtaSalarial() 

    /**
     * Method deleteCdMunEmprQuest
     * 
     */
    public void deleteCdMunEmprQuest()
    {
        this._has_cdMunEmprQuest= false;
    } //-- void deleteCdMunEmprQuest() 

    /**
     * Method deleteCdPagamentoSalCheque
     * 
     */
    public void deleteCdPagamentoSalCheque()
    {
        this._has_cdPagamentoSalCheque= false;
    } //-- void deleteCdPagamentoSalCheque() 

    /**
     * Method deleteCdPagamentoSalCredt
     * 
     */
    public void deleteCdPagamentoSalCredt()
    {
        this._has_cdPagamentoSalCredt= false;
    } //-- void deleteCdPagamentoSalCredt() 

    /**
     * Method deleteCdPagamentoSalDinheiro
     * 
     */
    public void deleteCdPagamentoSalDinheiro()
    {
        this._has_cdPagamentoSalDinheiro= false;
    } //-- void deleteCdPagamentoSalDinheiro() 

    /**
     * Method deleteCdPagamentoSalDivers
     * 
     */
    public void deleteCdPagamentoSalDivers()
    {
        this._has_cdPagamentoSalDivers= false;
    } //-- void deleteCdPagamentoSalDivers() 

    /**
     * Method deleteCdPagamentoSalarialMes
     * 
     */
    public void deleteCdPagamentoSalarialMes()
    {
        this._has_cdPagamentoSalarialMes= false;
    } //-- void deleteCdPagamentoSalarialMes() 

    /**
     * Method deleteCdPagamentoSalarialQzena
     * 
     */
    public void deleteCdPagamentoSalarialQzena()
    {
        this._has_cdPagamentoSalarialQzena= false;
    } //-- void deleteCdPagamentoSalarialQzena() 

    /**
     * Method deleteCdPssoaJuridCta
     * 
     */
    public void deleteCdPssoaJuridCta()
    {
        this._has_cdPssoaJuridCta= false;
    } //-- void deleteCdPssoaJuridCta() 

    /**
     * Method deleteCdSegmentoEmprQuest
     * 
     */
    public void deleteCdSegmentoEmprQuest()
    {
        this._has_cdSegmentoEmprQuest= false;
    } //-- void deleteCdSegmentoEmprQuest() 

    /**
     * Method deleteCdTipoContrCta
     * 
     */
    public void deleteCdTipoContrCta()
    {
        this._has_cdTipoContrCta= false;
    } //-- void deleteCdTipoContrCta() 

    /**
     * Method deleteCdTipoNecesInsta
     * 
     */
    public void deleteCdTipoNecesInsta()
    {
        this._has_cdTipoNecesInsta= false;
    } //-- void deleteCdTipoNecesInsta() 

    /**
     * Method deleteCdTurMaxOcorr
     * 
     */
    public void deleteCdTurMaxOcorr()
    {
        this._has_cdTurMaxOcorr= false;
    } //-- void deleteCdTurMaxOcorr() 

    /**
     * Method deleteCdUdistcAgEmpr
     * 
     */
    public void deleteCdUdistcAgEmpr()
    {
        this._has_cdUdistcAgEmpr= false;
    } //-- void deleteCdUdistcAgEmpr() 

    /**
     * Method deleteCdUespacConcedidoInsta
     * 
     */
    public void deleteCdUespacConcedidoInsta()
    {
        this._has_cdUespacConcedidoInsta= false;
    } //-- void deleteCdUespacConcedidoInsta() 

    /**
     * Method deleteCdUfEmpreQuest
     * 
     */
    public void deleteCdUfEmpreQuest()
    {
        this._has_cdUfEmpreQuest= false;
    } //-- void deleteCdUfEmpreQuest() 

    /**
     * Method deleteCdUnidadeMeddAg
     * 
     */
    public void deleteCdUnidadeMeddAg()
    {
        this._has_cdUnidadeMeddAg= false;
    } //-- void deleteCdUnidadeMeddAg() 

    /**
     * Method deleteCdUnidadeMeddEspac
     * 
     */
    public void deleteCdUnidadeMeddEspac()
    {
        this._has_cdUnidadeMeddEspac= false;
    } //-- void deleteCdUnidadeMeddEspac() 

    /**
     * Method deleteCdUnidadeMeddInsta
     * 
     */
    public void deleteCdUnidadeMeddInsta()
    {
        this._has_cdUnidadeMeddInsta= false;
    } //-- void deleteCdUnidadeMeddInsta() 

    /**
     * Method deleteCdUnidadeOrganizacionalCusto
     * 
     */
    public void deleteCdUnidadeOrganizacionalCusto()
    {
        this._has_cdUnidadeOrganizacionalCusto= false;
    } //-- void deleteCdUnidadeOrganizacionalCusto() 

    /**
     * Method deleteCdbancoVincInsta
     * 
     */
    public void deleteCdbancoVincInsta()
    {
        this._has_cdbancoVincInsta= false;
    } //-- void deleteCdbancoVincInsta() 

    /**
     * Method deleteCdprefeCsign
     * 
     */
    public void deleteCdprefeCsign()
    {
        this._has_cdprefeCsign= false;
    } //-- void deleteCdprefeCsign() 

    /**
     * Method deleteCdprefePabCren
     * 
     */
    public void deleteCdprefePabCren()
    {
        this._has_cdprefePabCren= false;
    } //-- void deleteCdprefePabCren() 

    /**
     * Method deleteDataReftFlPgto
     * 
     */
    public void deleteDataReftFlPgto()
    {
        this._has_dataReftFlPgto= false;
    } //-- void deleteDataReftFlPgto() 

    /**
     * Method deleteNroGrupoEconmQuest
     * 
     */
    public void deleteNroGrupoEconmQuest()
    {
        this._has_nroGrupoEconmQuest= false;
    } //-- void deleteNroGrupoEconmQuest() 

    /**
     * Method deleteNumConta
     * 
     */
    public void deleteNumConta()
    {
        this._has_numConta= false;
    } //-- void deleteNumConta() 

    /**
     * Method deleteNumContaDigito
     * 
     */
    public void deleteNumContaDigito()
    {
        this._has_numContaDigito= false;
    } //-- void deleteNumContaDigito() 

    /**
     * Method deleteNumDiaCredtPrim
     * 
     */
    public void deleteNumDiaCredtPrim()
    {
        this._has_numDiaCredtPrim= false;
    } //-- void deleteNumDiaCredtPrim() 

    /**
     * Method deleteNumDiaCredtSeg
     * 
     */
    public void deleteNumDiaCredtSeg()
    {
        this._has_numDiaCredtSeg= false;
    } //-- void deleteNumDiaCredtSeg() 

    /**
     * Method deleteNumFoneEmprQuest
     * 
     */
    public void deleteNumFoneEmprQuest()
    {
        this._has_numFoneEmprQuest= false;
    } //-- void deleteNumFoneEmprQuest() 

    /**
     * Method deleteNumFoneGerQuest
     * 
     */
    public void deleteNumFoneGerQuest()
    {
        this._has_numFoneGerQuest= false;
    } //-- void deleteNumFoneGerQuest() 

    /**
     * Method deleteNumSequenciaContratoCta
     * 
     */
    public void deleteNumSequenciaContratoCta()
    {
        this._has_numSequenciaContratoCta= false;
    } //-- void deleteNumSequenciaContratoCta() 

    /**
     * Method deleteQtdFuncionarioFlPgto
     * 
     */
    public void deleteQtdFuncionarioFlPgto()
    {
        this._has_qtdFuncionarioFlPgto= false;
    } //-- void deleteQtdFuncionarioFlPgto() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Method enumerateOcorrencias2
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias2()
    {
        return _ocorrencias2List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias2() 

    /**
     * Method enumerateOcorrencias3
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias3()
    {
        return _ocorrencias3List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias3() 

    /**
     * Method enumerateOcorrencias4
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias4()
    {
        return _ocorrencias4List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias4() 

    /**
     * Method enumerateOcorrencias5
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias5()
    {
        return _ocorrencias5List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias5() 

    /**
     * Method enumerateOcorrencias6
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias6()
    {
        return _ocorrencias6List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias6() 

    /**
     * Returns the value of field 'cdAgenciaVincInsta'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaVincInsta'.
     */
    public int getCdAgenciaVincInsta()
    {
        return this._cdAgenciaVincInsta;
    } //-- int getCdAgenciaVincInsta() 

    /**
     * Returns the value of field 'cdAgptoAtividadeQuest'.
     * 
     * @return long
     * @return the value of field 'cdAgptoAtividadeQuest'.
     */
    public long getCdAgptoAtividadeQuest()
    {
        return this._cdAgptoAtividadeQuest;
    } //-- long getCdAgptoAtividadeQuest() 

    /**
     * Returns the value of field 'cdCepComplemento'.
     * 
     * @return int
     * @return the value of field 'cdCepComplemento'.
     */
    public int getCdCepComplemento()
    {
        return this._cdCepComplemento;
    } //-- int getCdCepComplemento() 

    /**
     * Returns the value of field 'cdCepEmprQuest'.
     * 
     * @return int
     * @return the value of field 'cdCepEmprQuest'.
     */
    public int getCdCepEmprQuest()
    {
        return this._cdCepEmprQuest;
    } //-- int getCdCepEmprQuest() 

    /**
     * Returns the value of field 'cdCocMaxOcorr'.
     * 
     * @return int
     * @return the value of field 'cdCocMaxOcorr'.
     */
    public int getCdCocMaxOcorr()
    {
        return this._cdCocMaxOcorr;
    } //-- int getCdCocMaxOcorr() 

    /**
     * Returns the value of field 'cdConMaxOcorr'.
     * 
     * @return int
     * @return the value of field 'cdConMaxOcorr'.
     */
    public int getCdConMaxOcorr()
    {
        return this._cdConMaxOcorr;
    } //-- int getCdConMaxOcorr() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdDiscagemDiretaDistEmpr'.
     * 
     * @return int
     * @return the value of field 'cdDiscagemDiretaDistEmpr'.
     */
    public int getCdDiscagemDiretaDistEmpr()
    {
        return this._cdDiscagemDiretaDistEmpr;
    } //-- int getCdDiscagemDiretaDistEmpr() 

    /**
     * Returns the value of field 'cdDiscagemDiretaDistGer'.
     * 
     * @return int
     * @return the value of field 'cdDiscagemDiretaDistGer'.
     */
    public int getCdDiscagemDiretaDistGer()
    {
        return this._cdDiscagemDiretaDistGer;
    } //-- int getCdDiscagemDiretaDistGer() 

    /**
     * Returns the value of field 'cdDiscagemDiretaDistanGer'.
     * 
     * @return int
     * @return the value of field 'cdDiscagemDiretaDistanGer'.
     */
    public int getCdDiscagemDiretaDistanGer()
    {
        return this._cdDiscagemDiretaDistanGer;
    } //-- int getCdDiscagemDiretaDistanGer() 

    /**
     * Returns the value of field 'cdDiscagemDiretaInterEmpr'.
     * 
     * @return int
     * @return the value of field 'cdDiscagemDiretaInterEmpr'.
     */
    public int getCdDiscagemDiretaInterEmpr()
    {
        return this._cdDiscagemDiretaInterEmpr;
    } //-- int getCdDiscagemDiretaInterEmpr() 

    /**
     * Returns the value of field 'cdDistcAgenciaInsta'.
     * 
     * @return int
     * @return the value of field 'cdDistcAgenciaInsta'.
     */
    public int getCdDistcAgenciaInsta()
    {
        return this._cdDistcAgenciaInsta;
    } //-- int getCdDistcAgenciaInsta() 

    /**
     * Returns the value of field 'cdEstrtInstaEmpr'.
     * 
     * @return int
     * @return the value of field 'cdEstrtInstaEmpr'.
     */
    public int getCdEstrtInstaEmpr()
    {
        return this._cdEstrtInstaEmpr;
    } //-- int getCdEstrtInstaEmpr() 

    /**
     * Returns the value of field 'cdFaiMaxOcorr'.
     * 
     * @return int
     * @return the value of field 'cdFaiMaxOcorr'.
     */
    public int getCdFaiMaxOcorr()
    {
        return this._cdFaiMaxOcorr;
    } //-- int getCdFaiMaxOcorr() 

    /**
     * Returns the value of field 'cdFilMaxOcorr'.
     * 
     * @return int
     * @return the value of field 'cdFilMaxOcorr'.
     */
    public int getCdFilMaxOcorr()
    {
        return this._cdFilMaxOcorr;
    } //-- int getCdFilMaxOcorr() 

    /**
     * Returns the value of field 'cdFolMaxOcorr'.
     * 
     * @return int
     * @return the value of field 'cdFolMaxOcorr'.
     */
    public int getCdFolMaxOcorr()
    {
        return this._cdFolMaxOcorr;
    } //-- int getCdFolMaxOcorr() 

    /**
     * Returns the value of field 'cdFuncGerRelacionamento'.
     * 
     * @return long
     * @return the value of field 'cdFuncGerRelacionamento'.
     */
    public long getCdFuncGerRelacionamento()
    {
        return this._cdFuncGerRelacionamento;
    } //-- long getCdFuncGerRelacionamento() 

    /**
     * Returns the value of field 'cdFuncGerVisitante'.
     * 
     * @return long
     * @return the value of field 'cdFuncGerVisitante'.
     */
    public long getCdFuncGerVisitante()
    {
        return this._cdFuncGerVisitante;
    } //-- long getCdFuncGerVisitante() 

    /**
     * Returns the value of field 'cdIndicadorAgCcren'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAgCcren'.
     */
    public int getCdIndicadorAgCcren()
    {
        return this._cdIndicadorAgCcren;
    } //-- int getCdIndicadorAgCcren() 

    /**
     * Returns the value of field 'cdIndicadorAltoTurnover'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAltoTurnover'.
     */
    public int getCdIndicadorAltoTurnover()
    {
        return this._cdIndicadorAltoTurnover;
    } //-- int getCdIndicadorAltoTurnover() 

    /**
     * Returns the value of field 'cdIndicadorAtivacaoAPP'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAtivacaoAPP'.
     */
    public int getCdIndicadorAtivacaoAPP()
    {
        return this._cdIndicadorAtivacaoAPP;
    } //-- int getCdIndicadorAtivacaoAPP() 

    /**
     * Returns the value of field 'cdIndicadorFlQuest'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFlQuest'.
     */
    public int getCdIndicadorFlQuest()
    {
        return this._cdIndicadorFlQuest;
    } //-- int getCdIndicadorFlQuest() 

    /**
     * Returns the value of field 'cdIndicadorGrpQuest'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorGrpQuest'.
     */
    public int getCdIndicadorGrpQuest()
    {
        return this._cdIndicadorGrpQuest;
    } //-- int getCdIndicadorGrpQuest() 

    /**
     * Returns the value of field 'cdIndicadorInstaEstrt'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorInstaEstrt'.
     */
    public int getCdIndicadorInstaEstrt()
    {
        return this._cdIndicadorInstaEstrt;
    } //-- int getCdIndicadorInstaEstrt() 

    /**
     * Returns the value of field 'cdIndicadorNumConve'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorNumConve'.
     */
    public int getCdIndicadorNumConve()
    {
        return this._cdIndicadorNumConve;
    } //-- int getCdIndicadorNumConve() 

    /**
     * Returns the value of field 'cdIndicadorOfertaCartaoPrePago'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorOfertaCartaoPrePago'.
     */
    public int getCdIndicadorOfertaCartaoPrePago()
    {
        return this._cdIndicadorOfertaCartaoPrePago;
    } //-- int getCdIndicadorOfertaCartaoPrePago() 

    /**
     * Returns the value of field 'cdIndicadorPaeCcren'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorPaeCcren'.
     */
    public int getCdIndicadorPaeCcren()
    {
        return this._cdIndicadorPaeCcren;
    } //-- int getCdIndicadorPaeCcren() 

    /**
     * Returns the value of field 'cdIndicadorPerdaRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorPerdaRelacionamento'.
     */
    public int getCdIndicadorPerdaRelacionamento()
    {
        return this._cdIndicadorPerdaRelacionamento;
    } //-- int getCdIndicadorPerdaRelacionamento() 

    /**
     * Returns the value of field 'cdIndicadorQuestSalarial'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorQuestSalarial'.
     */
    public int getCdIndicadorQuestSalarial()
    {
        return this._cdIndicadorQuestSalarial;
    } //-- int getCdIndicadorQuestSalarial() 

    /**
     * Returns the value of field 'cdIndicadorVisitaInsta'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorVisitaInsta'.
     */
    public int getCdIndicadorVisitaInsta()
    {
        return this._cdIndicadorVisitaInsta;
    } //-- int getCdIndicadorVisitaInsta() 

    /**
     * Returns the value of field 'cdInstituicaoBancaria'.
     * 
     * @return int
     * @return the value of field 'cdInstituicaoBancaria'.
     */
    public int getCdInstituicaoBancaria()
    {
        return this._cdInstituicaoBancaria;
    } //-- int getCdInstituicaoBancaria() 

    /**
     * Returns the value of field 'cdLocCtaSalarial'.
     * 
     * @return int
     * @return the value of field 'cdLocCtaSalarial'.
     */
    public int getCdLocCtaSalarial()
    {
        return this._cdLocCtaSalarial;
    } //-- int getCdLocCtaSalarial() 

    /**
     * Returns the value of field 'cdMunEmprQuest'.
     * 
     * @return long
     * @return the value of field 'cdMunEmprQuest'.
     */
    public long getCdMunEmprQuest()
    {
        return this._cdMunEmprQuest;
    } //-- long getCdMunEmprQuest() 

    /**
     * Returns the value of field 'cdPagamentoSalCheque'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoSalCheque'.
     */
    public int getCdPagamentoSalCheque()
    {
        return this._cdPagamentoSalCheque;
    } //-- int getCdPagamentoSalCheque() 

    /**
     * Returns the value of field 'cdPagamentoSalCredt'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoSalCredt'.
     */
    public int getCdPagamentoSalCredt()
    {
        return this._cdPagamentoSalCredt;
    } //-- int getCdPagamentoSalCredt() 

    /**
     * Returns the value of field 'cdPagamentoSalDinheiro'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoSalDinheiro'.
     */
    public int getCdPagamentoSalDinheiro()
    {
        return this._cdPagamentoSalDinheiro;
    } //-- int getCdPagamentoSalDinheiro() 

    /**
     * Returns the value of field 'cdPagamentoSalDivers'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoSalDivers'.
     */
    public int getCdPagamentoSalDivers()
    {
        return this._cdPagamentoSalDivers;
    } //-- int getCdPagamentoSalDivers() 

    /**
     * Returns the value of field 'cdPagamentoSalarialMes'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoSalarialMes'.
     */
    public int getCdPagamentoSalarialMes()
    {
        return this._cdPagamentoSalarialMes;
    } //-- int getCdPagamentoSalarialMes() 

    /**
     * Returns the value of field 'cdPagamentoSalarialQzena'.
     * 
     * @return int
     * @return the value of field 'cdPagamentoSalarialQzena'.
     */
    public int getCdPagamentoSalarialQzena()
    {
        return this._cdPagamentoSalarialQzena;
    } //-- int getCdPagamentoSalarialQzena() 

    /**
     * Returns the value of field 'cdPssoaJuridCta'.
     * 
     * @return long
     * @return the value of field 'cdPssoaJuridCta'.
     */
    public long getCdPssoaJuridCta()
    {
        return this._cdPssoaJuridCta;
    } //-- long getCdPssoaJuridCta() 

    /**
     * Returns the value of field 'cdSegmentoEmprQuest'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoEmprQuest'.
     */
    public int getCdSegmentoEmprQuest()
    {
        return this._cdSegmentoEmprQuest;
    } //-- int getCdSegmentoEmprQuest() 

    /**
     * Returns the value of field 'cdTipoContrCta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrCta'.
     */
    public int getCdTipoContrCta()
    {
        return this._cdTipoContrCta;
    } //-- int getCdTipoContrCta() 

    /**
     * Returns the value of field 'cdTipoNecesInsta'.
     * 
     * @return int
     * @return the value of field 'cdTipoNecesInsta'.
     */
    public int getCdTipoNecesInsta()
    {
        return this._cdTipoNecesInsta;
    } //-- int getCdTipoNecesInsta() 

    /**
     * Returns the value of field 'cdTurMaxOcorr'.
     * 
     * @return int
     * @return the value of field 'cdTurMaxOcorr'.
     */
    public int getCdTurMaxOcorr()
    {
        return this._cdTurMaxOcorr;
    } //-- int getCdTurMaxOcorr() 

    /**
     * Returns the value of field 'cdUdistcAgEmpr'.
     * 
     * @return int
     * @return the value of field 'cdUdistcAgEmpr'.
     */
    public int getCdUdistcAgEmpr()
    {
        return this._cdUdistcAgEmpr;
    } //-- int getCdUdistcAgEmpr() 

    /**
     * Returns the value of field 'cdUespacConcedidoInsta'.
     * 
     * @return int
     * @return the value of field 'cdUespacConcedidoInsta'.
     */
    public int getCdUespacConcedidoInsta()
    {
        return this._cdUespacConcedidoInsta;
    } //-- int getCdUespacConcedidoInsta() 

    /**
     * Returns the value of field 'cdUfEmpreQuest'.
     * 
     * @return int
     * @return the value of field 'cdUfEmpreQuest'.
     */
    public int getCdUfEmpreQuest()
    {
        return this._cdUfEmpreQuest;
    } //-- int getCdUfEmpreQuest() 

    /**
     * Returns the value of field 'cdUnidadeMeddAg'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeMeddAg'.
     */
    public int getCdUnidadeMeddAg()
    {
        return this._cdUnidadeMeddAg;
    } //-- int getCdUnidadeMeddAg() 

    /**
     * Returns the value of field 'cdUnidadeMeddEspac'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeMeddEspac'.
     */
    public int getCdUnidadeMeddEspac()
    {
        return this._cdUnidadeMeddEspac;
    } //-- int getCdUnidadeMeddEspac() 

    /**
     * Returns the value of field 'cdUnidadeMeddInsta'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeMeddInsta'.
     */
    public int getCdUnidadeMeddInsta()
    {
        return this._cdUnidadeMeddInsta;
    } //-- int getCdUnidadeMeddInsta() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacionalCusto'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacionalCusto'.
     */
    public int getCdUnidadeOrganizacionalCusto()
    {
        return this._cdUnidadeOrganizacionalCusto;
    } //-- int getCdUnidadeOrganizacionalCusto() 

    /**
     * Returns the value of field 'cdbancoVincInsta'.
     * 
     * @return int
     * @return the value of field 'cdbancoVincInsta'.
     */
    public int getCdbancoVincInsta()
    {
        return this._cdbancoVincInsta;
    } //-- int getCdbancoVincInsta() 

    /**
     * Returns the value of field 'cdprefeCsign'.
     * 
     * @return int
     * @return the value of field 'cdprefeCsign'.
     */
    public int getCdprefeCsign()
    {
        return this._cdprefeCsign;
    } //-- int getCdprefeCsign() 

    /**
     * Returns the value of field 'cdprefePabCren'.
     * 
     * @return int
     * @return the value of field 'cdprefePabCren'.
     */
    public int getCdprefePabCren()
    {
        return this._cdprefePabCren;
    } //-- int getCdprefePabCren() 

    /**
     * Returns the value of field 'codIndicadorFedelize'.
     * 
     * @return String
     * @return the value of field 'codIndicadorFedelize'.
     */
    public java.lang.String getCodIndicadorFedelize()
    {
        return this._codIndicadorFedelize;
    } //-- java.lang.String getCodIndicadorFedelize() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dataReftFlPgto'.
     * 
     * @return int
     * @return the value of field 'dataReftFlPgto'.
     */
    public int getDataReftFlPgto()
    {
        return this._dataReftFlPgto;
    } //-- int getDataReftFlPgto() 

    /**
     * Returns the value of field 'dsAbrevEspacConcedido'.
     * 
     * @return String
     * @return the value of field 'dsAbrevEspacConcedido'.
     */
    public java.lang.String getDsAbrevEspacConcedido()
    {
        return this._dsAbrevEspacConcedido;
    } //-- java.lang.String getDsAbrevEspacConcedido() 

    /**
     * Returns the value of field 'dsAbrevInsta'.
     * 
     * @return String
     * @return the value of field 'dsAbrevInsta'.
     */
    public java.lang.String getDsAbrevInsta()
    {
        return this._dsAbrevInsta;
    } //-- java.lang.String getDsAbrevInsta() 

    /**
     * Returns the value of field 'dsAbrevMeddAg'.
     * 
     * @return String
     * @return the value of field 'dsAbrevMeddAg'.
     */
    public java.lang.String getDsAbrevMeddAg()
    {
        return this._dsAbrevMeddAg;
    } //-- java.lang.String getDsAbrevMeddAg() 

    /**
     * Returns the value of field 'dsAbrevSegmentoEmpr'.
     * 
     * @return String
     * @return the value of field 'dsAbrevSegmentoEmpr'.
     */
    public java.lang.String getDsAbrevSegmentoEmpr()
    {
        return this._dsAbrevSegmentoEmpr;
    } //-- java.lang.String getDsAbrevSegmentoEmpr() 

    /**
     * Returns the value of field 'dsAgenciaVincInsta'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaVincInsta'.
     */
    public java.lang.String getDsAgenciaVincInsta()
    {
        return this._dsAgenciaVincInsta;
    } //-- java.lang.String getDsAgenciaVincInsta() 

    /**
     * Returns the value of field 'dsBairroEmprQuest'.
     * 
     * @return String
     * @return the value of field 'dsBairroEmprQuest'.
     */
    public java.lang.String getDsBairroEmprQuest()
    {
        return this._dsBairroEmprQuest;
    } //-- java.lang.String getDsBairroEmprQuest() 

    /**
     * Returns the value of field 'dsEmailAgencia'.
     * 
     * @return String
     * @return the value of field 'dsEmailAgencia'.
     */
    public java.lang.String getDsEmailAgencia()
    {
        return this._dsEmailAgencia;
    } //-- java.lang.String getDsEmailAgencia() 

    /**
     * Returns the value of field 'dsEmailEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmailEmpresa'.
     */
    public java.lang.String getDsEmailEmpresa()
    {
        return this._dsEmailEmpresa;
    } //-- java.lang.String getDsEmailEmpresa() 

    /**
     * Returns the value of field 'dsEstruturaInstaEmpr'.
     * 
     * @return String
     * @return the value of field 'dsEstruturaInstaEmpr'.
     */
    public java.lang.String getDsEstruturaInstaEmpr()
    {
        return this._dsEstruturaInstaEmpr;
    } //-- java.lang.String getDsEstruturaInstaEmpr() 

    /**
     * Returns the value of field 'dsGerRelacionamentoQuest'.
     * 
     * @return String
     * @return the value of field 'dsGerRelacionamentoQuest'.
     */
    public java.lang.String getDsGerRelacionamentoQuest()
    {
        return this._dsGerRelacionamentoQuest;
    } //-- java.lang.String getDsGerRelacionamentoQuest() 

    /**
     * Returns the value of field 'dsGerVisitanteInsta'.
     * 
     * @return String
     * @return the value of field 'dsGerVisitanteInsta'.
     */
    public java.lang.String getDsGerVisitanteInsta()
    {
        return this._dsGerVisitanteInsta;
    } //-- java.lang.String getDsGerVisitanteInsta() 

    /**
     * Returns the value of field 'dsGrupoEconmQuest'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconmQuest'.
     */
    public java.lang.String getDsGrupoEconmQuest()
    {
        return this._dsGrupoEconmQuest;
    } //-- java.lang.String getDsGrupoEconmQuest() 

    /**
     * Returns the value of field 'dsIndicadorFlQuest'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorFlQuest'.
     */
    public java.lang.String getDsIndicadorFlQuest()
    {
        return this._dsIndicadorFlQuest;
    } //-- java.lang.String getDsIndicadorFlQuest() 

    /**
     * Returns the value of field 'dsIndicadorPerdarelacionamento'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorPerdarelacionamento'.
     */
    public java.lang.String getDsIndicadorPerdarelacionamento()
    {
        return this._dsIndicadorPerdarelacionamento;
    } //-- java.lang.String getDsIndicadorPerdarelacionamento() 

    /**
     * Returns the value of field 'dsIndicadorVisitaInsta'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorVisitaInsta'.
     */
    public java.lang.String getDsIndicadorVisitaInsta()
    {
        return this._dsIndicadorVisitaInsta;
    } //-- java.lang.String getDsIndicadorVisitaInsta() 

    /**
     * Returns the value of field 'dsLocCtaSalarial'.
     * 
     * @return String
     * @return the value of field 'dsLocCtaSalarial'.
     */
    public java.lang.String getDsLocCtaSalarial()
    {
        return this._dsLocCtaSalarial;
    } //-- java.lang.String getDsLocCtaSalarial() 

    /**
     * Returns the value of field 'dsLogradouroEmprQuest'.
     * 
     * @return String
     * @return the value of field 'dsLogradouroEmprQuest'.
     */
    public java.lang.String getDsLogradouroEmprQuest()
    {
        return this._dsLogradouroEmprQuest;
    } //-- java.lang.String getDsLogradouroEmprQuest() 

    /**
     * Returns the value of field 'dsMuncEmpreQuest'.
     * 
     * @return String
     * @return the value of field 'dsMuncEmpreQuest'.
     */
    public java.lang.String getDsMuncEmpreQuest()
    {
        return this._dsMuncEmpreQuest;
    } //-- java.lang.String getDsMuncEmpreQuest() 

    /**
     * Returns the value of field 'dsPagamentoSalDivers'.
     * 
     * @return String
     * @return the value of field 'dsPagamentoSalDivers'.
     */
    public java.lang.String getDsPagamentoSalDivers()
    {
        return this._dsPagamentoSalDivers;
    } //-- java.lang.String getDsPagamentoSalDivers() 

    /**
     * Returns the value of field 'dsRamoAtividadeQuest'.
     * 
     * @return String
     * @return the value of field 'dsRamoAtividadeQuest'.
     */
    public java.lang.String getDsRamoAtividadeQuest()
    {
        return this._dsRamoAtividadeQuest;
    } //-- java.lang.String getDsRamoAtividadeQuest() 

    /**
     * Returns the value of field 'dsSiteEmprQuest'.
     * 
     * @return String
     * @return the value of field 'dsSiteEmprQuest'.
     */
    public java.lang.String getDsSiteEmprQuest()
    {
        return this._dsSiteEmprQuest;
    } //-- java.lang.String getDsSiteEmprQuest() 

    /**
     * Returns the value of field 'dsTextoJustfCompl'.
     * 
     * @return String
     * @return the value of field 'dsTextoJustfCompl'.
     */
    public java.lang.String getDsTextoJustfCompl()
    {
        return this._dsTextoJustfCompl;
    } //-- java.lang.String getDsTextoJustfCompl() 

    /**
     * Returns the value of field 'dsTextoJustfConc'.
     * 
     * @return String
     * @return the value of field 'dsTextoJustfConc'.
     */
    public java.lang.String getDsTextoJustfConc()
    {
        return this._dsTextoJustfConc;
    } //-- java.lang.String getDsTextoJustfConc() 

    /**
     * Returns the value of field 'dsTextoJustfRecip'.
     * 
     * @return String
     * @return the value of field 'dsTextoJustfRecip'.
     */
    public java.lang.String getDsTextoJustfRecip()
    {
        return this._dsTextoJustfRecip;
    } //-- java.lang.String getDsTextoJustfRecip() 

    /**
     * Returns the value of field 'dsTipoNecessInsta'.
     * 
     * @return String
     * @return the value of field 'dsTipoNecessInsta'.
     */
    public java.lang.String getDsTipoNecessInsta()
    {
        return this._dsTipoNecessInsta;
    } //-- java.lang.String getDsTipoNecessInsta() 

    /**
     * Returns the value of field 'dsUfEmprQuest'.
     * 
     * @return String
     * @return the value of field 'dsUfEmprQuest'.
     */
    public java.lang.String getDsUfEmprQuest()
    {
        return this._dsUfEmprQuest;
    } //-- java.lang.String getDsUfEmprQuest() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nroGrupoEconmQuest'.
     * 
     * @return long
     * @return the value of field 'nroGrupoEconmQuest'.
     */
    public long getNroGrupoEconmQuest()
    {
        return this._nroGrupoEconmQuest;
    } //-- long getNroGrupoEconmQuest() 

    /**
     * Returns the value of field 'numConta'.
     * 
     * @return int
     * @return the value of field 'numConta'.
     */
    public int getNumConta()
    {
        return this._numConta;
    } //-- int getNumConta() 

    /**
     * Returns the value of field 'numContaDigito'.
     * 
     * @return int
     * @return the value of field 'numContaDigito'.
     */
    public int getNumContaDigito()
    {
        return this._numContaDigito;
    } //-- int getNumContaDigito() 

    /**
     * Returns the value of field 'numDiaCredtPrim'.
     * 
     * @return int
     * @return the value of field 'numDiaCredtPrim'.
     */
    public int getNumDiaCredtPrim()
    {
        return this._numDiaCredtPrim;
    } //-- int getNumDiaCredtPrim() 

    /**
     * Returns the value of field 'numDiaCredtSeg'.
     * 
     * @return int
     * @return the value of field 'numDiaCredtSeg'.
     */
    public int getNumDiaCredtSeg()
    {
        return this._numDiaCredtSeg;
    } //-- int getNumDiaCredtSeg() 

    /**
     * Returns the value of field 'numFoneEmprQuest'.
     * 
     * @return long
     * @return the value of field 'numFoneEmprQuest'.
     */
    public long getNumFoneEmprQuest()
    {
        return this._numFoneEmprQuest;
    } //-- long getNumFoneEmprQuest() 

    /**
     * Returns the value of field 'numFoneGerQuest'.
     * 
     * @return long
     * @return the value of field 'numFoneGerQuest'.
     */
    public long getNumFoneGerQuest()
    {
        return this._numFoneGerQuest;
    } //-- long getNumFoneGerQuest() 

    /**
     * Returns the value of field 'numSequenciaContratoCta'.
     * 
     * @return long
     * @return the value of field 'numSequenciaContratoCta'.
     */
    public long getNumSequenciaContratoCta()
    {
        return this._numSequenciaContratoCta;
    } //-- long getNumSequenciaContratoCta() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 getOcorrencias2(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias2: Index value '"+index+"' not in range [0.."+(_ocorrencias2List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 getOcorrencias2(int) 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2[] getOcorrencias2()
    {
        int size = _ocorrencias2List.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2[] getOcorrencias2() 

    /**
     * Method getOcorrencias2Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias2Count()
    {
        return _ocorrencias2List.size();
    } //-- int getOcorrencias2Count() 

    /**
     * Method getOcorrencias3
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3 getOcorrencias3(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias3List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias3: Index value '"+index+"' not in range [0.."+(_ocorrencias3List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3) _ocorrencias3List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3 getOcorrencias3(int) 

    /**
     * Method getOcorrencias3
     * 
     * 
     * 
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3[] getOcorrencias3()
    {
        int size = _ocorrencias3List.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3) _ocorrencias3List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3[] getOcorrencias3() 

    /**
     * Method getOcorrencias3Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias3Count()
    {
        return _ocorrencias3List.size();
    } //-- int getOcorrencias3Count() 

    /**
     * Method getOcorrencias4
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4 getOcorrencias4(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias4List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias4: Index value '"+index+"' not in range [0.."+(_ocorrencias4List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4) _ocorrencias4List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4 getOcorrencias4(int) 

    /**
     * Method getOcorrencias4
     * 
     * 
     * 
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4[] getOcorrencias4()
    {
        int size = _ocorrencias4List.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4) _ocorrencias4List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4[] getOcorrencias4() 

    /**
     * Method getOcorrencias4Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias4Count()
    {
        return _ocorrencias4List.size();
    } //-- int getOcorrencias4Count() 

    /**
     * Method getOcorrencias5
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 getOcorrencias5(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias5List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias5: Index value '"+index+"' not in range [0.."+(_ocorrencias5List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5) _ocorrencias5List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 getOcorrencias5(int) 

    /**
     * Method getOcorrencias5
     * 
     * 
     * 
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5[] getOcorrencias5()
    {
        int size = _ocorrencias5List.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5) _ocorrencias5List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5[] getOcorrencias5() 

    /**
     * Method getOcorrencias5Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias5Count()
    {
        return _ocorrencias5List.size();
    } //-- int getOcorrencias5Count() 

    /**
     * Method getOcorrencias6
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6 getOcorrencias6(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias6List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias6: Index value '"+index+"' not in range [0.."+(_ocorrencias6List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6) _ocorrencias6List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6 getOcorrencias6(int) 

    /**
     * Method getOcorrencias6
     * 
     * 
     * 
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6[] getOcorrencias6()
    {
        int size = _ocorrencias6List.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6) _ocorrencias6List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6[] getOcorrencias6() 

    /**
     * Method getOcorrencias6Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias6Count()
    {
        return _ocorrencias6List.size();
    } //-- int getOcorrencias6Count() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'pctgmPreNovAnoQuest'.
     * 
     * @return BigDecimal
     * @return the value of field 'pctgmPreNovAnoQuest'.
     */
    public java.math.BigDecimal getPctgmPreNovAnoQuest()
    {
        return this._pctgmPreNovAnoQuest;
    } //-- java.math.BigDecimal getPctgmPreNovAnoQuest() 

    /**
     * Returns the value of field 'qtdFuncionarioFlPgto'.
     * 
     * @return int
     * @return the value of field 'qtdFuncionarioFlPgto'.
     */
    public int getQtdFuncionarioFlPgto()
    {
        return this._qtdFuncionarioFlPgto;
    } //-- int getQtdFuncionarioFlPgto() 

    /**
     * Returns the value of field 'vlMediaSalarialMes'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlMediaSalarialMes'.
     */
    public java.math.BigDecimal getVlMediaSalarialMes()
    {
        return this._vlMediaSalarialMes;
    } //-- java.math.BigDecimal getVlMediaSalarialMes() 

    /**
     * Returns the value of field 'vlPagamentoMes'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoMes'.
     */
    public java.math.BigDecimal getVlPagamentoMes()
    {
        return this._vlPagamentoMes;
    } //-- java.math.BigDecimal getVlPagamentoMes() 

    /**
     * Method hasCdAgenciaVincInsta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaVincInsta()
    {
        return this._has_cdAgenciaVincInsta;
    } //-- boolean hasCdAgenciaVincInsta() 

    /**
     * Method hasCdAgptoAtividadeQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgptoAtividadeQuest()
    {
        return this._has_cdAgptoAtividadeQuest;
    } //-- boolean hasCdAgptoAtividadeQuest() 

    /**
     * Method hasCdCepComplemento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepComplemento()
    {
        return this._has_cdCepComplemento;
    } //-- boolean hasCdCepComplemento() 

    /**
     * Method hasCdCepEmprQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepEmprQuest()
    {
        return this._has_cdCepEmprQuest;
    } //-- boolean hasCdCepEmprQuest() 

    /**
     * Method hasCdCocMaxOcorr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCocMaxOcorr()
    {
        return this._has_cdCocMaxOcorr;
    } //-- boolean hasCdCocMaxOcorr() 

    /**
     * Method hasCdConMaxOcorr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConMaxOcorr()
    {
        return this._has_cdConMaxOcorr;
    } //-- boolean hasCdConMaxOcorr() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdDiscagemDiretaDistEmpr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDiscagemDiretaDistEmpr()
    {
        return this._has_cdDiscagemDiretaDistEmpr;
    } //-- boolean hasCdDiscagemDiretaDistEmpr() 

    /**
     * Method hasCdDiscagemDiretaDistGer
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDiscagemDiretaDistGer()
    {
        return this._has_cdDiscagemDiretaDistGer;
    } //-- boolean hasCdDiscagemDiretaDistGer() 

    /**
     * Method hasCdDiscagemDiretaDistanGer
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDiscagemDiretaDistanGer()
    {
        return this._has_cdDiscagemDiretaDistanGer;
    } //-- boolean hasCdDiscagemDiretaDistanGer() 

    /**
     * Method hasCdDiscagemDiretaInterEmpr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDiscagemDiretaInterEmpr()
    {
        return this._has_cdDiscagemDiretaInterEmpr;
    } //-- boolean hasCdDiscagemDiretaInterEmpr() 

    /**
     * Method hasCdDistcAgenciaInsta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDistcAgenciaInsta()
    {
        return this._has_cdDistcAgenciaInsta;
    } //-- boolean hasCdDistcAgenciaInsta() 

    /**
     * Method hasCdEstrtInstaEmpr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEstrtInstaEmpr()
    {
        return this._has_cdEstrtInstaEmpr;
    } //-- boolean hasCdEstrtInstaEmpr() 

    /**
     * Method hasCdFaiMaxOcorr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFaiMaxOcorr()
    {
        return this._has_cdFaiMaxOcorr;
    } //-- boolean hasCdFaiMaxOcorr() 

    /**
     * Method hasCdFilMaxOcorr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilMaxOcorr()
    {
        return this._has_cdFilMaxOcorr;
    } //-- boolean hasCdFilMaxOcorr() 

    /**
     * Method hasCdFolMaxOcorr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFolMaxOcorr()
    {
        return this._has_cdFolMaxOcorr;
    } //-- boolean hasCdFolMaxOcorr() 

    /**
     * Method hasCdFuncGerRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncGerRelacionamento()
    {
        return this._has_cdFuncGerRelacionamento;
    } //-- boolean hasCdFuncGerRelacionamento() 

    /**
     * Method hasCdFuncGerVisitante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncGerVisitante()
    {
        return this._has_cdFuncGerVisitante;
    } //-- boolean hasCdFuncGerVisitante() 

    /**
     * Method hasCdIndicadorAgCcren
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAgCcren()
    {
        return this._has_cdIndicadorAgCcren;
    } //-- boolean hasCdIndicadorAgCcren() 

    /**
     * Method hasCdIndicadorAltoTurnover
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAltoTurnover()
    {
        return this._has_cdIndicadorAltoTurnover;
    } //-- boolean hasCdIndicadorAltoTurnover() 

    /**
     * Method hasCdIndicadorAtivacaoAPP
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAtivacaoAPP()
    {
        return this._has_cdIndicadorAtivacaoAPP;
    } //-- boolean hasCdIndicadorAtivacaoAPP() 

    /**
     * Method hasCdIndicadorFlQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFlQuest()
    {
        return this._has_cdIndicadorFlQuest;
    } //-- boolean hasCdIndicadorFlQuest() 

    /**
     * Method hasCdIndicadorGrpQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorGrpQuest()
    {
        return this._has_cdIndicadorGrpQuest;
    } //-- boolean hasCdIndicadorGrpQuest() 

    /**
     * Method hasCdIndicadorInstaEstrt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorInstaEstrt()
    {
        return this._has_cdIndicadorInstaEstrt;
    } //-- boolean hasCdIndicadorInstaEstrt() 

    /**
     * Method hasCdIndicadorNumConve
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorNumConve()
    {
        return this._has_cdIndicadorNumConve;
    } //-- boolean hasCdIndicadorNumConve() 

    /**
     * Method hasCdIndicadorOfertaCartaoPrePago
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorOfertaCartaoPrePago()
    {
        return this._has_cdIndicadorOfertaCartaoPrePago;
    } //-- boolean hasCdIndicadorOfertaCartaoPrePago() 

    /**
     * Method hasCdIndicadorPaeCcren
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorPaeCcren()
    {
        return this._has_cdIndicadorPaeCcren;
    } //-- boolean hasCdIndicadorPaeCcren() 

    /**
     * Method hasCdIndicadorPerdaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorPerdaRelacionamento()
    {
        return this._has_cdIndicadorPerdaRelacionamento;
    } //-- boolean hasCdIndicadorPerdaRelacionamento() 

    /**
     * Method hasCdIndicadorQuestSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorQuestSalarial()
    {
        return this._has_cdIndicadorQuestSalarial;
    } //-- boolean hasCdIndicadorQuestSalarial() 

    /**
     * Method hasCdIndicadorVisitaInsta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorVisitaInsta()
    {
        return this._has_cdIndicadorVisitaInsta;
    } //-- boolean hasCdIndicadorVisitaInsta() 

    /**
     * Method hasCdInstituicaoBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInstituicaoBancaria()
    {
        return this._has_cdInstituicaoBancaria;
    } //-- boolean hasCdInstituicaoBancaria() 

    /**
     * Method hasCdLocCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLocCtaSalarial()
    {
        return this._has_cdLocCtaSalarial;
    } //-- boolean hasCdLocCtaSalarial() 

    /**
     * Method hasCdMunEmprQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMunEmprQuest()
    {
        return this._has_cdMunEmprQuest;
    } //-- boolean hasCdMunEmprQuest() 

    /**
     * Method hasCdPagamentoSalCheque
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoSalCheque()
    {
        return this._has_cdPagamentoSalCheque;
    } //-- boolean hasCdPagamentoSalCheque() 

    /**
     * Method hasCdPagamentoSalCredt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoSalCredt()
    {
        return this._has_cdPagamentoSalCredt;
    } //-- boolean hasCdPagamentoSalCredt() 

    /**
     * Method hasCdPagamentoSalDinheiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoSalDinheiro()
    {
        return this._has_cdPagamentoSalDinheiro;
    } //-- boolean hasCdPagamentoSalDinheiro() 

    /**
     * Method hasCdPagamentoSalDivers
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoSalDivers()
    {
        return this._has_cdPagamentoSalDivers;
    } //-- boolean hasCdPagamentoSalDivers() 

    /**
     * Method hasCdPagamentoSalarialMes
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoSalarialMes()
    {
        return this._has_cdPagamentoSalarialMes;
    } //-- boolean hasCdPagamentoSalarialMes() 

    /**
     * Method hasCdPagamentoSalarialQzena
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPagamentoSalarialQzena()
    {
        return this._has_cdPagamentoSalarialQzena;
    } //-- boolean hasCdPagamentoSalarialQzena() 

    /**
     * Method hasCdPssoaJuridCta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPssoaJuridCta()
    {
        return this._has_cdPssoaJuridCta;
    } //-- boolean hasCdPssoaJuridCta() 

    /**
     * Method hasCdSegmentoEmprQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoEmprQuest()
    {
        return this._has_cdSegmentoEmprQuest;
    } //-- boolean hasCdSegmentoEmprQuest() 

    /**
     * Method hasCdTipoContrCta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrCta()
    {
        return this._has_cdTipoContrCta;
    } //-- boolean hasCdTipoContrCta() 

    /**
     * Method hasCdTipoNecesInsta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoNecesInsta()
    {
        return this._has_cdTipoNecesInsta;
    } //-- boolean hasCdTipoNecesInsta() 

    /**
     * Method hasCdTurMaxOcorr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTurMaxOcorr()
    {
        return this._has_cdTurMaxOcorr;
    } //-- boolean hasCdTurMaxOcorr() 

    /**
     * Method hasCdUdistcAgEmpr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUdistcAgEmpr()
    {
        return this._has_cdUdistcAgEmpr;
    } //-- boolean hasCdUdistcAgEmpr() 

    /**
     * Method hasCdUespacConcedidoInsta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUespacConcedidoInsta()
    {
        return this._has_cdUespacConcedidoInsta;
    } //-- boolean hasCdUespacConcedidoInsta() 

    /**
     * Method hasCdUfEmpreQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUfEmpreQuest()
    {
        return this._has_cdUfEmpreQuest;
    } //-- boolean hasCdUfEmpreQuest() 

    /**
     * Method hasCdUnidadeMeddAg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeMeddAg()
    {
        return this._has_cdUnidadeMeddAg;
    } //-- boolean hasCdUnidadeMeddAg() 

    /**
     * Method hasCdUnidadeMeddEspac
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeMeddEspac()
    {
        return this._has_cdUnidadeMeddEspac;
    } //-- boolean hasCdUnidadeMeddEspac() 

    /**
     * Method hasCdUnidadeMeddInsta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeMeddInsta()
    {
        return this._has_cdUnidadeMeddInsta;
    } //-- boolean hasCdUnidadeMeddInsta() 

    /**
     * Method hasCdUnidadeOrganizacionalCusto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacionalCusto()
    {
        return this._has_cdUnidadeOrganizacionalCusto;
    } //-- boolean hasCdUnidadeOrganizacionalCusto() 

    /**
     * Method hasCdbancoVincInsta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdbancoVincInsta()
    {
        return this._has_cdbancoVincInsta;
    } //-- boolean hasCdbancoVincInsta() 

    /**
     * Method hasCdprefeCsign
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdprefeCsign()
    {
        return this._has_cdprefeCsign;
    } //-- boolean hasCdprefeCsign() 

    /**
     * Method hasCdprefePabCren
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdprefePabCren()
    {
        return this._has_cdprefePabCren;
    } //-- boolean hasCdprefePabCren() 

    /**
     * Method hasDataReftFlPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDataReftFlPgto()
    {
        return this._has_dataReftFlPgto;
    } //-- boolean hasDataReftFlPgto() 

    /**
     * Method hasNroGrupoEconmQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNroGrupoEconmQuest()
    {
        return this._has_nroGrupoEconmQuest;
    } //-- boolean hasNroGrupoEconmQuest() 

    /**
     * Method hasNumConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumConta()
    {
        return this._has_numConta;
    } //-- boolean hasNumConta() 

    /**
     * Method hasNumContaDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumContaDigito()
    {
        return this._has_numContaDigito;
    } //-- boolean hasNumContaDigito() 

    /**
     * Method hasNumDiaCredtPrim
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumDiaCredtPrim()
    {
        return this._has_numDiaCredtPrim;
    } //-- boolean hasNumDiaCredtPrim() 

    /**
     * Method hasNumDiaCredtSeg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumDiaCredtSeg()
    {
        return this._has_numDiaCredtSeg;
    } //-- boolean hasNumDiaCredtSeg() 

    /**
     * Method hasNumFoneEmprQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumFoneEmprQuest()
    {
        return this._has_numFoneEmprQuest;
    } //-- boolean hasNumFoneEmprQuest() 

    /**
     * Method hasNumFoneGerQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumFoneGerQuest()
    {
        return this._has_numFoneGerQuest;
    } //-- boolean hasNumFoneGerQuest() 

    /**
     * Method hasNumSequenciaContratoCta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumSequenciaContratoCta()
    {
        return this._has_numSequenciaContratoCta;
    } //-- boolean hasNumSequenciaContratoCta() 

    /**
     * Method hasQtdFuncionarioFlPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFlPgto()
    {
        return this._has_qtdFuncionarioFlPgto;
    } //-- boolean hasQtdFuncionarioFlPgto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeAllOcorrencias2
     * 
     */
    public void removeAllOcorrencias2()
    {
        _ocorrencias2List.removeAllElements();
    } //-- void removeAllOcorrencias2() 

    /**
     * Method removeAllOcorrencias3
     * 
     */
    public void removeAllOcorrencias3()
    {
        _ocorrencias3List.removeAllElements();
    } //-- void removeAllOcorrencias3() 

    /**
     * Method removeAllOcorrencias4
     * 
     */
    public void removeAllOcorrencias4()
    {
        _ocorrencias4List.removeAllElements();
    } //-- void removeAllOcorrencias4() 

    /**
     * Method removeAllOcorrencias5
     * 
     */
    public void removeAllOcorrencias5()
    {
        _ocorrencias5List.removeAllElements();
    } //-- void removeAllOcorrencias5() 

    /**
     * Method removeAllOcorrencias6
     * 
     */
    public void removeAllOcorrencias6()
    {
        _ocorrencias6List.removeAllElements();
    } //-- void removeAllOcorrencias6() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Method removeOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 removeOcorrencias2(int index)
    {
        java.lang.Object obj = _ocorrencias2List.elementAt(index);
        _ocorrencias2List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 removeOcorrencias2(int) 

    /**
     * Method removeOcorrencias3
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3 removeOcorrencias3(int index)
    {
        java.lang.Object obj = _ocorrencias3List.elementAt(index);
        _ocorrencias3List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3 removeOcorrencias3(int) 

    /**
     * Method removeOcorrencias4
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias4
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4 removeOcorrencias4(int index)
    {
        java.lang.Object obj = _ocorrencias4List.elementAt(index);
        _ocorrencias4List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4 removeOcorrencias4(int) 

    /**
     * Method removeOcorrencias5
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias5
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 removeOcorrencias5(int index)
    {
        java.lang.Object obj = _ocorrencias5List.elementAt(index);
        _ocorrencias5List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 removeOcorrencias5(int) 

    /**
     * Method removeOcorrencias6
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias6
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6 removeOcorrencias6(int index)
    {
        java.lang.Object obj = _ocorrencias6List.elementAt(index);
        _ocorrencias6List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6 removeOcorrencias6(int) 

    /**
     * Sets the value of field 'cdAgenciaVincInsta'.
     * 
     * @param cdAgenciaVincInsta the value of field
     * 'cdAgenciaVincInsta'.
     */
    public void setCdAgenciaVincInsta(int cdAgenciaVincInsta)
    {
        this._cdAgenciaVincInsta = cdAgenciaVincInsta;
        this._has_cdAgenciaVincInsta = true;
    } //-- void setCdAgenciaVincInsta(int) 

    /**
     * Sets the value of field 'cdAgptoAtividadeQuest'.
     * 
     * @param cdAgptoAtividadeQuest the value of field
     * 'cdAgptoAtividadeQuest'.
     */
    public void setCdAgptoAtividadeQuest(long cdAgptoAtividadeQuest)
    {
        this._cdAgptoAtividadeQuest = cdAgptoAtividadeQuest;
        this._has_cdAgptoAtividadeQuest = true;
    } //-- void setCdAgptoAtividadeQuest(long) 

    /**
     * Sets the value of field 'cdCepComplemento'.
     * 
     * @param cdCepComplemento the value of field 'cdCepComplemento'
     */
    public void setCdCepComplemento(int cdCepComplemento)
    {
        this._cdCepComplemento = cdCepComplemento;
        this._has_cdCepComplemento = true;
    } //-- void setCdCepComplemento(int) 

    /**
     * Sets the value of field 'cdCepEmprQuest'.
     * 
     * @param cdCepEmprQuest the value of field 'cdCepEmprQuest'.
     */
    public void setCdCepEmprQuest(int cdCepEmprQuest)
    {
        this._cdCepEmprQuest = cdCepEmprQuest;
        this._has_cdCepEmprQuest = true;
    } //-- void setCdCepEmprQuest(int) 

    /**
     * Sets the value of field 'cdCocMaxOcorr'.
     * 
     * @param cdCocMaxOcorr the value of field 'cdCocMaxOcorr'.
     */
    public void setCdCocMaxOcorr(int cdCocMaxOcorr)
    {
        this._cdCocMaxOcorr = cdCocMaxOcorr;
        this._has_cdCocMaxOcorr = true;
    } //-- void setCdCocMaxOcorr(int) 

    /**
     * Sets the value of field 'cdConMaxOcorr'.
     * 
     * @param cdConMaxOcorr the value of field 'cdConMaxOcorr'.
     */
    public void setCdConMaxOcorr(int cdConMaxOcorr)
    {
        this._cdConMaxOcorr = cdConMaxOcorr;
        this._has_cdConMaxOcorr = true;
    } //-- void setCdConMaxOcorr(int) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdDiscagemDiretaDistEmpr'.
     * 
     * @param cdDiscagemDiretaDistEmpr the value of field
     * 'cdDiscagemDiretaDistEmpr'.
     */
    public void setCdDiscagemDiretaDistEmpr(int cdDiscagemDiretaDistEmpr)
    {
        this._cdDiscagemDiretaDistEmpr = cdDiscagemDiretaDistEmpr;
        this._has_cdDiscagemDiretaDistEmpr = true;
    } //-- void setCdDiscagemDiretaDistEmpr(int) 

    /**
     * Sets the value of field 'cdDiscagemDiretaDistGer'.
     * 
     * @param cdDiscagemDiretaDistGer the value of field
     * 'cdDiscagemDiretaDistGer'.
     */
    public void setCdDiscagemDiretaDistGer(int cdDiscagemDiretaDistGer)
    {
        this._cdDiscagemDiretaDistGer = cdDiscagemDiretaDistGer;
        this._has_cdDiscagemDiretaDistGer = true;
    } //-- void setCdDiscagemDiretaDistGer(int) 

    /**
     * Sets the value of field 'cdDiscagemDiretaDistanGer'.
     * 
     * @param cdDiscagemDiretaDistanGer the value of field
     * 'cdDiscagemDiretaDistanGer'.
     */
    public void setCdDiscagemDiretaDistanGer(int cdDiscagemDiretaDistanGer)
    {
        this._cdDiscagemDiretaDistanGer = cdDiscagemDiretaDistanGer;
        this._has_cdDiscagemDiretaDistanGer = true;
    } //-- void setCdDiscagemDiretaDistanGer(int) 

    /**
     * Sets the value of field 'cdDiscagemDiretaInterEmpr'.
     * 
     * @param cdDiscagemDiretaInterEmpr the value of field
     * 'cdDiscagemDiretaInterEmpr'.
     */
    public void setCdDiscagemDiretaInterEmpr(int cdDiscagemDiretaInterEmpr)
    {
        this._cdDiscagemDiretaInterEmpr = cdDiscagemDiretaInterEmpr;
        this._has_cdDiscagemDiretaInterEmpr = true;
    } //-- void setCdDiscagemDiretaInterEmpr(int) 

    /**
     * Sets the value of field 'cdDistcAgenciaInsta'.
     * 
     * @param cdDistcAgenciaInsta the value of field
     * 'cdDistcAgenciaInsta'.
     */
    public void setCdDistcAgenciaInsta(int cdDistcAgenciaInsta)
    {
        this._cdDistcAgenciaInsta = cdDistcAgenciaInsta;
        this._has_cdDistcAgenciaInsta = true;
    } //-- void setCdDistcAgenciaInsta(int) 

    /**
     * Sets the value of field 'cdEstrtInstaEmpr'.
     * 
     * @param cdEstrtInstaEmpr the value of field 'cdEstrtInstaEmpr'
     */
    public void setCdEstrtInstaEmpr(int cdEstrtInstaEmpr)
    {
        this._cdEstrtInstaEmpr = cdEstrtInstaEmpr;
        this._has_cdEstrtInstaEmpr = true;
    } //-- void setCdEstrtInstaEmpr(int) 

    /**
     * Sets the value of field 'cdFaiMaxOcorr'.
     * 
     * @param cdFaiMaxOcorr the value of field 'cdFaiMaxOcorr'.
     */
    public void setCdFaiMaxOcorr(int cdFaiMaxOcorr)
    {
        this._cdFaiMaxOcorr = cdFaiMaxOcorr;
        this._has_cdFaiMaxOcorr = true;
    } //-- void setCdFaiMaxOcorr(int) 

    /**
     * Sets the value of field 'cdFilMaxOcorr'.
     * 
     * @param cdFilMaxOcorr the value of field 'cdFilMaxOcorr'.
     */
    public void setCdFilMaxOcorr(int cdFilMaxOcorr)
    {
        this._cdFilMaxOcorr = cdFilMaxOcorr;
        this._has_cdFilMaxOcorr = true;
    } //-- void setCdFilMaxOcorr(int) 

    /**
     * Sets the value of field 'cdFolMaxOcorr'.
     * 
     * @param cdFolMaxOcorr the value of field 'cdFolMaxOcorr'.
     */
    public void setCdFolMaxOcorr(int cdFolMaxOcorr)
    {
        this._cdFolMaxOcorr = cdFolMaxOcorr;
        this._has_cdFolMaxOcorr = true;
    } //-- void setCdFolMaxOcorr(int) 

    /**
     * Sets the value of field 'cdFuncGerRelacionamento'.
     * 
     * @param cdFuncGerRelacionamento the value of field
     * 'cdFuncGerRelacionamento'.
     */
    public void setCdFuncGerRelacionamento(long cdFuncGerRelacionamento)
    {
        this._cdFuncGerRelacionamento = cdFuncGerRelacionamento;
        this._has_cdFuncGerRelacionamento = true;
    } //-- void setCdFuncGerRelacionamento(long) 

    /**
     * Sets the value of field 'cdFuncGerVisitante'.
     * 
     * @param cdFuncGerVisitante the value of field
     * 'cdFuncGerVisitante'.
     */
    public void setCdFuncGerVisitante(long cdFuncGerVisitante)
    {
        this._cdFuncGerVisitante = cdFuncGerVisitante;
        this._has_cdFuncGerVisitante = true;
    } //-- void setCdFuncGerVisitante(long) 

    /**
     * Sets the value of field 'cdIndicadorAgCcren'.
     * 
     * @param cdIndicadorAgCcren the value of field
     * 'cdIndicadorAgCcren'.
     */
    public void setCdIndicadorAgCcren(int cdIndicadorAgCcren)
    {
        this._cdIndicadorAgCcren = cdIndicadorAgCcren;
        this._has_cdIndicadorAgCcren = true;
    } //-- void setCdIndicadorAgCcren(int) 

    /**
     * Sets the value of field 'cdIndicadorAltoTurnover'.
     * 
     * @param cdIndicadorAltoTurnover the value of field
     * 'cdIndicadorAltoTurnover'.
     */
    public void setCdIndicadorAltoTurnover(int cdIndicadorAltoTurnover)
    {
        this._cdIndicadorAltoTurnover = cdIndicadorAltoTurnover;
        this._has_cdIndicadorAltoTurnover = true;
    } //-- void setCdIndicadorAltoTurnover(int) 

    /**
     * Sets the value of field 'cdIndicadorAtivacaoAPP'.
     * 
     * @param cdIndicadorAtivacaoAPP the value of field
     * 'cdIndicadorAtivacaoAPP'.
     */
    public void setCdIndicadorAtivacaoAPP(int cdIndicadorAtivacaoAPP)
    {
        this._cdIndicadorAtivacaoAPP = cdIndicadorAtivacaoAPP;
        this._has_cdIndicadorAtivacaoAPP = true;
    } //-- void setCdIndicadorAtivacaoAPP(int) 

    /**
     * Sets the value of field 'cdIndicadorFlQuest'.
     * 
     * @param cdIndicadorFlQuest the value of field
     * 'cdIndicadorFlQuest'.
     */
    public void setCdIndicadorFlQuest(int cdIndicadorFlQuest)
    {
        this._cdIndicadorFlQuest = cdIndicadorFlQuest;
        this._has_cdIndicadorFlQuest = true;
    } //-- void setCdIndicadorFlQuest(int) 

    /**
     * Sets the value of field 'cdIndicadorGrpQuest'.
     * 
     * @param cdIndicadorGrpQuest the value of field
     * 'cdIndicadorGrpQuest'.
     */
    public void setCdIndicadorGrpQuest(int cdIndicadorGrpQuest)
    {
        this._cdIndicadorGrpQuest = cdIndicadorGrpQuest;
        this._has_cdIndicadorGrpQuest = true;
    } //-- void setCdIndicadorGrpQuest(int) 

    /**
     * Sets the value of field 'cdIndicadorInstaEstrt'.
     * 
     * @param cdIndicadorInstaEstrt the value of field
     * 'cdIndicadorInstaEstrt'.
     */
    public void setCdIndicadorInstaEstrt(int cdIndicadorInstaEstrt)
    {
        this._cdIndicadorInstaEstrt = cdIndicadorInstaEstrt;
        this._has_cdIndicadorInstaEstrt = true;
    } //-- void setCdIndicadorInstaEstrt(int) 

    /**
     * Sets the value of field 'cdIndicadorNumConve'.
     * 
     * @param cdIndicadorNumConve the value of field
     * 'cdIndicadorNumConve'.
     */
    public void setCdIndicadorNumConve(int cdIndicadorNumConve)
    {
        this._cdIndicadorNumConve = cdIndicadorNumConve;
        this._has_cdIndicadorNumConve = true;
    } //-- void setCdIndicadorNumConve(int) 

    /**
     * Sets the value of field 'cdIndicadorOfertaCartaoPrePago'.
     * 
     * @param cdIndicadorOfertaCartaoPrePago the value of field
     * 'cdIndicadorOfertaCartaoPrePago'.
     */
    public void setCdIndicadorOfertaCartaoPrePago(int cdIndicadorOfertaCartaoPrePago)
    {
        this._cdIndicadorOfertaCartaoPrePago = cdIndicadorOfertaCartaoPrePago;
        this._has_cdIndicadorOfertaCartaoPrePago = true;
    } //-- void setCdIndicadorOfertaCartaoPrePago(int) 

    /**
     * Sets the value of field 'cdIndicadorPaeCcren'.
     * 
     * @param cdIndicadorPaeCcren the value of field
     * 'cdIndicadorPaeCcren'.
     */
    public void setCdIndicadorPaeCcren(int cdIndicadorPaeCcren)
    {
        this._cdIndicadorPaeCcren = cdIndicadorPaeCcren;
        this._has_cdIndicadorPaeCcren = true;
    } //-- void setCdIndicadorPaeCcren(int) 

    /**
     * Sets the value of field 'cdIndicadorPerdaRelacionamento'.
     * 
     * @param cdIndicadorPerdaRelacionamento the value of field
     * 'cdIndicadorPerdaRelacionamento'.
     */
    public void setCdIndicadorPerdaRelacionamento(int cdIndicadorPerdaRelacionamento)
    {
        this._cdIndicadorPerdaRelacionamento = cdIndicadorPerdaRelacionamento;
        this._has_cdIndicadorPerdaRelacionamento = true;
    } //-- void setCdIndicadorPerdaRelacionamento(int) 

    /**
     * Sets the value of field 'cdIndicadorQuestSalarial'.
     * 
     * @param cdIndicadorQuestSalarial the value of field
     * 'cdIndicadorQuestSalarial'.
     */
    public void setCdIndicadorQuestSalarial(int cdIndicadorQuestSalarial)
    {
        this._cdIndicadorQuestSalarial = cdIndicadorQuestSalarial;
        this._has_cdIndicadorQuestSalarial = true;
    } //-- void setCdIndicadorQuestSalarial(int) 

    /**
     * Sets the value of field 'cdIndicadorVisitaInsta'.
     * 
     * @param cdIndicadorVisitaInsta the value of field
     * 'cdIndicadorVisitaInsta'.
     */
    public void setCdIndicadorVisitaInsta(int cdIndicadorVisitaInsta)
    {
        this._cdIndicadorVisitaInsta = cdIndicadorVisitaInsta;
        this._has_cdIndicadorVisitaInsta = true;
    } //-- void setCdIndicadorVisitaInsta(int) 

    /**
     * Sets the value of field 'cdInstituicaoBancaria'.
     * 
     * @param cdInstituicaoBancaria the value of field
     * 'cdInstituicaoBancaria'.
     */
    public void setCdInstituicaoBancaria(int cdInstituicaoBancaria)
    {
        this._cdInstituicaoBancaria = cdInstituicaoBancaria;
        this._has_cdInstituicaoBancaria = true;
    } //-- void setCdInstituicaoBancaria(int) 

    /**
     * Sets the value of field 'cdLocCtaSalarial'.
     * 
     * @param cdLocCtaSalarial the value of field 'cdLocCtaSalarial'
     */
    public void setCdLocCtaSalarial(int cdLocCtaSalarial)
    {
        this._cdLocCtaSalarial = cdLocCtaSalarial;
        this._has_cdLocCtaSalarial = true;
    } //-- void setCdLocCtaSalarial(int) 

    /**
     * Sets the value of field 'cdMunEmprQuest'.
     * 
     * @param cdMunEmprQuest the value of field 'cdMunEmprQuest'.
     */
    public void setCdMunEmprQuest(long cdMunEmprQuest)
    {
        this._cdMunEmprQuest = cdMunEmprQuest;
        this._has_cdMunEmprQuest = true;
    } //-- void setCdMunEmprQuest(long) 

    /**
     * Sets the value of field 'cdPagamentoSalCheque'.
     * 
     * @param cdPagamentoSalCheque the value of field
     * 'cdPagamentoSalCheque'.
     */
    public void setCdPagamentoSalCheque(int cdPagamentoSalCheque)
    {
        this._cdPagamentoSalCheque = cdPagamentoSalCheque;
        this._has_cdPagamentoSalCheque = true;
    } //-- void setCdPagamentoSalCheque(int) 

    /**
     * Sets the value of field 'cdPagamentoSalCredt'.
     * 
     * @param cdPagamentoSalCredt the value of field
     * 'cdPagamentoSalCredt'.
     */
    public void setCdPagamentoSalCredt(int cdPagamentoSalCredt)
    {
        this._cdPagamentoSalCredt = cdPagamentoSalCredt;
        this._has_cdPagamentoSalCredt = true;
    } //-- void setCdPagamentoSalCredt(int) 

    /**
     * Sets the value of field 'cdPagamentoSalDinheiro'.
     * 
     * @param cdPagamentoSalDinheiro the value of field
     * 'cdPagamentoSalDinheiro'.
     */
    public void setCdPagamentoSalDinheiro(int cdPagamentoSalDinheiro)
    {
        this._cdPagamentoSalDinheiro = cdPagamentoSalDinheiro;
        this._has_cdPagamentoSalDinheiro = true;
    } //-- void setCdPagamentoSalDinheiro(int) 

    /**
     * Sets the value of field 'cdPagamentoSalDivers'.
     * 
     * @param cdPagamentoSalDivers the value of field
     * 'cdPagamentoSalDivers'.
     */
    public void setCdPagamentoSalDivers(int cdPagamentoSalDivers)
    {
        this._cdPagamentoSalDivers = cdPagamentoSalDivers;
        this._has_cdPagamentoSalDivers = true;
    } //-- void setCdPagamentoSalDivers(int) 

    /**
     * Sets the value of field 'cdPagamentoSalarialMes'.
     * 
     * @param cdPagamentoSalarialMes the value of field
     * 'cdPagamentoSalarialMes'.
     */
    public void setCdPagamentoSalarialMes(int cdPagamentoSalarialMes)
    {
        this._cdPagamentoSalarialMes = cdPagamentoSalarialMes;
        this._has_cdPagamentoSalarialMes = true;
    } //-- void setCdPagamentoSalarialMes(int) 

    /**
     * Sets the value of field 'cdPagamentoSalarialQzena'.
     * 
     * @param cdPagamentoSalarialQzena the value of field
     * 'cdPagamentoSalarialQzena'.
     */
    public void setCdPagamentoSalarialQzena(int cdPagamentoSalarialQzena)
    {
        this._cdPagamentoSalarialQzena = cdPagamentoSalarialQzena;
        this._has_cdPagamentoSalarialQzena = true;
    } //-- void setCdPagamentoSalarialQzena(int) 

    /**
     * Sets the value of field 'cdPssoaJuridCta'.
     * 
     * @param cdPssoaJuridCta the value of field 'cdPssoaJuridCta'.
     */
    public void setCdPssoaJuridCta(long cdPssoaJuridCta)
    {
        this._cdPssoaJuridCta = cdPssoaJuridCta;
        this._has_cdPssoaJuridCta = true;
    } //-- void setCdPssoaJuridCta(long) 

    /**
     * Sets the value of field 'cdSegmentoEmprQuest'.
     * 
     * @param cdSegmentoEmprQuest the value of field
     * 'cdSegmentoEmprQuest'.
     */
    public void setCdSegmentoEmprQuest(int cdSegmentoEmprQuest)
    {
        this._cdSegmentoEmprQuest = cdSegmentoEmprQuest;
        this._has_cdSegmentoEmprQuest = true;
    } //-- void setCdSegmentoEmprQuest(int) 

    /**
     * Sets the value of field 'cdTipoContrCta'.
     * 
     * @param cdTipoContrCta the value of field 'cdTipoContrCta'.
     */
    public void setCdTipoContrCta(int cdTipoContrCta)
    {
        this._cdTipoContrCta = cdTipoContrCta;
        this._has_cdTipoContrCta = true;
    } //-- void setCdTipoContrCta(int) 

    /**
     * Sets the value of field 'cdTipoNecesInsta'.
     * 
     * @param cdTipoNecesInsta the value of field 'cdTipoNecesInsta'
     */
    public void setCdTipoNecesInsta(int cdTipoNecesInsta)
    {
        this._cdTipoNecesInsta = cdTipoNecesInsta;
        this._has_cdTipoNecesInsta = true;
    } //-- void setCdTipoNecesInsta(int) 

    /**
     * Sets the value of field 'cdTurMaxOcorr'.
     * 
     * @param cdTurMaxOcorr the value of field 'cdTurMaxOcorr'.
     */
    public void setCdTurMaxOcorr(int cdTurMaxOcorr)
    {
        this._cdTurMaxOcorr = cdTurMaxOcorr;
        this._has_cdTurMaxOcorr = true;
    } //-- void setCdTurMaxOcorr(int) 

    /**
     * Sets the value of field 'cdUdistcAgEmpr'.
     * 
     * @param cdUdistcAgEmpr the value of field 'cdUdistcAgEmpr'.
     */
    public void setCdUdistcAgEmpr(int cdUdistcAgEmpr)
    {
        this._cdUdistcAgEmpr = cdUdistcAgEmpr;
        this._has_cdUdistcAgEmpr = true;
    } //-- void setCdUdistcAgEmpr(int) 

    /**
     * Sets the value of field 'cdUespacConcedidoInsta'.
     * 
     * @param cdUespacConcedidoInsta the value of field
     * 'cdUespacConcedidoInsta'.
     */
    public void setCdUespacConcedidoInsta(int cdUespacConcedidoInsta)
    {
        this._cdUespacConcedidoInsta = cdUespacConcedidoInsta;
        this._has_cdUespacConcedidoInsta = true;
    } //-- void setCdUespacConcedidoInsta(int) 

    /**
     * Sets the value of field 'cdUfEmpreQuest'.
     * 
     * @param cdUfEmpreQuest the value of field 'cdUfEmpreQuest'.
     */
    public void setCdUfEmpreQuest(int cdUfEmpreQuest)
    {
        this._cdUfEmpreQuest = cdUfEmpreQuest;
        this._has_cdUfEmpreQuest = true;
    } //-- void setCdUfEmpreQuest(int) 

    /**
     * Sets the value of field 'cdUnidadeMeddAg'.
     * 
     * @param cdUnidadeMeddAg the value of field 'cdUnidadeMeddAg'.
     */
    public void setCdUnidadeMeddAg(int cdUnidadeMeddAg)
    {
        this._cdUnidadeMeddAg = cdUnidadeMeddAg;
        this._has_cdUnidadeMeddAg = true;
    } //-- void setCdUnidadeMeddAg(int) 

    /**
     * Sets the value of field 'cdUnidadeMeddEspac'.
     * 
     * @param cdUnidadeMeddEspac the value of field
     * 'cdUnidadeMeddEspac'.
     */
    public void setCdUnidadeMeddEspac(int cdUnidadeMeddEspac)
    {
        this._cdUnidadeMeddEspac = cdUnidadeMeddEspac;
        this._has_cdUnidadeMeddEspac = true;
    } //-- void setCdUnidadeMeddEspac(int) 

    /**
     * Sets the value of field 'cdUnidadeMeddInsta'.
     * 
     * @param cdUnidadeMeddInsta the value of field
     * 'cdUnidadeMeddInsta'.
     */
    public void setCdUnidadeMeddInsta(int cdUnidadeMeddInsta)
    {
        this._cdUnidadeMeddInsta = cdUnidadeMeddInsta;
        this._has_cdUnidadeMeddInsta = true;
    } //-- void setCdUnidadeMeddInsta(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacionalCusto'.
     * 
     * @param cdUnidadeOrganizacionalCusto the value of field
     * 'cdUnidadeOrganizacionalCusto'.
     */
    public void setCdUnidadeOrganizacionalCusto(int cdUnidadeOrganizacionalCusto)
    {
        this._cdUnidadeOrganizacionalCusto = cdUnidadeOrganizacionalCusto;
        this._has_cdUnidadeOrganizacionalCusto = true;
    } //-- void setCdUnidadeOrganizacionalCusto(int) 

    /**
     * Sets the value of field 'cdbancoVincInsta'.
     * 
     * @param cdbancoVincInsta the value of field 'cdbancoVincInsta'
     */
    public void setCdbancoVincInsta(int cdbancoVincInsta)
    {
        this._cdbancoVincInsta = cdbancoVincInsta;
        this._has_cdbancoVincInsta = true;
    } //-- void setCdbancoVincInsta(int) 

    /**
     * Sets the value of field 'cdprefeCsign'.
     * 
     * @param cdprefeCsign the value of field 'cdprefeCsign'.
     */
    public void setCdprefeCsign(int cdprefeCsign)
    {
        this._cdprefeCsign = cdprefeCsign;
        this._has_cdprefeCsign = true;
    } //-- void setCdprefeCsign(int) 

    /**
     * Sets the value of field 'cdprefePabCren'.
     * 
     * @param cdprefePabCren the value of field 'cdprefePabCren'.
     */
    public void setCdprefePabCren(int cdprefePabCren)
    {
        this._cdprefePabCren = cdprefePabCren;
        this._has_cdprefePabCren = true;
    } //-- void setCdprefePabCren(int) 

    /**
     * Sets the value of field 'codIndicadorFedelize'.
     * 
     * @param codIndicadorFedelize the value of field
     * 'codIndicadorFedelize'.
     */
    public void setCodIndicadorFedelize(java.lang.String codIndicadorFedelize)
    {
        this._codIndicadorFedelize = codIndicadorFedelize;
    } //-- void setCodIndicadorFedelize(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dataReftFlPgto'.
     * 
     * @param dataReftFlPgto the value of field 'dataReftFlPgto'.
     */
    public void setDataReftFlPgto(int dataReftFlPgto)
    {
        this._dataReftFlPgto = dataReftFlPgto;
        this._has_dataReftFlPgto = true;
    } //-- void setDataReftFlPgto(int) 

    /**
     * Sets the value of field 'dsAbrevEspacConcedido'.
     * 
     * @param dsAbrevEspacConcedido the value of field
     * 'dsAbrevEspacConcedido'.
     */
    public void setDsAbrevEspacConcedido(java.lang.String dsAbrevEspacConcedido)
    {
        this._dsAbrevEspacConcedido = dsAbrevEspacConcedido;
    } //-- void setDsAbrevEspacConcedido(java.lang.String) 

    /**
     * Sets the value of field 'dsAbrevInsta'.
     * 
     * @param dsAbrevInsta the value of field 'dsAbrevInsta'.
     */
    public void setDsAbrevInsta(java.lang.String dsAbrevInsta)
    {
        this._dsAbrevInsta = dsAbrevInsta;
    } //-- void setDsAbrevInsta(java.lang.String) 

    /**
     * Sets the value of field 'dsAbrevMeddAg'.
     * 
     * @param dsAbrevMeddAg the value of field 'dsAbrevMeddAg'.
     */
    public void setDsAbrevMeddAg(java.lang.String dsAbrevMeddAg)
    {
        this._dsAbrevMeddAg = dsAbrevMeddAg;
    } //-- void setDsAbrevMeddAg(java.lang.String) 

    /**
     * Sets the value of field 'dsAbrevSegmentoEmpr'.
     * 
     * @param dsAbrevSegmentoEmpr the value of field
     * 'dsAbrevSegmentoEmpr'.
     */
    public void setDsAbrevSegmentoEmpr(java.lang.String dsAbrevSegmentoEmpr)
    {
        this._dsAbrevSegmentoEmpr = dsAbrevSegmentoEmpr;
    } //-- void setDsAbrevSegmentoEmpr(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaVincInsta'.
     * 
     * @param dsAgenciaVincInsta the value of field
     * 'dsAgenciaVincInsta'.
     */
    public void setDsAgenciaVincInsta(java.lang.String dsAgenciaVincInsta)
    {
        this._dsAgenciaVincInsta = dsAgenciaVincInsta;
    } //-- void setDsAgenciaVincInsta(java.lang.String) 

    /**
     * Sets the value of field 'dsBairroEmprQuest'.
     * 
     * @param dsBairroEmprQuest the value of field
     * 'dsBairroEmprQuest'.
     */
    public void setDsBairroEmprQuest(java.lang.String dsBairroEmprQuest)
    {
        this._dsBairroEmprQuest = dsBairroEmprQuest;
    } //-- void setDsBairroEmprQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailAgencia'.
     * 
     * @param dsEmailAgencia the value of field 'dsEmailAgencia'.
     */
    public void setDsEmailAgencia(java.lang.String dsEmailAgencia)
    {
        this._dsEmailAgencia = dsEmailAgencia;
    } //-- void setDsEmailAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailEmpresa'.
     * 
     * @param dsEmailEmpresa the value of field 'dsEmailEmpresa'.
     */
    public void setDsEmailEmpresa(java.lang.String dsEmailEmpresa)
    {
        this._dsEmailEmpresa = dsEmailEmpresa;
    } //-- void setDsEmailEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsEstruturaInstaEmpr'.
     * 
     * @param dsEstruturaInstaEmpr the value of field
     * 'dsEstruturaInstaEmpr'.
     */
    public void setDsEstruturaInstaEmpr(java.lang.String dsEstruturaInstaEmpr)
    {
        this._dsEstruturaInstaEmpr = dsEstruturaInstaEmpr;
    } //-- void setDsEstruturaInstaEmpr(java.lang.String) 

    /**
     * Sets the value of field 'dsGerRelacionamentoQuest'.
     * 
     * @param dsGerRelacionamentoQuest the value of field
     * 'dsGerRelacionamentoQuest'.
     */
    public void setDsGerRelacionamentoQuest(java.lang.String dsGerRelacionamentoQuest)
    {
        this._dsGerRelacionamentoQuest = dsGerRelacionamentoQuest;
    } //-- void setDsGerRelacionamentoQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsGerVisitanteInsta'.
     * 
     * @param dsGerVisitanteInsta the value of field
     * 'dsGerVisitanteInsta'.
     */
    public void setDsGerVisitanteInsta(java.lang.String dsGerVisitanteInsta)
    {
        this._dsGerVisitanteInsta = dsGerVisitanteInsta;
    } //-- void setDsGerVisitanteInsta(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconmQuest'.
     * 
     * @param dsGrupoEconmQuest the value of field
     * 'dsGrupoEconmQuest'.
     */
    public void setDsGrupoEconmQuest(java.lang.String dsGrupoEconmQuest)
    {
        this._dsGrupoEconmQuest = dsGrupoEconmQuest;
    } //-- void setDsGrupoEconmQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorFlQuest'.
     * 
     * @param dsIndicadorFlQuest the value of field
     * 'dsIndicadorFlQuest'.
     */
    public void setDsIndicadorFlQuest(java.lang.String dsIndicadorFlQuest)
    {
        this._dsIndicadorFlQuest = dsIndicadorFlQuest;
    } //-- void setDsIndicadorFlQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorPerdarelacionamento'.
     * 
     * @param dsIndicadorPerdarelacionamento the value of field
     * 'dsIndicadorPerdarelacionamento'.
     */
    public void setDsIndicadorPerdarelacionamento(java.lang.String dsIndicadorPerdarelacionamento)
    {
        this._dsIndicadorPerdarelacionamento = dsIndicadorPerdarelacionamento;
    } //-- void setDsIndicadorPerdarelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorVisitaInsta'.
     * 
     * @param dsIndicadorVisitaInsta the value of field
     * 'dsIndicadorVisitaInsta'.
     */
    public void setDsIndicadorVisitaInsta(java.lang.String dsIndicadorVisitaInsta)
    {
        this._dsIndicadorVisitaInsta = dsIndicadorVisitaInsta;
    } //-- void setDsIndicadorVisitaInsta(java.lang.String) 

    /**
     * Sets the value of field 'dsLocCtaSalarial'.
     * 
     * @param dsLocCtaSalarial the value of field 'dsLocCtaSalarial'
     */
    public void setDsLocCtaSalarial(java.lang.String dsLocCtaSalarial)
    {
        this._dsLocCtaSalarial = dsLocCtaSalarial;
    } //-- void setDsLocCtaSalarial(java.lang.String) 

    /**
     * Sets the value of field 'dsLogradouroEmprQuest'.
     * 
     * @param dsLogradouroEmprQuest the value of field
     * 'dsLogradouroEmprQuest'.
     */
    public void setDsLogradouroEmprQuest(java.lang.String dsLogradouroEmprQuest)
    {
        this._dsLogradouroEmprQuest = dsLogradouroEmprQuest;
    } //-- void setDsLogradouroEmprQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsMuncEmpreQuest'.
     * 
     * @param dsMuncEmpreQuest the value of field 'dsMuncEmpreQuest'
     */
    public void setDsMuncEmpreQuest(java.lang.String dsMuncEmpreQuest)
    {
        this._dsMuncEmpreQuest = dsMuncEmpreQuest;
    } //-- void setDsMuncEmpreQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsPagamentoSalDivers'.
     * 
     * @param dsPagamentoSalDivers the value of field
     * 'dsPagamentoSalDivers'.
     */
    public void setDsPagamentoSalDivers(java.lang.String dsPagamentoSalDivers)
    {
        this._dsPagamentoSalDivers = dsPagamentoSalDivers;
    } //-- void setDsPagamentoSalDivers(java.lang.String) 

    /**
     * Sets the value of field 'dsRamoAtividadeQuest'.
     * 
     * @param dsRamoAtividadeQuest the value of field
     * 'dsRamoAtividadeQuest'.
     */
    public void setDsRamoAtividadeQuest(java.lang.String dsRamoAtividadeQuest)
    {
        this._dsRamoAtividadeQuest = dsRamoAtividadeQuest;
    } //-- void setDsRamoAtividadeQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsSiteEmprQuest'.
     * 
     * @param dsSiteEmprQuest the value of field 'dsSiteEmprQuest'.
     */
    public void setDsSiteEmprQuest(java.lang.String dsSiteEmprQuest)
    {
        this._dsSiteEmprQuest = dsSiteEmprQuest;
    } //-- void setDsSiteEmprQuest(java.lang.String) 

    /**
     * Sets the value of field 'dsTextoJustfCompl'.
     * 
     * @param dsTextoJustfCompl the value of field
     * 'dsTextoJustfCompl'.
     */
    public void setDsTextoJustfCompl(java.lang.String dsTextoJustfCompl)
    {
        this._dsTextoJustfCompl = dsTextoJustfCompl;
    } //-- void setDsTextoJustfCompl(java.lang.String) 

    /**
     * Sets the value of field 'dsTextoJustfConc'.
     * 
     * @param dsTextoJustfConc the value of field 'dsTextoJustfConc'
     */
    public void setDsTextoJustfConc(java.lang.String dsTextoJustfConc)
    {
        this._dsTextoJustfConc = dsTextoJustfConc;
    } //-- void setDsTextoJustfConc(java.lang.String) 

    /**
     * Sets the value of field 'dsTextoJustfRecip'.
     * 
     * @param dsTextoJustfRecip the value of field
     * 'dsTextoJustfRecip'.
     */
    public void setDsTextoJustfRecip(java.lang.String dsTextoJustfRecip)
    {
        this._dsTextoJustfRecip = dsTextoJustfRecip;
    } //-- void setDsTextoJustfRecip(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoNecessInsta'.
     * 
     * @param dsTipoNecessInsta the value of field
     * 'dsTipoNecessInsta'.
     */
    public void setDsTipoNecessInsta(java.lang.String dsTipoNecessInsta)
    {
        this._dsTipoNecessInsta = dsTipoNecessInsta;
    } //-- void setDsTipoNecessInsta(java.lang.String) 

    /**
     * Sets the value of field 'dsUfEmprQuest'.
     * 
     * @param dsUfEmprQuest the value of field 'dsUfEmprQuest'.
     */
    public void setDsUfEmprQuest(java.lang.String dsUfEmprQuest)
    {
        this._dsUfEmprQuest = dsUfEmprQuest;
    } //-- void setDsUfEmprQuest(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nroGrupoEconmQuest'.
     * 
     * @param nroGrupoEconmQuest the value of field
     * 'nroGrupoEconmQuest'.
     */
    public void setNroGrupoEconmQuest(long nroGrupoEconmQuest)
    {
        this._nroGrupoEconmQuest = nroGrupoEconmQuest;
        this._has_nroGrupoEconmQuest = true;
    } //-- void setNroGrupoEconmQuest(long) 

    /**
     * Sets the value of field 'numConta'.
     * 
     * @param numConta the value of field 'numConta'.
     */
    public void setNumConta(int numConta)
    {
        this._numConta = numConta;
        this._has_numConta = true;
    } //-- void setNumConta(int) 

    /**
     * Sets the value of field 'numContaDigito'.
     * 
     * @param numContaDigito the value of field 'numContaDigito'.
     */
    public void setNumContaDigito(int numContaDigito)
    {
        this._numContaDigito = numContaDigito;
        this._has_numContaDigito = true;
    } //-- void setNumContaDigito(int) 

    /**
     * Sets the value of field 'numDiaCredtPrim'.
     * 
     * @param numDiaCredtPrim the value of field 'numDiaCredtPrim'.
     */
    public void setNumDiaCredtPrim(int numDiaCredtPrim)
    {
        this._numDiaCredtPrim = numDiaCredtPrim;
        this._has_numDiaCredtPrim = true;
    } //-- void setNumDiaCredtPrim(int) 

    /**
     * Sets the value of field 'numDiaCredtSeg'.
     * 
     * @param numDiaCredtSeg the value of field 'numDiaCredtSeg'.
     */
    public void setNumDiaCredtSeg(int numDiaCredtSeg)
    {
        this._numDiaCredtSeg = numDiaCredtSeg;
        this._has_numDiaCredtSeg = true;
    } //-- void setNumDiaCredtSeg(int) 

    /**
     * Sets the value of field 'numFoneEmprQuest'.
     * 
     * @param numFoneEmprQuest the value of field 'numFoneEmprQuest'
     */
    public void setNumFoneEmprQuest(long numFoneEmprQuest)
    {
        this._numFoneEmprQuest = numFoneEmprQuest;
        this._has_numFoneEmprQuest = true;
    } //-- void setNumFoneEmprQuest(long) 

    /**
     * Sets the value of field 'numFoneGerQuest'.
     * 
     * @param numFoneGerQuest the value of field 'numFoneGerQuest'.
     */
    public void setNumFoneGerQuest(long numFoneGerQuest)
    {
        this._numFoneGerQuest = numFoneGerQuest;
        this._has_numFoneGerQuest = true;
    } //-- void setNumFoneGerQuest(long) 

    /**
     * Sets the value of field 'numSequenciaContratoCta'.
     * 
     * @param numSequenciaContratoCta the value of field
     * 'numSequenciaContratoCta'.
     */
    public void setNumSequenciaContratoCta(long numSequenciaContratoCta)
    {
        this._numSequenciaContratoCta = numSequenciaContratoCta;
        this._has_numSequenciaContratoCta = true;
    } //-- void setNumSequenciaContratoCta(long) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 15)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 15");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void setOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias2: Index value '"+index+"' not in range [0.." + (_ocorrencias2List.size() - 1) + "]");
        }
        if (!(index < 10)) {
            throw new IndexOutOfBoundsException("setOcorrencias2 has a maximum of 10");
        }
        _ocorrencias2List.setElementAt(vOcorrencias2, index);
    } //-- void setOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param ocorrencias2Array
     */
    public void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2[] ocorrencias2Array)
    {
        //-- copy array
        _ocorrencias2List.removeAllElements();
        for (int i = 0; i < ocorrencias2Array.length; i++) {
            _ocorrencias2List.addElement(ocorrencias2Array[i]);
        }
    } //-- void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias2) 

    /**
     * Method setOcorrencias3
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias3
     */
    public void setOcorrencias3(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias3List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias3: Index value '"+index+"' not in range [0.." + (_ocorrencias3List.size() - 1) + "]");
        }
        if (!(index < 15)) {
            throw new IndexOutOfBoundsException("setOcorrencias3 has a maximum of 15");
        }
        _ocorrencias3List.setElementAt(vOcorrencias3, index);
    } //-- void setOcorrencias3(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3) 

    /**
     * Method setOcorrencias3
     * 
     * 
     * 
     * @param ocorrencias3Array
     */
    public void setOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3[] ocorrencias3Array)
    {
        //-- copy array
        _ocorrencias3List.removeAllElements();
        for (int i = 0; i < ocorrencias3Array.length; i++) {
            _ocorrencias3List.addElement(ocorrencias3Array[i]);
        }
    } //-- void setOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias3) 

    /**
     * Method setOcorrencias4
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias4
     */
    public void setOcorrencias4(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4 vOcorrencias4)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias4List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias4: Index value '"+index+"' not in range [0.." + (_ocorrencias4List.size() - 1) + "]");
        }
        if (!(index < 10)) {
            throw new IndexOutOfBoundsException("setOcorrencias4 has a maximum of 10");
        }
        _ocorrencias4List.setElementAt(vOcorrencias4, index);
    } //-- void setOcorrencias4(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4) 

    /**
     * Method setOcorrencias4
     * 
     * 
     * 
     * @param ocorrencias4Array
     */
    public void setOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4[] ocorrencias4Array)
    {
        //-- copy array
        _ocorrencias4List.removeAllElements();
        for (int i = 0; i < ocorrencias4Array.length; i++) {
            _ocorrencias4List.addElement(ocorrencias4Array[i]);
        }
    } //-- void setOcorrencias4(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias4) 

    /**
     * Method setOcorrencias5
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias5
     */
    public void setOcorrencias5(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 vOcorrencias5)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias5List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias5: Index value '"+index+"' not in range [0.." + (_ocorrencias5List.size() - 1) + "]");
        }
        if (!(index < 10)) {
            throw new IndexOutOfBoundsException("setOcorrencias5 has a maximum of 10");
        }
        _ocorrencias5List.setElementAt(vOcorrencias5, index);
    } //-- void setOcorrencias5(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5) 

    /**
     * Method setOcorrencias5
     * 
     * 
     * 
     * @param ocorrencias5Array
     */
    public void setOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5[] ocorrencias5Array)
    {
        //-- copy array
        _ocorrencias5List.removeAllElements();
        for (int i = 0; i < ocorrencias5Array.length; i++) {
            _ocorrencias5List.addElement(ocorrencias5Array[i]);
        }
    } //-- void setOcorrencias5(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5) 

    /**
     * Method setOcorrencias6
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias6
     */
    public void setOcorrencias6(int index, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6 vOcorrencias6)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias6List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias6: Index value '"+index+"' not in range [0.." + (_ocorrencias6List.size() - 1) + "]");
        }
        if (!(index < 10)) {
            throw new IndexOutOfBoundsException("setOcorrencias6 has a maximum of 10");
        }
        _ocorrencias6List.setElementAt(vOcorrencias6, index);
    } //-- void setOcorrencias6(int, br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6) 

    /**
     * Method setOcorrencias6
     * 
     * 
     * 
     * @param ocorrencias6Array
     */
    public void setOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6[] ocorrencias6Array)
    {
        //-- copy array
        _ocorrencias6List.removeAllElements();
        for (int i = 0; i < ocorrencias6Array.length; i++) {
            _ocorrencias6List.addElement(ocorrencias6Array[i]);
        }
    } //-- void setOcorrencias6(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias6) 

    /**
     * Sets the value of field 'pctgmPreNovAnoQuest'.
     * 
     * @param pctgmPreNovAnoQuest the value of field
     * 'pctgmPreNovAnoQuest'.
     */
    public void setPctgmPreNovAnoQuest(java.math.BigDecimal pctgmPreNovAnoQuest)
    {
        this._pctgmPreNovAnoQuest = pctgmPreNovAnoQuest;
    } //-- void setPctgmPreNovAnoQuest(java.math.BigDecimal) 

    /**
     * Sets the value of field 'qtdFuncionarioFlPgto'.
     * 
     * @param qtdFuncionarioFlPgto the value of field
     * 'qtdFuncionarioFlPgto'.
     */
    public void setQtdFuncionarioFlPgto(int qtdFuncionarioFlPgto)
    {
        this._qtdFuncionarioFlPgto = qtdFuncionarioFlPgto;
        this._has_qtdFuncionarioFlPgto = true;
    } //-- void setQtdFuncionarioFlPgto(int) 

    /**
     * Sets the value of field 'vlMediaSalarialMes'.
     * 
     * @param vlMediaSalarialMes the value of field
     * 'vlMediaSalarialMes'.
     */
    public void setVlMediaSalarialMes(java.math.BigDecimal vlMediaSalarialMes)
    {
        this._vlMediaSalarialMes = vlMediaSalarialMes;
    } //-- void setVlMediaSalarialMes(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoMes'.
     * 
     * @param vlPagamentoMes the value of field 'vlPagamentoMes'.
     */
    public void setVlPagamentoMes(java.math.BigDecimal vlPagamentoMes)
    {
        this._vlPagamentoMes = vlPagamentoMes;
    } //-- void setVlPagamentoMes(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarConvenioContaSalarioResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate()


	public ListarConvenioContaSalarioResponse(String codMensagem, String mensagem, int cdFaiMaxOcorr, int cdTurMaxOcorr,  int cdFilMaxOcorr,
			 int cdConMaxOcorr, int cdFolMaxOcorr, int cdCocMaxOcorr, long cdPssoaJuridCta, int cdTipoContrCta, long numSequenciaContratoCta,
			 int cdInstituicaoBancaria, int cdUnidadeOrganizacionalCusto, int numConta, int numContaDigito, int cdIndicadorNumConve, 
			 long cdConveCtaSalarial, BigDecimal vlPagamentoMes, int qtdFuncionarioFlPgto, BigDecimal vlMediaSalarialMes, int numDiaCredtPrim, 
			 int numDiaCredtSeg, int cdIndicadorQuestSalarial, int cdPagamentoSalarialMes, int cdPagamentoSalarialQzena, int cdLocCtaSalarial, 
			 String dsLocCtaSalarial, int dataReftFlPgto, String codIndicadorFedelize, String dsEmailEmpresa, String dsEmailAgencia, 
			 int cdIndicadorAltoTurnover, int cdIndicadorOfertaCartaoPrePago, int cdIndicadorAtivacaoAPP, String dsTextoJustfConc, 
			 String dsTextoJustfRecip,String dsTextoJustfCompl, int cdIndicadorGrpQuest, long nroGrupoEconmQuest, String dsGrupoEconmQuest, 
			 int cdUnidadeMeddAg, String dsAbrevMeddAg, int cdUdistcAgEmpr, String dsLogradouroEmprQuest, String dsBairroEmprQuest, long cdMunEmprQuest,
			 String dsMuncEmpreQuest, int cdCepEmprQuest, int cdCepComplemento, int cdUfEmpreQuest, String dsUfEmprQuest, long cdAgptoAtividadeQuest,
			 String dsRamoAtividadeQuest, int cdDiscagemDiretaInterEmpr, int cdDiscagemDiretaDistEmpr, long numFoneEmprQuest, String dsSiteEmprQuest,
			 long cdFuncGerRelacionamento, String dsGerRelacionamentoQuest, int cdDiscagemDiretaDistGer, int cdDiscagemDiretaDistanGer, long numFoneGerQuest,
			 int cdSegmentoEmprQuest, String dsAbrevSegmentoEmpr, int cdIndicadorFlQuest, String dsIndicadorFlQuest, BigDecimal pctgmPreNovAnoQuest,
			 int cdprefeCsign, int cdprefePabCren, int cdIndicadorPaeCcren, int cdIndicadorAgCcren, int cdIndicadorInstaEstrt, int cdEstrtInstaEmpr,
			 String dsEstruturaInstaEmpr, int cdbancoVincInsta, int cdAgenciaVincInsta, String dsAgenciaVincInsta, int cdUnidadeMeddInsta, 
			 String dsAbrevInsta, int cdDistcAgenciaInsta, int cdIndicadorVisitaInsta, String dsIndicadorVisitaInsta, long cdFuncGerVisitante,
			 String dsGerVisitanteInsta, int cdUnidadeMeddEspac, String dsAbrevEspacConcedido, int cdUespacConcedidoInsta, int cdTipoNecesInsta,
			 String dsTipoNecessInsta, int cdIndicadorPerdaRelacionamento, String dsIndicadorPerdarelacionamento, int cdPagamentoSalCredt,
			 int cdPagamentoSalCheque, int cdPagamentoSalDinheiro, int cdPagamentoSalDivers, String dsPagamentoSalDivers,
			 Vector ocorrenciasList, Vector ocorrencias2List, Vector ocorrencias3List, Vector ocorrencias4List, Vector ocorrencias5List, 
			 Vector ocorrencias6List) {
		super();
		_codMensagem = codMensagem;
		_mensagem = mensagem;
		_cdFaiMaxOcorr = cdFaiMaxOcorr;
		_cdTurMaxOcorr = cdTurMaxOcorr;
		_cdFilMaxOcorr = cdFilMaxOcorr;
		_cdConMaxOcorr = cdConMaxOcorr;
		_cdFolMaxOcorr = cdFolMaxOcorr;
		_cdCocMaxOcorr = cdCocMaxOcorr;
		_cdPssoaJuridCta = cdPssoaJuridCta;
		_cdTipoContrCta = cdTipoContrCta;
		_numSequenciaContratoCta = numSequenciaContratoCta;
		_cdInstituicaoBancaria = cdInstituicaoBancaria;
		_cdUnidadeOrganizacionalCusto = cdUnidadeOrganizacionalCusto;
		_numConta = numConta;
		_numContaDigito = numContaDigito;
		_cdIndicadorNumConve = cdIndicadorNumConve;
		_cdConveCtaSalarial = cdConveCtaSalarial;
		_vlPagamentoMes = vlPagamentoMes;
		_qtdFuncionarioFlPgto = qtdFuncionarioFlPgto;
		_vlMediaSalarialMes = vlMediaSalarialMes;
		_numDiaCredtPrim = numDiaCredtPrim;
		_numDiaCredtSeg = numDiaCredtSeg;
		_cdIndicadorQuestSalarial = cdIndicadorQuestSalarial;
		_cdPagamentoSalarialMes = cdPagamentoSalarialMes;
		_cdPagamentoSalarialQzena = cdPagamentoSalarialQzena;
		_cdLocCtaSalarial = cdLocCtaSalarial;
		_dsLocCtaSalarial = dsLocCtaSalarial;
		_dataReftFlPgto = dataReftFlPgto;
		_codIndicadorFedelize = codIndicadorFedelize;
		_dsEmailEmpresa = dsEmailEmpresa;
		_dsEmailAgencia = dsEmailAgencia;
		_cdIndicadorAltoTurnover = cdIndicadorAltoTurnover;
		_cdIndicadorOfertaCartaoPrePago = cdIndicadorOfertaCartaoPrePago;
		_cdIndicadorAtivacaoAPP = cdIndicadorAtivacaoAPP;
		_dsTextoJustfConc = dsTextoJustfConc;
		_dsTextoJustfRecip = dsTextoJustfRecip;
		_dsTextoJustfCompl = dsTextoJustfCompl;
		_cdIndicadorGrpQuest = cdIndicadorGrpQuest;
		_nroGrupoEconmQuest = nroGrupoEconmQuest;
		_dsGrupoEconmQuest = dsGrupoEconmQuest;
		_cdUnidadeMeddAg = cdUnidadeMeddAg;
		_dsAbrevMeddAg = dsAbrevMeddAg;
		_cdUdistcAgEmpr = cdUdistcAgEmpr;
		_dsLogradouroEmprQuest = dsLogradouroEmprQuest;
		_dsBairroEmprQuest = dsBairroEmprQuest;
		_cdMunEmprQuest = cdMunEmprQuest;
		_dsMuncEmpreQuest = dsMuncEmpreQuest;
		_cdCepEmprQuest = cdCepEmprQuest;
		_cdCepComplemento = cdCepComplemento;
		_cdUfEmpreQuest = cdUfEmpreQuest;
		_dsUfEmprQuest = dsUfEmprQuest;
		_cdAgptoAtividadeQuest = cdAgptoAtividadeQuest;
		_dsRamoAtividadeQuest = dsRamoAtividadeQuest;
		_cdDiscagemDiretaInterEmpr = cdDiscagemDiretaInterEmpr;
		_cdDiscagemDiretaDistEmpr = cdDiscagemDiretaDistEmpr;
		_numFoneEmprQuest = numFoneEmprQuest;
		_dsSiteEmprQuest = dsSiteEmprQuest;
		_cdFuncGerRelacionamento = cdFuncGerRelacionamento;
		_dsGerRelacionamentoQuest = dsGerRelacionamentoQuest;
		_cdDiscagemDiretaDistGer = cdDiscagemDiretaDistGer;
		_cdDiscagemDiretaDistanGer = cdDiscagemDiretaDistanGer;
		_numFoneGerQuest = numFoneGerQuest;
		_cdSegmentoEmprQuest = cdSegmentoEmprQuest;
		_dsAbrevSegmentoEmpr = dsAbrevSegmentoEmpr;
		_cdIndicadorFlQuest = cdIndicadorFlQuest;
		_dsIndicadorFlQuest = dsIndicadorFlQuest;
		_pctgmPreNovAnoQuest = pctgmPreNovAnoQuest;
		_cdprefeCsign = cdprefeCsign;
		_cdprefePabCren = cdprefePabCren;
		_cdIndicadorPaeCcren = cdIndicadorPaeCcren;
		_cdIndicadorAgCcren = cdIndicadorAgCcren;
		_cdIndicadorInstaEstrt = cdIndicadorInstaEstrt;
		_cdEstrtInstaEmpr = cdEstrtInstaEmpr;
		_dsEstruturaInstaEmpr = dsEstruturaInstaEmpr;
		_cdbancoVincInsta = cdbancoVincInsta;
		_cdAgenciaVincInsta = cdAgenciaVincInsta;
		_dsAgenciaVincInsta = dsAgenciaVincInsta;
		_cdUnidadeMeddInsta = cdUnidadeMeddInsta;
		_dsAbrevInsta = dsAbrevInsta;
		_cdDistcAgenciaInsta = cdDistcAgenciaInsta;
		_cdIndicadorVisitaInsta = cdIndicadorVisitaInsta;
		_dsIndicadorVisitaInsta = dsIndicadorVisitaInsta;
		_cdFuncGerVisitante = cdFuncGerVisitante;
		_dsGerVisitanteInsta = dsGerVisitanteInsta;
		_cdUnidadeMeddEspac = cdUnidadeMeddEspac;
		_dsAbrevEspacConcedido = dsAbrevEspacConcedido;
		_cdUespacConcedidoInsta = cdUespacConcedidoInsta;
		_cdTipoNecesInsta = cdTipoNecesInsta;
		_dsTipoNecessInsta = dsTipoNecessInsta;
		_cdIndicadorPerdaRelacionamento = cdIndicadorPerdaRelacionamento;
		_dsIndicadorPerdarelacionamento = dsIndicadorPerdarelacionamento;
		_cdPagamentoSalCredt = cdPagamentoSalCredt;
		_cdPagamentoSalCheque = cdPagamentoSalCheque;
		_cdPagamentoSalDinheiro = cdPagamentoSalDinheiro;
		_cdPagamentoSalDivers = cdPagamentoSalDivers;
		_dsPagamentoSalDivers = dsPagamentoSalDivers;
		_ocorrenciasList = ocorrenciasList;
		_ocorrencias2List = ocorrencias2List;
		_ocorrencias3List = ocorrencias3List;
		_ocorrencias4List = ocorrencias4List;
		_ocorrencias5List = ocorrencias5List;
		_ocorrencias6List = ocorrencias6List;
	}
    

}
