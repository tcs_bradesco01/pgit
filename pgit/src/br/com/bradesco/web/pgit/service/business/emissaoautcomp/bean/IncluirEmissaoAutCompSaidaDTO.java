/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: IncluirEmissaoAutCompSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirEmissaoAutCompSaidaDTO {
	
	/** Atributo codigoMensagem. */
	private String codigoMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/**
	 * Get: codigoMensagem.
	 *
	 * @return codigoMensagem
	 */
	public String getCodigoMensagem() {
		return codigoMensagem;
	}
	
	/**
	 * Set: codigoMensagem.
	 *
	 * @param codigoMensagem the codigo mensagem
	 */
	public void setCodigoMensagem(String codigoMensagem) {
		this.codigoMensagem = codigoMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	} 
	
	
}
