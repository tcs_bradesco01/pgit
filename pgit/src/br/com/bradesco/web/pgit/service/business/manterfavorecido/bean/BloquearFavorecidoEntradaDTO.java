/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: BloquearFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloquearFavorecidoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequencialContratoNegocio. */
    private Long nrSequencialContratoNegocio;
    
    /** Atributo cdFavoritoClientePagador. */
    private Long cdFavoritoClientePagador;
    
    /** Atributo cdSituacaoFavorecido. */
    private Integer cdSituacaoFavorecido;
    
    /** Atributo cdMotivoBloqueioFavorecido. */
    private Integer cdMotivoBloqueioFavorecido;
    
    /**
     * Bloquear favorecido entrada dto.
     */
    public BloquearFavorecidoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Get: cdFavoritoClientePagador.
	 *
	 * @return cdFavoritoClientePagador
	 */
	public Long getCdFavoritoClientePagador() {
		return cdFavoritoClientePagador;
	}
	
	/**
	 * Set: cdFavoritoClientePagador.
	 *
	 * @param cdFavoritoClientePagador the cd favorito cliente pagador
	 */
	public void setCdFavoritoClientePagador(Long cdFavoritoClientePagador) {
		this.cdFavoritoClientePagador = cdFavoritoClientePagador;
	}
	
	/**
	 * Get: cdMotivoBloqueioFavorecido.
	 *
	 * @return cdMotivoBloqueioFavorecido
	 */
	public Integer getCdMotivoBloqueioFavorecido() {
		return cdMotivoBloqueioFavorecido;
	}
	
	/**
	 * Set: cdMotivoBloqueioFavorecido.
	 *
	 * @param cdMotivoBloqueioFavorecido the cd motivo bloqueio favorecido
	 */
	public void setCdMotivoBloqueioFavorecido(Integer cdMotivoBloqueioFavorecido) {
		this.cdMotivoBloqueioFavorecido = cdMotivoBloqueioFavorecido;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoFavorecido.
	 *
	 * @return cdSituacaoFavorecido
	 */
	public Integer getCdSituacaoFavorecido() {
		return cdSituacaoFavorecido;
	}
	
	/**
	 * Set: cdSituacaoFavorecido.
	 *
	 * @param cdSituacaoFavorecido the cd situacao favorecido
	 */
	public void setCdSituacaoFavorecido(Integer cdSituacaoFavorecido) {
		this.cdSituacaoFavorecido = cdSituacaoFavorecido;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequencialContratoNegocio.
	 *
	 * @return nrSequencialContratoNegocio
	 */
	public Long getNrSequencialContratoNegocio() {
		return nrSequencialContratoNegocio;
	}
	
	/**
	 * Set: nrSequencialContratoNegocio.
	 *
	 * @param nrSequencialContratoNegocio the nr sequencial contrato negocio
	 */
	public void setNrSequencialContratoNegocio(Long nrSequencialContratoNegocio) {
		this.nrSequencialContratoNegocio = nrSequencialContratoNegocio;
	}

}
