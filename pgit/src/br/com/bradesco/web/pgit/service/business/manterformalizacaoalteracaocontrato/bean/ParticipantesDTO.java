/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

/**
 * Nome: ParticipantesDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ParticipantesDTO {

	
	/** Atributo nrCpfCnpjParticipante. */
	private String nrCpfCnpjParticipante;
	
	/** Atributo nmRazaoSocialParticipante. */
	private String nmRazaoSocialParticipante;
	
	/** Atributo dsTipoParticipantePart. */
	private String dsTipoParticipantePart;
	
	/** Atributo dsAcaoManutencaoParticipante. */
	private String dsAcaoManutencaoParticipante;
	
	/**
	 * Get: nrCpfCnpjParticipante.
	 *
	 * @return nrCpfCnpjParticipante
	 */
	public String getNrCpfCnpjParticipante() {
		return nrCpfCnpjParticipante;
	}
	
	/**
	 * Set: nrCpfCnpjParticipante.
	 *
	 * @param nrCpfCnpjParticipante the nr cpf cnpj participante
	 */
	public void setNrCpfCnpjParticipante(String nrCpfCnpjParticipante) {
		this.nrCpfCnpjParticipante = nrCpfCnpjParticipante;
	}
	
	/**
	 * Get: nmRazaoSocialParticipante.
	 *
	 * @return nmRazaoSocialParticipante
	 */
	public String getNmRazaoSocialParticipante() {
		return nmRazaoSocialParticipante;
	}
	
	/**
	 * Set: nmRazaoSocialParticipante.
	 *
	 * @param nmRazaoSocialParticipante the nm razao social participante
	 */
	public void setNmRazaoSocialParticipante(String nmRazaoSocialParticipante) {
		this.nmRazaoSocialParticipante = nmRazaoSocialParticipante;
	}
	
	/**
	 * Get: dsTipoParticipantePart.
	 *
	 * @return dsTipoParticipantePart
	 */
	public String getDsTipoParticipantePart() {
		return dsTipoParticipantePart;
	}
	
	/**
	 * Set: dsTipoParticipantePart.
	 *
	 * @param dsTipoParticipantePart the ds tipo participante part
	 */
	public void setDsTipoParticipantePart(String dsTipoParticipantePart) {
		this.dsTipoParticipantePart = dsTipoParticipantePart;
	}
	
	/**
	 * Get: dsAcaoManutencaoParticipante.
	 *
	 * @return dsAcaoManutencaoParticipante
	 */
	public String getDsAcaoManutencaoParticipante() {
		return dsAcaoManutencaoParticipante;
	}
	
	/**
	 * Set: dsAcaoManutencaoParticipante.
	 *
	 * @param dsAcaoManutencaoParticipante the ds acao manutencao participante
	 */
	public void setDsAcaoManutencaoParticipante(String dsAcaoManutencaoParticipante) {
		this.dsAcaoManutencaoParticipante = dsAcaoManutencaoParticipante;
	}
	
	
	
	
	
	
	
	
	
	
	
}
