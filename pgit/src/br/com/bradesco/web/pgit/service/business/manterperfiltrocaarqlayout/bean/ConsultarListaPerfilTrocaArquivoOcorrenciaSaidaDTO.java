/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean;


/**
 * Nome: ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO{
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo dsLayoutProprio. */
	private String dsLayoutProprio;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdRelacionamentoProduto. */
	private Integer cdRelacionamentoProduto;
    
    /** Atributo dsProdutoServico. */
    private String dsProdutoServico;
	
	/** Atributo cdAplicacaoTransmicaoPagamento. */
	private Integer cdAplicacaoTransmicaoPagamento;
    
    /** Atributo dsAplicacaoTransmicaoPagamento. */
    private String dsAplicacaoTransmicaoPagamento;
	
	/** Atributo cdMeioPrincipalRemessa. */
	private Integer cdMeioPrincipalRemessa;
    
    /** Atributo dsMeioPrincRemss. */
    private String dsMeioPrincRemss;
	
	/** Atributo cdmeioAlternativoRemessa. */
	private Integer cdmeioAlternativoRemessa;
    
    /** Atributo dsMeioAltrnRemss. */
    private String dsMeioAltrnRemss;
	
	/** Atributo cdMeioPrincipalRetorno. */
	private Integer cdMeioPrincipalRetorno;
    
    /** Atributo dsMeioPrincRetor. */
    private String dsMeioPrincRetor;
	
	/** Atributo cdMeioAlternativoRetorno. */
	private Integer cdMeioAlternativoRetorno;
    
    /** Atributo dsMeioAltrnRetor. */
    private String dsMeioAltrnRetor;
	
	/** Atributo cdPessoaJuridicaParceiro. */
	private Long cdPessoaJuridicaParceiro;
    
    /** Atributo dsRzScialPcero. */
    private String dsRzScialPcero;
	
	
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo(){
		return cdPerfilTrocaArquivo;
	}

	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo){
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: dsLayoutProprio.
	 *
	 * @return dsLayoutProprio
	 */
	public String getDsLayoutProprio() {
		return dsLayoutProprio;
	}

	/**
	 * Set: dsLayoutProprio.
	 *
	 * @param dsLayoutProprio the ds layout proprio
	 */
	public void setDsLayoutProprio(String dsLayoutProprio) {
		this.dsLayoutProprio = dsLayoutProprio;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

	/**
	 * Get: dsProdutoServico.
	 *
	 * @return dsProdutoServico
	 */
	public String getDsProdutoServico() {
		return dsProdutoServico;
	}

	/**
	 * Set: dsProdutoServico.
	 *
	 * @param dsProdutoServico the ds produto servico
	 */
	public void setDsProdutoServico(String dsProdutoServico) {
		this.dsProdutoServico = dsProdutoServico;
	}

	/**
	 * Get: cdAplicacaoTransmicaoPagamento.
	 *
	 * @return cdAplicacaoTransmicaoPagamento
	 */
	public Integer getCdAplicacaoTransmicaoPagamento() {
		return cdAplicacaoTransmicaoPagamento;
	}

	/**
	 * Set: cdAplicacaoTransmicaoPagamento.
	 *
	 * @param cdAplicacaoTransmicaoPagamento the cd aplicacao transmicao pagamento
	 */
	public void setCdAplicacaoTransmicaoPagamento(
			Integer cdAplicacaoTransmicaoPagamento) {
		this.cdAplicacaoTransmicaoPagamento = cdAplicacaoTransmicaoPagamento;
	}

	/**
	 * Get: dsAplicacaoTransmicaoPagamento.
	 *
	 * @return dsAplicacaoTransmicaoPagamento
	 */
	public String getDsAplicacaoTransmicaoPagamento() {
		return dsAplicacaoTransmicaoPagamento;
	}

	/**
	 * Set: dsAplicacaoTransmicaoPagamento.
	 *
	 * @param dsAplicacaoTransmicaoPagamento the ds aplicacao transmicao pagamento
	 */
	public void setDsAplicacaoTransmicaoPagamento(
			String dsAplicacaoTransmicaoPagamento) {
		this.dsAplicacaoTransmicaoPagamento = dsAplicacaoTransmicaoPagamento;
	}

	/**
	 * Get: cdMeioPrincipalRemessa.
	 *
	 * @return cdMeioPrincipalRemessa
	 */
	public Integer getCdMeioPrincipalRemessa() {
		return cdMeioPrincipalRemessa;
	}

	/**
	 * Set: cdMeioPrincipalRemessa.
	 *
	 * @param cdMeioPrincipalRemessa the cd meio principal remessa
	 */
	public void setCdMeioPrincipalRemessa(Integer cdMeioPrincipalRemessa) {
		this.cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
	}

	/**
	 * Get: dsMeioPrincRemss.
	 *
	 * @return dsMeioPrincRemss
	 */
	public String getDsMeioPrincRemss() {
		return dsMeioPrincRemss;
	}

	/**
	 * Set: dsMeioPrincRemss.
	 *
	 * @param dsMeioPrincRemss the ds meio princ remss
	 */
	public void setDsMeioPrincRemss(String dsMeioPrincRemss) {
		this.dsMeioPrincRemss = dsMeioPrincRemss;
	}

	/**
	 * Get: cdmeioAlternativoRemessa.
	 *
	 * @return cdmeioAlternativoRemessa
	 */
	public Integer getCdmeioAlternativoRemessa() {
		return cdmeioAlternativoRemessa;
	}

	/**
	 * Set: cdmeioAlternativoRemessa.
	 *
	 * @param cdmeioAlternativoRemessa the cdmeio alternativo remessa
	 */
	public void setCdmeioAlternativoRemessa(Integer cdmeioAlternativoRemessa) {
		this.cdmeioAlternativoRemessa = cdmeioAlternativoRemessa;
	}

	/**
	 * Get: dsMeioAltrnRemss.
	 *
	 * @return dsMeioAltrnRemss
	 */
	public String getDsMeioAltrnRemss() {
		return dsMeioAltrnRemss;
	}

	/**
	 * Set: dsMeioAltrnRemss.
	 *
	 * @param dsMeioAltrnRemss the ds meio altrn remss
	 */
	public void setDsMeioAltrnRemss(String dsMeioAltrnRemss) {
		this.dsMeioAltrnRemss = dsMeioAltrnRemss;
	}

	/**
	 * Get: cdMeioPrincipalRetorno.
	 *
	 * @return cdMeioPrincipalRetorno
	 */
	public Integer getCdMeioPrincipalRetorno() {
		return cdMeioPrincipalRetorno;
	}

	/**
	 * Set: cdMeioPrincipalRetorno.
	 *
	 * @param cdMeioPrincipalRetorno the cd meio principal retorno
	 */
	public void setCdMeioPrincipalRetorno(Integer cdMeioPrincipalRetorno) {
		this.cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
	}

	/**
	 * Get: dsMeioPrincRetor.
	 *
	 * @return dsMeioPrincRetor
	 */
	public String getDsMeioPrincRetor() {
		return dsMeioPrincRetor;
	}

	/**
	 * Set: dsMeioPrincRetor.
	 *
	 * @param dsMeioPrincRetor the ds meio princ retor
	 */
	public void setDsMeioPrincRetor(String dsMeioPrincRetor) {
		this.dsMeioPrincRetor = dsMeioPrincRetor;
	}

	/**
	 * Get: cdMeioAlternativoRetorno.
	 *
	 * @return cdMeioAlternativoRetorno
	 */
	public Integer getCdMeioAlternativoRetorno() {
		return cdMeioAlternativoRetorno;
	}

	/**
	 * Set: cdMeioAlternativoRetorno.
	 *
	 * @param cdMeioAlternativoRetorno the cd meio alternativo retorno
	 */
	public void setCdMeioAlternativoRetorno(Integer cdMeioAlternativoRetorno) {
		this.cdMeioAlternativoRetorno = cdMeioAlternativoRetorno;
	}

	/**
	 * Get: dsMeioAltrnRetor.
	 *
	 * @return dsMeioAltrnRetor
	 */
	public String getDsMeioAltrnRetor() {
		return dsMeioAltrnRetor;
	}

	/**
	 * Set: dsMeioAltrnRetor.
	 *
	 * @param dsMeioAltrnRetor the ds meio altrn retor
	 */
	public void setDsMeioAltrnRetor(String dsMeioAltrnRetor) {
		this.dsMeioAltrnRetor = dsMeioAltrnRetor;
	}

	/**
	 * Get: cdPessoaJuridicaParceiro.
	 *
	 * @return cdPessoaJuridicaParceiro
	 */
	public Long getCdPessoaJuridicaParceiro() {
		return cdPessoaJuridicaParceiro;
	}

	/**
	 * Set: cdPessoaJuridicaParceiro.
	 *
	 * @param cdPessoaJuridicaParceiro the cd pessoa juridica parceiro
	 */
	public void setCdPessoaJuridicaParceiro(Long cdPessoaJuridicaParceiro) {
		this.cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
	}

	/**
	 * Get: dsRzScialPcero.
	 *
	 * @return dsRzScialPcero
	 */
	public String getDsRzScialPcero() {
		return dsRzScialPcero;
	}

	/**
	 * Set: dsRzScialPcero.
	 *
	 * @param dsRzScialPcero the ds rz scial pcero
	 */
	public void setDsRzScialPcero(String dsRzScialPcero) {
		this.dsRzScialPcero = dsRzScialPcero;
	}
	
	
}