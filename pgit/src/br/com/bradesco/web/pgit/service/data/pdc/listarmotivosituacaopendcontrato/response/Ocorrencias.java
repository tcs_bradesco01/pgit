/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMotivoSituacaoPendencia
     */
    private int _cdMotivoSituacaoPendencia = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoPendencia
     */
    private boolean _has_cdMotivoSituacaoPendencia;

    /**
     * Field _dsMotivoSituacaoPendencia
     */
    private java.lang.String _dsMotivoSituacaoPendencia;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoSituacaoPendencia
     * 
     */
    public void deleteCdMotivoSituacaoPendencia()
    {
        this._has_cdMotivoSituacaoPendencia= false;
    } //-- void deleteCdMotivoSituacaoPendencia() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPendencia'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoPendencia'.
     */
    public int getCdMotivoSituacaoPendencia()
    {
        return this._cdMotivoSituacaoPendencia;
    } //-- int getCdMotivoSituacaoPendencia() 

    /**
     * Returns the value of field 'dsMotivoSituacaoPendencia'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoPendencia'.
     */
    public java.lang.String getDsMotivoSituacaoPendencia()
    {
        return this._dsMotivoSituacaoPendencia;
    } //-- java.lang.String getDsMotivoSituacaoPendencia() 

    /**
     * Method hasCdMotivoSituacaoPendencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoPendencia()
    {
        return this._has_cdMotivoSituacaoPendencia;
    } //-- boolean hasCdMotivoSituacaoPendencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPendencia'.
     * 
     * @param cdMotivoSituacaoPendencia the value of field
     * 'cdMotivoSituacaoPendencia'.
     */
    public void setCdMotivoSituacaoPendencia(int cdMotivoSituacaoPendencia)
    {
        this._cdMotivoSituacaoPendencia = cdMotivoSituacaoPendencia;
        this._has_cdMotivoSituacaoPendencia = true;
    } //-- void setCdMotivoSituacaoPendencia(int) 

    /**
     * Sets the value of field 'dsMotivoSituacaoPendencia'.
     * 
     * @param dsMotivoSituacaoPendencia the value of field
     * 'dsMotivoSituacaoPendencia'.
     */
    public void setDsMotivoSituacaoPendencia(java.lang.String dsMotivoSituacaoPendencia)
    {
        this._dsMotivoSituacaoPendencia = dsMotivoSituacaoPendencia;
    } //-- void setDsMotivoSituacaoPendencia(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
