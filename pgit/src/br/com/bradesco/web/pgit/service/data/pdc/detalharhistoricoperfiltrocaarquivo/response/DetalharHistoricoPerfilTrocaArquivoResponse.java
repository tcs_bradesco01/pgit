/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistoricoPerfilTrocaArquivoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistoricoPerfilTrocaArquivoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPerfilTrocaArq
     */
    private long _cdPerfilTrocaArq = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArq
     */
    private boolean _has_cdPerfilTrocaArq;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsCodTipoLayout
     */
    private java.lang.String _dsCodTipoLayout;

    /**
     * Field _hrInclusaoRegistroHist
     */
    private java.lang.String _hrInclusaoRegistroHist;

    /**
     * Field _dsAplicFormat
     */
    private java.lang.String _dsAplicFormat;

    /**
     * Field _dsNomeCliente
     */
    private java.lang.String _dsNomeCliente;

    /**
     * Field _dsNomeArquivoRemessa
     */
    private java.lang.String _dsNomeArquivoRemessa;

    /**
     * Field _dsNomeArquivoRetorno
     */
    private java.lang.String _dsNomeArquivoRetorno;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _cdRejeicaoAcltoRemessa
     */
    private int _cdRejeicaoAcltoRemessa = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAcltoRemessa
     */
    private boolean _has_cdRejeicaoAcltoRemessa;

    /**
     * Field _dsRejeicaoAcltoRemessa
     */
    private java.lang.String _dsRejeicaoAcltoRemessa;

    /**
     * Field _qtMaxIncotRemessa
     */
    private int _qtMaxIncotRemessa = 0;

    /**
     * keeps track of state for field: _qtMaxIncotRemessa
     */
    private boolean _has_qtMaxIncotRemessa;

    /**
     * Field _percentualIncotRejeiRemessa
     */
    private java.math.BigDecimal _percentualIncotRejeiRemessa = new java.math.BigDecimal("0");

    /**
     * Field _cdNivelControleRemessa
     */
    private int _cdNivelControleRemessa = 0;

    /**
     * keeps track of state for field: _cdNivelControleRemessa
     */
    private boolean _has_cdNivelControleRemessa;

    /**
     * Field _dsNivelControleRemessa
     */
    private java.lang.String _dsNivelControleRemessa;

    /**
     * Field _cdControleNumeroRemessa
     */
    private int _cdControleNumeroRemessa = 0;

    /**
     * keeps track of state for field: _cdControleNumeroRemessa
     */
    private boolean _has_cdControleNumeroRemessa;

    /**
     * Field _dsControleNumeroRemessa
     */
    private java.lang.String _dsControleNumeroRemessa;

    /**
     * Field _cdPeriodicidadeContagemRemessa
     */
    private int _cdPeriodicidadeContagemRemessa = 0;

    /**
     * keeps track of state for field:
     * _cdPeriodicidadeContagemRemessa
     */
    private boolean _has_cdPeriodicidadeContagemRemessa;

    /**
     * Field _dsPeriodicidadeContagemRemessa
     */
    private java.lang.String _dsPeriodicidadeContagemRemessa;

    /**
     * Field _nrMaxContagemRemessa
     */
    private long _nrMaxContagemRemessa = 0;

    /**
     * keeps track of state for field: _nrMaxContagemRemessa
     */
    private boolean _has_nrMaxContagemRemessa;

    /**
     * Field _cdNivelControleRetorno
     */
    private int _cdNivelControleRetorno = 0;

    /**
     * keeps track of state for field: _cdNivelControleRetorno
     */
    private boolean _has_cdNivelControleRetorno;

    /**
     * Field _dsNivelControleRetorno
     */
    private java.lang.String _dsNivelControleRetorno;

    /**
     * Field _cdControleNumeroRetorno
     */
    private int _cdControleNumeroRetorno = 0;

    /**
     * keeps track of state for field: _cdControleNumeroRetorno
     */
    private boolean _has_cdControleNumeroRetorno;

    /**
     * Field _dsControleNumeroRetorno
     */
    private java.lang.String _dsControleNumeroRetorno;

    /**
     * Field _cdPeriodicodadeContagemRetorno
     */
    private int _cdPeriodicodadeContagemRetorno = 0;

    /**
     * keeps track of state for field:
     * _cdPeriodicodadeContagemRetorno
     */
    private boolean _has_cdPeriodicodadeContagemRetorno;

    /**
     * Field _dsPeriodicodadeContagemRetorno
     */
    private java.lang.String _dsPeriodicodadeContagemRetorno;

    /**
     * Field _nrMaxContagemRetorno
     */
    private long _nrMaxContagemRetorno = 0;

    /**
     * keeps track of state for field: _nrMaxContagemRetorno
     */
    private boolean _has_nrMaxContagemRetorno;

    /**
     * Field _cdMeioPrincipalRemessa
     */
    private int _cdMeioPrincipalRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRemessa
     */
    private boolean _has_cdMeioPrincipalRemessa;

    /**
     * Field _dsCodMeioPrincipalRemessa
     */
    private java.lang.String _dsCodMeioPrincipalRemessa;

    /**
     * Field _cdMeioAlternRemessa
     */
    private int _cdMeioAlternRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioAlternRemessa
     */
    private boolean _has_cdMeioAlternRemessa;

    /**
     * Field _dsCodMeioAlternRemessa
     */
    private java.lang.String _dsCodMeioAlternRemessa;

    /**
     * Field _cdMeioPrincipalRetorno
     */
    private int _cdMeioPrincipalRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRetorno
     */
    private boolean _has_cdMeioPrincipalRetorno;

    /**
     * Field _cdCodMeioPrincipalRetorno
     */
    private java.lang.String _cdCodMeioPrincipalRetorno;

    /**
     * Field _cdMeioAltrnRetorno
     */
    private int _cdMeioAltrnRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioAltrnRetorno
     */
    private boolean _has_cdMeioAltrnRetorno;

    /**
     * Field _dsMeioAltrnRetorno
     */
    private java.lang.String _dsMeioAltrnRetorno;

    /**
     * Field _cdSerieAplicTranmicao
     */
    private java.lang.String _cdSerieAplicTranmicao;

    /**
     * Field _cdSistemaOrigemArquivo
     */
    private java.lang.String _cdSistemaOrigemArquivo;

    /**
     * Field _dsSistemaOrigemArquivo
     */
    private java.lang.String _dsSistemaOrigemArquivo;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoexterno
     */
    private java.lang.String _cdUsuarioManutencaoexterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdResponsavelCustoEmpresa
     */
    private int _cdResponsavelCustoEmpresa = 0;

    /**
     * keeps track of state for field: _cdResponsavelCustoEmpresa
     */
    private boolean _has_cdResponsavelCustoEmpresa;

    /**
     * Field _dsResponsavelCustoEmpresa
     */
    private java.lang.String _dsResponsavelCustoEmpresa;

    /**
     * Field _cdPessoaJuridicaParceiro
     */
    private long _cdPessoaJuridicaParceiro = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaParceiro
     */
    private boolean _has_cdPessoaJuridicaParceiro;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;

    /**
     * Field _pcCustoOrganizacaoTransmissao
     */
    private java.math.BigDecimal _pcCustoOrganizacaoTransmissao = new java.math.BigDecimal("0");

    /**
     * Field _cdAplicacaoTransmPagamento
     */
    private long _cdAplicacaoTransmPagamento = 0;

    /**
     * keeps track of state for field: _cdAplicacaoTransmPagamento
     */
    private boolean _has_cdAplicacaoTransmPagamento;

    /**
     * Field _dsUtilizacaoEmpresaVan
     */
    private java.lang.String _dsUtilizacaoEmpresaVan;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsNomeContatoCliente
     */
    private java.lang.String _dsNomeContatoCliente;

    /**
     * Field _cdAreaFoneCliente
     */
    private int _cdAreaFoneCliente = 0;

    /**
     * keeps track of state for field: _cdAreaFoneCliente
     */
    private boolean _has_cdAreaFoneCliente;

    /**
     * Field _cdFoneContatoCliente
     */
    private long _cdFoneContatoCliente = 0;

    /**
     * keeps track of state for field: _cdFoneContatoCliente
     */
    private boolean _has_cdFoneContatoCliente;

    /**
     * Field _cdRamalContatoCliente
     */
    private java.lang.String _cdRamalContatoCliente;

    /**
     * Field _dsEmailContatoCliente
     */
    private java.lang.String _dsEmailContatoCliente;

    /**
     * Field _dsSolicitacaoPendente
     */
    private java.lang.String _dsSolicitacaoPendente;

    /**
     * Field _cdAreaFonePend
     */
    private int _cdAreaFonePend = 0;

    /**
     * keeps track of state for field: _cdAreaFonePend
     */
    private boolean _has_cdAreaFonePend;

    /**
     * Field _cdFoneSolicitacaoPend
     */
    private long _cdFoneSolicitacaoPend = 0;

    /**
     * keeps track of state for field: _cdFoneSolicitacaoPend
     */
    private boolean _has_cdFoneSolicitacaoPend;

    /**
     * Field _cdRamalSolctPend
     */
    private java.lang.String _cdRamalSolctPend;

    /**
     * Field _dsEmailSolctPend
     */
    private java.lang.String _dsEmailSolctPend;

    /**
     * Field _qtMesRegistroTrafg
     */
    private long _qtMesRegistroTrafg = 0;

    /**
     * keeps track of state for field: _qtMesRegistroTrafg
     */
    private boolean _has_qtMesRegistroTrafg;

    /**
     * Field _dsObsGeralPerfil
     */
    private java.lang.String _dsObsGeralPerfil;

    /**
     * Field _cdIndicadorGeracaoSegmentoB
     */
    private int _cdIndicadorGeracaoSegmentoB = 0;

    /**
     * keeps track of state for field: _cdIndicadorGeracaoSegmentoB
     */
    private boolean _has_cdIndicadorGeracaoSegmentoB;

    /**
     * Field _dsSegmentoB
     */
    private java.lang.String _dsSegmentoB;

    /**
     * Field _cdIndicadorGeracaoSegmentoZ
     */
    private int _cdIndicadorGeracaoSegmentoZ = 0;

    /**
     * keeps track of state for field: _cdIndicadorGeracaoSegmentoZ
     */
    private boolean _has_cdIndicadorGeracaoSegmentoZ;

    /**
     * Field _dsSegmentoZ
     */
    private java.lang.String _dsSegmentoZ;

    /**
     * Field _cdIndicadorConsisteContaDebito
     */
    private int _cdIndicadorConsisteContaDebito = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorConsisteContaDebito
     */
    private boolean _has_cdIndicadorConsisteContaDebito;

    /**
     * Field _dsIndicadorConsisteContaDebito
     */
    private java.lang.String _dsIndicadorConsisteContaDebito;

    /**
     * Field _cdIndicadorCpfLayout
     */
    private int _cdIndicadorCpfLayout = 0;

    /**
     * keeps track of state for field: _cdIndicadorCpfLayout
     */
    private boolean _has_cdIndicadorCpfLayout;

    /**
     * Field _dsIndicadorCpfLayout
     */
    private java.lang.String _dsIndicadorCpfLayout;

    /**
     * Field _cdIndicadorAssociacaoLayout
     */
    private int _cdIndicadorAssociacaoLayout = 0;

    /**
     * keeps track of state for field: _cdIndicadorAssociacaoLayout
     */
    private boolean _has_cdIndicadorAssociacaoLayout;

    /**
     * Field _dsIndicadorAssociacaoLayout
     */
    private java.lang.String _dsIndicadorAssociacaoLayout;

    /**
     * Field _cdIndicadorContaComplementar
     */
    private int _cdIndicadorContaComplementar = 0;

    /**
     * keeps track of state for field: _cdIndicadorContaComplementar
     */
    private boolean _has_cdIndicadorContaComplementar;

    /**
     * Field _dsIndicadorContaComplementar
     */
    private java.lang.String _dsIndicadorContaComplementar;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistoricoPerfilTrocaArquivoResponse() 
     {
        super();
        setPercentualIncotRejeiRemessa(new java.math.BigDecimal("0"));
        setPcCustoOrganizacaoTransmissao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.response.DetalharHistoricoPerfilTrocaArquivoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplicacaoTransmPagamento
     * 
     */
    public void deleteCdAplicacaoTransmPagamento()
    {
        this._has_cdAplicacaoTransmPagamento= false;
    } //-- void deleteCdAplicacaoTransmPagamento() 

    /**
     * Method deleteCdAreaFoneCliente
     * 
     */
    public void deleteCdAreaFoneCliente()
    {
        this._has_cdAreaFoneCliente= false;
    } //-- void deleteCdAreaFoneCliente() 

    /**
     * Method deleteCdAreaFonePend
     * 
     */
    public void deleteCdAreaFonePend()
    {
        this._has_cdAreaFonePend= false;
    } //-- void deleteCdAreaFonePend() 

    /**
     * Method deleteCdControleNumeroRemessa
     * 
     */
    public void deleteCdControleNumeroRemessa()
    {
        this._has_cdControleNumeroRemessa= false;
    } //-- void deleteCdControleNumeroRemessa() 

    /**
     * Method deleteCdControleNumeroRetorno
     * 
     */
    public void deleteCdControleNumeroRetorno()
    {
        this._has_cdControleNumeroRetorno= false;
    } //-- void deleteCdControleNumeroRetorno() 

    /**
     * Method deleteCdFoneContatoCliente
     * 
     */
    public void deleteCdFoneContatoCliente()
    {
        this._has_cdFoneContatoCliente= false;
    } //-- void deleteCdFoneContatoCliente() 

    /**
     * Method deleteCdFoneSolicitacaoPend
     * 
     */
    public void deleteCdFoneSolicitacaoPend()
    {
        this._has_cdFoneSolicitacaoPend= false;
    } //-- void deleteCdFoneSolicitacaoPend() 

    /**
     * Method deleteCdIndicadorAssociacaoLayout
     * 
     */
    public void deleteCdIndicadorAssociacaoLayout()
    {
        this._has_cdIndicadorAssociacaoLayout= false;
    } //-- void deleteCdIndicadorAssociacaoLayout() 

    /**
     * Method deleteCdIndicadorConsisteContaDebito
     * 
     */
    public void deleteCdIndicadorConsisteContaDebito()
    {
        this._has_cdIndicadorConsisteContaDebito= false;
    } //-- void deleteCdIndicadorConsisteContaDebito() 

    /**
     * Method deleteCdIndicadorContaComplementar
     * 
     */
    public void deleteCdIndicadorContaComplementar()
    {
        this._has_cdIndicadorContaComplementar= false;
    } //-- void deleteCdIndicadorContaComplementar() 

    /**
     * Method deleteCdIndicadorCpfLayout
     * 
     */
    public void deleteCdIndicadorCpfLayout()
    {
        this._has_cdIndicadorCpfLayout= false;
    } //-- void deleteCdIndicadorCpfLayout() 

    /**
     * Method deleteCdIndicadorGeracaoSegmentoB
     * 
     */
    public void deleteCdIndicadorGeracaoSegmentoB()
    {
        this._has_cdIndicadorGeracaoSegmentoB= false;
    } //-- void deleteCdIndicadorGeracaoSegmentoB() 

    /**
     * Method deleteCdIndicadorGeracaoSegmentoZ
     * 
     */
    public void deleteCdIndicadorGeracaoSegmentoZ()
    {
        this._has_cdIndicadorGeracaoSegmentoZ= false;
    } //-- void deleteCdIndicadorGeracaoSegmentoZ() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdMeioAlternRemessa
     * 
     */
    public void deleteCdMeioAlternRemessa()
    {
        this._has_cdMeioAlternRemessa= false;
    } //-- void deleteCdMeioAlternRemessa() 

    /**
     * Method deleteCdMeioAltrnRetorno
     * 
     */
    public void deleteCdMeioAltrnRetorno()
    {
        this._has_cdMeioAltrnRetorno= false;
    } //-- void deleteCdMeioAltrnRetorno() 

    /**
     * Method deleteCdMeioPrincipalRemessa
     * 
     */
    public void deleteCdMeioPrincipalRemessa()
    {
        this._has_cdMeioPrincipalRemessa= false;
    } //-- void deleteCdMeioPrincipalRemessa() 

    /**
     * Method deleteCdMeioPrincipalRetorno
     * 
     */
    public void deleteCdMeioPrincipalRetorno()
    {
        this._has_cdMeioPrincipalRetorno= false;
    } //-- void deleteCdMeioPrincipalRetorno() 

    /**
     * Method deleteCdNivelControleRemessa
     * 
     */
    public void deleteCdNivelControleRemessa()
    {
        this._has_cdNivelControleRemessa= false;
    } //-- void deleteCdNivelControleRemessa() 

    /**
     * Method deleteCdNivelControleRetorno
     * 
     */
    public void deleteCdNivelControleRetorno()
    {
        this._has_cdNivelControleRetorno= false;
    } //-- void deleteCdNivelControleRetorno() 

    /**
     * Method deleteCdPerfilTrocaArq
     * 
     */
    public void deleteCdPerfilTrocaArq()
    {
        this._has_cdPerfilTrocaArq= false;
    } //-- void deleteCdPerfilTrocaArq() 

    /**
     * Method deleteCdPeriodicidadeContagemRemessa
     * 
     */
    public void deleteCdPeriodicidadeContagemRemessa()
    {
        this._has_cdPeriodicidadeContagemRemessa= false;
    } //-- void deleteCdPeriodicidadeContagemRemessa() 

    /**
     * Method deleteCdPeriodicodadeContagemRetorno
     * 
     */
    public void deleteCdPeriodicodadeContagemRetorno()
    {
        this._has_cdPeriodicodadeContagemRetorno= false;
    } //-- void deleteCdPeriodicodadeContagemRetorno() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaParceiro
     * 
     */
    public void deleteCdPessoaJuridicaParceiro()
    {
        this._has_cdPessoaJuridicaParceiro= false;
    } //-- void deleteCdPessoaJuridicaParceiro() 

    /**
     * Method deleteCdRejeicaoAcltoRemessa
     * 
     */
    public void deleteCdRejeicaoAcltoRemessa()
    {
        this._has_cdRejeicaoAcltoRemessa= false;
    } //-- void deleteCdRejeicaoAcltoRemessa() 

    /**
     * Method deleteCdResponsavelCustoEmpresa
     * 
     */
    public void deleteCdResponsavelCustoEmpresa()
    {
        this._has_cdResponsavelCustoEmpresa= false;
    } //-- void deleteCdResponsavelCustoEmpresa() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNrMaxContagemRemessa
     * 
     */
    public void deleteNrMaxContagemRemessa()
    {
        this._has_nrMaxContagemRemessa= false;
    } //-- void deleteNrMaxContagemRemessa() 

    /**
     * Method deleteNrMaxContagemRetorno
     * 
     */
    public void deleteNrMaxContagemRetorno()
    {
        this._has_nrMaxContagemRetorno= false;
    } //-- void deleteNrMaxContagemRetorno() 

    /**
     * Method deleteQtMaxIncotRemessa
     * 
     */
    public void deleteQtMaxIncotRemessa()
    {
        this._has_qtMaxIncotRemessa= false;
    } //-- void deleteQtMaxIncotRemessa() 

    /**
     * Method deleteQtMesRegistroTrafg
     * 
     */
    public void deleteQtMesRegistroTrafg()
    {
        this._has_qtMesRegistroTrafg= false;
    } //-- void deleteQtMesRegistroTrafg() 

    /**
     * Returns the value of field 'cdAplicacaoTransmPagamento'.
     * 
     * @return long
     * @return the value of field 'cdAplicacaoTransmPagamento'.
     */
    public long getCdAplicacaoTransmPagamento()
    {
        return this._cdAplicacaoTransmPagamento;
    } //-- long getCdAplicacaoTransmPagamento() 

    /**
     * Returns the value of field 'cdAreaFoneCliente'.
     * 
     * @return int
     * @return the value of field 'cdAreaFoneCliente'.
     */
    public int getCdAreaFoneCliente()
    {
        return this._cdAreaFoneCliente;
    } //-- int getCdAreaFoneCliente() 

    /**
     * Returns the value of field 'cdAreaFonePend'.
     * 
     * @return int
     * @return the value of field 'cdAreaFonePend'.
     */
    public int getCdAreaFonePend()
    {
        return this._cdAreaFonePend;
    } //-- int getCdAreaFonePend() 

    /**
     * Returns the value of field 'cdCodMeioPrincipalRetorno'.
     * 
     * @return String
     * @return the value of field 'cdCodMeioPrincipalRetorno'.
     */
    public java.lang.String getCdCodMeioPrincipalRetorno()
    {
        return this._cdCodMeioPrincipalRetorno;
    } //-- java.lang.String getCdCodMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdControleNumeroRemessa'.
     * 
     * @return int
     * @return the value of field 'cdControleNumeroRemessa'.
     */
    public int getCdControleNumeroRemessa()
    {
        return this._cdControleNumeroRemessa;
    } //-- int getCdControleNumeroRemessa() 

    /**
     * Returns the value of field 'cdControleNumeroRetorno'.
     * 
     * @return int
     * @return the value of field 'cdControleNumeroRetorno'.
     */
    public int getCdControleNumeroRetorno()
    {
        return this._cdControleNumeroRetorno;
    } //-- int getCdControleNumeroRetorno() 

    /**
     * Returns the value of field 'cdFoneContatoCliente'.
     * 
     * @return long
     * @return the value of field 'cdFoneContatoCliente'.
     */
    public long getCdFoneContatoCliente()
    {
        return this._cdFoneContatoCliente;
    } //-- long getCdFoneContatoCliente() 

    /**
     * Returns the value of field 'cdFoneSolicitacaoPend'.
     * 
     * @return long
     * @return the value of field 'cdFoneSolicitacaoPend'.
     */
    public long getCdFoneSolicitacaoPend()
    {
        return this._cdFoneSolicitacaoPend;
    } //-- long getCdFoneSolicitacaoPend() 

    /**
     * Returns the value of field 'cdIndicadorAssociacaoLayout'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAssociacaoLayout'.
     */
    public int getCdIndicadorAssociacaoLayout()
    {
        return this._cdIndicadorAssociacaoLayout;
    } //-- int getCdIndicadorAssociacaoLayout() 

    /**
     * Returns the value of field 'cdIndicadorConsisteContaDebito'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorConsisteContaDebito'.
     */
    public int getCdIndicadorConsisteContaDebito()
    {
        return this._cdIndicadorConsisteContaDebito;
    } //-- int getCdIndicadorConsisteContaDebito() 

    /**
     * Returns the value of field 'cdIndicadorContaComplementar'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorContaComplementar'.
     */
    public int getCdIndicadorContaComplementar()
    {
        return this._cdIndicadorContaComplementar;
    } //-- int getCdIndicadorContaComplementar() 

    /**
     * Returns the value of field 'cdIndicadorCpfLayout'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorCpfLayout'.
     */
    public int getCdIndicadorCpfLayout()
    {
        return this._cdIndicadorCpfLayout;
    } //-- int getCdIndicadorCpfLayout() 

    /**
     * Returns the value of field 'cdIndicadorGeracaoSegmentoB'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorGeracaoSegmentoB'.
     */
    public int getCdIndicadorGeracaoSegmentoB()
    {
        return this._cdIndicadorGeracaoSegmentoB;
    } //-- int getCdIndicadorGeracaoSegmentoB() 

    /**
     * Returns the value of field 'cdIndicadorGeracaoSegmentoZ'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorGeracaoSegmentoZ'.
     */
    public int getCdIndicadorGeracaoSegmentoZ()
    {
        return this._cdIndicadorGeracaoSegmentoZ;
    } //-- int getCdIndicadorGeracaoSegmentoZ() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdMeioAlternRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternRemessa'.
     */
    public int getCdMeioAlternRemessa()
    {
        return this._cdMeioAlternRemessa;
    } //-- int getCdMeioAlternRemessa() 

    /**
     * Returns the value of field 'cdMeioAltrnRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioAltrnRetorno'.
     */
    public int getCdMeioAltrnRetorno()
    {
        return this._cdMeioAltrnRetorno;
    } //-- int getCdMeioAltrnRetorno() 

    /**
     * Returns the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRemessa'.
     */
    public int getCdMeioPrincipalRemessa()
    {
        return this._cdMeioPrincipalRemessa;
    } //-- int getCdMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRetorno'.
     */
    public int getCdMeioPrincipalRetorno()
    {
        return this._cdMeioPrincipalRetorno;
    } //-- int getCdMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdNivelControleRemessa'.
     * 
     * @return int
     * @return the value of field 'cdNivelControleRemessa'.
     */
    public int getCdNivelControleRemessa()
    {
        return this._cdNivelControleRemessa;
    } //-- int getCdNivelControleRemessa() 

    /**
     * Returns the value of field 'cdNivelControleRetorno'.
     * 
     * @return int
     * @return the value of field 'cdNivelControleRetorno'.
     */
    public int getCdNivelControleRetorno()
    {
        return this._cdNivelControleRetorno;
    } //-- int getCdNivelControleRetorno() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPerfilTrocaArq'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArq'.
     */
    public long getCdPerfilTrocaArq()
    {
        return this._cdPerfilTrocaArq;
    } //-- long getCdPerfilTrocaArq() 

    /**
     * Returns the value of field 'cdPeriodicidadeContagemRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicidadeContagemRemessa'.
     */
    public int getCdPeriodicidadeContagemRemessa()
    {
        return this._cdPeriodicidadeContagemRemessa;
    } //-- int getCdPeriodicidadeContagemRemessa() 

    /**
     * Returns the value of field 'cdPeriodicodadeContagemRetorno'.
     * 
     * @return int
     * @return the value of field 'cdPeriodicodadeContagemRetorno'.
     */
    public int getCdPeriodicodadeContagemRetorno()
    {
        return this._cdPeriodicodadeContagemRetorno;
    } //-- int getCdPeriodicodadeContagemRetorno() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaParceiro'.
     */
    public long getCdPessoaJuridicaParceiro()
    {
        return this._cdPessoaJuridicaParceiro;
    } //-- long getCdPessoaJuridicaParceiro() 

    /**
     * Returns the value of field 'cdRamalContatoCliente'.
     * 
     * @return String
     * @return the value of field 'cdRamalContatoCliente'.
     */
    public java.lang.String getCdRamalContatoCliente()
    {
        return this._cdRamalContatoCliente;
    } //-- java.lang.String getCdRamalContatoCliente() 

    /**
     * Returns the value of field 'cdRamalSolctPend'.
     * 
     * @return String
     * @return the value of field 'cdRamalSolctPend'.
     */
    public java.lang.String getCdRamalSolctPend()
    {
        return this._cdRamalSolctPend;
    } //-- java.lang.String getCdRamalSolctPend() 

    /**
     * Returns the value of field 'cdRejeicaoAcltoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAcltoRemessa'.
     */
    public int getCdRejeicaoAcltoRemessa()
    {
        return this._cdRejeicaoAcltoRemessa;
    } //-- int getCdRejeicaoAcltoRemessa() 

    /**
     * Returns the value of field 'cdResponsavelCustoEmpresa'.
     * 
     * @return int
     * @return the value of field 'cdResponsavelCustoEmpresa'.
     */
    public int getCdResponsavelCustoEmpresa()
    {
        return this._cdResponsavelCustoEmpresa;
    } //-- int getCdResponsavelCustoEmpresa() 

    /**
     * Returns the value of field 'cdSerieAplicTranmicao'.
     * 
     * @return String
     * @return the value of field 'cdSerieAplicTranmicao'.
     */
    public java.lang.String getCdSerieAplicTranmicao()
    {
        return this._cdSerieAplicTranmicao;
    } //-- java.lang.String getCdSerieAplicTranmicao() 

    /**
     * Returns the value of field 'cdSistemaOrigemArquivo'.
     * 
     * @return String
     * @return the value of field 'cdSistemaOrigemArquivo'.
     */
    public java.lang.String getCdSistemaOrigemArquivo()
    {
        return this._cdSistemaOrigemArquivo;
    } //-- java.lang.String getCdSistemaOrigemArquivo() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoexterno'.
     */
    public java.lang.String getCdUsuarioManutencaoexterno()
    {
        return this._cdUsuarioManutencaoexterno;
    } //-- java.lang.String getCdUsuarioManutencaoexterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAplicFormat'.
     * 
     * @return String
     * @return the value of field 'dsAplicFormat'.
     */
    public java.lang.String getDsAplicFormat()
    {
        return this._dsAplicFormat;
    } //-- java.lang.String getDsAplicFormat() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCodMeioAlternRemessa'.
     * 
     * @return String
     * @return the value of field 'dsCodMeioAlternRemessa'.
     */
    public java.lang.String getDsCodMeioAlternRemessa()
    {
        return this._dsCodMeioAlternRemessa;
    } //-- java.lang.String getDsCodMeioAlternRemessa() 

    /**
     * Returns the value of field 'dsCodMeioPrincipalRemessa'.
     * 
     * @return String
     * @return the value of field 'dsCodMeioPrincipalRemessa'.
     */
    public java.lang.String getDsCodMeioPrincipalRemessa()
    {
        return this._dsCodMeioPrincipalRemessa;
    } //-- java.lang.String getDsCodMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'dsCodTipoLayout'.
     * 
     * @return String
     * @return the value of field 'dsCodTipoLayout'.
     */
    public java.lang.String getDsCodTipoLayout()
    {
        return this._dsCodTipoLayout;
    } //-- java.lang.String getDsCodTipoLayout() 

    /**
     * Returns the value of field 'dsControleNumeroRemessa'.
     * 
     * @return String
     * @return the value of field 'dsControleNumeroRemessa'.
     */
    public java.lang.String getDsControleNumeroRemessa()
    {
        return this._dsControleNumeroRemessa;
    } //-- java.lang.String getDsControleNumeroRemessa() 

    /**
     * Returns the value of field 'dsControleNumeroRetorno'.
     * 
     * @return String
     * @return the value of field 'dsControleNumeroRetorno'.
     */
    public java.lang.String getDsControleNumeroRetorno()
    {
        return this._dsControleNumeroRetorno;
    } //-- java.lang.String getDsControleNumeroRetorno() 

    /**
     * Returns the value of field 'dsEmailContatoCliente'.
     * 
     * @return String
     * @return the value of field 'dsEmailContatoCliente'.
     */
    public java.lang.String getDsEmailContatoCliente()
    {
        return this._dsEmailContatoCliente;
    } //-- java.lang.String getDsEmailContatoCliente() 

    /**
     * Returns the value of field 'dsEmailSolctPend'.
     * 
     * @return String
     * @return the value of field 'dsEmailSolctPend'.
     */
    public java.lang.String getDsEmailSolctPend()
    {
        return this._dsEmailSolctPend;
    } //-- java.lang.String getDsEmailSolctPend() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsIndicadorAssociacaoLayout'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAssociacaoLayout'.
     */
    public java.lang.String getDsIndicadorAssociacaoLayout()
    {
        return this._dsIndicadorAssociacaoLayout;
    } //-- java.lang.String getDsIndicadorAssociacaoLayout() 

    /**
     * Returns the value of field 'dsIndicadorConsisteContaDebito'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorConsisteContaDebito'.
     */
    public java.lang.String getDsIndicadorConsisteContaDebito()
    {
        return this._dsIndicadorConsisteContaDebito;
    } //-- java.lang.String getDsIndicadorConsisteContaDebito() 

    /**
     * Returns the value of field 'dsIndicadorContaComplementar'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorContaComplementar'.
     */
    public java.lang.String getDsIndicadorContaComplementar()
    {
        return this._dsIndicadorContaComplementar;
    } //-- java.lang.String getDsIndicadorContaComplementar() 

    /**
     * Returns the value of field 'dsIndicadorCpfLayout'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorCpfLayout'.
     */
    public java.lang.String getDsIndicadorCpfLayout()
    {
        return this._dsIndicadorCpfLayout;
    } //-- java.lang.String getDsIndicadorCpfLayout() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsMeioAltrnRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMeioAltrnRetorno'.
     */
    public java.lang.String getDsMeioAltrnRetorno()
    {
        return this._dsMeioAltrnRetorno;
    } //-- java.lang.String getDsMeioAltrnRetorno() 

    /**
     * Returns the value of field 'dsNivelControleRemessa'.
     * 
     * @return String
     * @return the value of field 'dsNivelControleRemessa'.
     */
    public java.lang.String getDsNivelControleRemessa()
    {
        return this._dsNivelControleRemessa;
    } //-- java.lang.String getDsNivelControleRemessa() 

    /**
     * Returns the value of field 'dsNivelControleRetorno'.
     * 
     * @return String
     * @return the value of field 'dsNivelControleRetorno'.
     */
    public java.lang.String getDsNivelControleRetorno()
    {
        return this._dsNivelControleRetorno;
    } //-- java.lang.String getDsNivelControleRetorno() 

    /**
     * Returns the value of field 'dsNomeArquivoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRemessa'.
     */
    public java.lang.String getDsNomeArquivoRemessa()
    {
        return this._dsNomeArquivoRemessa;
    } //-- java.lang.String getDsNomeArquivoRemessa() 

    /**
     * Returns the value of field 'dsNomeArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRetorno'.
     */
    public java.lang.String getDsNomeArquivoRetorno()
    {
        return this._dsNomeArquivoRetorno;
    } //-- java.lang.String getDsNomeArquivoRetorno() 

    /**
     * Returns the value of field 'dsNomeCliente'.
     * 
     * @return String
     * @return the value of field 'dsNomeCliente'.
     */
    public java.lang.String getDsNomeCliente()
    {
        return this._dsNomeCliente;
    } //-- java.lang.String getDsNomeCliente() 

    /**
     * Returns the value of field 'dsNomeContatoCliente'.
     * 
     * @return String
     * @return the value of field 'dsNomeContatoCliente'.
     */
    public java.lang.String getDsNomeContatoCliente()
    {
        return this._dsNomeContatoCliente;
    } //-- java.lang.String getDsNomeContatoCliente() 

    /**
     * Returns the value of field 'dsObsGeralPerfil'.
     * 
     * @return String
     * @return the value of field 'dsObsGeralPerfil'.
     */
    public java.lang.String getDsObsGeralPerfil()
    {
        return this._dsObsGeralPerfil;
    } //-- java.lang.String getDsObsGeralPerfil() 

    /**
     * Returns the value of field 'dsPeriodicidadeContagemRemessa'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicidadeContagemRemessa'.
     */
    public java.lang.String getDsPeriodicidadeContagemRemessa()
    {
        return this._dsPeriodicidadeContagemRemessa;
    } //-- java.lang.String getDsPeriodicidadeContagemRemessa() 

    /**
     * Returns the value of field 'dsPeriodicodadeContagemRetorno'.
     * 
     * @return String
     * @return the value of field 'dsPeriodicodadeContagemRetorno'.
     */
    public java.lang.String getDsPeriodicodadeContagemRetorno()
    {
        return this._dsPeriodicodadeContagemRetorno;
    } //-- java.lang.String getDsPeriodicodadeContagemRetorno() 

    /**
     * Returns the value of field 'dsRejeicaoAcltoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoAcltoRemessa'.
     */
    public java.lang.String getDsRejeicaoAcltoRemessa()
    {
        return this._dsRejeicaoAcltoRemessa;
    } //-- java.lang.String getDsRejeicaoAcltoRemessa() 

    /**
     * Returns the value of field 'dsResponsavelCustoEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsResponsavelCustoEmpresa'.
     */
    public java.lang.String getDsResponsavelCustoEmpresa()
    {
        return this._dsResponsavelCustoEmpresa;
    } //-- java.lang.String getDsResponsavelCustoEmpresa() 

    /**
     * Returns the value of field 'dsSegmentoB'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoB'.
     */
    public java.lang.String getDsSegmentoB()
    {
        return this._dsSegmentoB;
    } //-- java.lang.String getDsSegmentoB() 

    /**
     * Returns the value of field 'dsSegmentoZ'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoZ'.
     */
    public java.lang.String getDsSegmentoZ()
    {
        return this._dsSegmentoZ;
    } //-- java.lang.String getDsSegmentoZ() 

    /**
     * Returns the value of field 'dsSistemaOrigemArquivo'.
     * 
     * @return String
     * @return the value of field 'dsSistemaOrigemArquivo'.
     */
    public java.lang.String getDsSistemaOrigemArquivo()
    {
        return this._dsSistemaOrigemArquivo;
    } //-- java.lang.String getDsSistemaOrigemArquivo() 

    /**
     * Returns the value of field 'dsSolicitacaoPendente'.
     * 
     * @return String
     * @return the value of field 'dsSolicitacaoPendente'.
     */
    public java.lang.String getDsSolicitacaoPendente()
    {
        return this._dsSolicitacaoPendente;
    } //-- java.lang.String getDsSolicitacaoPendente() 

    /**
     * Returns the value of field 'dsUtilizacaoEmpresaVan'.
     * 
     * @return String
     * @return the value of field 'dsUtilizacaoEmpresaVan'.
     */
    public java.lang.String getDsUtilizacaoEmpresaVan()
    {
        return this._dsUtilizacaoEmpresaVan;
    } //-- java.lang.String getDsUtilizacaoEmpresaVan() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHist'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHist'.
     */
    public java.lang.String getHrInclusaoRegistroHist()
    {
        return this._hrInclusaoRegistroHist;
    } //-- java.lang.String getHrInclusaoRegistroHist() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrMaxContagemRemessa'.
     * 
     * @return long
     * @return the value of field 'nrMaxContagemRemessa'.
     */
    public long getNrMaxContagemRemessa()
    {
        return this._nrMaxContagemRemessa;
    } //-- long getNrMaxContagemRemessa() 

    /**
     * Returns the value of field 'nrMaxContagemRetorno'.
     * 
     * @return long
     * @return the value of field 'nrMaxContagemRetorno'.
     */
    public long getNrMaxContagemRetorno()
    {
        return this._nrMaxContagemRetorno;
    } //-- long getNrMaxContagemRetorno() 

    /**
     * Returns the value of field 'pcCustoOrganizacaoTransmissao'.
     * 
     * @return BigDecimal
     * @return the value of field 'pcCustoOrganizacaoTransmissao'.
     */
    public java.math.BigDecimal getPcCustoOrganizacaoTransmissao()
    {
        return this._pcCustoOrganizacaoTransmissao;
    } //-- java.math.BigDecimal getPcCustoOrganizacaoTransmissao() 

    /**
     * Returns the value of field 'percentualIncotRejeiRemessa'.
     * 
     * @return BigDecimal
     * @return the value of field 'percentualIncotRejeiRemessa'.
     */
    public java.math.BigDecimal getPercentualIncotRejeiRemessa()
    {
        return this._percentualIncotRejeiRemessa;
    } //-- java.math.BigDecimal getPercentualIncotRejeiRemessa() 

    /**
     * Returns the value of field 'qtMaxIncotRemessa'.
     * 
     * @return int
     * @return the value of field 'qtMaxIncotRemessa'.
     */
    public int getQtMaxIncotRemessa()
    {
        return this._qtMaxIncotRemessa;
    } //-- int getQtMaxIncotRemessa() 

    /**
     * Returns the value of field 'qtMesRegistroTrafg'.
     * 
     * @return long
     * @return the value of field 'qtMesRegistroTrafg'.
     */
    public long getQtMesRegistroTrafg()
    {
        return this._qtMesRegistroTrafg;
    } //-- long getQtMesRegistroTrafg() 

    /**
     * Method hasCdAplicacaoTransmPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplicacaoTransmPagamento()
    {
        return this._has_cdAplicacaoTransmPagamento;
    } //-- boolean hasCdAplicacaoTransmPagamento() 

    /**
     * Method hasCdAreaFoneCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFoneCliente()
    {
        return this._has_cdAreaFoneCliente;
    } //-- boolean hasCdAreaFoneCliente() 

    /**
     * Method hasCdAreaFonePend
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFonePend()
    {
        return this._has_cdAreaFonePend;
    } //-- boolean hasCdAreaFonePend() 

    /**
     * Method hasCdControleNumeroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleNumeroRemessa()
    {
        return this._has_cdControleNumeroRemessa;
    } //-- boolean hasCdControleNumeroRemessa() 

    /**
     * Method hasCdControleNumeroRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleNumeroRetorno()
    {
        return this._has_cdControleNumeroRetorno;
    } //-- boolean hasCdControleNumeroRetorno() 

    /**
     * Method hasCdFoneContatoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFoneContatoCliente()
    {
        return this._has_cdFoneContatoCliente;
    } //-- boolean hasCdFoneContatoCliente() 

    /**
     * Method hasCdFoneSolicitacaoPend
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFoneSolicitacaoPend()
    {
        return this._has_cdFoneSolicitacaoPend;
    } //-- boolean hasCdFoneSolicitacaoPend() 

    /**
     * Method hasCdIndicadorAssociacaoLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAssociacaoLayout()
    {
        return this._has_cdIndicadorAssociacaoLayout;
    } //-- boolean hasCdIndicadorAssociacaoLayout() 

    /**
     * Method hasCdIndicadorConsisteContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorConsisteContaDebito()
    {
        return this._has_cdIndicadorConsisteContaDebito;
    } //-- boolean hasCdIndicadorConsisteContaDebito() 

    /**
     * Method hasCdIndicadorContaComplementar
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorContaComplementar()
    {
        return this._has_cdIndicadorContaComplementar;
    } //-- boolean hasCdIndicadorContaComplementar() 

    /**
     * Method hasCdIndicadorCpfLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorCpfLayout()
    {
        return this._has_cdIndicadorCpfLayout;
    } //-- boolean hasCdIndicadorCpfLayout() 

    /**
     * Method hasCdIndicadorGeracaoSegmentoB
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorGeracaoSegmentoB()
    {
        return this._has_cdIndicadorGeracaoSegmentoB;
    } //-- boolean hasCdIndicadorGeracaoSegmentoB() 

    /**
     * Method hasCdIndicadorGeracaoSegmentoZ
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorGeracaoSegmentoZ()
    {
        return this._has_cdIndicadorGeracaoSegmentoZ;
    } //-- boolean hasCdIndicadorGeracaoSegmentoZ() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdMeioAlternRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternRemessa()
    {
        return this._has_cdMeioAlternRemessa;
    } //-- boolean hasCdMeioAlternRemessa() 

    /**
     * Method hasCdMeioAltrnRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAltrnRetorno()
    {
        return this._has_cdMeioAltrnRetorno;
    } //-- boolean hasCdMeioAltrnRetorno() 

    /**
     * Method hasCdMeioPrincipalRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRemessa()
    {
        return this._has_cdMeioPrincipalRemessa;
    } //-- boolean hasCdMeioPrincipalRemessa() 

    /**
     * Method hasCdMeioPrincipalRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRetorno()
    {
        return this._has_cdMeioPrincipalRetorno;
    } //-- boolean hasCdMeioPrincipalRetorno() 

    /**
     * Method hasCdNivelControleRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelControleRemessa()
    {
        return this._has_cdNivelControleRemessa;
    } //-- boolean hasCdNivelControleRemessa() 

    /**
     * Method hasCdNivelControleRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelControleRetorno()
    {
        return this._has_cdNivelControleRetorno;
    } //-- boolean hasCdNivelControleRetorno() 

    /**
     * Method hasCdPerfilTrocaArq
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArq()
    {
        return this._has_cdPerfilTrocaArq;
    } //-- boolean hasCdPerfilTrocaArq() 

    /**
     * Method hasCdPeriodicidadeContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicidadeContagemRemessa()
    {
        return this._has_cdPeriodicidadeContagemRemessa;
    } //-- boolean hasCdPeriodicidadeContagemRemessa() 

    /**
     * Method hasCdPeriodicodadeContagemRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPeriodicodadeContagemRetorno()
    {
        return this._has_cdPeriodicodadeContagemRetorno;
    } //-- boolean hasCdPeriodicodadeContagemRetorno() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaParceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaParceiro()
    {
        return this._has_cdPessoaJuridicaParceiro;
    } //-- boolean hasCdPessoaJuridicaParceiro() 

    /**
     * Method hasCdRejeicaoAcltoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAcltoRemessa()
    {
        return this._has_cdRejeicaoAcltoRemessa;
    } //-- boolean hasCdRejeicaoAcltoRemessa() 

    /**
     * Method hasCdResponsavelCustoEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdResponsavelCustoEmpresa()
    {
        return this._has_cdResponsavelCustoEmpresa;
    } //-- boolean hasCdResponsavelCustoEmpresa() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNrMaxContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMaxContagemRemessa()
    {
        return this._has_nrMaxContagemRemessa;
    } //-- boolean hasNrMaxContagemRemessa() 

    /**
     * Method hasNrMaxContagemRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMaxContagemRetorno()
    {
        return this._has_nrMaxContagemRetorno;
    } //-- boolean hasNrMaxContagemRetorno() 

    /**
     * Method hasQtMaxIncotRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMaxIncotRemessa()
    {
        return this._has_qtMaxIncotRemessa;
    } //-- boolean hasQtMaxIncotRemessa() 

    /**
     * Method hasQtMesRegistroTrafg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesRegistroTrafg()
    {
        return this._has_qtMesRegistroTrafg;
    } //-- boolean hasQtMesRegistroTrafg() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplicacaoTransmPagamento'.
     * 
     * @param cdAplicacaoTransmPagamento the value of field
     * 'cdAplicacaoTransmPagamento'.
     */
    public void setCdAplicacaoTransmPagamento(long cdAplicacaoTransmPagamento)
    {
        this._cdAplicacaoTransmPagamento = cdAplicacaoTransmPagamento;
        this._has_cdAplicacaoTransmPagamento = true;
    } //-- void setCdAplicacaoTransmPagamento(long) 

    /**
     * Sets the value of field 'cdAreaFoneCliente'.
     * 
     * @param cdAreaFoneCliente the value of field
     * 'cdAreaFoneCliente'.
     */
    public void setCdAreaFoneCliente(int cdAreaFoneCliente)
    {
        this._cdAreaFoneCliente = cdAreaFoneCliente;
        this._has_cdAreaFoneCliente = true;
    } //-- void setCdAreaFoneCliente(int) 

    /**
     * Sets the value of field 'cdAreaFonePend'.
     * 
     * @param cdAreaFonePend the value of field 'cdAreaFonePend'.
     */
    public void setCdAreaFonePend(int cdAreaFonePend)
    {
        this._cdAreaFonePend = cdAreaFonePend;
        this._has_cdAreaFonePend = true;
    } //-- void setCdAreaFonePend(int) 

    /**
     * Sets the value of field 'cdCodMeioPrincipalRetorno'.
     * 
     * @param cdCodMeioPrincipalRetorno the value of field
     * 'cdCodMeioPrincipalRetorno'.
     */
    public void setCdCodMeioPrincipalRetorno(java.lang.String cdCodMeioPrincipalRetorno)
    {
        this._cdCodMeioPrincipalRetorno = cdCodMeioPrincipalRetorno;
    } //-- void setCdCodMeioPrincipalRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdControleNumeroRemessa'.
     * 
     * @param cdControleNumeroRemessa the value of field
     * 'cdControleNumeroRemessa'.
     */
    public void setCdControleNumeroRemessa(int cdControleNumeroRemessa)
    {
        this._cdControleNumeroRemessa = cdControleNumeroRemessa;
        this._has_cdControleNumeroRemessa = true;
    } //-- void setCdControleNumeroRemessa(int) 

    /**
     * Sets the value of field 'cdControleNumeroRetorno'.
     * 
     * @param cdControleNumeroRetorno the value of field
     * 'cdControleNumeroRetorno'.
     */
    public void setCdControleNumeroRetorno(int cdControleNumeroRetorno)
    {
        this._cdControleNumeroRetorno = cdControleNumeroRetorno;
        this._has_cdControleNumeroRetorno = true;
    } //-- void setCdControleNumeroRetorno(int) 

    /**
     * Sets the value of field 'cdFoneContatoCliente'.
     * 
     * @param cdFoneContatoCliente the value of field
     * 'cdFoneContatoCliente'.
     */
    public void setCdFoneContatoCliente(long cdFoneContatoCliente)
    {
        this._cdFoneContatoCliente = cdFoneContatoCliente;
        this._has_cdFoneContatoCliente = true;
    } //-- void setCdFoneContatoCliente(long) 

    /**
     * Sets the value of field 'cdFoneSolicitacaoPend'.
     * 
     * @param cdFoneSolicitacaoPend the value of field
     * 'cdFoneSolicitacaoPend'.
     */
    public void setCdFoneSolicitacaoPend(long cdFoneSolicitacaoPend)
    {
        this._cdFoneSolicitacaoPend = cdFoneSolicitacaoPend;
        this._has_cdFoneSolicitacaoPend = true;
    } //-- void setCdFoneSolicitacaoPend(long) 

    /**
     * Sets the value of field 'cdIndicadorAssociacaoLayout'.
     * 
     * @param cdIndicadorAssociacaoLayout the value of field
     * 'cdIndicadorAssociacaoLayout'.
     */
    public void setCdIndicadorAssociacaoLayout(int cdIndicadorAssociacaoLayout)
    {
        this._cdIndicadorAssociacaoLayout = cdIndicadorAssociacaoLayout;
        this._has_cdIndicadorAssociacaoLayout = true;
    } //-- void setCdIndicadorAssociacaoLayout(int) 

    /**
     * Sets the value of field 'cdIndicadorConsisteContaDebito'.
     * 
     * @param cdIndicadorConsisteContaDebito the value of field
     * 'cdIndicadorConsisteContaDebito'.
     */
    public void setCdIndicadorConsisteContaDebito(int cdIndicadorConsisteContaDebito)
    {
        this._cdIndicadorConsisteContaDebito = cdIndicadorConsisteContaDebito;
        this._has_cdIndicadorConsisteContaDebito = true;
    } //-- void setCdIndicadorConsisteContaDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorContaComplementar'.
     * 
     * @param cdIndicadorContaComplementar the value of field
     * 'cdIndicadorContaComplementar'.
     */
    public void setCdIndicadorContaComplementar(int cdIndicadorContaComplementar)
    {
        this._cdIndicadorContaComplementar = cdIndicadorContaComplementar;
        this._has_cdIndicadorContaComplementar = true;
    } //-- void setCdIndicadorContaComplementar(int) 

    /**
     * Sets the value of field 'cdIndicadorCpfLayout'.
     * 
     * @param cdIndicadorCpfLayout the value of field
     * 'cdIndicadorCpfLayout'.
     */
    public void setCdIndicadorCpfLayout(int cdIndicadorCpfLayout)
    {
        this._cdIndicadorCpfLayout = cdIndicadorCpfLayout;
        this._has_cdIndicadorCpfLayout = true;
    } //-- void setCdIndicadorCpfLayout(int) 

    /**
     * Sets the value of field 'cdIndicadorGeracaoSegmentoB'.
     * 
     * @param cdIndicadorGeracaoSegmentoB the value of field
     * 'cdIndicadorGeracaoSegmentoB'.
     */
    public void setCdIndicadorGeracaoSegmentoB(int cdIndicadorGeracaoSegmentoB)
    {
        this._cdIndicadorGeracaoSegmentoB = cdIndicadorGeracaoSegmentoB;
        this._has_cdIndicadorGeracaoSegmentoB = true;
    } //-- void setCdIndicadorGeracaoSegmentoB(int) 

    /**
     * Sets the value of field 'cdIndicadorGeracaoSegmentoZ'.
     * 
     * @param cdIndicadorGeracaoSegmentoZ the value of field
     * 'cdIndicadorGeracaoSegmentoZ'.
     */
    public void setCdIndicadorGeracaoSegmentoZ(int cdIndicadorGeracaoSegmentoZ)
    {
        this._cdIndicadorGeracaoSegmentoZ = cdIndicadorGeracaoSegmentoZ;
        this._has_cdIndicadorGeracaoSegmentoZ = true;
    } //-- void setCdIndicadorGeracaoSegmentoZ(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdMeioAlternRemessa'.
     * 
     * @param cdMeioAlternRemessa the value of field
     * 'cdMeioAlternRemessa'.
     */
    public void setCdMeioAlternRemessa(int cdMeioAlternRemessa)
    {
        this._cdMeioAlternRemessa = cdMeioAlternRemessa;
        this._has_cdMeioAlternRemessa = true;
    } //-- void setCdMeioAlternRemessa(int) 

    /**
     * Sets the value of field 'cdMeioAltrnRetorno'.
     * 
     * @param cdMeioAltrnRetorno the value of field
     * 'cdMeioAltrnRetorno'.
     */
    public void setCdMeioAltrnRetorno(int cdMeioAltrnRetorno)
    {
        this._cdMeioAltrnRetorno = cdMeioAltrnRetorno;
        this._has_cdMeioAltrnRetorno = true;
    } //-- void setCdMeioAltrnRetorno(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @param cdMeioPrincipalRemessa the value of field
     * 'cdMeioPrincipalRemessa'.
     */
    public void setCdMeioPrincipalRemessa(int cdMeioPrincipalRemessa)
    {
        this._cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
        this._has_cdMeioPrincipalRemessa = true;
    } //-- void setCdMeioPrincipalRemessa(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @param cdMeioPrincipalRetorno the value of field
     * 'cdMeioPrincipalRetorno'.
     */
    public void setCdMeioPrincipalRetorno(int cdMeioPrincipalRetorno)
    {
        this._cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
        this._has_cdMeioPrincipalRetorno = true;
    } //-- void setCdMeioPrincipalRetorno(int) 

    /**
     * Sets the value of field 'cdNivelControleRemessa'.
     * 
     * @param cdNivelControleRemessa the value of field
     * 'cdNivelControleRemessa'.
     */
    public void setCdNivelControleRemessa(int cdNivelControleRemessa)
    {
        this._cdNivelControleRemessa = cdNivelControleRemessa;
        this._has_cdNivelControleRemessa = true;
    } //-- void setCdNivelControleRemessa(int) 

    /**
     * Sets the value of field 'cdNivelControleRetorno'.
     * 
     * @param cdNivelControleRetorno the value of field
     * 'cdNivelControleRetorno'.
     */
    public void setCdNivelControleRetorno(int cdNivelControleRetorno)
    {
        this._cdNivelControleRetorno = cdNivelControleRetorno;
        this._has_cdNivelControleRetorno = true;
    } //-- void setCdNivelControleRetorno(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPerfilTrocaArq'.
     * 
     * @param cdPerfilTrocaArq the value of field 'cdPerfilTrocaArq'
     */
    public void setCdPerfilTrocaArq(long cdPerfilTrocaArq)
    {
        this._cdPerfilTrocaArq = cdPerfilTrocaArq;
        this._has_cdPerfilTrocaArq = true;
    } //-- void setCdPerfilTrocaArq(long) 

    /**
     * Sets the value of field 'cdPeriodicidadeContagemRemessa'.
     * 
     * @param cdPeriodicidadeContagemRemessa the value of field
     * 'cdPeriodicidadeContagemRemessa'.
     */
    public void setCdPeriodicidadeContagemRemessa(int cdPeriodicidadeContagemRemessa)
    {
        this._cdPeriodicidadeContagemRemessa = cdPeriodicidadeContagemRemessa;
        this._has_cdPeriodicidadeContagemRemessa = true;
    } //-- void setCdPeriodicidadeContagemRemessa(int) 

    /**
     * Sets the value of field 'cdPeriodicodadeContagemRetorno'.
     * 
     * @param cdPeriodicodadeContagemRetorno the value of field
     * 'cdPeriodicodadeContagemRetorno'.
     */
    public void setCdPeriodicodadeContagemRetorno(int cdPeriodicodadeContagemRetorno)
    {
        this._cdPeriodicodadeContagemRetorno = cdPeriodicodadeContagemRetorno;
        this._has_cdPeriodicodadeContagemRetorno = true;
    } //-- void setCdPeriodicodadeContagemRetorno(int) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @param cdPessoaJuridicaParceiro the value of field
     * 'cdPessoaJuridicaParceiro'.
     */
    public void setCdPessoaJuridicaParceiro(long cdPessoaJuridicaParceiro)
    {
        this._cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
        this._has_cdPessoaJuridicaParceiro = true;
    } //-- void setCdPessoaJuridicaParceiro(long) 

    /**
     * Sets the value of field 'cdRamalContatoCliente'.
     * 
     * @param cdRamalContatoCliente the value of field
     * 'cdRamalContatoCliente'.
     */
    public void setCdRamalContatoCliente(java.lang.String cdRamalContatoCliente)
    {
        this._cdRamalContatoCliente = cdRamalContatoCliente;
    } //-- void setCdRamalContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdRamalSolctPend'.
     * 
     * @param cdRamalSolctPend the value of field 'cdRamalSolctPend'
     */
    public void setCdRamalSolctPend(java.lang.String cdRamalSolctPend)
    {
        this._cdRamalSolctPend = cdRamalSolctPend;
    } //-- void setCdRamalSolctPend(java.lang.String) 

    /**
     * Sets the value of field 'cdRejeicaoAcltoRemessa'.
     * 
     * @param cdRejeicaoAcltoRemessa the value of field
     * 'cdRejeicaoAcltoRemessa'.
     */
    public void setCdRejeicaoAcltoRemessa(int cdRejeicaoAcltoRemessa)
    {
        this._cdRejeicaoAcltoRemessa = cdRejeicaoAcltoRemessa;
        this._has_cdRejeicaoAcltoRemessa = true;
    } //-- void setCdRejeicaoAcltoRemessa(int) 

    /**
     * Sets the value of field 'cdResponsavelCustoEmpresa'.
     * 
     * @param cdResponsavelCustoEmpresa the value of field
     * 'cdResponsavelCustoEmpresa'.
     */
    public void setCdResponsavelCustoEmpresa(int cdResponsavelCustoEmpresa)
    {
        this._cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
        this._has_cdResponsavelCustoEmpresa = true;
    } //-- void setCdResponsavelCustoEmpresa(int) 

    /**
     * Sets the value of field 'cdSerieAplicTranmicao'.
     * 
     * @param cdSerieAplicTranmicao the value of field
     * 'cdSerieAplicTranmicao'.
     */
    public void setCdSerieAplicTranmicao(java.lang.String cdSerieAplicTranmicao)
    {
        this._cdSerieAplicTranmicao = cdSerieAplicTranmicao;
    } //-- void setCdSerieAplicTranmicao(java.lang.String) 

    /**
     * Sets the value of field 'cdSistemaOrigemArquivo'.
     * 
     * @param cdSistemaOrigemArquivo the value of field
     * 'cdSistemaOrigemArquivo'.
     */
    public void setCdSistemaOrigemArquivo(java.lang.String cdSistemaOrigemArquivo)
    {
        this._cdSistemaOrigemArquivo = cdSistemaOrigemArquivo;
    } //-- void setCdSistemaOrigemArquivo(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @param cdUsuarioManutencaoexterno the value of field
     * 'cdUsuarioManutencaoexterno'.
     */
    public void setCdUsuarioManutencaoexterno(java.lang.String cdUsuarioManutencaoexterno)
    {
        this._cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
    } //-- void setCdUsuarioManutencaoexterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAplicFormat'.
     * 
     * @param dsAplicFormat the value of field 'dsAplicFormat'.
     */
    public void setDsAplicFormat(java.lang.String dsAplicFormat)
    {
        this._dsAplicFormat = dsAplicFormat;
    } //-- void setDsAplicFormat(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCodMeioAlternRemessa'.
     * 
     * @param dsCodMeioAlternRemessa the value of field
     * 'dsCodMeioAlternRemessa'.
     */
    public void setDsCodMeioAlternRemessa(java.lang.String dsCodMeioAlternRemessa)
    {
        this._dsCodMeioAlternRemessa = dsCodMeioAlternRemessa;
    } //-- void setDsCodMeioAlternRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsCodMeioPrincipalRemessa'.
     * 
     * @param dsCodMeioPrincipalRemessa the value of field
     * 'dsCodMeioPrincipalRemessa'.
     */
    public void setDsCodMeioPrincipalRemessa(java.lang.String dsCodMeioPrincipalRemessa)
    {
        this._dsCodMeioPrincipalRemessa = dsCodMeioPrincipalRemessa;
    } //-- void setDsCodMeioPrincipalRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsCodTipoLayout'.
     * 
     * @param dsCodTipoLayout the value of field 'dsCodTipoLayout'.
     */
    public void setDsCodTipoLayout(java.lang.String dsCodTipoLayout)
    {
        this._dsCodTipoLayout = dsCodTipoLayout;
    } //-- void setDsCodTipoLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsControleNumeroRemessa'.
     * 
     * @param dsControleNumeroRemessa the value of field
     * 'dsControleNumeroRemessa'.
     */
    public void setDsControleNumeroRemessa(java.lang.String dsControleNumeroRemessa)
    {
        this._dsControleNumeroRemessa = dsControleNumeroRemessa;
    } //-- void setDsControleNumeroRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsControleNumeroRetorno'.
     * 
     * @param dsControleNumeroRetorno the value of field
     * 'dsControleNumeroRetorno'.
     */
    public void setDsControleNumeroRetorno(java.lang.String dsControleNumeroRetorno)
    {
        this._dsControleNumeroRetorno = dsControleNumeroRetorno;
    } //-- void setDsControleNumeroRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailContatoCliente'.
     * 
     * @param dsEmailContatoCliente the value of field
     * 'dsEmailContatoCliente'.
     */
    public void setDsEmailContatoCliente(java.lang.String dsEmailContatoCliente)
    {
        this._dsEmailContatoCliente = dsEmailContatoCliente;
    } //-- void setDsEmailContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsEmailSolctPend'.
     * 
     * @param dsEmailSolctPend the value of field 'dsEmailSolctPend'
     */
    public void setDsEmailSolctPend(java.lang.String dsEmailSolctPend)
    {
        this._dsEmailSolctPend = dsEmailSolctPend;
    } //-- void setDsEmailSolctPend(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAssociacaoLayout'.
     * 
     * @param dsIndicadorAssociacaoLayout the value of field
     * 'dsIndicadorAssociacaoLayout'.
     */
    public void setDsIndicadorAssociacaoLayout(java.lang.String dsIndicadorAssociacaoLayout)
    {
        this._dsIndicadorAssociacaoLayout = dsIndicadorAssociacaoLayout;
    } //-- void setDsIndicadorAssociacaoLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorConsisteContaDebito'.
     * 
     * @param dsIndicadorConsisteContaDebito the value of field
     * 'dsIndicadorConsisteContaDebito'.
     */
    public void setDsIndicadorConsisteContaDebito(java.lang.String dsIndicadorConsisteContaDebito)
    {
        this._dsIndicadorConsisteContaDebito = dsIndicadorConsisteContaDebito;
    } //-- void setDsIndicadorConsisteContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorContaComplementar'.
     * 
     * @param dsIndicadorContaComplementar the value of field
     * 'dsIndicadorContaComplementar'.
     */
    public void setDsIndicadorContaComplementar(java.lang.String dsIndicadorContaComplementar)
    {
        this._dsIndicadorContaComplementar = dsIndicadorContaComplementar;
    } //-- void setDsIndicadorContaComplementar(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorCpfLayout'.
     * 
     * @param dsIndicadorCpfLayout the value of field
     * 'dsIndicadorCpfLayout'.
     */
    public void setDsIndicadorCpfLayout(java.lang.String dsIndicadorCpfLayout)
    {
        this._dsIndicadorCpfLayout = dsIndicadorCpfLayout;
    } //-- void setDsIndicadorCpfLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAltrnRetorno'.
     * 
     * @param dsMeioAltrnRetorno the value of field
     * 'dsMeioAltrnRetorno'.
     */
    public void setDsMeioAltrnRetorno(java.lang.String dsMeioAltrnRetorno)
    {
        this._dsMeioAltrnRetorno = dsMeioAltrnRetorno;
    } //-- void setDsMeioAltrnRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsNivelControleRemessa'.
     * 
     * @param dsNivelControleRemessa the value of field
     * 'dsNivelControleRemessa'.
     */
    public void setDsNivelControleRemessa(java.lang.String dsNivelControleRemessa)
    {
        this._dsNivelControleRemessa = dsNivelControleRemessa;
    } //-- void setDsNivelControleRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNivelControleRetorno'.
     * 
     * @param dsNivelControleRetorno the value of field
     * 'dsNivelControleRetorno'.
     */
    public void setDsNivelControleRetorno(java.lang.String dsNivelControleRetorno)
    {
        this._dsNivelControleRetorno = dsNivelControleRetorno;
    } //-- void setDsNivelControleRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRemessa'.
     * 
     * @param dsNomeArquivoRemessa the value of field
     * 'dsNomeArquivoRemessa'.
     */
    public void setDsNomeArquivoRemessa(java.lang.String dsNomeArquivoRemessa)
    {
        this._dsNomeArquivoRemessa = dsNomeArquivoRemessa;
    } //-- void setDsNomeArquivoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRetorno'.
     * 
     * @param dsNomeArquivoRetorno the value of field
     * 'dsNomeArquivoRetorno'.
     */
    public void setDsNomeArquivoRetorno(java.lang.String dsNomeArquivoRetorno)
    {
        this._dsNomeArquivoRetorno = dsNomeArquivoRetorno;
    } //-- void setDsNomeArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeCliente'.
     * 
     * @param dsNomeCliente the value of field 'dsNomeCliente'.
     */
    public void setDsNomeCliente(java.lang.String dsNomeCliente)
    {
        this._dsNomeCliente = dsNomeCliente;
    } //-- void setDsNomeCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeContatoCliente'.
     * 
     * @param dsNomeContatoCliente the value of field
     * 'dsNomeContatoCliente'.
     */
    public void setDsNomeContatoCliente(java.lang.String dsNomeContatoCliente)
    {
        this._dsNomeContatoCliente = dsNomeContatoCliente;
    } //-- void setDsNomeContatoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsObsGeralPerfil'.
     * 
     * @param dsObsGeralPerfil the value of field 'dsObsGeralPerfil'
     */
    public void setDsObsGeralPerfil(java.lang.String dsObsGeralPerfil)
    {
        this._dsObsGeralPerfil = dsObsGeralPerfil;
    } //-- void setDsObsGeralPerfil(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicidadeContagemRemessa'.
     * 
     * @param dsPeriodicidadeContagemRemessa the value of field
     * 'dsPeriodicidadeContagemRemessa'.
     */
    public void setDsPeriodicidadeContagemRemessa(java.lang.String dsPeriodicidadeContagemRemessa)
    {
        this._dsPeriodicidadeContagemRemessa = dsPeriodicidadeContagemRemessa;
    } //-- void setDsPeriodicidadeContagemRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsPeriodicodadeContagemRetorno'.
     * 
     * @param dsPeriodicodadeContagemRetorno the value of field
     * 'dsPeriodicodadeContagemRetorno'.
     */
    public void setDsPeriodicodadeContagemRetorno(java.lang.String dsPeriodicodadeContagemRetorno)
    {
        this._dsPeriodicodadeContagemRetorno = dsPeriodicodadeContagemRetorno;
    } //-- void setDsPeriodicodadeContagemRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoAcltoRemessa'.
     * 
     * @param dsRejeicaoAcltoRemessa the value of field
     * 'dsRejeicaoAcltoRemessa'.
     */
    public void setDsRejeicaoAcltoRemessa(java.lang.String dsRejeicaoAcltoRemessa)
    {
        this._dsRejeicaoAcltoRemessa = dsRejeicaoAcltoRemessa;
    } //-- void setDsRejeicaoAcltoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsResponsavelCustoEmpresa'.
     * 
     * @param dsResponsavelCustoEmpresa the value of field
     * 'dsResponsavelCustoEmpresa'.
     */
    public void setDsResponsavelCustoEmpresa(java.lang.String dsResponsavelCustoEmpresa)
    {
        this._dsResponsavelCustoEmpresa = dsResponsavelCustoEmpresa;
    } //-- void setDsResponsavelCustoEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoB'.
     * 
     * @param dsSegmentoB the value of field 'dsSegmentoB'.
     */
    public void setDsSegmentoB(java.lang.String dsSegmentoB)
    {
        this._dsSegmentoB = dsSegmentoB;
    } //-- void setDsSegmentoB(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoZ'.
     * 
     * @param dsSegmentoZ the value of field 'dsSegmentoZ'.
     */
    public void setDsSegmentoZ(java.lang.String dsSegmentoZ)
    {
        this._dsSegmentoZ = dsSegmentoZ;
    } //-- void setDsSegmentoZ(java.lang.String) 

    /**
     * Sets the value of field 'dsSistemaOrigemArquivo'.
     * 
     * @param dsSistemaOrigemArquivo the value of field
     * 'dsSistemaOrigemArquivo'.
     */
    public void setDsSistemaOrigemArquivo(java.lang.String dsSistemaOrigemArquivo)
    {
        this._dsSistemaOrigemArquivo = dsSistemaOrigemArquivo;
    } //-- void setDsSistemaOrigemArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsSolicitacaoPendente'.
     * 
     * @param dsSolicitacaoPendente the value of field
     * 'dsSolicitacaoPendente'.
     */
    public void setDsSolicitacaoPendente(java.lang.String dsSolicitacaoPendente)
    {
        this._dsSolicitacaoPendente = dsSolicitacaoPendente;
    } //-- void setDsSolicitacaoPendente(java.lang.String) 

    /**
     * Sets the value of field 'dsUtilizacaoEmpresaVan'.
     * 
     * @param dsUtilizacaoEmpresaVan the value of field
     * 'dsUtilizacaoEmpresaVan'.
     */
    public void setDsUtilizacaoEmpresaVan(java.lang.String dsUtilizacaoEmpresaVan)
    {
        this._dsUtilizacaoEmpresaVan = dsUtilizacaoEmpresaVan;
    } //-- void setDsUtilizacaoEmpresaVan(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHist'.
     * 
     * @param hrInclusaoRegistroHist the value of field
     * 'hrInclusaoRegistroHist'.
     */
    public void setHrInclusaoRegistroHist(java.lang.String hrInclusaoRegistroHist)
    {
        this._hrInclusaoRegistroHist = hrInclusaoRegistroHist;
    } //-- void setHrInclusaoRegistroHist(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrMaxContagemRemessa'.
     * 
     * @param nrMaxContagemRemessa the value of field
     * 'nrMaxContagemRemessa'.
     */
    public void setNrMaxContagemRemessa(long nrMaxContagemRemessa)
    {
        this._nrMaxContagemRemessa = nrMaxContagemRemessa;
        this._has_nrMaxContagemRemessa = true;
    } //-- void setNrMaxContagemRemessa(long) 

    /**
     * Sets the value of field 'nrMaxContagemRetorno'.
     * 
     * @param nrMaxContagemRetorno the value of field
     * 'nrMaxContagemRetorno'.
     */
    public void setNrMaxContagemRetorno(long nrMaxContagemRetorno)
    {
        this._nrMaxContagemRetorno = nrMaxContagemRetorno;
        this._has_nrMaxContagemRetorno = true;
    } //-- void setNrMaxContagemRetorno(long) 

    /**
     * Sets the value of field 'pcCustoOrganizacaoTransmissao'.
     * 
     * @param pcCustoOrganizacaoTransmissao the value of field
     * 'pcCustoOrganizacaoTransmissao'.
     */
    public void setPcCustoOrganizacaoTransmissao(java.math.BigDecimal pcCustoOrganizacaoTransmissao)
    {
        this._pcCustoOrganizacaoTransmissao = pcCustoOrganizacaoTransmissao;
    } //-- void setPcCustoOrganizacaoTransmissao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'percentualIncotRejeiRemessa'.
     * 
     * @param percentualIncotRejeiRemessa the value of field
     * 'percentualIncotRejeiRemessa'.
     */
    public void setPercentualIncotRejeiRemessa(java.math.BigDecimal percentualIncotRejeiRemessa)
    {
        this._percentualIncotRejeiRemessa = percentualIncotRejeiRemessa;
    } //-- void setPercentualIncotRejeiRemessa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'qtMaxIncotRemessa'.
     * 
     * @param qtMaxIncotRemessa the value of field
     * 'qtMaxIncotRemessa'.
     */
    public void setQtMaxIncotRemessa(int qtMaxIncotRemessa)
    {
        this._qtMaxIncotRemessa = qtMaxIncotRemessa;
        this._has_qtMaxIncotRemessa = true;
    } //-- void setQtMaxIncotRemessa(int) 

    /**
     * Sets the value of field 'qtMesRegistroTrafg'.
     * 
     * @param qtMesRegistroTrafg the value of field
     * 'qtMesRegistroTrafg'.
     */
    public void setQtMesRegistroTrafg(long qtMesRegistroTrafg)
    {
        this._qtMesRegistroTrafg = qtMesRegistroTrafg;
        this._has_qtMesRegistroTrafg = true;
    } //-- void setQtMesRegistroTrafg(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistoricoPerfilTrocaArquivoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.response.DetalharHistoricoPerfilTrocaArquivoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.response.DetalharHistoricoPerfilTrocaArquivoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.response.DetalharHistoricoPerfilTrocaArquivoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricoperfiltrocaarquivo.response.DetalharHistoricoPerfilTrocaArquivoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
