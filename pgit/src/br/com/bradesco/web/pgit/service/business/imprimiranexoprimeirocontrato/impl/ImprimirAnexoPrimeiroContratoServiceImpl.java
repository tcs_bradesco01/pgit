/**
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.impl
 * Compilador: JDK 1.5
 * Propósito: INSERIR O PROPÓSITO DAS CLASSES DO PACOTE
 * Data da criação: <dd/MM/yyyy>
 * Parâmetros de compilação: -d
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.IImprimirAnexoPrimeiroContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasContas;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasLayout;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasParticipantes;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontrato.request.ImprimirAnexoPrimeiroContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontrato.response.ImprimirAnexoPrimeiroContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.request.ImprimirAnexoPrimeiroContratoLoopRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.ImprimirAnexoPrimeiroContratoLoopResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ImprimirAnexoPrimeiroContratoServiceImpl
 * <p>
 * Propósito: Implementação do adaptador ImprimirAnexoPrimeiroContrato
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IImprimirAnexoPrimeiroContratoService
 */
public class ImprimirAnexoPrimeiroContratoServiceImpl implements IImprimirAnexoPrimeiroContratoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.IImprimirAnexoPrimeiroContratoService#imprimirAnexoPrimeiroContrato(br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoEntradaDTO)
	 */
	public ImprimirAnexoPrimeiroContratoSaidaDTO imprimirAnexoPrimeiroContrato(
					ImprimirAnexoPrimeiroContratoEntradaDTO entrada) {

		ImprimirAnexoPrimeiroContratoSaidaDTO saida = new ImprimirAnexoPrimeiroContratoSaidaDTO();
		ImprimirAnexoPrimeiroContratoRequest request = new ImprimirAnexoPrimeiroContratoRequest();
		ImprimirAnexoPrimeiroContratoResponse response = null;

		request.setCdpessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());

		response = getFactoryAdapter().getImprimirAnexoPrimeiroContratoPDCAdapter().invokeProcess(request);

		// Dados da proposta:
		saida.setNrSequenciaContrato(response.getNrSequenciaContrato());
		saida.setDtInclusaoContrato(response.getDtInclusaoContrato());
		saida.setDtInicioVigencia(response.getDtInicioVigencia());
		saida.setDtFinalVigencia(response.getDtFinalVigencia());

		// Dados do cliente representante
		saida.setCdCpfCnpjRepresentante(response.getCdCpfCnpjRepresentante());
		saida.setDsNomeRepresentante(response.getDsNomeRepresentante());
		saida.setCdGrupoEconomico(response.getCdGrupoEconomico());
		saida.setDsGrupoEconomico(saida.getCdGrupoEconomico() + " - " + response.getDsGrupoEconomico());
		saida.setCdAtividadeEconomica(response.getCdAtividadeEconomica());
		saida.setDsAtividadeEconomica(saida.getCdAtividadeEconomica() + " - " + response.getDsAtividadeEconomica());
		saida.setDsSegmentoEconomico(response.getDsSegmentoEconomico());
		saida.setDsSubSegmento(response.getDsSubSegmento());
		saida.setDsContrato(response.getDsContrato());
		saida.setCdAgenciaGestoraContrato(response.getCdAgenciaGestoraContrato());
		saida.setDsAgenciaGestoraContrato(saida.getCdAgenciaGestoraContrato() + " - "
						+ response.getDsAgenciaGestoraContrato());

		// Dados do participante
		OcorrenciasParticipantes participante = null;
		saida.setQtdeParticipante(response.getQtdeParticipante());
		List<OcorrenciasParticipantes> listaParticipantes = new ArrayList<OcorrenciasParticipantes>();

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			participante = new OcorrenciasParticipantes();

			participante.setCdCpfCnpjParticipante(response.getOcorrencias(i).getCdCpfCnpjParticipante());
			participante.setNmParticipante(response.getOcorrencias(i).getNmParticipante());
			participante.setCdGrupoEconomicoParticipante(response.getOcorrencias(i).getCdGrupoEconomicoParticipante());
			participante.setDsGrupoEconomicoParticipante(participante.getCdGrupoEconomicoParticipante() + " - "
							+ response.getOcorrencias(i).getDsGrupoEconomicoParticipante());
			
			
			if (response.getOcorrencias(i).getNmParticipante() != null && !"".equals(response.getOcorrencias(i).getNmParticipante().trim()) ) {
				listaParticipantes.add(participante);
			}
			
		}
		saida.setListaParticipantes(listaParticipantes);

		// CONTAS
		OcorrenciasContas conta = null;
		saida.setQtdeConta(response.getQtdeConta());
		List<OcorrenciasContas> listaContas = new ArrayList<OcorrenciasContas>();

		for (int i = 0; i < response.getOcorrencias1Count(); i++) {
			conta = new OcorrenciasContas();

			conta.setCdCpfCnpjConta(response.getOcorrencias1(i).getCdCpfCnpjConta());
			conta.setCdNomeParticipanteConta(response.getOcorrencias1(i).getCdNomeParticipanteConta());
			conta.setCdBancoConta(response.getOcorrencias1(i).getCdBancoConta());
			conta.setCdAgenciaConta(response.getOcorrencias1(i).getCdAgenciaConta());

			String cdAgencia = PgitUtil.formatAgencia(conta.getCdAgenciaConta(), response.getOcorrencias1(i).getDigitoAgenciaConta(), true);
			conta.setDigitoAgenciaConta(cdAgencia);

			conta.setCdContaConta(response.getOcorrencias1(i).getCdContaConta());

			String cdConta = PgitUtil.formatConta(conta.getCdContaConta(), response.getOcorrencias1(i).getDigitoContaConta(), true);
			conta.setDigitoContaConta(cdConta);
			conta.setDsTipoConta(response.getOcorrencias1(i).getDsTipoConta());
			
			
			if (response.getOcorrencias1(i).getCdNomeParticipanteConta() != null && !"".equals(response.getOcorrencias1(i).getCdNomeParticipanteConta().trim())) {
				listaContas.add(conta);
			}
			
		}
		saida.setListaContas(listaContas);

		// CONFIGURAÇÕES LAYOUT
		OcorrenciasLayout layout = null;
		saida.setQtLayout(response.getQtLayout());
		List<OcorrenciasLayout> listaLayouts = new ArrayList<OcorrenciasLayout>();

		for (int i = 0; i < response.getOcorrencias2Count(); i++) {
			layout = new OcorrenciasLayout();

			layout.setDsServicoLayout(response.getOcorrencias2(i).getDsServicoLayout());
			layout.setCdLayout(response.getOcorrencias2(i).getCdLayout());
			layout.setCdPerfilTrocaArq(response.getOcorrencias2(i).getCdPerfilTrocaArq());
			layout.setDsAplicativoTransmissao(response.getOcorrencias2(i).getDsAplicativoTransmissao());
			layout.setDsMeioTransmissao(response.getOcorrencias2(i).getDsMeioTransmissao());
			
			if (response.getOcorrencias2(i).getDsServicoLayout() != null && !"".equals(response.getOcorrencias2(i).getDsServicoLayout().trim())) {
				listaLayouts.add(layout);
			}

		}
		saida.setListaLayouts(listaLayouts);

		return saida;
	}	
	

	public ImprimirAnexoPrimeiroContratoSaidaDTO imprimirAnexoPrimeiroContratoLoop(
			ImprimirAnexoPrimeiroContratoEntradaDTO entrada) {
		
		
		ImprimirAnexoPrimeiroContratoSaidaDTO saida = new ImprimirAnexoPrimeiroContratoSaidaDTO();
		
		ImprimirAnexoPrimeiroContratoLoopRequest request = new ImprimirAnexoPrimeiroContratoLoopRequest();
		
		
		request.setCdpessoaJuridicaContrato(entrada.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
		
		ImprimirAnexoPrimeiroContratoLoopResponse response = getFactoryAdapter().getImprimirAnexoPrimeiroContratoLoopPDCAdapter().invokeProcess(request);

		// Dados da proposta:
		saida.setNrSequenciaContrato(response.getNrSequenciaContrato());
		saida.setDtInclusaoContrato(response.getDtInclusaoContrato());
		saida.setDtInicioVigencia(response.getDtInicioVigencia());
		saida.setDtFinalVigencia(response.getDtFinalVigencia());

		// Dados do cliente representante
		saida.setCdCpfCnpjRepresentante(response.getCdCpfCnpjRepresentante());
		saida.setDsNomeRepresentante(response.getDsNomeRepresentante());
		saida.setCdGrupoEconomico(response.getCdGrupoEconomico());
		saida.setDsGrupoEconomico(saida.getCdGrupoEconomico() + " - " + response.getDsGrupoEconomico());
		saida.setCdAtividadeEconomica(response.getCdAtividadeEconomica());
		saida.setDsAtividadeEconomica(saida.getCdAtividadeEconomica() + " - " + response.getDsAtividadeEconomica());
		saida.setDsSegmentoEconomico(response.getDsSegmentoEconomico());
		saida.setDsSubSegmento(response.getDsSubSegmento());
		saida.setDsContrato(response.getDsContrato());
		saida.setCdAgenciaGestoraContrato(response.getCdAgenciaGestoraContrato());
		saida.setDsAgenciaGestoraContrato(saida.getCdAgenciaGestoraContrato() + " - "
						+ response.getDsAgenciaGestoraContrato());

		// Dados do participante
		OcorrenciasParticipantes participante = null;
		saida.setQtdeParticipante(response.getQtdeParticipante());
		List<OcorrenciasParticipantes> listaParticipantes = new ArrayList<OcorrenciasParticipantes>();

		for (int i = 0; i < response.getOcorrencias1Count(); i++) {
			participante = new OcorrenciasParticipantes();

			participante.setCdCpfCnpjParticipante(response.getOcorrencias1(i).getCdCpfCnpjParticipante());
			participante.setNmParticipante(response.getOcorrencias1(i).getNmParticipante());
			participante.setCdGrupoEconomicoParticipante(response.getOcorrencias1(i).getCdGrupoEconomicoParticipante());
			participante.setDsGrupoEconomicoParticipante(participante.getCdGrupoEconomicoParticipante() + " - "
							+ response.getOcorrencias1(i).getDsGrupoEconomicoParticipante());
			
			
			if (response.getOcorrencias1(i).getNmParticipante() != null && !"".equals(response.getOcorrencias1(i).getNmParticipante().trim()) ) {
				listaParticipantes.add(participante);
			}
			
		}
		saida.setListaParticipantes(listaParticipantes);

		// CONTAS
		OcorrenciasContas conta = null;
		saida.setQtdeConta(response.getQtdeConta());
		List<OcorrenciasContas> listaContas = new ArrayList<OcorrenciasContas>();

		for (int i = 0; i < response.getOcorrencias2Count(); i++) {
			conta = new OcorrenciasContas();

			conta.setCdCpfCnpjConta(response.getOcorrencias2(i).getCdCpfCnpjConta());
			conta.setCdNomeParticipanteConta(response.getOcorrencias2(i).getCdNomeParticipanteConta());
			conta.setCdBancoConta(response.getOcorrencias2(i).getCdBancoConta());
			conta.setCdAgenciaConta(response.getOcorrencias2(i).getCdAgenciaConta());

			String cdAgencia = PgitUtil.formatAgencia(conta.getCdAgenciaConta(), response.getOcorrencias2(i).getDigitoAgenciaConta(), true);
			conta.setDigitoAgenciaConta(cdAgencia);

			conta.setCdContaConta(response.getOcorrencias2(i).getCdContaConta());

			String cdConta = PgitUtil.formatConta(conta.getCdContaConta(), response.getOcorrencias2(i).getDigitoContaConta(), true);
			conta.setDigitoContaConta(cdConta);
			conta.setDsTipoConta(response.getOcorrencias2(i).getDsTipoConta());
			
			
			if (response.getOcorrencias2(i).getCdNomeParticipanteConta() != null && !"".equals(response.getOcorrencias2(i).getCdNomeParticipanteConta().trim())) {
				listaContas.add(conta);
			}
			
		}
		saida.setListaContas(listaContas);

		// CONFIGURAÇÕES LAYOUT
		OcorrenciasLayout layout = null;
		saida.setQtLayout(response.getQtLayout());
		List<OcorrenciasLayout> listaLayouts = new ArrayList<OcorrenciasLayout>();

		for (int i = 0; i < response.getOcorrencias3Count(); i++) {
			layout = new OcorrenciasLayout();

			layout.setDsServicoLayout(response.getOcorrencias3(i).getDsServicoLayout());
			layout.setCdLayout(response.getOcorrencias3(i).getCdLayout());
			layout.setCdPerfilTrocaArq(response.getOcorrencias3(i).getCdPerfilTrocaArq());
			layout.setDsAplicativoTransmissao(response.getOcorrencias3(i).getDsAplicativoTransmissao());
			layout.setDsMeioTransmissao(response.getOcorrencias3(i).getDsMeioTransmissao());
			
			if (response.getOcorrencias3(i).getDsServicoLayout() != null && !"".equals(response.getOcorrencias3(i).getDsServicoLayout().trim())) {
				listaLayouts.add(layout);
			}

		}
		saida.setListaLayouts(listaLayouts);

		return saida;		
		
	}
	
}