/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

/**
 * Nome: ConsultarDescGrupoEconomicoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDescGrupoEconomicoEntradaDTO {

    /** Atributo cdGrupoEconomico. */
    private Long cdGrupoEconomico;

    /**
     * Get: cdGrupoEconomico.
     *
     * @return cdGrupoEconomico
     */
    public Long getCdGrupoEconomico() {
        return cdGrupoEconomico;
    }

    /**
     * Set: cdGrupoEconomico.
     *
     * @param cdGrupoEconomico the cd grupo economico
     */
    public void setCdGrupoEconomico(Long cdGrupoEconomico) {
        this.cdGrupoEconomico = cdGrupoEconomico;
    }
	
}
