/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ListarMotivoBloqueioFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoBloqueioFavorecidoSaidaDTO {
	
	/** Atributo cdMotivoBloqueioFavorecido. */
	private Integer cdMotivoBloqueioFavorecido;
    
    /** Atributo dtMotivoBloqueioFavorecido. */
    private String dtMotivoBloqueioFavorecido;
    
	/**
	 * Listar motivo bloqueio favorecido saida dto.
	 */
	public ListarMotivoBloqueioFavorecidoSaidaDTO() {
		super();
	}
	
	/**
	 * Listar motivo bloqueio favorecido saida dto.
	 *
	 * @param cdMotivoBloqueioFavorecido the cd motivo bloqueio favorecido
	 * @param dtMotivoBloqueioFavorecido the dt motivo bloqueio favorecido
	 */
	public ListarMotivoBloqueioFavorecidoSaidaDTO(Integer cdMotivoBloqueioFavorecido, String dtMotivoBloqueioFavorecido) {
		super();
		this.cdMotivoBloqueioFavorecido = cdMotivoBloqueioFavorecido;
		this.dtMotivoBloqueioFavorecido = dtMotivoBloqueioFavorecido;
	}

	/**
	 * Get: cdMotivoBloqueioFavorecido.
	 *
	 * @return cdMotivoBloqueioFavorecido
	 */
	public Integer getCdMotivoBloqueioFavorecido() {
		return cdMotivoBloqueioFavorecido;
	}
	
	/**
	 * Set: cdMotivoBloqueioFavorecido.
	 *
	 * @param cdMotivoBloqueioFavorecido the cd motivo bloqueio favorecido
	 */
	public void setCdMotivoBloqueioFavorecido(Integer cdMotivoBloqueioFavorecido) {
		this.cdMotivoBloqueioFavorecido = cdMotivoBloqueioFavorecido;
	}
	
	/**
	 * Get: dtMotivoBloqueioFavorecido.
	 *
	 * @return dtMotivoBloqueioFavorecido
	 */
	public String getDtMotivoBloqueioFavorecido() {
		return dtMotivoBloqueioFavorecido;
	}
	
	/**
	 * Set: dtMotivoBloqueioFavorecido.
	 *
	 * @param dtMotivoBloqueioFavorecido the dt motivo bloqueio favorecido
	 */
	public void setDtMotivoBloqueioFavorecido(String dtMotivoBloqueioFavorecido) {
		this.dtMotivoBloqueioFavorecido = dtMotivoBloqueioFavorecido;
	}

}
