/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdControleMensagemSalarial
     */
    private int _cdControleMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdControleMensagemSalarial
     */
    private boolean _has_cdControleMensagemSalarial;

    /**
     * Field _dsControleMensagemSalarial
     */
    private java.lang.String _dsControleMensagemSalarial;

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _nrEventoMensagemNegocio
     */
    private int _nrEventoMensagemNegocio = 0;

    /**
     * keeps track of state for field: _nrEventoMensagemNegocio
     */
    private boolean _has_nrEventoMensagemNegocio;

    /**
     * Field _dsEventoMensagemNegocio
     */
    private java.lang.String _dsEventoMensagemNegocio;

    /**
     * Field _cdReciboGeradorMensagem
     */
    private int _cdReciboGeradorMensagem = 0;

    /**
     * keeps track of state for field: _cdReciboGeradorMensagem
     */
    private boolean _has_cdReciboGeradorMensagem;

    /**
     * Field _dsReciboGeradorMensagem
     */
    private java.lang.String _dsReciboGeradorMensagem;

    /**
     * Field _cdIdiomaTextoMensagem
     */
    private int _cdIdiomaTextoMensagem = 0;

    /**
     * keeps track of state for field: _cdIdiomaTextoMensagem
     */
    private boolean _has_cdIdiomaTextoMensagem;

    /**
     * Field _dsIdiomaTextoMensagem
     */
    private java.lang.String _dsIdiomaTextoMensagem;

    /**
     * Field _nrOrdemLinhaMensagem
     */
    private long _nrOrdemLinhaMensagem = 0;

    /**
     * keeps track of state for field: _nrOrdemLinhaMensagem
     */
    private boolean _has_nrOrdemLinhaMensagem;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nrOperacaoFluxoInclusao
     */
    private java.lang.String _nrOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nrOperacaoFluxoManutencao
     */
    private java.lang.String _nrOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdControleMensagemSalarial
     * 
     */
    public void deleteCdControleMensagemSalarial()
    {
        this._has_cdControleMensagemSalarial= false;
    } //-- void deleteCdControleMensagemSalarial() 

    /**
     * Method deleteCdIdiomaTextoMensagem
     * 
     */
    public void deleteCdIdiomaTextoMensagem()
    {
        this._has_cdIdiomaTextoMensagem= false;
    } //-- void deleteCdIdiomaTextoMensagem() 

    /**
     * Method deleteCdReciboGeradorMensagem
     * 
     */
    public void deleteCdReciboGeradorMensagem()
    {
        this._has_cdReciboGeradorMensagem= false;
    } //-- void deleteCdReciboGeradorMensagem() 

    /**
     * Method deleteNrEventoMensagemNegocio
     * 
     */
    public void deleteNrEventoMensagemNegocio()
    {
        this._has_nrEventoMensagemNegocio= false;
    } //-- void deleteNrEventoMensagemNegocio() 

    /**
     * Method deleteNrOrdemLinhaMensagem
     * 
     */
    public void deleteNrOrdemLinhaMensagem()
    {
        this._has_nrOrdemLinhaMensagem= false;
    } //-- void deleteNrOrdemLinhaMensagem() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdControleMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdControleMensagemSalarial'.
     */
    public int getCdControleMensagemSalarial()
    {
        return this._cdControleMensagemSalarial;
    } //-- int getCdControleMensagemSalarial() 

    /**
     * Returns the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaTextoMensagem'.
     */
    public int getCdIdiomaTextoMensagem()
    {
        return this._cdIdiomaTextoMensagem;
    } //-- int getCdIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'cdReciboGeradorMensagem'.
     * 
     * @return int
     * @return the value of field 'cdReciboGeradorMensagem'.
     */
    public int getCdReciboGeradorMensagem()
    {
        return this._cdReciboGeradorMensagem;
    } //-- int getCdReciboGeradorMensagem() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsControleMensagemSalarial'.
     * 
     * @return String
     * @return the value of field 'dsControleMensagemSalarial'.
     */
    public java.lang.String getDsControleMensagemSalarial()
    {
        return this._dsControleMensagemSalarial;
    } //-- java.lang.String getDsControleMensagemSalarial() 

    /**
     * Returns the value of field 'dsEventoMensagemNegocio'.
     * 
     * @return String
     * @return the value of field 'dsEventoMensagemNegocio'.
     */
    public java.lang.String getDsEventoMensagemNegocio()
    {
        return this._dsEventoMensagemNegocio;
    } //-- java.lang.String getDsEventoMensagemNegocio() 

    /**
     * Returns the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @return String
     * @return the value of field 'dsIdiomaTextoMensagem'.
     */
    public java.lang.String getDsIdiomaTextoMensagem()
    {
        return this._dsIdiomaTextoMensagem;
    } //-- java.lang.String getDsIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'dsReciboGeradorMensagem'.
     * 
     * @return String
     * @return the value of field 'dsReciboGeradorMensagem'.
     */
    public java.lang.String getDsReciboGeradorMensagem()
    {
        return this._dsReciboGeradorMensagem;
    } //-- java.lang.String getDsReciboGeradorMensagem() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nrEventoMensagemNegocio'.
     * 
     * @return int
     * @return the value of field 'nrEventoMensagemNegocio'.
     */
    public int getNrEventoMensagemNegocio()
    {
        return this._nrEventoMensagemNegocio;
    } //-- int getNrEventoMensagemNegocio() 

    /**
     * Returns the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoInclusao'.
     */
    public java.lang.String getNrOperacaoFluxoInclusao()
    {
        return this._nrOperacaoFluxoInclusao;
    } //-- java.lang.String getNrOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nrOperacaoFluxoManutencao'.
     */
    public java.lang.String getNrOperacaoFluxoManutencao()
    {
        return this._nrOperacaoFluxoManutencao;
    } //-- java.lang.String getNrOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrOrdemLinhaMensagem'.
     * 
     * @return long
     * @return the value of field 'nrOrdemLinhaMensagem'.
     */
    public long getNrOrdemLinhaMensagem()
    {
        return this._nrOrdemLinhaMensagem;
    } //-- long getNrOrdemLinhaMensagem() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdControleMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleMensagemSalarial()
    {
        return this._has_cdControleMensagemSalarial;
    } //-- boolean hasCdControleMensagemSalarial() 

    /**
     * Method hasCdIdiomaTextoMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaTextoMensagem()
    {
        return this._has_cdIdiomaTextoMensagem;
    } //-- boolean hasCdIdiomaTextoMensagem() 

    /**
     * Method hasCdReciboGeradorMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdReciboGeradorMensagem()
    {
        return this._has_cdReciboGeradorMensagem;
    } //-- boolean hasCdReciboGeradorMensagem() 

    /**
     * Method hasNrEventoMensagemNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoMensagemNegocio()
    {
        return this._has_nrEventoMensagemNegocio;
    } //-- boolean hasNrEventoMensagemNegocio() 

    /**
     * Method hasNrOrdemLinhaMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOrdemLinhaMensagem()
    {
        return this._has_nrOrdemLinhaMensagem;
    } //-- boolean hasNrOrdemLinhaMensagem() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdControleMensagemSalarial'.
     * 
     * @param cdControleMensagemSalarial the value of field
     * 'cdControleMensagemSalarial'.
     */
    public void setCdControleMensagemSalarial(int cdControleMensagemSalarial)
    {
        this._cdControleMensagemSalarial = cdControleMensagemSalarial;
        this._has_cdControleMensagemSalarial = true;
    } //-- void setCdControleMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @param cdIdiomaTextoMensagem the value of field
     * 'cdIdiomaTextoMensagem'.
     */
    public void setCdIdiomaTextoMensagem(int cdIdiomaTextoMensagem)
    {
        this._cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
        this._has_cdIdiomaTextoMensagem = true;
    } //-- void setCdIdiomaTextoMensagem(int) 

    /**
     * Sets the value of field 'cdReciboGeradorMensagem'.
     * 
     * @param cdReciboGeradorMensagem the value of field
     * 'cdReciboGeradorMensagem'.
     */
    public void setCdReciboGeradorMensagem(int cdReciboGeradorMensagem)
    {
        this._cdReciboGeradorMensagem = cdReciboGeradorMensagem;
        this._has_cdReciboGeradorMensagem = true;
    } //-- void setCdReciboGeradorMensagem(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsControleMensagemSalarial'.
     * 
     * @param dsControleMensagemSalarial the value of field
     * 'dsControleMensagemSalarial'.
     */
    public void setDsControleMensagemSalarial(java.lang.String dsControleMensagemSalarial)
    {
        this._dsControleMensagemSalarial = dsControleMensagemSalarial;
    } //-- void setDsControleMensagemSalarial(java.lang.String) 

    /**
     * Sets the value of field 'dsEventoMensagemNegocio'.
     * 
     * @param dsEventoMensagemNegocio the value of field
     * 'dsEventoMensagemNegocio'.
     */
    public void setDsEventoMensagemNegocio(java.lang.String dsEventoMensagemNegocio)
    {
        this._dsEventoMensagemNegocio = dsEventoMensagemNegocio;
    } //-- void setDsEventoMensagemNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @param dsIdiomaTextoMensagem the value of field
     * 'dsIdiomaTextoMensagem'.
     */
    public void setDsIdiomaTextoMensagem(java.lang.String dsIdiomaTextoMensagem)
    {
        this._dsIdiomaTextoMensagem = dsIdiomaTextoMensagem;
    } //-- void setDsIdiomaTextoMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsReciboGeradorMensagem'.
     * 
     * @param dsReciboGeradorMensagem the value of field
     * 'dsReciboGeradorMensagem'.
     */
    public void setDsReciboGeradorMensagem(java.lang.String dsReciboGeradorMensagem)
    {
        this._dsReciboGeradorMensagem = dsReciboGeradorMensagem;
    } //-- void setDsReciboGeradorMensagem(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nrEventoMensagemNegocio'.
     * 
     * @param nrEventoMensagemNegocio the value of field
     * 'nrEventoMensagemNegocio'.
     */
    public void setNrEventoMensagemNegocio(int nrEventoMensagemNegocio)
    {
        this._nrEventoMensagemNegocio = nrEventoMensagemNegocio;
        this._has_nrEventoMensagemNegocio = true;
    } //-- void setNrEventoMensagemNegocio(int) 

    /**
     * Sets the value of field 'nrOperacaoFluxoInclusao'.
     * 
     * @param nrOperacaoFluxoInclusao the value of field
     * 'nrOperacaoFluxoInclusao'.
     */
    public void setNrOperacaoFluxoInclusao(java.lang.String nrOperacaoFluxoInclusao)
    {
        this._nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
    } //-- void setNrOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nrOperacaoFluxoManutencao'.
     * 
     * @param nrOperacaoFluxoManutencao the value of field
     * 'nrOperacaoFluxoManutencao'.
     */
    public void setNrOperacaoFluxoManutencao(java.lang.String nrOperacaoFluxoManutencao)
    {
        this._nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
    } //-- void setNrOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrOrdemLinhaMensagem'.
     * 
     * @param nrOrdemLinhaMensagem the value of field
     * 'nrOrdemLinhaMensagem'.
     */
    public void setNrOrdemLinhaMensagem(long nrOrdemLinhaMensagem)
    {
        this._nrOrdemLinhaMensagem = nrOrdemLinhaMensagem;
        this._has_nrOrdemLinhaMensagem = true;
    } //-- void setNrOrdemLinhaMensagem(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhelinhamensagem.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
