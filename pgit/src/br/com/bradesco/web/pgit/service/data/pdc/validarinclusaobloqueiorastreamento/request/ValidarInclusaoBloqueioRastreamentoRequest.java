/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ValidarInclusaoBloqueioRastreamentoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ValidarInclusaoBloqueioRastreamentoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdCpfCnpjBloqueio
     */
    private long _cdCpfCnpjBloqueio = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjBloqueio
     */
    private boolean _has_cdCpfCnpjBloqueio;

    /**
     * Field _cdFilialCnpjBloqueio
     */
    private int _cdFilialCnpjBloqueio = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjBloqueio
     */
    private boolean _has_cdFilialCnpjBloqueio;

    /**
     * Field _cdControleCnpjCpf
     */
    private int _cdControleCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdControleCnpjCpf
     */
    private boolean _has_cdControleCnpjCpf;


      //----------------/
     //- Constructors -/
    //----------------/

    public ValidarInclusaoBloqueioRastreamentoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.request.ValidarInclusaoBloqueioRastreamentoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCnpjCpf
     * 
     */
    public void deleteCdControleCnpjCpf()
    {
        this._has_cdControleCnpjCpf= false;
    } //-- void deleteCdControleCnpjCpf() 

    /**
     * Method deleteCdCpfCnpjBloqueio
     * 
     */
    public void deleteCdCpfCnpjBloqueio()
    {
        this._has_cdCpfCnpjBloqueio= false;
    } //-- void deleteCdCpfCnpjBloqueio() 

    /**
     * Method deleteCdFilialCnpjBloqueio
     * 
     */
    public void deleteCdFilialCnpjBloqueio()
    {
        this._has_cdFilialCnpjBloqueio= false;
    } //-- void deleteCdFilialCnpjBloqueio() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdControleCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjCpf'.
     */
    public int getCdControleCnpjCpf()
    {
        return this._cdControleCnpjCpf;
    } //-- int getCdControleCnpjCpf() 

    /**
     * Returns the value of field 'cdCpfCnpjBloqueio'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjBloqueio'.
     */
    public long getCdCpfCnpjBloqueio()
    {
        return this._cdCpfCnpjBloqueio;
    } //-- long getCdCpfCnpjBloqueio() 

    /**
     * Returns the value of field 'cdFilialCnpjBloqueio'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjBloqueio'.
     */
    public int getCdFilialCnpjBloqueio()
    {
        return this._cdFilialCnpjBloqueio;
    } //-- int getCdFilialCnpjBloqueio() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdControleCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjCpf()
    {
        return this._has_cdControleCnpjCpf;
    } //-- boolean hasCdControleCnpjCpf() 

    /**
     * Method hasCdCpfCnpjBloqueio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjBloqueio()
    {
        return this._has_cdCpfCnpjBloqueio;
    } //-- boolean hasCdCpfCnpjBloqueio() 

    /**
     * Method hasCdFilialCnpjBloqueio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjBloqueio()
    {
        return this._has_cdFilialCnpjBloqueio;
    } //-- boolean hasCdFilialCnpjBloqueio() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCnpjCpf'.
     * 
     * @param cdControleCnpjCpf the value of field
     * 'cdControleCnpjCpf'.
     */
    public void setCdControleCnpjCpf(int cdControleCnpjCpf)
    {
        this._cdControleCnpjCpf = cdControleCnpjCpf;
        this._has_cdControleCnpjCpf = true;
    } //-- void setCdControleCnpjCpf(int) 

    /**
     * Sets the value of field 'cdCpfCnpjBloqueio'.
     * 
     * @param cdCpfCnpjBloqueio the value of field
     * 'cdCpfCnpjBloqueio'.
     */
    public void setCdCpfCnpjBloqueio(long cdCpfCnpjBloqueio)
    {
        this._cdCpfCnpjBloqueio = cdCpfCnpjBloqueio;
        this._has_cdCpfCnpjBloqueio = true;
    } //-- void setCdCpfCnpjBloqueio(long) 

    /**
     * Sets the value of field 'cdFilialCnpjBloqueio'.
     * 
     * @param cdFilialCnpjBloqueio the value of field
     * 'cdFilialCnpjBloqueio'.
     */
    public void setCdFilialCnpjBloqueio(int cdFilialCnpjBloqueio)
    {
        this._cdFilialCnpjBloqueio = cdFilialCnpjBloqueio;
        this._has_cdFilialCnpjBloqueio = true;
    } //-- void setCdFilialCnpjBloqueio(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ValidarInclusaoBloqueioRastreamentoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.request.ValidarInclusaoBloqueioRastreamentoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.request.ValidarInclusaoBloqueioRastreamentoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.request.ValidarInclusaoBloqueioRastreamentoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.validarinclusaobloqueiorastreamento.request.ValidarInclusaoBloqueioRastreamentoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
