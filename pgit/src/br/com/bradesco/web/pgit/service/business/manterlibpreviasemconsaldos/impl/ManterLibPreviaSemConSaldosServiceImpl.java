/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.IManterLibPreviaSemConSaldosService;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.IManterLibPreviaSemConSaldosServiceConstants;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.AlterarLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.AlterarLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarDetLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarDetLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ExcluirLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ExcluirLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.IncluirLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.IncluirLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.request.AlterarLiberacaoPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarliberacaoprevia.response.AlterarLiberacaoPreviaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetliberacaoprevia.request.ConsultarDetLiberacaoPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetliberacaoprevia.response.ConsultarDetLiberacaoPreviaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.request.ConsultarLiberacaoPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarliberacaoprevia.response.ConsultarLiberacaoPreviaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirliberacaoprevia.request.ExcluirLiberacaoPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirliberacaoprevia.response.ExcluirLiberacaoPreviaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirliberacaoprevia.request.IncluirLiberacaoPreviaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirliberacaoprevia.response.IncluirLiberacaoPreviaResponse;
import br.com.bradesco.web.pgit.utils.CamposUtils;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterLibPreviaSemConSaldos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterLibPreviaSemConSaldosServiceImpl implements IManterLibPreviaSemConSaldosService {
	
	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	 /**
     * Construtor.
     */
    public ManterLibPreviaSemConSaldosServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.IManterLibPreviaSemConSaldosService#consultarLiberacaoPrevia(br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarLiberacaoPreviaEntradaDTO)
     */
    public List<ConsultarLiberacaoPreviaSaidaDTO> consultarLiberacaoPrevia (ConsultarLiberacaoPreviaEntradaDTO entradaDTO){
    	
    	ConsultarLiberacaoPreviaRequest request = new ConsultarLiberacaoPreviaRequest();
    	ConsultarLiberacaoPreviaResponse response = new ConsultarLiberacaoPreviaResponse();
    	List<ConsultarLiberacaoPreviaSaidaDTO> listaSaida = new ArrayList<ConsultarLiberacaoPreviaSaidaDTO>();
    	
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
    	request.setCdPessoaParticipacao(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaParticipacao()));
    	request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContrato()));
    	request.setDtFinalLiberacao(PgitUtil.verificaStringNula(entradaDTO.getDtFinalLiberacao()));
    	request.setDtInicioLiberacao(PgitUtil.verificaStringNula(entradaDTO.getDtInicioLiberacao()));
    	request.setNmPessoaJuridica(PgitUtil.verificaLongNulo(entradaDTO.getNmPessoaJuridica()));
    	request.setNrSequencialContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrSequencialContrato()));
    	request.setNrOcorrencias(IManterLibPreviaSemConSaldosServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);
    	
    	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
    	
    	ConsultarLiberacaoPreviaSaidaDTO saidaDTO;
    	response = getFactoryAdapter().getConsultarLiberacaoPreviaPDCAdapter().invokeProcess(request);
    	    	
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		saidaDTO = new ConsultarLiberacaoPreviaSaidaDTO();
    		saidaDTO.setCodMensagem(response.getCodMensagem());
    		saidaDTO.setMensagem(response.getMensagem());
    		saidaDTO.setNrCpfCnpj(response.getOcorrencias(i).getNrCpfCnpj());
    		
    		String auxNumeroInscricao;
    		auxNumeroInscricao = SiteUtil.formatNumber((response.getOcorrencias(i).getNrCpfCnpj() != null ? response.getOcorrencias(i).getNrCpfCnpj().toString() : ""), 15);
    		if(!auxNumeroInscricao.equals("000000000000000")){
    			if(auxNumeroInscricao.substring(9, 13).equals("0000")){
    				auxNumeroInscricao = CpfCnpjUtils.formatCpfCnpj(auxNumeroInscricao, 1);
    			}else{
    				auxNumeroInscricao = CpfCnpjUtils.formatCpfCnpj(auxNumeroInscricao, 2);
    			}	
    		}else{
    			auxNumeroInscricao = "";
    		}
    		
    		saidaDTO.setNrCpfCnpjFormatado(auxNumeroInscricao);
    		
    		saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
    		saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());    		
    		saidaDTO.setCdTipoPessoaJuridica(response.getOcorrencias(i).getCdTipoPessoaJuridica());
    		saidaDTO.setNrSequencialContratoNegocio(response.getOcorrencias(i).getNrSequencialContratoNegocio());
    		saidaDTO.setDsCodigoPessoaJuridica(response.getOcorrencias(i).getDsCodigoPessoaJuridica());
    		saidaDTO.setCdTipoSolicitacao(response.getOcorrencias(i).getCdTipoSolicitacao());
    		saidaDTO.setNrSolicitacao(response.getOcorrencias(i).getNrSolicitacao());
    		saidaDTO.setDtLiberacao(response.getOcorrencias(i).getDtLiberacao());
    		saidaDTO.setDtLiberacaoFormatada(FormatarData.formatarDataFromPdc(response.getOcorrencias(i).getDtLiberacao()));
    		saidaDTO.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
    		saidaDTO.setDsBancoDebito(response.getOcorrencias(i).getDsBancoDebito());
    		saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
    		saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
    		saidaDTO.setDsAgenciaDebito(response.getOcorrencias(i).getDsAgenciaDebito());
    		saidaDTO.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
    		saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
    		saidaDTO.setVlAutorizado(response.getOcorrencias(i).getVlAutorizado());
    		saidaDTO.setVlUtilizado(response.getOcorrencias(i).getVlUtilizado());
    		saidaDTO.setQtPagamento(response.getOcorrencias(i).getQtPagamento());    		
    		saidaDTO.setQtPagamentoFormatado(nf.format(saidaDTO.getQtPagamento()));
    		
    		saidaDTO.setBancoAgenciaContaDebitoFormatada(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(), response.getOcorrencias(i).getCdAgenciaDebito(), response.getOcorrencias(i).getCdDigitoAgenciaDebito(), response.getOcorrencias(i).getCdContaDebito(), response.getOcorrencias(i).getCdDigitoContaDebito(), true));
    		
    		saidaDTO.setCdTipoContaDebito(response.getOcorrencias(i).getCdTipoContaDebito());
    		saidaDTO.setSituacaoSolicitacao(response.getOcorrencias(i).getSituacaoSolicitacao());
    		saidaDTO.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
    		listaSaida.add(saidaDTO);    		
    	}
    	
    	return listaSaida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.IManterLibPreviaSemConSaldosService#consultarDetLiberacaoPrevia(br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarDetLiberacaoPreviaEntradaDTO)
     */
    public ConsultarDetLiberacaoPreviaSaidaDTO consultarDetLiberacaoPrevia (ConsultarDetLiberacaoPreviaEntradaDTO entradaDTO) {
    	
    	ConsultarDetLiberacaoPreviaSaidaDTO saidaDTO = new ConsultarDetLiberacaoPreviaSaidaDTO();
    	ConsultarDetLiberacaoPreviaRequest request = new ConsultarDetLiberacaoPreviaRequest();
    	ConsultarDetLiberacaoPreviaResponse response = new ConsultarDetLiberacaoPreviaResponse();
		
		request.setCdPessoaJuridica(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridica()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContrato()));
		request.setCdTipoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoSolicitacao()));
		request.setNrSequencialContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrSequencialContrato()));
		request.setNrSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacao()));
		
		response = getFactoryAdapter().getConsultarDetLiberacaoPreviaPDCAdapter().invokeProcess(request);
		
		saidaDTO = new ConsultarDetLiberacaoPreviaSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDtInclusao(response.getDtInclusao());
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(response.getDtManutencao());
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		
    	saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(saidaDTO.getDtInclusao().replace(".", "/"), saidaDTO.getHrInclusao()));				
		saidaDTO.setTipoCanalInclusaoFormatado(CamposUtils.formatadorCampos(saidaDTO.getCdCanalInclusao(), saidaDTO.getDsCanalInclusao()));
		saidaDTO.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao()==null || saidaDTO.getCdFluxoInclusao().equals("0")? "" : saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(saidaDTO.getDtManutencao().replace(".", "/"), saidaDTO.getHrManutencao()));		
		saidaDTO.setTipoCanalManutencaoFormatado(CamposUtils.formatadorCampos(saidaDTO.getCdCanalManutencao(), saidaDTO.getDsCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO.getCdFluxoManutencao()==null || saidaDTO.getCdFluxoManutencao().equals("0")? "" : saidaDTO.getCdFluxoManutencao());
		
		saidaDTO.setMotivoSolicitacao(response.getMotivoSolicitacao());
		
		return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.IManterLibPreviaSemConSaldosService#alterarLiberacaoPrevia(br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.AlterarLiberacaoPreviaEntradaDTO)
     */
    public AlterarLiberacaoPreviaSaidaDTO alterarLiberacaoPrevia (AlterarLiberacaoPreviaEntradaDTO entradaDTO){
    	
    	AlterarLiberacaoPreviaSaidaDTO saidaDTO = new AlterarLiberacaoPreviaSaidaDTO();
    	AlterarLiberacaoPreviaRequest request = new AlterarLiberacaoPreviaRequest();
    	AlterarLiberacaoPreviaResponse response = new AlterarLiberacaoPreviaResponse();
    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setCdTipoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoSolicitacao()));
    	request.setDtProgramada(PgitUtil.verificaStringNula(entradaDTO.getDtProgramada()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setNrSolicitacao(PgitUtil.verificaLongNulo(entradaDTO.getNrSolicitacao()));
    	request.setVlAutorizado(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlAutorizado()));
    	
    	response = getFactoryAdapter().getAlterarLiberacaoPreviaPDCAdapter().invokeProcess(request);
 	   	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
    	saidaDTO.setMensagem(response.getMensagem());
    	
    	return saidaDTO;
 	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.IManterLibPreviaSemConSaldosService#excluirLiberacaoPrevia(br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ExcluirLiberacaoPreviaEntradaDTO)
     */
    public ExcluirLiberacaoPreviaSaidaDTO excluirLiberacaoPrevia (ExcluirLiberacaoPreviaEntradaDTO entradaDTO) {
		
    	ExcluirLiberacaoPreviaSaidaDTO saidaDTO = new ExcluirLiberacaoPreviaSaidaDTO();
		ExcluirLiberacaoPreviaRequest request = new ExcluirLiberacaoPreviaRequest();
		ExcluirLiberacaoPreviaResponse response = new ExcluirLiberacaoPreviaResponse();
		
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setCdTipoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoSolicitacao()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setNrSolicitacao(PgitUtil.verificaLongNulo(entradaDTO.getNrSolicitacao()));
	
		response = getFactoryAdapter().getExcluirLiberacaoPreviaPDCAdapter().invokeProcess(request);	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
   
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.IManterLibPreviaSemConSaldosService#incluirLiberacaoPrevia(br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.IncluirLiberacaoPreviaEntradaDTO)
     */
    public IncluirLiberacaoPreviaSaidaDTO incluirLiberacaoPrevia(IncluirLiberacaoPreviaEntradaDTO entradaDTO) {
		
    	IncluirLiberacaoPreviaSaidaDTO saidaDTO = new IncluirLiberacaoPreviaSaidaDTO();
		IncluirLiberacaoPreviaRequest request = new IncluirLiberacaoPreviaRequest();
		IncluirLiberacaoPreviaResponse response = new IncluirLiberacaoPreviaResponse();
		
		request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));
		request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));
		request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));
		if(entradaDTO.getCdDigitoContaDebito() != null){
		    request.setCdDigitoContaDebito(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoContaDebito()));
		}else{
		    request.setCdDigitoContaDebito(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoContaDebito()));
		}
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setDtProgramada(PgitUtil.verificaStringNula(entradaDTO.getDtProgramada()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setVlAutorizado(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlAutorizado()));
				
		response = getFactoryAdapter().getIncluirLiberacaoPreviaPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
	
		return saidaDTO;
	}
 
}

