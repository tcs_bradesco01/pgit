/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;

/**
 * Nome: ImprimirContratoOcorrencias7SaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoOcorrencias7SaidaDTO {

    /** The ds produto salario internet banking. */
    private String dsProdutoSalarioInternetBanking = null;

    /** The ds servico salario internet banking. */
    private String dsServicoSalarioInternetBanking = null;

    /**
     * Instancia um novo ImprimirContratoOcorrencias7SaidaDTO
     *
     * @see
     */
    public ImprimirContratoOcorrencias7SaidaDTO() {
        super();
    }

    /**
     * Sets the ds produto salario internet banking.
     *
     * @param dsProdutoSalarioInternetBanking the ds produto salario internet banking
     */
    public void setDsProdutoSalarioInternetBanking(String dsProdutoSalarioInternetBanking) {
        this.dsProdutoSalarioInternetBanking = dsProdutoSalarioInternetBanking;
    }

    /**
     * Gets the ds produto salario internet banking.
     *
     * @return the ds produto salario internet banking
     */
    public String getDsProdutoSalarioInternetBanking() {
        return this.dsProdutoSalarioInternetBanking;
    }

    /**
     * Sets the ds servico salario internet banking.
     *
     * @param dsServicoSalarioInternetBanking the ds servico salario internet banking
     */
    public void setDsServicoSalarioInternetBanking(String dsServicoSalarioInternetBanking) {
        this.dsServicoSalarioInternetBanking = dsServicoSalarioInternetBanking;
    }

    /**
     * Gets the ds servico salario internet banking.
     *
     * @return the ds servico salario internet banking
     */
    public String getDsServicoSalarioInternetBanking() {
        return this.dsServicoSalarioInternetBanking;
    }
}