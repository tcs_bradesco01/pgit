/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarSolRecPagtosVencNaoPagoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarSolRecPagtosVencNaoPagoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ExcluirSolRecuperacaoPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ExcluirSolRecuperacaoPagtosSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManSolRecPagVencNaoPag
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManSolRecPagVencNaoPagService {
	
	/**
	 * Consultar sol rec pagtos venc nao pago.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar sol rec pagtos venc nao pago saida dt o>
	 */
	List<ConsultarSolRecPagtosVencNaoPagoSaidaDTO> consultarSolRecPagtosVencNaoPago(ConsultarSolRecPagtosVencNaoPagoEntradaDTO entradaDTO);
	
	/**
	 * Con pagtos sol rec ven nao pago.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar pagtos solicitacao recuperacao saida dt o>
	 */
	List<ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO> conPagtosSolRecVenNaoPago(ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO entradaDTO);
	
	/**
	 * Excluir sol recuperacao pagtos.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir sol recuperacao pagtos saida dto
	 */
	ExcluirSolRecuperacaoPagtosSaidaDTO excluirSolRecuperacaoPagtos(ExcluirSolRecuperacaoPagtosEntradaDTO entradaDTO);
	
	
	
	
 
}

