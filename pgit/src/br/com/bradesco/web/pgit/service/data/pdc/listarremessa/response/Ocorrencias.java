/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarremessa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field descricaoProdutoPagamento
     */
    private java.lang.String descricaoProdutoPagamento;

    /**
     * Field _codigoSistema
     */
    private java.lang.String _codigoSistema;

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filial
     */
    private int _filial = 0;

    /**
     * keeps track of state for field: _filial
     */
    private boolean _has_filial;

    /**
     * Field _controle
     */
    private int _controle = 0;

    /**
     * keeps track of state for field: _controle
     */
    private boolean _has_controle;

    /**
     * Field _nomeRazaoSocial
     */
    private java.lang.String _nomeRazaoSocial;

    /**
     * Field _perfilComunicacao
     */
    private long _perfilComunicacao = 0;

    /**
     * keeps track of state for field: _perfilComunicacao
     */
    private boolean _has_perfilComunicacao;

    /**
     * Field _numeroArquivoRemessa
     */
    private long _numeroArquivoRemessa = 0;

    /**
     * keeps track of state for field: _numeroArquivoRemessa
     */
    private boolean _has_numeroArquivoRemessa;

    /**
     * Field _horaInclusaoRemessa
     */
    private java.lang.String _horaInclusaoRemessa;

    /**
     * Field _bancoDebito
     */
    private int _bancoDebito = 0;

    /**
     * keeps track of state for field: _bancoDebito
     */
    private boolean _has_bancoDebito;

    /**
     * Field _agenciaBancariaDebito
     */
    private int _agenciaBancariaDebito = 0;

    /**
     * keeps track of state for field: _agenciaBancariaDebito
     */
    private boolean _has_agenciaBancariaDebito;

    /**
     * Field _razaoContaDebito
     */
    private int _razaoContaDebito = 0;

    /**
     * keeps track of state for field: _razaoContaDebito
     */
    private boolean _has_razaoContaDebito;

    /**
     * Field _contaDebito
     */
    private long _contaDebito = 0;

    /**
     * keeps track of state for field: _contaDebito
     */
    private boolean _has_contaDebito;

    /**
     * Field _codigoLancamento
     */
    private int _codigoLancamento = 0;

    /**
     * keeps track of state for field: _codigoLancamento
     */
    private boolean _has_codigoLancamento;

    /**
     * Field _codigoLayoutArquivo
     */
    private int _codigoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _codigoLayoutArquivo
     */
    private boolean _has_codigoLayoutArquivo;

    /**
     * Field _descricaoLayoutArquivo
     */
    private java.lang.String _descricaoLayoutArquivo;

    /**
     * Field _codigoMeioTransmissao
     */
    private int _codigoMeioTransmissao = 0;

    /**
     * keeps track of state for field: _codigoMeioTransmissao
     */
    private boolean _has_codigoMeioTransmissao;

    /**
     * Field _descricaoMeioTransmissao
     */
    private java.lang.String _descricaoMeioTransmissao;

    /**
     * Field _codigoAmbiente
     */
    private java.lang.String _codigoAmbiente;

    /**
     * Field _descricaoAmbiente
     */
    private java.lang.String _descricaoAmbiente;

    /**
     * Field _horaRecepcaoRemessa
     */
    private java.lang.String _horaRecepcaoRemessa;

    /**
     * Field _situacaoProcessamento
     */
    private int _situacaoProcessamento = 0;

    /**
     * keeps track of state for field: _situacaoProcessamento
     */
    private boolean _has_situacaoProcessamento;

    /**
     * Field _descricaoProcesssamento
     */
    private java.lang.String _descricaoProcesssamento;

    /**
     * Field _codigoProcessamentoRemessa
     */
    private int _codigoProcessamentoRemessa = 0;

    /**
     * keeps track of state for field: _codigoProcessamentoRemessa
     */
    private boolean _has_codigoProcessamentoRemessa;

    /**
     * Field _descricaoProcessamentoRemessa
     */
    private java.lang.String _descricaoProcessamentoRemessa;

    /**
     * Field _quantidadeRegistroRemessa
     */
    private long _quantidadeRegistroRemessa = 0;

    /**
     * keeps track of state for field: _quantidadeRegistroRemessa
     */
    private boolean _has_quantidadeRegistroRemessa;

    /**
     * Field _valorRegistroRemessa
     */
    private double _valorRegistroRemessa = 0;

    /**
     * keeps track of state for field: _valorRegistroRemessa
     */
    private boolean _has_valorRegistroRemessa;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarremessa.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteAgenciaBancariaDebito
     * 
     */
    public void deleteAgenciaBancariaDebito()
    {
        this._has_agenciaBancariaDebito= false;
    } //-- void deleteAgenciaBancariaDebito() 

    /**
     * Method deleteBancoDebito
     * 
     */
    public void deleteBancoDebito()
    {
        this._has_bancoDebito= false;
    } //-- void deleteBancoDebito() 

    /**
     * Method deleteCodigoLancamento
     * 
     */
    public void deleteCodigoLancamento()
    {
        this._has_codigoLancamento= false;
    } //-- void deleteCodigoLancamento() 

    /**
     * Method deleteCodigoLayoutArquivo
     * 
     */
    public void deleteCodigoLayoutArquivo()
    {
        this._has_codigoLayoutArquivo= false;
    } //-- void deleteCodigoLayoutArquivo() 

    /**
     * Method deleteCodigoMeioTransmissao
     * 
     */
    public void deleteCodigoMeioTransmissao()
    {
        this._has_codigoMeioTransmissao= false;
    } //-- void deleteCodigoMeioTransmissao() 

    /**
     * Method deleteCodigoProcessamentoRemessa
     * 
     */
    public void deleteCodigoProcessamentoRemessa()
    {
        this._has_codigoProcessamentoRemessa= false;
    } //-- void deleteCodigoProcessamentoRemessa() 

    /**
     * Method deleteContaDebito
     * 
     */
    public void deleteContaDebito()
    {
        this._has_contaDebito= false;
    } //-- void deleteContaDebito() 

    /**
     * Method deleteControle
     * 
     */
    public void deleteControle()
    {
        this._has_controle= false;
    } //-- void deleteControle() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilial
     * 
     */
    public void deleteFilial()
    {
        this._has_filial= false;
    } //-- void deleteFilial() 

    /**
     * Method deleteNumeroArquivoRemessa
     * 
     */
    public void deleteNumeroArquivoRemessa()
    {
        this._has_numeroArquivoRemessa= false;
    } //-- void deleteNumeroArquivoRemessa() 

    /**
     * Method deletePerfilComunicacao
     * 
     */
    public void deletePerfilComunicacao()
    {
        this._has_perfilComunicacao= false;
    } //-- void deletePerfilComunicacao() 

    /**
     * Method deleteQuantidadeRegistroRemessa
     * 
     */
    public void deleteQuantidadeRegistroRemessa()
    {
        this._has_quantidadeRegistroRemessa= false;
    } //-- void deleteQuantidadeRegistroRemessa() 

    /**
     * Method deleteRazaoContaDebito
     * 
     */
    public void deleteRazaoContaDebito()
    {
        this._has_razaoContaDebito= false;
    } //-- void deleteRazaoContaDebito() 

    /**
     * Method deleteSituacaoProcessamento
     * 
     */
    public void deleteSituacaoProcessamento()
    {
        this._has_situacaoProcessamento= false;
    } //-- void deleteSituacaoProcessamento() 

    /**
     * Method deleteValorRegistroRemessa
     * 
     */
    public void deleteValorRegistroRemessa()
    {
        this._has_valorRegistroRemessa= false;
    } //-- void deleteValorRegistroRemessa() 

    /**
     * Returns the value of field 'agenciaBancariaDebito'.
     * 
     * @return int
     * @return the value of field 'agenciaBancariaDebito'.
     */
    public int getAgenciaBancariaDebito()
    {
        return this._agenciaBancariaDebito;
    } //-- int getAgenciaBancariaDebito() 

    /**
     * Returns the value of field 'bancoDebito'.
     * 
     * @return int
     * @return the value of field 'bancoDebito'.
     */
    public int getBancoDebito()
    {
        return this._bancoDebito;
    } //-- int getBancoDebito() 

    /**
     * Returns the value of field 'codigoAmbiente'.
     * 
     * @return String
     * @return the value of field 'codigoAmbiente'.
     */
    public java.lang.String getCodigoAmbiente()
    {
        return this._codigoAmbiente;
    } //-- java.lang.String getCodigoAmbiente() 

    /**
     * Returns the value of field 'codigoLancamento'.
     * 
     * @return int
     * @return the value of field 'codigoLancamento'.
     */
    public int getCodigoLancamento()
    {
        return this._codigoLancamento;
    } //-- int getCodigoLancamento() 

    /**
     * Returns the value of field 'codigoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'codigoLayoutArquivo'.
     */
    public int getCodigoLayoutArquivo()
    {
        return this._codigoLayoutArquivo;
    } //-- int getCodigoLayoutArquivo() 

    /**
     * Returns the value of field 'codigoMeioTransmissao'.
     * 
     * @return int
     * @return the value of field 'codigoMeioTransmissao'.
     */
    public int getCodigoMeioTransmissao()
    {
        return this._codigoMeioTransmissao;
    } //-- int getCodigoMeioTransmissao() 

    /**
     * Returns the value of field 'codigoProcessamentoRemessa'.
     * 
     * @return int
     * @return the value of field 'codigoProcessamentoRemessa'.
     */
    public int getCodigoProcessamentoRemessa()
    {
        return this._codigoProcessamentoRemessa;
    } //-- int getCodigoProcessamentoRemessa() 

    /**
     * Returns the value of field 'codigoSistema'.
     * 
     * @return String
     * @return the value of field 'codigoSistema'.
     */
    public java.lang.String getCodigoSistema()
    {
        return this._codigoSistema;
    } //-- java.lang.String getCodigoSistema() 

    /**
     * Returns the value of field 'contaDebito'.
     * 
     * @return long
     * @return the value of field 'contaDebito'.
     */
    public long getContaDebito()
    {
        return this._contaDebito;
    } //-- long getContaDebito() 

    /**
     * Returns the value of field 'controle'.
     * 
     * @return int
     * @return the value of field 'controle'.
     */
    public int getControle()
    {
        return this._controle;
    } //-- int getControle() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'descricaoAmbiente'.
     * 
     * @return String
     * @return the value of field 'descricaoAmbiente'.
     */
    public java.lang.String getDescricaoAmbiente()
    {
        return this._descricaoAmbiente;
    } //-- java.lang.String getDescricaoAmbiente() 

    /**
     * Returns the value of field 'descricaoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'descricaoLayoutArquivo'.
     */
    public java.lang.String getDescricaoLayoutArquivo()
    {
        return this._descricaoLayoutArquivo;
    } //-- java.lang.String getDescricaoLayoutArquivo() 

    /**
     * Returns the value of field 'descricaoMeioTransmissao'.
     * 
     * @return String
     * @return the value of field 'descricaoMeioTransmissao'.
     */
    public java.lang.String getDescricaoMeioTransmissao()
    {
        return this._descricaoMeioTransmissao;
    } //-- java.lang.String getDescricaoMeioTransmissao() 

    /**
     * Returns the value of field 'descricaoProcessamentoRemessa'.
     * 
     * @return String
     * @return the value of field 'descricaoProcessamentoRemessa'.
     */
    public java.lang.String getDescricaoProcessamentoRemessa()
    {
        return this._descricaoProcessamentoRemessa;
    } //-- java.lang.String getDescricaoProcessamentoRemessa() 

    /**
     * Returns the value of field 'descricaoProcesssamento'.
     * 
     * @return String
     * @return the value of field 'descricaoProcesssamento'.
     */
    public java.lang.String getDescricaoProcesssamento()
    {
        return this._descricaoProcesssamento;
    } //-- java.lang.String getDescricaoProcesssamento() 

    /**
     * Returns the value of field 'descricaoProdutoPagamento'.
     * 
     * @return String
     * @return the value of field 'descricaoProdutoPagamento'.
     */
    public java.lang.String getDescricaoProdutoPagamento()
    {
        return this.descricaoProdutoPagamento;
    } //-- java.lang.String getDescricaoProdutoPagamento() 

    /**
     * Returns the value of field 'filial'.
     * 
     * @return int
     * @return the value of field 'filial'.
     */
    public int getFilial()
    {
        return this._filial;
    } //-- int getFilial() 

    /**
     * Returns the value of field 'horaInclusaoRemessa'.
     * 
     * @return String
     * @return the value of field 'horaInclusaoRemessa'.
     */
    public java.lang.String getHoraInclusaoRemessa()
    {
        return this._horaInclusaoRemessa;
    } //-- java.lang.String getHoraInclusaoRemessa() 

    /**
     * Returns the value of field 'horaRecepcaoRemessa'.
     * 
     * @return String
     * @return the value of field 'horaRecepcaoRemessa'.
     */
    public java.lang.String getHoraRecepcaoRemessa()
    {
        return this._horaRecepcaoRemessa;
    } //-- java.lang.String getHoraRecepcaoRemessa() 

    /**
     * Returns the value of field 'nomeRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'nomeRazaoSocial'.
     */
    public java.lang.String getNomeRazaoSocial()
    {
        return this._nomeRazaoSocial;
    } //-- java.lang.String getNomeRazaoSocial() 

    /**
     * Returns the value of field 'numeroArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'numeroArquivoRemessa'.
     */
    public long getNumeroArquivoRemessa()
    {
        return this._numeroArquivoRemessa;
    } //-- long getNumeroArquivoRemessa() 

    /**
     * Returns the value of field 'perfilComunicacao'.
     * 
     * @return long
     * @return the value of field 'perfilComunicacao'.
     */
    public long getPerfilComunicacao()
    {
        return this._perfilComunicacao;
    } //-- long getPerfilComunicacao() 

    /**
     * Returns the value of field 'quantidadeRegistroRemessa'.
     * 
     * @return long
     * @return the value of field 'quantidadeRegistroRemessa'.
     */
    public long getQuantidadeRegistroRemessa()
    {
        return this._quantidadeRegistroRemessa;
    } //-- long getQuantidadeRegistroRemessa() 

    /**
     * Returns the value of field 'razaoContaDebito'.
     * 
     * @return int
     * @return the value of field 'razaoContaDebito'.
     */
    public int getRazaoContaDebito()
    {
        return this._razaoContaDebito;
    } //-- int getRazaoContaDebito() 

    /**
     * Returns the value of field 'situacaoProcessamento'.
     * 
     * @return int
     * @return the value of field 'situacaoProcessamento'.
     */
    public int getSituacaoProcessamento()
    {
        return this._situacaoProcessamento;
    } //-- int getSituacaoProcessamento() 

    /**
     * Returns the value of field 'valorRegistroRemessa'.
     * 
     * @return double
     * @return the value of field 'valorRegistroRemessa'.
     */
    public double getValorRegistroRemessa()
    {
        return this._valorRegistroRemessa;
    } //-- double getValorRegistroRemessa() 

    /**
     * Method hasAgenciaBancariaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasAgenciaBancariaDebito()
    {
        return this._has_agenciaBancariaDebito;
    } //-- boolean hasAgenciaBancariaDebito() 

    /**
     * Method hasBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasBancoDebito()
    {
        return this._has_bancoDebito;
    } //-- boolean hasBancoDebito() 

    /**
     * Method hasCodigoLancamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoLancamento()
    {
        return this._has_codigoLancamento;
    } //-- boolean hasCodigoLancamento() 

    /**
     * Method hasCodigoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoLayoutArquivo()
    {
        return this._has_codigoLayoutArquivo;
    } //-- boolean hasCodigoLayoutArquivo() 

    /**
     * Method hasCodigoMeioTransmissao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoMeioTransmissao()
    {
        return this._has_codigoMeioTransmissao;
    } //-- boolean hasCodigoMeioTransmissao() 

    /**
     * Method hasCodigoProcessamentoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoProcessamentoRemessa()
    {
        return this._has_codigoProcessamentoRemessa;
    } //-- boolean hasCodigoProcessamentoRemessa() 

    /**
     * Method hasContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasContaDebito()
    {
        return this._has_contaDebito;
    } //-- boolean hasContaDebito() 

    /**
     * Method hasControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControle()
    {
        return this._has_controle;
    } //-- boolean hasControle() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilial()
    {
        return this._has_filial;
    } //-- boolean hasFilial() 

    /**
     * Method hasNumeroArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroArquivoRemessa()
    {
        return this._has_numeroArquivoRemessa;
    } //-- boolean hasNumeroArquivoRemessa() 

    /**
     * Method hasPerfilComunicacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPerfilComunicacao()
    {
        return this._has_perfilComunicacao;
    } //-- boolean hasPerfilComunicacao() 

    /**
     * Method hasQuantidadeRegistroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeRegistroRemessa()
    {
        return this._has_quantidadeRegistroRemessa;
    } //-- boolean hasQuantidadeRegistroRemessa() 

    /**
     * Method hasRazaoContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasRazaoContaDebito()
    {
        return this._has_razaoContaDebito;
    } //-- boolean hasRazaoContaDebito() 

    /**
     * Method hasSituacaoProcessamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasSituacaoProcessamento()
    {
        return this._has_situacaoProcessamento;
    } //-- boolean hasSituacaoProcessamento() 

    /**
     * Method hasValorRegistroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorRegistroRemessa()
    {
        return this._has_valorRegistroRemessa;
    } //-- boolean hasValorRegistroRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'agenciaBancariaDebito'.
     * 
     * @param agenciaBancariaDebito the value of field
     * 'agenciaBancariaDebito'.
     */
    public void setAgenciaBancariaDebito(int agenciaBancariaDebito)
    {
        this._agenciaBancariaDebito = agenciaBancariaDebito;
        this._has_agenciaBancariaDebito = true;
    } //-- void setAgenciaBancariaDebito(int) 

    /**
     * Sets the value of field 'bancoDebito'.
     * 
     * @param bancoDebito the value of field 'bancoDebito'.
     */
    public void setBancoDebito(int bancoDebito)
    {
        this._bancoDebito = bancoDebito;
        this._has_bancoDebito = true;
    } //-- void setBancoDebito(int) 

    /**
     * Sets the value of field 'codigoAmbiente'.
     * 
     * @param codigoAmbiente the value of field 'codigoAmbiente'.
     */
    public void setCodigoAmbiente(java.lang.String codigoAmbiente)
    {
        this._codigoAmbiente = codigoAmbiente;
    } //-- void setCodigoAmbiente(java.lang.String) 

    /**
     * Sets the value of field 'codigoLancamento'.
     * 
     * @param codigoLancamento the value of field 'codigoLancamento'
     */
    public void setCodigoLancamento(int codigoLancamento)
    {
        this._codigoLancamento = codigoLancamento;
        this._has_codigoLancamento = true;
    } //-- void setCodigoLancamento(int) 

    /**
     * Sets the value of field 'codigoLayoutArquivo'.
     * 
     * @param codigoLayoutArquivo the value of field
     * 'codigoLayoutArquivo'.
     */
    public void setCodigoLayoutArquivo(int codigoLayoutArquivo)
    {
        this._codigoLayoutArquivo = codigoLayoutArquivo;
        this._has_codigoLayoutArquivo = true;
    } //-- void setCodigoLayoutArquivo(int) 

    /**
     * Sets the value of field 'codigoMeioTransmissao'.
     * 
     * @param codigoMeioTransmissao the value of field
     * 'codigoMeioTransmissao'.
     */
    public void setCodigoMeioTransmissao(int codigoMeioTransmissao)
    {
        this._codigoMeioTransmissao = codigoMeioTransmissao;
        this._has_codigoMeioTransmissao = true;
    } //-- void setCodigoMeioTransmissao(int) 

    /**
     * Sets the value of field 'codigoProcessamentoRemessa'.
     * 
     * @param codigoProcessamentoRemessa the value of field
     * 'codigoProcessamentoRemessa'.
     */
    public void setCodigoProcessamentoRemessa(int codigoProcessamentoRemessa)
    {
        this._codigoProcessamentoRemessa = codigoProcessamentoRemessa;
        this._has_codigoProcessamentoRemessa = true;
    } //-- void setCodigoProcessamentoRemessa(int) 

    /**
     * Sets the value of field 'codigoSistema'.
     * 
     * @param codigoSistema the value of field 'codigoSistema'.
     */
    public void setCodigoSistema(java.lang.String codigoSistema)
    {
        this._codigoSistema = codigoSistema;
    } //-- void setCodigoSistema(java.lang.String) 

    /**
     * Sets the value of field 'contaDebito'.
     * 
     * @param contaDebito the value of field 'contaDebito'.
     */
    public void setContaDebito(long contaDebito)
    {
        this._contaDebito = contaDebito;
        this._has_contaDebito = true;
    } //-- void setContaDebito(long) 

    /**
     * Sets the value of field 'controle'.
     * 
     * @param controle the value of field 'controle'.
     */
    public void setControle(int controle)
    {
        this._controle = controle;
        this._has_controle = true;
    } //-- void setControle(int) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'descricaoAmbiente'.
     * 
     * @param descricaoAmbiente the value of field
     * 'descricaoAmbiente'.
     */
    public void setDescricaoAmbiente(java.lang.String descricaoAmbiente)
    {
        this._descricaoAmbiente = descricaoAmbiente;
    } //-- void setDescricaoAmbiente(java.lang.String) 

    /**
     * Sets the value of field 'descricaoLayoutArquivo'.
     * 
     * @param descricaoLayoutArquivo the value of field
     * 'descricaoLayoutArquivo'.
     */
    public void setDescricaoLayoutArquivo(java.lang.String descricaoLayoutArquivo)
    {
        this._descricaoLayoutArquivo = descricaoLayoutArquivo;
    } //-- void setDescricaoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'descricaoMeioTransmissao'.
     * 
     * @param descricaoMeioTransmissao the value of field
     * 'descricaoMeioTransmissao'.
     */
    public void setDescricaoMeioTransmissao(java.lang.String descricaoMeioTransmissao)
    {
        this._descricaoMeioTransmissao = descricaoMeioTransmissao;
    } //-- void setDescricaoMeioTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'descricaoProcessamentoRemessa'.
     * 
     * @param descricaoProcessamentoRemessa the value of field
     * 'descricaoProcessamentoRemessa'.
     */
    public void setDescricaoProcessamentoRemessa(java.lang.String descricaoProcessamentoRemessa)
    {
        this._descricaoProcessamentoRemessa = descricaoProcessamentoRemessa;
    } //-- void setDescricaoProcessamentoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'descricaoProcesssamento'.
     * 
     * @param descricaoProcesssamento the value of field
     * 'descricaoProcesssamento'.
     */
    public void setDescricaoProcesssamento(java.lang.String descricaoProcesssamento)
    {
        this._descricaoProcesssamento = descricaoProcesssamento;
    } //-- void setDescricaoProcesssamento(java.lang.String) 

    /**
     * Sets the value of field 'descricaoProdutoPagamento'.
     * 
     * @param descricaoProdutoPagamento the value of field
     * 'descricaoProdutoPagamento'.
     */
    public void setDescricaoProdutoPagamento(java.lang.String descricaoProdutoPagamento)
    {
        this.descricaoProdutoPagamento = descricaoProdutoPagamento;
    } //-- void setDescricaoProdutoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'filial'.
     * 
     * @param filial the value of field 'filial'.
     */
    public void setFilial(int filial)
    {
        this._filial = filial;
        this._has_filial = true;
    } //-- void setFilial(int) 

    /**
     * Sets the value of field 'horaInclusaoRemessa'.
     * 
     * @param horaInclusaoRemessa the value of field
     * 'horaInclusaoRemessa'.
     */
    public void setHoraInclusaoRemessa(java.lang.String horaInclusaoRemessa)
    {
        this._horaInclusaoRemessa = horaInclusaoRemessa;
    } //-- void setHoraInclusaoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'horaRecepcaoRemessa'.
     * 
     * @param horaRecepcaoRemessa the value of field
     * 'horaRecepcaoRemessa'.
     */
    public void setHoraRecepcaoRemessa(java.lang.String horaRecepcaoRemessa)
    {
        this._horaRecepcaoRemessa = horaRecepcaoRemessa;
    } //-- void setHoraRecepcaoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'nomeRazaoSocial'.
     * 
     * @param nomeRazaoSocial the value of field 'nomeRazaoSocial'.
     */
    public void setNomeRazaoSocial(java.lang.String nomeRazaoSocial)
    {
        this._nomeRazaoSocial = nomeRazaoSocial;
    } //-- void setNomeRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'numeroArquivoRemessa'.
     * 
     * @param numeroArquivoRemessa the value of field
     * 'numeroArquivoRemessa'.
     */
    public void setNumeroArquivoRemessa(long numeroArquivoRemessa)
    {
        this._numeroArquivoRemessa = numeroArquivoRemessa;
        this._has_numeroArquivoRemessa = true;
    } //-- void setNumeroArquivoRemessa(long) 

    /**
     * Sets the value of field 'perfilComunicacao'.
     * 
     * @param perfilComunicacao the value of field
     * 'perfilComunicacao'.
     */
    public void setPerfilComunicacao(long perfilComunicacao)
    {
        this._perfilComunicacao = perfilComunicacao;
        this._has_perfilComunicacao = true;
    } //-- void setPerfilComunicacao(long) 

    /**
     * Sets the value of field 'quantidadeRegistroRemessa'.
     * 
     * @param quantidadeRegistroRemessa the value of field
     * 'quantidadeRegistroRemessa'.
     */
    public void setQuantidadeRegistroRemessa(long quantidadeRegistroRemessa)
    {
        this._quantidadeRegistroRemessa = quantidadeRegistroRemessa;
        this._has_quantidadeRegistroRemessa = true;
    } //-- void setQuantidadeRegistroRemessa(long) 

    /**
     * Sets the value of field 'razaoContaDebito'.
     * 
     * @param razaoContaDebito the value of field 'razaoContaDebito'
     */
    public void setRazaoContaDebito(int razaoContaDebito)
    {
        this._razaoContaDebito = razaoContaDebito;
        this._has_razaoContaDebito = true;
    } //-- void setRazaoContaDebito(int) 

    /**
     * Sets the value of field 'situacaoProcessamento'.
     * 
     * @param situacaoProcessamento the value of field
     * 'situacaoProcessamento'.
     */
    public void setSituacaoProcessamento(int situacaoProcessamento)
    {
        this._situacaoProcessamento = situacaoProcessamento;
        this._has_situacaoProcessamento = true;
    } //-- void setSituacaoProcessamento(int) 

    /**
     * Sets the value of field 'valorRegistroRemessa'.
     * 
     * @param valorRegistroRemessa the value of field
     * 'valorRegistroRemessa'.
     */
    public void setValorRegistroRemessa(double valorRegistroRemessa)
    {
        this._valorRegistroRemessa = valorRegistroRemessa;
        this._has_valorRegistroRemessa = true;
    } //-- void setValorRegistroRemessa(double) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarremessa.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarremessa.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarremessa.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarremessa.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
