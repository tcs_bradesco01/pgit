/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaomovfavorecido.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: ExcluirSolEmiAviMovtoFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirSolEmiAviMovtoFavorecidoEntradaDTO{
    
    /** Atributo cdSolicitacaoPagamentoIntegrado. */
    private Integer cdSolicitacaoPagamentoIntegrado;
    
    /** Atributo nrSolicitacaoPagamentoIntegrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo nrRegistroEntrada. */
    private Integer nrRegistroEntrada;
    
    /** Atributo listaMovimentacoes. */
    private List<OcorrenciasExcluirSolEmiAviMovtoFavorecidoEntradaDTO> listaMovimentacoes = new ArrayList<OcorrenciasExcluirSolEmiAviMovtoFavorecidoEntradaDTO>();
    
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrRegistroEntrada.
	 *
	 * @return nrRegistroEntrada
	 */
	public Integer getNrRegistroEntrada() {
		return nrRegistroEntrada;
	}
	
	/**
	 * Set: nrRegistroEntrada.
	 *
	 * @param nrRegistroEntrada the nr registro entrada
	 */
	public void setNrRegistroEntrada(Integer nrRegistroEntrada) {
		this.nrRegistroEntrada = nrRegistroEntrada;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: listaMovimentacoes.
	 *
	 * @return listaMovimentacoes
	 */
	public List<OcorrenciasExcluirSolEmiAviMovtoFavorecidoEntradaDTO> getListaMovimentacoes() {
		return listaMovimentacoes;
	}
	
	/**
	 * Set: listaMovimentacoes.
	 *
	 * @param listaMovimentacoes the lista movimentacoes
	 */
	public void setListaMovimentacoes(
			List<OcorrenciasExcluirSolEmiAviMovtoFavorecidoEntradaDTO> listaMovimentacoes) {
		this.listaMovimentacoes = listaMovimentacoes;
	}
}