/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharSolicitCriacaoContaTipoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharSolicitCriacaoContaTipoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrSolicitContaDesp
     */
    private long _nrSolicitContaDesp = 0;

    /**
     * keeps track of state for field: _nrSolicitContaDesp
     */
    private boolean _has_nrSolicitContaDesp;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCpfCnpjPssoa
     */
    private long _cdCpfCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPssoa
     */
    private boolean _has_cdCpfCnpjPssoa;

    /**
     * Field _cdFilialCnpjPssoa
     */
    private int _cdFilialCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjPssoa
     */
    private boolean _has_cdFilialCnpjPssoa;

    /**
     * Field _cdControleCnpjPssoa
     */
    private int _cdControleCnpjPssoa = 0;

    /**
     * keeps track of state for field: _cdControleCnpjPssoa
     */
    private boolean _has_cdControleCnpjPssoa;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharSolicitCriacaoContaTipoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.request.DetalharSolicitCriacaoContaTipoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCnpjPssoa
     * 
     */
    public void deleteCdControleCnpjPssoa()
    {
        this._has_cdControleCnpjPssoa= false;
    } //-- void deleteCdControleCnpjPssoa() 

    /**
     * Method deleteCdCpfCnpjPssoa
     * 
     */
    public void deleteCdCpfCnpjPssoa()
    {
        this._has_cdCpfCnpjPssoa= false;
    } //-- void deleteCdCpfCnpjPssoa() 

    /**
     * Method deleteCdFilialCnpjPssoa
     * 
     */
    public void deleteCdFilialCnpjPssoa()
    {
        this._has_cdFilialCnpjPssoa= false;
    } //-- void deleteCdFilialCnpjPssoa() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteNrSolicitContaDesp
     * 
     */
    public void deleteNrSolicitContaDesp()
    {
        this._has_nrSolicitContaDesp= false;
    } //-- void deleteNrSolicitContaDesp() 

    /**
     * Returns the value of field 'cdControleCnpjPssoa'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjPssoa'.
     */
    public int getCdControleCnpjPssoa()
    {
        return this._cdControleCnpjPssoa;
    } //-- int getCdControleCnpjPssoa() 

    /**
     * Returns the value of field 'cdCpfCnpjPssoa'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPssoa'.
     */
    public long getCdCpfCnpjPssoa()
    {
        return this._cdCpfCnpjPssoa;
    } //-- long getCdCpfCnpjPssoa() 

    /**
     * Returns the value of field 'cdFilialCnpjPssoa'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjPssoa'.
     */
    public int getCdFilialCnpjPssoa()
    {
        return this._cdFilialCnpjPssoa;
    } //-- int getCdFilialCnpjPssoa() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'nrSolicitContaDesp'.
     * 
     * @return long
     * @return the value of field 'nrSolicitContaDesp'.
     */
    public long getNrSolicitContaDesp()
    {
        return this._nrSolicitContaDesp;
    } //-- long getNrSolicitContaDesp() 

    /**
     * Method hasCdControleCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjPssoa()
    {
        return this._has_cdControleCnpjPssoa;
    } //-- boolean hasCdControleCnpjPssoa() 

    /**
     * Method hasCdCpfCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPssoa()
    {
        return this._has_cdCpfCnpjPssoa;
    } //-- boolean hasCdCpfCnpjPssoa() 

    /**
     * Method hasCdFilialCnpjPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjPssoa()
    {
        return this._has_cdFilialCnpjPssoa;
    } //-- boolean hasCdFilialCnpjPssoa() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasNrSolicitContaDesp
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitContaDesp()
    {
        return this._has_nrSolicitContaDesp;
    } //-- boolean hasNrSolicitContaDesp() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCnpjPssoa'.
     * 
     * @param cdControleCnpjPssoa the value of field
     * 'cdControleCnpjPssoa'.
     */
    public void setCdControleCnpjPssoa(int cdControleCnpjPssoa)
    {
        this._cdControleCnpjPssoa = cdControleCnpjPssoa;
        this._has_cdControleCnpjPssoa = true;
    } //-- void setCdControleCnpjPssoa(int) 

    /**
     * Sets the value of field 'cdCpfCnpjPssoa'.
     * 
     * @param cdCpfCnpjPssoa the value of field 'cdCpfCnpjPssoa'.
     */
    public void setCdCpfCnpjPssoa(long cdCpfCnpjPssoa)
    {
        this._cdCpfCnpjPssoa = cdCpfCnpjPssoa;
        this._has_cdCpfCnpjPssoa = true;
    } //-- void setCdCpfCnpjPssoa(long) 

    /**
     * Sets the value of field 'cdFilialCnpjPssoa'.
     * 
     * @param cdFilialCnpjPssoa the value of field
     * 'cdFilialCnpjPssoa'.
     */
    public void setCdFilialCnpjPssoa(int cdFilialCnpjPssoa)
    {
        this._cdFilialCnpjPssoa = cdFilialCnpjPssoa;
        this._has_cdFilialCnpjPssoa = true;
    } //-- void setCdFilialCnpjPssoa(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'nrSolicitContaDesp'.
     * 
     * @param nrSolicitContaDesp the value of field
     * 'nrSolicitContaDesp'.
     */
    public void setNrSolicitContaDesp(long nrSolicitContaDesp)
    {
        this._nrSolicitContaDesp = nrSolicitContaDesp;
        this._has_nrSolicitContaDesp = true;
    } //-- void setNrSolicitContaDesp(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharSolicitCriacaoContaTipoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.request.DetalharSolicitCriacaoContaTipoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.request.DetalharSolicitCriacaoContaTipoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.request.DetalharSolicitCriacaoContaTipoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.request.DetalharSolicitCriacaoContaTipoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
