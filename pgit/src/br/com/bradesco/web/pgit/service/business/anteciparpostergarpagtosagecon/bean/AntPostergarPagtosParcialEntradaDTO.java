/*
 * Nome: br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: AntPostergarPagtosParcialEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AntPostergarPagtosParcialEntradaDTO {
	
	/** Atributo dtQuitacao. */
	private String dtQuitacao;
	
	/** Atributo listaOcorrencias. */
	private List<OcorrenciasAntPostergarPagtosParcialEntradaDTO> listaOcorrencias = new ArrayList<OcorrenciasAntPostergarPagtosParcialEntradaDTO>();
	
	/**
	 * Get: dtQuitacao.
	 *
	 * @return dtQuitacao
	 */
	public String getDtQuitacao() {
		return dtQuitacao;
	}
	
	/**
	 * Set: dtQuitacao.
	 *
	 * @param dtQuitacao the dt quitacao
	 */
	public void setDtQuitacao(String dtQuitacao) {
		this.dtQuitacao = dtQuitacao;
	}
	
	/**
	 * Get: listaOcorrencias.
	 *
	 * @return listaOcorrencias
	 */
	public List<OcorrenciasAntPostergarPagtosParcialEntradaDTO> getListaOcorrencias() {
		return listaOcorrencias;
	}
	
	/**
	 * Set: listaOcorrencias.
	 *
	 * @param listaOcorrencias the lista ocorrencias
	 */
	public void setListaOcorrencias(
			List<OcorrenciasAntPostergarPagtosParcialEntradaDTO> listaOcorrencias) {
		this.listaOcorrencias = listaOcorrencias;
	}
	
	

}
