/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacaoPagamento
     */
    private int _cdSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSolicitacaoPagamento
     */
    private boolean _has_cdSolicitacaoPagamento;

    /**
     * Field _nrSolicitacao
     */
    private int _nrSolicitacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacao
     */
    private boolean _has_nrSolicitacao;

    /**
     * Field _dsDataSolicitacao
     */
    private java.lang.String _dsDataSolicitacao;

    /**
     * Field _dsHoraSolicitacao
     */
    private java.lang.String _dsHoraSolicitacao;

    /**
     * Field _cdSituacao
     */
    private java.lang.String _cdSituacao;

    /**
     * Field _cdUsuario
     */
    private java.lang.String _cdUsuario;

    /**
     * Field _dsNomeUsuario
     */
    private java.lang.String _dsNomeUsuario;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSolicitacaoPagamento
     * 
     */
    public void deleteCdSolicitacaoPagamento()
    {
        this._has_cdSolicitacaoPagamento= false;
    } //-- void deleteCdSolicitacaoPagamento() 

    /**
     * Method deleteNrSolicitacao
     * 
     */
    public void deleteNrSolicitacao()
    {
        this._has_nrSolicitacao= false;
    } //-- void deleteNrSolicitacao() 

    /**
     * Returns the value of field 'cdSituacao'.
     * 
     * @return String
     * @return the value of field 'cdSituacao'.
     */
    public java.lang.String getCdSituacao()
    {
        return this._cdSituacao;
    } //-- java.lang.String getCdSituacao() 

    /**
     * Returns the value of field 'cdSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamento'.
     */
    public int getCdSolicitacaoPagamento()
    {
        return this._cdSolicitacaoPagamento;
    } //-- int getCdSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdUsuario'.
     * 
     * @return String
     * @return the value of field 'cdUsuario'.
     */
    public java.lang.String getCdUsuario()
    {
        return this._cdUsuario;
    } //-- java.lang.String getCdUsuario() 

    /**
     * Returns the value of field 'dsDataSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsDataSolicitacao'.
     */
    public java.lang.String getDsDataSolicitacao()
    {
        return this._dsDataSolicitacao;
    } //-- java.lang.String getDsDataSolicitacao() 

    /**
     * Returns the value of field 'dsHoraSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsHoraSolicitacao'.
     */
    public java.lang.String getDsHoraSolicitacao()
    {
        return this._dsHoraSolicitacao;
    } //-- java.lang.String getDsHoraSolicitacao() 

    /**
     * Returns the value of field 'dsNomeUsuario'.
     * 
     * @return String
     * @return the value of field 'dsNomeUsuario'.
     */
    public java.lang.String getDsNomeUsuario()
    {
        return this._dsNomeUsuario;
    } //-- java.lang.String getDsNomeUsuario() 

    /**
     * Returns the value of field 'nrSolicitacao'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacao'.
     */
    public int getNrSolicitacao()
    {
        return this._nrSolicitacao;
    } //-- int getNrSolicitacao() 

    /**
     * Method hasCdSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamento()
    {
        return this._has_cdSolicitacaoPagamento;
    } //-- boolean hasCdSolicitacaoPagamento() 

    /**
     * Method hasNrSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacao()
    {
        return this._has_nrSolicitacao;
    } //-- boolean hasNrSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacao'.
     * 
     * @param cdSituacao the value of field 'cdSituacao'.
     */
    public void setCdSituacao(java.lang.String cdSituacao)
    {
        this._cdSituacao = cdSituacao;
    } //-- void setCdSituacao(java.lang.String) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamento'.
     * 
     * @param cdSolicitacaoPagamento the value of field
     * 'cdSolicitacaoPagamento'.
     */
    public void setCdSolicitacaoPagamento(int cdSolicitacaoPagamento)
    {
        this._cdSolicitacaoPagamento = cdSolicitacaoPagamento;
        this._has_cdSolicitacaoPagamento = true;
    } //-- void setCdSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdUsuario'.
     * 
     * @param cdUsuario the value of field 'cdUsuario'.
     */
    public void setCdUsuario(java.lang.String cdUsuario)
    {
        this._cdUsuario = cdUsuario;
    } //-- void setCdUsuario(java.lang.String) 

    /**
     * Sets the value of field 'dsDataSolicitacao'.
     * 
     * @param dsDataSolicitacao the value of field
     * 'dsDataSolicitacao'.
     */
    public void setDsDataSolicitacao(java.lang.String dsDataSolicitacao)
    {
        this._dsDataSolicitacao = dsDataSolicitacao;
    } //-- void setDsDataSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsHoraSolicitacao'.
     * 
     * @param dsHoraSolicitacao the value of field
     * 'dsHoraSolicitacao'.
     */
    public void setDsHoraSolicitacao(java.lang.String dsHoraSolicitacao)
    {
        this._dsHoraSolicitacao = dsHoraSolicitacao;
    } //-- void setDsHoraSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeUsuario'.
     * 
     * @param dsNomeUsuario the value of field 'dsNomeUsuario'.
     */
    public void setDsNomeUsuario(java.lang.String dsNomeUsuario)
    {
        this._dsNomeUsuario = dsNomeUsuario;
    } //-- void setDsNomeUsuario(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacao'.
     * 
     * @param nrSolicitacao the value of field 'nrSolicitacao'.
     */
    public void setNrSolicitacao(int nrSolicitacao)
    {
        this._nrSolicitacao = nrSolicitacao;
        this._has_nrSolicitacao = true;
    } //-- void setNrSolicitacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
