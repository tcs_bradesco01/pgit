/*
 * Nome: br.com.bradesco.web.pgit.service.business.histcomplementar.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.histcomplementar.bean;

/**
 * Nome: ListarHistMsgHistoricoComplementarEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistMsgHistoricoComplementarEntradaDTO {
	
	/** Atributo codigoHistoricoComplementar. */
	private int codigoHistoricoComplementar;
	
	/** Atributo periodoInicial. */
	private String periodoInicial;
	
	/** Atributo periodoFinal. */
	private String periodoFinal;
	
	
	/**
	 * Get: codigoHistoricoComplementar.
	 *
	 * @return codigoHistoricoComplementar
	 */
	public int getCodigoHistoricoComplementar() {
		return codigoHistoricoComplementar;
	}
	
	/**
	 * Set: codigoHistoricoComplementar.
	 *
	 * @param codigoHistoricoComplementar the codigo historico complementar
	 */
	public void setCodigoHistoricoComplementar(int codigoHistoricoComplementar) {
		this.codigoHistoricoComplementar = codigoHistoricoComplementar;
	}
	
	/**
	 * Get: periodoFinal.
	 *
	 * @return periodoFinal
	 */
	public String getPeriodoFinal() {
		return periodoFinal;
	}
	
	/**
	 * Set: periodoFinal.
	 *
	 * @param periodoFinal the periodo final
	 */
	public void setPeriodoFinal(String periodoFinal) {
		this.periodoFinal = periodoFinal;
	}
	
	/**
	 * Get: periodoInicial.
	 *
	 * @return periodoInicial
	 */
	public String getPeriodoInicial() {
		return periodoInicial;
	}
	
	/**
	 * Set: periodoInicial.
	 *
	 * @param periodoInicial the periodo inicial
	 */
	public void setPeriodoInicial(String periodoInicial) {
		this.periodoInicial = periodoInicial;
	}
	
	

}

