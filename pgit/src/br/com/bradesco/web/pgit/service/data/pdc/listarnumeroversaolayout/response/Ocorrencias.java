/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroVersaoLayoutArquivo
     */
    private int _numeroVersaoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _numeroVersaoLayoutArquivo
     */
    private boolean _has_numeroVersaoLayoutArquivo;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNumeroVersaoLayoutArquivo
     * 
     */
    public void deleteNumeroVersaoLayoutArquivo()
    {
        this._has_numeroVersaoLayoutArquivo= false;
    } //-- void deleteNumeroVersaoLayoutArquivo() 

    /**
     * Returns the value of field 'numeroVersaoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'numeroVersaoLayoutArquivo'.
     */
    public int getNumeroVersaoLayoutArquivo()
    {
        return this._numeroVersaoLayoutArquivo;
    } //-- int getNumeroVersaoLayoutArquivo() 

    /**
     * Method hasNumeroVersaoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroVersaoLayoutArquivo()
    {
        return this._has_numeroVersaoLayoutArquivo;
    } //-- boolean hasNumeroVersaoLayoutArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'numeroVersaoLayoutArquivo'.
     * 
     * @param numeroVersaoLayoutArquivo the value of field
     * 'numeroVersaoLayoutArquivo'.
     */
    public void setNumeroVersaoLayoutArquivo(int numeroVersaoLayoutArquivo)
    {
        this._numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
        this._has_numeroVersaoLayoutArquivo = true;
    } //-- void setNumeroVersaoLayoutArquivo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarnumeroversaolayout.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
