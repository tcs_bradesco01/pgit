/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarformalanccnab.response.Ocorrencias;

/**
 * Nome: ListarFormaLancCnabSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarFormaLancCnabSaidaDTO {

    /** Atributo cdFormaLancamentoCnab. */
    private Integer cdFormaLancamentoCnab;
    
    /** Atributo dsFormaLancamentoCnab. */
    private String dsFormaLancamentoCnab;
    
	/**
	 * Listar forma lanc cnab saida dto.
	 *
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 * @param dsFormaLancamentoCnab the ds forma lancamento cnab
	 */
	public ListarFormaLancCnabSaidaDTO(Integer cdFormaLancamentoCnab, String dsFormaLancamentoCnab) {
		super();
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
		this.dsFormaLancamentoCnab = dsFormaLancamentoCnab;
	}

	/**
	 * Listar forma lanc cnab saida dto.
	 *
	 * @param saida the saida
	 */
	public ListarFormaLancCnabSaidaDTO(Ocorrencias saida) {
    	this.cdFormaLancamentoCnab = saida.getCdFormaLancamentoCnab();
        this.dsFormaLancamentoCnab = saida.getDsFormaLancamentoCnab();
	}
	
	/**
	 * Get: cdFormaLancamentoCnab.
	 *
	 * @return cdFormaLancamentoCnab
	 */
	public Integer getCdFormaLancamentoCnab() {
		return cdFormaLancamentoCnab;
	}
	
	/**
	 * Set: cdFormaLancamentoCnab.
	 *
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 */
	public void setCdFormaLancamentoCnab(Integer cdFormaLancamentoCnab) {
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
	}
	
	/**
	 * Get: dsFormaLancamentoCnab.
	 *
	 * @return dsFormaLancamentoCnab
	 */
	public String getDsFormaLancamentoCnab() {
		return dsFormaLancamentoCnab;
	}
	
	/**
	 * Set: dsFormaLancamentoCnab.
	 *
	 * @param dsFormaLancamentoCnab the ds forma lancamento cnab
	 */
	public void setDsFormaLancamentoCnab(String dsFormaLancamentoCnab) {
		this.dsFormaLancamentoCnab = dsFormaLancamentoCnab;
	}
    
    
    
}
