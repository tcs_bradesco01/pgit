/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.cadproccontrole.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcCtrlServiceConstants;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.EstatisticaProcessamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.EstatisticaProcessamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.HistoricoBloqueioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.HistoricoBloqueioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarprocessomassivo.request.AlterarProcessoMassivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarprocessomassivo.response.AlterarProcessoMassivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.request.BloqueioDesbloqueioProcessoMassivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueiodesbloqueioprocessomassivo.response.BloqueioDesbloqueioProcessoMassivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.request.DetalharHistoricoBloqueioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricobloqueio.response.DetalharHistoricoBloqueioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharprocessomassivo.request.DetalharProcessoMassivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharprocessomassivo.response.DetalharProcessoMassivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirprocessomassivo.request.ExcluirProcessoMassivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirprocessomassivo.response.ExcluirProcessoMassivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.request.IncluirProcessoMassivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirprocessomassivo.response.IncluirProcessoMassivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.request.ListarEstatisticaProcessoMassivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.response.ListarEstatisticaProcessoMassivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricobloqueio.request.ListarHistoricoBloqueioRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistoricobloqueio.response.ListarHistoricoBloqueioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.request.ListarProcessoMassivoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.response.ListarProcessoMassivoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarprocessomassivo.response.Ocorrencias;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterCadastroProcessosControle
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class CadProcCtrlServiceImpl implements ICadProcControleService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Construtor.
     */
    public CadProcCtrlServiceImpl() {
    }

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#listarProcessoMassivo(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO)
	 */
	public List<ProcessoMassivoSaidaDTO> listarProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO) {
		ListarProcessoMassivoRequest request = new ListarProcessoMassivoRequest();
		request.setCdSituacao(processoMassivoEntradaDTO.getCdSistema() != null ? processoMassivoEntradaDTO.getCdSistema() : "");
		request.setCdTipoProcessoSistema(processoMassivoEntradaDTO.getCdTipoProcessoSistema() != null ? processoMassivoEntradaDTO.getCdTipoProcessoSistema() : 0);
		request.setCdPeriodicidade(processoMassivoEntradaDTO.getCdPeriodicidade() != null ? processoMassivoEntradaDTO.getCdPeriodicidade() : 0);
		request.setQtConsultas(ICadProcCtrlServiceConstants.NUMERO_OCORRENCIAS_LISTAR_PROCESSO_MASSIVO);

		ListarProcessoMassivoResponse response = getFactoryAdapter().getListarProcessoMassivoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		List<ProcessoMassivoSaidaDTO> listaProcessos = new ArrayList<ProcessoMassivoSaidaDTO>();
		for (Ocorrencias o : response.getOcorrencias()) {
			ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = new ProcessoMassivoSaidaDTO();
			processoMassivoSaidaDTO.setCdSistema(o.getCdSistema());
			processoMassivoSaidaDTO.setDsCodigoSistema(o.getDsCdSistema());
			processoMassivoSaidaDTO.setCdProcessoSistema(o.getCdProcessoSistema());
			processoMassivoSaidaDTO.setCdTipoProcessoSistema(o.getCdTipoProcessoSistema());
			processoMassivoSaidaDTO.setDsTipoProcessoSistema(o.getDsTipoProcessoSistema());
			processoMassivoSaidaDTO.setCdPeriodicidade(o.getCdPeriodicidade());
			processoMassivoSaidaDTO.setDsPeriodicidade(o.getDsCodigoPeriodicidade());
			processoMassivoSaidaDTO.setCdSituacaoProcessoSistema(o.getCdSituacaoProcessoSistema());
			processoMassivoSaidaDTO.setDsSistemaProcessoSistema(o.getDsSituacaoProcessoSistema());
			processoMassivoSaidaDTO.setCdNetProcessamentoPgto(o.getCdNetProcessamentoPagamento());
			processoMassivoSaidaDTO.setCdJobProcessamentoPgto(o.getCdJobProcessamentoPagamento());
			listaProcessos.add(processoMassivoSaidaDTO);
		}
		return listaProcessos;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#incluirProcessoMassivo(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO)
	 */
	public ProcessoMassivoSaidaDTO incluirProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO) {
		IncluirProcessoMassivoRequest request = new IncluirProcessoMassivoRequest();
		request.setCdSistema(processoMassivoEntradaDTO.getCdSistema() != null ? processoMassivoEntradaDTO.getCdSistema() : "");
		request.setCdProcessoSistema(processoMassivoEntradaDTO.getCdProcessoSistema() != null ? processoMassivoEntradaDTO.getCdProcessoSistema() : 0);
		request.setCdTipoProcessoSistema(processoMassivoEntradaDTO.getCdTipoProcessoSistema() != null ? processoMassivoEntradaDTO.getCdTipoProcessoSistema() : 0);
		request.setCdPeriodicidade(processoMassivoEntradaDTO.getCdPeriodicidade() != null ? processoMassivoEntradaDTO.getCdPeriodicidade() : 0);
		request.setDsProcessoSistema(processoMassivoEntradaDTO.getDsProcessoSistema() != null ? processoMassivoEntradaDTO.getDsProcessoSistema() : "");
		request.setCdNetProcessamentoPagamento(processoMassivoEntradaDTO.getCdNetProcessamentoPgto() != null ? processoMassivoEntradaDTO.getCdNetProcessamentoPgto() : "");
		request.setCdJobProcessamentoPagamento(processoMassivoEntradaDTO.getCdJobProcessamentoPgto() != null ? processoMassivoEntradaDTO.getCdJobProcessamentoPgto() : "");

		IncluirProcessoMassivoResponse response = getFactoryAdapter().getIncluirProcessoMassivoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = new ProcessoMassivoSaidaDTO();
		processoMassivoSaidaDTO.setCodMensagem(response.getCodMensagem());
		processoMassivoSaidaDTO.setMensagem(response.getMensagem());
		return processoMassivoSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#detalharProcessoMassivo(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO)
	 */
	public ProcessoMassivoSaidaDTO detalharProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO) {
		DetalharProcessoMassivoRequest request = new DetalharProcessoMassivoRequest();
		request.setCdSistema(processoMassivoEntradaDTO.getCdSistema() != null ? processoMassivoEntradaDTO.getCdSistema() : "");
		request.setCdProcessoSistema(processoMassivoEntradaDTO.getCdProcessoSistema() != null ? processoMassivoEntradaDTO.getCdProcessoSistema() : 0);

		DetalharProcessoMassivoResponse response = getFactoryAdapter().getDetalharProcessoMassivoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = new ProcessoMassivoSaidaDTO();
		processoMassivoSaidaDTO.setCodMensagem(response.getCodMensagem());
		processoMassivoSaidaDTO.setMensagem(response.getMensagem());
		
		for (br.com.bradesco.web.pgit.service.data.pdc.detalharprocessomassivo.response.Ocorrencias o : response.getOcorrencias()) {
			processoMassivoSaidaDTO.setCdSistema(o.getCdSistema());
			processoMassivoSaidaDTO.setDsCodigoSistema(o.getDsCodigoSistema());
			processoMassivoSaidaDTO.setCdProcessoSistema(o.getCdProcessoSistema());
			processoMassivoSaidaDTO.setCdTipoProcessoSistema(o.getCdTipoProcessoSistema());
			processoMassivoSaidaDTO.setDsTipoProcessoSistema(o.getDsCodigoTipoProcessoSistema());
			processoMassivoSaidaDTO.setCdPeriodicidade(o.getCdPeriodicidade());
			processoMassivoSaidaDTO.setDsPeriodicidade(o.getDsCodigoPeriodicidade());
			processoMassivoSaidaDTO.setDsProcessoSistema(o.getDsProcessoSistema());
			processoMassivoSaidaDTO.setCdSituacaoProcessoSistema(o.getCdSituacaoProcessoSistema());
			processoMassivoSaidaDTO.setDsSistemaProcessoSistema(o.getDsSituacaoProcessoSistema());
			processoMassivoSaidaDTO.setCdNetProcessamentoPgto(o.getCdNetProcessamentoPagamento());
			processoMassivoSaidaDTO.setCdJobProcessamentoPgto(o.getCdJobProcessamentoPagamento());
			processoMassivoSaidaDTO.setCdCanalInclusao(o.getCdCanalInclusao() != 0 ? o.getCdCanalInclusao() : null);
			processoMassivoSaidaDTO.setDsCanalInclusao(o.getDsCanalInclusao());
			processoMassivoSaidaDTO.setCdAutenSegregacaoInclusao(o.getCdAutenSegregacaoInclusao());
			processoMassivoSaidaDTO.setNmOperacaoFluxoInclusao(!"0".equals(o.getNmOperacaoFluxoInclusao()) ? o.getNmOperacaoFluxoInclusao() : "");
			processoMassivoSaidaDTO.setHrInclusaoRegistro(o.getHrInclusaoRegistro());
			processoMassivoSaidaDTO.setCdCanalManutencao(o.getCdCanalManutencao() != 0 ? o.getCdCanalManutencao() : null);
			processoMassivoSaidaDTO.setDsCanalManutencao(o.getDsCanalManutencao());
			processoMassivoSaidaDTO.setCdAutenSegregacaoManutencao(o.getCdAutenSegregacaoManutencao());
			processoMassivoSaidaDTO.setNmOperacaoFluxoManutencao(!"0".equals(o.getNmOperacaoFluxoManutencao()) ? o.getNmOperacaoFluxoManutencao() : "");
			processoMassivoSaidaDTO.setHrInclusaoManutencao(o.getHrInclusaoManutencao());
			return processoMassivoSaidaDTO;
		}

		return processoMassivoSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#alterarProcessoMassivo(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO)
	 */
	public ProcessoMassivoSaidaDTO alterarProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO) {
		AlterarProcessoMassivoRequest request = new AlterarProcessoMassivoRequest();
		request.setCdSistema(processoMassivoEntradaDTO.getCdSistema() != null ? processoMassivoEntradaDTO.getCdSistema() : "");
		request.setCdProcessoSistema(processoMassivoEntradaDTO.getCdProcessoSistema() != null ? processoMassivoEntradaDTO.getCdProcessoSistema() : 0);
		request.setCdTipoProcessoSistema(processoMassivoEntradaDTO.getCdTipoProcessoSistema() != null ? processoMassivoEntradaDTO.getCdTipoProcessoSistema() : 0);
		request.setCdPeriodicidade(processoMassivoEntradaDTO.getCdPeriodicidade() != null ? processoMassivoEntradaDTO.getCdPeriodicidade() : 0);
		request.setDsProcessoSistema(processoMassivoEntradaDTO.getDsProcessoSistema() != null ? processoMassivoEntradaDTO.getDsProcessoSistema() : "");
		request.setCdNetProcessamentoPagamento(processoMassivoEntradaDTO.getCdNetProcessamentoPgto() != null ? processoMassivoEntradaDTO.getCdNetProcessamentoPgto() : "");
		request.setCdJobProcessamentoPagamento(processoMassivoEntradaDTO.getCdJobProcessamentoPgto() != null ? processoMassivoEntradaDTO.getCdJobProcessamentoPgto() : "");

		AlterarProcessoMassivoResponse response = getFactoryAdapter().getAlterarProcessoMassivoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = new ProcessoMassivoSaidaDTO();
		processoMassivoSaidaDTO.setCodMensagem(response.getCodMensagem());
		processoMassivoSaidaDTO.setMensagem(response.getMensagem());
		return processoMassivoSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#excluirProcessoMassivo(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO)
	 */
	public ProcessoMassivoSaidaDTO excluirProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO) {
		ExcluirProcessoMassivoRequest request = new ExcluirProcessoMassivoRequest();
		request.setCdSistema(processoMassivoEntradaDTO.getCdSistema() != null ? processoMassivoEntradaDTO.getCdSistema() : "");
		request.setCdProcessoSistema(processoMassivoEntradaDTO.getCdProcessoSistema() != null ? processoMassivoEntradaDTO.getCdProcessoSistema() : 0);

		ExcluirProcessoMassivoResponse response = getFactoryAdapter().getExcluirProcessoMassivoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = new ProcessoMassivoSaidaDTO();
		processoMassivoSaidaDTO.setCodMensagem(response.getCodMensagem());
		processoMassivoSaidaDTO.setMensagem(response.getMensagem());
		return processoMassivoSaidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#bloquearProcessoMassivo(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO)
	 */
	public ProcessoMassivoSaidaDTO bloquearProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO) {
		BloqueioDesbloqueioProcessoMassivoRequest request = new BloqueioDesbloqueioProcessoMassivoRequest();
		request.setCdSistema(processoMassivoEntradaDTO.getCdSistema() != null ? processoMassivoEntradaDTO.getCdSistema() : "");
		request.setCdProcessoSistema(processoMassivoEntradaDTO.getCdProcessoSistema() != null ? processoMassivoEntradaDTO.getCdProcessoSistema() : 0);
		request.setCdSituacaoProcessoSistema(processoMassivoEntradaDTO.getCdSituacaoProcessoSistema() != null ? processoMassivoEntradaDTO.getCdSituacaoProcessoSistema() : 0);
		request.setDsBloqueioProcessoSistema(processoMassivoEntradaDTO.getDsBloqueioProcessoSistema() != null ? processoMassivoEntradaDTO.getDsBloqueioProcessoSistema() : "");

		BloqueioDesbloqueioProcessoMassivoResponse response = getFactoryAdapter().getBloqueioDesbloqueioProcessoMassivoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = new ProcessoMassivoSaidaDTO();
		processoMassivoSaidaDTO.setCodMensagem(response.getCodMensagem());
		processoMassivoSaidaDTO.setMensagem(response.getMensagem());
		return processoMassivoSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#listarHistoricoProcessoMassivo(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.HistoricoBloqueioEntradaDTO)
	 */
	public List<HistoricoBloqueioSaidaDTO> listarHistoricoProcessoMassivo(HistoricoBloqueioEntradaDTO historicoBloqueioEntradaDTO) {
		ListarHistoricoBloqueioRequest request = new ListarHistoricoBloqueioRequest();
		request.setCdSistema(historicoBloqueioEntradaDTO.getCdSistema() != null ? historicoBloqueioEntradaDTO.getCdSistema() : "");
		request.setCdProcessoSistema(historicoBloqueioEntradaDTO.getCdProcessoSistema() != null ? historicoBloqueioEntradaDTO.getCdProcessoSistema() : 0);
		request.setDtDe(FormatarData.formataDiaMesAnoToPdc(historicoBloqueioEntradaDTO.getDtDe()));
		request.setDtAte(FormatarData.formataDiaMesAnoToPdc(historicoBloqueioEntradaDTO.getDtAte()));
		request.setQtConsultas(ICadProcCtrlServiceConstants.NUMERO_OCORRENCIAS_LISTAR_PROCESSO_MASSIVO);

		ListarHistoricoBloqueioResponse response = getFactoryAdapter().getListarHistoricoBloqueioPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		List<HistoricoBloqueioSaidaDTO> listaHistoricos = new ArrayList<HistoricoBloqueioSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarhistoricobloqueio.response.Ocorrencias o : response.getOcorrencias()) {
			HistoricoBloqueioSaidaDTO historicoBloqueioSaidaDTO = new HistoricoBloqueioSaidaDTO();
			historicoBloqueioSaidaDTO.setHrBloqueioProcessoSistema(o.getHrBloqueioProcessoSistema());
			historicoBloqueioSaidaDTO.setCdIndicadorTipoManutencao(o.getCdIndicadorTipoManutencao());
			historicoBloqueioSaidaDTO.setDsIndicadorTipoManutencao(o.getDsIndicadorTipoManutencao());
			historicoBloqueioSaidaDTO.setCdUsuarioInclusao(o.getCdUsuarioInclusao());
			historicoBloqueioSaidaDTO.setDsUsuarioInclusao(o.getDsUsuarioInclusao() != null && !o.getDsUsuarioInclusao().equals("")? o.getDsUsuarioInclusao():o.getCdUsuarioInclusao());
			listaHistoricos.add(historicoBloqueioSaidaDTO);
		}
		return listaHistoricos;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#detalharHistoricoProcessoMassivo(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.HistoricoBloqueioEntradaDTO)
	 */
	public HistoricoBloqueioSaidaDTO detalharHistoricoProcessoMassivo(HistoricoBloqueioEntradaDTO historicoBloqueioEntradaDTO) {
		DetalharHistoricoBloqueioRequest request = new DetalharHistoricoBloqueioRequest();
		request.setCdSistema(historicoBloqueioEntradaDTO.getCdSistema() != null ? historicoBloqueioEntradaDTO.getCdSistema() : "");
		request.setCdProcessoSistema(historicoBloqueioEntradaDTO.getCdProcessoSistema() != null ? historicoBloqueioEntradaDTO.getCdProcessoSistema() : 0);
		request.setDtSistema(historicoBloqueioEntradaDTO.getDtConsulta() != null ? historicoBloqueioEntradaDTO.getDtConsulta() : "");

		DetalharHistoricoBloqueioResponse response = getFactoryAdapter().getDetalharHistoricoBloqueioPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		HistoricoBloqueioSaidaDTO historicoBloqueioSaidaDTO = new HistoricoBloqueioSaidaDTO();
		historicoBloqueioSaidaDTO.setCdSistema(response.getCdSistema());
		historicoBloqueioSaidaDTO.setDsCodigoSistema(response.getDsCodigoSistema());
		historicoBloqueioSaidaDTO.setCdProcessoSistema(response.getCdProcessoSistema());
		historicoBloqueioSaidaDTO.setCdTipoProcessoSistema(response.getCdTipoProcessoSistema());
		historicoBloqueioSaidaDTO.setDsCodigoTipoProcessoSistema(response.getDsCodigoTipoProcessoSistema());
		historicoBloqueioSaidaDTO.setCdPeriodicidade(response.getCdPeriodicidade());
		historicoBloqueioSaidaDTO.setDsCodigoPeriodicidade(response.getDsCodigoPeriodicidade());
		historicoBloqueioSaidaDTO.setDsProcessoSistema(response.getDsProcessoSistema());
		historicoBloqueioSaidaDTO.setCdSituacaoProcessoSistema(response.getCdSituacaoProcessoSistema());
		historicoBloqueioSaidaDTO.setCdNetProcessamentoPgto(response.getCdNetProcessamentoPagamento());
		historicoBloqueioSaidaDTO.setCdobProcessamentoPgto(response.getCdJobProcessamentoPagamento());
		historicoBloqueioSaidaDTO.setDsBloqueioProcessoSistema(response.getDsBloqueioProcessoSistema());
		historicoBloqueioSaidaDTO.setCdCanalInclusao(response.getCdCanalInclusao() != 0 ? response.getCdCanalInclusao() : null);
		historicoBloqueioSaidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		historicoBloqueioSaidaDTO.setCdAutenSegregacaoInclusao(response.getCdAutenSegregacaoInclusao());
		historicoBloqueioSaidaDTO.setNmOperacaoFluxoInclusao(!"0".equals(response.getNmOperacaoFluxoInclusao()) ? response.getNmOperacaoFluxoInclusao() : "");
		historicoBloqueioSaidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		historicoBloqueioSaidaDTO.setCdCanalManutencao(response.getCdCanalManutencao() != 0 ? response.getCdCanalManutencao() : null);
		historicoBloqueioSaidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
		historicoBloqueioSaidaDTO.setCdAutenSegregacaoManutencao(response.getCdAutenSegregacaoManutencao());
		historicoBloqueioSaidaDTO.setNmOperacaoFluxoManutencao(!"0".equals(response.getNmOperacaoFluxoManutencao()) ? response.getNmOperacaoFluxoManutencao() : "");
		historicoBloqueioSaidaDTO.setHrInclusaoManutencao(response.getHrInclusaoManutencao());
		historicoBloqueioSaidaDTO.setDsSistemaProcesso(response.getDsSistemaProcesso());
			
		return historicoBloqueioSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService#listarEstatisticaProcessamento(br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.EstatisticaProcessamentoEntradaDTO)
	 */
	public List<EstatisticaProcessamentoSaidaDTO> listarEstatisticaProcessamento(EstatisticaProcessamentoEntradaDTO entradaDTO) {
		ListarEstatisticaProcessoMassivoRequest request = new ListarEstatisticaProcessoMassivoRequest();
		request.setCdSistema(entradaDTO.getCdSistema() != null ? entradaDTO.getCdSistema() : "");
		request.setCdTipoProcessoSistema(entradaDTO.getCdTipoProcessoSistema() != null ? entradaDTO.getCdTipoProcessoSistema() : 0);
		request.setDtInicioEstatisticaProcessoInicio(FormatarData.formataDiaMesAnoToPdc(entradaDTO.getDtInicioEstatisticaProcessoInicio()));
		request.setHrInicioEstatisticaProcessoInicio(entradaDTO.getHrInicioEstatisticaProcessoInicio() != null ? entradaDTO.getHrInicioEstatisticaProcessoInicio() : "");
		request.setHrInicioEstatisticaProcessoFim(entradaDTO.getHrInicioEstatisticaProcessoFim() != null ? entradaDTO.getHrInicioEstatisticaProcessoFim() : "");
		request.setDtInicioEstatisticaProcessoFim(FormatarData.formataDiaMesAnoToPdc(entradaDTO.getDtInicioEstatisticaProcessoFim()));
		request.setCdSituacaoEstatisticaSistema(entradaDTO.getCdSituacaoEstatisticaSistema() != null ? entradaDTO.getCdSituacaoEstatisticaSistema() : 0);
		request.setQtOcorrencias(ICadProcCtrlServiceConstants.NUMERO_OCORRENCIAS_LISTAR_ESTATISTICA_PROCESSAMENTO);

		ListarEstatisticaProcessoMassivoResponse response = getFactoryAdapter().getListarEstatisticaProcessoMassivoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return null;
		}

		List<EstatisticaProcessamentoSaidaDTO> listaEstatistica = new ArrayList<EstatisticaProcessamentoSaidaDTO>();
		for (br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.response.Ocorrencias o : response.getOcorrencias()) {
			EstatisticaProcessamentoSaidaDTO historicoBloqueioSaidaDTO = new EstatisticaProcessamentoSaidaDTO();
			historicoBloqueioSaidaDTO.setCdSistema(o.getCdSistema());
			historicoBloqueioSaidaDTO.setCdProcessoSistema(o.getCdProcessoSistema());
			historicoBloqueioSaidaDTO.setIdTipoProcessoSIstema(o.getDsTipoProcessoSistema());
			historicoBloqueioSaidaDTO.setHrInicioEstatisticaProcessoInicio(o.getHrInicioEstatisticaProcessoInicio());
			historicoBloqueioSaidaDTO.setHrInicioEstatisticaProcessoFim(o.getHrInicioEstatisticaProcessoFim());
			historicoBloqueioSaidaDTO.setCdSituacaoEstatisticaSistema(o.getCdSituacaoEstatisticaSistema());
			historicoBloqueioSaidaDTO.setDsSituacaoEstatisticaSistema(o.getDsSituacaoEstatisticaSistema());
			historicoBloqueioSaidaDTO.setQtRegistroProcessoSistema(o.getQtRegistrosProcessoSistema());
			historicoBloqueioSaidaDTO.setVlRegistroProcessoSistema(o.getVlRegistroProcessoSistema());
			listaEstatistica.add(historicoBloqueioSaidaDTO);
		}
		return listaEstatistica;
	}
}