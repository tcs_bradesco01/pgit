package br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean;

import java.math.BigDecimal;

public class DetalharSolicAntProcLotePgtoAgdaSaidaDTO {

	private String codMensagem;
	private String mensagem;
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Integer cdSolicitacaoPagamentoIntegrado;
	private Integer nrSolicitacaoPagamentoIntegrado;
	private String dsSituacaoSolicitaoPgto;
	private String dsMotivoSolicitacao;
	private String dsSolicitacao;
	private String hrSolicitacao;
	private Long nrLoteInterno;
	private String dsTipoLayoutArquivo;
	private String dtPgtoInicial;
	private String dtPgtoFinal;
	private Integer cdBancoDebito;
	private Integer cdAgenciaDebito;
	private Long cdContaDebito;
	private String dsProdutoServicoOperacao;
	private String dsModalidadePgtoCliente;
	private Long qtdeTotalPagtoPrevistoSoltc;
	private BigDecimal vlrTotPagtoPrevistoSolicitacao;
	private Long qtTotalPgtoEfetivadoSolicitacao;
	private BigDecimal vlTotalPgtoEfetuadoSolicitacao;
	private Integer cdCanalInclusao;
	private String dsCanalInclusao;
	private String cdAutenticacaoSegurancaInclusao;
	private String nmOperacaoFluxoInclusao;
	private String hrInclusaoRegistro;
	private Integer cdCanalManutencao;
	private String dsCanalManutencao;
	private String cdAutenticacaoSegurancaManutencao;
	private String nmOperacaoFluxoManutencao;
	private String hrManutencaoRegistro;

	private String nrCnpjCpf;
	private String dsRazaoSocial;
	private String dsEmpresa;
	private String nroContrato;
	private String dsContrato;
	private String cdSituacaoContrato;

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}

	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}

	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}

	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}

	public String getDsSituacaoSolicitaoPgto() {
		return dsSituacaoSolicitaoPgto;
	}

	public void setDsSituacaoSolicitaoPgto(String dsSituacaoSolicitaoPgto) {
		this.dsSituacaoSolicitaoPgto = dsSituacaoSolicitaoPgto;
	}

	public String getDsMotivoSolicitacao() {
		return dsMotivoSolicitacao;
	}

	public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
		this.dsMotivoSolicitacao = dsMotivoSolicitacao;
	}

	public String getDsSolicitacao() {
		return dsSolicitacao;
	}

	public void setDsSolicitacao(String dsSolicitacao) {
		this.dsSolicitacao = dsSolicitacao;
	}

	public String getHrSolicitacao() {
		return hrSolicitacao;
	}

	public void setHrSolicitacao(String hrSolicitacao) {
		this.hrSolicitacao = hrSolicitacao;
	}

	public Long getNrLoteInterno() {
		return nrLoteInterno;
	}

	public void setNrLoteInterno(Long nrLoteInterno) {
		this.nrLoteInterno = nrLoteInterno;
	}

	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	public String getDtPgtoInicial() {
		return dtPgtoInicial;
	}

	public void setDtPgtoInicial(String dtPgtoInicial) {
		this.dtPgtoInicial = dtPgtoInicial;
	}

	public String getDtPgtoFinal() {
		return dtPgtoFinal;
	}

	public void setDtPgtoFinal(String dtPgtoFinal) {
		this.dtPgtoFinal = dtPgtoFinal;
	}

	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}

	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	public String getDsModalidadePgtoCliente() {
		return dsModalidadePgtoCliente;
	}

	public void setDsModalidadePgtoCliente(String dsModalidadePgtoCliente) {
		this.dsModalidadePgtoCliente = dsModalidadePgtoCliente;
	}

	public Long getQtdeTotalPagtoPrevistoSoltc() {
		return qtdeTotalPagtoPrevistoSoltc;
	}

	public void setQtdeTotalPagtoPrevistoSoltc(Long qtdeTotalPagtoPrevistoSoltc) {
		this.qtdeTotalPagtoPrevistoSoltc = qtdeTotalPagtoPrevistoSoltc;
	}

	public BigDecimal getVlrTotPagtoPrevistoSolicitacao() {
		return vlrTotPagtoPrevistoSolicitacao;
	}

	public void setVlrTotPagtoPrevistoSolicitacao(
			BigDecimal vlrTotPagtoPrevistoSolicitacao) {
		this.vlrTotPagtoPrevistoSolicitacao = vlrTotPagtoPrevistoSolicitacao;
	}

	public Long getQtTotalPgtoEfetivadoSolicitacao() {
		return qtTotalPgtoEfetivadoSolicitacao;
	}

	public void setQtTotalPgtoEfetivadoSolicitacao(
			Long qtTotalPgtoEfetivadoSolicitacao) {
		this.qtTotalPgtoEfetivadoSolicitacao = qtTotalPgtoEfetivadoSolicitacao;
	}

	public BigDecimal getVlTotalPgtoEfetuadoSolicitacao() {
		return vlTotalPgtoEfetuadoSolicitacao;
	}

	public void setVlTotalPgtoEfetuadoSolicitacao(
			BigDecimal vlTotalPgtoEfetuadoSolicitacao) {
		this.vlTotalPgtoEfetuadoSolicitacao = vlTotalPgtoEfetuadoSolicitacao;
	}

	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}

	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}

	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}

	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}

	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}

	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}

	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	public String getDsEmpresa() {
		return dsEmpresa;
	}

	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	public String getNroContrato() {
		return nroContrato;
	}

	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	public String getDsContrato() {
		return dsContrato;
	}

	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

}
