/*
 * Nome: br.com.bradesco.web.pgit.service.business.orgaopagador.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.orgaopagador.bean;

import java.io.Serializable;

/**
 * Nome: OrgaoPagadorSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OrgaoPagadorSaidaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 4217179481917185014L;

	/** Atributo codMensagem. */
	private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

	/** Atributo cdOrgaoPagador. */
	private Integer cdOrgaoPagador;

    /** Atributo cdTipoUnidadeOrganizacional. */
    private Integer cdTipoUnidadeOrganizacional;

    /** Atributo dsTipoUnidadeOrganizacional. */
    private String dsTipoUnidadeOrganizacional;

    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;

    /** Atributo dsPessoaJuridica. */
    private String dsPessoaJuridica;

    /** Atributo nrSeqUnidadeOrganizacional. */
    private Integer nrSeqUnidadeOrganizacional;

    /** Atributo dsNrSeqUnidadeOrganizacional. */
    private String dsNrSeqUnidadeOrganizacional;

    /** Atributo cdSituacao. */
    private Integer cdSituacao;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdOrgaoPagador.
	 *
	 * @return cdOrgaoPagador
	 */
	public Integer getCdOrgaoPagador() {
		return cdOrgaoPagador;
	}

	/**
	 * Set: cdOrgaoPagador.
	 *
	 * @param cdOrgaoPagador the cd orgao pagador
	 */
	public void setCdOrgaoPagador(Integer cdOrgaoPagador) {
		this.cdOrgaoPagador = cdOrgaoPagador;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: cdTipoUnidadeOrganizacional.
	 *
	 * @return cdTipoUnidadeOrganizacional
	 */
	public Integer getCdTipoUnidadeOrganizacional() {
		return cdTipoUnidadeOrganizacional;
	}

	/**
	 * Set: cdTipoUnidadeOrganizacional.
	 *
	 * @param cdTipoUnidadeOrganizacional the cd tipo unidade organizacional
	 */
	public void setCdTipoUnidadeOrganizacional(Integer cdTipoUnidadeOrganizacional) {
		this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
	}

	/**
	 * Get: dsNrSeqUnidadeOrganizacional.
	 *
	 * @return dsNrSeqUnidadeOrganizacional
	 */
	public String getDsNrSeqUnidadeOrganizacional() {
		return dsNrSeqUnidadeOrganizacional;
	}

	/**
	 * Set: dsNrSeqUnidadeOrganizacional.
	 *
	 * @param dsNrSeqUnidadeOrganizacional the ds nr seq unidade organizacional
	 */
	public void setDsNrSeqUnidadeOrganizacional(String dsNrSeqUnidadeOrganizacional) {
		this.dsNrSeqUnidadeOrganizacional = dsNrSeqUnidadeOrganizacional;
	}

	/**
	 * Get: dsPessoaJuridica.
	 *
	 * @return dsPessoaJuridica
	 */
	public String getDsPessoaJuridica() {
		return dsPessoaJuridica;
	}

	/**
	 * Set: dsPessoaJuridica.
	 *
	 * @param dsPessoaJuridica the ds pessoa juridica
	 */
	public void setDsPessoaJuridica(String dsPessoaJuridica) {
		this.dsPessoaJuridica = dsPessoaJuridica;
	}

	/**
	 * Get: dsTipoUnidadeOrganizacional.
	 *
	 * @return dsTipoUnidadeOrganizacional
	 */
	public String getDsTipoUnidadeOrganizacional() {
		return dsTipoUnidadeOrganizacional;
	}

	/**
	 * Set: dsTipoUnidadeOrganizacional.
	 *
	 * @param dsTipoUnidadeOrganizacional the ds tipo unidade organizacional
	 */
	public void setDsTipoUnidadeOrganizacional(String dsTipoUnidadeOrganizacional) {
		this.dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
	}

	/**
	 * Get: nrSeqUnidadeOrganizacional.
	 *
	 * @return nrSeqUnidadeOrganizacional
	 */
	public Integer getNrSeqUnidadeOrganizacional() {
		return nrSeqUnidadeOrganizacional;
	}

	/**
	 * Set: nrSeqUnidadeOrganizacional.
	 *
	 * @param nrSeqUnidadeOrganizacional the nr seq unidade organizacional
	 */
	public void setNrSeqUnidadeOrganizacional(Integer nrSeqUnidadeOrganizacional) {
		this.nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OrgaoPagadorSaidaDTO)) {
			return false;
		}

		OrgaoPagadorSaidaDTO other = (OrgaoPagadorSaidaDTO) obj;
		return this.getCdOrgaoPagador().equals(other.getCdOrgaoPagador());
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {		
		return getCdOrgaoPagador().hashCode();
	}
}
