/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: DetalharExcRegraSegregacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharExcRegraSegregacaoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo codigoRegra. */
	private int codigoRegra;
	
	/** Atributo codigoExcecao. */
	private int codigoExcecao;
	
	/** Atributo cdTipoUnidadeOrganizacionalUsuario. */
	private int cdTipoUnidadeOrganizacionalUsuario;
	
	/** Atributo dsTipoUnidadeOrganizacionalUsuario. */
	private String dsTipoUnidadeOrganizacionalUsuario;
	
	/** Atributo cdEmpresaConglomeradoUsuario. */
	private long cdEmpresaConglomeradoUsuario;
	
	/** Atributo dsEmpresaConglomeradoUsuario. */
	private String dsEmpresaConglomeradoUsuario;
	
	/** Atributo cdUnidadeOrganizacionalUsuario. */
	private int cdUnidadeOrganizacionalUsuario;
	
	/** Atributo dsUnidadeOrganizacionalUsuario. */
	private String dsUnidadeOrganizacionalUsuario;
	
	/** Atributo cdTipoUnidadeOrganizacionalProprietario. */
	private int cdTipoUnidadeOrganizacionalProprietario;
	
	/** Atributo dsTipoUnidadeOrganizacionalProprietario. */
	private String dsTipoUnidadeOrganizacionalProprietario;
	
	/** Atributo cdEmpresaConglomeradoProprietario. */
	private long cdEmpresaConglomeradoProprietario;
	
	/** Atributo dsEmpresaConglomeradoProprietario. */
	private String dsEmpresaConglomeradoProprietario;
	
	/** Atributo cdUnidadeOrganizacionalProprietario. */
	private int cdUnidadeOrganizacionalProprietario;
	
	/** Atributo dsUnidadeOrganizacionalProprietario. */
	private String dsUnidadeOrganizacionalProprietario;
	
	/** Atributo tipoExcecao. */
	private int tipoExcecao;
	
	/** Atributo tipoAbrangenciaExcecao. */
	private int tipoAbrangenciaExcecao;
	
	/** Atributo cdTipoCanalInclusao. */
	private int cdTipoCanalInclusao;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoUsuario. */
	private String complementoUsuario;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo cdTipoCanalManutencao. */
	private int cdTipoCanalManutencao;
	
	/** Atributo dsTipoCanallManutencao. */
	private String dsTipoCanallManutencao;
	
	/** Atributo usuariolManutencao. */
	private String usuariolManutencao;
	
	/** Atributo complementolManutencao. */
	private String complementolManutencao;
	
	/** Atributo dataHoralManutencao. */
	private String dataHoralManutencao;
	
	/**
	 * Get: cdEmpresaConglomeradoProprietario.
	 *
	 * @return cdEmpresaConglomeradoProprietario
	 */
	public long getCdEmpresaConglomeradoProprietario() {
		return cdEmpresaConglomeradoProprietario;
	}
	
	/**
	 * Set: cdEmpresaConglomeradoProprietario.
	 *
	 * @param cdEmpresaConglomeradoProprietario the cd empresa conglomerado proprietario
	 */
	public void setCdEmpresaConglomeradoProprietario(
			long cdEmpresaConglomeradoProprietario) {
		this.cdEmpresaConglomeradoProprietario = cdEmpresaConglomeradoProprietario;
	}
	
	/**
	 * Get: cdEmpresaConglomeradoUsuario.
	 *
	 * @return cdEmpresaConglomeradoUsuario
	 */
	public long getCdEmpresaConglomeradoUsuario() {
		return cdEmpresaConglomeradoUsuario;
	}
	
	/**
	 * Set: cdEmpresaConglomeradoUsuario.
	 *
	 * @param cdEmpresaConglomeradoUsuario the cd empresa conglomerado usuario
	 */
	public void setCdEmpresaConglomeradoUsuario(long cdEmpresaConglomeradoUsuario) {
		this.cdEmpresaConglomeradoUsuario = cdEmpresaConglomeradoUsuario;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public int getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(int cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public int getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(int cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacionalProprietario.
	 *
	 * @return cdTipoUnidadeOrganizacionalProprietario
	 */
	public int getCdTipoUnidadeOrganizacionalProprietario() {
		return cdTipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacionalProprietario.
	 *
	 * @param cdTipoUnidadeOrganizacionalProprietario the cd tipo unidade organizacional proprietario
	 */
	public void setCdTipoUnidadeOrganizacionalProprietario(
			int cdTipoUnidadeOrganizacionalProprietario) {
		this.cdTipoUnidadeOrganizacionalProprietario = cdTipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacionalUsuario.
	 *
	 * @return cdTipoUnidadeOrganizacionalUsuario
	 */
	public int getCdTipoUnidadeOrganizacionalUsuario() {
		return cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacionalUsuario.
	 *
	 * @param cdTipoUnidadeOrganizacionalUsuario the cd tipo unidade organizacional usuario
	 */
	public void setCdTipoUnidadeOrganizacionalUsuario(
			int cdTipoUnidadeOrganizacionalUsuario) {
		this.cdTipoUnidadeOrganizacionalUsuario = cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: cdUnidadeOrganizacionalProprietario.
	 *
	 * @return cdUnidadeOrganizacionalProprietario
	 */
	public int getCdUnidadeOrganizacionalProprietario() {
		return cdUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Set: cdUnidadeOrganizacionalProprietario.
	 *
	 * @param cdUnidadeOrganizacionalProprietario the cd unidade organizacional proprietario
	 */
	public void setCdUnidadeOrganizacionalProprietario(
			int cdUnidadeOrganizacionalProprietario) {
		this.cdUnidadeOrganizacionalProprietario = cdUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Get: cdUnidadeOrganizacionalUsuario.
	 *
	 * @return cdUnidadeOrganizacionalUsuario
	 */
	public int getCdUnidadeOrganizacionalUsuario() {
		return cdUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: cdUnidadeOrganizacionalUsuario.
	 *
	 * @param cdUnidadeOrganizacionalUsuario the cd unidade organizacional usuario
	 */
	public void setCdUnidadeOrganizacionalUsuario(int cdUnidadeOrganizacionalUsuario) {
		this.cdUnidadeOrganizacionalUsuario = cdUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: codigoExcecao.
	 *
	 * @return codigoExcecao
	 */
	public int getCodigoExcecao() {
		return codigoExcecao;
	}
	
	/**
	 * Set: codigoExcecao.
	 *
	 * @param codigoExcecao the codigo excecao
	 */
	public void setCodigoExcecao(int codigoExcecao) {
		this.codigoExcecao = codigoExcecao;
	}
	
	/**
	 * Get: codigoRegra.
	 *
	 * @return codigoRegra
	 */
	public int getCodigoRegra() {
		return codigoRegra;
	}
	
	/**
	 * Set: codigoRegra.
	 *
	 * @param codigoRegra the codigo regra
	 */
	public void setCodigoRegra(int codigoRegra) {
		this.codigoRegra = codigoRegra;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: complementolManutencao.
	 *
	 * @return complementolManutencao
	 */
	public String getComplementolManutencao() {
		return complementolManutencao;
	}
	
	/**
	 * Set: complementolManutencao.
	 *
	 * @param complementolManutencao the complementol manutencao
	 */
	public void setComplementolManutencao(String complementolManutencao) {
		this.complementolManutencao = complementolManutencao;
	}
	
	/**
	 * Get: complementoUsuario.
	 *
	 * @return complementoUsuario
	 */
	public String getComplementoUsuario() {
		return complementoUsuario;
	}
	
	/**
	 * Set: complementoUsuario.
	 *
	 * @param complementoUsuario the complemento usuario
	 */
	public void setComplementoUsuario(String complementoUsuario) {
		this.complementoUsuario = complementoUsuario;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoralManutencao.
	 *
	 * @return dataHoralManutencao
	 */
	public String getDataHoralManutencao() {
		return dataHoralManutencao;
	}
	
	/**
	 * Set: dataHoralManutencao.
	 *
	 * @param dataHoralManutencao the data horal manutencao
	 */
	public void setDataHoralManutencao(String dataHoralManutencao) {
		this.dataHoralManutencao = dataHoralManutencao;
	}
	
	/**
	 * Get: dsEmpresaConglomeradoProprietario.
	 *
	 * @return dsEmpresaConglomeradoProprietario
	 */
	public String getDsEmpresaConglomeradoProprietario() {
		return dsEmpresaConglomeradoProprietario;
	}
	
	/**
	 * Set: dsEmpresaConglomeradoProprietario.
	 *
	 * @param dsEmpresaConglomeradoProprietario the ds empresa conglomerado proprietario
	 */
	public void setDsEmpresaConglomeradoProprietario(
			String dsEmpresaConglomeradoProprietario) {
		this.dsEmpresaConglomeradoProprietario = dsEmpresaConglomeradoProprietario;
	}
	
	/**
	 * Get: dsEmpresaConglomeradoUsuario.
	 *
	 * @return dsEmpresaConglomeradoUsuario
	 */
	public String getDsEmpresaConglomeradoUsuario() {
		return dsEmpresaConglomeradoUsuario;
	}
	
	/**
	 * Set: dsEmpresaConglomeradoUsuario.
	 *
	 * @param dsEmpresaConglomeradoUsuario the ds empresa conglomerado usuario
	 */
	public void setDsEmpresaConglomeradoUsuario(String dsEmpresaConglomeradoUsuario) {
		this.dsEmpresaConglomeradoUsuario = dsEmpresaConglomeradoUsuario;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanallManutencao.
	 *
	 * @return dsTipoCanallManutencao
	 */
	public String getDsTipoCanallManutencao() {
		return dsTipoCanallManutencao;
	}
	
	/**
	 * Set: dsTipoCanallManutencao.
	 *
	 * @param dsTipoCanallManutencao the ds tipo canall manutencao
	 */
	public void setDsTipoCanallManutencao(String dsTipoCanallManutencao) {
		this.dsTipoCanallManutencao = dsTipoCanallManutencao;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacionalProprietario.
	 *
	 * @return dsTipoUnidadeOrganizacionalProprietario
	 */
	public String getDsTipoUnidadeOrganizacionalProprietario() {
		return dsTipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacionalProprietario.
	 *
	 * @param dsTipoUnidadeOrganizacionalProprietario the ds tipo unidade organizacional proprietario
	 */
	public void setDsTipoUnidadeOrganizacionalProprietario(
			String dsTipoUnidadeOrganizacionalProprietario) {
		this.dsTipoUnidadeOrganizacionalProprietario = dsTipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacionalUsuario.
	 *
	 * @return dsTipoUnidadeOrganizacionalUsuario
	 */
	public String getDsTipoUnidadeOrganizacionalUsuario() {
		return dsTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacionalUsuario.
	 *
	 * @param dsTipoUnidadeOrganizacionalUsuario the ds tipo unidade organizacional usuario
	 */
	public void setDsTipoUnidadeOrganizacionalUsuario(
			String dsTipoUnidadeOrganizacionalUsuario) {
		this.dsTipoUnidadeOrganizacionalUsuario = dsTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: dsUnidadeOrganizacionalProprietario.
	 *
	 * @return dsUnidadeOrganizacionalProprietario
	 */
	public String getDsUnidadeOrganizacionalProprietario() {
		return dsUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Set: dsUnidadeOrganizacionalProprietario.
	 *
	 * @param dsUnidadeOrganizacionalProprietario the ds unidade organizacional proprietario
	 */
	public void setDsUnidadeOrganizacionalProprietario(
			String dsUnidadeOrganizacionalProprietario) {
		this.dsUnidadeOrganizacionalProprietario = dsUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Get: dsUnidadeOrganizacionalUsuario.
	 *
	 * @return dsUnidadeOrganizacionalUsuario
	 */
	public String getDsUnidadeOrganizacionalUsuario() {
		return dsUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: dsUnidadeOrganizacionalUsuario.
	 *
	 * @param dsUnidadeOrganizacionalUsuario the ds unidade organizacional usuario
	 */
	public void setDsUnidadeOrganizacionalUsuario(
			String dsUnidadeOrganizacionalUsuario) {
		this.dsUnidadeOrganizacionalUsuario = dsUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: tipoAbrangenciaExcecao.
	 *
	 * @return tipoAbrangenciaExcecao
	 */
	public int getTipoAbrangenciaExcecao() {
		return tipoAbrangenciaExcecao;
	}
	
	/**
	 * Set: tipoAbrangenciaExcecao.
	 *
	 * @param tipoAbrangenciaExcecao the tipo abrangencia excecao
	 */
	public void setTipoAbrangenciaExcecao(int tipoAbrangenciaExcecao) {
		this.tipoAbrangenciaExcecao = tipoAbrangenciaExcecao;
	}
	
	/**
	 * Get: tipoExcecao.
	 *
	 * @return tipoExcecao
	 */
	public int getTipoExcecao() {
		return tipoExcecao;
	}
	
	/**
	 * Set: tipoExcecao.
	 *
	 * @param tipoExcecao the tipo excecao
	 */
	public void setTipoExcecao(int tipoExcecao) {
		this.tipoExcecao = tipoExcecao;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuariolManutencao.
	 *
	 * @return usuariolManutencao
	 */
	public String getUsuariolManutencao() {
		return usuariolManutencao;
	}
	
	/**
	 * Set: usuariolManutencao.
	 *
	 * @param usuariolManutencao the usuariol manutencao
	 */
	public void setUsuariolManutencao(String usuariolManutencao) {
		this.usuariolManutencao = usuariolManutencao;
	}
	
	
	
	


	

	


}
