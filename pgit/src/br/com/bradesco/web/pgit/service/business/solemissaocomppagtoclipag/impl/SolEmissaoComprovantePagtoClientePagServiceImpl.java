/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagServiceConstants;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ExcluirSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ExcluirSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.IncluirSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.IncluirSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ListarIncluirSolEmiComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ListarIncluirSolEmiComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.request.ConsultarAgenciaOperadoraRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.response.ConsultarAgenciaOperadoraResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.request.ConsultarAgenciaOpeTarifaPadraoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaopetarifapadrao.response.ConsultarAgenciaOpeTarifaPadraoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.request.ConsultarSolEmiComprovanteCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantecliepagador.response.ConsultarSolEmiComprovanteCliePagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemicomprovantecliepagador.request.DetalharSolEmiComprovanteCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemicomprovantecliepagador.response.DetalharSolEmiComprovanteCliePagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantecliepagador.request.ExcluirSolEmiComprovanteCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantecliepagador.response.ExcluirSolEmiComprovanteCliePagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantecliepagador.request.IncluirSolEmiComprovanteCliePagadorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantecliepagador.response.IncluirSolEmiComprovanteCliePagadorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarincluirsolemicomprovante.request.ListarIncluirSolEmiComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarincluirsolemicomprovante.response.ListarIncluirSolEmiComprovanteResponse;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: SolEmissaoComprovantePagtoClientePag
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SolEmissaoComprovantePagtoClientePagServiceImpl implements
		ISolEmissaoComprovantePagtoClientePagService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * Construtor.
	 */
	public SolEmissaoComprovantePagtoClientePagServiceImpl() {
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService#listarIncluirSolEmiComprovante(br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ListarIncluirSolEmiComprovanteEntradaDTO)
	 */
	public List<ListarIncluirSolEmiComprovanteSaidaDTO> listarIncluirSolEmiComprovante(
			ListarIncluirSolEmiComprovanteEntradaDTO entradaDTO) {
		List<ListarIncluirSolEmiComprovanteSaidaDTO> listaSaida = new ArrayList<ListarIncluirSolEmiComprovanteSaidaDTO>();
		ListarIncluirSolEmiComprovanteRequest request = new ListarIncluirSolEmiComprovanteRequest();
		ListarIncluirSolEmiComprovanteResponse response = new ListarIncluirSolEmiComprovanteResponse();

		request.setNrOcorrencias(PgitUtil.verificaIntegerNulo(entradaDTO
				.getNrOcorrencias()));
		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setDtCreditoPagamentoInicio(PgitUtil
				.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
		request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO
				.getDtCreditoPagamentoFim()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDTO
				.getCdControlePagamentoDe()));
		request.setCdControlePagamentoAte(PgitUtil
				.verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
		request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDTO
				.getVlPagamentoDe()));
		request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDTO
				.getVlPagamentoAte()));
		request.setCdFavorecidoClientePagador(PgitUtil
				.verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
		request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO
				.getCdInscricaoFavorecido()));
		request.setCdIdentificacaoInscricaoFavorecido(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdIdentificacaoInscricaoFavorecido()));

		response = getFactoryAdapter()
				.getListarIncluirSolEmiComprovantePDCAdapter().invokeProcess(
						request);

		ListarIncluirSolEmiComprovanteSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ListarIncluirSolEmiComprovanteSaidaDTO();

			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setNumeroConsultas(response.getNumeroConsultas());
			saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i)
					.getCdProdutoServicoOperacao());
			saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i)
					.getDsResumoProdutoServico());
			saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i)
					.getCdProdutoServicoRelacionado());
			saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i)
					.getDsOperacaoProdutoServico());
			saidaDTO.setCdControlePagamento(response.getOcorrencias(i)
					.getCdControlePagamento());
			saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i)
					.getDtCreditoPagamento());
			saidaDTO.setVlEfetivoPagamento(response.getOcorrencias(i)
					.getVlEfetivoPagamento());
			saidaDTO.setCdInscricaoFavorecido(response.getOcorrencias(i)
					.getCdInscricaoFavorecido());
			saidaDTO.setDsFavorecido(response.getOcorrencias(i)
					.getDsFavorecido());
			saidaDTO.setCdBancoDebito(response.getOcorrencias(i)
					.getCdBancoDebito());
			saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i)
					.getCdAgenciaDebito());
			saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i)
					.getCdDigitoAgenciaDebito());
			saidaDTO.setCdContaDebito(response.getOcorrencias(i)
					.getCdContaDebito());
			saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i)
					.getCdDigitoContaDebito());
			saidaDTO.setDtCreditoPagamentoFormatada(DateUtils
					.formartarDataPDCparaPadraPtBr(response.getOcorrencias(i)
							.getDtCreditoPagamento(), "dd.MM.yyyy",
							"dd/MM/yyyy"));
			saidaDTO.setFavorecidoFormatado(PgitUtil.concatenarCampos(response
					.getOcorrencias(i).getCdInscricaoFavorecido(), response
					.getOcorrencias(i).getDsFavorecido()));
			saidaDTO.setContaDebitoFormatada(PgitUtil.formatBancoAgenciaConta(
					saidaDTO.getCdBancoDebito(), saidaDTO.getCdAgenciaDebito(),
					saidaDTO.getCdDigitoAgenciaDebito(), saidaDTO
							.getCdContaDebito(), saidaDTO
							.getCdDigitoContaDebito(), true));
			saidaDTO.setDsEfetivacaoPagamento(response.getOcorrencias(i)
					.getDsEfetivacaoPagamento());
			saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i)
					.getCdSituacaoOperacaoPagamento());
			saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i)
					.getDsSituacaoOperacaoPagamento());
			saidaDTO.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());

			listaSaida.add(saidaDTO);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService#consultarAgenciaOperadora(br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraEntradaDTO)
	 */
	public ConsultarAgenciaOperadoraSaidaDTO consultarAgenciaOperadora(
			ConsultarAgenciaOperadoraEntradaDTO entradaDTO) {
		ConsultarAgenciaOperadoraSaidaDTO saidaDTO = new ConsultarAgenciaOperadoraSaidaDTO();
		ConsultarAgenciaOperadoraRequest request = new ConsultarAgenciaOperadoraRequest();
		ConsultarAgenciaOperadoraResponse response = new ConsultarAgenciaOperadoraResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));

		response = getFactoryAdapter().getConsultarAgenciaOperadoraPDCAdapter()
				.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdPessoaJuridicaDepartamento(response
				.getCdPessoaJuridicaDepartamento());
		saidaDTO.setNrSequenciaDepartamento(response
				.getNrSequenciaDepartamento());
		saidaDTO.setCdAgencia(response.getCdAgencia());
		saidaDTO.setDsAgencia(response.getDsAgencia());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService#consultarAgenciaOpeTarifaPadrao(br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO)
	 */
	public ConsultarAgenciaOpeTarifaPadraoSaidaDTO consultarAgenciaOpeTarifaPadrao(
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO) {
		ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = new ConsultarAgenciaOpeTarifaPadraoSaidaDTO();
		ConsultarAgenciaOpeTarifaPadraoRequest request = new ConsultarAgenciaOpeTarifaPadraoRequest();
		ConsultarAgenciaOpeTarifaPadraoResponse response = new ConsultarAgenciaOpeTarifaPadraoResponse();

		request.setCdPessoaJuridicaEmpresa(PgitUtil.verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaEmpresa()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setCdTipoTarifa(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTipoTarifa()));

		response = getFactoryAdapter()
				.getConsultarAgenciaOpeTarifaPadraoPDCAdapter().invokeProcess(
						request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setVlTarifaPadrao(response.getVlTarifaPadrao());
		saidaDTO.setCdAgenciaOperadora(response.getCdAgenciaOperadora());
		saidaDTO.setDsAgenciaOperadora(response.getDsAgenciaOperadora());

		saidaDTO.setCdIndicadorDescontoBloqueio(response
				.getCdIndicadorDescontoBloqueio());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService#consultarSolEmiComprovanteCliePagador(br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarSolEmiComprovanteCliePagadorEntradaDTO)
	 */
	public List<ConsultarSolEmiComprovanteCliePagadorSaidaDTO> consultarSolEmiComprovanteCliePagador(
			ConsultarSolEmiComprovanteCliePagadorEntradaDTO entradaDTO) {
		List<ConsultarSolEmiComprovanteCliePagadorSaidaDTO> listaRetorno = new ArrayList<ConsultarSolEmiComprovanteCliePagadorSaidaDTO>();
		ConsultarSolEmiComprovanteCliePagadorRequest request = new ConsultarSolEmiComprovanteCliePagadorRequest();
		ConsultarSolEmiComprovanteCliePagadorResponse response = new ConsultarSolEmiComprovanteCliePagadorResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdSituacaoSolicitacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacao()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setDtFimSolicitacao(PgitUtil.verificaStringNula(entradaDTO
				.getDtFimSolicitacao()));
		request.setDtInicioSolicitacao(PgitUtil.verificaStringNula(entradaDTO
				.getDtInicioSolicitacao()));
		request
				.setNrOcorrencias(ISolEmissaoComprovantePagtoClientePagServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));

		response = getFactoryAdapter()
				.getConsultarSolEmiComprovanteCliePagadorPDCAdapter()
				.invokeProcess(request);
		ConsultarSolEmiComprovanteCliePagadorSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ConsultarSolEmiComprovanteCliePagadorSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setNumeroLinhas(response.getNumeroLinhas());

			saidaDTO.setCdMotivoSituacao(response.getOcorrencias(i)
					.getCdMotivoSituacao());
			saidaDTO.setCdPessoaJuridica(response.getOcorrencias(i)
					.getCdPessoaJuridica());
			saidaDTO.setCdSituacaoSolicitacao(response.getOcorrencias(i)
					.getCdSituacaoSolicitacao());
			saidaDTO
					.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
			saidaDTO.setCdTipoContrato(response.getOcorrencias(i)
					.getCdTipoContrato());
			saidaDTO.setCdTipoSolicitacao(response.getOcorrencias(i)
					.getCdTipoSolicitacao());
			saidaDTO.setCdUsuarioInclusao(response.getOcorrencias(i)
					.getCdUsuarioInclusao());
			saidaDTO.setDsMotivoSituacao(response.getOcorrencias(i)
					.getDsMotivoSituacao());
			saidaDTO.setDsSituacaoSolicitacao(response.getOcorrencias(i)
					.getDsSituacaoSolicitacao());
			saidaDTO
					.setDsTipoCanal(response.getOcorrencias(i).getDsTipoCanal());
			saidaDTO.setDtHoraAtendimento(response.getOcorrencias(i)
					.getDtHoraAtendimento());
			saidaDTO.setDtHoraSolicitacao(FormatarData
					.formatarDataTrilha(response.getOcorrencias(i)
							.getDtHoraSolicitacao()));
			saidaDTO.setNrSequenciaContrato(response.getOcorrencias(i)
					.getNrSequenciaContrato());
			saidaDTO.setNrSolicitacao(response.getOcorrencias(i)
					.getNrSolicitacao());

			saidaDTO.setDtHoraFormatada(FormatarData
					.formatarDataTrilha(saidaDTO.getDtHoraSolicitacao()));
			saidaDTO.setSituacaoSolicitacao(PgitUtil.concatenarCampos(response
					.getOcorrencias(i).getCdSituacaoSolicitacao(), response
					.getOcorrencias(i).getDsSituacaoSolicitacao()));
			saidaDTO.setMotivoSituacao(PgitUtil.concatenarCampos(response
					.getOcorrencias(i).getCdMotivoSituacao(), response
					.getOcorrencias(i).getDsMotivoSituacao()));

			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService#incluirSolEmiComprovanteCliePagador(br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.IncluirSolEmiComprovanteCliePagadorEntradaDTO)
	 */
	public IncluirSolEmiComprovanteCliePagadorSaidaDTO incluirSolEmiComprovanteCliePagador(
			IncluirSolEmiComprovanteCliePagadorEntradaDTO entradaDTO) {
		IncluirSolEmiComprovanteCliePagadorSaidaDTO saidaDTO = new IncluirSolEmiComprovanteCliePagadorSaidaDTO();
		IncluirSolEmiComprovanteCliePagadorRequest request = new IncluirSolEmiComprovanteCliePagadorRequest();
		IncluirSolEmiComprovanteCliePagadorResponse response = new IncluirSolEmiComprovanteCliePagadorResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request
				.setDtInicioPeriodoMovimentacao(PgitUtil
						.verificaStringNula(entradaDTO
								.getDtInicioPeriodoMovimentacao()));
		request.setDtFimPeriodoMovimentacao(PgitUtil
				.verificaStringNula(entradaDTO.getDtFimPeriodoMovimentacao()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdProdutoOperacaoRelacionado()));
		request.setCdRecebedorCredito(PgitUtil.verificaLongNulo(entradaDTO
				.getCdRecebedorCredito()));
		request.setCdTipoInscricaoRecebedor(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoInscricaoRecebedor()));
		request.setCdCpfCnpjRecebedor(PgitUtil.verificaLongNulo(entradaDTO
				.getCdCpfCnpjRecebedor()));
		request.setCdFilialCnpjRecebedor(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdFilialCnpjRecebedor()));
		request.setCdControleCpfRecebedor(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdControleCpfRecebedor()));
		request.setCdDestinoCorrespSolicitacao(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdDestinoCorrespSolicitacao()));
		request.setCdTipoPostagemSolicitacao(PgitUtil
				.verificaStringNula(entradaDTO.getCdTipoPostagemSolicitacao()));
		request.setDsLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO
				.getDsLogradouroPagador()));
		request.setDsNumeroLogradouroPagador(PgitUtil
				.verificaStringNula(entradaDTO.getDsNumeroLogradouroPagador()));
		request.setDsComplementoLogradouroPagador(PgitUtil
				.verificaStringNula(entradaDTO
						.getDsComplementoLogradouroPagador()));
		request.setDsBairroClientePagador(PgitUtil
				.verificaStringNula(entradaDTO.getDsBairroClientePagador()));
		request.setDsMunicipioClientePagador(PgitUtil
				.verificaStringNula(entradaDTO.getDsMunicipioClientePagador()));
		request.setCdSiglaUfPagador(PgitUtil.verificaStringNula(entradaDTO
				.getCdSiglaUfPagador()));
		request.setCdCepPagador(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdCepPagador()));
		request.setCdCepComplementoPagador(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdCepComplementoPagador()));
		request.setDsEmailClientePagador(PgitUtil.verificaStringNula(entradaDTO
				.getDsEmailClientePagador()));
		request.setCdPessoaJuridicaDepto(PgitUtil.verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaDepto()));
		request.setNrSequenciaUnidadeDepto(PgitUtil
				.verificaIntegerNulo(entradaDTO.getNrSequenciaUnidadeDepto()));
		request.setCdPercentualBonifTarifaSolicitacao(PgitUtil
				.verificaBigDecimalNulo(entradaDTO
						.getCdPercentualBonifTarifaSolicitacao()));
		request.setVlTarifaNegocioSolicitacao(PgitUtil
				.verificaBigDecimalNulo(entradaDTO
						.getVlTarifaNegocioSolicitacao()));
		request.setNrOcorrencias(entradaDTO.getListaOcorrencias().size());
		request.setCdDepartamentoUnidade(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdDepartamentoUnidade()));

		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantecliepagador.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantecliepagador.request.Ocorrencias>();

		for (int i = 0; i < entradaDTO.getListaOcorrencias().size(); i++) {
			br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantecliepagador.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantecliepagador.request.Ocorrencias();
			ocorrencia.setCdTipoCanal(entradaDTO.getListaOcorrencias().get(i)
					.getCdTipoCanal());
			ocorrencia.setCdControlePagamento(entradaDTO.getListaOcorrencias()
					.get(i).getCdControlePagamento());
			ocorrencia.setCdModalidadePagamentoCliente(entradaDTO
					.getListaOcorrencias().get(i)
					.getCdModalidadePagamentoCliente());
			ocorrencias.add(ocorrencia);
		}

		request
				.setOcorrencias(ocorrencias
						.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantecliepagador.request.Ocorrencias[0]));

		response = getFactoryAdapter()
				.getIncluirSolEmiComprovanteCliePagadorPDCAdapter()
				.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService#detalharSolEmiComprovanteCliePagador(br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharSolEmiComprovanteCliePagadorEntradaDTO)
	 */
	public DetalharSolEmiComprovanteCliePagadorSaidaDTO detalharSolEmiComprovanteCliePagador(
			DetalharSolEmiComprovanteCliePagadorEntradaDTO entradaDTO) {
		DetalharSolEmiComprovanteCliePagadorSaidaDTO saidaDTO = new DetalharSolEmiComprovanteCliePagadorSaidaDTO();
		DetalharSolEmiComprovanteCliePagadorRequest request = new DetalharSolEmiComprovanteCliePagadorRequest();
		DetalharSolEmiComprovanteCliePagadorResponse response = new DetalharSolEmiComprovanteCliePagadorResponse();

		request.setCdPessoaJuridicaEmpr(PgitUtil.verificaLongNulo(entradaDTO
				.getCdPessoaJuridicaEmpr()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request
				.setCdTipoSolicitacaoPagamento(PgitUtil
						.verificaIntegerNulo(entradaDTO
								.getCdTipoSolicitacaoPagamento()));
		request.setCdSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdSolicitacao()));
		request
				.setNrOcorrencias(ISolEmissaoComprovantePagtoClientePagServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);

		response = getFactoryAdapter()
				.getDetalharSolEmiComprovanteCliePagadorPDCAdapter()
				.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdSolicitacaoPagamentoIntegrado(response
				.getCdSolicitacaoPagamentoIntegrado());
		saidaDTO.setNrSolicitacaoPagamentoIntegrado(response
				.getNrSolicitacaoPagamentoIntegrado());
		saidaDTO.setCdSituacaoSolicitacao(response.getCdSituacaoSolicitacao());
		saidaDTO.setDsSituacaoSolicitacao(response.getDsSituacaoSolicitacao());
		saidaDTO.setDtInicioPeriodoMovimentacao(DateUtils
				.trocarSeparadorDeData(response
						.getDtInicioPeriodoMovimentacao(), ".", "/"));
		saidaDTO.setDtFimPeriodoMovimentacao(DateUtils.trocarSeparadorDeData(
				response.getDtFimPeriodoMovimentacao(), ".", "/"));
		saidaDTO.setCdProdutoServicoOperacao(response
				.getCdProdutoServicoOperacao());
		saidaDTO.setDsProdutoServicoOperacao(response
				.getDsProdutoServicoOperacao());
		saidaDTO.setCdProdutoOperacaoRelacionado(response
				.getCdProdutoOperacaoRelacionado());
		saidaDTO.setDsProdutoOperacaoRelacionado(response
				.getDsProdutoOperacaoRelacionado());
		saidaDTO.setCdRecebedorCredito(response.getCdRecebedorCredito());
		saidaDTO.setCdTipoInscricaoRecebedor(response
				.getCdTipoInscricaoRecebedor());
		saidaDTO.setCdCpfCnpjRecebedor(response.getCdCpfCnpjRecebedor());
		saidaDTO.setCdFilialCnpjRecebedor(response.getCdFilialCnpjRecebedor());
		saidaDTO
				.setCdControleCpfRecebedor(response.getCdControleCpfRecebedor());
		saidaDTO.setCdDestinoCorrespSolicitacao(response
				.getCdDestinoCorrespSolicitacao());
		saidaDTO.setCdTipoPostagemSolicitacao(response
				.getCdTipoPostagemSolicitacao());
		saidaDTO.setDsLogradouroPagador(response.getDsLogradouroPagador());
		saidaDTO.setDsNumeroLogradouroPagador(response
				.getDsNumeroLogradouroPagador());
		saidaDTO.setDsComplementoLogradouroPagador(response
				.getDsComplementoLogradouroPagador());
		saidaDTO
				.setDsBairroClientePagador(response.getDsBairroClientePagador());
		saidaDTO.setDsMunicipioClientePagador(response
				.getDsMunicipioClientePagador());
		saidaDTO.setCdSiglaUfPagador(response.getCdSiglaUfPagador());
		saidaDTO.setCdCepPagador(response.getCdCepPagador());
		saidaDTO.setCdCepComplementoPagador(response
				.getCdCepComplementoPagador());
		saidaDTO.setDsEmailClientePagador(response.getDsEmailClientePagador());
		saidaDTO.setCdAgenciaOperadora(response.getCdAgenciaOperadora());
		saidaDTO.setDsAgencia(response.getDsAgencia());
		saidaDTO.setCdDepartamentoUnidade(response.getCdDepartamentoUnidade());
		saidaDTO.setVlTarifa(response.getVlTarifa());
		saidaDTO
				.setCdPercentualDescTarifa(response.getCdPercentualDescTarifa());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao().equals("0") ? "" : response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setNrLinhas(saidaDTO.getListaOcorrencias().size());
		saidaDTO.setCepFormatado(PgitUtil.formatCep(response.getCdCepPagador(),
				response.getCdCepComplementoPagador()));

		DetalharOcorrenciasDTO ocorrencia;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			ocorrencia = new DetalharOcorrenciasDTO();
			ocorrencia.setCdTipoCanal(response.getOcorrencias(i)
					.getCdTipoCanal());
			ocorrencia.setCdControlePagamento(response.getOcorrencias(i)
					.getCdControlePagamento());
			ocorrencia.setDtPagamento(DateUtils.trocarSeparadorDeData(response
					.getOcorrencias(i).getDtPagamento(), ".", "/"));
			ocorrencia.setVlPagamento(response.getOcorrencias(i)
					.getVlPagamento());
			ocorrencia.setCdFavorecido(response.getOcorrencias(i)
					.getCdFavorecido());
			ocorrencia.setDsFavorecido(response.getOcorrencias(i)
					.getDsFavorecido());
			ocorrencia.setFavorecidoFormatado(PgitUtil.concatenarCampos(
					response.getOcorrencias(i).getCdFavorecido(), response
							.getOcorrencias(i).getDsFavorecido()));
			ocorrencia.setCdBancoDebito(response.getOcorrencias(i)
					.getCdBancoDebito());
			ocorrencia.setCdAgenciaDebito(response.getOcorrencias(i)
					.getCdAgenciaDebito());
			ocorrencia.setCdDigitoAgenciaDebito(response.getOcorrencias(i)
					.getCdDigitoAgenciaDebito());
			ocorrencia.setCdContaDebito(response.getOcorrencias(i)
					.getCdContaDebito());
			ocorrencia.setCdDigitoContaDebito(response.getOcorrencias(i)
					.getCdDigitoContaDebito());
			ocorrencia.setContaDebitoFormatada(PgitUtil
					.formatBancoAgenciaConta(response.getOcorrencias(i)
							.getCdBancoDebito(), response.getOcorrencias(i)
							.getCdAgenciaDebito(), ocorrencia
							.getCdDigitoAgenciaDebito(), response
							.getOcorrencias(i).getCdContaDebito(), response
							.getOcorrencias(i).getCdDigitoContaDebito(), true));
			ocorrencia.setCdProdutoServico(response.getOcorrencias(i)
					.getCdProdutoServico());
			ocorrencia.setCdSituacaoPagamento(response.getOcorrencias(i)
					.getCdSituacaoPagamento());
			ocorrencia.setDsSituacaoPagamento(response.getOcorrencias(i)
					.getDsSituacaoPagamento());
			ocorrencia.setDsCodigoProdutoServicoOperacao(response
					.getOcorrencias(i).getDsCodigoProdutoServicoOperacao());
			ocorrencia.setCdProdutoRelacionado(response.getOcorrencias(i)
					.getCdProdutoRelacionado());
			ocorrencia.setDsCodigoProdutoServicoRelacionado(response
					.getOcorrencias(i).getDsCodigoProdutoServicoRelacionado());

			saidaDTO.getListaOcorrencias().add(ocorrencia);
		}

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService#excluirSolEmiComprovanteCliePagador(br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ExcluirSolEmiComprovanteCliePagadorEntradaDTO)
	 */
	public ExcluirSolEmiComprovanteCliePagadorSaidaDTO excluirSolEmiComprovanteCliePagador(
			ExcluirSolEmiComprovanteCliePagadorEntradaDTO entradaDTO) {
		ExcluirSolEmiComprovanteCliePagadorRequest request = new ExcluirSolEmiComprovanteCliePagadorRequest();
		ExcluirSolEmiComprovanteCliePagadorResponse response = new ExcluirSolEmiComprovanteCliePagadorResponse();
		ExcluirSolEmiComprovanteCliePagadorSaidaDTO saidaDTO = new ExcluirSolEmiComprovanteCliePagadorSaidaDTO();

		request.setCdSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdSolicitacaoPagamentoIntegrado()));
		request.setNrSolicitacaoPagamentoIntegrado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getNrSolicitacaoPagamentoIntegrado()));
		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setNrRegistrosEntrada(PgitUtil.verificaIntegerNulo(entradaDTO
				.getComprovantes().size()));

		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantecliepagador.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantecliepagador.request.Ocorrencias>();

		for (int i = 0; i < entradaDTO.getComprovantes().size(); i++) {
			br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantecliepagador.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantecliepagador.request.Ocorrencias();
			ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO
					.getComprovantes().get(i).getCdTipoCanal()));
			ocorrencia.setCdControlePagamento(PgitUtil
					.verificaStringNula(entradaDTO.getComprovantes().get(i)
							.getCdControlePagamento()));

			ocorrencias.add(ocorrencia);
		}

		request
				.setOcorrencias(ocorrencias
						.toArray(new br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantecliepagador.request.Ocorrencias[0]));

		response = getFactoryAdapter()
				.getExcluirSolEmiComprovanteCliePagadorPDCAdapter()
				.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;
	}
}