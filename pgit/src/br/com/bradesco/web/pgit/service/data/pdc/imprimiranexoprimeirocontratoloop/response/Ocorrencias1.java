/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias1.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias1 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCpfCnpjParticipante
     */
    private java.lang.String _cdCpfCnpjParticipante;

    /**
     * Field _nmParticipante
     */
    private java.lang.String _nmParticipante;

    /**
     * Field _cdGrupoEconomicoParticipante
     */
    private long _cdGrupoEconomicoParticipante = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomicoParticipante
     */
    private boolean _has_cdGrupoEconomicoParticipante;

    /**
     * Field _dsGrupoEconomicoParticipante
     */
    private java.lang.String _dsGrupoEconomicoParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias1() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdGrupoEconomicoParticipante
     * 
     */
    public void deleteCdGrupoEconomicoParticipante()
    {
        this._has_cdGrupoEconomicoParticipante= false;
    } //-- void deleteCdGrupoEconomicoParticipante() 

    /**
     * Returns the value of field 'cdCpfCnpjParticipante'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjParticipante'.
     */
    public java.lang.String getCdCpfCnpjParticipante()
    {
        return this._cdCpfCnpjParticipante;
    } //-- java.lang.String getCdCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdGrupoEconomicoParticipante'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomicoParticipante'.
     */
    public long getCdGrupoEconomicoParticipante()
    {
        return this._cdGrupoEconomicoParticipante;
    } //-- long getCdGrupoEconomicoParticipante() 

    /**
     * Returns the value of field 'dsGrupoEconomicoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomicoParticipante'.
     */
    public java.lang.String getDsGrupoEconomicoParticipante()
    {
        return this._dsGrupoEconomicoParticipante;
    } //-- java.lang.String getDsGrupoEconomicoParticipante() 

    /**
     * Returns the value of field 'nmParticipante'.
     * 
     * @return String
     * @return the value of field 'nmParticipante'.
     */
    public java.lang.String getNmParticipante()
    {
        return this._nmParticipante;
    } //-- java.lang.String getNmParticipante() 

    /**
     * Method hasCdGrupoEconomicoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomicoParticipante()
    {
        return this._has_cdGrupoEconomicoParticipante;
    } //-- boolean hasCdGrupoEconomicoParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCpfCnpjParticipante'.
     * 
     * @param cdCpfCnpjParticipante the value of field
     * 'cdCpfCnpjParticipante'.
     */
    public void setCdCpfCnpjParticipante(java.lang.String cdCpfCnpjParticipante)
    {
        this._cdCpfCnpjParticipante = cdCpfCnpjParticipante;
    } //-- void setCdCpfCnpjParticipante(java.lang.String) 

    /**
     * Sets the value of field 'cdGrupoEconomicoParticipante'.
     * 
     * @param cdGrupoEconomicoParticipante the value of field
     * 'cdGrupoEconomicoParticipante'.
     */
    public void setCdGrupoEconomicoParticipante(long cdGrupoEconomicoParticipante)
    {
        this._cdGrupoEconomicoParticipante = cdGrupoEconomicoParticipante;
        this._has_cdGrupoEconomicoParticipante = true;
    } //-- void setCdGrupoEconomicoParticipante(long) 

    /**
     * Sets the value of field 'dsGrupoEconomicoParticipante'.
     * 
     * @param dsGrupoEconomicoParticipante the value of field
     * 'dsGrupoEconomicoParticipante'.
     */
    public void setDsGrupoEconomicoParticipante(java.lang.String dsGrupoEconomicoParticipante)
    {
        this._dsGrupoEconomicoParticipante = dsGrupoEconomicoParticipante;
    } //-- void setDsGrupoEconomicoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nmParticipante'.
     * 
     * @param nmParticipante the value of field 'nmParticipante'.
     */
    public void setNmParticipante(java.lang.String nmParticipante)
    {
        this._nmParticipante = nmParticipante;
    } //-- void setNmParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias1
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
