/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

/**
 * Nome: ListarSoliRelatorioPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSoliRelatorioPagamentoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dataHoraSolicitacao. */
	private String dataHoraSolicitacao;
    
    /** Atributo cdSolicitacaoPagamento. */
    private Integer cdSolicitacaoPagamento;
    
    /** Atributo nrSolicitacao. */
    private Integer nrSolicitacao;
    
    /** Atributo dsTipoRelatorio. */
    private String dsTipoRelatorio;
    
    /** Atributo dsDataSolicitacao. */
    private String dsDataSolicitacao;
    
    /** Atributo dsHoraSolicitacao. */
    private String dsHoraSolicitacao;
    
    /** Atributo cdSituacao. */
    private String cdSituacao;
    
    /** Atributo cdUsuario. */
    private String cdUsuario;
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: dsTipoRelatorio.
	 *
	 * @return dsTipoRelatorio
	 */
	public String getDsTipoRelatorio() {
		return dsTipoRelatorio;
	}
	
	/**
	 * Set: dsTipoRelatorio.
	 *
	 * @param dsTipoRelatorio the ds tipo relatorio
	 */
	public void setDsTipoRelatorio(String dsTipoRelatorio) {
		this.dsTipoRelatorio = dsTipoRelatorio;
	}
	
	/**
	 * Get: dsDataSolicitacao.
	 *
	 * @return dsDataSolicitacao
	 */
	public String getDsDataSolicitacao() {
		return dsDataSolicitacao;
	}
	
	/**
	 * Set: dsDataSolicitacao.
	 *
	 * @param dsDataSolicitacao the ds data solicitacao
	 */
	public void setDsDataSolicitacao(String dsDataSolicitacao) {
		this.dsDataSolicitacao = dsDataSolicitacao;
	}
	
	/**
	 * Get: dsHoraSolicitacao.
	 *
	 * @return dsHoraSolicitacao
	 */
	public String getDsHoraSolicitacao() {
		return dsHoraSolicitacao;
	}
	
	/**
	 * Set: dsHoraSolicitacao.
	 *
	 * @param dsHoraSolicitacao the ds hora solicitacao
	 */
	public void setDsHoraSolicitacao(String dsHoraSolicitacao) {
		this.dsHoraSolicitacao = dsHoraSolicitacao;
	}
	
	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public String getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}
	
	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(String dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}
	
	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public String getCdSituacao() {
		return cdSituacao;
	}
	
	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(String cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}
	
	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}	

}
