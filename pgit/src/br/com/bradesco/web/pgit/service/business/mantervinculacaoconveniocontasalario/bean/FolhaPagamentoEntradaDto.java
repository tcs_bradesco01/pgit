package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

/**
 * The Class FolhaPagamentoEntradaDto.
 */
public class FolhaPagamentoEntradaDto {

	/** The cdpessoa juridica contrato. */
	private Long cdpessoaJuridicaContrato = null;

	/** The cd tipo contrato negocio. */
	private Integer cdTipoContratoNegocio = null;

	/** The nr sequencia contrato negocio. */
	private Long nrSequenciaContratoNegocio = null;

	/**
	 * Gets the cdpessoa juridica contrato.
	 * 
	 * @return the cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Sets the cdpessoa juridica contrato.
	 * 
	 * @param cdpessoaJuridicaContrato
	 *            the cdpessoaJuridicaContrato to set
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Gets the cd tipo contrato negocio.
	 * 
	 * @return the cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Sets the cd tipo contrato negocio.
	 * 
	 * @param cdTipoContratoNegocio
	 *            the cdTipoContratoNegocio to set
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Gets the nr sequencia contrato negocio.
	 * 
	 * @return the nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Sets the nr sequencia contrato negocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nrSequenciaContratoNegocio to set
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

}
