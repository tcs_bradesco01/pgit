/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdArquivoRetorno
     */
    private int _cdArquivoRetorno = 0;

    /**
     * keeps track of state for field: _cdArquivoRetorno
     */
    private boolean _has_cdArquivoRetorno;

    /**
     * Field _dsArquivoRetorno
     */
    private java.lang.String _dsArquivoRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdArquivoRetorno
     * 
     */
    public void deleteCdArquivoRetorno()
    {
        this._has_cdArquivoRetorno= false;
    } //-- void deleteCdArquivoRetorno() 

    /**
     * Returns the value of field 'cdArquivoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdArquivoRetorno'.
     */
    public int getCdArquivoRetorno()
    {
        return this._cdArquivoRetorno;
    } //-- int getCdArquivoRetorno() 

    /**
     * Returns the value of field 'dsArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsArquivoRetorno'.
     */
    public java.lang.String getDsArquivoRetorno()
    {
        return this._dsArquivoRetorno;
    } //-- java.lang.String getDsArquivoRetorno() 

    /**
     * Method hasCdArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdArquivoRetorno()
    {
        return this._has_cdArquivoRetorno;
    } //-- boolean hasCdArquivoRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdArquivoRetorno'.
     * 
     * @param cdArquivoRetorno the value of field 'cdArquivoRetorno'
     */
    public void setCdArquivoRetorno(int cdArquivoRetorno)
    {
        this._cdArquivoRetorno = cdArquivoRetorno;
        this._has_cdArquivoRetorno = true;
    } //-- void setCdArquivoRetorno(int) 

    /**
     * Sets the value of field 'dsArquivoRetorno'.
     * 
     * @param dsArquivoRetorno the value of field 'dsArquivoRetorno'
     */
    public void setDsArquivoRetorno(java.lang.String dsArquivoRetorno)
    {
        this._dsArquivoRetorno = dsArquivoRetorno;
    } //-- void setDsArquivoRetorno(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
