/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoLoteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoLoteSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLoteLayout. */
	private Integer cdTipoLoteLayout;
	
	/** Atributo dsTipoLoteLayout. */
	private String dsTipoLoteLayout;
	
	/**
	 * Tipo lote saida dto.
	 *
	 * @param cdTipoLoteLayout the cd tipo lote layout
	 * @param dsTipoLoteLayout the ds tipo lote layout
	 */
	public TipoLoteSaidaDTO(Integer cdTipoLoteLayout, String dsTipoLoteLayout) {
		super();
		this.cdTipoLoteLayout = cdTipoLoteLayout;
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdTipoLoteLayout.
	 *
	 * @return cdTipoLoteLayout
	 */
	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}
	
	/**
	 * Set: cdTipoLoteLayout.
	 *
	 * @param cdTipoLoteLayout the cd tipo lote layout
	 */
	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}
	
	/**
	 * Get: dsTipoLoteLayout.
	 *
	 * @return dsTipoLoteLayout
	 */
	public String getDsTipoLoteLayout() {
		return dsTipoLoteLayout;
	}
	
	/**
	 * Set: dsTipoLoteLayout.
	 *
	 * @param dsTipoLoteLayout the ds tipo lote layout
	 */
	public void setDsTipoLoteLayout(String dsTipoLoteLayout) {
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}
	
	

}
