/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

// TODO: Auto-generated Javadoc
/**
 * Nome: OcorrenciasServicoPagamentos
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasServicoPagamentos {
	
	/** Atributo dsServicoTipoServico. */
	private String dsServicoTipoServico;
    
    /** Atributo dsFormaEnvioPagamento. */
    private String dsFormaEnvioPagamento;
    
    /** Atributo dsServicoFormaAutorizacaoPagamento. */
    private String dsServicoFormaAutorizacaoPagamento;
    
    /** Atributo dsIndicadorGeradorRetorno. */
    private String dsIndicadorGeradorRetorno;
    
    /** Atributo dsServicoIndicadorRetornoSeparado. */
    private String dsServicoIndicadorRetornoSeparado;
    
    /** Atributo cdServicoIndicadorListaDebito. */
    private String cdServicoIndicadorListaDebito;
    
    /** Atributo dsServicoListaDebito. */
    private String dsServicoListaDebito;
    
    /** Atributo dsServicoTratamentoListaDebito. */
    private String dsServicoTratamentoListaDebito;
    
    /** Atributo dsServicoAutorizacaoComplementoAgencia. */
    private String dsServicoAutorizacaoComplementoAgencia;
    
    /** Atributo dsServicoPreAutorizacaoCliente. */
    private String dsServicoPreAutorizacaoCliente;
    
    /** The ds servico tipo data float. */
    private String dsServicoTipoDataFloat;
    
    /** The cd servico floating. */
    private String cdServicoFloating;
    
    /** The cd servico tipo consolidado. */
    private String cdServicoTipoConsolidado;
	/**
	 * Get: dsServicoTipoServico.
	 *
	 * @return dsServicoTipoServico
	 */
	public String getDsServicoTipoServico() {
		return dsServicoTipoServico;
	}
	
	/**
	 * Set: dsServicoTipoServico.
	 *
	 * @param dsServicoTipoServico the ds servico tipo servico
	 */
	public void setDsServicoTipoServico(String dsServicoTipoServico) {
		this.dsServicoTipoServico = dsServicoTipoServico;
	}
	
	/**
	 * Get: dsFormaEnvioPagamento.
	 *
	 * @return dsFormaEnvioPagamento
	 */
	public String getDsFormaEnvioPagamento() {
		return dsFormaEnvioPagamento;
	}
	
	/**
	 * Set: dsFormaEnvioPagamento.
	 *
	 * @param dsFormaEnvioPagamento the ds forma envio pagamento
	 */
	public void setDsFormaEnvioPagamento(String dsFormaEnvioPagamento) {
		this.dsFormaEnvioPagamento = dsFormaEnvioPagamento;
	}
	
	/**
	 * Get: dsServicoFormaAutorizacaoPagamento.
	 *
	 * @return dsServicoFormaAutorizacaoPagamento
	 */
	public String getDsServicoFormaAutorizacaoPagamento() {
		return dsServicoFormaAutorizacaoPagamento;
	}
	
	/**
	 * Set: dsServicoFormaAutorizacaoPagamento.
	 *
	 * @param dsServicoFormaAutorizacaoPagamento the ds servico forma autorizacao pagamento
	 */
	public void setDsServicoFormaAutorizacaoPagamento(
			String dsServicoFormaAutorizacaoPagamento) {
		this.dsServicoFormaAutorizacaoPagamento = dsServicoFormaAutorizacaoPagamento;
	}
	
	/**
	 * Get: dsIndicadorGeradorRetorno.
	 *
	 * @return dsIndicadorGeradorRetorno
	 */
	public String getDsIndicadorGeradorRetorno() {
		return dsIndicadorGeradorRetorno;
	}
	
	/**
	 * Set: dsIndicadorGeradorRetorno.
	 *
	 * @param dsIndicadorGeradorRetorno the ds indicador gerador retorno
	 */
	public void setDsIndicadorGeradorRetorno(String dsIndicadorGeradorRetorno) {
		this.dsIndicadorGeradorRetorno = dsIndicadorGeradorRetorno;
	}
	
	/**
	 * Get: dsServicoIndicadorRetornoSeparado.
	 *
	 * @return dsServicoIndicadorRetornoSeparado
	 */
	public String getDsServicoIndicadorRetornoSeparado() {
		return dsServicoIndicadorRetornoSeparado;
	}
	
	/**
	 * Set: dsServicoIndicadorRetornoSeparado.
	 *
	 * @param dsServicoIndicadorRetornoSeparado the ds servico indicador retorno separado
	 */
	public void setDsServicoIndicadorRetornoSeparado(
			String dsServicoIndicadorRetornoSeparado) {
		this.dsServicoIndicadorRetornoSeparado = dsServicoIndicadorRetornoSeparado;
	}
	
	/**
	 * Get: cdServicoIndicadorListaDebito.
	 *
	 * @return cdServicoIndicadorListaDebito
	 */
	public String getCdServicoIndicadorListaDebito() {
		return cdServicoIndicadorListaDebito;
	}
	
	/**
	 * Set: cdServicoIndicadorListaDebito.
	 *
	 * @param cdServicoIndicadorListaDebito the cd servico indicador lista debito
	 */
	public void setCdServicoIndicadorListaDebito(
			String cdServicoIndicadorListaDebito) {
		this.cdServicoIndicadorListaDebito = cdServicoIndicadorListaDebito;
	}
	
	/**
	 * Get: dsServicoListaDebito.
	 *
	 * @return dsServicoListaDebito
	 */
	public String getDsServicoListaDebito() {
		return dsServicoListaDebito;
	}
	
	/**
	 * Set: dsServicoListaDebito.
	 *
	 * @param dsServicoListaDebito the ds servico lista debito
	 */
	public void setDsServicoListaDebito(String dsServicoListaDebito) {
		this.dsServicoListaDebito = dsServicoListaDebito;
	}
	
	/**
	 * Get: dsServicoTratamentoListaDebito.
	 *
	 * @return dsServicoTratamentoListaDebito
	 */
	public String getDsServicoTratamentoListaDebito() {
		return dsServicoTratamentoListaDebito;
	}
	
	/**
	 * Set: dsServicoTratamentoListaDebito.
	 *
	 * @param dsServicoTratamentoListaDebito the ds servico tratamento lista debito
	 */
	public void setDsServicoTratamentoListaDebito(
			String dsServicoTratamentoListaDebito) {
		this.dsServicoTratamentoListaDebito = dsServicoTratamentoListaDebito;
	}
	
	/**
	 * Get: dsServicoAutorizacaoComplementoAgencia.
	 *
	 * @return dsServicoAutorizacaoComplementoAgencia
	 */
	public String getDsServicoAutorizacaoComplementoAgencia() {
		return dsServicoAutorizacaoComplementoAgencia;
	}
	
	/**
	 * Set: dsServicoAutorizacaoComplementoAgencia.
	 *
	 * @param dsServicoAutorizacaoComplementoAgencia the ds servico autorizacao complemento agencia
	 */
	public void setDsServicoAutorizacaoComplementoAgencia(
			String dsServicoAutorizacaoComplementoAgencia) {
		this.dsServicoAutorizacaoComplementoAgencia = dsServicoAutorizacaoComplementoAgencia;
	}
	
	/**
	 * Get: dsServicoPreAutorizacaoCliente.
	 *
	 * @return dsServicoPreAutorizacaoCliente
	 */
	public String getDsServicoPreAutorizacaoCliente() {
		return dsServicoPreAutorizacaoCliente;
	}
	
	/**
	 * Set: dsServicoPreAutorizacaoCliente.
	 *
	 * @param dsServicoPreAutorizacaoCliente the ds servico pre autorizacao cliente
	 */
	public void setDsServicoPreAutorizacaoCliente(
			String dsServicoPreAutorizacaoCliente) {
		this.dsServicoPreAutorizacaoCliente = dsServicoPreAutorizacaoCliente;
	}

	/**
	 * Gets the ds servico tipo data float.
	 *
	 * @return the dsServicoTipoDataFloat
	 */
	public String getDsServicoTipoDataFloat() {
		return dsServicoTipoDataFloat;
	}

	/**
	 * Sets the ds servico tipo data float.
	 *
	 * @param dsServicoTipoDataFloat the dsServicoTipoDataFloat to set
	 */
	public void setDsServicoTipoDataFloat(String dsServicoTipoDataFloat) {
		this.dsServicoTipoDataFloat = dsServicoTipoDataFloat;
	}

	/**
	 * Gets the cd servico floating.
	 *
	 * @return the cdServicoFloating
	 */
	public String getCdServicoFloating() {
		return cdServicoFloating;
	}

	/**
	 * Sets the cd servico floating.
	 *
	 * @param cdServicoFloating the cdServicoFloating to set
	 */
	public void setCdServicoFloating(String cdServicoFloating) {
		this.cdServicoFloating = cdServicoFloating;
	}

	/**
	 * Gets the cd servico tipo consolidado.
	 *
	 * @return the cdServicoTipoConsolidado
	 */
	public String getCdServicoTipoConsolidado() {
		return cdServicoTipoConsolidado;
	}

	/**
	 * Sets the cd servico tipo consolidado.
	 *
	 * @param cdServicoTipoConsolidado the cdServicoTipoConsolidado to set
	 */
	public void setCdServicoTipoConsolidado(String cdServicoTipoConsolidado) {
		this.cdServicoTipoConsolidado = cdServicoTipoConsolidado;
	}
    
    
}
