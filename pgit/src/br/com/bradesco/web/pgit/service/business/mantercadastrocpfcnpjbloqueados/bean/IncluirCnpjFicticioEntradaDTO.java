/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean;

/**
 * Nome: IncluirCnpjFicticioEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirCnpjFicticioEntradaDTO {
	
    /** Atributo cdCnpjCpf. */
    private Long cdCnpjCpf ;
    
    /** Atributo cdFilialCnpjCpf. */
    private Integer cdFilialCnpjCpf;
    
    /** Atributo cdCtrlCpfCnpj. */
    private Integer cdCtrlCpfCnpj;
    
	/**
	 * Get: cdCnpjCpf.
	 *
	 * @return cdCnpjCpf
	 */
	public Long getCdCnpjCpf() {
		return cdCnpjCpf;
	}
	
	/**
	 * Set: cdCnpjCpf.
	 *
	 * @param cdCnpjCpf the cd cnpj cpf
	 */
	public void setCdCnpjCpf(Long cdCnpjCpf) {
		this.cdCnpjCpf = cdCnpjCpf;
	}
	
	/**
	 * Get: cdFilialCnpjCpf.
	 *
	 * @return cdFilialCnpjCpf
	 */
	public Integer getCdFilialCnpjCpf() {
		return cdFilialCnpjCpf;
	}
	
	/**
	 * Set: cdFilialCnpjCpf.
	 *
	 * @param cdFilialCnpjCpf the cd filial cnpj cpf
	 */
	public void setCdFilialCnpjCpf(Integer cdFilialCnpjCpf) {
		this.cdFilialCnpjCpf = cdFilialCnpjCpf;
	}
	
	/**
	 * Get: cdCtrlCpfCnpj.
	 *
	 * @return cdCtrlCpfCnpj
	 */
	public Integer getCdCtrlCpfCnpj() {
		return cdCtrlCpfCnpj;
	}
	
	/**
	 * Set: cdCtrlCpfCnpj.
	 *
	 * @param cdCtrlCpfCnpj the cd ctrl cpf cnpj
	 */
	public void setCdCtrlCpfCnpj(Integer cdCtrlCpfCnpj) {
		this.cdCtrlCpfCnpj = cdCtrlCpfCnpj;
	}

}
