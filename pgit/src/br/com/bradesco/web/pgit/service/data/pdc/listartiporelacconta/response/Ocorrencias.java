/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _tpRelacionamentoContrato
     */
    private int _tpRelacionamentoContrato = 0;

    /**
     * keeps track of state for field: _tpRelacionamentoContrato
     */
    private boolean _has_tpRelacionamentoContrato;

    /**
     * Field _rsTipoRelacionamentoContrato
     */
    private java.lang.String _rsTipoRelacionamentoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteTpRelacionamentoContrato
     * 
     */
    public void deleteTpRelacionamentoContrato()
    {
        this._has_tpRelacionamentoContrato= false;
    } //-- void deleteTpRelacionamentoContrato() 

    /**
     * Returns the value of field 'rsTipoRelacionamentoContrato'.
     * 
     * @return String
     * @return the value of field 'rsTipoRelacionamentoContrato'.
     */
    public java.lang.String getRsTipoRelacionamentoContrato()
    {
        return this._rsTipoRelacionamentoContrato;
    } //-- java.lang.String getRsTipoRelacionamentoContrato() 

    /**
     * Returns the value of field 'tpRelacionamentoContrato'.
     * 
     * @return int
     * @return the value of field 'tpRelacionamentoContrato'.
     */
    public int getTpRelacionamentoContrato()
    {
        return this._tpRelacionamentoContrato;
    } //-- int getTpRelacionamentoContrato() 

    /**
     * Method hasTpRelacionamentoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasTpRelacionamentoContrato()
    {
        return this._has_tpRelacionamentoContrato;
    } //-- boolean hasTpRelacionamentoContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'rsTipoRelacionamentoContrato'.
     * 
     * @param rsTipoRelacionamentoContrato the value of field
     * 'rsTipoRelacionamentoContrato'.
     */
    public void setRsTipoRelacionamentoContrato(java.lang.String rsTipoRelacionamentoContrato)
    {
        this._rsTipoRelacionamentoContrato = rsTipoRelacionamentoContrato;
    } //-- void setRsTipoRelacionamentoContrato(java.lang.String) 

    /**
     * Sets the value of field 'tpRelacionamentoContrato'.
     * 
     * @param tpRelacionamentoContrato the value of field
     * 'tpRelacionamentoContrato'.
     */
    public void setTpRelacionamentoContrato(int tpRelacionamentoContrato)
    {
        this._tpRelacionamentoContrato = tpRelacionamentoContrato;
        this._has_tpRelacionamentoContrato = true;
    } //-- void setTpRelacionamentoContrato(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiporelacconta.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
