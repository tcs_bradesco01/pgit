/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.consultartipoconta.response.Ocorrencias;

/**
 * Nome: ConsultarTipoContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarTipoContaSaidaDTO{
    
    /** Atributo cdEmpresa. */
    private Long cdEmpresa;
    
    /** Atributo cdTipo. */
    private Integer cdTipo;
    
    /** Atributo dsTipo. */
    private String dsTipo;
    
    /** Atributo nrContrato. */
    private Long nrContrato;
    
	/**
	 * Consultar tipo conta saida dto.
	 *
	 * @param saida the saida
	 */
	public ConsultarTipoContaSaidaDTO(Ocorrencias saida) {
		this.cdEmpresa = saida.getCdEmpresa();
		this.cdTipo = saida.getCdTipo();
		this.dsTipo = saida.getDsTipo();
		this.nrContrato = saida.getNrContrato();
	}
	
	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}
	
	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	
	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}
	
	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}
	
	/**
	 * Get: dsTipo.
	 *
	 * @return dsTipo
	 */
	public String getDsTipo() {
		return dsTipo;
	}
	
	/**
	 * Set: dsTipo.
	 *
	 * @param dsTipo the ds tipo
	 */
	public void setDsTipo(String dsTipo) {
		this.dsTipo = dsTipo;
	}
	
	/**
	 * Get: nrContrato.
	 *
	 * @return nrContrato
	 */
	public Long getNrContrato() {
		return nrContrato;
	}
	
	/**
	 * Set: nrContrato.
	 *
	 * @param nrContrato the nr contrato
	 */
	public void setNrContrato(Long nrContrato) {
		this.nrContrato = nrContrato;
	}

}
