package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;

// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 03/05/17.
 */
public class ListarContasExclusaoOcorrenciasSaidaDTO {

    /** The cd pessoa juridica vinculo. */
    private Long cdPessoaJuridicaVinculo;

    /** The cd tipo contrato vinculo. */
    private Integer cdTipoContratoVinculo;

    /** The nr sequencia contrato vinculo. */
    private Long nrSequenciaContratoVinculo;

    /** The cd tipo vinculo contrato. */
    private Integer cdTipoVinculoContrato;

    /** The cd cpf cnpj. */
    private String cdCpfCnpj;

    /** The cd participante. */
    private Long cdParticipante;

    /** The nm participante. */
    private String nmParticipante;

    /** The cd banco. */
    private Integer cdBanco;

    /** The ds banco. */
    private String dsBanco;

    /** The cd agencia. */
    private Integer cdAgencia;

    /** The cd digito agencia. */
    private Integer cdDigitoAgencia;

    /** The ds agencia. */
    private String dsAgencia;

    /** The cd conta. */
    private Long cdConta;

    /** The cd digito conta. */
    private String cdDigitoConta;

    /** The cd tipo conta. */
    private Integer cdTipoConta;

    /** The ds tipo conta. */
    private String dsTipoConta;

    /** The cd situacao. */
    private Integer cdSituacao;

    /** The ds situacao. */
    private String dsSituacao;

    /** The cd situacao vinculacao conta. */
    private Integer cdSituacaoVinculacaoConta;

    /** The ds situacao vinculacao conta. */
    private String dsSituacaoVinculacaoConta;

    /**
     * Get banco formatado.
     *
     * @return the banco formatado
     */
    public String getBancoFormatado() {
        return PgitUtil.formatBanco(getCdBanco(), getDsBanco(), false);
    }

    /**
     * Get agencia formatada.
     *
     * @return the agencia formatada
     */
    public String getAgenciaFormatada() {
        return PgitUtil.formatAgencia(getCdAgencia(), getCdDigitoAgencia(), getDsAgencia(), false);
    }

    /**
     * Get conta formatada.
     *
     * @return the conta formatada
     */
    public String getContaFormatada() {
        return PgitUtil.formatConta(getCdConta(), getCdDigitoConta(), false);
    }

    /**
     * Set cd pessoa juridica vinculo.
     *
     * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
     */
    public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
        this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
    }

    /**
     * Get cd pessoa juridica vinculo.
     *
     * @return the cd pessoa juridica vinculo
     */
    public Long getCdPessoaJuridicaVinculo() {
        return this.cdPessoaJuridicaVinculo;
    }

    /**
     * Set cd tipo contrato vinculo.
     *
     * @param cdTipoContratoVinculo the cd tipo contrato vinculo
     */
    public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
        this.cdTipoContratoVinculo = cdTipoContratoVinculo;
    }

    /**
     * Get cd tipo contrato vinculo.
     *
     * @return the cd tipo contrato vinculo
     */
    public Integer getCdTipoContratoVinculo() {
        return this.cdTipoContratoVinculo;
    }

    /**
     * Set nr sequencia contrato vinculo.
     *
     * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
     */
    public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
        this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
    }

    /**
     * Get nr sequencia contrato vinculo.
     *
     * @return the nr sequencia contrato vinculo
     */
    public Long getNrSequenciaContratoVinculo() {
        return this.nrSequenciaContratoVinculo;
    }

    /**
     * Set cd tipo vinculo contrato.
     *
     * @param cdTipoVinculoContrato the cd tipo vinculo contrato
     */
    public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
        this.cdTipoVinculoContrato = cdTipoVinculoContrato;
    }

    /**
     * Get cd tipo vinculo contrato.
     *
     * @return the cd tipo vinculo contrato
     */
    public Integer getCdTipoVinculoContrato() {
        return this.cdTipoVinculoContrato;
    }

    /**
     * Set cd cpf cnpj.
     *
     * @param cdCpfCnpj the cd cpf cnpj
     */
    public void setCdCpfCnpj(String cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    /**
     * Get cd cpf cnpj.
     *
     * @return the cd cpf cnpj
     */
    public String getCdCpfCnpj() {
        return this.cdCpfCnpj;
    }

    /**
     * Set cd participante.
     *
     * @param cdParticipante the cd participante
     */
    public void setCdParticipante(Long cdParticipante) {
        this.cdParticipante = cdParticipante;
    }

    /**
     * Get cd participante.
     *
     * @return the cd participante
     */
    public Long getCdParticipante() {
        return this.cdParticipante;
    }

    /**
     * Set nm participante.
     *
     * @param nmParticipante the nm participante
     */
    public void setNmParticipante(String nmParticipante) {
        this.nmParticipante = nmParticipante;
    }

    /**
     * Get nm participante.
     *
     * @return the nm participante
     */
    public String getNmParticipante() {
        return this.nmParticipante;
    }

    /**
     * Set cd banco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
        this.cdBanco = cdBanco;
    }

    /**
     * Get cd banco.
     *
     * @return the cd banco
     */
    public Integer getCdBanco() {
        return this.cdBanco;
    }

    /**
     * Set ds banco.
     *
     * @param dsBanco the ds banco
     */
    public void setDsBanco(String dsBanco) {
        this.dsBanco = dsBanco;
    }

    /**
     * Get ds banco.
     *
     * @return the ds banco
     */
    public String getDsBanco() {
        return this.dsBanco;
    }

    /**
     * Set cd agencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
        this.cdAgencia = cdAgencia;
    }

    /**
     * Get cd agencia.
     *
     * @return the cd agencia
     */
    public Integer getCdAgencia() {
        return this.cdAgencia;
    }

    /**
     * Set cd digito agencia.
     *
     * @param cdDigitoAgencia the cd digito agencia
     */
    public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
        this.cdDigitoAgencia = cdDigitoAgencia;
    }

    /**
     * Get cd digito agencia.
     *
     * @return the cd digito agencia
     */
    public Integer getCdDigitoAgencia() {
        return this.cdDigitoAgencia;
    }

    /**
     * Set ds agencia.
     *
     * @param dsAgencia the ds agencia
     */
    public void setDsAgencia(String dsAgencia) {
        this.dsAgencia = dsAgencia;
    }

    /**
     * Get ds agencia.
     *
     * @return the ds agencia
     */
    public String getDsAgencia() {
        return this.dsAgencia;
    }

    /**
     * Set cd conta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
        this.cdConta = cdConta;
    }

    /**
     * Get cd conta.
     *
     * @return the cd conta
     */
    public Long getCdConta() {
        return this.cdConta;
    }

    /**
     * Set cd digito conta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
        this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Get cd digito conta.
     *
     * @return the cd digito conta
     */
    public String getCdDigitoConta() {
        return this.cdDigitoConta;
    }

    /**
     * Set cd tipo conta.
     *
     * @param cdTipoConta the cd tipo conta
     */
    public void setCdTipoConta(Integer cdTipoConta) {
        this.cdTipoConta = cdTipoConta;
    }

    /**
     * Get cd tipo conta.
     *
     * @return the cd tipo conta
     */
    public Integer getCdTipoConta() {
        return this.cdTipoConta;
    }

    /**
     * Set ds tipo conta.
     *
     * @param dsTipoConta the ds tipo conta
     */
    public void setDsTipoConta(String dsTipoConta) {
        this.dsTipoConta = dsTipoConta;
    }

    /**
     * Get ds tipo conta.
     *
     * @return the ds tipo conta
     */
    public String getDsTipoConta() {
        return this.dsTipoConta;
    }

    /**
     * Set cd situacao.
     *
     * @param cdSituacao the cd situacao
     */
    public void setCdSituacao(Integer cdSituacao) {
        this.cdSituacao = cdSituacao;
    }

    /**
     * Get cd situacao.
     *
     * @return the cd situacao
     */
    public Integer getCdSituacao() {
        return this.cdSituacao;
    }

    /**
     * Set ds situacao.
     *
     * @param dsSituacao the ds situacao
     */
    public void setDsSituacao(String dsSituacao) {
        this.dsSituacao = dsSituacao;
    }

    /**
     * Get ds situacao.
     *
     * @return the ds situacao
     */
    public String getDsSituacao() {
        return this.dsSituacao;
    }

    /**
     * Set cd situacao vinculacao conta.
     *
     * @param cdSituacaoVinculacaoConta the cd situacao vinculacao conta
     */
    public void setCdSituacaoVinculacaoConta(Integer cdSituacaoVinculacaoConta) {
        this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
    }

    /**
     * Get cd situacao vinculacao conta.
     *
     * @return the cd situacao vinculacao conta
     */
    public Integer getCdSituacaoVinculacaoConta() {
        return this.cdSituacaoVinculacaoConta;
    }

    /**
     * Set ds situacao vinculacao conta.
     *
     * @param dsSituacaoVinculacaoConta the ds situacao vinculacao conta
     */
    public void setDsSituacaoVinculacaoConta(String dsSituacaoVinculacaoConta) {
        this.dsSituacaoVinculacaoConta = dsSituacaoVinculacaoConta;
    }

    /**
     * Get ds situacao vinculacao conta.
     *
     * @return the ds situacao vinculacao conta
     */
    public String getDsSituacaoVinculacaoConta() {
        return this.dsSituacaoVinculacaoConta;
    }
}