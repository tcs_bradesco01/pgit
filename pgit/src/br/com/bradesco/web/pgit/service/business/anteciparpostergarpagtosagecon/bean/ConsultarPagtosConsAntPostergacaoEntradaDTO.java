/*
 * Nome: br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;

/**
 * Nome: ConsultarPagtosConsAntPostergacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtosConsAntPostergacaoEntradaDTO{
    
    /** Atributo cdTipoPesquisa. */
    private Integer cdTipoPesquisa;
    
    /** Atributo cdAgendamentoPagosNaoPagos. */
    private Integer cdAgendamentoPagosNaoPagos;
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato;
    
    /** Atributo nrSequencialContrato. */
    private Long nrSequencialContrato;
    
    /** Atributo dtInicialPagamento. */
    private String dtInicialPagamento;
    
    /** Atributo dtFinalPagamento. */
    private String dtFinalPagamento;
    
    /** Atributo nrRemessa. */
    private Long nrRemessa;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;
    
    /** Atributo cdMotivoSituacaoPagamento. */
    private Integer cdMotivoSituacaoPagamento;
    
    /** Atributo cdTipoAgendamento. */
    private Integer cdTipoAgendamento;
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdAgendamentoPagosNaoPagos.
	 *
	 * @return cdAgendamentoPagosNaoPagos
	 */
	public Integer getCdAgendamentoPagosNaoPagos() {
		return cdAgendamentoPagosNaoPagos;
	}
	
	/**
	 * Set: cdAgendamentoPagosNaoPagos.
	 *
	 * @param cdAgendamentoPagosNaoPagos the cd agendamento pagos nao pagos
	 */
	public void setCdAgendamentoPagosNaoPagos(Integer cdAgendamentoPagosNaoPagos) {
		this.cdAgendamentoPagosNaoPagos = cdAgendamentoPagosNaoPagos;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdMotivoSituacaoPagamento.
	 *
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}
	
	/**
	 * Set: cdMotivoSituacaoPagamento.
	 *
	 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoPesquisa.
	 *
	 * @return cdTipoPesquisa
	 */
	public Integer getCdTipoPesquisa() {
		return cdTipoPesquisa;
	}
	
	/**
	 * Set: cdTipoPesquisa.
	 *
	 * @param cdTipoPesquisa the cd tipo pesquisa
	 */
	public void setCdTipoPesquisa(Integer cdTipoPesquisa) {
		this.cdTipoPesquisa = cdTipoPesquisa;
	}
	
	/**
	 * Get: dtFinalPagamento.
	 *
	 * @return dtFinalPagamento
	 */
	public String getDtFinalPagamento() {
		return dtFinalPagamento;
	}
	
	/**
	 * Set: dtFinalPagamento.
	 *
	 * @param dtFinalPagamento the dt final pagamento
	 */
	public void setDtFinalPagamento(String dtFinalPagamento) {
		this.dtFinalPagamento = dtFinalPagamento;
	}
	
	/**
	 * Get: dtInicialPagamento.
	 *
	 * @return dtInicialPagamento
	 */
	public String getDtInicialPagamento() {
		return dtInicialPagamento;
	}
	
	/**
	 * Set: dtInicialPagamento.
	 *
	 * @param dtInicialPagamento the dt inicial pagamento
	 */
	public void setDtInicialPagamento(String dtInicialPagamento) {
		this.dtInicialPagamento = dtInicialPagamento;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrRemessa.
	 *
	 * @return nrRemessa
	 */
	public Long getNrRemessa() {
		return nrRemessa;
	}
	
	/**
	 * Set: nrRemessa.
	 *
	 * @param nrRemessa the nr remessa
	 */
	public void setNrRemessa(Long nrRemessa) {
		this.nrRemessa = nrRemessa;
	}
	
	/**
	 * Get: nrSequencialContrato.
	 *
	 * @return nrSequencialContrato
	 */
	public Long getNrSequencialContrato() {
		return nrSequencialContrato;
	}
	
	/**
	 * Set: nrSequencialContrato.
	 *
	 * @param nrSequencialContrato the nr sequencial contrato
	 */
	public void setNrSequencialContrato(Long nrSequencialContrato) {
		this.nrSequencialContrato = nrSequencialContrato;
	}
	
	/**
	 * Get: cdTipoAgendamento.
	 *
	 * @return cdTipoAgendamento
	 */
	public Integer getCdTipoAgendamento() {
		return cdTipoAgendamento;
	}
	
	/**
	 * Set: cdTipoAgendamento.
	 *
	 * @param cdTipoAgendamento the cd tipo agendamento
	 */
	public void setCdTipoAgendamento(Integer cdTipoAgendamento) {
		this.cdTipoAgendamento = cdTipoAgendamento;
	}
    
    
}
