/*
 * Nome: br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean;

/**
 * Nome: ListarPrioridadeCredTipoCompSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarPrioridadeCredTipoCompSaidaDTO {

	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo tipoCompromisso. */
	private int tipoCompromisso;
	
	/** Atributo prioridade. */
	private int prioridade;
	
	/** Atributo dataInicioVigencia. */
	private String dataInicioVigencia;
	
	/** Atributo dataFimVigencia. */
	private String dataFimVigencia;
	
	/** Atributo cdCustoPrioridadeProduto. */
	private String cdCustoPrioridadeProduto;
	
	/** Atributo dsCustoPrioridadeProduto. */
	private String dsCustoPrioridadeProduto;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/**
	 * Get: dataInicioVigencia.
	 *
	 * @return dataInicioVigencia
	 */
	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}
	
	/**
	 * Set: dataInicioVigencia.
	 *
	 * @param dataInicioVigencia the data inicio vigencia
	 */
	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}
	
	/**
	 * Get: prioridade.
	 *
	 * @return prioridade
	 */
	public int getPrioridade() {
		return prioridade;
	}
	
	/**
	 * Set: prioridade.
	 *
	 * @param prioridade the prioridade
	 */
	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: tipoCompromisso.
	 *
	 * @return tipoCompromisso
	 */
	public int getTipoCompromisso() {
		return tipoCompromisso;
	}
	
	/**
	 * Set: tipoCompromisso.
	 *
	 * @param tipoCompromisso the tipo compromisso
	 */
	public void setTipoCompromisso(int tipoCompromisso) {
		this.tipoCompromisso = tipoCompromisso;
	}
	
	/**
	 * Get: dataFimVigencia.
	 *
	 * @return dataFimVigencia
	 */
	public String getDataFimVigencia() {
		return dataFimVigencia;
	}
	
	/**
	 * Set: dataFimVigencia.
	 *
	 * @param dataFimVigencia the data fim vigencia
	 */
	public void setDataFimVigencia(String dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}
	
	/**
	 * Get: cdCustoPrioridadeProduto.
	 *
	 * @return cdCustoPrioridadeProduto
	 */
	public String getCdCustoPrioridadeProduto() {
		return cdCustoPrioridadeProduto;
	}
	
	/**
	 * Set: cdCustoPrioridadeProduto.
	 *
	 * @param cdCustoPrioridadeProduto the cd custo prioridade produto
	 */
	public void setCdCustoPrioridadeProduto(String cdCustoPrioridadeProduto) {
		this.cdCustoPrioridadeProduto = cdCustoPrioridadeProduto;
	}
	
	/**
	 * Get: dsCustoPrioridadeProduto.
	 *
	 * @return dsCustoPrioridadeProduto
	 */
	public String getDsCustoPrioridadeProduto() {
		return dsCustoPrioridadeProduto;
	}
	
	/**
	 * Set: dsCustoPrioridadeProduto.
	 *
	 * @param dsCustoPrioridadeProduto the ds custo prioridade produto
	 */
	public void setDsCustoPrioridadeProduto(String dsCustoPrioridadeProduto) {
		this.dsCustoPrioridadeProduto = dsCustoPrioridadeProduto;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
}
