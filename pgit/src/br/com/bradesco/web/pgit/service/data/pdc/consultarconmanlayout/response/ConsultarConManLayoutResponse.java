/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarConManLayoutResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarConManLayoutResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdSituacaoLayoutContrato
     */
    private int _cdSituacaoLayoutContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoLayoutContrato
     */
    private boolean _has_cdSituacaoLayoutContrato;

    /**
     * Field _dsSituacaoLayoutContrato
     */
    private java.lang.String _dsSituacaoLayoutContrato;

    /**
     * Field _cdResponsavelCustoEmpresa
     */
    private int _cdResponsavelCustoEmpresa = 0;

    /**
     * keeps track of state for field: _cdResponsavelCustoEmpresa
     */
    private boolean _has_cdResponsavelCustoEmpresa;

    /**
     * Field _dsResponsavelCustoEmpresa
     */
    private java.lang.String _dsResponsavelCustoEmpresa;

    /**
     * Field _percentualCustoOrganizacaoTransmissao
     */
    private java.math.BigDecimal _percentualCustoOrganizacaoTransmissao = new java.math.BigDecimal("0");

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExter
     */
    private java.lang.String _cdUsuarioInclusaoExter;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExter
     */
    private java.lang.String _cdUsuarioManutencaoExter;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdMsgLinExtrt
     */
    private int _cdMsgLinExtrt = 0;

    /**
     * keeps track of state for field: _cdMsgLinExtrt
     */
    private boolean _has_cdMsgLinExtrt;

    /**
     * Field _cdTipoControlePagamento
     */
    private int _cdTipoControlePagamento = 0;

    /**
     * keeps track of state for field: _cdTipoControlePagamento
     */
    private boolean _has_cdTipoControlePagamento;

    /**
     * Field _dsTipoControlePagamento
     */
    private java.lang.String _dsTipoControlePagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarConManLayoutResponse() 
     {
        super();
        setPercentualCustoOrganizacaoTransmissao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.ConsultarConManLayoutResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMsgLinExtrt
     * 
     */
    public void deleteCdMsgLinExtrt()
    {
        this._has_cdMsgLinExtrt= false;
    } //-- void deleteCdMsgLinExtrt() 

    /**
     * Method deleteCdResponsavelCustoEmpresa
     * 
     */
    public void deleteCdResponsavelCustoEmpresa()
    {
        this._has_cdResponsavelCustoEmpresa= false;
    } //-- void deleteCdResponsavelCustoEmpresa() 

    /**
     * Method deleteCdSituacaoLayoutContrato
     * 
     */
    public void deleteCdSituacaoLayoutContrato()
    {
        this._has_cdSituacaoLayoutContrato= false;
    } //-- void deleteCdSituacaoLayoutContrato() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoControlePagamento
     * 
     */
    public void deleteCdTipoControlePagamento()
    {
        this._has_cdTipoControlePagamento= false;
    } //-- void deleteCdTipoControlePagamento() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdMsgLinExtrt'.
     * 
     * @return int
     * @return the value of field 'cdMsgLinExtrt'.
     */
    public int getCdMsgLinExtrt()
    {
        return this._cdMsgLinExtrt;
    } //-- int getCdMsgLinExtrt() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdResponsavelCustoEmpresa'.
     * 
     * @return int
     * @return the value of field 'cdResponsavelCustoEmpresa'.
     */
    public int getCdResponsavelCustoEmpresa()
    {
        return this._cdResponsavelCustoEmpresa;
    } //-- int getCdResponsavelCustoEmpresa() 

    /**
     * Returns the value of field 'cdSituacaoLayoutContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoLayoutContrato'.
     */
    public int getCdSituacaoLayoutContrato()
    {
        return this._cdSituacaoLayoutContrato;
    } //-- int getCdSituacaoLayoutContrato() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoControlePagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoControlePagamento'.
     */
    public int getCdTipoControlePagamento()
    {
        return this._cdTipoControlePagamento;
    } //-- int getCdTipoControlePagamento() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExter'.
     */
    public java.lang.String getCdUsuarioInclusaoExter()
    {
        return this._cdUsuarioInclusaoExter;
    } //-- java.lang.String getCdUsuarioInclusaoExter() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExter'.
     */
    public java.lang.String getCdUsuarioManutencaoExter()
    {
        return this._cdUsuarioManutencaoExter;
    } //-- java.lang.String getCdUsuarioManutencaoExter() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsResponsavelCustoEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsResponsavelCustoEmpresa'.
     */
    public java.lang.String getDsResponsavelCustoEmpresa()
    {
        return this._dsResponsavelCustoEmpresa;
    } //-- java.lang.String getDsResponsavelCustoEmpresa() 

    /**
     * Returns the value of field 'dsSituacaoLayoutContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoLayoutContrato'.
     */
    public java.lang.String getDsSituacaoLayoutContrato()
    {
        return this._dsSituacaoLayoutContrato;
    } //-- java.lang.String getDsSituacaoLayoutContrato() 

    /**
     * Returns the value of field 'dsTipoControlePagamento'.
     * 
     * @return String
     * @return the value of field 'dsTipoControlePagamento'.
     */
    public java.lang.String getDsTipoControlePagamento()
    {
        return this._dsTipoControlePagamento;
    } //-- java.lang.String getDsTipoControlePagamento() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field
     * 'percentualCustoOrganizacaoTransmissao'.
     * 
     * @return BigDecimal
     * @return the value of field
     * 'percentualCustoOrganizacaoTransmissao'.
     */
    public java.math.BigDecimal getPercentualCustoOrganizacaoTransmissao()
    {
        return this._percentualCustoOrganizacaoTransmissao;
    } //-- java.math.BigDecimal getPercentualCustoOrganizacaoTransmissao() 

    /**
     * Method hasCdMsgLinExtrt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMsgLinExtrt()
    {
        return this._has_cdMsgLinExtrt;
    } //-- boolean hasCdMsgLinExtrt() 

    /**
     * Method hasCdResponsavelCustoEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdResponsavelCustoEmpresa()
    {
        return this._has_cdResponsavelCustoEmpresa;
    } //-- boolean hasCdResponsavelCustoEmpresa() 

    /**
     * Method hasCdSituacaoLayoutContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoLayoutContrato()
    {
        return this._has_cdSituacaoLayoutContrato;
    } //-- boolean hasCdSituacaoLayoutContrato() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoControlePagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoControlePagamento()
    {
        return this._has_cdTipoControlePagamento;
    } //-- boolean hasCdTipoControlePagamento() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMsgLinExtrt'.
     * 
     * @param cdMsgLinExtrt the value of field 'cdMsgLinExtrt'.
     */
    public void setCdMsgLinExtrt(int cdMsgLinExtrt)
    {
        this._cdMsgLinExtrt = cdMsgLinExtrt;
        this._has_cdMsgLinExtrt = true;
    } //-- void setCdMsgLinExtrt(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdResponsavelCustoEmpresa'.
     * 
     * @param cdResponsavelCustoEmpresa the value of field
     * 'cdResponsavelCustoEmpresa'.
     */
    public void setCdResponsavelCustoEmpresa(int cdResponsavelCustoEmpresa)
    {
        this._cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
        this._has_cdResponsavelCustoEmpresa = true;
    } //-- void setCdResponsavelCustoEmpresa(int) 

    /**
     * Sets the value of field 'cdSituacaoLayoutContrato'.
     * 
     * @param cdSituacaoLayoutContrato the value of field
     * 'cdSituacaoLayoutContrato'.
     */
    public void setCdSituacaoLayoutContrato(int cdSituacaoLayoutContrato)
    {
        this._cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
        this._has_cdSituacaoLayoutContrato = true;
    } //-- void setCdSituacaoLayoutContrato(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoControlePagamento'.
     * 
     * @param cdTipoControlePagamento the value of field
     * 'cdTipoControlePagamento'.
     */
    public void setCdTipoControlePagamento(int cdTipoControlePagamento)
    {
        this._cdTipoControlePagamento = cdTipoControlePagamento;
        this._has_cdTipoControlePagamento = true;
    } //-- void setCdTipoControlePagamento(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @param cdUsuarioInclusaoExter the value of field
     * 'cdUsuarioInclusaoExter'.
     */
    public void setCdUsuarioInclusaoExter(java.lang.String cdUsuarioInclusaoExter)
    {
        this._cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
    } //-- void setCdUsuarioInclusaoExter(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @param cdUsuarioManutencaoExter the value of field
     * 'cdUsuarioManutencaoExter'.
     */
    public void setCdUsuarioManutencaoExter(java.lang.String cdUsuarioManutencaoExter)
    {
        this._cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
    } //-- void setCdUsuarioManutencaoExter(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsResponsavelCustoEmpresa'.
     * 
     * @param dsResponsavelCustoEmpresa the value of field
     * 'dsResponsavelCustoEmpresa'.
     */
    public void setDsResponsavelCustoEmpresa(java.lang.String dsResponsavelCustoEmpresa)
    {
        this._dsResponsavelCustoEmpresa = dsResponsavelCustoEmpresa;
    } //-- void setDsResponsavelCustoEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoLayoutContrato'.
     * 
     * @param dsSituacaoLayoutContrato the value of field
     * 'dsSituacaoLayoutContrato'.
     */
    public void setDsSituacaoLayoutContrato(java.lang.String dsSituacaoLayoutContrato)
    {
        this._dsSituacaoLayoutContrato = dsSituacaoLayoutContrato;
    } //-- void setDsSituacaoLayoutContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoControlePagamento'.
     * 
     * @param dsTipoControlePagamento the value of field
     * 'dsTipoControlePagamento'.
     */
    public void setDsTipoControlePagamento(java.lang.String dsTipoControlePagamento)
    {
        this._dsTipoControlePagamento = dsTipoControlePagamento;
    } //-- void setDsTipoControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field
     * 'percentualCustoOrganizacaoTransmissao'.
     * 
     * @param percentualCustoOrganizacaoTransmissao the value of
     * field 'percentualCustoOrganizacaoTransmissao'.
     */
    public void setPercentualCustoOrganizacaoTransmissao(java.math.BigDecimal percentualCustoOrganizacaoTransmissao)
    {
        this._percentualCustoOrganizacaoTransmissao = percentualCustoOrganizacaoTransmissao;
    } //-- void setPercentualCustoOrganizacaoTransmissao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarConManLayoutResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.ConsultarConManLayoutResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.ConsultarConManLayoutResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.ConsultarConManLayoutResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.ConsultarConManLayoutResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
