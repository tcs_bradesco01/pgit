/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConRelacaoConManutencoesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConRelacaoConManutencoesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ExcluirManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ExcluirManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.IncluirManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.IncluirManterFormAltContratoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Manterformalizacaoalteracaocontrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterformalizacaoalteracaocontratoService {

	/**
	 * Consultar lista manter form alt contrato.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar manter form alt contrato saida dt o>
	 */
	List<ConsultarManterFormAltContratoSaidaDTO> consultarListaManterFormAltContrato(ConsultarManterFormAltContratoEntradaDTO entradaDTO);
	
	/**
	 * Incluir manter form alt contrato saida dto.
	 *
	 * @param incluirManterFormAltContratoEntradaDTO the incluir manter form alt contrato entrada dto
	 * @return the incluir manter form alt contrato saida dto
	 */
	IncluirManterFormAltContratoSaidaDTO incluirManterFormAltContratoSaidaDTO(IncluirManterFormAltContratoEntradaDTO incluirManterFormAltContratoEntradaDTO);
	
	/**
	 * Excluir manter form alt contrato saida dto.
	 *
	 * @param excluirManterFormAltContratoEntradaDTO the excluir manter form alt contrato entrada dto
	 * @return the excluir manter form alt contrato saida dto
	 */
	ExcluirManterFormAltContratoSaidaDTO excluirManterFormAltContratoSaidaDTO(ExcluirManterFormAltContratoEntradaDTO excluirManterFormAltContratoEntradaDTO);
	
	/**
	 * Detalhar manter form alt contrato saida dto.
	 *
	 * @param detalharManterFormAltContratoEntradaDTO the detalhar manter form alt contrato entrada dto
	 * @return the detalhar manter form alt contrato saida dto
	 */
	DetalharManterFormAltContratoSaidaDTO detalharManterFormAltContratoSaidaDTO(DetalharManterFormAltContratoEntradaDTO detalharManterFormAltContratoEntradaDTO);
	
	/**
	 * Con relacao con manutencoes.
	 *
	 * @param entrada the entrada
	 * @return the con relacao con manutencoes saida dto
	 */
	ConRelacaoConManutencoesSaidaDTO conRelacaoConManutencoes(ConRelacaoConManutencoesEntradaDTO entrada);	
	
	
}

