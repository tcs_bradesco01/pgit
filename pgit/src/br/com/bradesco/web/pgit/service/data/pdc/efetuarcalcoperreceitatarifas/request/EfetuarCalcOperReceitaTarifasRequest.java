/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class EfetuarCalcOperReceitaTarifasRequest.
 * 
 * @version $Revision$ $Date$
 */
public class EfetuarCalcOperReceitaTarifasRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _vlTarifaTotalPad
     */
    private java.math.BigDecimal _vlTarifaTotalPad = new java.math.BigDecimal("0");

    /**
     * Field _vlTarifaTotalPro
     */
    private java.math.BigDecimal _vlTarifaTotalPro = new java.math.BigDecimal("0");

    /**
     * Field _qtOcorrencias
     */
    private int _qtOcorrencias = 0;

    /**
     * keeps track of state for field: _qtOcorrencias
     */
    private boolean _has_qtOcorrencias;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public EfetuarCalcOperReceitaTarifasRequest() 
     {
        super();
        setVlTarifaTotalPad(new java.math.BigDecimal("0"));
        setVlTarifaTotalPro(new java.math.BigDecimal("0"));
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.EfetuarCalcOperReceitaTarifasRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias) 

    /**
     * Method deleteQtOcorrencias
     * 
     */
    public void deleteQtOcorrencias()
    {
        this._has_qtOcorrencias= false;
    } //-- void deleteQtOcorrencias() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'qtOcorrencias'.
     * 
     * @return int
     * @return the value of field 'qtOcorrencias'.
     */
    public int getQtOcorrencias()
    {
        return this._qtOcorrencias;
    } //-- int getQtOcorrencias() 

    /**
     * Returns the value of field 'vlTarifaTotalPad'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaTotalPad'.
     */
    public java.math.BigDecimal getVlTarifaTotalPad()
    {
        return this._vlTarifaTotalPad;
    } //-- java.math.BigDecimal getVlTarifaTotalPad() 

    /**
     * Returns the value of field 'vlTarifaTotalPro'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaTotalPro'.
     */
    public java.math.BigDecimal getVlTarifaTotalPro()
    {
        return this._vlTarifaTotalPro;
    } //-- java.math.BigDecimal getVlTarifaTotalPro() 

    /**
     * Method hasQtOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOcorrencias()
    {
        return this._has_qtOcorrencias;
    } //-- boolean hasQtOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias) 

    /**
     * Sets the value of field 'qtOcorrencias'.
     * 
     * @param qtOcorrencias the value of field 'qtOcorrencias'.
     */
    public void setQtOcorrencias(int qtOcorrencias)
    {
        this._qtOcorrencias = qtOcorrencias;
        this._has_qtOcorrencias = true;
    } //-- void setQtOcorrencias(int) 

    /**
     * Sets the value of field 'vlTarifaTotalPad'.
     * 
     * @param vlTarifaTotalPad the value of field 'vlTarifaTotalPad'
     */
    public void setVlTarifaTotalPad(java.math.BigDecimal vlTarifaTotalPad)
    {
        this._vlTarifaTotalPad = vlTarifaTotalPad;
    } //-- void setVlTarifaTotalPad(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaTotalPro'.
     * 
     * @param vlTarifaTotalPro the value of field 'vlTarifaTotalPro'
     */
    public void setVlTarifaTotalPro(java.math.BigDecimal vlTarifaTotalPro)
    {
        this._vlTarifaTotalPro = vlTarifaTotalPro;
    } //-- void setVlTarifaTotalPro(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return EfetuarCalcOperReceitaTarifasRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.EfetuarCalcOperReceitaTarifasRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.EfetuarCalcOperReceitaTarifasRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.EfetuarCalcOperReceitaTarifasRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.EfetuarCalcOperReceitaTarifasRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
