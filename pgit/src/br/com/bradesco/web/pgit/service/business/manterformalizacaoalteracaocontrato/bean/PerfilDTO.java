/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

/**
 * Nome: PerfilDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PerfilDTO {
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
    
    /** Atributo dsLayoutProprio. */
    private String dsLayoutProprio;
    
    /** Atributo dsCodigoTipoLayout. */
    private String dsCodigoTipoLayout;
    
    /** Atributo dsProdutoServicoRelacionadoPerfil. */
    private String dsProdutoServicoRelacionadoPerfil;
    
    /** Atributo dsAplicacaoFormato. */
    private String dsAplicacaoFormato;
    
    /** Atributo dsMeioPrincipalRemessa. */
    private String dsMeioPrincipalRemessa;
    
    /** Atributo dsMeioAlternadoRemessa. */
    private String  dsMeioAlternadoRemessa;
    
    /** Atributo dsMeioPrincipalRetorno. */
    private String dsMeioPrincipalRetorno;
    
    /** Atributo dsMeioAlternadoRetorno. */
    private String dsMeioAlternadoRetorno;
    
    /** Atributo dsEmpresa. */
    private String dsEmpresa;
    
    /** Atributo dsAcaoManutencaoPerfil. */
    private String dsAcaoManutencaoPerfil;
	
	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo() {
		return cdPerfilTrocaArquivo;
	}
	
	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}
	
	/**
	 * Get: dsLayoutProprio.
	 *
	 * @return dsLayoutProprio
	 */
	public String getDsLayoutProprio() {
		return dsLayoutProprio;
	}
	
	/**
	 * Set: dsLayoutProprio.
	 *
	 * @param dsLayoutProprio the ds layout proprio
	 */
	public void setDsLayoutProprio(String dsLayoutProprio) {
		this.dsLayoutProprio = dsLayoutProprio;
	}
	
	/**
	 * Get: dsCodigoTipoLayout.
	 *
	 * @return dsCodigoTipoLayout
	 */
	public String getDsCodigoTipoLayout() {
		return dsCodigoTipoLayout;
	}
	
	/**
	 * Set: dsCodigoTipoLayout.
	 *
	 * @param dsCodigoTipoLayout the ds codigo tipo layout
	 */
	public void setDsCodigoTipoLayout(String dsCodigoTipoLayout) {
		this.dsCodigoTipoLayout = dsCodigoTipoLayout;
	}
	
	/**
	 * Get: dsProdutoServicoRelacionadoPerfil.
	 *
	 * @return dsProdutoServicoRelacionadoPerfil
	 */
	public String getDsProdutoServicoRelacionadoPerfil() {
		return dsProdutoServicoRelacionadoPerfil;
	}
	
	/**
	 * Set: dsProdutoServicoRelacionadoPerfil.
	 *
	 * @param dsProdutoServicoRelacionadoPerfil the ds produto servico relacionado perfil
	 */
	public void setDsProdutoServicoRelacionadoPerfil(
			String dsProdutoServicoRelacionadoPerfil) {
		this.dsProdutoServicoRelacionadoPerfil = dsProdutoServicoRelacionadoPerfil;
	}
	
	/**
	 * Get: dsAplicacaoFormato.
	 *
	 * @return dsAplicacaoFormato
	 */
	public String getDsAplicacaoFormato() {
		return dsAplicacaoFormato;
	}
	
	/**
	 * Set: dsAplicacaoFormato.
	 *
	 * @param dsAplicacaoFormato the ds aplicacao formato
	 */
	public void setDsAplicacaoFormato(String dsAplicacaoFormato) {
		this.dsAplicacaoFormato = dsAplicacaoFormato;
	}
	
	/**
	 * Get: dsMeioPrincipalRemessa.
	 *
	 * @return dsMeioPrincipalRemessa
	 */
	public String getDsMeioPrincipalRemessa() {
		return dsMeioPrincipalRemessa;
	}
	
	/**
	 * Set: dsMeioPrincipalRemessa.
	 *
	 * @param dsMeioPrincipalRemessa the ds meio principal remessa
	 */
	public void setDsMeioPrincipalRemessa(String dsMeioPrincipalRemessa) {
		this.dsMeioPrincipalRemessa = dsMeioPrincipalRemessa;
	}
	
	/**
	 * Get: dsMeioAlternadoRemessa.
	 *
	 * @return dsMeioAlternadoRemessa
	 */
	public String getDsMeioAlternadoRemessa() {
		return dsMeioAlternadoRemessa;
	}
	
	/**
	 * Set: dsMeioAlternadoRemessa.
	 *
	 * @param dsMeioAlternadoRemessa the ds meio alternado remessa
	 */
	public void setDsMeioAlternadoRemessa(String dsMeioAlternadoRemessa) {
		this.dsMeioAlternadoRemessa = dsMeioAlternadoRemessa;
	}
	
	/**
	 * Get: dsMeioPrincipalRetorno.
	 *
	 * @return dsMeioPrincipalRetorno
	 */
	public String getDsMeioPrincipalRetorno() {
		return dsMeioPrincipalRetorno;
	}
	
	/**
	 * Set: dsMeioPrincipalRetorno.
	 *
	 * @param dsMeioPrincipalRetorno the ds meio principal retorno
	 */
	public void setDsMeioPrincipalRetorno(String dsMeioPrincipalRetorno) {
		this.dsMeioPrincipalRetorno = dsMeioPrincipalRetorno;
	}
	
	/**
	 * Get: dsMeioAlternadoRetorno.
	 *
	 * @return dsMeioAlternadoRetorno
	 */
	public String getDsMeioAlternadoRetorno() {
		return dsMeioAlternadoRetorno;
	}
	
	/**
	 * Set: dsMeioAlternadoRetorno.
	 *
	 * @param dsMeioAlternadoRetorno the ds meio alternado retorno
	 */
	public void setDsMeioAlternadoRetorno(String dsMeioAlternadoRetorno) {
		this.dsMeioAlternadoRetorno = dsMeioAlternadoRetorno;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: dsAcaoManutencaoPerfil.
	 *
	 * @return dsAcaoManutencaoPerfil
	 */
	public String getDsAcaoManutencaoPerfil() {
		return dsAcaoManutencaoPerfil;
	}
	
	/**
	 * Set: dsAcaoManutencaoPerfil.
	 *
	 * @param dsAcaoManutencaoPerfil the ds acao manutencao perfil
	 */
	public void setDsAcaoManutencaoPerfil(String dsAcaoManutencaoPerfil) {
		this.dsAcaoManutencaoPerfil = dsAcaoManutencaoPerfil;
	}
	
}
