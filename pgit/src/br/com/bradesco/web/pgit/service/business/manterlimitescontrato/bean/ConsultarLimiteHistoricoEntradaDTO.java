package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;

import java.sql.Date;


/**
 * Arquivo criado em 22/09/15.
 */
public class ConsultarLimiteHistoricoEntradaDTO {

    private Integer maxOcorrencias;

    private Long cdpessoaJuridicaContrato;

    private Integer cdTipoContratoNegocio;

    private Long nrSequenciaContratoNegocio;

    private Long cdPessoaJuridicaVinculo;

    private Integer cdTipoContratoVinculo;

    private Long nrSequenciaContratoVinculo;

    private Integer cdTipoVincContrato;
    
    private String dtInicioManutencao;
    
    private String dtFimManutencao;

    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
        this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
    }

    public Long getCdPessoaJuridicaVinculo() {
        return this.cdPessoaJuridicaVinculo;
    }

    public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
        this.cdTipoContratoVinculo = cdTipoContratoVinculo;
    }

    public Integer getCdTipoContratoVinculo() {
        return this.cdTipoContratoVinculo;
    }

    public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
        this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
    }

    public Long getNrSequenciaContratoVinculo() {
        return this.nrSequenciaContratoVinculo;
    }

    public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
        this.cdTipoVincContrato = cdTipoVincContrato;
    }

    public Integer getCdTipoVincContrato() {
        return this.cdTipoVincContrato;
    }

	/**
	 * @return the dtInicioManutencao
	 */
	public String getDtInicioManutencao() {
		return dtInicioManutencao;
	}

	/**
	 * @param dtInicioManutencao the dtInicioManutencao to set
	 */
	public void setDtInicioManutencao(String dtInicioManutencao) {
		this.dtInicioManutencao = dtInicioManutencao;
	}

	/**
	 * @return the dtFimManutencao
	 */
	public String getDtFimManutencao() {
		return dtFimManutencao;
	}

	/**
	 * @param dtFimManutencao the dtFimManutencao to set
	 */
	public void setDtFimManutencao(String dtFimManutencao) {
		this.dtFimManutencao = dtFimManutencao;
	}

}