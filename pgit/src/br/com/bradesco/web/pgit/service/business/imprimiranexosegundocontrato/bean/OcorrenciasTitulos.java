/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * Nome: OcorrenciasTitulos
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasTitulos {

	/** Atributo dsFtiTipoTitulo. */
	private String dsFtiTipoTitulo;
	
	/** Atributo dsFtiTipoConsultaSaldo. */
	private String dsFtiTipoConsultaSaldo;
	
	/** Atributo dsFtiTipoProcessamento. */
	private String dsFtiTipoProcessamento;
	
	/** Atributo dsFtiTipoTratamentoFeriado. */
	private String dsFtiTipoTratamentoFeriado;
	
	/** Atributo dsFtiTipoRejeicaoEfetivacao. */
	private String dsFtiTipoRejeicaoEfetivacao;
	
	/** Atributo dsFtiTipoRejeicaoAgendamento. */
	private String dsFtiTipoRejeicaoAgendamento;
	
	/** Atributo cdFtiIndicadorPercentualMaximo. */
	private String cdFtiIndicadorPercentualMaximo;
	
	/** Atributo pcFtiMaximoInconsistente. */
	private Integer pcFtiMaximoInconsistente;
	
	/** Atributo cdFtiIndicadorQuantidadeMaxima. */
	private String cdFtiIndicadorQuantidadeMaxima;
	
	/** Atributo qtFtiMaximaInconsistencia. */
	private Integer qtFtiMaximaInconsistencia;
	
	/** Atributo cdFtiIndicadorValorMaximo. */
	private String cdFtiIndicadorValorMaximo;
	
	/** Atributo vlFtiMaximoFavorecidoNao. */
	private BigDecimal vlFtiMaximoFavorecidoNao;
	
	/** Atributo dsFtiPrioridadeDebito. */
	private String dsFtiPrioridadeDebito;
	
	/** Atributo cdFtiIndicadorValorDia. */
	private String cdFtiIndicadorValorDia;
	
	/** Atributo vlFtiLimiteDiario. */
	private BigDecimal vlFtiLimiteDiario;
	
	/** Atributo cdFtiIndicadorValorIndividual. */
	private String cdFtiIndicadorValorIndividual;
	
	/** Atributo vlFtiLimiteIndividual. */
	private BigDecimal vlFtiLimiteIndividual;
	
	/** Atributo cdFtiIndicadorQuantidadeFloating. */
	private String cdFtiIndicadorQuantidadeFloating;
	
	/** Atributo qtFtiDiaFloating. */
	private Integer qtFtiDiaFloating;
	
	/** Atributo cdFtiIndicadorCadastroFavorecido. */
	private String cdFtiIndicadorCadastroFavorecido;
	
	/** Atributo cdFtiUtilizadoFavorecido. */
	private String cdFtiUtilizadoFavorecido;
	
	/** Atributo cdFtiIndicadorQuantidadeRepique. */
	private String cdFtiIndicadorQuantidadeRepique;
	
	/** Atributo qtFtiDiaRepique. */
	private Integer qtFtiDiaRepique;
	
	/** Atributo dsTIpoConsultaSaldoSuperior. */
	private String dsTIpoConsultaSaldoSuperior;
	
	/** The fti pagamento vencido. */
	private String ftiPagamentoVencido; 
	
	/** The fti pagamento menor. */
	private String ftiPagamentoMenor;   
	
	/** The fti ds tipo cons favorecido. */
	private String ftiDsTipoConsFavorecido; 
	
	/** The fti cferi loc. */
	private String ftiCferiLoc;   
	
	/** The fti quantidade max vencido. */
	private Integer ftiQuantidadeMaxVencido; 

	/**
	 * Get: dsFtiTipoTitulo.
	 *
	 * @return dsFtiTipoTitulo
	 */
	public String getDsFtiTipoTitulo() {
		return dsFtiTipoTitulo;
	}

	/**
	 * Set: dsFtiTipoTitulo.
	 *
	 * @param dsFtiTipoTitulo the ds fti tipo titulo
	 */
	public void setDsFtiTipoTitulo(String dsFtiTipoTitulo) {
		this.dsFtiTipoTitulo = dsFtiTipoTitulo;
	}

	/**
	 * Get: dsFtiTipoConsultaSaldo.
	 *
	 * @return dsFtiTipoConsultaSaldo
	 */
	public String getDsFtiTipoConsultaSaldo() {
		return dsFtiTipoConsultaSaldo;
	}

	/**
	 * Set: dsFtiTipoConsultaSaldo.
	 *
	 * @param dsFtiTipoConsultaSaldo the ds fti tipo consulta saldo
	 */
	public void setDsFtiTipoConsultaSaldo(String dsFtiTipoConsultaSaldo) {
		this.dsFtiTipoConsultaSaldo = dsFtiTipoConsultaSaldo;
	}

	/**
	 * Get: dsFtiTipoProcessamento.
	 *
	 * @return dsFtiTipoProcessamento
	 */
	public String getDsFtiTipoProcessamento() {
		return dsFtiTipoProcessamento;
	}

	/**
	 * Set: dsFtiTipoProcessamento.
	 *
	 * @param dsFtiTipoProcessamento the ds fti tipo processamento
	 */
	public void setDsFtiTipoProcessamento(String dsFtiTipoProcessamento) {
		this.dsFtiTipoProcessamento = dsFtiTipoProcessamento;
	}

	/**
	 * Get: dsFtiTipoTratamentoFeriado.
	 *
	 * @return dsFtiTipoTratamentoFeriado
	 */
	public String getDsFtiTipoTratamentoFeriado() {
		return dsFtiTipoTratamentoFeriado;
	}

	/**
	 * Set: dsFtiTipoTratamentoFeriado.
	 *
	 * @param dsFtiTipoTratamentoFeriado the ds fti tipo tratamento feriado
	 */
	public void setDsFtiTipoTratamentoFeriado(String dsFtiTipoTratamentoFeriado) {
		this.dsFtiTipoTratamentoFeriado = dsFtiTipoTratamentoFeriado;
	}

	/**
	 * Get: dsFtiTipoRejeicaoEfetivacao.
	 *
	 * @return dsFtiTipoRejeicaoEfetivacao
	 */
	public String getDsFtiTipoRejeicaoEfetivacao() {
		return dsFtiTipoRejeicaoEfetivacao;
	}

	/**
	 * Set: dsFtiTipoRejeicaoEfetivacao.
	 *
	 * @param dsFtiTipoRejeicaoEfetivacao the ds fti tipo rejeicao efetivacao
	 */
	public void setDsFtiTipoRejeicaoEfetivacao(
			String dsFtiTipoRejeicaoEfetivacao) {
		this.dsFtiTipoRejeicaoEfetivacao = dsFtiTipoRejeicaoEfetivacao;
	}

	/**
	 * Get: dsFtiTipoRejeicaoAgendamento.
	 *
	 * @return dsFtiTipoRejeicaoAgendamento
	 */
	public String getDsFtiTipoRejeicaoAgendamento() {
		return dsFtiTipoRejeicaoAgendamento;
	}

	/**
	 * Set: dsFtiTipoRejeicaoAgendamento.
	 *
	 * @param dsFtiTipoRejeicaoAgendamento the ds fti tipo rejeicao agendamento
	 */
	public void setDsFtiTipoRejeicaoAgendamento(
			String dsFtiTipoRejeicaoAgendamento) {
		this.dsFtiTipoRejeicaoAgendamento = dsFtiTipoRejeicaoAgendamento;
	}

	/**
	 * Get: cdFtiIndicadorPercentualMaximo.
	 *
	 * @return cdFtiIndicadorPercentualMaximo
	 */
	public String getCdFtiIndicadorPercentualMaximo() {
		return cdFtiIndicadorPercentualMaximo;
	}

	/**
	 * Set: cdFtiIndicadorPercentualMaximo.
	 *
	 * @param cdFtiIndicadorPercentualMaximo the cd fti indicador percentual maximo
	 */
	public void setCdFtiIndicadorPercentualMaximo(
			String cdFtiIndicadorPercentualMaximo) {
		this.cdFtiIndicadorPercentualMaximo = cdFtiIndicadorPercentualMaximo;
	}

	/**
	 * Get: pcFtiMaximoInconsistente.
	 *
	 * @return pcFtiMaximoInconsistente
	 */
	public Integer getPcFtiMaximoInconsistente() {
		return pcFtiMaximoInconsistente;
	}

	/**
	 * Set: pcFtiMaximoInconsistente.
	 *
	 * @param pcFtiMaximoInconsistente the pc fti maximo inconsistente
	 */
	public void setPcFtiMaximoInconsistente(Integer pcFtiMaximoInconsistente) {
		this.pcFtiMaximoInconsistente = pcFtiMaximoInconsistente;
	}

	/**
	 * Get: cdFtiIndicadorQuantidadeMaxima.
	 *
	 * @return cdFtiIndicadorQuantidadeMaxima
	 */
	public String getCdFtiIndicadorQuantidadeMaxima() {
		return cdFtiIndicadorQuantidadeMaxima;
	}

	/**
	 * Set: cdFtiIndicadorQuantidadeMaxima.
	 *
	 * @param cdFtiIndicadorQuantidadeMaxima the cd fti indicador quantidade maxima
	 */
	public void setCdFtiIndicadorQuantidadeMaxima(
			String cdFtiIndicadorQuantidadeMaxima) {
		this.cdFtiIndicadorQuantidadeMaxima = cdFtiIndicadorQuantidadeMaxima;
	}

	/**
	 * Get: qtFtiMaximaInconsistencia.
	 *
	 * @return qtFtiMaximaInconsistencia
	 */
	public Integer getQtFtiMaximaInconsistencia() {
		return qtFtiMaximaInconsistencia;
	}

	/**
	 * Set: qtFtiMaximaInconsistencia.
	 *
	 * @param qtFtiMaximaInconsistencia the qt fti maxima inconsistencia
	 */
	public void setQtFtiMaximaInconsistencia(Integer qtFtiMaximaInconsistencia) {
		this.qtFtiMaximaInconsistencia = qtFtiMaximaInconsistencia;
	}

	/**
	 * Get: cdFtiIndicadorValorMaximo.
	 *
	 * @return cdFtiIndicadorValorMaximo
	 */
	public String getCdFtiIndicadorValorMaximo() {
		return cdFtiIndicadorValorMaximo;
	}

	/**
	 * Set: cdFtiIndicadorValorMaximo.
	 *
	 * @param cdFtiIndicadorValorMaximo the cd fti indicador valor maximo
	 */
	public void setCdFtiIndicadorValorMaximo(String cdFtiIndicadorValorMaximo) {
		this.cdFtiIndicadorValorMaximo = cdFtiIndicadorValorMaximo;
	}

	/**
	 * Get: vlFtiMaximoFavorecidoNao.
	 *
	 * @return vlFtiMaximoFavorecidoNao
	 */
	public BigDecimal getVlFtiMaximoFavorecidoNao() {
		return vlFtiMaximoFavorecidoNao;
	}

	/**
	 * Set: vlFtiMaximoFavorecidoNao.
	 *
	 * @param vlFtiMaximoFavorecidoNao the vl fti maximo favorecido nao
	 */
	public void setVlFtiMaximoFavorecidoNao(BigDecimal vlFtiMaximoFavorecidoNao) {
		this.vlFtiMaximoFavorecidoNao = vlFtiMaximoFavorecidoNao;
	}

	/**
	 * Get: dsFtiPrioridadeDebito.
	 *
	 * @return dsFtiPrioridadeDebito
	 */
	public String getDsFtiPrioridadeDebito() {
		return dsFtiPrioridadeDebito;
	}

	/**
	 * Set: dsFtiPrioridadeDebito.
	 *
	 * @param dsFtiPrioridadeDebito the ds fti prioridade debito
	 */
	public void setDsFtiPrioridadeDebito(String dsFtiPrioridadeDebito) {
		this.dsFtiPrioridadeDebito = dsFtiPrioridadeDebito;
	}

	/**
	 * Get: cdFtiIndicadorValorDia.
	 *
	 * @return cdFtiIndicadorValorDia
	 */
	public String getCdFtiIndicadorValorDia() {
		return cdFtiIndicadorValorDia;
	}

	/**
	 * Set: cdFtiIndicadorValorDia.
	 *
	 * @param cdFtiIndicadorValorDia the cd fti indicador valor dia
	 */
	public void setCdFtiIndicadorValorDia(String cdFtiIndicadorValorDia) {
		this.cdFtiIndicadorValorDia = cdFtiIndicadorValorDia;
	}

	/**
	 * Get: vlFtiLimiteDiario.
	 *
	 * @return vlFtiLimiteDiario
	 */
	public BigDecimal getVlFtiLimiteDiario() {
		return vlFtiLimiteDiario;
	}

	/**
	 * Set: vlFtiLimiteDiario.
	 *
	 * @param vlFtiLimiteDiario the vl fti limite diario
	 */
	public void setVlFtiLimiteDiario(BigDecimal vlFtiLimiteDiario) {
		this.vlFtiLimiteDiario = vlFtiLimiteDiario;
	}

	/**
	 * Get: cdFtiIndicadorValorIndividual.
	 *
	 * @return cdFtiIndicadorValorIndividual
	 */
	public String getCdFtiIndicadorValorIndividual() {
		return cdFtiIndicadorValorIndividual;
	}

	/**
	 * Set: cdFtiIndicadorValorIndividual.
	 *
	 * @param cdFtiIndicadorValorIndividual the cd fti indicador valor individual
	 */
	public void setCdFtiIndicadorValorIndividual(
			String cdFtiIndicadorValorIndividual) {
		this.cdFtiIndicadorValorIndividual = cdFtiIndicadorValorIndividual;
	}

	/**
	 * Get: vlFtiLimiteIndividual.
	 *
	 * @return vlFtiLimiteIndividual
	 */
	public BigDecimal getVlFtiLimiteIndividual() {
		return vlFtiLimiteIndividual;
	}

	/**
	 * Set: vlFtiLimiteIndividual.
	 *
	 * @param vlFtiLimiteIndividual the vl fti limite individual
	 */
	public void setVlFtiLimiteIndividual(BigDecimal vlFtiLimiteIndividual) {
		this.vlFtiLimiteIndividual = vlFtiLimiteIndividual;
	}

	/**
	 * Get: cdFtiIndicadorQuantidadeFloating.
	 *
	 * @return cdFtiIndicadorQuantidadeFloating
	 */
	public String getCdFtiIndicadorQuantidadeFloating() {
		return cdFtiIndicadorQuantidadeFloating;
	}

	/**
	 * Set: cdFtiIndicadorQuantidadeFloating.
	 *
	 * @param cdFtiIndicadorQuantidadeFloating the cd fti indicador quantidade floating
	 */
	public void setCdFtiIndicadorQuantidadeFloating(
			String cdFtiIndicadorQuantidadeFloating) {
		this.cdFtiIndicadorQuantidadeFloating = cdFtiIndicadorQuantidadeFloating;
	}

	/**
	 * Get: qtFtiDiaFloating.
	 *
	 * @return qtFtiDiaFloating
	 */
	public Integer getQtFtiDiaFloating() {
		return qtFtiDiaFloating;
	}

	/**
	 * Set: qtFtiDiaFloating.
	 *
	 * @param qtFtiDiaFloating the qt fti dia floating
	 */
	public void setQtFtiDiaFloating(Integer qtFtiDiaFloating) {
		this.qtFtiDiaFloating = qtFtiDiaFloating;
	}

	/**
	 * Get: cdFtiIndicadorCadastroFavorecido.
	 *
	 * @return cdFtiIndicadorCadastroFavorecido
	 */
	public String getCdFtiIndicadorCadastroFavorecido() {
		return cdFtiIndicadorCadastroFavorecido;
	}

	/**
	 * Set: cdFtiIndicadorCadastroFavorecido.
	 *
	 * @param cdFtiIndicadorCadastroFavorecido the cd fti indicador cadastro favorecido
	 */
	public void setCdFtiIndicadorCadastroFavorecido(
			String cdFtiIndicadorCadastroFavorecido) {
		this.cdFtiIndicadorCadastroFavorecido = cdFtiIndicadorCadastroFavorecido;
	}

	/**
	 * Get: cdFtiUtilizadoFavorecido.
	 *
	 * @return cdFtiUtilizadoFavorecido
	 */
	public String getCdFtiUtilizadoFavorecido() {
		return cdFtiUtilizadoFavorecido;
	}

	/**
	 * Set: cdFtiUtilizadoFavorecido.
	 *
	 * @param cdFtiUtilizadoFavorecido the cd fti utilizado favorecido
	 */
	public void setCdFtiUtilizadoFavorecido(String cdFtiUtilizadoFavorecido) {
		this.cdFtiUtilizadoFavorecido = cdFtiUtilizadoFavorecido;
	}

	/**
	 * Get: cdFtiIndicadorQuantidadeRepique.
	 *
	 * @return cdFtiIndicadorQuantidadeRepique
	 */
	public String getCdFtiIndicadorQuantidadeRepique() {
		return cdFtiIndicadorQuantidadeRepique;
	}

	/**
	 * Set: cdFtiIndicadorQuantidadeRepique.
	 *
	 * @param cdFtiIndicadorQuantidadeRepique the cd fti indicador quantidade repique
	 */
	public void setCdFtiIndicadorQuantidadeRepique(
			String cdFtiIndicadorQuantidadeRepique) {
		this.cdFtiIndicadorQuantidadeRepique = cdFtiIndicadorQuantidadeRepique;
	}

	/**
	 * Get: qtFtiDiaRepique.
	 *
	 * @return qtFtiDiaRepique
	 */
	public Integer getQtFtiDiaRepique() {
		return qtFtiDiaRepique;
	}

	/**
	 * Set: qtFtiDiaRepique.
	 *
	 * @param qtFtiDiaRepique the qt fti dia repique
	 */
	public void setQtFtiDiaRepique(Integer qtFtiDiaRepique) {
		this.qtFtiDiaRepique = qtFtiDiaRepique;
	}

	/**
	 * Get: dsTIpoConsultaSaldoSuperior.
	 *
	 * @return dsTIpoConsultaSaldoSuperior
	 */
	public String getDsTIpoConsultaSaldoSuperior() {
		return dsTIpoConsultaSaldoSuperior;
	}

	/**
	 * Set: dsTIpoConsultaSaldoSuperior.
	 *
	 * @param dsTIpoConsultaSaldoSuperior the ds t ipo consulta saldo superior
	 */
	public void setDsTIpoConsultaSaldoSuperior(
			String dsTIpoConsultaSaldoSuperior) {
		this.dsTIpoConsultaSaldoSuperior = dsTIpoConsultaSaldoSuperior;
	}

	/**
	 * Gets the fti pagamento vencido.
	 *
	 * @return the ftiPagamentoVencido
	 */
	public String getFtiPagamentoVencido() {
		return ftiPagamentoVencido;
	}

	/**
	 * Sets the fti pagamento vencido.
	 *
	 * @param ftiPagamentoVencido the ftiPagamentoVencido to set
	 */
	public void setFtiPagamentoVencido(String ftiPagamentoVencido) {
		this.ftiPagamentoVencido = ftiPagamentoVencido;
	}

	/**
	 * Gets the fti pagamento menor.
	 *
	 * @return the ftiPagamentoMenor
	 */
	public String getFtiPagamentoMenor() {
		return ftiPagamentoMenor;
	}

	/**
	 * Sets the fti pagamento menor.
	 *
	 * @param ftiPagamentoMenor the ftiPagamentoMenor to set
	 */
	public void setFtiPagamentoMenor(String ftiPagamentoMenor) {
		this.ftiPagamentoMenor = ftiPagamentoMenor;
	}

	/**
	 * Gets the fti ds tipo cons favorecido.
	 *
	 * @return the ftiDsTipoConsFavorecido
	 */
	public String getFtiDsTipoConsFavorecido() {
		return ftiDsTipoConsFavorecido;
	}

	/**
	 * Sets the fti ds tipo cons favorecido.
	 *
	 * @param ftiDsTipoConsFavorecido the ftiDsTipoConsFavorecido to set
	 */
	public void setFtiDsTipoConsFavorecido(String ftiDsTipoConsFavorecido) {
		this.ftiDsTipoConsFavorecido = ftiDsTipoConsFavorecido;
	}

	/**
	 * Gets the fti cferi loc.
	 *
	 * @return the ftiCferiLoc
	 */
	public String getFtiCferiLoc() {
		return ftiCferiLoc;
	}

	/**
	 * Sets the fti cferi loc.
	 *
	 * @param ftiCferiLoc the ftiCferiLoc to set
	 */
	public void setFtiCferiLoc(String ftiCferiLoc) {
		this.ftiCferiLoc = ftiCferiLoc;
	}

	/**
	 * Gets the fti quantidade max vencido.
	 *
	 * @return the ftiQuantidadeMaxVencido
	 */
	public Integer getFtiQuantidadeMaxVencido() {
		return ftiQuantidadeMaxVencido;
	}

	/**
	 * Sets the fti quantidade max vencido.
	 *
	 * @param ftiQuantidadeMaxVencido the ftiQuantidadeMaxVencido to set
	 */
	public void setFtiQuantidadeMaxVencido(Integer ftiQuantidadeMaxVencido) {
		this.ftiQuantidadeMaxVencido = ftiQuantidadeMaxVencido;
	}
}
