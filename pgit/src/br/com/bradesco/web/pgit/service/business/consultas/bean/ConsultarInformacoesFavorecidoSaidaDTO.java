/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

/**
 * Nome: ConsultarInformacoesFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarInformacoesFavorecidoSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nmFavorecido. */
    private String nmFavorecido;
    
    /** Atributo cdFavorecido. */
    private Long cdFavorecido;
    
    /** Atributo inscricaoFavorecido. */
    private String inscricaoFavorecido;
    
    /** Atributo dsTipoFavorecido. */
    private String dsTipoFavorecido;
    
    
	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}
	
	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoFavorecido.
	 *
	 * @return dsTipoFavorecido
	 */
	public String getDsTipoFavorecido() {
		return dsTipoFavorecido;
	}
	
	/**
	 * Set: dsTipoFavorecido.
	 *
	 * @param dsTipoFavorecido the ds tipo favorecido
	 */
	public void setDsTipoFavorecido(String dsTipoFavorecido) {
		this.dsTipoFavorecido = dsTipoFavorecido;
	}
	
	/**
	 * Get: inscricaoFavorecido.
	 *
	 * @return inscricaoFavorecido
	 */
	public String getInscricaoFavorecido() {
		return inscricaoFavorecido;
	}
	
	/**
	 * Set: inscricaoFavorecido.
	 *
	 * @param inscricaoFavorecido the inscricao favorecido
	 */
	public void setInscricaoFavorecido(String inscricaoFavorecido) {
		this.inscricaoFavorecido = inscricaoFavorecido;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nmFavorecido.
	 *
	 * @return nmFavorecido
	 */
	public String getNmFavorecido() {
		return nmFavorecido;
	}
	
	/**
	 * Set: nmFavorecido.
	 *
	 * @param nmFavorecido the nm favorecido
	 */
	public void setNmFavorecido(String nmFavorecido) {
		this.nmFavorecido = nmFavorecido;
	}
    
    
}
