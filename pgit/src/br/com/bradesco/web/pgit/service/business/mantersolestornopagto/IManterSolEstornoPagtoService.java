/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantersolestornopagto;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarDetalhesSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarDetalhesSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.AutorizarSolicitacaoEstornoPatosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.AutorizarSolicitacaoEstornoPatosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarPagtosSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarPagtosSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarSolicitacaoEstornoPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ConsultarSolicitacaoEstornoPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ExcluirSolicitacaoEstornoPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolestornopagto.bean.ExcluirSolicitacaoEstornoPagtosSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterSolEstornoPagto
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterSolEstornoPagtoService {
	
	/**
	 * Excluir solicitacao estorno pagtos.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir solicitacao estorno pagtos saida dto
	 */
	ExcluirSolicitacaoEstornoPagtosSaidaDTO excluirSolicitacaoEstornoPagtos (ExcluirSolicitacaoEstornoPagtosEntradaDTO entradaDTO);
	
	/**
	 * Consultar solicitacao estorno pagtos.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar solicitacao estorno pagtos saida dt o>
	 */
	List<ConsultarSolicitacaoEstornoPagtosSaidaDTO> consultarSolicitacaoEstornoPagtos (ConsultarSolicitacaoEstornoPagtosEntradaDTO entradaDTO);
	
	/**
	 * Consultar pagtos solicitacao estorno.
	 *
	 * @param entrada the entrada
	 * @return the consultar pagtos solicitacao estorno saida dto
	 */
	ConsultarPagtosSolicitacaoEstornoSaidaDTO consultarPagtosSolicitacaoEstorno(ConsultarPagtosSolicitacaoEstornoEntradaDTO entrada);
	
	/**
	 * Consultar detalhes solicitacao.
	 *
	 * @param entrada the entrada
	 * @return the consultar detalhes solicitacao saida dto
	 */
	ConsultarDetalhesSolicitacaoSaidaDTO consultarDetalhesSolicitacao(ConsultarDetalhesSolicitacaoEntradaDTO entrada);

	/**
	 * Autorizar solicitacao estorno patos.
	 *
	 * @param entrada the entrada
	 * @return the autorizar solicitacao estorno patos saida dto
	 */
	AutorizarSolicitacaoEstornoPatosSaidaDTO autorizarSolicitacaoEstornoPatos (AutorizarSolicitacaoEstornoPatosEntradaDTO entrada);
}


