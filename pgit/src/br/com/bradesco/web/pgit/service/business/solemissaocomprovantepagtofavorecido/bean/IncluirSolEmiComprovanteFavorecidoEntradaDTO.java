/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Nome: IncluirSolEmiComprovanteFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSolEmiComprovanteFavorecidoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
    
    /** Atributo dtInicioPeriodoMovimentacao. */
    private String dtInicioPeriodoMovimentacao;
    
    /** Atributo dtFimPeriodoMovimentacao. */
    private String dtFimPeriodoMovimentacao;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo cdRecebedorCredito. */
    private Long cdRecebedorCredito;
    
    /** Atributo cdTipoInscricaoRecebedor. */
    private Integer cdTipoInscricaoRecebedor;
    
    /** Atributo cdCpfCnpjRecebedor. */
    private Long cdCpfCnpjRecebedor;
    
    /** Atributo cdFilialCnpjRecebedor. */
    private Integer cdFilialCnpjRecebedor;
    
    /** Atributo cdControleCpfRecebedor. */
    private Integer cdControleCpfRecebedor;
    
    /** Atributo cdDestinoCorrespSolicitacao. */
    private Integer cdDestinoCorrespSolicitacao;
    
    /** Atributo cdTipoPostagemSolicitacao. */
    private String cdTipoPostagemSolicitacao;
    
    /** Atributo dsLogradouroPagador. */
    private String dsLogradouroPagador;
    
    /** Atributo dsNumeroLogradouroPagador. */
    private String dsNumeroLogradouroPagador;
    
    /** Atributo dsComplementoLogradouroPagador. */
    private String dsComplementoLogradouroPagador;
    
    /** Atributo dsBairroClientePagador. */
    private String dsBairroClientePagador;
    
    /** Atributo dsMunicipioClientePagador. */
    private String dsMunicipioClientePagador;
    
    /** Atributo cdSiglaUfPagador. */
    private String cdSiglaUfPagador;
    
    /** Atributo cdCepPagador. */
    private Integer cdCepPagador;
    
    /** Atributo cdCepComplementoPagador. */
    private Integer cdCepComplementoPagador;
    
    /** Atributo dsEmailClientePagador. */
    private String dsEmailClientePagador;
    
    /** Atributo cdPessoaJuridicaDepto. */
    private Long cdPessoaJuridicaDepto;
    
    /** Atributo nrSequenciaUnidadeDepto. */
    private Integer nrSequenciaUnidadeDepto;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdPercentualBonifTarifaSolicitacao. */
    private BigDecimal cdPercentualBonifTarifaSolicitacao;
    
    /** Atributo vlTarifaNegocioSolicitacao. */
    private BigDecimal vlTarifaNegocioSolicitacao;
    
    /** Atributo nrOcorrencias. */
    private Integer nrOcorrencias;
    
    /** Atributo comprovantes. */
    private List<IncluirSolEmiComprovanteFavorecidoOcorrenciasDTO> comprovantes = new ArrayList<IncluirSolEmiComprovanteFavorecidoOcorrenciasDTO>();
    
    /** Atributo cdDepartamentoUnidade. */
    private Integer cdDepartamentoUnidade;
	
    /**
     * Get: cdDepartamentoUnidade.
     *
     * @return cdDepartamentoUnidade
     */
    public Integer getCdDepartamentoUnidade() {
		return cdDepartamentoUnidade;
	}
	
	/**
	 * Set: cdDepartamentoUnidade.
	 *
	 * @param cdDepartamentoUnidade the cd departamento unidade
	 */
	public void setCdDepartamentoUnidade(Integer cdDepartamentoUnidade) {
		this.cdDepartamentoUnidade = cdDepartamentoUnidade;
	}
	
	/**
	 * Get: cdCepComplementoPagador.
	 *
	 * @return cdCepComplementoPagador
	 */
	public Integer getCdCepComplementoPagador() {
		return cdCepComplementoPagador;
	}
	
	/**
	 * Set: cdCepComplementoPagador.
	 *
	 * @param cdCepComplementoPagador the cd cep complemento pagador
	 */
	public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
		this.cdCepComplementoPagador = cdCepComplementoPagador;
	}
	
	/**
	 * Get: cdCepPagador.
	 *
	 * @return cdCepPagador
	 */
	public Integer getCdCepPagador() {
		return cdCepPagador;
	}
	
	/**
	 * Set: cdCepPagador.
	 *
	 * @param cdCepPagador the cd cep pagador
	 */
	public void setCdCepPagador(Integer cdCepPagador) {
		this.cdCepPagador = cdCepPagador;
	}
	
	/**
	 * Get: cdControleCpfRecebedor.
	 *
	 * @return cdControleCpfRecebedor
	 */
	public Integer getCdControleCpfRecebedor() {
		return cdControleCpfRecebedor;
	}
	
	/**
	 * Set: cdControleCpfRecebedor.
	 *
	 * @param cdControleCpfRecebedor the cd controle cpf recebedor
	 */
	public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
		this.cdControleCpfRecebedor = cdControleCpfRecebedor;
	}
	
	/**
	 * Get: cdCpfCnpjRecebedor.
	 *
	 * @return cdCpfCnpjRecebedor
	 */
	public Long getCdCpfCnpjRecebedor() {
		return cdCpfCnpjRecebedor;
	}
	
	/**
	 * Set: cdCpfCnpjRecebedor.
	 *
	 * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
	 */
	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}
	
	/**
	 * Get: cdDestinoCorrespSolicitacao.
	 *
	 * @return cdDestinoCorrespSolicitacao
	 */
	public Integer getCdDestinoCorrespSolicitacao() {
		return cdDestinoCorrespSolicitacao;
	}
	
	/**
	 * Set: cdDestinoCorrespSolicitacao.
	 *
	 * @param cdDestinoCorrespSolicitacao the cd destino corresp solicitacao
	 */
	public void setCdDestinoCorrespSolicitacao(Integer cdDestinoCorrespSolicitacao) {
		this.cdDestinoCorrespSolicitacao = cdDestinoCorrespSolicitacao;
	}
	
	/**
	 * Get: cdFilialCnpjRecebedor.
	 *
	 * @return cdFilialCnpjRecebedor
	 */
	public Integer getCdFilialCnpjRecebedor() {
		return cdFilialCnpjRecebedor;
	}
	
	/**
	 * Set: cdFilialCnpjRecebedor.
	 *
	 * @param cdFilialCnpjRecebedor the cd filial cnpj recebedor
	 */
	public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
		this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
	}
	
	/**
	 * Get: cdPercentualBonifTarifaSolicitacao.
	 *
	 * @return cdPercentualBonifTarifaSolicitacao
	 */
	public BigDecimal getCdPercentualBonifTarifaSolicitacao() {
		return cdPercentualBonifTarifaSolicitacao;
	}
	
	/**
	 * Set: cdPercentualBonifTarifaSolicitacao.
	 *
	 * @param cdPercentualBonifTarifaSolicitacao the cd percentual bonif tarifa solicitacao
	 */
	public void setCdPercentualBonifTarifaSolicitacao(
			BigDecimal cdPercentualBonifTarifaSolicitacao) {
		this.cdPercentualBonifTarifaSolicitacao = cdPercentualBonifTarifaSolicitacao;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaDepto.
	 *
	 * @return cdPessoaJuridicaDepto
	 */
	public Long getCdPessoaJuridicaDepto() {
		return cdPessoaJuridicaDepto;
	}
	
	/**
	 * Set: cdPessoaJuridicaDepto.
	 *
	 * @param cdPessoaJuridicaDepto the cd pessoa juridica depto
	 */
	public void setCdPessoaJuridicaDepto(Long cdPessoaJuridicaDepto) {
		this.cdPessoaJuridicaDepto = cdPessoaJuridicaDepto;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRecebedorCredito.
	 *
	 * @return cdRecebedorCredito
	 */
	public Long getCdRecebedorCredito() {
		return cdRecebedorCredito;
	}
	
	/**
	 * Set: cdRecebedorCredito.
	 *
	 * @param cdRecebedorCredito the cd recebedor credito
	 */
	public void setCdRecebedorCredito(Long cdRecebedorCredito) {
		this.cdRecebedorCredito = cdRecebedorCredito;
	}
	
	/**
	 * Get: cdSiglaUfPagador.
	 *
	 * @return cdSiglaUfPagador
	 */
	public String getCdSiglaUfPagador() {
		return cdSiglaUfPagador;
	}
	
	/**
	 * Set: cdSiglaUfPagador.
	 *
	 * @param cdSiglaUfPagador the cd sigla uf pagador
	 */
	public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
		this.cdSiglaUfPagador = cdSiglaUfPagador;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoInscricaoRecebedor.
	 *
	 * @return cdTipoInscricaoRecebedor
	 */
	public Integer getCdTipoInscricaoRecebedor() {
		return cdTipoInscricaoRecebedor;
	}
	
	/**
	 * Set: cdTipoInscricaoRecebedor.
	 *
	 * @param cdTipoInscricaoRecebedor the cd tipo inscricao recebedor
	 */
	public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
		this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
	}
	
	/**
	 * Get: cdTipoPostagemSolicitacao.
	 *
	 * @return cdTipoPostagemSolicitacao
	 */
	public String getCdTipoPostagemSolicitacao() {
		return cdTipoPostagemSolicitacao;
	}
	
	/**
	 * Set: cdTipoPostagemSolicitacao.
	 *
	 * @param cdTipoPostagemSolicitacao the cd tipo postagem solicitacao
	 */
	public void setCdTipoPostagemSolicitacao(String cdTipoPostagemSolicitacao) {
		this.cdTipoPostagemSolicitacao = cdTipoPostagemSolicitacao;
	}
	
	/**
	 * Get: dsBairroClientePagador.
	 *
	 * @return dsBairroClientePagador
	 */
	public String getDsBairroClientePagador() {
		return dsBairroClientePagador;
	}
	
	/**
	 * Set: dsBairroClientePagador.
	 *
	 * @param dsBairroClientePagador the ds bairro cliente pagador
	 */
	public void setDsBairroClientePagador(String dsBairroClientePagador) {
		this.dsBairroClientePagador = dsBairroClientePagador;
	}
	
	/**
	 * Get: dsComplementoLogradouroPagador.
	 *
	 * @return dsComplementoLogradouroPagador
	 */
	public String getDsComplementoLogradouroPagador() {
		return dsComplementoLogradouroPagador;
	}
	
	/**
	 * Set: dsComplementoLogradouroPagador.
	 *
	 * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
	 */
	public void setDsComplementoLogradouroPagador(
			String dsComplementoLogradouroPagador) {
		this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
	}
	
	/**
	 * Get: dsEmailClientePagador.
	 *
	 * @return dsEmailClientePagador
	 */
	public String getDsEmailClientePagador() {
		return dsEmailClientePagador;
	}
	
	/**
	 * Set: dsEmailClientePagador.
	 *
	 * @param dsEmailClientePagador the ds email cliente pagador
	 */
	public void setDsEmailClientePagador(String dsEmailClientePagador) {
		this.dsEmailClientePagador = dsEmailClientePagador;
	}
	
	/**
	 * Get: dsLogradouroPagador.
	 *
	 * @return dsLogradouroPagador
	 */
	public String getDsLogradouroPagador() {
		return dsLogradouroPagador;
	}
	
	/**
	 * Set: dsLogradouroPagador.
	 *
	 * @param dsLogradouroPagador the ds logradouro pagador
	 */
	public void setDsLogradouroPagador(String dsLogradouroPagador) {
		this.dsLogradouroPagador = dsLogradouroPagador;
	}
	
	/**
	 * Get: dsMunicipioClientePagador.
	 *
	 * @return dsMunicipioClientePagador
	 */
	public String getDsMunicipioClientePagador() {
		return dsMunicipioClientePagador;
	}
	
	/**
	 * Set: dsMunicipioClientePagador.
	 *
	 * @param dsMunicipioClientePagador the ds municipio cliente pagador
	 */
	public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
		this.dsMunicipioClientePagador = dsMunicipioClientePagador;
	}
	
	/**
	 * Get: dsNumeroLogradouroPagador.
	 *
	 * @return dsNumeroLogradouroPagador
	 */
	public String getDsNumeroLogradouroPagador() {
		return dsNumeroLogradouroPagador;
	}
	
	/**
	 * Set: dsNumeroLogradouroPagador.
	 *
	 * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
	 */
	public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
		this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
	}
	
	/**
	 * Get: dtFimPeriodoMovimentacao.
	 *
	 * @return dtFimPeriodoMovimentacao
	 */
	public String getDtFimPeriodoMovimentacao() {
		return dtFimPeriodoMovimentacao;
	}
	
	/**
	 * Set: dtFimPeriodoMovimentacao.
	 *
	 * @param dtFimPeriodoMovimentacao the dt fim periodo movimentacao
	 */
	public void setDtFimPeriodoMovimentacao(String dtFimPeriodoMovimentacao) {
		this.dtFimPeriodoMovimentacao = dtFimPeriodoMovimentacao;
	}
	
	/**
	 * Get: dtInicioPeriodoMovimentacao.
	 *
	 * @return dtInicioPeriodoMovimentacao
	 */
	public String getDtInicioPeriodoMovimentacao() {
		return dtInicioPeriodoMovimentacao;
	}
	
	/**
	 * Set: dtInicioPeriodoMovimentacao.
	 *
	 * @param dtInicioPeriodoMovimentacao the dt inicio periodo movimentacao
	 */
	public void setDtInicioPeriodoMovimentacao(String dtInicioPeriodoMovimentacao) {
		this.dtInicioPeriodoMovimentacao = dtInicioPeriodoMovimentacao;
	}
	
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaUnidadeDepto.
	 *
	 * @return nrSequenciaUnidadeDepto
	 */
	public Integer getNrSequenciaUnidadeDepto() {
		return nrSequenciaUnidadeDepto;
	}
	
	/**
	 * Set: nrSequenciaUnidadeDepto.
	 *
	 * @param nrSequenciaUnidadeDepto the nr sequencia unidade depto
	 */
	public void setNrSequenciaUnidadeDepto(Integer nrSequenciaUnidadeDepto) {
		this.nrSequenciaUnidadeDepto = nrSequenciaUnidadeDepto;
	}
	
	/**
	 * Get: comprovantes.
	 *
	 * @return comprovantes
	 */
	public List<IncluirSolEmiComprovanteFavorecidoOcorrenciasDTO> getComprovantes() {
		return comprovantes;
	}
	
	/**
	 * Set: comprovantes.
	 *
	 * @param comprovantes the comprovantes
	 */
	public void setComprovantes(
			List<IncluirSolEmiComprovanteFavorecidoOcorrenciasDTO> comprovantes) {
		this.comprovantes = comprovantes;
	}
	
	/**
	 * Get: vlTarifaNegocioSolicitacao.
	 *
	 * @return vlTarifaNegocioSolicitacao
	 */
	public BigDecimal getVlTarifaNegocioSolicitacao() {
		return vlTarifaNegocioSolicitacao;
	}
	
	/**
	 * Set: vlTarifaNegocioSolicitacao.
	 *
	 * @param vlTarifaNegocioSolicitacao the vl tarifa negocio solicitacao
	 */
	public void setVlTarifaNegocioSolicitacao(BigDecimal vlTarifaNegocioSolicitacao) {
		this.vlTarifaNegocioSolicitacao = vlTarifaNegocioSolicitacao;
	}
}
