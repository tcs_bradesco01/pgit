/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.bean;

/**
 * Nome: ArquivoRecebidoOcorrenciasDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ArquivoRecebidoOcorrenciasDTO {
	//Request
	/** Atributo codigoSistemaLegado. */
	private String codigoSistemaLegado;
	
	/** Atributo cpfCnpj. */
	private long cpfCnpj;
	
	/** Atributo filialCnpj. */
	private int filialCnpj;
	
	/** Atributo controleCpfCnpj. */
	private int controleCpfCnpj;
	
	/** Atributo perfil. */
	private long perfil;
	
	/** Atributo numeroArquivoRemessa. */
	private long numeroArquivoRemessa;
	
	/** Atributo numeroLoteRemessa. */
	private long numeroLoteRemessa;
	
	/** Atributo numeroSequenciaRemessa. */
	private long numeroSequenciaRemessa;
	
	/** Atributo tipoPesquisa. */
	private String tipoPesquisa;
	
	/** Atributo recurso. */
	private int recurso;
	
	/** Atributo filler. */
	private String filler;
	
	//Response
	/** Atributo numeroEventoMensagem. */
	private int numeroEventoMensagem;
	
	/** Atributo textoMensagem. */
	private String textoMensagem;
	
	/** Atributo nivelTipoMensagem. */
	private int nivelTipoMensagem;
	
	/** Atributo conteudoEnviado. */
	private String conteudoEnviado;

		
	/**
	 * Get: codigoSistemaLegado.
	 *
	 * @return codigoSistemaLegado
	 */
	public String getCodigoSistemaLegado() {
		return codigoSistemaLegado;
	}
	
	/**
	 * Set: codigoSistemaLegado.
	 *
	 * @param codigoSistemaLegado the codigo sistema legado
	 */
	public void setCodigoSistemaLegado(String codigoSistemaLegado) {
		this.codigoSistemaLegado = codigoSistemaLegado;
	}
	
	/**
	 * Get: controleCpfCnpj.
	 *
	 * @return controleCpfCnpj
	 */
	public int getControleCpfCnpj() {
		return controleCpfCnpj;
	}
	
	/**
	 * Set: controleCpfCnpj.
	 *
	 * @param controleCpfCnpj the controle cpf cnpj
	 */
	public void setControleCpfCnpj(int controleCpfCnpj) {
		this.controleCpfCnpj = controleCpfCnpj;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public long getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(long cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: filialCnpj.
	 *
	 * @return filialCnpj
	 */
	public int getFilialCnpj() {
		return filialCnpj;
	}
	
	/**
	 * Set: filialCnpj.
	 *
	 * @param filialCnpj the filial cnpj
	 */
	public void setFilialCnpj(int filialCnpj) {
		this.filialCnpj = filialCnpj;
	}
	
	/**
	 * Get: filler.
	 *
	 * @return filler
	 */
	public String getFiller() {
		return filler;
	}
	
	/**
	 * Set: filler.
	 *
	 * @param filler the filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
	/**
	 * Get: numeroArquivoRemessa.
	 *
	 * @return numeroArquivoRemessa
	 */
	public long getNumeroArquivoRemessa() {
		return numeroArquivoRemessa;
	}
	
	/**
	 * Set: numeroArquivoRemessa.
	 *
	 * @param numeroArquivoRemessa the numero arquivo remessa
	 */
	public void setNumeroArquivoRemessa(long numeroArquivoRemessa) {
		this.numeroArquivoRemessa = numeroArquivoRemessa;
	}
	
	/**
	 * Get: numeroLoteRemessa.
	 *
	 * @return numeroLoteRemessa
	 */
	public long getNumeroLoteRemessa() {
		return numeroLoteRemessa;
	}
	
	/**
	 * Set: numeroLoteRemessa.
	 *
	 * @param numeroLoteRemessa the numero lote remessa
	 */
	public void setNumeroLoteRemessa(long numeroLoteRemessa) {
		this.numeroLoteRemessa = numeroLoteRemessa;
	}
	
	/**
	 * Get: numeroSequenciaRemessa.
	 *
	 * @return numeroSequenciaRemessa
	 */
	public long getNumeroSequenciaRemessa() {
		return numeroSequenciaRemessa;
	}
	
	/**
	 * Set: numeroSequenciaRemessa.
	 *
	 * @param numeroSequenciaRemessa the numero sequencia remessa
	 */
	public void setNumeroSequenciaRemessa(long numeroSequenciaRemessa) {
		this.numeroSequenciaRemessa = numeroSequenciaRemessa;
	}
	
	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public long getPerfil() {
		return perfil;
	}
	
	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(long perfil) {
		this.perfil = perfil;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public int getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(int recurso) {
		this.recurso = recurso;
	}
	
	/**
	 * Get: tipoPesquisa.
	 *
	 * @return tipoPesquisa
	 */
	public String getTipoPesquisa() {
		return tipoPesquisa;
	}
	
	/**
	 * Set: tipoPesquisa.
	 *
	 * @param tipoPesquisa the tipo pesquisa
	 */
	public void setTipoPesquisa(String tipoPesquisa) {
		this.tipoPesquisa = tipoPesquisa;
	}
	
	/**
	 * Get: conteudoEnviado.
	 *
	 * @return conteudoEnviado
	 */
	public String getConteudoEnviado() {
		return conteudoEnviado;
	}
	
	/**
	 * Set: conteudoEnviado.
	 *
	 * @param conteudoEnviado the conteudo enviado
	 */
	public void setConteudoEnviado(String conteudoEnviado) {
		this.conteudoEnviado = conteudoEnviado;
	}
	
	/**
	 * Get: nivelTipoMensagem.
	 *
	 * @return nivelTipoMensagem
	 */
	public int getNivelTipoMensagem() {
		return nivelTipoMensagem;
	}
	
	/**
	 * Set: nivelTipoMensagem.
	 *
	 * @param nivelTipoMensagem the nivel tipo mensagem
	 */
	public void setNivelTipoMensagem(int nivelTipoMensagem) {
		this.nivelTipoMensagem = nivelTipoMensagem;
	}
	
	/**
	 * Get: numeroEventoMensagem.
	 *
	 * @return numeroEventoMensagem
	 */
	public int getNumeroEventoMensagem() {
		return numeroEventoMensagem;
	}
	
	/**
	 * Set: numeroEventoMensagem.
	 *
	 * @param numeroEventoMensagem the numero evento mensagem
	 */
	public void setNumeroEventoMensagem(int numeroEventoMensagem) {
		this.numeroEventoMensagem = numeroEventoMensagem;
	}
	
	/**
	 * Get: textoMensagem.
	 *
	 * @return textoMensagem
	 */
	public String getTextoMensagem() {
		return textoMensagem;
	}
	
	/**
	 * Set: textoMensagem.
	 *
	 * @param textoMensagem the texto mensagem
	 */
	public void setTextoMensagem(String textoMensagem) {
		this.textoMensagem = textoMensagem;
	}

}
