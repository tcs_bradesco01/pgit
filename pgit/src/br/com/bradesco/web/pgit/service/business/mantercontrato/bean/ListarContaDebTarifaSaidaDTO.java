package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.List;

public class ListarContaDebTarifaSaidaDTO {
	
   private  String codMensagem;
   private  String mensagem;
   private  Integer numeroLinhas;
   private List<ListarContaDebTarifaOcorrencias> ocorrencias;
   
	public String getCodMensagem() {
		return codMensagem;
	}
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
	public List<ListarContaDebTarifaOcorrencias> getOcorrencias() {
		return ocorrencias;
	}
	public void setOcorrencias(List<ListarContaDebTarifaOcorrencias> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

}
