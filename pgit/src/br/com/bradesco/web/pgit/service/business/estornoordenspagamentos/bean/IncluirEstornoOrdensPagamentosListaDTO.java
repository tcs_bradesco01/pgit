/*
 * Nome: br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean;

import java.math.BigDecimal;

/**
 * Nome: IncluirEstornoOrdensPagamentosListaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirEstornoOrdensPagamentosListaDTO {

	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
	
	/** Atributo dtEfetivacao. */
	private String dtEfetivacao;
	
	/** Atributo cdModalidadePagamento. */
	private Integer cdModalidadePagamento;
	
	/** Atributo cdValorEstorno. */
	private BigDecimal cdValorEstorno;

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}

	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}

	/**
	 * Get: dtEfetivacao.
	 *
	 * @return dtEfetivacao
	 */
	public String getDtEfetivacao() {
		return dtEfetivacao;
	}

	/**
	 * Set: dtEfetivacao.
	 *
	 * @param dtEfetivacao the dt efetivacao
	 */
	public void setDtEfetivacao(String dtEfetivacao) {
		this.dtEfetivacao = dtEfetivacao;
	}

	/**
	 * Get: cdModalidadePagamento.
	 *
	 * @return cdModalidadePagamento
	 */
	public Integer getCdModalidadePagamento() {
		return cdModalidadePagamento;
	}

	/**
	 * Set: cdModalidadePagamento.
	 *
	 * @param cdModalidadePagamento the cd modalidade pagamento
	 */
	public void setCdModalidadePagamento(Integer cdModalidadePagamento) {
		this.cdModalidadePagamento = cdModalidadePagamento;
	}

	/**
	 * Get: cdValorEstorno.
	 *
	 * @return cdValorEstorno
	 */
	public BigDecimal getCdValorEstorno() {
		return cdValorEstorno;
	}

	/**
	 * Set: cdValorEstorno.
	 *
	 * @param cdValorEstorno the cd valor estorno
	 */
	public void setCdValorEstorno(BigDecimal cdValorEstorno) {
		this.cdValorEstorno = cdValorEstorno;
	}
}