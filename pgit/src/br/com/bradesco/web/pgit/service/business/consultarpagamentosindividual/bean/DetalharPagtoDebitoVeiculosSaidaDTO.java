/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharPagtoDebitoVeiculosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPagtoDebitoVeiculosSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nrCotaParcelaTrib. */
    private String nrCotaParcelaTrib;
    
    /** Atributo dtAnoExercTributo. */
    private String dtAnoExercTributo;
    
    /** Atributo cdPlacaVeiculo. */
    private String cdPlacaVeiculo;
    
    /** Atributo cdRenavamVeicTributo. */
    private String cdRenavamVeicTributo;
    
    /** Atributo cdMunicArrecadTrib. */
    private String cdMunicArrecadTrib;
    
    /** Atributo cdUfTributo. */
    private String cdUfTributo;
    
    /** Atributo nrNsuTributo. */
    private String nrNsuTributo;
    
    /** Atributo cdOpcaoTributo. */
    private String cdOpcaoTributo;
    
    /** Atributo cdOpcaoRetirada. */
    private String cdOpcaoRetirada;
    
    /** Atributo vlPrincipalTributo. */
    private BigDecimal vlPrincipalTributo;
    
    /** Atributo vlAtualMonetar. */
    private BigDecimal vlAtualMonetar;
    
    /** Atributo vlAcrescimoFinanc. */
    private BigDecimal vlAcrescimoFinanc;
    
    /** Atributo vlDesconto. */
    private BigDecimal vlDesconto;
    
    /** Atributo vlAbatimento. */
    private BigDecimal vlAbatimento;
    
    /** Atributo vlMulta. */
    private BigDecimal vlMulta;
    
    /** Atributo vlMoraJuros. */
    private BigDecimal vlMoraJuros;
    
    /** Atributo vlTotalTributo. */
    private BigDecimal vlTotalTributo;
    
    /** Atributo dsObservacaoTributo. */
    private String dsObservacaoTributo;
    
    /** Atributo cdIndicadorModalidadePgit. */
    private Integer cdIndicadorModalidadePgit;
    
    /** Atributo dsPagamento. */
    private String dsPagamento;
    
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;
    
    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;
    
    /** Atributo dsTipoContaDebito. */
    private String dsTipoContaDebito;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdSituacaoContrato. */
    private String cdSituacaoContrato;
    
    /** Atributo dtAgendamento. */
    private String dtAgendamento;
    
    /** Atributo vlAgendado. */
    private BigDecimal vlAgendado;
    
    /** Atributo vlEfetivo. */
    private BigDecimal vlEfetivo;
    
    /** Atributo cdIndicadorEconomicoMoeda. */
    private Integer cdIndicadorEconomicoMoeda;
    
    /** Atributo qtMoeda. */
    private BigDecimal qtMoeda;
    
    /** Atributo dtVencimento. */
    private String dtVencimento;
    
    /** Atributo dsMensagemPrimeiraLinha. */
    private String dsMensagemPrimeiraLinha;
    
    /** Atributo dsMensagemSegundaLinha. */
    private String dsMensagemSegundaLinha;
    
    /** Atributo dsUsoEmpresa. */
    private String dsUsoEmpresa;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;
    
    /** Atributo cdMotivoSituacaoPagamento. */
    private Integer cdMotivoSituacaoPagamento;
    
    /** Atributo cdListaDebito. */
    private String cdListaDebito;
    
    /** Atributo cdTipoInscricaoFavorecido. */
    private Integer cdTipoInscricaoFavorecido;
    
    /** Atributo nrDocumento. */
    private String nrDocumento;
    
    /** Atributo cdSerieDocumento. */
    private String cdSerieDocumento;
    
    /** Atributo cdTipoDocumento. */
    private Integer cdTipoDocumento;
    
    /** Atributo dsTipoDocumento. */
    private String dsTipoDocumento;
    
    /** Atributo vlDocumento. */
    private BigDecimal vlDocumento;
    
    /** Atributo dtEmissaoDocumento. */
    private String dtEmissaoDocumento;
    
    /** Atributo nrSequenciaArquivoRemessa. */
    private String nrSequenciaArquivoRemessa;
    
    /** Atributo nrLoteArquivoRemessa. */
    private String nrLoteArquivoRemessa;
    
    /** Atributo cdFavorecido. */
    private String cdFavorecido;
    
    /** Atributo dsBancoFavorecido. */
    private String dsBancoFavorecido;
    
    /** Atributo dsAgenciaFavorecido. */
    private String dsAgenciaFavorecido;
    
    /** Atributo cdTipoContaFavorecido. */
    private Integer cdTipoContaFavorecido;
    
    /** Atributo dsTipoContaFavorecido. */
    private String dsTipoContaFavorecido;
    
    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;
    
    /** Atributo cdTipoManutencao. */
    private Integer cdTipoManutencao;
    
    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;
    
    /** Atributo dtInclusao. */
    private String dtInclusao;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdUsuarioInclusaoInterno. */
    private String cdUsuarioInclusaoInterno;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsTipoCanalInclusao. */
    private String dsTipoCanalInclusao;
    
    /** Atributo cdFluxoInclusao. */
    private String cdFluxoInclusao;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdUsuarioManutencaoInterno. */
    private String cdUsuarioManutencaoInterno;
    
    /** Atributo cdUsuarioManutencaoExterno. */
    private String cdUsuarioManutencaoExterno;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsTipoCanalManutencao. */
    private String dsTipoCanalManutencao;
    
    /** Atributo cdFluxoManutencao. */
    private String cdFluxoManutencao;
    
    /** Atributo dsMoeda. */
    private String dsMoeda;
    
    //TRILHA DE AUDITORIA
    /** Atributo dataHoraInclusaoFormatada. */
    private String dataHoraInclusaoFormatada;
    
    /** Atributo usuarioInclusaoFormatado. */
    private String usuarioInclusaoFormatado;
    
    /** Atributo tipoCanalInclusaoFormatado. */
    private String tipoCanalInclusaoFormatado;
    
    /** Atributo complementoInclusaoFormatado. */
    private String complementoInclusaoFormatado;
    
    /** Atributo dataHoraManutencaoFormatada. */
    private String dataHoraManutencaoFormatada;
    
    /** Atributo usuarioManutencaoFormatado. */
    private String usuarioManutencaoFormatado;
    
    /** Atributo tipoCanalManutencaoFormatado. */
    private String tipoCanalManutencaoFormatado;
    
    /** Atributo complementoManutencaoFormatado. */
    private String complementoManutencaoFormatado;  
    
    // CLIENTE
    /** Atributo cdCpfCnpjCliente. */
    private Long cdCpfCnpjCliente;

    /** Atributo nomeCliente. */
    private String nomeCliente;

    /** Atributo dsPessoaJuridicaContrato. */
    private String dsPessoaJuridicaContrato;

    /** Atributo nrSequenciaContratoNegocio. */
    private String nrSequenciaContratoNegocio;
    
    /** Atributo cpfCnpjClienteFormatado. */
    private String cpfCnpjClienteFormatado;

    // CONTA DE D�BITO
    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;

    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;

    /** Atributo cdDigAgenciaDebto. */
    private String cdDigAgenciaDebto;

    /** Atributo cdContaDebito. */
    private Long cdContaDebito;

    /** Atributo dsDigAgenciaDebito. */
    private String dsDigAgenciaDebito;

    /** Atributo bancoDebitoFormatado. */
    private String bancoDebitoFormatado;

    /** Atributo agenciaDebitoFormatada. */
    private String agenciaDebitoFormatada;

    /** Atributo contaDebitoFormatada. */
    private String contaDebitoFormatada;

    // FAVORECIDO
    /** Atributo nmInscriFavorecido. */
    private String nmInscriFavorecido;

    /** Atributo dsNomeFavorecido. */
    private String dsNomeFavorecido;

    /** Atributo cdBancoCredito. */
    private Integer cdBancoCredito;

    /** Atributo cdAgenciaCredito. */
    private Integer cdAgenciaCredito;

    /** Atributo cdDigAgenciaCredito. */
    private String cdDigAgenciaCredito;

    /** Atributo cdContaCredito. */
    private Long cdContaCredito;

    /** Atributo cdDigContaCredito. */
    private String cdDigContaCredito;

    /** Atributo numeroInscricaoFavorecidoFormatado. */
    private String numeroInscricaoFavorecidoFormatado;
    
    /** Atributo bancoCreditoFormatado. */
    private String bancoCreditoFormatado;

    /** Atributo agenciaCreditoFormatada. */
    private String agenciaCreditoFormatada;

    /** Atributo contaCreditoFormatada. */
    private String contaCreditoFormatada;    

    // PAGAMENTO
    /** Atributo dtPagamento. */
    private String dtPagamento;

    /** Atributo cdSituacaoPagamento. */
    private String cdSituacaoPagamento;

    /** Atributo cdIndicadorAuto. */
    private Integer cdIndicadorAuto;

    /** Atributo cdIndicadorSemConsulta. */
    private Integer cdIndicadorSemConsulta;

    /** Atributo dsTipoServicoOperacao. */
    private String dsTipoServicoOperacao;

    /** Atributo dsModalidadeRelacionado. */
    private String dsModalidadeRelacionado;

    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;
    
    /** Atributo dtFloatingPagamento. */
    private String dtFloatingPagamento;
	
	/** Atributo dtEfetivFloatPgto. */
	private String dtEfetivFloatPgto;
	
	/** Atributo vlFloatingPagamento. */
	private BigDecimal vlFloatingPagamento;
    
	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}
	
	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}
	
	/**
	 * Get: cdFluxoInclusao.
	 *
	 * @return cdFluxoInclusao
	 */
	public String getCdFluxoInclusao() {
		return cdFluxoInclusao;
	}
	
	/**
	 * Set: cdFluxoInclusao.
	 *
	 * @param cdFluxoInclusao the cd fluxo inclusao
	 */
	public void setCdFluxoInclusao(String cdFluxoInclusao) {
		this.cdFluxoInclusao = cdFluxoInclusao;
	}
	
	/**
	 * Get: cdFluxoManutencao.
	 *
	 * @return cdFluxoManutencao
	 */
	public String getCdFluxoManutencao() {
		return cdFluxoManutencao;
	}
	
	/**
	 * Set: cdFluxoManutencao.
	 *
	 * @param cdFluxoManutencao the cd fluxo manutencao
	 */
	public void setCdFluxoManutencao(String cdFluxoManutencao) {
		this.cdFluxoManutencao = cdFluxoManutencao;
	}
	
	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public Integer getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}
	
	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(Integer cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}
	
	/**
	 * Get: cdIndicadorModalidadePgit.
	 *
	 * @return cdIndicadorModalidadePgit
	 */
	public Integer getCdIndicadorModalidadePgit() {
		return cdIndicadorModalidadePgit;
	}
	
	/**
	 * Set: cdIndicadorModalidadePgit.
	 *
	 * @param cdIndicadorModalidadePgit the cd indicador modalidade pgit
	 */
	public void setCdIndicadorModalidadePgit(Integer cdIndicadorModalidadePgit) {
		this.cdIndicadorModalidadePgit = cdIndicadorModalidadePgit;
	}
	
	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}
	
	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}
	
	/**
	 * Get: cdMotivoSituacaoPagamento.
	 *
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}
	
	/**
	 * Set: cdMotivoSituacaoPagamento.
	 *
	 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}
	
	/**
	 * Get: cdMunicArrecadTrib.
	 *
	 * @return cdMunicArrecadTrib
	 */
	public String getCdMunicArrecadTrib() {
		return cdMunicArrecadTrib;
	}
	
	/**
	 * Set: cdMunicArrecadTrib.
	 *
	 * @param cdMunicArrecadTrib the cd munic arrecad trib
	 */
	public void setCdMunicArrecadTrib(String cdMunicArrecadTrib) {
		this.cdMunicArrecadTrib = cdMunicArrecadTrib;
	}
	
	/**
	 * Get: cdOpcaoRetirada.
	 *
	 * @return cdOpcaoRetirada
	 */
	public String getCdOpcaoRetirada() {
		return cdOpcaoRetirada;
	}
	
	/**
	 * Set: cdOpcaoRetirada.
	 *
	 * @param cdOpcaoRetirada the cd opcao retirada
	 */
	public void setCdOpcaoRetirada(String cdOpcaoRetirada) {
		this.cdOpcaoRetirada = cdOpcaoRetirada;
	}
	
	/**
	 * Get: cdOpcaoTributo.
	 *
	 * @return cdOpcaoTributo
	 */
	public String getCdOpcaoTributo() {
		return cdOpcaoTributo;
	}
	
	/**
	 * Set: cdOpcaoTributo.
	 *
	 * @param cdOpcaoTributo the cd opcao tributo
	 */
	public void setCdOpcaoTributo(String cdOpcaoTributo) {
		this.cdOpcaoTributo = cdOpcaoTributo;
	}
	
	/**
	 * Get: cdPlacaVeiculo.
	 *
	 * @return cdPlacaVeiculo
	 */
	public String getCdPlacaVeiculo() {
		return cdPlacaVeiculo;
	}
	
	/**
	 * Set: cdPlacaVeiculo.
	 *
	 * @param cdPlacaVeiculo the cd placa veiculo
	 */
	public void setCdPlacaVeiculo(String cdPlacaVeiculo) {
		this.cdPlacaVeiculo = cdPlacaVeiculo;
	}
	
	/**
	 * Get: cdRenavamVeicTributo.
	 *
	 * @return cdRenavamVeicTributo
	 */
	public String getCdRenavamVeicTributo() {
		return cdRenavamVeicTributo;
	}
	
	/**
	 * Set: cdRenavamVeicTributo.
	 *
	 * @param cdRenavamVeicTributo the cd renavam veic tributo
	 */
	public void setCdRenavamVeicTributo(String cdRenavamVeicTributo) {
		this.cdRenavamVeicTributo = cdRenavamVeicTributo;
	}
	
	/**
	 * Get: cdSerieDocumento.
	 *
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}
	
	/**
	 * Set: cdSerieDocumento.
	 *
	 * @param cdSerieDocumento the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoContaFavorecido.
	 *
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}
	
	/**
	 * Set: cdTipoContaFavorecido.
	 *
	 * @param cdTipoContaFavorecido the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}
	
	/**
	 * Get: cdTipoDocumento.
	 *
	 * @return cdTipoDocumento
	 */
	public Integer getCdTipoDocumento() {
		return cdTipoDocumento;
	}
	
	/**
	 * Set: cdTipoDocumento.
	 *
	 * @param cdTipoDocumento the cd tipo documento
	 */
	public void setCdTipoDocumento(Integer cdTipoDocumento) {
		this.cdTipoDocumento = cdTipoDocumento;
	}
	
	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public Integer getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}
	
	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoPagador the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(Integer cdTipoInscricaoPagador) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoPagador;
	}
	
	/**
	 * Get: cdTipoManutencao.
	 *
	 * @return cdTipoManutencao
	 */
	public Integer getCdTipoManutencao() {
		return cdTipoManutencao;
	}
	
	/**
	 * Set: cdTipoManutencao.
	 *
	 * @param cdTipoManutencao the cd tipo manutencao
	 */
	public void setCdTipoManutencao(Integer cdTipoManutencao) {
		this.cdTipoManutencao = cdTipoManutencao;
	}
	
	/**
	 * Get: cdUfTributo.
	 *
	 * @return cdUfTributo
	 */
	public String getCdUfTributo() {
		return cdUfTributo;
	}
	
	/**
	 * Set: cdUfTributo.
	 *
	 * @param cdUfTributo the cd uf tributo
	 */
	public void setCdUfTributo(String cdUfTributo) {
		this.cdUfTributo = cdUfTributo;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioInclusaoInterno.
	 *
	 * @return cdUsuarioInclusaoInterno
	 */
	public String getCdUsuarioInclusaoInterno() {
		return cdUsuarioInclusaoInterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoInterno.
	 *
	 * @param cdUsuarioInclusaoInterno the cd usuario inclusao interno
	 */
	public void setCdUsuarioInclusaoInterno(String cdUsuarioInclusaoInterno) {
		this.cdUsuarioInclusaoInterno = cdUsuarioInclusaoInterno;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutencaoInterno.
	 *
	 * @return cdUsuarioManutencaoInterno
	 */
	public String getCdUsuarioManutencaoInterno() {
		return cdUsuarioManutencaoInterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoInterno.
	 *
	 * @param cdUsuarioManutencaoInterno the cd usuario manutencao interno
	 */
	public void setCdUsuarioManutencaoInterno(String cdUsuarioManutencaoInterno) {
		this.cdUsuarioManutencaoInterno = cdUsuarioManutencaoInterno;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}
	
	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}
	
	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}
	
	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}
	
	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}
	
	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}
	
	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}
	
	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsMensagemPrimeiraLinha.
	 *
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}
	
	/**
	 * Set: dsMensagemPrimeiraLinha.
	 *
	 * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}
	
	/**
	 * Get: dsMensagemSegundaLinha.
	 *
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}
	
	/**
	 * Set: dsMensagemSegundaLinha.
	 *
	 * @param dsMensagemSegundaLinha the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}
	
	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}
	
	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}
	
	/**
	 * Get: dsObservacaoTributo.
	 *
	 * @return dsObservacaoTributo
	 */
	public String getDsObservacaoTributo() {
		return dsObservacaoTributo;
	}
	
	/**
	 * Set: dsObservacaoTributo.
	 *
	 * @param dsObservacaoTributo the ds observacao tributo
	 */
	public void setDsObservacaoTributo(String dsObservacaoTributo) {
		this.dsObservacaoTributo = dsObservacaoTributo;
	}
	
	/**
	 * Get: dsPagamento.
	 *
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}
	
	/**
	 * Set: dsPagamento.
	 *
	 * @param dsPagamento the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: dsTipoContaDebito.
	 *
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}
	
	/**
	 * Set: dsTipoContaDebito.
	 *
	 * @param dsTipoContaDebito the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}
	
	/**
	 * Get: dsTipoContaFavorecido.
	 *
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}
	
	/**
	 * Set: dsTipoContaFavorecido.
	 *
	 * @param dsTipoContaFavorecido the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}
	
	/**
	 * Get: dsTipoDocumento.
	 *
	 * @return dsTipoDocumento
	 */
	public String getDsTipoDocumento() {
		return dsTipoDocumento;
	}
	
	/**
	 * Set: dsTipoDocumento.
	 *
	 * @param dsTipoDocumento the ds tipo documento
	 */
	public void setDsTipoDocumento(String dsTipoDocumento) {
		this.dsTipoDocumento = dsTipoDocumento;
	}
	
	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}
	
	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}
	
	/**
	 * Get: dsUsoEmpresa.
	 *
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}
	
	/**
	 * Set: dsUsoEmpresa.
	 *
	 * @param dsUsoEmpresa the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}
	
	/**
	 * Get: dtAgendamento.
	 *
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}
	
	/**
	 * Set: dtAgendamento.
	 *
	 * @param dtAgendamento the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}
	
	/**
	 * Get: dtAnoExercTributo.
	 *
	 * @return dtAnoExercTributo
	 */
	public String getDtAnoExercTributo() {
		return dtAnoExercTributo;
	}
	
	/**
	 * Set: dtAnoExercTributo.
	 *
	 * @param dtAnoExercTributo the dt ano exerc tributo
	 */
	public void setDtAnoExercTributo(String dtAnoExercTributo) {
		this.dtAnoExercTributo = dtAnoExercTributo;
	}
	
	/**
	 * Get: dtEmissaoDocumento.
	 *
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}
	
	/**
	 * Set: dtEmissaoDocumento.
	 *
	 * @param dtEmissaoDocumento the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}
	
	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}
	
	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}
	
	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}
	
	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}
	
	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}
	
	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	
	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}
	
	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}
	
	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}
	
	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrCotaParcelaTrib.
	 *
	 * @return nrCotaParcelaTrib
	 */
	public String getNrCotaParcelaTrib() {
		return nrCotaParcelaTrib;
	}
	
	/**
	 * Set: nrCotaParcelaTrib.
	 *
	 * @param nrCotaParcelaTrib the nr cota parcela trib
	 */
	public void setNrCotaParcelaTrib(String nrCotaParcelaTrib) {
		this.nrCotaParcelaTrib = nrCotaParcelaTrib;
	}
	
	/**
	 * Get: nrDocumento.
	 *
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}
	
	/**
	 * Set: nrDocumento.
	 *
	 * @param nrDocumento the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}
	
	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}
	
	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}
	
	/**
	 * Get: nrNsuTributo.
	 *
	 * @return nrNsuTributo
	 */
	public String getNrNsuTributo() {
		return nrNsuTributo;
	}
	
	/**
	 * Set: nrNsuTributo.
	 *
	 * @param nrNsuTributo the nr nsu tributo
	 */
	public void setNrNsuTributo(String nrNsuTributo) {
		this.nrNsuTributo = nrNsuTributo;
	}
	
	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}
	
	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}
	
	/**
	 * Get: qtMoeda.
	 *
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}
	
	/**
	 * Set: qtMoeda.
	 *
	 * @param qtMoeda the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}
	
	/**
	 * Get: vlAbatimento.
	 *
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}
	
	/**
	 * Set: vlAbatimento.
	 *
	 * @param vlAbatimento the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}
	
	/**
	 * Get: vlAcrescimoFinanc.
	 *
	 * @return vlAcrescimoFinanc
	 */
	public BigDecimal getVlAcrescimoFinanc() {
		return vlAcrescimoFinanc;
	}
	
	/**
	 * Set: vlAcrescimoFinanc.
	 *
	 * @param vlAcrescimoFinanc the vl acrescimo financ
	 */
	public void setVlAcrescimoFinanc(BigDecimal vlAcrescimoFinanc) {
		this.vlAcrescimoFinanc = vlAcrescimoFinanc;
	}
	
	/**
	 * Get: vlAgendado.
	 *
	 * @return vlAgendado
	 */
	public BigDecimal getVlAgendado() {
		return vlAgendado;
	}
	
	/**
	 * Set: vlAgendado.
	 *
	 * @param vlAgendado the vl agendado
	 */
	public void setVlAgendado(BigDecimal vlAgendado) {
		this.vlAgendado = vlAgendado;
	}
	
	/**
	 * Get: vlAtualMonetar.
	 *
	 * @return vlAtualMonetar
	 */
	public BigDecimal getVlAtualMonetar() {
		return vlAtualMonetar;
	}
	
	/**
	 * Set: vlAtualMonetar.
	 *
	 * @param vlAtualMonetar the vl atual monetar
	 */
	public void setVlAtualMonetar(BigDecimal vlAtualMonetar) {
		this.vlAtualMonetar = vlAtualMonetar;
	}
	
	/**
	 * Get: vlDesconto.
	 *
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}
	
	/**
	 * Set: vlDesconto.
	 *
	 * @param vlDesconto the vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}
	
	/**
	 * Get: vlDocumento.
	 *
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}
	
	/**
	 * Set: vlDocumento.
	 *
	 * @param vlDocumento the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}
	
	/**
	 * Get: vlEfetivo.
	 *
	 * @return vlEfetivo
	 */
	public BigDecimal getVlEfetivo() {
		return vlEfetivo;
	}
	
	/**
	 * Set: vlEfetivo.
	 *
	 * @param vlEfetivo the vl efetivo
	 */
	public void setVlEfetivo(BigDecimal vlEfetivo) {
		this.vlEfetivo = vlEfetivo;
	}
	
	/**
	 * Get: vlMoraJuros.
	 *
	 * @return vlMoraJuros
	 */
	public BigDecimal getVlMoraJuros() {
		return vlMoraJuros;
	}
	
	/**
	 * Set: vlMoraJuros.
	 *
	 * @param vlMoraJuros the vl mora juros
	 */
	public void setVlMoraJuros(BigDecimal vlMoraJuros) {
		this.vlMoraJuros = vlMoraJuros;
	}
	
	/**
	 * Get: vlMulta.
	 *
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}
	
	/**
	 * Set: vlMulta.
	 *
	 * @param vlMulta the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}
	
	/**
	 * Get: vlPrincipalTributo.
	 *
	 * @return vlPrincipalTributo
	 */
	public BigDecimal getVlPrincipalTributo() {
		return vlPrincipalTributo;
	}
	
	/**
	 * Set: vlPrincipalTributo.
	 *
	 * @param vlPrincipalTributo the vl principal tributo
	 */
	public void setVlPrincipalTributo(BigDecimal vlPrincipalTributo) {
		this.vlPrincipalTributo = vlPrincipalTributo;
	}
	
	/**
	 * Get: vlTotalTributo.
	 *
	 * @return vlTotalTributo
	 */
	public BigDecimal getVlTotalTributo() {
		return vlTotalTributo;
	}
	
	/**
	 * Set: vlTotalTributo.
	 *
	 * @param vlTotalTributo the vl total tributo
	 */
	public void setVlTotalTributo(BigDecimal vlTotalTributo) {
		this.vlTotalTributo = vlTotalTributo;
	}
	
	/**
	 * Get: complementoInclusaoFormatado.
	 *
	 * @return complementoInclusaoFormatado
	 */
	public String getComplementoInclusaoFormatado() {
		return complementoInclusaoFormatado;
	}
	
	/**
	 * Set: complementoInclusaoFormatado.
	 *
	 * @param complementoInclusaoFormatado the complemento inclusao formatado
	 */
	public void setComplementoInclusaoFormatado(String complementoInclusaoFormatado) {
		this.complementoInclusaoFormatado = complementoInclusaoFormatado;
	}
	
	/**
	 * Get: complementoManutencaoFormatado.
	 *
	 * @return complementoManutencaoFormatado
	 */
	public String getComplementoManutencaoFormatado() {
		return complementoManutencaoFormatado;
	}
	
	/**
	 * Set: complementoManutencaoFormatado.
	 *
	 * @param complementoManutencaoFormatado the complemento manutencao formatado
	 */
	public void setComplementoManutencaoFormatado(
			String complementoManutencaoFormatado) {
		this.complementoManutencaoFormatado = complementoManutencaoFormatado;
	}
	
	/**
	 * Get: dataHoraInclusaoFormatada.
	 *
	 * @return dataHoraInclusaoFormatada
	 */
	public String getDataHoraInclusaoFormatada() {
		return dataHoraInclusaoFormatada;
	}
	
	/**
	 * Set: dataHoraInclusaoFormatada.
	 *
	 * @param dataHoraInclusaoFormatada the data hora inclusao formatada
	 */
	public void setDataHoraInclusaoFormatada(String dataHoraInclusaoFormatada) {
		this.dataHoraInclusaoFormatada = dataHoraInclusaoFormatada;
	}
	
	/**
	 * Get: dataHoraManutencaoFormatada.
	 *
	 * @return dataHoraManutencaoFormatada
	 */
	public String getDataHoraManutencaoFormatada() {
		return dataHoraManutencaoFormatada;
	}
	
	/**
	 * Set: dataHoraManutencaoFormatada.
	 *
	 * @param dataHoraManutencaoFormatada the data hora manutencao formatada
	 */
	public void setDataHoraManutencaoFormatada(String dataHoraManutencaoFormatada) {
		this.dataHoraManutencaoFormatada = dataHoraManutencaoFormatada;
	}
	
	/**
	 * Get: tipoCanalInclusaoFormatado.
	 *
	 * @return tipoCanalInclusaoFormatado
	 */
	public String getTipoCanalInclusaoFormatado() {
		return tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Set: tipoCanalInclusaoFormatado.
	 *
	 * @param tipoCanalInclusaoFormatado the tipo canal inclusao formatado
	 */
	public void setTipoCanalInclusaoFormatado(String tipoCanalInclusaoFormatado) {
		this.tipoCanalInclusaoFormatado = tipoCanalInclusaoFormatado;
	}
	
	/**
	 * Get: tipoCanalManutencaoFormatado.
	 *
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado() {
		return tipoCanalManutencaoFormatado;
	}
	
	/**
	 * Set: tipoCanalManutencaoFormatado.
	 *
	 * @param tipoCanalManutencaoFormatado the tipo canal manutencao formatado
	 */
	public void setTipoCanalManutencaoFormatado(String tipoCanalManutencaoFormatado) {
		this.tipoCanalManutencaoFormatado = tipoCanalManutencaoFormatado;
	}
	
	/**
	 * Get: usuarioInclusaoFormatado.
	 *
	 * @return usuarioInclusaoFormatado
	 */
	public String getUsuarioInclusaoFormatado() {
		return usuarioInclusaoFormatado;
	}
	
	/**
	 * Set: usuarioInclusaoFormatado.
	 *
	 * @param usuarioInclusaoFormatado the usuario inclusao formatado
	 */
	public void setUsuarioInclusaoFormatado(String usuarioInclusaoFormatado) {
		this.usuarioInclusaoFormatado = usuarioInclusaoFormatado;
	}
	
	/**
	 * Get: usuarioManutencaoFormatado.
	 *
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}
	
	/**
	 * Set: usuarioManutencaoFormatado.
	 *
	 * @param usuarioManutencaoFormatado the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}
	
	/**
	 * Get: dsMoeda.
	 *
	 * @return dsMoeda
	 */
	public String getDsMoeda() {
		return dsMoeda;
	}
	
	/**
	 * Set: dsMoeda.
	 *
	 * @param dsMoeda the ds moeda
	 */
	public void setDsMoeda(String dsMoeda) {
		this.dsMoeda = dsMoeda;
	}
	
	/**
	 * Get: agenciaCreditoFormatada.
	 *
	 * @return agenciaCreditoFormatada
	 */
	public String getAgenciaCreditoFormatada() {
	    return agenciaCreditoFormatada;
	}
	
	/**
	 * Set: agenciaCreditoFormatada.
	 *
	 * @param agenciaCreditoFormatada the agencia credito formatada
	 */
	public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
	    this.agenciaCreditoFormatada = agenciaCreditoFormatada;
	}
	
	/**
	 * Get: agenciaDebitoFormatada.
	 *
	 * @return agenciaDebitoFormatada
	 */
	public String getAgenciaDebitoFormatada() {
	    return agenciaDebitoFormatada;
	}
	
	/**
	 * Set: agenciaDebitoFormatada.
	 *
	 * @param agenciaDebitoFormatada the agencia debito formatada
	 */
	public void setAgenciaDebitoFormatada(String agenciaDebitoFormatada) {
	    this.agenciaDebitoFormatada = agenciaDebitoFormatada;
	}
	
	/**
	 * Get: bancoCreditoFormatado.
	 *
	 * @return bancoCreditoFormatado
	 */
	public String getBancoCreditoFormatado() {
	    return bancoCreditoFormatado;
	}
	
	/**
	 * Set: bancoCreditoFormatado.
	 *
	 * @param bancoCreditoFormatado the banco credito formatado
	 */
	public void setBancoCreditoFormatado(String bancoCreditoFormatado) {
	    this.bancoCreditoFormatado = bancoCreditoFormatado;
	}
	
	/**
	 * Get: bancoDebitoFormatado.
	 *
	 * @return bancoDebitoFormatado
	 */
	public String getBancoDebitoFormatado() {
	    return bancoDebitoFormatado;
	}
	
	/**
	 * Set: bancoDebitoFormatado.
	 *
	 * @param bancoDebitoFormatado the banco debito formatado
	 */
	public void setBancoDebitoFormatado(String bancoDebitoFormatado) {
	    this.bancoDebitoFormatado = bancoDebitoFormatado;
	}
	
	/**
	 * Get: cdAgenciaCredito.
	 *
	 * @return cdAgenciaCredito
	 */
	public Integer getCdAgenciaCredito() {
	    return cdAgenciaCredito;
	}
	
	/**
	 * Set: cdAgenciaCredito.
	 *
	 * @param cdAgenciaCredito the cd agencia credito
	 */
	public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
	    this.cdAgenciaCredito = cdAgenciaCredito;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
	    return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
	    this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public Integer getCdBancoCredito() {
	    return cdBancoCredito;
	}
	
	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(Integer cdBancoCredito) {
	    this.cdBancoCredito = cdBancoCredito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
	    return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
	    this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaCredito.
	 *
	 * @return cdContaCredito
	 */
	public Long getCdContaCredito() {
	    return cdContaCredito;
	}
	
	/**
	 * Set: cdContaCredito.
	 *
	 * @param cdContaCredito the cd conta credito
	 */
	public void setCdContaCredito(Long cdContaCredito) {
	    this.cdContaCredito = cdContaCredito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
	    return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
	    this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
	    return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
	    this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdCpfCnpjCliente.
	 *
	 * @return cdCpfCnpjCliente
	 */
	public Long getCdCpfCnpjCliente() {
	    return cdCpfCnpjCliente;
	}
	
	/**
	 * Set: cdCpfCnpjCliente.
	 *
	 * @param cdCpfCnpjCliente the cd cpf cnpj cliente
	 */
	public void setCdCpfCnpjCliente(Long cdCpfCnpjCliente) {
	    this.cdCpfCnpjCliente = cdCpfCnpjCliente;
	}
	
	/**
	 * Get: cdDigAgenciaCredito.
	 *
	 * @return cdDigAgenciaCredito
	 */
	public String getCdDigAgenciaCredito() {
	    return cdDigAgenciaCredito;
	}
	
	/**
	 * Set: cdDigAgenciaCredito.
	 *
	 * @param cdDigAgenciaCredito the cd dig agencia credito
	 */
	public void setCdDigAgenciaCredito(String cdDigAgenciaCredito) {
	    this.cdDigAgenciaCredito = cdDigAgenciaCredito;
	}
	
	/**
	 * Get: cdDigAgenciaDebto.
	 *
	 * @return cdDigAgenciaDebto
	 */
	public String getCdDigAgenciaDebto() {
	    return cdDigAgenciaDebto;
	}
	
	/**
	 * Set: cdDigAgenciaDebto.
	 *
	 * @param cdDigAgenciaDebto the cd dig agencia debto
	 */
	public void setCdDigAgenciaDebto(String cdDigAgenciaDebto) {
	    this.cdDigAgenciaDebto = cdDigAgenciaDebto;
	}
	
	/**
	 * Get: cdDigContaCredito.
	 *
	 * @return cdDigContaCredito
	 */
	public String getCdDigContaCredito() {
	    return cdDigContaCredito;
	}
	
	/**
	 * Set: cdDigContaCredito.
	 *
	 * @param cdDigContaCredito the cd dig conta credito
	 */
	public void setCdDigContaCredito(String cdDigContaCredito) {
	    this.cdDigContaCredito = cdDigContaCredito;
	}
	
	/**
	 * Get: cdIndicadorAuto.
	 *
	 * @return cdIndicadorAuto
	 */
	public Integer getCdIndicadorAuto() {
	    return cdIndicadorAuto;
	}
	
	/**
	 * Set: cdIndicadorAuto.
	 *
	 * @param cdIndicadorAuto the cd indicador auto
	 */
	public void setCdIndicadorAuto(Integer cdIndicadorAuto) {
	    this.cdIndicadorAuto = cdIndicadorAuto;
	}
	
	/**
	 * Get: cdIndicadorSemConsulta.
	 *
	 * @return cdIndicadorSemConsulta
	 */
	public Integer getCdIndicadorSemConsulta() {
	    return cdIndicadorSemConsulta;
	}
	
	/**
	 * Set: cdIndicadorSemConsulta.
	 *
	 * @param cdIndicadorSemConsulta the cd indicador sem consulta
	 */
	public void setCdIndicadorSemConsulta(Integer cdIndicadorSemConsulta) {
	    this.cdIndicadorSemConsulta = cdIndicadorSemConsulta;
	}
	
	/**
	 * Get: cdSituacaoPagamento.
	 *
	 * @return cdSituacaoPagamento
	 */
	public String getCdSituacaoPagamento() {
	    return cdSituacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoPagamento.
	 *
	 * @param cdSituacaoPagamento the cd situacao pagamento
	 */
	public void setCdSituacaoPagamento(String cdSituacaoPagamento) {
	    this.cdSituacaoPagamento = cdSituacaoPagamento;
	}
	
	/**
	 * Get: contaCreditoFormatada.
	 *
	 * @return contaCreditoFormatada
	 */
	public String getContaCreditoFormatada() {
	    return contaCreditoFormatada;
	}
	
	/**
	 * Set: contaCreditoFormatada.
	 *
	 * @param contaCreditoFormatada the conta credito formatada
	 */
	public void setContaCreditoFormatada(String contaCreditoFormatada) {
	    this.contaCreditoFormatada = contaCreditoFormatada;
	}
	
	/**
	 * Get: contaDebitoFormatada.
	 *
	 * @return contaDebitoFormatada
	 */
	public String getContaDebitoFormatada() {
	    return contaDebitoFormatada;
	}
	
	/**
	 * Set: contaDebitoFormatada.
	 *
	 * @param contaDebitoFormatada the conta debito formatada
	 */
	public void setContaDebitoFormatada(String contaDebitoFormatada) {
	    this.contaDebitoFormatada = contaDebitoFormatada;
	}
	
	/**
	 * Get: cpfCnpjClienteFormatado.
	 *
	 * @return cpfCnpjClienteFormatado
	 */
	public String getCpfCnpjClienteFormatado() {
	    return cpfCnpjClienteFormatado;
	}
	
	/**
	 * Set: cpfCnpjClienteFormatado.
	 *
	 * @param cpfCnpjClienteFormatado the cpf cnpj cliente formatado
	 */
	public void setCpfCnpjClienteFormatado(String cpfCnpjClienteFormatado) {
	    this.cpfCnpjClienteFormatado = cpfCnpjClienteFormatado;
	}
	
	/**
	 * Get: dsDigAgenciaDebito.
	 *
	 * @return dsDigAgenciaDebito
	 */
	public String getDsDigAgenciaDebito() {
	    return dsDigAgenciaDebito;
	}
	
	/**
	 * Set: dsDigAgenciaDebito.
	 *
	 * @param dsDigAgenciaDebito the ds dig agencia debito
	 */
	public void setDsDigAgenciaDebito(String dsDigAgenciaDebito) {
	    this.dsDigAgenciaDebito = dsDigAgenciaDebito;
	}
	
	/**
	 * Get: dsModalidadeRelacionado.
	 *
	 * @return dsModalidadeRelacionado
	 */
	public String getDsModalidadeRelacionado() {
	    return dsModalidadeRelacionado;
	}
	
	/**
	 * Set: dsModalidadeRelacionado.
	 *
	 * @param dsModalidadeRelacionado the ds modalidade relacionado
	 */
	public void setDsModalidadeRelacionado(String dsModalidadeRelacionado) {
	    this.dsModalidadeRelacionado = dsModalidadeRelacionado;
	}
	
	/**
	 * Get: dsNomeFavorecido.
	 *
	 * @return dsNomeFavorecido
	 */
	public String getDsNomeFavorecido() {
	    return dsNomeFavorecido;
	}
	
	/**
	 * Set: dsNomeFavorecido.
	 *
	 * @param dsNomeFavorecido the ds nome favorecido
	 */
	public void setDsNomeFavorecido(String dsNomeFavorecido) {
	    this.dsNomeFavorecido = dsNomeFavorecido;
	}
	
	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
	    return dsPessoaJuridicaContrato;
	}
	
	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
	    this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}
	
	/**
	 * Get: dsTipoServicoOperacao.
	 *
	 * @return dsTipoServicoOperacao
	 */
	public String getDsTipoServicoOperacao() {
	    return dsTipoServicoOperacao;
	}
	
	/**
	 * Set: dsTipoServicoOperacao.
	 *
	 * @param dsTipoServicoOperacao the ds tipo servico operacao
	 */
	public void setDsTipoServicoOperacao(String dsTipoServicoOperacao) {
	    this.dsTipoServicoOperacao = dsTipoServicoOperacao;
	}
	
	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
	    return dtPagamento;
	}
	
	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
	    this.dtPagamento = dtPagamento;
	}
	
	/**
	 * Get: nmInscriFavorecido.
	 *
	 * @return nmInscriFavorecido
	 */
	public String getNmInscriFavorecido() {
	    return nmInscriFavorecido;
	}
	
	/**
	 * Set: nmInscriFavorecido.
	 *
	 * @param nmInscriFavorecido the nm inscri favorecido
	 */
	public void setNmInscriFavorecido(String nmInscriFavorecido) {
	    this.nmInscriFavorecido = nmInscriFavorecido;
	}
	
	/**
	 * Get: nomeCliente.
	 *
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
	    return nomeCliente;
	}
	
	/**
	 * Set: nomeCliente.
	 *
	 * @param nomeCliente the nome cliente
	 */
	public void setNomeCliente(String nomeCliente) {
	    this.nomeCliente = nomeCliente;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public String getNrSequenciaContratoNegocio() {
	    return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(String nrSequenciaContratoNegocio) {
	    this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroInscricaoFavorecidoFormatado.
	 *
	 * @return numeroInscricaoFavorecidoFormatado
	 */
	public String getNumeroInscricaoFavorecidoFormatado() {
	    return numeroInscricaoFavorecidoFormatado;
	}
	
	/**
	 * Set: numeroInscricaoFavorecidoFormatado.
	 *
	 * @param numeroInscricaoFavorecidoFormatado the numero inscricao favorecido formatado
	 */
	public void setNumeroInscricaoFavorecidoFormatado(String numeroInscricaoFavorecidoFormatado) {
	    this.numeroInscricaoFavorecidoFormatado = numeroInscricaoFavorecidoFormatado;
	}
	
	/**
	 * Get: dtFloatingPagamento.
	 *
	 * @return dtFloatingPagamento
	 */
	public String getDtFloatingPagamento() {
		return dtFloatingPagamento;
	}
	
	/**
	 * Set: dtFloatingPagamento.
	 *
	 * @param dtFloatingPagamento the dt floating pagamento
	 */
	public void setDtFloatingPagamento(String dtFloatingPagamento) {
		this.dtFloatingPagamento = dtFloatingPagamento;
	}
	
	/**
	 * Get: dtEfetivFloatPgto.
	 *
	 * @return dtEfetivFloatPgto
	 */
	public String getDtEfetivFloatPgto() {
		return dtEfetivFloatPgto;
	}
	
	/**
	 * Set: dtEfetivFloatPgto.
	 *
	 * @param dtEfetivFloatPgto the dt efetiv float pgto
	 */
	public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
		this.dtEfetivFloatPgto = dtEfetivFloatPgto;
	}
	
	/**
	 * Get: vlFloatingPagamento.
	 *
	 * @return vlFloatingPagamento
	 */
	public BigDecimal getVlFloatingPagamento() {
		return vlFloatingPagamento;
	}
	
	/**
	 * Set: vlFloatingPagamento.
	 *
	 * @param vlFloatingPagamento the vl floating pagamento
	 */
	public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
		this.vlFloatingPagamento = vlFloatingPagamento;
	}

}