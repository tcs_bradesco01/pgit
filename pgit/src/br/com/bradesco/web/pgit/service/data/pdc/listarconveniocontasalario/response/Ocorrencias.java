/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.math.BigDecimal;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
	 * 
	 */
	private static final long serialVersionUID = -7516606905343842422L;

	/**
     * Field _nSeqFaixaSalarial
     */
    private int _nSeqFaixaSalarial = 0;

    /**
     * keeps track of state for field: _nSeqFaixaSalarial
     */
    private boolean _has_nSeqFaixaSalarial;

    /**
     * Field _vlInicialFaixaSalarial
     */
    private java.math.BigDecimal _vlInicialFaixaSalarial = new java.math.BigDecimal("0");

    /**
     * Field _vlFinalFaixaSalarial
     */
    private java.math.BigDecimal _vlFinalFaixaSalarial = new java.math.BigDecimal("0");

    /**
     * Field _qtdFuncionarioFilial01
     */
    private long _qtdFuncionarioFilial01 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial01
     */
    private boolean _has_qtdFuncionarioFilial01;

    /**
     * Field _qtdFuncionarioFilial02
     */
    private long _qtdFuncionarioFilial02 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial02
     */
    private boolean _has_qtdFuncionarioFilial02;

    /**
     * Field _qtdFuncionarioFilial03
     */
    private long _qtdFuncionarioFilial03 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial03
     */
    private boolean _has_qtdFuncionarioFilial03;

    /**
     * Field _qtdFuncionarioFilial04
     */
    private long _qtdFuncionarioFilial04 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial04
     */
    private boolean _has_qtdFuncionarioFilial04;

    /**
     * Field _qtdFuncionarioFilial05
     */
    private long _qtdFuncionarioFilial05 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial05
     */
    private boolean _has_qtdFuncionarioFilial05;

    /**
     * Field _qtdFuncionarioFilial06
     */
    private long _qtdFuncionarioFilial06 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial06
     */
    private boolean _has_qtdFuncionarioFilial06;

    /**
     * Field _qtdFuncionarioFilial07
     */
    private long _qtdFuncionarioFilial07 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial07
     */
    private boolean _has_qtdFuncionarioFilial07;

    /**
     * Field _qtdFuncionarioFilial08
     */
    private long _qtdFuncionarioFilial08 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial08
     */
    private boolean _has_qtdFuncionarioFilial08;

    /**
     * Field _qtdFuncionarioFilial09
     */
    private long _qtdFuncionarioFilial09 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial09
     */
    private boolean _has_qtdFuncionarioFilial09;

    /**
     * Field _qtdFuncionarioFilial10
     */
    private long _qtdFuncionarioFilial10 = 0;

    /**
     * keeps track of state for field: _qtdFuncionarioFilial10
     */
    private boolean _has_qtdFuncionarioFilial10;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlInicialFaixaSalarial(new java.math.BigDecimal("0"));
        setVlFinalFaixaSalarial(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteNSeqFaixaSalarial
     * 
     */
    public void deleteNSeqFaixaSalarial()
    {
        this._has_nSeqFaixaSalarial= false;
    } //-- void deleteNSeqFaixaSalarial() 

    /**
     * Method deleteQtdFuncionarioFilial01
     * 
     */
    public void deleteQtdFuncionarioFilial01()
    {
        this._has_qtdFuncionarioFilial01= false;
    } //-- void deleteQtdFuncionarioFilial01() 

    /**
     * Method deleteQtdFuncionarioFilial02
     * 
     */
    public void deleteQtdFuncionarioFilial02()
    {
        this._has_qtdFuncionarioFilial02= false;
    } //-- void deleteQtdFuncionarioFilial02() 

    /**
     * Method deleteQtdFuncionarioFilial03
     * 
     */
    public void deleteQtdFuncionarioFilial03()
    {
        this._has_qtdFuncionarioFilial03= false;
    } //-- void deleteQtdFuncionarioFilial03() 

    /**
     * Method deleteQtdFuncionarioFilial04
     * 
     */
    public void deleteQtdFuncionarioFilial04()
    {
        this._has_qtdFuncionarioFilial04= false;
    } //-- void deleteQtdFuncionarioFilial04() 

    /**
     * Method deleteQtdFuncionarioFilial05
     * 
     */
    public void deleteQtdFuncionarioFilial05()
    {
        this._has_qtdFuncionarioFilial05= false;
    } //-- void deleteQtdFuncionarioFilial05() 

    /**
     * Method deleteQtdFuncionarioFilial06
     * 
     */
    public void deleteQtdFuncionarioFilial06()
    {
        this._has_qtdFuncionarioFilial06= false;
    } //-- void deleteQtdFuncionarioFilial06() 

    /**
     * Method deleteQtdFuncionarioFilial07
     * 
     */
    public void deleteQtdFuncionarioFilial07()
    {
        this._has_qtdFuncionarioFilial07= false;
    } //-- void deleteQtdFuncionarioFilial07() 

    /**
     * Method deleteQtdFuncionarioFilial08
     * 
     */
    public void deleteQtdFuncionarioFilial08()
    {
        this._has_qtdFuncionarioFilial08= false;
    } //-- void deleteQtdFuncionarioFilial08() 

    /**
     * Method deleteQtdFuncionarioFilial09
     * 
     */
    public void deleteQtdFuncionarioFilial09()
    {
        this._has_qtdFuncionarioFilial09= false;
    } //-- void deleteQtdFuncionarioFilial09() 

    /**
     * Method deleteQtdFuncionarioFilial10
     * 
     */
    public void deleteQtdFuncionarioFilial10()
    {
        this._has_qtdFuncionarioFilial10= false;
    } //-- void deleteQtdFuncionarioFilial10() 

    /**
     * Returns the value of field 'nSeqFaixaSalarial'.
     * 
     * @return int
     * @return the value of field 'nSeqFaixaSalarial'.
     */
    public int getNSeqFaixaSalarial()
    {
        return this._nSeqFaixaSalarial;
    } //-- int getNSeqFaixaSalarial() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial01'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial01'.
     */
    public long getQtdFuncionarioFilial01()
    {
        return this._qtdFuncionarioFilial01;
    } //-- long getQtdFuncionarioFilial01() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial02'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial02'.
     */
    public long getQtdFuncionarioFilial02()
    {
        return this._qtdFuncionarioFilial02;
    } //-- long getQtdFuncionarioFilial02() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial03'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial03'.
     */
    public long getQtdFuncionarioFilial03()
    {
        return this._qtdFuncionarioFilial03;
    } //-- long getQtdFuncionarioFilial03() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial04'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial04'.
     */
    public long getQtdFuncionarioFilial04()
    {
        return this._qtdFuncionarioFilial04;
    } //-- long getQtdFuncionarioFilial04() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial05'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial05'.
     */
    public long getQtdFuncionarioFilial05()
    {
        return this._qtdFuncionarioFilial05;
    } //-- long getQtdFuncionarioFilial05() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial06'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial06'.
     */
    public long getQtdFuncionarioFilial06()
    {
        return this._qtdFuncionarioFilial06;
    } //-- long getQtdFuncionarioFilial06() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial07'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial07'.
     */
    public long getQtdFuncionarioFilial07()
    {
        return this._qtdFuncionarioFilial07;
    } //-- long getQtdFuncionarioFilial07() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial08'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial08'.
     */
    public long getQtdFuncionarioFilial08()
    {
        return this._qtdFuncionarioFilial08;
    } //-- long getQtdFuncionarioFilial08() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial09'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial09'.
     */
    public long getQtdFuncionarioFilial09()
    {
        return this._qtdFuncionarioFilial09;
    } //-- long getQtdFuncionarioFilial09() 

    /**
     * Returns the value of field 'qtdFuncionarioFilial10'.
     * 
     * @return long
     * @return the value of field 'qtdFuncionarioFilial10'.
     */
    public long getQtdFuncionarioFilial10()
    {
        return this._qtdFuncionarioFilial10;
    } //-- long getQtdFuncionarioFilial10() 

    /**
     * Returns the value of field 'vlFinalFaixaSalarial'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFinalFaixaSalarial'.
     */
    public java.math.BigDecimal getVlFinalFaixaSalarial()
    {
        return this._vlFinalFaixaSalarial;
    } //-- java.math.BigDecimal getVlFinalFaixaSalarial() 

    /**
     * Returns the value of field 'vlInicialFaixaSalarial'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlInicialFaixaSalarial'.
     */
    public java.math.BigDecimal getVlInicialFaixaSalarial()
    {
        return this._vlInicialFaixaSalarial;
    } //-- java.math.BigDecimal getVlInicialFaixaSalarial() 

    /**
     * Method hasNSeqFaixaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNSeqFaixaSalarial()
    {
        return this._has_nSeqFaixaSalarial;
    } //-- boolean hasNSeqFaixaSalarial() 

    /**
     * Method hasQtdFuncionarioFilial01
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial01()
    {
        return this._has_qtdFuncionarioFilial01;
    } //-- boolean hasQtdFuncionarioFilial01() 

    /**
     * Method hasQtdFuncionarioFilial02
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial02()
    {
        return this._has_qtdFuncionarioFilial02;
    } //-- boolean hasQtdFuncionarioFilial02() 

    /**
     * Method hasQtdFuncionarioFilial03
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial03()
    {
        return this._has_qtdFuncionarioFilial03;
    } //-- boolean hasQtdFuncionarioFilial03() 

    /**
     * Method hasQtdFuncionarioFilial04
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial04()
    {
        return this._has_qtdFuncionarioFilial04;
    } //-- boolean hasQtdFuncionarioFilial04() 

    /**
     * Method hasQtdFuncionarioFilial05
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial05()
    {
        return this._has_qtdFuncionarioFilial05;
    } //-- boolean hasQtdFuncionarioFilial05() 

    /**
     * Method hasQtdFuncionarioFilial06
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial06()
    {
        return this._has_qtdFuncionarioFilial06;
    } //-- boolean hasQtdFuncionarioFilial06() 

    /**
     * Method hasQtdFuncionarioFilial07
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial07()
    {
        return this._has_qtdFuncionarioFilial07;
    } //-- boolean hasQtdFuncionarioFilial07() 

    /**
     * Method hasQtdFuncionarioFilial08
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial08()
    {
        return this._has_qtdFuncionarioFilial08;
    } //-- boolean hasQtdFuncionarioFilial08() 

    /**
     * Method hasQtdFuncionarioFilial09
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial09()
    {
        return this._has_qtdFuncionarioFilial09;
    } //-- boolean hasQtdFuncionarioFilial09() 

    /**
     * Method hasQtdFuncionarioFilial10
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdFuncionarioFilial10()
    {
        return this._has_qtdFuncionarioFilial10;
    } //-- boolean hasQtdFuncionarioFilial10() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'nSeqFaixaSalarial'.
     * 
     * @param nSeqFaixaSalarial the value of field
     * 'nSeqFaixaSalarial'.
     */
    public void setNSeqFaixaSalarial(int nSeqFaixaSalarial)
    {
        this._nSeqFaixaSalarial = nSeqFaixaSalarial;
        this._has_nSeqFaixaSalarial = true;
    } //-- void setNSeqFaixaSalarial(int) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial01'.
     * 
     * @param qtdFuncionarioFilial01 the value of field
     * 'qtdFuncionarioFilial01'.
     */
    public void setQtdFuncionarioFilial01(long qtdFuncionarioFilial01)
    {
        this._qtdFuncionarioFilial01 = qtdFuncionarioFilial01;
        this._has_qtdFuncionarioFilial01 = true;
    } //-- void setQtdFuncionarioFilial01(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial02'.
     * 
     * @param qtdFuncionarioFilial02 the value of field
     * 'qtdFuncionarioFilial02'.
     */
    public void setQtdFuncionarioFilial02(long qtdFuncionarioFilial02)
    {
        this._qtdFuncionarioFilial02 = qtdFuncionarioFilial02;
        this._has_qtdFuncionarioFilial02 = true;
    } //-- void setQtdFuncionarioFilial02(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial03'.
     * 
     * @param qtdFuncionarioFilial03 the value of field
     * 'qtdFuncionarioFilial03'.
     */
    public void setQtdFuncionarioFilial03(long qtdFuncionarioFilial03)
    {
        this._qtdFuncionarioFilial03 = qtdFuncionarioFilial03;
        this._has_qtdFuncionarioFilial03 = true;
    } //-- void setQtdFuncionarioFilial03(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial04'.
     * 
     * @param qtdFuncionarioFilial04 the value of field
     * 'qtdFuncionarioFilial04'.
     */
    public void setQtdFuncionarioFilial04(long qtdFuncionarioFilial04)
    {
        this._qtdFuncionarioFilial04 = qtdFuncionarioFilial04;
        this._has_qtdFuncionarioFilial04 = true;
    } //-- void setQtdFuncionarioFilial04(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial05'.
     * 
     * @param qtdFuncionarioFilial05 the value of field
     * 'qtdFuncionarioFilial05'.
     */
    public void setQtdFuncionarioFilial05(long qtdFuncionarioFilial05)
    {
        this._qtdFuncionarioFilial05 = qtdFuncionarioFilial05;
        this._has_qtdFuncionarioFilial05 = true;
    } //-- void setQtdFuncionarioFilial05(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial06'.
     * 
     * @param qtdFuncionarioFilial06 the value of field
     * 'qtdFuncionarioFilial06'.
     */
    public void setQtdFuncionarioFilial06(long qtdFuncionarioFilial06)
    {
        this._qtdFuncionarioFilial06 = qtdFuncionarioFilial06;
        this._has_qtdFuncionarioFilial06 = true;
    } //-- void setQtdFuncionarioFilial06(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial07'.
     * 
     * @param qtdFuncionarioFilial07 the value of field
     * 'qtdFuncionarioFilial07'.
     */
    public void setQtdFuncionarioFilial07(long qtdFuncionarioFilial07)
    {
        this._qtdFuncionarioFilial07 = qtdFuncionarioFilial07;
        this._has_qtdFuncionarioFilial07 = true;
    } //-- void setQtdFuncionarioFilial07(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial08'.
     * 
     * @param qtdFuncionarioFilial08 the value of field
     * 'qtdFuncionarioFilial08'.
     */
    public void setQtdFuncionarioFilial08(long qtdFuncionarioFilial08)
    {
        this._qtdFuncionarioFilial08 = qtdFuncionarioFilial08;
        this._has_qtdFuncionarioFilial08 = true;
    } //-- void setQtdFuncionarioFilial08(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial09'.
     * 
     * @param qtdFuncionarioFilial09 the value of field
     * 'qtdFuncionarioFilial09'.
     */
    public void setQtdFuncionarioFilial09(long qtdFuncionarioFilial09)
    {
        this._qtdFuncionarioFilial09 = qtdFuncionarioFilial09;
        this._has_qtdFuncionarioFilial09 = true;
    } //-- void setQtdFuncionarioFilial09(long) 

    /**
     * Sets the value of field 'qtdFuncionarioFilial10'.
     * 
     * @param qtdFuncionarioFilial10 the value of field
     * 'qtdFuncionarioFilial10'.
     */
    public void setQtdFuncionarioFilial10(long qtdFuncionarioFilial10)
    {
        this._qtdFuncionarioFilial10 = qtdFuncionarioFilial10;
        this._has_qtdFuncionarioFilial10 = true;
    } //-- void setQtdFuncionarioFilial10(long) 

    /**
     * Sets the value of field 'vlFinalFaixaSalarial'.
     * 
     * @param vlFinalFaixaSalarial the value of field
     * 'vlFinalFaixaSalarial'.
     */
    public void setVlFinalFaixaSalarial(java.math.BigDecimal vlFinalFaixaSalarial)
    {
        this._vlFinalFaixaSalarial = vlFinalFaixaSalarial;
    } //-- void setVlFinalFaixaSalarial(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlInicialFaixaSalarial'.
     * 
     * @param vlInicialFaixaSalarial the value of field
     * 'vlInicialFaixaSalarial'.
     */
    public void setVlInicialFaixaSalarial(java.math.BigDecimal vlInicialFaixaSalarial)
    {
        this._vlInicialFaixaSalarial = vlInicialFaixaSalarial;
    } //-- void setVlInicialFaixaSalarial(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate()


	public Ocorrencias(int nSeqFaixaSalarial, BigDecimal vlInicialFaixaSalarial, BigDecimal vlFinalFaixaSalarial,
						long qtdFuncionarioFilial01, long qtdFuncionarioFilial02, long qtdFuncionarioFilial03, 
						long qtdFuncionarioFilial04, long qtdFuncionarioFilial05, long qtdFuncionarioFilial06, 
						long qtdFuncionarioFilial07, long qtdFuncionarioFilial08, long qtdFuncionarioFilial09, 
						long qtdFuncionarioFilial10) {
		super();
		_nSeqFaixaSalarial = nSeqFaixaSalarial;
		_vlInicialFaixaSalarial = vlInicialFaixaSalarial;
		_vlFinalFaixaSalarial = vlFinalFaixaSalarial;
		_qtdFuncionarioFilial01 = qtdFuncionarioFilial01;
		_qtdFuncionarioFilial02 = qtdFuncionarioFilial02;
		_qtdFuncionarioFilial03 = qtdFuncionarioFilial03;
		_qtdFuncionarioFilial04 = qtdFuncionarioFilial04;
		_qtdFuncionarioFilial05 = qtdFuncionarioFilial05;
		_qtdFuncionarioFilial06 = qtdFuncionarioFilial06;
		_qtdFuncionarioFilial07 = qtdFuncionarioFilial07;
		_qtdFuncionarioFilial08 = qtdFuncionarioFilial08;
		_qtdFuncionarioFilial09 = qtdFuncionarioFilial09;
		_qtdFuncionarioFilial10 = qtdFuncionarioFilial10;
	}
    
    

}
