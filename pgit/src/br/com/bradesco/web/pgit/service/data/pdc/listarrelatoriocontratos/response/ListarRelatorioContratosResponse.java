/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarRelatorioContratosResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ListarRelatorioContratosResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _numeroConsultas
     */
    private int _numeroConsultas = 0;

    /**
     * keeps track of state for field: _numeroConsultas
     */
    private boolean _has_numeroConsultas;

    /**
     * Field _ocorenciasList
     */
    private java.util.Vector _ocorenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarRelatorioContratosResponse() 
     {
        super();
        _ocorenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.ListarRelatorioContratosResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorencias
     * 
     * 
     * 
     * @param vOcorencias
     */
    public void addOcorencias(br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias vOcorencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorenciasList.addElement(vOcorencias);
    } //-- void addOcorencias(br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias) 

    /**
     * Method addOcorencias
     * 
     * 
     * 
     * @param index
     * @param vOcorencias
     */
    public void addOcorencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias vOcorencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorenciasList.insertElementAt(vOcorencias, index);
    } //-- void addOcorencias(int, br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias) 

    /**
     * Method deleteNumeroConsultas
     * 
     */
    public void deleteNumeroConsultas()
    {
        this._has_numeroConsultas= false;
    } //-- void deleteNumeroConsultas() 

    /**
     * Method enumerateOcorencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorencias()
    {
        return _ocorenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorencias() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numeroConsultas'.
     * 
     * @return int
     * @return the value of field 'numeroConsultas'.
     */
    public int getNumeroConsultas()
    {
        return this._numeroConsultas;
    } //-- int getNumeroConsultas() 

    /**
     * Method getOcorencias
     * 
     * 
     * 
     * @param index
     * @return Ocorencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias getOcorencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorencias: Index value '"+index+"' not in range [0.."+(_ocorenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias) _ocorenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias getOcorencias(int) 

    /**
     * Method getOcorencias
     * 
     * 
     * 
     * @return Ocorencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias[] getOcorencias()
    {
        int size = _ocorenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias) _ocorenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias[] getOcorencias() 

    /**
     * Method getOcorenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorenciasCount()
    {
        return _ocorenciasList.size();
    } //-- int getOcorenciasCount() 

    /**
     * Method hasNumeroConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroConsultas()
    {
        return this._has_numeroConsultas;
    } //-- boolean hasNumeroConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorencias
     * 
     */
    public void removeAllOcorencias()
    {
        _ocorenciasList.removeAllElements();
    } //-- void removeAllOcorencias() 

    /**
     * Method removeOcorencias
     * 
     * 
     * 
     * @param index
     * @return Ocorencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias removeOcorencias(int index)
    {
        java.lang.Object obj = _ocorenciasList.elementAt(index);
        _ocorenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias removeOcorencias(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numeroConsultas'.
     * 
     * @param numeroConsultas the value of field 'numeroConsultas'.
     */
    public void setNumeroConsultas(int numeroConsultas)
    {
        this._numeroConsultas = numeroConsultas;
        this._has_numeroConsultas = true;
    } //-- void setNumeroConsultas(int) 

    /**
     * Method setOcorencias
     * 
     * 
     * 
     * @param index
     * @param vOcorencias
     */
    public void setOcorencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias vOcorencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorencias: Index value '"+index+"' not in range [0.." + (_ocorenciasList.size() - 1) + "]");
        }
        _ocorenciasList.setElementAt(vOcorencias, index);
    } //-- void setOcorencias(int, br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias) 

    /**
     * Method setOcorencias
     * 
     * 
     * 
     * @param ocorenciasArray
     */
    public void setOcorencias(br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias[] ocorenciasArray)
    {
        //-- copy array
        _ocorenciasList.removeAllElements();
        for (int i = 0; i < ocorenciasArray.length; i++) {
            _ocorenciasList.addElement(ocorenciasArray[i]);
        }
    } //-- void setOcorencias(br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorencias) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarRelatorioContratosResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.ListarRelatorioContratosResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.ListarRelatorioContratosResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.ListarRelatorioContratosResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.ListarRelatorioContratosResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
