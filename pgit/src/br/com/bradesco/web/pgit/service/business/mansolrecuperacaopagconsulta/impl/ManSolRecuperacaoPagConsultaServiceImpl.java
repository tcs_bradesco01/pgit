/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.IManSolRecuperacaoPagConsultaService;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.IManSolRecuperacaoPagConsultaServiceConstants;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarSolRecuperacaoPagtosBackupEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarSolRecuperacaoPagtosBackupSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ExcluirSolRecPagtosVencidoNaoPagoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ExcluirSolRecPagtosVencidoNaoPagoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.bean.SolicitarRecuparacaoPagtosConsultaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.request.ConsultarPagtosSolicitacaoRecuperacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.response.ConsultarPagtosSolicitacaoRecuperacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecuperacaopagtosbackup.request.ConsultarSolRecuperacaoPagtosBackupRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolrecuperacaopagtosbackup.response.ConsultarSolRecuperacaoPagtosBackupResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolrecpagtosvencidonaopago.request.ExcluirSolRecPagtosVencidoNaoPagoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolrecpagtosvencidonaopago.response.ExcluirSolRecPagtosVencidoNaoPagoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosbackupconsulta.request.SolicitarRecPagtosBackupConsultaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrecpagtosbackupconsulta.response.SolicitarRecPagtosBackupConsultaResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManSolRecuperacaoPagConsulta
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManSolRecuperacaoPagConsultaServiceImpl implements IManSolRecuperacaoPagConsultaService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
    /**
     * Construtor.
     */
    public ManSolRecuperacaoPagConsultaServiceImpl() {
    }
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.IManSolRecuperacaoPagConsultaService#consultarPagtosSolicitacaoRecuperacao(br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO)
	 */
	public ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO consultarPagtosSolicitacaoRecuperacao(ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO entradaDTO) {

		ConsultarPagtosSolicitacaoRecuperacaoRequest request = new ConsultarPagtosSolicitacaoRecuperacaoRequest();
		ConsultarPagtosSolicitacaoRecuperacaoResponse response = new ConsultarPagtosSolicitacaoRecuperacaoResponse();
		ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO saidaDTO = new ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO();
		
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
		
		response = getFactoryAdapter().getConsultarPagtosSolicitacaoRecuperacaoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCdPessoaJuridicaContrato(response.getCdPessoaJuridicaContrato());
        saidaDTO.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
        saidaDTO.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
        saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
        saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
        saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
        saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
        saidaDTO.setCdContaDebito(response.getCdContaDebito());
        saidaDTO.setCdDigitoContaDebito(response.getCdDigitoContaDebito());
        saidaDTO.setCdTipoContaDebito(response.getCdTipoContaDebito());
        saidaDTO.setDsContaDebito(response.getDsContaDebito());
        saidaDTO.setDtInicioPeriodoPesquisa(response.getDtInicioPeriodoPesquisa());
        saidaDTO.setDtFimPeriodoPesquisa(response.getDtFimPeriodoPesquisa());
        saidaDTO.setCdProduto(response.getCdProduto());
        saidaDTO.setCdProdutoRelacionado(response.getCdProdutoRelacionado());
        saidaDTO.setNrSequenciaArquivoRemessa(response.getNrSequenciaArquivoRemessa());
        saidaDTO.setCdPessoa(response.getCdPessoa());
        saidaDTO.setDsPessoas(response.getDsPessoas());
        saidaDTO.setCdControlePagamentoInicial(response.getCdControlePagamentoInicial());
        saidaDTO.setCdControlePagamentoFinal(response.getCdControlePagamentoFinal());
        saidaDTO.setVlrInicio(response.getVlrInicio());
        saidaDTO.setVlFim(response.getVlFim());
        saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
        saidaDTO.setCdMotivoSituacaoPagamento(response.getCdMotivoSituacaoPagamento());
        saidaDTO.setLinhaDigitavel(response.getLinhaDigitavel());
        saidaDTO.setCdFavorecidoCliente(response.getCdFavorecidoCliente() == 0L ? "" : String.valueOf(response.getCdFavorecidoCliente()));
        saidaDTO.setCdTipoInscricaoRecebedor(response.getCdTipoInscricaoRecebedor());
        saidaDTO.setCdInscricaoRecebedor(response.getCdInscricaoRecebedor());
        saidaDTO.setCdCpfCnpjRecebedor(response.getCdCpfCnpjRecebedor());
        saidaDTO.setCdFilialCnpjRecebedor(response.getCdFilialCnpjRecebedor());
        saidaDTO.setCdControleCpfRecebedor(response.getCdControleCpfRecebedor());
        saidaDTO.setCdInscricaoFavorecido(response.getCdInscricaoFavorecido());
        saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
        saidaDTO.setDsBancoCredito(response.getDsBancoCredito());
        saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
        saidaDTO.setDsAgenciaCredito(response.getDsAgenciaCredito());
        saidaDTO.setCdContaCredito(response.getCdContaCredito());
        saidaDTO.setDigitoContaCredito(response.getDigitoContaCredito());
        saidaDTO.setCdTipoContaCredito(response.getCdTipoContaCredito());
        saidaDTO.setDsContaCredito(response.getDsContaCredito());
        saidaDTO.setDtRecuperacaoPagamento(response.getDtRecuperacaoPagamento());
        saidaDTO.setDtNovoVencimentoPagamento(response.getDtNovoVencimentoPagamento());
        saidaDTO.setVlTarifaPadrao(response.getVlTarifaPadrao());
        saidaDTO.setVlTarifaNegociacao(response.getVlTarifaNegociacao());
        saidaDTO.setPercentualBonificacaoTarifa(response.getPercentualBonificacaoTarifa());
        saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
        saidaDTO.setDtInclusao(response.getDtInclusao());
        saidaDTO.setHrInclusao(response.getHrInclusao());
        saidaDTO.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao());
        saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
        saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
        saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
        saidaDTO.setDtManutencao(response.getDtManutencao());
        saidaDTO.setHrManutencao(response.getHrManutencao());
        saidaDTO.setCdOperacaoCanalManutencao(response.getCdOperacaoCanalManutencao());
        saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
        saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
        saidaDTO.setCdAcaoManutencao(response.getCdAcaoManutencao());
        saidaDTO.setDsMotivoSituacaoPagamento(response.getDsMotivoSituacaoPagamento());
        saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
        saidaDTO.setDsSituacaoPagamento(response.getDsSituacaoPagamento());
        saidaDTO.setCpfCnpjFormatado(saidaDTO.getCdCpfCnpjRecebedor() == 0L && saidaDTO.getCdFilialCnpjRecebedor() == 0L && saidaDTO.getCdControleCpfRecebedor() == 0 ? "" :
        		CpfCnpjUtils.formatCpfCnpjCompleto(saidaDTO.getCdCpfCnpjRecebedor(), saidaDTO.getCdFilialCnpjRecebedor(), saidaDTO.getCdControleCpfRecebedor()));
        
        saidaDTO.setCdDigitoAgenciaDebito(response.getCdDigitoAgenciaDebito());
        saidaDTO.setCdDigitoAgenciaCredito(response.getCdDigitoAgenciaCredito());
        
        saidaDTO.setBancoSalario(response.getECbcoSalario());
        saidaDTO.setContaSalario(response.getEContaSalario());
        saidaDTO.setAgenciaSalario(response.getEAgenciaSalario());
        saidaDTO.setDigitoSalario(response.getEDigContaSalario());
        saidaDTO.setLoteInterno(response.getELoteInterno());
        saidaDTO.setDestinoPagamento(response.getEDestPgtoRecup());        
        saidaDTO.setBancoPgtoDestino(response.getECbcoPgto());
        saidaDTO.setIspbPgtoDestino(response.getEIspbPgtoDest());
        saidaDTO.setContaPgtoDestino(response.getEContaPgtoDest());
        
       return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.IManSolRecuperacaoPagConsultaService#consultarSolRecuperacaoPagtosBackup(br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarSolRecuperacaoPagtosBackupEntradaDTO)
	 */
	public List<ConsultarSolRecuperacaoPagtosBackupSaidaDTO> consultarSolRecuperacaoPagtosBackup(ConsultarSolRecuperacaoPagtosBackupEntradaDTO entradaDTO) {
		List<ConsultarSolRecuperacaoPagtosBackupSaidaDTO> listaSaida = new ArrayList<ConsultarSolRecuperacaoPagtosBackupSaidaDTO>();
       
		ConsultarSolRecuperacaoPagtosBackupRequest request = new ConsultarSolRecuperacaoPagtosBackupRequest();
		ConsultarSolRecuperacaoPagtosBackupResponse response = new ConsultarSolRecuperacaoPagtosBackupResponse();
        
        request.setNrOcorrencias(IManSolRecuperacaoPagConsultaServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        request.setCdFinalidadeRecuperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFinalidadeRecuperacao()));
        request.setCdSelecaoRecuperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSelecaoRecuperacao()));
        request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamento()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));
        request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));
        request.setCdDigitoAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDigitoAgenciaDebito()));
        request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));
        request.setCdDigitoContaDebito(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoContaDebito()));
        request.setCdTipoContaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContaDebito()));
        request.setDtInicioPesquisa(PgitUtil.verificaStringNula(entradaDTO.getDtInicioPesquisa()));
        request.setDtFimPesquisa(PgitUtil.verificaStringNula(entradaDTO.getDtFimPesquisa()));
        request.setCdProduto(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProduto()));
        request.setCdProdutoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoRelacionado()));
        request.setCdSituacaoSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacaoPagamento()));
        request.setCdMotivoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSolicitacao()));
        request.setENsoltcPgto(PgitUtil.verificaIntegerNulo(entradaDTO.getNmrSoltcPgto()));

        response = getFactoryAdapter().getConsultarSolRecuperacaoPagtosBackupPDCAdapter().invokeProcess(request);
        
        for (int i = 0; i < response.getOcorrencias().length; i++) {
        	ConsultarSolRecuperacaoPagtosBackupSaidaDTO saidaDTO = new ConsultarSolRecuperacaoPagtosBackupSaidaDTO();
        	saidaDTO.setCdSolicitacaoPagamento(response.getOcorrencias(i).getCdSolicitacaoPagamento());
            saidaDTO.setNrSolicitacaoPagamento(response.getOcorrencias(i).getNrSolicitacaoPagamento());
            saidaDTO.setCdCpfCnpjParticipante(response.getOcorrencias(i).getCdCpfCnpjParticipante());
            saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
            saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
            saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            saidaDTO.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
            saidaDTO.setDsDataSolicitacao(FormatarData.formatarDataFromPdc(response.getOcorrencias(i).getDsDataSolicitacao()));
            saidaDTO.setDsHoraSolicitacao(response.getOcorrencias(i).getDsHoraSolicitacao());
            saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
            saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
            saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
            saidaDTO.setDsResumoServicoRelacionado(response.getOcorrencias(i).getDsResumoServicoRelacionado());
            saidaDTO.setCdSituacaoSolicitacaoPagamento(response.getOcorrencias(i).getCdSituacaoSolicitacaoPagamento());
            saidaDTO.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
            saidaDTO.setCdMotivoSolicitacao(response.getOcorrencias(i).getCdMotivoSolicitacao());
            saidaDTO.setDsMotivoSolicitacao(response.getOcorrencias(i).getDsMotivoSolicitacao());
            saidaDTO.setDtRecuperacao(response.getOcorrencias(i).getDtRecuperacao());
            listaSaida.add(saidaDTO);
		}
		
		return listaSaida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.IManSolRecuperacaoPagConsultaService#excluirSolRecPagtosVencidoNaoPago(br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ExcluirSolRecPagtosVencidoNaoPagoEntradaDTO)
	 */
	public ExcluirSolRecPagtosVencidoNaoPagoSaidaDTO excluirSolRecPagtosVencidoNaoPago(ExcluirSolRecPagtosVencidoNaoPagoEntradaDTO entradaDTO) {
		ExcluirSolRecPagtosVencidoNaoPagoRequest request = new ExcluirSolRecPagtosVencidoNaoPagoRequest();
		ExcluirSolRecPagtosVencidoNaoPagoResponse response = new ExcluirSolRecPagtosVencidoNaoPagoResponse();
		ExcluirSolRecPagtosVencidoNaoPagoSaidaDTO saidaDTO = new ExcluirSolRecPagtosVencidoNaoPagoSaidaDTO();
		
		request.setCdSolicitacaoPagamento(entradaDTO.getCdSolicitacaoPagamento());
		request.setNrSolicitacaoPagamento(entradaDTO.getNrSolicitacaoPagamento());
		
		response = getFactoryAdapter().getExcluirSolRecPagtosVencidoNaoPagoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
}