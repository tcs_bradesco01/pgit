package br.com.bradesco.web.pgit.service.business.combo.bean;


/**
 * Arquivo criado em 06/11/15.
 */
public class ListarTipoProcessamentoLayoutArquivoEntradaDTO {

    private Long cdpessoaJuridicaContrato;

    private Integer cdTipoContratoNegocio;

    private Long nrSequenciaContratoNegocio;

    private Integer cdProdutoServicoOperacao;

    private Integer cdProdutoOperacaoRelacionado;

    private Integer cdRelacionamentoProduto;
    
	private int cdServico;
	
	private int cdModalidade;
	
	private int cdParametroPesquisa;
	
	private int cdParametroTela;
	
	

    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }

    public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
        this.cdRelacionamentoProduto = cdRelacionamentoProduto;
    }

    public Integer getCdRelacionamentoProduto() {
        return this.cdRelacionamentoProduto;
    }

	public int getCdServico() {
		return cdServico;
	}

	public void setCdServico(int cdServico) {
		this.cdServico = cdServico;
	}

	public int getCdModalidade() {
		return cdModalidade;
	}

	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	public int getCdParametroPesquisa() {
		return cdParametroPesquisa;
	}

	public void setCdParametroPesquisa(int cdParametroPesquisa) {
		this.cdParametroPesquisa = cdParametroPesquisa;
	}

	public int getCdParametroTela() {
		return cdParametroTela;
	}

	public void setCdParametroTela(int cdParametroTela) {
		this.cdParametroTela = cdParametroTela;
	}
}