/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterassocservmodalidade.bean;

/**
 * Nome: ManterAssocServModalidadeDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterAssocServModalidadeDTO {
	
	
	/** Atributo tipoServico. */
	private String tipoServico;
	
	/** Atributo modalidadeServico. */
	private String modalidadeServico;
	
	/** Atributo tipoLote. */
	private String tipoLote;
	
	/** Atributo tipoLayoutArquivo. */
	private String tipoLayoutArquivo;
	
	
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public String getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(String modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public String getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}
	
	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(String tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}
	
	/**
	 * Get: tipoLote.
	 *
	 * @return tipoLote
	 */
	public String getTipoLote() {
		return tipoLote;
	}
	
	/**
	 * Set: tipoLote.
	 *
	 * @param tipoLote the tipo lote
	 */
	public void setTipoLote(String tipoLote) {
		this.tipoLote = tipoLote;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
	

}
