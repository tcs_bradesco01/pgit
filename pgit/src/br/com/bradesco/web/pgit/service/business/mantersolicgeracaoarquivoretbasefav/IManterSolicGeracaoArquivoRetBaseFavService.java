/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.DetalharSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.DetalharSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ExcluirSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ExcluirSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.IncluirSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.IncluirSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ListarSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ListarSolBaseFavorecidoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterSolicGeracaoArquivoRetBaseFav
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterSolicGeracaoArquivoRetBaseFavService {
    
    /**
     * Listar sol base favorecido.
     *
     * @param entradaDTO the entrada dto
     * @return the list< listar sol base favorecido saida dt o>
     */
    List<ListarSolBaseFavorecidoSaidaDTO> listarSolBaseFavorecido (ListarSolBaseFavorecidoEntradaDTO entradaDTO);
    
    /**
     * Detalhar sol base favorecido.
     *
     * @param entradaDTO the entrada dto
     * @return the detalhar sol base favorecido saida dto
     */
    DetalharSolBaseFavorecidoSaidaDTO detalharSolBaseFavorecido(DetalharSolBaseFavorecidoEntradaDTO entradaDTO);
    
    /**
     * Incluir sol base favorecido.
     *
     * @param entradaDTO the entrada dto
     * @return the incluir sol base favorecido saida dto
     */
    IncluirSolBaseFavorecidoSaidaDTO incluirSolBaseFavorecido(IncluirSolBaseFavorecidoEntradaDTO entradaDTO);
    
    /**
     * Excluir sol base favorecido.
     *
     * @param entradaDTO the entrada dto
     * @return the excluir sol base favorecido saida dto
     */
    ExcluirSolBaseFavorecidoSaidaDTO excluirSolBaseFavorecido(ExcluirSolBaseFavorecidoEntradaDTO entradaDTO);
}

