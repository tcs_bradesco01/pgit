/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarConManLayoutSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConManLayoutSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
    /** Atributo dsTipoLayoutArquivo. */
    private String dsTipoLayoutArquivo;
    
    /** Atributo cdSituacaoLayoutContrato. */
    private Integer cdSituacaoLayoutContrato;
    
    /** Atributo dsSituacaoLayoutContrato. */
    private String dsSituacaoLayoutContrato;
    
    /** Atributo cdResponsavelCustoEmpresa. */
    private Integer cdResponsavelCustoEmpresa;
    
    /** Atributo dsResponsavelCustoEmpresa. */
    private String dsResponsavelCustoEmpresa;
    
    /** Atributo percentualCustoOrganizacaoTransmissao. */
    private BigDecimal percentualCustoOrganizacaoTransmissao;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExter. */
    private String cdUsuarioInclusaoExter;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExter. */
    private String cdUsuarioManutencaoExter;
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo cdMsgLinExtrt. */
    private Integer cdMsgLinExtrt;
    
    /** Atributo cdTipoControlePagamento. */
    private Integer cdTipoControlePagamento; 
    
    /** Atributo dsTipoControlePagamento. */
    private String dsTipoControlePagamento;
    
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdResponsavelCustoEmpresa.
	 *
	 * @return cdResponsavelCustoEmpresa
	 */
	public Integer getCdResponsavelCustoEmpresa() {
		return cdResponsavelCustoEmpresa;
	}
	
	/**
	 * Set: cdResponsavelCustoEmpresa.
	 *
	 * @param cdResponsavelCustoEmpresa the cd responsavel custo empresa
	 */
	public void setCdResponsavelCustoEmpresa(Integer cdResponsavelCustoEmpresa) {
		this.cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
	}
	
	/**
	 * Get: cdSituacaoLayoutContrato.
	 *
	 * @return cdSituacaoLayoutContrato
	 */
	public Integer getCdSituacaoLayoutContrato() {
		return cdSituacaoLayoutContrato;
	}
	
	/**
	 * Set: cdSituacaoLayoutContrato.
	 *
	 * @param cdSituacaoLayoutContrato the cd situacao layout contrato
	 */
	public void setCdSituacaoLayoutContrato(Integer cdSituacaoLayoutContrato) {
		this.cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
	}
	
	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}
	
	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExter.
	 *
	 * @return cdUsuarioInclusaoExter
	 */
	public String getCdUsuarioInclusaoExter() {
		return cdUsuarioInclusaoExter;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExter.
	 *
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 */
	public void setCdUsuarioInclusaoExter(String cdUsuarioInclusaoExter) {
		this.cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExter.
	 *
	 * @return cdUsuarioManutencaoExter
	 */
	public String getCdUsuarioManutencaoExter() {
		return cdUsuarioManutencaoExter;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExter.
	 *
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 */
	public void setCdUsuarioManutencaoExter(String cdUsuarioManutencaoExter) {
		this.cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsResponsavelCustoEmpresa.
	 *
	 * @return dsResponsavelCustoEmpresa
	 */
	public String getDsResponsavelCustoEmpresa() {
		return dsResponsavelCustoEmpresa;
	}
	
	/**
	 * Set: dsResponsavelCustoEmpresa.
	 *
	 * @param dsResponsavelCustoEmpresa the ds responsavel custo empresa
	 */
	public void setDsResponsavelCustoEmpresa(String dsResponsavelCustoEmpresa) {
		this.dsResponsavelCustoEmpresa = dsResponsavelCustoEmpresa;
	}
	
	/**
	 * Get: dsSituacaoLayoutContrato.
	 *
	 * @return dsSituacaoLayoutContrato
	 */
	public String getDsSituacaoLayoutContrato() {
		return dsSituacaoLayoutContrato;
	}
	
	/**
	 * Set: dsSituacaoLayoutContrato.
	 *
	 * @param dsSituacaoLayoutContrato the ds situacao layout contrato
	 */
	public void setDsSituacaoLayoutContrato(String dsSituacaoLayoutContrato) {
		this.dsSituacaoLayoutContrato = dsSituacaoLayoutContrato;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: percentualCustoOrganizacaoTransmissao.
	 *
	 * @return percentualCustoOrganizacaoTransmissao
	 */
	public BigDecimal getPercentualCustoOrganizacaoTransmissao() {
		return percentualCustoOrganizacaoTransmissao;
	}
	
	/**
	 * Set: percentualCustoOrganizacaoTransmissao.
	 *
	 * @param percentualCustoOrganizacaoTransmissao the percentual custo organizacao transmissao
	 */
	public void setPercentualCustoOrganizacaoTransmissao(
			BigDecimal percentualCustoOrganizacaoTransmissao) {
		this.percentualCustoOrganizacaoTransmissao = percentualCustoOrganizacaoTransmissao;
	}
	
	/**
	 * Get: cdMsgLinExtrt.
	 *
	 * @return cdMsgLinExtrt
	 */
	public Integer getCdMsgLinExtrt() {
		return cdMsgLinExtrt;
	}
	
	/**
	 * Set: cdMsgLinExtrt.
	 *
	 * @param cdMsgLinExtrt the cd msg lin extrt
	 */
	public void setCdMsgLinExtrt(Integer cdMsgLinExtrt) {
		this.cdMsgLinExtrt = cdMsgLinExtrt;
	}

	public Integer getCdTipoControlePagamento() {
		return cdTipoControlePagamento;
	}

	public void setCdTipoControlePagamento(Integer cdTipoControlePagamento) {
		this.cdTipoControlePagamento = cdTipoControlePagamento;
	}

	public String getDsTipoControlePagamento() {
		return dsTipoControlePagamento;
	}

	public void setDsTipoControlePagamento(String dsTipoControlePagamento) {
		this.dsTipoControlePagamento = dsTipoControlePagamento;
	}  
	
}