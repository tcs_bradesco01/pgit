/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharContratoMigradoHsbcResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharContratoMigradoHsbcResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdCpfCnpjPagador
     */
    private long _cdCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPagador
     */
    private boolean _has_cdCpfCnpjPagador;

    /**
     * Field _cdFilialCpfCnpjPagador
     */
    private int _cdFilialCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjPagador
     */
    private boolean _has_cdFilialCpfCnpjPagador;

    /**
     * Field _cdControleCpfCnpjPagador
     */
    private int _cdControleCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpjPagador
     */
    private boolean _has_cdControleCpfCnpjPagador;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgenciaBancaria
     */
    private int _cdAgenciaBancaria = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancaria
     */
    private boolean _has_cdAgenciaBancaria;

    /**
     * Field _cdContaBancaria
     */
    private long _cdContaBancaria = 0;

    /**
     * keeps track of state for field: _cdContaBancaria
     */
    private boolean _has_cdContaBancaria;

    /**
     * Field _cdDigitoContaBancaria
     */
    private java.lang.String _cdDigitoContaBancaria;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharContratoMigradoHsbcResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.response.DetalharContratoMigradoHsbcResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaBancaria
     * 
     */
    public void deleteCdAgenciaBancaria()
    {
        this._has_cdAgenciaBancaria= false;
    } //-- void deleteCdAgenciaBancaria() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdContaBancaria
     * 
     */
    public void deleteCdContaBancaria()
    {
        this._has_cdContaBancaria= false;
    } //-- void deleteCdContaBancaria() 

    /**
     * Method deleteCdControleCpfCnpjPagador
     * 
     */
    public void deleteCdControleCpfCnpjPagador()
    {
        this._has_cdControleCpfCnpjPagador= false;
    } //-- void deleteCdControleCpfCnpjPagador() 

    /**
     * Method deleteCdCpfCnpjPagador
     * 
     */
    public void deleteCdCpfCnpjPagador()
    {
        this._has_cdCpfCnpjPagador= false;
    } //-- void deleteCdCpfCnpjPagador() 

    /**
     * Method deleteCdFilialCpfCnpjPagador
     * 
     */
    public void deleteCdFilialCpfCnpjPagador()
    {
        this._has_cdFilialCpfCnpjPagador= false;
    } //-- void deleteCdFilialCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdAgenciaBancaria'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancaria'.
     */
    public int getCdAgenciaBancaria()
    {
        return this._cdAgenciaBancaria;
    } //-- int getCdAgenciaBancaria() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdContaBancaria'.
     * 
     * @return long
     * @return the value of field 'cdContaBancaria'.
     */
    public long getCdContaBancaria()
    {
        return this._cdContaBancaria;
    } //-- long getCdContaBancaria() 

    /**
     * Returns the value of field 'cdControleCpfCnpjPagador'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpjPagador'.
     */
    public int getCdControleCpfCnpjPagador()
    {
        return this._cdControleCpfCnpjPagador;
    } //-- int getCdControleCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdCpfCnpjPagador'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPagador'.
     */
    public long getCdCpfCnpjPagador()
    {
        return this._cdCpfCnpjPagador;
    } //-- long getCdCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdDigitoContaBancaria'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaBancaria'.
     */
    public java.lang.String getCdDigitoContaBancaria()
    {
        return this._cdDigitoContaBancaria;
    } //-- java.lang.String getCdDigitoContaBancaria() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjPagador'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjPagador'.
     */
    public int getCdFilialCpfCnpjPagador()
    {
        return this._cdFilialCpfCnpjPagador;
    } //-- int getCdFilialCpfCnpjPagador() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdAgenciaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancaria()
    {
        return this._has_cdAgenciaBancaria;
    } //-- boolean hasCdAgenciaBancaria() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdContaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancaria()
    {
        return this._has_cdContaBancaria;
    } //-- boolean hasCdContaBancaria() 

    /**
     * Method hasCdControleCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpjPagador()
    {
        return this._has_cdControleCpfCnpjPagador;
    } //-- boolean hasCdControleCpfCnpjPagador() 

    /**
     * Method hasCdCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPagador()
    {
        return this._has_cdCpfCnpjPagador;
    } //-- boolean hasCdCpfCnpjPagador() 

    /**
     * Method hasCdFilialCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjPagador()
    {
        return this._has_cdFilialCpfCnpjPagador;
    } //-- boolean hasCdFilialCpfCnpjPagador() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaBancaria'.
     * 
     * @param cdAgenciaBancaria the value of field
     * 'cdAgenciaBancaria'.
     */
    public void setCdAgenciaBancaria(int cdAgenciaBancaria)
    {
        this._cdAgenciaBancaria = cdAgenciaBancaria;
        this._has_cdAgenciaBancaria = true;
    } //-- void setCdAgenciaBancaria(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdContaBancaria'.
     * 
     * @param cdContaBancaria the value of field 'cdContaBancaria'.
     */
    public void setCdContaBancaria(long cdContaBancaria)
    {
        this._cdContaBancaria = cdContaBancaria;
        this._has_cdContaBancaria = true;
    } //-- void setCdContaBancaria(long) 

    /**
     * Sets the value of field 'cdControleCpfCnpjPagador'.
     * 
     * @param cdControleCpfCnpjPagador the value of field
     * 'cdControleCpfCnpjPagador'.
     */
    public void setCdControleCpfCnpjPagador(int cdControleCpfCnpjPagador)
    {
        this._cdControleCpfCnpjPagador = cdControleCpfCnpjPagador;
        this._has_cdControleCpfCnpjPagador = true;
    } //-- void setCdControleCpfCnpjPagador(int) 

    /**
     * Sets the value of field 'cdCpfCnpjPagador'.
     * 
     * @param cdCpfCnpjPagador the value of field 'cdCpfCnpjPagador'
     */
    public void setCdCpfCnpjPagador(long cdCpfCnpjPagador)
    {
        this._cdCpfCnpjPagador = cdCpfCnpjPagador;
        this._has_cdCpfCnpjPagador = true;
    } //-- void setCdCpfCnpjPagador(long) 

    /**
     * Sets the value of field 'cdDigitoContaBancaria'.
     * 
     * @param cdDigitoContaBancaria the value of field
     * 'cdDigitoContaBancaria'.
     */
    public void setCdDigitoContaBancaria(java.lang.String cdDigitoContaBancaria)
    {
        this._cdDigitoContaBancaria = cdDigitoContaBancaria;
    } //-- void setCdDigitoContaBancaria(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjPagador'.
     * 
     * @param cdFilialCpfCnpjPagador the value of field
     * 'cdFilialCpfCnpjPagador'.
     */
    public void setCdFilialCpfCnpjPagador(int cdFilialCpfCnpjPagador)
    {
        this._cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
        this._has_cdFilialCpfCnpjPagador = true;
    } //-- void setCdFilialCpfCnpjPagador(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharContratoMigradoHsbcResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.response.DetalharContratoMigradoHsbcResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.response.DetalharContratoMigradoHsbcResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.response.DetalharContratoMigradoHsbcResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.response.DetalharContratoMigradoHsbcResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
