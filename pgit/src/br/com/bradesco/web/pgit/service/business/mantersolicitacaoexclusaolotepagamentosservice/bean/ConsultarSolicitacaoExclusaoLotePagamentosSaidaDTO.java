package br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean;

import java.util.List;

public class ConsultarSolicitacaoExclusaoLotePagamentosSaidaDTO {

	private String codMensagem;
	private String mensagem;
	private Integer numeroLinhas;
	private List<OcorrenciasSolicitacaoExclusaoLotePagamentosDTO> ocorrencias;

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	public List<OcorrenciasSolicitacaoExclusaoLotePagamentosDTO> getOcorrencias() {
		return ocorrencias;
	}

	public void setOcorrencias(
			List<OcorrenciasSolicitacaoExclusaoLotePagamentosDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

}
