/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetalhesSolicitacaoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalhesSolicitacaoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdBancoEstorno
     */
    private int _cdBancoEstorno = 0;

    /**
     * keeps track of state for field: _cdBancoEstorno
     */
    private boolean _has_cdBancoEstorno;

    /**
     * Field _dsBancoEstorno
     */
    private java.lang.String _dsBancoEstorno;

    /**
     * Field _cdAgenciaEstorno
     */
    private int _cdAgenciaEstorno = 0;

    /**
     * keeps track of state for field: _cdAgenciaEstorno
     */
    private boolean _has_cdAgenciaEstorno;

    /**
     * Field _cdDigitoAgenciaEstorno
     */
    private int _cdDigitoAgenciaEstorno = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaEstorno
     */
    private boolean _has_cdDigitoAgenciaEstorno;

    /**
     * Field _dsAgenciaEstorno
     */
    private java.lang.String _dsAgenciaEstorno;

    /**
     * Field _cdContaEstorno
     */
    private long _cdContaEstorno = 0;

    /**
     * keeps track of state for field: _cdContaEstorno
     */
    private boolean _has_cdContaEstorno;

    /**
     * Field _cdDigitoContaEstorno
     */
    private java.lang.String _cdDigitoContaEstorno;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;

    /**
     * Field _situacaoSolicitacao
     */
    private java.lang.String _situacaoSolicitacao;

    /**
     * Field _cdMotivoSolicitacao
     */
    private java.lang.String _cdMotivoSolicitacao;

    /**
     * Field _dsObservacao
     */
    private java.lang.String _dsObservacao;

    /**
     * Field _dsDataSolicitacao
     */
    private java.lang.String _dsDataSolicitacao;

    /**
     * Field _dsHoraSolicitacao
     */
    private java.lang.String _dsHoraSolicitacao;

    /**
     * Field _dtAutorizacao
     */
    private java.lang.String _dtAutorizacao;

    /**
     * Field _hrAutorizacao
     */
    private java.lang.String _hrAutorizacao;

    /**
     * Field _cdUsuarioAutorizacao
     */
    private java.lang.String _cdUsuarioAutorizacao;

    /**
     * Field _dtPrevistaEstorno
     */
    private java.lang.String _dtPrevistaEstorno;

    /**
     * Field _cdValorPrevistoEstorno
     */
    private java.math.BigDecimal _cdValorPrevistoEstorno = new java.math.BigDecimal("0");

    /**
     * Field _cdFormaEstrnPagamento
     */
    private int _cdFormaEstrnPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaEstrnPagamento
     */
    private boolean _has_cdFormaEstrnPagamento;

    /**
     * Field _dsFormaEstrnPagamento
     */
    private java.lang.String _dsFormaEstrnPagamento;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdOperacaoFluxoInclusao
     */
    private java.lang.String _cdOperacaoFluxoInclusao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _cdOperacaoFluxoManutencao
     */
    private java.lang.String _cdOperacaoFluxoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalhesSolicitacaoResponse() 
     {
        super();
        setCdValorPrevistoEstorno(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.response.ConsultarDetalhesSolicitacaoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaEstorno
     * 
     */
    public void deleteCdAgenciaEstorno()
    {
        this._has_cdAgenciaEstorno= false;
    } //-- void deleteCdAgenciaEstorno() 

    /**
     * Method deleteCdBancoEstorno
     * 
     */
    public void deleteCdBancoEstorno()
    {
        this._has_cdBancoEstorno= false;
    } //-- void deleteCdBancoEstorno() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdContaEstorno
     * 
     */
    public void deleteCdContaEstorno()
    {
        this._has_cdContaEstorno= false;
    } //-- void deleteCdContaEstorno() 

    /**
     * Method deleteCdDigitoAgenciaEstorno
     * 
     */
    public void deleteCdDigitoAgenciaEstorno()
    {
        this._has_cdDigitoAgenciaEstorno= false;
    } //-- void deleteCdDigitoAgenciaEstorno() 

    /**
     * Method deleteCdFormaEstrnPagamento
     * 
     */
    public void deleteCdFormaEstrnPagamento()
    {
        this._has_cdFormaEstrnPagamento= false;
    } //-- void deleteCdFormaEstrnPagamento() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdAgenciaEstorno'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaEstorno'.
     */
    public int getCdAgenciaEstorno()
    {
        return this._cdAgenciaEstorno;
    } //-- int getCdAgenciaEstorno() 

    /**
     * Returns the value of field 'cdBancoEstorno'.
     * 
     * @return int
     * @return the value of field 'cdBancoEstorno'.
     */
    public int getCdBancoEstorno()
    {
        return this._cdBancoEstorno;
    } //-- int getCdBancoEstorno() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdContaEstorno'.
     * 
     * @return long
     * @return the value of field 'cdContaEstorno'.
     */
    public long getCdContaEstorno()
    {
        return this._cdContaEstorno;
    } //-- long getCdContaEstorno() 

    /**
     * Returns the value of field 'cdDigitoAgenciaEstorno'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaEstorno'.
     */
    public int getCdDigitoAgenciaEstorno()
    {
        return this._cdDigitoAgenciaEstorno;
    } //-- int getCdDigitoAgenciaEstorno() 

    /**
     * Returns the value of field 'cdDigitoContaEstorno'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaEstorno'.
     */
    public java.lang.String getCdDigitoContaEstorno()
    {
        return this._cdDigitoContaEstorno;
    } //-- java.lang.String getCdDigitoContaEstorno() 

    /**
     * Returns the value of field 'cdFormaEstrnPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaEstrnPagamento'.
     */
    public int getCdFormaEstrnPagamento()
    {
        return this._cdFormaEstrnPagamento;
    } //-- int getCdFormaEstrnPagamento() 

    /**
     * Returns the value of field 'cdMotivoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdMotivoSolicitacao'.
     */
    public java.lang.String getCdMotivoSolicitacao()
    {
        return this._cdMotivoSolicitacao;
    } //-- java.lang.String getCdMotivoSolicitacao() 

    /**
     * Returns the value of field 'cdOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoFluxoInclusao'.
     */
    public java.lang.String getCdOperacaoFluxoInclusao()
    {
        return this._cdOperacaoFluxoInclusao;
    } //-- java.lang.String getCdOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'cdOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoFluxoManutencao'.
     */
    public java.lang.String getCdOperacaoFluxoManutencao()
    {
        return this._cdOperacaoFluxoManutencao;
    } //-- java.lang.String getCdOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdUsuarioAutorizacao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioAutorizacao'.
     */
    public java.lang.String getCdUsuarioAutorizacao()
    {
        return this._cdUsuarioAutorizacao;
    } //-- java.lang.String getCdUsuarioAutorizacao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdValorPrevistoEstorno'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdValorPrevistoEstorno'.
     */
    public java.math.BigDecimal getCdValorPrevistoEstorno()
    {
        return this._cdValorPrevistoEstorno;
    } //-- java.math.BigDecimal getCdValorPrevistoEstorno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaEstorno'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaEstorno'.
     */
    public java.lang.String getDsAgenciaEstorno()
    {
        return this._dsAgenciaEstorno;
    } //-- java.lang.String getDsAgenciaEstorno() 

    /**
     * Returns the value of field 'dsBancoEstorno'.
     * 
     * @return String
     * @return the value of field 'dsBancoEstorno'.
     */
    public java.lang.String getDsBancoEstorno()
    {
        return this._dsBancoEstorno;
    } //-- java.lang.String getDsBancoEstorno() 

    /**
     * Returns the value of field 'dsDataSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsDataSolicitacao'.
     */
    public java.lang.String getDsDataSolicitacao()
    {
        return this._dsDataSolicitacao;
    } //-- java.lang.String getDsDataSolicitacao() 

    /**
     * Returns the value of field 'dsFormaEstrnPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaEstrnPagamento'.
     */
    public java.lang.String getDsFormaEstrnPagamento()
    {
        return this._dsFormaEstrnPagamento;
    } //-- java.lang.String getDsFormaEstrnPagamento() 

    /**
     * Returns the value of field 'dsHoraSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsHoraSolicitacao'.
     */
    public java.lang.String getDsHoraSolicitacao()
    {
        return this._dsHoraSolicitacao;
    } //-- java.lang.String getDsHoraSolicitacao() 

    /**
     * Returns the value of field 'dsObservacao'.
     * 
     * @return String
     * @return the value of field 'dsObservacao'.
     */
    public java.lang.String getDsObservacao()
    {
        return this._dsObservacao;
    } //-- java.lang.String getDsObservacao() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'dtAutorizacao'.
     * 
     * @return String
     * @return the value of field 'dtAutorizacao'.
     */
    public java.lang.String getDtAutorizacao()
    {
        return this._dtAutorizacao;
    } //-- java.lang.String getDtAutorizacao() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtPrevistaEstorno'.
     * 
     * @return String
     * @return the value of field 'dtPrevistaEstorno'.
     */
    public java.lang.String getDtPrevistaEstorno()
    {
        return this._dtPrevistaEstorno;
    } //-- java.lang.String getDtPrevistaEstorno() 

    /**
     * Returns the value of field 'hrAutorizacao'.
     * 
     * @return String
     * @return the value of field 'hrAutorizacao'.
     */
    public java.lang.String getHrAutorizacao()
    {
        return this._hrAutorizacao;
    } //-- java.lang.String getHrAutorizacao() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'situacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'situacaoSolicitacao'.
     */
    public java.lang.String getSituacaoSolicitacao()
    {
        return this._situacaoSolicitacao;
    } //-- java.lang.String getSituacaoSolicitacao() 

    /**
     * Method hasCdAgenciaEstorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaEstorno()
    {
        return this._has_cdAgenciaEstorno;
    } //-- boolean hasCdAgenciaEstorno() 

    /**
     * Method hasCdBancoEstorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoEstorno()
    {
        return this._has_cdBancoEstorno;
    } //-- boolean hasCdBancoEstorno() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdContaEstorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaEstorno()
    {
        return this._has_cdContaEstorno;
    } //-- boolean hasCdContaEstorno() 

    /**
     * Method hasCdDigitoAgenciaEstorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaEstorno()
    {
        return this._has_cdDigitoAgenciaEstorno;
    } //-- boolean hasCdDigitoAgenciaEstorno() 

    /**
     * Method hasCdFormaEstrnPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEstrnPagamento()
    {
        return this._has_cdFormaEstrnPagamento;
    } //-- boolean hasCdFormaEstrnPagamento() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaEstorno'.
     * 
     * @param cdAgenciaEstorno the value of field 'cdAgenciaEstorno'
     */
    public void setCdAgenciaEstorno(int cdAgenciaEstorno)
    {
        this._cdAgenciaEstorno = cdAgenciaEstorno;
        this._has_cdAgenciaEstorno = true;
    } //-- void setCdAgenciaEstorno(int) 

    /**
     * Sets the value of field 'cdBancoEstorno'.
     * 
     * @param cdBancoEstorno the value of field 'cdBancoEstorno'.
     */
    public void setCdBancoEstorno(int cdBancoEstorno)
    {
        this._cdBancoEstorno = cdBancoEstorno;
        this._has_cdBancoEstorno = true;
    } //-- void setCdBancoEstorno(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdContaEstorno'.
     * 
     * @param cdContaEstorno the value of field 'cdContaEstorno'.
     */
    public void setCdContaEstorno(long cdContaEstorno)
    {
        this._cdContaEstorno = cdContaEstorno;
        this._has_cdContaEstorno = true;
    } //-- void setCdContaEstorno(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaEstorno'.
     * 
     * @param cdDigitoAgenciaEstorno the value of field
     * 'cdDigitoAgenciaEstorno'.
     */
    public void setCdDigitoAgenciaEstorno(int cdDigitoAgenciaEstorno)
    {
        this._cdDigitoAgenciaEstorno = cdDigitoAgenciaEstorno;
        this._has_cdDigitoAgenciaEstorno = true;
    } //-- void setCdDigitoAgenciaEstorno(int) 

    /**
     * Sets the value of field 'cdDigitoContaEstorno'.
     * 
     * @param cdDigitoContaEstorno the value of field
     * 'cdDigitoContaEstorno'.
     */
    public void setCdDigitoContaEstorno(java.lang.String cdDigitoContaEstorno)
    {
        this._cdDigitoContaEstorno = cdDigitoContaEstorno;
    } //-- void setCdDigitoContaEstorno(java.lang.String) 

    /**
     * Sets the value of field 'cdFormaEstrnPagamento'.
     * 
     * @param cdFormaEstrnPagamento the value of field
     * 'cdFormaEstrnPagamento'.
     */
    public void setCdFormaEstrnPagamento(int cdFormaEstrnPagamento)
    {
        this._cdFormaEstrnPagamento = cdFormaEstrnPagamento;
        this._has_cdFormaEstrnPagamento = true;
    } //-- void setCdFormaEstrnPagamento(int) 

    /**
     * Sets the value of field 'cdMotivoSolicitacao'.
     * 
     * @param cdMotivoSolicitacao the value of field
     * 'cdMotivoSolicitacao'.
     */
    public void setCdMotivoSolicitacao(java.lang.String cdMotivoSolicitacao)
    {
        this._cdMotivoSolicitacao = cdMotivoSolicitacao;
    } //-- void setCdMotivoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoFluxoInclusao'.
     * 
     * @param cdOperacaoFluxoInclusao the value of field
     * 'cdOperacaoFluxoInclusao'.
     */
    public void setCdOperacaoFluxoInclusao(java.lang.String cdOperacaoFluxoInclusao)
    {
        this._cdOperacaoFluxoInclusao = cdOperacaoFluxoInclusao;
    } //-- void setCdOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoFluxoManutencao'.
     * 
     * @param cdOperacaoFluxoManutencao the value of field
     * 'cdOperacaoFluxoManutencao'.
     */
    public void setCdOperacaoFluxoManutencao(java.lang.String cdOperacaoFluxoManutencao)
    {
        this._cdOperacaoFluxoManutencao = cdOperacaoFluxoManutencao;
    } //-- void setCdOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdUsuarioAutorizacao'.
     * 
     * @param cdUsuarioAutorizacao the value of field
     * 'cdUsuarioAutorizacao'.
     */
    public void setCdUsuarioAutorizacao(java.lang.String cdUsuarioAutorizacao)
    {
        this._cdUsuarioAutorizacao = cdUsuarioAutorizacao;
    } //-- void setCdUsuarioAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdValorPrevistoEstorno'.
     * 
     * @param cdValorPrevistoEstorno the value of field
     * 'cdValorPrevistoEstorno'.
     */
    public void setCdValorPrevistoEstorno(java.math.BigDecimal cdValorPrevistoEstorno)
    {
        this._cdValorPrevistoEstorno = cdValorPrevistoEstorno;
    } //-- void setCdValorPrevistoEstorno(java.math.BigDecimal) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaEstorno'.
     * 
     * @param dsAgenciaEstorno the value of field 'dsAgenciaEstorno'
     */
    public void setDsAgenciaEstorno(java.lang.String dsAgenciaEstorno)
    {
        this._dsAgenciaEstorno = dsAgenciaEstorno;
    } //-- void setDsAgenciaEstorno(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoEstorno'.
     * 
     * @param dsBancoEstorno the value of field 'dsBancoEstorno'.
     */
    public void setDsBancoEstorno(java.lang.String dsBancoEstorno)
    {
        this._dsBancoEstorno = dsBancoEstorno;
    } //-- void setDsBancoEstorno(java.lang.String) 

    /**
     * Sets the value of field 'dsDataSolicitacao'.
     * 
     * @param dsDataSolicitacao the value of field
     * 'dsDataSolicitacao'.
     */
    public void setDsDataSolicitacao(java.lang.String dsDataSolicitacao)
    {
        this._dsDataSolicitacao = dsDataSolicitacao;
    } //-- void setDsDataSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaEstrnPagamento'.
     * 
     * @param dsFormaEstrnPagamento the value of field
     * 'dsFormaEstrnPagamento'.
     */
    public void setDsFormaEstrnPagamento(java.lang.String dsFormaEstrnPagamento)
    {
        this._dsFormaEstrnPagamento = dsFormaEstrnPagamento;
    } //-- void setDsFormaEstrnPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsHoraSolicitacao'.
     * 
     * @param dsHoraSolicitacao the value of field
     * 'dsHoraSolicitacao'.
     */
    public void setDsHoraSolicitacao(java.lang.String dsHoraSolicitacao)
    {
        this._dsHoraSolicitacao = dsHoraSolicitacao;
    } //-- void setDsHoraSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsObservacao'.
     * 
     * @param dsObservacao the value of field 'dsObservacao'.
     */
    public void setDsObservacao(java.lang.String dsObservacao)
    {
        this._dsObservacao = dsObservacao;
    } //-- void setDsObservacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dtAutorizacao'.
     * 
     * @param dtAutorizacao the value of field 'dtAutorizacao'.
     */
    public void setDtAutorizacao(java.lang.String dtAutorizacao)
    {
        this._dtAutorizacao = dtAutorizacao;
    } //-- void setDtAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtPrevistaEstorno'.
     * 
     * @param dtPrevistaEstorno the value of field
     * 'dtPrevistaEstorno'.
     */
    public void setDtPrevistaEstorno(java.lang.String dtPrevistaEstorno)
    {
        this._dtPrevistaEstorno = dtPrevistaEstorno;
    } //-- void setDtPrevistaEstorno(java.lang.String) 

    /**
     * Sets the value of field 'hrAutorizacao'.
     * 
     * @param hrAutorizacao the value of field 'hrAutorizacao'.
     */
    public void setHrAutorizacao(java.lang.String hrAutorizacao)
    {
        this._hrAutorizacao = hrAutorizacao;
    } //-- void setHrAutorizacao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'situacaoSolicitacao'.
     * 
     * @param situacaoSolicitacao the value of field
     * 'situacaoSolicitacao'.
     */
    public void setSituacaoSolicitacao(java.lang.String situacaoSolicitacao)
    {
        this._situacaoSolicitacao = situacaoSolicitacao;
    } //-- void setSituacaoSolicitacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalhesSolicitacaoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.response.ConsultarDetalhesSolicitacaoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.response.ConsultarDetalhesSolicitacaoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.response.ConsultarDetalhesSolicitacaoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacao.response.ConsultarDetalhesSolicitacaoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
