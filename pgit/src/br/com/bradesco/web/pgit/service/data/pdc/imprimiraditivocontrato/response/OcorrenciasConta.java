/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class OcorrenciasConta.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasConta implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdAcaoConta
     */
    private int _cdAcaoConta = 0;

    /**
     * keeps track of state for field: _cdAcaoConta
     */
    private boolean _has_cdAcaoConta;

    /**
     * Field _cdBancoConta
     */
    private int _cdBancoConta = 0;

    /**
     * keeps track of state for field: _cdBancoConta
     */
    private boolean _has_cdBancoConta;

    /**
     * Field _dsBancoConta
     */
    private java.lang.String _dsBancoConta;

    /**
     * Field _cdAgenciaConta
     */
    private int _cdAgenciaConta = 0;

    /**
     * keeps track of state for field: _cdAgenciaConta
     */
    private boolean _has_cdAgenciaConta;

    /**
     * Field _digitoAgenciaConta
     */
    private int _digitoAgenciaConta = 0;

    /**
     * keeps track of state for field: _digitoAgenciaConta
     */
    private boolean _has_digitoAgenciaConta;

    /**
     * Field _dsAgenciaConta
     */
    private java.lang.String _dsAgenciaConta;

    /**
     * Field _cdContaConta
     */
    private long _cdContaConta = 0;

    /**
     * keeps track of state for field: _cdContaConta
     */
    private boolean _has_cdContaConta;

    /**
     * Field _digitoContaConta
     */
    private java.lang.String _digitoContaConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasConta() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoConta
     * 
     */
    public void deleteCdAcaoConta()
    {
        this._has_cdAcaoConta= false;
    } //-- void deleteCdAcaoConta() 

    /**
     * Method deleteCdAgenciaConta
     * 
     */
    public void deleteCdAgenciaConta()
    {
        this._has_cdAgenciaConta= false;
    } //-- void deleteCdAgenciaConta() 

    /**
     * Method deleteCdBancoConta
     * 
     */
    public void deleteCdBancoConta()
    {
        this._has_cdBancoConta= false;
    } //-- void deleteCdBancoConta() 

    /**
     * Method deleteCdContaConta
     * 
     */
    public void deleteCdContaConta()
    {
        this._has_cdContaConta= false;
    } //-- void deleteCdContaConta() 

    /**
     * Method deleteDigitoAgenciaConta
     * 
     */
    public void deleteDigitoAgenciaConta()
    {
        this._has_digitoAgenciaConta= false;
    } //-- void deleteDigitoAgenciaConta() 

    /**
     * Returns the value of field 'cdAcaoConta'.
     * 
     * @return int
     * @return the value of field 'cdAcaoConta'.
     */
    public int getCdAcaoConta()
    {
        return this._cdAcaoConta;
    } //-- int getCdAcaoConta() 

    /**
     * Returns the value of field 'cdAgenciaConta'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaConta'.
     */
    public int getCdAgenciaConta()
    {
        return this._cdAgenciaConta;
    } //-- int getCdAgenciaConta() 

    /**
     * Returns the value of field 'cdBancoConta'.
     * 
     * @return int
     * @return the value of field 'cdBancoConta'.
     */
    public int getCdBancoConta()
    {
        return this._cdBancoConta;
    } //-- int getCdBancoConta() 

    /**
     * Returns the value of field 'cdContaConta'.
     * 
     * @return long
     * @return the value of field 'cdContaConta'.
     */
    public long getCdContaConta()
    {
        return this._cdContaConta;
    } //-- long getCdContaConta() 

    /**
     * Returns the value of field 'digitoAgenciaConta'.
     * 
     * @return int
     * @return the value of field 'digitoAgenciaConta'.
     */
    public int getDigitoAgenciaConta()
    {
        return this._digitoAgenciaConta;
    } //-- int getDigitoAgenciaConta() 

    /**
     * Returns the value of field 'digitoContaConta'.
     * 
     * @return String
     * @return the value of field 'digitoContaConta'.
     */
    public java.lang.String getDigitoContaConta()
    {
        return this._digitoContaConta;
    } //-- java.lang.String getDigitoContaConta() 

    /**
     * Returns the value of field 'dsAgenciaConta'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaConta'.
     */
    public java.lang.String getDsAgenciaConta()
    {
        return this._dsAgenciaConta;
    } //-- java.lang.String getDsAgenciaConta() 

    /**
     * Returns the value of field 'dsBancoConta'.
     * 
     * @return String
     * @return the value of field 'dsBancoConta'.
     */
    public java.lang.String getDsBancoConta()
    {
        return this._dsBancoConta;
    } //-- java.lang.String getDsBancoConta() 

    /**
     * Method hasCdAcaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoConta()
    {
        return this._has_cdAcaoConta;
    } //-- boolean hasCdAcaoConta() 

    /**
     * Method hasCdAgenciaConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaConta()
    {
        return this._has_cdAgenciaConta;
    } //-- boolean hasCdAgenciaConta() 

    /**
     * Method hasCdBancoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoConta()
    {
        return this._has_cdBancoConta;
    } //-- boolean hasCdBancoConta() 

    /**
     * Method hasCdContaConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaConta()
    {
        return this._has_cdContaConta;
    } //-- boolean hasCdContaConta() 

    /**
     * Method hasDigitoAgenciaConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDigitoAgenciaConta()
    {
        return this._has_digitoAgenciaConta;
    } //-- boolean hasDigitoAgenciaConta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoConta'.
     * 
     * @param cdAcaoConta the value of field 'cdAcaoConta'.
     */
    public void setCdAcaoConta(int cdAcaoConta)
    {
        this._cdAcaoConta = cdAcaoConta;
        this._has_cdAcaoConta = true;
    } //-- void setCdAcaoConta(int) 

    /**
     * Sets the value of field 'cdAgenciaConta'.
     * 
     * @param cdAgenciaConta the value of field 'cdAgenciaConta'.
     */
    public void setCdAgenciaConta(int cdAgenciaConta)
    {
        this._cdAgenciaConta = cdAgenciaConta;
        this._has_cdAgenciaConta = true;
    } //-- void setCdAgenciaConta(int) 

    /**
     * Sets the value of field 'cdBancoConta'.
     * 
     * @param cdBancoConta the value of field 'cdBancoConta'.
     */
    public void setCdBancoConta(int cdBancoConta)
    {
        this._cdBancoConta = cdBancoConta;
        this._has_cdBancoConta = true;
    } //-- void setCdBancoConta(int) 

    /**
     * Sets the value of field 'cdContaConta'.
     * 
     * @param cdContaConta the value of field 'cdContaConta'.
     */
    public void setCdContaConta(long cdContaConta)
    {
        this._cdContaConta = cdContaConta;
        this._has_cdContaConta = true;
    } //-- void setCdContaConta(long) 

    /**
     * Sets the value of field 'digitoAgenciaConta'.
     * 
     * @param digitoAgenciaConta the value of field
     * 'digitoAgenciaConta'.
     */
    public void setDigitoAgenciaConta(int digitoAgenciaConta)
    {
        this._digitoAgenciaConta = digitoAgenciaConta;
        this._has_digitoAgenciaConta = true;
    } //-- void setDigitoAgenciaConta(int) 

    /**
     * Sets the value of field 'digitoContaConta'.
     * 
     * @param digitoContaConta the value of field 'digitoContaConta'
     */
    public void setDigitoContaConta(java.lang.String digitoContaConta)
    {
        this._digitoContaConta = digitoContaConta;
    } //-- void setDigitoContaConta(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaConta'.
     * 
     * @param dsAgenciaConta the value of field 'dsAgenciaConta'.
     */
    public void setDsAgenciaConta(java.lang.String dsAgenciaConta)
    {
        this._dsAgenciaConta = dsAgenciaConta;
    } //-- void setDsAgenciaConta(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoConta'.
     * 
     * @param dsBancoConta the value of field 'dsBancoConta'.
     */
    public void setDsBancoConta(java.lang.String dsBancoConta)
    {
        this._dsBancoConta = dsBancoConta;
    } //-- void setDsBancoConta(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasConta
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiraditivocontrato.response.OcorrenciasConta unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
