/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;
import java.math.BigDecimal;

/**
 * Nome: ConsultarListaServLimDiaIndvSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaServLimDiaIndvSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdParametroTela. */
	private Integer cdParametroTela;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo dsProdutoOperacaoRelacionado. */
	private String dsProdutoOperacaoRelacionado;
	
	/** Atributo cdRelacionamentoProduto. */
	private Integer cdRelacionamentoProduto;
	
	/** Atributo dtInclusaoServico. */
	private String dtInclusaoServico;
	
	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo dsMadalidade. */
	private String dsMadalidade;
	
	/** Atributo vlLimiteIndvdPagamento. */
	private BigDecimal vlLimiteIndvdPagamento;
	
	/** Atributo vlLimiteDiaPagamento. */
	private BigDecimal vlLimiteDiaPagamento;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas(){
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas){
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdParametroTela.
	 *
	 * @return cdParametroTela
	 */
	public Integer getCdParametroTela(){
		return cdParametroTela;
	}

	/**
	 * Set: cdParametroTela.
	 *
	 * @param cdParametroTela the cd parametro tela
	 */
	public void setCdParametroTela(Integer cdParametroTela){
		this.cdParametroTela = cdParametroTela;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao(){
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao){
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao(){
		return dsProdutoServicoOperacao;
	}

	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao){
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado(){
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado){
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado(){
		return dsProdutoOperacaoRelacionado;
	}

	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado){
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto(){
		return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto){
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

	/**
	 * Get: dtInclusaoServico.
	 *
	 * @return dtInclusaoServico
	 */
	public String getDtInclusaoServico(){
		return dtInclusaoServico;
	}

	/**
	 * Set: dtInclusaoServico.
	 *
	 * @param dtInclusaoServico the dt inclusao servico
	 */
	public void setDtInclusaoServico(String dtInclusaoServico){
		this.dtInclusaoServico = dtInclusaoServico;
	}

	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico(){
		return dsServico;
	}

	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico){
		this.dsServico = dsServico;
	}

	/**
	 * Get: dsMadalidade.
	 *
	 * @return dsMadalidade
	 */
	public String getDsMadalidade(){
		return dsMadalidade;
	}

	/**
	 * Set: dsMadalidade.
	 *
	 * @param dsMadalidade the ds madalidade
	 */
	public void setDsMadalidade(String dsMadalidade){
		this.dsMadalidade = dsMadalidade;
	}

	/**
	 * Get: vlLimiteIndvdPagamento.
	 *
	 * @return vlLimiteIndvdPagamento
	 */
	public BigDecimal getVlLimiteIndvdPagamento(){
		return vlLimiteIndvdPagamento;
	}

	/**
	 * Set: vlLimiteIndvdPagamento.
	 *
	 * @param vlLimiteIndvdPagamento the vl limite indvd pagamento
	 */
	public void setVlLimiteIndvdPagamento(BigDecimal vlLimiteIndvdPagamento){
		this.vlLimiteIndvdPagamento = vlLimiteIndvdPagamento;
	}

	/**
	 * Get: vlLimiteDiaPagamento.
	 *
	 * @return vlLimiteDiaPagamento
	 */
	public BigDecimal getVlLimiteDiaPagamento(){
		return vlLimiteDiaPagamento;
	}

	/**
	 * Set: vlLimiteDiaPagamento.
	 *
	 * @param vlLimiteDiaPagamento the vl limite dia pagamento
	 */
	public void setVlLimiteDiaPagamento(BigDecimal vlLimiteDiaPagamento){
		this.vlLimiteDiaPagamento = vlLimiteDiaPagamento;
	}
}