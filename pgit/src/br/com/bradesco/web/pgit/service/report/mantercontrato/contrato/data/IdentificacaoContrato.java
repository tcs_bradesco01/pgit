/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.domain
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 04/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;
import java.util.ArrayList;
import java.util.List;

/**
 * Nome: Identificacao
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IdentificacaoContrato {

    /** Atributo nome. */
    private String nome = null;
    
    /** Atributo cnpjCpf. */
    private String cnpjCpf = null;
    
    /** Atributo endereco. */
    private String endereco = null;
    
    /** Atributo numConvenio. */
    private String numConvenio = null;
    
    /** Atributo agGestoraConvenio. */
    private String agGestoraConvenio = null;
    
    /** Atributo identificacaoMatriz. */
    private DadosIdentificacao identificacaoMatriz = null;

    /** Atributo identificacaoParticipantes. */
    private List<DadosIdentificacao> identificacaoParticipantes = null;
    
    /** Atributo contas. */
    private List<Conta> contas = new ArrayList<Conta>();
    
    /** Atributo formaAutorizacaoSelecionada. */
    private String formaAutorizacaoSelecionada = null;
    
    /** Atributo formaAutorizacaoParagrafo. */
    private String formaAutorizacaoParagrafo = null;
    
    /** Atributo hrLimiteTransmissaoArquivo. */
    private String hrLimiteTransmissaoArquivo = null;
    
    /** Atributo hrLimiteEfetivacaoPagamento. */
    private String hrLimiteEfetivacaoPagamento = null;
    
    /** Atributo hrCicloEfetivacaoPagamento. */
    private String hrCicloEfetivacaoPagamento = null;
    
    /** Atributo hrLimiteConsultaPagamento. */
    private String hrLimiteConsultaPagamento = null;
    
    /** Atributo assinatura. */
    private Assinatura assinatura = null;
    
    /**
     * Instancia um novo IdentificacaoContrato
     *
     * @see
     */
    public IdentificacaoContrato() {
        super();
    }

    /**
     * Nome: getNome
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Nome: setNome
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Nome: getCnpjCpf
     *
     * @return cnpjCpf
     */
    public String getCnpjCpf() {
        return cnpjCpf;
    }

    /**
     * Nome: setCnpjCpf
     *
     * @param cnpjCpf
     */
    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

    /**
     * Nome: getEndereco
     *
     * @return endereco
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * Nome: setEndereco
     *
     * @param endereco
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * Nome: getNumConvenio
     *
     * @return numConvenio
     */
    public String getNumConvenio() {
        return numConvenio;
    }

    /**
     * Nome: setNumConvenio
     *
     * @param numConvenio
     */
    public void setNumConvenio(String numConvenio) {
        this.numConvenio = numConvenio;
    }

    /**
     * Nome: getAgGestoraConvenio
     *
     * @return agGestoraConvenio
     */
    public String getAgGestoraConvenio() {
        return agGestoraConvenio;
    }

    /**
     * Nome: setAgGestoraConvenio
     *
     * @param agGestoraConvenio
     */
    public void setAgGestoraConvenio(String agGestoraConvenio) {
        this.agGestoraConvenio = agGestoraConvenio;
    }

    /**
     * Nome: getIdentificacaoMatriz
     *
     * @return identificacaoMatriz
     */
    public DadosIdentificacao getIdentificacaoMatriz() {
        return identificacaoMatriz;
    }

    /**
     * Nome: setIdentificacaoMatriz
     *
     * @param identificacaoMatriz
     */
    public void setIdentificacaoMatriz(DadosIdentificacao identificacaoMatriz) {
        this.identificacaoMatriz = identificacaoMatriz;
    }

    /**
     * Nome: getIdentificacaoParticipantes
     *
     * @return identificacaoParticipantes
     */
    public List<DadosIdentificacao> getIdentificacaoParticipantes() {
        return identificacaoParticipantes;
    }

    /**
     * Nome: setIdentificacaoParticipantes
     *
     * @param identificacaoParticipantes
     */
    public void setIdentificacaoParticipantes(List<DadosIdentificacao> identificacaoParticipantes) {
        this.identificacaoParticipantes = identificacaoParticipantes;
    }

    /**
     * Nome: getContas
     *
     * @return contas
     */
    public List<Conta> getContas() {
        return contas;
    }

    /**
     * Nome: setContas
     *
     * @param contas
     */
    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }

    /**
     * Nome: getFormaAutorizacaoSelecionada
     *
     * @return formaAutorizacaoSelecionada
     */
    public String getFormaAutorizacaoSelecionada() {
        return formaAutorizacaoSelecionada;
    }

    /**
     * Nome: setFormaAutorizacaoSelecionada
     *
     * @param formaAutorizacaoSelecionada
     */
    public void setFormaAutorizacaoSelecionada(String formaAutorizacaoSelecionada) {
        this.formaAutorizacaoSelecionada = formaAutorizacaoSelecionada;
    }

    /**
     * Nome: getFormaAutorizacaoParagrafo
     *
     * @return formaAutorizacaoParagrafo
     */
    public String getFormaAutorizacaoParagrafo() {
        return formaAutorizacaoParagrafo;
    }

    /**
     * Nome: setFormaAutorizacaoParagrafo
     *
     * @param formaAutorizacaoParagrafo
     */
    public void setFormaAutorizacaoParagrafo(String formaAutorizacaoParagrafo) {
        this.formaAutorizacaoParagrafo = formaAutorizacaoParagrafo;
    }

    /**
     * Nome: getAssinatura
     *
     * @return assinatura
     */
    public Assinatura getAssinatura() {
        return assinatura;
    }

    /**
     * Nome: setAssinatura
     *
     * @param assinatura
     */
    public void setAssinatura(Assinatura assinatura) {
        this.assinatura = assinatura;
    }

	/**
	 * @param hrLimiteTransmissaoArquivo the hrLimiteTransmissaoArquivo to set
	 */
	public void setHrLimiteTransmissaoArquivo(String hrLimiteTransmissaoArquivo) {
		this.hrLimiteTransmissaoArquivo = hrLimiteTransmissaoArquivo;
	}

	/**
	 * @return the hrLimiteTransmissaoArquivo
	 */
	public String getHrLimiteTransmissaoArquivo() {
		return hrLimiteTransmissaoArquivo;
	}

	/**
	 * @param hrLimiteEfetivacaoPagamento the hrLimiteEfetivacaoPagamento to set
	 */
	public void setHrLimiteEfetivacaoPagamento(
			String hrLimiteEfetivacaoPagamento) {
		this.hrLimiteEfetivacaoPagamento = hrLimiteEfetivacaoPagamento;
	}

	/**
	 * @return the hrLimiteEfetivacaoPagamento
	 */
	public String getHrLimiteEfetivacaoPagamento() {
		return hrLimiteEfetivacaoPagamento;
	}

	/**
	 * @param hrCicloEfetivacaoPagamento the hrCicloEfetivacaoPagamento to set
	 */
	public void setHrCicloEfetivacaoPagamento(String hrCicloEfetivacaoPagamento) {
		this.hrCicloEfetivacaoPagamento = hrCicloEfetivacaoPagamento;
	}

	/**
	 * @return the hrCicloEfetivacaoPagamento
	 */
	public String getHrCicloEfetivacaoPagamento() {
		return hrCicloEfetivacaoPagamento;
	}

	/**
	 * @param hrLimiteConsultaPagamento the hrLimiteConsultaPagamento to set
	 */
	public void setHrLimiteConsultaPagamento(String hrLimiteConsultaPagamento) {
		this.hrLimiteConsultaPagamento = hrLimiteConsultaPagamento;
	}

	/**
	 * @return the hrLimiteConsultaPagamento
	 */
	public String getHrLimiteConsultaPagamento() {
		return hrLimiteConsultaPagamento;
	}
}
