/*
 * Nome: br.com.bradesco.web.pgit.service.business.duplarqretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.duplarqretorno.bean;

/**
 * Nome: DetalharDuplicacaoArqRetornoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharDuplicacaoArqRetornoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPessoaJuridVinc. */
	private Long cdPessoaJuridVinc;
	
	/** Atributo cdTipoContratoVinc. */
	private Integer cdTipoContratoVinc;
	
	/** Atributo nrSeqContratoVinc. */
	private Long nrSeqContratoVinc;
	
	/** Atributo cdPessoaJuridContrato. */
	private Long cdPessoaJuridContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSeqContratoNegocio. */
	private Long nrSeqContratoNegocio;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdTipoArquivoRetorno. */
	private Integer cdTipoArquivoRetorno;
	
	/** Atributo dsTipoArquivoRetorno. */
	private String dsTipoArquivoRetorno;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo dsPessoa. */
	private String dsPessoa;
	
	/** Atributo cdInstrucaoEnvioRetorno. */
	private Integer cdInstrucaoEnvioRetorno;
	
	/** Atributo cdIndicadorDuplicidadeRetorno. */
	private Integer cdIndicadorDuplicidadeRetorno;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenSegrcInclusao. */
	private String cdAutenSegrcInclusao;
	
	/** Atributo nmOperFluxoInclusao. */
	private String nmOperFluxoInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenSegrcManutencao. */
	private String cdAutenSegrcManutencao;
	
	/** Atributo nmOperFluxoManutencao. */
	private String nmOperFluxoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;
	
	/**
	 * Get: cdAutenSegrcInclusao.
	 *
	 * @return cdAutenSegrcInclusao
	 */
	public String getCdAutenSegrcInclusao() {
		return cdAutenSegrcInclusao;
	}
	
	/**
	 * Set: cdAutenSegrcInclusao.
	 *
	 * @param cdAutenSegrcInclusao the cd auten segrc inclusao
	 */
	public void setCdAutenSegrcInclusao(String cdAutenSegrcInclusao) {
		this.cdAutenSegrcInclusao = cdAutenSegrcInclusao;
	}
	
	/**
	 * Get: cdAutenSegrcManutencao.
	 *
	 * @return cdAutenSegrcManutencao
	 */
	public String getCdAutenSegrcManutencao() {
		return cdAutenSegrcManutencao;
	}
	
	/**
	 * Set: cdAutenSegrcManutencao.
	 *
	 * @param cdAutenSegrcManutencao the cd auten segrc manutencao
	 */
	public void setCdAutenSegrcManutencao(String cdAutenSegrcManutencao) {
		this.cdAutenSegrcManutencao = cdAutenSegrcManutencao;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdIndicadorDuplicidadeRetorno.
	 *
	 * @return cdIndicadorDuplicidadeRetorno
	 */
	public Integer getCdIndicadorDuplicidadeRetorno() {
		return cdIndicadorDuplicidadeRetorno;
	}
	
	/**
	 * Set: cdIndicadorDuplicidadeRetorno.
	 *
	 * @param cdIndicadorDuplicidadeRetorno the cd indicador duplicidade retorno
	 */
	public void setCdIndicadorDuplicidadeRetorno(
			Integer cdIndicadorDuplicidadeRetorno) {
		this.cdIndicadorDuplicidadeRetorno = cdIndicadorDuplicidadeRetorno;
	}
	
	/**
	 * Get: cdInstrucaoEnvioRetorno.
	 *
	 * @return cdInstrucaoEnvioRetorno
	 */
	public Integer getCdInstrucaoEnvioRetorno() {
		return cdInstrucaoEnvioRetorno;
	}
	
	/**
	 * Set: cdInstrucaoEnvioRetorno.
	 *
	 * @param cdInstrucaoEnvioRetorno the cd instrucao envio retorno
	 */
	public void setCdInstrucaoEnvioRetorno(Integer cdInstrucaoEnvioRetorno) {
		this.cdInstrucaoEnvioRetorno = cdInstrucaoEnvioRetorno;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdPessoaJuridContrato.
	 *
	 * @return cdPessoaJuridContrato
	 */
	public Long getCdPessoaJuridContrato() {
		return cdPessoaJuridContrato;
	}
	
	/**
	 * Set: cdPessoaJuridContrato.
	 *
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 */
	public void setCdPessoaJuridContrato(Long cdPessoaJuridContrato) {
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
	}
	
	/**
	 * Get: cdPessoaJuridVinc.
	 *
	 * @return cdPessoaJuridVinc
	 */
	public Long getCdPessoaJuridVinc() {
		return cdPessoaJuridVinc;
	}
	
	/**
	 * Set: cdPessoaJuridVinc.
	 *
	 * @param cdPessoaJuridVinc the cd pessoa jurid vinc
	 */
	public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
		this.cdPessoaJuridVinc = cdPessoaJuridVinc;
	}
	
	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return cdTipoArquivoRetorno
	 */
	public Integer getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}
	
	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdTipoArquivoRetorno the cd tipo arquivo retorno
	 */
	public void setCdTipoArquivoRetorno(Integer cdTipoArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public Integer getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}
	
	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: dsTipoArquivoRetorno.
	 *
	 * @return dsTipoArquivoRetorno
	 */
	public String getDsTipoArquivoRetorno() {
		return dsTipoArquivoRetorno;
	}
	
	/**
	 * Set: dsTipoArquivoRetorno.
	 *
	 * @param dsTipoArquivoRetorno the ds tipo arquivo retorno
	 */
	public void setDsTipoArquivoRetorno(String dsTipoArquivoRetorno) {
		this.dsTipoArquivoRetorno = dsTipoArquivoRetorno;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nmOperFluxoInclusao.
	 *
	 * @return nmOperFluxoInclusao
	 */
	public String getNmOperFluxoInclusao() {
		return nmOperFluxoInclusao;
	}
	
	/**
	 * Set: nmOperFluxoInclusao.
	 *
	 * @param nmOperFluxoInclusao the nm oper fluxo inclusao
	 */
	public void setNmOperFluxoInclusao(String nmOperFluxoInclusao) {
		this.nmOperFluxoInclusao = nmOperFluxoInclusao;
	}
	
	/**
	 * Get: nmOperFluxoManutencao.
	 *
	 * @return nmOperFluxoManutencao
	 */
	public String getNmOperFluxoManutencao() {
		return nmOperFluxoManutencao;
	}
	
	/**
	 * Set: nmOperFluxoManutencao.
	 *
	 * @param nmOperFluxoManutencao the nm oper fluxo manutencao
	 */
	public void setNmOperFluxoManutencao(String nmOperFluxoManutencao) {
		this.nmOperFluxoManutencao = nmOperFluxoManutencao;
	}
	
	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}
	
	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}
	
	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public Long getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}
	
	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}
	
	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
	    return cpfCnpjParticipante;
	}
	
	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpj the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpj) {
	    this.cpfCnpjParticipante = cpfCnpj;
	}
	
	
	
}
