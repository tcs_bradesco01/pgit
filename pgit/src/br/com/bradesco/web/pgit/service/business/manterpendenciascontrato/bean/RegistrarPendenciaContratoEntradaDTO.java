/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean;

import java.util.Date;

/**
 * Nome: RegistrarPendenciaContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegistrarPendenciaContratoEntradaDTO {
	
	
	/** Atributo cdPessoaJuridicaContrato. */
	private int cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private int nrSequenciaContratoNegocio;
	
	/** Atributo cdPendenciaPagamentoIntegrado. */
	private int cdPendenciaPagamentoIntegrado;
	
	/** Atributo nrPendenciaPagamentoIntegrado. */
	private int nrPendenciaPagamentoIntegrado;
	
	/** Atributo codSituacaoPendenciaContrato. */
	private int codSituacaoPendenciaContrato;
	
	/** Atributo codTipoBaixaPendencia. */
	private int codTipoBaixaPendencia;
	
	/** Atributo dtAtendimentoPendenciaContrato. */
	private Date dtAtendimentoPendenciaContrato;
	
	/** Atributo codMotivoBaixaPendencia. */
	private int codMotivoBaixaPendencia;
	
	
	/**
	 * Get: cdPendenciaPagamentoIntegrado.
	 *
	 * @return cdPendenciaPagamentoIntegrado
	 */
	public int getCdPendenciaPagamentoIntegrado() {
		return cdPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: cdPendenciaPagamentoIntegrado.
	 *
	 * @param cdPendenciaPagamentoIntegrado the cd pendencia pagamento integrado
	 */
	public void setCdPendenciaPagamentoIntegrado(int cdPendenciaPagamentoIntegrado) {
		this.cdPendenciaPagamentoIntegrado = cdPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public int getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(int cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: codMotivoBaixaPendencia.
	 *
	 * @return codMotivoBaixaPendencia
	 */
	public int getCodMotivoBaixaPendencia() {
		return codMotivoBaixaPendencia;
	}
	
	/**
	 * Set: codMotivoBaixaPendencia.
	 *
	 * @param codMotivoBaixaPendencia the cod motivo baixa pendencia
	 */
	public void setCodMotivoBaixaPendencia(int codMotivoBaixaPendencia) {
		this.codMotivoBaixaPendencia = codMotivoBaixaPendencia;
	}
	
	/**
	 * Get: codSituacaoPendenciaContrato.
	 *
	 * @return codSituacaoPendenciaContrato
	 */
	public int getCodSituacaoPendenciaContrato() {
		return codSituacaoPendenciaContrato;
	}
	
	/**
	 * Set: codSituacaoPendenciaContrato.
	 *
	 * @param codSituacaoPendenciaContrato the cod situacao pendencia contrato
	 */
	public void setCodSituacaoPendenciaContrato(int codSituacaoPendenciaContrato) {
		this.codSituacaoPendenciaContrato = codSituacaoPendenciaContrato;
	}
	
	/**
	 * Get: codTipoBaixaPendencia.
	 *
	 * @return codTipoBaixaPendencia
	 */
	public int getCodTipoBaixaPendencia() {
		return codTipoBaixaPendencia;
	}
	
	/**
	 * Set: codTipoBaixaPendencia.
	 *
	 * @param codTipoBaixaPendencia the cod tipo baixa pendencia
	 */
	public void setCodTipoBaixaPendencia(int codTipoBaixaPendencia) {
		this.codTipoBaixaPendencia = codTipoBaixaPendencia;
	}
	
	/**
	 * Get: dtAtendimentoPendenciaContrato.
	 *
	 * @return dtAtendimentoPendenciaContrato
	 */
	public Date getDtAtendimentoPendenciaContrato() {
		return dtAtendimentoPendenciaContrato;
	}
	
	/**
	 * Set: dtAtendimentoPendenciaContrato.
	 *
	 * @param dtAtendimentoPendenciaContrato the dt atendimento pendencia contrato
	 */
	public void setDtAtendimentoPendenciaContrato(
			Date dtAtendimentoPendenciaContrato) {
		this.dtAtendimentoPendenciaContrato = dtAtendimentoPendenciaContrato;
	}
	
	/**
	 * Get: nrPendenciaPagamentoIntegrado.
	 *
	 * @return nrPendenciaPagamentoIntegrado
	 */
	public int getNrPendenciaPagamentoIntegrado() {
		return nrPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: nrPendenciaPagamentoIntegrado.
	 *
	 * @param nrPendenciaPagamentoIntegrado the nr pendencia pagamento integrado
	 */
	public void setNrPendenciaPagamentoIntegrado(int nrPendenciaPagamentoIntegrado) {
		this.nrPendenciaPagamentoIntegrado = nrPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public int getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(int nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	
	
	
	

}
