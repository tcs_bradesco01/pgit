/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCpfCnpjTerceiro
     */
    private long _cdCpfCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjTerceiro
     */
    private boolean _has_cdCpfCnpjTerceiro;

    /**
     * Field _cdFilialCnpjTerceiro
     */
    private int _cdFilialCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjTerceiro
     */
    private boolean _has_cdFilialCnpjTerceiro;

    /**
     * Field _cdControleCnpjTerceiro
     */
    private int _cdControleCnpjTerceiro = 0;

    /**
     * keeps track of state for field: _cdControleCnpjTerceiro
     */
    private boolean _has_cdControleCnpjTerceiro;

    /**
     * Field _nrRastreaTerceiro
     */
    private int _nrRastreaTerceiro = 0;

    /**
     * keeps track of state for field: _nrRastreaTerceiro
     */
    private boolean _has_nrRastreaTerceiro;

    /**
     * Field _dtInicioRastreaTerceiro
     */
    private java.lang.String _dtInicioRastreaTerceiro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCnpjTerceiro
     * 
     */
    public void deleteCdControleCnpjTerceiro()
    {
        this._has_cdControleCnpjTerceiro= false;
    } //-- void deleteCdControleCnpjTerceiro() 

    /**
     * Method deleteCdCpfCnpjTerceiro
     * 
     */
    public void deleteCdCpfCnpjTerceiro()
    {
        this._has_cdCpfCnpjTerceiro= false;
    } //-- void deleteCdCpfCnpjTerceiro() 

    /**
     * Method deleteCdFilialCnpjTerceiro
     * 
     */
    public void deleteCdFilialCnpjTerceiro()
    {
        this._has_cdFilialCnpjTerceiro= false;
    } //-- void deleteCdFilialCnpjTerceiro() 

    /**
     * Method deleteNrRastreaTerceiro
     * 
     */
    public void deleteNrRastreaTerceiro()
    {
        this._has_nrRastreaTerceiro= false;
    } //-- void deleteNrRastreaTerceiro() 

    /**
     * Returns the value of field 'cdControleCnpjTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjTerceiro'.
     */
    public int getCdControleCnpjTerceiro()
    {
        return this._cdControleCnpjTerceiro;
    } //-- int getCdControleCnpjTerceiro() 

    /**
     * Returns the value of field 'cdCpfCnpjTerceiro'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjTerceiro'.
     */
    public long getCdCpfCnpjTerceiro()
    {
        return this._cdCpfCnpjTerceiro;
    } //-- long getCdCpfCnpjTerceiro() 

    /**
     * Returns the value of field 'cdFilialCnpjTerceiro'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjTerceiro'.
     */
    public int getCdFilialCnpjTerceiro()
    {
        return this._cdFilialCnpjTerceiro;
    } //-- int getCdFilialCnpjTerceiro() 

    /**
     * Returns the value of field 'dtInicioRastreaTerceiro'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreaTerceiro'.
     */
    public java.lang.String getDtInicioRastreaTerceiro()
    {
        return this._dtInicioRastreaTerceiro;
    } //-- java.lang.String getDtInicioRastreaTerceiro() 

    /**
     * Returns the value of field 'nrRastreaTerceiro'.
     * 
     * @return int
     * @return the value of field 'nrRastreaTerceiro'.
     */
    public int getNrRastreaTerceiro()
    {
        return this._nrRastreaTerceiro;
    } //-- int getNrRastreaTerceiro() 

    /**
     * Method hasCdControleCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjTerceiro()
    {
        return this._has_cdControleCnpjTerceiro;
    } //-- boolean hasCdControleCnpjTerceiro() 

    /**
     * Method hasCdCpfCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjTerceiro()
    {
        return this._has_cdCpfCnpjTerceiro;
    } //-- boolean hasCdCpfCnpjTerceiro() 

    /**
     * Method hasCdFilialCnpjTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjTerceiro()
    {
        return this._has_cdFilialCnpjTerceiro;
    } //-- boolean hasCdFilialCnpjTerceiro() 

    /**
     * Method hasNrRastreaTerceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrRastreaTerceiro()
    {
        return this._has_nrRastreaTerceiro;
    } //-- boolean hasNrRastreaTerceiro() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCnpjTerceiro'.
     * 
     * @param cdControleCnpjTerceiro the value of field
     * 'cdControleCnpjTerceiro'.
     */
    public void setCdControleCnpjTerceiro(int cdControleCnpjTerceiro)
    {
        this._cdControleCnpjTerceiro = cdControleCnpjTerceiro;
        this._has_cdControleCnpjTerceiro = true;
    } //-- void setCdControleCnpjTerceiro(int) 

    /**
     * Sets the value of field 'cdCpfCnpjTerceiro'.
     * 
     * @param cdCpfCnpjTerceiro the value of field
     * 'cdCpfCnpjTerceiro'.
     */
    public void setCdCpfCnpjTerceiro(long cdCpfCnpjTerceiro)
    {
        this._cdCpfCnpjTerceiro = cdCpfCnpjTerceiro;
        this._has_cdCpfCnpjTerceiro = true;
    } //-- void setCdCpfCnpjTerceiro(long) 

    /**
     * Sets the value of field 'cdFilialCnpjTerceiro'.
     * 
     * @param cdFilialCnpjTerceiro the value of field
     * 'cdFilialCnpjTerceiro'.
     */
    public void setCdFilialCnpjTerceiro(int cdFilialCnpjTerceiro)
    {
        this._cdFilialCnpjTerceiro = cdFilialCnpjTerceiro;
        this._has_cdFilialCnpjTerceiro = true;
    } //-- void setCdFilialCnpjTerceiro(int) 

    /**
     * Sets the value of field 'dtInicioRastreaTerceiro'.
     * 
     * @param dtInicioRastreaTerceiro the value of field
     * 'dtInicioRastreaTerceiro'.
     */
    public void setDtInicioRastreaTerceiro(java.lang.String dtInicioRastreaTerceiro)
    {
        this._dtInicioRastreaTerceiro = dtInicioRastreaTerceiro;
    } //-- void setDtInicioRastreaTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'nrRastreaTerceiro'.
     * 
     * @param nrRastreaTerceiro the value of field
     * 'nrRastreaTerceiro'.
     */
    public void setNrRastreaTerceiro(int nrRastreaTerceiro)
    {
        this._nrRastreaTerceiro = nrRastreaTerceiro;
        this._has_nrRastreaTerceiro = true;
    } //-- void setNrRastreaTerceiro(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartercparticipantecontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
