/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;

/**
 * Nome: IManterSolicAntecipacaoPostergarLotePagamentoService
 * <p>
 * Prop�sito: Interface do adaptador
 * ManterSolicAntecipacaoPostergarLotePagamento
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public interface IManterSolicAntecipacaoPostergarLotePagamentoService {

	public ConsultarLotePagamentosSaidaDTO consultarSolicAntPosDtPgto(
			ConsultarLotePagamentosEntradaDTO entrada);

	public DetalharLotePagamentosSaidaDTO detalharSolicAntPosDtPgto(
			DetalharLotePagamentosEntradaDTO entrada);

	public ExcluirLotePagamentosSaidaDTO excluirSolicAntPosDtPgto(
			ExcluirLotePagamentosEntradaDTO entrada);

	public IncluirLotePagamentosSaidaDTO incluirSolicAntPosDtPgto(
			IncluirLotePagamentosEntradaDTO entrada);
}