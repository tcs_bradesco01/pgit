/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.formaliqpagtoint;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.AlterarLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.AlterarLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharHistLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharHistLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ExcluirLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ExcluirLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.IncluirLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.IncluirLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarHistLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarHistLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarLiquidacaoPagamentoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterFormaLiquidacaoPagamentoIntegrado
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IFormaLiqPgtoIntService {

	/**
	 * Consultar liquidacao pagto.
	 *
	 * @param entrada the entrada
	 * @return the listar liquidacao pagamento saida dto
	 */
	ListarLiquidacaoPagamentoSaidaDTO consultarLiquidacaoPagto(ListarLiquidacaoPagamentoEntradaDTO entrada);

	/**
	 * Incluir liq pagamento integrado.
	 *
	 * @param entrada the entrada
	 * @return the incluir liquidacao pagamento saida dto
	 */
	IncluirLiquidacaoPagamentoSaidaDTO incluirLiqPagamentoIntegrado(IncluirLiquidacaoPagamentoEntradaDTO entrada);

	/**
	 * Excluir liq pagamento integrado.
	 *
	 * @param entrada the entrada
	 * @return the excluir liquidacao pagamento saida dto
	 */
	ExcluirLiquidacaoPagamentoSaidaDTO excluirLiqPagamentoIntegrado(ExcluirLiquidacaoPagamentoEntradaDTO entrada);

	/**
	 * Detalhe liq pagamento integrado.
	 *
	 * @param entrada the entrada
	 * @return the detalhar liquidacao pagamento saida dto
	 */
	DetalharLiquidacaoPagamentoSaidaDTO detalheLiqPagamentoIntegrado(DetalharLiquidacaoPagamentoEntradaDTO entrada);

	/**
	 * Consultar hist liquidacao pagto.
	 *
	 * @param entrada the entrada
	 * @return the list< listar hist liquidacao pagamento saida dt o>
	 */
	List<ListarHistLiquidacaoPagamentoSaidaDTO> consultarHistLiquidacaoPagto(ListarHistLiquidacaoPagamentoEntradaDTO entrada);

	/**
	 * Alterar liquidacao pagto.
	 *
	 * @param entrada the entrada
	 * @return the alterar liquidacao pagamento saida dto
	 */
	AlterarLiquidacaoPagamentoSaidaDTO alterarLiquidacaoPagto(AlterarLiquidacaoPagamentoEntradaDTO entrada);

	/**
	 * Detalhe hist liq pagamento integrado.
	 *
	 * @param entrada the entrada
	 * @return the detalhar hist liquidacao pagamento saida dto
	 */
	DetalharHistLiquidacaoPagamentoSaidaDTO detalheHistLiqPagamentoIntegrado(DetalharHistLiquidacaoPagamentoEntradaDTO entrada);

}