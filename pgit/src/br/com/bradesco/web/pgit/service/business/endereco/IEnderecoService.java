/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.endereco;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.endereco.bean.DatalharEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DatalharEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DetalharHistoricoEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DetalharHistoricoEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ExcluirEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ExcluirEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.IncluirEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.IncluirEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoEmailOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoParticipanteOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarHistoricoEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.SubstituirVincEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.SubstituirVincEnderecoParticipanteSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Endereco
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IEnderecoService {

	/**
	 * Listar endereco participante.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar endereco participante ocorrencias saida dt o>
	 */
	List<ListarEnderecoParticipanteOcorrenciasSaidaDTO> listarEnderecoParticipante(ListarEnderecoParticipanteEntradaDTO entradaDTO);

	/**
	 * Incluir endereco participante.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the incluir endereco participante saida dto
	 */
	IncluirEnderecoParticipanteSaidaDTO incluirEnderecoParticipante(IncluirEnderecoParticipanteEntradaDTO entradaDTO);

	/**
	 * Alterar endereco participante.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the substituir vinc endereco participante saida dto
	 */
	SubstituirVincEnderecoParticipanteSaidaDTO alterarEnderecoParticipante(SubstituirVincEnderecoParticipanteEntradaDTO entradaDTO);

	/**
	 * Excluir endereco participante.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir endereco participante saida dto
	 */
	ExcluirEnderecoParticipanteSaidaDTO excluirEnderecoParticipante(ExcluirEnderecoParticipanteEntradaDTO entradaDTO);

	/**
	 * Detalhar endereco participante.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the datalhar endereco participante saida dto
	 */
	DatalharEnderecoParticipanteSaidaDTO detalharEnderecoParticipante(DatalharEnderecoParticipanteEntradaDTO entradaDTO);

	/**
	 * Listar historico endereco participante.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar historico endereco participante ocorrencias saida dt o>
	 */
	List<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO> listarHistoricoEnderecoParticipante(ListarHistoricoEnderecoParticipanteEntradaDTO entradaDTO);

	/**
	 * Detalhar historico endereco participante.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar historico endereco participante saida dto
	 */
	DetalharHistoricoEnderecoParticipanteSaidaDTO detalharHistoricoEnderecoParticipante(DetalharHistoricoEnderecoParticipanteEntradaDTO entradaDTO);

	/**
	 * Listar endereco email.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< listar endereco email ocorrencias saida dt o>
	 */
	List<ListarEnderecoEmailOcorrenciasSaidaDTO> listarEnderecoEmail(ListarEnderecoEmailEntradaDTO entradaDTO);
}

