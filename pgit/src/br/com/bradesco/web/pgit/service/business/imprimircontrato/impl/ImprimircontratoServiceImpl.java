/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.imprimircontrato.impl;

import java.util.List;

import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.IImprimircontratoService;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.request.ImprimirContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.response.ImprimirContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontratomulticanal.request.ImprimirContratoPgitMulticanalRequest;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Imprimircontrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ImprimircontratoServiceImpl implements IImprimircontratoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimircontrato.IImprimircontratoService#imprimirContratoSaidaDTO(br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoEntradaDTO)
	 */
	public ImprimirContratoSaidaDTO imprimirContratoSaidaDTO(ImprimirContratoEntradaDTO imprimirContratoEntradaDTO) {
		ImprimirContratoRequest request = new ImprimirContratoRequest();

		request.setCdClubPessoa(imprimirContratoEntradaDTO.getCdClubPessoa());
		request.setCdControleCpfCnpj(imprimirContratoEntradaDTO.getCdControleCpfCnpj());
		request.setCdCorpoCpfCnpj(imprimirContratoEntradaDTO.getCdCorpoCpfCnpj());
		request.setCdCpfCnpjParticipante(imprimirContratoEntradaDTO.getCdCpfCnpjParticipante());
		request.setCdDigitoCpfCnpj(imprimirContratoEntradaDTO.getCdDigitoCpfCnpj());
		request.setCdPessoaJuridicaContrato(imprimirContratoEntradaDTO.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(imprimirContratoEntradaDTO.getCdTipoContratoNegocio());
		request.setNmEmpresa(imprimirContratoEntradaDTO.getNmEmpresa());
		request.setNmParticipante(imprimirContratoEntradaDTO.getNmParticipante());
		request.setNrSequenciaContratoNegocio(imprimirContratoEntradaDTO.getNrSequenciaContratoNegocio());
		request.setCdFuncionarioBradesco(imprimirContratoEntradaDTO.getCodFuncionalBradesco());

		ImprimirContratoResponse response = getFactoryAdapter().getImprimirContratoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return new ImprimirContratoSaidaDTO();
		}

		return new ImprimirContratoSaidaDTO(response);
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.imprimircontrato.IImprimircontratoService#imprimirContratoMulticanal(br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoEntradaDTO)
	 */
	public long imprimirContratoMulticanal(ImprimirContratoEntradaDTO imprimirContratoEntradaDTO) {
		ImprimirContratoPgitMulticanalRequest request = new ImprimirContratoPgitMulticanalRequest();
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(imprimirContratoEntradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(imprimirContratoEntradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(imprimirContratoEntradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdVersao(1);
		getFactoryAdapter().getImprimirContratoMulticanalPDCAdapter().invokeProcess(request);
		List<?> listaProtocolos = BradescoCommonServiceFactory.getSessionManager().getProtocols();
		Long protocolo = new Long(0l);
		if (listaProtocolos != null && !listaProtocolos.isEmpty()) {
			protocolo = Long.valueOf((String)listaProtocolos.get(listaProtocolos.size()-1));
		}
		return protocolo;
	}

}