/*
 * Nome: br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean;

/**
 * Nome: AssLoteLayoutArquivoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssLoteLayoutArquivoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdTipoLoteLayout. */
	private Integer cdTipoLoteLayout;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo dsTipoLoteLayout. */
	private String dsTipoLoteLayout;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo nrOperacaoFluxoInclusao. */
	private String nrOperacaoFluxoInclusao;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo hrManutencaoRegistroManutencao. */
	private String hrManutencaoRegistroManutencao;
	
	/** Atributo nrOperacaoFluxoManutencao. */
	private String nrOperacaoFluxoManutencao;
	
	/**
	 * Ass lote layout arquivo saida dto.
	 */
	public AssLoteLayoutArquivoSaidaDTO() {
		super();
	}
	
	/**
	 * Ass lote layout arquivo saida dto.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 * @param cdTipoLoteLayout the cd tipo lote layout
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 * @param dsTipoLoteLayout the ds tipo lote layout
	 */
	public AssLoteLayoutArquivoSaidaDTO(Integer cdTipoLayoutArquivo, Integer cdTipoLoteLayout, String dsTipoLayoutArquivo, String dsTipoLoteLayout) {
		super();
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
		this.cdTipoLoteLayout = cdTipoLoteLayout;
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdTipoLoteLayout.
	 *
	 * @return cdTipoLoteLayout
	 */
	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}

	/**
	 * Set: cdTipoLoteLayout.
	 *
	 * @param cdTipoLoteLayout the cd tipo lote layout
	 */
	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: dsTipoLoteLayout.
	 *
	 * @return dsTipoLoteLayout
	 */
	public String getDsTipoLoteLayout() {
		return dsTipoLoteLayout;
	}

	/**
	 * Set: dsTipoLoteLayout.
	 *
	 * @param dsTipoLoteLayout the ds tipo lote layout
	 */
	public void setDsTipoLoteLayout(String dsTipoLoteLayout) {
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencaoRegistroManutencao.
	 *
	 * @return hrManutencaoRegistroManutencao
	 */
	public String getHrManutencaoRegistroManutencao() {
		return hrManutencaoRegistroManutencao;
	}

	/**
	 * Set: hrManutencaoRegistroManutencao.
	 *
	 * @param hrManutencaoRegistroManutencao the hr manutencao registro manutencao
	 */
	public void setHrManutencaoRegistroManutencao(
			String hrManutencaoRegistroManutencao) {
		this.hrManutencaoRegistroManutencao = hrManutencaoRegistroManutencao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrOperacaoFluxoInclusao.
	 *
	 * @return nrOperacaoFluxoInclusao
	 */
	public String getNrOperacaoFluxoInclusao() {
		return nrOperacaoFluxoInclusao;
	}

	/**
	 * Set: nrOperacaoFluxoInclusao.
	 *
	 * @param nrOperacaoFluxoInclusao the nr operacao fluxo inclusao
	 */
	public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
		this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
	}

	/**
	 * Get: nrOperacaoFluxoManutencao.
	 *
	 * @return nrOperacaoFluxoManutencao
	 */
	public String getNrOperacaoFluxoManutencao() {
		return nrOperacaoFluxoManutencao;
	}

	/**
	 * Set: nrOperacaoFluxoManutencao.
	 *
	 * @param nrOperacaoFluxoManutencao the nr operacao fluxo manutencao
	 */
	public void setNrOperacaoFluxoManutencao(String nrOperacaoFluxoManutencao) {
		this.nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	
	
}
