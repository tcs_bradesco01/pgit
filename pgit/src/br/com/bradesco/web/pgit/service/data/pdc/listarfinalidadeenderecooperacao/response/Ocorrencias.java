/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoServicoOper
     */
    private int _cdProdutoServicoOper = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOper
     */
    private boolean _has_cdProdutoServicoOper;

    /**
     * Field _dsProdutoServicoOper
     */
    private java.lang.String _dsProdutoServicoOper;

    /**
     * Field _cdProdutoOperRelacionado
     */
    private int _cdProdutoOperRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperRelacionado
     */
    private boolean _has_cdProdutoOperRelacionado;

    /**
     * Field _dsProdutoOperRelacionado
     */
    private java.lang.String _dsProdutoOperRelacionado;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _dsOperacaoProdutoServico
     */
    private java.lang.String _dsOperacaoProdutoServico;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _cdFinalidadeEnderecoContrato
     */
    private int _cdFinalidadeEnderecoContrato = 0;

    /**
     * keeps track of state for field: _cdFinalidadeEnderecoContrato
     */
    private boolean _has_cdFinalidadeEnderecoContrato;

    /**
     * Field _dsFinalidadeEnderecoContrato
     */
    private java.lang.String _dsFinalidadeEnderecoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFinalidadeEnderecoContrato
     * 
     */
    public void deleteCdFinalidadeEnderecoContrato()
    {
        this._has_cdFinalidadeEnderecoContrato= false;
    } //-- void deleteCdFinalidadeEnderecoContrato() 

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdProdutoOperRelacionado
     * 
     */
    public void deleteCdProdutoOperRelacionado()
    {
        this._has_cdProdutoOperRelacionado= false;
    } //-- void deleteCdProdutoOperRelacionado() 

    /**
     * Method deleteCdProdutoServicoOper
     * 
     */
    public void deleteCdProdutoServicoOper()
    {
        this._has_cdProdutoServicoOper= false;
    } //-- void deleteCdProdutoServicoOper() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdFinalidadeEnderecoContrato'.
     * 
     * @return int
     * @return the value of field 'cdFinalidadeEnderecoContrato'.
     */
    public int getCdFinalidadeEnderecoContrato()
    {
        return this._cdFinalidadeEnderecoContrato;
    } //-- int getCdFinalidadeEnderecoContrato() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field 'cdProdutoOperRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperRelacionado'.
     */
    public int getCdProdutoOperRelacionado()
    {
        return this._cdProdutoOperRelacionado;
    } //-- int getCdProdutoOperRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOper'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOper'.
     */
    public int getCdProdutoServicoOper()
    {
        return this._cdProdutoServicoOper;
    } //-- int getCdProdutoServicoOper() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'dsFinalidadeEnderecoContrato'.
     * 
     * @return String
     * @return the value of field 'dsFinalidadeEnderecoContrato'.
     */
    public java.lang.String getDsFinalidadeEnderecoContrato()
    {
        return this._dsFinalidadeEnderecoContrato;
    } //-- java.lang.String getDsFinalidadeEnderecoContrato() 

    /**
     * Returns the value of field 'dsOperacaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoProdutoServico'.
     */
    public java.lang.String getDsOperacaoProdutoServico()
    {
        return this._dsOperacaoProdutoServico;
    } //-- java.lang.String getDsOperacaoProdutoServico() 

    /**
     * Returns the value of field 'dsProdutoOperRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperRelacionado'.
     */
    public java.lang.String getDsProdutoOperRelacionado()
    {
        return this._dsProdutoOperRelacionado;
    } //-- java.lang.String getDsProdutoOperRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOper'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOper'.
     */
    public java.lang.String getDsProdutoServicoOper()
    {
        return this._dsProdutoServicoOper;
    } //-- java.lang.String getDsProdutoServicoOper() 

    /**
     * Method hasCdFinalidadeEnderecoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFinalidadeEnderecoContrato()
    {
        return this._has_cdFinalidadeEnderecoContrato;
    } //-- boolean hasCdFinalidadeEnderecoContrato() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdProdutoOperRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperRelacionado()
    {
        return this._has_cdProdutoOperRelacionado;
    } //-- boolean hasCdProdutoOperRelacionado() 

    /**
     * Method hasCdProdutoServicoOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOper()
    {
        return this._has_cdProdutoServicoOper;
    } //-- boolean hasCdProdutoServicoOper() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFinalidadeEnderecoContrato'.
     * 
     * @param cdFinalidadeEnderecoContrato the value of field
     * 'cdFinalidadeEnderecoContrato'.
     */
    public void setCdFinalidadeEnderecoContrato(int cdFinalidadeEnderecoContrato)
    {
        this._cdFinalidadeEnderecoContrato = cdFinalidadeEnderecoContrato;
        this._has_cdFinalidadeEnderecoContrato = true;
    } //-- void setCdFinalidadeEnderecoContrato(int) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdProdutoOperRelacionado'.
     * 
     * @param cdProdutoOperRelacionado the value of field
     * 'cdProdutoOperRelacionado'.
     */
    public void setCdProdutoOperRelacionado(int cdProdutoOperRelacionado)
    {
        this._cdProdutoOperRelacionado = cdProdutoOperRelacionado;
        this._has_cdProdutoOperRelacionado = true;
    } //-- void setCdProdutoOperRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOper'.
     * 
     * @param cdProdutoServicoOper the value of field
     * 'cdProdutoServicoOper'.
     */
    public void setCdProdutoServicoOper(int cdProdutoServicoOper)
    {
        this._cdProdutoServicoOper = cdProdutoServicoOper;
        this._has_cdProdutoServicoOper = true;
    } //-- void setCdProdutoServicoOper(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'dsFinalidadeEnderecoContrato'.
     * 
     * @param dsFinalidadeEnderecoContrato the value of field
     * 'dsFinalidadeEnderecoContrato'.
     */
    public void setDsFinalidadeEnderecoContrato(java.lang.String dsFinalidadeEnderecoContrato)
    {
        this._dsFinalidadeEnderecoContrato = dsFinalidadeEnderecoContrato;
    } //-- void setDsFinalidadeEnderecoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsOperacaoProdutoServico'.
     * 
     * @param dsOperacaoProdutoServico the value of field
     * 'dsOperacaoProdutoServico'.
     */
    public void setDsOperacaoProdutoServico(java.lang.String dsOperacaoProdutoServico)
    {
        this._dsOperacaoProdutoServico = dsOperacaoProdutoServico;
    } //-- void setDsOperacaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperRelacionado'.
     * 
     * @param dsProdutoOperRelacionado the value of field
     * 'dsProdutoOperRelacionado'.
     */
    public void setDsProdutoOperRelacionado(java.lang.String dsProdutoOperRelacionado)
    {
        this._dsProdutoOperRelacionado = dsProdutoOperRelacionado;
    } //-- void setDsProdutoOperRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOper'.
     * 
     * @param dsProdutoServicoOper the value of field
     * 'dsProdutoServicoOper'.
     */
    public void setDsProdutoServicoOper(java.lang.String dsProdutoServicoOper)
    {
        this._dsProdutoServicoOper = dsProdutoServicoOper;
    } //-- void setDsProdutoServicoOper(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarfinalidadeenderecooperacao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
