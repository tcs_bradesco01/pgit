package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/02/16.
 */
public class ListarLayoutsContratoSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem;

    /** The mensagem. */
    private String mensagem;

    /** The nr linhas. */
    private Integer nrLinhas;

    /** The ocorrencias. */
    private List<ListarLayoutsContratoOcorrenciasSaidaDTO> ocorrencias;

    /**
     * Set cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Set mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Set nr linhas.
     *
     * @param nrLinhas the nr linhas
     */
    public void setNrLinhas(Integer nrLinhas) {
        this.nrLinhas = nrLinhas;
    }

    /**
     * Get nr linhas.
     *
     * @return the nr linhas
     */
    public Integer getNrLinhas() {
        return this.nrLinhas;
    }

    /**
     * Set ocorrencias.
     *
     * @param ocorrencias the ocorrencias
     */
    public void setOcorrencias(List<ListarLayoutsContratoOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    /**
     * Get ocorrencias.
     *
     * @return the ocorrencias
     */
    public List<ListarLayoutsContratoOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}