/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Nome: DetalharPagamentosConsolidadosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class DetalharPagamentosConsolidadosSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem = null;

	/** Atributo mensagem. */
	private String mensagem = null;

	/** Atributo dsBancoDebito. */
	private String dsBancoDebito = null;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito = null;

	/** Atributo dsTipoContaDebito. */
	private String dsTipoContaDebito = null;

	/** Atributo dsContrato. */
	private String dsContrato = null;

	/** Atributo dsSituacaoContrato. */
	private String dsSituacaoContrato = null;

	/** Atributo nomeCliente. */
	private String nomeCliente = null;

	/** Atributo numeroConsultas. */
	private Integer numeroConsultas = null;

	/** Atributo nrPagamento. */
	private String nrPagamento = null;

	/** Atributo vlPagamento. */
	private BigDecimal vlPagamento = null;

	/** Atributo cdCnpjCpfFavorecido. */
	private Long cdCnpjCpfFavorecido = null;

	/** Atributo cdFilialCnpjCpfFavorecido. */
	private Integer cdFilialCnpjCpfFavorecido = null;

	/** Atributo cdControleCnpjCpfFavorecido. */
	private Integer cdControleCnpjCpfFavorecido = null;

	/** Atributo cdFavorecido. */
	private Long cdFavorecido = null;

	/** Atributo dsBeneficio. */
	private String dsBeneficio = null;

	/** Atributo cdBanco. */
	private Integer cdBanco = null;

	/** Atributo cdAgencia. */
	private Integer cdAgencia = null;

	/** Atributo cdDigitoAgencia. */
	private String cdDigitoAgencia = null;

	/** Atributo cdConta. */
	private Long cdConta = null;

	/** Atributo cdDigitoConta. */
	private String cdDigitoConta = null;

	/** Atributo cdTipoCanal. */
	private Integer cdTipoCanal = null;

	/** Atributo cdTipoTela. */
	private Integer cdTipoTela = null;

	/** Atributo dsEfetivacaoPagamento. */
	private String dsEfetivacaoPagamento = null;

	/** Atributo bancoAgenciaContaCreditoFormatado. */
	private String bancoAgenciaContaCreditoFormatado = null;

	/** Atributo bancoFormatado. */
	private String bancoFormatado = null;

	/** Atributo agenciaFormatada. */
	private String agenciaFormatada = null;

	/** Atributo contaFormatada. */
	private String contaFormatada = null;
	
	private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;

	/** Atributo listaOcorrecnias. */
	private List<OcorrenciasDetalharPagamentosConsolidados> listaOcorrecnias = 
	    new ArrayList<OcorrenciasDetalharPagamentosConsolidados>();

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}

	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}

	public String getDsContrato() {
		return dsContrato;
	}

	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}

	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}

	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}

	public String getNrPagamento() {
		return nrPagamento;
	}

	public void setNrPagamento(String nrPagamento) {
		this.nrPagamento = nrPagamento;
	}

	public BigDecimal getVlPagamento() {
		return vlPagamento;
	}

	public void setVlPagamento(BigDecimal vlPagamento) {
		this.vlPagamento = vlPagamento;
	}

	public Long getCdCnpjCpfFavorecido() {
		return cdCnpjCpfFavorecido;
	}

	public void setCdCnpjCpfFavorecido(Long cdCnpjCpfFavorecido) {
		this.cdCnpjCpfFavorecido = cdCnpjCpfFavorecido;
	}

	public Integer getCdFilialCnpjCpfFavorecido() {
		return cdFilialCnpjCpfFavorecido;
	}

	public void setCdFilialCnpjCpfFavorecido(Integer cdFilialCnpjCpfFavorecido) {
		this.cdFilialCnpjCpfFavorecido = cdFilialCnpjCpfFavorecido;
	}

	public Integer getCdControleCnpjCpfFavorecido() {
		return cdControleCnpjCpfFavorecido;
	}

	public void setCdControleCnpjCpfFavorecido(
			Integer cdControleCnpjCpfFavorecido) {
		this.cdControleCnpjCpfFavorecido = cdControleCnpjCpfFavorecido;
	}

	public Long getCdFavorecido() {
		return cdFavorecido;
	}

	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	public String getDsBeneficio() {
		return dsBeneficio;
	}

	public void setDsBeneficio(String dsBeneficio) {
		this.dsBeneficio = dsBeneficio;
	}

	public Integer getCdBanco() {
		return cdBanco;
	}

	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	public Integer getCdAgencia() {
		return cdAgencia;
	}

	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	public String getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}

	public void setCdDigitoAgencia(String cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	public Long getCdConta() {
		return cdConta;
	}

	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}

	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}

	public Integer getCdTipoTela() {
		return cdTipoTela;
	}

	public void setCdTipoTela(Integer cdTipoTela) {
		this.cdTipoTela = cdTipoTela;
	}

	public String getDsEfetivacaoPagamento() {
		return dsEfetivacaoPagamento;
	}

	public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
		this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
	}

	public String getBancoAgenciaContaCreditoFormatado() {
		return bancoAgenciaContaCreditoFormatado;
	}

	public void setBancoAgenciaContaCreditoFormatado(
			String bancoAgenciaContaCreditoFormatado) {
		this.bancoAgenciaContaCreditoFormatado = bancoAgenciaContaCreditoFormatado;
	}

	public String getBancoFormatado() {
		return bancoFormatado;
	}

	public void setBancoFormatado(String bancoFormatado) {
		this.bancoFormatado = bancoFormatado;
	}

	public String getAgenciaFormatada() {
		return agenciaFormatada;
	}

	public void setAgenciaFormatada(String agenciaFormatada) {
		this.agenciaFormatada = agenciaFormatada;
	}

	public String getContaFormatada() {
		return contaFormatada;
	}

	public void setContaFormatada(String contaFormatada) {
		this.contaFormatada = contaFormatada;
	}

	public List<OcorrenciasDetalharPagamentosConsolidados> getListaOcorrecnias() {
		return listaOcorrecnias;
	}

	public void setListaOcorrecnias(
			List<OcorrenciasDetalharPagamentosConsolidados> listaOcorrecnias) {
		this.listaOcorrecnias = listaOcorrecnias;
	}

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}

}