/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.math.BigDecimal;

/**
 * The Class DetalharPiramideOcorrencias2SaidaDTO.
 */
public class DetalharPiramideOcorrencias2SaidaDTO {

    /** The cd faixa renov quest. */
    private Integer cdFaixaRenovQuest;

    /** The ds faixa renov quest. */
    private String dsFaixaRenovQuest;

    /** The qtd func casa quest. */
    private Long qtdFuncCasaQuest;

    /** The qtd func deslg quest. */
    private Long qtdFuncDeslgQuest;

    /** The ptc renov faixa quest. */
    private BigDecimal ptcRenovFaixaQuest;

    /**
     * Set cd faixa renov quest.
     * 
     * @param cdFaixaRenovQuest
     *            the cd faixa renov quest
     */
    public void setCdFaixaRenovQuest(Integer cdFaixaRenovQuest) {
        this.cdFaixaRenovQuest = cdFaixaRenovQuest;
    }

    /**
     * Get cd faixa renov quest.
     * 
     * @return the cd faixa renov quest
     */
    public Integer getCdFaixaRenovQuest() {
        return this.cdFaixaRenovQuest;
    }

    /**
     * Set ds faixa renov quest.
     * 
     * @param dsFaixaRenovQuest
     *            the ds faixa renov quest
     */
    public void setDsFaixaRenovQuest(String dsFaixaRenovQuest) {
        this.dsFaixaRenovQuest = dsFaixaRenovQuest;
    }

    /**
     * Get ds faixa renov quest.
     * 
     * @return the ds faixa renov quest
     */
    public String getDsFaixaRenovQuest() {
        return this.dsFaixaRenovQuest;
    }

    /**
     * Set qtd func casa quest.
     * 
     * @param qtdFuncCasaQuest
     *            the qtd func casa quest
     */
    public void setQtdFuncCasaQuest(Long qtdFuncCasaQuest) {
        this.qtdFuncCasaQuest = qtdFuncCasaQuest;
    }

    /**
     * Get qtd func casa quest.
     * 
     * @return the qtd func casa quest
     */
    public Long getQtdFuncCasaQuest() {
        return this.qtdFuncCasaQuest;
    }

    /**
     * Set qtd func deslg quest.
     * 
     * @param qtdFuncDeslgQuest
     *            the qtd func deslg quest
     */
    public void setQtdFuncDeslgQuest(Long qtdFuncDeslgQuest) {
        this.qtdFuncDeslgQuest = qtdFuncDeslgQuest;
    }

    /**
     * Get qtd func deslg quest.
     * 
     * @return the qtd func deslg quest
     */
    public Long getQtdFuncDeslgQuest() {
        return this.qtdFuncDeslgQuest;
    }

    /**
     * Set ptc renov faixa quest.
     * 
     * @param ptcRenovFaixaQuest
     *            the ptc renov faixa quest
     */
    public void setPtcRenovFaixaQuest(BigDecimal ptcRenovFaixaQuest) {
        this.ptcRenovFaixaQuest = ptcRenovFaixaQuest;
    }

    /**
     * Get ptc renov faixa quest.
     * 
     * @return the ptc renov faixa quest
     */
    public BigDecimal getPtcRenovFaixaQuest() {
        return this.ptcRenovFaixaQuest;
    }
}