/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharVincConvnCtaSalarioResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharVincConvnCtaSalarioResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _dsConvCtaSalarial
     */
    private java.lang.String _dsConvCtaSalarial;

    /**
     * Field _cdSitConvCta
     */
    private int _cdSitConvCta = 0;

    /**
     * keeps track of state for field: _cdSitConvCta
     */
    private boolean _has_cdSitConvCta;

    /**
     * Field _cdBcoEmprConvn
     */
    private int _cdBcoEmprConvn = 0;

    /**
     * keeps track of state for field: _cdBcoEmprConvn
     */
    private boolean _has_cdBcoEmprConvn;

    /**
     * Field _cdAgenciaEmprConvn
     */
    private int _cdAgenciaEmprConvn = 0;

    /**
     * keeps track of state for field: _cdAgenciaEmprConvn
     */
    private boolean _has_cdAgenciaEmprConvn;

    /**
     * Field _cdCtaEmprConvn
     */
    private long _cdCtaEmprConvn = 0;

    /**
     * keeps track of state for field: _cdCtaEmprConvn
     */
    private boolean _has_cdCtaEmprConvn;

    /**
     * Field _cdDigitoEmprConvn
     */
    private java.lang.String _cdDigitoEmprConvn;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdOperacaoFluxoInclusao
     */
    private java.lang.String _cdOperacaoFluxoInclusao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;

    /**
     * Field _cdOperacaoFluxoManutencao
     */
    private java.lang.String _cdOperacaoFluxoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharVincConvnCtaSalarioResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.response.DetalharVincConvnCtaSalarioResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaEmprConvn
     * 
     */
    public void deleteCdAgenciaEmprConvn()
    {
        this._has_cdAgenciaEmprConvn= false;
    } //-- void deleteCdAgenciaEmprConvn() 

    /**
     * Method deleteCdBcoEmprConvn
     * 
     */
    public void deleteCdBcoEmprConvn()
    {
        this._has_cdBcoEmprConvn= false;
    } //-- void deleteCdBcoEmprConvn() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdCtaEmprConvn
     * 
     */
    public void deleteCdCtaEmprConvn()
    {
        this._has_cdCtaEmprConvn= false;
    } //-- void deleteCdCtaEmprConvn() 

    /**
     * Method deleteCdSitConvCta
     * 
     */
    public void deleteCdSitConvCta()
    {
        this._has_cdSitConvCta= false;
    } //-- void deleteCdSitConvCta() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdAgenciaEmprConvn'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaEmprConvn'.
     */
    public int getCdAgenciaEmprConvn()
    {
        return this._cdAgenciaEmprConvn;
    } //-- int getCdAgenciaEmprConvn() 

    /**
     * Returns the value of field 'cdBcoEmprConvn'.
     * 
     * @return int
     * @return the value of field 'cdBcoEmprConvn'.
     */
    public int getCdBcoEmprConvn()
    {
        return this._cdBcoEmprConvn;
    } //-- int getCdBcoEmprConvn() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdCtaEmprConvn'.
     * 
     * @return long
     * @return the value of field 'cdCtaEmprConvn'.
     */
    public long getCdCtaEmprConvn()
    {
        return this._cdCtaEmprConvn;
    } //-- long getCdCtaEmprConvn() 

    /**
     * Returns the value of field 'cdDigitoEmprConvn'.
     * 
     * @return String
     * @return the value of field 'cdDigitoEmprConvn'.
     */
    public java.lang.String getCdDigitoEmprConvn()
    {
        return this._cdDigitoEmprConvn;
    } //-- java.lang.String getCdDigitoEmprConvn() 

    /**
     * Returns the value of field 'cdOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoFluxoInclusao'.
     */
    public java.lang.String getCdOperacaoFluxoInclusao()
    {
        return this._cdOperacaoFluxoInclusao;
    } //-- java.lang.String getCdOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'cdOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoFluxoManutencao'.
     */
    public java.lang.String getCdOperacaoFluxoManutencao()
    {
        return this._cdOperacaoFluxoManutencao;
    } //-- java.lang.String getCdOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'cdSitConvCta'.
     * 
     * @return int
     * @return the value of field 'cdSitConvCta'.
     */
    public int getCdSitConvCta()
    {
        return this._cdSitConvCta;
    } //-- int getCdSitConvCta() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsConvCtaSalarial'.
     * 
     * @return String
     * @return the value of field 'dsConvCtaSalarial'.
     */
    public java.lang.String getDsConvCtaSalarial()
    {
        return this._dsConvCtaSalarial;
    } //-- java.lang.String getDsConvCtaSalarial() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdAgenciaEmprConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaEmprConvn()
    {
        return this._has_cdAgenciaEmprConvn;
    } //-- boolean hasCdAgenciaEmprConvn() 

    /**
     * Method hasCdBcoEmprConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBcoEmprConvn()
    {
        return this._has_cdBcoEmprConvn;
    } //-- boolean hasCdBcoEmprConvn() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdCtaEmprConvn
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtaEmprConvn()
    {
        return this._has_cdCtaEmprConvn;
    } //-- boolean hasCdCtaEmprConvn() 

    /**
     * Method hasCdSitConvCta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSitConvCta()
    {
        return this._has_cdSitConvCta;
    } //-- boolean hasCdSitConvCta() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaEmprConvn'.
     * 
     * @param cdAgenciaEmprConvn the value of field
     * 'cdAgenciaEmprConvn'.
     */
    public void setCdAgenciaEmprConvn(int cdAgenciaEmprConvn)
    {
        this._cdAgenciaEmprConvn = cdAgenciaEmprConvn;
        this._has_cdAgenciaEmprConvn = true;
    } //-- void setCdAgenciaEmprConvn(int) 

    /**
     * Sets the value of field 'cdBcoEmprConvn'.
     * 
     * @param cdBcoEmprConvn the value of field 'cdBcoEmprConvn'.
     */
    public void setCdBcoEmprConvn(int cdBcoEmprConvn)
    {
        this._cdBcoEmprConvn = cdBcoEmprConvn;
        this._has_cdBcoEmprConvn = true;
    } //-- void setCdBcoEmprConvn(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdCtaEmprConvn'.
     * 
     * @param cdCtaEmprConvn the value of field 'cdCtaEmprConvn'.
     */
    public void setCdCtaEmprConvn(long cdCtaEmprConvn)
    {
        this._cdCtaEmprConvn = cdCtaEmprConvn;
        this._has_cdCtaEmprConvn = true;
    } //-- void setCdCtaEmprConvn(long) 

    /**
     * Sets the value of field 'cdDigitoEmprConvn'.
     * 
     * @param cdDigitoEmprConvn the value of field
     * 'cdDigitoEmprConvn'.
     */
    public void setCdDigitoEmprConvn(java.lang.String cdDigitoEmprConvn)
    {
        this._cdDigitoEmprConvn = cdDigitoEmprConvn;
    } //-- void setCdDigitoEmprConvn(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoFluxoInclusao'.
     * 
     * @param cdOperacaoFluxoInclusao the value of field
     * 'cdOperacaoFluxoInclusao'.
     */
    public void setCdOperacaoFluxoInclusao(java.lang.String cdOperacaoFluxoInclusao)
    {
        this._cdOperacaoFluxoInclusao = cdOperacaoFluxoInclusao;
    } //-- void setCdOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoFluxoManutencao'.
     * 
     * @param cdOperacaoFluxoManutencao the value of field
     * 'cdOperacaoFluxoManutencao'.
     */
    public void setCdOperacaoFluxoManutencao(java.lang.String cdOperacaoFluxoManutencao)
    {
        this._cdOperacaoFluxoManutencao = cdOperacaoFluxoManutencao;
    } //-- void setCdOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdSitConvCta'.
     * 
     * @param cdSitConvCta the value of field 'cdSitConvCta'.
     */
    public void setCdSitConvCta(int cdSitConvCta)
    {
        this._cdSitConvCta = cdSitConvCta;
        this._has_cdSitConvCta = true;
    } //-- void setCdSitConvCta(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsConvCtaSalarial'.
     * 
     * @param dsConvCtaSalarial the value of field
     * 'dsConvCtaSalarial'.
     */
    public void setDsConvCtaSalarial(java.lang.String dsConvCtaSalarial)
    {
        this._dsConvCtaSalarial = dsConvCtaSalarial;
    } //-- void setDsConvCtaSalarial(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharVincConvnCtaSalarioResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.response.DetalharVincConvnCtaSalarioResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.response.DetalharVincConvnCtaSalarioResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.response.DetalharVincConvnCtaSalarioResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharvincconvnctasalario.response.DetalharVincConvnCtaSalarioResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
