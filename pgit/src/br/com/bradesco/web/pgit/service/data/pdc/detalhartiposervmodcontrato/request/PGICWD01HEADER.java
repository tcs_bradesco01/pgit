/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class PGICWD01HEADER.
 * 
 * @version $Revision$ $Date$
 */
public class PGICWD01HEADER implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _PGICWD01CODLAYOUT
     */
    private java.lang.String _PGICWD01CODLAYOUT = "PGICWD01";

    /**
     * Field _PGICWD01TAMLAYOUT
     */
    private int _PGICWD01TAMLAYOUT = 00053;

    /**
     * keeps track of state for field: _PGICWD01TAMLAYOUT
     */
    private boolean _has_PGICWD01TAMLAYOUT;


      //----------------/
     //- Constructors -/
    //----------------/

    public PGICWD01HEADER() 
     {
        super();
        setPGICWD01CODLAYOUT("PGICWD01");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.request.PGICWD01HEADER()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deletePGICWD01TAMLAYOUT
     * 
     */
    public void deletePGICWD01TAMLAYOUT()
    {
        this._has_PGICWD01TAMLAYOUT= false;
    } //-- void deletePGICWD01TAMLAYOUT() 

    /**
     * Returns the value of field 'PGICWD01CODLAYOUT'.
     * 
     * @return String
     * @return the value of field 'PGICWD01CODLAYOUT'.
     */
    public java.lang.String getPGICWD01CODLAYOUT()
    {
        return this._PGICWD01CODLAYOUT;
    } //-- java.lang.String getPGICWD01CODLAYOUT() 

    /**
     * Returns the value of field 'PGICWD01TAMLAYOUT'.
     * 
     * @return int
     * @return the value of field 'PGICWD01TAMLAYOUT'.
     */
    public int getPGICWD01TAMLAYOUT()
    {
        return this._PGICWD01TAMLAYOUT;
    } //-- int getPGICWD01TAMLAYOUT() 

    /**
     * Method hasPGICWD01TAMLAYOUT
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPGICWD01TAMLAYOUT()
    {
        return this._has_PGICWD01TAMLAYOUT;
    } //-- boolean hasPGICWD01TAMLAYOUT() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'PGICWD01CODLAYOUT'.
     * 
     * @param PGICWD01CODLAYOUT the value of field
     * 'PGICWD01CODLAYOUT'.
     */
    public void setPGICWD01CODLAYOUT(java.lang.String PGICWD01CODLAYOUT)
    {
        this._PGICWD01CODLAYOUT = PGICWD01CODLAYOUT;
    } //-- void setPGICWD01CODLAYOUT(java.lang.String) 

    /**
     * Sets the value of field 'PGICWD01TAMLAYOUT'.
     * 
     * @param PGICWD01TAMLAYOUT the value of field
     * 'PGICWD01TAMLAYOUT'.
     */
    public void setPGICWD01TAMLAYOUT(int PGICWD01TAMLAYOUT)
    {
        this._PGICWD01TAMLAYOUT = PGICWD01TAMLAYOUT;
        this._has_PGICWD01TAMLAYOUT = true;
    } //-- void setPGICWD01TAMLAYOUT(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return PGICWD01HEADER
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.request.PGICWD01HEADER unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.request.PGICWD01HEADER) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.request.PGICWD01HEADER.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalhartiposervmodcontrato.request.PGICWD01HEADER unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
