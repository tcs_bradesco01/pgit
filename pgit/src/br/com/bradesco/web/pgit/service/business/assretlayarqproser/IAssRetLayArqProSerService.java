/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.assretlayarqproser;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ConsultarAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ConsultarAssVersaoLayoutLoteServicoSaidaDto;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.DetalharAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.DetalharAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssVersaoLayoutLoteServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssVersaoLayoutLoteServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssVersaoLayoutLoteServicoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: AssRetLayArqProSer
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IAssRetLayArqProSerService {

    /**
     * Listar ass ret lay arq pro ser.
     * 
     * @param listarAssRetLayArqProSerEntradaDTO
     *            the listar ass ret lay arq pro ser entrada dto
     * @return the list< listar ass ret lay arq pro ser saida dt o>
     */
    List<ListarAssRetLayArqProSerSaidaDTO> listarAssRetLayArqProSer(
        ListarAssRetLayArqProSerEntradaDTO listarAssRetLayArqProSerEntradaDTO);

    /**
     * Detalhar ass ret lay arq pro ser.
     * 
     * @param detalharAssRetLayArqProSerEntradaDTO
     *            the detalhar ass ret lay arq pro ser entrada dto
     * @return the detalhar ass ret lay arq pro ser saida dto
     */
    DetalharAssRetLayArqProSerSaidaDTO detalharAssRetLayArqProSer(
        DetalharAssRetLayArqProSerEntradaDTO detalharAssRetLayArqProSerEntradaDTO);

    /**
     * Incluir ass ret lay arq pro ser.
     * 
     * @param incluirAssRetLayArqProSerEntradaDTO
     *            the incluir ass ret lay arq pro ser entrada dto
     * @return the incluir ass ret lay arq pro ser saida dto
     */
    IncluirAssRetLayArqProSerSaidaDTO incluirAssRetLayArqProSer(
        IncluirAssRetLayArqProSerEntradaDTO incluirAssRetLayArqProSerEntradaDTO);

    /**
     * Excluir ass ret lay arq pro ser.
     * 
     * @param excluirAssRetLayArqProSerEntradaDTO
     *            the excluir ass ret lay arq pro ser entrada dto
     * @return the excluir ass ret lay arq pro ser saida dto
     */
    ExcluirAssRetLayArqProSerSaidaDTO excluirAssRetLayArqProSer(
        ExcluirAssRetLayArqProSerEntradaDTO excluirAssRetLayArqProSerEntradaDTO);

    /**
     * M�todo de exemplo.
     */
    void sampleAssRetLayArqProSer();

    /**
     * Incluir ass ret lay arq pro ser.
     * 
     * @param IncluirAssVersaoLayoutLoteServicoEntradaDTO
     *            the incluir ass versao layout lote servico entrada dto
     * @return the incluir ass versao layout lote servico saida dto
     */
    IncluirAssVersaoLayoutLoteServicoSaidaDTO incluirAssVersaoLayoutLoteServico(
        IncluirAssVersaoLayoutLoteServicoEntradaDTO entrada);

    /**
     * Excluir ass ret lay arq pro ser.
     * 
     * @param ExcluirAssVersaoLayoutLoteServicoEntradaDTO
     *            the exclusao ass versao layout lote servico entrada dto
     * @return the exclusao ass versao layout lote servico saida dto
     */
    ExcluirAssVersaoLayoutLoteServicoSaidaDTO excluirAssVersaoLayoutLoteServico(
        ExcluirAssVersaoLayoutLoteServicoEntradaDTO entrada);

    /**
     * Listar ass ret lay arq pro ser.
     * 
     * @param ListarAssRetLayArqProSerEntradaDTO
     *            the listar ass ret lay arq pro ser entrada dto
     * @return the listar ass ret lay arq pro ser saida dto
     */
    ListarAssVersaoLayoutLoteServicoSaidaDTO listarAssVersaoLayoutLoteServico(
        ListarAssVersaoLayoutLoteServicoEntradaDTO entrada);

    /**
     * Consulta ass ret lay arq pro ser.
     * 
     * @param ConsultarAssVersaoLayoutLoteServicoEntradaDTO
     *            the consultar ass ret lay arq pro ser entrada dto
     * @return the consultar ass ret lay arq pro ser saida dto
     */
    ConsultarAssVersaoLayoutLoteServicoSaidaDto ConsultarAssVersaoLayoutLoteServico(
        ConsultarAssVersaoLayoutLoteServicoEntradaDTO entrada);
}
