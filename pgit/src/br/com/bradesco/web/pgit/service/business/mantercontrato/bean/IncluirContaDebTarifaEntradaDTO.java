package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.List;

public class IncluirContaDebTarifaEntradaDTO {
	
    private Long cdPessoaJuridicaNegocio;
    private Integer cdTipoContratoNegocio;
    private Long nrSequenciaContratoNegocio;
    private Long cdPessoaJuridicaVinculo;
    private Integer cdTipoContratoVinculo;
    private Long nrSequenciaContratoVinculo;
    private Integer cdTipoVincContrato;
    private Integer cdTipoOperacao;
    private Integer nrOcorrencias;
    private List<IncluirContaDebTarifaOcorrencias> ocorrencias;
    
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}
	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}
	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}
	public Integer getCdTipoVincContrato() {
		return cdTipoVincContrato;
	}
	public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
		this.cdTipoVincContrato = cdTipoVincContrato;
	}
	public Integer getCdTipoOperacao() {
		return cdTipoOperacao;
	}
	public void setCdTipoOperacao(Integer cdTipoOperacao) {
		this.cdTipoOperacao = cdTipoOperacao;
	}
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	public List<IncluirContaDebTarifaOcorrencias> getOcorrencias() {
		return ocorrencias;
	}
	public void setOcorrencias(List<IncluirContaDebTarifaOcorrencias> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

}
