/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.AlterarLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.AlterarLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharHistLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharHistLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ExcluirLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ExcluirLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.IncluirLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.IncluirLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarHistLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarHistLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarLiquidacaoPagamentoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarliquidacaopagamento.request.AlterarLiquidacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarliquidacaopagamento.response.AlterarLiquidacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.request.DetalharHistLiquidacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistliquidacaopagamento.response.DetalharHistLiquidacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.request.DetalharLiquidacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.response.DetalharLiquidacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.request.ExcluirLiquidacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirliquidacaopagamento.response.ExcluirLiquidacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.request.IncluirLiquidacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirliquidacaopagamento.response.IncluirLiquidacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistliquidacaopagamento.request.ListarHistLiquidacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistliquidacaopagamento.response.ListarHistLiquidacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.request.ListarLiquidacaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.ListarLiquidacaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarliquidacaopagamento.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterFormaLiquidacaoPagamentoIntegrado
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class FormaLiqPagtoIntServiceImpl implements IFormaLiqPgtoIntService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService#consultarLiquidacaoPagto(br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarLiquidacaoPagamentoEntradaDTO)
     */
    public ListarLiquidacaoPagamentoSaidaDTO consultarLiquidacaoPagto(ListarLiquidacaoPagamentoEntradaDTO entrada) {
    	ListarLiquidacaoPagamentoRequest request = new ListarLiquidacaoPagamentoRequest();
    	request.setCdFormaLiquidacao(entrada.getCdFormaLiquidacao() == null ? 0:entrada.getCdFormaLiquidacao());
    	request.setDtFinalVigencia(entrada.getDtFinalVigencia() == null ? "":entrada.getDtFinalVigencia());
    	request.setDtInicioVigencia(entrada.getDtInicioVigencia() == null ? "":entrada.getDtInicioVigencia() );
    	request.setHrInclusaoRegistroHist(entrada.getHrInclusaoRegistroHist() == null ? "":entrada.getHrInclusaoRegistroHist());
    	request.setHrConsultaFolhaPgto("");
    	request.setHrConsultasSaldoPagamento("");
    	request.setQtConsultas(100);

    	ListarLiquidacaoPagamentoResponse response = getFactoryAdapter().getListarLiquidacaoPagamentoPDCAdapter().invokeProcess(request);

    	List<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO> list = new ArrayList<ListarLiquidacaoPagamentoOcorrenciaSaidaDTO>();

    	ListarLiquidacaoPagamentoOcorrenciaSaidaDTO ocorrencia = null;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			Ocorrencias occor = response.getOcorrencias(i);

    		ocorrencia = new ListarLiquidacaoPagamentoOcorrenciaSaidaDTO();
    		ocorrencia.setDsFormaLiquidacao(occor.getDsFormaLiquidacao());
			ocorrencia.setCdControleHora(occor.getCdControleHora());
			ocorrencia.setCdFormaLiquidacao(occor.getCdFormaLiquidacao());
			ocorrencia.setCdPrioridadeDebito(occor.getCdPrioridadeDebito());
			ocorrencia.setCdSistema(occor.getCdSistema());
			ocorrencia.setHrConsultaFolhaPgto(occor.getHrConsultaFolhaPgto());
			ocorrencia.setHrConsultasSaldoPagamento(occor.getHrConsultasSaldoPagamento());
			ocorrencia.setHrLimiteProcessamento(occor.getHrLimiteProcessamento());
			ocorrencia.setQtMinutosMargemSeguranca(occor.getQtMinutosMargemSeguranca());
			if(request.getCdFormaLiquidacao() == 0 && occor.getDsFormaLiquidacao().equalsIgnoreCase("T�TULOS OUTROS BANCOS")){
				ocorrencia.setQtMinutosValorSuperior(occor.getQtMinutosValorSuperior());
				ocorrencia.setHrLimiteValorSuperior(extrair5DigitosHorario(occor.getHrLimiteValorSuperior()));
				
			}else if(request.getCdFormaLiquidacao() == 24){
				ocorrencia.setQtMinutosValorSuperior(occor.getQtMinutosValorSuperior());
				ocorrencia.setHrLimiteValorSuperior(extrair5DigitosHorario(occor.getHrLimiteValorSuperior()));
				
			}else{
				ocorrencia.setQtMinutosValorSuperior(null);
				ocorrencia.setHrLimiteValorSuperior("");
			}
			ocorrencia.setQtdTempoAgendamentoCobranca(occor.getQtdTempoAgendamentoCobranca());
			ocorrencia.setQtdTempoEfetivacaoCiclicaCobranca(occor.getQtdTempoEfetivacaoCiclicaCobranca());
			ocorrencia.setQtdTempoEfetivacaoDiariaCobranca(occor.getQtdTempoEfetivacaoDiariaCobranca());
			ocorrencia.setQtdLimiteProcessamentoExcedido(occor.getQtdLimiteSemResposta());			
			list.add(ocorrencia);
    	}

    	ListarLiquidacaoPagamentoSaidaDTO saida = new ListarLiquidacaoPagamentoSaidaDTO();
    	saida.setOcorrencias(list);
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    	saida.setNumeroConsultas(response.getNumeroConsultas());

    	return saida;
    }

    /**
     * Extrair5 digitos horario.
     *
     * @param horario the horario
     * @return the string
     */
    private String extrair5DigitosHorario(String horario) {
    	if (horario == null || horario.trim().equals("") || horario.trim().length() < 5) {
			return "00:00";
		}

    	return horario.substring(0, 5);
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService#incluirLiqPagamentoIntegrado(br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.IncluirLiquidacaoPagamentoEntradaDTO)
     */
    public IncluirLiquidacaoPagamentoSaidaDTO incluirLiqPagamentoIntegrado(IncluirLiquidacaoPagamentoEntradaDTO entrada) {
    	IncluirLiquidacaoPagamentoRequest request = new IncluirLiquidacaoPagamentoRequest();
	    request.setCdControleHoraLimite(PgitUtil.verificaIntegerNulo(entrada.getCdControleHoraLimite()));
	    request.setCdFormaLiquidacao(PgitUtil.verificaIntegerNulo(entrada.getCdFormaLiquidacao()));
	    request.setCdPrioridadeDebForma(PgitUtil.verificaIntegerNulo(entrada.getCdPrioridadeDebForma()));
	    request.setCdSistema(PgitUtil.verificaStringNula(entrada.getCdSistema()));
	    request.setHrLimiteProcessamento(PgitUtil.verificaStringNula(entrada.getHrLimiteProcessamento()));
	    request.setQtMinutoMargemSeguranca(PgitUtil.verificaIntegerNulo(entrada.getQtMinutoMargemSeguranca()));
		request.setHrConsultasSaldoPagamento(PgitUtil.verificaStringNula(entrada.getHrConsultasSaldoPagamento()));
    	request.setHrConsultaFolhaPgto(PgitUtil.verificaStringNula(entrada.getHrConsultaFolhaPgto()));
    	request.setQtMinutosValorSuperior(PgitUtil.verificaIntegerNulo(entrada.getQtMinutosValorSuperior()));
    	request.setHrLimiteValorSuperior(PgitUtil.verificaStringNula(entrada.getHrLimiteValorSuperior()));
    	request.setQtdTempoAgendamentoCobranca(PgitUtil.verificaIntegerNulo(entrada.getQtdTempoAgendamentoCobranca()));
        request.setQtdTempoEfetivacaoCiclicaCobranca(PgitUtil.verificaIntegerNulo(entrada
            .getQtdTempoEfetivacaoCiclicaCobranca()));
        request.setQtdTempoEfetivacaoDiariaCobranca(PgitUtil.verificaIntegerNulo(entrada
            .getQtdTempoEfetivacaoDiariaCobranca()));
        request.setQtdLimiteSemResposta(PgitUtil.verificaIntegerNulo(entrada
                .getQtdLimiteProcessamentoExcedido()));

    	IncluirLiquidacaoPagamentoResponse response = factoryAdapter.getIncluirLiquidacaoPagamentoPDCAdapter().invokeProcess(request);

    	return new IncluirLiquidacaoPagamentoSaidaDTO(response.getCodMensagem(), response.getMensagem());
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService#excluirLiqPagamentoIntegrado(br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ExcluirLiquidacaoPagamentoEntradaDTO)
     */
    public ExcluirLiquidacaoPagamentoSaidaDTO  excluirLiqPagamentoIntegrado(ExcluirLiquidacaoPagamentoEntradaDTO entrada) {
    	
    	ExcluirLiquidacaoPagamentoRequest request = new ExcluirLiquidacaoPagamentoRequest();
    	
    	request.setCdFormaLiquidacao(entrada.getCdFormaLiquidacao());
    	request.setDtFinalVigencia(entrada.getDtFinalVigencia());
    	request.setDtInicioVigencia(entrada.getDtInicioVigencia());
    	request.setHrInclusaoRegistroHistorico(entrada.getHrInclusaoRegistroHistorico());
    	request.setNumeroOcorrencias(entrada.getNumeroOcorrencias());
    	request.setHrConsultasSaldoPagamento(entrada.getHrConsultasSaldoPagamento());
    	request.setHrConsultaFolhaPgto(entrada.getHrConsultaFolhaPgto());
    	
    	ExcluirLiquidacaoPagamentoResponse response = getFactoryAdapter().getExcluirLiquidacaoPagamentoPDCAdapter().invokeProcess(request);
    	
    	ExcluirLiquidacaoPagamentoSaidaDTO saida = new ExcluirLiquidacaoPagamentoSaidaDTO();
    	
    	
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    
    	
    	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService#detalheLiqPagamentoIntegrado(br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharLiquidacaoPagamentoEntradaDTO)
     */
    public DetalharLiquidacaoPagamentoSaidaDTO detalheLiqPagamentoIntegrado(DetalharLiquidacaoPagamentoEntradaDTO entrada) {
    	DetalharLiquidacaoPagamentoRequest request = new DetalharLiquidacaoPagamentoRequest();
    	request.setCdFormaLiquidacao(entrada.getCdFormaLiquidacao());
    	request.setDtFinalVigencia(entrada.getDtFinalVigencia());
    	request.setDtInicioVigencia(entrada.getDtInicioVigencia());
    	request.setHrInclusaoRegistroHistorico(entrada.getHrInclusaoRegistroHistorico());
    	request.setHrConsultasSaldoPagamento(entrada.getHrConsultasSaldoPagamento());
    	request.setHrConsultaFolhaPgto(entrada.getHrConsultaFolhaPgto());
    	request.setQtConsultas(entrada.getQtConsultas());

    	DetalharLiquidacaoPagamentoResponse response = getFactoryAdapter().getDetalharLiquidacaoPagamentoPDCAdapter().invokeProcess(request);

    	DetalharLiquidacaoPagamentoSaidaDTO saida = new DetalharLiquidacaoPagamentoSaidaDTO();
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    	saida.setHrConsultasSaldoPagamento(response.getHrConsultasSaldoPagamento());
    	saida.setHrConsultaFolhaPgto(response.getHrConsultaFolhaPgto());
    	saida.setCdAutenticacaoSegregacaoInclusao(response.getCdAutenticacaoSegregacaoInclusao());
    	saida.setCdAutenticacaoSegregacaoManutencao(response.getCdAutenticacaoSegregacaoManutencao());
    	saida.setCdCanalInclusao(response.getCdCanalInclusao());
    	saida.setCdCanalManutencao(response.getCdCanalManutencao());
    	saida.setCdControleHoraLimite(response.getCdControleHoraLimite());
    	saida.setCdFormaLiquidacao(response.getCdFormaLiquidacao());
    	saida.setCdPrioridadeDebito(response.getCdPrioridadeDebito());
    	saida.setCdSituacao(response.getCdSituacao());
    	saida.setDsFormaLiquidacao(response.getDsFormaLiquidacao());
    	saida.setDsSituacao(response.getDsSituacao());
    	saida.setHrConsultaFolhaPgto(response.getHrConsultaFolhaPgto());
    	saida.setHrConsultasSaldoPagamento(response.getHrConsultasSaldoPagamento());
    	saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
    	saida.setHrLimiteProcessamentoPagamento(response.getHrLimiteProcessamentoPagamento());
    	saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
    	saida.setNmOperacaoFluxoInlcusao(response.getNmOperacaoFluxoInlcusao());
    	saida.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
    	saida.setQtMinutosMargemSeguranca(response.getQtMinutosMargemSeguranca());
    	saida.setDsCanalInclusao(response.getDsCanalInclusao());
    	saida.setDsCanalManutencao(response.getDsCanalManutencao());
    	saida.setHrConsultasSaldoPagamento(response.getHrConsultasSaldoPagamento());
    	saida.setHrConsultaFolhaPgto(response.getHrConsultaFolhaPgto());
	    if(request.getCdFormaLiquidacao() != 24 || !response.getDsFormaLiquidacao().equalsIgnoreCase("T�TULOS OUTROS BANCOS")){
	    	saida.setQtMinutosValorSuperior(null);
	    	saida.setHrLimiteValorSuperior("");
	    }else{
	    	saida.setQtMinutosValorSuperior(response.getQtMinutosValorSuperior());
	    	saida.setHrLimiteValorSuperior(extrair5DigitosHorario(response.getHrLimiteValorSuperior()));
	    }
	    saida.setQtdTempoAgendamentoCobranca(response.getQtdTempoAgendamentoCobranca());
	    saida.setQtdTempoEfetivacaoCiclicaCobranca(response.getQtdTempoEfetivacaoCiclicaCobranca());
	    saida.setQtdTempoEfetivacaoDiariaCobranca(response.getQtdTempoEfetivacaoDiariaCobranca());
	    saida.setQtdLimiteProcessamentoExcedido(response.getQtdLimiteSemResposta());
    	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService#consultarHistLiquidacaoPagto(br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.ListarHistLiquidacaoPagamentoEntradaDTO)
     */
    public List<ListarHistLiquidacaoPagamentoSaidaDTO> consultarHistLiquidacaoPagto(ListarHistLiquidacaoPagamentoEntradaDTO entrada) {
    	
    	ListarHistLiquidacaoPagamentoRequest request = new ListarHistLiquidacaoPagamentoRequest();
    	
    	request.setCdFormaLiquidacao(entrada.getCdFormaLiquidacao() == null ? 0:entrada.getCdFormaLiquidacao());
    	request.setDtFinalVigencia(entrada.getDtFinalVigencia());
    	request.setDtInicioVigencia(entrada.getDtInicioVigencia());
    	request.setHrInclusaoRegistroHistorico(entrada.getHrInclusaoRegistroHistorico());
    	request.setQtConsultas(entrada.getQtConsultas());
    	
    	
    	ListarHistLiquidacaoPagamentoResponse response = getFactoryAdapter().getListarHistLiquidacaoPagamentoPDCAdapter().invokeProcess(request);
    	List<ListarHistLiquidacaoPagamentoSaidaDTO> list = new ArrayList<ListarHistLiquidacaoPagamentoSaidaDTO>();
    	
    	for (br.com.bradesco.web.pgit.service.data.pdc.listarhistliquidacaopagamento.response.Ocorrencias ocorrencia : response.getOcorrencias()) {
    		list.add(new ListarHistLiquidacaoPagamentoSaidaDTO(ocorrencia));
		}
    	
    	return list;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService#detalheHistLiqPagamentoIntegrado(br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.DetalharHistLiquidacaoPagamentoEntradaDTO)
     */
    public DetalharHistLiquidacaoPagamentoSaidaDTO detalheHistLiqPagamentoIntegrado(DetalharHistLiquidacaoPagamentoEntradaDTO entrada) {
    	DetalharHistLiquidacaoPagamentoRequest request = new DetalharHistLiquidacaoPagamentoRequest();
    	request.setCdFormaLiquidacao(entrada.getCdFormaLiquidacao());
    	request.setDtFinalVigencia(entrada.getDtFinalVigencia());
    	request.setDtInicioVigencia(entrada.getDtInicioVigencia());
    	request.setHrInclusaoRegistroHistorico(entrada.getHrInclusaoRegistroHistorico());
    	request.setQtConsultas(entrada.getQtConsultas());
    	request.setHrConsultasSaldoPagamento(entrada.getHrConsultasSaldoPagamento());
    	request.setHrConsultaFolhaPgto(entrada.getHrConsultaFolhaPgto());

    	DetalharHistLiquidacaoPagamentoResponse response = getFactoryAdapter().getDetalharHistLiquidacaoPagamentoPDCAdapter().invokeProcess(request);

    	DetalharHistLiquidacaoPagamentoSaidaDTO saida = new DetalharHistLiquidacaoPagamentoSaidaDTO();
    	saida.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
    	saida.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
    	saida.setCdCanalInclusao(response.getCdCanalInclusao());
    	saida.setCdCanalManutencao(response.getCdCanalManutencao());
    	saida.setCdFormaLiquidacao(response.getCdFormaLiquidacao());
    	saida.setDsFormaLiquidacao(response.getDsFormaLiquidacao());
    	saida.setCdPrioridadeDebitoForma(response.getCdPrioridadeDebitoForma());
    	saida.setCdSistema(response.getCdSistema());
    	saida.setDsCanalInclusao(response.getDsCanalInclusao());
    	saida.setDsCanalManutencao(response.getDsCanalManutencao());
    	saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
    	saida.setHrLimiteProcedimentoPagamento(response.getHrLimiteProcedimentoPagamento());
    	saida.setHrManutencaoRegistroManutencao(response.getHrManutencaoRegistroManutencao());
    	saida.setNrOperacaoFluxoInclusao(response.getNrOperacaoFluxoInclusao());
    	saida.setNrOperacaoFluxoManutencao(response.getNrOperacaoFluxoManutencao());
    	saida.setQtMinutoMargemSeguranca(response.getQtMinutoMargemSeguranca());
    	saida.setDsSistema(response.getDsSistema());
    	saida.setHrConsultasSaldoPagamento(response.getHrConsultasSaldoPagamento());
    	saida.setHrConsultaFolhaPgto(response.getHrConsultaFolhaPgto());
    	saida.setQtMinutosValorSuperior(response.getQtMinutosValorSuperior());
    	saida.setHrLimiteValorSuperior(response.getHrLimiteValorSuperior());
    	saida.setQtdTempoAgendamentoCobranca(response.getQtdTempoAgendamentoCobranca());
        saida.setQtdTempoEfetivacaoCiclicaCobranca(response.getQtdTempoEfetivacaoCiclicaCobranca());
        saida.setQtdTempoEfetivacaoDiariaCobranca(response.getQtdTempoEfetivacaoDiariaCobranca());
        saida.setQtdLimiteProcessamentoExcedido(response.getQtdLimiteSemResposta());
        
    	return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.formaliqpagtoint.IFormaLiqPgtoIntService#alterarLiquidacaoPagto(br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean.AlterarLiquidacaoPagamentoEntradaDTO)
     */
    public AlterarLiquidacaoPagamentoSaidaDTO alterarLiquidacaoPagto(AlterarLiquidacaoPagamentoEntradaDTO entrada) {
    	AlterarLiquidacaoPagamentoRequest request = new AlterarLiquidacaoPagamentoRequest();
    	request.setCdControleHoraLimite(entrada.getCdControleHoraLimite());
    	request.setCdFormaLiquidacao(entrada.getCdFormaLiquidacao());
    	request.setCdPrioridadeDebForma(entrada.getCdPrioridadeDebForma());
    	request.setCdSistema(entrada.getCdSistema());
    	request.setHrLimiteProcessamento(entrada.getHrLimiteProcessamento().replace(':', '.'));
    	request.setQtMinutoMargemSeguranca(entrada.getQtMinutoMargemSeguranca() == null ? 0 : entrada.getQtMinutoMargemSeguranca());
    	request.setHrConsultasSaldoPagamento(entrada.getHrConsultasSaldoPagamento());
    	request.setHrConsultaFolhaPgto(entrada.getHrConsultaFolhaPgto());
    	request.setQtMinutosValorSuperior(PgitUtil.verificaIntegerNulo(entrada.getQtMinutosValorSuperior()));
    	request.setHrLimiteValorSuperior(PgitUtil.verificaStringNula(entrada.getHrLimiteValorSuperior()));
        request.setQtdTempoAgendamentoCobranca(PgitUtil.verificaIntegerNulo(entrada.getQtdTempoAgendamentoCobranca()));
        request.setQtdTempoEfetivacaoCiclicaCobranca(PgitUtil.verificaIntegerNulo(entrada
            .getQtdTempoEfetivacaoCiclicaCobranca()));
        request.setQtdTempoEfetivacaoDiariaCobranca(PgitUtil.verificaIntegerNulo(entrada
            .getQtdTempoEfetivacaoDiariaCobranca()));
        request.setQtdLimiteSemResposta(PgitUtil.verificaIntegerNulo(entrada.getQtdLimiteProcessamentoExcedido()));
        
    	AlterarLiquidacaoPagamentoResponse response = getFactoryAdapter().getAlterarLiquidacaoPagamentoPDCAdapter().invokeProcess(request);

    	return new AlterarLiquidacaoPagamentoSaidaDTO(response.getCodMensagem(), response.getMensagem());
    }

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}