package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;


/**
 * Arquivo criado em 29/02/16.
 */
public class ConsultarManutencaoConfigContratoSaidaDTO {

    private String codMensagem;

    private String mensagem;

    private Long cdpessoaJuridicaContrato;

    private Integer cdTipoContratoNegocio;

    private Long nrSequenciaContratoNegocio;

    private Integer cdTipoManutencaoContrato;

    private Long nrManutencaoContratoNegocio;

    private Integer cdSituacaoManutencaoContrato;

    private String dsSituacaoManutencaoContrato;

    private Integer cdMotivoSituacaoManutencao;

    private String dsMotivoSituacaoManutencao;

    private Long nrAditivoContratoNegocio;

    private Integer cdIndicadorTipoManutencao;

    private String dsIndicadorTipoManutencao;

    private Integer cdFormaAutorizacaoPagamento;

    private String dsFormaAutorizacaoPagamento;

    private Long cdPessoaJuridica;

    private Integer nrSequenciaUnidadeOrganizacional;

    private Integer cdTipoResponsavelOrganizacao;

    private Integer cdUnidadeOrganizacional;

    private String dsUnidadeOrganizacional;

    private Long cdFuncionarioGerenteContrato;

    private String dsFuncionarioGerenteContrato;

    private String cdUsuarioInclusao;

    private String cdUsuarioInclusaoExterno;

    private String hrInclusaoRegistro;

    private String cdOperacaoCanalInclusao;

    private Integer cdTipoCanalInclusao;

    private String dsTipoCanalInclusao;

    private String cdUsuarioManutencao;

    private String cdUsuarioManutencaoExterno;

    private String hrManutencaoRegistro;

    private String cdOperacaoCanalManutencao;

    private Integer cdTipoCanalManutencao;

    private String dsTipoCanalManutencao;
    
    public String getAgenciaGestoraFormatada(){
        if( (getCdUnidadeOrganizacional() == null || getCdUnidadeOrganizacional() == 0) || 
            (getDsUnidadeOrganizacional() == null || "".equals(getDsUnidadeOrganizacional()))){
            
            return getCdUnidadeOrganizacional() + getDsUnidadeOrganizacional();
        }

        return getCdUnidadeOrganizacional() + " - " +  getDsUnidadeOrganizacional();
    }

    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    public String getCodMensagem() {
        return this.codMensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    public void setCdTipoManutencaoContrato(Integer cdTipoManutencaoContrato) {
        this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
    }

    public Integer getCdTipoManutencaoContrato() {
        return this.cdTipoManutencaoContrato;
    }

    public void setNrManutencaoContratoNegocio(Long nrManutencaoContratoNegocio) {
        this.nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
    }

    public Long getNrManutencaoContratoNegocio() {
        return this.nrManutencaoContratoNegocio;
    }

    public void setCdSituacaoManutencaoContrato(Integer cdSituacaoManutencaoContrato) {
        this.cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
    }

    public Integer getCdSituacaoManutencaoContrato() {
        return this.cdSituacaoManutencaoContrato;
    }

    public void setDsSituacaoManutencaoContrato(String dsSituacaoManutencaoContrato) {
        this.dsSituacaoManutencaoContrato = dsSituacaoManutencaoContrato;
    }

    public String getDsSituacaoManutencaoContrato() {
        return this.dsSituacaoManutencaoContrato;
    }

    public void setCdMotivoSituacaoManutencao(Integer cdMotivoSituacaoManutencao) {
        this.cdMotivoSituacaoManutencao = cdMotivoSituacaoManutencao;
    }

    public Integer getCdMotivoSituacaoManutencao() {
        return this.cdMotivoSituacaoManutencao;
    }

    public void setDsMotivoSituacaoManutencao(String dsMotivoSituacaoManutencao) {
        this.dsMotivoSituacaoManutencao = dsMotivoSituacaoManutencao;
    }

    public String getDsMotivoSituacaoManutencao() {
        return this.dsMotivoSituacaoManutencao;
    }

    public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
        this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
    }

    public Long getNrAditivoContratoNegocio() {
        return this.nrAditivoContratoNegocio;
    }

    public void setCdIndicadorTipoManutencao(Integer cdIndicadorTipoManutencao) {
        this.cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
    }

    public Integer getCdIndicadorTipoManutencao() {
        return this.cdIndicadorTipoManutencao;
    }

    public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao) {
        this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    }

    public String getDsIndicadorTipoManutencao() {
        return this.dsIndicadorTipoManutencao;
    }

    public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
        this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
    }

    public Integer getCdFormaAutorizacaoPagamento() {
        return this.cdFormaAutorizacaoPagamento;
    }

    public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
        this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    }

    public String getDsFormaAutorizacaoPagamento() {
        return this.dsFormaAutorizacaoPagamento;
    }

    public void setCdPessoaJuridica(Long cdPessoaJuridica) {
        this.cdPessoaJuridica = cdPessoaJuridica;
    }

    public Long getCdPessoaJuridica() {
        return this.cdPessoaJuridica;
    }

    public void setNrSequenciaUnidadeOrganizacional(Integer nrSequenciaUnidadeOrganizacional) {
        this.nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
    }

    public Integer getNrSequenciaUnidadeOrganizacional() {
        return this.nrSequenciaUnidadeOrganizacional;
    }

    public void setCdTipoResponsavelOrganizacao(Integer cdTipoResponsavelOrganizacao) {
        this.cdTipoResponsavelOrganizacao = cdTipoResponsavelOrganizacao;
    }

    public Integer getCdTipoResponsavelOrganizacao() {
        return this.cdTipoResponsavelOrganizacao;
    }

    public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
        this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
    }

    public Integer getCdUnidadeOrganizacional() {
        return this.cdUnidadeOrganizacional;
    }

    public void setDsUnidadeOrganizacional(String dsUnidadeOrganizacional) {
        this.dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    }

    public String getDsUnidadeOrganizacional() {
        return this.dsUnidadeOrganizacional;
    }

    public void setCdFuncionarioGerenteContrato(Long cdFuncionarioGerenteContrato) {
        this.cdFuncionarioGerenteContrato = cdFuncionarioGerenteContrato;
    }

    public Long getCdFuncionarioGerenteContrato() {
        return this.cdFuncionarioGerenteContrato;
    }

    public void setDsFuncionarioGerenteContrato(String dsFuncionarioGerenteContrato) {
        this.dsFuncionarioGerenteContrato = dsFuncionarioGerenteContrato;
    }

    public String getDsFuncionarioGerenteContrato() {
        return this.dsFuncionarioGerenteContrato;
    }

    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    public String getCdUsuarioInclusao() {
        return this.cdUsuarioInclusao;
    }

    public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
        this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    }

    public String getCdUsuarioInclusaoExterno() {
        return this.cdUsuarioInclusaoExterno;
    }

    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
        this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    public String getHrInclusaoRegistro() {
        return this.hrInclusaoRegistro;
    }

    public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
        this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    }

    public String getCdOperacaoCanalInclusao() {
        return this.cdOperacaoCanalInclusao;
    }

    public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
        this.cdTipoCanalInclusao = cdTipoCanalInclusao;
    }

    public Integer getCdTipoCanalInclusao() {
        return this.cdTipoCanalInclusao;
    }

    public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
        this.dsTipoCanalInclusao = dsTipoCanalInclusao;
    }

    public String getDsTipoCanalInclusao() {
        return this.dsTipoCanalInclusao;
    }

    public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
        this.cdUsuarioManutencao = cdUsuarioManutencao;
    }

    public String getCdUsuarioManutencao() {
        return this.cdUsuarioManutencao;
    }

    public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
        this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    }

    public String getCdUsuarioManutencaoExterno() {
        return this.cdUsuarioManutencaoExterno;
    }

    public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
        this.hrManutencaoRegistro = hrManutencaoRegistro;
    }

    public String getHrManutencaoRegistro() {
        return this.hrManutencaoRegistro;
    }

    public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
        this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    }

    public String getCdOperacaoCanalManutencao() {
        return this.cdOperacaoCanalManutencao;
    }

    public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
        this.cdTipoCanalManutencao = cdTipoCanalManutencao;
    }

    public Integer getCdTipoCanalManutencao() {
        return this.cdTipoCanalManutencao;
    }

    public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
        this.dsTipoCanalManutencao = dsTipoCanalManutencao;
    }

    public String getDsTipoCanalManutencao() {
        return this.dsTipoCanalManutencao;
    }
}