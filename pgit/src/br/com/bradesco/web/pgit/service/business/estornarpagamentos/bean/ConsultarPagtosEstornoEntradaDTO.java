/*
 * Nome: br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.estornarpagamentos.bean;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Nome: ConsultarPagtosEstornoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagtosEstornoEntradaDTO {
	
	/** Atributo cdTipoPesquisa. */
	private int cdTipoPesquisa;
	
	/** Atributo numeroOcorrencias. */
	private int numeroOcorrencias;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/** Atributo cdBanco. */
	private int cdBanco;
	
	/** Atributo cdAgencia. */
	private int cdAgencia;
	
	/** Atributo dgAgenciaBancaria. */
	private int dgAgenciaBancaria;
	
	/** Atributo cdConta. */
	private long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo nrArquivoRemssaPagamento. */
	private long nrArquivoRemssaPagamento;
	
	/** Atributo cdParticipante. */
	private long cdParticipante;
	
	/** Atributo cdprodutoServicoOperacao. */
	private int cdprodutoServicoOperacao;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private int cdProdutoServicoRelacionado;
	
	/** Atributo cdControlePagamentoDe. */
	private String cdControlePagamentoDe;
	
	/** Atributo vlPagamentoDe. */
	private BigDecimal vlPagamentoDe;
	
	/** Atributo vlPagamentoAte. */
	private BigDecimal vlPagamentoAte;
	
	/** Atributo cdFavorecidoClientePagador. */
	private long cdFavorecidoClientePagador;
	
	/** Atributo cdInscricaoFavorecido. */
	private long cdInscricaoFavorecido;
	
	/** Atributo cdIdentificacaoInscricaoFavorecido. */
	private int cdIdentificacaoInscricaoFavorecido;
	
	/** Atributo dataInicialPagamentoFiltro. */
	private Date dataInicialPagamentoFiltro;
	
	/** Atributo dataFinalPagamentoFiltro. */
	private Date dataFinalPagamentoFiltro;
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public int getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(int cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public int getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(int cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdControlePagamentoDe.
	 *
	 * @return cdControlePagamentoDe
	 */
	public String getCdControlePagamentoDe() {
		return cdControlePagamentoDe;
	}
	
	/**
	 * Set: cdControlePagamentoDe.
	 *
	 * @param cdControlePagamentoDe the cd controle pagamento de
	 */
	public void setCdControlePagamentoDe(String cdControlePagamentoDe) {
		this.cdControlePagamentoDe = cdControlePagamentoDe;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdFavorecidoClientePagador.
	 *
	 * @return cdFavorecidoClientePagador
	 */
	public long getCdFavorecidoClientePagador() {
		return cdFavorecidoClientePagador;
	}
	
	/**
	 * Set: cdFavorecidoClientePagador.
	 *
	 * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
	 */
	public void setCdFavorecidoClientePagador(long cdFavorecidoClientePagador) {
		this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
	}
	
	/**
	 * Get: cdIdentificacaoInscricaoFavorecido.
	 *
	 * @return cdIdentificacaoInscricaoFavorecido
	 */
	public int getCdIdentificacaoInscricaoFavorecido() {
		return cdIdentificacaoInscricaoFavorecido;
	}
	
	/**
	 * Set: cdIdentificacaoInscricaoFavorecido.
	 *
	 * @param cdIdentificacaoInscricaoFavorecido the cd identificacao inscricao favorecido
	 */
	public void setCdIdentificacaoInscricaoFavorecido(
			int cdIdentificacaoInscricaoFavorecido) {
		this.cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
	}
	
	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public long getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}
	
	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(long cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}
	
	/**
	 * Get: cdParticipante.
	 *
	 * @return cdParticipante
	 */
	public long getCdParticipante() {
		return cdParticipante;
	}
	
	/**
	 * Set: cdParticipante.
	 *
	 * @param cdParticipante the cd participante
	 */
	public void setCdParticipante(long cdParticipante) {
		this.cdParticipante = cdParticipante;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdprodutoServicoOperacao.
	 *
	 * @return cdprodutoServicoOperacao
	 */
	public int getCdprodutoServicoOperacao() {
		return cdprodutoServicoOperacao;
	}
	
	/**
	 * Set: cdprodutoServicoOperacao.
	 *
	 * @param cdprodutoServicoOperacao the cdproduto servico operacao
	 */
	public void setCdprodutoServicoOperacao(int cdprodutoServicoOperacao) {
		this.cdprodutoServicoOperacao = cdprodutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public int getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoPesquisa.
	 *
	 * @return cdTipoPesquisa
	 */
	public int getCdTipoPesquisa() {
		return cdTipoPesquisa;
	}
	
	/**
	 * Set: cdTipoPesquisa.
	 *
	 * @param cdTipoPesquisa the cd tipo pesquisa
	 */
	public void setCdTipoPesquisa(int cdTipoPesquisa) {
		this.cdTipoPesquisa = cdTipoPesquisa;
	}
	
	/**
	 * Get: dgAgenciaBancaria.
	 *
	 * @return dgAgenciaBancaria
	 */
	public int getDgAgenciaBancaria() {
		return dgAgenciaBancaria;
	}
	
	/**
	 * Set: dgAgenciaBancaria.
	 *
	 * @param dgAgenciaBancaria the dg agencia bancaria
	 */
	public void setDgAgenciaBancaria(int dgAgenciaBancaria) {
		this.dgAgenciaBancaria = dgAgenciaBancaria;
	}
	
	/**
	 * Get: nrArquivoRemssaPagamento.
	 *
	 * @return nrArquivoRemssaPagamento
	 */
	public long getNrArquivoRemssaPagamento() {
		return nrArquivoRemssaPagamento;
	}
	
	/**
	 * Set: nrArquivoRemssaPagamento.
	 *
	 * @param nrArquivoRemssaPagamento the nr arquivo remssa pagamento
	 */
	public void setNrArquivoRemssaPagamento(long nrArquivoRemssaPagamento) {
		this.nrArquivoRemssaPagamento = nrArquivoRemssaPagamento;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public int getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(int numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	/**
	 * Get: vlPagamentoAte.
	 *
	 * @return vlPagamentoAte
	 */
	public BigDecimal getVlPagamentoAte() {
		return vlPagamentoAte;
	}
	
	/**
	 * Set: vlPagamentoAte.
	 *
	 * @param vlPagamentoAte the vl pagamento ate
	 */
	public void setVlPagamentoAte(BigDecimal vlPagamentoAte) {
		this.vlPagamentoAte = vlPagamentoAte;
	}
	
	/**
	 * Get: vlPagamentoDe.
	 *
	 * @return vlPagamentoDe
	 */
	public BigDecimal getVlPagamentoDe() {
		return vlPagamentoDe;
	}
	
	/**
	 * Set: vlPagamentoDe.
	 *
	 * @param vlPagamentoDe the vl pagamento de
	 */
	public void setVlPagamentoDe(BigDecimal vlPagamentoDe) {
		this.vlPagamentoDe = vlPagamentoDe;
	}
	
	/**
	 * Get: dataInicialPagamentoFiltro.
	 *
	 * @return dataInicialPagamentoFiltro
	 */
	public Date getDataInicialPagamentoFiltro() {
		return dataInicialPagamentoFiltro;
	}
	
	/**
	 * Set: dataInicialPagamentoFiltro.
	 *
	 * @param dataInicialPagamentoFiltro the data inicial pagamento filtro
	 */
	public void setDataInicialPagamentoFiltro(Date dataInicialPagamentoFiltro) {
		this.dataInicialPagamentoFiltro = dataInicialPagamentoFiltro;
	}
	
	/**
	 * Get: dataFinalPagamentoFiltro.
	 *
	 * @return dataFinalPagamentoFiltro
	 */
	public Date getDataFinalPagamentoFiltro() {
		return dataFinalPagamentoFiltro;
	}
	
	/**
	 * Set: dataFinalPagamentoFiltro.
	 *
	 * @param dataFinalPagamentoFiltro the data final pagamento filtro
	 */
	public void setDataFinalPagamentoFiltro(Date dataFinalPagamentoFiltro) {
		this.dataFinalPagamentoFiltro = dataFinalPagamentoFiltro;
	}
	
	

}
