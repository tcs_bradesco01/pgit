/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdprodutoServicoRelacionado
     */
    private java.lang.String _cdprodutoServicoRelacionado;

    /**
     * Field _cdRelacionamentoProdutoProduto
     */
    private int _cdRelacionamentoProdutoProduto = 0;

    /**
     * keeps track of state for field:
     * _cdRelacionamentoProdutoProduto
     */
    private boolean _has_cdRelacionamentoProdutoProduto;

    /**
     * Field _dsRelacionamentoProdutoProduto
     */
    private java.lang.String _dsRelacionamentoProdutoProduto;

    /**
     * Field _cdTipoProdutoServico
     */
    private int _cdTipoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdTipoProdutoServico
     */
    private boolean _has_cdTipoProdutoServico;

    /**
     * Field _dsTipoProdutoServico
     */
    private java.lang.String _dsTipoProdutoServico;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdParametroTela
     */
    private int _cdParametroTela = 0;

    /**
     * keeps track of state for field: _cdParametroTela
     */
    private boolean _has_cdParametroTela;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdParametroTela
     * 
     */
    public void deleteCdParametroTela()
    {
        this._has_cdParametroTela= false;
    } //-- void deleteCdParametroTela() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionamentoProdutoProduto
     * 
     */
    public void deleteCdRelacionamentoProdutoProduto()
    {
        this._has_cdRelacionamentoProdutoProduto= false;
    } //-- void deleteCdRelacionamentoProdutoProduto() 

    /**
     * Method deleteCdTipoProdutoServico
     * 
     */
    public void deleteCdTipoProdutoServico()
    {
        this._has_cdTipoProdutoServico= false;
    } //-- void deleteCdTipoProdutoServico() 

    /**
     * Returns the value of field 'cdParametroTela'.
     * 
     * @return int
     * @return the value of field 'cdParametroTela'.
     */
    public int getCdParametroTela()
    {
        return this._cdParametroTela;
    } //-- int getCdParametroTela() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProdutoProduto'.
     */
    public int getCdRelacionamentoProdutoProduto()
    {
        return this._cdRelacionamentoProdutoProduto;
    } //-- int getCdRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'cdTipoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdTipoProdutoServico'.
     */
    public int getCdTipoProdutoServico()
    {
        return this._cdTipoProdutoServico;
    } //-- int getCdTipoProdutoServico() 

    /**
     * Returns the value of field 'cdprodutoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'cdprodutoServicoRelacionado'.
     */
    public java.lang.String getCdprodutoServicoRelacionado()
    {
        return this._cdprodutoServicoRelacionado;
    } //-- java.lang.String getCdprodutoServicoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsRelacionamentoProdutoProduto'.
     * 
     * @return String
     * @return the value of field 'dsRelacionamentoProdutoProduto'.
     */
    public java.lang.String getDsRelacionamentoProdutoProduto()
    {
        return this._dsRelacionamentoProdutoProduto;
    } //-- java.lang.String getDsRelacionamentoProdutoProduto() 

    /**
     * Returns the value of field 'dsTipoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsTipoProdutoServico'.
     */
    public java.lang.String getDsTipoProdutoServico()
    {
        return this._dsTipoProdutoServico;
    } //-- java.lang.String getDsTipoProdutoServico() 

    /**
     * Method hasCdParametroTela
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParametroTela()
    {
        return this._has_cdParametroTela;
    } //-- boolean hasCdParametroTela() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionamentoProdutoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProdutoProduto()
    {
        return this._has_cdRelacionamentoProdutoProduto;
    } //-- boolean hasCdRelacionamentoProdutoProduto() 

    /**
     * Method hasCdTipoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProdutoServico()
    {
        return this._has_cdTipoProdutoServico;
    } //-- boolean hasCdTipoProdutoServico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdParametroTela'.
     * 
     * @param cdParametroTela the value of field 'cdParametroTela'.
     */
    public void setCdParametroTela(int cdParametroTela)
    {
        this._cdParametroTela = cdParametroTela;
        this._has_cdParametroTela = true;
    } //-- void setCdParametroTela(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProdutoProduto'.
     * 
     * @param cdRelacionamentoProdutoProduto the value of field
     * 'cdRelacionamentoProdutoProduto'.
     */
    public void setCdRelacionamentoProdutoProduto(int cdRelacionamentoProdutoProduto)
    {
        this._cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
        this._has_cdRelacionamentoProdutoProduto = true;
    } //-- void setCdRelacionamentoProdutoProduto(int) 

    /**
     * Sets the value of field 'cdTipoProdutoServico'.
     * 
     * @param cdTipoProdutoServico the value of field
     * 'cdTipoProdutoServico'.
     */
    public void setCdTipoProdutoServico(int cdTipoProdutoServico)
    {
        this._cdTipoProdutoServico = cdTipoProdutoServico;
        this._has_cdTipoProdutoServico = true;
    } //-- void setCdTipoProdutoServico(int) 

    /**
     * Sets the value of field 'cdprodutoServicoRelacionado'.
     * 
     * @param cdprodutoServicoRelacionado the value of field
     * 'cdprodutoServicoRelacionado'.
     */
    public void setCdprodutoServicoRelacionado(java.lang.String cdprodutoServicoRelacionado)
    {
        this._cdprodutoServicoRelacionado = cdprodutoServicoRelacionado;
    } //-- void setCdprodutoServicoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsRelacionamentoProdutoProduto'.
     * 
     * @param dsRelacionamentoProdutoProduto the value of field
     * 'dsRelacionamentoProdutoProduto'.
     */
    public void setDsRelacionamentoProdutoProduto(java.lang.String dsRelacionamentoProdutoProduto)
    {
        this._dsRelacionamentoProdutoProduto = dsRelacionamentoProdutoProduto;
    } //-- void setDsRelacionamentoProdutoProduto(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoProdutoServico'.
     * 
     * @param dsTipoProdutoServico the value of field
     * 'dsTipoProdutoServico'.
     */
    public void setDsTipoProdutoServico(java.lang.String dsTipoProdutoServico)
    {
        this._dsTipoProdutoServico = dsTipoProdutoServico;
    } //-- void setDsTipoProdutoServico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarservicorelacionadocdpsincluir.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
