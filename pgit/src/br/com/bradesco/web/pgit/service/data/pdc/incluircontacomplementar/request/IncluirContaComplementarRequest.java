/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirContaComplementarRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirContaComplementarRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdContaComplementar
     */
    private int _cdContaComplementar = 0;

    /**
     * keeps track of state for field: _cdContaComplementar
     */
    private boolean _has_cdContaComplementar;

    /**
     * Field _cdCorpoCnpjCpf
     */
    private long _cdCorpoCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCorpoCnpjCpf
     */
    private boolean _has_cdCorpoCnpjCpf;

    /**
     * Field _cdFilialCnpjCpf
     */
    private int _cdFilialCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjCpf
     */
    private boolean _has_cdFilialCnpjCpf;

    /**
     * Field _cdControleCnpjCpf
     */
    private int _cdControleCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdControleCnpjCpf
     */
    private boolean _has_cdControleCnpjCpf;

    /**
     * Field _cdpessoaJuridicaContratoConta
     */
    private long _cdpessoaJuridicaContratoConta = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContratoCont
     */
    private boolean _has_cdpessoaJuridicaContratoConta;

    /**
     * Field _cdTipoContratoConta
     */
    private int _cdTipoContratoConta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoConta
     */
    private boolean _has_cdTipoContratoConta;

    /**
     * Field _nrSequenciaContratoConta
     */
    private long _nrSequenciaContratoConta = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoConta
     */
    private boolean _has_nrSequenciaContratoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirContaComplementarRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.request.IncluirContaComplementarRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdContaComplementar
     * 
     */
    public void deleteCdContaComplementar()
    {
        this._has_cdContaComplementar= false;
    } //-- void deleteCdContaComplementar() 

    /**
     * Method deleteCdControleCnpjCpf
     * 
     */
    public void deleteCdControleCnpjCpf()
    {
        this._has_cdControleCnpjCpf= false;
    } //-- void deleteCdControleCnpjCpf() 

    /**
     * Method deleteCdCorpoCnpjCpf
     * 
     */
    public void deleteCdCorpoCnpjCpf()
    {
        this._has_cdCorpoCnpjCpf= false;
    } //-- void deleteCdCorpoCnpjCpf() 

    /**
     * Method deleteCdFilialCnpjCpf
     * 
     */
    public void deleteCdFilialCnpjCpf()
    {
        this._has_cdFilialCnpjCpf= false;
    } //-- void deleteCdFilialCnpjCpf() 

    /**
     * Method deleteCdTipoContratoConta
     * 
     */
    public void deleteCdTipoContratoConta()
    {
        this._has_cdTipoContratoConta= false;
    } //-- void deleteCdTipoContratoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteCdpessoaJuridicaContratoConta
     * 
     */
    public void deleteCdpessoaJuridicaContratoConta()
    {
        this._has_cdpessoaJuridicaContratoConta= false;
    } //-- void deleteCdpessoaJuridicaContratoConta() 

    /**
     * Method deleteNrSequenciaContratoConta
     * 
     */
    public void deleteNrSequenciaContratoConta()
    {
        this._has_nrSequenciaContratoConta= false;
    } //-- void deleteNrSequenciaContratoConta() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdContaComplementar'.
     * 
     * @return int
     * @return the value of field 'cdContaComplementar'.
     */
    public int getCdContaComplementar()
    {
        return this._cdContaComplementar;
    } //-- int getCdContaComplementar() 

    /**
     * Returns the value of field 'cdControleCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjCpf'.
     */
    public int getCdControleCnpjCpf()
    {
        return this._cdControleCnpjCpf;
    } //-- int getCdControleCnpjCpf() 

    /**
     * Returns the value of field 'cdCorpoCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCnpjCpf'.
     */
    public long getCdCorpoCnpjCpf()
    {
        return this._cdCorpoCnpjCpf;
    } //-- long getCdCorpoCnpjCpf() 

    /**
     * Returns the value of field 'cdFilialCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjCpf'.
     */
    public int getCdFilialCnpjCpf()
    {
        return this._cdFilialCnpjCpf;
    } //-- int getCdFilialCnpjCpf() 

    /**
     * Returns the value of field 'cdTipoContratoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoConta'.
     */
    public int getCdTipoContratoConta()
    {
        return this._cdTipoContratoConta;
    } //-- int getCdTipoContratoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContratoConta'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContratoConta'.
     */
    public long getCdpessoaJuridicaContratoConta()
    {
        return this._cdpessoaJuridicaContratoConta;
    } //-- long getCdpessoaJuridicaContratoConta() 

    /**
     * Returns the value of field 'nrSequenciaContratoConta'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoConta'.
     */
    public long getNrSequenciaContratoConta()
    {
        return this._nrSequenciaContratoConta;
    } //-- long getNrSequenciaContratoConta() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdContaComplementar
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaComplementar()
    {
        return this._has_cdContaComplementar;
    } //-- boolean hasCdContaComplementar() 

    /**
     * Method hasCdControleCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjCpf()
    {
        return this._has_cdControleCnpjCpf;
    } //-- boolean hasCdControleCnpjCpf() 

    /**
     * Method hasCdCorpoCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCnpjCpf()
    {
        return this._has_cdCorpoCnpjCpf;
    } //-- boolean hasCdCorpoCnpjCpf() 

    /**
     * Method hasCdFilialCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjCpf()
    {
        return this._has_cdFilialCnpjCpf;
    } //-- boolean hasCdFilialCnpjCpf() 

    /**
     * Method hasCdTipoContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoConta()
    {
        return this._has_cdTipoContratoConta;
    } //-- boolean hasCdTipoContratoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasCdpessoaJuridicaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContratoConta()
    {
        return this._has_cdpessoaJuridicaContratoConta;
    } //-- boolean hasCdpessoaJuridicaContratoConta() 

    /**
     * Method hasNrSequenciaContratoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoConta()
    {
        return this._has_nrSequenciaContratoConta;
    } //-- boolean hasNrSequenciaContratoConta() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdContaComplementar'.
     * 
     * @param cdContaComplementar the value of field
     * 'cdContaComplementar'.
     */
    public void setCdContaComplementar(int cdContaComplementar)
    {
        this._cdContaComplementar = cdContaComplementar;
        this._has_cdContaComplementar = true;
    } //-- void setCdContaComplementar(int) 

    /**
     * Sets the value of field 'cdControleCnpjCpf'.
     * 
     * @param cdControleCnpjCpf the value of field
     * 'cdControleCnpjCpf'.
     */
    public void setCdControleCnpjCpf(int cdControleCnpjCpf)
    {
        this._cdControleCnpjCpf = cdControleCnpjCpf;
        this._has_cdControleCnpjCpf = true;
    } //-- void setCdControleCnpjCpf(int) 

    /**
     * Sets the value of field 'cdCorpoCnpjCpf'.
     * 
     * @param cdCorpoCnpjCpf the value of field 'cdCorpoCnpjCpf'.
     */
    public void setCdCorpoCnpjCpf(long cdCorpoCnpjCpf)
    {
        this._cdCorpoCnpjCpf = cdCorpoCnpjCpf;
        this._has_cdCorpoCnpjCpf = true;
    } //-- void setCdCorpoCnpjCpf(long) 

    /**
     * Sets the value of field 'cdFilialCnpjCpf'.
     * 
     * @param cdFilialCnpjCpf the value of field 'cdFilialCnpjCpf'.
     */
    public void setCdFilialCnpjCpf(int cdFilialCnpjCpf)
    {
        this._cdFilialCnpjCpf = cdFilialCnpjCpf;
        this._has_cdFilialCnpjCpf = true;
    } //-- void setCdFilialCnpjCpf(int) 

    /**
     * Sets the value of field 'cdTipoContratoConta'.
     * 
     * @param cdTipoContratoConta the value of field
     * 'cdTipoContratoConta'.
     */
    public void setCdTipoContratoConta(int cdTipoContratoConta)
    {
        this._cdTipoContratoConta = cdTipoContratoConta;
        this._has_cdTipoContratoConta = true;
    } //-- void setCdTipoContratoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContratoConta'.
     * 
     * @param cdpessoaJuridicaContratoConta the value of field
     * 'cdpessoaJuridicaContratoConta'.
     */
    public void setCdpessoaJuridicaContratoConta(long cdpessoaJuridicaContratoConta)
    {
        this._cdpessoaJuridicaContratoConta = cdpessoaJuridicaContratoConta;
        this._has_cdpessoaJuridicaContratoConta = true;
    } //-- void setCdpessoaJuridicaContratoConta(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoConta'.
     * 
     * @param nrSequenciaContratoConta the value of field
     * 'nrSequenciaContratoConta'.
     */
    public void setNrSequenciaContratoConta(long nrSequenciaContratoConta)
    {
        this._nrSequenciaContratoConta = nrSequenciaContratoConta;
        this._has_nrSequenciaContratoConta = true;
    } //-- void setNrSequenciaContratoConta(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirContaComplementarRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.request.IncluirContaComplementarRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.request.IncluirContaComplementarRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.request.IncluirContaComplementarRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.request.IncluirContaComplementarRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
