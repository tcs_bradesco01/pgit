/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMotivoSituacaoAditivoContSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoSituacaoAditivoContSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdMotivoSituacaoOperacao. */
	private Integer cdMotivoSituacaoOperacao;
	
	/** Atributo dsMotivoSituacaoOperacao. */
	private String dsMotivoSituacaoOperacao;
	
	/**
	 * Listar motivo situacao aditivo cont saida dto.
	 *
	 * @param cdMotivoSituacaoOperacao the cd motivo situacao operacao
	 * @param dsMotivoSituacaoOperacao the ds motivo situacao operacao
	 */
	public ListarMotivoSituacaoAditivoContSaidaDTO(Integer cdMotivoSituacaoOperacao, String dsMotivoSituacaoOperacao) {
		super();
		this.cdMotivoSituacaoOperacao = cdMotivoSituacaoOperacao;
		this.dsMotivoSituacaoOperacao = dsMotivoSituacaoOperacao;
	}
	
	/**
	 * Get: cdMotivoSituacaoOperacao.
	 *
	 * @return cdMotivoSituacaoOperacao
	 */
	public Integer getCdMotivoSituacaoOperacao() {
		return cdMotivoSituacaoOperacao;
	}
	
	/**
	 * Set: cdMotivoSituacaoOperacao.
	 *
	 * @param cdMotivoSituacaoOperacao the cd motivo situacao operacao
	 */
	public void setCdMotivoSituacaoOperacao(Integer cdMotivoSituacaoOperacao) {
		this.cdMotivoSituacaoOperacao = cdMotivoSituacaoOperacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsMotivoSituacaoOperacao.
	 *
	 * @return dsMotivoSituacaoOperacao
	 */
	public String getDsMotivoSituacaoOperacao() {
		return dsMotivoSituacaoOperacao;
	}
	
	/**
	 * Set: dsMotivoSituacaoOperacao.
	 *
	 * @param dsMotivoSituacaoOperacao the ds motivo situacao operacao
	 */
	public void setDsMotivoSituacaoOperacao(String dsMotivoSituacaoOperacao) {
		this.dsMotivoSituacaoOperacao = dsMotivoSituacaoOperacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
