/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ConsultarManutencaoMeioTransmissaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManutencaoMeioTransmissaoEntradaDTO {

	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoManutencaoContrato. */
	private Integer cdTipoManutencaoContrato;
	
	/** Atributo nrManutencaoContratoNegocio. */
	private Long nrManutencaoContratoNegocio;

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdTipoManutencaoContrato.
	 *
	 * @return cdTipoManutencaoContrato
	 */
	public Integer getCdTipoManutencaoContrato() {
		return cdTipoManutencaoContrato;
	}
	
	/**
	 * Set: cdTipoManutencaoContrato.
	 *
	 * @param cdTipoManutencaoContrato the cd tipo manutencao contrato
	 */
	public void setCdTipoManutencaoContrato(Integer cdTipoManutencaoContrato) {
		this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
	}
	
	/**
	 * Get: nrManutencaoContratoNegocio.
	 *
	 * @return nrManutencaoContratoNegocio
	 */
	public Long getNrManutencaoContratoNegocio() {
		return nrManutencaoContratoNegocio;
	}
	
	/**
	 * Set: nrManutencaoContratoNegocio.
	 *
	 * @param nrManutencaoContratoNegocio the nr manutencao contrato negocio
	 */
	public void setNrManutencaoContratoNegocio(Long nrManutencaoContratoNegocio) {
		this.nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
	}
}
