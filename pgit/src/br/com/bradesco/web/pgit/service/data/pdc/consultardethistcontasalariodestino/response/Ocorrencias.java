/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _tpManutencao
     */
    private java.lang.String _tpManutencao;

    /**
     * Field _cdBancoDestino
     */
    private int _cdBancoDestino = 0;

    /**
     * keeps track of state for field: _cdBancoDestino
     */
    private boolean _has_cdBancoDestino;

    /**
     * Field _dsBancoDestino
     */
    private java.lang.String _dsBancoDestino;

    /**
     * Field _cdAgenciaDestino
     */
    private int _cdAgenciaDestino = 0;

    /**
     * keeps track of state for field: _cdAgenciaDestino
     */
    private boolean _has_cdAgenciaDestino;

    /**
     * Field _dgAgenciaDestino
     */
    private int _dgAgenciaDestino = 0;

    /**
     * keeps track of state for field: _dgAgenciaDestino
     */
    private boolean _has_dgAgenciaDestino;

    /**
     * Field _cdContaBancariaDestino
     */
    private long _cdContaBancariaDestino = 0;

    /**
     * keeps track of state for field: _cdContaBancariaDestino
     */
    private boolean _has_cdContaBancariaDestino;

    /**
     * Field _dgContaDestino
     */
    private java.lang.String _dgContaDestino;

    /**
     * Field _dsAgenciaDestino
     */
    private java.lang.String _dsAgenciaDestino;

    /**
     * Field _cdTipoContaDestino
     */
    private java.lang.String _cdTipoContaDestino;

    /**
     * Field _cdContingenciaTed
     */
    private java.lang.String _cdContingenciaTed;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExter
     */
    private java.lang.String _cdUsuarioInclusaoExter;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperCanalInclusao
     */
    private java.lang.String _cdOperCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExter
     */
    private java.lang.String _cdUsuarioManutencaoExter;

    /**
     * Field _dtMantuencao
     */
    private java.lang.String _dtMantuencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperCanalManutencao
     */
    private java.lang.String _cdOperCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDestino
     * 
     */
    public void deleteCdAgenciaDestino()
    {
        this._has_cdAgenciaDestino= false;
    } //-- void deleteCdAgenciaDestino() 

    /**
     * Method deleteCdBancoDestino
     * 
     */
    public void deleteCdBancoDestino()
    {
        this._has_cdBancoDestino= false;
    } //-- void deleteCdBancoDestino() 

    /**
     * Method deleteCdContaBancariaDestino
     * 
     */
    public void deleteCdContaBancariaDestino()
    {
        this._has_cdContaBancariaDestino= false;
    } //-- void deleteCdContaBancariaDestino() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteDgAgenciaDestino
     * 
     */
    public void deleteDgAgenciaDestino()
    {
        this._has_dgAgenciaDestino= false;
    } //-- void deleteDgAgenciaDestino() 

    /**
     * Returns the value of field 'cdAgenciaDestino'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDestino'.
     */
    public int getCdAgenciaDestino()
    {
        return this._cdAgenciaDestino;
    } //-- int getCdAgenciaDestino() 

    /**
     * Returns the value of field 'cdBancoDestino'.
     * 
     * @return int
     * @return the value of field 'cdBancoDestino'.
     */
    public int getCdBancoDestino()
    {
        return this._cdBancoDestino;
    } //-- int getCdBancoDestino() 

    /**
     * Returns the value of field 'cdContaBancariaDestino'.
     * 
     * @return long
     * @return the value of field 'cdContaBancariaDestino'.
     */
    public long getCdContaBancariaDestino()
    {
        return this._cdContaBancariaDestino;
    } //-- long getCdContaBancariaDestino() 

    /**
     * Returns the value of field 'cdContingenciaTed'.
     * 
     * @return String
     * @return the value of field 'cdContingenciaTed'.
     */
    public java.lang.String getCdContingenciaTed()
    {
        return this._cdContingenciaTed;
    } //-- java.lang.String getCdContingenciaTed() 

    /**
     * Returns the value of field 'cdOperCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalInclusao'.
     */
    public java.lang.String getCdOperCanalInclusao()
    {
        return this._cdOperCanalInclusao;
    } //-- java.lang.String getCdOperCanalInclusao() 

    /**
     * Returns the value of field 'cdOperCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperCanalManutencao'.
     */
    public java.lang.String getCdOperCanalManutencao()
    {
        return this._cdOperCanalManutencao;
    } //-- java.lang.String getCdOperCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContaDestino'.
     * 
     * @return String
     * @return the value of field 'cdTipoContaDestino'.
     */
    public java.lang.String getCdTipoContaDestino()
    {
        return this._cdTipoContaDestino;
    } //-- java.lang.String getCdTipoContaDestino() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExter'.
     */
    public java.lang.String getCdUsuarioInclusaoExter()
    {
        return this._cdUsuarioInclusaoExter;
    } //-- java.lang.String getCdUsuarioInclusaoExter() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExter'.
     */
    public java.lang.String getCdUsuarioManutencaoExter()
    {
        return this._cdUsuarioManutencaoExter;
    } //-- java.lang.String getCdUsuarioManutencaoExter() 

    /**
     * Returns the value of field 'dgAgenciaDestino'.
     * 
     * @return int
     * @return the value of field 'dgAgenciaDestino'.
     */
    public int getDgAgenciaDestino()
    {
        return this._dgAgenciaDestino;
    } //-- int getDgAgenciaDestino() 

    /**
     * Returns the value of field 'dgContaDestino'.
     * 
     * @return String
     * @return the value of field 'dgContaDestino'.
     */
    public java.lang.String getDgContaDestino()
    {
        return this._dgContaDestino;
    } //-- java.lang.String getDgContaDestino() 

    /**
     * Returns the value of field 'dsAgenciaDestino'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDestino'.
     */
    public java.lang.String getDsAgenciaDestino()
    {
        return this._dsAgenciaDestino;
    } //-- java.lang.String getDsAgenciaDestino() 

    /**
     * Returns the value of field 'dsBancoDestino'.
     * 
     * @return String
     * @return the value of field 'dsBancoDestino'.
     */
    public java.lang.String getDsBancoDestino()
    {
        return this._dsBancoDestino;
    } //-- java.lang.String getDsBancoDestino() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtMantuencao'.
     * 
     * @return String
     * @return the value of field 'dtMantuencao'.
     */
    public java.lang.String getDtMantuencao()
    {
        return this._dtMantuencao;
    } //-- java.lang.String getDtMantuencao() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'tpManutencao'.
     * 
     * @return String
     * @return the value of field 'tpManutencao'.
     */
    public java.lang.String getTpManutencao()
    {
        return this._tpManutencao;
    } //-- java.lang.String getTpManutencao() 

    /**
     * Method hasCdAgenciaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDestino()
    {
        return this._has_cdAgenciaDestino;
    } //-- boolean hasCdAgenciaDestino() 

    /**
     * Method hasCdBancoDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDestino()
    {
        return this._has_cdBancoDestino;
    } //-- boolean hasCdBancoDestino() 

    /**
     * Method hasCdContaBancariaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancariaDestino()
    {
        return this._has_cdContaBancariaDestino;
    } //-- boolean hasCdContaBancariaDestino() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasDgAgenciaDestino
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDgAgenciaDestino()
    {
        return this._has_dgAgenciaDestino;
    } //-- boolean hasDgAgenciaDestino() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDestino'.
     * 
     * @param cdAgenciaDestino the value of field 'cdAgenciaDestino'
     */
    public void setCdAgenciaDestino(int cdAgenciaDestino)
    {
        this._cdAgenciaDestino = cdAgenciaDestino;
        this._has_cdAgenciaDestino = true;
    } //-- void setCdAgenciaDestino(int) 

    /**
     * Sets the value of field 'cdBancoDestino'.
     * 
     * @param cdBancoDestino the value of field 'cdBancoDestino'.
     */
    public void setCdBancoDestino(int cdBancoDestino)
    {
        this._cdBancoDestino = cdBancoDestino;
        this._has_cdBancoDestino = true;
    } //-- void setCdBancoDestino(int) 

    /**
     * Sets the value of field 'cdContaBancariaDestino'.
     * 
     * @param cdContaBancariaDestino the value of field
     * 'cdContaBancariaDestino'.
     */
    public void setCdContaBancariaDestino(long cdContaBancariaDestino)
    {
        this._cdContaBancariaDestino = cdContaBancariaDestino;
        this._has_cdContaBancariaDestino = true;
    } //-- void setCdContaBancariaDestino(long) 

    /**
     * Sets the value of field 'cdContingenciaTed'.
     * 
     * @param cdContingenciaTed the value of field
     * 'cdContingenciaTed'.
     */
    public void setCdContingenciaTed(java.lang.String cdContingenciaTed)
    {
        this._cdContingenciaTed = cdContingenciaTed;
    } //-- void setCdContingenciaTed(java.lang.String) 

    /**
     * Sets the value of field 'cdOperCanalInclusao'.
     * 
     * @param cdOperCanalInclusao the value of field
     * 'cdOperCanalInclusao'.
     */
    public void setCdOperCanalInclusao(java.lang.String cdOperCanalInclusao)
    {
        this._cdOperCanalInclusao = cdOperCanalInclusao;
    } //-- void setCdOperCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperCanalManutencao'.
     * 
     * @param cdOperCanalManutencao the value of field
     * 'cdOperCanalManutencao'.
     */
    public void setCdOperCanalManutencao(java.lang.String cdOperCanalManutencao)
    {
        this._cdOperCanalManutencao = cdOperCanalManutencao;
    } //-- void setCdOperCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContaDestino'.
     * 
     * @param cdTipoContaDestino the value of field
     * 'cdTipoContaDestino'.
     */
    public void setCdTipoContaDestino(java.lang.String cdTipoContaDestino)
    {
        this._cdTipoContaDestino = cdTipoContaDestino;
    } //-- void setCdTipoContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @param cdUsuarioInclusaoExter the value of field
     * 'cdUsuarioInclusaoExter'.
     */
    public void setCdUsuarioInclusaoExter(java.lang.String cdUsuarioInclusaoExter)
    {
        this._cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
    } //-- void setCdUsuarioInclusaoExter(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @param cdUsuarioManutencaoExter the value of field
     * 'cdUsuarioManutencaoExter'.
     */
    public void setCdUsuarioManutencaoExter(java.lang.String cdUsuarioManutencaoExter)
    {
        this._cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
    } //-- void setCdUsuarioManutencaoExter(java.lang.String) 

    /**
     * Sets the value of field 'dgAgenciaDestino'.
     * 
     * @param dgAgenciaDestino the value of field 'dgAgenciaDestino'
     */
    public void setDgAgenciaDestino(int dgAgenciaDestino)
    {
        this._dgAgenciaDestino = dgAgenciaDestino;
        this._has_dgAgenciaDestino = true;
    } //-- void setDgAgenciaDestino(int) 

    /**
     * Sets the value of field 'dgContaDestino'.
     * 
     * @param dgContaDestino the value of field 'dgContaDestino'.
     */
    public void setDgContaDestino(java.lang.String dgContaDestino)
    {
        this._dgContaDestino = dgContaDestino;
    } //-- void setDgContaDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDestino'.
     * 
     * @param dsAgenciaDestino the value of field 'dsAgenciaDestino'
     */
    public void setDsAgenciaDestino(java.lang.String dsAgenciaDestino)
    {
        this._dsAgenciaDestino = dsAgenciaDestino;
    } //-- void setDsAgenciaDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDestino'.
     * 
     * @param dsBancoDestino the value of field 'dsBancoDestino'.
     */
    public void setDsBancoDestino(java.lang.String dsBancoDestino)
    {
        this._dsBancoDestino = dsBancoDestino;
    } //-- void setDsBancoDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtMantuencao'.
     * 
     * @param dtMantuencao the value of field 'dtMantuencao'.
     */
    public void setDtMantuencao(java.lang.String dtMantuencao)
    {
        this._dtMantuencao = dtMantuencao;
    } //-- void setDtMantuencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'tpManutencao'.
     * 
     * @param tpManutencao the value of field 'tpManutencao'.
     */
    public void setTpManutencao(java.lang.String tpManutencao)
    {
        this._tpManutencao = tpManutencao;
    } //-- void setTpManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
