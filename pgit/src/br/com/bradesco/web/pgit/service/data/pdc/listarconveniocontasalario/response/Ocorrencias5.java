/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.math.BigDecimal;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencias5.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias5 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
	 * 
	 */
	private static final long serialVersionUID = 8852888444859507195L;

	/**
     * Field _cdBancoFlQuest
     */
    private int _cdBancoFlQuest = 0;

    /**
     * keeps track of state for field: _cdBancoFlQuest
     */
    private boolean _has_cdBancoFlQuest;

    /**
     * Field _dsBancoFlQuest
     */
    private java.lang.String _dsBancoFlQuest;

    /**
     * Field _pctBancoFlQuest
     */
    private java.math.BigDecimal _pctBancoFlQuest = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias5() 
     {
        super();
        setPctBancoFlQuest(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdBancoFlQuest
     * 
     */
    public void deleteCdBancoFlQuest()
    {
        this._has_cdBancoFlQuest= false;
    } //-- void deleteCdBancoFlQuest() 

    /**
     * Returns the value of field 'cdBancoFlQuest'.
     * 
     * @return int
     * @return the value of field 'cdBancoFlQuest'.
     */
    public int getCdBancoFlQuest()
    {
        return this._cdBancoFlQuest;
    } //-- int getCdBancoFlQuest() 

    /**
     * Returns the value of field 'dsBancoFlQuest'.
     * 
     * @return String
     * @return the value of field 'dsBancoFlQuest'.
     */
    public java.lang.String getDsBancoFlQuest()
    {
        return this._dsBancoFlQuest;
    } //-- java.lang.String getDsBancoFlQuest() 

    /**
     * Returns the value of field 'pctBancoFlQuest'.
     * 
     * @return BigDecimal
     * @return the value of field 'pctBancoFlQuest'.
     */
    public java.math.BigDecimal getPctBancoFlQuest()
    {
        return this._pctBancoFlQuest;
    } //-- java.math.BigDecimal getPctBancoFlQuest() 

    /**
     * Method hasCdBancoFlQuest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoFlQuest()
    {
        return this._has_cdBancoFlQuest;
    } //-- boolean hasCdBancoFlQuest() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdBancoFlQuest'.
     * 
     * @param cdBancoFlQuest the value of field 'cdBancoFlQuest'.
     */
    public void setCdBancoFlQuest(int cdBancoFlQuest)
    {
        this._cdBancoFlQuest = cdBancoFlQuest;
        this._has_cdBancoFlQuest = true;
    } //-- void setCdBancoFlQuest(int) 

    /**
     * Sets the value of field 'dsBancoFlQuest'.
     * 
     * @param dsBancoFlQuest the value of field 'dsBancoFlQuest'.
     */
    public void setDsBancoFlQuest(java.lang.String dsBancoFlQuest)
    {
        this._dsBancoFlQuest = dsBancoFlQuest;
    } //-- void setDsBancoFlQuest(java.lang.String) 

    /**
     * Sets the value of field 'pctBancoFlQuest'.
     * 
     * @param pctBancoFlQuest the value of field 'pctBancoFlQuest'.
     */
    public void setPctBancoFlQuest(java.math.BigDecimal pctBancoFlQuest)
    {
        this._pctBancoFlQuest = pctBancoFlQuest;
    } //-- void setPctBancoFlQuest(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias5
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 


	public Ocorrencias5(int cdBancoFlQuest, String dsBancoFlQuest, BigDecimal pctBancoFlQuest) {
		super();
		_cdBancoFlQuest = cdBancoFlQuest;
		_dsBancoFlQuest = dsBancoFlQuest;
		_pctBancoFlQuest = pctBancoFlQuest;
	}
  
}
