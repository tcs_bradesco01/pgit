/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _nrMensagemArquivoRetorno
     */
    private int _nrMensagemArquivoRetorno = 0;

    /**
     * keeps track of state for field: _nrMensagemArquivoRetorno
     */
    private boolean _has_nrMensagemArquivoRetorno;

    /**
     * Field _cdMensagemArquivoRetorno
     */
    private java.lang.String _cdMensagemArquivoRetorno;

    /**
     * Field _cdTipoMensagemRetorno
     */
    private int _cdTipoMensagemRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemRetorno
     */
    private boolean _has_cdTipoMensagemRetorno;

    /**
     * Field _dsTipoMansagemRetorno
     */
    private java.lang.String _dsTipoMansagemRetorno;

    /**
     * Field _dsMensagemLayoutRetorno
     */
    private java.lang.String _dsMensagemLayoutRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoMensagemRetorno
     * 
     */
    public void deleteCdTipoMensagemRetorno()
    {
        this._has_cdTipoMensagemRetorno= false;
    } //-- void deleteCdTipoMensagemRetorno() 

    /**
     * Method deleteNrMensagemArquivoRetorno
     * 
     */
    public void deleteNrMensagemArquivoRetorno()
    {
        this._has_nrMensagemArquivoRetorno= false;
    } //-- void deleteNrMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'cdMensagemArquivoRetorno'.
     */
    public java.lang.String getCdMensagemArquivoRetorno()
    {
        return this._cdMensagemArquivoRetorno;
    } //-- java.lang.String getCdMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoMensagemRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemRetorno'.
     */
    public int getCdTipoMensagemRetorno()
    {
        return this._cdTipoMensagemRetorno;
    } //-- int getCdTipoMensagemRetorno() 

    /**
     * Returns the value of field 'dsMensagemLayoutRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMensagemLayoutRetorno'.
     */
    public java.lang.String getDsMensagemLayoutRetorno()
    {
        return this._dsMensagemLayoutRetorno;
    } //-- java.lang.String getDsMensagemLayoutRetorno() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoMansagemRetorno'.
     * 
     * @return String
     * @return the value of field 'dsTipoMansagemRetorno'.
     */
    public java.lang.String getDsTipoMansagemRetorno()
    {
        return this._dsTipoMansagemRetorno;
    } //-- java.lang.String getDsTipoMansagemRetorno() 

    /**
     * Returns the value of field 'nrMensagemArquivoRetorno'.
     * 
     * @return int
     * @return the value of field 'nrMensagemArquivoRetorno'.
     */
    public int getNrMensagemArquivoRetorno()
    {
        return this._nrMensagemArquivoRetorno;
    } //-- int getNrMensagemArquivoRetorno() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoMensagemRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemRetorno()
    {
        return this._has_cdTipoMensagemRetorno;
    } //-- boolean hasCdTipoMensagemRetorno() 

    /**
     * Method hasNrMensagemArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMensagemArquivoRetorno()
    {
        return this._has_nrMensagemArquivoRetorno;
    } //-- boolean hasNrMensagemArquivoRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @param cdMensagemArquivoRetorno the value of field
     * 'cdMensagemArquivoRetorno'.
     */
    public void setCdMensagemArquivoRetorno(java.lang.String cdMensagemArquivoRetorno)
    {
        this._cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
    } //-- void setCdMensagemArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoMensagemRetorno'.
     * 
     * @param cdTipoMensagemRetorno the value of field
     * 'cdTipoMensagemRetorno'.
     */
    public void setCdTipoMensagemRetorno(int cdTipoMensagemRetorno)
    {
        this._cdTipoMensagemRetorno = cdTipoMensagemRetorno;
        this._has_cdTipoMensagemRetorno = true;
    } //-- void setCdTipoMensagemRetorno(int) 

    /**
     * Sets the value of field 'dsMensagemLayoutRetorno'.
     * 
     * @param dsMensagemLayoutRetorno the value of field
     * 'dsMensagemLayoutRetorno'.
     */
    public void setDsMensagemLayoutRetorno(java.lang.String dsMensagemLayoutRetorno)
    {
        this._dsMensagemLayoutRetorno = dsMensagemLayoutRetorno;
    } //-- void setDsMensagemLayoutRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoMansagemRetorno'.
     * 
     * @param dsTipoMansagemRetorno the value of field
     * 'dsTipoMansagemRetorno'.
     */
    public void setDsTipoMansagemRetorno(java.lang.String dsTipoMansagemRetorno)
    {
        this._dsTipoMansagemRetorno = dsTipoMansagemRetorno;
    } //-- void setDsTipoMansagemRetorno(java.lang.String) 

    /**
     * Sets the value of field 'nrMensagemArquivoRetorno'.
     * 
     * @param nrMensagemArquivoRetorno the value of field
     * 'nrMensagemArquivoRetorno'.
     */
    public void setNrMensagemArquivoRetorno(int nrMensagemArquivoRetorno)
    {
        this._nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
        this._has_nrMensagemArquivoRetorno = true;
    } //-- void setNrMensagemArquivoRetorno(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmsglayoutarqretorno.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
