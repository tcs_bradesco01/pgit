/*
 * Nome: br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assolotelayoutarquivo.bean;

/**
 * Nome: AssLoteLayoutArquivoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssLoteLayoutArquivoEntradaDTO {


	
	/** Atributo cdTipoLayoutArquivo. */
	private int cdTipoLayoutArquivo;
	
	/** Atributo cdTipoLoteLayout. */
	private int cdTipoLoteLayout;
	
	/** Atributo qtConsultas. */
	private int qtConsultas;
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public int getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoLoteLayout.
	 *
	 * @return cdTipoLoteLayout
	 */
	public int getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}
	
	/**
	 * Set: cdTipoLoteLayout.
	 *
	 * @param cdTipoLoteLayout the cd tipo lote layout
	 */
	public void setCdTipoLoteLayout(int cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}
	
	/**
	 * Get: qtConsultas.
	 *
	 * @return qtConsultas
	 */
	public int getQtConsultas() {
		return qtConsultas;
	}
	
	/**
	 * Set: qtConsultas.
	 *
	 * @param qtConsultas the qt consultas
	 */
	public void setQtConsultas(int qtConsultas) {
		this.qtConsultas = qtConsultas;
	}

	
	
}
