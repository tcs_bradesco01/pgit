/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 25/05/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: VerificarAtributosDadosBasicosSaidaDto
 * <p>
 * Prop�sito:
 * </p>
 * 	
 * @author : todo!
 * 
 * @version :
 */
public class VerificarAtributosDadosBasicosSaidaDto {

    /** Atributo codMensagem. */
    private String codMensagem = null;

    /** Atributo mensagem. */
    private String mensagem = null;

    /** Atributo cdFormaAutorizacaoPagamentoAlterar. */
    private Integer cdFormaAutorizacaoPagamentoAlterar = null;

    /**
     * Nome: getCodMensagem
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }

    /**
     * Nome: setCodMensagem
     *
     * @param codMensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Nome: getMensagem
     *
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Nome: setMensagem
     *
     * @param mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Nome: getCdFormaAutorizacaoPagamentoAlterar
     *
     * @return cdFormaAutorizacaoPagamentoAlterar
     */
    public Integer getCdFormaAutorizacaoPagamentoAlterar() {
        return cdFormaAutorizacaoPagamentoAlterar;
    }

    /**
     * Nome: setCdFormaAutorizacaoPagamentoAlterar
     *
     * @param cdFormaAutorizacaoPagamentoAlterar
     */
    public void setCdFormaAutorizacaoPagamentoAlterar(Integer cdFormaAutorizacaoPagamentoAlterar) {
        this.cdFormaAutorizacaoPagamentoAlterar = cdFormaAutorizacaoPagamentoAlterar;
    }
}
