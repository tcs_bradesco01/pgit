/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean;

/**
 * Nome: DetalharAssLayoutArqProdServicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharAssLayoutArqProdServicoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLayoutArquivo. */
	private int cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;	
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdIdentificadorLayoutNegocio. */
	private Integer cdIdentificadorLayoutNegocio;
	
	/** Atributo dsIdentificadorLayoutNegocio. */
	private String dsIdentificadorLayoutNegocio;
	
	/** Atributo cdIndicadorLayoutDefault. */
	private Integer cdIndicadorLayoutDefault;
	
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public int getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: cdIdentificadorLayoutNegocio.
	 *
	 * @return cdIdentificadorLayoutNegocio
	 */
	public Integer getCdIdentificadorLayoutNegocio() {
		return cdIdentificadorLayoutNegocio;
	}
	
	/**
	 * Set: cdIdentificadorLayoutNegocio.
	 *
	 * @param cdIdentificadorLayoutNegocio the cd identificador layout negocio
	 */
	public void setCdIdentificadorLayoutNegocio(Integer cdIdentificadorLayoutNegocio) {
		this.cdIdentificadorLayoutNegocio = cdIdentificadorLayoutNegocio;
	}
	
	/**
	 * Get: dsIdentificadorLayoutNegocio.
	 *
	 * @return dsIdentificadorLayoutNegocio
	 */
	public String getDsIdentificadorLayoutNegocio() {
		return dsIdentificadorLayoutNegocio;
	}
	
	/**
	 * Set: dsIdentificadorLayoutNegocio.
	 *
	 * @param dsIdentificadorLayoutNegocio the ds identificador layout negocio
	 */
	public void setDsIdentificadorLayoutNegocio(String dsIdentificadorLayoutNegocio) {
		this.dsIdentificadorLayoutNegocio = dsIdentificadorLayoutNegocio;
	}

	/**
	 * @param cdIndicadorLayoutDefault the cdIndicadorLayoutDefault to set
	 */
	public void setCdIndicadorLayoutDefault(Integer cdIndicadorLayoutDefault) {
		this.cdIndicadorLayoutDefault = cdIndicadorLayoutDefault;
	}

	/**
	 * @return the cdIndicadorLayoutDefault
	 */
	public Integer getCdIndicadorLayoutDefault() {
		return cdIndicadorLayoutDefault;
	}
	
	

}
