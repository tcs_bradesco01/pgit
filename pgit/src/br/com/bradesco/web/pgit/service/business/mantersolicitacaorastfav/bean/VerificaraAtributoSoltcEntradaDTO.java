/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean;

/**
 * Nome: VerificaraAtributoSoltcEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class VerificaraAtributoSoltcEntradaDTO {

	/** Atributo cdFuncionario. */
	private Integer cdFuncionario;

	/**
	 * Verificara atributo soltc entrada dto.
	 *
	 * @param cdFuncionario the cd funcionario
	 */
	public VerificaraAtributoSoltcEntradaDTO(Integer cdFuncionario) {
		super();
		this.cdFuncionario = cdFuncionario;
	}

	/**
	 * Get: cdFuncionario.
	 *
	 * @return cdFuncionario
	 */
	public Integer getCdFuncionario() {
		return cdFuncionario;
	}

	/**
	 * Set: cdFuncionario.
	 *
	 * @param cdFuncionario the cd funcionario
	 */
	public void setCdFuncionario(Integer cdFuncionario) {
		this.cdFuncionario = cdFuncionario;
	}

}
