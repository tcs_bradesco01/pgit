/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMotivoBloqueioConta
     */
    private int _cdMotivoBloqueioConta = 0;

    /**
     * keeps track of state for field: _cdMotivoBloqueioConta
     */
    private boolean _has_cdMotivoBloqueioConta;

    /**
     * Field _dsMotivoBloqueioConta
     */
    private java.lang.String _dsMotivoBloqueioConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoBloqueioConta
     * 
     */
    public void deleteCdMotivoBloqueioConta()
    {
        this._has_cdMotivoBloqueioConta= false;
    } //-- void deleteCdMotivoBloqueioConta() 

    /**
     * Returns the value of field 'cdMotivoBloqueioConta'.
     * 
     * @return int
     * @return the value of field 'cdMotivoBloqueioConta'.
     */
    public int getCdMotivoBloqueioConta()
    {
        return this._cdMotivoBloqueioConta;
    } //-- int getCdMotivoBloqueioConta() 

    /**
     * Returns the value of field 'dsMotivoBloqueioConta'.
     * 
     * @return String
     * @return the value of field 'dsMotivoBloqueioConta'.
     */
    public java.lang.String getDsMotivoBloqueioConta()
    {
        return this._dsMotivoBloqueioConta;
    } //-- java.lang.String getDsMotivoBloqueioConta() 

    /**
     * Method hasCdMotivoBloqueioConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoBloqueioConta()
    {
        return this._has_cdMotivoBloqueioConta;
    } //-- boolean hasCdMotivoBloqueioConta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoBloqueioConta'.
     * 
     * @param cdMotivoBloqueioConta the value of field
     * 'cdMotivoBloqueioConta'.
     */
    public void setCdMotivoBloqueioConta(int cdMotivoBloqueioConta)
    {
        this._cdMotivoBloqueioConta = cdMotivoBloqueioConta;
        this._has_cdMotivoBloqueioConta = true;
    } //-- void setCdMotivoBloqueioConta(int) 

    /**
     * Sets the value of field 'dsMotivoBloqueioConta'.
     * 
     * @param dsMotivoBloqueioConta the value of field
     * 'dsMotivoBloqueioConta'.
     */
    public void setDsMotivoBloqueioConta(java.lang.String dsMotivoBloqueioConta)
    {
        this._dsMotivoBloqueioConta = dsMotivoBloqueioConta;
    } //-- void setDsMotivoBloqueioConta(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmotivobloqueioconta.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
