/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirVinculoContaSalarioDestinoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirVinculoContaSalarioDestinoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridNegocio
     */
    private long _cdPessoaJuridNegocio = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridNegocio
     */
    private boolean _has_cdPessoaJuridNegocio;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _cdBancoSalario
     */
    private int _cdBancoSalario = 0;

    /**
     * keeps track of state for field: _cdBancoSalario
     */
    private boolean _has_cdBancoSalario;

    /**
     * Field _cdAgenciaSalario
     */
    private int _cdAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdAgenciaSalario
     */
    private boolean _has_cdAgenciaSalario;

    /**
     * Field _cdDigitoAgenciaSalario
     */
    private int _cdDigitoAgenciaSalario = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaSalario
     */
    private boolean _has_cdDigitoAgenciaSalario;

    /**
     * Field _cdContaSalario
     */
    private long _cdContaSalario = 0;

    /**
     * keeps track of state for field: _cdContaSalario
     */
    private boolean _has_cdContaSalario;

    /**
     * Field _cdDigitoContaSalario
     */
    private java.lang.String _cdDigitoContaSalario;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _cdContingenciaTed
     */
    private int _cdContingenciaTed = 0;

    /**
     * keeps track of state for field: _cdContingenciaTed
     */
    private boolean _has_cdContingenciaTed;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirVinculoContaSalarioDestinoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.request.IncluirVinculoContaSalarioDestinoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdAgenciaSalario
     * 
     */
    public void deleteCdAgenciaSalario()
    {
        this._has_cdAgenciaSalario= false;
    } //-- void deleteCdAgenciaSalario() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdBancoSalario
     * 
     */
    public void deleteCdBancoSalario()
    {
        this._has_cdBancoSalario= false;
    } //-- void deleteCdBancoSalario() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdContaSalario
     * 
     */
    public void deleteCdContaSalario()
    {
        this._has_cdContaSalario= false;
    } //-- void deleteCdContaSalario() 

    /**
     * Method deleteCdContingenciaTed
     * 
     */
    public void deleteCdContingenciaTed()
    {
        this._has_cdContingenciaTed= false;
    } //-- void deleteCdContingenciaTed() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdDigitoAgenciaSalario
     * 
     */
    public void deleteCdDigitoAgenciaSalario()
    {
        this._has_cdDigitoAgenciaSalario= false;
    } //-- void deleteCdDigitoAgenciaSalario() 

    /**
     * Method deleteCdPessoaJuridNegocio
     * 
     */
    public void deleteCdPessoaJuridNegocio()
    {
        this._has_cdPessoaJuridNegocio= false;
    } //-- void deleteCdPessoaJuridNegocio() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaSalario'.
     */
    public int getCdAgenciaSalario()
    {
        return this._cdAgenciaSalario;
    } //-- int getCdAgenciaSalario() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdBancoSalario'.
     * 
     * @return int
     * @return the value of field 'cdBancoSalario'.
     */
    public int getCdBancoSalario()
    {
        return this._cdBancoSalario;
    } //-- int getCdBancoSalario() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdContaSalario'.
     * 
     * @return long
     * @return the value of field 'cdContaSalario'.
     */
    public long getCdContaSalario()
    {
        return this._cdContaSalario;
    } //-- long getCdContaSalario() 

    /**
     * Returns the value of field 'cdContingenciaTed'.
     * 
     * @return int
     * @return the value of field 'cdContingenciaTed'.
     */
    public int getCdContingenciaTed()
    {
        return this._cdContingenciaTed;
    } //-- int getCdContingenciaTed() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaSalario'.
     */
    public int getCdDigitoAgenciaSalario()
    {
        return this._cdDigitoAgenciaSalario;
    } //-- int getCdDigitoAgenciaSalario() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdDigitoContaSalario'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaSalario'.
     */
    public java.lang.String getCdDigitoContaSalario()
    {
        return this._cdDigitoContaSalario;
    } //-- java.lang.String getCdDigitoContaSalario() 

    /**
     * Returns the value of field 'cdPessoaJuridNegocio'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridNegocio'.
     */
    public long getCdPessoaJuridNegocio()
    {
        return this._cdPessoaJuridNegocio;
    } //-- long getCdPessoaJuridNegocio() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaSalario()
    {
        return this._has_cdAgenciaSalario;
    } //-- boolean hasCdAgenciaSalario() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdBancoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoSalario()
    {
        return this._has_cdBancoSalario;
    } //-- boolean hasCdBancoSalario() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdContaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaSalario()
    {
        return this._has_cdContaSalario;
    } //-- boolean hasCdContaSalario() 

    /**
     * Method hasCdContingenciaTed
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContingenciaTed()
    {
        return this._has_cdContingenciaTed;
    } //-- boolean hasCdContingenciaTed() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdDigitoAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaSalario()
    {
        return this._has_cdDigitoAgenciaSalario;
    } //-- boolean hasCdDigitoAgenciaSalario() 

    /**
     * Method hasCdPessoaJuridNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridNegocio()
    {
        return this._has_cdPessoaJuridNegocio;
    } //-- boolean hasCdPessoaJuridNegocio() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdAgenciaSalario'.
     * 
     * @param cdAgenciaSalario the value of field 'cdAgenciaSalario'
     */
    public void setCdAgenciaSalario(int cdAgenciaSalario)
    {
        this._cdAgenciaSalario = cdAgenciaSalario;
        this._has_cdAgenciaSalario = true;
    } //-- void setCdAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdBancoSalario'.
     * 
     * @param cdBancoSalario the value of field 'cdBancoSalario'.
     */
    public void setCdBancoSalario(int cdBancoSalario)
    {
        this._cdBancoSalario = cdBancoSalario;
        this._has_cdBancoSalario = true;
    } //-- void setCdBancoSalario(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdContaSalario'.
     * 
     * @param cdContaSalario the value of field 'cdContaSalario'.
     */
    public void setCdContaSalario(long cdContaSalario)
    {
        this._cdContaSalario = cdContaSalario;
        this._has_cdContaSalario = true;
    } //-- void setCdContaSalario(long) 

    /**
     * Sets the value of field 'cdContingenciaTed'.
     * 
     * @param cdContingenciaTed the value of field
     * 'cdContingenciaTed'.
     */
    public void setCdContingenciaTed(int cdContingenciaTed)
    {
        this._cdContingenciaTed = cdContingenciaTed;
        this._has_cdContingenciaTed = true;
    } //-- void setCdContingenciaTed(int) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoAgenciaSalario'.
     * 
     * @param cdDigitoAgenciaSalario the value of field
     * 'cdDigitoAgenciaSalario'.
     */
    public void setCdDigitoAgenciaSalario(int cdDigitoAgenciaSalario)
    {
        this._cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
        this._has_cdDigitoAgenciaSalario = true;
    } //-- void setCdDigitoAgenciaSalario(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaSalario'.
     * 
     * @param cdDigitoContaSalario the value of field
     * 'cdDigitoContaSalario'.
     */
    public void setCdDigitoContaSalario(java.lang.String cdDigitoContaSalario)
    {
        this._cdDigitoContaSalario = cdDigitoContaSalario;
    } //-- void setCdDigitoContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridNegocio'.
     * 
     * @param cdPessoaJuridNegocio the value of field
     * 'cdPessoaJuridNegocio'.
     */
    public void setCdPessoaJuridNegocio(long cdPessoaJuridNegocio)
    {
        this._cdPessoaJuridNegocio = cdPessoaJuridNegocio;
        this._has_cdPessoaJuridNegocio = true;
    } //-- void setCdPessoaJuridNegocio(long) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirVinculoContaSalarioDestinoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.request.IncluirVinculoContaSalarioDestinoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.request.IncluirVinculoContaSalarioDestinoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.request.IncluirVinculoContaSalarioDestinoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.request.IncluirVinculoContaSalarioDestinoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
