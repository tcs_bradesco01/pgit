/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

/**
 * Nome: VinculoRelacaoConManutencoesDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class VinculoRelacaoConManutencoesDTO {
	
	/** Atributo nrCpfCnpjConta. */
	private String nrCpfCnpjConta;
	
	/** Atributo nmRazaoSocialConta. */
	private String nmRazaoSocialConta;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo dsBanco. */
	private String dsBanco;
	
	/** Atributo cdAgencia. */
	private Integer cdAgencia;
	
	/** Atributo dsAgencia. */
	private String dsAgencia;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo dsAcaoManutencaoConta. */
	private String dsAcaoManutencaoConta;
	
	/** Atributo cdConta. */
	private Long cdConta;
	
	/** Atributo cdTipoConta. */
	private String cdTipoConta;
	
	/** Atributo dsConta. */
	private String dsConta;
	
	/**
	 * Get: nrCpfCnpjConta.
	 *
	 * @return nrCpfCnpjConta
	 */
	public String getNrCpfCnpjConta() {
		return nrCpfCnpjConta;
	}
	
	/**
	 * Set: nrCpfCnpjConta.
	 *
	 * @param nrCpfCnpjConta the nr cpf cnpj conta
	 */
	public void setNrCpfCnpjConta(String nrCpfCnpjConta) {
		this.nrCpfCnpjConta = nrCpfCnpjConta;
	}
	
	/**
	 * Get: nmRazaoSocialConta.
	 *
	 * @return nmRazaoSocialConta
	 */
	public String getNmRazaoSocialConta() {
		return nmRazaoSocialConta;
	}
	
	/**
	 * Set: nmRazaoSocialConta.
	 *
	 * @param nmRazaoSocialConta the nm razao social conta
	 */
	public void setNmRazaoSocialConta(String nmRazaoSocialConta) {
		this.nmRazaoSocialConta = nmRazaoSocialConta;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: dsBanco.
	 *
	 * @return dsBanco
	 */
	public String getDsBanco() {
		return dsBanco;
	}
	
	/**
	 * Set: dsBanco.
	 *
	 * @param dsBanco the ds banco
	 */
	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: dsAcaoManutencaoConta.
	 *
	 * @return dsAcaoManutencaoConta
	 */
	public String getDsAcaoManutencaoConta() {
		return dsAcaoManutencaoConta;
	}
	
	/**
	 * Set: dsAcaoManutencaoConta.
	 *
	 * @param dsAcaoManutencaoConta the ds acao manutencao conta
	 */
	public void setDsAcaoManutencaoConta(String dsAcaoManutencaoConta) {
		this.dsAcaoManutencaoConta = dsAcaoManutencaoConta;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public String getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(String cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: dsConta.
	 *
	 * @return dsConta
	 */
	public String getDsConta() {
		return dsConta;
	}
	
	/**
	 * Set: dsConta.
	 *
	 * @param dsConta the ds conta
	 */
	public void setDsConta(String dsConta) {
		this.dsConta = dsConta;
	}

}
