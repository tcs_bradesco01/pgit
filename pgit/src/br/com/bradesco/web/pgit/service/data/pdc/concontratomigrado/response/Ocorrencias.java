/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _centroCustoOrigem
     */
    private java.lang.String _centroCustoOrigem;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _dtMigracao
     */
    private java.lang.String _dtMigracao;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _filiaCnpj
     */
    private int _filiaCnpj = 0;

    /**
     * keeps track of state for field: _filiaCnpj
     */
    private boolean _has_filiaCnpj;

    /**
     * Field _digitoCnpj
     */
    private int _digitoCnpj = 0;

    /**
     * keeps track of state for field: _digitoCnpj
     */
    private boolean _has_digitoCnpj;

    /**
     * Field _dsNome
     */
    private java.lang.String _dsNome;

    /**
     * Field _cdPerfil
     */
    private long _cdPerfil = 0;

    /**
     * keeps track of state for field: _cdPerfil
     */
    private boolean _has_cdPerfil;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _razao
     */
    private int _razao = 0;

    /**
     * keeps track of state for field: _razao
     */
    private boolean _has_razao;

    /**
     * Field _cdConta
     */
    private int _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _digitoConta
     */
    private int _digitoConta = 0;

    /**
     * keeps track of state for field: _digitoConta
     */
    private boolean _has_digitoConta;

    /**
     * Field _cdLancamento
     */
    private int _cdLancamento = 0;

    /**
     * keeps track of state for field: _cdLancamento
     */
    private boolean _has_cdLancamento;

    /**
     * Field _cdCpfCnpjRepresentante
     */
    private long _cdCpfCnpjRepresentante = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRepresentante
     */
    private boolean _has_cdCpfCnpjRepresentante;

    /**
     * Field _cdFilialRepresentante
     */
    private int _cdFilialRepresentante = 0;

    /**
     * keeps track of state for field: _cdFilialRepresentante
     */
    private boolean _has_cdFilialRepresentante;

    /**
     * Field _cdCpfDigitoRepresentante
     */
    private int _cdCpfDigitoRepresentante = 0;

    /**
     * keeps track of state for field: _cdCpfDigitoRepresentante
     */
    private boolean _has_cdCpfDigitoRepresentante;

    /**
     * Field _dsRezaoSocialRepresentente
     */
    private java.lang.String _dsRezaoSocialRepresentente;

    /**
     * Field _dsPessoaJuridicaContrato
     */
    private java.lang.String _dsPessoaJuridicaContrato;

    /**
     * Field _dsTipoContratoNegocio
     */
    private java.lang.String _dsTipoContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdCpfCnpjRepresentante
     * 
     */
    public void deleteCdCpfCnpjRepresentante()
    {
        this._has_cdCpfCnpjRepresentante= false;
    } //-- void deleteCdCpfCnpjRepresentante() 

    /**
     * Method deleteCdCpfDigitoRepresentante
     * 
     */
    public void deleteCdCpfDigitoRepresentante()
    {
        this._has_cdCpfDigitoRepresentante= false;
    } //-- void deleteCdCpfDigitoRepresentante() 

    /**
     * Method deleteCdFilialRepresentante
     * 
     */
    public void deleteCdFilialRepresentante()
    {
        this._has_cdFilialRepresentante= false;
    } //-- void deleteCdFilialRepresentante() 

    /**
     * Method deleteCdLancamento
     * 
     */
    public void deleteCdLancamento()
    {
        this._has_cdLancamento= false;
    } //-- void deleteCdLancamento() 

    /**
     * Method deleteCdPerfil
     * 
     */
    public void deleteCdPerfil()
    {
        this._has_cdPerfil= false;
    } //-- void deleteCdPerfil() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteDigitoCnpj
     * 
     */
    public void deleteDigitoCnpj()
    {
        this._has_digitoCnpj= false;
    } //-- void deleteDigitoCnpj() 

    /**
     * Method deleteDigitoConta
     * 
     */
    public void deleteDigitoConta()
    {
        this._has_digitoConta= false;
    } //-- void deleteDigitoConta() 

    /**
     * Method deleteFiliaCnpj
     * 
     */
    public void deleteFiliaCnpj()
    {
        this._has_filiaCnpj= false;
    } //-- void deleteFiliaCnpj() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteRazao
     * 
     */
    public void deleteRazao()
    {
        this._has_razao= false;
    } //-- void deleteRazao() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return int
     * @return the value of field 'cdConta'.
     */
    public int getCdConta()
    {
        return this._cdConta;
    } //-- int getCdConta() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRepresentante'.
     */
    public long getCdCpfCnpjRepresentante()
    {
        return this._cdCpfCnpjRepresentante;
    } //-- long getCdCpfCnpjRepresentante() 

    /**
     * Returns the value of field 'cdCpfDigitoRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdCpfDigitoRepresentante'.
     */
    public int getCdCpfDigitoRepresentante()
    {
        return this._cdCpfDigitoRepresentante;
    } //-- int getCdCpfDigitoRepresentante() 

    /**
     * Returns the value of field 'cdFilialRepresentante'.
     * 
     * @return int
     * @return the value of field 'cdFilialRepresentante'.
     */
    public int getCdFilialRepresentante()
    {
        return this._cdFilialRepresentante;
    } //-- int getCdFilialRepresentante() 

    /**
     * Returns the value of field 'cdLancamento'.
     * 
     * @return int
     * @return the value of field 'cdLancamento'.
     */
    public int getCdLancamento()
    {
        return this._cdLancamento;
    } //-- int getCdLancamento() 

    /**
     * Returns the value of field 'cdPerfil'.
     * 
     * @return long
     * @return the value of field 'cdPerfil'.
     */
    public long getCdPerfil()
    {
        return this._cdPerfil;
    } //-- long getCdPerfil() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'centroCustoOrigem'.
     * 
     * @return String
     * @return the value of field 'centroCustoOrigem'.
     */
    public java.lang.String getCentroCustoOrigem()
    {
        return this._centroCustoOrigem;
    } //-- java.lang.String getCentroCustoOrigem() 

    /**
     * Returns the value of field 'digitoCnpj'.
     * 
     * @return int
     * @return the value of field 'digitoCnpj'.
     */
    public int getDigitoCnpj()
    {
        return this._digitoCnpj;
    } //-- int getDigitoCnpj() 

    /**
     * Returns the value of field 'digitoConta'.
     * 
     * @return int
     * @return the value of field 'digitoConta'.
     */
    public int getDigitoConta()
    {
        return this._digitoConta;
    } //-- int getDigitoConta() 

    /**
     * Returns the value of field 'dsNome'.
     * 
     * @return String
     * @return the value of field 'dsNome'.
     */
    public java.lang.String getDsNome()
    {
        return this._dsNome;
    } //-- java.lang.String getDsNome() 

    /**
     * Returns the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaContrato'.
     */
    public java.lang.String getDsPessoaJuridicaContrato()
    {
        return this._dsPessoaJuridicaContrato;
    } //-- java.lang.String getDsPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsRezaoSocialRepresentente'.
     * 
     * @return String
     * @return the value of field 'dsRezaoSocialRepresentente'.
     */
    public java.lang.String getDsRezaoSocialRepresentente()
    {
        return this._dsRezaoSocialRepresentente;
    } //-- java.lang.String getDsRezaoSocialRepresentente() 

    /**
     * Returns the value of field 'dsTipoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsTipoContratoNegocio'.
     */
    public java.lang.String getDsTipoContratoNegocio()
    {
        return this._dsTipoContratoNegocio;
    } //-- java.lang.String getDsTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtMigracao'.
     * 
     * @return String
     * @return the value of field 'dtMigracao'.
     */
    public java.lang.String getDtMigracao()
    {
        return this._dtMigracao;
    } //-- java.lang.String getDtMigracao() 

    /**
     * Returns the value of field 'filiaCnpj'.
     * 
     * @return int
     * @return the value of field 'filiaCnpj'.
     */
    public int getFiliaCnpj()
    {
        return this._filiaCnpj;
    } //-- int getFiliaCnpj() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'razao'.
     * 
     * @return int
     * @return the value of field 'razao'.
     */
    public int getRazao()
    {
        return this._razao;
    } //-- int getRazao() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdCpfCnpjRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRepresentante()
    {
        return this._has_cdCpfCnpjRepresentante;
    } //-- boolean hasCdCpfCnpjRepresentante() 

    /**
     * Method hasCdCpfDigitoRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfDigitoRepresentante()
    {
        return this._has_cdCpfDigitoRepresentante;
    } //-- boolean hasCdCpfDigitoRepresentante() 

    /**
     * Method hasCdFilialRepresentante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialRepresentante()
    {
        return this._has_cdFilialRepresentante;
    } //-- boolean hasCdFilialRepresentante() 

    /**
     * Method hasCdLancamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdLancamento()
    {
        return this._has_cdLancamento;
    } //-- boolean hasCdLancamento() 

    /**
     * Method hasCdPerfil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfil()
    {
        return this._has_cdPerfil;
    } //-- boolean hasCdPerfil() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasDigitoCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDigitoCnpj()
    {
        return this._has_digitoCnpj;
    } //-- boolean hasDigitoCnpj() 

    /**
     * Method hasDigitoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDigitoConta()
    {
        return this._has_digitoConta;
    } //-- boolean hasDigitoConta() 

    /**
     * Method hasFiliaCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFiliaCnpj()
    {
        return this._has_filiaCnpj;
    } //-- boolean hasFiliaCnpj() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasRazao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasRazao()
    {
        return this._has_razao;
    } //-- boolean hasRazao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(int cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @param cdCpfCnpjRepresentante the value of field
     * 'cdCpfCnpjRepresentante'.
     */
    public void setCdCpfCnpjRepresentante(long cdCpfCnpjRepresentante)
    {
        this._cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
        this._has_cdCpfCnpjRepresentante = true;
    } //-- void setCdCpfCnpjRepresentante(long) 

    /**
     * Sets the value of field 'cdCpfDigitoRepresentante'.
     * 
     * @param cdCpfDigitoRepresentante the value of field
     * 'cdCpfDigitoRepresentante'.
     */
    public void setCdCpfDigitoRepresentante(int cdCpfDigitoRepresentante)
    {
        this._cdCpfDigitoRepresentante = cdCpfDigitoRepresentante;
        this._has_cdCpfDigitoRepresentante = true;
    } //-- void setCdCpfDigitoRepresentante(int) 

    /**
     * Sets the value of field 'cdFilialRepresentante'.
     * 
     * @param cdFilialRepresentante the value of field
     * 'cdFilialRepresentante'.
     */
    public void setCdFilialRepresentante(int cdFilialRepresentante)
    {
        this._cdFilialRepresentante = cdFilialRepresentante;
        this._has_cdFilialRepresentante = true;
    } //-- void setCdFilialRepresentante(int) 

    /**
     * Sets the value of field 'cdLancamento'.
     * 
     * @param cdLancamento the value of field 'cdLancamento'.
     */
    public void setCdLancamento(int cdLancamento)
    {
        this._cdLancamento = cdLancamento;
        this._has_cdLancamento = true;
    } //-- void setCdLancamento(int) 

    /**
     * Sets the value of field 'cdPerfil'.
     * 
     * @param cdPerfil the value of field 'cdPerfil'.
     */
    public void setCdPerfil(long cdPerfil)
    {
        this._cdPerfil = cdPerfil;
        this._has_cdPerfil = true;
    } //-- void setCdPerfil(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'centroCustoOrigem'.
     * 
     * @param centroCustoOrigem the value of field
     * 'centroCustoOrigem'.
     */
    public void setCentroCustoOrigem(java.lang.String centroCustoOrigem)
    {
        this._centroCustoOrigem = centroCustoOrigem;
    } //-- void setCentroCustoOrigem(java.lang.String) 

    /**
     * Sets the value of field 'digitoCnpj'.
     * 
     * @param digitoCnpj the value of field 'digitoCnpj'.
     */
    public void setDigitoCnpj(int digitoCnpj)
    {
        this._digitoCnpj = digitoCnpj;
        this._has_digitoCnpj = true;
    } //-- void setDigitoCnpj(int) 

    /**
     * Sets the value of field 'digitoConta'.
     * 
     * @param digitoConta the value of field 'digitoConta'.
     */
    public void setDigitoConta(int digitoConta)
    {
        this._digitoConta = digitoConta;
        this._has_digitoConta = true;
    } //-- void setDigitoConta(int) 

    /**
     * Sets the value of field 'dsNome'.
     * 
     * @param dsNome the value of field 'dsNome'.
     */
    public void setDsNome(java.lang.String dsNome)
    {
        this._dsNome = dsNome;
    } //-- void setDsNome(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaContrato'.
     * 
     * @param dsPessoaJuridicaContrato the value of field
     * 'dsPessoaJuridicaContrato'.
     */
    public void setDsPessoaJuridicaContrato(java.lang.String dsPessoaJuridicaContrato)
    {
        this._dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
    } //-- void setDsPessoaJuridicaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsRezaoSocialRepresentente'.
     * 
     * @param dsRezaoSocialRepresentente the value of field
     * 'dsRezaoSocialRepresentente'.
     */
    public void setDsRezaoSocialRepresentente(java.lang.String dsRezaoSocialRepresentente)
    {
        this._dsRezaoSocialRepresentente = dsRezaoSocialRepresentente;
    } //-- void setDsRezaoSocialRepresentente(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoContratoNegocio'.
     * 
     * @param dsTipoContratoNegocio the value of field
     * 'dsTipoContratoNegocio'.
     */
    public void setDsTipoContratoNegocio(java.lang.String dsTipoContratoNegocio)
    {
        this._dsTipoContratoNegocio = dsTipoContratoNegocio;
    } //-- void setDsTipoContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dtMigracao'.
     * 
     * @param dtMigracao the value of field 'dtMigracao'.
     */
    public void setDtMigracao(java.lang.String dtMigracao)
    {
        this._dtMigracao = dtMigracao;
    } //-- void setDtMigracao(java.lang.String) 

    /**
     * Sets the value of field 'filiaCnpj'.
     * 
     * @param filiaCnpj the value of field 'filiaCnpj'.
     */
    public void setFiliaCnpj(int filiaCnpj)
    {
        this._filiaCnpj = filiaCnpj;
        this._has_filiaCnpj = true;
    } //-- void setFiliaCnpj(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'razao'.
     * 
     * @param razao the value of field 'razao'.
     */
    public void setRazao(int razao)
    {
        this._razao = razao;
        this._has_razao = true;
    } //-- void setRazao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
