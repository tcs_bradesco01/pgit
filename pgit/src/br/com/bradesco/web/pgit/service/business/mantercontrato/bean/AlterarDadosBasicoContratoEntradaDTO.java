/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: AlterarDadosBasicoContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarDadosBasicoContratoEntradaDTO {

	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo cdSituacaoContrato. */
	private int cdSituacaoContrato;

	/** Atributo cdMotivoSituacaoOperacao. */
	private int cdMotivoSituacaoOperacao;

	/** Atributo cdIndicadorEconomicoMoeda. */
	private int cdIndicadorEconomicoMoeda;

	/** Atributo cdIdioma. */
	private int cdIdioma;

	/** Atributo cdPermissaoEncerramentoContrato. */
	private String cdPermissaoEncerramentoContrato;

	/** Atributo dsContratoNegocio. */
	private String dsContratoNegocio;

	/** Atributo hrAberturaContratoNegocio. */
	private String hrAberturaContratoNegocio;

	/** Atributo cdUsuario. */
	private String cdUsuario;

	/** Atributo cdUnidadeOperacionalContrato. */
	private int cdUnidadeOperacionalContrato;

	/** Atributo cdFuncionarioBradesco. */
	private String cdFuncionarioBradesco;

	/** Atributo cdPessoaJuridica. */
	private long cdPessoaJuridica;

	/** Atributo nrSequenciaUnidadeOrganizacional. */
	private int nrSequenciaUnidadeOrganizacional;

	/** Atributo cdSetorContrato. */
	private Integer cdSetorContrato;

	/** Atributo vlSaldoVirtualTeste. */
	private BigDecimal vlSaldoVirtualTeste;

	/** Atributo cdFormaAutorizacaoPagamento. */
	private Integer cdFormaAutorizacaoPagamento = null;

	/**
	 * Get: vlSaldoVirtualTeste.
	 *
	 * @return vlSaldoVirtualTeste
	 */
	public BigDecimal getVlSaldoVirtualTeste() {
		return vlSaldoVirtualTeste;
	}

	/**
	 * Set: vlSaldoVirtualTeste.
	 *
	 * @param vlSaldoVirtualTeste the vl saldo virtual teste
	 */
	public void setVlSaldoVirtualTeste(BigDecimal vlSaldoVirtualTeste) {
		this.vlSaldoVirtualTeste = vlSaldoVirtualTeste;
	}

	/**
	 * Get: cdFuncionarioBradesco.
	 *
	 * @return cdFuncionarioBradesco
	 */
	public String getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}

	/**
	 * Set: cdFuncionarioBradesco.
	 *
	 * @param cdFuncionarioBradesco the cd funcionario bradesco
	 */
	public void setCdFuncionarioBradesco(String cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}

	/**
	 * Get: cdIdioma.
	 *
	 * @return cdIdioma
	 */
	public int getCdIdioma() {
		return cdIdioma;
	}

	/**
	 * Set: cdIdioma.
	 *
	 * @param cdIdioma the cd idioma
	 */
	public void setCdIdioma(int cdIdioma) {
		this.cdIdioma = cdIdioma;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public int getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(int cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdMotivoSituacaoOperacao.
	 *
	 * @return cdMotivoSituacaoOperacao
	 */
	public int getCdMotivoSituacaoOperacao() {
		return cdMotivoSituacaoOperacao;
	}

	/**
	 * Set: cdMotivoSituacaoOperacao.
	 *
	 * @param cdMotivoSituacaoOperacao the cd motivo situacao operacao
	 */
	public void setCdMotivoSituacaoOperacao(int cdMotivoSituacaoOperacao) {
		this.cdMotivoSituacaoOperacao = cdMotivoSituacaoOperacao;
	}

	/**
	 * Get: cdPermissaoEncerramentoContrato.
	 *
	 * @return cdPermissaoEncerramentoContrato
	 */
	public String getCdPermissaoEncerramentoContrato() {
		return cdPermissaoEncerramentoContrato;
	}

	/**
	 * Set: cdPermissaoEncerramentoContrato.
	 *
	 * @param cdPermissaoEncerramentoContrato the cd permissao encerramento contrato
	 */
	public void setCdPermissaoEncerramentoContrato(String cdPermissaoEncerramentoContrato) {
		this.cdPermissaoEncerramentoContrato = cdPermissaoEncerramentoContrato;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdSetorContrato.
	 *
	 * @return cdSetorContrato
	 */
	public Integer getCdSetorContrato() {
		return cdSetorContrato;
	}

	/**
	 * Set: cdSetorContrato.
	 *
	 * @param cdSetorContrato the cd setor contrato
	 */
	public void setCdSetorContrato(Integer cdSetorContrato) {
		this.cdSetorContrato = cdSetorContrato;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public int getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(int cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdUnidadeOperacionalContrato.
	 *
	 * @return cdUnidadeOperacionalContrato
	 */
	public int getCdUnidadeOperacionalContrato() {
		return cdUnidadeOperacionalContrato;
	}

	/**
	 * Set: cdUnidadeOperacionalContrato.
	 *
	 * @param cdUnidadeOperacionalContrato the cd unidade operacional contrato
	 */
	public void setCdUnidadeOperacionalContrato(int cdUnidadeOperacionalContrato) {
		this.cdUnidadeOperacionalContrato = cdUnidadeOperacionalContrato;
	}

	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * Get: dsContratoNegocio.
	 *
	 * @return dsContratoNegocio
	 */
	public String getDsContratoNegocio() {
		return dsContratoNegocio;
	}

	/**
	 * Set: dsContratoNegocio.
	 *
	 * @param dsContratoNegocio the ds contrato negocio
	 */
	public void setDsContratoNegocio(String dsContratoNegocio) {
		this.dsContratoNegocio = dsContratoNegocio;
	}

	/**
	 * Get: hrAberturaContratoNegocio.
	 *
	 * @return hrAberturaContratoNegocio
	 */
	public String getHrAberturaContratoNegocio() {
		return hrAberturaContratoNegocio;
	}

	/**
	 * Set: hrAberturaContratoNegocio.
	 *
	 * @param hrAberturaContratoNegocio the hr abertura contrato negocio
	 */
	public void setHrAberturaContratoNegocio(String hrAberturaContratoNegocio) {
		this.hrAberturaContratoNegocio = hrAberturaContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: nrSequenciaUnidadeOrganizacional.
	 *
	 * @return nrSequenciaUnidadeOrganizacional
	 */
	public int getNrSequenciaUnidadeOrganizacional() {
		return nrSequenciaUnidadeOrganizacional;
	}

	/**
	 * Set: nrSequenciaUnidadeOrganizacional.
	 *
	 * @param nrSequenciaUnidadeOrganizacional the nr sequencia unidade organizacional
	 */
	public void setNrSequenciaUnidadeOrganizacional(int nrSequenciaUnidadeOrganizacional) {
		this.nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
	}

    /**
     * Nome: getCdFormaAutorizacaoPagamento
     *
     * @return cdFormaAutorizacaoPagamento
     */
    public Integer getCdFormaAutorizacaoPagamento() {
        return cdFormaAutorizacaoPagamento;
    }

    /**
     * Nome: setCdFormaAutorizacaoPagamento
     *
     * @param cdFormaAutorizacaoPagamento
     */
    public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
        this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
    }

}
