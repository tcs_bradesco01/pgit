/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantercadcontadestino;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.AlterarCadContasDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.AlterarCadContasDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConVinculoContaSalarioDestEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConVinculoContaSalarioDestSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarDetHistContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarDetHistContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ExcluirVinculoContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ExcluirVinculoContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.IncluirCadContaDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.IncluirCadContaDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListaVinculoContaSalarioDestEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListaVinculoContaSalarioDestSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListarHistVinculoContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListarHistVinculoContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ValidarServicoContratadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ValidarServicoContratadoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterCadContaDestino
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterCadContaDestinoService {
	
	 /**
 	 * Pesquisar cad conta salario destino.
 	 *
 	 * @param entrada the entrada
 	 * @return the list< lista vinculo conta salario dest saida dt o>
 	 */
 	List<ListaVinculoContaSalarioDestSaidaDTO> pesquisarCadContaSalarioDestino(ListaVinculoContaSalarioDestEntradaDTO entrada);
	
	 /**
 	 * Excluir vinc conta salario destino.
 	 *
 	 * @param entrada the entrada
 	 * @return the excluir vinculo conta salario destino saida dto
 	 */
 	ExcluirVinculoContaSalarioDestinoSaidaDTO excluirVincContaSalarioDestino(ExcluirVinculoContaSalarioDestinoEntradaDTO entrada);
	
	 /**
 	 * Detalhar vinculo conta salario dest.
 	 *
 	 * @param entrada the entrada
 	 * @return the con vinculo conta salario dest saida dto
 	 */
 	ConVinculoContaSalarioDestSaidaDTO detalharVinculoContaSalarioDest (ConVinculoContaSalarioDestEntradaDTO entrada);
	
	 /**
 	 * Incluir vinculo conta salario dest.
 	 *
 	 * @param entrada the entrada
 	 * @return the incluir cad conta destino saida dto
 	 */
 	IncluirCadContaDestinoSaidaDTO incluirVinculoContaSalarioDest(IncluirCadContaDestinoEntradaDTO entrada);
	
	 /**
 	 * Consultar historico conta salario dest.
 	 *
 	 * @param entrada the entrada
 	 * @return the list< listar hist vinculo conta salario destino saida dt o>
 	 */
 	List<ListarHistVinculoContaSalarioDestinoSaidaDTO> consultarHistoricoContaSalarioDest(ListarHistVinculoContaSalarioDestinoEntradaDTO entrada);
	
	 /**
 	 * Alterar cadastro conta destino.
 	 *
 	 * @param entrada the entrada
 	 * @return the alterar cad contas destino saida dto
 	 */
 	AlterarCadContasDestinoSaidaDTO alterarCadastroContaDestino (AlterarCadContasDestinoEntradaDTO entrada);
	
	 /**
 	 * Detalhar hist vinculo conta salario dest.
 	 *
 	 * @param entradaDetHistorico the entrada det historico
 	 * @return the consultar det hist conta salario destino saida dto
 	 */
 	ConsultarDetHistContaSalarioDestinoSaidaDTO detalharHistVinculoContaSalarioDest(ConsultarDetHistContaSalarioDestinoEntradaDTO entradaDetHistorico);
	
	 /**
 	 * Consultar descricao banco agencia.
 	 *
 	 * @param entradaConsultarBancoAgencia the entrada consultar banco agencia
 	 * @return the consultar banco agencia saida dto
 	 */
 	ConsultarBancoAgenciaSaidaDTO consultarDescricaoBancoAgencia(ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia);

	 /**
 	 * Validar servico contratado.
 	 *
 	 * @param entrada the entrada
 	 * @return the validar servico contratado saida dto
 	 */
 	ValidarServicoContratadoSaidaDTO validarServicoContratado(ValidarServicoContratadoEntradaDTO entrada);
}