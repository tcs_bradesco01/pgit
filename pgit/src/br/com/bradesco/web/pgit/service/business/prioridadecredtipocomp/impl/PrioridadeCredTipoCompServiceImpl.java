/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.IPrioridadeCredTipoCompService;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.AlterarPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.AlterarPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.DetalharPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.DetalharPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ExcluirPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ExcluirPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.IncluirPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.IncluirPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ListarPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ListarPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarprioridadetipocompromisso.request.AlterarPrioridadeTipoCompromissoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarprioridadetipocompromisso.response.AlterarPrioridadeTipoCompromissoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.request.DetalharPrioridadeTipoCompromissoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharprioridadetipocompromisso.response.DetalharPrioridadeTipoCompromissoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.request.ExcluirPrioridadeTipoCompromissoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirprioridadetipocompromisso.response.ExcluirPrioridadeTipoCompromissoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.request.IncluirPrioridadeTipoCompromissoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirprioridadetipocompromisso.response.IncluirPrioridadeTipoCompromissoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.request.ListarPrioridadeTipoCompromissoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.response.ListarPrioridadeTipoCompromissoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: PrioridadeCredTipoComp
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class PrioridadeCredTipoCompServiceImpl implements IPrioridadeCredTipoCompService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.IPrioridadeCredTipoCompService#excluirPrioridadeCredTipoComp(br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ExcluirPrioridadeCredTipoCompEntradaDTO)
	 */
	public ExcluirPrioridadeCredTipoCompSaidaDTO excluirPrioridadeCredTipoComp(ExcluirPrioridadeCredTipoCompEntradaDTO excluirPrioridadeCredTipoCompEntradaDTO) {
		
		ExcluirPrioridadeCredTipoCompSaidaDTO excluirPrioridadeCredTipoCompSaidaDTO = new ExcluirPrioridadeCredTipoCompSaidaDTO();
		ExcluirPrioridadeTipoCompromissoRequest excluirPrioridadeTipoCompromissoRequest = new ExcluirPrioridadeTipoCompromissoRequest();
		ExcluirPrioridadeTipoCompromissoResponse excluirPrioridadeTipoCompromissoResponse = new ExcluirPrioridadeTipoCompromissoResponse();
		
		
		excluirPrioridadeTipoCompromissoRequest.setCdCompromissoPrioridadeProduto(excluirPrioridadeCredTipoCompEntradaDTO.getTipoCompromisso());
	
	
		excluirPrioridadeTipoCompromissoResponse = getFactoryAdapter().getExcluirPrioridadeTipoCompromissoPDCAdapter().invokeProcess(excluirPrioridadeTipoCompromissoRequest);
	
		excluirPrioridadeCredTipoCompSaidaDTO.setCodMensagem(excluirPrioridadeTipoCompromissoResponse.getCodMensagem());
		excluirPrioridadeCredTipoCompSaidaDTO.setMensagem(excluirPrioridadeTipoCompromissoResponse.getMensagem());
		
		return excluirPrioridadeCredTipoCompSaidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.IPrioridadeCredTipoCompService#incluirPrioridadeCredTipoComp(br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.IncluirPrioridadeCredTipoCompEntradaDTO)
	 */
	public IncluirPrioridadeCredTipoCompSaidaDTO incluirPrioridadeCredTipoComp(IncluirPrioridadeCredTipoCompEntradaDTO incluirPrioridadeCredTipoCompEntradaDTO) {
	
		IncluirPrioridadeCredTipoCompSaidaDTO incluirPrioridadeCredTipoCompSaidaDTO = new IncluirPrioridadeCredTipoCompSaidaDTO();
		IncluirPrioridadeTipoCompromissoRequest incluirPrioridadeTipoCompromissoRequest = new IncluirPrioridadeTipoCompromissoRequest();
		IncluirPrioridadeTipoCompromissoResponse incluirPrioridadeTipoCompromissoResponse = new IncluirPrioridadeTipoCompromissoResponse();
		
		
		incluirPrioridadeTipoCompromissoRequest.setCdCompromissoPrioridadeProduto(incluirPrioridadeCredTipoCompEntradaDTO.getTipoCompromisso());
		incluirPrioridadeTipoCompromissoRequest.setCdOrdemPrioridadeCompromisso(incluirPrioridadeCredTipoCompEntradaDTO.getPrioridade());
		incluirPrioridadeTipoCompromissoRequest.setDtInicioVigenciaCompromisso(incluirPrioridadeCredTipoCompEntradaDTO.getDataInicioVigencia());
		incluirPrioridadeTipoCompromissoRequest.setDtFimVigenciaCompromisso(incluirPrioridadeCredTipoCompEntradaDTO.getDataFimVigencia());
		incluirPrioridadeTipoCompromissoRequest.setCentroCustoPrioridadeProduto(incluirPrioridadeCredTipoCompEntradaDTO.getCentroCusto());
		
		incluirPrioridadeTipoCompromissoResponse = getFactoryAdapter().getIncluirPrioridadeTipoCompromissoPDCAdapter().invokeProcess(incluirPrioridadeTipoCompromissoRequest);
		
		incluirPrioridadeCredTipoCompSaidaDTO.setCodMensagem(incluirPrioridadeTipoCompromissoResponse.getCodMensagem());
		incluirPrioridadeCredTipoCompSaidaDTO.setMensagem(incluirPrioridadeTipoCompromissoResponse.getMensagem());
		
	
		return incluirPrioridadeCredTipoCompSaidaDTO;
		
		
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.IPrioridadeCredTipoCompService#listarPrioridadeCredTipoComp(br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ListarPrioridadeCredTipoCompEntradaDTO)
	 */
	public List<ListarPrioridadeCredTipoCompSaidaDTO> listarPrioridadeCredTipoComp(ListarPrioridadeCredTipoCompEntradaDTO entrada) {
		List<ListarPrioridadeCredTipoCompSaidaDTO> listaRetorno = new ArrayList<ListarPrioridadeCredTipoCompSaidaDTO>();
		ListarPrioridadeTipoCompromissoRequest request = new ListarPrioridadeTipoCompromissoRequest();
		ListarPrioridadeTipoCompromissoResponse response = new ListarPrioridadeTipoCompromissoResponse();

		request.setCdCompromissoPrioridadeProduto(PgitUtil.verificaIntegerNulo(entrada.getTipoCompromisso()));
		request.setCdOrdemPrioridadeCompromisso(PgitUtil.verificaIntegerNulo(entrada.getPrioridade()));
		request.setCdCustoPrioridadeProduto(PgitUtil.verificaStringNula(entrada.getCdCustoPrioridadeProduto()));
		request.setDtInicioVigenciaCompromisso(FormatarData.formataDiaMesAnoToPdc(entrada.getDtInicioVig()));
		request.setQtConsultas(40);

		response = getFactoryAdapter().getListarPrioridadeTipoCompromissoPDCAdapter().invokeProcess(request);
		ListarPrioridadeCredTipoCompSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO =  new ListarPrioridadeCredTipoCompSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setTipoCompromisso(response.getOcorrencias(i).getCdCompromissoPrioridadeProduto());
			saidaDTO.setDataInicioVigencia(response.getOcorrencias(i).getDtInicioVigenciaCompromisso()!=null  &&  !response.getOcorrencias(i).getDtInicioVigenciaCompromisso().equals("")?response.getOcorrencias(i).getDtInicioVigenciaCompromisso():"");	
			saidaDTO.setDataFimVigencia(response.getOcorrencias(i).getDtFimVigenciaCompromisso()!=null  &&  !response.getOcorrencias(i).getDtFimVigenciaCompromisso().equals("")?response.getOcorrencias(i).getDtFimVigenciaCompromisso():"");
			saidaDTO.setPrioridade(response.getOcorrencias(i).getCdOrdemPrioridadeCompromisso());

			saidaDTO.setCdCustoPrioridadeProduto(response.getOcorrencias(i).getCdCustoPrioridadeProduto());
			saidaDTO.setDsCustoPrioridadeProduto(response.getOcorrencias(i).getDsCustoPrioridadeProduto());			

			saidaDTO.setCentroCusto(PgitUtil.concatenarCampos(saidaDTO.getCdCustoPrioridadeProduto(), saidaDTO.getDsCustoPrioridadeProduto(), "-"));
			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.IPrioridadeCredTipoCompService#detalharPrioridadeCredTipoComp(br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.DetalharPrioridadeCredTipoCompEntradaDTO)
	 */
	public DetalharPrioridadeCredTipoCompSaidaDTO detalharPrioridadeCredTipoComp(DetalharPrioridadeCredTipoCompEntradaDTO detalharPrioridadeCredTipoCompEntradaDTO) {
		DetalharPrioridadeCredTipoCompSaidaDTO saidaDTO = new DetalharPrioridadeCredTipoCompSaidaDTO();
		DetalharPrioridadeTipoCompromissoRequest request = new DetalharPrioridadeTipoCompromissoRequest();
		DetalharPrioridadeTipoCompromissoResponse response = new DetalharPrioridadeTipoCompromissoResponse();

		request.setCdCompromissoPrioridadeProduto(detalharPrioridadeCredTipoCompEntradaDTO.getTipoCompromisso());

		response = getFactoryAdapter().getDetalharPrioridadeTipoCompromissoPDCAdapter().invokeProcess(request);

		saidaDTO = new DetalharPrioridadeCredTipoCompSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		saidaDTO.setTipoComprimisso(response.getCdCompromissoPrioridadeProduto());
		saidaDTO.setPrioridade(response.getCdOrdemPrioridadeCompromisso());
		saidaDTO.setDataInicioVigencia(response.getDtInicioVigenciaCompromisso());
		saidaDTO.setDataFinalVigencia(response.getDtFimVigenciaCompromisso());
		saidaDTO.setCdCanalInclusao(response.getCdCanalInclusao());
		saidaDTO.setCdCanalManutencao(response.getCdCanaManutencao());
		saidaDTO.setDataHoraInclusao(response.getHrInclusaoRegistro());
		saidaDTO.setDataHoraManutencao(response.getHrManutencaoRegistro());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setDsCanalManutencao(response.getDsCanaManutencao());
		saidaDTO.setDtFimVigenciaCompromisso(response.getDtFimVigenciaCompromisso());
		saidaDTO.setDtInicioVigenciaCompromisso(response.getDtInicioVigenciaCompromisso());
		saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saidaDTO.setNumOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
		saidaDTO.setNumOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
		saidaDTO.setAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
		saidaDTO.setCentroCusto(response.getCentroCustoPrioridadeProduto());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.IPrioridadeCredTipoCompService#alterarPrioridadeCredTipoCompSaidaDTO(br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.AlterarPrioridadeCredTipoCompEntradaDTO)
	 */
	public AlterarPrioridadeCredTipoCompSaidaDTO alterarPrioridadeCredTipoCompSaidaDTO(AlterarPrioridadeCredTipoCompEntradaDTO alterarPrioridadeCredTipoCompEntradaDTO) {
		AlterarPrioridadeTipoCompromissoRequest alterarPrioridadeTipoCompromissoRequest = new AlterarPrioridadeTipoCompromissoRequest();
		AlterarPrioridadeTipoCompromissoResponse alterarPrioridadeTipoCompromissoResponse = new AlterarPrioridadeTipoCompromissoResponse();
		AlterarPrioridadeCredTipoCompSaidaDTO alterarPrioridadeCredTipoCompSaidaDTO = new AlterarPrioridadeCredTipoCompSaidaDTO();

		alterarPrioridadeTipoCompromissoRequest.setCdCompromissoPrioridadeProduto(PgitUtil.verificaIntegerNulo(alterarPrioridadeCredTipoCompEntradaDTO.getTipoCompromisso()));
		alterarPrioridadeTipoCompromissoRequest.setCdOrdemPrioridadeCompromisso(PgitUtil.verificaIntegerNulo(alterarPrioridadeCredTipoCompEntradaDTO.getPrioridade()));
		alterarPrioridadeTipoCompromissoRequest.setDtFimVigenciaCompromisso(PgitUtil.verificaStringNula(alterarPrioridadeCredTipoCompEntradaDTO.getDataFimVigencia()));
		alterarPrioridadeTipoCompromissoRequest.setDtInicioVigenciaCompromisso(PgitUtil.verificaStringNula(alterarPrioridadeCredTipoCompEntradaDTO.getDataInicioVigencia()));
		alterarPrioridadeTipoCompromissoRequest.setCentroCustoPrioridadeProduto(PgitUtil.verificaStringNula(alterarPrioridadeCredTipoCompEntradaDTO.getCentroCustoPrioridadeProduto()));

		alterarPrioridadeTipoCompromissoResponse = getFactoryAdapter().getAlterarPrioridadeTipoCompromissoPDCAdapter().invokeProcess(alterarPrioridadeTipoCompromissoRequest);

		alterarPrioridadeCredTipoCompSaidaDTO.setCodMensagem(alterarPrioridadeTipoCompromissoResponse.getCodMensagem());
		alterarPrioridadeCredTipoCompSaidaDTO.setMensagem(alterarPrioridadeTipoCompromissoResponse.getMensagem());

		return alterarPrioridadeCredTipoCompSaidaDTO;
	}
}