/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

// TODO: Auto-generated Javadoc
/**
 * Nome: DetalharHistAssocTrocaArqParticipanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharHistAssocTrocaArqParticipanteSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsCodigoTipoLayout. */
	private String dsCodigoTipoLayout;
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
	
	/** Atributo cdCorpoCpfCnpj. */
	private Long cdCorpoCpfCnpj;
	
	/** Atributo cdCnpjContrato. */
	private Integer cdCnpjContrato;
	
	/** Atributo cdCpfCnpjDigito. */
	private Integer cdCpfCnpjDigito;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo cdTipoParticipante. */
	private String cdTipoParticipante;
	
	/** Atributo dsTipoParticipante. */
	private String dsTipoParticipante;
	
	/** Atributo cdSituacaoParticipante. */
	private Long cdSituacaoParticipante;
	
	/** Atributo cdDescricaoSituacaoParticapante. */
	private String cdDescricaoSituacaoParticapante;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo dsClub. */
	private String dsClub;
	
	/** Atributo cdCanal. */
	private String cdCanal;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo dsIndicadorTipoManutencao. */
	private String dsIndicadorTipoManutencao;	
	
	/** Atributo cdContratoMae. */
	private Long cdContratoMae;
	
	/** Atributo dsRazaoSocialContratoMae. */
	private String dsRazaoSocialContratoMae;
	
	/** Atributo cdPerfilContratoMae. */
	private Long cdPerfilContratoMae;
	
	/** Atributo dsTipoLayoutArquivoContratoMae. */
	private String dsTipoLayoutArquivoContratoMae;

	/** Atributo cdCpfCnpj. */
	private Long cdCpfCnpj;
	
	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;
	
	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;
	
	public String getCpfCnpjContratoMaeFormatado(){
		return CpfCnpjUtils.formatarCpfCnpj(cdCpfCnpj, cdFilialCnpj, cdControleCnpj);
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: dsCodigoTipoLayout.
	 *
	 * @return dsCodigoTipoLayout
	 */
	public String getDsCodigoTipoLayout(){
		return dsCodigoTipoLayout;
	}

	/**
	 * Set: dsCodigoTipoLayout.
	 *
	 * @param dsCodigoTipoLayout the ds codigo tipo layout
	 */
	public void setDsCodigoTipoLayout(String dsCodigoTipoLayout){
		this.dsCodigoTipoLayout = dsCodigoTipoLayout;
	}

	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo(){
		return cdPerfilTrocaArquivo;
	}

	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo){
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj(){
		return cdCorpoCpfCnpj;
	}

	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj){
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

	/**
	 * Get: cdCnpjContrato.
	 *
	 * @return cdCnpjContrato
	 */
	public Integer getCdCnpjContrato(){
		return cdCnpjContrato;
	}

	/**
	 * Set: cdCnpjContrato.
	 *
	 * @param cdCnpjContrato the cd cnpj contrato
	 */
	public void setCdCnpjContrato(Integer cdCnpjContrato){
		this.cdCnpjContrato = cdCnpjContrato;
	}

	/**
	 * Get: cdCpfCnpjDigito.
	 *
	 * @return cdCpfCnpjDigito
	 */
	public Integer getCdCpfCnpjDigito(){
		return cdCpfCnpjDigito;
	}

	/**
	 * Set: cdCpfCnpjDigito.
	 *
	 * @param cdCpfCnpjDigito the cd cpf cnpj digito
	 */
	public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito){
		this.cdCpfCnpjDigito = cdCpfCnpjDigito;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial(){
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial){
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: cdTipoParticipante.
	 *
	 * @return cdTipoParticipante
	 */
	public String getCdTipoParticipante(){
		return cdTipoParticipante;
	}

	/**
	 * Set: cdTipoParticipante.
	 *
	 * @param cdTipoParticipante the cd tipo participante
	 */
	public void setCdTipoParticipante(String cdTipoParticipante){
		this.cdTipoParticipante = cdTipoParticipante;
	}

	/**
	 * Get: dsTipoParticipante.
	 *
	 * @return dsTipoParticipante
	 */
	public String getDsTipoParticipante(){
		return dsTipoParticipante;
	}

	/**
	 * Set: dsTipoParticipante.
	 *
	 * @param dsTipoParticipante the ds tipo participante
	 */
	public void setDsTipoParticipante(String dsTipoParticipante){
		this.dsTipoParticipante = dsTipoParticipante;
	}

	/**
	 * Get: cdSituacaoParticipante.
	 *
	 * @return cdSituacaoParticipante
	 */
	public Long getCdSituacaoParticipante(){
		return cdSituacaoParticipante;
	}

	/**
	 * Set: cdSituacaoParticipante.
	 *
	 * @param cdSituacaoParticipante the cd situacao participante
	 */
	public void setCdSituacaoParticipante(Long cdSituacaoParticipante){
		this.cdSituacaoParticipante = cdSituacaoParticipante;
	}

	/**
	 * Get: cdDescricaoSituacaoParticapante.
	 *
	 * @return cdDescricaoSituacaoParticapante
	 */
	public String getCdDescricaoSituacaoParticapante(){
		return cdDescricaoSituacaoParticapante;
	}

	/**
	 * Set: cdDescricaoSituacaoParticapante.
	 *
	 * @param cdDescricaoSituacaoParticapante the cd descricao situacao particapante
	 */
	public void setCdDescricaoSituacaoParticapante(String cdDescricaoSituacaoParticapante){
		this.cdDescricaoSituacaoParticapante = cdDescricaoSituacaoParticapante;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa(){
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa){
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: dsClub.
	 *
	 * @return dsClub
	 */
	public String getDsClub(){
		return dsClub;
	}

	/**
	 * Set: dsClub.
	 *
	 * @param dsClub the ds club
	 */
	public void setDsClub(String dsClub){
		this.dsClub = dsClub;
	}

	/**
	 * Get: cdCanal.
	 *
	 * @return cdCanal
	 */
	public String getCdCanal(){
		return cdCanal;
	}

	/**
	 * Set: cdCanal.
	 *
	 * @param cdCanal the cd canal
	 */
	public void setCdCanal(String cdCanal){
		this.cdCanal = cdCanal;
	}
	
	/**
	 * Get: dsIndicadorTipoManutencao.
	 *
	 * @return dsIndicadorTipoManutencao
	 */
	public String getDsIndicadorTipoManutencao(){
		return dsIndicadorTipoManutencao;
	}

	/**
	 * Set: dsIndicadorTipoManutencao.
	 *
	 * @param dsIndicadorTipoManutencao the ds indicador tipo manutencao
	 */
	public void setDsIndicadorTipoManutencao(String dsIndicadorTipoManutencao){
		this.dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao(){
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao){
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao(){
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao){
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao(){
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao){
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno(){
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno){
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro(){
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro){
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao(){
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao){
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao(){
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao){
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao(){
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao){
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno(){
		return cdUsuarioManutencaoExterno;
	}

	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno){
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro(){
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro){
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Gets the cd contrato mae.
	 *
	 * @return the cdContratoMae
	 */
	public Long getCdContratoMae() {
		return cdContratoMae;
	}

	/**
	 * Sets the cd contrato mae.
	 *
	 * @param cdContratoMae the cdContratoMae to set
	 */
	public void setCdContratoMae(Long cdContratoMae) {
		this.cdContratoMae = cdContratoMae;
	}

	/**
	 * Gets the ds razao social contrato mae.
	 *
	 * @return the dsRazaoSocialContratoMae
	 */
	public String getDsRazaoSocialContratoMae() {
		return dsRazaoSocialContratoMae;
	}

	/**
	 * Sets the ds razao social contrato mae.
	 *
	 * @param dsRazaoSocialContratoMae the dsRazaoSocialContratoMae to set
	 */
	public void setDsRazaoSocialContratoMae(String dsRazaoSocialContratoMae) {
		this.dsRazaoSocialContratoMae = dsRazaoSocialContratoMae;
	}

	/**
	 * Gets the cd perfil contrato mae.
	 *
	 * @return the cdPerfilContratoMae
	 */
	public Long getCdPerfilContratoMae() {
		return cdPerfilContratoMae;
	}

	/**
	 * Sets the cd perfil contrato mae.
	 *
	 * @param cdPerfilContratoMae the cdPerfilContratoMae to set
	 */
	public void setCdPerfilContratoMae(Long cdPerfilContratoMae) {
		this.cdPerfilContratoMae = cdPerfilContratoMae;
	}

	/**
	 * Gets the ds tipo layout arquivo contrato mae.
	 *
	 * @return the dsTipoLayoutArquivoContratoMae
	 */
	public String getDsTipoLayoutArquivoContratoMae() {
		return dsTipoLayoutArquivoContratoMae;
	}

	/**
	 * Sets the ds tipo layout arquivo contrato mae.
	 *
	 * @param dsTipoLayoutArquivoContratoMae the dsTipoLayoutArquivoContratoMae to set
	 */
	public void setDsTipoLayoutArquivoContratoMae(
			String dsTipoLayoutArquivoContratoMae) {
		this.dsTipoLayoutArquivoContratoMae = dsTipoLayoutArquivoContratoMae;
	}

	/**
	 * @return the cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * @param cdCpfCnpj the cdCpfCnpj to set
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * @return the cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * @param cdFilialCnpj the cdFilialCnpj to set
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * @return the cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * @param cdControleCnpj the cdControleCnpj to set
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}
	
	
}