/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirSolicitacaoRastreamentoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirSolicitacaoRastreamentoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtInicioRastreamento
     */
    private java.lang.String _dtInicioRastreamento;

    /**
     * Field _dtFimRastreamento
     */
    private java.lang.String _dtFimRastreamento;

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _dgConta
     */
    private java.lang.String _dgConta;

    /**
     * Field _cdIndicadorInclusaoFavorecido
     */
    private int _cdIndicadorInclusaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdIndicadorInclusaoFavorecid
     */
    private boolean _has_cdIndicadorInclusaoFavorecido;

    /**
     * Field _cdIndicadorDestinoRetorno
     */
    private int _cdIndicadorDestinoRetorno = 0;

    /**
     * keeps track of state for field: _cdIndicadorDestinoRetorno
     */
    private boolean _has_cdIndicadorDestinoRetorno;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _numPercentualDescTarifa
     */
    private java.math.BigDecimal _numPercentualDescTarifa = new java.math.BigDecimal("0");

    /**
     * Field _vlrTarifaAtual
     */
    private java.math.BigDecimal _vlrTarifaAtual = new java.math.BigDecimal("0");

    /**
     * Field _cdFormaFavorecido
     */
    private int _cdFormaFavorecido = 0;

    /**
     * keeps track of state for field: _cdFormaFavorecido
     */
    private boolean _has_cdFormaFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirSolicitacaoRastreamentoRequest() 
     {
        super();
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
        setNumPercentualDescTarifa(new java.math.BigDecimal("0"));
        setVlrTarifaAtual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.request.IncluirSolicitacaoRastreamentoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdFormaFavorecido
     * 
     */
    public void deleteCdFormaFavorecido()
    {
        this._has_cdFormaFavorecido= false;
    } //-- void deleteCdFormaFavorecido() 

    /**
     * Method deleteCdIndicadorDestinoRetorno
     * 
     */
    public void deleteCdIndicadorDestinoRetorno()
    {
        this._has_cdIndicadorDestinoRetorno= false;
    } //-- void deleteCdIndicadorDestinoRetorno() 

    /**
     * Method deleteCdIndicadorInclusaoFavorecido
     * 
     */
    public void deleteCdIndicadorInclusaoFavorecido()
    {
        this._has_cdIndicadorInclusaoFavorecido= false;
    } //-- void deleteCdIndicadorInclusaoFavorecido() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdFormaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdFormaFavorecido'.
     */
    public int getCdFormaFavorecido()
    {
        return this._cdFormaFavorecido;
    } //-- int getCdFormaFavorecido() 

    /**
     * Returns the value of field 'cdIndicadorDestinoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorDestinoRetorno'.
     */
    public int getCdIndicadorDestinoRetorno()
    {
        return this._cdIndicadorDestinoRetorno;
    } //-- int getCdIndicadorDestinoRetorno() 

    /**
     * Returns the value of field 'cdIndicadorInclusaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorInclusaoFavorecido'.
     */
    public int getCdIndicadorInclusaoFavorecido()
    {
        return this._cdIndicadorInclusaoFavorecido;
    } //-- int getCdIndicadorInclusaoFavorecido() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dgConta'.
     * 
     * @return String
     * @return the value of field 'dgConta'.
     */
    public java.lang.String getDgConta()
    {
        return this._dgConta;
    } //-- java.lang.String getDgConta() 

    /**
     * Returns the value of field 'dtFimRastreamento'.
     * 
     * @return String
     * @return the value of field 'dtFimRastreamento'.
     */
    public java.lang.String getDtFimRastreamento()
    {
        return this._dtFimRastreamento;
    } //-- java.lang.String getDtFimRastreamento() 

    /**
     * Returns the value of field 'dtInicioRastreamento'.
     * 
     * @return String
     * @return the value of field 'dtInicioRastreamento'.
     */
    public java.lang.String getDtInicioRastreamento()
    {
        return this._dtInicioRastreamento;
    } //-- java.lang.String getDtInicioRastreamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numPercentualDescTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'numPercentualDescTarifa'.
     */
    public java.math.BigDecimal getNumPercentualDescTarifa()
    {
        return this._numPercentualDescTarifa;
    } //-- java.math.BigDecimal getNumPercentualDescTarifa() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Returns the value of field 'vlrTarifaAtual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaAtual'.
     */
    public java.math.BigDecimal getVlrTarifaAtual()
    {
        return this._vlrTarifaAtual;
    } //-- java.math.BigDecimal getVlrTarifaAtual() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdFormaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaFavorecido()
    {
        return this._has_cdFormaFavorecido;
    } //-- boolean hasCdFormaFavorecido() 

    /**
     * Method hasCdIndicadorDestinoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorDestinoRetorno()
    {
        return this._has_cdIndicadorDestinoRetorno;
    } //-- boolean hasCdIndicadorDestinoRetorno() 

    /**
     * Method hasCdIndicadorInclusaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorInclusaoFavorecido()
    {
        return this._has_cdIndicadorInclusaoFavorecido;
    } //-- boolean hasCdIndicadorInclusaoFavorecido() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdFormaFavorecido'.
     * 
     * @param cdFormaFavorecido the value of field
     * 'cdFormaFavorecido'.
     */
    public void setCdFormaFavorecido(int cdFormaFavorecido)
    {
        this._cdFormaFavorecido = cdFormaFavorecido;
        this._has_cdFormaFavorecido = true;
    } //-- void setCdFormaFavorecido(int) 

    /**
     * Sets the value of field 'cdIndicadorDestinoRetorno'.
     * 
     * @param cdIndicadorDestinoRetorno the value of field
     * 'cdIndicadorDestinoRetorno'.
     */
    public void setCdIndicadorDestinoRetorno(int cdIndicadorDestinoRetorno)
    {
        this._cdIndicadorDestinoRetorno = cdIndicadorDestinoRetorno;
        this._has_cdIndicadorDestinoRetorno = true;
    } //-- void setCdIndicadorDestinoRetorno(int) 

    /**
     * Sets the value of field 'cdIndicadorInclusaoFavorecido'.
     * 
     * @param cdIndicadorInclusaoFavorecido the value of field
     * 'cdIndicadorInclusaoFavorecido'.
     */
    public void setCdIndicadorInclusaoFavorecido(int cdIndicadorInclusaoFavorecido)
    {
        this._cdIndicadorInclusaoFavorecido = cdIndicadorInclusaoFavorecido;
        this._has_cdIndicadorInclusaoFavorecido = true;
    } //-- void setCdIndicadorInclusaoFavorecido(int) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dgConta'.
     * 
     * @param dgConta the value of field 'dgConta'.
     */
    public void setDgConta(java.lang.String dgConta)
    {
        this._dgConta = dgConta;
    } //-- void setDgConta(java.lang.String) 

    /**
     * Sets the value of field 'dtFimRastreamento'.
     * 
     * @param dtFimRastreamento the value of field
     * 'dtFimRastreamento'.
     */
    public void setDtFimRastreamento(java.lang.String dtFimRastreamento)
    {
        this._dtFimRastreamento = dtFimRastreamento;
    } //-- void setDtFimRastreamento(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioRastreamento'.
     * 
     * @param dtInicioRastreamento the value of field
     * 'dtInicioRastreamento'.
     */
    public void setDtInicioRastreamento(java.lang.String dtInicioRastreamento)
    {
        this._dtInicioRastreamento = dtInicioRastreamento;
    } //-- void setDtInicioRastreamento(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numPercentualDescTarifa'.
     * 
     * @param numPercentualDescTarifa the value of field
     * 'numPercentualDescTarifa'.
     */
    public void setNumPercentualDescTarifa(java.math.BigDecimal numPercentualDescTarifa)
    {
        this._numPercentualDescTarifa = numPercentualDescTarifa;
    } //-- void setNumPercentualDescTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTarifaAtual'.
     * 
     * @param vlrTarifaAtual the value of field 'vlrTarifaAtual'.
     */
    public void setVlrTarifaAtual(java.math.BigDecimal vlrTarifaAtual)
    {
        this._vlrTarifaAtual = vlrTarifaAtual;
    } //-- void setVlrTarifaAtual(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirSolicitacaoRastreamentoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.request.IncluirSolicitacaoRastreamentoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.request.IncluirSolicitacaoRastreamentoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.request.IncluirSolicitacaoRastreamentoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorastreamento.request.IncluirSolicitacaoRastreamentoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
