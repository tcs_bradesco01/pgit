/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class VerificaraAtributoSoltcRequest.
 * 
 * @version $Revision$ $Date$
 */
public class VerificaraAtributoSoltcRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFuncionario
     */
    private int _cdFuncionario = 0;

    /**
     * keeps track of state for field: _cdFuncionario
     */
    private boolean _has_cdFuncionario;


      //----------------/
     //- Constructors -/
    //----------------/

    public VerificaraAtributoSoltcRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.request.VerificaraAtributoSoltcRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFuncionario
     * 
     */
    public void deleteCdFuncionario()
    {
        this._has_cdFuncionario= false;
    } //-- void deleteCdFuncionario() 

    /**
     * Returns the value of field 'cdFuncionario'.
     * 
     * @return int
     * @return the value of field 'cdFuncionario'.
     */
    public int getCdFuncionario()
    {
        return this._cdFuncionario;
    } //-- int getCdFuncionario() 

    /**
     * Method hasCdFuncionario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionario()
    {
        return this._has_cdFuncionario;
    } //-- boolean hasCdFuncionario() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFuncionario'.
     * 
     * @param cdFuncionario the value of field 'cdFuncionario'.
     */
    public void setCdFuncionario(int cdFuncionario)
    {
        this._cdFuncionario = cdFuncionario;
        this._has_cdFuncionario = true;
    } //-- void setCdFuncionario(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return VerificaraAtributoSoltcRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.request.VerificaraAtributoSoltcRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.request.VerificaraAtributoSoltcRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.request.VerificaraAtributoSoltcRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.verificaraatributosoltc.request.VerificaraAtributoSoltcRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
