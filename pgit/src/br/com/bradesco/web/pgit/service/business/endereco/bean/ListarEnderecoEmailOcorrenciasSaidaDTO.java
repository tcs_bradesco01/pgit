/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.SiteUtil;


/**
 * Nome: ListarEnderecoEmailOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarEnderecoEmailOcorrenciasSaidaDTO {

    /** Atributo cdCpfCnpjRecebedor. */
    private Long cdCpfCnpjRecebedor;

    /** Atributo cdFilialCnpjRecebedor. */
    private Integer cdFilialCnpjRecebedor;

    /** Atributo cdControleCpfRecebedor. */
    private Integer cdControleCpfRecebedor;

    /** Atributo cdNomeRazao. */
    private String cdNomeRazao;

    /** Atributo dsLogradouroPagador. */
    private String dsLogradouroPagador;

    /** Atributo dsNumeroLogradouroPagador. */
    private String dsNumeroLogradouroPagador;

    /** Atributo dsComplementoLogradouroPagador. */
    private String dsComplementoLogradouroPagador;

    /** Atributo dsBairroClientePagador. */
    private String dsBairroClientePagador;

    /** Atributo dsMunicipioClientePagador. */
    private String dsMunicipioClientePagador;

    /** Atributo cdSiglaUfPagador. */
    private String cdSiglaUfPagador;

    /** Atributo dsPais. */
    private String dsPais;

    /** Atributo cdCepPagador. */
    private Integer cdCepPagador;

    /** Atributo cdCepComplementoPagador. */
    private Integer cdCepComplementoPagador;

    /** Atributo dsCpfCnpjRecebedor. */
    private Long dsCpfCnpjRecebedor;

    /** Atributo dsFilialCnpjRecebedor. */
    private Integer dsFilialCnpjRecebedor;

    /** Atributo dsControleCpfRecebedor. */
    private Integer dsControleCpfRecebedor;

    /** Atributo dsNomeRazao. */
    private String dsNomeRazao;

    /** Atributo cdEnderecoEletronico. */
    private String cdEnderecoEletronico;

    /** Atributo nrSequenciaEndereco. */
    private Integer nrSequenciaEndereco;

    /** Atributo cdClub. */
    private Long cdClub;

    /** Atributo cdTipo. */
    private String cdTipo;

    /**
     * Set: cdCpfCnpjRecebedor.
     *
     * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
     */
    public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
        this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
    }

    /**
     * Get: cdCpfCnpjRecebedor.
     *
     * @return cdCpfCnpjRecebedor
     */
    public Long getCdCpfCnpjRecebedor() {
        return this.cdCpfCnpjRecebedor;
    }

    /**
     * Set: cdFilialCnpjRecebedor.
     *
     * @param cdFilialCnpjRecebedor the cd filial cnpj recebedor
     */
    public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
        this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
    }

    /**
     * Get: cdFilialCnpjRecebedor.
     *
     * @return cdFilialCnpjRecebedor
     */
    public Integer getCdFilialCnpjRecebedor() {
        return this.cdFilialCnpjRecebedor;
    }

    /**
     * Set: cdControleCpfRecebedor.
     *
     * @param cdControleCpfRecebedor the cd controle cpf recebedor
     */
    public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
        this.cdControleCpfRecebedor = cdControleCpfRecebedor;
    }

    /**
     * Get: cdControleCpfRecebedor.
     *
     * @return cdControleCpfRecebedor
     */
    public Integer getCdControleCpfRecebedor() {
        return this.cdControleCpfRecebedor;
    }

    /**
     * Set: cdNomeRazao.
     *
     * @param cdNomeRazao the cd nome razao
     */
    public void setCdNomeRazao(String cdNomeRazao) {
        this.cdNomeRazao = cdNomeRazao;
    }

    /**
     * Get: cdNomeRazao.
     *
     * @return cdNomeRazao
     */
    public String getCdNomeRazao() {
        return this.cdNomeRazao;
    }

    /**
     * Set: dsLogradouroPagador.
     *
     * @param dsLogradouroPagador the ds logradouro pagador
     */
    public void setDsLogradouroPagador(String dsLogradouroPagador) {
        this.dsLogradouroPagador = dsLogradouroPagador;
    }

    /**
     * Get: dsLogradouroPagador.
     *
     * @return dsLogradouroPagador
     */
    public String getDsLogradouroPagador() {
        return this.dsLogradouroPagador;
    }

    /**
     * Set: dsNumeroLogradouroPagador.
     *
     * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
     */
    public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
        this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
    }

    /**
     * Get: dsNumeroLogradouroPagador.
     *
     * @return dsNumeroLogradouroPagador
     */
    public String getDsNumeroLogradouroPagador() {
        return this.dsNumeroLogradouroPagador;
    }

    /**
     * Set: dsComplementoLogradouroPagador.
     *
     * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
     */
    public void setDsComplementoLogradouroPagador(String dsComplementoLogradouroPagador) {
        this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
    }

    /**
     * Get: dsComplementoLogradouroPagador.
     *
     * @return dsComplementoLogradouroPagador
     */
    public String getDsComplementoLogradouroPagador() {
        return this.dsComplementoLogradouroPagador;
    }

    /**
     * Set: dsBairroClientePagador.
     *
     * @param dsBairroClientePagador the ds bairro cliente pagador
     */
    public void setDsBairroClientePagador(String dsBairroClientePagador) {
        this.dsBairroClientePagador = dsBairroClientePagador;
    }

    /**
     * Get: dsBairroClientePagador.
     *
     * @return dsBairroClientePagador
     */
    public String getDsBairroClientePagador() {
        return this.dsBairroClientePagador;
    }

    /**
     * Set: dsMunicipioClientePagador.
     *
     * @param dsMunicipioClientePagador the ds municipio cliente pagador
     */
    public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
        this.dsMunicipioClientePagador = dsMunicipioClientePagador;
    }

    /**
     * Get: dsMunicipioClientePagador.
     *
     * @return dsMunicipioClientePagador
     */
    public String getDsMunicipioClientePagador() {
        return this.dsMunicipioClientePagador;
    }

    /**
     * Set: cdSiglaUfPagador.
     *
     * @param cdSiglaUfPagador the cd sigla uf pagador
     */
    public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
        this.cdSiglaUfPagador = cdSiglaUfPagador;
    }

    /**
     * Get: cdSiglaUfPagador.
     *
     * @return cdSiglaUfPagador
     */
    public String getCdSiglaUfPagador() {
        return this.cdSiglaUfPagador;
    }

    /**
     * Set: dsPais.
     *
     * @param dsPais the ds pais
     */
    public void setDsPais(String dsPais) {
        this.dsPais = dsPais;
    }

    /**
     * Get: dsPais.
     *
     * @return dsPais
     */
    public String getDsPais() {
        return this.dsPais;
    }

    /**
     * Set: cdCepPagador.
     *
     * @param cdCepPagador the cd cep pagador
     */
    public void setCdCepPagador(Integer cdCepPagador) {
        this.cdCepPagador = cdCepPagador;
    }

    /**
     * Get: cdCepPagador.
     *
     * @return cdCepPagador
     */
    public Integer getCdCepPagador() {
        return this.cdCepPagador;
    }

    /**
     * Set: cdCepComplementoPagador.
     *
     * @param cdCepComplementoPagador the cd cep complemento pagador
     */
    public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
        this.cdCepComplementoPagador = cdCepComplementoPagador;
    }

    /**
     * Get: cdCepComplementoPagador.
     *
     * @return cdCepComplementoPagador
     */
    public Integer getCdCepComplementoPagador() {
        return this.cdCepComplementoPagador;
    }

    /**
     * Set: dsCpfCnpjRecebedor.
     *
     * @param dsCpfCnpjRecebedor the ds cpf cnpj recebedor
     */
    public void setDsCpfCnpjRecebedor(Long dsCpfCnpjRecebedor) {
        this.dsCpfCnpjRecebedor = dsCpfCnpjRecebedor;
    }

    /**
     * Get: dsCpfCnpjRecebedor.
     *
     * @return dsCpfCnpjRecebedor
     */
    public Long getDsCpfCnpjRecebedor() {
        return this.dsCpfCnpjRecebedor;
    }

    /**
     * Set: dsFilialCnpjRecebedor.
     *
     * @param dsFilialCnpjRecebedor the ds filial cnpj recebedor
     */
    public void setDsFilialCnpjRecebedor(Integer dsFilialCnpjRecebedor) {
        this.dsFilialCnpjRecebedor = dsFilialCnpjRecebedor;
    }

    /**
     * Get: dsFilialCnpjRecebedor.
     *
     * @return dsFilialCnpjRecebedor
     */
    public Integer getDsFilialCnpjRecebedor() {
        return this.dsFilialCnpjRecebedor;
    }

    /**
     * Set: dsControleCpfRecebedor.
     *
     * @param dsControleCpfRecebedor the ds controle cpf recebedor
     */
    public void setDsControleCpfRecebedor(Integer dsControleCpfRecebedor) {
        this.dsControleCpfRecebedor = dsControleCpfRecebedor;
    }

    /**
     * Get: dsControleCpfRecebedor.
     *
     * @return dsControleCpfRecebedor
     */
    public Integer getDsControleCpfRecebedor() {
        return this.dsControleCpfRecebedor;
    }

    /**
     * Set: dsNomeRazao.
     *
     * @param dsNomeRazao the ds nome razao
     */
    public void setDsNomeRazao(String dsNomeRazao) {
        this.dsNomeRazao = dsNomeRazao;
    }

    /**
     * Get: dsNomeRazao.
     *
     * @return dsNomeRazao
     */
    public String getDsNomeRazao() {
        return this.dsNomeRazao;
    }

    /**
     * Set: cdEnderecoEletronico.
     *
     * @param cdEnderecoEletronico the cd endereco eletronico
     */
    public void setCdEnderecoEletronico(String cdEnderecoEletronico) {
        this.cdEnderecoEletronico = cdEnderecoEletronico;
    }

    /**
     * Get: cdEnderecoEletronico.
     *
     * @return cdEnderecoEletronico
     */
    public String getCdEnderecoEletronico() {
        return this.cdEnderecoEletronico;
    }

    /**
     * Set: nrSequenciaEndereco.
     *
     * @param nrSequenciaEndereco the nr sequencia endereco
     */
    public void setNrSequenciaEndereco(Integer nrSequenciaEndereco) {
        this.nrSequenciaEndereco = nrSequenciaEndereco;
    }

    /**
     * Get: nrSequenciaEndereco.
     *
     * @return nrSequenciaEndereco
     */
    public Integer getNrSequenciaEndereco() {
        return this.nrSequenciaEndereco;
    }

    /**
     * Set: cdClub.
     *
     * @param cdClub the cd club
     */
    public void setCdClub(Long cdClub) {
        this.cdClub = cdClub;
    }

    /**
     * Get: cdClub.
     *
     * @return cdClub
     */
    public Long getCdClub() {
        return this.cdClub;
    }

    /**
     * Get: cdTipo.
     *
     * @return cdTipo
     */
    public String getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(String cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: cpfCnpjRecebedorFormatado.
	 *
	 * @return cpfCnpjRecebedorFormatado
	 */
	public String getCpfCnpjRecebedorFormatado() {
    	return CpfCnpjUtils.formatarCpfCnpj(getCdCpfCnpjRecebedor(), getCdFilialCnpjRecebedor(), getCdControleCpfRecebedor());
    }

	/**
	 * Get: dsCpfCnpjRecebedorFormatado.
	 *
	 * @return dsCpfCnpjRecebedorFormatado
	 */
	public String getDsCpfCnpjRecebedorFormatado() {
    	return CpfCnpjUtils.formatarCpfCnpj(getDsCpfCnpjRecebedor(), getDsFilialCnpjRecebedor(), getDsControleCpfRecebedor());
    }

	/**
	 * Get: dsEndereco.
	 *
	 * @return dsEndereco
	 */
	public String getDsEndereco() {
		StringBuilder dsEndereco = new StringBuilder();
		if (TipoEnderecoEnum.EMAIL.toString().equals(getCdTipo())) {
			dsEndereco.append(getCdEnderecoEletronico());
		} else if (TipoEnderecoEnum.CORREIO.toString().equals(getCdTipo())) {
			dsEndereco.append(getDsEnderecoEnd());
		}
		return dsEndereco.toString();
	}

	/**
	 * Get: dsEnderecoEnd.
	 *
	 * @return dsEnderecoEnd
	 */
	public String getDsEnderecoEnd() {
    	StringBuilder end = new StringBuilder();
    	if (!SiteUtil.isEmptyOrNull(dsLogradouroPagador)) {
    		end.append(dsLogradouroPagador);
        	end.append(" ");
    	}
    	if (!SiteUtil.isEmptyOrNull(dsNumeroLogradouroPagador)) {
    		end.append(dsNumeroLogradouroPagador);
        	end.append(" ");
    	}
    	if (!SiteUtil.isEmptyOrNull(dsComplementoLogradouroPagador)) {
    		end.append(dsComplementoLogradouroPagador);
        	end.append(" ");
    	}
    	if (!SiteUtil.isEmptyOrNull(dsBairroClientePagador)) {
    		end.append(dsBairroClientePagador);
        	end.append(" ");	
    	}
    	if (!SiteUtil.isEmptyOrNull(dsMunicipioClientePagador)) {
    		end.append(dsMunicipioClientePagador);
        	end.append(" ");	
    	}
    	if (!SiteUtil.isEmptyOrNull(cdSiglaUfPagador)) {
    		end.append(cdSiglaUfPagador);
        	end.append(" ");	
    	}
    	if (cdCepPagador != null && cdCepPagador != 0) {
    		end.append(cdCepPagador);
        	end.append(" ");	
    	}
    	if (!SiteUtil.isEmptyOrNull(dsPais)) {
    		end.append(dsPais);
    	}
		return end.toString();
    }
}