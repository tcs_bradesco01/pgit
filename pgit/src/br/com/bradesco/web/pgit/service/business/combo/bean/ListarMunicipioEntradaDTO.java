/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMunicipioEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMunicipioEntradaDTO {
	
	/** Atributo cdUf. */
	private Integer cdUf;

	/**
	 * Get: cdUf.
	 *
	 * @return cdUf
	 */
	public Integer getCdUf() {
		return cdUf;
	}

	/**
	 * Set: cdUf.
	 *
	 * @param cdUf the cd uf
	 */
	public void setCdUf(Integer cdUf) {
		this.cdUf = cdUf;
	}
	
}
