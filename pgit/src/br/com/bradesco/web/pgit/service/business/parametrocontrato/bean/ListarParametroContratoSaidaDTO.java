package br.com.bradesco.web.pgit.service.business.parametrocontrato.bean;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/01/16.
 */
public class ListarParametroContratoSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem = null;

    /** The mensagem. */
    private String mensagem = null;

    /** The nr linhas. */
    private Integer nrLinhas = null;

    /** The ocorrencias. */
    private List<ListarParametroContratoOcorrenciasSaidaDTO> ocorrencias = null;

    /**
     * Sets the cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Gets the cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Sets the mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Gets the mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Sets the nr linhas.
     *
     * @param nrLinhas the nr linhas
     */
    public void setNrLinhas(Integer nrLinhas) {
        this.nrLinhas = nrLinhas;
    }

    /**
     * Gets the nr linhas.
     *
     * @return the nr linhas
     */
    public Integer getNrLinhas() {
        return this.nrLinhas;
    }

    /**
     * Sets the ocorrencias.
     *
     * @param ocorrencias the ocorrencias
     */
    public void setOcorrencias(List<ListarParametroContratoOcorrenciasSaidaDTO> ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    /**
     * Gets the ocorrencias.
     *
     * @return the ocorrencias
     */
    public List<ListarParametroContratoOcorrenciasSaidaDTO> getOcorrencias() {
        return this.ocorrencias;
    }
}