/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarOperacaoContaDebTarifaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ListarOperacaoContaDebTarifaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdLayout
     */
    private java.lang.String _cdLayout = "PGICW980";

    /**
     * Field _tamanhoLayout
     */
    private int _tamanhoLayout = 16936;

    /**
     * keeps track of state for field: _tamanhoLayout
     */
    private boolean _has_tamanhoLayout;

    /**
     * Field _numeroLinhas
     */
    private int _numeroLinhas = 0;

    /**
     * keeps track of state for field: _numeroLinhas
     */
    private boolean _has_numeroLinhas;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarOperacaoContaDebTarifaResponse() 
     {
        super();
        setCdLayout("PGICW980");
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.ListarOperacaoContaDebTarifaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias) 

    /**
     * Method deleteNumeroLinhas
     * 
     */
    public void deleteNumeroLinhas()
    {
        this._has_numeroLinhas= false;
    } //-- void deleteNumeroLinhas() 

    /**
     * Method deleteTamanhoLayout
     * 
     */
    public void deleteTamanhoLayout()
    {
        this._has_tamanhoLayout= false;
    } //-- void deleteTamanhoLayout() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdLayout'.
     * 
     * @return String
     * @return the value of field 'cdLayout'.
     */
    public java.lang.String getCdLayout()
    {
        return this._cdLayout;
    } //-- java.lang.String getCdLayout() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numeroLinhas'.
     * 
     * @return int
     * @return the value of field 'numeroLinhas'.
     */
    public int getNumeroLinhas()
    {
        return this._numeroLinhas;
    } //-- int getNumeroLinhas() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'tamanhoLayout'.
     * 
     * @return int
     * @return the value of field 'tamanhoLayout'.
     */
    public int getTamanhoLayout()
    {
        return this._tamanhoLayout;
    } //-- int getTamanhoLayout() 

    /**
     * Method hasNumeroLinhas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLinhas()
    {
        return this._has_numeroLinhas;
    } //-- boolean hasNumeroLinhas() 

    /**
     * Method hasTamanhoLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasTamanhoLayout()
    {
        return this._has_tamanhoLayout;
    } //-- boolean hasTamanhoLayout() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdLayout'.
     * 
     * @param cdLayout the value of field 'cdLayout'.
     */
    public void setCdLayout(java.lang.String cdLayout)
    {
        this._cdLayout = cdLayout;
    } //-- void setCdLayout(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numeroLinhas'.
     * 
     * @param numeroLinhas the value of field 'numeroLinhas'.
     */
    public void setNumeroLinhas(int numeroLinhas)
    {
        this._numeroLinhas = numeroLinhas;
        this._has_numeroLinhas = true;
    } //-- void setNumeroLinhas(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.Ocorrencias) 

    /**
     * Sets the value of field 'tamanhoLayout'.
     * 
     * @param tamanhoLayout the value of field 'tamanhoLayout'.
     */
    public void setTamanhoLayout(int tamanhoLayout)
    {
        this._tamanhoLayout = tamanhoLayout;
        this._has_tamanhoLayout = true;
    } //-- void setTamanhoLayout(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarOperacaoContaDebTarifaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.ListarOperacaoContaDebTarifaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.ListarOperacaoContaDebTarifaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.ListarOperacaoContaDebTarifaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontadebtarifa.response.ListarOperacaoContaDebTarifaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
