/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.cadproccontrole;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.EstatisticaProcessamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.EstatisticaProcessamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.HistoricoBloqueioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.HistoricoBloqueioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterCadastroProcessosControle
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ICadProcControleService {

     /**
      * Listar processo massivo.
      *
      * @param processoMassivoEntradaDTO the processo massivo entrada dto
      * @return the list< processo massivo saida dt o>
      */
     List<ProcessoMassivoSaidaDTO> listarProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO);

     /**
      * Detalhar processo massivo.
      *
      * @param processoMassivoEntradaDTO the processo massivo entrada dto
      * @return the processo massivo saida dto
      */
     ProcessoMassivoSaidaDTO detalharProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO);

     /**
      * Incluir processo massivo.
      *
      * @param processoMassivoEntradaDTO the processo massivo entrada dto
      * @return the processo massivo saida dto
      */
     ProcessoMassivoSaidaDTO incluirProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO);

     /**
      * Alterar processo massivo.
      *
      * @param processoMassivoEntradaDTO the processo massivo entrada dto
      * @return the processo massivo saida dto
      */
     ProcessoMassivoSaidaDTO alterarProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO);

     /**
      * Excluir processo massivo.
      *
      * @param processoMassivoEntradaDTO the processo massivo entrada dto
      * @return the processo massivo saida dto
      */
     ProcessoMassivoSaidaDTO excluirProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO);

     /**
      * Bloquear processo massivo.
      *
      * @param processoMassivoEntradaDTO the processo massivo entrada dto
      * @return the processo massivo saida dto
      */
     ProcessoMassivoSaidaDTO bloquearProcessoMassivo(ProcessoMassivoEntradaDTO processoMassivoEntradaDTO);

     /**
      * Listar historico processo massivo.
      *
      * @param historicoBloqueioEntradaDTO the historico bloqueio entrada dto
      * @return the list< historico bloqueio saida dt o>
      */
     List<HistoricoBloqueioSaidaDTO> listarHistoricoProcessoMassivo(HistoricoBloqueioEntradaDTO historicoBloqueioEntradaDTO);

     /**
      * Detalhar historico processo massivo.
      *
      * @param historicoBloqueioEntradaDTO the historico bloqueio entrada dto
      * @return the historico bloqueio saida dto
      */
     HistoricoBloqueioSaidaDTO detalharHistoricoProcessoMassivo(HistoricoBloqueioEntradaDTO historicoBloqueioEntradaDTO);

     /**
      * Listar estatistica processamento.
      *
      * @param entradaDTO the entrada dto
      * @return the list< estatistica processamento saida dt o>
      */
     List<EstatisticaProcessamentoSaidaDTO> listarEstatisticaProcessamento(EstatisticaProcessamentoEntradaDTO entradaDTO);
}

