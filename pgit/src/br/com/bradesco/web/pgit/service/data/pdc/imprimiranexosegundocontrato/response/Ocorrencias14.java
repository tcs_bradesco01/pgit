/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias14.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias14 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsTriTipoTributo
     */
    private java.lang.String _dsTriTipoTributo;

    /**
     * Field _dsTriTipoConsultaSaldo
     */
    private java.lang.String _dsTriTipoConsultaSaldo;

    /**
     * Field _dsTriTipoProcessamento
     */
    private java.lang.String _dsTriTipoProcessamento;

    /**
     * Field _dsTriTipoTratamentoFeriado
     */
    private java.lang.String _dsTriTipoTratamentoFeriado;

    /**
     * Field _dsTriTipoRejeicaoEfetivacao
     */
    private java.lang.String _dsTriTipoRejeicaoEfetivacao;

    /**
     * Field _dsTriTipoRejeicaoAgendado
     */
    private java.lang.String _dsTriTipoRejeicaoAgendado;

    /**
     * Field _cdTriIndicadorValorMaximo
     */
    private java.lang.String _cdTriIndicadorValorMaximo;

    /**
     * Field _vlTriMaximoFavorecidoNao
     */
    private java.math.BigDecimal _vlTriMaximoFavorecidoNao = new java.math.BigDecimal("0");

    /**
     * Field _dsTriPrioridadeDebito
     */
    private java.lang.String _dsTriPrioridadeDebito;

    /**
     * Field _cdTriIndicadorValorDia
     */
    private java.lang.String _cdTriIndicadorValorDia;

    /**
     * Field _vlTriLimiteDiario
     */
    private java.math.BigDecimal _vlTriLimiteDiario = new java.math.BigDecimal("0");

    /**
     * Field _cdTriIndicadorValorIndividual
     */
    private java.lang.String _cdTriIndicadorValorIndividual;

    /**
     * Field _vlTriLimiteIndividual
     */
    private java.math.BigDecimal _vlTriLimiteIndividual = new java.math.BigDecimal("0");

    /**
     * Field _cdTriIndicadorQuantidadeFloating
     */
    private java.lang.String _cdTriIndicadorQuantidadeFloating;

    /**
     * Field _qttriDiaFloating
     */
    private int _qttriDiaFloating = 0;

    /**
     * keeps track of state for field: _qttriDiaFloating
     */
    private boolean _has_qttriDiaFloating;

    /**
     * Field _cdTriIndicadorCadastroFavorecido
     */
    private java.lang.String _cdTriIndicadorCadastroFavorecido;

    /**
     * Field _cdTriUtilizacaoCadastroFavorecido
     */
    private java.lang.String _cdTriUtilizacaoCadastroFavorecido;

    /**
     * Field _cdTriIndicadorQuantidadeRepique
     */
    private java.lang.String _cdTriIndicadorQuantidadeRepique;

    /**
     * Field _qtTriDiaRepique
     */
    private int _qtTriDiaRepique = 0;

    /**
     * keeps track of state for field: _qtTriDiaRepique
     */
    private boolean _has_qtTriDiaRepique;

    /**
     * Field _dsTriTipoConsFavorecido
     */
    private java.lang.String _dsTriTipoConsFavorecido;

    /**
     * Field _cdTriCferiLoc
     */
    private java.lang.String _cdTriCferiLoc;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias14() 
     {
        super();
        setVlTriMaximoFavorecidoNao(new java.math.BigDecimal("0"));
        setVlTriLimiteDiario(new java.math.BigDecimal("0"));
        setVlTriLimiteIndividual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteQtTriDiaRepique
     * 
     */
    public void deleteQtTriDiaRepique()
    {
        this._has_qtTriDiaRepique= false;
    } //-- void deleteQtTriDiaRepique() 

    /**
     * Method deleteQttriDiaFloating
     * 
     */
    public void deleteQttriDiaFloating()
    {
        this._has_qttriDiaFloating= false;
    } //-- void deleteQttriDiaFloating() 

    /**
     * Returns the value of field 'cdTriCferiLoc'.
     * 
     * @return String
     * @return the value of field 'cdTriCferiLoc'.
     */
    public java.lang.String getCdTriCferiLoc()
    {
        return this._cdTriCferiLoc;
    } //-- java.lang.String getCdTriCferiLoc() 

    /**
     * Returns the value of field
     * 'cdTriIndicadorCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdTriIndicadorCadastroFavorecido'
     */
    public java.lang.String getCdTriIndicadorCadastroFavorecido()
    {
        return this._cdTriIndicadorCadastroFavorecido;
    } //-- java.lang.String getCdTriIndicadorCadastroFavorecido() 

    /**
     * Returns the value of field
     * 'cdTriIndicadorQuantidadeFloating'.
     * 
     * @return String
     * @return the value of field 'cdTriIndicadorQuantidadeFloating'
     */
    public java.lang.String getCdTriIndicadorQuantidadeFloating()
    {
        return this._cdTriIndicadorQuantidadeFloating;
    } //-- java.lang.String getCdTriIndicadorQuantidadeFloating() 

    /**
     * Returns the value of field
     * 'cdTriIndicadorQuantidadeRepique'.
     * 
     * @return String
     * @return the value of field 'cdTriIndicadorQuantidadeRepique'.
     */
    public java.lang.String getCdTriIndicadorQuantidadeRepique()
    {
        return this._cdTriIndicadorQuantidadeRepique;
    } //-- java.lang.String getCdTriIndicadorQuantidadeRepique() 

    /**
     * Returns the value of field 'cdTriIndicadorValorDia'.
     * 
     * @return String
     * @return the value of field 'cdTriIndicadorValorDia'.
     */
    public java.lang.String getCdTriIndicadorValorDia()
    {
        return this._cdTriIndicadorValorDia;
    } //-- java.lang.String getCdTriIndicadorValorDia() 

    /**
     * Returns the value of field 'cdTriIndicadorValorIndividual'.
     * 
     * @return String
     * @return the value of field 'cdTriIndicadorValorIndividual'.
     */
    public java.lang.String getCdTriIndicadorValorIndividual()
    {
        return this._cdTriIndicadorValorIndividual;
    } //-- java.lang.String getCdTriIndicadorValorIndividual() 

    /**
     * Returns the value of field 'cdTriIndicadorValorMaximo'.
     * 
     * @return String
     * @return the value of field 'cdTriIndicadorValorMaximo'.
     */
    public java.lang.String getCdTriIndicadorValorMaximo()
    {
        return this._cdTriIndicadorValorMaximo;
    } //-- java.lang.String getCdTriIndicadorValorMaximo() 

    /**
     * Returns the value of field
     * 'cdTriUtilizacaoCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'cdTriUtilizacaoCadastroFavorecido'.
     */
    public java.lang.String getCdTriUtilizacaoCadastroFavorecido()
    {
        return this._cdTriUtilizacaoCadastroFavorecido;
    } //-- java.lang.String getCdTriUtilizacaoCadastroFavorecido() 

    /**
     * Returns the value of field 'dsTriPrioridadeDebito'.
     * 
     * @return String
     * @return the value of field 'dsTriPrioridadeDebito'.
     */
    public java.lang.String getDsTriPrioridadeDebito()
    {
        return this._dsTriPrioridadeDebito;
    } //-- java.lang.String getDsTriPrioridadeDebito() 

    /**
     * Returns the value of field 'dsTriTipoConsFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTriTipoConsFavorecido'.
     */
    public java.lang.String getDsTriTipoConsFavorecido()
    {
        return this._dsTriTipoConsFavorecido;
    } //-- java.lang.String getDsTriTipoConsFavorecido() 

    /**
     * Returns the value of field 'dsTriTipoConsultaSaldo'.
     * 
     * @return String
     * @return the value of field 'dsTriTipoConsultaSaldo'.
     */
    public java.lang.String getDsTriTipoConsultaSaldo()
    {
        return this._dsTriTipoConsultaSaldo;
    } //-- java.lang.String getDsTriTipoConsultaSaldo() 

    /**
     * Returns the value of field 'dsTriTipoProcessamento'.
     * 
     * @return String
     * @return the value of field 'dsTriTipoProcessamento'.
     */
    public java.lang.String getDsTriTipoProcessamento()
    {
        return this._dsTriTipoProcessamento;
    } //-- java.lang.String getDsTriTipoProcessamento() 

    /**
     * Returns the value of field 'dsTriTipoRejeicaoAgendado'.
     * 
     * @return String
     * @return the value of field 'dsTriTipoRejeicaoAgendado'.
     */
    public java.lang.String getDsTriTipoRejeicaoAgendado()
    {
        return this._dsTriTipoRejeicaoAgendado;
    } //-- java.lang.String getDsTriTipoRejeicaoAgendado() 

    /**
     * Returns the value of field 'dsTriTipoRejeicaoEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dsTriTipoRejeicaoEfetivacao'.
     */
    public java.lang.String getDsTriTipoRejeicaoEfetivacao()
    {
        return this._dsTriTipoRejeicaoEfetivacao;
    } //-- java.lang.String getDsTriTipoRejeicaoEfetivacao() 

    /**
     * Returns the value of field 'dsTriTipoTratamentoFeriado'.
     * 
     * @return String
     * @return the value of field 'dsTriTipoTratamentoFeriado'.
     */
    public java.lang.String getDsTriTipoTratamentoFeriado()
    {
        return this._dsTriTipoTratamentoFeriado;
    } //-- java.lang.String getDsTriTipoTratamentoFeriado() 

    /**
     * Returns the value of field 'dsTriTipoTributo'.
     * 
     * @return String
     * @return the value of field 'dsTriTipoTributo'.
     */
    public java.lang.String getDsTriTipoTributo()
    {
        return this._dsTriTipoTributo;
    } //-- java.lang.String getDsTriTipoTributo() 

    /**
     * Returns the value of field 'qtTriDiaRepique'.
     * 
     * @return int
     * @return the value of field 'qtTriDiaRepique'.
     */
    public int getQtTriDiaRepique()
    {
        return this._qtTriDiaRepique;
    } //-- int getQtTriDiaRepique() 

    /**
     * Returns the value of field 'qttriDiaFloating'.
     * 
     * @return int
     * @return the value of field 'qttriDiaFloating'.
     */
    public int getQttriDiaFloating()
    {
        return this._qttriDiaFloating;
    } //-- int getQttriDiaFloating() 

    /**
     * Returns the value of field 'vlTriLimiteDiario'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTriLimiteDiario'.
     */
    public java.math.BigDecimal getVlTriLimiteDiario()
    {
        return this._vlTriLimiteDiario;
    } //-- java.math.BigDecimal getVlTriLimiteDiario() 

    /**
     * Returns the value of field 'vlTriLimiteIndividual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTriLimiteIndividual'.
     */
    public java.math.BigDecimal getVlTriLimiteIndividual()
    {
        return this._vlTriLimiteIndividual;
    } //-- java.math.BigDecimal getVlTriLimiteIndividual() 

    /**
     * Returns the value of field 'vlTriMaximoFavorecidoNao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTriMaximoFavorecidoNao'.
     */
    public java.math.BigDecimal getVlTriMaximoFavorecidoNao()
    {
        return this._vlTriMaximoFavorecidoNao;
    } //-- java.math.BigDecimal getVlTriMaximoFavorecidoNao() 

    /**
     * Method hasQtTriDiaRepique
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtTriDiaRepique()
    {
        return this._has_qtTriDiaRepique;
    } //-- boolean hasQtTriDiaRepique() 

    /**
     * Method hasQttriDiaFloating
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQttriDiaFloating()
    {
        return this._has_qttriDiaFloating;
    } //-- boolean hasQttriDiaFloating() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTriCferiLoc'.
     * 
     * @param cdTriCferiLoc the value of field 'cdTriCferiLoc'.
     */
    public void setCdTriCferiLoc(java.lang.String cdTriCferiLoc)
    {
        this._cdTriCferiLoc = cdTriCferiLoc;
    } //-- void setCdTriCferiLoc(java.lang.String) 

    /**
     * Sets the value of field 'cdTriIndicadorCadastroFavorecido'.
     * 
     * @param cdTriIndicadorCadastroFavorecido the value of field
     * 'cdTriIndicadorCadastroFavorecido'.
     */
    public void setCdTriIndicadorCadastroFavorecido(java.lang.String cdTriIndicadorCadastroFavorecido)
    {
        this._cdTriIndicadorCadastroFavorecido = cdTriIndicadorCadastroFavorecido;
    } //-- void setCdTriIndicadorCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdTriIndicadorQuantidadeFloating'.
     * 
     * @param cdTriIndicadorQuantidadeFloating the value of field
     * 'cdTriIndicadorQuantidadeFloating'.
     */
    public void setCdTriIndicadorQuantidadeFloating(java.lang.String cdTriIndicadorQuantidadeFloating)
    {
        this._cdTriIndicadorQuantidadeFloating = cdTriIndicadorQuantidadeFloating;
    } //-- void setCdTriIndicadorQuantidadeFloating(java.lang.String) 

    /**
     * Sets the value of field 'cdTriIndicadorQuantidadeRepique'.
     * 
     * @param cdTriIndicadorQuantidadeRepique the value of field
     * 'cdTriIndicadorQuantidadeRepique'.
     */
    public void setCdTriIndicadorQuantidadeRepique(java.lang.String cdTriIndicadorQuantidadeRepique)
    {
        this._cdTriIndicadorQuantidadeRepique = cdTriIndicadorQuantidadeRepique;
    } //-- void setCdTriIndicadorQuantidadeRepique(java.lang.String) 

    /**
     * Sets the value of field 'cdTriIndicadorValorDia'.
     * 
     * @param cdTriIndicadorValorDia the value of field
     * 'cdTriIndicadorValorDia'.
     */
    public void setCdTriIndicadorValorDia(java.lang.String cdTriIndicadorValorDia)
    {
        this._cdTriIndicadorValorDia = cdTriIndicadorValorDia;
    } //-- void setCdTriIndicadorValorDia(java.lang.String) 

    /**
     * Sets the value of field 'cdTriIndicadorValorIndividual'.
     * 
     * @param cdTriIndicadorValorIndividual the value of field
     * 'cdTriIndicadorValorIndividual'.
     */
    public void setCdTriIndicadorValorIndividual(java.lang.String cdTriIndicadorValorIndividual)
    {
        this._cdTriIndicadorValorIndividual = cdTriIndicadorValorIndividual;
    } //-- void setCdTriIndicadorValorIndividual(java.lang.String) 

    /**
     * Sets the value of field 'cdTriIndicadorValorMaximo'.
     * 
     * @param cdTriIndicadorValorMaximo the value of field
     * 'cdTriIndicadorValorMaximo'.
     */
    public void setCdTriIndicadorValorMaximo(java.lang.String cdTriIndicadorValorMaximo)
    {
        this._cdTriIndicadorValorMaximo = cdTriIndicadorValorMaximo;
    } //-- void setCdTriIndicadorValorMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdTriUtilizacaoCadastroFavorecido'.
     * 
     * @param cdTriUtilizacaoCadastroFavorecido the value of field
     * 'cdTriUtilizacaoCadastroFavorecido'.
     */
    public void setCdTriUtilizacaoCadastroFavorecido(java.lang.String cdTriUtilizacaoCadastroFavorecido)
    {
        this._cdTriUtilizacaoCadastroFavorecido = cdTriUtilizacaoCadastroFavorecido;
    } //-- void setCdTriUtilizacaoCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTriPrioridadeDebito'.
     * 
     * @param dsTriPrioridadeDebito the value of field
     * 'dsTriPrioridadeDebito'.
     */
    public void setDsTriPrioridadeDebito(java.lang.String dsTriPrioridadeDebito)
    {
        this._dsTriPrioridadeDebito = dsTriPrioridadeDebito;
    } //-- void setDsTriPrioridadeDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsTriTipoConsFavorecido'.
     * 
     * @param dsTriTipoConsFavorecido the value of field
     * 'dsTriTipoConsFavorecido'.
     */
    public void setDsTriTipoConsFavorecido(java.lang.String dsTriTipoConsFavorecido)
    {
        this._dsTriTipoConsFavorecido = dsTriTipoConsFavorecido;
    } //-- void setDsTriTipoConsFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTriTipoConsultaSaldo'.
     * 
     * @param dsTriTipoConsultaSaldo the value of field
     * 'dsTriTipoConsultaSaldo'.
     */
    public void setDsTriTipoConsultaSaldo(java.lang.String dsTriTipoConsultaSaldo)
    {
        this._dsTriTipoConsultaSaldo = dsTriTipoConsultaSaldo;
    } //-- void setDsTriTipoConsultaSaldo(java.lang.String) 

    /**
     * Sets the value of field 'dsTriTipoProcessamento'.
     * 
     * @param dsTriTipoProcessamento the value of field
     * 'dsTriTipoProcessamento'.
     */
    public void setDsTriTipoProcessamento(java.lang.String dsTriTipoProcessamento)
    {
        this._dsTriTipoProcessamento = dsTriTipoProcessamento;
    } //-- void setDsTriTipoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTriTipoRejeicaoAgendado'.
     * 
     * @param dsTriTipoRejeicaoAgendado the value of field
     * 'dsTriTipoRejeicaoAgendado'.
     */
    public void setDsTriTipoRejeicaoAgendado(java.lang.String dsTriTipoRejeicaoAgendado)
    {
        this._dsTriTipoRejeicaoAgendado = dsTriTipoRejeicaoAgendado;
    } //-- void setDsTriTipoRejeicaoAgendado(java.lang.String) 

    /**
     * Sets the value of field 'dsTriTipoRejeicaoEfetivacao'.
     * 
     * @param dsTriTipoRejeicaoEfetivacao the value of field
     * 'dsTriTipoRejeicaoEfetivacao'.
     */
    public void setDsTriTipoRejeicaoEfetivacao(java.lang.String dsTriTipoRejeicaoEfetivacao)
    {
        this._dsTriTipoRejeicaoEfetivacao = dsTriTipoRejeicaoEfetivacao;
    } //-- void setDsTriTipoRejeicaoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'dsTriTipoTratamentoFeriado'.
     * 
     * @param dsTriTipoTratamentoFeriado the value of field
     * 'dsTriTipoTratamentoFeriado'.
     */
    public void setDsTriTipoTratamentoFeriado(java.lang.String dsTriTipoTratamentoFeriado)
    {
        this._dsTriTipoTratamentoFeriado = dsTriTipoTratamentoFeriado;
    } //-- void setDsTriTipoTratamentoFeriado(java.lang.String) 

    /**
     * Sets the value of field 'dsTriTipoTributo'.
     * 
     * @param dsTriTipoTributo the value of field 'dsTriTipoTributo'
     */
    public void setDsTriTipoTributo(java.lang.String dsTriTipoTributo)
    {
        this._dsTriTipoTributo = dsTriTipoTributo;
    } //-- void setDsTriTipoTributo(java.lang.String) 

    /**
     * Sets the value of field 'qtTriDiaRepique'.
     * 
     * @param qtTriDiaRepique the value of field 'qtTriDiaRepique'.
     */
    public void setQtTriDiaRepique(int qtTriDiaRepique)
    {
        this._qtTriDiaRepique = qtTriDiaRepique;
        this._has_qtTriDiaRepique = true;
    } //-- void setQtTriDiaRepique(int) 

    /**
     * Sets the value of field 'qttriDiaFloating'.
     * 
     * @param qttriDiaFloating the value of field 'qttriDiaFloating'
     */
    public void setQttriDiaFloating(int qttriDiaFloating)
    {
        this._qttriDiaFloating = qttriDiaFloating;
        this._has_qttriDiaFloating = true;
    } //-- void setQttriDiaFloating(int) 

    /**
     * Sets the value of field 'vlTriLimiteDiario'.
     * 
     * @param vlTriLimiteDiario the value of field
     * 'vlTriLimiteDiario'.
     */
    public void setVlTriLimiteDiario(java.math.BigDecimal vlTriLimiteDiario)
    {
        this._vlTriLimiteDiario = vlTriLimiteDiario;
    } //-- void setVlTriLimiteDiario(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTriLimiteIndividual'.
     * 
     * @param vlTriLimiteIndividual the value of field
     * 'vlTriLimiteIndividual'.
     */
    public void setVlTriLimiteIndividual(java.math.BigDecimal vlTriLimiteIndividual)
    {
        this._vlTriLimiteIndividual = vlTriLimiteIndividual;
    } //-- void setVlTriLimiteIndividual(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTriMaximoFavorecidoNao'.
     * 
     * @param vlTriMaximoFavorecidoNao the value of field
     * 'vlTriMaximoFavorecidoNao'.
     */
    public void setVlTriMaximoFavorecidoNao(java.math.BigDecimal vlTriMaximoFavorecidoNao)
    {
        this._vlTriMaximoFavorecidoNao = vlTriMaximoFavorecidoNao;
    } //-- void setVlTriMaximoFavorecidoNao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias14
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias14 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
