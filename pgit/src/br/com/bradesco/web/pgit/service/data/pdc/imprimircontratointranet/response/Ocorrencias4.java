/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias4.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias4 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsProdutoSalarioGuicheCaixa
     */
    private java.lang.String _dsProdutoSalarioGuicheCaixa;

    /**
     * Field _dsServicoSalarioGuicheCaixa
     */
    private java.lang.String _dsServicoSalarioGuicheCaixa;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias4() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'dsProdutoSalarioGuicheCaixa'.
     * 
     * @return String
     * @return the value of field 'dsProdutoSalarioGuicheCaixa'.
     */
    public java.lang.String getDsProdutoSalarioGuicheCaixa()
    {
        return this._dsProdutoSalarioGuicheCaixa;
    } //-- java.lang.String getDsProdutoSalarioGuicheCaixa() 

    /**
     * Returns the value of field 'dsServicoSalarioGuicheCaixa'.
     * 
     * @return String
     * @return the value of field 'dsServicoSalarioGuicheCaixa'.
     */
    public java.lang.String getDsServicoSalarioGuicheCaixa()
    {
        return this._dsServicoSalarioGuicheCaixa;
    } //-- java.lang.String getDsServicoSalarioGuicheCaixa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'dsProdutoSalarioGuicheCaixa'.
     * 
     * @param dsProdutoSalarioGuicheCaixa the value of field
     * 'dsProdutoSalarioGuicheCaixa'.
     */
    public void setDsProdutoSalarioGuicheCaixa(java.lang.String dsProdutoSalarioGuicheCaixa)
    {
        this._dsProdutoSalarioGuicheCaixa = dsProdutoSalarioGuicheCaixa;
    } //-- void setDsProdutoSalarioGuicheCaixa(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoSalarioGuicheCaixa'.
     * 
     * @param dsServicoSalarioGuicheCaixa the value of field
     * 'dsServicoSalarioGuicheCaixa'.
     */
    public void setDsServicoSalarioGuicheCaixa(java.lang.String dsServicoSalarioGuicheCaixa)
    {
        this._dsServicoSalarioGuicheCaixa = dsServicoSalarioGuicheCaixa;
    } //-- void setDsServicoSalarioGuicheCaixa(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias4
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimircontratointranet.response.Ocorrencias4 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
