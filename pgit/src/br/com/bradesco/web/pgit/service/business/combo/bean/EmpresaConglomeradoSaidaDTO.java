/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.io.Serializable;

/**
 * Nome: EmpresaConglomeradoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmpresaConglomeradoSaidaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 4861621340505212413L;

	/** Atributo codClub. */
	private Long codClub;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
	
	/**
	 * Get: codClub.
	 *
	 * @return codClub
	 */
	public Long getCodClub() {
		return codClub;
	}
	
	/**
	 * Set: codClub.
	 *
	 * @param cdPessoaJuridicaContrato the cod club
	 */
	public void setCodClub(Long cdPessoaJuridicaContrato) {
		this.codClub = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsCnpj the ds razao social
	 */
	public void setDsRazaoSocial(String dsCnpj) {
		this.dsRazaoSocial = dsCnpj;
	}
    
    
}
