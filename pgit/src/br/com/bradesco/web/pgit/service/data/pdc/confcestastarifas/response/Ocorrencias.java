/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.confcestastarifas.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.math.BigDecimal;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      /**
	 * 
	 */
	private static final long serialVersionUID = 337973251750400748L;

	//--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCestaTariafaFlexbilizacao
     */
    private int _cdCestaTariafaFlexbilizacao = 0;

    /**
     * keeps track of state for field: _cdCestaTariafaFlexbilizacao
     */
    private boolean _has_cdCestaTariafaFlexbilizacao;

    /**
     * Field _dsCestaTariafaFlexbilizacao
     */
    private java.lang.String _dsCestaTariafaFlexbilizacao;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _vlTariafaFlexbilizacaoContrato
     */
    private java.math.BigDecimal _vlTariafaFlexbilizacaoContrato = new java.math.BigDecimal("0");

    /**
     * Field _porcentagemTariafaFlexbilizacaoContrato
     */
    private java.math.BigDecimal _porcentagemTariafaFlexbilizacaoContrato = new java.math.BigDecimal("0");

    /**
     * Field _dataVigenciaInicio
     */
    private java.lang.String _dataVigenciaInicio;

    /**
     * Field _dataVigenciaFim
     */
    private java.lang.String _dataVigenciaFim;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
        setVlTariafaFlexbilizacaoContrato(new java.math.BigDecimal("0"));
        setPorcentagemTariafaFlexbilizacaoContrato(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.confcestastarifas.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCestaTariafaFlexbilizacao
     * 
     */
    public void deleteCdCestaTariafaFlexbilizacao()
    {
        this._has_cdCestaTariafaFlexbilizacao= false;
    } //-- void deleteCdCestaTariafaFlexbilizacao() 

    /**
     * Returns the value of field 'cdCestaTariafaFlexbilizacao'.
     * 
     * @return int
     * @return the value of field 'cdCestaTariafaFlexbilizacao'.
     */
    public int getCdCestaTariafaFlexbilizacao()
    {
        return this._cdCestaTariafaFlexbilizacao;
    } //-- int getCdCestaTariafaFlexbilizacao() 

    /**
     * Returns the value of field 'dataVigenciaFim'.
     * 
     * @return String
     * @return the value of field 'dataVigenciaFim'.
     */
    public java.lang.String getDataVigenciaFim()
    {
        return this._dataVigenciaFim;
    } //-- java.lang.String getDataVigenciaFim() 

    /**
     * Returns the value of field 'dataVigenciaInicio'.
     * 
     * @return String
     * @return the value of field 'dataVigenciaInicio'.
     */
    public java.lang.String getDataVigenciaInicio()
    {
        return this._dataVigenciaInicio;
    } //-- java.lang.String getDataVigenciaInicio() 

    /**
     * Returns the value of field 'dsCestaTariafaFlexbilizacao'.
     * 
     * @return String
     * @return the value of field 'dsCestaTariafaFlexbilizacao'.
     */
    public java.lang.String getDsCestaTariafaFlexbilizacao()
    {
        return this._dsCestaTariafaFlexbilizacao;
    } //-- java.lang.String getDsCestaTariafaFlexbilizacao() 

    /**
     * Returns the value of field
     * 'porcentagemTariafaFlexbilizacaoContrato'.
     * 
     * @return BigDecimal
     * @return the value of field
     * 'porcentagemTariafaFlexbilizacaoContrato'.
     */
    public java.math.BigDecimal getPorcentagemTariafaFlexbilizacaoContrato()
    {
        return this._porcentagemTariafaFlexbilizacaoContrato;
    } //-- java.math.BigDecimal getPorcentagemTariafaFlexbilizacaoContrato() 

    /**
     * Returns the value of field 'vlTariafaFlexbilizacaoContrato'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTariafaFlexbilizacaoContrato'.
     */
    public java.math.BigDecimal getVlTariafaFlexbilizacaoContrato()
    {
        return this._vlTariafaFlexbilizacaoContrato;
    } //-- java.math.BigDecimal getVlTariafaFlexbilizacaoContrato() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Method hasCdCestaTariafaFlexbilizacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCestaTariafaFlexbilizacao()
    {
        return this._has_cdCestaTariafaFlexbilizacao;
    } //-- boolean hasCdCestaTariafaFlexbilizacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCestaTariafaFlexbilizacao'.
     * 
     * @param cdCestaTariafaFlexbilizacao the value of field
     * 'cdCestaTariafaFlexbilizacao'.
     */
    public void setCdCestaTariafaFlexbilizacao(int cdCestaTariafaFlexbilizacao)
    {
        this._cdCestaTariafaFlexbilizacao = cdCestaTariafaFlexbilizacao;
        this._has_cdCestaTariafaFlexbilizacao = true;
    } //-- void setCdCestaTariafaFlexbilizacao(int) 

    /**
     * Sets the value of field 'dataVigenciaFim'.
     * 
     * @param dataVigenciaFim the value of field 'dataVigenciaFim'.
     */
    public void setDataVigenciaFim(java.lang.String dataVigenciaFim)
    {
        this._dataVigenciaFim = dataVigenciaFim;
    } //-- void setDataVigenciaFim(java.lang.String) 

    /**
     * Sets the value of field 'dataVigenciaInicio'.
     * 
     * @param dataVigenciaInicio the value of field
     * 'dataVigenciaInicio'.
     */
    public void setDataVigenciaInicio(java.lang.String dataVigenciaInicio)
    {
        this._dataVigenciaInicio = dataVigenciaInicio;
    } //-- void setDataVigenciaInicio(java.lang.String) 

    /**
     * Sets the value of field 'dsCestaTariafaFlexbilizacao'.
     * 
     * @param dsCestaTariafaFlexbilizacao the value of field
     * 'dsCestaTariafaFlexbilizacao'.
     */
    public void setDsCestaTariafaFlexbilizacao(java.lang.String dsCestaTariafaFlexbilizacao)
    {
        this._dsCestaTariafaFlexbilizacao = dsCestaTariafaFlexbilizacao;
    } //-- void setDsCestaTariafaFlexbilizacao(java.lang.String) 

    /**
     * Sets the value of field
     * 'porcentagemTariafaFlexbilizacaoContrato'.
     * 
     * @param porcentagemTariafaFlexbilizacaoContrato the value of
     * field 'porcentagemTariafaFlexbilizacaoContrato'.
     */
    public void setPorcentagemTariafaFlexbilizacaoContrato(java.math.BigDecimal porcentagemTariafaFlexbilizacaoContrato)
    {
        this._porcentagemTariafaFlexbilizacaoContrato = porcentagemTariafaFlexbilizacaoContrato;
    } //-- void setPorcentagemTariafaFlexbilizacaoContrato(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTariafaFlexbilizacaoContrato'.
     * 
     * @param vlTariafaFlexbilizacaoContrato the value of field
     * 'vlTariafaFlexbilizacaoContrato'.
     */
    public void setVlTariafaFlexbilizacaoContrato(java.math.BigDecimal vlTariafaFlexbilizacaoContrato)
    {
        this._vlTariafaFlexbilizacaoContrato = vlTariafaFlexbilizacaoContrato;
    } //-- void setVlTariafaFlexbilizacaoContrato(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.confcestastarifas.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.confcestastarifas.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.confcestastarifas.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.confcestastarifas.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 


	public Ocorrencias(int cdCestaTariafaFlexbilizacao,
			String dsCestaTariafaFlexbilizacao, BigDecimal vlTarifaPadrao,
			BigDecimal vlTariafaFlexbilizacaoContrato,
			BigDecimal porcentagemTariafaFlexbilizacaoContrato,
			String dataVigenciaInicio, String dataVigenciaFim) {
		super();
		_cdCestaTariafaFlexbilizacao = cdCestaTariafaFlexbilizacao;
		_dsCestaTariafaFlexbilizacao = dsCestaTariafaFlexbilizacao;
		_vlTarifaPadrao = vlTarifaPadrao;
		_vlTariafaFlexbilizacaoContrato = vlTariafaFlexbilizacaoContrato;
		_porcentagemTariafaFlexbilizacaoContrato = porcentagemTariafaFlexbilizacaoContrato;
		_dataVigenciaInicio = dataVigenciaInicio;
		_dataVigenciaFim = dataVigenciaFim;
	}
    
    

}
