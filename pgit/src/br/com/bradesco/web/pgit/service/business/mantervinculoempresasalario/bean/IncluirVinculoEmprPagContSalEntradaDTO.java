/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

/**
 * Nome: IncluirVinculoEmprPagContSalEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirVinculoEmprPagContSalEntradaDTO {
    
    /** Atributo cdPessoaJuridicaNegocio. */
    private Long cdPessoaJuridicaNegocio;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdBancoSalario. */
    private Integer cdBancoSalario;

    /** Atributo cdAgenciaSalario. */
    private Integer cdAgenciaSalario;

    /** Atributo cdDigitoAgenciaSalario. */
    private Integer cdDigitoAgenciaSalario;

    /** Atributo cdContaSalario. */
    private Long cdContaSalario;

    /** Atributo dsDigitoContaSalario. */
    private String dsDigitoContaSalario;

    /** Atributo dsBancoSalario. */
    private String dsBancoSalario;

    /** Atributo dsAgenciaSalario. */
    private String dsAgenciaSalario;

    /**
     * Get: banco.
     *
     * @return banco
     */
    public String getBanco() {
	if (dsBancoSalario != null && !dsBancoSalario.trim().equals("")) {
	    return cdBancoSalario + " " + dsBancoSalario;
	} else {
	    return String.valueOf(cdBancoSalario);
	}
    }

    /**
     * Get: agencia.
     *
     * @return agencia
     */
    public String getAgencia() {
	if (!dsAgenciaSalario.trim().equals("") && dsAgenciaSalario != null) {
	    return cdAgenciaSalario + " " + dsAgenciaSalario;
	} else {
	    return String.valueOf(cdAgenciaSalario);
	}
    }

    /**
     * Get: contaDigito.
     *
     * @return contaDigito
     */
    public String getContaDigito() {
	return dsDigitoContaSalario == null || dsDigitoContaSalario.trim().equals("") ? String.valueOf(cdContaSalario) : cdContaSalario + "-" + dsDigitoContaSalario;
    }

    /**
     * Get: cdPessoaJuridicaNegocio.
     *
     * @return cdPessoaJuridicaNegocio
     */
    public Long getCdPessoaJuridicaNegocio() {
	return cdPessoaJuridicaNegocio;
    }

    /**
     * Set: cdPessoaJuridicaNegocio.
     *
     * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
     */
    public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
	this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
	return nrSequenciaContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
	this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: cdBancoSalario.
     *
     * @return cdBancoSalario
     */
    public Integer getCdBancoSalario() {
	return cdBancoSalario;
    }

    /**
     * Set: cdBancoSalario.
     *
     * @param cdBancoSalario the cd banco salario
     */
    public void setCdBancoSalario(Integer cdBancoSalario) {
	this.cdBancoSalario = cdBancoSalario;
    }

    /**
     * Get: cdAgenciaSalario.
     *
     * @return cdAgenciaSalario
     */
    public Integer getCdAgenciaSalario() {
	return cdAgenciaSalario;
    }

    /**
     * Set: cdAgenciaSalario.
     *
     * @param cdAgenciaSalario the cd agencia salario
     */
    public void setCdAgenciaSalario(Integer cdAgenciaSalario) {
	this.cdAgenciaSalario = cdAgenciaSalario;
    }

    /**
     * Get: cdDigitoAgenciaSalario.
     *
     * @return cdDigitoAgenciaSalario
     */
    public Integer getCdDigitoAgenciaSalario() {
	return cdDigitoAgenciaSalario;
    }

    /**
     * Set: cdDigitoAgenciaSalario.
     *
     * @param cdDigitoAgenciaSalario the cd digito agencia salario
     */
    public void setCdDigitoAgenciaSalario(Integer cdDigitoAgenciaSalario) {
	this.cdDigitoAgenciaSalario = cdDigitoAgenciaSalario;
    }

    /**
     * Get: cdContaSalario.
     *
     * @return cdContaSalario
     */
    public Long getCdContaSalario() {
	return cdContaSalario;
    }

    /**
     * Set: cdContaSalario.
     *
     * @param cdContaSalario the cd conta salario
     */
    public void setCdContaSalario(Long cdContaSalario) {
	this.cdContaSalario = cdContaSalario;
    }

    /**
     * Get: dsDigitoContaSalario.
     *
     * @return dsDigitoContaSalario
     */
    public String getDsDigitoContaSalario() {
	return dsDigitoContaSalario;
    }

    /**
     * Set: dsDigitoContaSalario.
     *
     * @param dsDigitoContaSalario the ds digito conta salario
     */
    public void setDsDigitoContaSalario(String dsDigitoContaSalario) {
	this.dsDigitoContaSalario = dsDigitoContaSalario;
    }

    /**
     * Get: dsAgenciaSalario.
     *
     * @return dsAgenciaSalario
     */
    public String getDsAgenciaSalario() {
	return dsAgenciaSalario;
    }

    /**
     * Set: dsAgenciaSalario.
     *
     * @param dsAgenciaSalario the ds agencia salario
     */
    public void setDsAgenciaSalario(String dsAgenciaSalario) {
	this.dsAgenciaSalario = dsAgenciaSalario;
    }

    /**
     * Get: dsBancoSalario.
     *
     * @return dsBancoSalario
     */
    public String getDsBancoSalario() {
	return dsBancoSalario;
    }

    /**
     * Set: dsBancoSalario.
     *
     * @param dsBancoSalario the ds banco salario
     */
    public void setDsBancoSalario(String dsBancoSalario) {
	this.dsBancoSalario = dsBancoSalario;
    }
}