/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.impl;

import java.util.ArrayList;

import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.IConsultaSolicitacaoEstornoOPService;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.IConsultaSolicitacaoEstornoOPServiceConstants;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultaSolicitacaoEstornoOPEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultaSolicitacaoEstornoOPListaDTO;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultaSolicitacaoEstornoOPSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultarDetalhesSolicitacaoOPEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultarDetalhesSolicitacaoOPSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.request.ConsSolicitacaoEstornoPagtosOPRequest;
import br.com.bradesco.web.pgit.service.data.pdc.conssolicitacaoestornopagtosop.response.ConsSolicitacaoEstornoPagtosOPResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.request.ConsultarDetalhesSolicitacaoOPRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhessolicitacaoop.response.ConsultarDetalhesSolicitacaoOPResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ConsultaSolicitacaoEstornoOPServiceImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultaSolicitacaoEstornoOPServiceImpl implements IConsultaSolicitacaoEstornoOPService {

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.IConsultaSolicitacaoEstornoOPService#consSolicitacaoEstornoPagtosOP(br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultaSolicitacaoEstornoOPEntradaDTO)
	 */
	public ConsultaSolicitacaoEstornoOPSaidaDTO consSolicitacaoEstornoPagtosOP(ConsultaSolicitacaoEstornoOPEntradaDTO entradaDTO) {
		ConsSolicitacaoEstornoPagtosOPRequest request = new ConsSolicitacaoEstornoPagtosOPRequest();
		ConsSolicitacaoEstornoPagtosOPResponse response = new ConsSolicitacaoEstornoPagtosOPResponse();
		ConsultaSolicitacaoEstornoOPSaidaDTO saidaDTO = new ConsultaSolicitacaoEstornoOPSaidaDTO();

		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setDtSolicitacaoInicio(FormatarData.formataDiaMesAnoToPdc(entradaDTO.getDtSolicitacaoInicio()));
		request.setDtSolicitacaoFim(FormatarData.formataDiaMesAnoToPdc(entradaDTO.getDtSolicitacaoFim()));
		request.setCdSituacaoSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacaoPagamento()));
		request.setCdMotivoSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoSolicitacao()));
		request.setCdModalidadePagamentoCliente(PgitUtil.verificaIntegerNulo(entradaDTO.getCdModalidadePagamentoCliente()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setNrOcorrencias(IConsultaSolicitacaoEstornoOPServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);

		response = getFactoryAdapter().getConsSolicitacaoEstornoPagtosOPPDCAdapter().invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setNumeroLinhas(response.getNumeroLinhas());
		saidaDTO.setOcorrencias(new ArrayList<ConsultaSolicitacaoEstornoOPListaDTO>());
		ConsultaSolicitacaoEstornoOPListaDTO dto;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			dto = new ConsultaSolicitacaoEstornoOPListaDTO();
			dto.setCdSolicitacaoPagamento(response.getOcorrencias(i).getCdSolicitacaoPagamento());
			dto.setNrSolicitacaoPagamentoIntegrado(response.getOcorrencias(i).getNrSolicitacaoPagamentoIntegrado());
			dto.setCdCpfCnpjParticipante(response.getOcorrencias(i).getCdCpfCnpjParticipante());
			dto.setCdPessoaCompleto(response.getOcorrencias(i).getCdPessoaCompleto());
			dto.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
			dto.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			dto.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
			dto.setCdModalidade(response.getOcorrencias(i).getCdModalidade());
			dto.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
			dto.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
			dto.setDsSituacaoSolicitante(response.getOcorrencias(i).getDsSituacaoSolicitante());
			dto.setCdMotivoSituacaoSolicitante(response.getOcorrencias(i).getCdMotivoSituacaoSolicitante());
			dto.setDsMotivoSituacaoSolicitante(response.getOcorrencias(i).getDsMotivoSituacaoSolicitante());
			dto.setDsDataSolicitacao(response.getOcorrencias(i).getDsDataSolicitacao());
			dto.setDsHoraSolicitacao(response.getOcorrencias(i).getDsHoraSolicitacao());

			saidaDTO.getOcorrencias().add(dto);
		}

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.IConsultaSolicitacaoEstornoOPService#consultarDetalhesSolicitacaoOP(br.com.bradesco.web.pgit.service.business.consultasolicitacaoestornoop.bean.ConsultarDetalhesSolicitacaoOPEntradaDTO)
	 */
	public ConsultarDetalhesSolicitacaoOPSaidaDTO consultarDetalhesSolicitacaoOP(ConsultarDetalhesSolicitacaoOPEntradaDTO entrada) {
		ConsultarDetalhesSolicitacaoOPRequest request = new ConsultarDetalhesSolicitacaoOPRequest();
		ConsultarDetalhesSolicitacaoOPResponse response = new ConsultarDetalhesSolicitacaoOPResponse();
		ConsultarDetalhesSolicitacaoOPSaidaDTO saida = new ConsultarDetalhesSolicitacaoOPSaidaDTO();

		request.setCdTipoAcao(PgitUtil.verificaIntegerNulo(entrada.getCdTipoAcao()));
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSolicitacaoPagamento()));
		request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacaoPagamento()));
		request.setDsObservacao(PgitUtil.verificaStringNula(entrada.getDsObservacao()));

		response = getFactoryAdapter().getConsultarDetalhesSolicitacaoOPPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setCdBancoEstorno(response.getCdBancoEstorno());
		saida.setDsBancoEstorno(response.getDsBancoEstorno());
		saida.setCdAgenciaEstorno(response.getCdAgenciaEstorno());
		saida.setCdDigitoAgenciaEstorno(response.getCdDigitoAgenciaEstorno());
		saida.setDsAgenciaEstorno(response.getDsAgenciaEstorno());
		saida.setCdContaEstorno(response.getCdContaEstorno());
		saida.setCdDigitoContaEstorno(response.getCdDigitoContaEstorno());
		saida.setDsTipoConta(response.getDsTipoConta());
		saida.setSituacaoSolicitacao(response.getSituacaoSolicitacao());
		saida.setCdMotivoSolicitacao(response.getCdMotivoSolicitacao());
		saida.setDsObservacao(response.getDsObservacao());
		saida.setDsDataSolicitacao(response.getDsDataSolicitacao());
		saida.setDsHoraSolicitacao(response.getDsHoraSolicitacao());
		saida.setDtAutorizacao(FormatarData.formatarData(response.getDtAutorizacao()));
		saida.setHrAutorizacao(response.getHrAutorizacao());
		saida.setCdUsuarioAutorizacao(response.getCdUsuarioAutorizacao());
		saida.setDtPrevistaEstorno(FormatarData.formatarData(response.getDtPrevistaEstorno()));
		saida.setCdValorPrevistoEstorno(response.getCdValorPrevistoEstorno());

		return saida;
	}

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}
