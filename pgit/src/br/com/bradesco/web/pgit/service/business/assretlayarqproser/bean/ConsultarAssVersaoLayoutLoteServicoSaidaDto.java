/*
 * Nome: br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean;

/**
 * Nome: ConsultarAssVersaoLayoutLoteServicoSaidaDto
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public class ConsultarAssVersaoLayoutLoteServicoSaidaDto {

    private String codMensagem = null;
    private String mensagem = null;
    private Integer cdTipoLayoutArquivo = null;
    private String dsTipoLayoutArquivo = null;
    private Integer nrVersaoLayoutArquivo = null;
    private Integer cdTipoLoteLayout = null;
    private String dsTipoLoteLayout = null;
    private Integer nrVersaoLoteLayout = null;
    private Integer cdTipoServicoCnab = null;
    private String dsTipoServicoCnab = null;
    private String cdUsuarioInclusao = null;
    private String cdUsuarioInclusaoExterno = null;
    private String hrInclusaoRegistro = null;
    private String cdOperacaoCanalInclusao = null;
    private Integer cdTipoCanalInclusao = null;
    private String cdUsuarioManutencao = null;
    private String cdUsuarioManutencaoExterno = null;
    private String hrManutencaoRegistro = null;
    private String cdOperacaoCanalManutencao = null;
    private Integer cdTipoCanalManutencao = null;


    public String getCodMensagem() {
    	return codMensagem;
    }

    public void setCodMensagem(String codMensagemAux) {
        this.codMensagem = codMensagemAux;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagemAux) {
        this.mensagem = mensagemAux;
    }

    public Integer getCdTipoLayoutArquivo() {
        return cdTipoLayoutArquivo;
    }

    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivoAux) {
        this.cdTipoLayoutArquivo = cdTipoLayoutArquivoAux;
    }

    public String getDsTipoLayoutArquivo() {
        return dsTipoLayoutArquivo;
    }

    public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivoAux) {
        this.dsTipoLayoutArquivo = dsTipoLayoutArquivoAux;
    }

    public Integer getNrVersaoLayoutArquivo() {
        return nrVersaoLayoutArquivo;
    }

    public void setNrVersaoLayoutArquivo(Integer nrVersaoLayoutArquivoAux) {
        this.nrVersaoLayoutArquivo = nrVersaoLayoutArquivoAux;
    }

    public Integer getCdTipoLoteLayout() {
        return cdTipoLoteLayout;
    }

    public void setCdTipoLoteLayout(Integer cdTipoLoteLayoutAux) {
        this.cdTipoLoteLayout = cdTipoLoteLayoutAux;
    }

    public String getDsTipoLoteLayout() {
        return dsTipoLoteLayout;
    }

    public void setDsTipoLoteLayout(String dsTipoLoteLayoutAux) {
        this.dsTipoLoteLayout = dsTipoLoteLayoutAux;
    }

    public Integer getNrVersaoLoteLayout() {
        return nrVersaoLoteLayout;
    }

    public void setNrVersaoLoteLayout(Integer nrVersaoLoteLayoutAux) {
        this.nrVersaoLoteLayout = nrVersaoLoteLayoutAux;
    }

    public Integer getCdTipoServicoCnab() {
        return cdTipoServicoCnab;
    }

    public void setCdTipoServicoCnab(Integer cdTipoServicoCnabAux) {
        this.cdTipoServicoCnab = cdTipoServicoCnabAux;
    }

    public String getDsTipoServicoCnab() {
        return dsTipoServicoCnab;
    }

    public void setDsTipoServicoCnab(String dsTipoServicoCnabAux) {
        this.dsTipoServicoCnab = dsTipoServicoCnabAux;
    }

    public String getCdUsuarioInclusao() {
        return cdUsuarioInclusao;
    }

    public void setCdUsuarioInclusao(String cdUsuarioInclusaoAux) {
        this.cdUsuarioInclusao = cdUsuarioInclusaoAux;
    }

    public String getCdUsuarioInclusaoExterno() {
        return cdUsuarioInclusaoExterno;
    }

    public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExternoAux) {
        this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExternoAux;
    }

    public String getHrInclusaoRegistro() {
        return hrInclusaoRegistro;
    }

    public void setHrInclusaoRegistro(String hrInclusaoRegistroAux) {
        this.hrInclusaoRegistro = hrInclusaoRegistroAux;
    }

    public String getCdOperacaoCanalInclusao() {
        return cdOperacaoCanalInclusao;
    }

    public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusaoAux) {
        this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusaoAux;
    }

    public Integer getCdTipoCanalInclusao() {
        return cdTipoCanalInclusao;
    }

    public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusaoAux) {
        this.cdTipoCanalInclusao = cdTipoCanalInclusaoAux;
    }

    public String getCdUsuarioManutencao() {
        return cdUsuarioManutencao;
    }

    public void setCdUsuarioManutencao(String cdUsuarioManutencaoAux) {
        this.cdUsuarioManutencao = cdUsuarioManutencaoAux;
    }

    public String getCdUsuarioManutencaoExterno() {
        return cdUsuarioManutencaoExterno;
    }

    public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExternoAux) {
        this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExternoAux;
    }

    public String getHrManutencaoRegistro() {
        return hrManutencaoRegistro;
    }

    public void setHrManutencaoRegistro(String hrManutencaoRegistroAux) {
        this.hrManutencaoRegistro = hrManutencaoRegistroAux;
    }

    public String getCdOperacaoCanalManutencao() {
        return cdOperacaoCanalManutencao;
    }

    public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencaoAux) {
        this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencaoAux;
    }

    public Integer getCdTipoCanalManutencao() {
        return cdTipoCanalManutencao;
    }

    public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencaoAux) {
        this.cdTipoCanalManutencao = cdTipoCanalManutencaoAux;
    }

}
