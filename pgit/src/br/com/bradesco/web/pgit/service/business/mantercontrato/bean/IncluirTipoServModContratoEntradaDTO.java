/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;

/**
 * Nome: IncluirTipoServModContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirTipoServModContratoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;
	
	/** Atributo nrQtdeServicos. */
	private Integer nrQtdeServicos;
	
	/** Atributo nrQtdeModalidades. */
	private Integer nrQtdeModalidades;
	
	/** Atributo listaServicos. */
	private List<Integer> listaServicos;
	
	/** Atributo listaModalidade. */
	private List<ListarServicosSaidaDTO> listaModalidade;
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: nrQtdeServicos.
	 *
	 * @return nrQtdeServicos
	 */
	public Integer getNrQtdeServicos() {
		return nrQtdeServicos;
	}
	
	/**
	 * Set: nrQtdeServicos.
	 *
	 * @param nrQtdeServicos the nr qtde servicos
	 */
	public void setNrQtdeServicos(Integer nrQtdeServicos) {
		this.nrQtdeServicos = nrQtdeServicos;
	}
	
	/**
	 * Get: nrQtdeModalidades.
	 *
	 * @return nrQtdeModalidades
	 */
	public Integer getNrQtdeModalidades() {
		return nrQtdeModalidades;
	}
	
	/**
	 * Set: nrQtdeModalidades.
	 *
	 * @param nrQtdeModalidades the nr qtde modalidades
	 */
	public void setNrQtdeModalidades(Integer nrQtdeModalidades) {
		this.nrQtdeModalidades = nrQtdeModalidades;
	}
	
	/**
	 * Get: listaServicos.
	 *
	 * @return listaServicos
	 */
	public List<Integer> getListaServicos() {
		return listaServicos;
	}
	
	/**
	 * Set: listaServicos.
	 *
	 * @param listaServicos the lista servicos
	 */
	public void setListaServicos(List<Integer> listaServicos) {
		this.listaServicos = listaServicos;
	}
	
	/**
	 * Get: listaModalidade.
	 *
	 * @return listaModalidade
	 */
	public List<ListarServicosSaidaDTO> getListaModalidade() {
		return listaModalidade;
	}
	
	/**
	 * Set: listaModalidade.
	 *
	 * @param listaModalidade the lista modalidade
	 */
	public void setListaModalidade(
			List<ListarServicosSaidaDTO> listaModalidade) {
		this.listaModalidade = listaModalidade;
	}
	
	
	
	
	
     
	

}
