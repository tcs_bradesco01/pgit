package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;


/**
 * Arquivo criado em 30/12/15.
 */
public class ListarContratoMaeEntradaDTO {

    private Integer maxOcorrencias;

    private Long cdPessoaJuridicaMae;

    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    public void setCdPessoaJuridicaMae(Long cdPessoaJuridicaMae) {
        this.cdPessoaJuridicaMae = cdPessoaJuridicaMae;
    }

    public Long getCdPessoaJuridicaMae() {
        return this.cdPessoaJuridicaMae;
    }
}