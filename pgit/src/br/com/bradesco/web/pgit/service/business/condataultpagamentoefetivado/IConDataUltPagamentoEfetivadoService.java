/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean.ListarUltimaDataPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean.ListarUltimaDataPagtoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ConDataUltPagamentoEfetivado
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConDataUltPagamentoEfetivadoService {

   /**
    * Listar ultima data pagto.
    *
    * @param entrada the entrada
    * @return the list< listar ultima data pagto saida dt o>
    */
   List<ListarUltimaDataPagtoSaidaDTO> listarUltimaDataPagto (ListarUltimaDataPagtoEntradaDTO entrada);
}

