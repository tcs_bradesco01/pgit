/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.bean;

/**
 * Nome: ArquivoRecebidoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ArquivoRecebidoDTO {

	/** Atributo cpf_cliente. */
	private String cpf_cliente;

	/** Atributo cpf_filial. */
	private String cpf_filial;

	/** Atributo cpf_controle. */
	private String cpf_controle;

	/** Atributo cpf_completo. */
	private String cpf_completo;
	
	/** Atributo codSistemaLegado. */
	private String codSistemaLegado;
	
	/** Atributo nome. */
	private String nome;

	/** Atributo perfil. */
	private String perfil;

	/** Atributo produto. */
	private String produto;

	/** Atributo remessa. */
	private String remessa;

	/** Atributo numeroArqRemessa. */
	private String numeroArqRemessa;

	/** Atributo dataRecepcao. */
	private String dataRecepcao;

	/** Atributo dataRecepcaoInclusao. */
	private String dataRecepcaoInclusao;

	/** Atributo dataRecepcaoDe. */
	private String dataRecepcaoDe;

	/** Atributo dataRecepcaoAte. */
	private String dataRecepcaoAte;

	/** Atributo processamento. */
	private String processamento;

	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;

	/** Atributo codigoResultadoProcessamento. */
	private String codigoResultadoProcessamento;

	/** Atributo qtdRegistros. */
	private String qtdRegistros;

	/** Atributo valorTotal. */
	private String valorTotal;

	/** Atributo totalRegistros. */
	private int totalRegistros;

	/** Atributo banco. */
	private String banco;

	/** Atributo agencia. */
	private String agencia;

	/** Atributo razao. */
	private String razao;

	/** Atributo conta. */
	private String conta;

	/** Atributo codigoLancamento. */
	private String codigoLancamento;

	/** Atributo canalTransmissao. */
	private String canalTransmissao;

	/** Atributo descricaoCanalTransmissao. */
	private String descricaoCanalTransmissao;

	/** Atributo ambienteProcessamento. */
	private String ambienteProcessamento;

	/** Atributo descricaoAmbienteProcessamento. */
	private String descricaoAmbienteProcessamento;

	/** Atributo layout. */
	private String layout;

	/** Atributo descricaoLayout. */
	private String descricaoLayout;

	/** Atributo situacaoProcessamento. */
	private String situacaoProcessamento;

	/** Atributo resultProcessamento. */
	private String resultProcessamento;

	/** Atributo habilitaBtnOcorrencia. */
	private String habilitaBtnOcorrencia;

	/** Atributo habilitaBtnLote. */
	private String habilitaBtnLote;

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: ambienteProcessamento.
	 *
	 * @return ambienteProcessamento
	 */
	public String getAmbienteProcessamento() {
		return ambienteProcessamento;
	}

	/**
	 * Set: ambienteProcessamento.
	 *
	 * @param ambienteProcessamento the ambiente processamento
	 */
	public void setAmbienteProcessamento(String ambienteProcessamento) {
		this.ambienteProcessamento = ambienteProcessamento;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: canalTransmissao.
	 *
	 * @return canalTransmissao
	 */
	public String getCanalTransmissao() {
		return canalTransmissao;
	}

	/**
	 * Set: canalTransmissao.
	 *
	 * @param canalTransmissao the canal transmissao
	 */
	public void setCanalTransmissao(String canalTransmissao) {
		this.canalTransmissao = canalTransmissao;
	}

	/**
	 * Get: codigoLancamento.
	 *
	 * @return codigoLancamento
	 */
	public String getCodigoLancamento() {
		return codigoLancamento;
	}

	/**
	 * Set: codigoLancamento.
	 *
	 * @param codigoLancamento the codigo lancamento
	 */
	public void setCodigoLancamento(String codigoLancamento) {
		this.codigoLancamento = codigoLancamento;
	}

	/**
	 * Get: codigoResultadoProcessamento.
	 *
	 * @return codigoResultadoProcessamento
	 */
	public String getCodigoResultadoProcessamento() {
		return codigoResultadoProcessamento;
	}

	/**
	 * Set: codigoResultadoProcessamento.
	 *
	 * @param codigoResultadoProcessamento the codigo resultado processamento
	 */
	public void setCodigoResultadoProcessamento(String codigoResultadoProcessamento) {
		this.codigoResultadoProcessamento = codigoResultadoProcessamento;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: cpf_cliente.
	 *
	 * @return cpf_cliente
	 */
	public String getCpf_cliente() {
		return cpf_cliente;
	}

	/**
	 * Set: cpf_cliente.
	 *
	 * @param cpfCliente the cpf_cliente
	 */
	public void setCpf_cliente(String cpfCliente) {
		this.cpf_cliente = cpfCliente;
	}

	/**
	 * Get: cpf_completo.
	 *
	 * @return cpf_completo
	 */
	public String getCpf_completo() {
		return cpf_completo;
	}

	/**
	 * Set: cpf_completo.
	 *
	 * @param cpfCompleto the cpf_completo
	 */
	public void setCpf_completo(String cpfCompleto) {
		this.cpf_completo = cpfCompleto;
	}

	/**
	 * Get: cpf_controle.
	 *
	 * @return cpf_controle
	 */
	public String getCpf_controle() {
		return cpf_controle;
	}

	/**
	 * Set: cpf_controle.
	 *
	 * @param cpfControle the cpf_controle
	 */
	public void setCpf_controle(String cpfControle) {
		this.cpf_controle = cpfControle;
	}

	/**
	 * Get: cpf_filial.
	 *
	 * @return cpf_filial
	 */
	public String getCpf_filial() {
		return cpf_filial;
	}

	/**
	 * Set: cpf_filial.
	 *
	 * @param cpfFilial the cpf_filial
	 */
	public void setCpf_filial(String cpfFilial) {
		this.cpf_filial = cpfFilial;
	}

	/**
	 * Get: dataRecepcao.
	 *
	 * @return dataRecepcao
	 */
	public String getDataRecepcao() {
		return dataRecepcao;
	}

	/**
	 * Set: dataRecepcao.
	 *
	 * @param dataRecepcao the data recepcao
	 */
	public void setDataRecepcao(String dataRecepcao) {
		this.dataRecepcao = dataRecepcao;
	}

	/**
	 * Get: dataRecepcaoAte.
	 *
	 * @return dataRecepcaoAte
	 */
	public String getDataRecepcaoAte() {
		return dataRecepcaoAte;
	}

	/**
	 * Set: dataRecepcaoAte.
	 *
	 * @param dataRecepcaoAte the data recepcao ate
	 */
	public void setDataRecepcaoAte(String dataRecepcaoAte) {
		this.dataRecepcaoAte = dataRecepcaoAte;
	}

	/**
	 * Get: dataRecepcaoDe.
	 *
	 * @return dataRecepcaoDe
	 */
	public String getDataRecepcaoDe() {
		return dataRecepcaoDe;
	}

	/**
	 * Set: dataRecepcaoDe.
	 *
	 * @param dataRecepcaoDe the data recepcao de
	 */
	public void setDataRecepcaoDe(String dataRecepcaoDe) {
		this.dataRecepcaoDe = dataRecepcaoDe;
	}

	/**
	 * Get: dataRecepcaoInclusao.
	 *
	 * @return dataRecepcaoInclusao
	 */
	public String getDataRecepcaoInclusao() {
		return dataRecepcaoInclusao;
	}

	/**
	 * Set: dataRecepcaoInclusao.
	 *
	 * @param dataRecepcaoInclusao the data recepcao inclusao
	 */
	public void setDataRecepcaoInclusao(String dataRecepcaoInclusao) {
		this.dataRecepcaoInclusao = dataRecepcaoInclusao;
	}

	/**
	 * Get: descricaoAmbienteProcessamento.
	 *
	 * @return descricaoAmbienteProcessamento
	 */
	public String getDescricaoAmbienteProcessamento() {
		return descricaoAmbienteProcessamento;
	}

	/**
	 * Set: descricaoAmbienteProcessamento.
	 *
	 * @param descricaoAmbienteProcessamento the descricao ambiente processamento
	 */
	public void setDescricaoAmbienteProcessamento(String descricaoAmbienteProcessamento) {
		this.descricaoAmbienteProcessamento = descricaoAmbienteProcessamento;
	}

	/**
	 * Get: descricaoCanalTransmissao.
	 *
	 * @return descricaoCanalTransmissao
	 */
	public String getDescricaoCanalTransmissao() {
		return descricaoCanalTransmissao;
	}

	/**
	 * Set: descricaoCanalTransmissao.
	 *
	 * @param descricaoCanalTransmissao the descricao canal transmissao
	 */
	public void setDescricaoCanalTransmissao(String descricaoCanalTransmissao) {
		this.descricaoCanalTransmissao = descricaoCanalTransmissao;
	}

	/**
	 * Get: descricaoLayout.
	 *
	 * @return descricaoLayout
	 */
	public String getDescricaoLayout() {
		return descricaoLayout;
	}

	/**
	 * Set: descricaoLayout.
	 *
	 * @param descricaoLayout the descricao layout
	 */
	public void setDescricaoLayout(String descricaoLayout) {
		this.descricaoLayout = descricaoLayout;
	}

	/**
	 * Get: habilitaBtnLote.
	 *
	 * @return habilitaBtnLote
	 */
	public String getHabilitaBtnLote() {
		return habilitaBtnLote;
	}

	/**
	 * Set: habilitaBtnLote.
	 *
	 * @param habilitaBtnLote the habilita btn lote
	 */
	public void setHabilitaBtnLote(String habilitaBtnLote) {
		this.habilitaBtnLote = habilitaBtnLote;
	}

	/**
	 * Get: habilitaBtnOcorrencia.
	 *
	 * @return habilitaBtnOcorrencia
	 */
	public String getHabilitaBtnOcorrencia() {
		return habilitaBtnOcorrencia;
	}

	/**
	 * Set: habilitaBtnOcorrencia.
	 *
	 * @param habilitaBtnOcorrencia the habilita btn ocorrencia
	 */
	public void setHabilitaBtnOcorrencia(String habilitaBtnOcorrencia) {
		this.habilitaBtnOcorrencia = habilitaBtnOcorrencia;
	}

	/**
	 * Get: layout.
	 *
	 * @return layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Set: layout.
	 *
	 * @param layout the layout
	 */
	public void setLayout(String layout) {
		this.layout = layout;
	}

	/**
	 * Get: nome.
	 *
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Set: nome.
	 *
	 * @param nome the nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Get: numeroArqRemessa.
	 *
	 * @return numeroArqRemessa
	 */
	public String getNumeroArqRemessa() {
		return numeroArqRemessa;
	}

	/**
	 * Set: numeroArqRemessa.
	 *
	 * @param numeroArqRemessa the numero arq remessa
	 */
	public void setNumeroArqRemessa(String numeroArqRemessa) {
		this.numeroArqRemessa = numeroArqRemessa;
	}

	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public String getPerfil() {
		return perfil;
	}

	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	/**
	 * Get: processamento.
	 *
	 * @return processamento
	 */
	public String getProcessamento() {
		return processamento;
	}

	/**
	 * Set: processamento.
	 *
	 * @param processamento the processamento
	 */
	public void setProcessamento(String processamento) {
		this.processamento = processamento;
	}

	/**
	 * Get: produto.
	 *
	 * @return produto
	 */
	public String getProduto() {
		return produto;
	}

	/**
	 * Set: produto.
	 *
	 * @param produto the produto
	 */
	public void setProduto(String produto) {
		this.produto = produto;
	}

	/**
	 * Get: qtdRegistros.
	 *
	 * @return qtdRegistros
	 */
	public String getQtdRegistros() {
		return qtdRegistros;
	}

	/**
	 * Set: qtdRegistros.
	 *
	 * @param qtdRegistros the qtd registros
	 */
	public void setQtdRegistros(String qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	/**
	 * Get: razao.
	 *
	 * @return razao
	 */
	public String getRazao() {
		return razao;
	}

	/**
	 * Set: razao.
	 *
	 * @param razao the razao
	 */
	public void setRazao(String razao) {
		this.razao = razao;
	}

	/**
	 * Get: remessa.
	 *
	 * @return remessa
	 */
	public String getRemessa() {
		return remessa;
	}

	/**
	 * Set: remessa.
	 *
	 * @param remessa the remessa
	 */
	public void setRemessa(String remessa) {
		this.remessa = remessa;
	}

	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}

	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}

	/**
	 * Get: resultProcessamento.
	 *
	 * @return resultProcessamento
	 */
	public String getResultProcessamento() {
		return resultProcessamento;
	}

	/**
	 * Set: resultProcessamento.
	 *
	 * @param resultProcessamento the result processamento
	 */
	public void setResultProcessamento(String resultProcessamento) {
		this.resultProcessamento = resultProcessamento;
	}

	/**
	 * Get: situacaoProcessamento.
	 *
	 * @return situacaoProcessamento
	 */
	public String getSituacaoProcessamento() {
		return situacaoProcessamento;
	}

	/**
	 * Set: situacaoProcessamento.
	 *
	 * @param situacaoProcessamento the situacao processamento
	 */
	public void setSituacaoProcessamento(String situacaoProcessamento) {
		this.situacaoProcessamento = situacaoProcessamento;
	}

	/**
	 * Get: totalRegistros.
	 *
	 * @return totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}

	/**
	 * Set: totalRegistros.
	 *
	 * @param totalRegistros the total registros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	/**
	 * Get: valorTotal.
	 *
	 * @return valorTotal
	 */
	public String getValorTotal() {
		return valorTotal;
	}

	/**
	 * Set: valorTotal.
	 *
	 * @param valorTotal the valor total
	 */
	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}

	/**
	 * Get: codSistemaLegado.
	 *
	 * @return codSistemaLegado
	 */
	public String getCodSistemaLegado() {
		return codSistemaLegado;
	}

	/**
	 * Set: codSistemaLegado.
	 *
	 * @param codSistemaLegado the cod sistema legado
	 */
	public void setCodSistemaLegado(String codSistemaLegado) {
		this.codSistemaLegado = codSistemaLegado;
	}
	
	
	

}
