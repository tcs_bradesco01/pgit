/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class VerificarAtributosServicoModalidadeResponse.
 * 
 * @version $Revision$ $Date$
 */
public class VerificarAtributosServicoModalidadeResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdIndicadorAcaoNaoVida
     */
    private java.lang.String _cdIndicadorAcaoNaoVida;

    /**
     * Field _cdIndicadorAcertoDadoRecadastro
     */
    private java.lang.String _cdIndicadorAcertoDadoRecadastro;

    /**
     * Field _cdIndicadorAgendaDebitoVeiculo
     */
    private java.lang.String _cdIndicadorAgendaDebitoVeiculo;

    /**
     * Field _cdIndicadorAgendaPagamentoVencido
     */
    private java.lang.String _cdIndicadorAgendaPagamentoVencido;

    /**
     * Field _cdIndicadorAgendaValorMenor
     */
    private java.lang.String _cdIndicadorAgendaValorMenor;

    /**
     * Field _cdIndicadorAgrupamentoAviso
     */
    private java.lang.String _cdIndicadorAgrupamentoAviso;

    /**
     * Field _cdIndicadorAgrupamentoComprovado
     */
    private java.lang.String _cdIndicadorAgrupamentoComprovado;

    /**
     * Field _cdIndicadorAgrupamentoFormularioRecadastro
     */
    private java.lang.String _cdIndicadorAgrupamentoFormularioRecadastro;

    /**
     * Field _cdIndicadorAntecRecadastroBeneficio
     */
    private java.lang.String _cdIndicadorAntecRecadastroBeneficio;

    /**
     * Field _cdIndicadorAreaReservada
     */
    private java.lang.String _cdIndicadorAreaReservada;

    /**
     * Field _cdIndicadorBaseRecadastroBeneficio
     */
    private java.lang.String _cdIndicadorBaseRecadastroBeneficio;

    /**
     * Field _cdIndicadorBloqueioEmissaoPplta
     */
    private java.lang.String _cdIndicadorBloqueioEmissaoPplta;

    /**
     * Field _cdIndicadorCapituloTituloRegistro
     */
    private java.lang.String _cdIndicadorCapituloTituloRegistro;

    /**
     * Field _cdIndicadorCobrancaTarifa
     */
    private java.lang.String _cdIndicadorCobrancaTarifa;

    /**
     * Field _cdIndicadorConsDebitoVeiculo
     */
    private java.lang.String _cdIndicadorConsDebitoVeiculo;

    /**
     * Field _cdIndicadorConsEndereco
     */
    private java.lang.String _cdIndicadorConsEndereco;

    /**
     * Field _cdIndicadorConsSaldoPagamento
     */
    private java.lang.String _cdIndicadorConsSaldoPagamento;

    /**
     * Field _cdIndicadorContagemConsSaudo
     */
    private java.lang.String _cdIndicadorContagemConsSaudo;

    /**
     * Field _cdIndicadorCreditoNaoUtilizado
     */
    private java.lang.String _cdIndicadorCreditoNaoUtilizado;

    /**
     * Field _cdIndicadorCriterioEnquaBeneficio
     */
    private java.lang.String _cdIndicadorCriterioEnquaBeneficio;

    /**
     * Field _cdIndicadorCriterioEnqua
     */
    private java.lang.String _cdIndicadorCriterioEnqua;

    /**
     * Field _cdIndicadorCriterioRastreabilidadeTitulo
     */
    private java.lang.String _cdIndicadorCriterioRastreabilidadeTitulo;

    /**
     * Field _cdIndicadorCtciaEspecieBeneficio
     */
    private java.lang.String _cdIndicadorCtciaEspecieBeneficio;

    /**
     * Field _cdIndicadorCtciaIdentificacaoBeneficio
     */
    private java.lang.String _cdIndicadorCtciaIdentificacaoBeneficio;

    /**
     * Field _cdIndicadorCtciaInscricaoFavorecido
     */
    private java.lang.String _cdIndicadorCtciaInscricaoFavorecido;

    /**
     * Field _cdIndicadorCtciaProprietarioVeiculo
     */
    private java.lang.String _cdIndicadorCtciaProprietarioVeiculo;

    /**
     * Field _cdIndicadorDispzContaCredito
     */
    private java.lang.String _cdIndicadorDispzContaCredito;

    /**
     * Field _cdIndicadorDispzDiversar
     */
    private java.lang.String _cdIndicadorDispzDiversar;

    /**
     * Field _cdIndicadorDispzDiversasNao
     */
    private java.lang.String _cdIndicadorDispzDiversasNao;

    /**
     * Field _cdIndicadorDispzSalario
     */
    private java.lang.String _cdIndicadorDispzSalario;

    /**
     * Field _cdIndicadorDispzSalarioNao
     */
    private java.lang.String _cdIndicadorDispzSalarioNao;

    /**
     * Field _cdIndicadorDestinoAviso
     */
    private java.lang.String _cdIndicadorDestinoAviso;

    /**
     * Field _cdIndicadorDestinoComprovante
     */
    private java.lang.String _cdIndicadorDestinoComprovante;

    /**
     * Field _cdIndicadorDestinoFormularioRecadastro
     */
    private java.lang.String _cdIndicadorDestinoFormularioRecadastro;

    /**
     * Field _cdIndicadorEnvelopeAberto
     */
    private java.lang.String _cdIndicadorEnvelopeAberto;

    /**
     * Field _cdIndicadorFavorecidoConsPagamento
     */
    private java.lang.String _cdIndicadorFavorecidoConsPagamento;

    /**
     * Field _cdIndicadorFormaAutorizacaoPagamento
     */
    private java.lang.String _cdIndicadorFormaAutorizacaoPagamento;

    /**
     * Field _cdIndicadorFormaEnvioPagamento
     */
    private java.lang.String _cdIndicadorFormaEnvioPagamento;

    /**
     * Field _cdIndicadorFormaEstornoPagamento
     */
    private java.lang.String _cdIndicadorFormaEstornoPagamento;

    /**
     * Field _cdIndicadorFormaExpiracaoCredito
     */
    private java.lang.String _cdIndicadorFormaExpiracaoCredito;

    /**
     * Field _cdIndicadorFormaManutencao
     */
    private java.lang.String _cdIndicadorFormaManutencao;

    /**
     * Field _cdIndicadorFrasePreCadastro
     */
    private java.lang.String _cdIndicadorFrasePreCadastro;

    /**
     * Field _cdIndicadorAgendaTitulo
     */
    private java.lang.String _cdIndicadorAgendaTitulo;

    /**
     * Field _cdIndicadorAutorizacaoCliente
     */
    private java.lang.String _cdIndicadorAutorizacaoCliente;

    /**
     * Field _cdIndicadorAutorizacaoComplemento
     */
    private java.lang.String _cdIndicadorAutorizacaoComplemento;

    /**
     * Field _cdIndicadorCadastroOrg
     */
    private java.lang.String _cdIndicadorCadastroOrg;

    /**
     * Field _cdIndicadorCadastroProcd
     */
    private java.lang.String _cdIndicadorCadastroProcd;

    /**
     * Field _cdIndicadorCataoSalario
     */
    private java.lang.String _cdIndicadorCataoSalario;

    /**
     * Field _cdIndicadorEconomicoReajuste
     */
    private java.lang.String _cdIndicadorEconomicoReajuste;

    /**
     * Field _cdIndicadorExpiracaoCredito
     */
    private java.lang.String _cdIndicadorExpiracaoCredito;

    /**
     * Field _cdIndicadorLancamentoPagamento
     */
    private java.lang.String _cdIndicadorLancamentoPagamento;

    /**
     * Field _cdIndicadorMensagemPerso
     */
    private java.lang.String _cdIndicadorMensagemPerso;

    /**
     * Field _cdIndicadorLancamentoFuturoCredito
     */
    private java.lang.String _cdIndicadorLancamentoFuturoCredito;

    /**
     * Field _cdIndicadorLancamentoFuturoDebito
     */
    private java.lang.String _cdIndicadorLancamentoFuturoDebito;

    /**
     * Field _cdIndicadorLiberacaoLoteProcesso
     */
    private java.lang.String _cdIndicadorLiberacaoLoteProcesso;

    /**
     * Field _cdIndicadorManutencaoBaseRecadastro
     */
    private java.lang.String _cdIndicadorManutencaoBaseRecadastro;

    /**
     * Field _cdIndicadorMidiaDisponivel
     */
    private java.lang.String _cdIndicadorMidiaDisponivel;

    /**
     * Field _cdIndicadorMidiaMensagemRecadastro
     */
    private java.lang.String _cdIndicadorMidiaMensagemRecadastro;

    /**
     * Field _cdIndicadorMomentoAvisoRacadastro
     */
    private java.lang.String _cdIndicadorMomentoAvisoRacadastro;

    /**
     * Field _cdIndicadorMomentoCreditoEfetivacao
     */
    private java.lang.String _cdIndicadorMomentoCreditoEfetivacao;

    /**
     * Field _cdIndicadorMomentoDebitoPagamento
     */
    private java.lang.String _cdIndicadorMomentoDebitoPagamento;

    /**
     * Field _cdIndicadorMomentoFormularioRecadastro
     */
    private java.lang.String _cdIndicadorMomentoFormularioRecadastro;

    /**
     * Field _cdIndicadorMomentoProcessamentoPagamento
     */
    private java.lang.String _cdIndicadorMomentoProcessamentoPagamento;

    /**
     * Field _cdIndicadorMensagemRecadastroMidia
     */
    private java.lang.String _cdIndicadorMensagemRecadastroMidia;

    /**
     * Field _cdIndicadorPermissaoDebitoOnline
     */
    private java.lang.String _cdIndicadorPermissaoDebitoOnline;

    /**
     * Field _cdIndicadorNaturezaOperacaoPagamento
     */
    private java.lang.String _cdIndicadorNaturezaOperacaoPagamento;

    /**
     * Field _cdIndicadorPeriodicidadeAviso
     */
    private java.lang.String _cdIndicadorPeriodicidadeAviso;

    /**
     * Field _cdIndicadorPerdcCobrancaTarifa
     */
    private java.lang.String _cdIndicadorPerdcCobrancaTarifa;

    /**
     * Field _cdIndicadorPerdcComprovante
     */
    private java.lang.String _cdIndicadorPerdcComprovante;

    /**
     * Field _cdIndicadorPerdcConsultaVeiculo
     */
    private java.lang.String _cdIndicadorPerdcConsultaVeiculo;

    /**
     * Field _cdIndicadorPerdcEnvioRemessa
     */
    private java.lang.String _cdIndicadorPerdcEnvioRemessa;

    /**
     * Field _cdIndicadorPerdcManutencaoProcd
     */
    private java.lang.String _cdIndicadorPerdcManutencaoProcd;

    /**
     * Field _cdIndicadorPagamentoNaoUtilizado
     */
    private java.lang.String _cdIndicadorPagamentoNaoUtilizado;

    /**
     * Field _cdIndicadorPrincipalEnquaRecadastro
     */
    private java.lang.String _cdIndicadorPrincipalEnquaRecadastro;

    /**
     * Field _cdIndicadorPrioridadeEfetivacaoPagamento
     */
    private java.lang.String _cdIndicadorPrioridadeEfetivacaoPagamento;

    /**
     * Field _cdIndicadorRejeicaoAgendaLote
     */
    private java.lang.String _cdIndicadorRejeicaoAgendaLote;

    /**
     * Field _cdIndicadorRejeicaoEfetivacaoLote
     */
    private java.lang.String _cdIndicadorRejeicaoEfetivacaoLote;

    /**
     * Field _cdIndicadorRejeicaoLote
     */
    private java.lang.String _cdIndicadorRejeicaoLote;

    /**
     * Field _cdIndicadorRastreabilidadeNotaFiscal
     */
    private java.lang.String _cdIndicadorRastreabilidadeNotaFiscal;

    /**
     * Field _cdIndicadorRastreabilidadeTituloTerceiro
     */
    private java.lang.String _cdIndicadorRastreabilidadeTituloTerceiro;

    /**
     * Field _cdIndicadorTipoCargaRecadastro
     */
    private java.lang.String _cdIndicadorTipoCargaRecadastro;

    /**
     * Field _cdIndicadorTipoCataoSalario
     */
    private java.lang.String _cdIndicadorTipoCataoSalario;

    /**
     * Field _cdIndicadorTipoDataFloat
     */
    private java.lang.String _cdIndicadorTipoDataFloat;

    /**
     * Field _cdIndicadorTipoDivergenciaVeiculo
     */
    private java.lang.String _cdIndicadorTipoDivergenciaVeiculo;

    /**
     * Field _cdIndicadorTipoIdBeneficio
     */
    private java.lang.String _cdIndicadorTipoIdBeneficio;

    /**
     * Field _cdIndicadorTipoLayoutArquivo
     */
    private java.lang.String _cdIndicadorTipoLayoutArquivo;

    /**
     * Field _cdIndicadorTipoReajusteTarifa
     */
    private java.lang.String _cdIndicadorTipoReajusteTarifa;

    /**
     * Field _cdIndicadorContratoContaTransferencia
     */
    private java.lang.String _cdIndicadorContratoContaTransferencia;

    /**
     * Field _cdIndicadorUtilizacaoFavorecidoControle
     */
    private java.lang.String _cdIndicadorUtilizacaoFavorecidoControle;

    /**
     * Field _dtIndicadorEnquaContaSalario
     */
    private java.lang.String _dtIndicadorEnquaContaSalario;

    /**
     * Field _dtIndicadorInicioBloqueioPplta
     */
    private java.lang.String _dtIndicadorInicioBloqueioPplta;

    /**
     * Field _dtIndicadorInicioRastreabilidadeTitulo
     */
    private java.lang.String _dtIndicadorInicioRastreabilidadeTitulo;

    /**
     * Field _dtIndicadorFimAcertoRecadastro
     */
    private java.lang.String _dtIndicadorFimAcertoRecadastro;

    /**
     * Field _dtIndicadorFimRecadastroBeneficio
     */
    private java.lang.String _dtIndicadorFimRecadastroBeneficio;

    /**
     * Field _dtIndicadorinicioAcertoRecadastro
     */
    private java.lang.String _dtIndicadorinicioAcertoRecadastro;

    /**
     * Field _dtIndicadorInicioRecadastroBeneficio
     */
    private java.lang.String _dtIndicadorInicioRecadastroBeneficio;

    /**
     * Field _dtIndicadorLimiteVinculoCarga
     */
    private java.lang.String _dtIndicadorLimiteVinculoCarga;

    /**
     * Field _nrIndicadorFechamentoApuracaoTarifa
     */
    private java.lang.String _nrIndicadorFechamentoApuracaoTarifa;

    /**
     * Field _cdIndicadorPercentualIndicadorReajusteTarifa
     */
    private java.lang.String _cdIndicadorPercentualIndicadorReajusteTarifa;

    /**
     * Field _cdIndicadorPercentualMaximoInconLote
     */
    private java.lang.String _cdIndicadorPercentualMaximoInconLote;

    /**
     * Field _cdIndicadorPercentualReducaoTarifaCatalogo
     */
    private java.lang.String _cdIndicadorPercentualReducaoTarifaCatalogo;

    /**
     * Field _qtIndicadorAntecedencia
     */
    private java.lang.String _qtIndicadorAntecedencia;

    /**
     * Field _qtIndicadorAnteriorVencimentoComprovado
     */
    private java.lang.String _qtIndicadorAnteriorVencimentoComprovado;

    /**
     * Field _qtIndicadorDiaCobrancaTarifa
     */
    private java.lang.String _qtIndicadorDiaCobrancaTarifa;

    /**
     * Field _qtIndicadorDiaExpiracao
     */
    private java.lang.String _qtIndicadorDiaExpiracao;

    /**
     * Field _cdIndicadorDiaFloatPagamento
     */
    private java.lang.String _cdIndicadorDiaFloatPagamento;

    /**
     * Field _qtIndicadorDiaInatividadeFavorecido
     */
    private java.lang.String _qtIndicadorDiaInatividadeFavorecido;

    /**
     * Field _qtIndicadorDiaRepiqConsulta
     */
    private java.lang.String _qtIndicadorDiaRepiqConsulta;

    /**
     * Field _qtIndicadorEtapasRecadastroBeneficio
     */
    private java.lang.String _qtIndicadorEtapasRecadastroBeneficio;

    /**
     * Field _qtIndicadorFaseRecadastroBeneficio
     */
    private java.lang.String _qtIndicadorFaseRecadastroBeneficio;

    /**
     * Field _qtIndicadorLimiteLinha
     */
    private java.lang.String _qtIndicadorLimiteLinha;

    /**
     * Field _qtIndicadorLimiteSolicitacaoCatao
     */
    private java.lang.String _qtIndicadorLimiteSolicitacaoCatao;

    /**
     * Field _qtIndicadorMaximaInconLote
     */
    private java.lang.String _qtIndicadorMaximaInconLote;

    /**
     * Field _qtIndicadorMaximaTituloVencido
     */
    private java.lang.String _qtIndicadorMaximaTituloVencido;

    /**
     * Field _qtIndicadorMesComprovante
     */
    private java.lang.String _qtIndicadorMesComprovante;

    /**
     * Field _qtIndicadorMesEtapaRecadastro
     */
    private java.lang.String _qtIndicadorMesEtapaRecadastro;

    /**
     * Field _qtIndicadorMesFaseRecadastro
     */
    private java.lang.String _qtIndicadorMesFaseRecadastro;

    /**
     * Field _qtIndicadorMesReajusteTarifa
     */
    private java.lang.String _qtIndicadorMesReajusteTarifa;

    /**
     * Field _qtIndicadorViaAviso
     */
    private java.lang.String _qtIndicadorViaAviso;

    /**
     * Field _qtIndicadorViaCobranca
     */
    private java.lang.String _qtIndicadorViaCobranca;

    /**
     * Field _qtIndicadorViaComprovante
     */
    private java.lang.String _qtIndicadorViaComprovante;

    /**
     * Field _vlIndicadorFavorecidoNaoCadastro
     */
    private java.lang.String _vlIndicadorFavorecidoNaoCadastro;

    /**
     * Field _vlIndicadorLimiteDiaPagamento
     */
    private java.lang.String _vlIndicadorLimiteDiaPagamento;

    /**
     * Field _vlIndicadorLimiteIndividualPagamento
     */
    private java.lang.String _vlIndicadorLimiteIndividualPagamento;

    /**
     * Field _cdIndicadorEmissaoAviso
     */
    private java.lang.String _cdIndicadorEmissaoAviso;

    /**
     * Field _cdIndicadorRetornoInternet
     */
    private java.lang.String _cdIndicadorRetornoInternet;

    /**
     * Field _cdIndicadorListaDebito
     */
    private java.lang.String _cdIndicadorListaDebito;

    /**
     * Field _cdIndicadorTipoFormacaoLista
     */
    private java.lang.String _cdIndicadorTipoFormacaoLista;

    /**
     * Field _cdIndicadorTipoConsistenciaLista
     */
    private java.lang.String _cdIndicadorTipoConsistenciaLista;

    /**
     * Field _cdIndicadorTipoConsultaComprovante
     */
    private java.lang.String _cdIndicadorTipoConsultaComprovante;

    /**
     * Field _cdIndicadorValidacaoNomeFavorecido
     */
    private java.lang.String _cdIndicadorValidacaoNomeFavorecido;

    /**
     * Field _cdIndicadorTipoContaFavorecidoT
     */
    private java.lang.String _cdIndicadorTipoContaFavorecidoT;

    /**
     * Field _cdIndLancamentoPersonalizado
     */
    private java.lang.String _cdIndLancamentoPersonalizado;

    /**
     * Field _cdIndicadorAgendaRastreabilidadeFilial
     */
    private java.lang.String _cdIndicadorAgendaRastreabilidadeFilial;

    /**
     * Field _cdIndicadorAdesaoSacador
     */
    private java.lang.String _cdIndicadorAdesaoSacador;

    /**
     * Field _cdIndicadorMeioPagamentoCredito
     */
    private java.lang.String _cdIndicadorMeioPagamentoCredito;

    /**
     * Field _cdIndicadorTipoIsncricaoFavorecido
     */
    private java.lang.String _cdIndicadorTipoIsncricaoFavorecido;

    /**
     * Field _cdIndicadorBancoPostal
     */
    private java.lang.String _cdIndicadorBancoPostal;

    /**
     * Field _cdIndicadorFormularioContratoCliente
     */
    private java.lang.String _cdIndicadorFormularioContratoCliente;

    /**
     * Field _dsIndicadorAreaResrd
     */
    private java.lang.String _dsIndicadorAreaResrd;

    /**
     * Field _cdDependencia
     */
    private int _cdDependencia = 0;

    /**
     * keeps track of state for field: _cdDependencia
     */
    private boolean _has_cdDependencia;

    /**
     * Field _cdIndicadorConsultaSaldoSuperior
     */
    private java.lang.String _cdIndicadorConsultaSaldoSuperior;


      //----------------/
     //- Constructors -/
    //----------------/

    public VerificarAtributosServicoModalidadeResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.response.VerificarAtributosServicoModalidadeResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdDependencia
     * 
     */
    public void deleteCdDependencia()
    {
        this._has_cdDependencia= false;
    } //-- void deleteCdDependencia() 

    /**
     * Returns the value of field 'cdDependencia'.
     * 
     * @return int
     * @return the value of field 'cdDependencia'.
     */
    public int getCdDependencia()
    {
        return this._cdDependencia;
    } //-- int getCdDependencia() 

    /**
     * Returns the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @return String
     * @return the value of field 'cdIndLancamentoPersonalizado'.
     */
    public java.lang.String getCdIndLancamentoPersonalizado()
    {
        return this._cdIndLancamentoPersonalizado;
    } //-- java.lang.String getCdIndLancamentoPersonalizado() 

    /**
     * Returns the value of field 'cdIndicadorAcaoNaoVida'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAcaoNaoVida'.
     */
    public java.lang.String getCdIndicadorAcaoNaoVida()
    {
        return this._cdIndicadorAcaoNaoVida;
    } //-- java.lang.String getCdIndicadorAcaoNaoVida() 

    /**
     * Returns the value of field
     * 'cdIndicadorAcertoDadoRecadastro'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAcertoDadoRecadastro'.
     */
    public java.lang.String getCdIndicadorAcertoDadoRecadastro()
    {
        return this._cdIndicadorAcertoDadoRecadastro;
    } //-- java.lang.String getCdIndicadorAcertoDadoRecadastro() 

    /**
     * Returns the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAdesaoSacador'.
     */
    public java.lang.String getCdIndicadorAdesaoSacador()
    {
        return this._cdIndicadorAdesaoSacador;
    } //-- java.lang.String getCdIndicadorAdesaoSacador() 

    /**
     * Returns the value of field 'cdIndicadorAgendaDebitoVeiculo'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAgendaDebitoVeiculo'.
     */
    public java.lang.String getCdIndicadorAgendaDebitoVeiculo()
    {
        return this._cdIndicadorAgendaDebitoVeiculo;
    } //-- java.lang.String getCdIndicadorAgendaDebitoVeiculo() 

    /**
     * Returns the value of field
     * 'cdIndicadorAgendaPagamentoVencido'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorAgendaPagamentoVencido'.
     */
    public java.lang.String getCdIndicadorAgendaPagamentoVencido()
    {
        return this._cdIndicadorAgendaPagamentoVencido;
    } //-- java.lang.String getCdIndicadorAgendaPagamentoVencido() 

    /**
     * Returns the value of field
     * 'cdIndicadorAgendaRastreabilidadeFilial'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorAgendaRastreabilidadeFilial'.
     */
    public java.lang.String getCdIndicadorAgendaRastreabilidadeFilial()
    {
        return this._cdIndicadorAgendaRastreabilidadeFilial;
    } //-- java.lang.String getCdIndicadorAgendaRastreabilidadeFilial() 

    /**
     * Returns the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAgendaTitulo'.
     */
    public java.lang.String getCdIndicadorAgendaTitulo()
    {
        return this._cdIndicadorAgendaTitulo;
    } //-- java.lang.String getCdIndicadorAgendaTitulo() 

    /**
     * Returns the value of field 'cdIndicadorAgendaValorMenor'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAgendaValorMenor'.
     */
    public java.lang.String getCdIndicadorAgendaValorMenor()
    {
        return this._cdIndicadorAgendaValorMenor;
    } //-- java.lang.String getCdIndicadorAgendaValorMenor() 

    /**
     * Returns the value of field 'cdIndicadorAgrupamentoAviso'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAgrupamentoAviso'.
     */
    public java.lang.String getCdIndicadorAgrupamentoAviso()
    {
        return this._cdIndicadorAgrupamentoAviso;
    } //-- java.lang.String getCdIndicadorAgrupamentoAviso() 

    /**
     * Returns the value of field
     * 'cdIndicadorAgrupamentoComprovado'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAgrupamentoComprovado'
     */
    public java.lang.String getCdIndicadorAgrupamentoComprovado()
    {
        return this._cdIndicadorAgrupamentoComprovado;
    } //-- java.lang.String getCdIndicadorAgrupamentoComprovado() 

    /**
     * Returns the value of field
     * 'cdIndicadorAgrupamentoFormularioRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorAgrupamentoFormularioRecadastro'.
     */
    public java.lang.String getCdIndicadorAgrupamentoFormularioRecadastro()
    {
        return this._cdIndicadorAgrupamentoFormularioRecadastro;
    } //-- java.lang.String getCdIndicadorAgrupamentoFormularioRecadastro() 

    /**
     * Returns the value of field
     * 'cdIndicadorAntecRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorAntecRecadastroBeneficio'.
     */
    public java.lang.String getCdIndicadorAntecRecadastroBeneficio()
    {
        return this._cdIndicadorAntecRecadastroBeneficio;
    } //-- java.lang.String getCdIndicadorAntecRecadastroBeneficio() 

    /**
     * Returns the value of field 'cdIndicadorAreaReservada'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAreaReservada'.
     */
    public java.lang.String getCdIndicadorAreaReservada()
    {
        return this._cdIndicadorAreaReservada;
    } //-- java.lang.String getCdIndicadorAreaReservada() 

    /**
     * Returns the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorAutorizacaoCliente'.
     */
    public java.lang.String getCdIndicadorAutorizacaoCliente()
    {
        return this._cdIndicadorAutorizacaoCliente;
    } //-- java.lang.String getCdIndicadorAutorizacaoCliente() 

    /**
     * Returns the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public java.lang.String getCdIndicadorAutorizacaoComplemento()
    {
        return this._cdIndicadorAutorizacaoComplemento;
    } //-- java.lang.String getCdIndicadorAutorizacaoComplemento() 

    /**
     * Returns the value of field 'cdIndicadorBancoPostal'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorBancoPostal'.
     */
    public java.lang.String getCdIndicadorBancoPostal()
    {
        return this._cdIndicadorBancoPostal;
    } //-- java.lang.String getCdIndicadorBancoPostal() 

    /**
     * Returns the value of field
     * 'cdIndicadorBaseRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorBaseRecadastroBeneficio'.
     */
    public java.lang.String getCdIndicadorBaseRecadastroBeneficio()
    {
        return this._cdIndicadorBaseRecadastroBeneficio;
    } //-- java.lang.String getCdIndicadorBaseRecadastroBeneficio() 

    /**
     * Returns the value of field
     * 'cdIndicadorBloqueioEmissaoPplta'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorBloqueioEmissaoPplta'.
     */
    public java.lang.String getCdIndicadorBloqueioEmissaoPplta()
    {
        return this._cdIndicadorBloqueioEmissaoPplta;
    } //-- java.lang.String getCdIndicadorBloqueioEmissaoPplta() 

    /**
     * Returns the value of field 'cdIndicadorCadastroOrg'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorCadastroOrg'.
     */
    public java.lang.String getCdIndicadorCadastroOrg()
    {
        return this._cdIndicadorCadastroOrg;
    } //-- java.lang.String getCdIndicadorCadastroOrg() 

    /**
     * Returns the value of field 'cdIndicadorCadastroProcd'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorCadastroProcd'.
     */
    public java.lang.String getCdIndicadorCadastroProcd()
    {
        return this._cdIndicadorCadastroProcd;
    } //-- java.lang.String getCdIndicadorCadastroProcd() 

    /**
     * Returns the value of field
     * 'cdIndicadorCapituloTituloRegistro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorCapituloTituloRegistro'.
     */
    public java.lang.String getCdIndicadorCapituloTituloRegistro()
    {
        return this._cdIndicadorCapituloTituloRegistro;
    } //-- java.lang.String getCdIndicadorCapituloTituloRegistro() 

    /**
     * Returns the value of field 'cdIndicadorCataoSalario'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorCataoSalario'.
     */
    public java.lang.String getCdIndicadorCataoSalario()
    {
        return this._cdIndicadorCataoSalario;
    } //-- java.lang.String getCdIndicadorCataoSalario() 

    /**
     * Returns the value of field 'cdIndicadorCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorCobrancaTarifa'.
     */
    public java.lang.String getCdIndicadorCobrancaTarifa()
    {
        return this._cdIndicadorCobrancaTarifa;
    } //-- java.lang.String getCdIndicadorCobrancaTarifa() 

    /**
     * Returns the value of field 'cdIndicadorConsDebitoVeiculo'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorConsDebitoVeiculo'.
     */
    public java.lang.String getCdIndicadorConsDebitoVeiculo()
    {
        return this._cdIndicadorConsDebitoVeiculo;
    } //-- java.lang.String getCdIndicadorConsDebitoVeiculo() 

    /**
     * Returns the value of field 'cdIndicadorConsEndereco'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorConsEndereco'.
     */
    public java.lang.String getCdIndicadorConsEndereco()
    {
        return this._cdIndicadorConsEndereco;
    } //-- java.lang.String getCdIndicadorConsEndereco() 

    /**
     * Returns the value of field 'cdIndicadorConsSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorConsSaldoPagamento'.
     */
    public java.lang.String getCdIndicadorConsSaldoPagamento()
    {
        return this._cdIndicadorConsSaldoPagamento;
    } //-- java.lang.String getCdIndicadorConsSaldoPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorConsultaSaldoSuperior'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorConsultaSaldoSuperior'
     */
    public java.lang.String getCdIndicadorConsultaSaldoSuperior()
    {
        return this._cdIndicadorConsultaSaldoSuperior;
    } //-- java.lang.String getCdIndicadorConsultaSaldoSuperior() 

    /**
     * Returns the value of field 'cdIndicadorContagemConsSaudo'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorContagemConsSaudo'.
     */
    public java.lang.String getCdIndicadorContagemConsSaudo()
    {
        return this._cdIndicadorContagemConsSaudo;
    } //-- java.lang.String getCdIndicadorContagemConsSaudo() 

    /**
     * Returns the value of field
     * 'cdIndicadorContratoContaTransferencia'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorContratoContaTransferencia'.
     */
    public java.lang.String getCdIndicadorContratoContaTransferencia()
    {
        return this._cdIndicadorContratoContaTransferencia;
    } //-- java.lang.String getCdIndicadorContratoContaTransferencia() 

    /**
     * Returns the value of field 'cdIndicadorCreditoNaoUtilizado'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorCreditoNaoUtilizado'.
     */
    public java.lang.String getCdIndicadorCreditoNaoUtilizado()
    {
        return this._cdIndicadorCreditoNaoUtilizado;
    } //-- java.lang.String getCdIndicadorCreditoNaoUtilizado() 

    /**
     * Returns the value of field 'cdIndicadorCriterioEnqua'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorCriterioEnqua'.
     */
    public java.lang.String getCdIndicadorCriterioEnqua()
    {
        return this._cdIndicadorCriterioEnqua;
    } //-- java.lang.String getCdIndicadorCriterioEnqua() 

    /**
     * Returns the value of field
     * 'cdIndicadorCriterioEnquaBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorCriterioEnquaBeneficio'.
     */
    public java.lang.String getCdIndicadorCriterioEnquaBeneficio()
    {
        return this._cdIndicadorCriterioEnquaBeneficio;
    } //-- java.lang.String getCdIndicadorCriterioEnquaBeneficio() 

    /**
     * Returns the value of field
     * 'cdIndicadorCriterioRastreabilidadeTitulo'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorCriterioRastreabilidadeTitulo'.
     */
    public java.lang.String getCdIndicadorCriterioRastreabilidadeTitulo()
    {
        return this._cdIndicadorCriterioRastreabilidadeTitulo;
    } //-- java.lang.String getCdIndicadorCriterioRastreabilidadeTitulo() 

    /**
     * Returns the value of field
     * 'cdIndicadorCtciaEspecieBeneficio'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorCtciaEspecieBeneficio'
     */
    public java.lang.String getCdIndicadorCtciaEspecieBeneficio()
    {
        return this._cdIndicadorCtciaEspecieBeneficio;
    } //-- java.lang.String getCdIndicadorCtciaEspecieBeneficio() 

    /**
     * Returns the value of field
     * 'cdIndicadorCtciaIdentificacaoBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorCtciaIdentificacaoBeneficio'.
     */
    public java.lang.String getCdIndicadorCtciaIdentificacaoBeneficio()
    {
        return this._cdIndicadorCtciaIdentificacaoBeneficio;
    } //-- java.lang.String getCdIndicadorCtciaIdentificacaoBeneficio() 

    /**
     * Returns the value of field
     * 'cdIndicadorCtciaInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorCtciaInscricaoFavorecido'.
     */
    public java.lang.String getCdIndicadorCtciaInscricaoFavorecido()
    {
        return this._cdIndicadorCtciaInscricaoFavorecido;
    } //-- java.lang.String getCdIndicadorCtciaInscricaoFavorecido() 

    /**
     * Returns the value of field
     * 'cdIndicadorCtciaProprietarioVeiculo'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorCtciaProprietarioVeiculo'.
     */
    public java.lang.String getCdIndicadorCtciaProprietarioVeiculo()
    {
        return this._cdIndicadorCtciaProprietarioVeiculo;
    } //-- java.lang.String getCdIndicadorCtciaProprietarioVeiculo() 

    /**
     * Returns the value of field 'cdIndicadorDestinoAviso'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDestinoAviso'.
     */
    public java.lang.String getCdIndicadorDestinoAviso()
    {
        return this._cdIndicadorDestinoAviso;
    } //-- java.lang.String getCdIndicadorDestinoAviso() 

    /**
     * Returns the value of field 'cdIndicadorDestinoComprovante'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDestinoComprovante'.
     */
    public java.lang.String getCdIndicadorDestinoComprovante()
    {
        return this._cdIndicadorDestinoComprovante;
    } //-- java.lang.String getCdIndicadorDestinoComprovante() 

    /**
     * Returns the value of field
     * 'cdIndicadorDestinoFormularioRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorDestinoFormularioRecadastro'.
     */
    public java.lang.String getCdIndicadorDestinoFormularioRecadastro()
    {
        return this._cdIndicadorDestinoFormularioRecadastro;
    } //-- java.lang.String getCdIndicadorDestinoFormularioRecadastro() 

    /**
     * Returns the value of field 'cdIndicadorDiaFloatPagamento'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDiaFloatPagamento'.
     */
    public java.lang.String getCdIndicadorDiaFloatPagamento()
    {
        return this._cdIndicadorDiaFloatPagamento;
    } //-- java.lang.String getCdIndicadorDiaFloatPagamento() 

    /**
     * Returns the value of field 'cdIndicadorDispzContaCredito'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDispzContaCredito'.
     */
    public java.lang.String getCdIndicadorDispzContaCredito()
    {
        return this._cdIndicadorDispzContaCredito;
    } //-- java.lang.String getCdIndicadorDispzContaCredito() 

    /**
     * Returns the value of field 'cdIndicadorDispzDiversar'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDispzDiversar'.
     */
    public java.lang.String getCdIndicadorDispzDiversar()
    {
        return this._cdIndicadorDispzDiversar;
    } //-- java.lang.String getCdIndicadorDispzDiversar() 

    /**
     * Returns the value of field 'cdIndicadorDispzDiversasNao'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDispzDiversasNao'.
     */
    public java.lang.String getCdIndicadorDispzDiversasNao()
    {
        return this._cdIndicadorDispzDiversasNao;
    } //-- java.lang.String getCdIndicadorDispzDiversasNao() 

    /**
     * Returns the value of field 'cdIndicadorDispzSalario'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDispzSalario'.
     */
    public java.lang.String getCdIndicadorDispzSalario()
    {
        return this._cdIndicadorDispzSalario;
    } //-- java.lang.String getCdIndicadorDispzSalario() 

    /**
     * Returns the value of field 'cdIndicadorDispzSalarioNao'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorDispzSalarioNao'.
     */
    public java.lang.String getCdIndicadorDispzSalarioNao()
    {
        return this._cdIndicadorDispzSalarioNao;
    } //-- java.lang.String getCdIndicadorDispzSalarioNao() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorEconomicoReajuste'.
     */
    public java.lang.String getCdIndicadorEconomicoReajuste()
    {
        return this._cdIndicadorEconomicoReajuste;
    } //-- java.lang.String getCdIndicadorEconomicoReajuste() 

    /**
     * Returns the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorEmissaoAviso'.
     */
    public java.lang.String getCdIndicadorEmissaoAviso()
    {
        return this._cdIndicadorEmissaoAviso;
    } //-- java.lang.String getCdIndicadorEmissaoAviso() 

    /**
     * Returns the value of field 'cdIndicadorEnvelopeAberto'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorEnvelopeAberto'.
     */
    public java.lang.String getCdIndicadorEnvelopeAberto()
    {
        return this._cdIndicadorEnvelopeAberto;
    } //-- java.lang.String getCdIndicadorEnvelopeAberto() 

    /**
     * Returns the value of field 'cdIndicadorExpiracaoCredito'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorExpiracaoCredito'.
     */
    public java.lang.String getCdIndicadorExpiracaoCredito()
    {
        return this._cdIndicadorExpiracaoCredito;
    } //-- java.lang.String getCdIndicadorExpiracaoCredito() 

    /**
     * Returns the value of field
     * 'cdIndicadorFavorecidoConsPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorFavorecidoConsPagamento'.
     */
    public java.lang.String getCdIndicadorFavorecidoConsPagamento()
    {
        return this._cdIndicadorFavorecidoConsPagamento;
    } //-- java.lang.String getCdIndicadorFavorecidoConsPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorFormaAutorizacaoPagamento'.
     */
    public java.lang.String getCdIndicadorFormaAutorizacaoPagamento()
    {
        return this._cdIndicadorFormaAutorizacaoPagamento;
    } //-- java.lang.String getCdIndicadorFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdIndicadorFormaEnvioPagamento'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorFormaEnvioPagamento'.
     */
    public java.lang.String getCdIndicadorFormaEnvioPagamento()
    {
        return this._cdIndicadorFormaEnvioPagamento;
    } //-- java.lang.String getCdIndicadorFormaEnvioPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorFormaEstornoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorFormaEstornoPagamento'
     */
    public java.lang.String getCdIndicadorFormaEstornoPagamento()
    {
        return this._cdIndicadorFormaEstornoPagamento;
    } //-- java.lang.String getCdIndicadorFormaEstornoPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorFormaExpiracaoCredito'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorFormaExpiracaoCredito'
     */
    public java.lang.String getCdIndicadorFormaExpiracaoCredito()
    {
        return this._cdIndicadorFormaExpiracaoCredito;
    } //-- java.lang.String getCdIndicadorFormaExpiracaoCredito() 

    /**
     * Returns the value of field 'cdIndicadorFormaManutencao'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorFormaManutencao'.
     */
    public java.lang.String getCdIndicadorFormaManutencao()
    {
        return this._cdIndicadorFormaManutencao;
    } //-- java.lang.String getCdIndicadorFormaManutencao() 

    /**
     * Returns the value of field
     * 'cdIndicadorFormularioContratoCliente'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorFormularioContratoCliente'.
     */
    public java.lang.String getCdIndicadorFormularioContratoCliente()
    {
        return this._cdIndicadorFormularioContratoCliente;
    } //-- java.lang.String getCdIndicadorFormularioContratoCliente() 

    /**
     * Returns the value of field 'cdIndicadorFrasePreCadastro'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorFrasePreCadastro'.
     */
    public java.lang.String getCdIndicadorFrasePreCadastro()
    {
        return this._cdIndicadorFrasePreCadastro;
    } //-- java.lang.String getCdIndicadorFrasePreCadastro() 

    /**
     * Returns the value of field
     * 'cdIndicadorLancamentoFuturoCredito'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorLancamentoFuturoCredito'.
     */
    public java.lang.String getCdIndicadorLancamentoFuturoCredito()
    {
        return this._cdIndicadorLancamentoFuturoCredito;
    } //-- java.lang.String getCdIndicadorLancamentoFuturoCredito() 

    /**
     * Returns the value of field
     * 'cdIndicadorLancamentoFuturoDebito'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorLancamentoFuturoDebito'.
     */
    public java.lang.String getCdIndicadorLancamentoFuturoDebito()
    {
        return this._cdIndicadorLancamentoFuturoDebito;
    } //-- java.lang.String getCdIndicadorLancamentoFuturoDebito() 

    /**
     * Returns the value of field 'cdIndicadorLancamentoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorLancamentoPagamento'.
     */
    public java.lang.String getCdIndicadorLancamentoPagamento()
    {
        return this._cdIndicadorLancamentoPagamento;
    } //-- java.lang.String getCdIndicadorLancamentoPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorLiberacaoLoteProcesso'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorLiberacaoLoteProcesso'
     */
    public java.lang.String getCdIndicadorLiberacaoLoteProcesso()
    {
        return this._cdIndicadorLiberacaoLoteProcesso;
    } //-- java.lang.String getCdIndicadorLiberacaoLoteProcesso() 

    /**
     * Returns the value of field 'cdIndicadorListaDebito'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorListaDebito'.
     */
    public java.lang.String getCdIndicadorListaDebito()
    {
        return this._cdIndicadorListaDebito;
    } //-- java.lang.String getCdIndicadorListaDebito() 

    /**
     * Returns the value of field
     * 'cdIndicadorManutencaoBaseRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorManutencaoBaseRecadastro'.
     */
    public java.lang.String getCdIndicadorManutencaoBaseRecadastro()
    {
        return this._cdIndicadorManutencaoBaseRecadastro;
    } //-- java.lang.String getCdIndicadorManutencaoBaseRecadastro() 

    /**
     * Returns the value of field
     * 'cdIndicadorMeioPagamentoCredito'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorMeioPagamentoCredito'.
     */
    public java.lang.String getCdIndicadorMeioPagamentoCredito()
    {
        return this._cdIndicadorMeioPagamentoCredito;
    } //-- java.lang.String getCdIndicadorMeioPagamentoCredito() 

    /**
     * Returns the value of field 'cdIndicadorMensagemPerso'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorMensagemPerso'.
     */
    public java.lang.String getCdIndicadorMensagemPerso()
    {
        return this._cdIndicadorMensagemPerso;
    } //-- java.lang.String getCdIndicadorMensagemPerso() 

    /**
     * Returns the value of field
     * 'cdIndicadorMensagemRecadastroMidia'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorMensagemRecadastroMidia'.
     */
    public java.lang.String getCdIndicadorMensagemRecadastroMidia()
    {
        return this._cdIndicadorMensagemRecadastroMidia;
    } //-- java.lang.String getCdIndicadorMensagemRecadastroMidia() 

    /**
     * Returns the value of field 'cdIndicadorMidiaDisponivel'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorMidiaDisponivel'.
     */
    public java.lang.String getCdIndicadorMidiaDisponivel()
    {
        return this._cdIndicadorMidiaDisponivel;
    } //-- java.lang.String getCdIndicadorMidiaDisponivel() 

    /**
     * Returns the value of field
     * 'cdIndicadorMidiaMensagemRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorMidiaMensagemRecadastro'.
     */
    public java.lang.String getCdIndicadorMidiaMensagemRecadastro()
    {
        return this._cdIndicadorMidiaMensagemRecadastro;
    } //-- java.lang.String getCdIndicadorMidiaMensagemRecadastro() 

    /**
     * Returns the value of field
     * 'cdIndicadorMomentoAvisoRacadastro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorMomentoAvisoRacadastro'.
     */
    public java.lang.String getCdIndicadorMomentoAvisoRacadastro()
    {
        return this._cdIndicadorMomentoAvisoRacadastro;
    } //-- java.lang.String getCdIndicadorMomentoAvisoRacadastro() 

    /**
     * Returns the value of field
     * 'cdIndicadorMomentoCreditoEfetivacao'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorMomentoCreditoEfetivacao'.
     */
    public java.lang.String getCdIndicadorMomentoCreditoEfetivacao()
    {
        return this._cdIndicadorMomentoCreditoEfetivacao;
    } //-- java.lang.String getCdIndicadorMomentoCreditoEfetivacao() 

    /**
     * Returns the value of field
     * 'cdIndicadorMomentoDebitoPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorMomentoDebitoPagamento'.
     */
    public java.lang.String getCdIndicadorMomentoDebitoPagamento()
    {
        return this._cdIndicadorMomentoDebitoPagamento;
    } //-- java.lang.String getCdIndicadorMomentoDebitoPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorMomentoFormularioRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorMomentoFormularioRecadastro'.
     */
    public java.lang.String getCdIndicadorMomentoFormularioRecadastro()
    {
        return this._cdIndicadorMomentoFormularioRecadastro;
    } //-- java.lang.String getCdIndicadorMomentoFormularioRecadastro() 

    /**
     * Returns the value of field
     * 'cdIndicadorMomentoProcessamentoPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorMomentoProcessamentoPagamento'.
     */
    public java.lang.String getCdIndicadorMomentoProcessamentoPagamento()
    {
        return this._cdIndicadorMomentoProcessamentoPagamento;
    } //-- java.lang.String getCdIndicadorMomentoProcessamentoPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorNaturezaOperacaoPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorNaturezaOperacaoPagamento'.
     */
    public java.lang.String getCdIndicadorNaturezaOperacaoPagamento()
    {
        return this._cdIndicadorNaturezaOperacaoPagamento;
    } //-- java.lang.String getCdIndicadorNaturezaOperacaoPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorPagamentoNaoUtilizado'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPagamentoNaoUtilizado'
     */
    public java.lang.String getCdIndicadorPagamentoNaoUtilizado()
    {
        return this._cdIndicadorPagamentoNaoUtilizado;
    } //-- java.lang.String getCdIndicadorPagamentoNaoUtilizado() 

    /**
     * Returns the value of field
     * 'cdIndicadorPercentualIndicadorReajusteTarifa'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorPercentualIndicadorReajusteTarifa'.
     */
    public java.lang.String getCdIndicadorPercentualIndicadorReajusteTarifa()
    {
        return this._cdIndicadorPercentualIndicadorReajusteTarifa;
    } //-- java.lang.String getCdIndicadorPercentualIndicadorReajusteTarifa() 

    /**
     * Returns the value of field
     * 'cdIndicadorPercentualMaximoInconLote'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorPercentualMaximoInconLote'.
     */
    public java.lang.String getCdIndicadorPercentualMaximoInconLote()
    {
        return this._cdIndicadorPercentualMaximoInconLote;
    } //-- java.lang.String getCdIndicadorPercentualMaximoInconLote() 

    /**
     * Returns the value of field
     * 'cdIndicadorPercentualReducaoTarifaCatalogo'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorPercentualReducaoTarifaCatalogo'.
     */
    public java.lang.String getCdIndicadorPercentualReducaoTarifaCatalogo()
    {
        return this._cdIndicadorPercentualReducaoTarifaCatalogo;
    } //-- java.lang.String getCdIndicadorPercentualReducaoTarifaCatalogo() 

    /**
     * Returns the value of field 'cdIndicadorPerdcCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPerdcCobrancaTarifa'.
     */
    public java.lang.String getCdIndicadorPerdcCobrancaTarifa()
    {
        return this._cdIndicadorPerdcCobrancaTarifa;
    } //-- java.lang.String getCdIndicadorPerdcCobrancaTarifa() 

    /**
     * Returns the value of field 'cdIndicadorPerdcComprovante'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPerdcComprovante'.
     */
    public java.lang.String getCdIndicadorPerdcComprovante()
    {
        return this._cdIndicadorPerdcComprovante;
    } //-- java.lang.String getCdIndicadorPerdcComprovante() 

    /**
     * Returns the value of field
     * 'cdIndicadorPerdcConsultaVeiculo'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPerdcConsultaVeiculo'.
     */
    public java.lang.String getCdIndicadorPerdcConsultaVeiculo()
    {
        return this._cdIndicadorPerdcConsultaVeiculo;
    } //-- java.lang.String getCdIndicadorPerdcConsultaVeiculo() 

    /**
     * Returns the value of field 'cdIndicadorPerdcEnvioRemessa'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPerdcEnvioRemessa'.
     */
    public java.lang.String getCdIndicadorPerdcEnvioRemessa()
    {
        return this._cdIndicadorPerdcEnvioRemessa;
    } //-- java.lang.String getCdIndicadorPerdcEnvioRemessa() 

    /**
     * Returns the value of field
     * 'cdIndicadorPerdcManutencaoProcd'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPerdcManutencaoProcd'.
     */
    public java.lang.String getCdIndicadorPerdcManutencaoProcd()
    {
        return this._cdIndicadorPerdcManutencaoProcd;
    } //-- java.lang.String getCdIndicadorPerdcManutencaoProcd() 

    /**
     * Returns the value of field 'cdIndicadorPeriodicidadeAviso'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPeriodicidadeAviso'.
     */
    public java.lang.String getCdIndicadorPeriodicidadeAviso()
    {
        return this._cdIndicadorPeriodicidadeAviso;
    } //-- java.lang.String getCdIndicadorPeriodicidadeAviso() 

    /**
     * Returns the value of field
     * 'cdIndicadorPermissaoDebitoOnline'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPermissaoDebitoOnline'
     */
    public java.lang.String getCdIndicadorPermissaoDebitoOnline()
    {
        return this._cdIndicadorPermissaoDebitoOnline;
    } //-- java.lang.String getCdIndicadorPermissaoDebitoOnline() 

    /**
     * Returns the value of field
     * 'cdIndicadorPrincipalEnquaRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorPrincipalEnquaRecadastro'.
     */
    public java.lang.String getCdIndicadorPrincipalEnquaRecadastro()
    {
        return this._cdIndicadorPrincipalEnquaRecadastro;
    } //-- java.lang.String getCdIndicadorPrincipalEnquaRecadastro() 

    /**
     * Returns the value of field
     * 'cdIndicadorPrioridadeEfetivacaoPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorPrioridadeEfetivacaoPagamento'.
     */
    public java.lang.String getCdIndicadorPrioridadeEfetivacaoPagamento()
    {
        return this._cdIndicadorPrioridadeEfetivacaoPagamento;
    } //-- java.lang.String getCdIndicadorPrioridadeEfetivacaoPagamento() 

    /**
     * Returns the value of field
     * 'cdIndicadorRastreabilidadeNotaFiscal'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorRastreabilidadeNotaFiscal'.
     */
    public java.lang.String getCdIndicadorRastreabilidadeNotaFiscal()
    {
        return this._cdIndicadorRastreabilidadeNotaFiscal;
    } //-- java.lang.String getCdIndicadorRastreabilidadeNotaFiscal() 

    /**
     * Returns the value of field
     * 'cdIndicadorRastreabilidadeTituloTerceiro'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorRastreabilidadeTituloTerceiro'.
     */
    public java.lang.String getCdIndicadorRastreabilidadeTituloTerceiro()
    {
        return this._cdIndicadorRastreabilidadeTituloTerceiro;
    } //-- java.lang.String getCdIndicadorRastreabilidadeTituloTerceiro() 

    /**
     * Returns the value of field 'cdIndicadorRejeicaoAgendaLote'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorRejeicaoAgendaLote'.
     */
    public java.lang.String getCdIndicadorRejeicaoAgendaLote()
    {
        return this._cdIndicadorRejeicaoAgendaLote;
    } //-- java.lang.String getCdIndicadorRejeicaoAgendaLote() 

    /**
     * Returns the value of field
     * 'cdIndicadorRejeicaoEfetivacaoLote'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorRejeicaoEfetivacaoLote'.
     */
    public java.lang.String getCdIndicadorRejeicaoEfetivacaoLote()
    {
        return this._cdIndicadorRejeicaoEfetivacaoLote;
    } //-- java.lang.String getCdIndicadorRejeicaoEfetivacaoLote() 

    /**
     * Returns the value of field 'cdIndicadorRejeicaoLote'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorRejeicaoLote'.
     */
    public java.lang.String getCdIndicadorRejeicaoLote()
    {
        return this._cdIndicadorRejeicaoLote;
    } //-- java.lang.String getCdIndicadorRejeicaoLote() 

    /**
     * Returns the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorRetornoInternet'.
     */
    public java.lang.String getCdIndicadorRetornoInternet()
    {
        return this._cdIndicadorRetornoInternet;
    } //-- java.lang.String getCdIndicadorRetornoInternet() 

    /**
     * Returns the value of field 'cdIndicadorTipoCargaRecadastro'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoCargaRecadastro'.
     */
    public java.lang.String getCdIndicadorTipoCargaRecadastro()
    {
        return this._cdIndicadorTipoCargaRecadastro;
    } //-- java.lang.String getCdIndicadorTipoCargaRecadastro() 

    /**
     * Returns the value of field 'cdIndicadorTipoCataoSalario'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoCataoSalario'.
     */
    public java.lang.String getCdIndicadorTipoCataoSalario()
    {
        return this._cdIndicadorTipoCataoSalario;
    } //-- java.lang.String getCdIndicadorTipoCataoSalario() 

    /**
     * Returns the value of field
     * 'cdIndicadorTipoConsistenciaLista'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoConsistenciaLista'
     */
    public java.lang.String getCdIndicadorTipoConsistenciaLista()
    {
        return this._cdIndicadorTipoConsistenciaLista;
    } //-- java.lang.String getCdIndicadorTipoConsistenciaLista() 

    /**
     * Returns the value of field
     * 'cdIndicadorTipoConsultaComprovante'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorTipoConsultaComprovante'.
     */
    public java.lang.String getCdIndicadorTipoConsultaComprovante()
    {
        return this._cdIndicadorTipoConsultaComprovante;
    } //-- java.lang.String getCdIndicadorTipoConsultaComprovante() 

    /**
     * Returns the value of field
     * 'cdIndicadorTipoContaFavorecidoT'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoContaFavorecidoT'.
     */
    public java.lang.String getCdIndicadorTipoContaFavorecidoT()
    {
        return this._cdIndicadorTipoContaFavorecidoT;
    } //-- java.lang.String getCdIndicadorTipoContaFavorecidoT() 

    /**
     * Returns the value of field 'cdIndicadorTipoDataFloat'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoDataFloat'.
     */
    public java.lang.String getCdIndicadorTipoDataFloat()
    {
        return this._cdIndicadorTipoDataFloat;
    } //-- java.lang.String getCdIndicadorTipoDataFloat() 

    /**
     * Returns the value of field
     * 'cdIndicadorTipoDivergenciaVeiculo'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorTipoDivergenciaVeiculo'.
     */
    public java.lang.String getCdIndicadorTipoDivergenciaVeiculo()
    {
        return this._cdIndicadorTipoDivergenciaVeiculo;
    } //-- java.lang.String getCdIndicadorTipoDivergenciaVeiculo() 

    /**
     * Returns the value of field 'cdIndicadorTipoFormacaoLista'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoFormacaoLista'.
     */
    public java.lang.String getCdIndicadorTipoFormacaoLista()
    {
        return this._cdIndicadorTipoFormacaoLista;
    } //-- java.lang.String getCdIndicadorTipoFormacaoLista() 

    /**
     * Returns the value of field 'cdIndicadorTipoIdBeneficio'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoIdBeneficio'.
     */
    public java.lang.String getCdIndicadorTipoIdBeneficio()
    {
        return this._cdIndicadorTipoIdBeneficio;
    } //-- java.lang.String getCdIndicadorTipoIdBeneficio() 

    /**
     * Returns the value of field
     * 'cdIndicadorTipoIsncricaoFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorTipoIsncricaoFavorecido'.
     */
    public java.lang.String getCdIndicadorTipoIsncricaoFavorecido()
    {
        return this._cdIndicadorTipoIsncricaoFavorecido;
    } //-- java.lang.String getCdIndicadorTipoIsncricaoFavorecido() 

    /**
     * Returns the value of field 'cdIndicadorTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoLayoutArquivo'.
     */
    public java.lang.String getCdIndicadorTipoLayoutArquivo()
    {
        return this._cdIndicadorTipoLayoutArquivo;
    } //-- java.lang.String getCdIndicadorTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdIndicadorTipoReajusteTarifa'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorTipoReajusteTarifa'.
     */
    public java.lang.String getCdIndicadorTipoReajusteTarifa()
    {
        return this._cdIndicadorTipoReajusteTarifa;
    } //-- java.lang.String getCdIndicadorTipoReajusteTarifa() 

    /**
     * Returns the value of field
     * 'cdIndicadorUtilizacaoFavorecidoControle'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorUtilizacaoFavorecidoControle'.
     */
    public java.lang.String getCdIndicadorUtilizacaoFavorecidoControle()
    {
        return this._cdIndicadorUtilizacaoFavorecidoControle;
    } //-- java.lang.String getCdIndicadorUtilizacaoFavorecidoControle() 

    /**
     * Returns the value of field
     * 'cdIndicadorValidacaoNomeFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'cdIndicadorValidacaoNomeFavorecido'.
     */
    public java.lang.String getCdIndicadorValidacaoNomeFavorecido()
    {
        return this._cdIndicadorValidacaoNomeFavorecido;
    } //-- java.lang.String getCdIndicadorValidacaoNomeFavorecido() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsIndicadorAreaResrd'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAreaResrd'.
     */
    public java.lang.String getDsIndicadorAreaResrd()
    {
        return this._dsIndicadorAreaResrd;
    } //-- java.lang.String getDsIndicadorAreaResrd() 

    /**
     * Returns the value of field 'dtIndicadorEnquaContaSalario'.
     * 
     * @return String
     * @return the value of field 'dtIndicadorEnquaContaSalario'.
     */
    public java.lang.String getDtIndicadorEnquaContaSalario()
    {
        return this._dtIndicadorEnquaContaSalario;
    } //-- java.lang.String getDtIndicadorEnquaContaSalario() 

    /**
     * Returns the value of field 'dtIndicadorFimAcertoRecadastro'.
     * 
     * @return String
     * @return the value of field 'dtIndicadorFimAcertoRecadastro'.
     */
    public java.lang.String getDtIndicadorFimAcertoRecadastro()
    {
        return this._dtIndicadorFimAcertoRecadastro;
    } //-- java.lang.String getDtIndicadorFimAcertoRecadastro() 

    /**
     * Returns the value of field
     * 'dtIndicadorFimRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'dtIndicadorFimRecadastroBeneficio'.
     */
    public java.lang.String getDtIndicadorFimRecadastroBeneficio()
    {
        return this._dtIndicadorFimRecadastroBeneficio;
    } //-- java.lang.String getDtIndicadorFimRecadastroBeneficio() 

    /**
     * Returns the value of field 'dtIndicadorInicioBloqueioPplta'.
     * 
     * @return String
     * @return the value of field 'dtIndicadorInicioBloqueioPplta'.
     */
    public java.lang.String getDtIndicadorInicioBloqueioPplta()
    {
        return this._dtIndicadorInicioBloqueioPplta;
    } //-- java.lang.String getDtIndicadorInicioBloqueioPplta() 

    /**
     * Returns the value of field
     * 'dtIndicadorInicioRastreabilidadeTitulo'.
     * 
     * @return String
     * @return the value of field
     * 'dtIndicadorInicioRastreabilidadeTitulo'.
     */
    public java.lang.String getDtIndicadorInicioRastreabilidadeTitulo()
    {
        return this._dtIndicadorInicioRastreabilidadeTitulo;
    } //-- java.lang.String getDtIndicadorInicioRastreabilidadeTitulo() 

    /**
     * Returns the value of field
     * 'dtIndicadorInicioRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'dtIndicadorInicioRecadastroBeneficio'.
     */
    public java.lang.String getDtIndicadorInicioRecadastroBeneficio()
    {
        return this._dtIndicadorInicioRecadastroBeneficio;
    } //-- java.lang.String getDtIndicadorInicioRecadastroBeneficio() 

    /**
     * Returns the value of field 'dtIndicadorLimiteVinculoCarga'.
     * 
     * @return String
     * @return the value of field 'dtIndicadorLimiteVinculoCarga'.
     */
    public java.lang.String getDtIndicadorLimiteVinculoCarga()
    {
        return this._dtIndicadorLimiteVinculoCarga;
    } //-- java.lang.String getDtIndicadorLimiteVinculoCarga() 

    /**
     * Returns the value of field
     * 'dtIndicadorinicioAcertoRecadastro'.
     * 
     * @return String
     * @return the value of field
     * 'dtIndicadorinicioAcertoRecadastro'.
     */
    public java.lang.String getDtIndicadorinicioAcertoRecadastro()
    {
        return this._dtIndicadorinicioAcertoRecadastro;
    } //-- java.lang.String getDtIndicadorinicioAcertoRecadastro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field
     * 'nrIndicadorFechamentoApuracaoTarifa'.
     * 
     * @return String
     * @return the value of field
     * 'nrIndicadorFechamentoApuracaoTarifa'.
     */
    public java.lang.String getNrIndicadorFechamentoApuracaoTarifa()
    {
        return this._nrIndicadorFechamentoApuracaoTarifa;
    } //-- java.lang.String getNrIndicadorFechamentoApuracaoTarifa() 

    /**
     * Returns the value of field 'qtIndicadorAntecedencia'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorAntecedencia'.
     */
    public java.lang.String getQtIndicadorAntecedencia()
    {
        return this._qtIndicadorAntecedencia;
    } //-- java.lang.String getQtIndicadorAntecedencia() 

    /**
     * Returns the value of field
     * 'qtIndicadorAnteriorVencimentoComprovado'.
     * 
     * @return String
     * @return the value of field
     * 'qtIndicadorAnteriorVencimentoComprovado'.
     */
    public java.lang.String getQtIndicadorAnteriorVencimentoComprovado()
    {
        return this._qtIndicadorAnteriorVencimentoComprovado;
    } //-- java.lang.String getQtIndicadorAnteriorVencimentoComprovado() 

    /**
     * Returns the value of field 'qtIndicadorDiaCobrancaTarifa'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorDiaCobrancaTarifa'.
     */
    public java.lang.String getQtIndicadorDiaCobrancaTarifa()
    {
        return this._qtIndicadorDiaCobrancaTarifa;
    } //-- java.lang.String getQtIndicadorDiaCobrancaTarifa() 

    /**
     * Returns the value of field 'qtIndicadorDiaExpiracao'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorDiaExpiracao'.
     */
    public java.lang.String getQtIndicadorDiaExpiracao()
    {
        return this._qtIndicadorDiaExpiracao;
    } //-- java.lang.String getQtIndicadorDiaExpiracao() 

    /**
     * Returns the value of field
     * 'qtIndicadorDiaInatividadeFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'qtIndicadorDiaInatividadeFavorecido'.
     */
    public java.lang.String getQtIndicadorDiaInatividadeFavorecido()
    {
        return this._qtIndicadorDiaInatividadeFavorecido;
    } //-- java.lang.String getQtIndicadorDiaInatividadeFavorecido() 

    /**
     * Returns the value of field 'qtIndicadorDiaRepiqConsulta'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorDiaRepiqConsulta'.
     */
    public java.lang.String getQtIndicadorDiaRepiqConsulta()
    {
        return this._qtIndicadorDiaRepiqConsulta;
    } //-- java.lang.String getQtIndicadorDiaRepiqConsulta() 

    /**
     * Returns the value of field
     * 'qtIndicadorEtapasRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'qtIndicadorEtapasRecadastroBeneficio'.
     */
    public java.lang.String getQtIndicadorEtapasRecadastroBeneficio()
    {
        return this._qtIndicadorEtapasRecadastroBeneficio;
    } //-- java.lang.String getQtIndicadorEtapasRecadastroBeneficio() 

    /**
     * Returns the value of field
     * 'qtIndicadorFaseRecadastroBeneficio'.
     * 
     * @return String
     * @return the value of field
     * 'qtIndicadorFaseRecadastroBeneficio'.
     */
    public java.lang.String getQtIndicadorFaseRecadastroBeneficio()
    {
        return this._qtIndicadorFaseRecadastroBeneficio;
    } //-- java.lang.String getQtIndicadorFaseRecadastroBeneficio() 

    /**
     * Returns the value of field 'qtIndicadorLimiteLinha'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorLimiteLinha'.
     */
    public java.lang.String getQtIndicadorLimiteLinha()
    {
        return this._qtIndicadorLimiteLinha;
    } //-- java.lang.String getQtIndicadorLimiteLinha() 

    /**
     * Returns the value of field
     * 'qtIndicadorLimiteSolicitacaoCatao'.
     * 
     * @return String
     * @return the value of field
     * 'qtIndicadorLimiteSolicitacaoCatao'.
     */
    public java.lang.String getQtIndicadorLimiteSolicitacaoCatao()
    {
        return this._qtIndicadorLimiteSolicitacaoCatao;
    } //-- java.lang.String getQtIndicadorLimiteSolicitacaoCatao() 

    /**
     * Returns the value of field 'qtIndicadorMaximaInconLote'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorMaximaInconLote'.
     */
    public java.lang.String getQtIndicadorMaximaInconLote()
    {
        return this._qtIndicadorMaximaInconLote;
    } //-- java.lang.String getQtIndicadorMaximaInconLote() 

    /**
     * Returns the value of field 'qtIndicadorMaximaTituloVencido'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorMaximaTituloVencido'.
     */
    public java.lang.String getQtIndicadorMaximaTituloVencido()
    {
        return this._qtIndicadorMaximaTituloVencido;
    } //-- java.lang.String getQtIndicadorMaximaTituloVencido() 

    /**
     * Returns the value of field 'qtIndicadorMesComprovante'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorMesComprovante'.
     */
    public java.lang.String getQtIndicadorMesComprovante()
    {
        return this._qtIndicadorMesComprovante;
    } //-- java.lang.String getQtIndicadorMesComprovante() 

    /**
     * Returns the value of field 'qtIndicadorMesEtapaRecadastro'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorMesEtapaRecadastro'.
     */
    public java.lang.String getQtIndicadorMesEtapaRecadastro()
    {
        return this._qtIndicadorMesEtapaRecadastro;
    } //-- java.lang.String getQtIndicadorMesEtapaRecadastro() 

    /**
     * Returns the value of field 'qtIndicadorMesFaseRecadastro'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorMesFaseRecadastro'.
     */
    public java.lang.String getQtIndicadorMesFaseRecadastro()
    {
        return this._qtIndicadorMesFaseRecadastro;
    } //-- java.lang.String getQtIndicadorMesFaseRecadastro() 

    /**
     * Returns the value of field 'qtIndicadorMesReajusteTarifa'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorMesReajusteTarifa'.
     */
    public java.lang.String getQtIndicadorMesReajusteTarifa()
    {
        return this._qtIndicadorMesReajusteTarifa;
    } //-- java.lang.String getQtIndicadorMesReajusteTarifa() 

    /**
     * Returns the value of field 'qtIndicadorViaAviso'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorViaAviso'.
     */
    public java.lang.String getQtIndicadorViaAviso()
    {
        return this._qtIndicadorViaAviso;
    } //-- java.lang.String getQtIndicadorViaAviso() 

    /**
     * Returns the value of field 'qtIndicadorViaCobranca'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorViaCobranca'.
     */
    public java.lang.String getQtIndicadorViaCobranca()
    {
        return this._qtIndicadorViaCobranca;
    } //-- java.lang.String getQtIndicadorViaCobranca() 

    /**
     * Returns the value of field 'qtIndicadorViaComprovante'.
     * 
     * @return String
     * @return the value of field 'qtIndicadorViaComprovante'.
     */
    public java.lang.String getQtIndicadorViaComprovante()
    {
        return this._qtIndicadorViaComprovante;
    } //-- java.lang.String getQtIndicadorViaComprovante() 

    /**
     * Returns the value of field
     * 'vlIndicadorFavorecidoNaoCadastro'.
     * 
     * @return String
     * @return the value of field 'vlIndicadorFavorecidoNaoCadastro'
     */
    public java.lang.String getVlIndicadorFavorecidoNaoCadastro()
    {
        return this._vlIndicadorFavorecidoNaoCadastro;
    } //-- java.lang.String getVlIndicadorFavorecidoNaoCadastro() 

    /**
     * Returns the value of field 'vlIndicadorLimiteDiaPagamento'.
     * 
     * @return String
     * @return the value of field 'vlIndicadorLimiteDiaPagamento'.
     */
    public java.lang.String getVlIndicadorLimiteDiaPagamento()
    {
        return this._vlIndicadorLimiteDiaPagamento;
    } //-- java.lang.String getVlIndicadorLimiteDiaPagamento() 

    /**
     * Returns the value of field
     * 'vlIndicadorLimiteIndividualPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'vlIndicadorLimiteIndividualPagamento'.
     */
    public java.lang.String getVlIndicadorLimiteIndividualPagamento()
    {
        return this._vlIndicadorLimiteIndividualPagamento;
    } //-- java.lang.String getVlIndicadorLimiteIndividualPagamento() 

    /**
     * Method hasCdDependencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDependencia()
    {
        return this._has_cdDependencia;
    } //-- boolean hasCdDependencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdDependencia'.
     * 
     * @param cdDependencia the value of field 'cdDependencia'.
     */
    public void setCdDependencia(int cdDependencia)
    {
        this._cdDependencia = cdDependencia;
        this._has_cdDependencia = true;
    } //-- void setCdDependencia(int) 

    /**
     * Sets the value of field 'cdIndLancamentoPersonalizado'.
     * 
     * @param cdIndLancamentoPersonalizado the value of field
     * 'cdIndLancamentoPersonalizado'.
     */
    public void setCdIndLancamentoPersonalizado(java.lang.String cdIndLancamentoPersonalizado)
    {
        this._cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
    } //-- void setCdIndLancamentoPersonalizado(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAcaoNaoVida'.
     * 
     * @param cdIndicadorAcaoNaoVida the value of field
     * 'cdIndicadorAcaoNaoVida'.
     */
    public void setCdIndicadorAcaoNaoVida(java.lang.String cdIndicadorAcaoNaoVida)
    {
        this._cdIndicadorAcaoNaoVida = cdIndicadorAcaoNaoVida;
    } //-- void setCdIndicadorAcaoNaoVida(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAcertoDadoRecadastro'.
     * 
     * @param cdIndicadorAcertoDadoRecadastro the value of field
     * 'cdIndicadorAcertoDadoRecadastro'.
     */
    public void setCdIndicadorAcertoDadoRecadastro(java.lang.String cdIndicadorAcertoDadoRecadastro)
    {
        this._cdIndicadorAcertoDadoRecadastro = cdIndicadorAcertoDadoRecadastro;
    } //-- void setCdIndicadorAcertoDadoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAdesaoSacador'.
     * 
     * @param cdIndicadorAdesaoSacador the value of field
     * 'cdIndicadorAdesaoSacador'.
     */
    public void setCdIndicadorAdesaoSacador(java.lang.String cdIndicadorAdesaoSacador)
    {
        this._cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
    } //-- void setCdIndicadorAdesaoSacador(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAgendaDebitoVeiculo'.
     * 
     * @param cdIndicadorAgendaDebitoVeiculo the value of field
     * 'cdIndicadorAgendaDebitoVeiculo'.
     */
    public void setCdIndicadorAgendaDebitoVeiculo(java.lang.String cdIndicadorAgendaDebitoVeiculo)
    {
        this._cdIndicadorAgendaDebitoVeiculo = cdIndicadorAgendaDebitoVeiculo;
    } //-- void setCdIndicadorAgendaDebitoVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAgendaPagamentoVencido'.
     * 
     * @param cdIndicadorAgendaPagamentoVencido the value of field
     * 'cdIndicadorAgendaPagamentoVencido'.
     */
    public void setCdIndicadorAgendaPagamentoVencido(java.lang.String cdIndicadorAgendaPagamentoVencido)
    {
        this._cdIndicadorAgendaPagamentoVencido = cdIndicadorAgendaPagamentoVencido;
    } //-- void setCdIndicadorAgendaPagamentoVencido(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorAgendaRastreabilidadeFilial'.
     * 
     * @param cdIndicadorAgendaRastreabilidadeFilial the value of
     * field 'cdIndicadorAgendaRastreabilidadeFilial'.
     */
    public void setCdIndicadorAgendaRastreabilidadeFilial(java.lang.String cdIndicadorAgendaRastreabilidadeFilial)
    {
        this._cdIndicadorAgendaRastreabilidadeFilial = cdIndicadorAgendaRastreabilidadeFilial;
    } //-- void setCdIndicadorAgendaRastreabilidadeFilial(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAgendaTitulo'.
     * 
     * @param cdIndicadorAgendaTitulo the value of field
     * 'cdIndicadorAgendaTitulo'.
     */
    public void setCdIndicadorAgendaTitulo(java.lang.String cdIndicadorAgendaTitulo)
    {
        this._cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
    } //-- void setCdIndicadorAgendaTitulo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAgendaValorMenor'.
     * 
     * @param cdIndicadorAgendaValorMenor the value of field
     * 'cdIndicadorAgendaValorMenor'.
     */
    public void setCdIndicadorAgendaValorMenor(java.lang.String cdIndicadorAgendaValorMenor)
    {
        this._cdIndicadorAgendaValorMenor = cdIndicadorAgendaValorMenor;
    } //-- void setCdIndicadorAgendaValorMenor(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAgrupamentoAviso'.
     * 
     * @param cdIndicadorAgrupamentoAviso the value of field
     * 'cdIndicadorAgrupamentoAviso'.
     */
    public void setCdIndicadorAgrupamentoAviso(java.lang.String cdIndicadorAgrupamentoAviso)
    {
        this._cdIndicadorAgrupamentoAviso = cdIndicadorAgrupamentoAviso;
    } //-- void setCdIndicadorAgrupamentoAviso(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAgrupamentoComprovado'.
     * 
     * @param cdIndicadorAgrupamentoComprovado the value of field
     * 'cdIndicadorAgrupamentoComprovado'.
     */
    public void setCdIndicadorAgrupamentoComprovado(java.lang.String cdIndicadorAgrupamentoComprovado)
    {
        this._cdIndicadorAgrupamentoComprovado = cdIndicadorAgrupamentoComprovado;
    } //-- void setCdIndicadorAgrupamentoComprovado(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorAgrupamentoFormularioRecadastro'.
     * 
     * @param cdIndicadorAgrupamentoFormularioRecadastro the value
     * of field 'cdIndicadorAgrupamentoFormularioRecadastro'.
     */
    public void setCdIndicadorAgrupamentoFormularioRecadastro(java.lang.String cdIndicadorAgrupamentoFormularioRecadastro)
    {
        this._cdIndicadorAgrupamentoFormularioRecadastro = cdIndicadorAgrupamentoFormularioRecadastro;
    } //-- void setCdIndicadorAgrupamentoFormularioRecadastro(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorAntecRecadastroBeneficio'.
     * 
     * @param cdIndicadorAntecRecadastroBeneficio the value of
     * field 'cdIndicadorAntecRecadastroBeneficio'.
     */
    public void setCdIndicadorAntecRecadastroBeneficio(java.lang.String cdIndicadorAntecRecadastroBeneficio)
    {
        this._cdIndicadorAntecRecadastroBeneficio = cdIndicadorAntecRecadastroBeneficio;
    } //-- void setCdIndicadorAntecRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAreaReservada'.
     * 
     * @param cdIndicadorAreaReservada the value of field
     * 'cdIndicadorAreaReservada'.
     */
    public void setCdIndicadorAreaReservada(java.lang.String cdIndicadorAreaReservada)
    {
        this._cdIndicadorAreaReservada = cdIndicadorAreaReservada;
    } //-- void setCdIndicadorAreaReservada(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoCliente'.
     * 
     * @param cdIndicadorAutorizacaoCliente the value of field
     * 'cdIndicadorAutorizacaoCliente'.
     */
    public void setCdIndicadorAutorizacaoCliente(java.lang.String cdIndicadorAutorizacaoCliente)
    {
        this._cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
    } //-- void setCdIndicadorAutorizacaoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoComplemento'.
     * 
     * @param cdIndicadorAutorizacaoComplemento the value of field
     * 'cdIndicadorAutorizacaoComplemento'.
     */
    public void setCdIndicadorAutorizacaoComplemento(java.lang.String cdIndicadorAutorizacaoComplemento)
    {
        this._cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
    } //-- void setCdIndicadorAutorizacaoComplemento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorBancoPostal'.
     * 
     * @param cdIndicadorBancoPostal the value of field
     * 'cdIndicadorBancoPostal'.
     */
    public void setCdIndicadorBancoPostal(java.lang.String cdIndicadorBancoPostal)
    {
        this._cdIndicadorBancoPostal = cdIndicadorBancoPostal;
    } //-- void setCdIndicadorBancoPostal(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorBaseRecadastroBeneficio'.
     * 
     * @param cdIndicadorBaseRecadastroBeneficio the value of field
     * 'cdIndicadorBaseRecadastroBeneficio'.
     */
    public void setCdIndicadorBaseRecadastroBeneficio(java.lang.String cdIndicadorBaseRecadastroBeneficio)
    {
        this._cdIndicadorBaseRecadastroBeneficio = cdIndicadorBaseRecadastroBeneficio;
    } //-- void setCdIndicadorBaseRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorBloqueioEmissaoPplta'.
     * 
     * @param cdIndicadorBloqueioEmissaoPplta the value of field
     * 'cdIndicadorBloqueioEmissaoPplta'.
     */
    public void setCdIndicadorBloqueioEmissaoPplta(java.lang.String cdIndicadorBloqueioEmissaoPplta)
    {
        this._cdIndicadorBloqueioEmissaoPplta = cdIndicadorBloqueioEmissaoPplta;
    } //-- void setCdIndicadorBloqueioEmissaoPplta(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCadastroOrg'.
     * 
     * @param cdIndicadorCadastroOrg the value of field
     * 'cdIndicadorCadastroOrg'.
     */
    public void setCdIndicadorCadastroOrg(java.lang.String cdIndicadorCadastroOrg)
    {
        this._cdIndicadorCadastroOrg = cdIndicadorCadastroOrg;
    } //-- void setCdIndicadorCadastroOrg(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCadastroProcd'.
     * 
     * @param cdIndicadorCadastroProcd the value of field
     * 'cdIndicadorCadastroProcd'.
     */
    public void setCdIndicadorCadastroProcd(java.lang.String cdIndicadorCadastroProcd)
    {
        this._cdIndicadorCadastroProcd = cdIndicadorCadastroProcd;
    } //-- void setCdIndicadorCadastroProcd(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCapituloTituloRegistro'.
     * 
     * @param cdIndicadorCapituloTituloRegistro the value of field
     * 'cdIndicadorCapituloTituloRegistro'.
     */
    public void setCdIndicadorCapituloTituloRegistro(java.lang.String cdIndicadorCapituloTituloRegistro)
    {
        this._cdIndicadorCapituloTituloRegistro = cdIndicadorCapituloTituloRegistro;
    } //-- void setCdIndicadorCapituloTituloRegistro(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCataoSalario'.
     * 
     * @param cdIndicadorCataoSalario the value of field
     * 'cdIndicadorCataoSalario'.
     */
    public void setCdIndicadorCataoSalario(java.lang.String cdIndicadorCataoSalario)
    {
        this._cdIndicadorCataoSalario = cdIndicadorCataoSalario;
    } //-- void setCdIndicadorCataoSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCobrancaTarifa'.
     * 
     * @param cdIndicadorCobrancaTarifa the value of field
     * 'cdIndicadorCobrancaTarifa'.
     */
    public void setCdIndicadorCobrancaTarifa(java.lang.String cdIndicadorCobrancaTarifa)
    {
        this._cdIndicadorCobrancaTarifa = cdIndicadorCobrancaTarifa;
    } //-- void setCdIndicadorCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorConsDebitoVeiculo'.
     * 
     * @param cdIndicadorConsDebitoVeiculo the value of field
     * 'cdIndicadorConsDebitoVeiculo'.
     */
    public void setCdIndicadorConsDebitoVeiculo(java.lang.String cdIndicadorConsDebitoVeiculo)
    {
        this._cdIndicadorConsDebitoVeiculo = cdIndicadorConsDebitoVeiculo;
    } //-- void setCdIndicadorConsDebitoVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorConsEndereco'.
     * 
     * @param cdIndicadorConsEndereco the value of field
     * 'cdIndicadorConsEndereco'.
     */
    public void setCdIndicadorConsEndereco(java.lang.String cdIndicadorConsEndereco)
    {
        this._cdIndicadorConsEndereco = cdIndicadorConsEndereco;
    } //-- void setCdIndicadorConsEndereco(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorConsSaldoPagamento'.
     * 
     * @param cdIndicadorConsSaldoPagamento the value of field
     * 'cdIndicadorConsSaldoPagamento'.
     */
    public void setCdIndicadorConsSaldoPagamento(java.lang.String cdIndicadorConsSaldoPagamento)
    {
        this._cdIndicadorConsSaldoPagamento = cdIndicadorConsSaldoPagamento;
    } //-- void setCdIndicadorConsSaldoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorConsultaSaldoSuperior'.
     * 
     * @param cdIndicadorConsultaSaldoSuperior the value of field
     * 'cdIndicadorConsultaSaldoSuperior'.
     */
    public void setCdIndicadorConsultaSaldoSuperior(java.lang.String cdIndicadorConsultaSaldoSuperior)
    {
        this._cdIndicadorConsultaSaldoSuperior = cdIndicadorConsultaSaldoSuperior;
    } //-- void setCdIndicadorConsultaSaldoSuperior(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorContagemConsSaudo'.
     * 
     * @param cdIndicadorContagemConsSaudo the value of field
     * 'cdIndicadorContagemConsSaudo'.
     */
    public void setCdIndicadorContagemConsSaudo(java.lang.String cdIndicadorContagemConsSaudo)
    {
        this._cdIndicadorContagemConsSaudo = cdIndicadorContagemConsSaudo;
    } //-- void setCdIndicadorContagemConsSaudo(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorContratoContaTransferencia'.
     * 
     * @param cdIndicadorContratoContaTransferencia the value of
     * field 'cdIndicadorContratoContaTransferencia'.
     */
    public void setCdIndicadorContratoContaTransferencia(java.lang.String cdIndicadorContratoContaTransferencia)
    {
        this._cdIndicadorContratoContaTransferencia = cdIndicadorContratoContaTransferencia;
    } //-- void setCdIndicadorContratoContaTransferencia(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCreditoNaoUtilizado'.
     * 
     * @param cdIndicadorCreditoNaoUtilizado the value of field
     * 'cdIndicadorCreditoNaoUtilizado'.
     */
    public void setCdIndicadorCreditoNaoUtilizado(java.lang.String cdIndicadorCreditoNaoUtilizado)
    {
        this._cdIndicadorCreditoNaoUtilizado = cdIndicadorCreditoNaoUtilizado;
    } //-- void setCdIndicadorCreditoNaoUtilizado(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCriterioEnqua'.
     * 
     * @param cdIndicadorCriterioEnqua the value of field
     * 'cdIndicadorCriterioEnqua'.
     */
    public void setCdIndicadorCriterioEnqua(java.lang.String cdIndicadorCriterioEnqua)
    {
        this._cdIndicadorCriterioEnqua = cdIndicadorCriterioEnqua;
    } //-- void setCdIndicadorCriterioEnqua(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCriterioEnquaBeneficio'.
     * 
     * @param cdIndicadorCriterioEnquaBeneficio the value of field
     * 'cdIndicadorCriterioEnquaBeneficio'.
     */
    public void setCdIndicadorCriterioEnquaBeneficio(java.lang.String cdIndicadorCriterioEnquaBeneficio)
    {
        this._cdIndicadorCriterioEnquaBeneficio = cdIndicadorCriterioEnquaBeneficio;
    } //-- void setCdIndicadorCriterioEnquaBeneficio(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorCriterioRastreabilidadeTitulo'.
     * 
     * @param cdIndicadorCriterioRastreabilidadeTitulo the value of
     * field 'cdIndicadorCriterioRastreabilidadeTitulo'.
     */
    public void setCdIndicadorCriterioRastreabilidadeTitulo(java.lang.String cdIndicadorCriterioRastreabilidadeTitulo)
    {
        this._cdIndicadorCriterioRastreabilidadeTitulo = cdIndicadorCriterioRastreabilidadeTitulo;
    } //-- void setCdIndicadorCriterioRastreabilidadeTitulo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorCtciaEspecieBeneficio'.
     * 
     * @param cdIndicadorCtciaEspecieBeneficio the value of field
     * 'cdIndicadorCtciaEspecieBeneficio'.
     */
    public void setCdIndicadorCtciaEspecieBeneficio(java.lang.String cdIndicadorCtciaEspecieBeneficio)
    {
        this._cdIndicadorCtciaEspecieBeneficio = cdIndicadorCtciaEspecieBeneficio;
    } //-- void setCdIndicadorCtciaEspecieBeneficio(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorCtciaIdentificacaoBeneficio'.
     * 
     * @param cdIndicadorCtciaIdentificacaoBeneficio the value of
     * field 'cdIndicadorCtciaIdentificacaoBeneficio'.
     */
    public void setCdIndicadorCtciaIdentificacaoBeneficio(java.lang.String cdIndicadorCtciaIdentificacaoBeneficio)
    {
        this._cdIndicadorCtciaIdentificacaoBeneficio = cdIndicadorCtciaIdentificacaoBeneficio;
    } //-- void setCdIndicadorCtciaIdentificacaoBeneficio(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorCtciaInscricaoFavorecido'.
     * 
     * @param cdIndicadorCtciaInscricaoFavorecido the value of
     * field 'cdIndicadorCtciaInscricaoFavorecido'.
     */
    public void setCdIndicadorCtciaInscricaoFavorecido(java.lang.String cdIndicadorCtciaInscricaoFavorecido)
    {
        this._cdIndicadorCtciaInscricaoFavorecido = cdIndicadorCtciaInscricaoFavorecido;
    } //-- void setCdIndicadorCtciaInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorCtciaProprietarioVeiculo'.
     * 
     * @param cdIndicadorCtciaProprietarioVeiculo the value of
     * field 'cdIndicadorCtciaProprietarioVeiculo'.
     */
    public void setCdIndicadorCtciaProprietarioVeiculo(java.lang.String cdIndicadorCtciaProprietarioVeiculo)
    {
        this._cdIndicadorCtciaProprietarioVeiculo = cdIndicadorCtciaProprietarioVeiculo;
    } //-- void setCdIndicadorCtciaProprietarioVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorDestinoAviso'.
     * 
     * @param cdIndicadorDestinoAviso the value of field
     * 'cdIndicadorDestinoAviso'.
     */
    public void setCdIndicadorDestinoAviso(java.lang.String cdIndicadorDestinoAviso)
    {
        this._cdIndicadorDestinoAviso = cdIndicadorDestinoAviso;
    } //-- void setCdIndicadorDestinoAviso(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorDestinoComprovante'.
     * 
     * @param cdIndicadorDestinoComprovante the value of field
     * 'cdIndicadorDestinoComprovante'.
     */
    public void setCdIndicadorDestinoComprovante(java.lang.String cdIndicadorDestinoComprovante)
    {
        this._cdIndicadorDestinoComprovante = cdIndicadorDestinoComprovante;
    } //-- void setCdIndicadorDestinoComprovante(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorDestinoFormularioRecadastro'.
     * 
     * @param cdIndicadorDestinoFormularioRecadastro the value of
     * field 'cdIndicadorDestinoFormularioRecadastro'.
     */
    public void setCdIndicadorDestinoFormularioRecadastro(java.lang.String cdIndicadorDestinoFormularioRecadastro)
    {
        this._cdIndicadorDestinoFormularioRecadastro = cdIndicadorDestinoFormularioRecadastro;
    } //-- void setCdIndicadorDestinoFormularioRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorDiaFloatPagamento'.
     * 
     * @param cdIndicadorDiaFloatPagamento the value of field
     * 'cdIndicadorDiaFloatPagamento'.
     */
    public void setCdIndicadorDiaFloatPagamento(java.lang.String cdIndicadorDiaFloatPagamento)
    {
        this._cdIndicadorDiaFloatPagamento = cdIndicadorDiaFloatPagamento;
    } //-- void setCdIndicadorDiaFloatPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorDispzContaCredito'.
     * 
     * @param cdIndicadorDispzContaCredito the value of field
     * 'cdIndicadorDispzContaCredito'.
     */
    public void setCdIndicadorDispzContaCredito(java.lang.String cdIndicadorDispzContaCredito)
    {
        this._cdIndicadorDispzContaCredito = cdIndicadorDispzContaCredito;
    } //-- void setCdIndicadorDispzContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorDispzDiversar'.
     * 
     * @param cdIndicadorDispzDiversar the value of field
     * 'cdIndicadorDispzDiversar'.
     */
    public void setCdIndicadorDispzDiversar(java.lang.String cdIndicadorDispzDiversar)
    {
        this._cdIndicadorDispzDiversar = cdIndicadorDispzDiversar;
    } //-- void setCdIndicadorDispzDiversar(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorDispzDiversasNao'.
     * 
     * @param cdIndicadorDispzDiversasNao the value of field
     * 'cdIndicadorDispzDiversasNao'.
     */
    public void setCdIndicadorDispzDiversasNao(java.lang.String cdIndicadorDispzDiversasNao)
    {
        this._cdIndicadorDispzDiversasNao = cdIndicadorDispzDiversasNao;
    } //-- void setCdIndicadorDispzDiversasNao(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorDispzSalario'.
     * 
     * @param cdIndicadorDispzSalario the value of field
     * 'cdIndicadorDispzSalario'.
     */
    public void setCdIndicadorDispzSalario(java.lang.String cdIndicadorDispzSalario)
    {
        this._cdIndicadorDispzSalario = cdIndicadorDispzSalario;
    } //-- void setCdIndicadorDispzSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorDispzSalarioNao'.
     * 
     * @param cdIndicadorDispzSalarioNao the value of field
     * 'cdIndicadorDispzSalarioNao'.
     */
    public void setCdIndicadorDispzSalarioNao(java.lang.String cdIndicadorDispzSalarioNao)
    {
        this._cdIndicadorDispzSalarioNao = cdIndicadorDispzSalarioNao;
    } //-- void setCdIndicadorDispzSalarioNao(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoReajuste'.
     * 
     * @param cdIndicadorEconomicoReajuste the value of field
     * 'cdIndicadorEconomicoReajuste'.
     */
    public void setCdIndicadorEconomicoReajuste(java.lang.String cdIndicadorEconomicoReajuste)
    {
        this._cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
    } //-- void setCdIndicadorEconomicoReajuste(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorEmissaoAviso'.
     * 
     * @param cdIndicadorEmissaoAviso the value of field
     * 'cdIndicadorEmissaoAviso'.
     */
    public void setCdIndicadorEmissaoAviso(java.lang.String cdIndicadorEmissaoAviso)
    {
        this._cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
    } //-- void setCdIndicadorEmissaoAviso(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorEnvelopeAberto'.
     * 
     * @param cdIndicadorEnvelopeAberto the value of field
     * 'cdIndicadorEnvelopeAberto'.
     */
    public void setCdIndicadorEnvelopeAberto(java.lang.String cdIndicadorEnvelopeAberto)
    {
        this._cdIndicadorEnvelopeAberto = cdIndicadorEnvelopeAberto;
    } //-- void setCdIndicadorEnvelopeAberto(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorExpiracaoCredito'.
     * 
     * @param cdIndicadorExpiracaoCredito the value of field
     * 'cdIndicadorExpiracaoCredito'.
     */
    public void setCdIndicadorExpiracaoCredito(java.lang.String cdIndicadorExpiracaoCredito)
    {
        this._cdIndicadorExpiracaoCredito = cdIndicadorExpiracaoCredito;
    } //-- void setCdIndicadorExpiracaoCredito(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorFavorecidoConsPagamento'.
     * 
     * @param cdIndicadorFavorecidoConsPagamento the value of field
     * 'cdIndicadorFavorecidoConsPagamento'.
     */
    public void setCdIndicadorFavorecidoConsPagamento(java.lang.String cdIndicadorFavorecidoConsPagamento)
    {
        this._cdIndicadorFavorecidoConsPagamento = cdIndicadorFavorecidoConsPagamento;
    } //-- void setCdIndicadorFavorecidoConsPagamento(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorFormaAutorizacaoPagamento'.
     * 
     * @param cdIndicadorFormaAutorizacaoPagamento the value of
     * field 'cdIndicadorFormaAutorizacaoPagamento'.
     */
    public void setCdIndicadorFormaAutorizacaoPagamento(java.lang.String cdIndicadorFormaAutorizacaoPagamento)
    {
        this._cdIndicadorFormaAutorizacaoPagamento = cdIndicadorFormaAutorizacaoPagamento;
    } //-- void setCdIndicadorFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorFormaEnvioPagamento'.
     * 
     * @param cdIndicadorFormaEnvioPagamento the value of field
     * 'cdIndicadorFormaEnvioPagamento'.
     */
    public void setCdIndicadorFormaEnvioPagamento(java.lang.String cdIndicadorFormaEnvioPagamento)
    {
        this._cdIndicadorFormaEnvioPagamento = cdIndicadorFormaEnvioPagamento;
    } //-- void setCdIndicadorFormaEnvioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorFormaEstornoPagamento'.
     * 
     * @param cdIndicadorFormaEstornoPagamento the value of field
     * 'cdIndicadorFormaEstornoPagamento'.
     */
    public void setCdIndicadorFormaEstornoPagamento(java.lang.String cdIndicadorFormaEstornoPagamento)
    {
        this._cdIndicadorFormaEstornoPagamento = cdIndicadorFormaEstornoPagamento;
    } //-- void setCdIndicadorFormaEstornoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorFormaExpiracaoCredito'.
     * 
     * @param cdIndicadorFormaExpiracaoCredito the value of field
     * 'cdIndicadorFormaExpiracaoCredito'.
     */
    public void setCdIndicadorFormaExpiracaoCredito(java.lang.String cdIndicadorFormaExpiracaoCredito)
    {
        this._cdIndicadorFormaExpiracaoCredito = cdIndicadorFormaExpiracaoCredito;
    } //-- void setCdIndicadorFormaExpiracaoCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorFormaManutencao'.
     * 
     * @param cdIndicadorFormaManutencao the value of field
     * 'cdIndicadorFormaManutencao'.
     */
    public void setCdIndicadorFormaManutencao(java.lang.String cdIndicadorFormaManutencao)
    {
        this._cdIndicadorFormaManutencao = cdIndicadorFormaManutencao;
    } //-- void setCdIndicadorFormaManutencao(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorFormularioContratoCliente'.
     * 
     * @param cdIndicadorFormularioContratoCliente the value of
     * field 'cdIndicadorFormularioContratoCliente'.
     */
    public void setCdIndicadorFormularioContratoCliente(java.lang.String cdIndicadorFormularioContratoCliente)
    {
        this._cdIndicadorFormularioContratoCliente = cdIndicadorFormularioContratoCliente;
    } //-- void setCdIndicadorFormularioContratoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorFrasePreCadastro'.
     * 
     * @param cdIndicadorFrasePreCadastro the value of field
     * 'cdIndicadorFrasePreCadastro'.
     */
    public void setCdIndicadorFrasePreCadastro(java.lang.String cdIndicadorFrasePreCadastro)
    {
        this._cdIndicadorFrasePreCadastro = cdIndicadorFrasePreCadastro;
    } //-- void setCdIndicadorFrasePreCadastro(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorLancamentoFuturoCredito'.
     * 
     * @param cdIndicadorLancamentoFuturoCredito the value of field
     * 'cdIndicadorLancamentoFuturoCredito'.
     */
    public void setCdIndicadorLancamentoFuturoCredito(java.lang.String cdIndicadorLancamentoFuturoCredito)
    {
        this._cdIndicadorLancamentoFuturoCredito = cdIndicadorLancamentoFuturoCredito;
    } //-- void setCdIndicadorLancamentoFuturoCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorLancamentoFuturoDebito'.
     * 
     * @param cdIndicadorLancamentoFuturoDebito the value of field
     * 'cdIndicadorLancamentoFuturoDebito'.
     */
    public void setCdIndicadorLancamentoFuturoDebito(java.lang.String cdIndicadorLancamentoFuturoDebito)
    {
        this._cdIndicadorLancamentoFuturoDebito = cdIndicadorLancamentoFuturoDebito;
    } //-- void setCdIndicadorLancamentoFuturoDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorLancamentoPagamento'.
     * 
     * @param cdIndicadorLancamentoPagamento the value of field
     * 'cdIndicadorLancamentoPagamento'.
     */
    public void setCdIndicadorLancamentoPagamento(java.lang.String cdIndicadorLancamentoPagamento)
    {
        this._cdIndicadorLancamentoPagamento = cdIndicadorLancamentoPagamento;
    } //-- void setCdIndicadorLancamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorLiberacaoLoteProcesso'.
     * 
     * @param cdIndicadorLiberacaoLoteProcesso the value of field
     * 'cdIndicadorLiberacaoLoteProcesso'.
     */
    public void setCdIndicadorLiberacaoLoteProcesso(java.lang.String cdIndicadorLiberacaoLoteProcesso)
    {
        this._cdIndicadorLiberacaoLoteProcesso = cdIndicadorLiberacaoLoteProcesso;
    } //-- void setCdIndicadorLiberacaoLoteProcesso(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorListaDebito'.
     * 
     * @param cdIndicadorListaDebito the value of field
     * 'cdIndicadorListaDebito'.
     */
    public void setCdIndicadorListaDebito(java.lang.String cdIndicadorListaDebito)
    {
        this._cdIndicadorListaDebito = cdIndicadorListaDebito;
    } //-- void setCdIndicadorListaDebito(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorManutencaoBaseRecadastro'.
     * 
     * @param cdIndicadorManutencaoBaseRecadastro the value of
     * field 'cdIndicadorManutencaoBaseRecadastro'.
     */
    public void setCdIndicadorManutencaoBaseRecadastro(java.lang.String cdIndicadorManutencaoBaseRecadastro)
    {
        this._cdIndicadorManutencaoBaseRecadastro = cdIndicadorManutencaoBaseRecadastro;
    } //-- void setCdIndicadorManutencaoBaseRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorMeioPagamentoCredito'.
     * 
     * @param cdIndicadorMeioPagamentoCredito the value of field
     * 'cdIndicadorMeioPagamentoCredito'.
     */
    public void setCdIndicadorMeioPagamentoCredito(java.lang.String cdIndicadorMeioPagamentoCredito)
    {
        this._cdIndicadorMeioPagamentoCredito = cdIndicadorMeioPagamentoCredito;
    } //-- void setCdIndicadorMeioPagamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorMensagemPerso'.
     * 
     * @param cdIndicadorMensagemPerso the value of field
     * 'cdIndicadorMensagemPerso'.
     */
    public void setCdIndicadorMensagemPerso(java.lang.String cdIndicadorMensagemPerso)
    {
        this._cdIndicadorMensagemPerso = cdIndicadorMensagemPerso;
    } //-- void setCdIndicadorMensagemPerso(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorMensagemRecadastroMidia'.
     * 
     * @param cdIndicadorMensagemRecadastroMidia the value of field
     * 'cdIndicadorMensagemRecadastroMidia'.
     */
    public void setCdIndicadorMensagemRecadastroMidia(java.lang.String cdIndicadorMensagemRecadastroMidia)
    {
        this._cdIndicadorMensagemRecadastroMidia = cdIndicadorMensagemRecadastroMidia;
    } //-- void setCdIndicadorMensagemRecadastroMidia(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorMidiaDisponivel'.
     * 
     * @param cdIndicadorMidiaDisponivel the value of field
     * 'cdIndicadorMidiaDisponivel'.
     */
    public void setCdIndicadorMidiaDisponivel(java.lang.String cdIndicadorMidiaDisponivel)
    {
        this._cdIndicadorMidiaDisponivel = cdIndicadorMidiaDisponivel;
    } //-- void setCdIndicadorMidiaDisponivel(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorMidiaMensagemRecadastro'.
     * 
     * @param cdIndicadorMidiaMensagemRecadastro the value of field
     * 'cdIndicadorMidiaMensagemRecadastro'.
     */
    public void setCdIndicadorMidiaMensagemRecadastro(java.lang.String cdIndicadorMidiaMensagemRecadastro)
    {
        this._cdIndicadorMidiaMensagemRecadastro = cdIndicadorMidiaMensagemRecadastro;
    } //-- void setCdIndicadorMidiaMensagemRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorMomentoAvisoRacadastro'.
     * 
     * @param cdIndicadorMomentoAvisoRacadastro the value of field
     * 'cdIndicadorMomentoAvisoRacadastro'.
     */
    public void setCdIndicadorMomentoAvisoRacadastro(java.lang.String cdIndicadorMomentoAvisoRacadastro)
    {
        this._cdIndicadorMomentoAvisoRacadastro = cdIndicadorMomentoAvisoRacadastro;
    } //-- void setCdIndicadorMomentoAvisoRacadastro(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorMomentoCreditoEfetivacao'.
     * 
     * @param cdIndicadorMomentoCreditoEfetivacao the value of
     * field 'cdIndicadorMomentoCreditoEfetivacao'.
     */
    public void setCdIndicadorMomentoCreditoEfetivacao(java.lang.String cdIndicadorMomentoCreditoEfetivacao)
    {
        this._cdIndicadorMomentoCreditoEfetivacao = cdIndicadorMomentoCreditoEfetivacao;
    } //-- void setCdIndicadorMomentoCreditoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorMomentoDebitoPagamento'.
     * 
     * @param cdIndicadorMomentoDebitoPagamento the value of field
     * 'cdIndicadorMomentoDebitoPagamento'.
     */
    public void setCdIndicadorMomentoDebitoPagamento(java.lang.String cdIndicadorMomentoDebitoPagamento)
    {
        this._cdIndicadorMomentoDebitoPagamento = cdIndicadorMomentoDebitoPagamento;
    } //-- void setCdIndicadorMomentoDebitoPagamento(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorMomentoFormularioRecadastro'.
     * 
     * @param cdIndicadorMomentoFormularioRecadastro the value of
     * field 'cdIndicadorMomentoFormularioRecadastro'.
     */
    public void setCdIndicadorMomentoFormularioRecadastro(java.lang.String cdIndicadorMomentoFormularioRecadastro)
    {
        this._cdIndicadorMomentoFormularioRecadastro = cdIndicadorMomentoFormularioRecadastro;
    } //-- void setCdIndicadorMomentoFormularioRecadastro(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorMomentoProcessamentoPagamento'.
     * 
     * @param cdIndicadorMomentoProcessamentoPagamento the value of
     * field 'cdIndicadorMomentoProcessamentoPagamento'.
     */
    public void setCdIndicadorMomentoProcessamentoPagamento(java.lang.String cdIndicadorMomentoProcessamentoPagamento)
    {
        this._cdIndicadorMomentoProcessamentoPagamento = cdIndicadorMomentoProcessamentoPagamento;
    } //-- void setCdIndicadorMomentoProcessamentoPagamento(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorNaturezaOperacaoPagamento'.
     * 
     * @param cdIndicadorNaturezaOperacaoPagamento the value of
     * field 'cdIndicadorNaturezaOperacaoPagamento'.
     */
    public void setCdIndicadorNaturezaOperacaoPagamento(java.lang.String cdIndicadorNaturezaOperacaoPagamento)
    {
        this._cdIndicadorNaturezaOperacaoPagamento = cdIndicadorNaturezaOperacaoPagamento;
    } //-- void setCdIndicadorNaturezaOperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPagamentoNaoUtilizado'.
     * 
     * @param cdIndicadorPagamentoNaoUtilizado the value of field
     * 'cdIndicadorPagamentoNaoUtilizado'.
     */
    public void setCdIndicadorPagamentoNaoUtilizado(java.lang.String cdIndicadorPagamentoNaoUtilizado)
    {
        this._cdIndicadorPagamentoNaoUtilizado = cdIndicadorPagamentoNaoUtilizado;
    } //-- void setCdIndicadorPagamentoNaoUtilizado(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorPercentualIndicadorReajusteTarifa'.
     * 
     * @param cdIndicadorPercentualIndicadorReajusteTarifa the
     * value of field 'cdIndicadorPercentualIndicadorReajusteTarifa'
     */
    public void setCdIndicadorPercentualIndicadorReajusteTarifa(java.lang.String cdIndicadorPercentualIndicadorReajusteTarifa)
    {
        this._cdIndicadorPercentualIndicadorReajusteTarifa = cdIndicadorPercentualIndicadorReajusteTarifa;
    } //-- void setCdIndicadorPercentualIndicadorReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorPercentualMaximoInconLote'.
     * 
     * @param cdIndicadorPercentualMaximoInconLote the value of
     * field 'cdIndicadorPercentualMaximoInconLote'.
     */
    public void setCdIndicadorPercentualMaximoInconLote(java.lang.String cdIndicadorPercentualMaximoInconLote)
    {
        this._cdIndicadorPercentualMaximoInconLote = cdIndicadorPercentualMaximoInconLote;
    } //-- void setCdIndicadorPercentualMaximoInconLote(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorPercentualReducaoTarifaCatalogo'.
     * 
     * @param cdIndicadorPercentualReducaoTarifaCatalogo the value
     * of field 'cdIndicadorPercentualReducaoTarifaCatalogo'.
     */
    public void setCdIndicadorPercentualReducaoTarifaCatalogo(java.lang.String cdIndicadorPercentualReducaoTarifaCatalogo)
    {
        this._cdIndicadorPercentualReducaoTarifaCatalogo = cdIndicadorPercentualReducaoTarifaCatalogo;
    } //-- void setCdIndicadorPercentualReducaoTarifaCatalogo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPerdcCobrancaTarifa'.
     * 
     * @param cdIndicadorPerdcCobrancaTarifa the value of field
     * 'cdIndicadorPerdcCobrancaTarifa'.
     */
    public void setCdIndicadorPerdcCobrancaTarifa(java.lang.String cdIndicadorPerdcCobrancaTarifa)
    {
        this._cdIndicadorPerdcCobrancaTarifa = cdIndicadorPerdcCobrancaTarifa;
    } //-- void setCdIndicadorPerdcCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPerdcComprovante'.
     * 
     * @param cdIndicadorPerdcComprovante the value of field
     * 'cdIndicadorPerdcComprovante'.
     */
    public void setCdIndicadorPerdcComprovante(java.lang.String cdIndicadorPerdcComprovante)
    {
        this._cdIndicadorPerdcComprovante = cdIndicadorPerdcComprovante;
    } //-- void setCdIndicadorPerdcComprovante(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPerdcConsultaVeiculo'.
     * 
     * @param cdIndicadorPerdcConsultaVeiculo the value of field
     * 'cdIndicadorPerdcConsultaVeiculo'.
     */
    public void setCdIndicadorPerdcConsultaVeiculo(java.lang.String cdIndicadorPerdcConsultaVeiculo)
    {
        this._cdIndicadorPerdcConsultaVeiculo = cdIndicadorPerdcConsultaVeiculo;
    } //-- void setCdIndicadorPerdcConsultaVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPerdcEnvioRemessa'.
     * 
     * @param cdIndicadorPerdcEnvioRemessa the value of field
     * 'cdIndicadorPerdcEnvioRemessa'.
     */
    public void setCdIndicadorPerdcEnvioRemessa(java.lang.String cdIndicadorPerdcEnvioRemessa)
    {
        this._cdIndicadorPerdcEnvioRemessa = cdIndicadorPerdcEnvioRemessa;
    } //-- void setCdIndicadorPerdcEnvioRemessa(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPerdcManutencaoProcd'.
     * 
     * @param cdIndicadorPerdcManutencaoProcd the value of field
     * 'cdIndicadorPerdcManutencaoProcd'.
     */
    public void setCdIndicadorPerdcManutencaoProcd(java.lang.String cdIndicadorPerdcManutencaoProcd)
    {
        this._cdIndicadorPerdcManutencaoProcd = cdIndicadorPerdcManutencaoProcd;
    } //-- void setCdIndicadorPerdcManutencaoProcd(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPeriodicidadeAviso'.
     * 
     * @param cdIndicadorPeriodicidadeAviso the value of field
     * 'cdIndicadorPeriodicidadeAviso'.
     */
    public void setCdIndicadorPeriodicidadeAviso(java.lang.String cdIndicadorPeriodicidadeAviso)
    {
        this._cdIndicadorPeriodicidadeAviso = cdIndicadorPeriodicidadeAviso;
    } //-- void setCdIndicadorPeriodicidadeAviso(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorPermissaoDebitoOnline'.
     * 
     * @param cdIndicadorPermissaoDebitoOnline the value of field
     * 'cdIndicadorPermissaoDebitoOnline'.
     */
    public void setCdIndicadorPermissaoDebitoOnline(java.lang.String cdIndicadorPermissaoDebitoOnline)
    {
        this._cdIndicadorPermissaoDebitoOnline = cdIndicadorPermissaoDebitoOnline;
    } //-- void setCdIndicadorPermissaoDebitoOnline(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorPrincipalEnquaRecadastro'.
     * 
     * @param cdIndicadorPrincipalEnquaRecadastro the value of
     * field 'cdIndicadorPrincipalEnquaRecadastro'.
     */
    public void setCdIndicadorPrincipalEnquaRecadastro(java.lang.String cdIndicadorPrincipalEnquaRecadastro)
    {
        this._cdIndicadorPrincipalEnquaRecadastro = cdIndicadorPrincipalEnquaRecadastro;
    } //-- void setCdIndicadorPrincipalEnquaRecadastro(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorPrioridadeEfetivacaoPagamento'.
     * 
     * @param cdIndicadorPrioridadeEfetivacaoPagamento the value of
     * field 'cdIndicadorPrioridadeEfetivacaoPagamento'.
     */
    public void setCdIndicadorPrioridadeEfetivacaoPagamento(java.lang.String cdIndicadorPrioridadeEfetivacaoPagamento)
    {
        this._cdIndicadorPrioridadeEfetivacaoPagamento = cdIndicadorPrioridadeEfetivacaoPagamento;
    } //-- void setCdIndicadorPrioridadeEfetivacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorRastreabilidadeNotaFiscal'.
     * 
     * @param cdIndicadorRastreabilidadeNotaFiscal the value of
     * field 'cdIndicadorRastreabilidadeNotaFiscal'.
     */
    public void setCdIndicadorRastreabilidadeNotaFiscal(java.lang.String cdIndicadorRastreabilidadeNotaFiscal)
    {
        this._cdIndicadorRastreabilidadeNotaFiscal = cdIndicadorRastreabilidadeNotaFiscal;
    } //-- void setCdIndicadorRastreabilidadeNotaFiscal(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorRastreabilidadeTituloTerceiro'.
     * 
     * @param cdIndicadorRastreabilidadeTituloTerceiro the value of
     * field 'cdIndicadorRastreabilidadeTituloTerceiro'.
     */
    public void setCdIndicadorRastreabilidadeTituloTerceiro(java.lang.String cdIndicadorRastreabilidadeTituloTerceiro)
    {
        this._cdIndicadorRastreabilidadeTituloTerceiro = cdIndicadorRastreabilidadeTituloTerceiro;
    } //-- void setCdIndicadorRastreabilidadeTituloTerceiro(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorRejeicaoAgendaLote'.
     * 
     * @param cdIndicadorRejeicaoAgendaLote the value of field
     * 'cdIndicadorRejeicaoAgendaLote'.
     */
    public void setCdIndicadorRejeicaoAgendaLote(java.lang.String cdIndicadorRejeicaoAgendaLote)
    {
        this._cdIndicadorRejeicaoAgendaLote = cdIndicadorRejeicaoAgendaLote;
    } //-- void setCdIndicadorRejeicaoAgendaLote(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorRejeicaoEfetivacaoLote'.
     * 
     * @param cdIndicadorRejeicaoEfetivacaoLote the value of field
     * 'cdIndicadorRejeicaoEfetivacaoLote'.
     */
    public void setCdIndicadorRejeicaoEfetivacaoLote(java.lang.String cdIndicadorRejeicaoEfetivacaoLote)
    {
        this._cdIndicadorRejeicaoEfetivacaoLote = cdIndicadorRejeicaoEfetivacaoLote;
    } //-- void setCdIndicadorRejeicaoEfetivacaoLote(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorRejeicaoLote'.
     * 
     * @param cdIndicadorRejeicaoLote the value of field
     * 'cdIndicadorRejeicaoLote'.
     */
    public void setCdIndicadorRejeicaoLote(java.lang.String cdIndicadorRejeicaoLote)
    {
        this._cdIndicadorRejeicaoLote = cdIndicadorRejeicaoLote;
    } //-- void setCdIndicadorRejeicaoLote(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorRetornoInternet'.
     * 
     * @param cdIndicadorRetornoInternet the value of field
     * 'cdIndicadorRetornoInternet'.
     */
    public void setCdIndicadorRetornoInternet(java.lang.String cdIndicadorRetornoInternet)
    {
        this._cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
    } //-- void setCdIndicadorRetornoInternet(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoCargaRecadastro'.
     * 
     * @param cdIndicadorTipoCargaRecadastro the value of field
     * 'cdIndicadorTipoCargaRecadastro'.
     */
    public void setCdIndicadorTipoCargaRecadastro(java.lang.String cdIndicadorTipoCargaRecadastro)
    {
        this._cdIndicadorTipoCargaRecadastro = cdIndicadorTipoCargaRecadastro;
    } //-- void setCdIndicadorTipoCargaRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoCataoSalario'.
     * 
     * @param cdIndicadorTipoCataoSalario the value of field
     * 'cdIndicadorTipoCataoSalario'.
     */
    public void setCdIndicadorTipoCataoSalario(java.lang.String cdIndicadorTipoCataoSalario)
    {
        this._cdIndicadorTipoCataoSalario = cdIndicadorTipoCataoSalario;
    } //-- void setCdIndicadorTipoCataoSalario(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoConsistenciaLista'.
     * 
     * @param cdIndicadorTipoConsistenciaLista the value of field
     * 'cdIndicadorTipoConsistenciaLista'.
     */
    public void setCdIndicadorTipoConsistenciaLista(java.lang.String cdIndicadorTipoConsistenciaLista)
    {
        this._cdIndicadorTipoConsistenciaLista = cdIndicadorTipoConsistenciaLista;
    } //-- void setCdIndicadorTipoConsistenciaLista(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorTipoConsultaComprovante'.
     * 
     * @param cdIndicadorTipoConsultaComprovante the value of field
     * 'cdIndicadorTipoConsultaComprovante'.
     */
    public void setCdIndicadorTipoConsultaComprovante(java.lang.String cdIndicadorTipoConsultaComprovante)
    {
        this._cdIndicadorTipoConsultaComprovante = cdIndicadorTipoConsultaComprovante;
    } //-- void setCdIndicadorTipoConsultaComprovante(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoContaFavorecidoT'.
     * 
     * @param cdIndicadorTipoContaFavorecidoT the value of field
     * 'cdIndicadorTipoContaFavorecidoT'.
     */
    public void setCdIndicadorTipoContaFavorecidoT(java.lang.String cdIndicadorTipoContaFavorecidoT)
    {
        this._cdIndicadorTipoContaFavorecidoT = cdIndicadorTipoContaFavorecidoT;
    } //-- void setCdIndicadorTipoContaFavorecidoT(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoDataFloat'.
     * 
     * @param cdIndicadorTipoDataFloat the value of field
     * 'cdIndicadorTipoDataFloat'.
     */
    public void setCdIndicadorTipoDataFloat(java.lang.String cdIndicadorTipoDataFloat)
    {
        this._cdIndicadorTipoDataFloat = cdIndicadorTipoDataFloat;
    } //-- void setCdIndicadorTipoDataFloat(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoDivergenciaVeiculo'.
     * 
     * @param cdIndicadorTipoDivergenciaVeiculo the value of field
     * 'cdIndicadorTipoDivergenciaVeiculo'.
     */
    public void setCdIndicadorTipoDivergenciaVeiculo(java.lang.String cdIndicadorTipoDivergenciaVeiculo)
    {
        this._cdIndicadorTipoDivergenciaVeiculo = cdIndicadorTipoDivergenciaVeiculo;
    } //-- void setCdIndicadorTipoDivergenciaVeiculo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoFormacaoLista'.
     * 
     * @param cdIndicadorTipoFormacaoLista the value of field
     * 'cdIndicadorTipoFormacaoLista'.
     */
    public void setCdIndicadorTipoFormacaoLista(java.lang.String cdIndicadorTipoFormacaoLista)
    {
        this._cdIndicadorTipoFormacaoLista = cdIndicadorTipoFormacaoLista;
    } //-- void setCdIndicadorTipoFormacaoLista(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoIdBeneficio'.
     * 
     * @param cdIndicadorTipoIdBeneficio the value of field
     * 'cdIndicadorTipoIdBeneficio'.
     */
    public void setCdIndicadorTipoIdBeneficio(java.lang.String cdIndicadorTipoIdBeneficio)
    {
        this._cdIndicadorTipoIdBeneficio = cdIndicadorTipoIdBeneficio;
    } //-- void setCdIndicadorTipoIdBeneficio(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorTipoIsncricaoFavorecido'.
     * 
     * @param cdIndicadorTipoIsncricaoFavorecido the value of field
     * 'cdIndicadorTipoIsncricaoFavorecido'.
     */
    public void setCdIndicadorTipoIsncricaoFavorecido(java.lang.String cdIndicadorTipoIsncricaoFavorecido)
    {
        this._cdIndicadorTipoIsncricaoFavorecido = cdIndicadorTipoIsncricaoFavorecido;
    } //-- void setCdIndicadorTipoIsncricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoLayoutArquivo'.
     * 
     * @param cdIndicadorTipoLayoutArquivo the value of field
     * 'cdIndicadorTipoLayoutArquivo'.
     */
    public void setCdIndicadorTipoLayoutArquivo(java.lang.String cdIndicadorTipoLayoutArquivo)
    {
        this._cdIndicadorTipoLayoutArquivo = cdIndicadorTipoLayoutArquivo;
    } //-- void setCdIndicadorTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoReajusteTarifa'.
     * 
     * @param cdIndicadorTipoReajusteTarifa the value of field
     * 'cdIndicadorTipoReajusteTarifa'.
     */
    public void setCdIndicadorTipoReajusteTarifa(java.lang.String cdIndicadorTipoReajusteTarifa)
    {
        this._cdIndicadorTipoReajusteTarifa = cdIndicadorTipoReajusteTarifa;
    } //-- void setCdIndicadorTipoReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorUtilizacaoFavorecidoControle'.
     * 
     * @param cdIndicadorUtilizacaoFavorecidoControle the value of
     * field 'cdIndicadorUtilizacaoFavorecidoControle'.
     */
    public void setCdIndicadorUtilizacaoFavorecidoControle(java.lang.String cdIndicadorUtilizacaoFavorecidoControle)
    {
        this._cdIndicadorUtilizacaoFavorecidoControle = cdIndicadorUtilizacaoFavorecidoControle;
    } //-- void setCdIndicadorUtilizacaoFavorecidoControle(java.lang.String) 

    /**
     * Sets the value of field
     * 'cdIndicadorValidacaoNomeFavorecido'.
     * 
     * @param cdIndicadorValidacaoNomeFavorecido the value of field
     * 'cdIndicadorValidacaoNomeFavorecido'.
     */
    public void setCdIndicadorValidacaoNomeFavorecido(java.lang.String cdIndicadorValidacaoNomeFavorecido)
    {
        this._cdIndicadorValidacaoNomeFavorecido = cdIndicadorValidacaoNomeFavorecido;
    } //-- void setCdIndicadorValidacaoNomeFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorAreaResrd'.
     * 
     * @param dsIndicadorAreaResrd the value of field
     * 'dsIndicadorAreaResrd'.
     */
    public void setDsIndicadorAreaResrd(java.lang.String dsIndicadorAreaResrd)
    {
        this._dsIndicadorAreaResrd = dsIndicadorAreaResrd;
    } //-- void setDsIndicadorAreaResrd(java.lang.String) 

    /**
     * Sets the value of field 'dtIndicadorEnquaContaSalario'.
     * 
     * @param dtIndicadorEnquaContaSalario the value of field
     * 'dtIndicadorEnquaContaSalario'.
     */
    public void setDtIndicadorEnquaContaSalario(java.lang.String dtIndicadorEnquaContaSalario)
    {
        this._dtIndicadorEnquaContaSalario = dtIndicadorEnquaContaSalario;
    } //-- void setDtIndicadorEnquaContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'dtIndicadorFimAcertoRecadastro'.
     * 
     * @param dtIndicadorFimAcertoRecadastro the value of field
     * 'dtIndicadorFimAcertoRecadastro'.
     */
    public void setDtIndicadorFimAcertoRecadastro(java.lang.String dtIndicadorFimAcertoRecadastro)
    {
        this._dtIndicadorFimAcertoRecadastro = dtIndicadorFimAcertoRecadastro;
    } //-- void setDtIndicadorFimAcertoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'dtIndicadorFimRecadastroBeneficio'.
     * 
     * @param dtIndicadorFimRecadastroBeneficio the value of field
     * 'dtIndicadorFimRecadastroBeneficio'.
     */
    public void setDtIndicadorFimRecadastroBeneficio(java.lang.String dtIndicadorFimRecadastroBeneficio)
    {
        this._dtIndicadorFimRecadastroBeneficio = dtIndicadorFimRecadastroBeneficio;
    } //-- void setDtIndicadorFimRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtIndicadorInicioBloqueioPplta'.
     * 
     * @param dtIndicadorInicioBloqueioPplta the value of field
     * 'dtIndicadorInicioBloqueioPplta'.
     */
    public void setDtIndicadorInicioBloqueioPplta(java.lang.String dtIndicadorInicioBloqueioPplta)
    {
        this._dtIndicadorInicioBloqueioPplta = dtIndicadorInicioBloqueioPplta;
    } //-- void setDtIndicadorInicioBloqueioPplta(java.lang.String) 

    /**
     * Sets the value of field
     * 'dtIndicadorInicioRastreabilidadeTitulo'.
     * 
     * @param dtIndicadorInicioRastreabilidadeTitulo the value of
     * field 'dtIndicadorInicioRastreabilidadeTitulo'.
     */
    public void setDtIndicadorInicioRastreabilidadeTitulo(java.lang.String dtIndicadorInicioRastreabilidadeTitulo)
    {
        this._dtIndicadorInicioRastreabilidadeTitulo = dtIndicadorInicioRastreabilidadeTitulo;
    } //-- void setDtIndicadorInicioRastreabilidadeTitulo(java.lang.String) 

    /**
     * Sets the value of field
     * 'dtIndicadorInicioRecadastroBeneficio'.
     * 
     * @param dtIndicadorInicioRecadastroBeneficio the value of
     * field 'dtIndicadorInicioRecadastroBeneficio'.
     */
    public void setDtIndicadorInicioRecadastroBeneficio(java.lang.String dtIndicadorInicioRecadastroBeneficio)
    {
        this._dtIndicadorInicioRecadastroBeneficio = dtIndicadorInicioRecadastroBeneficio;
    } //-- void setDtIndicadorInicioRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'dtIndicadorLimiteVinculoCarga'.
     * 
     * @param dtIndicadorLimiteVinculoCarga the value of field
     * 'dtIndicadorLimiteVinculoCarga'.
     */
    public void setDtIndicadorLimiteVinculoCarga(java.lang.String dtIndicadorLimiteVinculoCarga)
    {
        this._dtIndicadorLimiteVinculoCarga = dtIndicadorLimiteVinculoCarga;
    } //-- void setDtIndicadorLimiteVinculoCarga(java.lang.String) 

    /**
     * Sets the value of field 'dtIndicadorinicioAcertoRecadastro'.
     * 
     * @param dtIndicadorinicioAcertoRecadastro the value of field
     * 'dtIndicadorinicioAcertoRecadastro'.
     */
    public void setDtIndicadorinicioAcertoRecadastro(java.lang.String dtIndicadorinicioAcertoRecadastro)
    {
        this._dtIndicadorinicioAcertoRecadastro = dtIndicadorinicioAcertoRecadastro;
    } //-- void setDtIndicadorinicioAcertoRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field
     * 'nrIndicadorFechamentoApuracaoTarifa'.
     * 
     * @param nrIndicadorFechamentoApuracaoTarifa the value of
     * field 'nrIndicadorFechamentoApuracaoTarifa'.
     */
    public void setNrIndicadorFechamentoApuracaoTarifa(java.lang.String nrIndicadorFechamentoApuracaoTarifa)
    {
        this._nrIndicadorFechamentoApuracaoTarifa = nrIndicadorFechamentoApuracaoTarifa;
    } //-- void setNrIndicadorFechamentoApuracaoTarifa(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorAntecedencia'.
     * 
     * @param qtIndicadorAntecedencia the value of field
     * 'qtIndicadorAntecedencia'.
     */
    public void setQtIndicadorAntecedencia(java.lang.String qtIndicadorAntecedencia)
    {
        this._qtIndicadorAntecedencia = qtIndicadorAntecedencia;
    } //-- void setQtIndicadorAntecedencia(java.lang.String) 

    /**
     * Sets the value of field
     * 'qtIndicadorAnteriorVencimentoComprovado'.
     * 
     * @param qtIndicadorAnteriorVencimentoComprovado the value of
     * field 'qtIndicadorAnteriorVencimentoComprovado'.
     */
    public void setQtIndicadorAnteriorVencimentoComprovado(java.lang.String qtIndicadorAnteriorVencimentoComprovado)
    {
        this._qtIndicadorAnteriorVencimentoComprovado = qtIndicadorAnteriorVencimentoComprovado;
    } //-- void setQtIndicadorAnteriorVencimentoComprovado(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorDiaCobrancaTarifa'.
     * 
     * @param qtIndicadorDiaCobrancaTarifa the value of field
     * 'qtIndicadorDiaCobrancaTarifa'.
     */
    public void setQtIndicadorDiaCobrancaTarifa(java.lang.String qtIndicadorDiaCobrancaTarifa)
    {
        this._qtIndicadorDiaCobrancaTarifa = qtIndicadorDiaCobrancaTarifa;
    } //-- void setQtIndicadorDiaCobrancaTarifa(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorDiaExpiracao'.
     * 
     * @param qtIndicadorDiaExpiracao the value of field
     * 'qtIndicadorDiaExpiracao'.
     */
    public void setQtIndicadorDiaExpiracao(java.lang.String qtIndicadorDiaExpiracao)
    {
        this._qtIndicadorDiaExpiracao = qtIndicadorDiaExpiracao;
    } //-- void setQtIndicadorDiaExpiracao(java.lang.String) 

    /**
     * Sets the value of field
     * 'qtIndicadorDiaInatividadeFavorecido'.
     * 
     * @param qtIndicadorDiaInatividadeFavorecido the value of
     * field 'qtIndicadorDiaInatividadeFavorecido'.
     */
    public void setQtIndicadorDiaInatividadeFavorecido(java.lang.String qtIndicadorDiaInatividadeFavorecido)
    {
        this._qtIndicadorDiaInatividadeFavorecido = qtIndicadorDiaInatividadeFavorecido;
    } //-- void setQtIndicadorDiaInatividadeFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorDiaRepiqConsulta'.
     * 
     * @param qtIndicadorDiaRepiqConsulta the value of field
     * 'qtIndicadorDiaRepiqConsulta'.
     */
    public void setQtIndicadorDiaRepiqConsulta(java.lang.String qtIndicadorDiaRepiqConsulta)
    {
        this._qtIndicadorDiaRepiqConsulta = qtIndicadorDiaRepiqConsulta;
    } //-- void setQtIndicadorDiaRepiqConsulta(java.lang.String) 

    /**
     * Sets the value of field
     * 'qtIndicadorEtapasRecadastroBeneficio'.
     * 
     * @param qtIndicadorEtapasRecadastroBeneficio the value of
     * field 'qtIndicadorEtapasRecadastroBeneficio'.
     */
    public void setQtIndicadorEtapasRecadastroBeneficio(java.lang.String qtIndicadorEtapasRecadastroBeneficio)
    {
        this._qtIndicadorEtapasRecadastroBeneficio = qtIndicadorEtapasRecadastroBeneficio;
    } //-- void setQtIndicadorEtapasRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field
     * 'qtIndicadorFaseRecadastroBeneficio'.
     * 
     * @param qtIndicadorFaseRecadastroBeneficio the value of field
     * 'qtIndicadorFaseRecadastroBeneficio'.
     */
    public void setQtIndicadorFaseRecadastroBeneficio(java.lang.String qtIndicadorFaseRecadastroBeneficio)
    {
        this._qtIndicadorFaseRecadastroBeneficio = qtIndicadorFaseRecadastroBeneficio;
    } //-- void setQtIndicadorFaseRecadastroBeneficio(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorLimiteLinha'.
     * 
     * @param qtIndicadorLimiteLinha the value of field
     * 'qtIndicadorLimiteLinha'.
     */
    public void setQtIndicadorLimiteLinha(java.lang.String qtIndicadorLimiteLinha)
    {
        this._qtIndicadorLimiteLinha = qtIndicadorLimiteLinha;
    } //-- void setQtIndicadorLimiteLinha(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorLimiteSolicitacaoCatao'.
     * 
     * @param qtIndicadorLimiteSolicitacaoCatao the value of field
     * 'qtIndicadorLimiteSolicitacaoCatao'.
     */
    public void setQtIndicadorLimiteSolicitacaoCatao(java.lang.String qtIndicadorLimiteSolicitacaoCatao)
    {
        this._qtIndicadorLimiteSolicitacaoCatao = qtIndicadorLimiteSolicitacaoCatao;
    } //-- void setQtIndicadorLimiteSolicitacaoCatao(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorMaximaInconLote'.
     * 
     * @param qtIndicadorMaximaInconLote the value of field
     * 'qtIndicadorMaximaInconLote'.
     */
    public void setQtIndicadorMaximaInconLote(java.lang.String qtIndicadorMaximaInconLote)
    {
        this._qtIndicadorMaximaInconLote = qtIndicadorMaximaInconLote;
    } //-- void setQtIndicadorMaximaInconLote(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorMaximaTituloVencido'.
     * 
     * @param qtIndicadorMaximaTituloVencido the value of field
     * 'qtIndicadorMaximaTituloVencido'.
     */
    public void setQtIndicadorMaximaTituloVencido(java.lang.String qtIndicadorMaximaTituloVencido)
    {
        this._qtIndicadorMaximaTituloVencido = qtIndicadorMaximaTituloVencido;
    } //-- void setQtIndicadorMaximaTituloVencido(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorMesComprovante'.
     * 
     * @param qtIndicadorMesComprovante the value of field
     * 'qtIndicadorMesComprovante'.
     */
    public void setQtIndicadorMesComprovante(java.lang.String qtIndicadorMesComprovante)
    {
        this._qtIndicadorMesComprovante = qtIndicadorMesComprovante;
    } //-- void setQtIndicadorMesComprovante(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorMesEtapaRecadastro'.
     * 
     * @param qtIndicadorMesEtapaRecadastro the value of field
     * 'qtIndicadorMesEtapaRecadastro'.
     */
    public void setQtIndicadorMesEtapaRecadastro(java.lang.String qtIndicadorMesEtapaRecadastro)
    {
        this._qtIndicadorMesEtapaRecadastro = qtIndicadorMesEtapaRecadastro;
    } //-- void setQtIndicadorMesEtapaRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorMesFaseRecadastro'.
     * 
     * @param qtIndicadorMesFaseRecadastro the value of field
     * 'qtIndicadorMesFaseRecadastro'.
     */
    public void setQtIndicadorMesFaseRecadastro(java.lang.String qtIndicadorMesFaseRecadastro)
    {
        this._qtIndicadorMesFaseRecadastro = qtIndicadorMesFaseRecadastro;
    } //-- void setQtIndicadorMesFaseRecadastro(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorMesReajusteTarifa'.
     * 
     * @param qtIndicadorMesReajusteTarifa the value of field
     * 'qtIndicadorMesReajusteTarifa'.
     */
    public void setQtIndicadorMesReajusteTarifa(java.lang.String qtIndicadorMesReajusteTarifa)
    {
        this._qtIndicadorMesReajusteTarifa = qtIndicadorMesReajusteTarifa;
    } //-- void setQtIndicadorMesReajusteTarifa(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorViaAviso'.
     * 
     * @param qtIndicadorViaAviso the value of field
     * 'qtIndicadorViaAviso'.
     */
    public void setQtIndicadorViaAviso(java.lang.String qtIndicadorViaAviso)
    {
        this._qtIndicadorViaAviso = qtIndicadorViaAviso;
    } //-- void setQtIndicadorViaAviso(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorViaCobranca'.
     * 
     * @param qtIndicadorViaCobranca the value of field
     * 'qtIndicadorViaCobranca'.
     */
    public void setQtIndicadorViaCobranca(java.lang.String qtIndicadorViaCobranca)
    {
        this._qtIndicadorViaCobranca = qtIndicadorViaCobranca;
    } //-- void setQtIndicadorViaCobranca(java.lang.String) 

    /**
     * Sets the value of field 'qtIndicadorViaComprovante'.
     * 
     * @param qtIndicadorViaComprovante the value of field
     * 'qtIndicadorViaComprovante'.
     */
    public void setQtIndicadorViaComprovante(java.lang.String qtIndicadorViaComprovante)
    {
        this._qtIndicadorViaComprovante = qtIndicadorViaComprovante;
    } //-- void setQtIndicadorViaComprovante(java.lang.String) 

    /**
     * Sets the value of field 'vlIndicadorFavorecidoNaoCadastro'.
     * 
     * @param vlIndicadorFavorecidoNaoCadastro the value of field
     * 'vlIndicadorFavorecidoNaoCadastro'.
     */
    public void setVlIndicadorFavorecidoNaoCadastro(java.lang.String vlIndicadorFavorecidoNaoCadastro)
    {
        this._vlIndicadorFavorecidoNaoCadastro = vlIndicadorFavorecidoNaoCadastro;
    } //-- void setVlIndicadorFavorecidoNaoCadastro(java.lang.String) 

    /**
     * Sets the value of field 'vlIndicadorLimiteDiaPagamento'.
     * 
     * @param vlIndicadorLimiteDiaPagamento the value of field
     * 'vlIndicadorLimiteDiaPagamento'.
     */
    public void setVlIndicadorLimiteDiaPagamento(java.lang.String vlIndicadorLimiteDiaPagamento)
    {
        this._vlIndicadorLimiteDiaPagamento = vlIndicadorLimiteDiaPagamento;
    } //-- void setVlIndicadorLimiteDiaPagamento(java.lang.String) 

    /**
     * Sets the value of field
     * 'vlIndicadorLimiteIndividualPagamento'.
     * 
     * @param vlIndicadorLimiteIndividualPagamento the value of
     * field 'vlIndicadorLimiteIndividualPagamento'.
     */
    public void setVlIndicadorLimiteIndividualPagamento(java.lang.String vlIndicadorLimiteIndividualPagamento)
    {
        this._vlIndicadorLimiteIndividualPagamento = vlIndicadorLimiteIndividualPagamento;
    } //-- void setVlIndicadorLimiteIndividualPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return VerificarAtributosServicoModalidadeResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.response.VerificarAtributosServicoModalidadeResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.response.VerificarAtributosServicoModalidadeResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.response.VerificarAtributosServicoModalidadeResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.verificaratributosservicomodalidade.response.VerificarAtributosServicoModalidadeResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
