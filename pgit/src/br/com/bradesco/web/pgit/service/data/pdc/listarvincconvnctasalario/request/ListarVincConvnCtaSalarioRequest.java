/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarVincConvnCtaSalarioRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarVincConvnCtaSalarioRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maxOcorrencias
     */
    private int _maxOcorrencias = 0;

    /**
     * keeps track of state for field: _maxOcorrencias
     */
    private boolean _has_maxOcorrencias;

    /**
     * Field _codPessoaJuridica
     */
    private long _codPessoaJuridica = 0;

    /**
     * keeps track of state for field: _codPessoaJuridica
     */
    private boolean _has_codPessoaJuridica;

    /**
     * Field _codTipoContratoNegocio
     */
    private int _codTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _codTipoContratoNegocio
     */
    private boolean _has_codTipoContratoNegocio;

    /**
     * Field _numSeqContratoNegocio
     */
    private long _numSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _numSeqContratoNegocio
     */
    private boolean _has_numSeqContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarVincConvnCtaSalarioRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.request.ListarVincConvnCtaSalarioRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCodPessoaJuridica
     * 
     */
    public void deleteCodPessoaJuridica()
    {
        this._has_codPessoaJuridica= false;
    } //-- void deleteCodPessoaJuridica() 

    /**
     * Method deleteCodTipoContratoNegocio
     * 
     */
    public void deleteCodTipoContratoNegocio()
    {
        this._has_codTipoContratoNegocio= false;
    } //-- void deleteCodTipoContratoNegocio() 

    /**
     * Method deleteMaxOcorrencias
     * 
     */
    public void deleteMaxOcorrencias()
    {
        this._has_maxOcorrencias= false;
    } //-- void deleteMaxOcorrencias() 

    /**
     * Method deleteNumSeqContratoNegocio
     * 
     */
    public void deleteNumSeqContratoNegocio()
    {
        this._has_numSeqContratoNegocio= false;
    } //-- void deleteNumSeqContratoNegocio() 

    /**
     * Returns the value of field 'codPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'codPessoaJuridica'.
     */
    public long getCodPessoaJuridica()
    {
        return this._codPessoaJuridica;
    } //-- long getCodPessoaJuridica() 

    /**
     * Returns the value of field 'codTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'codTipoContratoNegocio'.
     */
    public int getCodTipoContratoNegocio()
    {
        return this._codTipoContratoNegocio;
    } //-- int getCodTipoContratoNegocio() 

    /**
     * Returns the value of field 'maxOcorrencias'.
     * 
     * @return int
     * @return the value of field 'maxOcorrencias'.
     */
    public int getMaxOcorrencias()
    {
        return this._maxOcorrencias;
    } //-- int getMaxOcorrencias() 

    /**
     * Returns the value of field 'numSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'numSeqContratoNegocio'.
     */
    public long getNumSeqContratoNegocio()
    {
        return this._numSeqContratoNegocio;
    } //-- long getNumSeqContratoNegocio() 

    /**
     * Method hasCodPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodPessoaJuridica()
    {
        return this._has_codPessoaJuridica;
    } //-- boolean hasCodPessoaJuridica() 

    /**
     * Method hasCodTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodTipoContratoNegocio()
    {
        return this._has_codTipoContratoNegocio;
    } //-- boolean hasCodTipoContratoNegocio() 

    /**
     * Method hasMaxOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaxOcorrencias()
    {
        return this._has_maxOcorrencias;
    } //-- boolean hasMaxOcorrencias() 

    /**
     * Method hasNumSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumSeqContratoNegocio()
    {
        return this._has_numSeqContratoNegocio;
    } //-- boolean hasNumSeqContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'codPessoaJuridica'.
     * 
     * @param codPessoaJuridica the value of field
     * 'codPessoaJuridica'.
     */
    public void setCodPessoaJuridica(long codPessoaJuridica)
    {
        this._codPessoaJuridica = codPessoaJuridica;
        this._has_codPessoaJuridica = true;
    } //-- void setCodPessoaJuridica(long) 

    /**
     * Sets the value of field 'codTipoContratoNegocio'.
     * 
     * @param codTipoContratoNegocio the value of field
     * 'codTipoContratoNegocio'.
     */
    public void setCodTipoContratoNegocio(int codTipoContratoNegocio)
    {
        this._codTipoContratoNegocio = codTipoContratoNegocio;
        this._has_codTipoContratoNegocio = true;
    } //-- void setCodTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'maxOcorrencias'.
     * 
     * @param maxOcorrencias the value of field 'maxOcorrencias'.
     */
    public void setMaxOcorrencias(int maxOcorrencias)
    {
        this._maxOcorrencias = maxOcorrencias;
        this._has_maxOcorrencias = true;
    } //-- void setMaxOcorrencias(int) 

    /**
     * Sets the value of field 'numSeqContratoNegocio'.
     * 
     * @param numSeqContratoNegocio the value of field
     * 'numSeqContratoNegocio'.
     */
    public void setNumSeqContratoNegocio(long numSeqContratoNegocio)
    {
        this._numSeqContratoNegocio = numSeqContratoNegocio;
        this._has_numSeqContratoNegocio = true;
    } //-- void setNumSeqContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarVincConvnCtaSalarioRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.request.ListarVincConvnCtaSalarioRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.request.ListarVincConvnCtaSalarioRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.request.ListarVincConvnCtaSalarioRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarvincconvnctasalario.request.ListarVincConvnCtaSalarioRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
