/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarMotivoBloqueioFavorecidoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarMotivoBloqueioFavorecidoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencia
     */
    private int _numeroOcorrencia = 0;

    /**
     * keeps track of state for field: _numeroOcorrencia
     */
    private boolean _has_numeroOcorrencia;

    /**
     * Field _cdSituacaoVinculacaoConta
     */
    private int _cdSituacaoVinculacaoConta = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoConta
     */
    private boolean _has_cdSituacaoVinculacaoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarMotivoBloqueioFavorecidoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.request.ListarMotivoBloqueioFavorecidoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoVinculacaoConta
     * 
     */
    public void deleteCdSituacaoVinculacaoConta()
    {
        this._has_cdSituacaoVinculacaoConta= false;
    } //-- void deleteCdSituacaoVinculacaoConta() 

    /**
     * Method deleteNumeroOcorrencia
     * 
     */
    public void deleteNumeroOcorrencia()
    {
        this._has_numeroOcorrencia= false;
    } //-- void deleteNumeroOcorrencia() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoConta'.
     */
    public int getCdSituacaoVinculacaoConta()
    {
        return this._cdSituacaoVinculacaoConta;
    } //-- int getCdSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'numeroOcorrencia'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencia'.
     */
    public int getNumeroOcorrencia()
    {
        return this._numeroOcorrencia;
    } //-- int getNumeroOcorrencia() 

    /**
     * Method hasCdSituacaoVinculacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoConta()
    {
        return this._has_cdSituacaoVinculacaoConta;
    } //-- boolean hasCdSituacaoVinculacaoConta() 

    /**
     * Method hasNumeroOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencia()
    {
        return this._has_numeroOcorrencia;
    } //-- boolean hasNumeroOcorrencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @param cdSituacaoVinculacaoConta the value of field
     * 'cdSituacaoVinculacaoConta'.
     */
    public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta)
    {
        this._cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
        this._has_cdSituacaoVinculacaoConta = true;
    } //-- void setCdSituacaoVinculacaoConta(int) 

    /**
     * Sets the value of field 'numeroOcorrencia'.
     * 
     * @param numeroOcorrencia the value of field 'numeroOcorrencia'
     */
    public void setNumeroOcorrencia(int numeroOcorrencia)
    {
        this._numeroOcorrencia = numeroOcorrencia;
        this._has_numeroOcorrencia = true;
    } //-- void setNumeroOcorrencia(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarMotivoBloqueioFavorecidoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.request.ListarMotivoBloqueioFavorecidoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.request.ListarMotivoBloqueioFavorecidoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.request.ListarMotivoBloqueioFavorecidoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.request.ListarMotivoBloqueioFavorecidoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
