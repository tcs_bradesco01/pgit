/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharMsgLayoutArqRetornoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharMsgLayoutArqRetornoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdMensagemArquivoRetorno
     */
    private java.lang.String _cdMensagemArquivoRetorno;

    /**
     * Field _cdTipoMensagemRetorno
     */
    private int _cdTipoMensagemRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemRetorno
     */
    private boolean _has_cdTipoMensagemRetorno;

    /**
     * Field _nrPosicaoInicialMensagem
     */
    private int _nrPosicaoInicialMensagem = 0;

    /**
     * keeps track of state for field: _nrPosicaoInicialMensagem
     */
    private boolean _has_nrPosicaoInicialMensagem;

    /**
     * Field _nrPosicaoFinalMensagem
     */
    private int _nrPosicaoFinalMensagem = 0;

    /**
     * keeps track of state for field: _nrPosicaoFinalMensagem
     */
    private boolean _has_nrPosicaoFinalMensagem;

    /**
     * Field _cdNivelMensagemLayout
     */
    private int _cdNivelMensagemLayout = 0;

    /**
     * keeps track of state for field: _cdNivelMensagemLayout
     */
    private boolean _has_cdNivelMensagemLayout;

    /**
     * Field _dsMensagemLayoutRetorno
     */
    private java.lang.String _dsMensagemLayoutRetorno;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenticacaoSegurancaInclusao
     */
    private java.lang.String _cdAutenticacaoSegurancaInclusao;

    /**
     * Field _nmOperacaoFluxoInclusao
     */
    private java.lang.String _nmOperacaoFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenticacaoSegurancaManutencao
     */
    private java.lang.String _cdAutenticacaoSegurancaManutencao;

    /**
     * Field _nmOperacaoFluxoManutencao
     */
    private java.lang.String _nmOperacaoFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharMsgLayoutArqRetornoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.response.DetalharMsgLayoutArqRetornoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdNivelMensagemLayout
     * 
     */
    public void deleteCdNivelMensagemLayout()
    {
        this._has_cdNivelMensagemLayout= false;
    } //-- void deleteCdNivelMensagemLayout() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoMensagemRetorno
     * 
     */
    public void deleteCdTipoMensagemRetorno()
    {
        this._has_cdTipoMensagemRetorno= false;
    } //-- void deleteCdTipoMensagemRetorno() 

    /**
     * Method deleteNrPosicaoFinalMensagem
     * 
     */
    public void deleteNrPosicaoFinalMensagem()
    {
        this._has_nrPosicaoFinalMensagem= false;
    } //-- void deleteNrPosicaoFinalMensagem() 

    /**
     * Method deleteNrPosicaoInicialMensagem
     * 
     */
    public void deleteNrPosicaoInicialMensagem()
    {
        this._has_nrPosicaoInicialMensagem= false;
    } //-- void deleteNrPosicaoInicialMensagem() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenticacaoSegurancaInclusao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaInclusao()
    {
        return this._cdAutenticacaoSegurancaInclusao;
    } //-- java.lang.String getCdAutenticacaoSegurancaInclusao() 

    /**
     * Returns the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @return String
     * @return the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public java.lang.String getCdAutenticacaoSegurancaManutencao()
    {
        return this._cdAutenticacaoSegurancaManutencao;
    } //-- java.lang.String getCdAutenticacaoSegurancaManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'cdMensagemArquivoRetorno'.
     */
    public java.lang.String getCdMensagemArquivoRetorno()
    {
        return this._cdMensagemArquivoRetorno;
    } //-- java.lang.String getCdMensagemArquivoRetorno() 

    /**
     * Returns the value of field 'cdNivelMensagemLayout'.
     * 
     * @return int
     * @return the value of field 'cdNivelMensagemLayout'.
     */
    public int getCdNivelMensagemLayout()
    {
        return this._cdNivelMensagemLayout;
    } //-- int getCdNivelMensagemLayout() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoMensagemRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemRetorno'.
     */
    public int getCdTipoMensagemRetorno()
    {
        return this._cdTipoMensagemRetorno;
    } //-- int getCdTipoMensagemRetorno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsMensagemLayoutRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMensagemLayoutRetorno'.
     */
    public java.lang.String getDsMensagemLayoutRetorno()
    {
        return this._dsMensagemLayoutRetorno;
    } //-- java.lang.String getDsMensagemLayoutRetorno() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoInclusao'.
     */
    public java.lang.String getNmOperacaoFluxoInclusao()
    {
        return this._nmOperacaoFluxoInclusao;
    } //-- java.lang.String getNmOperacaoFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxoManutencao'.
     */
    public java.lang.String getNmOperacaoFluxoManutencao()
    {
        return this._nmOperacaoFluxoManutencao;
    } //-- java.lang.String getNmOperacaoFluxoManutencao() 

    /**
     * Returns the value of field 'nrPosicaoFinalMensagem'.
     * 
     * @return int
     * @return the value of field 'nrPosicaoFinalMensagem'.
     */
    public int getNrPosicaoFinalMensagem()
    {
        return this._nrPosicaoFinalMensagem;
    } //-- int getNrPosicaoFinalMensagem() 

    /**
     * Returns the value of field 'nrPosicaoInicialMensagem'.
     * 
     * @return int
     * @return the value of field 'nrPosicaoInicialMensagem'.
     */
    public int getNrPosicaoInicialMensagem()
    {
        return this._nrPosicaoInicialMensagem;
    } //-- int getNrPosicaoInicialMensagem() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdNivelMensagemLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelMensagemLayout()
    {
        return this._has_cdNivelMensagemLayout;
    } //-- boolean hasCdNivelMensagemLayout() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoMensagemRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemRetorno()
    {
        return this._has_cdTipoMensagemRetorno;
    } //-- boolean hasCdTipoMensagemRetorno() 

    /**
     * Method hasNrPosicaoFinalMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPosicaoFinalMensagem()
    {
        return this._has_nrPosicaoFinalMensagem;
    } //-- boolean hasNrPosicaoFinalMensagem() 

    /**
     * Method hasNrPosicaoInicialMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrPosicaoInicialMensagem()
    {
        return this._has_nrPosicaoInicialMensagem;
    } //-- boolean hasNrPosicaoInicialMensagem() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaInclusao'.
     * 
     * @param cdAutenticacaoSegurancaInclusao the value of field
     * 'cdAutenticacaoSegurancaInclusao'.
     */
    public void setCdAutenticacaoSegurancaInclusao(java.lang.String cdAutenticacaoSegurancaInclusao)
    {
        this._cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
    } //-- void setCdAutenticacaoSegurancaInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenticacaoSegurancaManutencao'.
     * 
     * @param cdAutenticacaoSegurancaManutencao the value of field
     * 'cdAutenticacaoSegurancaManutencao'.
     */
    public void setCdAutenticacaoSegurancaManutencao(java.lang.String cdAutenticacaoSegurancaManutencao)
    {
        this._cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
    } //-- void setCdAutenticacaoSegurancaManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdMensagemArquivoRetorno'.
     * 
     * @param cdMensagemArquivoRetorno the value of field
     * 'cdMensagemArquivoRetorno'.
     */
    public void setCdMensagemArquivoRetorno(java.lang.String cdMensagemArquivoRetorno)
    {
        this._cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
    } //-- void setCdMensagemArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdNivelMensagemLayout'.
     * 
     * @param cdNivelMensagemLayout the value of field
     * 'cdNivelMensagemLayout'.
     */
    public void setCdNivelMensagemLayout(int cdNivelMensagemLayout)
    {
        this._cdNivelMensagemLayout = cdNivelMensagemLayout;
        this._has_cdNivelMensagemLayout = true;
    } //-- void setCdNivelMensagemLayout(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoMensagemRetorno'.
     * 
     * @param cdTipoMensagemRetorno the value of field
     * 'cdTipoMensagemRetorno'.
     */
    public void setCdTipoMensagemRetorno(int cdTipoMensagemRetorno)
    {
        this._cdTipoMensagemRetorno = cdTipoMensagemRetorno;
        this._has_cdTipoMensagemRetorno = true;
    } //-- void setCdTipoMensagemRetorno(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsMensagemLayoutRetorno'.
     * 
     * @param dsMensagemLayoutRetorno the value of field
     * 'dsMensagemLayoutRetorno'.
     */
    public void setDsMensagemLayoutRetorno(java.lang.String dsMensagemLayoutRetorno)
    {
        this._dsMensagemLayoutRetorno = dsMensagemLayoutRetorno;
    } //-- void setDsMensagemLayoutRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoInclusao'.
     * 
     * @param nmOperacaoFluxoInclusao the value of field
     * 'nmOperacaoFluxoInclusao'.
     */
    public void setNmOperacaoFluxoInclusao(java.lang.String nmOperacaoFluxoInclusao)
    {
        this._nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
    } //-- void setNmOperacaoFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxoManutencao'.
     * 
     * @param nmOperacaoFluxoManutencao the value of field
     * 'nmOperacaoFluxoManutencao'.
     */
    public void setNmOperacaoFluxoManutencao(java.lang.String nmOperacaoFluxoManutencao)
    {
        this._nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
    } //-- void setNmOperacaoFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrPosicaoFinalMensagem'.
     * 
     * @param nrPosicaoFinalMensagem the value of field
     * 'nrPosicaoFinalMensagem'.
     */
    public void setNrPosicaoFinalMensagem(int nrPosicaoFinalMensagem)
    {
        this._nrPosicaoFinalMensagem = nrPosicaoFinalMensagem;
        this._has_nrPosicaoFinalMensagem = true;
    } //-- void setNrPosicaoFinalMensagem(int) 

    /**
     * Sets the value of field 'nrPosicaoInicialMensagem'.
     * 
     * @param nrPosicaoInicialMensagem the value of field
     * 'nrPosicaoInicialMensagem'.
     */
    public void setNrPosicaoInicialMensagem(int nrPosicaoInicialMensagem)
    {
        this._nrPosicaoInicialMensagem = nrPosicaoInicialMensagem;
        this._has_nrPosicaoInicialMensagem = true;
    } //-- void setNrPosicaoInicialMensagem(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharMsgLayoutArqRetornoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.response.DetalharMsgLayoutArqRetornoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.response.DetalharMsgLayoutArqRetornoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.response.DetalharMsgLayoutArqRetornoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharmsglayoutarqretorno.response.DetalharMsgLayoutArqRetornoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
