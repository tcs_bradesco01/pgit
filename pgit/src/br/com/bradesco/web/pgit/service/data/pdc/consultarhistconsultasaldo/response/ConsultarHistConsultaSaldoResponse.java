/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarHistConsultaSaldoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarHistConsultaSaldoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _dsBancoDebito
     */
    private java.lang.String _dsBancoDebito;

    /**
     * Field _dsAgenciaDebito
     */
    private java.lang.String _dsAgenciaDebito;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _dsSituacaoContrato
     */
    private java.lang.String _dsSituacaoContrato;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _dtVencimento
     */
    private java.lang.String _dtVencimento;

    /**
     * Field _nrArquivoRemessa
     */
    private long _nrArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrArquivoRemessa
     */
    private boolean _has_nrArquivoRemessa;

    /**
     * Field _cdSituacaoPagamentoCliente
     */
    private int _cdSituacaoPagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdSituacaoPagamentoCliente
     */
    private boolean _has_cdSituacaoPagamentoCliente;

    /**
     * Field _dsSituacaoPagamentoCliente
     */
    private java.lang.String _dsSituacaoPagamentoCliente;

    /**
     * Field _cdMotivoPagamentoCliente
     */
    private int _cdMotivoPagamentoCliente = 0;

    /**
     * keeps track of state for field: _cdMotivoPagamentoCliente
     */
    private boolean _has_cdMotivoPagamentoCliente;

    /**
     * Field _dsMotivoPagamentoCliente
     */
    private java.lang.String _dsMotivoPagamentoCliente;

    /**
     * Field _cdBancoCredito
     */
    private int _cdBancoCredito = 0;

    /**
     * keeps track of state for field: _cdBancoCredito
     */
    private boolean _has_cdBancoCredito;

    /**
     * Field _dsBancoCredito
     */
    private java.lang.String _dsBancoCredito;

    /**
     * Field _cdAgenciaBancariaCredito
     */
    private int _cdAgenciaBancariaCredito = 0;

    /**
     * keeps track of state for field: _cdAgenciaBancariaCredito
     */
    private boolean _has_cdAgenciaBancariaCredito;

    /**
     * Field _cdDigitoAgenciaCredito
     */
    private int _cdDigitoAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdDigitoAgenciaCredito
     */
    private boolean _has_cdDigitoAgenciaCredito;

    /**
     * Field _dsAgenciaCredito
     */
    private java.lang.String _dsAgenciaCredito;

    /**
     * Field _cdContaBancariaCredito
     */
    private long _cdContaBancariaCredito = 0;

    /**
     * keeps track of state for field: _cdContaBancariaCredito
     */
    private boolean _has_cdContaBancariaCredito;

    /**
     * Field _cdDigitoContaCredito
     */
    private java.lang.String _cdDigitoContaCredito;

    /**
     * Field _cdTipoContaCredito
     */
    private java.lang.String _cdTipoContaCredito;

    /**
     * Field _cdTipoInscricaoFavorecido
     */
    private java.lang.String _cdTipoInscricaoFavorecido;

    /**
     * Field _inscricaoFavorecido
     */
    private long _inscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _inscricaoFavorecido
     */
    private boolean _has_inscricaoFavorecido;

    /**
     * Field _numeroConsultas
     */
    private int _numeroConsultas = 0;

    /**
     * keeps track of state for field: _numeroConsultas
     */
    private boolean _has_numeroConsultas;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarHistConsultaSaldoResponse() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.ConsultarHistConsultaSaldoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 200)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 200");
        }
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_ocorrenciasList.size() < 200)) {
            throw new IndexOutOfBoundsException("addOcorrencias has a maximum of 200");
        }
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias) 

    /**
     * Method deleteCdAgenciaBancariaCredito
     * 
     */
    public void deleteCdAgenciaBancariaCredito()
    {
        this._has_cdAgenciaBancariaCredito= false;
    } //-- void deleteCdAgenciaBancariaCredito() 

    /**
     * Method deleteCdBancoCredito
     * 
     */
    public void deleteCdBancoCredito()
    {
        this._has_cdBancoCredito= false;
    } //-- void deleteCdBancoCredito() 

    /**
     * Method deleteCdContaBancariaCredito
     * 
     */
    public void deleteCdContaBancariaCredito()
    {
        this._has_cdContaBancariaCredito= false;
    } //-- void deleteCdContaBancariaCredito() 

    /**
     * Method deleteCdDigitoAgenciaCredito
     * 
     */
    public void deleteCdDigitoAgenciaCredito()
    {
        this._has_cdDigitoAgenciaCredito= false;
    } //-- void deleteCdDigitoAgenciaCredito() 

    /**
     * Method deleteCdMotivoPagamentoCliente
     * 
     */
    public void deleteCdMotivoPagamentoCliente()
    {
        this._has_cdMotivoPagamentoCliente= false;
    } //-- void deleteCdMotivoPagamentoCliente() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdSituacaoPagamentoCliente
     * 
     */
    public void deleteCdSituacaoPagamentoCliente()
    {
        this._has_cdSituacaoPagamentoCliente= false;
    } //-- void deleteCdSituacaoPagamentoCliente() 

    /**
     * Method deleteInscricaoFavorecido
     * 
     */
    public void deleteInscricaoFavorecido()
    {
        this._has_inscricaoFavorecido= false;
    } //-- void deleteInscricaoFavorecido() 

    /**
     * Method deleteNrArquivoRemessa
     * 
     */
    public void deleteNrArquivoRemessa()
    {
        this._has_nrArquivoRemessa= false;
    } //-- void deleteNrArquivoRemessa() 

    /**
     * Method deleteNumeroConsultas
     * 
     */
    public void deleteNumeroConsultas()
    {
        this._has_numeroConsultas= false;
    } //-- void deleteNumeroConsultas() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdAgenciaBancariaCredito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaBancariaCredito'.
     */
    public int getCdAgenciaBancariaCredito()
    {
        return this._cdAgenciaBancariaCredito;
    } //-- int getCdAgenciaBancariaCredito() 

    /**
     * Returns the value of field 'cdBancoCredito'.
     * 
     * @return int
     * @return the value of field 'cdBancoCredito'.
     */
    public int getCdBancoCredito()
    {
        return this._cdBancoCredito;
    } //-- int getCdBancoCredito() 

    /**
     * Returns the value of field 'cdContaBancariaCredito'.
     * 
     * @return long
     * @return the value of field 'cdContaBancariaCredito'.
     */
    public long getCdContaBancariaCredito()
    {
        return this._cdContaBancariaCredito;
    } //-- long getCdContaBancariaCredito() 

    /**
     * Returns the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgenciaCredito'.
     */
    public int getCdDigitoAgenciaCredito()
    {
        return this._cdDigitoAgenciaCredito;
    } //-- int getCdDigitoAgenciaCredito() 

    /**
     * Returns the value of field 'cdDigitoContaCredito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaCredito'.
     */
    public java.lang.String getCdDigitoContaCredito()
    {
        return this._cdDigitoContaCredito;
    } //-- java.lang.String getCdDigitoContaCredito() 

    /**
     * Returns the value of field 'cdMotivoPagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdMotivoPagamentoCliente'.
     */
    public int getCdMotivoPagamentoCliente()
    {
        return this._cdMotivoPagamentoCliente;
    } //-- int getCdMotivoPagamentoCliente() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdSituacaoPagamentoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPagamentoCliente'.
     */
    public int getCdSituacaoPagamentoCliente()
    {
        return this._cdSituacaoPagamentoCliente;
    } //-- int getCdSituacaoPagamentoCliente() 

    /**
     * Returns the value of field 'cdTipoContaCredito'.
     * 
     * @return String
     * @return the value of field 'cdTipoContaCredito'.
     */
    public java.lang.String getCdTipoContaCredito()
    {
        return this._cdTipoContaCredito;
    } //-- java.lang.String getCdTipoContaCredito() 

    /**
     * Returns the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdTipoInscricaoFavorecido'.
     */
    public java.lang.String getCdTipoInscricaoFavorecido()
    {
        return this._cdTipoInscricaoFavorecido;
    } //-- java.lang.String getCdTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaCredito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaCredito'.
     */
    public java.lang.String getDsAgenciaCredito()
    {
        return this._dsAgenciaCredito;
    } //-- java.lang.String getDsAgenciaCredito() 

    /**
     * Returns the value of field 'dsAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDebito'.
     */
    public java.lang.String getDsAgenciaDebito()
    {
        return this._dsAgenciaDebito;
    } //-- java.lang.String getDsAgenciaDebito() 

    /**
     * Returns the value of field 'dsBancoCredito'.
     * 
     * @return String
     * @return the value of field 'dsBancoCredito'.
     */
    public java.lang.String getDsBancoCredito()
    {
        return this._dsBancoCredito;
    } //-- java.lang.String getDsBancoCredito() 

    /**
     * Returns the value of field 'dsBancoDebito'.
     * 
     * @return String
     * @return the value of field 'dsBancoDebito'.
     */
    public java.lang.String getDsBancoDebito()
    {
        return this._dsBancoDebito;
    } //-- java.lang.String getDsBancoDebito() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsMotivoPagamentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsMotivoPagamentoCliente'.
     */
    public java.lang.String getDsMotivoPagamentoCliente()
    {
        return this._dsMotivoPagamentoCliente;
    } //-- java.lang.String getDsMotivoPagamentoCliente() 

    /**
     * Returns the value of field 'dsSituacaoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContrato'.
     */
    public java.lang.String getDsSituacaoContrato()
    {
        return this._dsSituacaoContrato;
    } //-- java.lang.String getDsSituacaoContrato() 

    /**
     * Returns the value of field 'dsSituacaoPagamentoCliente'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPagamentoCliente'.
     */
    public java.lang.String getDsSituacaoPagamentoCliente()
    {
        return this._dsSituacaoPagamentoCliente;
    } //-- java.lang.String getDsSituacaoPagamentoCliente() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'dtVencimento'.
     * 
     * @return String
     * @return the value of field 'dtVencimento'.
     */
    public java.lang.String getDtVencimento()
    {
        return this._dtVencimento;
    } //-- java.lang.String getDtVencimento() 

    /**
     * Returns the value of field 'inscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'inscricaoFavorecido'.
     */
    public long getInscricaoFavorecido()
    {
        return this._inscricaoFavorecido;
    } //-- long getInscricaoFavorecido() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemessa'.
     */
    public long getNrArquivoRemessa()
    {
        return this._nrArquivoRemessa;
    } //-- long getNrArquivoRemessa() 

    /**
     * Returns the value of field 'numeroConsultas'.
     * 
     * @return int
     * @return the value of field 'numeroConsultas'.
     */
    public int getNumeroConsultas()
    {
        return this._numeroConsultas;
    } //-- int getNumeroConsultas() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdAgenciaBancariaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaBancariaCredito()
    {
        return this._has_cdAgenciaBancariaCredito;
    } //-- boolean hasCdAgenciaBancariaCredito() 

    /**
     * Method hasCdBancoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoCredito()
    {
        return this._has_cdBancoCredito;
    } //-- boolean hasCdBancoCredito() 

    /**
     * Method hasCdContaBancariaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancariaCredito()
    {
        return this._has_cdContaBancariaCredito;
    } //-- boolean hasCdContaBancariaCredito() 

    /**
     * Method hasCdDigitoAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgenciaCredito()
    {
        return this._has_cdDigitoAgenciaCredito;
    } //-- boolean hasCdDigitoAgenciaCredito() 

    /**
     * Method hasCdMotivoPagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoPagamentoCliente()
    {
        return this._has_cdMotivoPagamentoCliente;
    } //-- boolean hasCdMotivoPagamentoCliente() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdSituacaoPagamentoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPagamentoCliente()
    {
        return this._has_cdSituacaoPagamentoCliente;
    } //-- boolean hasCdSituacaoPagamentoCliente() 

    /**
     * Method hasInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasInscricaoFavorecido()
    {
        return this._has_inscricaoFavorecido;
    } //-- boolean hasInscricaoFavorecido() 

    /**
     * Method hasNrArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemessa()
    {
        return this._has_nrArquivoRemessa;
    } //-- boolean hasNrArquivoRemessa() 

    /**
     * Method hasNumeroConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroConsultas()
    {
        return this._has_numeroConsultas;
    } //-- boolean hasNumeroConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdAgenciaBancariaCredito'.
     * 
     * @param cdAgenciaBancariaCredito the value of field
     * 'cdAgenciaBancariaCredito'.
     */
    public void setCdAgenciaBancariaCredito(int cdAgenciaBancariaCredito)
    {
        this._cdAgenciaBancariaCredito = cdAgenciaBancariaCredito;
        this._has_cdAgenciaBancariaCredito = true;
    } //-- void setCdAgenciaBancariaCredito(int) 

    /**
     * Sets the value of field 'cdBancoCredito'.
     * 
     * @param cdBancoCredito the value of field 'cdBancoCredito'.
     */
    public void setCdBancoCredito(int cdBancoCredito)
    {
        this._cdBancoCredito = cdBancoCredito;
        this._has_cdBancoCredito = true;
    } //-- void setCdBancoCredito(int) 

    /**
     * Sets the value of field 'cdContaBancariaCredito'.
     * 
     * @param cdContaBancariaCredito the value of field
     * 'cdContaBancariaCredito'.
     */
    public void setCdContaBancariaCredito(long cdContaBancariaCredito)
    {
        this._cdContaBancariaCredito = cdContaBancariaCredito;
        this._has_cdContaBancariaCredito = true;
    } //-- void setCdContaBancariaCredito(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @param cdDigitoAgenciaCredito the value of field
     * 'cdDigitoAgenciaCredito'.
     */
    public void setCdDigitoAgenciaCredito(int cdDigitoAgenciaCredito)
    {
        this._cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
        this._has_cdDigitoAgenciaCredito = true;
    } //-- void setCdDigitoAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdDigitoContaCredito'.
     * 
     * @param cdDigitoContaCredito the value of field
     * 'cdDigitoContaCredito'.
     */
    public void setCdDigitoContaCredito(java.lang.String cdDigitoContaCredito)
    {
        this._cdDigitoContaCredito = cdDigitoContaCredito;
    } //-- void setCdDigitoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdMotivoPagamentoCliente'.
     * 
     * @param cdMotivoPagamentoCliente the value of field
     * 'cdMotivoPagamentoCliente'.
     */
    public void setCdMotivoPagamentoCliente(int cdMotivoPagamentoCliente)
    {
        this._cdMotivoPagamentoCliente = cdMotivoPagamentoCliente;
        this._has_cdMotivoPagamentoCliente = true;
    } //-- void setCdMotivoPagamentoCliente(int) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdSituacaoPagamentoCliente'.
     * 
     * @param cdSituacaoPagamentoCliente the value of field
     * 'cdSituacaoPagamentoCliente'.
     */
    public void setCdSituacaoPagamentoCliente(int cdSituacaoPagamentoCliente)
    {
        this._cdSituacaoPagamentoCliente = cdSituacaoPagamentoCliente;
        this._has_cdSituacaoPagamentoCliente = true;
    } //-- void setCdSituacaoPagamentoCliente(int) 

    /**
     * Sets the value of field 'cdTipoContaCredito'.
     * 
     * @param cdTipoContaCredito the value of field
     * 'cdTipoContaCredito'.
     */
    public void setCdTipoContaCredito(java.lang.String cdTipoContaCredito)
    {
        this._cdTipoContaCredito = cdTipoContaCredito;
    } //-- void setCdTipoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @param cdTipoInscricaoFavorecido the value of field
     * 'cdTipoInscricaoFavorecido'.
     */
    public void setCdTipoInscricaoFavorecido(java.lang.String cdTipoInscricaoFavorecido)
    {
        this._cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
    } //-- void setCdTipoInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaCredito'.
     * 
     * @param dsAgenciaCredito the value of field 'dsAgenciaCredito'
     */
    public void setDsAgenciaCredito(java.lang.String dsAgenciaCredito)
    {
        this._dsAgenciaCredito = dsAgenciaCredito;
    } //-- void setDsAgenciaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDebito'.
     * 
     * @param dsAgenciaDebito the value of field 'dsAgenciaDebito'.
     */
    public void setDsAgenciaDebito(java.lang.String dsAgenciaDebito)
    {
        this._dsAgenciaDebito = dsAgenciaDebito;
    } //-- void setDsAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoCredito'.
     * 
     * @param dsBancoCredito the value of field 'dsBancoCredito'.
     */
    public void setDsBancoCredito(java.lang.String dsBancoCredito)
    {
        this._dsBancoCredito = dsBancoCredito;
    } //-- void setDsBancoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDebito'.
     * 
     * @param dsBancoDebito the value of field 'dsBancoDebito'.
     */
    public void setDsBancoDebito(java.lang.String dsBancoDebito)
    {
        this._dsBancoDebito = dsBancoDebito;
    } //-- void setDsBancoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoPagamentoCliente'.
     * 
     * @param dsMotivoPagamentoCliente the value of field
     * 'dsMotivoPagamentoCliente'.
     */
    public void setDsMotivoPagamentoCliente(java.lang.String dsMotivoPagamentoCliente)
    {
        this._dsMotivoPagamentoCliente = dsMotivoPagamentoCliente;
    } //-- void setDsMotivoPagamentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoContrato'.
     * 
     * @param dsSituacaoContrato the value of field
     * 'dsSituacaoContrato'.
     */
    public void setDsSituacaoContrato(java.lang.String dsSituacaoContrato)
    {
        this._dsSituacaoContrato = dsSituacaoContrato;
    } //-- void setDsSituacaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPagamentoCliente'.
     * 
     * @param dsSituacaoPagamentoCliente the value of field
     * 'dsSituacaoPagamentoCliente'.
     */
    public void setDsSituacaoPagamentoCliente(java.lang.String dsSituacaoPagamentoCliente)
    {
        this._dsSituacaoPagamentoCliente = dsSituacaoPagamentoCliente;
    } //-- void setDsSituacaoPagamentoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dtVencimento'.
     * 
     * @param dtVencimento the value of field 'dtVencimento'.
     */
    public void setDtVencimento(java.lang.String dtVencimento)
    {
        this._dtVencimento = dtVencimento;
    } //-- void setDtVencimento(java.lang.String) 

    /**
     * Sets the value of field 'inscricaoFavorecido'.
     * 
     * @param inscricaoFavorecido the value of field
     * 'inscricaoFavorecido'.
     */
    public void setInscricaoFavorecido(long inscricaoFavorecido)
    {
        this._inscricaoFavorecido = inscricaoFavorecido;
        this._has_inscricaoFavorecido = true;
    } //-- void setInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemessa'.
     * 
     * @param nrArquivoRemessa the value of field 'nrArquivoRemessa'
     */
    public void setNrArquivoRemessa(long nrArquivoRemessa)
    {
        this._nrArquivoRemessa = nrArquivoRemessa;
        this._has_nrArquivoRemessa = true;
    } //-- void setNrArquivoRemessa(long) 

    /**
     * Sets the value of field 'numeroConsultas'.
     * 
     * @param numeroConsultas the value of field 'numeroConsultas'.
     */
    public void setNumeroConsultas(int numeroConsultas)
    {
        this._numeroConsultas = numeroConsultas;
        this._has_numeroConsultas = true;
    } //-- void setNumeroConsultas(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        if (!(index < 200)) {
            throw new IndexOutOfBoundsException("setOcorrencias has a maximum of 200");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarHistConsultaSaldoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.ConsultarHistConsultaSaldoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.ConsultarHistConsultaSaldoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.ConsultarHistConsultaSaldoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.ConsultarHistConsultaSaldoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
