/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cpfCnpj
     */
    private long _cpfCnpj = 0;

    /**
     * keeps track of state for field: _cpfCnpj
     */
    private boolean _has_cpfCnpj;

    /**
     * Field _filialCnpj
     */
    private int _filialCnpj = 0;

    /**
     * keeps track of state for field: _filialCnpj
     */
    private boolean _has_filialCnpj;

    /**
     * Field _controle
     */
    private int _controle = 0;

    /**
     * keeps track of state for field: _controle
     */
    private boolean _has_controle;

    /**
     * Field _nomeRazao
     */
    private java.lang.String _nomeRazao;

    /**
     * Field _codigoSistemaLegado
     */
    private java.lang.String _codigoSistemaLegado;

    /**
     * Field _descricaoProdutoServico
     */
    private java.lang.String _descricaoProdutoServico;

    /**
     * Field _codigoPerfilComunicacao
     */
    private long _codigoPerfilComunicacao = 0;

    /**
     * keeps track of state for field: _codigoPerfilComunicacao
     */
    private boolean _has_codigoPerfilComunicacao;

    /**
     * Field _numeroArquivoRetorno
     */
    private long _numeroArquivoRetorno = 0;

    /**
     * keeps track of state for field: _numeroArquivoRetorno
     */
    private boolean _has_numeroArquivoRetorno;

    /**
     * Field _horaInclusaoArquivoRetorno
     */
    private java.lang.String _horaInclusaoArquivoRetorno;

    /**
     * Field _codigoBancoDebito
     */
    private int _codigoBancoDebito = 0;

    /**
     * keeps track of state for field: _codigoBancoDebito
     */
    private boolean _has_codigoBancoDebito;

    /**
     * Field _agenciaBancariaDebito
     */
    private int _agenciaBancariaDebito = 0;

    /**
     * keeps track of state for field: _agenciaBancariaDebito
     */
    private boolean _has_agenciaBancariaDebito;

    /**
     * Field _contaRazaoDebito
     */
    private int _contaRazaoDebito = 0;

    /**
     * keeps track of state for field: _contaRazaoDebito
     */
    private boolean _has_contaRazaoDebito;

    /**
     * Field _contaBancariaDebito
     */
    private long _contaBancariaDebito = 0;

    /**
     * keeps track of state for field: _contaBancariaDebito
     */
    private boolean _has_contaBancariaDebito;

    /**
     * Field _codigoLancamentoContaDebito
     */
    private int _codigoLancamentoContaDebito = 0;

    /**
     * keeps track of state for field: _codigoLancamentoContaDebito
     */
    private boolean _has_codigoLancamentoContaDebito;

    /**
     * Field _codigoMeioTransmissao
     */
    private int _codigoMeioTransmissao = 0;

    /**
     * keeps track of state for field: _codigoMeioTransmissao
     */
    private boolean _has_codigoMeioTransmissao;

    /**
     * Field _descricaoMeioTransmissao
     */
    private java.lang.String _descricaoMeioTransmissao;

    /**
     * Field _horaGeracaoRetorno
     */
    private java.lang.String _horaGeracaoRetorno;

    /**
     * Field _codigoTipoRetorno
     */
    private int _codigoTipoRetorno = 0;

    /**
     * keeps track of state for field: _codigoTipoRetorno
     */
    private boolean _has_codigoTipoRetorno;

    /**
     * Field _descricaoTipoRetorno
     */
    private java.lang.String _descricaoTipoRetorno;

    /**
     * Field _codigoSituacaoRetorno
     */
    private int _codigoSituacaoRetorno = 0;

    /**
     * keeps track of state for field: _codigoSituacaoRetorno
     */
    private boolean _has_codigoSituacaoRetorno;

    /**
     * Field _descricaoSituacaoRetorno
     */
    private java.lang.String _descricaoSituacaoRetorno;

    /**
     * Field _codigoTipoLayout
     */
    private int _codigoTipoLayout = 0;

    /**
     * keeps track of state for field: _codigoTipoLayout
     */
    private boolean _has_codigoTipoLayout;

    /**
     * Field _descricaoTipoLayout
     */
    private java.lang.String _descricaoTipoLayout;

    /**
     * Field _codigoAmbiente
     */
    private java.lang.String _codigoAmbiente;

    /**
     * Field _descricaoAmbiente
     */
    private java.lang.String _descricaoAmbiente;

    /**
     * Field _quantidadeRegistro
     */
    private long _quantidadeRegistro = 0;

    /**
     * keeps track of state for field: _quantidadeRegistro
     */
    private boolean _has_quantidadeRegistro;

    /**
     * Field _valorRegistro
     */
    private double _valorRegistro = 0;

    /**
     * keeps track of state for field: _valorRegistro
     */
    private boolean _has_valorRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteAgenciaBancariaDebito
     * 
     */
    public void deleteAgenciaBancariaDebito()
    {
        this._has_agenciaBancariaDebito= false;
    } //-- void deleteAgenciaBancariaDebito() 

    /**
     * Method deleteCodigoBancoDebito
     * 
     */
    public void deleteCodigoBancoDebito()
    {
        this._has_codigoBancoDebito= false;
    } //-- void deleteCodigoBancoDebito() 

    /**
     * Method deleteCodigoLancamentoContaDebito
     * 
     */
    public void deleteCodigoLancamentoContaDebito()
    {
        this._has_codigoLancamentoContaDebito= false;
    } //-- void deleteCodigoLancamentoContaDebito() 

    /**
     * Method deleteCodigoMeioTransmissao
     * 
     */
    public void deleteCodigoMeioTransmissao()
    {
        this._has_codigoMeioTransmissao= false;
    } //-- void deleteCodigoMeioTransmissao() 

    /**
     * Method deleteCodigoPerfilComunicacao
     * 
     */
    public void deleteCodigoPerfilComunicacao()
    {
        this._has_codigoPerfilComunicacao= false;
    } //-- void deleteCodigoPerfilComunicacao() 

    /**
     * Method deleteCodigoSituacaoRetorno
     * 
     */
    public void deleteCodigoSituacaoRetorno()
    {
        this._has_codigoSituacaoRetorno= false;
    } //-- void deleteCodigoSituacaoRetorno() 

    /**
     * Method deleteCodigoTipoLayout
     * 
     */
    public void deleteCodigoTipoLayout()
    {
        this._has_codigoTipoLayout= false;
    } //-- void deleteCodigoTipoLayout() 

    /**
     * Method deleteCodigoTipoRetorno
     * 
     */
    public void deleteCodigoTipoRetorno()
    {
        this._has_codigoTipoRetorno= false;
    } //-- void deleteCodigoTipoRetorno() 

    /**
     * Method deleteContaBancariaDebito
     * 
     */
    public void deleteContaBancariaDebito()
    {
        this._has_contaBancariaDebito= false;
    } //-- void deleteContaBancariaDebito() 

    /**
     * Method deleteContaRazaoDebito
     * 
     */
    public void deleteContaRazaoDebito()
    {
        this._has_contaRazaoDebito= false;
    } //-- void deleteContaRazaoDebito() 

    /**
     * Method deleteControle
     * 
     */
    public void deleteControle()
    {
        this._has_controle= false;
    } //-- void deleteControle() 

    /**
     * Method deleteCpfCnpj
     * 
     */
    public void deleteCpfCnpj()
    {
        this._has_cpfCnpj= false;
    } //-- void deleteCpfCnpj() 

    /**
     * Method deleteFilialCnpj
     * 
     */
    public void deleteFilialCnpj()
    {
        this._has_filialCnpj= false;
    } //-- void deleteFilialCnpj() 

    /**
     * Method deleteNumeroArquivoRetorno
     * 
     */
    public void deleteNumeroArquivoRetorno()
    {
        this._has_numeroArquivoRetorno= false;
    } //-- void deleteNumeroArquivoRetorno() 

    /**
     * Method deleteQuantidadeRegistro
     * 
     */
    public void deleteQuantidadeRegistro()
    {
        this._has_quantidadeRegistro= false;
    } //-- void deleteQuantidadeRegistro() 

    /**
     * Method deleteValorRegistro
     * 
     */
    public void deleteValorRegistro()
    {
        this._has_valorRegistro= false;
    } //-- void deleteValorRegistro() 

    /**
     * Returns the value of field 'agenciaBancariaDebito'.
     * 
     * @return int
     * @return the value of field 'agenciaBancariaDebito'.
     */
    public int getAgenciaBancariaDebito()
    {
        return this._agenciaBancariaDebito;
    } //-- int getAgenciaBancariaDebito() 

    /**
     * Returns the value of field 'codigoAmbiente'.
     * 
     * @return String
     * @return the value of field 'codigoAmbiente'.
     */
    public java.lang.String getCodigoAmbiente()
    {
        return this._codigoAmbiente;
    } //-- java.lang.String getCodigoAmbiente() 

    /**
     * Returns the value of field 'codigoBancoDebito'.
     * 
     * @return int
     * @return the value of field 'codigoBancoDebito'.
     */
    public int getCodigoBancoDebito()
    {
        return this._codigoBancoDebito;
    } //-- int getCodigoBancoDebito() 

    /**
     * Returns the value of field 'codigoLancamentoContaDebito'.
     * 
     * @return int
     * @return the value of field 'codigoLancamentoContaDebito'.
     */
    public int getCodigoLancamentoContaDebito()
    {
        return this._codigoLancamentoContaDebito;
    } //-- int getCodigoLancamentoContaDebito() 

    /**
     * Returns the value of field 'codigoMeioTransmissao'.
     * 
     * @return int
     * @return the value of field 'codigoMeioTransmissao'.
     */
    public int getCodigoMeioTransmissao()
    {
        return this._codigoMeioTransmissao;
    } //-- int getCodigoMeioTransmissao() 

    /**
     * Returns the value of field 'codigoPerfilComunicacao'.
     * 
     * @return long
     * @return the value of field 'codigoPerfilComunicacao'.
     */
    public long getCodigoPerfilComunicacao()
    {
        return this._codigoPerfilComunicacao;
    } //-- long getCodigoPerfilComunicacao() 

    /**
     * Returns the value of field 'codigoSistemaLegado'.
     * 
     * @return String
     * @return the value of field 'codigoSistemaLegado'.
     */
    public java.lang.String getCodigoSistemaLegado()
    {
        return this._codigoSistemaLegado;
    } //-- java.lang.String getCodigoSistemaLegado() 

    /**
     * Returns the value of field 'codigoSituacaoRetorno'.
     * 
     * @return int
     * @return the value of field 'codigoSituacaoRetorno'.
     */
    public int getCodigoSituacaoRetorno()
    {
        return this._codigoSituacaoRetorno;
    } //-- int getCodigoSituacaoRetorno() 

    /**
     * Returns the value of field 'codigoTipoLayout'.
     * 
     * @return int
     * @return the value of field 'codigoTipoLayout'.
     */
    public int getCodigoTipoLayout()
    {
        return this._codigoTipoLayout;
    } //-- int getCodigoTipoLayout() 

    /**
     * Returns the value of field 'codigoTipoRetorno'.
     * 
     * @return int
     * @return the value of field 'codigoTipoRetorno'.
     */
    public int getCodigoTipoRetorno()
    {
        return this._codigoTipoRetorno;
    } //-- int getCodigoTipoRetorno() 

    /**
     * Returns the value of field 'contaBancariaDebito'.
     * 
     * @return long
     * @return the value of field 'contaBancariaDebito'.
     */
    public long getContaBancariaDebito()
    {
        return this._contaBancariaDebito;
    } //-- long getContaBancariaDebito() 

    /**
     * Returns the value of field 'contaRazaoDebito'.
     * 
     * @return int
     * @return the value of field 'contaRazaoDebito'.
     */
    public int getContaRazaoDebito()
    {
        return this._contaRazaoDebito;
    } //-- int getContaRazaoDebito() 

    /**
     * Returns the value of field 'controle'.
     * 
     * @return int
     * @return the value of field 'controle'.
     */
    public int getControle()
    {
        return this._controle;
    } //-- int getControle() 

    /**
     * Returns the value of field 'cpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cpfCnpj'.
     */
    public long getCpfCnpj()
    {
        return this._cpfCnpj;
    } //-- long getCpfCnpj() 

    /**
     * Returns the value of field 'descricaoAmbiente'.
     * 
     * @return String
     * @return the value of field 'descricaoAmbiente'.
     */
    public java.lang.String getDescricaoAmbiente()
    {
        return this._descricaoAmbiente;
    } //-- java.lang.String getDescricaoAmbiente() 

    /**
     * Returns the value of field 'descricaoMeioTransmissao'.
     * 
     * @return String
     * @return the value of field 'descricaoMeioTransmissao'.
     */
    public java.lang.String getDescricaoMeioTransmissao()
    {
        return this._descricaoMeioTransmissao;
    } //-- java.lang.String getDescricaoMeioTransmissao() 

    /**
     * Returns the value of field 'descricaoProdutoServico'.
     * 
     * @return String
     * @return the value of field 'descricaoProdutoServico'.
     */
    public java.lang.String getDescricaoProdutoServico()
    {
        return this._descricaoProdutoServico;
    } //-- java.lang.String getDescricaoProdutoServico() 

    /**
     * Returns the value of field 'descricaoSituacaoRetorno'.
     * 
     * @return String
     * @return the value of field 'descricaoSituacaoRetorno'.
     */
    public java.lang.String getDescricaoSituacaoRetorno()
    {
        return this._descricaoSituacaoRetorno;
    } //-- java.lang.String getDescricaoSituacaoRetorno() 

    /**
     * Returns the value of field 'descricaoTipoLayout'.
     * 
     * @return String
     * @return the value of field 'descricaoTipoLayout'.
     */
    public java.lang.String getDescricaoTipoLayout()
    {
        return this._descricaoTipoLayout;
    } //-- java.lang.String getDescricaoTipoLayout() 

    /**
     * Returns the value of field 'descricaoTipoRetorno'.
     * 
     * @return String
     * @return the value of field 'descricaoTipoRetorno'.
     */
    public java.lang.String getDescricaoTipoRetorno()
    {
        return this._descricaoTipoRetorno;
    } //-- java.lang.String getDescricaoTipoRetorno() 

    /**
     * Returns the value of field 'filialCnpj'.
     * 
     * @return int
     * @return the value of field 'filialCnpj'.
     */
    public int getFilialCnpj()
    {
        return this._filialCnpj;
    } //-- int getFilialCnpj() 

    /**
     * Returns the value of field 'horaGeracaoRetorno'.
     * 
     * @return String
     * @return the value of field 'horaGeracaoRetorno'.
     */
    public java.lang.String getHoraGeracaoRetorno()
    {
        return this._horaGeracaoRetorno;
    } //-- java.lang.String getHoraGeracaoRetorno() 

    /**
     * Returns the value of field 'horaInclusaoArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'horaInclusaoArquivoRetorno'.
     */
    public java.lang.String getHoraInclusaoArquivoRetorno()
    {
        return this._horaInclusaoArquivoRetorno;
    } //-- java.lang.String getHoraInclusaoArquivoRetorno() 

    /**
     * Returns the value of field 'nomeRazao'.
     * 
     * @return String
     * @return the value of field 'nomeRazao'.
     */
    public java.lang.String getNomeRazao()
    {
        return this._nomeRazao;
    } //-- java.lang.String getNomeRazao() 

    /**
     * Returns the value of field 'numeroArquivoRetorno'.
     * 
     * @return long
     * @return the value of field 'numeroArquivoRetorno'.
     */
    public long getNumeroArquivoRetorno()
    {
        return this._numeroArquivoRetorno;
    } //-- long getNumeroArquivoRetorno() 

    /**
     * Returns the value of field 'quantidadeRegistro'.
     * 
     * @return long
     * @return the value of field 'quantidadeRegistro'.
     */
    public long getQuantidadeRegistro()
    {
        return this._quantidadeRegistro;
    } //-- long getQuantidadeRegistro() 

    /**
     * Returns the value of field 'valorRegistro'.
     * 
     * @return double
     * @return the value of field 'valorRegistro'.
     */
    public double getValorRegistro()
    {
        return this._valorRegistro;
    } //-- double getValorRegistro() 

    /**
     * Method hasAgenciaBancariaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasAgenciaBancariaDebito()
    {
        return this._has_agenciaBancariaDebito;
    } //-- boolean hasAgenciaBancariaDebito() 

    /**
     * Method hasCodigoBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoBancoDebito()
    {
        return this._has_codigoBancoDebito;
    } //-- boolean hasCodigoBancoDebito() 

    /**
     * Method hasCodigoLancamentoContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoLancamentoContaDebito()
    {
        return this._has_codigoLancamentoContaDebito;
    } //-- boolean hasCodigoLancamentoContaDebito() 

    /**
     * Method hasCodigoMeioTransmissao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoMeioTransmissao()
    {
        return this._has_codigoMeioTransmissao;
    } //-- boolean hasCodigoMeioTransmissao() 

    /**
     * Method hasCodigoPerfilComunicacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoPerfilComunicacao()
    {
        return this._has_codigoPerfilComunicacao;
    } //-- boolean hasCodigoPerfilComunicacao() 

    /**
     * Method hasCodigoSituacaoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoSituacaoRetorno()
    {
        return this._has_codigoSituacaoRetorno;
    } //-- boolean hasCodigoSituacaoRetorno() 

    /**
     * Method hasCodigoTipoLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoTipoLayout()
    {
        return this._has_codigoTipoLayout;
    } //-- boolean hasCodigoTipoLayout() 

    /**
     * Method hasCodigoTipoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodigoTipoRetorno()
    {
        return this._has_codigoTipoRetorno;
    } //-- boolean hasCodigoTipoRetorno() 

    /**
     * Method hasContaBancariaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasContaBancariaDebito()
    {
        return this._has_contaBancariaDebito;
    } //-- boolean hasContaBancariaDebito() 

    /**
     * Method hasContaRazaoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasContaRazaoDebito()
    {
        return this._has_contaRazaoDebito;
    } //-- boolean hasContaRazaoDebito() 

    /**
     * Method hasControle
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasControle()
    {
        return this._has_controle;
    } //-- boolean hasControle() 

    /**
     * Method hasCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCpfCnpj()
    {
        return this._has_cpfCnpj;
    } //-- boolean hasCpfCnpj() 

    /**
     * Method hasFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFilialCnpj()
    {
        return this._has_filialCnpj;
    } //-- boolean hasFilialCnpj() 

    /**
     * Method hasNumeroArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroArquivoRetorno()
    {
        return this._has_numeroArquivoRetorno;
    } //-- boolean hasNumeroArquivoRetorno() 

    /**
     * Method hasQuantidadeRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeRegistro()
    {
        return this._has_quantidadeRegistro;
    } //-- boolean hasQuantidadeRegistro() 

    /**
     * Method hasValorRegistro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorRegistro()
    {
        return this._has_valorRegistro;
    } //-- boolean hasValorRegistro() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'agenciaBancariaDebito'.
     * 
     * @param agenciaBancariaDebito the value of field
     * 'agenciaBancariaDebito'.
     */
    public void setAgenciaBancariaDebito(int agenciaBancariaDebito)
    {
        this._agenciaBancariaDebito = agenciaBancariaDebito;
        this._has_agenciaBancariaDebito = true;
    } //-- void setAgenciaBancariaDebito(int) 

    /**
     * Sets the value of field 'codigoAmbiente'.
     * 
     * @param codigoAmbiente the value of field 'codigoAmbiente'.
     */
    public void setCodigoAmbiente(java.lang.String codigoAmbiente)
    {
        this._codigoAmbiente = codigoAmbiente;
    } //-- void setCodigoAmbiente(java.lang.String) 

    /**
     * Sets the value of field 'codigoBancoDebito'.
     * 
     * @param codigoBancoDebito the value of field
     * 'codigoBancoDebito'.
     */
    public void setCodigoBancoDebito(int codigoBancoDebito)
    {
        this._codigoBancoDebito = codigoBancoDebito;
        this._has_codigoBancoDebito = true;
    } //-- void setCodigoBancoDebito(int) 

    /**
     * Sets the value of field 'codigoLancamentoContaDebito'.
     * 
     * @param codigoLancamentoContaDebito the value of field
     * 'codigoLancamentoContaDebito'.
     */
    public void setCodigoLancamentoContaDebito(int codigoLancamentoContaDebito)
    {
        this._codigoLancamentoContaDebito = codigoLancamentoContaDebito;
        this._has_codigoLancamentoContaDebito = true;
    } //-- void setCodigoLancamentoContaDebito(int) 

    /**
     * Sets the value of field 'codigoMeioTransmissao'.
     * 
     * @param codigoMeioTransmissao the value of field
     * 'codigoMeioTransmissao'.
     */
    public void setCodigoMeioTransmissao(int codigoMeioTransmissao)
    {
        this._codigoMeioTransmissao = codigoMeioTransmissao;
        this._has_codigoMeioTransmissao = true;
    } //-- void setCodigoMeioTransmissao(int) 

    /**
     * Sets the value of field 'codigoPerfilComunicacao'.
     * 
     * @param codigoPerfilComunicacao the value of field
     * 'codigoPerfilComunicacao'.
     */
    public void setCodigoPerfilComunicacao(long codigoPerfilComunicacao)
    {
        this._codigoPerfilComunicacao = codigoPerfilComunicacao;
        this._has_codigoPerfilComunicacao = true;
    } //-- void setCodigoPerfilComunicacao(long) 

    /**
     * Sets the value of field 'codigoSistemaLegado'.
     * 
     * @param codigoSistemaLegado the value of field
     * 'codigoSistemaLegado'.
     */
    public void setCodigoSistemaLegado(java.lang.String codigoSistemaLegado)
    {
        this._codigoSistemaLegado = codigoSistemaLegado;
    } //-- void setCodigoSistemaLegado(java.lang.String) 

    /**
     * Sets the value of field 'codigoSituacaoRetorno'.
     * 
     * @param codigoSituacaoRetorno the value of field
     * 'codigoSituacaoRetorno'.
     */
    public void setCodigoSituacaoRetorno(int codigoSituacaoRetorno)
    {
        this._codigoSituacaoRetorno = codigoSituacaoRetorno;
        this._has_codigoSituacaoRetorno = true;
    } //-- void setCodigoSituacaoRetorno(int) 

    /**
     * Sets the value of field 'codigoTipoLayout'.
     * 
     * @param codigoTipoLayout the value of field 'codigoTipoLayout'
     */
    public void setCodigoTipoLayout(int codigoTipoLayout)
    {
        this._codigoTipoLayout = codigoTipoLayout;
        this._has_codigoTipoLayout = true;
    } //-- void setCodigoTipoLayout(int) 

    /**
     * Sets the value of field 'codigoTipoRetorno'.
     * 
     * @param codigoTipoRetorno the value of field
     * 'codigoTipoRetorno'.
     */
    public void setCodigoTipoRetorno(int codigoTipoRetorno)
    {
        this._codigoTipoRetorno = codigoTipoRetorno;
        this._has_codigoTipoRetorno = true;
    } //-- void setCodigoTipoRetorno(int) 

    /**
     * Sets the value of field 'contaBancariaDebito'.
     * 
     * @param contaBancariaDebito the value of field
     * 'contaBancariaDebito'.
     */
    public void setContaBancariaDebito(long contaBancariaDebito)
    {
        this._contaBancariaDebito = contaBancariaDebito;
        this._has_contaBancariaDebito = true;
    } //-- void setContaBancariaDebito(long) 

    /**
     * Sets the value of field 'contaRazaoDebito'.
     * 
     * @param contaRazaoDebito the value of field 'contaRazaoDebito'
     */
    public void setContaRazaoDebito(int contaRazaoDebito)
    {
        this._contaRazaoDebito = contaRazaoDebito;
        this._has_contaRazaoDebito = true;
    } //-- void setContaRazaoDebito(int) 

    /**
     * Sets the value of field 'controle'.
     * 
     * @param controle the value of field 'controle'.
     */
    public void setControle(int controle)
    {
        this._controle = controle;
        this._has_controle = true;
    } //-- void setControle(int) 

    /**
     * Sets the value of field 'cpfCnpj'.
     * 
     * @param cpfCnpj the value of field 'cpfCnpj'.
     */
    public void setCpfCnpj(long cpfCnpj)
    {
        this._cpfCnpj = cpfCnpj;
        this._has_cpfCnpj = true;
    } //-- void setCpfCnpj(long) 

    /**
     * Sets the value of field 'descricaoAmbiente'.
     * 
     * @param descricaoAmbiente the value of field
     * 'descricaoAmbiente'.
     */
    public void setDescricaoAmbiente(java.lang.String descricaoAmbiente)
    {
        this._descricaoAmbiente = descricaoAmbiente;
    } //-- void setDescricaoAmbiente(java.lang.String) 

    /**
     * Sets the value of field 'descricaoMeioTransmissao'.
     * 
     * @param descricaoMeioTransmissao the value of field
     * 'descricaoMeioTransmissao'.
     */
    public void setDescricaoMeioTransmissao(java.lang.String descricaoMeioTransmissao)
    {
        this._descricaoMeioTransmissao = descricaoMeioTransmissao;
    } //-- void setDescricaoMeioTransmissao(java.lang.String) 

    /**
     * Sets the value of field 'descricaoProdutoServico'.
     * 
     * @param descricaoProdutoServico the value of field
     * 'descricaoProdutoServico'.
     */
    public void setDescricaoProdutoServico(java.lang.String descricaoProdutoServico)
    {
        this._descricaoProdutoServico = descricaoProdutoServico;
    } //-- void setDescricaoProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'descricaoSituacaoRetorno'.
     * 
     * @param descricaoSituacaoRetorno the value of field
     * 'descricaoSituacaoRetorno'.
     */
    public void setDescricaoSituacaoRetorno(java.lang.String descricaoSituacaoRetorno)
    {
        this._descricaoSituacaoRetorno = descricaoSituacaoRetorno;
    } //-- void setDescricaoSituacaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'descricaoTipoLayout'.
     * 
     * @param descricaoTipoLayout the value of field
     * 'descricaoTipoLayout'.
     */
    public void setDescricaoTipoLayout(java.lang.String descricaoTipoLayout)
    {
        this._descricaoTipoLayout = descricaoTipoLayout;
    } //-- void setDescricaoTipoLayout(java.lang.String) 

    /**
     * Sets the value of field 'descricaoTipoRetorno'.
     * 
     * @param descricaoTipoRetorno the value of field
     * 'descricaoTipoRetorno'.
     */
    public void setDescricaoTipoRetorno(java.lang.String descricaoTipoRetorno)
    {
        this._descricaoTipoRetorno = descricaoTipoRetorno;
    } //-- void setDescricaoTipoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'filialCnpj'.
     * 
     * @param filialCnpj the value of field 'filialCnpj'.
     */
    public void setFilialCnpj(int filialCnpj)
    {
        this._filialCnpj = filialCnpj;
        this._has_filialCnpj = true;
    } //-- void setFilialCnpj(int) 

    /**
     * Sets the value of field 'horaGeracaoRetorno'.
     * 
     * @param horaGeracaoRetorno the value of field
     * 'horaGeracaoRetorno'.
     */
    public void setHoraGeracaoRetorno(java.lang.String horaGeracaoRetorno)
    {
        this._horaGeracaoRetorno = horaGeracaoRetorno;
    } //-- void setHoraGeracaoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'horaInclusaoArquivoRetorno'.
     * 
     * @param horaInclusaoArquivoRetorno the value of field
     * 'horaInclusaoArquivoRetorno'.
     */
    public void setHoraInclusaoArquivoRetorno(java.lang.String horaInclusaoArquivoRetorno)
    {
        this._horaInclusaoArquivoRetorno = horaInclusaoArquivoRetorno;
    } //-- void setHoraInclusaoArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'nomeRazao'.
     * 
     * @param nomeRazao the value of field 'nomeRazao'.
     */
    public void setNomeRazao(java.lang.String nomeRazao)
    {
        this._nomeRazao = nomeRazao;
    } //-- void setNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'numeroArquivoRetorno'.
     * 
     * @param numeroArquivoRetorno the value of field
     * 'numeroArquivoRetorno'.
     */
    public void setNumeroArquivoRetorno(long numeroArquivoRetorno)
    {
        this._numeroArquivoRetorno = numeroArquivoRetorno;
        this._has_numeroArquivoRetorno = true;
    } //-- void setNumeroArquivoRetorno(long) 

    /**
     * Sets the value of field 'quantidadeRegistro'.
     * 
     * @param quantidadeRegistro the value of field
     * 'quantidadeRegistro'.
     */
    public void setQuantidadeRegistro(long quantidadeRegistro)
    {
        this._quantidadeRegistro = quantidadeRegistro;
        this._has_quantidadeRegistro = true;
    } //-- void setQuantidadeRegistro(long) 

    /**
     * Sets the value of field 'valorRegistro'.
     * 
     * @param valorRegistro the value of field 'valorRegistro'.
     */
    public void setValorRegistro(double valorRegistro)
    {
        this._valorRegistro = valorRegistro;
        this._has_valorRegistro = true;
    } //-- void setValorRegistro(double) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarretorno.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
