/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean;

/**
 * Nome: DetalharMsgLayoutArqRetornoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharMsgLayoutArqRetornoEntradaDTO{
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo nrMensagemArquivoRetorno. */
	private Integer nrMensagemArquivoRetorno;
	
	/** Atributo cdMensagemArquivoRetorno. */
	private String cdMensagemArquivoRetorno;

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: nrMensagemArquivoRetorno.
	 *
	 * @return nrMensagemArquivoRetorno
	 */
	public Integer getNrMensagemArquivoRetorno(){
		return nrMensagemArquivoRetorno;
	}

	/**
	 * Set: nrMensagemArquivoRetorno.
	 *
	 * @param nrMensagemArquivoRetorno the nr mensagem arquivo retorno
	 */
	public void setNrMensagemArquivoRetorno(Integer nrMensagemArquivoRetorno){
		this.nrMensagemArquivoRetorno = nrMensagemArquivoRetorno;
	}

	/**
	 * Get: cdMensagemArquivoRetorno.
	 *
	 * @return cdMensagemArquivoRetorno
	 */
	public String getCdMensagemArquivoRetorno(){
		return cdMensagemArquivoRetorno;
	}

	/**
	 * Set: cdMensagemArquivoRetorno.
	 *
	 * @param cdMensagemArquivoRetorno the cd mensagem arquivo retorno
	 */
	public void setCdMensagemArquivoRetorno(String cdMensagemArquivoRetorno){
		this.cdMensagemArquivoRetorno = cdMensagemArquivoRetorno;
	}
}