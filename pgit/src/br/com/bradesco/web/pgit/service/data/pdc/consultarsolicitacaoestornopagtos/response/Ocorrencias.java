/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacaoPagamento
     */
    private int _cdSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSolicitacaoPagamento
     */
    private boolean _has_cdSolicitacaoPagamento;

    /**
     * Field _nrSolicitacaoPagamentoIntegrado
     */
    private int _nrSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _nrSolicitacaoPagamentoIntegrado
     */
    private boolean _has_nrSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdCpfCnpjParticipante
     */
    private java.lang.String _cdCpfCnpjParticipante;

    /**
     * Field _cdPessoaCompleto
     */
    private java.lang.String _cdPessoaCompleto;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _dsModalidade
     */
    private java.lang.String _dsModalidade;

    /**
     * Field _cdSituacaoSolicitacao
     */
    private int _cdSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoSolicitacao
     */
    private boolean _has_cdSituacaoSolicitacao;

    /**
     * Field _dsSituacaoSolicitante
     */
    private java.lang.String _dsSituacaoSolicitante;

    /**
     * Field _cdMotivoSituacaoSolicitante
     */
    private int _cdMotivoSituacaoSolicitante = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoSolicitante
     */
    private boolean _has_cdMotivoSituacaoSolicitante;

    /**
     * Field _dsMotivoSituacaoSolicitante
     */
    private java.lang.String _dsMotivoSituacaoSolicitante;

    /**
     * Field _dsDataSolicitacao
     */
    private java.lang.String _dsDataSolicitacao;

    /**
     * Field _dsHoraSolicitacao
     */
    private java.lang.String _dsHoraSolicitacao;

    /**
     * Field _cdFormaEstrnPagamento
     */
    private int _cdFormaEstrnPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaEstrnPagamento
     */
    private boolean _has_cdFormaEstrnPagamento;

    /**
     * Field _dsFormaEstrnPagamento
     */
    private java.lang.String _dsFormaEstrnPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaEstrnPagamento
     * 
     */
    public void deleteCdFormaEstrnPagamento()
    {
        this._has_cdFormaEstrnPagamento= false;
    } //-- void deleteCdFormaEstrnPagamento() 

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdMotivoSituacaoSolicitante
     * 
     */
    public void deleteCdMotivoSituacaoSolicitante()
    {
        this._has_cdMotivoSituacaoSolicitante= false;
    } //-- void deleteCdMotivoSituacaoSolicitante() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoSolicitacao
     * 
     */
    public void deleteCdSituacaoSolicitacao()
    {
        this._has_cdSituacaoSolicitacao= false;
    } //-- void deleteCdSituacaoSolicitacao() 

    /**
     * Method deleteCdSolicitacaoPagamento
     * 
     */
    public void deleteCdSolicitacaoPagamento()
    {
        this._has_cdSolicitacaoPagamento= false;
    } //-- void deleteCdSolicitacaoPagamento() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteNrSolicitacaoPagamentoIntegrado()
    {
        this._has_nrSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteNrSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdCpfCnpjParticipante'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjParticipante'.
     */
    public java.lang.String getCdCpfCnpjParticipante()
    {
        return this._cdCpfCnpjParticipante;
    } //-- java.lang.String getCdCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdFormaEstrnPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaEstrnPagamento'.
     */
    public int getCdFormaEstrnPagamento()
    {
        return this._cdFormaEstrnPagamento;
    } //-- int getCdFormaEstrnPagamento() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdMotivoSituacaoSolicitante'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoSolicitante'.
     */
    public int getCdMotivoSituacaoSolicitante()
    {
        return this._cdMotivoSituacaoSolicitante;
    } //-- int getCdMotivoSituacaoSolicitante() 

    /**
     * Returns the value of field 'cdPessoaCompleto'.
     * 
     * @return String
     * @return the value of field 'cdPessoaCompleto'.
     */
    public java.lang.String getCdPessoaCompleto()
    {
        return this._cdPessoaCompleto;
    } //-- java.lang.String getCdPessoaCompleto() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacao'.
     */
    public int getCdSituacaoSolicitacao()
    {
        return this._cdSituacaoSolicitacao;
    } //-- int getCdSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamento'.
     */
    public int getCdSolicitacaoPagamento()
    {
        return this._cdSolicitacaoPagamento;
    } //-- int getCdSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsDataSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsDataSolicitacao'.
     */
    public java.lang.String getDsDataSolicitacao()
    {
        return this._dsDataSolicitacao;
    } //-- java.lang.String getDsDataSolicitacao() 

    /**
     * Returns the value of field 'dsFormaEstrnPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaEstrnPagamento'.
     */
    public java.lang.String getDsFormaEstrnPagamento()
    {
        return this._dsFormaEstrnPagamento;
    } //-- java.lang.String getDsFormaEstrnPagamento() 

    /**
     * Returns the value of field 'dsHoraSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsHoraSolicitacao'.
     */
    public java.lang.String getDsHoraSolicitacao()
    {
        return this._dsHoraSolicitacao;
    } //-- java.lang.String getDsHoraSolicitacao() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModalidade'.
     */
    public java.lang.String getDsModalidade()
    {
        return this._dsModalidade;
    } //-- java.lang.String getDsModalidade() 

    /**
     * Returns the value of field 'dsMotivoSituacaoSolicitante'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoSolicitante'.
     */
    public java.lang.String getDsMotivoSituacaoSolicitante()
    {
        return this._dsMotivoSituacaoSolicitante;
    } //-- java.lang.String getDsMotivoSituacaoSolicitante() 

    /**
     * Returns the value of field 'dsSituacaoSolicitante'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitante'.
     */
    public java.lang.String getDsSituacaoSolicitante()
    {
        return this._dsSituacaoSolicitante;
    } //-- java.lang.String getDsSituacaoSolicitante() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamentoIntegrado'.
     */
    public int getNrSolicitacaoPagamentoIntegrado()
    {
        return this._nrSolicitacaoPagamentoIntegrado;
    } //-- int getNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdFormaEstrnPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaEstrnPagamento()
    {
        return this._has_cdFormaEstrnPagamento;
    } //-- boolean hasCdFormaEstrnPagamento() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdMotivoSituacaoSolicitante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoSolicitante()
    {
        return this._has_cdMotivoSituacaoSolicitante;
    } //-- boolean hasCdMotivoSituacaoSolicitante() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacao()
    {
        return this._has_cdSituacaoSolicitacao;
    } //-- boolean hasCdSituacaoSolicitacao() 

    /**
     * Method hasCdSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamento()
    {
        return this._has_cdSolicitacaoPagamento;
    } //-- boolean hasCdSolicitacaoPagamento() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamentoIntegrado()
    {
        return this._has_nrSolicitacaoPagamentoIntegrado;
    } //-- boolean hasNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCpfCnpjParticipante'.
     * 
     * @param cdCpfCnpjParticipante the value of field
     * 'cdCpfCnpjParticipante'.
     */
    public void setCdCpfCnpjParticipante(java.lang.String cdCpfCnpjParticipante)
    {
        this._cdCpfCnpjParticipante = cdCpfCnpjParticipante;
    } //-- void setCdCpfCnpjParticipante(java.lang.String) 

    /**
     * Sets the value of field 'cdFormaEstrnPagamento'.
     * 
     * @param cdFormaEstrnPagamento the value of field
     * 'cdFormaEstrnPagamento'.
     */
    public void setCdFormaEstrnPagamento(int cdFormaEstrnPagamento)
    {
        this._cdFormaEstrnPagamento = cdFormaEstrnPagamento;
        this._has_cdFormaEstrnPagamento = true;
    } //-- void setCdFormaEstrnPagamento(int) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoSolicitante'.
     * 
     * @param cdMotivoSituacaoSolicitante the value of field
     * 'cdMotivoSituacaoSolicitante'.
     */
    public void setCdMotivoSituacaoSolicitante(int cdMotivoSituacaoSolicitante)
    {
        this._cdMotivoSituacaoSolicitante = cdMotivoSituacaoSolicitante;
        this._has_cdMotivoSituacaoSolicitante = true;
    } //-- void setCdMotivoSituacaoSolicitante(int) 

    /**
     * Sets the value of field 'cdPessoaCompleto'.
     * 
     * @param cdPessoaCompleto the value of field 'cdPessoaCompleto'
     */
    public void setCdPessoaCompleto(java.lang.String cdPessoaCompleto)
    {
        this._cdPessoaCompleto = cdPessoaCompleto;
    } //-- void setCdPessoaCompleto(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacao'.
     * 
     * @param cdSituacaoSolicitacao the value of field
     * 'cdSituacaoSolicitacao'.
     */
    public void setCdSituacaoSolicitacao(int cdSituacaoSolicitacao)
    {
        this._cdSituacaoSolicitacao = cdSituacaoSolicitacao;
        this._has_cdSituacaoSolicitacao = true;
    } //-- void setCdSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamento'.
     * 
     * @param cdSolicitacaoPagamento the value of field
     * 'cdSolicitacaoPagamento'.
     */
    public void setCdSolicitacaoPagamento(int cdSolicitacaoPagamento)
    {
        this._cdSolicitacaoPagamento = cdSolicitacaoPagamento;
        this._has_cdSolicitacaoPagamento = true;
    } //-- void setCdSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dsDataSolicitacao'.
     * 
     * @param dsDataSolicitacao the value of field
     * 'dsDataSolicitacao'.
     */
    public void setDsDataSolicitacao(java.lang.String dsDataSolicitacao)
    {
        this._dsDataSolicitacao = dsDataSolicitacao;
    } //-- void setDsDataSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaEstrnPagamento'.
     * 
     * @param dsFormaEstrnPagamento the value of field
     * 'dsFormaEstrnPagamento'.
     */
    public void setDsFormaEstrnPagamento(java.lang.String dsFormaEstrnPagamento)
    {
        this._dsFormaEstrnPagamento = dsFormaEstrnPagamento;
    } //-- void setDsFormaEstrnPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsHoraSolicitacao'.
     * 
     * @param dsHoraSolicitacao the value of field
     * 'dsHoraSolicitacao'.
     */
    public void setDsHoraSolicitacao(java.lang.String dsHoraSolicitacao)
    {
        this._dsHoraSolicitacao = dsHoraSolicitacao;
    } //-- void setDsHoraSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(java.lang.String dsModalidade)
    {
        this._dsModalidade = dsModalidade;
    } //-- void setDsModalidade(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoSolicitante'.
     * 
     * @param dsMotivoSituacaoSolicitante the value of field
     * 'dsMotivoSituacaoSolicitante'.
     */
    public void setDsMotivoSituacaoSolicitante(java.lang.String dsMotivoSituacaoSolicitante)
    {
        this._dsMotivoSituacaoSolicitante = dsMotivoSituacaoSolicitante;
    } //-- void setDsMotivoSituacaoSolicitante(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitante'.
     * 
     * @param dsSituacaoSolicitante the value of field
     * 'dsSituacaoSolicitante'.
     */
    public void setDsSituacaoSolicitante(java.lang.String dsSituacaoSolicitante)
    {
        this._dsSituacaoSolicitante = dsSituacaoSolicitante;
    } //-- void setDsSituacaoSolicitante(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @param nrSolicitacaoPagamentoIntegrado the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     */
    public void setNrSolicitacaoPagamentoIntegrado(int nrSolicitacaoPagamentoIntegrado)
    {
        this._nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
        this._has_nrSolicitacaoPagamentoIntegrado = true;
    } //-- void setNrSolicitacaoPagamentoIntegrado(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoestornopagtos.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
