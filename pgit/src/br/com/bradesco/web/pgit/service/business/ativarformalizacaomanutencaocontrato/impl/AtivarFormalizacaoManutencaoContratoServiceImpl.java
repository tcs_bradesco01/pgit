/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.impl;

import br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.IAtivarFormalizacaoManutencaoContratoService;
import br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean.AtivarAditivoContratoPendAssEntradaDTO;
import br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean.AtivarAditivoContratoPendAssSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.ativaraditivocontratopendass.request.AtivarAditivoContratoPendAssRequest;
import br.com.bradesco.web.pgit.service.data.pdc.ativaraditivocontratopendass.response.AtivarAditivoContratoPendAssResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: AtivarFormalizacaoManutencaoContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class AtivarFormalizacaoManutencaoContratoServiceImpl implements IAtivarFormalizacaoManutencaoContratoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter; 

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
     * Construtor.
     */
    public AtivarFormalizacaoManutencaoContratoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.IAtivarFormalizacaoManutencaoContrato#sampleAtivarFormalizacaoManutencaoContrato()
     */
    public void sampleAtivarFormalizacaoManutencaoContrato() {
        // TODO: Implementa�ao
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.IAtivarFormalizacaoManutencaoContratoService#ativarFormalizacaoManutencaoContrato(br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean.AtivarAditivoContratoPendAssEntradaDTO)
     */
    public AtivarAditivoContratoPendAssSaidaDTO ativarFormalizacaoManutencaoContrato(AtivarAditivoContratoPendAssEntradaDTO ativarAditivoContratoPendAssEntradaDTO) {
		
    	AtivarAditivoContratoPendAssSaidaDTO ativarAditivoContratoPendAssSaidaDTO = new AtivarAditivoContratoPendAssSaidaDTO();
		AtivarAditivoContratoPendAssRequest request = new AtivarAditivoContratoPendAssRequest();
		AtivarAditivoContratoPendAssResponse response = new AtivarAditivoContratoPendAssResponse();
		
		request.setCdMotivoSituacaoAditivo(ativarAditivoContratoPendAssEntradaDTO.getCdMotivoSituacaoContrato());
		request.setNrAditivoContratoNegocio(ativarAditivoContratoPendAssEntradaDTO.getNrAditivoContratoNegocio());
		request.setCdPessoaJuridicaContrato(ativarAditivoContratoPendAssEntradaDTO.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(ativarAditivoContratoPendAssEntradaDTO.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(ativarAditivoContratoPendAssEntradaDTO.getNrSequenciaContratoNegocio());
		
		response = getFactoryAdapter().getAtivarAditivoContratoPendAssPDCAdapter().invokeProcess(request);
		
		ativarAditivoContratoPendAssSaidaDTO.setCodMensagem(response.getCodMensagem());
		ativarAditivoContratoPendAssSaidaDTO.setMensagem(response.getMensagem());
	
		return ativarAditivoContratoPendAssSaidaDTO;
	}
    
}

