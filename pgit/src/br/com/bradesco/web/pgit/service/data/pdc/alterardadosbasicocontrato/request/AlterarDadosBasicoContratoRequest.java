/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarDadosBasicoContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarDadosBasicoContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdSituacaoContrato
     */
    private int _cdSituacaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoContrato
     */
    private boolean _has_cdSituacaoContrato;

    /**
     * Field _cdMotivoSituacaoOperacao
     */
    private int _cdMotivoSituacaoOperacao = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoOperacao
     */
    private boolean _has_cdMotivoSituacaoOperacao;

    /**
     * Field _cdIndicadorEconomicoMoeda
     */
    private int _cdIndicadorEconomicoMoeda = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomicoMoeda
     */
    private boolean _has_cdIndicadorEconomicoMoeda;

    /**
     * Field _cdIdioma
     */
    private int _cdIdioma = 0;

    /**
     * keeps track of state for field: _cdIdioma
     */
    private boolean _has_cdIdioma;

    /**
     * Field _cdPermissaoEncerramentoContrato
     */
    private java.lang.String _cdPermissaoEncerramentoContrato;

    /**
     * Field _dsContratoNegocio
     */
    private java.lang.String _dsContratoNegocio;

    /**
     * Field _cdSetorContrato
     */
    private int _cdSetorContrato = 0;

    /**
     * keeps track of state for field: _cdSetorContrato
     */
    private boolean _has_cdSetorContrato;

    /**
     * Field _hrAberturaContratoNegocio
     */
    private java.lang.String _hrAberturaContratoNegocio;

    /**
     * Field _cdUsuario
     */
    private java.lang.String _cdUsuario;

    /**
     * Field _cdUnidadeOperacionalContrato
     */
    private int _cdUnidadeOperacionalContrato = 0;

    /**
     * keeps track of state for field: _cdUnidadeOperacionalContrato
     */
    private boolean _has_cdUnidadeOperacionalContrato;

    /**
     * Field _cdFuncionarioBradesco
     */
    private java.lang.String _cdFuncionarioBradesco;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _nrSequenciaUnidadeOrganizacional
     */
    private int _nrSequenciaUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaUnidadeOrganizacional
     */
    private boolean _has_nrSequenciaUnidadeOrganizacional;

    /**
     * Field _vlSaldoVirtualTeste
     */
    private java.math.BigDecimal _vlSaldoVirtualTeste = new java.math.BigDecimal("0");

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarDadosBasicoContratoRequest() 
     {
        super();
        setVlSaldoVirtualTeste(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.request.AlterarDadosBasicoContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdIdioma
     * 
     */
    public void deleteCdIdioma()
    {
        this._has_cdIdioma= false;
    } //-- void deleteCdIdioma() 

    /**
     * Method deleteCdIndicadorEconomicoMoeda
     * 
     */
    public void deleteCdIndicadorEconomicoMoeda()
    {
        this._has_cdIndicadorEconomicoMoeda= false;
    } //-- void deleteCdIndicadorEconomicoMoeda() 

    /**
     * Method deleteCdMotivoSituacaoOperacao
     * 
     */
    public void deleteCdMotivoSituacaoOperacao()
    {
        this._has_cdMotivoSituacaoOperacao= false;
    } //-- void deleteCdMotivoSituacaoOperacao() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSetorContrato
     * 
     */
    public void deleteCdSetorContrato()
    {
        this._has_cdSetorContrato= false;
    } //-- void deleteCdSetorContrato() 

    /**
     * Method deleteCdSituacaoContrato
     * 
     */
    public void deleteCdSituacaoContrato()
    {
        this._has_cdSituacaoContrato= false;
    } //-- void deleteCdSituacaoContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdUnidadeOperacionalContrato
     * 
     */
    public void deleteCdUnidadeOperacionalContrato()
    {
        this._has_cdUnidadeOperacionalContrato= false;
    } //-- void deleteCdUnidadeOperacionalContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaUnidadeOrganizacional
     * 
     */
    public void deleteNrSequenciaUnidadeOrganizacional()
    {
        this._has_nrSequenciaUnidadeOrganizacional= false;
    } //-- void deleteNrSequenciaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFuncionarioBradesco'.
     * 
     * @return String
     * @return the value of field 'cdFuncionarioBradesco'.
     */
    public java.lang.String getCdFuncionarioBradesco()
    {
        return this._cdFuncionarioBradesco;
    } //-- java.lang.String getCdFuncionarioBradesco() 

    /**
     * Returns the value of field 'cdIdioma'.
     * 
     * @return int
     * @return the value of field 'cdIdioma'.
     */
    public int getCdIdioma()
    {
        return this._cdIdioma;
    } //-- int getCdIdioma() 

    /**
     * Returns the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomicoMoeda'.
     */
    public int getCdIndicadorEconomicoMoeda()
    {
        return this._cdIndicadorEconomicoMoeda;
    } //-- int getCdIndicadorEconomicoMoeda() 

    /**
     * Returns the value of field 'cdMotivoSituacaoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoOperacao'.
     */
    public int getCdMotivoSituacaoOperacao()
    {
        return this._cdMotivoSituacaoOperacao;
    } //-- int getCdMotivoSituacaoOperacao() 

    /**
     * Returns the value of field
     * 'cdPermissaoEncerramentoContrato'.
     * 
     * @return String
     * @return the value of field 'cdPermissaoEncerramentoContrato'.
     */
    public java.lang.String getCdPermissaoEncerramentoContrato()
    {
        return this._cdPermissaoEncerramentoContrato;
    } //-- java.lang.String getCdPermissaoEncerramentoContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSetorContrato'.
     * 
     * @return int
     * @return the value of field 'cdSetorContrato'.
     */
    public int getCdSetorContrato()
    {
        return this._cdSetorContrato;
    } //-- int getCdSetorContrato() 

    /**
     * Returns the value of field 'cdSituacaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContrato'.
     */
    public int getCdSituacaoContrato()
    {
        return this._cdSituacaoContrato;
    } //-- int getCdSituacaoContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUnidadeOperacionalContrato'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOperacionalContrato'.
     */
    public int getCdUnidadeOperacionalContrato()
    {
        return this._cdUnidadeOperacionalContrato;
    } //-- int getCdUnidadeOperacionalContrato() 

    /**
     * Returns the value of field 'cdUsuario'.
     * 
     * @return String
     * @return the value of field 'cdUsuario'.
     */
    public java.lang.String getCdUsuario()
    {
        return this._cdUsuario;
    } //-- java.lang.String getCdUsuario() 

    /**
     * Returns the value of field 'dsContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsContratoNegocio'.
     */
    public java.lang.String getDsContratoNegocio()
    {
        return this._dsContratoNegocio;
    } //-- java.lang.String getDsContratoNegocio() 

    /**
     * Returns the value of field 'hrAberturaContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'hrAberturaContratoNegocio'.
     */
    public java.lang.String getHrAberturaContratoNegocio()
    {
        return this._hrAberturaContratoNegocio;
    } //-- java.lang.String getHrAberturaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaUnidadeOrganizacional'
     */
    public int getNrSequenciaUnidadeOrganizacional()
    {
        return this._nrSequenciaUnidadeOrganizacional;
    } //-- int getNrSequenciaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'vlSaldoVirtualTeste'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlSaldoVirtualTeste'.
     */
    public java.math.BigDecimal getVlSaldoVirtualTeste()
    {
        return this._vlSaldoVirtualTeste;
    } //-- java.math.BigDecimal getVlSaldoVirtualTeste() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdIdioma
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdioma()
    {
        return this._has_cdIdioma;
    } //-- boolean hasCdIdioma() 

    /**
     * Method hasCdIndicadorEconomicoMoeda
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomicoMoeda()
    {
        return this._has_cdIndicadorEconomicoMoeda;
    } //-- boolean hasCdIndicadorEconomicoMoeda() 

    /**
     * Method hasCdMotivoSituacaoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoOperacao()
    {
        return this._has_cdMotivoSituacaoOperacao;
    } //-- boolean hasCdMotivoSituacaoOperacao() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSetorContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSetorContrato()
    {
        return this._has_cdSetorContrato;
    } //-- boolean hasCdSetorContrato() 

    /**
     * Method hasCdSituacaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContrato()
    {
        return this._has_cdSituacaoContrato;
    } //-- boolean hasCdSituacaoContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdUnidadeOperacionalContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOperacionalContrato()
    {
        return this._has_cdUnidadeOperacionalContrato;
    } //-- boolean hasCdUnidadeOperacionalContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaUnidadeOrganizacional()
    {
        return this._has_nrSequenciaUnidadeOrganizacional;
    } //-- boolean hasNrSequenciaUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFuncionarioBradesco'.
     * 
     * @param cdFuncionarioBradesco the value of field
     * 'cdFuncionarioBradesco'.
     */
    public void setCdFuncionarioBradesco(java.lang.String cdFuncionarioBradesco)
    {
        this._cdFuncionarioBradesco = cdFuncionarioBradesco;
    } //-- void setCdFuncionarioBradesco(java.lang.String) 

    /**
     * Sets the value of field 'cdIdioma'.
     * 
     * @param cdIdioma the value of field 'cdIdioma'.
     */
    public void setCdIdioma(int cdIdioma)
    {
        this._cdIdioma = cdIdioma;
        this._has_cdIdioma = true;
    } //-- void setCdIdioma(int) 

    /**
     * Sets the value of field 'cdIndicadorEconomicoMoeda'.
     * 
     * @param cdIndicadorEconomicoMoeda the value of field
     * 'cdIndicadorEconomicoMoeda'.
     */
    public void setCdIndicadorEconomicoMoeda(int cdIndicadorEconomicoMoeda)
    {
        this._cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
        this._has_cdIndicadorEconomicoMoeda = true;
    } //-- void setCdIndicadorEconomicoMoeda(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoOperacao'.
     * 
     * @param cdMotivoSituacaoOperacao the value of field
     * 'cdMotivoSituacaoOperacao'.
     */
    public void setCdMotivoSituacaoOperacao(int cdMotivoSituacaoOperacao)
    {
        this._cdMotivoSituacaoOperacao = cdMotivoSituacaoOperacao;
        this._has_cdMotivoSituacaoOperacao = true;
    } //-- void setCdMotivoSituacaoOperacao(int) 

    /**
     * Sets the value of field 'cdPermissaoEncerramentoContrato'.
     * 
     * @param cdPermissaoEncerramentoContrato the value of field
     * 'cdPermissaoEncerramentoContrato'.
     */
    public void setCdPermissaoEncerramentoContrato(java.lang.String cdPermissaoEncerramentoContrato)
    {
        this._cdPermissaoEncerramentoContrato = cdPermissaoEncerramentoContrato;
    } //-- void setCdPermissaoEncerramentoContrato(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSetorContrato'.
     * 
     * @param cdSetorContrato the value of field 'cdSetorContrato'.
     */
    public void setCdSetorContrato(int cdSetorContrato)
    {
        this._cdSetorContrato = cdSetorContrato;
        this._has_cdSetorContrato = true;
    } //-- void setCdSetorContrato(int) 

    /**
     * Sets the value of field 'cdSituacaoContrato'.
     * 
     * @param cdSituacaoContrato the value of field
     * 'cdSituacaoContrato'.
     */
    public void setCdSituacaoContrato(int cdSituacaoContrato)
    {
        this._cdSituacaoContrato = cdSituacaoContrato;
        this._has_cdSituacaoContrato = true;
    } //-- void setCdSituacaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUnidadeOperacionalContrato'.
     * 
     * @param cdUnidadeOperacionalContrato the value of field
     * 'cdUnidadeOperacionalContrato'.
     */
    public void setCdUnidadeOperacionalContrato(int cdUnidadeOperacionalContrato)
    {
        this._cdUnidadeOperacionalContrato = cdUnidadeOperacionalContrato;
        this._has_cdUnidadeOperacionalContrato = true;
    } //-- void setCdUnidadeOperacionalContrato(int) 

    /**
     * Sets the value of field 'cdUsuario'.
     * 
     * @param cdUsuario the value of field 'cdUsuario'.
     */
    public void setCdUsuario(java.lang.String cdUsuario)
    {
        this._cdUsuario = cdUsuario;
    } //-- void setCdUsuario(java.lang.String) 

    /**
     * Sets the value of field 'dsContratoNegocio'.
     * 
     * @param dsContratoNegocio the value of field
     * 'dsContratoNegocio'.
     */
    public void setDsContratoNegocio(java.lang.String dsContratoNegocio)
    {
        this._dsContratoNegocio = dsContratoNegocio;
    } //-- void setDsContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'hrAberturaContratoNegocio'.
     * 
     * @param hrAberturaContratoNegocio the value of field
     * 'hrAberturaContratoNegocio'.
     */
    public void setHrAberturaContratoNegocio(java.lang.String hrAberturaContratoNegocio)
    {
        this._hrAberturaContratoNegocio = hrAberturaContratoNegocio;
    } //-- void setHrAberturaContratoNegocio(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaUnidadeOrganizacional'.
     * 
     * @param nrSequenciaUnidadeOrganizacional the value of field
     * 'nrSequenciaUnidadeOrganizacional'.
     */
    public void setNrSequenciaUnidadeOrganizacional(int nrSequenciaUnidadeOrganizacional)
    {
        this._nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
        this._has_nrSequenciaUnidadeOrganizacional = true;
    } //-- void setNrSequenciaUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'vlSaldoVirtualTeste'.
     * 
     * @param vlSaldoVirtualTeste the value of field
     * 'vlSaldoVirtualTeste'.
     */
    public void setVlSaldoVirtualTeste(java.math.BigDecimal vlSaldoVirtualTeste)
    {
        this._vlSaldoVirtualTeste = vlSaldoVirtualTeste;
    } //-- void setVlSaldoVirtualTeste(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarDadosBasicoContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.request.AlterarDadosBasicoContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.request.AlterarDadosBasicoContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.request.AlterarDadosBasicoContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterardadosbasicocontrato.request.AlterarDadosBasicoContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
