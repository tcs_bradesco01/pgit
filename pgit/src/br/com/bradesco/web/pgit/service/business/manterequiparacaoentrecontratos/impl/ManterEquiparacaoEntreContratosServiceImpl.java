/**
 * Nome: br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.impl
 * Compilador: JDK 1.5
 * Propósito: INSERIR O PROPÓSITO DAS CLASSES DO PACOTE
 * Data da criação: <dd/MM/yyyy>
 * Parâmetros de compilação: -d
 */
package br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.impl;

import java.util.ArrayList;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.IManterEquiparacaoEntreContratosService;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.IncluirEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.IncluirEquiparacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoOcorrencias;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarServicoEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarServicoEquiparacaoContratoOcorrencias;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarServicoEquiparacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.IncluirEquiparacaoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.response.IncluirEquiparacaoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadeequiparacaocontrato.request.ListarModalidadeEquiparacaoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmodalidadeequiparacaocontrato.response.ListarModalidadeEquiparacaoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicoequiparacaocontrato.request.ListarServicoEquiparacaoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarservicoequiparacaocontrato.response.ListarServicoEquiparacaoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirequiparacaocontrato.request.Ocorrencias;

/**
 * Nome: ManterEquiparacaoEntreContratosServiceImpl
 * <p>
 * Propósito: Implementação do adaptador ManterEquiparacaoEntreContratos
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IManterEquiparacaoEntreContratosService
 */
public class ManterEquiparacaoEntreContratosServiceImpl implements IManterEquiparacaoEntreContratosService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * Manter equiparacao entre contratos service impl.
	 */
	public ManterEquiparacaoEntreContratosServiceImpl() {
		// TODO: Implementação
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.IManterEquiparacaoEntreContratosService#listarModalidadeEquiparacaoContrato(br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoEntradaDTO)
	 */
	public ListarModalidadeEquiparacaoContratoSaidaDTO listarModalidadeEquiparacaoContrato(
					ListarModalidadeEquiparacaoContratoEntradaDTO entrada) {

		ListarModalidadeEquiparacaoContratoRequest request = new ListarModalidadeEquiparacaoContratoRequest();
		ListarModalidadeEquiparacaoContratoResponse response = null;
		ListarModalidadeEquiparacaoContratoSaidaDTO saida = new ListarModalidadeEquiparacaoContratoSaidaDTO();

		request.setNrOcorrencias(50);
		request.setCdPessoaJuridicaContrato1(entrada.getCdPessoaJuridicaContrato1());
		request.setCdTipoContratoNegocio1(entrada.getCdTipoContratoNegocio1());
		request.setNrSequenciaContratoNegocio1(entrada.getNrSequenciaContratoNegocio1());
		request.setCdPessoaJuridicaContrato2(entrada.getCdPessoaJuridicaContrato2());
		request.setCdTipoContratoNegocio2(entrada.getCdTipoContratoNegocio2());
		request.setNrSequenciaContratoNegocio2(entrada.getNrSequenciaContratoNegocio2());

		response = getFactoryAdapter().getListarModalidadeEquiparacaoContratoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());

		List<ListarModalidadeEquiparacaoContratoOcorrencias> ocorrencias = new ArrayList<ListarModalidadeEquiparacaoContratoOcorrencias>();
		ListarModalidadeEquiparacaoContratoOcorrencias ocorrencia = null;

		for (int i = 0; i < response.getNumeroLinhas(); i++) {
			ocorrencia = new ListarModalidadeEquiparacaoContratoOcorrencias();
			ocorrencia.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			ocorrencia.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			ocorrencia.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			ocorrencia.setDsProdutoOperRelacionado(response.getOcorrencias(i).getDsProdutoOperRelacionado());

			ocorrencias.add(ocorrencia);
		}
		saida.setOcorrencias(ocorrencias);

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.IManterEquiparacaoEntreContratosService#listarServicoEquiparacaoContrato(br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarServicoEquiparacaoContratoEntradaDTO)
	 */
	public ListarServicoEquiparacaoContratoSaidaDTO listarServicoEquiparacaoContrato(
					ListarServicoEquiparacaoContratoEntradaDTO entrada) {

		ListarServicoEquiparacaoContratoRequest request = new ListarServicoEquiparacaoContratoRequest();
		ListarServicoEquiparacaoContratoResponse response = null;
		ListarServicoEquiparacaoContratoSaidaDTO saida = new ListarServicoEquiparacaoContratoSaidaDTO();

		request.setNrOcorrencias(50);
		request.setCdPessoaJuridicaContrato1(entrada.getCdPessoaJuridicaContrato1());
		request.setCdTipoContratoNegocio1(entrada.getCdTipoContratoNegocio1());
		request.setNrSequenciaContratoNegocio1(entrada.getNrSequenciaContratoNegocio1());
		request.setCdPessoaJuridicaContrato2(entrada.getCdPessoaJuridicaContrato2());
		request.setCdTipoContratoNegocio2(entrada.getCdTipoContratoNegocio2());
		request.setNrSequenciaContratoNegocio2(entrada.getNrSequenciaContratoNegocio2());

		response = getFactoryAdapter().getListarServicoEquiparacaoContratoPDCAdapter().invokeProcess(request);

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNumeroLinhas(response.getNumeroLinhas());

		List<ListarServicoEquiparacaoContratoOcorrencias> ocorrencias = new ArrayList<ListarServicoEquiparacaoContratoOcorrencias>();
		ListarServicoEquiparacaoContratoOcorrencias ocorrencia = null;

		for (int i = 0; i < response.getNumeroLinhas(); i++) {
			ocorrencia = new ListarServicoEquiparacaoContratoOcorrencias();
			ocorrencia.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			ocorrencia.setDsProdutoOperRelacionado(response.getOcorrencias(i).getDsProdutoOperRelacionado());

			ocorrencias.add(ocorrencia);
		}
		saida.setOcorrencias(ocorrencias);

		return saida;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.IManterEquiparacaoEntreContratosService#incluirEquiparacaoContrato(br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.IncluirEquiparacaoContratoEntradaDTO)
	 */
	public IncluirEquiparacaoContratoSaidaDTO incluirEquiparacaoContrato(IncluirEquiparacaoContratoEntradaDTO entrada) {
		IncluirEquiparacaoContratoRequest request = new IncluirEquiparacaoContratoRequest();
		IncluirEquiparacaoContratoResponse response = null;
		IncluirEquiparacaoContratoSaidaDTO saida = new IncluirEquiparacaoContratoSaidaDTO();

		request.setMaxOcorrencias(entrada.getOcorrencias().size());

		Ocorrencias ocorrencia = null;
		for (int i = 0; i < entrada.getOcorrencias().size(); i++) {
			ocorrencia = new Ocorrencias();
			ocorrencia.setCdTipoSelecao(entrada.getOcorrencias().get(i).getCdTipoSelecao());
			ocorrencia.setCdPessoaJuridicaContrato1(entrada.getOcorrencias().get(i).getCdPessoaJuridicaContrato1());
			ocorrencia.setCdTipoContratoNegocio1(entrada.getOcorrencias().get(i).getCdTipoContratoNegocio1());
			ocorrencia.setNrSequenciaContratoNegocio1(entrada.getOcorrencias().get(i).getNrSequenciaContratoNegocio1());
			ocorrencia.setCdPessoaJuridicaContrato2(entrada.getOcorrencias().get(i).getCdPessoaJuridicaContrato2());
			ocorrencia.setCdTipoContratoNegocio2(entrada.getOcorrencias().get(i).getCdTipoContratoNegocio2());
			ocorrencia.setNrSequenciaContratoNegocio2(entrada.getOcorrencias().get(i).getNrSequenciaContratoNegocio2());
			ocorrencia.setCdProdutoServicoOperacao(entrada.getOcorrencias().get(i).getCdProdutoServicoOperacao());
			ocorrencia.setCdProdutoOperacaoRelacionado(entrada.getOcorrencias().get(i)
							.getCdProdutoOperacaoRelacionado());

			request.addOcorrencias(ocorrencia);
		}
		
		response = getFactoryAdapter().getIncluirEquiparacaoContratoPDCAdapter().invokeProcess(request);
		
		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		
		return saida;
	}

}