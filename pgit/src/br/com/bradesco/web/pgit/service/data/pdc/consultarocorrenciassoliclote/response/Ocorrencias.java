/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCpfCnpjPagador
     */
    private long _cdCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPagador
     */
    private boolean _has_cdCpfCnpjPagador;

    /**
     * Field _cdFilialCpfCnpjPagador
     */
    private int _cdFilialCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjPagador
     */
    private boolean _has_cdFilialCpfCnpjPagador;

    /**
     * Field _cdTipoCtaRecebedor
     */
    private int _cdTipoCtaRecebedor = 0;

    /**
     * keeps track of state for field: _cdTipoCtaRecebedor
     */
    private boolean _has_cdTipoCtaRecebedor;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _dsTipoServico
     */
    private java.lang.String _dsTipoServico;

    /**
     * Field _dsModalidade
     */
    private java.lang.String _dsModalidade;

    /**
     * Field _cdControlePagamento
     */
    private java.lang.String _cdControlePagamento;

    /**
     * Field _dsPagto
     */
    private java.lang.String _dsPagto;

    /**
     * Field _vlPagto
     */
    private java.math.BigDecimal _vlPagto = new java.math.BigDecimal("0");

    /**
     * Field _dsRazaoSocialFav
     */
    private java.lang.String _dsRazaoSocialFav;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdBcoRecebedor
     */
    private int _cdBcoRecebedor = 0;

    /**
     * keeps track of state for field: _cdBcoRecebedor
     */
    private boolean _has_cdBcoRecebedor;

    /**
     * Field _cdAgenciaRecebedor
     */
    private int _cdAgenciaRecebedor = 0;

    /**
     * keeps track of state for field: _cdAgenciaRecebedor
     */
    private boolean _has_cdAgenciaRecebedor;

    /**
     * Field _cdContaRecebedor
     */
    private long _cdContaRecebedor = 0;

    /**
     * keeps track of state for field: _cdContaRecebedor
     */
    private boolean _has_cdContaRecebedor;

    /**
     * Field _dsMotivoErro
     */
    private java.lang.String _dsMotivoErro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagto(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdAgenciaRecebedor
     * 
     */
    public void deleteCdAgenciaRecebedor()
    {
        this._has_cdAgenciaRecebedor= false;
    } //-- void deleteCdAgenciaRecebedor() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdBcoRecebedor
     * 
     */
    public void deleteCdBcoRecebedor()
    {
        this._has_cdBcoRecebedor= false;
    } //-- void deleteCdBcoRecebedor() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdContaRecebedor
     * 
     */
    public void deleteCdContaRecebedor()
    {
        this._has_cdContaRecebedor= false;
    } //-- void deleteCdContaRecebedor() 

    /**
     * Method deleteCdCpfCnpjPagador
     * 
     */
    public void deleteCdCpfCnpjPagador()
    {
        this._has_cdCpfCnpjPagador= false;
    } //-- void deleteCdCpfCnpjPagador() 

    /**
     * Method deleteCdFilialCpfCnpjPagador
     * 
     */
    public void deleteCdFilialCpfCnpjPagador()
    {
        this._has_cdFilialCpfCnpjPagador= false;
    } //-- void deleteCdFilialCpfCnpjPagador() 

    /**
     * Method deleteCdTipoCtaRecebedor
     * 
     */
    public void deleteCdTipoCtaRecebedor()
    {
        this._has_cdTipoCtaRecebedor= false;
    } //-- void deleteCdTipoCtaRecebedor() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdAgenciaRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaRecebedor'.
     */
    public int getCdAgenciaRecebedor()
    {
        return this._cdAgenciaRecebedor;
    } //-- int getCdAgenciaRecebedor() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdBcoRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdBcoRecebedor'.
     */
    public int getCdBcoRecebedor()
    {
        return this._cdBcoRecebedor;
    } //-- int getCdBcoRecebedor() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdContaRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdContaRecebedor'.
     */
    public long getCdContaRecebedor()
    {
        return this._cdContaRecebedor;
    } //-- long getCdContaRecebedor() 

    /**
     * Returns the value of field 'cdControlePagamento'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamento'.
     */
    public java.lang.String getCdControlePagamento()
    {
        return this._cdControlePagamento;
    } //-- java.lang.String getCdControlePagamento() 

    /**
     * Returns the value of field 'cdCpfCnpjPagador'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPagador'.
     */
    public long getCdCpfCnpjPagador()
    {
        return this._cdCpfCnpjPagador;
    } //-- long getCdCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjPagador'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjPagador'.
     */
    public int getCdFilialCpfCnpjPagador()
    {
        return this._cdFilialCpfCnpjPagador;
    } //-- int getCdFilialCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdTipoCtaRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdTipoCtaRecebedor'.
     */
    public int getCdTipoCtaRecebedor()
    {
        return this._cdTipoCtaRecebedor;
    } //-- int getCdTipoCtaRecebedor() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModalidade'.
     */
    public java.lang.String getDsModalidade()
    {
        return this._dsModalidade;
    } //-- java.lang.String getDsModalidade() 

    /**
     * Returns the value of field 'dsMotivoErro'.
     * 
     * @return String
     * @return the value of field 'dsMotivoErro'.
     */
    public java.lang.String getDsMotivoErro()
    {
        return this._dsMotivoErro;
    } //-- java.lang.String getDsMotivoErro() 

    /**
     * Returns the value of field 'dsPagto'.
     * 
     * @return String
     * @return the value of field 'dsPagto'.
     */
    public java.lang.String getDsPagto()
    {
        return this._dsPagto;
    } //-- java.lang.String getDsPagto() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsRazaoSocialFav'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocialFav'.
     */
    public java.lang.String getDsRazaoSocialFav()
    {
        return this._dsRazaoSocialFav;
    } //-- java.lang.String getDsRazaoSocialFav() 

    /**
     * Returns the value of field 'dsTipoServico'.
     * 
     * @return String
     * @return the value of field 'dsTipoServico'.
     */
    public java.lang.String getDsTipoServico()
    {
        return this._dsTipoServico;
    } //-- java.lang.String getDsTipoServico() 

    /**
     * Returns the value of field 'vlPagto'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagto'.
     */
    public java.math.BigDecimal getVlPagto()
    {
        return this._vlPagto;
    } //-- java.math.BigDecimal getVlPagto() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdAgenciaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaRecebedor()
    {
        return this._has_cdAgenciaRecebedor;
    } //-- boolean hasCdAgenciaRecebedor() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdBcoRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBcoRecebedor()
    {
        return this._has_cdBcoRecebedor;
    } //-- boolean hasCdBcoRecebedor() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdContaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaRecebedor()
    {
        return this._has_cdContaRecebedor;
    } //-- boolean hasCdContaRecebedor() 

    /**
     * Method hasCdCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPagador()
    {
        return this._has_cdCpfCnpjPagador;
    } //-- boolean hasCdCpfCnpjPagador() 

    /**
     * Method hasCdFilialCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjPagador()
    {
        return this._has_cdFilialCpfCnpjPagador;
    } //-- boolean hasCdFilialCpfCnpjPagador() 

    /**
     * Method hasCdTipoCtaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCtaRecebedor()
    {
        return this._has_cdTipoCtaRecebedor;
    } //-- boolean hasCdTipoCtaRecebedor() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdAgenciaRecebedor'.
     * 
     * @param cdAgenciaRecebedor the value of field
     * 'cdAgenciaRecebedor'.
     */
    public void setCdAgenciaRecebedor(int cdAgenciaRecebedor)
    {
        this._cdAgenciaRecebedor = cdAgenciaRecebedor;
        this._has_cdAgenciaRecebedor = true;
    } //-- void setCdAgenciaRecebedor(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdBcoRecebedor'.
     * 
     * @param cdBcoRecebedor the value of field 'cdBcoRecebedor'.
     */
    public void setCdBcoRecebedor(int cdBcoRecebedor)
    {
        this._cdBcoRecebedor = cdBcoRecebedor;
        this._has_cdBcoRecebedor = true;
    } //-- void setCdBcoRecebedor(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdContaRecebedor'.
     * 
     * @param cdContaRecebedor the value of field 'cdContaRecebedor'
     */
    public void setCdContaRecebedor(long cdContaRecebedor)
    {
        this._cdContaRecebedor = cdContaRecebedor;
        this._has_cdContaRecebedor = true;
    } //-- void setCdContaRecebedor(long) 

    /**
     * Sets the value of field 'cdControlePagamento'.
     * 
     * @param cdControlePagamento the value of field
     * 'cdControlePagamento'.
     */
    public void setCdControlePagamento(java.lang.String cdControlePagamento)
    {
        this._cdControlePagamento = cdControlePagamento;
    } //-- void setCdControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdCpfCnpjPagador'.
     * 
     * @param cdCpfCnpjPagador the value of field 'cdCpfCnpjPagador'
     */
    public void setCdCpfCnpjPagador(long cdCpfCnpjPagador)
    {
        this._cdCpfCnpjPagador = cdCpfCnpjPagador;
        this._has_cdCpfCnpjPagador = true;
    } //-- void setCdCpfCnpjPagador(long) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjPagador'.
     * 
     * @param cdFilialCpfCnpjPagador the value of field
     * 'cdFilialCpfCnpjPagador'.
     */
    public void setCdFilialCpfCnpjPagador(int cdFilialCpfCnpjPagador)
    {
        this._cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
        this._has_cdFilialCpfCnpjPagador = true;
    } //-- void setCdFilialCpfCnpjPagador(int) 

    /**
     * Sets the value of field 'cdTipoCtaRecebedor'.
     * 
     * @param cdTipoCtaRecebedor the value of field
     * 'cdTipoCtaRecebedor'.
     */
    public void setCdTipoCtaRecebedor(int cdTipoCtaRecebedor)
    {
        this._cdTipoCtaRecebedor = cdTipoCtaRecebedor;
        this._has_cdTipoCtaRecebedor = true;
    } //-- void setCdTipoCtaRecebedor(int) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(java.lang.String dsModalidade)
    {
        this._dsModalidade = dsModalidade;
    } //-- void setDsModalidade(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoErro'.
     * 
     * @param dsMotivoErro the value of field 'dsMotivoErro'.
     */
    public void setDsMotivoErro(java.lang.String dsMotivoErro)
    {
        this._dsMotivoErro = dsMotivoErro;
    } //-- void setDsMotivoErro(java.lang.String) 

    /**
     * Sets the value of field 'dsPagto'.
     * 
     * @param dsPagto the value of field 'dsPagto'.
     */
    public void setDsPagto(java.lang.String dsPagto)
    {
        this._dsPagto = dsPagto;
    } //-- void setDsPagto(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocialFav'.
     * 
     * @param dsRazaoSocialFav the value of field 'dsRazaoSocialFav'
     */
    public void setDsRazaoSocialFav(java.lang.String dsRazaoSocialFav)
    {
        this._dsRazaoSocialFav = dsRazaoSocialFav;
    } //-- void setDsRazaoSocialFav(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoServico'.
     * 
     * @param dsTipoServico the value of field 'dsTipoServico'.
     */
    public void setDsTipoServico(java.lang.String dsTipoServico)
    {
        this._dsTipoServico = dsTipoServico;
    } //-- void setDsTipoServico(java.lang.String) 

    /**
     * Sets the value of field 'vlPagto'.
     * 
     * @param vlPagto the value of field 'vlPagto'.
     */
    public void setVlPagto(java.math.BigDecimal vlPagto)
    {
        this._vlPagto = vlPagto;
    } //-- void setVlPagto(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarocorrenciassoliclote.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
