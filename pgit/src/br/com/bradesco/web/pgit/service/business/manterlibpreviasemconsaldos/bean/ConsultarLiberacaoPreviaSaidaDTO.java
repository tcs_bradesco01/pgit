/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarLiberacaoPreviaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarLiberacaoPreviaSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo nrCpfCnpj. */
	private String nrCpfCnpj;
	
	/** Atributo nrCpfCnpjFormatado. */
	private String nrCpfCnpjFormatado;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;	
	
	/** Atributo cdTipoPessoaJuridica. */
	private Integer cdTipoPessoaJuridica;
	
	/** Atributo nrSequencialContratoNegocio. */
	private Long nrSequencialContratoNegocio;
	
	/** Atributo dsCodigoPessoaJuridica. */
	private String dsCodigoPessoaJuridica;
	
	/** Atributo cdTipoSolicitacao. */
	private Integer cdTipoSolicitacao;
	
	/** Atributo nrSolicitacao. */
	private Integer nrSolicitacao;
	
	/** Atributo dtLiberacao. */
	private String dtLiberacao;
	
	/** Atributo dtLiberacaoFormatada. */
	private String dtLiberacaoFormatada;
	
	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;
	
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;
	
	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;
	
	/** Atributo cdDigitoAgenciaDebito. */
	private Integer cdDigitoAgenciaDebito;
	
	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;
	
	/** Atributo cdContaDebito. */
	private Long cdContaDebito;
	
	/** Atributo cdDigitoContaDebito. */
	private String cdDigitoContaDebito;
	
	/** Atributo vlAutorizado. */
	private BigDecimal vlAutorizado;
	
	/** Atributo vlUtilizado. */
	private BigDecimal vlUtilizado;
	
	/** Atributo qtPagamento. */
	private Long qtPagamento;
	
	/** Atributo qtPagamentoFormatado. */
	private String qtPagamentoFormatado;
	
	/** Atributo bancoAgenciaContaDebitoFormatada. */
	private String bancoAgenciaContaDebitoFormatada;
	  
	  /** Atributo cdTipoContaDebito. */
  	private String cdTipoContaDebito;
	  
  	/** Atributo situacaoSolicitacao. */
  	private Integer situacaoSolicitacao;
	  
  	/** Atributo dsSituacaoSolicitacao. */
  	private String dsSituacaoSolicitacao;
	
	/**
	 * Get: cdTipoContaDebito.
	 *
	 * @return cdTipoContaDebito
	 */
	public String getCdTipoContaDebito() {
		return cdTipoContaDebito;
	}
	
	/**
	 * Set: cdTipoContaDebito.
	 *
	 * @param cdTipoContaDebito the cd tipo conta debito
	 */
	public void setCdTipoContaDebito(String cdTipoContaDebito) {
		this.cdTipoContaDebito = cdTipoContaDebito;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: situacaoSolicitacao.
	 *
	 * @return situacaoSolicitacao
	 */
	public Integer getSituacaoSolicitacao() {
		return situacaoSolicitacao;
	}
	
	/**
	 * Set: situacaoSolicitacao.
	 *
	 * @param situacaoSolicitacao the situacao solicitacao
	 */
	public void setSituacaoSolicitacao(Integer situacaoSolicitacao) {
		this.situacaoSolicitacao = situacaoSolicitacao;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdDigitoAgenciaDebito.
	 *
	 * @return cdDigitoAgenciaDebito
	 */
	public Integer getCdDigitoAgenciaDebito() {
		return cdDigitoAgenciaDebito;
	}
	
	/**
	 * Set: cdDigitoAgenciaDebito.
	 *
	 * @param cdDigitoAgenciaDebito the cd digito agencia debito
	 */
	public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
		this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
	}
	
	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public String getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}
	
	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoPessoaJuridica.
	 *
	 * @return cdTipoPessoaJuridica
	 */
	public Integer getCdTipoPessoaJuridica() {
		return cdTipoPessoaJuridica;
	}
	
	/**
	 * Set: cdTipoPessoaJuridica.
	 *
	 * @param cdTipoPessoaJuridica the cd tipo pessoa juridica
	 */
	public void setCdTipoPessoaJuridica(Integer cdTipoPessoaJuridica) {
		this.cdTipoPessoaJuridica = cdTipoPessoaJuridica;
	}
	
	/**
	 * Get: cdTipoSolicitacao.
	 *
	 * @return cdTipoSolicitacao
	 */
	public Integer getCdTipoSolicitacao() {
		return cdTipoSolicitacao;
	}
	
	/**
	 * Set: cdTipoSolicitacao.
	 *
	 * @param cdTipoSolicitacao the cd tipo solicitacao
	 */
	public void setCdTipoSolicitacao(Integer cdTipoSolicitacao) {
		this.cdTipoSolicitacao = cdTipoSolicitacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}
	
	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}
	
	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}
	
	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}
	
	/**
	 * Get: dsCodigoPessoaJuridica.
	 *
	 * @return dsCodigoPessoaJuridica
	 */
	public String getDsCodigoPessoaJuridica() {
		return dsCodigoPessoaJuridica;
	}
	
	/**
	 * Set: dsCodigoPessoaJuridica.
	 *
	 * @param dsCodigoPessoaJuridica the ds codigo pessoa juridica
	 */
	public void setDsCodigoPessoaJuridica(String dsCodigoPessoaJuridica) {
		this.dsCodigoPessoaJuridica = dsCodigoPessoaJuridica;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dtLiberacao.
	 *
	 * @return dtLiberacao
	 */
	public String getDtLiberacao() {
		return dtLiberacao;
	}
	
	/**
	 * Set: dtLiberacao.
	 *
	 * @param dtLiberacao the dt liberacao
	 */
	public void setDtLiberacao(String dtLiberacao) {
		this.dtLiberacao = dtLiberacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrCpfCnpj.
	 *
	 * @return nrCpfCnpj
	 */
	public String getNrCpfCnpj() {
		return nrCpfCnpj;
	}
	
	/**
	 * Set: nrCpfCnpj.
	 *
	 * @param nrCpfCnpj the nr cpf cnpj
	 */
	public void setNrCpfCnpj(String nrCpfCnpj) {
		this.nrCpfCnpj = nrCpfCnpj;
	}
	
	/**
	 * Get: nrSequencialContratoNegocio.
	 *
	 * @return nrSequencialContratoNegocio
	 */
	public Long getNrSequencialContratoNegocio() {
		return nrSequencialContratoNegocio;
	}
	
	/**
	 * Set: nrSequencialContratoNegocio.
	 *
	 * @param nrSequencialContratoNegocio the nr sequencial contrato negocio
	 */
	public void setNrSequencialContratoNegocio(Long nrSequencialContratoNegocio) {
		this.nrSequencialContratoNegocio = nrSequencialContratoNegocio;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: qtPagamento.
	 *
	 * @return qtPagamento
	 */
	public Long getQtPagamento() {
		return qtPagamento;
	}
	
	/**
	 * Set: qtPagamento.
	 *
	 * @param qtPagamento the qt pagamento
	 */
	public void setQtPagamento(Long qtPagamento) {
		this.qtPagamento = qtPagamento;
	}
	
	/**
	 * Get: vlAutorizado.
	 *
	 * @return vlAutorizado
	 */
	public BigDecimal getVlAutorizado() {
		return vlAutorizado;
	}
	
	/**
	 * Set: vlAutorizado.
	 *
	 * @param vlAutorizado the vl autorizado
	 */
	public void setVlAutorizado(BigDecimal vlAutorizado) {
		this.vlAutorizado = vlAutorizado;
	}
	
	/**
	 * Get: vlUtilizado.
	 *
	 * @return vlUtilizado
	 */
	public BigDecimal getVlUtilizado() {
		return vlUtilizado;
	}
	
	/**
	 * Set: vlUtilizado.
	 *
	 * @param vlUtilizado the vl utilizado
	 */
	public void setVlUtilizado(BigDecimal vlUtilizado) {
		this.vlUtilizado = vlUtilizado;
	}
	
	/**
	 * Get: bancoAgenciaContaDebitoFormatada.
	 *
	 * @return bancoAgenciaContaDebitoFormatada
	 */
	public String getBancoAgenciaContaDebitoFormatada() {
		return bancoAgenciaContaDebitoFormatada;
	}
	
	/**
	 * Set: bancoAgenciaContaDebitoFormatada.
	 *
	 * @param bancoAgenciaContaDebitoFormatada the banco agencia conta debito formatada
	 */
	public void setBancoAgenciaContaDebitoFormatada(
			String bancoAgenciaContaDebitoFormatada) {
		this.bancoAgenciaContaDebitoFormatada = bancoAgenciaContaDebitoFormatada;
	}
	
	/**
	 * Get: qtPagamentoFormatado.
	 *
	 * @return qtPagamentoFormatado
	 */
	public String getQtPagamentoFormatado() {
		return qtPagamentoFormatado;
	}
	
	/**
	 * Set: qtPagamentoFormatado.
	 *
	 * @param qtPagamentoFormatado the qt pagamento formatado
	 */
	public void setQtPagamentoFormatado(String qtPagamentoFormatado) {
		this.qtPagamentoFormatado = qtPagamentoFormatado;
	}
	
	/**
	 * Get: nrCpfCnpjFormatado.
	 *
	 * @return nrCpfCnpjFormatado
	 */
	public String getNrCpfCnpjFormatado() {
		return nrCpfCnpjFormatado;
	}
	
	/**
	 * Set: nrCpfCnpjFormatado.
	 *
	 * @param nrCpfCnpjFormatado the nr cpf cnpj formatado
	 */
	public void setNrCpfCnpjFormatado(String nrCpfCnpjFormatado) {
		this.nrCpfCnpjFormatado = nrCpfCnpjFormatado;
	}
	
	/**
	 * Get: dtLiberacaoFormatada.
	 *
	 * @return dtLiberacaoFormatada
	 */
	public String getDtLiberacaoFormatada() {
		return dtLiberacaoFormatada;
	}
	
	/**
	 * Set: dtLiberacaoFormatada.
	 *
	 * @param dtLiberacaoFormatada the dt liberacao formatada
	 */
	public void setDtLiberacaoFormatada(String dtLiberacaoFormatada) {
		this.dtLiberacaoFormatada = dtLiberacaoFormatada;
	}

	
}
