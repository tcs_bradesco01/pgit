package br.com.bradesco.web.pgit.service.business.confcestastarifas.dto;

import java.math.BigDecimal;

public class OcorrenciasSaidaDTO {
	
	private Integer cdCestaTariafaFlexbilizacao;
	private String dsCestaTariafaFlexbilizacao; 
	private BigDecimal vlTarifaPadrao;
	private BigDecimal vlTariafaFlexbilizacaoContrato;
	private BigDecimal porcentagemTariafaFlexbilizacaoContrato;
	private String dataVigenciaInicio; 
	private String dataVigenciaFim;

	public Integer getCdCestaTariafaFlexbilizacao() {
		return cdCestaTariafaFlexbilizacao;
	}

	public void setCdCestaTariafaFlexbilizacao(Integer cdCestaTariafaFlexbilizacao) {
		this.cdCestaTariafaFlexbilizacao = cdCestaTariafaFlexbilizacao;
	}

	public String getDsCestaTariafaFlexbilizacao() {
		return dsCestaTariafaFlexbilizacao;
	}

	public void setDsCestaTariafaFlexbilizacao(String dsCestaTariafaFlexbilizacao) {
		this.dsCestaTariafaFlexbilizacao = dsCestaTariafaFlexbilizacao;
	}

	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}

	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}

	public BigDecimal getVlTariafaFlexbilizacaoContrato() {
		return vlTariafaFlexbilizacaoContrato;
	}

	public void setVlTariafaFlexbilizacaoContrato(
			BigDecimal vlTariafaFlexbilizacaoContrato) {
		this.vlTariafaFlexbilizacaoContrato = vlTariafaFlexbilizacaoContrato;
	}

	public BigDecimal getPorcentagemTariafaFlexbilizacaoContrato() {
		return porcentagemTariafaFlexbilizacaoContrato;
	}

	public void setPorcentagemTariafaFlexbilizacaoContrato(
			BigDecimal porcentagemTariafaFlexbilizacaoContrato) {
		this.porcentagemTariafaFlexbilizacaoContrato = porcentagemTariafaFlexbilizacaoContrato;
	}

	public String getDataVigenciaInicio() {
		return dataVigenciaInicio;
	}

	public void setDataVigenciaInicio(String dataVigenciaInicio) {
		this.dataVigenciaInicio = dataVigenciaInicio;
	}

	public String getDataVigenciaFim() {
		return dataVigenciaFim;
	}

	public void setDataVigenciaFim(String dataVigenciaFim) {
		this.dataVigenciaFim = dataVigenciaFim;
	}

}
