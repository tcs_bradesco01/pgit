/*
 * Nome: br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

import static br.com.bradesco.web.pgit.utils.SiteUtil.isEmptyOrNull;
import static br.com.bradesco.web.pgit.utils.SiteUtil.not;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: DetalharOperacaoServicoPagtoIntegradoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharOperacaoServicoPagtoIntegradoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdOperacaoServicoIntegrado. */
	private Integer cdOperacaoServicoIntegrado;
	
	/** Atributo cdOperacaoCanalIncusao. */
	private String cdOperacaoCanalIncusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private String cdTipoCanalInclusao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private String cdTipoCanalManutencao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo usuarioInclusaoFormatado. */
	private String usuarioInclusaoFormatado;
	
	/** Atributo complementoInclusaoFormatado. */
	private String complementoInclusaoFormatado;
	
	/** Atributo usuarioManutencaoFormatado. */
	private String usuarioManutencaoFormatado;
	
	/** Atributo complementoManutencaoFormatado. */
	private String complementoManutencaoFormatado;

	/** Atributo cdNatureza. */
	private String cdNatureza;
	
	/**
	 * Atributo Integer
	 */
	private Integer cdNaturezaOperacaoPagamento;

	/**
	 * Atributo Integer
	 */
	private Integer cdTipoAlcadaTarifa;
	
	/**
	 * Atributo String
	 */
	private String dsTipoAlcadaTarifa;
	
	/**
	 * Atributo BigDecimal
	 */
	private BigDecimal vrAlcadaTarifaAgencia;
	
	/**
	 * Atributo BigDecimal
	 */
	private BigDecimal pcAlcadaTarifaAgencia;

	/**
	 * Atributo String
	 */
	private String dsTipoCanalInclusao;

	/**
	 * Atributo String
	 */
	private String dsTipoCanalManutencao;

	/**
	 * Get: cdNatureza.
	 *
	 * @return cdNatureza
	 */
	public String getCdNatureza() {
	    return cdNatureza;
	}

	/**
	 * Set: cdNatureza.
	 *
	 * @param cdNatureza the cd natureza
	 */
	public void setCdNatureza(String cdNatureza) {
	    this.cdNatureza = cdNatureza;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdOperacaoServicoIntegrado.
	 *
	 * @return cdOperacaoServicoIntegrado
	 */
	public Integer getCdOperacaoServicoIntegrado(){
		return cdOperacaoServicoIntegrado;
	}

	/**
	 * Set: cdOperacaoServicoIntegrado.
	 *
	 * @param cdOperacaoServicoIntegrado the cd operacao servico integrado
	 */
	public void setCdOperacaoServicoIntegrado(Integer cdOperacaoServicoIntegrado){
		this.cdOperacaoServicoIntegrado = cdOperacaoServicoIntegrado;
	}

	/**
	 * Get: cdOperacaoCanalIncusao.
	 *
	 * @return cdOperacaoCanalIncusao
	 */
	public String getCdOperacaoCanalIncusao(){
		return cdOperacaoCanalIncusao;
	}

	/**
	 * Set: cdOperacaoCanalIncusao.
	 *
	 * @param cdOperacaoCanalIncusao the cd operacao canal incusao
	 */
	public void setCdOperacaoCanalIncusao(String cdOperacaoCanalIncusao){
		this.cdOperacaoCanalIncusao = cdOperacaoCanalIncusao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public String getCdTipoCanalInclusao(){
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(String cdTipoCanalInclusao){
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao(){
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao){
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno(){
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno){
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro(){
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro){
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao(){
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao){
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public String getCdTipoCanalManutencao(){
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(String cdTipoCanalManutencao){
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao(){
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao){
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno(){
		return cdUsuarioManutencaoExterno;
	}

	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno){
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro(){
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro){
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: complementoInclusaoFormatado.
	 *
	 * @return complementoInclusaoFormatado
	 */
	public String getComplementoInclusaoFormatado() {
		return complementoInclusaoFormatado;
	}

	/**
	 * Set: complementoInclusaoFormatado.
	 *
	 * @param complementoInclusaoFormatado the complemento inclusao formatado
	 */
	public void setComplementoInclusaoFormatado(String complementoInclusaoFormatado) {
		this.complementoInclusaoFormatado = complementoInclusaoFormatado;
	}

	/**
	 * Get: complementoManutencaoFormatado.
	 *
	 * @return complementoManutencaoFormatado
	 */
	public String getComplementoManutencaoFormatado() {
		return complementoManutencaoFormatado;
	}

	/**
	 * Set: complementoManutencaoFormatado.
	 *
	 * @param complementoManutencaoFormatado the complemento manutencao formatado
	 */
	public void setComplementoManutencaoFormatado(
			String complementoManutencaoFormatado) {
		this.complementoManutencaoFormatado = complementoManutencaoFormatado;
	}

	/**
	 * Get: usuarioInclusaoFormatado.
	 *
	 * @return usuarioInclusaoFormatado
	 */
	public String getUsuarioInclusaoFormatado() {
		return usuarioInclusaoFormatado;
	}

	/**
	 * Set: usuarioInclusaoFormatado.
	 *
	 * @param usuarioInclusaoFormatado the usuario inclusao formatado
	 */
	public void setUsuarioInclusaoFormatado(String usuarioInclusaoFormatado) {
		this.usuarioInclusaoFormatado = usuarioInclusaoFormatado;
	}

	/**
	 * Get: usuarioManutencaoFormatado.
	 *
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}

	/**
	 * Set: usuarioManutencaoFormatado.
	 *
	 * @param usuarioManutencaoFormatado the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}

    /**
     * Nome: getCdNaturezaOperacaoPagamento
     *
     * @return cdNaturezaOperacaoPagamento
     */
    public Integer getCdNaturezaOperacaoPagamento() {
        return cdNaturezaOperacaoPagamento;
    }

    /**
     * Nome: setCdNaturezaOperacaoPagamento
     *
     * @param cdNaturezaOperacaoPagamento
     */
    public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
        this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
    }

    /**
     * Nome: getCdTipoAlcadaTarifa
     *
     * @return cdTipoAlcadaTarifa
     */
    public Integer getCdTipoAlcadaTarifa() {
        return cdTipoAlcadaTarifa;
    }

    /**
     * Nome: setCdTipoAlcadaTarifa
     *
     * @param cdTipoAlcadaTarifa
     */
    public void setCdTipoAlcadaTarifa(Integer cdTipoAlcadaTarifa) {
        this.cdTipoAlcadaTarifa = cdTipoAlcadaTarifa;
    }

    /**
     * Nome: getDsTipoAlcadaTarifa
     *
     * @return dsTipoAlcadaTarifa
     */
    public String getDsTipoAlcadaTarifa() {
        return dsTipoAlcadaTarifa;
    }

    /**
     * Nome: setDsTipoAlcadaTarifa
     *
     * @param dsTipoAlcadaTarifa
     */
    public void setDsTipoAlcadaTarifa(String dsTipoAlcadaTarifa) {
        this.dsTipoAlcadaTarifa = dsTipoAlcadaTarifa;
    }

    /**
     * Nome: getVrAlcadaTarifaAgencia
     *
     * @return vrAlcadaTarifaAgencia
     */
    public BigDecimal getVrAlcadaTarifaAgencia() {
        return vrAlcadaTarifaAgencia;
    }

    /**
     * Nome: setVrAlcadaTarifaAgencia
     *
     * @param vrAlcadaTarifaAgencia
     */
    public void setVrAlcadaTarifaAgencia(BigDecimal vrAlcadaTarifaAgencia) {
        this.vrAlcadaTarifaAgencia = vrAlcadaTarifaAgencia;
    }

    /**
     * Nome: getPcAlcadaTarifaAgencia
     *
     * @return pcAlcadaTarifaAgencia
     */
    public BigDecimal getPcAlcadaTarifaAgencia() {
        return pcAlcadaTarifaAgencia;
    }

    /**
     * Nome: setPcAlcadaTarifaAgencia
     *
     * @param pcAlcadaTarifaAgencia
     */
    public void setPcAlcadaTarifaAgencia(BigDecimal pcAlcadaTarifaAgencia) {
        this.pcAlcadaTarifaAgencia = pcAlcadaTarifaAgencia;
    }

    /**
     * Nome: getDsTipoCanalInclusao
     *
     * @return dsTipoCanalInclusao
     */
    public String getDsTipoCanalInclusao() {
        return dsTipoCanalInclusao;
    }

    /**
     * Nome: setDsTipoCanalInclusao
     *
     * @param dsTipoCanalInclusao
     */
    public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
        this.dsTipoCanalInclusao = dsTipoCanalInclusao;
    }

    /**
     * Nome: getDsTipoCanalManutencao
     *
     * @return dsTipoCanalManutencao
     */
    public String getDsTipoCanalManutencao() {
        return dsTipoCanalManutencao;
    }

    /**
     * Nome: setDsTipoCanalManutencao
     *
     * @param dsTipoCanalManutencao
     */
    public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
        this.dsTipoCanalManutencao = dsTipoCanalManutencao;
    }
	
    /**
     * Nome: getTipoCanalInclusaoFormatado
     * 
     * @return
     */
    public String getTipoCanalInclusaoFormatado(){
        if(not(isEmptyOrNull(getCdTipoCanalInclusao()))){
            return getCdTipoCanalInclusao() + " - " + getDsTipoCanalInclusao();
        }
        
        return "";
    }
    
    /**
     * Nome: getTipoCanalIManutencaoFormatado
     * 
     * @return
     */
    public String getTipoCanalManutencaoFormatado(){
        if(not(isEmptyOrNull(getCdTipoCanalManutencao()))){
            return getCdTipoCanalManutencao() + " - " + getDsTipoCanalManutencao();
        }
        
        return "";
    }
    
    /**
     * Nome: getPercentualAlcadaTarifaAgenciaFormatado
     * 
     * @return
     */
    public String getPercentualAlcadaTarifaAgenciaFormatado(){
        return PgitUtil.formatNumberPercent(getPcAlcadaTarifaAgencia());
    }
	
}