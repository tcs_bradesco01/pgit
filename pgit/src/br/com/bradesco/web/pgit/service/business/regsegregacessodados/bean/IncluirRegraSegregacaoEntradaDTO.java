/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: IncluirRegraSegregacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirRegraSegregacaoEntradaDTO {
	
	/** Atributo tipoUnidadeOrganizacionalUsuario. */
	private int tipoUnidadeOrganizacionalUsuario;
	
	/** Atributo tipoUnidadeOrganizacionalProprietarioDados. */
	private int tipoUnidadeOrganizacionalProprietarioDados;
	
	/** Atributo tipoAcao. */
	private int tipoAcao;
	
	/** Atributo nivelPermissaoDados. */
	private int nivelPermissaoDados;
	
	/**
	 * Get: nivelPermissaoDados.
	 *
	 * @return nivelPermissaoDados
	 */
	public int getNivelPermissaoDados() {
		return nivelPermissaoDados;
	}
	
	/**
	 * Set: nivelPermissaoDados.
	 *
	 * @param nivelPermissaoDados the nivel permissao dados
	 */
	public void setNivelPermissaoDados(int nivelPermissaoDados) {
		this.nivelPermissaoDados = nivelPermissaoDados;
	}
	
	/**
	 * Get: tipoAcao.
	 *
	 * @return tipoAcao
	 */
	public int getTipoAcao() {
		return tipoAcao;
	}
	
	/**
	 * Set: tipoAcao.
	 *
	 * @param tipoAcao the tipo acao
	 */
	public void setTipoAcao(int tipoAcao) {
		this.tipoAcao = tipoAcao;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @return tipoUnidadeOrganizacionalProprietarioDados
	 */
	public int getTipoUnidadeOrganizacionalProprietarioDados() {
		return tipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalProprietarioDados.
	 *
	 * @param tipoUnidadeOrganizacionalProprietarioDados the tipo unidade organizacional proprietario dados
	 */
	public void setTipoUnidadeOrganizacionalProprietarioDados(
			int tipoUnidadeOrganizacionalProprietarioDados) {
		this.tipoUnidadeOrganizacionalProprietarioDados = tipoUnidadeOrganizacionalProprietarioDados;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @return tipoUnidadeOrganizacionalUsuario
	 */
	public int getTipoUnidadeOrganizacionalUsuario() {
		return tipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @param tipoUnidadeOrganizacionalUsuario the tipo unidade organizacional usuario
	 */
	public void setTipoUnidadeOrganizacionalUsuario(
			int tipoUnidadeOrganizacionalUsuario) {
		this.tipoUnidadeOrganizacionalUsuario = tipoUnidadeOrganizacionalUsuario;
	}

		
	
	
	


}
