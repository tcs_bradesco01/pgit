package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

public class ConsultarContratoEntradaDTO {

	private Integer nrOcorrencias;
	private Long cdClubPessoa;
	private Long cdPessoaJuridica;
	private Integer cdTipoContrato;
	private Long nrSequenciaContrato;
	private Integer cdAgencia;
	private Long cdFuncionarioBradesco;
	private Integer cdSituacaoContrato;
	private Integer cdParametroPrograma;
	private Long cdCpfCnpjPssoa;
	private Integer cdFilialCnpjPssoa;
	private Integer cdControleCpfPssoa;
	private String dsAmbiente;

	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}

	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}

	public Long getCdClubPessoa() {
		return cdClubPessoa;
	}

	public void setCdClubPessoa(Long cdClubPessoa) {
		this.cdClubPessoa = cdClubPessoa;
	}

	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	public Integer getCdAgencia() {
		return cdAgencia;
	}

	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	public Long getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}

	public void setCdFuncionarioBradesco(Long cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}

	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	public Integer getCdParametroPrograma() {
		return cdParametroPrograma;
	}

	public void setCdParametroPrograma(Integer cdParametroPrograma) {
		this.cdParametroPrograma = cdParametroPrograma;
	}

	public Long getCdCpfCnpjPssoa() {
		return cdCpfCnpjPssoa;
	}

	public void setCdCpfCnpjPssoa(Long cdCpfCnpjPssoa) {
		this.cdCpfCnpjPssoa = cdCpfCnpjPssoa;
	}

	public Integer getCdFilialCnpjPssoa() {
		return cdFilialCnpjPssoa;
	}

	public void setCdFilialCnpjPssoa(Integer cdFilialCnpjPssoa) {
		this.cdFilialCnpjPssoa = cdFilialCnpjPssoa;
	}

	public Integer getCdControleCpfPssoa() {
		return cdControleCpfPssoa;
	}

	public void setCdControleCpfPssoa(Integer cdControleCpfPssoa) {
		this.cdControleCpfPssoa = cdControleCpfPssoa;
	}

	public String getDsAmbiente() {
		return dsAmbiente;
	}

	public void setDsAmbiente(String dsAmbiente) {
		this.dsAmbiente = dsAmbiente;
	}

}
