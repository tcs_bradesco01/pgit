/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdRegraAcessoDado
     */
    private int _cdRegraAcessoDado = 0;

    /**
     * keeps track of state for field: _cdRegraAcessoDado
     */
    private boolean _has_cdRegraAcessoDado;

    /**
     * Field _cdExcecaoAcessoDado
     */
    private int _cdExcecaoAcessoDado = 0;

    /**
     * keeps track of state for field: _cdExcecaoAcessoDado
     */
    private boolean _has_cdExcecaoAcessoDado;

    /**
     * Field _cdTipoUnidadeUsuario
     */
    private int _cdTipoUnidadeUsuario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeUsuario
     */
    private boolean _has_cdTipoUnidadeUsuario;

    /**
     * Field _dsTipoUnidadeUsuario
     */
    private java.lang.String _dsTipoUnidadeUsuario;

    /**
     * Field _cdPessoaJuridicaUsuario
     */
    private long _cdPessoaJuridicaUsuario = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaUsuario
     */
    private boolean _has_cdPessoaJuridicaUsuario;

    /**
     * Field _nrUnidadeOrganizacionalUsuario
     */
    private int _nrUnidadeOrganizacionalUsuario = 0;

    /**
     * keeps track of state for field:
     * _nrUnidadeOrganizacionalUsuario
     */
    private boolean _has_nrUnidadeOrganizacionalUsuario;

    /**
     * Field _dsUnidadeOrganizaciolUsuario
     */
    private java.lang.String _dsUnidadeOrganizaciolUsuario;

    /**
     * Field _cdTipoUnidadeProprietario
     */
    private int _cdTipoUnidadeProprietario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeProprietario
     */
    private boolean _has_cdTipoUnidadeProprietario;

    /**
     * Field _dsTipoUnidadeProprietario
     */
    private java.lang.String _dsTipoUnidadeProprietario;

    /**
     * Field _cdPessoaJuridicaProprietario
     */
    private long _cdPessoaJuridicaProprietario = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaProprietario
     */
    private boolean _has_cdPessoaJuridicaProprietario;

    /**
     * Field _nrUnidadeOrganizacionalProprietario
     */
    private int _nrUnidadeOrganizacionalProprietario = 0;

    /**
     * keeps track of state for field:
     * _nrUnidadeOrganizacionalProprietario
     */
    private boolean _has_nrUnidadeOrganizacionalProprietario;

    /**
     * Field _dsUnidadeOrganizacionalProprietario
     */
    private java.lang.String _dsUnidadeOrganizacionalProprietario;

    /**
     * Field _cdTipoExcecaoAcesso
     */
    private int _cdTipoExcecaoAcesso = 0;

    /**
     * keeps track of state for field: _cdTipoExcecaoAcesso
     */
    private boolean _has_cdTipoExcecaoAcesso;

    /**
     * Field _cdTipoAbrangenciaExcecao
     */
    private int _cdTipoAbrangenciaExcecao = 0;

    /**
     * keeps track of state for field: _cdTipoAbrangenciaExcecao
     */
    private boolean _has_cdTipoAbrangenciaExcecao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdExcecaoAcessoDado
     * 
     */
    public void deleteCdExcecaoAcessoDado()
    {
        this._has_cdExcecaoAcessoDado= false;
    } //-- void deleteCdExcecaoAcessoDado() 

    /**
     * Method deleteCdPessoaJuridicaProprietario
     * 
     */
    public void deleteCdPessoaJuridicaProprietario()
    {
        this._has_cdPessoaJuridicaProprietario= false;
    } //-- void deleteCdPessoaJuridicaProprietario() 

    /**
     * Method deleteCdPessoaJuridicaUsuario
     * 
     */
    public void deleteCdPessoaJuridicaUsuario()
    {
        this._has_cdPessoaJuridicaUsuario= false;
    } //-- void deleteCdPessoaJuridicaUsuario() 

    /**
     * Method deleteCdRegraAcessoDado
     * 
     */
    public void deleteCdRegraAcessoDado()
    {
        this._has_cdRegraAcessoDado= false;
    } //-- void deleteCdRegraAcessoDado() 

    /**
     * Method deleteCdTipoAbrangenciaExcecao
     * 
     */
    public void deleteCdTipoAbrangenciaExcecao()
    {
        this._has_cdTipoAbrangenciaExcecao= false;
    } //-- void deleteCdTipoAbrangenciaExcecao() 

    /**
     * Method deleteCdTipoExcecaoAcesso
     * 
     */
    public void deleteCdTipoExcecaoAcesso()
    {
        this._has_cdTipoExcecaoAcesso= false;
    } //-- void deleteCdTipoExcecaoAcesso() 

    /**
     * Method deleteCdTipoUnidadeProprietario
     * 
     */
    public void deleteCdTipoUnidadeProprietario()
    {
        this._has_cdTipoUnidadeProprietario= false;
    } //-- void deleteCdTipoUnidadeProprietario() 

    /**
     * Method deleteCdTipoUnidadeUsuario
     * 
     */
    public void deleteCdTipoUnidadeUsuario()
    {
        this._has_cdTipoUnidadeUsuario= false;
    } //-- void deleteCdTipoUnidadeUsuario() 

    /**
     * Method deleteNrUnidadeOrganizacionalProprietario
     * 
     */
    public void deleteNrUnidadeOrganizacionalProprietario()
    {
        this._has_nrUnidadeOrganizacionalProprietario= false;
    } //-- void deleteNrUnidadeOrganizacionalProprietario() 

    /**
     * Method deleteNrUnidadeOrganizacionalUsuario
     * 
     */
    public void deleteNrUnidadeOrganizacionalUsuario()
    {
        this._has_nrUnidadeOrganizacionalUsuario= false;
    } //-- void deleteNrUnidadeOrganizacionalUsuario() 

    /**
     * Returns the value of field 'cdExcecaoAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdExcecaoAcessoDado'.
     */
    public int getCdExcecaoAcessoDado()
    {
        return this._cdExcecaoAcessoDado;
    } //-- int getCdExcecaoAcessoDado() 

    /**
     * Returns the value of field 'cdPessoaJuridicaProprietario'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaProprietario'.
     */
    public long getCdPessoaJuridicaProprietario()
    {
        return this._cdPessoaJuridicaProprietario;
    } //-- long getCdPessoaJuridicaProprietario() 

    /**
     * Returns the value of field 'cdPessoaJuridicaUsuario'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaUsuario'.
     */
    public long getCdPessoaJuridicaUsuario()
    {
        return this._cdPessoaJuridicaUsuario;
    } //-- long getCdPessoaJuridicaUsuario() 

    /**
     * Returns the value of field 'cdRegraAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdRegraAcessoDado'.
     */
    public int getCdRegraAcessoDado()
    {
        return this._cdRegraAcessoDado;
    } //-- int getCdRegraAcessoDado() 

    /**
     * Returns the value of field 'cdTipoAbrangenciaExcecao'.
     * 
     * @return int
     * @return the value of field 'cdTipoAbrangenciaExcecao'.
     */
    public int getCdTipoAbrangenciaExcecao()
    {
        return this._cdTipoAbrangenciaExcecao;
    } //-- int getCdTipoAbrangenciaExcecao() 

    /**
     * Returns the value of field 'cdTipoExcecaoAcesso'.
     * 
     * @return int
     * @return the value of field 'cdTipoExcecaoAcesso'.
     */
    public int getCdTipoExcecaoAcesso()
    {
        return this._cdTipoExcecaoAcesso;
    } //-- int getCdTipoExcecaoAcesso() 

    /**
     * Returns the value of field 'cdTipoUnidadeProprietario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeProprietario'.
     */
    public int getCdTipoUnidadeProprietario()
    {
        return this._cdTipoUnidadeProprietario;
    } //-- int getCdTipoUnidadeProprietario() 

    /**
     * Returns the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeUsuario'.
     */
    public int getCdTipoUnidadeUsuario()
    {
        return this._cdTipoUnidadeUsuario;
    } //-- int getCdTipoUnidadeUsuario() 

    /**
     * Returns the value of field 'dsTipoUnidadeProprietario'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeProprietario'.
     */
    public java.lang.String getDsTipoUnidadeProprietario()
    {
        return this._dsTipoUnidadeProprietario;
    } //-- java.lang.String getDsTipoUnidadeProprietario() 

    /**
     * Returns the value of field 'dsTipoUnidadeUsuario'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeUsuario'.
     */
    public java.lang.String getDsTipoUnidadeUsuario()
    {
        return this._dsTipoUnidadeUsuario;
    } //-- java.lang.String getDsTipoUnidadeUsuario() 

    /**
     * Returns the value of field 'dsUnidadeOrganizaciolUsuario'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizaciolUsuario'.
     */
    public java.lang.String getDsUnidadeOrganizaciolUsuario()
    {
        return this._dsUnidadeOrganizaciolUsuario;
    } //-- java.lang.String getDsUnidadeOrganizaciolUsuario() 

    /**
     * Returns the value of field
     * 'dsUnidadeOrganizacionalProprietario'.
     * 
     * @return String
     * @return the value of field
     * 'dsUnidadeOrganizacionalProprietario'.
     */
    public java.lang.String getDsUnidadeOrganizacionalProprietario()
    {
        return this._dsUnidadeOrganizacionalProprietario;
    } //-- java.lang.String getDsUnidadeOrganizacionalProprietario() 

    /**
     * Returns the value of field
     * 'nrUnidadeOrganizacionalProprietario'.
     * 
     * @return int
     * @return the value of field
     * 'nrUnidadeOrganizacionalProprietario'.
     */
    public int getNrUnidadeOrganizacionalProprietario()
    {
        return this._nrUnidadeOrganizacionalProprietario;
    } //-- int getNrUnidadeOrganizacionalProprietario() 

    /**
     * Returns the value of field 'nrUnidadeOrganizacionalUsuario'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrganizacionalUsuario'.
     */
    public int getNrUnidadeOrganizacionalUsuario()
    {
        return this._nrUnidadeOrganizacionalUsuario;
    } //-- int getNrUnidadeOrganizacionalUsuario() 

    /**
     * Method hasCdExcecaoAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdExcecaoAcessoDado()
    {
        return this._has_cdExcecaoAcessoDado;
    } //-- boolean hasCdExcecaoAcessoDado() 

    /**
     * Method hasCdPessoaJuridicaProprietario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaProprietario()
    {
        return this._has_cdPessoaJuridicaProprietario;
    } //-- boolean hasCdPessoaJuridicaProprietario() 

    /**
     * Method hasCdPessoaJuridicaUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaUsuario()
    {
        return this._has_cdPessoaJuridicaUsuario;
    } //-- boolean hasCdPessoaJuridicaUsuario() 

    /**
     * Method hasCdRegraAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAcessoDado()
    {
        return this._has_cdRegraAcessoDado;
    } //-- boolean hasCdRegraAcessoDado() 

    /**
     * Method hasCdTipoAbrangenciaExcecao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAbrangenciaExcecao()
    {
        return this._has_cdTipoAbrangenciaExcecao;
    } //-- boolean hasCdTipoAbrangenciaExcecao() 

    /**
     * Method hasCdTipoExcecaoAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoExcecaoAcesso()
    {
        return this._has_cdTipoExcecaoAcesso;
    } //-- boolean hasCdTipoExcecaoAcesso() 

    /**
     * Method hasCdTipoUnidadeProprietario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeProprietario()
    {
        return this._has_cdTipoUnidadeProprietario;
    } //-- boolean hasCdTipoUnidadeProprietario() 

    /**
     * Method hasCdTipoUnidadeUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeUsuario()
    {
        return this._has_cdTipoUnidadeUsuario;
    } //-- boolean hasCdTipoUnidadeUsuario() 

    /**
     * Method hasNrUnidadeOrganizacionalProprietario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacionalProprietario()
    {
        return this._has_nrUnidadeOrganizacionalProprietario;
    } //-- boolean hasNrUnidadeOrganizacionalProprietario() 

    /**
     * Method hasNrUnidadeOrganizacionalUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacionalUsuario()
    {
        return this._has_nrUnidadeOrganizacionalUsuario;
    } //-- boolean hasNrUnidadeOrganizacionalUsuario() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdExcecaoAcessoDado'.
     * 
     * @param cdExcecaoAcessoDado the value of field
     * 'cdExcecaoAcessoDado'.
     */
    public void setCdExcecaoAcessoDado(int cdExcecaoAcessoDado)
    {
        this._cdExcecaoAcessoDado = cdExcecaoAcessoDado;
        this._has_cdExcecaoAcessoDado = true;
    } //-- void setCdExcecaoAcessoDado(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaProprietario'.
     * 
     * @param cdPessoaJuridicaProprietario the value of field
     * 'cdPessoaJuridicaProprietario'.
     */
    public void setCdPessoaJuridicaProprietario(long cdPessoaJuridicaProprietario)
    {
        this._cdPessoaJuridicaProprietario = cdPessoaJuridicaProprietario;
        this._has_cdPessoaJuridicaProprietario = true;
    } //-- void setCdPessoaJuridicaProprietario(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaUsuario'.
     * 
     * @param cdPessoaJuridicaUsuario the value of field
     * 'cdPessoaJuridicaUsuario'.
     */
    public void setCdPessoaJuridicaUsuario(long cdPessoaJuridicaUsuario)
    {
        this._cdPessoaJuridicaUsuario = cdPessoaJuridicaUsuario;
        this._has_cdPessoaJuridicaUsuario = true;
    } //-- void setCdPessoaJuridicaUsuario(long) 

    /**
     * Sets the value of field 'cdRegraAcessoDado'.
     * 
     * @param cdRegraAcessoDado the value of field
     * 'cdRegraAcessoDado'.
     */
    public void setCdRegraAcessoDado(int cdRegraAcessoDado)
    {
        this._cdRegraAcessoDado = cdRegraAcessoDado;
        this._has_cdRegraAcessoDado = true;
    } //-- void setCdRegraAcessoDado(int) 

    /**
     * Sets the value of field 'cdTipoAbrangenciaExcecao'.
     * 
     * @param cdTipoAbrangenciaExcecao the value of field
     * 'cdTipoAbrangenciaExcecao'.
     */
    public void setCdTipoAbrangenciaExcecao(int cdTipoAbrangenciaExcecao)
    {
        this._cdTipoAbrangenciaExcecao = cdTipoAbrangenciaExcecao;
        this._has_cdTipoAbrangenciaExcecao = true;
    } //-- void setCdTipoAbrangenciaExcecao(int) 

    /**
     * Sets the value of field 'cdTipoExcecaoAcesso'.
     * 
     * @param cdTipoExcecaoAcesso the value of field
     * 'cdTipoExcecaoAcesso'.
     */
    public void setCdTipoExcecaoAcesso(int cdTipoExcecaoAcesso)
    {
        this._cdTipoExcecaoAcesso = cdTipoExcecaoAcesso;
        this._has_cdTipoExcecaoAcesso = true;
    } //-- void setCdTipoExcecaoAcesso(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeProprietario'.
     * 
     * @param cdTipoUnidadeProprietario the value of field
     * 'cdTipoUnidadeProprietario'.
     */
    public void setCdTipoUnidadeProprietario(int cdTipoUnidadeProprietario)
    {
        this._cdTipoUnidadeProprietario = cdTipoUnidadeProprietario;
        this._has_cdTipoUnidadeProprietario = true;
    } //-- void setCdTipoUnidadeProprietario(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @param cdTipoUnidadeUsuario the value of field
     * 'cdTipoUnidadeUsuario'.
     */
    public void setCdTipoUnidadeUsuario(int cdTipoUnidadeUsuario)
    {
        this._cdTipoUnidadeUsuario = cdTipoUnidadeUsuario;
        this._has_cdTipoUnidadeUsuario = true;
    } //-- void setCdTipoUnidadeUsuario(int) 

    /**
     * Sets the value of field 'dsTipoUnidadeProprietario'.
     * 
     * @param dsTipoUnidadeProprietario the value of field
     * 'dsTipoUnidadeProprietario'.
     */
    public void setDsTipoUnidadeProprietario(java.lang.String dsTipoUnidadeProprietario)
    {
        this._dsTipoUnidadeProprietario = dsTipoUnidadeProprietario;
    } //-- void setDsTipoUnidadeProprietario(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeUsuario'.
     * 
     * @param dsTipoUnidadeUsuario the value of field
     * 'dsTipoUnidadeUsuario'.
     */
    public void setDsTipoUnidadeUsuario(java.lang.String dsTipoUnidadeUsuario)
    {
        this._dsTipoUnidadeUsuario = dsTipoUnidadeUsuario;
    } //-- void setDsTipoUnidadeUsuario(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizaciolUsuario'.
     * 
     * @param dsUnidadeOrganizaciolUsuario the value of field
     * 'dsUnidadeOrganizaciolUsuario'.
     */
    public void setDsUnidadeOrganizaciolUsuario(java.lang.String dsUnidadeOrganizaciolUsuario)
    {
        this._dsUnidadeOrganizaciolUsuario = dsUnidadeOrganizaciolUsuario;
    } //-- void setDsUnidadeOrganizaciolUsuario(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsUnidadeOrganizacionalProprietario'.
     * 
     * @param dsUnidadeOrganizacionalProprietario the value of
     * field 'dsUnidadeOrganizacionalProprietario'.
     */
    public void setDsUnidadeOrganizacionalProprietario(java.lang.String dsUnidadeOrganizacionalProprietario)
    {
        this._dsUnidadeOrganizacionalProprietario = dsUnidadeOrganizacionalProprietario;
    } //-- void setDsUnidadeOrganizacionalProprietario(java.lang.String) 

    /**
     * Sets the value of field
     * 'nrUnidadeOrganizacionalProprietario'.
     * 
     * @param nrUnidadeOrganizacionalProprietario the value of
     * field 'nrUnidadeOrganizacionalProprietario'.
     */
    public void setNrUnidadeOrganizacionalProprietario(int nrUnidadeOrganizacionalProprietario)
    {
        this._nrUnidadeOrganizacionalProprietario = nrUnidadeOrganizacionalProprietario;
        this._has_nrUnidadeOrganizacionalProprietario = true;
    } //-- void setNrUnidadeOrganizacionalProprietario(int) 

    /**
     * Sets the value of field 'nrUnidadeOrganizacionalUsuario'.
     * 
     * @param nrUnidadeOrganizacionalUsuario the value of field
     * 'nrUnidadeOrganizacionalUsuario'.
     */
    public void setNrUnidadeOrganizacionalUsuario(int nrUnidadeOrganizacionalUsuario)
    {
        this._nrUnidadeOrganizacionalUsuario = nrUnidadeOrganizacionalUsuario;
        this._has_nrUnidadeOrganizacionalUsuario = true;
    } //-- void setNrUnidadeOrganizacionalUsuario(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarexcecaoregrasegregacaoacesso.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
