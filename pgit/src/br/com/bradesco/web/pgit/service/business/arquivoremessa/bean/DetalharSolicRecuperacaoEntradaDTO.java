/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoremessa.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoremessa.bean;

/**
 * Nome: DetalharSolicRecuperacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolicRecuperacaoEntradaDTO {
	
	/** Atributo nrSolicitacaoRecuperacao. */
	private Integer nrSolicitacaoRecuperacao;

	/**
	 * Get: nrSolicitacaoRecuperacao.
	 *
	 * @return nrSolicitacaoRecuperacao
	 */
	public Integer getNrSolicitacaoRecuperacao() {
		return nrSolicitacaoRecuperacao;
	}

	/**
	 * Set: nrSolicitacaoRecuperacao.
	 *
	 * @param nrSolicitacaoRecuperacao the nr solicitacao recuperacao
	 */
	public void setNrSolicitacaoRecuperacao(Integer nrSolicitacaoRecuperacao) {
		this.nrSolicitacaoRecuperacao = nrSolicitacaoRecuperacao;
	}
	
	

}
