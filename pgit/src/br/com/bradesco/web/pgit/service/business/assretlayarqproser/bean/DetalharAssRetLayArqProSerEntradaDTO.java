/*
 * Nome: br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean;

/**
 * Nome: DetalharAssRetLayArqProSerEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharAssRetLayArqProSerEntradaDTO {
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo tipoArquivoRetorno. */
	private int tipoArquivoRetorno;
	
	/** Atributo tipoLayoutArquivo. */
	private int tipoLayoutArquivo;
	
	/**
	 * Get: tipoArquivoRetorno.
	 *
	 * @return tipoArquivoRetorno
	 */
	public int getTipoArquivoRetorno() {
		return tipoArquivoRetorno;
	}
	
	/**
	 * Set: tipoArquivoRetorno.
	 *
	 * @param tipoArquivoRetorno the tipo arquivo retorno
	 */
	public void setTipoArquivoRetorno(int tipoArquivoRetorno) {
		this.tipoArquivoRetorno = tipoArquivoRetorno;
	}
	
	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public int getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}
	
	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(int tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	

}
