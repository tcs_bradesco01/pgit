/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean;

import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.ISolrelempresasacompServiceConstants;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioempresaacompanhada.response.Ocorrencias;


/**
 * Arquivo criado em 18/07/14.
 */
public class SolicitacaoRelatorioEmpresasAcompOcorrencia {

	/** Atributo PENDENTE. */
	private static int PENDENTE = 1;

    /** Atributo cdSolicitacaoPagamentoIntegrado. */
    private Integer cdSolicitacaoPagamentoIntegrado;

    /** Atributo nrSolicitacaoPagamentoIntegrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;

    /** Atributo cdSituacaoSolicitacaoPagamento. */
    private Integer cdSituacaoSolicitacaoPagamento;

    /** Atributo hrSolicitacaoPagamento. */
    private String hrSolicitacaoPagamento;

    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;

    /**
     * Solicitacao relatorio empresas acomp ocorrencia.
     */
    public SolicitacaoRelatorioEmpresasAcompOcorrencia() {
		super();
	}

    /**
     * Solicitacao relatorio empresas acomp ocorrencia.
     *
     * @param solicitacao the solicitacao
     */
    public SolicitacaoRelatorioEmpresasAcompOcorrencia(Ocorrencias solicitacao) {
    	setCdSolicitacaoPagamentoIntegrado(solicitacao.getCdSolicitacaoPagamentoIntegrado());
    	setNrSolicitacaoPagamentoIntegrado(solicitacao.getNrSolicitacaoPagamentoIntegrado());
    	setCdSituacaoSolicitacaoPagamento(solicitacao.getCdSituacaoSolicitacaoPagamento());
    	setHrSolicitacaoPagamento(solicitacao.getHrSolicitacaoPagamento());
    	setCdUsuarioInclusao(solicitacao.getCdUsuarioInclusao());
	}

	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(Integer cdSolicitacaoPagamentoIntegrado) {
        this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get: cdSolicitacaoPagamentoIntegrado.
     *
     * @return cdSolicitacaoPagamentoIntegrado
     */
    public Integer getCdSolicitacaoPagamentoIntegrado() {
        return this.cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set: nrSolicitacaoPagamentoIntegrado.
     *
     * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
     */
    public void setNrSolicitacaoPagamentoIntegrado(Integer nrSolicitacaoPagamentoIntegrado) {
        this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get: nrSolicitacaoPagamentoIntegrado.
     *
     * @return nrSolicitacaoPagamentoIntegrado
     */
    public Integer getNrSolicitacaoPagamentoIntegrado() {
        return this.nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set: cdSituacaoSolicitacaoPagamento.
     *
     * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
     */
    public void setCdSituacaoSolicitacaoPagamento(Integer cdSituacaoSolicitacaoPagamento) {
        this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
    }

    /**
     * Get: cdSituacaoSolicitacaoPagamento.
     *
     * @return cdSituacaoSolicitacaoPagamento
     */
    public Integer getCdSituacaoSolicitacaoPagamento() {
        return this.cdSituacaoSolicitacaoPagamento;
    }

    /**
     * Set: hrSolicitacaoPagamento.
     *
     * @param hrSolicitacaoPagamento the hr solicitacao pagamento
     */
    public void setHrSolicitacaoPagamento(String hrSolicitacaoPagamento) {
        this.hrSolicitacaoPagamento = hrSolicitacaoPagamento;
    }

    /**
     * Get: hrSolicitacaoPagamento.
     *
     * @return hrSolicitacaoPagamento
     */
    public String getHrSolicitacaoPagamento() {
        return this.hrSolicitacaoPagamento;
    }

    /**
     * Set: cdUsuarioInclusao.
     *
     * @param cdUsuarioInclusao the cd usuario inclusao
     */
    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    /**
     * Get: cdUsuarioInclusao.
     *
     * @return cdUsuarioInclusao
     */
    public String getCdUsuarioInclusao() {
        return this.cdUsuarioInclusao;
    }

    /**
     * Get: situacao.
     *
     * @return situacao
     */
    public String getSituacao() {
    	if (cdSituacaoSolicitacaoPagamento == null) {
			return "";
		}

    	if (cdSituacaoSolicitacaoPagamento == PENDENTE) {
			return ISolrelempresasacompServiceConstants.PENDENTE;
		}

    	return ISolrelempresasacompServiceConstants.ATENDIDO;
    }
}