/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class EfetuarValidacaoTarifaOperTipoServicoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class EfetuarValidacaoTarifaOperTipoServicoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaProposta
     */
    private long _cdPessoaJuridicaProposta = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaProposta
     */
    private boolean _has_cdPessoaJuridicaProposta;

    /**
     * Field _cdTipoContratoProposta
     */
    private int _cdTipoContratoProposta = 0;

    /**
     * keeps track of state for field: _cdTipoContratoProposta
     */
    private boolean _has_cdTipoContratoProposta;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdprodutoServicoOperacao
     */
    private int _cdprodutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdprodutoServicoOperacao
     */
    private boolean _has_cdprodutoServicoOperacao;

    /**
     * Field _cdProdutoServRelacionado
     */
    private int _cdProdutoServRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServRelacionado
     */
    private boolean _has_cdProdutoServRelacionado;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _vlrTarifaContratoMinimo
     */
    private java.math.BigDecimal _vlrTarifaContratoMinimo = new java.math.BigDecimal("0");

    /**
     * Field _vlrTarifaContratoMaximo
     */
    private java.math.BigDecimal _vlrTarifaContratoMaximo = new java.math.BigDecimal("0");

    /**
     * Field _vlrTarifaContratoNegocio
     */
    private java.math.BigDecimal _vlrTarifaContratoNegocio = new java.math.BigDecimal("0");

    /**
     * Field _cdNegocioRenegocio
     */
    private int _cdNegocioRenegocio = 0;

    /**
     * keeps track of state for field: _cdNegocioRenegocio
     */
    private boolean _has_cdNegocioRenegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public EfetuarValidacaoTarifaOperTipoServicoRequest() 
     {
        super();
        setVlrTarifaContratoMinimo(new java.math.BigDecimal("0"));
        setVlrTarifaContratoMaximo(new java.math.BigDecimal("0"));
        setVlrTarifaContratoNegocio(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.request.EfetuarValidacaoTarifaOperTipoServicoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdNegocioRenegocio
     * 
     */
    public void deleteCdNegocioRenegocio()
    {
        this._has_cdNegocioRenegocio= false;
    } //-- void deleteCdNegocioRenegocio() 

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdPessoaJuridicaProposta
     * 
     */
    public void deleteCdPessoaJuridicaProposta()
    {
        this._has_cdPessoaJuridicaProposta= false;
    } //-- void deleteCdPessoaJuridicaProposta() 

    /**
     * Method deleteCdProdutoServRelacionado
     * 
     */
    public void deleteCdProdutoServRelacionado()
    {
        this._has_cdProdutoServRelacionado= false;
    } //-- void deleteCdProdutoServRelacionado() 

    /**
     * Method deleteCdTipoContratoProposta
     * 
     */
    public void deleteCdTipoContratoProposta()
    {
        this._has_cdTipoContratoProposta= false;
    } //-- void deleteCdTipoContratoProposta() 

    /**
     * Method deleteCdprodutoServicoOperacao
     * 
     */
    public void deleteCdprodutoServicoOperacao()
    {
        this._has_cdprodutoServicoOperacao= false;
    } //-- void deleteCdprodutoServicoOperacao() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdNegocioRenegocio'.
     * 
     * @return int
     * @return the value of field 'cdNegocioRenegocio'.
     */
    public int getCdNegocioRenegocio()
    {
        return this._cdNegocioRenegocio;
    } //-- int getCdNegocioRenegocio() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaProposta'.
     */
    public long getCdPessoaJuridicaProposta()
    {
        return this._cdPessoaJuridicaProposta;
    } //-- long getCdPessoaJuridicaProposta() 

    /**
     * Returns the value of field 'cdProdutoServRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServRelacionado'.
     */
    public int getCdProdutoServRelacionado()
    {
        return this._cdProdutoServRelacionado;
    } //-- int getCdProdutoServRelacionado() 

    /**
     * Returns the value of field 'cdTipoContratoProposta'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoProposta'.
     */
    public int getCdTipoContratoProposta()
    {
        return this._cdTipoContratoProposta;
    } //-- int getCdTipoContratoProposta() 

    /**
     * Returns the value of field 'cdprodutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdprodutoServicoOperacao'.
     */
    public int getCdprodutoServicoOperacao()
    {
        return this._cdprodutoServicoOperacao;
    } //-- int getCdprodutoServicoOperacao() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'vlrTarifaContratoMaximo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaContratoMaximo'.
     */
    public java.math.BigDecimal getVlrTarifaContratoMaximo()
    {
        return this._vlrTarifaContratoMaximo;
    } //-- java.math.BigDecimal getVlrTarifaContratoMaximo() 

    /**
     * Returns the value of field 'vlrTarifaContratoMinimo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaContratoMinimo'.
     */
    public java.math.BigDecimal getVlrTarifaContratoMinimo()
    {
        return this._vlrTarifaContratoMinimo;
    } //-- java.math.BigDecimal getVlrTarifaContratoMinimo() 

    /**
     * Returns the value of field 'vlrTarifaContratoNegocio'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrTarifaContratoNegocio'.
     */
    public java.math.BigDecimal getVlrTarifaContratoNegocio()
    {
        return this._vlrTarifaContratoNegocio;
    } //-- java.math.BigDecimal getVlrTarifaContratoNegocio() 

    /**
     * Method hasCdNegocioRenegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNegocioRenegocio()
    {
        return this._has_cdNegocioRenegocio;
    } //-- boolean hasCdNegocioRenegocio() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdPessoaJuridicaProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaProposta()
    {
        return this._has_cdPessoaJuridicaProposta;
    } //-- boolean hasCdPessoaJuridicaProposta() 

    /**
     * Method hasCdProdutoServRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServRelacionado()
    {
        return this._has_cdProdutoServRelacionado;
    } //-- boolean hasCdProdutoServRelacionado() 

    /**
     * Method hasCdTipoContratoProposta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoProposta()
    {
        return this._has_cdTipoContratoProposta;
    } //-- boolean hasCdTipoContratoProposta() 

    /**
     * Method hasCdprodutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdprodutoServicoOperacao()
    {
        return this._has_cdprodutoServicoOperacao;
    } //-- boolean hasCdprodutoServicoOperacao() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdNegocioRenegocio'.
     * 
     * @param cdNegocioRenegocio the value of field
     * 'cdNegocioRenegocio'.
     */
    public void setCdNegocioRenegocio(int cdNegocioRenegocio)
    {
        this._cdNegocioRenegocio = cdNegocioRenegocio;
        this._has_cdNegocioRenegocio = true;
    } //-- void setCdNegocioRenegocio(int) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaProposta'.
     * 
     * @param cdPessoaJuridicaProposta the value of field
     * 'cdPessoaJuridicaProposta'.
     */
    public void setCdPessoaJuridicaProposta(long cdPessoaJuridicaProposta)
    {
        this._cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
        this._has_cdPessoaJuridicaProposta = true;
    } //-- void setCdPessoaJuridicaProposta(long) 

    /**
     * Sets the value of field 'cdProdutoServRelacionado'.
     * 
     * @param cdProdutoServRelacionado the value of field
     * 'cdProdutoServRelacionado'.
     */
    public void setCdProdutoServRelacionado(int cdProdutoServRelacionado)
    {
        this._cdProdutoServRelacionado = cdProdutoServRelacionado;
        this._has_cdProdutoServRelacionado = true;
    } //-- void setCdProdutoServRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoContratoProposta'.
     * 
     * @param cdTipoContratoProposta the value of field
     * 'cdTipoContratoProposta'.
     */
    public void setCdTipoContratoProposta(int cdTipoContratoProposta)
    {
        this._cdTipoContratoProposta = cdTipoContratoProposta;
        this._has_cdTipoContratoProposta = true;
    } //-- void setCdTipoContratoProposta(int) 

    /**
     * Sets the value of field 'cdprodutoServicoOperacao'.
     * 
     * @param cdprodutoServicoOperacao the value of field
     * 'cdprodutoServicoOperacao'.
     */
    public void setCdprodutoServicoOperacao(int cdprodutoServicoOperacao)
    {
        this._cdprodutoServicoOperacao = cdprodutoServicoOperacao;
        this._has_cdprodutoServicoOperacao = true;
    } //-- void setCdprodutoServicoOperacao(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'vlrTarifaContratoMaximo'.
     * 
     * @param vlrTarifaContratoMaximo the value of field
     * 'vlrTarifaContratoMaximo'.
     */
    public void setVlrTarifaContratoMaximo(java.math.BigDecimal vlrTarifaContratoMaximo)
    {
        this._vlrTarifaContratoMaximo = vlrTarifaContratoMaximo;
    } //-- void setVlrTarifaContratoMaximo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTarifaContratoMinimo'.
     * 
     * @param vlrTarifaContratoMinimo the value of field
     * 'vlrTarifaContratoMinimo'.
     */
    public void setVlrTarifaContratoMinimo(java.math.BigDecimal vlrTarifaContratoMinimo)
    {
        this._vlrTarifaContratoMinimo = vlrTarifaContratoMinimo;
    } //-- void setVlrTarifaContratoMinimo(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrTarifaContratoNegocio'.
     * 
     * @param vlrTarifaContratoNegocio the value of field
     * 'vlrTarifaContratoNegocio'.
     */
    public void setVlrTarifaContratoNegocio(java.math.BigDecimal vlrTarifaContratoNegocio)
    {
        this._vlrTarifaContratoNegocio = vlrTarifaContratoNegocio;
    } //-- void setVlrTarifaContratoNegocio(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return EfetuarValidacaoTarifaOperTipoServicoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.request.EfetuarValidacaoTarifaOperTipoServicoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.request.EfetuarValidacaoTarifaOperTipoServicoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.request.EfetuarValidacaoTarifaOperTipoServicoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaotarifaopertiposervico.request.EfetuarValidacaoTarifaOperTipoServicoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
