/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean;

/**
 * Nome: ListarContratoMarqSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContratoMarqSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroLinhas. */
    private Integer numeroLinhas;
    
    /** Atributo cdPessoJuridicaContrato. */
    private Long cdPessoJuridicaContrato;
    
    /** Atributo dsPessoaJuridicaContrato. */
    private String dsPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo dsTipoContratoNegocio. */
    private String dsTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdSituacaoContratoNegocio. */
    private Integer cdSituacaoContratoNegocio;
    
    /** Atributo dsSituacaoContratoNegocio. */
    private String dsSituacaoContratoNegocio;
    
	/**
	 * Get: cdPessoJuridicaContrato.
	 *
	 * @return cdPessoJuridicaContrato
	 */
	public Long getCdPessoJuridicaContrato() {
		return cdPessoJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoJuridicaContrato.
	 *
	 * @param cdPessoJuridicaContrato the cd pesso juridica contrato
	 */
	public void setCdPessoJuridicaContrato(Long cdPessoJuridicaContrato) {
		this.cdPessoJuridicaContrato = cdPessoJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoContratoNegocio.
	 *
	 * @return cdSituacaoContratoNegocio
	 */
	public Integer getCdSituacaoContratoNegocio() {
		return cdSituacaoContratoNegocio;
	}
	
	/**
	 * Set: cdSituacaoContratoNegocio.
	 *
	 * @param cdSituacaoContratoNegocio the cd situacao contrato negocio
	 */
	public void setCdSituacaoContratoNegocio(Integer cdSituacaoContratoNegocio) {
		this.cdSituacaoContratoNegocio = cdSituacaoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}
	
	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}
	
	/**
	 * Get: dsSituacaoContratoNegocio.
	 *
	 * @return dsSituacaoContratoNegocio
	 */
	public String getDsSituacaoContratoNegocio() {
		return dsSituacaoContratoNegocio;
	}
	
	/**
	 * Set: dsSituacaoContratoNegocio.
	 *
	 * @param dsSituacaoContratoNegocio the ds situacao contrato negocio
	 */
	public void setDsSituacaoContratoNegocio(String dsSituacaoContratoNegocio) {
		this.dsSituacaoContratoNegocio = dsSituacaoContratoNegocio;
	}
	
	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}
	
	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	
	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
    
    
}
