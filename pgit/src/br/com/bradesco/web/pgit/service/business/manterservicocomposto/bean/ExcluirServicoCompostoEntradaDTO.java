/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean;

/**
 * Nome: ExcluirServicoCompostoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirServicoCompostoEntradaDTO {
	
    /** Atributo cdServicoCompostoPagamento. */
    private Long cdServicoCompostoPagamento;
    
    /** Atributo dsServicoCompostoPagamento. */
    private String dsServicoCompostoPagamento;
    
    /** Atributo cdTipoServicoCnab. */
    private Integer cdTipoServicoCnab;
    
    /** Atributo cdFormaLancamentoCnab. */
    private Integer cdFormaLancamentoCnab;
    
    /** Atributo cdFormaLiquidacao. */
    private Integer cdFormaLiquidacao;
    
    /** Atributo qtOcorrencias. */
    private Integer qtOcorrencias;
    
    /** Atributo qtDiaLctoFuturo. */
    private Integer qtDiaLctoFuturo;
    
    /** Atributo cdIndLctoFuturo. */
    private Integer cdIndLctoFuturo;
    
	/**
	 * Excluir servico composto entrada dto.
	 */
	public ExcluirServicoCompostoEntradaDTO() {
		super();
	}
	
	/**
	 * Get: cdFormaLancamentoCnab.
	 *
	 * @return cdFormaLancamentoCnab
	 */
	public Integer getCdFormaLancamentoCnab() {
		return cdFormaLancamentoCnab;
	}
	
	/**
	 * Set: cdFormaLancamentoCnab.
	 *
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 */
	public void setCdFormaLancamentoCnab(Integer cdFormaLancamentoCnab) {
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
	}
	
	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}
	
	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}
	
	/**
	 * Get: cdServicoCompostoPagamento.
	 *
	 * @return cdServicoCompostoPagamento
	 */
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}
	
	/**
	 * Set: cdServicoCompostoPagamento.
	 *
	 * @param cdServicoCompostoPagamento the cd servico composto pagamento
	 */
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}
	
	/**
	 * Get: cdTipoServicoCnab.
	 *
	 * @return cdTipoServicoCnab
	 */
	public Integer getCdTipoServicoCnab() {
		return cdTipoServicoCnab;
	}
	
	/**
	 * Set: cdTipoServicoCnab.
	 *
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 */
	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}
	
	/**
	 * Get: dsServicoCompostoPagamento.
	 *
	 * @return dsServicoCompostoPagamento
	 */
	public String getDsServicoCompostoPagamento() {
		return dsServicoCompostoPagamento;
	}
	
	/**
	 * Set: dsServicoCompostoPagamento.
	 *
	 * @param dsServicoCompostoPagamento the ds servico composto pagamento
	 */
	public void setDsServicoCompostoPagamento(String dsServicoCompostoPagamento) {
		this.dsServicoCompostoPagamento = dsServicoCompostoPagamento;
	}
	
	/**
	 * Get: qtOcorrencias.
	 *
	 * @return qtOcorrencias
	 */
	public Integer getQtOcorrencias() {
		return qtOcorrencias;
	}
	
	/**
	 * Set: qtOcorrencias.
	 *
	 * @param qtOcorrencias the qt ocorrencias
	 */
	public void setQtOcorrencias(Integer qtOcorrencias) {
		this.qtOcorrencias = qtOcorrencias;
	}
	
	/**
	 * Get: qtDiaLctoFuturo.
	 *
	 * @return qtDiaLctoFuturo
	 */
	public Integer getQtDiaLctoFuturo() {
		return qtDiaLctoFuturo;
	}
	
	/**
	 * Set: qtDiaLctoFuturo.
	 *
	 * @param qtDiaLctoFuturo the qt dia lcto futuro
	 */
	public void setQtDiaLctoFuturo(Integer qtDiaLctoFuturo) {
		this.qtDiaLctoFuturo = qtDiaLctoFuturo;
	}
	
	/**
	 * Get: cdIndLctoFuturo.
	 *
	 * @return cdIndLctoFuturo
	 */
	public Integer getCdIndLctoFuturo() {
		return cdIndLctoFuturo;
	}
	
	/**
	 * Set: cdIndLctoFuturo.
	 *
	 * @param cdIndLctoFuturo the cd ind lcto futuro
	 */
	public void setCdIndLctoFuturo(Integer cdIndLctoFuturo) {
		this.cdIndLctoFuturo = cdIndLctoFuturo;
	}
}
