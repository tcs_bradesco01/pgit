/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistAssocTrocaArqParticipanteResponse.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistAssocTrocaArqParticipanteResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsCodigoTipoLayout
     */
    private java.lang.String _dsCodigoTipoLayout;

    /**
     * Field _cdPerfilTrocaArquivo
     */
    private long _cdPerfilTrocaArquivo = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArquivo
     */
    private boolean _has_cdPerfilTrocaArquivo;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdCnpjContrato
     */
    private int _cdCnpjContrato = 0;

    /**
     * keeps track of state for field: _cdCnpjContrato
     */
    private boolean _has_cdCnpjContrato;

    /**
     * Field _cdCpfCnpjDigito
     */
    private int _cdCpfCnpjDigito = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjDigito
     */
    private boolean _has_cdCpfCnpjDigito;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdTipoParticipante
     */
    private java.lang.String _cdTipoParticipante;

    /**
     * Field _dsTipoParticipante
     */
    private java.lang.String _dsTipoParticipante;

    /**
     * Field _cdSituacaoParticipante
     */
    private long _cdSituacaoParticipante = 0;

    /**
     * keeps track of state for field: _cdSituacaoParticipante
     */
    private boolean _has_cdSituacaoParticipante;

    /**
     * Field _cdDescricaoSituacaoParticapante
     */
    private java.lang.String _cdDescricaoSituacaoParticapante;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _dsClub
     */
    private java.lang.String _dsClub;

    /**
     * Field _cdCanal
     */
    private java.lang.String _cdCanal;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdTipoLayoutMae
     */
    private int _cdTipoLayoutMae = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutMae
     */
    private boolean _has_cdTipoLayoutMae;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdPerfilTrocaMae
     */
    private long _cdPerfilTrocaMae = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaMae
     */
    private boolean _has_cdPerfilTrocaMae;

    /**
     * Field _cdPessoaJuridicaMae
     */
    private long _cdPessoaJuridicaMae = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaMae
     */
    private boolean _has_cdPessoaJuridicaMae;

    /**
     * Field _cdTipoContratoMae
     */
    private int _cdTipoContratoMae = 0;

    /**
     * keeps track of state for field: _cdTipoContratoMae
     */
    private boolean _has_cdTipoContratoMae;

    /**
     * Field _nrSequencialContratoMae
     */
    private long _nrSequencialContratoMae = 0;

    /**
     * keeps track of state for field: _nrSequencialContratoMae
     */
    private boolean _has_nrSequencialContratoMae;

    /**
     * Field _cdTipoParticipanteMae
     */
    private int _cdTipoParticipanteMae = 0;

    /**
     * keeps track of state for field: _cdTipoParticipanteMae
     */
    private boolean _has_cdTipoParticipanteMae;

    /**
     * Field _cdCpessoaMae
     */
    private long _cdCpessoaMae = 0;

    /**
     * keeps track of state for field: _cdCpessoaMae
     */
    private boolean _has_cdCpessoaMae;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdFilialCnpj
     */
    private int _cdFilialCnpj = 0;

    /**
     * keeps track of state for field: _cdFilialCnpj
     */
    private boolean _has_cdFilialCnpj;

    /**
     * Field _cdControleCnpj
     */
    private int _cdControleCnpj = 0;

    /**
     * keeps track of state for field: _cdControleCnpj
     */
    private boolean _has_cdControleCnpj;

    /**
     * Field _dsNomeRazao
     */
    private java.lang.String _dsNomeRazao;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistAssocTrocaArqParticipanteResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.response.DetalharHistAssocTrocaArqParticipanteResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjContrato
     * 
     */
    public void deleteCdCnpjContrato()
    {
        this._has_cdCnpjContrato= false;
    } //-- void deleteCdCnpjContrato() 

    /**
     * Method deleteCdControleCnpj
     * 
     */
    public void deleteCdControleCnpj()
    {
        this._has_cdControleCnpj= false;
    } //-- void deleteCdControleCnpj() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdCpessoaMae
     * 
     */
    public void deleteCdCpessoaMae()
    {
        this._has_cdCpessoaMae= false;
    } //-- void deleteCdCpessoaMae() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdCpfCnpjDigito
     * 
     */
    public void deleteCdCpfCnpjDigito()
    {
        this._has_cdCpfCnpjDigito= false;
    } //-- void deleteCdCpfCnpjDigito() 

    /**
     * Method deleteCdFilialCnpj
     * 
     */
    public void deleteCdFilialCnpj()
    {
        this._has_cdFilialCnpj= false;
    } //-- void deleteCdFilialCnpj() 

    /**
     * Method deleteCdPerfilTrocaArquivo
     * 
     */
    public void deleteCdPerfilTrocaArquivo()
    {
        this._has_cdPerfilTrocaArquivo= false;
    } //-- void deleteCdPerfilTrocaArquivo() 

    /**
     * Method deleteCdPerfilTrocaMae
     * 
     */
    public void deleteCdPerfilTrocaMae()
    {
        this._has_cdPerfilTrocaMae= false;
    } //-- void deleteCdPerfilTrocaMae() 

    /**
     * Method deleteCdPessoaJuridicaMae
     * 
     */
    public void deleteCdPessoaJuridicaMae()
    {
        this._has_cdPessoaJuridicaMae= false;
    } //-- void deleteCdPessoaJuridicaMae() 

    /**
     * Method deleteCdSituacaoParticipante
     * 
     */
    public void deleteCdSituacaoParticipante()
    {
        this._has_cdSituacaoParticipante= false;
    } //-- void deleteCdSituacaoParticipante() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContratoMae
     * 
     */
    public void deleteCdTipoContratoMae()
    {
        this._has_cdTipoContratoMae= false;
    } //-- void deleteCdTipoContratoMae() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoLayoutMae
     * 
     */
    public void deleteCdTipoLayoutMae()
    {
        this._has_cdTipoLayoutMae= false;
    } //-- void deleteCdTipoLayoutMae() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdTipoParticipanteMae
     * 
     */
    public void deleteCdTipoParticipanteMae()
    {
        this._has_cdTipoParticipanteMae= false;
    } //-- void deleteCdTipoParticipanteMae() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequencialContratoMae
     * 
     */
    public void deleteNrSequencialContratoMae()
    {
        this._has_nrSequencialContratoMae= false;
    } //-- void deleteNrSequencialContratoMae() 

    /**
     * Returns the value of field 'cdCanal'.
     * 
     * @return String
     * @return the value of field 'cdCanal'.
     */
    public java.lang.String getCdCanal()
    {
        return this._cdCanal;
    } //-- java.lang.String getCdCanal() 

    /**
     * Returns the value of field 'cdCnpjContrato'.
     * 
     * @return int
     * @return the value of field 'cdCnpjContrato'.
     */
    public int getCdCnpjContrato()
    {
        return this._cdCnpjContrato;
    } //-- int getCdCnpjContrato() 

    /**
     * Returns the value of field 'cdControleCnpj'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpj'.
     */
    public int getCdControleCnpj()
    {
        return this._cdControleCnpj;
    } //-- int getCdControleCnpj() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdCpessoaMae'.
     * 
     * @return long
     * @return the value of field 'cdCpessoaMae'.
     */
    public long getCdCpessoaMae()
    {
        return this._cdCpessoaMae;
    } //-- long getCdCpessoaMae() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpjDigito'.
     * 
     * @return int
     * @return the value of field 'cdCpfCnpjDigito'.
     */
    public int getCdCpfCnpjDigito()
    {
        return this._cdCpfCnpjDigito;
    } //-- int getCdCpfCnpjDigito() 

    /**
     * Returns the value of field
     * 'cdDescricaoSituacaoParticapante'.
     * 
     * @return String
     * @return the value of field 'cdDescricaoSituacaoParticapante'.
     */
    public java.lang.String getCdDescricaoSituacaoParticapante()
    {
        return this._cdDescricaoSituacaoParticapante;
    } //-- java.lang.String getCdDescricaoSituacaoParticapante() 

    /**
     * Returns the value of field 'cdFilialCnpj'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpj'.
     */
    public int getCdFilialCnpj()
    {
        return this._cdFilialCnpj;
    } //-- int getCdFilialCnpj() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArquivo'.
     */
    public long getCdPerfilTrocaArquivo()
    {
        return this._cdPerfilTrocaArquivo;
    } //-- long getCdPerfilTrocaArquivo() 

    /**
     * Returns the value of field 'cdPerfilTrocaMae'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaMae'.
     */
    public long getCdPerfilTrocaMae()
    {
        return this._cdPerfilTrocaMae;
    } //-- long getCdPerfilTrocaMae() 

    /**
     * Returns the value of field 'cdPessoaJuridicaMae'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaMae'.
     */
    public long getCdPessoaJuridicaMae()
    {
        return this._cdPessoaJuridicaMae;
    } //-- long getCdPessoaJuridicaMae() 

    /**
     * Returns the value of field 'cdSituacaoParticipante'.
     * 
     * @return long
     * @return the value of field 'cdSituacaoParticipante'.
     */
    public long getCdSituacaoParticipante()
    {
        return this._cdSituacaoParticipante;
    } //-- long getCdSituacaoParticipante() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContratoMae'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoMae'.
     */
    public int getCdTipoContratoMae()
    {
        return this._cdTipoContratoMae;
    } //-- int getCdTipoContratoMae() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoLayoutMae'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutMae'.
     */
    public int getCdTipoLayoutMae()
    {
        return this._cdTipoLayoutMae;
    } //-- int getCdTipoLayoutMae() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdTipoParticipante'.
     * 
     * @return String
     * @return the value of field 'cdTipoParticipante'.
     */
    public java.lang.String getCdTipoParticipante()
    {
        return this._cdTipoParticipante;
    } //-- java.lang.String getCdTipoParticipante() 

    /**
     * Returns the value of field 'cdTipoParticipanteMae'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipanteMae'.
     */
    public int getCdTipoParticipanteMae()
    {
        return this._cdTipoParticipanteMae;
    } //-- int getCdTipoParticipanteMae() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsClub'.
     * 
     * @return String
     * @return the value of field 'dsClub'.
     */
    public java.lang.String getDsClub()
    {
        return this._dsClub;
    } //-- java.lang.String getDsClub() 

    /**
     * Returns the value of field 'dsCodigoTipoLayout'.
     * 
     * @return String
     * @return the value of field 'dsCodigoTipoLayout'.
     */
    public java.lang.String getDsCodigoTipoLayout()
    {
        return this._dsCodigoTipoLayout;
    } //-- java.lang.String getDsCodigoTipoLayout() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsNomeRazao'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazao'.
     */
    public java.lang.String getDsNomeRazao()
    {
        return this._dsNomeRazao;
    } //-- java.lang.String getDsNomeRazao() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipante'.
     */
    public java.lang.String getDsTipoParticipante()
    {
        return this._dsTipoParticipante;
    } //-- java.lang.String getDsTipoParticipante() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequencialContratoMae'.
     * 
     * @return long
     * @return the value of field 'nrSequencialContratoMae'.
     */
    public long getNrSequencialContratoMae()
    {
        return this._nrSequencialContratoMae;
    } //-- long getNrSequencialContratoMae() 

    /**
     * Method hasCdCnpjContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjContrato()
    {
        return this._has_cdCnpjContrato;
    } //-- boolean hasCdCnpjContrato() 

    /**
     * Method hasCdControleCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpj()
    {
        return this._has_cdControleCnpj;
    } //-- boolean hasCdControleCnpj() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdCpessoaMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpessoaMae()
    {
        return this._has_cdCpessoaMae;
    } //-- boolean hasCdCpessoaMae() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdCpfCnpjDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjDigito()
    {
        return this._has_cdCpfCnpjDigito;
    } //-- boolean hasCdCpfCnpjDigito() 

    /**
     * Method hasCdFilialCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpj()
    {
        return this._has_cdFilialCnpj;
    } //-- boolean hasCdFilialCnpj() 

    /**
     * Method hasCdPerfilTrocaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArquivo()
    {
        return this._has_cdPerfilTrocaArquivo;
    } //-- boolean hasCdPerfilTrocaArquivo() 

    /**
     * Method hasCdPerfilTrocaMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaMae()
    {
        return this._has_cdPerfilTrocaMae;
    } //-- boolean hasCdPerfilTrocaMae() 

    /**
     * Method hasCdPessoaJuridicaMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaMae()
    {
        return this._has_cdPessoaJuridicaMae;
    } //-- boolean hasCdPessoaJuridicaMae() 

    /**
     * Method hasCdSituacaoParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoParticipante()
    {
        return this._has_cdSituacaoParticipante;
    } //-- boolean hasCdSituacaoParticipante() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContratoMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoMae()
    {
        return this._has_cdTipoContratoMae;
    } //-- boolean hasCdTipoContratoMae() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoLayoutMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutMae()
    {
        return this._has_cdTipoLayoutMae;
    } //-- boolean hasCdTipoLayoutMae() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdTipoParticipanteMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipanteMae()
    {
        return this._has_cdTipoParticipanteMae;
    } //-- boolean hasCdTipoParticipanteMae() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequencialContratoMae
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequencialContratoMae()
    {
        return this._has_nrSequencialContratoMae;
    } //-- boolean hasNrSequencialContratoMae() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCanal'.
     * 
     * @param cdCanal the value of field 'cdCanal'.
     */
    public void setCdCanal(java.lang.String cdCanal)
    {
        this._cdCanal = cdCanal;
    } //-- void setCdCanal(java.lang.String) 

    /**
     * Sets the value of field 'cdCnpjContrato'.
     * 
     * @param cdCnpjContrato the value of field 'cdCnpjContrato'.
     */
    public void setCdCnpjContrato(int cdCnpjContrato)
    {
        this._cdCnpjContrato = cdCnpjContrato;
        this._has_cdCnpjContrato = true;
    } //-- void setCdCnpjContrato(int) 

    /**
     * Sets the value of field 'cdControleCnpj'.
     * 
     * @param cdControleCnpj the value of field 'cdControleCnpj'.
     */
    public void setCdControleCnpj(int cdControleCnpj)
    {
        this._cdControleCnpj = cdControleCnpj;
        this._has_cdControleCnpj = true;
    } //-- void setCdControleCnpj(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdCpessoaMae'.
     * 
     * @param cdCpessoaMae the value of field 'cdCpessoaMae'.
     */
    public void setCdCpessoaMae(long cdCpessoaMae)
    {
        this._cdCpessoaMae = cdCpessoaMae;
        this._has_cdCpessoaMae = true;
    } //-- void setCdCpessoaMae(long) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdCpfCnpjDigito'.
     * 
     * @param cdCpfCnpjDigito the value of field 'cdCpfCnpjDigito'.
     */
    public void setCdCpfCnpjDigito(int cdCpfCnpjDigito)
    {
        this._cdCpfCnpjDigito = cdCpfCnpjDigito;
        this._has_cdCpfCnpjDigito = true;
    } //-- void setCdCpfCnpjDigito(int) 

    /**
     * Sets the value of field 'cdDescricaoSituacaoParticapante'.
     * 
     * @param cdDescricaoSituacaoParticapante the value of field
     * 'cdDescricaoSituacaoParticapante'.
     */
    public void setCdDescricaoSituacaoParticapante(java.lang.String cdDescricaoSituacaoParticapante)
    {
        this._cdDescricaoSituacaoParticapante = cdDescricaoSituacaoParticapante;
    } //-- void setCdDescricaoSituacaoParticapante(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpj'.
     * 
     * @param cdFilialCnpj the value of field 'cdFilialCnpj'.
     */
    public void setCdFilialCnpj(int cdFilialCnpj)
    {
        this._cdFilialCnpj = cdFilialCnpj;
        this._has_cdFilialCnpj = true;
    } //-- void setCdFilialCnpj(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPerfilTrocaArquivo'.
     * 
     * @param cdPerfilTrocaArquivo the value of field
     * 'cdPerfilTrocaArquivo'.
     */
    public void setCdPerfilTrocaArquivo(long cdPerfilTrocaArquivo)
    {
        this._cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
        this._has_cdPerfilTrocaArquivo = true;
    } //-- void setCdPerfilTrocaArquivo(long) 

    /**
     * Sets the value of field 'cdPerfilTrocaMae'.
     * 
     * @param cdPerfilTrocaMae the value of field 'cdPerfilTrocaMae'
     */
    public void setCdPerfilTrocaMae(long cdPerfilTrocaMae)
    {
        this._cdPerfilTrocaMae = cdPerfilTrocaMae;
        this._has_cdPerfilTrocaMae = true;
    } //-- void setCdPerfilTrocaMae(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaMae'.
     * 
     * @param cdPessoaJuridicaMae the value of field
     * 'cdPessoaJuridicaMae'.
     */
    public void setCdPessoaJuridicaMae(long cdPessoaJuridicaMae)
    {
        this._cdPessoaJuridicaMae = cdPessoaJuridicaMae;
        this._has_cdPessoaJuridicaMae = true;
    } //-- void setCdPessoaJuridicaMae(long) 

    /**
     * Sets the value of field 'cdSituacaoParticipante'.
     * 
     * @param cdSituacaoParticipante the value of field
     * 'cdSituacaoParticipante'.
     */
    public void setCdSituacaoParticipante(long cdSituacaoParticipante)
    {
        this._cdSituacaoParticipante = cdSituacaoParticipante;
        this._has_cdSituacaoParticipante = true;
    } //-- void setCdSituacaoParticipante(long) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContratoMae'.
     * 
     * @param cdTipoContratoMae the value of field
     * 'cdTipoContratoMae'.
     */
    public void setCdTipoContratoMae(int cdTipoContratoMae)
    {
        this._cdTipoContratoMae = cdTipoContratoMae;
        this._has_cdTipoContratoMae = true;
    } //-- void setCdTipoContratoMae(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoLayoutMae'.
     * 
     * @param cdTipoLayoutMae the value of field 'cdTipoLayoutMae'.
     */
    public void setCdTipoLayoutMae(int cdTipoLayoutMae)
    {
        this._cdTipoLayoutMae = cdTipoLayoutMae;
        this._has_cdTipoLayoutMae = true;
    } //-- void setCdTipoLayoutMae(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdTipoParticipante'.
     * 
     * @param cdTipoParticipante the value of field
     * 'cdTipoParticipante'.
     */
    public void setCdTipoParticipante(java.lang.String cdTipoParticipante)
    {
        this._cdTipoParticipante = cdTipoParticipante;
    } //-- void setCdTipoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoParticipanteMae'.
     * 
     * @param cdTipoParticipanteMae the value of field
     * 'cdTipoParticipanteMae'.
     */
    public void setCdTipoParticipanteMae(int cdTipoParticipanteMae)
    {
        this._cdTipoParticipanteMae = cdTipoParticipanteMae;
        this._has_cdTipoParticipanteMae = true;
    } //-- void setCdTipoParticipanteMae(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsClub'.
     * 
     * @param dsClub the value of field 'dsClub'.
     */
    public void setDsClub(java.lang.String dsClub)
    {
        this._dsClub = dsClub;
    } //-- void setDsClub(java.lang.String) 

    /**
     * Sets the value of field 'dsCodigoTipoLayout'.
     * 
     * @param dsCodigoTipoLayout the value of field
     * 'dsCodigoTipoLayout'.
     */
    public void setDsCodigoTipoLayout(java.lang.String dsCodigoTipoLayout)
    {
        this._dsCodigoTipoLayout = dsCodigoTipoLayout;
    } //-- void setDsCodigoTipoLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeRazao'.
     * 
     * @param dsNomeRazao the value of field 'dsNomeRazao'.
     */
    public void setDsNomeRazao(java.lang.String dsNomeRazao)
    {
        this._dsNomeRazao = dsNomeRazao;
    } //-- void setDsNomeRazao(java.lang.String) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipante'.
     * 
     * @param dsTipoParticipante the value of field
     * 'dsTipoParticipante'.
     */
    public void setDsTipoParticipante(java.lang.String dsTipoParticipante)
    {
        this._dsTipoParticipante = dsTipoParticipante;
    } //-- void setDsTipoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequencialContratoMae'.
     * 
     * @param nrSequencialContratoMae the value of field
     * 'nrSequencialContratoMae'.
     */
    public void setNrSequencialContratoMae(long nrSequencialContratoMae)
    {
        this._nrSequencialContratoMae = nrSequencialContratoMae;
        this._has_nrSequencialContratoMae = true;
    } //-- void setNrSequencialContratoMae(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistAssocTrocaArqParticipanteResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.response.DetalharHistAssocTrocaArqParticipanteResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.response.DetalharHistAssocTrocaArqParticipanteResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.response.DetalharHistAssocTrocaArqParticipanteResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistassoctrocaarqparticipante.response.DetalharHistAssocTrocaArqParticipanteResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
