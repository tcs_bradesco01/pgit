/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ExcluirVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ExcluirVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarTiposPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarTiposPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarVincMsgLancOperSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: VincMsgLanctOpeContratada
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IVincMsgLanctOpeContratadaService {
	
	/**
	 * Listar vinc msg lanc oper.
	 *
	 * @param listarVincMsgLancOperEntradaDTO the listar vinc msg lanc oper entrada dto
	 * @return the list< listar vinc msg lanc oper saida dt o>
	 */
	List<ListarVincMsgLancOperSaidaDTO> listarVincMsgLancOper(ListarVincMsgLancOperEntradaDTO listarVincMsgLancOperEntradaDTO);
	
	/**
	 * Incluir vinc msg lanc oper.
	 *
	 * @param incluirVincMsgLancOperEntradaDTO the incluir vinc msg lanc oper entrada dto
	 * @return the incluir vinc msg lanc oper saida dto
	 */
	IncluirVincMsgLancOperSaidaDTO incluirVincMsgLancOper(IncluirVincMsgLancOperEntradaDTO incluirVincMsgLancOperEntradaDTO);
	
	/**
	 * Detalhar vinc msg lanc oper.
	 *
	 * @param detalharVincMsgLancOperEntradaDTO the detalhar vinc msg lanc oper entrada dto
	 * @return the detalhar vinc msg lanc oper saida dto
	 */
	DetalharVincMsgLancOperSaidaDTO detalharVincMsgLancOper(DetalharVincMsgLancOperEntradaDTO detalharVincMsgLancOperEntradaDTO);
	
	/**
	 * Excluir vinc msg lanc oper.
	 *
	 * @param excluirVincMsgLancOperEntradaDTO the excluir vinc msg lanc oper entrada dto
	 * @return the excluir vinc msg lanc oper saida dto
	 */
	ExcluirVincMsgLancOperSaidaDTO excluirVincMsgLancOper(ExcluirVincMsgLancOperEntradaDTO excluirVincMsgLancOperEntradaDTO);
	
	/**
	 * Listar tipos pagto .
	 *
	 * @param entrada the  listar tipos pagto entrada dto 
	 * @return the excluir listar tipos pagto saida dto
	 */
	ListarTiposPagtoSaidaDTO listarTiposPagto();
}

