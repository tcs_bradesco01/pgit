/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacao
     */
    private int _cdSolicitacao = 0;

    /**
     * keeps track of state for field: _cdSolicitacao
     */
    private boolean _has_cdSolicitacao;

    /**
     * Field _nrSolicitacao
     */
    private int _nrSolicitacao = 0;

    /**
     * keeps track of state for field: _nrSolicitacao
     */
    private boolean _has_nrSolicitacao;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdAmbienteAtivoSolicitacao
     */
    private java.lang.String _cdAmbienteAtivoSolicitacao;

    /**
     * Field _dtProgramadaMudancaAmbiente
     */
    private java.lang.String _dtProgramadaMudancaAmbiente;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;

    /**
     * Field _dtSolicitacao
     */
    private java.lang.String _dtSolicitacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _dsProdutoServicoRelacionado
     */
    private java.lang.String _dsProdutoServicoRelacionado;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdCnpjParticipante
     */
    private long _cdCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCnpjParticipante
     */
    private boolean _has_cdCnpjParticipante;

    /**
     * Field _cdFilialParticipante
     */
    private int _cdFilialParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialParticipante
     */
    private boolean _has_cdFilialParticipante;

    /**
     * Field _cdControleCnpjParticipante
     */
    private int _cdControleCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdControleCnpjParticipante
     */
    private boolean _has_cdControleCnpjParticipante;

    /**
     * Field _dsNomeRazaoParticipante
     */
    private java.lang.String _dsNomeRazaoParticipante;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjParticipante
     * 
     */
    public void deleteCdCnpjParticipante()
    {
        this._has_cdCnpjParticipante= false;
    } //-- void deleteCdCnpjParticipante() 

    /**
     * Method deleteCdControleCnpjParticipante
     * 
     */
    public void deleteCdControleCnpjParticipante()
    {
        this._has_cdControleCnpjParticipante= false;
    } //-- void deleteCdControleCnpjParticipante() 

    /**
     * Method deleteCdFilialParticipante
     * 
     */
    public void deleteCdFilialParticipante()
    {
        this._has_cdFilialParticipante= false;
    } //-- void deleteCdFilialParticipante() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdSolicitacao
     * 
     */
    public void deleteCdSolicitacao()
    {
        this._has_cdSolicitacao= false;
    } //-- void deleteCdSolicitacao() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteNrSolicitacao
     * 
     */
    public void deleteNrSolicitacao()
    {
        this._has_nrSolicitacao= false;
    } //-- void deleteNrSolicitacao() 

    /**
     * Returns the value of field 'cdAmbienteAtivoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'cdAmbienteAtivoSolicitacao'.
     */
    public java.lang.String getCdAmbienteAtivoSolicitacao()
    {
        return this._cdAmbienteAtivoSolicitacao;
    } //-- java.lang.String getCdAmbienteAtivoSolicitacao() 

    /**
     * Returns the value of field 'cdCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCnpjParticipante'.
     */
    public long getCdCnpjParticipante()
    {
        return this._cdCnpjParticipante;
    } //-- long getCdCnpjParticipante() 

    /**
     * Returns the value of field 'cdControleCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdControleCnpjParticipante'.
     */
    public int getCdControleCnpjParticipante()
    {
        return this._cdControleCnpjParticipante;
    } //-- int getCdControleCnpjParticipante() 

    /**
     * Returns the value of field 'cdFilialParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialParticipante'.
     */
    public int getCdFilialParticipante()
    {
        return this._cdFilialParticipante;
    } //-- int getCdFilialParticipante() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field 'cdSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacao'.
     */
    public int getCdSolicitacao()
    {
        return this._cdSolicitacao;
    } //-- int getCdSolicitacao() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'dsNomeRazaoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazaoParticipante'.
     */
    public java.lang.String getDsNomeRazaoParticipante()
    {
        return this._dsNomeRazaoParticipante;
    } //-- java.lang.String getDsNomeRazaoParticipante() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Returns the value of field 'dsProdutoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoRelacionado'.
     */
    public java.lang.String getDsProdutoServicoRelacionado()
    {
        return this._dsProdutoServicoRelacionado;
    } //-- java.lang.String getDsProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dtProgramadaMudancaAmbiente'.
     * 
     * @return String
     * @return the value of field 'dtProgramadaMudancaAmbiente'.
     */
    public java.lang.String getDtProgramadaMudancaAmbiente()
    {
        return this._dtProgramadaMudancaAmbiente;
    } //-- java.lang.String getDtProgramadaMudancaAmbiente() 

    /**
     * Returns the value of field 'dtSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtSolicitacao'.
     */
    public java.lang.String getDtSolicitacao()
    {
        return this._dtSolicitacao;
    } //-- java.lang.String getDtSolicitacao() 

    /**
     * Returns the value of field 'nrSolicitacao'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacao'.
     */
    public int getNrSolicitacao()
    {
        return this._nrSolicitacao;
    } //-- int getNrSolicitacao() 

    /**
     * Method hasCdCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjParticipante()
    {
        return this._has_cdCnpjParticipante;
    } //-- boolean hasCdCnpjParticipante() 

    /**
     * Method hasCdControleCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCnpjParticipante()
    {
        return this._has_cdControleCnpjParticipante;
    } //-- boolean hasCdControleCnpjParticipante() 

    /**
     * Method hasCdFilialParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialParticipante()
    {
        return this._has_cdFilialParticipante;
    } //-- boolean hasCdFilialParticipante() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacao()
    {
        return this._has_cdSolicitacao;
    } //-- boolean hasCdSolicitacao() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasNrSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacao()
    {
        return this._has_nrSolicitacao;
    } //-- boolean hasNrSolicitacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAmbienteAtivoSolicitacao'.
     * 
     * @param cdAmbienteAtivoSolicitacao the value of field
     * 'cdAmbienteAtivoSolicitacao'.
     */
    public void setCdAmbienteAtivoSolicitacao(java.lang.String cdAmbienteAtivoSolicitacao)
    {
        this._cdAmbienteAtivoSolicitacao = cdAmbienteAtivoSolicitacao;
    } //-- void setCdAmbienteAtivoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'cdCnpjParticipante'.
     * 
     * @param cdCnpjParticipante the value of field
     * 'cdCnpjParticipante'.
     */
    public void setCdCnpjParticipante(long cdCnpjParticipante)
    {
        this._cdCnpjParticipante = cdCnpjParticipante;
        this._has_cdCnpjParticipante = true;
    } //-- void setCdCnpjParticipante(long) 

    /**
     * Sets the value of field 'cdControleCnpjParticipante'.
     * 
     * @param cdControleCnpjParticipante the value of field
     * 'cdControleCnpjParticipante'.
     */
    public void setCdControleCnpjParticipante(int cdControleCnpjParticipante)
    {
        this._cdControleCnpjParticipante = cdControleCnpjParticipante;
        this._has_cdControleCnpjParticipante = true;
    } //-- void setCdControleCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdFilialParticipante'.
     * 
     * @param cdFilialParticipante the value of field
     * 'cdFilialParticipante'.
     */
    public void setCdFilialParticipante(int cdFilialParticipante)
    {
        this._cdFilialParticipante = cdFilialParticipante;
        this._has_cdFilialParticipante = true;
    } //-- void setCdFilialParticipante(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSolicitacao'.
     * 
     * @param cdSolicitacao the value of field 'cdSolicitacao'.
     */
    public void setCdSolicitacao(int cdSolicitacao)
    {
        this._cdSolicitacao = cdSolicitacao;
        this._has_cdSolicitacao = true;
    } //-- void setCdSolicitacao(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'dsNomeRazaoParticipante'.
     * 
     * @param dsNomeRazaoParticipante the value of field
     * 'dsNomeRazaoParticipante'.
     */
    public void setDsNomeRazaoParticipante(java.lang.String dsNomeRazaoParticipante)
    {
        this._dsNomeRazaoParticipante = dsNomeRazaoParticipante;
    } //-- void setDsNomeRazaoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoRelacionado'.
     * 
     * @param dsProdutoServicoRelacionado the value of field
     * 'dsProdutoServicoRelacionado'.
     */
    public void setDsProdutoServicoRelacionado(java.lang.String dsProdutoServicoRelacionado)
    {
        this._dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
    } //-- void setDsProdutoServicoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtProgramadaMudancaAmbiente'.
     * 
     * @param dtProgramadaMudancaAmbiente the value of field
     * 'dtProgramadaMudancaAmbiente'.
     */
    public void setDtProgramadaMudancaAmbiente(java.lang.String dtProgramadaMudancaAmbiente)
    {
        this._dtProgramadaMudancaAmbiente = dtProgramadaMudancaAmbiente;
    } //-- void setDtProgramadaMudancaAmbiente(java.lang.String) 

    /**
     * Sets the value of field 'dtSolicitacao'.
     * 
     * @param dtSolicitacao the value of field 'dtSolicitacao'.
     */
    public void setDtSolicitacao(java.lang.String dtSolicitacao)
    {
        this._dtSolicitacao = dtSolicitacao;
    } //-- void setDtSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacao'.
     * 
     * @param nrSolicitacao the value of field 'nrSolicitacao'.
     */
    public void setNrSolicitacao(int nrSolicitacao)
    {
        this._nrSolicitacao = nrSolicitacao;
        this._has_nrSolicitacao = true;
    } //-- void setNrSolicitacao(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaomudancaambpartc.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
