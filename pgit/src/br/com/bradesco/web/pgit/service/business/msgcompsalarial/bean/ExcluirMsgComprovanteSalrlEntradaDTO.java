/*
 * Nome: br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean;

/**
 * Nome: ExcluirMsgComprovanteSalrlEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirMsgComprovanteSalrlEntradaDTO {
	
	/** Atributo codigoTipoMensagem. */
	private int codigoTipoMensagem;

	/**
	 * Get: codigoTipoMensagem.
	 *
	 * @return codigoTipoMensagem
	 */
	public int getCodigoTipoMensagem() {
		return codigoTipoMensagem;
	}

	/**
	 * Set: codigoTipoMensagem.
	 *
	 * @param codigoTipoMensagem the codigo tipo mensagem
	 */
	public void setCodigoTipoMensagem(int codigoTipoMensagem) {
		this.codigoTipoMensagem = codigoTipoMensagem;
	}

}
