/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _hrConsultasSaldoPagamento
     */
    private java.lang.String _hrConsultasSaldoPagamento;

    /**
     * Field _cdSituacao
     */
    private int _cdSituacao = 0;

    /**
     * keeps track of state for field: _cdSituacao
     */
    private boolean _has_cdSituacao;

    /**
     * Field _cdValor
     */
    private java.math.BigDecimal _cdValor = new java.math.BigDecimal("0");

    /**
     * Field _dsSinal
     */
    private java.lang.String _dsSinal = " ";


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setCdValor(new java.math.BigDecimal("0"));
        setDsSinal(" ");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacao
     * 
     */
    public void deleteCdSituacao()
    {
        this._has_cdSituacao= false;
    } //-- void deleteCdSituacao() 

    /**
     * Returns the value of field 'cdSituacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacao'.
     */
    public int getCdSituacao()
    {
        return this._cdSituacao;
    } //-- int getCdSituacao() 

    /**
     * Returns the value of field 'cdValor'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdValor'.
     */
    public java.math.BigDecimal getCdValor()
    {
        return this._cdValor;
    } //-- java.math.BigDecimal getCdValor() 

    /**
     * Returns the value of field 'dsSinal'.
     * 
     * @return String
     * @return the value of field 'dsSinal'.
     */
    public java.lang.String getDsSinal()
    {
        return this._dsSinal;
    } //-- java.lang.String getDsSinal() 

    /**
     * Returns the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrConsultasSaldoPagamento'.
     */
    public java.lang.String getHrConsultasSaldoPagamento()
    {
        return this._hrConsultasSaldoPagamento;
    } //-- java.lang.String getHrConsultasSaldoPagamento() 

    /**
     * Method hasCdSituacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacao()
    {
        return this._has_cdSituacao;
    } //-- boolean hasCdSituacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacao'.
     * 
     * @param cdSituacao the value of field 'cdSituacao'.
     */
    public void setCdSituacao(int cdSituacao)
    {
        this._cdSituacao = cdSituacao;
        this._has_cdSituacao = true;
    } //-- void setCdSituacao(int) 

    /**
     * Sets the value of field 'cdValor'.
     * 
     * @param cdValor the value of field 'cdValor'.
     */
    public void setCdValor(java.math.BigDecimal cdValor)
    {
        this._cdValor = cdValor;
    } //-- void setCdValor(java.math.BigDecimal) 

    /**
     * Sets the value of field 'dsSinal'.
     * 
     * @param dsSinal the value of field 'dsSinal'.
     */
    public void setDsSinal(java.lang.String dsSinal)
    {
        this._dsSinal = dsSinal;
    } //-- void setDsSinal(java.lang.String) 

    /**
     * Sets the value of field 'hrConsultasSaldoPagamento'.
     * 
     * @param hrConsultasSaldoPagamento the value of field
     * 'hrConsultasSaldoPagamento'.
     */
    public void setHrConsultasSaldoPagamento(java.lang.String hrConsultasSaldoPagamento)
    {
        this._hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
    } //-- void setHrConsultasSaldoPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
