/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO {
    
    /** Atributo cdTipoMeioTransmissao. */
    private Integer cdTipoMeioTransmissao;
    
    /** Atributo dsTipoMeioTransmissao. */
    private String dsTipoMeioTransmissao;
	
    /**
     * Get: cdTipoMeioTransmissao.
     *
     * @return cdTipoMeioTransmissao
     */
    public Integer getCdTipoMeioTransmissao() {
		return cdTipoMeioTransmissao;
	}
	
	/**
	 * Set: cdTipoMeioTransmissao.
	 *
	 * @param cdTipoMeioTransmissao the cd tipo meio transmissao
	 */
	public void setCdTipoMeioTransmissao(Integer cdTipoMeioTransmissao) {
		this.cdTipoMeioTransmissao = cdTipoMeioTransmissao;
	}
	
	/**
	 * Get: dsTipoMeioTransmissao.
	 *
	 * @return dsTipoMeioTransmissao
	 */
	public String getDsTipoMeioTransmissao() {
		return dsTipoMeioTransmissao;
	}
	
	/**
	 * Set: dsTipoMeioTransmissao.
	 *
	 * @param dsTipoMeioTransmissao the ds tipo meio transmissao
	 */
	public void setDsTipoMeioTransmissao(String dsTipoMeioTransmissao) {
		this.dsTipoMeioTransmissao = dsTipoMeioTransmissao;
	}

}
