/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoretorno.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ComboProdutoDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.OcorrenciaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoretorno.IArquivoRetornoService;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.Cmpi0000Bean;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;

/**
 * Nome: ArquivoRetornoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ArquivoRetornoBean {
	
	/** Atributo logger. */
	private ILogManager logger;

	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;

	/** Atributo mostraBotoes0007. */
	private boolean mostraBotoes0007;

	/** Atributo mostraBotoes0008. */
	private boolean mostraBotoes0008;

	/** Atributo mostraBotoes0010. */
	private boolean mostraBotoes0010;

	/** Atributo mostraBotoes0011. */
	private boolean mostraBotoes0011;

	/** Atributo codListaIncosistentes. */
	private String codListaIncosistentes;

	/** Atributo codListaRetornoRadio. */
	private int codListaRetornoRadio;

	/** Atributo filtro. */
	private int filtro;

	/** Atributo cpfFiltro. */
	private String cpfFiltro;

	/** Atributo dataDeFiltro. */
	private Date dataDeFiltro;

	/** Atributo dataAteFiltro. */
	private Date dataAteFiltro;

	/** Atributo listaProduto. */
	private List<SelectItem> listaProduto = null;

	/** Atributo listaProdutoAux. */
	private List<ComboProdutoDTO> listaProdutoAux;

	/** Atributo produtoFiltro. */
	private String produtoFiltro;

	/** Atributo perfilFiltro. */
	private String perfilFiltro;

	/** Atributo validaCnpj. */
	private String validaCnpj;

	/** Atributo tipoRetornoFiltro. */
	private String tipoRetornoFiltro;

	/** Atributo sequenciaRetornoFiltro. */
	private String sequenciaRetornoFiltro;

	/** Atributo situacaoFiltro. */
	private String situacaoFiltro;

	/** Atributo cpf. */
	private String cpf;

	/** Atributo dataDe. */
	private String dataDe;

	/** Atributo dataAte. */
	private String dataAte;

	/** Atributo produto. */
	private String produto;

	/** Atributo perfil. */
	private String perfil;

	/** Atributo processamento. */
	private String processamento;

	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;

	/** Atributo nomeRazao. */
	private String nomeRazao;

	/** Atributo banco. */
	private String banco;

	/** Atributo agencia. */
	private String agencia;

	/** Atributo razao. */
	private String razao;

	/** Atributo conta. */
	private String conta;

	/** Atributo codigoLancamento. */
	private String codigoLancamento;

	/** Atributo dataHoraRecepcao. */
	private String dataHoraRecepcao;

	/** Atributo canalTransmissao. */
	private String canalTransmissao;

	/** Atributo layout. */
	private String layout;

	/** Atributo qtdRegistrosRetorno. */
	private String qtdRegistrosRetorno;

	/** Atributo valorTotalRetorno. */
	private String valorTotalRetorno;

	/** Atributo sequenciaRetorno. */
	private String sequenciaRetorno;

	/** Atributo qtdRegistrosLote. */
	private String qtdRegistrosLote;

	/** Atributo modalidadePagamento. */
	private String modalidadePagamento;

	/** Atributo valorTotalLote. */
	private String valorTotalLote;

	/** Atributo processamentoLote. */
	private String processamentoLote;

	/** Atributo condicaoLote. */
	private String condicaoLote;

	/** Atributo qtdInconsistentes. */
	private String qtdInconsistentes;

	/** Atributo valorInconsistentes. */
	private String valorInconsistentes;

	/** Atributo sequenciaRegistros. */
	private String sequenciaRegistros;

	/** Atributo instrucaoMovimento. */
	private String instrucaoMovimento;

	/** Atributo numeroPagamento. */
	private String numeroPagamento;

	/** Atributo codListaLote. */
	private int codListaLote;

	/** Atributo hdnCodListaLote. */
	private int hdnCodListaLote;

	/** Atributo sequenciaLote. */
	private String sequenciaLote;

	/** Atributo situacaoProcessamentoRetorno. */
	private String situacaoProcessamentoRetorno;

	/** Atributo resultadoProcessamentoLote. */
	private String resultadoProcessamentoLote;

	/** Atributo situacaoProcessamentoLote. */
	private String situacaoProcessamentoLote;

	/** Atributo modalidadePagamentoLote. */
	private String modalidadePagamentoLote;

	/** Atributo bancoAgenciaConta. */
	private String bancoAgenciaConta;

	/** Atributo qtdConsistentes. */
	private String qtdConsistentes;

	/** Atributo valorConsistentes. */
	private String valorConsistentes;

	/** Atributo dataNascimentoFundacao. */
	private Date dataNascimentoFundacao;

	/** Atributo filtroIdentificacaoCliente. */
	private String filtroIdentificacaoCliente;

	/** Atributo cpfCnpjIdentificaoCliente. */
	private String cpfCnpjIdentificaoCliente;

	/** Atributo nomeRazaoIdentificaCliente. */
	private String nomeRazaoIdentificaCliente;

	/** Atributo bancoIdentificacaoCliente. */
	private String bancoIdentificacaoCliente;

	/** Atributo agenciaIdentificacaoCliente. */
	private String agenciaIdentificacaoCliente;

	/** Atributo contaIdentificacaoCliente. */
	private String contaIdentificacaoCliente;

	/** Atributo listaGridRetorno. */
	private List<ArquivoRetornoDTO> listaGridRetorno;

	/** Atributo listaControleRetorno. */
	private List<SelectItem> listaControleRetorno = new ArrayList<SelectItem>();

	/** Atributo listaControleLote. */
	private List<SelectItem> listaControleLote = new ArrayList<SelectItem>();

	/** Atributo listaControleInconsistentes. */
	private List<SelectItem> listaControleInconsistentes;

	/** Atributo listaGridOcorrencia. */
	private List<OcorrenciaDTO> listaGridOcorrencia;

	/** Atributo listaControleIncosistentes. */
	private List<SelectItem> listaControleIncosistentes;

	/** Atributo arquivoRemessaRetornoImpl. */
	private IArquivoRetornoService arquivoRemessaRetornoImpl;

	/** Atributo listaCodigo. */
	private List<SelectItem> listaCodigo = new ArrayList<SelectItem>();

	/** Atributo listaGridCliente. */
	private List<ListarClientesDTO> listaGridCliente;

	/** Atributo ambienteProcessamento. */
	private String ambienteProcessamento;

	/** Atributo listaOcorrencia. */
	private List<OcorrenciaDTO> listaOcorrencia;

	/** Atributo listaLote. */
	private List<LoteRetornoDTO> listaLote;

	/** Atributo listaControleCliente. */
	private List<SelectItem> listaControleCliente = new ArrayList<SelectItem>();

	/** Atributo hiddenRadioCliente. */
	private int hiddenRadioCliente;

	/** Atributo hiddenRadioFiltroArgumento. */
	private int hiddenRadioFiltroArgumento;

	/** Atributo validaCpf. */
	private String validaCpf;

	/** Atributo validaCpfCliente. */
	private String validaCpfCliente;

	/** Atributo numeroLoteRetorno. */
	private Long numeroLoteRetorno;

	/** Atributo horaInclusao. */
	private String horaInclusao;

	/** Atributo numeroArquivoRetorno. */
	private Long numeroArquivoRetorno;

	/** Atributo disabledCampos. */
	private boolean disabledCampos = false;

	/** Atributo listaOcorrenciaRetorno. */
	private List<OcorrenciaRetornoDTO> listaOcorrenciaRetorno;

	/** Atributo cmpi0000Bean. */
	private Cmpi0000Bean cmpi0000Bean;

	/** Atributo dataHoraGeracao. */
	private String dataHoraGeracao;

	/** Atributo tipoRetorno. */
	private String tipoRetorno;

	/** Atributo situacaoRetorno. */
	private String situacaoRetorno;

	/** Atributo operacoesEfetivadasConsistentesQtde. */
	private int operacoesEfetivadasConsistentesQtde;

	/** Atributo operacoesNaoEfetivadasInconsistentesQtde. */
	private int operacoesNaoEfetivadasInconsistentesQtde;

	/** Atributo totalGeralQuantidade. */
	private int totalGeralQuantidade;

	/** Atributo operacoesEfetivadasConsistentesValor. */
	private double operacoesEfetivadasConsistentesValor;

	/** Atributo operacoesNaoEfetivadasInconsistentesValor. */
	private double operacoesNaoEfetivadasInconsistentesValor;

	/** Atributo totalGeralValor. */
	private double totalGeralValor;
	
	/** Atributo desabPerfil. */
	private boolean desabPerfil;
	
	/** Atributo desabRetorno. */
	private boolean desabRetorno;

	/** Atributo desabSitProcRet. */
	private boolean desabSitProcRet;

	/** Atributo desabInconsistente. */
	private boolean desabInconsistente;
	
	/** Atributo itemSelecionado. */
	private String itemSelecionado;

	/** Atributo listaGridInconsistentes. */
	private List<InconsistentesRetornoDTO> listaGridInconsistentes;

	/** Atributo codListaInconsistentesRadio. */
	private int codListaInconsistentesRadio;

	/**
	 * Arquivo retorno bean.
	 */
	public ArquivoRetornoBean() {
		
	}
	
	/**
	 * Load combo produto.
	 */
	private void loadComboProduto(){
		
		if (listaProduto == null){
			logger.debug(this, "Iniciando a consulta de servi�os  para popular o combo de servi�os.");
			try {
				List<ComboProdutoDTO> comboProdutoList = arquivoRemessaRetornoImpl.listaComboProduto();
				int i = 0;
				listaProduto = new ArrayList<SelectItem>();
				for (i = 0; i < comboProdutoList.size(); i++) {
					listaProduto.add(new SelectItem(comboProdutoList.get(i).getCentroCusto(), comboProdutoList.get(i).getDescricaoCentroCusto()));
				}
			} catch (PdcAdapterException p) {
				this.setListaProduto(new ArrayList<SelectItem>());
				logger.debug(this, "N�o foi possivel carregar a lista do PDC.");
				logger.debug(this, "/t C�dgio: " + p.getCode()+ "/t Mensagen: " + p.getMessage());
			}finally{
				logger.debug(this, "Finalizando o carregamento da lista de produtos.");
			}
		}
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		
		logger.debug(this, "Iniciando a limpeza dos dados da p�gina.");
		
		cmpi0000Bean.setNomeRazaoFundacao("");
		cmpi0000Bean.setCpf("");
		cmpi0000Bean.setCpf1("");
		cmpi0000Bean.setCpf2("");
		cmpi0000Bean.setCnpj1("");
		cmpi0000Bean.setCnpj2("");
		cmpi0000Bean.setCnpj3("");		
		cmpi0000Bean.setNomeRazaoIdentificaCliente("");
		cmpi0000Bean.setCnpj1aux("");
		cmpi0000Bean.setCnpj2aux("");
		cmpi0000Bean.setCnpj3aux("");
		cmpi0000Bean.setAgenciaIdentificacaoCliente("");
		cmpi0000Bean.setBancoIdentificacaoCliente("");
		cmpi0000Bean.setContaIdentificacaoCliente("");
		cmpi0000Bean.setDiaNasc("");
		cmpi0000Bean.setMesNasc("");
		cmpi0000Bean.setAnoNasc("");
		
		this.cmpi0000Bean.setDesabDataRecRet(true);
		this.cmpi0000Bean.setDesabDataFinRet(true);
		this.cmpi0000Bean.setDesabServicoRet(true);
		this.cmpi0000Bean.setDesabCodEmpresaRet(true);
		this.cmpi0000Bean.setDesabSeqRemessaRet(true);
		this.setDesabSitProcRet(true);
		this.cmpi0000Bean.setDesabBtnConsultarRet(true);
		this.cmpi0000Bean.setDesabResTipo(true);
		this.cmpi0000Bean.limparDadosIdentificaoCliente();
		
		itemSelecionado = "0";
		desabPerfil = true;
		desabRetorno = true;
		desabSitProcRet = true;
		
		this.setDataDeFiltro(new Date());
		this.setDataAteFiltro(new Date());
		this.setProdutoFiltro("");
		this.setPerfilFiltro("");
		this.setTipoRetornoFiltro("");
		this.setSequenciaRetornoFiltro("");
		this.setSituacaoFiltro("");
		this.setMostraBotoes0007(false);
		this.setListaGridRetorno(new ArrayList<ArquivoRetornoDTO>());
		
		setItemSelecionado("0");
		
		loadComboProduto();
		
		logger.debug(this, "Finalizando a limpeza dos dados da p�gina.");
	}
	
	/**
	 * Limpar erro.
	 */
	public void limparErro() {
		
		logger.debug(this, "Iniciando a limpeza dos campos.");
		
		cmpi0000Bean.setNomeRazaoFundacao("");
		cmpi0000Bean.setCpf("");
		cmpi0000Bean.setCnpj1("");
		cmpi0000Bean.setCnpj2("");
		cmpi0000Bean.setCnpj3("");		
		cmpi0000Bean.setNomeRazaoIdentificaCliente("");
		cmpi0000Bean.setCnpj1aux("");
		cmpi0000Bean.setCnpj2aux("");
		cmpi0000Bean.setCnpj3aux("");
		cmpi0000Bean.setAgenciaIdentificacaoCliente("");
		cmpi0000Bean.setBancoIdentificacaoCliente("");
		cmpi0000Bean.setContaIdentificacaoCliente("");
		cmpi0000Bean.setDiaNasc("");
		cmpi0000Bean.setMesNasc("");
		cmpi0000Bean.setAnoNasc("");	

		this.cmpi0000Bean.setDesabDataRecRet(true);
		this.cmpi0000Bean.setDesabDataFinRet(true);
		this.cmpi0000Bean.setDesabServicoRet(true);
		this.cmpi0000Bean.setDesabCodEmpresaRet(true);
		this.cmpi0000Bean.setDesabSeqRemessaRet(true);
		this.setDesabSitProcRet(true);
		this.cmpi0000Bean.setDesabBtnConsultarRet(true);
		this.cmpi0000Bean.setDesabResTipo(true);
		this.cmpi0000Bean.limparDadosIdentificaoCliente();
		this.setDataDeFiltro(new Date());
		this.setDataAteFiltro(new Date());
		this.setProdutoFiltro("");
		this.setPerfilFiltro("");
		this.setTipoRetornoFiltro("");
		this.setSequenciaRetornoFiltro("");
		this.setSituacaoFiltro("");
		this.setItemSelecionado("0");
		
		logger.debug(this, "Finalizando a limpeza dos campos.");
		
	}


	/**
	 * Consultar cliente.
	 *
	 * @return the string
	 */
	public String consultarCliente() {
		
		logger.debug(this, "Iniciando a consulta dos clientes.");
		
		try {
			String validaDocumento = validarCampos();

			cmpi0000Bean.setTela("RETORNO");
			cmpi0000Bean.setValidaCpfCliente(validaDocumento);
			cmpi0000Bean.setHiddenRadioFiltroArgumento(getHiddenRadioFiltroArgumento());

			if (validaDocumento.equals("T")) {

				ListarClientesDTO dto = new ListarClientesDTO();

				if (!SiteUtil.isEmptyOrNull(cmpi0000Bean.getCnpj1())) {
					dto.setCodigoCpfCnpjCliente(cmpi0000Bean.getCnpj1());

				} else if (!SiteUtil.isEmptyOrNull(cmpi0000Bean.getCpf1())) {
					dto.setCodigoCpfCnpjCliente(cmpi0000Bean.getCpf1());
				}

				if (!SiteUtil.isEmptyOrNull(cmpi0000Bean.getCnpj2())) {
					dto.setCodigoFilialCnpj(cmpi0000Bean.getCnpj2());
				}

				if (!SiteUtil.isEmptyOrNull(cmpi0000Bean.getCnpj3())) {
					dto.setControleCpfCnpj(cmpi0000Bean.getCnpj3());

				} else if (!SiteUtil.isEmptyOrNull(cmpi0000Bean.getCpf2())) {
					dto.setControleCpfCnpj(cmpi0000Bean.getCpf2());
				}

				String dataNasc = "";

				if ((!SiteUtil.isEmptyOrNull(this.cmpi0000Bean.getDiaNasc()) && (!SiteUtil.isEmptyOrNull(this.cmpi0000Bean.getMesNasc())) && (!SiteUtil.isEmptyOrNull(this.cmpi0000Bean.getAnoNasc())))) {
					dataNasc = this.cmpi0000Bean.getDiaNasc() + "." + this.cmpi0000Bean.getMesNasc() + "." + this.cmpi0000Bean.getAnoNasc();
				}
				dto.setDataNascimentoFundacao(dataNasc);

				dto.setNomeRazao(cmpi0000Bean.getNomeRazaoIdentificaCliente().toUpperCase());
				dto.setCodigoBancoDebito(cmpi0000Bean.getBancoIdentificacaoCliente());
				dto.setAgenciaBancaria(cmpi0000Bean.getAgenciaIdentificacaoCliente());
				dto.setContaBancaria(cmpi0000Bean.getContaIdentificacaoCliente());

				this.cmpi0000Bean.setListaGridCliente(arquivoRemessaRetornoImpl.listaGridClientes(dto));

				if (this.cmpi0000Bean.getListaGridCliente().size() > 1) {
					this.cmpi0000Bean.carregaListaClientes();
					this.cmpi0000Bean.limparCamposDePesquisa();
					this.setDataDeFiltro(new Date());
					this.setDataAteFiltro(new Date());
					this.setProdutoFiltro("");
					this.setPerfilFiltro("");
					this.setTipoRetornoFiltro("");
					this.setSequenciaRetornoFiltro("");
					this.setSituacaoFiltro("");
					this.setMostraBotoes0007(false);
					this.setItemSelecionado("0");
					this.setDesabSitProcRet(true);

					return "CONSULTAR_CLIENTE";
				} else {

					this.cmpi0000Bean.setCpf(this.cmpi0000Bean.getListaGridCliente().get(0).getCpf());
					this.cmpi0000Bean.setNomeRazaoFundacao(this.cmpi0000Bean.getListaGridCliente().get(0).getNomeRazao());

					this.cmpi0000Bean.setCnpj1aux(new String(this.cmpi0000Bean.getListaGridCliente().get(0).getCpfCnpjCliente().replace(".", "")));
					this.cmpi0000Bean.setCnpj2aux(new String(this.cmpi0000Bean.getListaGridCliente().get(0).getFilialCnpj()));
					this.cmpi0000Bean.setCnpj3aux(new String(this.cmpi0000Bean.getListaGridCliente().get(0).getControleCpfCnpj()));

					this.cmpi0000Bean.setDesabDataRecRet(false);
					this.cmpi0000Bean.setDesabDataFinRet(false);
					this.cmpi0000Bean.setDesabServicoRet(false);
					this.cmpi0000Bean.setDesabCodEmpresaRet(false);
					this.cmpi0000Bean.setDesabSeqRemessaRet(false);
					this.setDesabSitProcRet(false);
					this.cmpi0000Bean.setDesabBtnConsultarRet(false);
					this.cmpi0000Bean.setDesabResTipo(false);
					this.cmpi0000Bean.limparCamposDePesquisa();
					this.setDataDeFiltro(new Date());
					this.setDataAteFiltro(new Date());
					this.setProdutoFiltro("");
					this.setPerfilFiltro("");
					this.setTipoRetornoFiltro("");
					this.setSequenciaRetornoFiltro("");
					this.setSituacaoFiltro("");
					this.setItemSelecionado("0");

				}
			}
			
		} catch (PdcAdapterException p) {
			String code = p.getCode().substring(p.getCode().length()-8,p.getCode().length());
			BradescoFacesUtils.addInfoModalMessage( "(" + code + ") " + p.getMessage(), "/CMPI0007.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			limparErro();
			return null;			
		} finally{
			logger.debug(this, "Finalizando a consulta dos clientes.");
			habilitarFiltros();
			this.setListaGridRetorno(new ArrayList<ArquivoRetornoDTO>());
		}

		return "ok";
	}

	/**
	 * Habilitar filtros.
	 */
	void habilitarFiltros(){
		if (this.produtoFiltro == null) {
			this.produtoFiltro="";
		}
		if (this.perfilFiltro == null) {
			this.perfilFiltro="";
		}

		this.setDesabPerfil(this.produtoFiltro.equals(""));
		this.setDesabRetorno(this.perfilFiltro.equals(""));

	}
	
	/**
	 * Validar campos.
	 *
	 * @return the string
	 */
	private String validarCampos() {

		logger.debug(this, "Iniciando a valida��o dos campos.");
		
		String resultado = "T";

		if (filtro == 1) {

			if (this.cmpi0000Bean.getCpf1().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getCpf1())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getCpf2().equals("")) {
				resultado = "F";
			}
		}

		if (filtro == 0) {

			if (this.cmpi0000Bean.getCnpj1().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getCnpj1())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getCnpj2().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getCnpj2())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getCnpj3().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getCnpj3())) {
				resultado = "F";
			}
		}

		if (filtro == 2) {

			if (this.cmpi0000Bean.getNomeRazaoIdentificaCliente().equals("")) {
				resultado = "F";
			}
		}

		if (filtro == 3) {
			if (this.cmpi0000Bean.getBancoIdentificacaoCliente().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getBancoIdentificacaoCliente())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getAgenciaIdentificacaoCliente().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getAgenciaIdentificacaoCliente())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getContaIdentificacaoCliente().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getContaIdentificacaoCliente())) {
				resultado = "F";
			}
		}

		logger.debug(this, "Finalizando a valida��o dos campos.");
		
		return resultado;
	}

	/**
	 * Validar campo menor zero.
	 *
	 * @param campo the campo
	 * @return true, if validar campo menor zero
	 */
	private boolean validarCampoMenorZero(String campo) {

		logger.debug(this, "Iniciando a valida��o dos campos.");
		
		Long valor = new Long(campo);

		boolean resultado = false;

		if (valor.longValue() < 0) {
			resultado = true;
		}

		logger.debug(this, "Iniciando a valida��o dos campos.");
		
		return resultado;
	}

	/**
	 * Validar campos de pesquisa.
	 *
	 * @return true, if validar campos de pesquisa
	 */
	private boolean validarCamposDePesquisa() {

		logger.debug(this, "Iniciando a valida��o dos campos de pesquisa.");
		
		boolean resultado = true;

		if (this.getPerfil() != null) {
			if (validarCampoMenorZero(this.getPerfil())) {
				resultado = false;
			}
		}

		if (this.getSequenciaRetorno() != null) {

			String sequencia = this.getSequenciaRetorno().replace(".", "");

			if (validarCampoMenorZero(sequencia)) {
				resultado = false;
			}

		}
		
		logger.debug(this, "Finalizando a valida��o dos campos de pesquisa.");
		
		return resultado;

	}

	/**
	 * Find lista lote.
	 */
	private void findListaLote(){
		
		ArquivoRetornoDTO dto = new ArquivoRetornoDTO();

		if (cmpi0000Bean.getCnpj1aux() != null) {
			dto.setCpf_cliente(cmpi0000Bean.getCnpj1aux());
		} else {
			dto.setCpf_cliente("");
		}

		if (cmpi0000Bean.getCnpj2aux() != null) {
			dto.setCpf_filial(cmpi0000Bean.getCnpj2aux());
		} else {
			dto.setCpf_filial("");
		}

		if (cmpi0000Bean.getCnpj3aux() != null) {
			dto.setCpf_controle(cmpi0000Bean.getCnpj3aux());
		} else {
			dto.setCpf_controle("");
		}

		if (this.getDataDeFiltro() == null || this.getDataDeFiltro().equals("")) {
			dto.setHoraRecepcaoInicio("");
		} else {
			dto.setHoraRecepcaoInicio(SiteUtil.dateToString(this.getDataDeFiltro(), "dd.MM.yyyy"));
		}

		if (this.getDataAteFiltro() == null || this.getDataAteFiltro().equals("")) {
			dto.setHoraRecepcaoFim("");
		} else {
			dto.setHoraRecepcaoFim(SiteUtil.dateToString(this.getDataAteFiltro(), "dd.MM.yyyy"));
		}

		dto.setProduto(this.getProdutoFiltro());
		dto.setPerfil(this.getPerfilFiltro());
		dto.setTipoArquivoRetorno(this.getTipoRetornoFiltro());
		dto.setNumeroArquivoRetorno(this.getSequenciaRetornoFiltro());
		dto.setSituacaoGeracaoRetorno(this.getSituacaoFiltro());

		setListaGridRetorno(arquivoRemessaRetornoImpl.listaGrid(dto));		
	}
	
	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		
		logger.debug(this, "Iniciando o carregamenteo da lista.");
		
		try {
			if (this.validarCamposDePesquisa()) {
				
				//limpa a lista para preencher com os dados da nova consulta
				this.setListaGridRetorno(new ArrayList<ArquivoRetornoDTO>());
				
				findListaLote();
				
				List<SelectItem> listaSelectItem = new ArrayList<SelectItem>();
				
				int i = 0;
				for (i = 0; i <= this.getListaGridRetorno().size(); i++) {
					listaSelectItem.add(new SelectItem(i, " "));
				}
				this.setListaControleRetorno(listaSelectItem);
				if (this.getListaGridRetorno().size() > 10) {
					this.setMostraBotoes0007(true);
				} else {
					this.setMostraBotoes0007(false);
				}
			}
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), "/CMPI0007.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			return null;			
		} finally {
			logger.debug(this, "Finalizando o carregamento da lista.");
			habilitarFiltros();
		}

		return "ok";
	}
	
	/**
	 * Pesquisar arquivos lotes.
	 *
	 * @param event the event
	 */
	public void pesquisarArquivosLotes(ActionEvent event){
		findListaLote();
	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {

		logger.debug(this, "Iniciando a limpeza dos campos.");
		
		cmpi0000Bean.setNomeRazaoFundacao("");
		cmpi0000Bean.setCpf("");
		cmpi0000Bean.setCpf1("");
		cmpi0000Bean.setCpf2("");
		cmpi0000Bean.setCnpj1("");
		cmpi0000Bean.setCnpj2("");
		cmpi0000Bean.setCnpj3("");		
		cmpi0000Bean.setNomeRazaoIdentificaCliente("");
		cmpi0000Bean.setCnpj1aux("");
		cmpi0000Bean.setCnpj2aux("");
		cmpi0000Bean.setCnpj3aux("");
		cmpi0000Bean.setAgenciaIdentificacaoCliente("");
		cmpi0000Bean.setBancoIdentificacaoCliente("");
		cmpi0000Bean.setContaIdentificacaoCliente("");
		cmpi0000Bean.setDiaNasc("");
		cmpi0000Bean.setMesNasc("");
		cmpi0000Bean.setAnoNasc("");	

		this.setDataDeFiltro(new Date());
		this.setDataAteFiltro(new Date());
		this.setProdutoFiltro("");
		this.setPerfilFiltro("");
		this.setTipoRetornoFiltro("");
		this.setSequenciaRetornoFiltro("");
		this.setSituacaoFiltro("");
		this.setMostraBotoes0007(false);

		desabPerfil = true;
		desabRetorno = true;
		desabSitProcRet = true;

		this.cmpi0000Bean.setDesabDataRecRet(true);
		this.cmpi0000Bean.setDesabDataFinRet(true);
		this.cmpi0000Bean.setDesabServicoRet(true);
		this.cmpi0000Bean.setDesabCodEmpresaRet(true);
		this.cmpi0000Bean.setDesabSeqRemessaRet(true);
		this.setDesabSitProcRet(true);
		this.cmpi0000Bean.setDesabBtnConsultarRet(true);
		this.cmpi0000Bean.setDesabResTipo(true);
		this.cmpi0000Bean.limparDadosIdentificaoCliente();
		
		this.setListaGridRetorno(new ArrayList<ArquivoRetornoDTO>());

		setItemSelecionado("0");
		
		logger.debug(this, "Finalizar a limpeza dos campos.");
		
		return "ok";

	}

	/**
	 * Find lotes.
	 *
	 * @param arquivoRetornoDTO the arquivo retorno dto
	 */
	private void findLotes(ArquivoRetornoDTO arquivoRetornoDTO){
		
		LoteRetornoDTO loteDTO = new LoteRetornoDTO();

		loteDTO.setCodigoSistemaLegado(arquivoRetornoDTO.getCodigoSistemaLegado());
		loteDTO.setCodigoCpfCnpj(arquivoRetornoDTO.getCpf_cliente());
		loteDTO.setCodigoFilialCpfCnpj(arquivoRetornoDTO.getCpf_filial());
		loteDTO.setControleCpfCnpj(arquivoRetornoDTO.getCpf_controle());
		loteDTO.setCodigoPerfil(arquivoRetornoDTO.getPerfil());
		loteDTO.setHoraInclusao(arquivoRetornoDTO.getHoraInclusao());
		loteDTO.setNumeroArquivoRetorno(new Long(arquivoRetornoDTO.getNumeroArquivoRetorno().replace(".", "")));

		setListaLote(arquivoRemessaRetornoImpl.listaLote(loteDTO));
	}
	
	/**
	 * Consultar lotes.
	 *
	 * @return the string
	 */
	public String consultarLotes() {
		
		logger.debug(this, "Iniciando a consulta dos lotes.");
		
		try {
			ArquivoRetornoDTO arquivoRetornoDTO = getListaGridRetorno().get(getCodListaRetornoRadio());

			setCpf(arquivoRetornoDTO.getCpf_completo());
			setNomeRazao(arquivoRetornoDTO.getNome());
			setProduto(arquivoRetornoDTO.getProduto());
			setPerfil(arquivoRetornoDTO.getPerfil());
			setBanco(arquivoRetornoDTO.getBancoDebito());
			setAgencia(arquivoRetornoDTO.getAgenciaDebito());
			setRazao(arquivoRetornoDTO.getRazaoContaDebito());
			setConta(arquivoRetornoDTO.getContaBancaria());
			setCodigoLancamento(arquivoRetornoDTO.getLancamentoContaDebito());
			setCanalTransmissao(arquivoRetornoDTO.getMeioTransmissaoArquivo());
			setSequenciaRetorno(arquivoRetornoDTO.getNumeroArquivoRetorno());
			setDataHoraGeracao(arquivoRetornoDTO.getHoraGeracaoRetorno());
			setTipoRetorno(arquivoRetornoDTO.getDescricaoTipoArquivo());
			setSituacaoProcessamentoRetorno(arquivoRetornoDTO.getDescricaoSituacao());
			setLayout(arquivoRetornoDTO.getTipoLayout());
			setAmbienteProcessamento(arquivoRetornoDTO.getCodigoAmbiente());
			setQtdRegistrosRetorno(arquivoRetornoDTO.getQtdeOcorrencias());
			setValorTotalRetorno(arquivoRetornoDTO.getValorRegistroArquivoRetorno());
			setHoraInclusao(arquivoRetornoDTO.getHoraInclusao());
			setNumeroArquivoRetorno(arquivoRetornoDTO.getNumeroArquivoRetornoLong());

			findLotes(arquivoRetornoDTO);
			
			List<SelectItem> listaSelectItem = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaLote().size(); i++) {
				listaSelectItem.add(new SelectItem(i, ""));
			}
			setListaControleLote(listaSelectItem);
			if (this.getListaLote().size() > 10) {
				this.setMostraBotoes0008(true);
			} else {
				this.setMostraBotoes0008(false);
			}
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), "/CMPI0007.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			limparErro();
			return null;			
		} finally {
			logger.debug(this, "Finalizando a consulta dos lotes.");
		}

		return "CONSULTAR_LOTES";
	}

	/**
	 * Pesquisar lostes.
	 *
	 * @param event the event
	 */
	public void pesquisarLostes(ActionEvent event){
		ArquivoRetornoDTO arquivoRetornoDTO = getListaGridRetorno().get(getCodListaRetornoRadio());
		findLotes(arquivoRetornoDTO);
	}
	
	/**
	 * Consultar estatisticas lote.
	 *
	 * @return the string
	 */
	public String consultarEstatisticasLote() {
		
		logger.debug(this, "Inicando a consulta das estat�sticas dos lotes.");
		
		try {
			LoteRetornoDTO loteDTO = (LoteRetornoDTO) getListaLote().get(getHdnCodListaLote());
			setSequenciaLote(loteDTO.getSeqLote());
			setBancoAgenciaConta(loteDTO.getBcoAgeCtaDebito());
			setModalidadePagamentoLote(loteDTO.getModalidadePagto());
			setNumeroLoteRetorno(loteDTO.getNumeroLoteRetornoLong());

			setValorConsistentes(PgitUtil.formatarCampoDeValorMonetario(loteDTO.getValorCon()));
			setValorInconsistentes(PgitUtil.formatarCampoDeValorMonetario(loteDTO.getValorInc()));
			setValorTotalLote(PgitUtil.formatarCampoDeValorMonetario(loteDTO.getValorTotal()));
	
			setQtdConsistentes(PgitUtil.formatarCampoQuantidade(String.valueOf(loteDTO.getQuantidadeCon())));
			setQtdInconsistentes(PgitUtil.formatarCampoQuantidade(String.valueOf(loteDTO.getQuantidadeInc())));
			setQtdRegistrosLote(PgitUtil.formatarCampoQuantidade(String.valueOf(loteDTO.getQuantidadeTotal())));

			desabInconsistente = loteDTO.getValorInc().equals(Double.valueOf(0));
			
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), "/CMPI0008.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			limparErro();
			return null;			
		} finally {
			logger.debug(this, "Finalizando a consulta das estat�sticas dos lotes.");			
		}
		
		return "ESTATISTICAS_LOTE";
	}

	/**
	 * Find inconsistencias.
	 */
	private void findInconsistencias(){
		InconsistentesRetornoDTO dto = new InconsistentesRetornoDTO();

		ArquivoRetornoDTO arquivoRetornoDTO = getListaGridRetorno().get(getCodListaRetornoRadio());

		LoteRetornoDTO loteDTO = (LoteRetornoDTO) getListaLote().get(getHdnCodListaLote());

		dto.setCpfCnpj(arquivoRetornoDTO.getCpf_cliente());
		dto.setCodigoFilialCnpj(arquivoRetornoDTO.getCpf_filial());
		dto.setControleCnpj(dto.getControleCnpj());
		dto.setCodigoPerfil(arquivoRetornoDTO.getPerfil());
		dto.setSistemaLegado(arquivoRetornoDTO.getCodigoSistemaLegado());
		dto.setNumeroArquivoRetorno(arquivoRetornoDTO.getNumeroArquivoRetorno().replace(".", ""));
		dto.setNumeroLoteRetorno(loteDTO.getNumeroLoteRetornoLong());
		dto.setNumeroSequenciaRetorno("0");
		dto.setHoraInclusao(arquivoRetornoDTO.getHoraInclusao());

		this.setListaGridInconsistentes(arquivoRemessaRetornoImpl.listaGridInconsistentes(dto));
	}
	
	/**
	 * Carrega lista inconsistentes.
	 *
	 * @return the string
	 */
	public String carregaListaInconsistentes() {
		
		logger.debug(this, "Inicando o carregamento da lista de inconsist�ncias dos lotes.");
		
		try {
			findInconsistencias();
			
			List<SelectItem> listaSelectItem = new ArrayList<SelectItem>();
			
			int i = 0;
			for (i = 0; i <= this.getListaGridInconsistentes().size(); i++) {
				listaSelectItem.add(new SelectItem(i, " "));
			}

			this.setListaControleInconsistentes(listaSelectItem);
			if (this.getListaGridInconsistentes().size() > 10) {
				this.setMostraBotoes0010(true);
			} else {
				this.setMostraBotoes0010(false);
			}
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), "/CMPI0009.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			limparErro();
			return null;			
		}finally{
			logger.debug(this, "Finalizando o carregamento da lista de inconsist�ncias dos lotes.");
		}

		return "AVANCAR_INCONSISTENTES";
	}
	
	/**
	 * Pesquisar inconsistencia.
	 */
	public void pesquisarInconsistencia(){
		findInconsistencias();
	}


	/**
	 * Find ocorrencias.
	 */
	private void findOcorrencias(){
		
		ArquivoRetornoDTO arquivoRetornoDTO = getListaGridRetorno().get(getCodListaRetornoRadio());
		LoteRetornoDTO loteDTO = (LoteRetornoDTO) getListaLote().get(getHdnCodListaLote());
		InconsistentesRetornoDTO inconsistentesRetornoDTO = getListaGridInconsistentes().get(getCodListaInconsistentesRadio());
		
		OcorrenciaRetornoDTO ocorrenciaRetornoDTO = new OcorrenciaRetornoDTO();

		ocorrenciaRetornoDTO.setCodigoPerfil(arquivoRetornoDTO.getPerfil());
		ocorrenciaRetornoDTO.setControleCnpj(arquivoRetornoDTO.getCpf_controle());
		ocorrenciaRetornoDTO.setCpfCnpj(arquivoRetornoDTO.getCpf_cliente());
		ocorrenciaRetornoDTO.setFilalCnpj(arquivoRetornoDTO.getCpf_filial());
		ocorrenciaRetornoDTO.setSistemaLegado(arquivoRetornoDTO.getCodigoSistemaLegado());
		ocorrenciaRetornoDTO.setNumeroSequenciaRegistroRetorno(inconsistentesRetornoDTO.getNumeroSequenciaRetorno());
		ocorrenciaRetornoDTO.setNumeroArquivoRetorno(arquivoRetornoDTO.getNumeroArquivoRetornoLong());
		ocorrenciaRetornoDTO.setNumeroLoteRetorno(loteDTO.getNumeroLoteRetornoLong());
		ocorrenciaRetornoDTO.setHoraInclusao(arquivoRetornoDTO.getHoraInclusao());

		setListaOcorrenciaRetorno(arquivoRemessaRetornoImpl.listaOcorrenciaRetorno(ocorrenciaRetornoDTO));		
	}
	
	/**
	 * Consultar ocorrencias retorno.
	 *
	 * @return the string
	 */
	public String consultarOcorrenciasRetorno() {
		
		logger.debug(this, "Inicando a consulta das ocorr�ncias do retorno.");
		
		try {
			
			InconsistentesRetornoDTO inconsistentesRetornoDTO = getListaGridInconsistentes().get(getCodListaInconsistentesRadio());
			
			setSequenciaRegistros(inconsistentesRetornoDTO.getNumeroSequenciaRetorno());
			setNumeroPagamento(inconsistentesRetornoDTO.getControlePagamento());
			
			findOcorrencias();

			if (this.getListaOcorrenciaRetorno().size() > 10) {
				this.setMostraBotoes0011(true);
			} else {
				this.setMostraBotoes0011(false);
			}
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), "/CMPI0010.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			limparErro();
			return null;			
		}finally{
			logger.debug(this, "Finalizando a consulta das ocorr�ncias do retorno.");
		}

		return "AVANCAR_OCORRENCIA";
	}
	
	/**
	 * Pesquisar ocorrencias.
	 *
	 * @param event the event
	 */
	public void pesquisarOcorrencias(ActionEvent event){
		findOcorrencias();
	}

	/**
	 * Get: dataHoraGeracao.
	 *
	 * @return dataHoraGeracao
	 */
	public String getDataHoraGeracao() {
		return dataHoraGeracao;
	}

	/**
	 * Set: dataHoraGeracao.
	 *
	 * @param dataHoraGeracao the data hora geracao
	 */
	public void setDataHoraGeracao(String dataHoraGeracao) {
		this.dataHoraGeracao = dataHoraGeracao;
	}

	/**
	 * Get: listaOcorrenciaRetorno.
	 *
	 * @return listaOcorrenciaRetorno
	 */
	public List<OcorrenciaRetornoDTO> getListaOcorrenciaRetorno() {
		return listaOcorrenciaRetorno;
	}

	/**
	 * Set: listaOcorrenciaRetorno.
	 *
	 * @param listaOcorrenciaRetorno the lista ocorrencia retorno
	 */
	public void setListaOcorrenciaRetorno(List<OcorrenciaRetornoDTO> listaOcorrenciaRetorno) {
		this.listaOcorrenciaRetorno = listaOcorrenciaRetorno;
	}

	/**
	 * Get: horaInclusao.
	 *
	 * @return horaInclusao
	 */
	public String getHoraInclusao() {
		return horaInclusao;
	}

	/**
	 * Set: horaInclusao.
	 *
	 * @param horaInclusao the hora inclusao
	 */
	public void setHoraInclusao(String horaInclusao) {
		this.horaInclusao = horaInclusao;
	}

	/**
	 * Get: numeroLoteRetorno.
	 *
	 * @return numeroLoteRetorno
	 */
	public Long getNumeroLoteRetorno() {
		return numeroLoteRetorno;
	}

	/**
	 * Set: numeroLoteRetorno.
	 *
	 * @param numeroLoteRetorno the numero lote retorno
	 */
	public void setNumeroLoteRetorno(Long numeroLoteRetorno) {
		this.numeroLoteRetorno = numeroLoteRetorno;
	}

	/**
	 * Get: numeroArquivoRetorno.
	 *
	 * @return numeroArquivoRetorno
	 */
	public Long getNumeroArquivoRetorno() {
		return numeroArquivoRetorno;
	}

	/**
	 * Set: numeroArquivoRetorno.
	 *
	 * @param numeroArquivoRetorno the numero arquivo retorno
	 */
	public void setNumeroArquivoRetorno(Long numeroArquivoRetorno) {
		this.numeroArquivoRetorno = numeroArquivoRetorno;
	}

	/**
	 * Get: disabledCampos.
	 *
	 * @return disabledCampos
	 */
	public boolean getDisabledCampos() {
		return disabledCampos;
	}

	/**
	 * Set: disabledCampos.
	 *
	 * @param habilitaPerfil the disabled campos
	 */
	public void setDisabledCampos(boolean habilitaPerfil) {
		this.disabledCampos = habilitaPerfil;
	}

	/**
	 * Get: validaCnpj.
	 *
	 * @return validaCnpj
	 */
	public String getValidaCnpj() {
		return validaCnpj;
	}

	/**
	 * Set: validaCnpj.
	 *
	 * @param validaCnpj the valida cnpj
	 */
	public void setValidaCnpj(String validaCnpj) {
		this.validaCnpj = validaCnpj;
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: agenciaIdentificacaoCliente.
	 *
	 * @return agenciaIdentificacaoCliente
	 */
	public String getAgenciaIdentificacaoCliente() {
		return agenciaIdentificacaoCliente;
	}

	/**
	 * Set: agenciaIdentificacaoCliente.
	 *
	 * @param agenciaIdentificacaoCliente the agencia identificacao cliente
	 */
	public void setAgenciaIdentificacaoCliente(String agenciaIdentificacaoCliente) {
		this.agenciaIdentificacaoCliente = agenciaIdentificacaoCliente;
	}

	/**
	 * Get: ambienteProcessamento.
	 *
	 * @return ambienteProcessamento
	 */
	public String getAmbienteProcessamento() {
		return ambienteProcessamento;
	}

	/**
	 * Set: ambienteProcessamento.
	 *
	 * @param ambienteProcessamento the ambiente processamento
	 */
	public void setAmbienteProcessamento(String ambienteProcessamento) {
		this.ambienteProcessamento = ambienteProcessamento;
	}

	/**
	 * Get: arquivoRemessaRetornoImpl.
	 *
	 * @return arquivoRemessaRetornoImpl
	 */
	public IArquivoRetornoService getArquivoRemessaRetornoImpl() {
		return arquivoRemessaRetornoImpl;
	}

	/**
	 * Set: arquivoRemessaRetornoImpl.
	 *
	 * @param arquivoRemessaRetornoImpl the arquivo remessa retorno impl
	 */
	public void setArquivoRemessaRetornoImpl(IArquivoRetornoService arquivoRemessaRetornoImpl) {
		this.arquivoRemessaRetornoImpl = arquivoRemessaRetornoImpl;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: bancoAgenciaConta.
	 *
	 * @return bancoAgenciaConta
	 */
	public String getBancoAgenciaConta() {
		return bancoAgenciaConta;
	}

	/**
	 * Set: bancoAgenciaConta.
	 *
	 * @param bancoAgenciaConta the banco agencia conta
	 */
	public void setBancoAgenciaConta(String bancoAgenciaConta) {
		this.bancoAgenciaConta = bancoAgenciaConta;
	}

	/**
	 * Get: bancoIdentificacaoCliente.
	 *
	 * @return bancoIdentificacaoCliente
	 */
	public String getBancoIdentificacaoCliente() {
		return bancoIdentificacaoCliente;
	}

	/**
	 * Set: bancoIdentificacaoCliente.
	 *
	 * @param bancoIdentificacaoCliente the banco identificacao cliente
	 */
	public void setBancoIdentificacaoCliente(String bancoIdentificacaoCliente) {
		this.bancoIdentificacaoCliente = bancoIdentificacaoCliente;
	}

	/**
	 * Get: canalTransmissao.
	 *
	 * @return canalTransmissao
	 */
	public String getCanalTransmissao() {
		return canalTransmissao;
	}

	/**
	 * Set: canalTransmissao.
	 *
	 * @param canalTransmissao the canal transmissao
	 */
	public void setCanalTransmissao(String canalTransmissao) {
		this.canalTransmissao = canalTransmissao;
	}

	/**
	 * Get: cmpi0000Bean.
	 *
	 * @return cmpi0000Bean
	 */
	public Cmpi0000Bean getCmpi0000Bean() {
		return cmpi0000Bean;
	}

	/**
	 * Set: cmpi0000Bean.
	 *
	 * @param cmpi0000Bean the cmpi0000 bean
	 */
	public void setCmpi0000Bean(Cmpi0000Bean cmpi0000Bean) {
		this.cmpi0000Bean = cmpi0000Bean;
	}

	/**
	 * Get: codigoLancamento.
	 *
	 * @return codigoLancamento
	 */
	public String getCodigoLancamento() {
		return codigoLancamento;
	}

	/**
	 * Set: codigoLancamento.
	 *
	 * @param codigoLancamento the codigo lancamento
	 */
	public void setCodigoLancamento(String codigoLancamento) {
		this.codigoLancamento = codigoLancamento;
	}

	/**
	 * Get: codListaInconsistentesRadio.
	 *
	 * @return codListaInconsistentesRadio
	 */
	public int getCodListaInconsistentesRadio() {
		return codListaInconsistentesRadio;
	}

	/**
	 * Set: codListaInconsistentesRadio.
	 *
	 * @param codListaInconsistentesRadio the cod lista inconsistentes radio
	 */
	public void setCodListaInconsistentesRadio(int codListaInconsistentesRadio) {
		this.codListaInconsistentesRadio = codListaInconsistentesRadio;
	}

	/**
	 * Get: codListaIncosistentes.
	 *
	 * @return codListaIncosistentes
	 */
	public String getCodListaIncosistentes() {
		return codListaIncosistentes;
	}

	/**
	 * Set: codListaIncosistentes.
	 *
	 * @param codListaIncosistentes the cod lista incosistentes
	 */
	public void setCodListaIncosistentes(String codListaIncosistentes) {
		this.codListaIncosistentes = codListaIncosistentes;
	}

	/**
	 * Get: codListaLote.
	 *
	 * @return codListaLote
	 */
	public int getCodListaLote() {
		return codListaLote;
	}

	/**
	 * Set: codListaLote.
	 *
	 * @param codListaLote the cod lista lote
	 */
	public void setCodListaLote(int codListaLote) {
		this.codListaLote = codListaLote;
	}

	/**
	 * Get: codListaRetornoRadio.
	 *
	 * @return codListaRetornoRadio
	 */
	public int getCodListaRetornoRadio() {
		return codListaRetornoRadio;
	}

	/**
	 * Set: codListaRetornoRadio.
	 *
	 * @param codListaRetornoRadio the cod lista retorno radio
	 */
	public void setCodListaRetornoRadio(int codListaRetornoRadio) {
		this.codListaRetornoRadio = codListaRetornoRadio;
	}

	/**
	 * Get: condicaoLote.
	 *
	 * @return condicaoLote
	 */
	public String getCondicaoLote() {
		return condicaoLote;
	}

	/**
	 * Set: condicaoLote.
	 *
	 * @param condicaoLote the condicao lote
	 */
	public void setCondicaoLote(String condicaoLote) {
		this.condicaoLote = condicaoLote;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: contaIdentificacaoCliente.
	 *
	 * @return contaIdentificacaoCliente
	 */
	public String getContaIdentificacaoCliente() {
		return contaIdentificacaoCliente;
	}

	/**
	 * Set: contaIdentificacaoCliente.
	 *
	 * @param contaIdentificacaoCliente the conta identificacao cliente
	 */
	public void setContaIdentificacaoCliente(String contaIdentificacaoCliente) {
		this.contaIdentificacaoCliente = contaIdentificacaoCliente;
	}

	/**
	 * Get: cpf.
	 *
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Set: cpf.
	 *
	 * @param cpf the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Get: cpfCnpjIdentificaoCliente.
	 *
	 * @return cpfCnpjIdentificaoCliente
	 */
	public String getCpfCnpjIdentificaoCliente() {
		return cpfCnpjIdentificaoCliente;
	}

	/**
	 * Set: cpfCnpjIdentificaoCliente.
	 *
	 * @param cpfCnpjIdentificaoCliente the cpf cnpj identificao cliente
	 */
	public void setCpfCnpjIdentificaoCliente(String cpfCnpjIdentificaoCliente) {
		this.cpfCnpjIdentificaoCliente = cpfCnpjIdentificaoCliente;
	}

	/**
	 * Get: cpfFiltro.
	 *
	 * @return cpfFiltro
	 */
	public String getCpfFiltro() {
		return cpfFiltro;
	}

	/**
	 * Set: cpfFiltro.
	 *
	 * @param cpfFiltro the cpf filtro
	 */
	public void setCpfFiltro(String cpfFiltro) {
		this.cpfFiltro = cpfFiltro;
	}

	/**
	 * Get: dataAte.
	 *
	 * @return dataAte
	 */
	public String getDataAte() {
		return dataAte;
	}

	/**
	 * Set: dataAte.
	 *
	 * @param dataAte the data ate
	 */
	public void setDataAte(String dataAte) {
		this.dataAte = dataAte;
	}

	/**
	 * Get: dataAteFiltro.
	 *
	 * @return dataAteFiltro
	 */
	public Date getDataAteFiltro() {

		if (dataAteFiltro == null) {
			dataAteFiltro = new Date();
		}

		return dataAteFiltro;
	}

	/**
	 * Set: dataAteFiltro.
	 *
	 * @param dataAteFiltro the data ate filtro
	 */
	public void setDataAteFiltro(Date dataAteFiltro) {
		this.dataAteFiltro = dataAteFiltro;
	}

	/**
	 * Get: dataDe.
	 *
	 * @return dataDe
	 */
	public String getDataDe() {
		return dataDe;
	}

	/**
	 * Set: dataDe.
	 *
	 * @param dataDe the data de
	 */
	public void setDataDe(String dataDe) {
		this.dataDe = dataDe;
	}

	/**
	 * Get: dataDeFiltro.
	 *
	 * @return dataDeFiltro
	 */
	public Date getDataDeFiltro() {

		if (dataDeFiltro == null) {
			dataDeFiltro = new Date();
		}

		return dataDeFiltro;
	}

	/**
	 * Set: dataDeFiltro.
	 *
	 * @param dataDeFiltro the data de filtro
	 */
	public void setDataDeFiltro(Date dataDeFiltro) {
		this.dataDeFiltro = dataDeFiltro;
	}

	/**
	 * Get: dataHoraRecepcao.
	 *
	 * @return dataHoraRecepcao
	 */
	public String getDataHoraRecepcao() {
		return dataHoraRecepcao;
	}

	/**
	 * Set: dataHoraRecepcao.
	 *
	 * @param dataHoraRecepcao the data hora recepcao
	 */
	public void setDataHoraRecepcao(String dataHoraRecepcao) {
		this.dataHoraRecepcao = dataHoraRecepcao;
	}

	/**
	 * Get: dataNascimentoFundacao.
	 *
	 * @return dataNascimentoFundacao
	 */
	public Date getDataNascimentoFundacao() {
		return dataNascimentoFundacao;
	}

	/**
	 * Set: dataNascimentoFundacao.
	 *
	 * @param dataNascimentoFundacao the data nascimento fundacao
	 */
	public void setDataNascimentoFundacao(Date dataNascimentoFundacao) {
		this.dataNascimentoFundacao = dataNascimentoFundacao;
	}

	/**
	 * Get: filtro.
	 *
	 * @return filtro
	 */
	public int getFiltro() {
		return filtro;
	}

	/**
	 * Set: filtro.
	 *
	 * @param filtro the filtro
	 */
	public void setFiltro(int filtro) {
		this.filtro = filtro;
	}

	/**
	 * Get: filtroIdentificacaoCliente.
	 *
	 * @return filtroIdentificacaoCliente
	 */
	public String getFiltroIdentificacaoCliente() {
		return filtroIdentificacaoCliente;
	}

	/**
	 * Set: filtroIdentificacaoCliente.
	 *
	 * @param filtroIdentificacaoCliente the filtro identificacao cliente
	 */
	public void setFiltroIdentificacaoCliente(String filtroIdentificacaoCliente) {
		this.filtroIdentificacaoCliente = filtroIdentificacaoCliente;
	}

	/**
	 * Get: hdnCodListaLote.
	 *
	 * @return hdnCodListaLote
	 */
	public int getHdnCodListaLote() {
		return hdnCodListaLote;
	}

	/**
	 * Set: hdnCodListaLote.
	 *
	 * @param hdnCodListaLote the hdn cod lista lote
	 */
	public void setHdnCodListaLote(int hdnCodListaLote) {
		this.hdnCodListaLote = hdnCodListaLote;
	}

	/**
	 * Get: hiddenRadioCliente.
	 *
	 * @return hiddenRadioCliente
	 */
	public int getHiddenRadioCliente() {
		return hiddenRadioCliente;
	}

	/**
	 * Set: hiddenRadioCliente.
	 *
	 * @param hiddenRadioCliente the hidden radio cliente
	 */
	public void setHiddenRadioCliente(int hiddenRadioCliente) {
		this.hiddenRadioCliente = hiddenRadioCliente;
	}

	/**
	 * Get: hiddenRadioFiltroArgumento.
	 *
	 * @return hiddenRadioFiltroArgumento
	 */
	public int getHiddenRadioFiltroArgumento() {
		return hiddenRadioFiltroArgumento;
	}

	/**
	 * Set: hiddenRadioFiltroArgumento.
	 *
	 * @param hiddenRadioFiltroArgumento the hidden radio filtro argumento
	 */
	public void setHiddenRadioFiltroArgumento(int hiddenRadioFiltroArgumento) {
		this.hiddenRadioFiltroArgumento = hiddenRadioFiltroArgumento;
	}

	/**
	 * Get: instrucaoMovimento.
	 *
	 * @return instrucaoMovimento
	 */
	public String getInstrucaoMovimento() {
		return instrucaoMovimento;
	}

	/**
	 * Set: instrucaoMovimento.
	 *
	 * @param instrucaoMovimento the instrucao movimento
	 */
	public void setInstrucaoMovimento(String instrucaoMovimento) {
		this.instrucaoMovimento = instrucaoMovimento;
	}

	/**
	 * Get: layout.
	 *
	 * @return layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Set: layout.
	 *
	 * @param layout the layout
	 */
	public void setLayout(String layout) {
		this.layout = layout;
	}

	/**
	 * Get: listaCodigo.
	 *
	 * @return listaCodigo
	 */
	public List<SelectItem> getListaCodigo() {
		return listaCodigo;
	}

	/**
	 * Set: listaCodigo.
	 *
	 * @param listaCodigo the lista codigo
	 */
	public void setListaCodigo(List<SelectItem> listaCodigo) {
		this.listaCodigo = listaCodigo;
	}

	/**
	 * Get: listaControleCliente.
	 *
	 * @return listaControleCliente
	 */
	public List<SelectItem> getListaControleCliente() {
		return listaControleCliente;
	}

	/**
	 * Set: listaControleCliente.
	 *
	 * @param listaControleCliente the lista controle cliente
	 */
	public void setListaControleCliente(List<SelectItem> listaControleCliente) {
		this.listaControleCliente = listaControleCliente;
	}

	/**
	 * Get: listaControleInconsistentes.
	 *
	 * @return listaControleInconsistentes
	 */
	public List<SelectItem> getListaControleInconsistentes() {
		return listaControleInconsistentes;
	}

	/**
	 * Set: listaControleInconsistentes.
	 *
	 * @param listaControleInconsistentes the lista controle inconsistentes
	 */
	public void setListaControleInconsistentes(List<SelectItem> listaControleInconsistentes) {
		this.listaControleInconsistentes = listaControleInconsistentes;
	}

	/**
	 * Get: listaControleIncosistentes.
	 *
	 * @return listaControleIncosistentes
	 */
	public List<SelectItem> getListaControleIncosistentes() {
		return listaControleIncosistentes;
	}

	/**
	 * Set: listaControleIncosistentes.
	 *
	 * @param listaControleIncosistentes the lista controle incosistentes
	 */
	public void setListaControleIncosistentes(List<SelectItem> listaControleIncosistentes) {
		this.listaControleIncosistentes = listaControleIncosistentes;
	}

	/**
	 * Get: listaControleLote.
	 *
	 * @return listaControleLote
	 */
	public List<SelectItem> getListaControleLote() {
		return listaControleLote;
	}

	/**
	 * Set: listaControleLote.
	 *
	 * @param listaControleLote the lista controle lote
	 */
	public void setListaControleLote(List<SelectItem> listaControleLote) {
		this.listaControleLote = listaControleLote;
	}

	/**
	 * Get: listaControleRetorno.
	 *
	 * @return listaControleRetorno
	 */
	public List<SelectItem> getListaControleRetorno() {
		return listaControleRetorno;
	}

	/**
	 * Set: listaControleRetorno.
	 *
	 * @param listaControleRetorno the lista controle retorno
	 */
	public void setListaControleRetorno(List<SelectItem> listaControleRetorno) {
		this.listaControleRetorno = listaControleRetorno;
	}

	/**
	 * Get: listaGridCliente.
	 *
	 * @return listaGridCliente
	 */
	public List<ListarClientesDTO> getListaGridCliente() {
		return listaGridCliente;
	}

	/**
	 * Set: listaGridCliente.
	 *
	 * @param listaGridCliente the lista grid cliente
	 */
	public void setListaGridCliente(List<ListarClientesDTO> listaGridCliente) {
		this.listaGridCliente = listaGridCliente;
	}

	/**
	 * Get: listaGridInconsistentes.
	 *
	 * @return listaGridInconsistentes
	 */
	public List<InconsistentesRetornoDTO> getListaGridInconsistentes() {
		return listaGridInconsistentes;
	}

	/**
	 * Set: listaGridInconsistentes.
	 *
	 * @param listaGridInconsistentes the lista grid inconsistentes
	 */
	public void setListaGridInconsistentes(List<InconsistentesRetornoDTO> listaGridInconsistentes) {
		this.listaGridInconsistentes = listaGridInconsistentes;
	}

	/**
	 * Get: listaGridOcorrencia.
	 *
	 * @return listaGridOcorrencia
	 */
	public List<OcorrenciaDTO> getListaGridOcorrencia() {
		return listaGridOcorrencia;
	}

	/**
	 * Set: listaGridOcorrencia.
	 *
	 * @param listaGridOcorrencia the lista grid ocorrencia
	 */
	public void setListaGridOcorrencia(List<OcorrenciaDTO> listaGridOcorrencia) {
		this.listaGridOcorrencia = listaGridOcorrencia;
	}

	/**
	 * Get: listaGridRetorno.
	 *
	 * @return listaGridRetorno
	 */
	public List<ArquivoRetornoDTO> getListaGridRetorno() {
		return listaGridRetorno;
	}

	/**
	 * Set: listaGridRetorno.
	 *
	 * @param listaGridRetorno the lista grid retorno
	 */
	public void setListaGridRetorno(List<ArquivoRetornoDTO> listaGridRetorno) {
		this.listaGridRetorno = listaGridRetorno;
	}

	/**
	 * Get: listaLote.
	 *
	 * @return listaLote
	 */
	public List<LoteRetornoDTO> getListaLote() {
		return listaLote;
	}

	/**
	 * Set: listaLote.
	 *
	 * @param listaLote the lista lote
	 */
	public void setListaLote(List<LoteRetornoDTO> listaLote) {
		this.listaLote = listaLote;
	}

	/**
	 * Get: listaOcorrencia.
	 *
	 * @return listaOcorrencia
	 */
	public List<OcorrenciaDTO> getListaOcorrencia() {
		return listaOcorrencia;
	}

	/**
	 * Set: listaOcorrencia.
	 *
	 * @param listaOcorrencia the lista ocorrencia
	 */
	public void setListaOcorrencia(List<OcorrenciaDTO> listaOcorrencia) {
		this.listaOcorrencia = listaOcorrencia;
	}

	/**
	 * Get: listaProdutoAux.
	 *
	 * @return listaProdutoAux
	 */
	public List<ComboProdutoDTO> getListaProdutoAux() {
		return listaProdutoAux;
	}

	/**
	 * Set: listaProdutoAux.
	 *
	 * @param listaProdutoAux the lista produto aux
	 */
	public void setListaProdutoAux(List<ComboProdutoDTO> listaProdutoAux) {
		this.listaProdutoAux = listaProdutoAux;
	}

	/**
	 * Get: modalidadePagamento.
	 *
	 * @return modalidadePagamento
	 */
	public String getModalidadePagamento() {
		return modalidadePagamento;
	}

	/**
	 * Set: modalidadePagamento.
	 *
	 * @param modalidadePagamento the modalidade pagamento
	 */
	public void setModalidadePagamento(String modalidadePagamento) {
		this.modalidadePagamento = modalidadePagamento;
	}

	/**
	 * Get: modalidadePagamentoLote.
	 *
	 * @return modalidadePagamentoLote
	 */
	public String getModalidadePagamentoLote() {
		return modalidadePagamentoLote;
	}

	/**
	 * Set: modalidadePagamentoLote.
	 *
	 * @param modalidadePagamentoLote the modalidade pagamento lote
	 */
	public void setModalidadePagamentoLote(String modalidadePagamentoLote) {
		this.modalidadePagamentoLote = modalidadePagamentoLote;
	}

	/**
	 * Is mostra botoes0007.
	 *
	 * @return true, if is mostra botoes0007
	 */
	public boolean isMostraBotoes0007() {
		return mostraBotoes0007;
	}

	/**
	 * Set: mostraBotoes0007.
	 *
	 * @param mostraBotoes0007 the mostra botoes0007
	 */
	public void setMostraBotoes0007(boolean mostraBotoes0007) {
		this.mostraBotoes0007 = mostraBotoes0007;
	}

	/**
	 * Is mostra botoes0008.
	 *
	 * @return true, if is mostra botoes0008
	 */
	public boolean isMostraBotoes0008() {
		return mostraBotoes0008;
	}

	/**
	 * Set: mostraBotoes0008.
	 *
	 * @param mostraBotoes0008 the mostra botoes0008
	 */
	public void setMostraBotoes0008(boolean mostraBotoes0008) {
		this.mostraBotoes0008 = mostraBotoes0008;
	}

	/**
	 * Is mostra botoes0010.
	 *
	 * @return true, if is mostra botoes0010
	 */
	public boolean isMostraBotoes0010() {
		return mostraBotoes0010;
	}

	/**
	 * Set: mostraBotoes0010.
	 *
	 * @param mostraBotoes0010 the mostra botoes0010
	 */
	public void setMostraBotoes0010(boolean mostraBotoes0010) {
		this.mostraBotoes0010 = mostraBotoes0010;
	}

	/**
	 * Is mostra botoes0011.
	 *
	 * @return true, if is mostra botoes0011
	 */
	public boolean isMostraBotoes0011() {
		return mostraBotoes0011;
	}

	/**
	 * Set: mostraBotoes0011.
	 *
	 * @param mostraBotoes0011 the mostra botoes0011
	 */
	public void setMostraBotoes0011(boolean mostraBotoes0011) {
		this.mostraBotoes0011 = mostraBotoes0011;
	}

	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}

	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	/**
	 * Get: nomeRazaoIdentificaCliente.
	 *
	 * @return nomeRazaoIdentificaCliente
	 */
	public String getNomeRazaoIdentificaCliente() {
		return nomeRazaoIdentificaCliente;
	}

	/**
	 * Set: nomeRazaoIdentificaCliente.
	 *
	 * @param nomeRazaoIdentificaCliente the nome razao identifica cliente
	 */
	public void setNomeRazaoIdentificaCliente(String nomeRazaoIdentificaCliente) {
		this.nomeRazaoIdentificaCliente = nomeRazaoIdentificaCliente;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: operacoesEfetivadasConsistentesQtde.
	 *
	 * @return operacoesEfetivadasConsistentesQtde
	 */
	public int getOperacoesEfetivadasConsistentesQtde() {
		return operacoesEfetivadasConsistentesQtde;
	}

	/**
	 * Set: operacoesEfetivadasConsistentesQtde.
	 *
	 * @param operacoesEfetivadasConsistentesQtde the operacoes efetivadas consistentes qtde
	 */
	public void setOperacoesEfetivadasConsistentesQtde(int operacoesEfetivadasConsistentesQtde) {
		this.operacoesEfetivadasConsistentesQtde = operacoesEfetivadasConsistentesQtde;
	}

	/**
	 * Get: operacoesEfetivadasConsistentesValor.
	 *
	 * @return operacoesEfetivadasConsistentesValor
	 */
	public double getOperacoesEfetivadasConsistentesValor() {
		return operacoesEfetivadasConsistentesValor;
	}

	/**
	 * Set: operacoesEfetivadasConsistentesValor.
	 *
	 * @param operacoesEfetivadasConsistentesValor the operacoes efetivadas consistentes valor
	 */
	public void setOperacoesEfetivadasConsistentesValor(double operacoesEfetivadasConsistentesValor) {
		this.operacoesEfetivadasConsistentesValor = operacoesEfetivadasConsistentesValor;
	}

	/**
	 * Get: operacoesNaoEfetivadasInconsistentesQtde.
	 *
	 * @return operacoesNaoEfetivadasInconsistentesQtde
	 */
	public int getOperacoesNaoEfetivadasInconsistentesQtde() {
		return operacoesNaoEfetivadasInconsistentesQtde;
	}

	/**
	 * Set: operacoesNaoEfetivadasInconsistentesQtde.
	 *
	 * @param operacoesNaoEfetivadasInconsistentesQtde the operacoes nao efetivadas inconsistentes qtde
	 */
	public void setOperacoesNaoEfetivadasInconsistentesQtde(int operacoesNaoEfetivadasInconsistentesQtde) {
		this.operacoesNaoEfetivadasInconsistentesQtde = operacoesNaoEfetivadasInconsistentesQtde;
	}

	/**
	 * Get: operacoesNaoEfetivadasInconsistentesValor.
	 *
	 * @return operacoesNaoEfetivadasInconsistentesValor
	 */
	public double getOperacoesNaoEfetivadasInconsistentesValor() {
		return operacoesNaoEfetivadasInconsistentesValor;
	}

	/**
	 * Set: operacoesNaoEfetivadasInconsistentesValor.
	 *
	 * @param operacoesNaoEfetivadasInconsistentesValor the operacoes nao efetivadas inconsistentes valor
	 */
	public void setOperacoesNaoEfetivadasInconsistentesValor(double operacoesNaoEfetivadasInconsistentesValor) {
		this.operacoesNaoEfetivadasInconsistentesValor = operacoesNaoEfetivadasInconsistentesValor;
	}

	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public String getPerfil() {
		return perfil;
	}

	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	/**
	 * Get: perfilFiltro.
	 *
	 * @return perfilFiltro
	 */
	public String getPerfilFiltro() {
		return perfilFiltro;
	}

	/**
	 * Set: perfilFiltro.
	 *
	 * @param perfilFiltro the perfil filtro
	 */
	public void setPerfilFiltro(String perfilFiltro) {
		this.perfilFiltro = perfilFiltro;
	}

	/**
	 * Get: processamento.
	 *
	 * @return processamento
	 */
	public String getProcessamento() {
		return processamento;
	}

	/**
	 * Set: processamento.
	 *
	 * @param processamento the processamento
	 */
	public void setProcessamento(String processamento) {
		this.processamento = processamento;
	}

	/**
	 * Get: processamentoLote.
	 *
	 * @return processamentoLote
	 */
	public String getProcessamentoLote() {
		return processamentoLote;
	}

	/**
	 * Set: processamentoLote.
	 *
	 * @param processamentoLote the processamento lote
	 */
	public void setProcessamentoLote(String processamentoLote) {
		this.processamentoLote = processamentoLote;
	}

	/**
	 * Get: produto.
	 *
	 * @return produto
	 */
	public String getProduto() {
		return produto;
	}

	/**
	 * Set: produto.
	 *
	 * @param produto the produto
	 */
	public void setProduto(String produto) {
		this.produto = produto;
	}

	/**
	 * Get: produtoFiltro.
	 *
	 * @return produtoFiltro
	 */
	public String getProdutoFiltro() {
		return produtoFiltro;
	}

	/**
	 * Set: produtoFiltro.
	 *
	 * @param produtoFiltro the produto filtro
	 */
	public void setProdutoFiltro(String produtoFiltro) {
		this.produtoFiltro = produtoFiltro;
	}

	/**
	 * Get: qtdConsistentes.
	 *
	 * @return qtdConsistentes
	 */
	public String getQtdConsistentes() {
		return qtdConsistentes;
	}

	/**
	 * Set: qtdConsistentes.
	 *
	 * @param qtdConsistentes the qtd consistentes
	 */
	public void setQtdConsistentes(String qtdConsistentes) {
		this.qtdConsistentes = qtdConsistentes;
	}

	/**
	 * Get: qtdInconsistentes.
	 *
	 * @return qtdInconsistentes
	 */
	public String getQtdInconsistentes() {
		return qtdInconsistentes;
	}

	/**
	 * Set: qtdInconsistentes.
	 *
	 * @param qtdInconsistentes the qtd inconsistentes
	 */
	public void setQtdInconsistentes(String qtdInconsistentes) {
		this.qtdInconsistentes = qtdInconsistentes;
	}

	/**
	 * Get: qtdRegistrosLote.
	 *
	 * @return qtdRegistrosLote
	 */
	public String getQtdRegistrosLote() {
		return qtdRegistrosLote;
	}

	/**
	 * Set: qtdRegistrosLote.
	 *
	 * @param qtdRegistrosLote the qtd registros lote
	 */
	public void setQtdRegistrosLote(String qtdRegistrosLote) {
		this.qtdRegistrosLote = qtdRegistrosLote;
	}

	/**
	 * Get: qtdRegistrosRetorno.
	 *
	 * @return qtdRegistrosRetorno
	 */
	public String getQtdRegistrosRetorno() {
		return qtdRegistrosRetorno;
	}

	/**
	 * Set: qtdRegistrosRetorno.
	 *
	 * @param qtdRegistrosRetorno the qtd registros retorno
	 */
	public void setQtdRegistrosRetorno(String qtdRegistrosRetorno) {
		this.qtdRegistrosRetorno = qtdRegistrosRetorno;
	}

	/**
	 * Get: razao.
	 *
	 * @return razao
	 */
	public String getRazao() {
		return razao;
	}

	/**
	 * Set: razao.
	 *
	 * @param razao the razao
	 */
	public void setRazao(String razao) {
		this.razao = razao;
	}

	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}

	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}

	/**
	 * Get: resultadoProcessamentoLote.
	 *
	 * @return resultadoProcessamentoLote
	 */
	public String getResultadoProcessamentoLote() {
		return resultadoProcessamentoLote;
	}

	/**
	 * Set: resultadoProcessamentoLote.
	 *
	 * @param resultadoProcessamentoLote the resultado processamento lote
	 */
	public void setResultadoProcessamentoLote(String resultadoProcessamentoLote) {
		this.resultadoProcessamentoLote = resultadoProcessamentoLote;
	}

	/**
	 * Get: sequenciaLote.
	 *
	 * @return sequenciaLote
	 */
	public String getSequenciaLote() {
		return sequenciaLote;
	}

	/**
	 * Set: sequenciaLote.
	 *
	 * @param sequenciaLote the sequencia lote
	 */
	public void setSequenciaLote(String sequenciaLote) {
		this.sequenciaLote = sequenciaLote;
	}

	/**
	 * Get: sequenciaRegistros.
	 *
	 * @return sequenciaRegistros
	 */
	public String getSequenciaRegistros() {
		return sequenciaRegistros;
	}

	/**
	 * Set: sequenciaRegistros.
	 *
	 * @param sequenciaRegistros the sequencia registros
	 */
	public void setSequenciaRegistros(String sequenciaRegistros) {
		this.sequenciaRegistros = sequenciaRegistros;
	}

	/**
	 * Get: sequenciaRetorno.
	 *
	 * @return sequenciaRetorno
	 */
	public String getSequenciaRetorno() {
		return sequenciaRetorno;
	}

	/**
	 * Set: sequenciaRetorno.
	 *
	 * @param sequenciaRetorno the sequencia retorno
	 */
	public void setSequenciaRetorno(String sequenciaRetorno) {
		this.sequenciaRetorno = sequenciaRetorno;
	}

	/**
	 * Get: sequenciaRetornoFiltro.
	 *
	 * @return sequenciaRetornoFiltro
	 */
	public String getSequenciaRetornoFiltro() {
		return sequenciaRetornoFiltro;
	}

	/**
	 * Set: sequenciaRetornoFiltro.
	 *
	 * @param sequenciaRetornoFiltro the sequencia retorno filtro
	 */
	public void setSequenciaRetornoFiltro(String sequenciaRetornoFiltro) {
		this.sequenciaRetornoFiltro = sequenciaRetornoFiltro;
	}

	/**
	 * Get: situacaoFiltro.
	 *
	 * @return situacaoFiltro
	 */
	public String getSituacaoFiltro() {
		return situacaoFiltro;
	}

	/**
	 * Set: situacaoFiltro.
	 *
	 * @param situacaoFiltro the situacao filtro
	 */
	public void setSituacaoFiltro(String situacaoFiltro) {
		this.situacaoFiltro = situacaoFiltro;
	}

	/**
	 * Get: situacaoProcessamentoLote.
	 *
	 * @return situacaoProcessamentoLote
	 */
	public String getSituacaoProcessamentoLote() {
		return situacaoProcessamentoLote;
	}

	/**
	 * Set: situacaoProcessamentoLote.
	 *
	 * @param situacaoProcessamentoLote the situacao processamento lote
	 */
	public void setSituacaoProcessamentoLote(String situacaoProcessamentoLote) {
		this.situacaoProcessamentoLote = situacaoProcessamentoLote;
	}

	/**
	 * Get: situacaoProcessamentoRetorno.
	 *
	 * @return situacaoProcessamentoRetorno
	 */
	public String getSituacaoProcessamentoRetorno() {
		return situacaoProcessamentoRetorno;
	}

	/**
	 * Set: situacaoProcessamentoRetorno.
	 *
	 * @param situacaoProcessamentoRetorno the situacao processamento retorno
	 */
	public void setSituacaoProcessamentoRetorno(String situacaoProcessamentoRetorno) {
		this.situacaoProcessamentoRetorno = situacaoProcessamentoRetorno;
	}

	/**
	 * Get: situacaoRetorno.
	 *
	 * @return situacaoRetorno
	 */
	public String getSituacaoRetorno() {
		return situacaoRetorno;
	}

	/**
	 * Set: situacaoRetorno.
	 *
	 * @param situacaoRetorno the situacao retorno
	 */
	public void setSituacaoRetorno(String situacaoRetorno) {
		this.situacaoRetorno = situacaoRetorno;
	}

	/**
	 * Get: tipoRetorno.
	 *
	 * @return tipoRetorno
	 */
	public String getTipoRetorno() {
		return tipoRetorno;
	}

	/**
	 * Set: tipoRetorno.
	 *
	 * @param tipoRetorno the tipo retorno
	 */
	public void setTipoRetorno(String tipoRetorno) {
		this.tipoRetorno = tipoRetorno;
	}

	/**
	 * Get: tipoRetornoFiltro.
	 *
	 * @return tipoRetornoFiltro
	 */
	public String getTipoRetornoFiltro() {
		return tipoRetornoFiltro;
	}

	/**
	 * Set: tipoRetornoFiltro.
	 *
	 * @param tipoRetornoFiltro the tipo retorno filtro
	 */
	public void setTipoRetornoFiltro(String tipoRetornoFiltro) {
		this.tipoRetornoFiltro = tipoRetornoFiltro;
	}

	/**
	 * Get: totalGeralQuantidade.
	 *
	 * @return totalGeralQuantidade
	 */
	public int getTotalGeralQuantidade() {
		return totalGeralQuantidade;
	}

	/**
	 * Set: totalGeralQuantidade.
	 *
	 * @param totalGeralQuantidade the total geral quantidade
	 */
	public void setTotalGeralQuantidade(int totalGeralQuantidade) {
		this.totalGeralQuantidade = totalGeralQuantidade;
	}

	/**
	 * Get: totalGeralValor.
	 *
	 * @return totalGeralValor
	 */
	public double getTotalGeralValor() {
		return totalGeralValor;
	}

	/**
	 * Set: totalGeralValor.
	 *
	 * @param totalGeralValor the total geral valor
	 */
	public void setTotalGeralValor(double totalGeralValor) {
		this.totalGeralValor = totalGeralValor;
	}

	/**
	 * Get: validaCpf.
	 *
	 * @return validaCpf
	 */
	public String getValidaCpf() {
		return validaCpf;
	}

	/**
	 * Set: validaCpf.
	 *
	 * @param validaCpf the valida cpf
	 */
	public void setValidaCpf(String validaCpf) {
		this.validaCpf = validaCpf;
	}

	/**
	 * Get: validaCpfCliente.
	 *
	 * @return validaCpfCliente
	 */
	public String getValidaCpfCliente() {
		return validaCpfCliente;
	}

	/**
	 * Set: validaCpfCliente.
	 *
	 * @param validaCpfCliente the valida cpf cliente
	 */
	public void setValidaCpfCliente(String validaCpfCliente) {
		this.validaCpfCliente = validaCpfCliente;
	}

	/**
	 * Get: valorConsistentes.
	 *
	 * @return valorConsistentes
	 */
	public String getValorConsistentes() {
		return valorConsistentes;
	}

	/**
	 * Set: valorConsistentes.
	 *
	 * @param valorConsistentes the valor consistentes
	 */
	public void setValorConsistentes(String valorConsistentes) {
		this.valorConsistentes = valorConsistentes;
	}

	/**
	 * Get: valorInconsistentes.
	 *
	 * @return valorInconsistentes
	 */
	public String getValorInconsistentes() {
		return valorInconsistentes;
	}

	/**
	 * Set: valorInconsistentes.
	 *
	 * @param valorInconsistentes the valor inconsistentes
	 */
	public void setValorInconsistentes(String valorInconsistentes) {
		this.valorInconsistentes = valorInconsistentes;
	}

	/**
	 * Get: valorTotalLote.
	 *
	 * @return valorTotalLote
	 */
	public String getValorTotalLote() {
		return valorTotalLote;
	}

	/**
	 * Set: valorTotalLote.
	 *
	 * @param valorTotalLote the valor total lote
	 */
	public void setValorTotalLote(String valorTotalLote) {
		this.valorTotalLote = valorTotalLote;
	}

	/**
	 * Get: valorTotalRetorno.
	 *
	 * @return valorTotalRetorno
	 */
	public String getValorTotalRetorno() {
		return valorTotalRetorno;
	}

	/**
	 * Set: valorTotalRetorno.
	 *
	 * @param valorTotalRetorno the valor total retorno
	 */
	public void setValorTotalRetorno(String valorTotalRetorno) {
		this.valorTotalRetorno = valorTotalRetorno;
	}

	/**
	 * Get: listaProduto.
	 *
	 * @return listaProduto
	 */
	public List<SelectItem> getListaProduto() {
		loadComboProduto();
		return listaProduto;
	}

	/**
	 * Set: listaProduto.
	 *
	 * @param listaProduto the lista produto
	 */
	public void setListaProduto(List<SelectItem> listaProduto) {
		this.listaProduto = listaProduto;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public String getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(String itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Is desab perfil.
	 *
	 * @return true, if is desab perfil
	 */
	public boolean isDesabPerfil() {
		return desabPerfil;
	}

	/**
	 * Set: desabPerfil.
	 *
	 * @param desabPerfil the desab perfil
	 */
	public void setDesabPerfil(boolean desabPerfil) {
		this.desabPerfil = desabPerfil;
	}

	/**
	 * Is desab retorno.
	 *
	 * @return true, if is desab retorno
	 */
	public boolean isDesabRetorno() {
		return desabRetorno;
	}

	/**
	 * Set: desabRetorno.
	 *
	 * @param desabRetorno the desab retorno
	 */
	public void setDesabRetorno(boolean desabRetorno) {
		this.desabRetorno = desabRetorno;
	}
	
	/**
	 * Is desab sit proc ret.
	 *
	 * @return true, if is desab sit proc ret
	 */
	public boolean isDesabSitProcRet() {
		return desabSitProcRet;
	}

	/**
	 * Set: desabSitProcRet.
	 *
	 * @param desabSitProcRet the desab sit proc ret
	 */
	public void setDesabSitProcRet(boolean desabSitProcRet) {
		this.desabSitProcRet = desabSitProcRet;
	}

	/**
	 * Is desab inconsistente.
	 *
	 * @return true, if is desab inconsistente
	 */
	public boolean isDesabInconsistente() {
		return desabInconsistente;
	}

	/**
	 * Set: desabInconsistente.
	 *
	 * @param desabInconsistente the desab inconsistente
	 */
	public void setDesabInconsistente(boolean desabInconsistente) {
		this.desabInconsistente = desabInconsistente;
	}

	/**
	 * Get: logger.
	 *
	 * @return logger
	 */
	public ILogManager getLogger() {
		return logger;
	}

	/**
	 * Set: logger.
	 *
	 * @param logger the logger
	 */
	public void setLogger(ILogManager logger) {
		this.logger = logger;
	}
	
	/**
	 * Voltar remessa.
	 *
	 * @return the string
	 */
	public String voltarRemessa(){
		this.loadComboProduto();
		this.findListaLote();
		
		return "VOLTAR_PESQUISA";
	}
	
	/**
	 * Voltar lote.
	 *
	 * @return the string
	 */
	public String voltarLote(){
		ArquivoRetornoDTO arquivoRetornoDTO = getListaGridRetorno().get(getCodListaRetornoRadio());

		findLotes(arquivoRetornoDTO);

		return "VOLTAR_PESQUISA_DADOS";
	}
	
	/**
	 * Voltar inconsistencia.
	 *
	 * @return the string
	 */
	public String voltarInconsistencia(){
		this.findInconsistencias();
		
		return "VOLTAR_CMPI0010";
	}

}