/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.exceptions
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.exceptions;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * Nome: ConOcorrenciaServiceException
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConOcorrenciaServiceException extends
		BradescoApplicationException {

	/**
	 * Con ocorrencia service exception.
	 */
	public ConOcorrenciaServiceException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con ocorrencia service exception.
	 *
	 * @param message the message
	 */
	public ConOcorrenciaServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con ocorrencia service exception.
	 *
	 * @param cause the cause
	 */
	public ConOcorrenciaServiceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con ocorrencia service exception.
	 *
	 * @param message the message
	 * @param code the code
	 */
	public ConOcorrenciaServiceException(String message, String code) {
		super(message, code);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con ocorrencia service exception.
	 *
	 * @param cause the cause
	 * @param code the code
	 */
	public ConOcorrenciaServiceException(Throwable cause, String code) {
		super(cause, code);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con ocorrencia service exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public ConOcorrenciaServiceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con ocorrencia service exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param code the code
	 */
	public ConOcorrenciaServiceException(String message, Throwable cause,
			String code) {
		super(message, cause, code);
		// TODO Auto-generated constructor stub
	}
}
