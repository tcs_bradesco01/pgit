/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.consultas;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarInformacoesFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarInformacoesFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Consultas
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IConsultasService {
	
    /**
     * Consultar desc banco agencia conta.
     *
     * @param consultarDescBancoAgenciaContaEntradaDTO the consultar desc banco agencia conta entrada dto
     * @return the consultar desc banco agencia conta saida dto
     */
    ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaConta(ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO);
    
    /**
     * Consultar informacoes favorecido.
     *
     * @param entradaDTO the entrada dto
     * @return the consultar informacoes favorecido saida dto
     */
    ConsultarInformacoesFavorecidoSaidaDTO consultarInformacoesFavorecido(ConsultarInformacoesFavorecidoEntradaDTO entradaDTO);
    
    /**
     * Detalhar dados contrato.
     *
     * @param entradaDTO the entrada dto
     * @return the detalhar dados contrato saida dto
     */
    DetalharDadosContratoSaidaDTO detalharDadosContrato(DetalharDadosContratoEntradaDTO entradaDTO);
    
    /**
     * Consultar email.
     *
     * @param entrada the entrada
     * @return the list< consultar email saida dt o>
     */
    List<ConsultarEmailSaidaDTO> consultarEmail(ConsultarEmailEntradaDTO entrada);
    
    /**
     * Consultar endereco.
     *
     * @param entrada the entrada
     * @return the list< consultar endereco saida dt o>
     */
    List<ConsultarEnderecoSaidaDTO> consultarEndereco(ConsultarEnderecoEntradaDTO entrada);
    
    /**
     * Consultar desc grupo economico.
     *
     * @param entradaDTO the entrada dto
     * @return the consultar desc grupo economico saida dto
     */
    ConsultarDescGrupoEconomicoSaidaDTO consultarDescGrupoEconomico(ConsultarDescGrupoEconomicoEntradaDTO entradaDTO);

    /**
     * Listar tipo retirada.
     * 
     * @param entrada
     *            the entrada
     * @return the listar tipo retirada saida dto
     */
    ListarTipoRetiradaSaidaDTO listarTipoRetirada(ListarTipoRetiradaEntradaDTO entrada);
}

