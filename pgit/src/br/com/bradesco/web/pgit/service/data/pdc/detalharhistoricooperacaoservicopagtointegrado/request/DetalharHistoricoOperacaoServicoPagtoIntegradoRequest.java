/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharHistoricoOperacaoServicoPagtoIntegradoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharHistoricoOperacaoServicoPagtoIntegradoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoServicoOperacional
     */
    private int _cdProdutoServicoOperacional = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacional
     */
    private boolean _has_cdProdutoServicoOperacional;

    /**
     * Field _cdProdutoOperacionalRelacionado
     */
    private int _cdProdutoOperacionalRelacionado = 0;

    /**
     * keeps track of state for field:
     * _cdProdutoOperacionalRelacionado
     */
    private boolean _has_cdProdutoOperacionalRelacionado;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _hrInclusaoRegistroHistorico
     */
    private java.lang.String _hrInclusaoRegistroHistorico;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharHistoricoOperacaoServicoPagtoIntegradoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.request.DetalharHistoricoOperacaoServicoPagtoIntegradoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdProdutoOperacionalRelacionado
     * 
     */
    public void deleteCdProdutoOperacionalRelacionado()
    {
        this._has_cdProdutoOperacionalRelacionado= false;
    } //-- void deleteCdProdutoOperacionalRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacional
     * 
     */
    public void deleteCdProdutoServicoOperacional()
    {
        this._has_cdProdutoServicoOperacional= false;
    } //-- void deleteCdProdutoServicoOperacional() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field
     * 'cdProdutoOperacionalRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacionalRelacionado'.
     */
    public int getCdProdutoOperacionalRelacionado()
    {
        return this._cdProdutoOperacionalRelacionado;
    } //-- int getCdProdutoOperacionalRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacional'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacional'.
     */
    public int getCdProdutoServicoOperacional()
    {
        return this._cdProdutoServicoOperacional;
    } //-- int getCdProdutoServicoOperacional() 

    /**
     * Returns the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistroHistorico'.
     */
    public java.lang.String getHrInclusaoRegistroHistorico()
    {
        return this._hrInclusaoRegistroHistorico;
    } //-- java.lang.String getHrInclusaoRegistroHistorico() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdProdutoOperacionalRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacionalRelacionado()
    {
        return this._has_cdProdutoOperacionalRelacionado;
    } //-- boolean hasCdProdutoOperacionalRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacional()
    {
        return this._has_cdProdutoServicoOperacional;
    } //-- boolean hasCdProdutoServicoOperacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdProdutoOperacionalRelacionado'.
     * 
     * @param cdProdutoOperacionalRelacionado the value of field
     * 'cdProdutoOperacionalRelacionado'.
     */
    public void setCdProdutoOperacionalRelacionado(int cdProdutoOperacionalRelacionado)
    {
        this._cdProdutoOperacionalRelacionado = cdProdutoOperacionalRelacionado;
        this._has_cdProdutoOperacionalRelacionado = true;
    } //-- void setCdProdutoOperacionalRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacional'.
     * 
     * @param cdProdutoServicoOperacional the value of field
     * 'cdProdutoServicoOperacional'.
     */
    public void setCdProdutoServicoOperacional(int cdProdutoServicoOperacional)
    {
        this._cdProdutoServicoOperacional = cdProdutoServicoOperacional;
        this._has_cdProdutoServicoOperacional = true;
    } //-- void setCdProdutoServicoOperacional(int) 

    /**
     * Sets the value of field 'hrInclusaoRegistroHistorico'.
     * 
     * @param hrInclusaoRegistroHistorico the value of field
     * 'hrInclusaoRegistroHistorico'.
     */
    public void setHrInclusaoRegistroHistorico(java.lang.String hrInclusaoRegistroHistorico)
    {
        this._hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
    } //-- void setHrInclusaoRegistroHistorico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharHistoricoOperacaoServicoPagtoIntegradoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.request.DetalharHistoricoOperacaoServicoPagtoIntegradoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.request.DetalharHistoricoOperacaoServicoPagtoIntegradoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.request.DetalharHistoricoOperacaoServicoPagtoIntegradoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharhistoricooperacaoservicopagtointegrado.request.DetalharHistoricoOperacaoServicoPagtoIntegradoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
