/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * The Class DetalharPiramideOcorrencias3SaidaDTO.
 */
public class DetalharPiramideOcorrencias3SaidaDTO {

    /** The ds logradouro filial quest. */
    private String dsLogradouroFilialQuest;

    /** The ds bairro filial quest. */
    private String dsBairroFilialQuest;

    /** The cd municipio filial quest. */
    private Long cdMunicipioFilialQuest;

    /** The ds munc filial quest. */
    private String dsMuncFilialQuest;

    /** The cd cep filial quest. */
    private Integer cdCepFilialQuest;

    /** The cd cep compl quesf. */
    private Integer cdCepComplQuesf;

    /** The cd uf filial quest. */
    private Integer cdUfFilialQuest;

    /** The cd segmento filial quest. */
    private String cdSegmentoFilialQuest;

    /** The qtd func filal quest. */
    private Long qtdFuncFilalQuest;

    /**
     * Set ds logradouro filial quest.
     * 
     * @param dsLogradouroFilialQuest
     *            the ds logradouro filial quest
     */
    public void setDsLogradouroFilialQuest(String dsLogradouroFilialQuest) {
        this.dsLogradouroFilialQuest = dsLogradouroFilialQuest;
    }

    /**
     * Get ds logradouro filial quest.
     * 
     * @return the ds logradouro filial quest
     */
    public String getDsLogradouroFilialQuest() {
        return this.dsLogradouroFilialQuest;
    }

    /**
     * Set ds bairro filial quest.
     * 
     * @param dsBairroFilialQuest
     *            the ds bairro filial quest
     */
    public void setDsBairroFilialQuest(String dsBairroFilialQuest) {
        this.dsBairroFilialQuest = dsBairroFilialQuest;
    }

    /**
     * Get ds bairro filial quest.
     * 
     * @return the ds bairro filial quest
     */
    public String getDsBairroFilialQuest() {
        return this.dsBairroFilialQuest;
    }

    /**
     * Set cd municipio filial quest.
     * 
     * @param cdMunicipioFilialQuest
     *            the cd municipio filial quest
     */
    public void setCdMunicipioFilialQuest(Long cdMunicipioFilialQuest) {
        this.cdMunicipioFilialQuest = cdMunicipioFilialQuest;
    }

    /**
     * Get cd municipio filial quest.
     * 
     * @return the cd municipio filial quest
     */
    public Long getCdMunicipioFilialQuest() {
        return this.cdMunicipioFilialQuest;
    }

    /**
     * Set ds munc filial quest.
     * 
     * @param dsMuncFilialQuest
     *            the ds munc filial quest
     */
    public void setDsMuncFilialQuest(String dsMuncFilialQuest) {
        this.dsMuncFilialQuest = dsMuncFilialQuest;
    }

    /**
     * Get ds munc filial quest.
     * 
     * @return the ds munc filial quest
     */
    public String getDsMuncFilialQuest() {
        return this.dsMuncFilialQuest;
    }

    /**
     * Set cd cep filial quest.
     * 
     * @param cdCepFilialQuest
     *            the cd cep filial quest
     */
    public void setCdCepFilialQuest(Integer cdCepFilialQuest) {
        this.cdCepFilialQuest = cdCepFilialQuest;
    }

    /**
     * Get cd cep filial quest.
     * 
     * @return the cd cep filial quest
     */
    public Integer getCdCepFilialQuest() {
        return this.cdCepFilialQuest;
    }

    /**
     * Set cd cep compl quesf.
     * 
     * @param cdCepComplQuesf
     *            the cd cep compl quesf
     */
    public void setCdCepComplQuesf(Integer cdCepComplQuesf) {
        this.cdCepComplQuesf = cdCepComplQuesf;
    }

    /**
     * Get cd cep compl quesf.
     * 
     * @return the cd cep compl quesf
     */
    public Integer getCdCepComplQuesf() {
        return this.cdCepComplQuesf;
    }

    /**
     * Set cd uf filial quest.
     * 
     * @param cdUfFilialQuest
     *            the cd uf filial quest
     */
    public void setCdUfFilialQuest(Integer cdUfFilialQuest) {
        this.cdUfFilialQuest = cdUfFilialQuest;
    }

    /**
     * Get cd uf filial quest.
     * 
     * @return the cd uf filial quest
     */
    public Integer getCdUfFilialQuest() {
        return this.cdUfFilialQuest;
    }

    /**
     * Set cd segmento filial quest.
     * 
     * @param cdSegmentoFilialQuest
     *            the cd segmento filial quest
     */
    public void setCdSegmentoFilialQuest(String cdSegmentoFilialQuest) {
        this.cdSegmentoFilialQuest = cdSegmentoFilialQuest;
    }

    /**
     * Get cd segmento filial quest.
     * 
     * @return the cd segmento filial quest
     */
    public String getCdSegmentoFilialQuest() {
        return this.cdSegmentoFilialQuest;
    }

    /**
     * Set qtd func filal quest.
     * 
     * @param qtdFuncFilalQuest
     *            the qtd func filal quest
     */
    public void setQtdFuncFilalQuest(Long qtdFuncFilalQuest) {
        this.qtdFuncFilalQuest = qtdFuncFilalQuest;
    }

    /**
     * Get qtd func filal quest.
     * 
     * @return the qtd func filal quest
     */
    public Long getQtdFuncFilalQuest() {
        return this.qtdFuncFilalQuest;
    }
    
    /**
     * Get cep.
     * 
     * @return the cep
     */
    public String getCep() {
        return PgitUtil.formatarCep(cdCepFilialQuest, cdCepComplQuesf);
    }
}